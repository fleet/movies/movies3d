# Welcome to the MOVIES3D C++ and GUI code repository

MOVIES3D is a software designed by [Ifremer](https://wwz.ifremer.fr/en) to [display and process underwater active acoustic data](http://flotte.ifremer.fr/content/download/6032/129677/file/MOVIES3D_general.pdf) produced by [sonars](https://en.wikipedia.org/wiki/Sonar) and [echosounders](https://en.wikipedia.org/wiki/Echo_sounding).

Please proceed to [the wiki](https://gitlab.ifremer.fr/fleet/movies/movies_script/-/wikis/home) for more information on how to get and use MOVIES3D and its APIs.

# How to build

Movies3D use CMake as build system.

Movies3D application can only be built for Windows platforms.

pyMovies python binding can be built for Windows and Linux platforms.

Consider _rootdir_ as the root directory (contains bin, extern and src subdirectories).

## Windows - MSVC 2015

Prerequisites :
* Installation of MSVC 2015 for C++ 
* CMake >= 3.25
* Matlab 2014
* Python 3.10.X
* python "build" package (pip install build)
* NSIS 3.09


Then consider that the following commands must be executed from a VS2015 prompt (Windows Start Menu > Visual Studio 2015 > Invite de commande des outils natifs x64 de VS2015).

To build extern, run commands :
* cd _rootdir_/extern
* mkdir build
* cd build
* cmake -G "Visual Studio 14 2015 Win64" ..
* cmake --build . --config Release

To generate Visual Studio 2015 solution, run commands : 
* cd _rootdir_
* mkdir build
* cd build
* cmake -G "Visual Studio 14 2015 Win64" ../src
* Then open the M3D.sln solution generated

The project can also be built from command line :
* cmake --build . --config Release

The Movies3D setup can be built using command line :
* cpack -G "NSIS"

The Movies3D archive can be built using command line :
* cpack -G "ZIP"

The pyMovies wheel can be built using command line :
* cmake --build . --config Release --target PythonLinkWheel


## Windows - MSVC 2022

Prerequisites :
* Installation of MSVC 2022 for C++ with additionnal components
** MSVC v140 - VS 2015 C++ Build Tools
** Outils C++ CMake pour Windows
* CMake >= 3.25
* Matlab 2014
* Python 3.10.X
* python "build" package (pip install build)


Then consider that the following commands must be executed from a VS2022 prompt (Windows Start Menu > Visual Studio 2022 > x64 Native Tools Command Prompt for VS 2022).

To build extern, run commands :
* cd _rootdir_/extern
* mkdir build
* cd build
* cmake -G "Visual Studio 17 2022" -A x64 -T v140 ..
* cmake --build . --config Release

To generate Visual Studio 2022 solution, run commands : 
* cd _rootdir_
* mkdir build
* cd build
* cmake -G "Visual Studio 17 2022" -A x64 -T v140 ../src
* The open the M3D.sln solution generated

The project can also be built form command line :
* cmake --build .

The Movies3D setup can be built using command line :
* cpack -G "NSIS"

The Movies3D archive can be built using command line :
* cpack -G "ZIP"

The pyMovies wheel can be built using command line :
* cmake --build . --config Release --target PythonLinkWheel


> Visual Studio 2022 support import of CMake projects (not tested)

## Linux

Prerequisites :
* docker

The project come with a docker image. The dockerfile is stored in _rootdir_/compiler

To build the image :
* cd _rootdir_
* docker build -t movies .

To run the image :
* docker run -it movies --mount source=.,target=src
 docker run -v C:\dev\workspaces\movies3d:/src -it movies



Then consider that the following commands must be executed in the created container.

Build extern :
* cd _rootdir_/extern
* mkdir build
* cd build
* cmake ..
* cmake --build .

The pyMovies wheel can be built using command line :
* cd _rootdir_
* mkdir build
* cd build
* cmake ../src -DCMAKE_BUILD_TYPE=Release
* cmake --build . --target PythonLinkWheel

# Run tests

Automated tests are stored in _rootdir_/src/Tests

See [README.md](src/Tests/REAMD.md) for more informations.

# Jenkins

The project come with jenkins file :
* Jenkinsfile.linux
* Jenkinsfile.windows

The jenkins file automate :
* build of extern and project
* execution of tests
* generation of Movies3D packaging (only on Windows)
* generation of pyMovies wheel

A jenkins agent 'IFREMER-M3D-CI' is required for Windows and Linux build.
This agent must be running on a Windows host.
The following prerequisites must be met on this host :
* For windows :
** Installation of MSVC 2015 for C++ 
** CMake >= 3.25
** Python 3.10.X
** NSIS 3.09
** Matlab 2014
* For linux :
** Docker