This directory contains docker image to build pyMovies under Linux.

This images is base on image mambaorg/micromamba.

The environnement file (requirements_compil.yml) install all required tools and dependencies.

See the root README.md of Movies3D project to build pyMovies for Linux.