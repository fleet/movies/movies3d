# How to build Boost Python Bindings

Prerequistes :
- on ubuntu, a symbolic link python must be available in path and link to python3 binary (python3 is not sufficient)
- on anaconda, a symbolic link must be available to link python3.7m include directory to python3.7

How to build :
- Consider <root> as the M3D extern subdirectory
- Download boost 1.71.0 sources
- extract boost zip sources in <root>
- from extern directory run commands :
```
export ROOT=/home/globe/Movies/movies3d/extern
cd $ROOT/boost_1_71_0/tools/build
./bootstrap.sh
./b2 install --prefix=$ROOT/boost_b2
export PATH=$PATH:$ROOT/boost_b2/bin
# Hack for anaconda to link to the right include dir
ln -s $CONDA_PREFIX/include/python3.7m $CONDA_PREFIX/include/python3.7
cd $ROOT/boost_1_71_0
mkdir build
b2 --build_dir=build --prefix=$ROOT/boost toolset=gcc --with-python install
```

Help :
https://www.boost.org/doc/libs/1_71_0/libs/python/doc/html/building/no_install_quickstart.html#building.no_install_quickstart.basic_procedure
