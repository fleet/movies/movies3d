Pour compiler M3D

1/ Les librairies doivent être compilés préalablement
- ffts

Pour cela :
- créer un sous-dossier "build" dans le dossier "extern"
- se positionner dans ce dossier "build"
- executer les comandes suivantes 
-- cmake -G "NMake Makefikes" ..
-- nmake


2/ Les librairies pré-compilées (boost, vtk, python, xercesc) doivent dans un 1er temps être dezippé tels quels (option 'extraire ici'), de sorte que l'arborescence du dossier extern contienent les dossiers et sous-dossiers suivants :

boost_1_60_0_vs2015/include
boost_1_60_0_vs2015/lib
Python/Python27
Python/Python27_x64
vtk-7.1.1-vs2015/bin
vtk-7.1.1-vs2015/include
vtk-7.1.1-vs2015/lib
xercesc/bin
xercesc/dom
xercesc/...

On utilisera désormais vtk-7.1.1-vs2015-opengl1.zip pour des raisons de compatibilités avec certains ordinateurs IFREMER.
vtk-7.1.1-vs2015-opengl2.zip est conservé pour permettre notamment de récupérer un vtk compilé avec les informations de debug pour AQtime.