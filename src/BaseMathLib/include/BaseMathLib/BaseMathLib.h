
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the BASEMATHLIB_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// BASEMATHLIB_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef WIN32
#ifdef BASEMATHLIB_EXPORTS
#define BASEMATHLIB_API __declspec(dllexport)
#define BASEMATHLIB_EXPIMP_TEMPLATE
#else
#define BASEMATHLIB_API __declspec(dllimport)
#define BASEMATHLIB_EXPIMP_TEMPLATE
#endif
#else
#define BASEMATHLIB_API
#define BASEMATHLIB_EXPIMP_TEMPLATE
#endif

// This class is exported from the BaseMathLib.dll
#ifndef BASEMATHLIB_EXPORTS_CLASS
#define BASEMATHLIB_EXPORTS_CLASS

#ifdef WIN32
#pragma warning( disable : 4244 ) 
#pragma warning( disable : 4267 ) 
#pragma warning( disable : 4251 ) 
#endif

#include <complex>
#include <vector>
#include <cmath>

BASEMATHLIB_API double round(double x);

typedef std::complex<float> complex_t;
typedef std::vector<complex_t> v_complex_t;

#endif
