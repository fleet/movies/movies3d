#pragma once
#include "BaseMathLib.h"
#include "BaseMathLib/Vector3.h"

#include <algorithm>

namespace BaseMathLib
{

	class BASEMATHLIB_API Box
	{
	public:
		Box();
		Box(const Vector3D & pointA, const Vector3D & pointB);
		~Box();

		void Extends(const Box &refBox);

		inline Box& operator= (const Box& rkBox)
		{
			m_minBox = rkBox.m_minBox;
			m_maxBox = rkBox.m_maxBox;
			return *this;
		}

		inline bool isEmpty() const
		{
			return m_minBox.x == m_maxBox.x && m_minBox.y == m_maxBox.y && m_minBox.z == m_maxBox.z;
		}

		inline void setEmpty()
		{
			m_minBox = m_maxBox = Vector3D(0.0);
		}

		// check if a point is contained in the box
		inline bool IsContained(const Vector3D &refPos) const
		{
			return m_minBox.z <= refPos.z && refPos.z <= m_maxBox.z && m_minBox.x <= refPos.x && refPos.x <= m_maxBox.x && m_minBox.y <= refPos.y && refPos.y <= m_maxBox.y;
		}

		Vector3D m_minBox;
		Vector3D m_maxBox;
	};
}


