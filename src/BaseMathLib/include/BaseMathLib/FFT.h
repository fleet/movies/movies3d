#pragma once

#include "BaseMathLib.h"

#include <complex>
#include <memory>

class BASEMATHLIB_API FFTComputer
{
public:
	static bool forward(v_complex_t & data);
	static bool backward(v_complex_t& data);
};

