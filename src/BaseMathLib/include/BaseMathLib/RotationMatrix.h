#include "BaseMathLib.h"

#ifndef BASEM_ROTATION_MATRIX
#define BASEM_ROTATION_MATRIX

#include "BaseMathLib/Vector3.h"

#include <cstring>

namespace BaseMathLib
{

	class BASEMATHLIB_API Matrix3
	{
	public:
		inline Matrix3() { }
		inline ~Matrix3() { }
		// member access, allows use of construct mat[r][c]
		inline explicit Matrix3(const double arr[3][3])
		{
			memcpy(m, arr, 9 * sizeof(double));
		}
		inline Matrix3(const Matrix3& rkMatrix)
		{
			memcpy(m, rkMatrix.m, 9 * sizeof(double));
		}
		Matrix3(double Entry00, double Entry01, double Entry02,
			double Entry10, double Entry11, double Entry12,
			double Entry20, double Entry21, double Entry22)
		{
			m[0][0] = Entry00;
			m[0][1] = Entry01;
			m[0][2] = Entry02;
			m[1][0] = Entry10;
			m[1][1] = Entry11;
			m[1][2] = Entry12;
			m[2][0] = Entry20;
			m[2][1] = Entry21;
			m[2][2] = Entry22;
		}
		inline double* operator[] (unsigned int iRow) const
		{
			return (double*)m[iRow];
		}

		/**
		* create a rotation matrix from the yaw, pitch and roll angle
		*
		*/
		void CreateRotationMatrix(const double& YAngle, const double& PAngle, const double& RAngle);

		// column access
		Vector3<double> GetColumn(unsigned int iCol) const;
		void SetColumn(unsigned int iCol, const Vector3<double>& vec);

		// assignment and comparison

		inline Matrix3& operator= (const Matrix3& rkMatrix)
		{
			memcpy(m, rkMatrix.m, 9 * sizeof(double));
			return *this;
		}   bool operator== (const Matrix3& rkMatrix) const;


		inline bool operator!= (const Matrix3& rkMatrix) const
		{
			return !operator==(rkMatrix);
		}
		Matrix3 operator+ (const Matrix3& rkMatrix) const;
		Matrix3 operator- (const Matrix3& rkMatrix) const;
		Matrix3 operator* (const Matrix3& rkMatrix) const;
		Matrix3 operator- () const;

		// matrix * vector [3x3 * 3x1 = 3x1]
		Vector3D operator* (const Vector3D& rkVector) const;
		// vector * matrix [1x3 * 3x3 = 1x3]
		BASEMATHLIB_API	friend Vector3D operator* (const Vector3D& rkVector,
			const Matrix3& rkMatrix);


		// matrix * scalar
		Matrix3 operator* (double fScalar) const;

		// scalar * matrix
		friend Matrix3 operator* (double fScalar, const Matrix3& rkMatrix);

		static const Matrix3 ZERO;
		static const Matrix3 IDENTITY;

		double m[3][3];

	};

}

#endif