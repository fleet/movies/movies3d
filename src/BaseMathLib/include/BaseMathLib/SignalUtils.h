#pragma once

#include "BaseMathLib/BaseMathLib.h"

#include <complex>

BASEMATHLIB_API std::vector<float> hanning(int N);

BASEMATHLIB_API std::vector<float> hanning2(int halfN);

BASEMATHLIB_API void conv2Complex(
	const v_complex_t & a,
	const v_complex_t & b,
	v_complex_t & c);

BASEMATHLIB_API void pulseCompression(const v_complex_t & transmitSignal
	, v_complex_t & complexSamples
	, float signalSquareNorm
	, float fe
	, float df
	, const int nbQuadrants);

BASEMATHLIB_API void xcorr(const v_complex_t & in, v_complex_t & out);

BASEMATHLIB_API std::vector<double> smoothed(const std::vector<double>& values, int span);