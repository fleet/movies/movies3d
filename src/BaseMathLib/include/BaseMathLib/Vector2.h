#ifndef BASEM_VECTOR_2
#define BASEM_VECTOR_2

#include "BaseMathLib.h"
#include <math.h>
#include <iostream>
#include <assert.h>

namespace BaseMathLib
{
	template <typename T> class BASEMATHLIB_API Vector2
	{

	public:
		T x;
		T y;

		Vector2() { x = y = 0; }
		Vector2(T v) : x(v), y(v) {}
		Vector2(T _x, T _y) : x(_x), y(_y) {}
		Vector2(const T p[2]) : x(p[0]), y(p[1]) {}

		inline T squaredLength(void) const { return x * x + y * y; }
		inline double length(void) const { return sqrt((double)squaredLength()); }

		inline T dotProduct(const Vector2& vec) const
		{
			return x * vec.x + y * vec.y;
		}
		inline T* ptr(void) { return &x; }
		inline const T* ptr(void)	const { return &x; }

		/** Normalises the vector.
		@remarks
		This method normalises the vector such that it's
		length is 1.
		*/
		inline void normalise()
		{
			double Length = length();

			// Will also work for zero-sized vectors, but will change nothing
			if (Length > 0)
			{
				double InvLength = 1.0 / Length;
				x *= InvLength;
				y *= InvLength;
			}
		}

		inline T operator[](int i) const { return (&x)[i]; }
		inline T& operator[](int i) { return (&x)[i]; }

		inline Vector2& operator = (const Vector2& rkVector)
		{
			x = rkVector.x;
			y = rkVector.y;

			return *this;
		}

		inline Vector2& operator = (const double Scalar)
		{
			x = Scalar;
			y = Scalar;

			return *this;
		}

		inline bool operator == (const Vector2& rkVector) const
		{
			return (x == rkVector.x && y == rkVector.y);
		}

		inline bool operator != (const Vector2& rkVector) const
		{
			return (x != rkVector.x || y != rkVector.y);
		}

		// arithmetic operations
		inline Vector2 operator + (const Vector2& rkVector) const
		{
			return Vector2(
				x + rkVector.x,
				y + rkVector.y);
		}

		inline Vector2 operator - (const Vector2& rkVector) const
		{
			return Vector2(
				x - rkVector.x,
				y - rkVector.y);
		}

		inline Vector2 operator * (const double Scalar) const
		{
			return Vector2(
				x * Scalar,
				y * Scalar);
		}

		inline Vector2 operator * (const Vector2& rhs) const
		{
			return Vector2(
				x * rhs.x,
				y * rhs.y);
		}

		inline Vector2 operator / (const double Scalar) const
		{
			assert(Scalar != 0.0);

			double fInv = 1.0 / Scalar;

			return Vector2(
				x * fInv,
				y * fInv);
		}

		inline Vector2 operator / (const Vector2& rhs) const
		{
			return Vector2(
				x / rhs.x,
				y / rhs.y);
		}

		inline const Vector2& operator + () const
		{
			return *this;
		}

		inline Vector2 operator - () const
		{
			return Vector2(-x, -y);
		}

		// overloaded operators to help Vector2
		inline friend Vector2 operator * (const double Scalar, const Vector2& rkVector)
		{
			return Vector2(
				Scalar * rkVector.x,
				Scalar * rkVector.y);
		}

		inline friend Vector2 operator / (const double Scalar, const Vector2& rkVector)
		{
			return Vector2(
				Scalar / rkVector.x,
				Scalar / rkVector.y);
		}

		inline friend Vector2 operator + (const Vector2& lhs, const double rhs)
		{
			return Vector2(
				lhs.x + rhs,
				lhs.y + rhs);
		}

		inline friend Vector2 operator + (const double lhs, const Vector2& rhs)
		{
			return Vector2(
				lhs + rhs.x,
				lhs + rhs.y);
		}

		inline friend Vector2 operator - (const Vector2& lhs, const double rhs)
		{
			return Vector2(
				lhs.x - rhs,
				lhs.y - rhs);
		}

		inline friend Vector2 operator - (const double lhs, const Vector2& rhs)
		{
			return Vector2(
				lhs - rhs.x,
				lhs - rhs.y);
		}
		// arithmetic updates
		inline Vector2& operator += (const Vector2& rkVector)
		{
			x += rkVector.x;
			y += rkVector.y;

			return *this;
		}

		inline Vector2& operator += (const double fScaler)
		{
			x += fScaler;
			y += fScaler;

			return *this;
		}

		inline Vector2& operator -= (const Vector2& rkVector)
		{
			x -= rkVector.x;
			y -= rkVector.y;

			return *this;
		}

		inline Vector2& operator -= (const double fScaler)
		{
			x -= fScaler;
			y -= fScaler;

			return *this;
		}

		inline Vector2& operator *= (const double Scalar)
		{
			x *= Scalar;
			y *= Scalar;

			return *this;
		}

		inline Vector2& operator *= (const Vector2& rkVector)
		{
			x *= rkVector.x;
			y *= rkVector.y;

			return *this;
		}

		inline Vector2& operator /= (const double Scalar)
		{
			assert(Scalar != 0.0);

			double fInv = 1.0 / Scalar;

			x *= fInv;
			y *= fInv;

			return *this;
		}

		inline Vector2& operator /= (const Vector2& rkVector)
		{
			x /= rkVector.x;
			y /= rkVector.y;

			return *this;
		}
		inline void operator-=(Vector2 v) { x -= v.x; y -= v.y; }

		friend bool operator<(Vector2 v1, Vector2 v2)
		{
			return v1.x < v2.x && v1.y < v2.y;
		}
		friend bool operator>(Vector2 v1, Vector2 v2)
		{
			return v1.x > v2.x && v1.y > v2.y;
		}
		friend bool operator<=(Vector2 v1, Vector2 v2)
		{
			return v1.x <= v2.x && v1.y <= v2.y;
		}
		friend bool operator>=(Vector2 v1, Vector2 v2)
		{
			return v1.x >= v2.x && v1.y >= v2.y;
		}

		// IPSIS - OTK - ajout operateur << et >>
		// pour genericité des méthodes de serialisation / deserialisation dans MovConfig
		inline friend std::ostream& operator << (std::ostream& os, const Vector2& rkVector)
		{
			os << rkVector.x << ";" << rkVector.y;
			return os;
		}

		inline friend std::istream& operator >> (std::istream& is, Vector2& rkVector)
		{
			char c;
			is >> rkVector.x;
			is >> c;
			is >> rkVector.y;
			return is;
		}
	};

	BASEMATHLIB_EXPIMP_TEMPLATE template class BASEMATHLIB_API Vector2<int>;
	BASEMATHLIB_EXPIMP_TEMPLATE template class BASEMATHLIB_API Vector2<float>;
	BASEMATHLIB_EXPIMP_TEMPLATE template class BASEMATHLIB_API Vector2<double>;

	typedef Vector2<int> Vector2I;
	typedef Vector2<float> Vector2F;
	typedef Vector2<double> Vector2D;
}


#endif
