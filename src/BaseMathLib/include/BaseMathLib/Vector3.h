#ifndef BASEM_VECTOR_3
#define BASEM_VECTOR_3

#include "BaseMathLib.h"
#include "BaseMathLib/Vector2.h"

#include <math.h>
#include <iostream>
#include <assert.h>

namespace BaseMathLib
{
	template <typename T> class BASEMATHLIB_API Vector3
	{
	public:

		T x;
		T y;
		T z;

		Vector3() { x = y = z = 0; }
		Vector3(T v) : x(v), y(v), z(v) {}
		Vector3(T _x, T _y, T _z) : x(_x), y(_y), z(_z) {}
		Vector3(Vector2<T> f2, T _z) : x(f2.x), y(f2.y), z(_z) {}
		Vector3(const T p[3]) : x(p[0]), y(p[1]), z(p[2]) {}

		inline T squaredLength() const { return x * x + y * y + z * z; }

		inline double length() const { return sqrt((double)squaredLength()); }

		inline T dotProduct(const Vector3<T>& vec) const
		{
			return x * vec.x + y * vec.y + z*vec.z;
		}

		inline T* ptr() { return &x; }

		inline const T* ptr()	const { return &x; }

		inline double normalise()
		{
			const double Length = length();
			// Will also work for zero-sized vectors, but will change nothing
			if (Length > 0)
			{
				double InvLength = 1.0 / Length;
				x *= InvLength;
				y *= InvLength;
				z *= InvLength;
			}
			return Length;
		}
		
		inline double distance(const Vector3& rhs) const
		{
			return (*this - rhs).length();
		}

		inline double squaredDistance(const Vector3& rhs) const
		{
			return (*this - rhs).squaredLength();
		}

		inline T operator[](int i) const { return (&x)[i]; }

		inline T& operator[](int i) { return (&x)[i]; }

		inline friend Vector3 vectorialProduct(Vector3 v1, Vector3 v2)
		{
			return Vector3(
				v1.y * v2.z - v1.z * v2.y,
				v1.z * v2.x - v1.x * v2.z,
				v1.x * v2.y - v1.y * v2.x);
		}
		
		Vector3& operator = (Vector3 const& rkVector)
		{
			x = rkVector.x;
			y = rkVector.y;
			z = rkVector.z;
			return *this;
		}

		Vector3& operator = (double const Scalar)
		{
			x = Scalar;
			y = Scalar;
			z = Scalar;
			return *this;
		}

		inline bool operator == (const Vector3& rkVector) const
		{
			return (x == rkVector.x && y == rkVector.y && z == rkVector.z);
		}

		inline bool operator != (const Vector3& rkVector) const
		{
			return (x != rkVector.x || y != rkVector.y || z != rkVector.z);
		}

		// arithmetic operations
		inline Vector3 operator + (const Vector3& rkVector) const
		{
			return Vector3(
				x + rkVector.x,
				y + rkVector.y,
				z + rkVector.z);
		}

		inline Vector3 operator - (const Vector3& rkVector) const
		{
			return Vector3(
				x - rkVector.x,
				y - rkVector.y,
				z - rkVector.z);
		}

		inline Vector3 operator * (const double Scalar) const
		{
			return Vector3(
				x * Scalar,
				y * Scalar,
				z * Scalar);
		}

		inline Vector3 operator * (const Vector3& rhs) const
		{
			return Vector3(
				x * rhs.x,
				y * rhs.y,
				z * rhs.z);
		}

		inline Vector3 operator / (const double Scalar) const
		{
			assert(Scalar != 0.0);
			double fInv = 1.0 / Scalar;
			return Vector3(
				x * fInv,
				y * fInv,
				z * fInv);
		}

		inline Vector3 operator / (const Vector3& rhs) const
		{
			return Vector3(
				x / rhs.x,
				y / rhs.y,
				z / rhs.z);
		}

		inline const Vector3& operator + () const
		{
			return *this;
		}

		inline Vector3 operator - () const
		{
			return Vector3(-x, -y, -z);
		}

		inline friend Vector3 operator * (const double Scalar, const Vector3& rkVector)
		{
			return Vector3(
				Scalar * rkVector.x,
				Scalar * rkVector.y,
				Scalar * rkVector.z);
		}

		inline friend Vector3 operator / (const double Scalar, const Vector3& rkVector)
		{
			return Vector3(
				Scalar / rkVector.x,
				Scalar / rkVector.y,
				Scalar / rkVector.z);
		}

		inline friend Vector3 operator + (const Vector3& lhs, const double rhs)
		{
			return Vector3(
				lhs.x + rhs,
				lhs.y + rhs,
				lhs.z + rhs);
		}

		inline friend Vector3 operator + (const double lhs, const Vector3& rhs)
		{
			return Vector3(
				lhs + rhs.x,
				lhs + rhs.y,
				lhs + rhs.z);
		}

		inline friend Vector3 operator - (const Vector3& lhs, const double rhs)
		{
			return Vector3(
				lhs.x - rhs,
				lhs.y - rhs,
				lhs.z - rhs);
		}

		inline friend Vector3 operator - (const double lhs, const Vector3& rhs)
		{
			return Vector3(
				lhs - rhs.x,
				lhs - rhs.y,
				lhs - rhs.z);
		}

		inline Vector3& operator /= (const Vector3& rkVector)
		{
			x /= rkVector.x;
			y /= rkVector.y;
			z /= rkVector.z;
			return *this;
		}

		inline Vector3& operator /= (const double Scalar)
		{
			assert(Scalar != 0.0);
			double fInv = 1.0 / Scalar;
			x *= fInv;
			y *= fInv;
			z *= fInv;
			return *this;
		}

		inline Vector3& operator *= (const double Scalar)
		{
			x *= Scalar;
			y *= Scalar;
			z *= Scalar;
			return *this;
		}

		inline Vector3& operator *= (const Vector3& rkVector)
		{
			x *= rkVector.x;
			y *= rkVector.y;
			z *= rkVector.z;

			return *this;
		}

		inline Vector3& operator -= (const Vector3& rkVector)
		{
			x -= rkVector.x;
			y -= rkVector.y;
			z -= rkVector.z;
			return *this;
		}

		inline Vector3& operator -= (const double Scalar)
		{
			x -= Scalar;
			y -= Scalar;
			z -= Scalar;
			return *this;
		}

		inline Vector3& operator += (const Vector3& rkVector)
		{
			x += rkVector.x;
			y += rkVector.y;
			z += rkVector.z;
			return *this;
		}

		inline Vector3& operator += (const double Scalar)
		{
			x += Scalar;
			y += Scalar;
			z += Scalar;
			return *this;
		}

		// IPSIS - OTK - ajout operateur << et >>
		// pour genericité des méthodes de serialisation / deserialisation dans MovConfig
		inline friend std::ostream& operator << (std::ostream& os, const Vector3& rkVector)
		{
			os << rkVector.x << ";" << rkVector.y << ";" << rkVector.z;
			return os;
		}

		inline friend std::istream& operator >> (std::istream& is, Vector3& rkVector)
		{
			char c;
			is >> rkVector.x;
			is >> c;
			is >> rkVector.y;
			is >> c;
			is >> rkVector.z;
			return is;
		}

		static Vector3 ToViewOpengl(const Vector3 &ref)
		{
			return Vector3(ref.y, -ref.z, -ref.x);
		}

		static Vector3 ToViewReal(const Vector3 &ref)
		{
			return Vector3(-ref.z, ref.x, -ref.y);
		}
	};

	BASEMATHLIB_EXPIMP_TEMPLATE template class BASEMATHLIB_API Vector3<int>;
	BASEMATHLIB_EXPIMP_TEMPLATE template class BASEMATHLIB_API Vector3<float>;
	BASEMATHLIB_EXPIMP_TEMPLATE template class BASEMATHLIB_API Vector3<double>;

	typedef Vector3<int> Vector3I;
	typedef Vector3<float> Vector3F;
	typedef Vector3<double> Vector3D;
}

#endif
