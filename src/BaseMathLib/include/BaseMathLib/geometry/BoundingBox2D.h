// -*- C++ -*-
// ****************************************************************************
// Class: BoundingBox2D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "BaseMathLib/BaseMathLib.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace BaseMathLib
{
	class BASEMATHLIB_API BoundingBox2D
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		BoundingBox2D(void);

		// Destructeur
		virtual ~BoundingBox2D(void);

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//Reset
		virtual void Reset();

		//Init
		void Init(double x, double y);

		//MaJ les donn�es membres
		void Update(double * points, int nbPoints);

		//MaJ les donn�es membres
		void Update(const BoundingBox2D& other);

		//MaJ les donn�es membres
		inline void Update(double x, double y);

		//test d'intersection
		bool Intersect(const BoundingBox2D& other) const;

		//test de distance
		bool CloseTo(const BoundingBox2D& other, double thresholdX, double thresholdY) const;


		// *********************************************************************
		// Variables membres
		// *********************************************************************

		double m_dXMin;
		double m_dXMax;
		double m_dYMin;
		double m_dYMax;

	protected:

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//MaJ les donn�es membres (optimis� pour r�duire les tests)
		//obligation d'appeler Init avant le premier appel � UpdateOpti
		inline void UpdateOpti(double x, double y);
	};
};
