// -*- C++ -*-
// ****************************************************************************
// Class: GeometryTools2D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "BaseMathLib/BaseMathLib.h"

namespace BaseMathLib {

	enum TLinesIntersection {
		TLINES_CROSS,
		TLINES_EQUAL,
		TLINES_PARALLEL,
		TLINES_UNKNOWN
	};

	enum TSegmentsIntersection {
		TSEGMENTS_COLINEAR,
		TSEGMENTS_COMMONPOINT,
		TSEGMENTS_COMMONPOINT_COLINEAR,
		TSEGMENTS_COMMONPOINT_OVERLAP,
		TSEGMENTS_CROSS,
		TSEGMENTS_EQUAL,
		TSEGMENTS_OVERLAP,
		TSEGMENTS_PARALLEL,
		TSEGMENTS_OTHER,
		TSEGMENTS_UNKNOWN
	};


	typedef int TPoint2DState;

#define TPoint_Undefined	0
#define TPoint_TopMost		1
#define TPoint_RightMost	2
#define TPoint_BottomMost	4
#define TPoint_LeftMost		8


	/**
	 * <p>Titre : </p>
	 *
	 * <p>Description : </p>
	 *
	 * <p>Copyright : Copyright (c) 2006</p>
	 *
	 * <p>Soci�t� : IPSIS</p>
	 *
	 * @author non attribuable
	 * @version 1.0
	 */
	class BASEMATHLIB_API GeometryTools2D {

		GeometryTools2D();

	public:
		/**
	   * intersectSegments
	   * Methode :
	   * segment : vecteur double (X1 Y1 X2 Y2)
	   * point : vecteur double (X Y)
	   * si p appartient aux segments s1 et s2 :
	   *         * p  = intersection des segments s1 et s2
	   * sinon :
	   *         * p = NaN
	   * Resultat : true si p appartient aux segments s1 et s2
	   *
	   * @param s1 double [4]
	   * @param s2 double [4]
	   * @param p double [2]
	   *
	   * @return TSegmentsIntersection
	   */
		static TSegmentsIntersection intersectSegments(double* s1, double* s2, double* p);
		static TSegmentsIntersection intersectSegments(double s1_Ax, double s1_Ay, double s1_Bx, double s1_By,
			double s2_Ax, double s2_Ay, double s2_Bx, double s2_By,
			double* p);

		/**
	   * intersectSegmentsSimple
	   * Methode :
	   * segment : vecteur double (X1 Y1 X2 Y2)
	   * point : vecteur double (X Y)
	   * si p appartient aux segments s1 et s2 :
	   *         * p  = intersection des segments s1 et s2
	   * sinon :
	   *         * p = NaN
	   * Resultat : true si p appartient aux segments s1 et s2
	   *
	   * @param s1 double [4]
	   * @param s2 double [4]
	   * @param p double [2]
	   *
	   * @return TSegmentsIntersection
	   */
		static TSegmentsIntersection intersectSegmentsSimple(double* s1, double* s2, double* p);
		static inline TSegmentsIntersection intersectSegmentsSimple(double s1_Ax, double s1_Ay, double s1_Bx, double s1_By,
			double s2_Ax, double s2_Ay, double s2_Bx, double s2_By,
			double* p);
		/**
		* intersectLines
		 * Methode :
		 * segment : vecteur double (X1 Y1 X2 Y2)
		 * point : vecteur double (X Y)
		 * si une intersection existe entre les droites (s1) et (s2) :
		 *         * p = intersection des droites  (s1) et (s2)
		 *         * p = s1(A) si S1 et S2 confondues
		 * sinon :
		 *         * p = NaN
		 * Resultat : true si p appartient aux droites (s1) et (s2)
		*
		* @param s1 double [4]
		* @param s2 double [4]
		* @param p double [2]
		*
		* @return TLinesIntersection
		*/
		static TLinesIntersection intersectLines(double* s1, double* s2, double* p);
		static TLinesIntersection intersectLines(double s1_Ax, double s1_Ay, double s1_Bx, double s1_By,
			double s2_Ax, double s2_Ay, double s2_Bx, double s2_By,
			double* p);

		/**
		 * computeLinesCrossParam
		 * Methode : renseigne dans params les param�tres d'intersection des droites
		 * segment : vecteur double (XA YA XB YB)
		 * params : vecteur double (UA, UB, VI, V2)
		*
		* @param s1 double [4]
		* @param s2 double [4]
		* @param params double [4]
		*
		*/
		static void computeLinesCrossParam(double s1_Ax, double s1_Ay, double s1_Bx, double s1_By,
			double s2_Ax, double s2_Ay, double s2_Bx, double s2_By,
			double* params);

		/**
		* perpendicularOffset
		 * Methode :
		 * segment : vecteur double (XA YA XB YB)
		 * point : vecteur double (X Y)
		 * cr��e dans s_moved une copie du segment s perpendiculairement d�cal�e de l'offset off
		 * RQ : le sens du d�placement du segment [AB] est tel que l'angle (BAB') > 0 avec B' le projet� de B
		*
		* @param s double [4]
		* @param offset double
		* @param s_moved double [4]
		*
		* @return bool
		*/
		static void perpendicularOffset(double* s, double offset, double* s_moved);

		/**
		* extractValues
		*
		* @param s double []
		* @param res (out) double [nbValues]
		*/
		static void extractValues(double* s, int iBegin, int nbValues, double* res);

		/*/**
		* equals
		*
		* @param p1 double[size1]
		* @param size1
		* @param p2 double[size2]
		* @param size2
		*/
		static bool equals(double* p1, int size1, double* p2, int size2);

		/**
		* equals
		*
		* @param p1 double*
		* @param p2 double*
		* @param delta double
		*/
		static bool equals(double* p1, double* p2, double delta);

		/**
		* equals
		*
		* @param p1 double*
		* @param p2 double*
		* @param delta double
		*/
		static bool equals(double p1, double p2);

		/**
		* equals
		*
		* @param p1 double*
		* @param p2 double*
		* @param delta double
		*/
		static bool lessOrEquals(double p1, double p2);

		/**
		* equals
		*
		* @param p1 double*
		* @param p2 double*
		* @param delta double
		*/
		static bool equals(double p1, double p2, double delta);

		/**
		 * getSegmentDistance
		 *
		 * @param seg double[4]
		 * @return double
		 */
		static double getSegmentDistance(double* seg);

		/**
		* getSegmentDistance
		*
		* @param p1 double[2]
		* @param p2 double[2]
		* @return double
		*/
		static double getSegmentDistance(double* pt1, double* pt2);


		static double getDistancePointToLine(double* pt, double* seg);

		/**
		* pointInPolygon
		*
		* p : vecteur double (XA YA...XN YN)
		* pt : vecteur double (2)
		*
		* @param polygon double [psize]
		* @param psize
		* @param pt double[2]
		* @return bool
		*/
		static bool PointInPolygon(double*polygon, int psize, double* pt);
		static bool PointInPolygon(double* polygon, int psize, double px, double py);

		/**
		* PointsInPolygon
		* Teste p_inside dans p
		* p_inside : vecteur double (XA YA...XN YN)
		* p : vecteur double (XA' YA'...XN' YN')
		*
		* @param p_inside double[p_insidesize]
		* @param p_insidesize
		* @param p_inside double[psize]
		* @param psize
		* @return bool
		*/
		static bool PointsInPolygon(double*p_inside, int p_insidesize, double* p, int psize);

		/**
		* is the point (px, py) close to the polygon ?
		*
		* @param polygon double[]
		* @param px double
		* @param py double
		* @return bool
		*/
		static bool PointCloseToPolygon(double* polygon, int psize,
			double px, double py,
			double thresholdx, double thresholdy);
		/**
		 * SegmentsCrossing
		 *
		 * @param xa double
		 * @param ya double
		 * @param xb double
		 * @param yb double
		 * @param xa2 double
		 * @param ya2 double
		 * @param xb2 double
		 * @param yb2 double
		 * @return bool
		 */
		static bool SegmentsCrossing(double xa1, double ya1,
			double xb1, double yb1,
			double xa2, double ya2,
			double xb2, double yb2,
			double& intersect_x, double& intersect_y);

		/**
		* PolygonsCrossing
		 * Methode :
		 * p : vecteur double (XA YA...XN YN)
		 * Teste les polygones s'intersectent
		 * Renvoie true si les polygones s'intersectent
		*
		 * @param p1 double [size1]
		 * @param size1 int
		 * @param p2 double [size2]
		 * @param size2 int
		 * @return bool
		*/
		static bool PolygonsCrossing(double* p1, int size1, double * p2, int size2, double& intersect_x, double& intersect_y);

		/**
		 * hit
		 *
		 * @param polygon double[psize]
		 * @param psize int
		 * @param left double
		 * @param top double
		 * @param right double
		 * @param bottom double
		 * @return bool
		 */
		static bool PolygonIntersectBBox(double* polygon, int psize, double left, double top, double right, double bottom);

		/**
		 * hitSegment with bounding box (left, top, right, bottom)
		 *
		 * @param xi double
		 * @param yi double
		 * @param xj double
		 * @param yj double
		 * @param left double
		 * @param top double
		 * @param right double
		 * @param bottom double
		 * @return bool
		 */
		static bool SegmentIntersectBBox(double xa, double ya, double xb, double yb,
			double left, double top, double right, double bottom);

		/**
		 * BBoxIntersectBBox
		 *
		 * @param left1 double
		 * @param top1 double
		 * @param right1 double
		 * @param bottom1 double
		 * @param left2 double
		 * @param top2 double
		 * @param right2 double
		 * @param bottom2 double
		 * @return bool
		 */
		static bool BBoxIntersectBBox(double left1, double top1, double right1, double bottom1,
			double left2, double top2, double right2, double bottom2);

		//Determiner les statuts de chaque point du polygone (TPoint2DState)
		static void GetPointsState(double * points, TPoint2DState* states, int size);

		//Ajouter des marges en X et Y au polygone
		static void AddMargins(double * points, int size, double xMargin, double yMargin, double ** points_out, int& size_out);

		//set the value in the interval [min, max]
		static double SetInRange(double min, double max, double value);

	private:

	};

};
