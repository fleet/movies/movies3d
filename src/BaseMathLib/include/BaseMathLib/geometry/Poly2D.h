// -*- C++ -*-
// ****************************************************************************
// Class: Poly2D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "BaseMathLib/geometry/BoundingBox2D.h"
#include <vector>
#include <assert.h>
#include <cstdint>

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace BaseMathLib
{
	class BASEMATHLIB_API Poly2D
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		Poly2D(void);

		// Destructeur
		virtual ~Poly2D(void);

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//Init
		virtual void Alloc(int _nbPoints);

		//MaJ BBox
		inline void UpdateBBox() { m_bbox.Update(m_points, m_nbPoints); };

		//test d'intersection
		bool Intersect(Poly2D& other);

		//test de distance
		bool CloseTo(Poly2D& other, double thresholdX, double thresholdY);

		//calcul de la surface convexe en repere orthonorm�
		virtual double ComputeConvexOrthoArea() const;

		//calcul de la surface convexe en repere polaire
		virtual double ComputeConvexPolarArea() const;

		// *********************************************************************
		// Accesseurs
		// *********************************************************************
		inline BoundingBox2D& GetBoundingBox() { return m_bbox; }
		inline double * GetPoints() { return m_points; }
		inline void SetPoints(double * pPoints, int nbPoints) { m_points = pPoints; m_nbPoints = nbPoints; }
		inline const double  * GetPoints() const { return m_points; }
		inline int		 GetNbPoints() const { return m_nbPoints; }

		inline const double * GetPoint(int iPoint) const { return GetPointX(iPoint); }
		inline const double * GetPointX(int iPoint) const { assert(2 * iPoint < 2 * m_nbPoints); return &m_points[2 * iPoint]; }
		inline const double * GetPointY(int iPoint) const { assert(2 * iPoint + 1 < 2 * m_nbPoints); return &m_points[2 * iPoint + 1]; }

		inline double * GetPoint(int iPoint) { return GetPointX(iPoint); }
		inline double * GetPointX(int iPoint) { assert(2 * iPoint < 2 * m_nbPoints); return &m_points[2 * iPoint]; }
		inline double * GetPointY(int iPoint) { assert(2 * iPoint + 1 < 2 * m_nbPoints); return &m_points[2 * iPoint + 1]; }

	protected:

		// calcul du point minimal et maximal sur l'axe angulaire/X
		virtual void ComputeMinMaxIndex(std::int32_t& minIndex, std::int32_t& maxIndex) const;

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************
		double *		m_points; //(x0, y0);(x1, y1);...
		int				m_nbPoints;
		BoundingBox2D	m_bbox;
	};
};
