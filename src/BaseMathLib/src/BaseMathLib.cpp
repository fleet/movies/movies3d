// BaseMathLib.cpp : Defines the entry point for the DLL application.
//
#include "BaseMathLib/BaseMathLib.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <cassert>

#include <cstring>
#include <map>
#include <list>
#include <algorithm>

BASEMATHLIB_API double round(double x)
{
	return x >= 0.0 ? floor(x + 0.5) : ceil(x - 0.5);
}
