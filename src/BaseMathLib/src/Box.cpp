#include "BaseMathLib/Box.h"

using namespace BaseMathLib;

Box::Box()
{
	this->setEmpty();
}

Box::Box(const Vector3D & pointA, const Vector3D & pointB)
{
    m_minBox.x = std::min<double>(pointA.x, pointB.x);
    m_minBox.y = std::min<double>(pointA.y, pointB.y);
    m_minBox.z = std::min<double>(pointA.z, pointB.z);

    m_maxBox.x = std::max<double>(pointA.x, pointB.x);
    m_maxBox.y = std::max<double>(pointA.y, pointB.y);
    m_maxBox.z = std::max<double>(pointA.z, pointB.z);
}

Box::~Box()
{
}

void Box::Extends(const Box &refBox)
{
	if (!isEmpty())
	{
        m_minBox.x = std::min<double>(m_minBox.x, refBox.m_minBox.x);
        m_minBox.x = std::min<double>(m_minBox.x, refBox.m_maxBox.x);
        m_minBox.y = std::min<double>(m_minBox.y, refBox.m_minBox.y);
        m_minBox.y = std::min<double>(m_minBox.y, refBox.m_maxBox.y);
        m_minBox.z = std::min<double>(m_minBox.z, refBox.m_minBox.z);
        m_minBox.z = std::min<double>(m_minBox.z, refBox.m_maxBox.z);

        m_maxBox.x = std::max<double>(m_maxBox.x, refBox.m_minBox.x);
        m_maxBox.x = std::max<double>(m_maxBox.x, refBox.m_maxBox.x);
        m_maxBox.y = std::max<double>(m_maxBox.y, refBox.m_maxBox.y);
        m_maxBox.y = std::max<double>(m_maxBox.y, refBox.m_minBox.y);
        m_maxBox.z = std::max<double>(m_maxBox.z, refBox.m_minBox.z);
        m_maxBox.z = std::max<double>(m_maxBox.z, refBox.m_maxBox.z);
	}
	else
	{
		m_minBox = refBox.m_minBox;
		m_maxBox = refBox.m_maxBox;
	}
}




