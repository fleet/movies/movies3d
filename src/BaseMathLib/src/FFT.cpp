#include "BaseMathLib/FFT.h"

#include <map>

#include "ffts/ffts.h"

struct FFTComputerImpl
{
    struct FFTPlan
    {
        size_t length;
        v_complex_t tmp;
        ffts_plan_t* forward_plan;
        ffts_plan_t* backward_plan;

        FFTPlan(int _length)
            : length(_length)
            , tmp(_length)
        {
            forward_plan = ffts_init_1d(length, FFTS_FORWARD);
            backward_plan = ffts_init_1d(length, FFTS_BACKWARD);
        }

        ~FFTPlan()
        {
            if (forward_plan != nullptr)
            {
                ffts_free(forward_plan);
            }

            if (backward_plan != nullptr)
            {
                ffts_free(backward_plan);
            }
        }

        bool exec(v_complex_t& data, bool forward)
        {
            auto plan = forward ? forward_plan : backward_plan;
            if (plan == nullptr)
            {
                return false;
            }

            if (data.size() != length)
            {
                return false;
            }

            ffts_execute(plan, data.data(), tmp.data());
            tmp.swap(data);

            if (!forward)
            {
                const float f = 1.f / length;
                for (int i = 0; i < length; ++i)
                {
                    data[i] *= f;
                }
            }

            return true;            
        }
    };

    // Classe template pour la gestion d'objet dans un cache limit� en taille
    template<class Key, class T>
    class Cache
    {
        struct Entry {
            inline Entry() : keyPtr(NULL) {}
            inline Entry(T* data) : keyPtr(NULL), obj(data), previous(NULL), next(NULL) {}
            const Key* keyPtr;
            T* obj;
            Entry* previous;
            Entry* next;
        };

        Entry* first;
        Entry* last;
        std::map<Key, Entry> map;
        int mx;

        inline void unlink(Entry& entry)
        {
            if (entry.previous) entry.previous->next = entry.next;
            if (entry.next) entry.next->previous = entry.previous;
            if (last == &entry) last = entry.previous;
            if (first == &entry) first = entry.next;
            T* obj = entry.obj;
            map.erase(*entry.keyPtr);
            delete obj;
        }

        void trim(int m)
        {
            Entry* e = last;
            while (e && map.size() > m) {
                Entry* u = e;
                e = e->previous;
                unlink(*u);
            }
        }

    public:

        Cache(int _mx) : mx(_mx), first(NULL), last(NULL) {}
        ~Cache() { clear(); }

        inline int count() const { return map.size(); }

        void clear()
        {
            while (first) { delete first->obj; first = first->next; }
            map.clear();
            last = NULL;
        }

        void insert(const std::pair<Key, T>& p)
        {
            insert(p.first, p.second);
        }

        void insert(const Key& key, T* object)
        {
            remove(key);
            trim(mx - 1);
            Entry se(object);
            auto res = map.insert(std::pair<Key, Entry>(key, se));
            Entry* e = &(res.first->second);
            e->keyPtr = &(res.first->first);
            if (first) first->previous = e;
            e->next = first;
            first = e;
            if (!last) last = first;
        }

        bool remove(const Key& key)
        {
            bool ok = false;
            typename std::map<Key, Entry>::iterator it = map.find(key);
            if (it != map.end())
            {
                unlink(it->second);
                ok = true;
            }
            return ok;
        }

        T* get(const Key& key) const
        {
            T* obj = NULL;
            typename std::map<Key, Entry>::const_iterator it = map.find(key);
            if (it != map.end())
            {
                obj = it->second.obj;
            }
            return obj;
        }
    };

    class FFTPlanCache
    {
        Cache<int, FFTPlan> m_values;

    public:
        FFTPlanCache(int n = 1)
            : m_values(n)
        {
        }

        FFTPlan * get(int n)
        {
            auto instance = m_values.get(n);
            if (instance == NULL)
            {
                instance = new FFTPlan(n);
                m_values.insert(n, instance);
            }
            return instance;
        }
    };


    static bool compute(v_complex_t& data, bool forward)
    {
        static FFTPlanCache fftPlanCache;
        auto plan = fftPlanCache.get(data.size());
        return plan->exec(data, forward);
    }
};

bool FFTComputer::forward(v_complex_t & data)
{
    return FFTComputerImpl::compute(data, true);
}

bool FFTComputer::backward(v_complex_t& data)
{
    return FFTComputerImpl::compute(data, false);
}

