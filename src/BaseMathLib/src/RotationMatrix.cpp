#include "BaseMathLib/RotationMatrix.h"

using namespace BaseMathLib;

#include <assert.h>
#include <cstring>

const Matrix3 Matrix3::ZERO(0, 0, 0, 0, 0, 0, 0, 0, 0);
const Matrix3 Matrix3::IDENTITY(1, 0, 0, 0, 1, 0, 0, 0, 1);
Vector3D Matrix3::GetColumn(unsigned int iCol) const
{

	return Vector3D(m[0][iCol], m[1][iCol],
		m[2][iCol]);
}
//-----------------------------------------------------------------------
void Matrix3::SetColumn(unsigned int  iCol, const Vector3D& vec)
{
	assert(iCol < 3);
	m[0][iCol] = vec.x;
	m[1][iCol] = vec.y;
	m[2][iCol] = vec.z;

}


//-----------------------------------------------------------------------
bool Matrix3::operator== (const Matrix3& rkMatrix) const
{
	for (unsigned int iRow = 0; iRow < 3; iRow++)
	{
		for (unsigned int iCol = 0; iCol < 3; iCol++)
		{
			if (m[iRow][iCol] != rkMatrix.m[iRow][iCol])
				return false;
		}
	}

	return true;
}
//-----------------------------------------------------------------------
Matrix3 Matrix3::operator+ (const Matrix3& rkMatrix) const
{
	Matrix3 kSum;
	for (unsigned int iRow = 0; iRow < 3; iRow++)
	{
		for (unsigned int iCol = 0; iCol < 3; iCol++)
		{
			kSum.m[iRow][iCol] = m[iRow][iCol] +
				rkMatrix.m[iRow][iCol];
		}
	}
	return kSum;
}
//-----------------------------------------------------------------------
Matrix3 Matrix3::operator- (const Matrix3& rkMatrix) const
{
	Matrix3 kDiff;
	for (unsigned int iRow = 0; iRow < 3; iRow++)
	{
		for (unsigned int iCol = 0; iCol < 3; iCol++)
		{
			kDiff.m[iRow][iCol] = m[iRow][iCol] -
				rkMatrix.m[iRow][iCol];
		}
	}
	return kDiff;
}
//-----------------------------------------------------------------------
Matrix3 Matrix3::operator* (const Matrix3& rkMatrix) const
{
	Matrix3 kProd;
	for (unsigned int iRow = 0; iRow < 3; iRow++)
	{
		for (unsigned int iCol = 0; iCol < 3; iCol++)
		{
			kProd.m[iRow][iCol] =
				m[iRow][0] * rkMatrix.m[0][iCol] +
				m[iRow][1] * rkMatrix.m[1][iCol] +
				m[iRow][2] * rkMatrix.m[2][iCol];
		}
	}
	return kProd;
}
//-----------------------------------------------------------------------
Vector3D Matrix3::operator* (const Vector3D& rkPoint) const
{
	Vector3D kProd;
	for (unsigned int iRow = 0; iRow < 3; iRow++)
	{
		kProd[iRow] =
			m[iRow][0] * rkPoint[0] +
			m[iRow][1] * rkPoint[1] +
			m[iRow][2] * rkPoint[2];
	}
	return kProd;
}
//-----------------------------------------------------------------------
Vector3D operator* (const Vector3D& rkPoint, const Matrix3& rkMatrix)
{
	Vector3D kProd;
	for (unsigned int iRow = 0; iRow < 3; iRow++)
	{
		kProd[iRow] =
			rkPoint[0] * rkMatrix.m[0][iRow] +
			rkPoint[1] * rkMatrix.m[1][iRow] +
			rkPoint[2] * rkMatrix.m[2][iRow];
	}
	return kProd;
}
//-----------------------------------------------------------------------
Matrix3 Matrix3::operator- () const
{
	Matrix3 kNeg;
	for (unsigned int iRow = 0; iRow < 3; iRow++)
	{
		for (unsigned int iCol = 0; iCol < 3; iCol++)
			kNeg[iRow][iCol] = -m[iRow][iCol];
	}
	return kNeg;
}
//-----------------------------------------------------------------------
Matrix3 Matrix3::operator* (double fScalar) const
{
	Matrix3 kProd;
	for (unsigned int iRow = 0; iRow < 3; iRow++)
	{
		for (unsigned int iCol = 0; iCol < 3; iCol++)
			kProd[iRow][iCol] = fScalar*m[iRow][iCol];
	}
	return kProd;
}
//-----------------------------------------------------------------------
Matrix3 operator* (double fScalar, const Matrix3& rkMatrix)
{
	Matrix3 kProd;
	for (unsigned int iRow = 0; iRow < 3; iRow++)
	{
		for (unsigned int iCol = 0; iCol < 3; iCol++)
			kProd[iRow][iCol] = fScalar*rkMatrix.m[iRow][iCol];
	}
	return kProd;
}

void Matrix3::CreateRotationMatrix(const double& YAngle, const double& PAngle, const double& RAngle)
{
	double fCos, fSin;

#pragma message(" _______________________________________________ ")
#pragma message("!                                               !")
#pragma message("!          Attention verifier les sens,         !")
#pragma message("!          les angles de rotations v�rifier     !")
#pragma message("!          aussi les multiplications de matrice !")
#pragma message("!_______________________________________________!")


#pragma message(" ______________________________________________________________________________________________ ")
#pragma message("!                                                                                              !")
#pragma message("!        Optimiser les calculs et affectation                                                  !")
#pragma message("!        gain potentiel de 1000%                                                               !")
#pragma message("!        cf:http://jeux.developpez.com/faq/matquat/?page=transformations                       !")
#pragma message("!______________________________________________________________________________________________!")


	fCos = cos(RAngle);
	fSin = sin(RAngle);
	Matrix3 kXMat(1.0, 0.0, 0.0, 0.0, fCos, -fSin, 0.0, fSin, fCos);

	fCos = cos(PAngle);
	fSin = sin(PAngle);
	Matrix3 kYMat(fCos, 0.0, fSin, 0.0, 1.0, 0.0, -fSin, 0.0, fCos);

	fCos = cos(YAngle);
	fSin = sin(YAngle);
	Matrix3 kZMat(fCos, -fSin, 0.0, fSin, fCos, 0.0, 0.0, 0.0, 1.0);

	/*
	A       = cos(angle_x);
	B       = sin(angle_x);
	C       = cos(angle_y);
	D       = sin(angle_y);
	E       = cos(angle_z);
	F       = sin(angle_z);

	AD      =   A * D;
	BD      =   B * D;

	mat[0]  =   C * E;
	mat[1]  =  -C * F;
	mat[2]  =  -D;
	mat[4]  = -BD * E + A * F;
	mat[5]  =  BD * F + A * E;
	mat[6]  =  -B * C;
	mat[8]  =  AD * E + B * F;
	mat[9]  = -AD * F + B * E;
	mat[10] =   A * C;
	*/
	*this = kZMat*(kYMat*kXMat);
}