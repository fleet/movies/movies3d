#include "BaseMathLib/SignalUtils.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>

#include "BaseMathLib/FFT.h"

int nextpow2(int n) { return log2(n) + 1; }

std::vector<float> hanning(int N)
{
	std::vector<float> result(N, 0);

	int half, i, idx;

	if (N % 2 == 0)
	{
		half = N / 2;
		for (i = 0; i < half; i++)
			result[i] = (float)(0.5 * (1 - cos(2 * M_PI * i / (N - 1))));

		idx = half - 1;
		for (i = half; i < N; i++) {
			result[i] = result[idx];
			idx--;
		}
	}
	else
	{
		half = (N + 1) / 2;
		for (i = 0; i < half; i++)
			result[i] = (float)(0.5 * (1 - cos(2 * M_PI * i / (N - 1))));

		idx = half - 2;
		for (i = half; i < N; i++) {
			result[i] = result[idx];
			idx--;
		}
	}

	return result;
}

std::vector<float> hanning2(int halfN)
{
	auto N = 2 * halfN;
	std::vector<float> result(N, 0);

	for (auto i = 0; i < N; i++)
	{
		result[i] = (float)(0.5 * (1 - cos(M_PI * i / halfN)));
		result[i] *= result[i];
	}

	return result;
}

void conv2Complex(
	const v_complex_t & a,
	const v_complex_t & b,
	v_complex_t & c)
{
	const int a_size = a.size();
	const int b_size = b.size();
	const int n = std::max<int>(a_size + b_size - 1, std::max<int>(a_size, b_size));
	c.resize(n);
	
	v_complex_t atmp(n);
	std::copy_n(a.begin(), a_size, atmp.begin());

	v_complex_t btmp(n);
	std::copy_n(b.begin(), b_size, btmp.begin());

	FFTComputer::forward(atmp);
	FFTComputer::forward(btmp);
	for (int i = 0; i < n; ++i)
	{
		c[i] = atmp[i] * btmp[i];
	}
	FFTComputer::backward(c);
}

void pulseCompression(const v_complex_t & transmitSignal
	, v_complex_t & complexSamples
	, float signalSquaredNorm
	, float fe
	, float df
	, const int nbQuadrants)
{
	const int signal_size = transmitSignal.size();
	const int nb_samples = complexSamples.size() / nbQuadrants;
	const int n = signal_size + nb_samples - 1;

	const long Ndf = signal_size * 2;
	const long Nfft = Ndf;
	const long shift = std::max<long>(1, floor(Ndf * 0.5));
	const long nb_fft = std::max<long>(1, ceil((double)nb_samples / Ndf * 2 - 1));

	//Hanning Window on sur 2*0.2 (a bit different than the classic hann window)
	const int nShapingSamples = floor(0.4 / 2.0 * Ndf);
	const std::vector<float> windowFunction = hanning2(nShapingSamples);
	std::vector<float> shapingWindow(Ndf, 1);
	std::copy_n(windowFunction.begin(), nShapingSamples, shapingWindow.begin());
	std::copy_n(windowFunction.begin() + nShapingSamples, nShapingSamples, shapingWindow.begin() + Ndf - nShapingSamples);

	v_complex_t result(n * nbQuadrants);
		
	v_complex_t a(Nfft);
	std::copy_n(transmitSignal.begin(), signal_size, a.begin());

	FFTComputer::forward(a);

	for (int i = 0; i < Nfft; ++i)
	{
		a[i] = std::conj(a[i]);
	}

	v_complex_t b(Nfft);

	for (int q = 0; q < nbQuadrants; ++q)
	{
		for (long ic = 0; ic < nb_fft; ++ic)
		{
			const long ic_shift = ic * shift;
			const int nbdfmax = std::min<int>(Ndf, (nb_samples - ic_shift));
			for (int i = 0; i < nbdfmax; ++i)
			{
				int j = (i + ic_shift) * nbQuadrants;
				b[i] = complexSamples[j + q];
			}

			for (int i = nbdfmax; i < Nfft; ++i)
			{
				b[i] = complex_t();
			}

			FFTComputer::forward(b);
			for (int i = 0; i < Nfft; ++i)
			{
				b[i] = a[i] * b[i];
			}
			FFTComputer::backward(b);

			for (int i = 0; i < shift; ++i)
			{
				int j = (i + ic_shift) * nbQuadrants;
				result[j + q] = b[i];
			}
		}
	}

	const int nb_result = nb_fft * shift * nbQuadrants;
	complexSamples.resize(nb_result);

	const float inv_signalSquaredNorm = 1.0 / signalSquaredNorm;
	for (int i = 0; i < nb_result; ++i)
	{
		complexSamples[i] = result[i] * inv_signalSquaredNorm;
	}
}

void xcorr(const v_complex_t & in, v_complex_t & out)
{
	const int M = in.size();
	const int maxlag = M - 1;
	const int fft_size = pow(2, nextpow2(2 * M - 1));

	v_complex_t a(fft_size);
	std::copy_n(in.begin(), M, a.begin());

	FFTComputer::forward(a);
	for (auto & c : a)
	{
		c.real(std::norm(c));
		c.imag(0.f);
	}
	FFTComputer::backward(a);

	out.resize(2 * M - 1);

	std::copy_n(a.begin() + (fft_size - maxlag), maxlag, out.begin());
	std::copy_n(a.begin(), M, out.begin() + maxlag);
}

std::vector<double> smoothed(const std::vector<double>& values, int span)
{
	std::vector<double> result(values.size());

	int halfSpan = std::max<int>(0, (int)floor((span - 1) / 2.0));
	int index = 0;
	for (auto it = values.cbegin(); it != values.cend(); ++it, ++index)
	{
		if (!isnan(*it))
		{
			double smoothedValue = *it;

			int spanNb = std::min<int>(std::min<int>(halfSpan, index), values.size() - 1 - index);
			int actualSpanNb = 0;

			for (int i = 1; i <= spanNb; ++i)
			{
				auto vi1 = *(it - i);
				auto vi2 = *(it + i);
				if (!isnan(vi1) && !isnan(vi2))
				{
					smoothedValue += vi1;
					smoothedValue += vi2;
					++actualSpanNb;
				}
				else
				{
					break;
				}
			}

			smoothedValue /= 2 * actualSpanNb + 1;
			result[index] = smoothedValue;
		}
		else
		{
			result[index] = *it;
		}
	}

	return result;
}