// -*- C++ -*-
// ****************************************************************************
// Class: BoundingBox2D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************
#include "BaseMathLib/geometry/BoundingBox2D.h"

// ***************************************************************************
// Dependences
// ***************************************************************************
#include <limits>
#include "BaseMathLib/geometry/GeometryTools2D.h"

using namespace BaseMathLib;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
BoundingBox2D::BoundingBox2D(void)
{
	m_dXMin = m_dYMin = std::numeric_limits<double>::max();
	m_dXMax = m_dYMax = -std::numeric_limits<double>::max();
}

// Destructeur
BoundingBox2D::~BoundingBox2D(void)
{}

// *********************************************************************
// M�thodes
// *********************************************************************

//Reset
void BoundingBox2D::Reset()
{
	m_dXMin = m_dYMin = std::numeric_limits<double>::max();
	m_dXMax = m_dYMax = -std::numeric_limits<double>::max();
}

//Init
void BoundingBox2D::Init(double x, double y)
{
	m_dXMin = m_dXMax = x;
	m_dYMin = m_dYMax = y;
}

/**
 * Update
 * Methode : MaJ les donn�es membres
 * @param double points[nbPoints]
 * @param int nbPoints (2*n)
 * @return void
 */
void BoundingBox2D::Update(double * points, int nbPoints)
{
	double x, y;
	double* iterPoints = points;
	double* points_end = points + 2 * nbPoints;

	if (nbPoints > 1)
	{
		//init avec le premier point pour optimiser les tests
		Init(*points, *(points + 1));
		iterPoints += 2;
	}

	while (iterPoints != points_end)
	{
		x = *iterPoints;
		y = *(iterPoints + 1);
		iterPoints += 2;

		//MaJ optimis�
		UpdateOpti(x, y);
	}
}

/**
 * Update
 * Methode : MaJ les donn�es membres
 * @param double x
 * @param double y
 * @return void
 */
void BoundingBox2D::Update(double x, double y)
{
	if (m_dXMax < x)
	{
		m_dXMax = x;
	}
	if (m_dXMin > x)
	{
		m_dXMin = x;
	}
	if (m_dYMax < y)
	{
		m_dYMax = y;
	}
	if (m_dYMin > y)
	{
		m_dYMin = y;
	}
}

/**
 * UpdateOpti
 * Methode : (optimis� pour r�duire les tests)
 *           obligation d'appeler Init avant le premier appel � UpdateOpti
 * @param double x
 * @param double y
 * @return void
 */
void BoundingBox2D::UpdateOpti(double x, double y)
{
	if (m_dXMax < x)
	{
		m_dXMax = x;
	}
	else if (m_dXMin > x)
	{
		m_dXMin = x;
	}
	if (m_dYMax < y)
	{
		m_dYMax = y;
	}
	else if (m_dYMin > y)
	{
		m_dYMin = y;
	}
}

/**
 * Update
 * Methode : MaJ les donn�es membres
 * @param const BoundingBox2D& other
 * @return void
 */
void BoundingBox2D::Update(const BoundingBox2D& other)
{
	Update(other.m_dXMin, other.m_dYMin);
	Update(other.m_dXMax, other.m_dYMin);
	Update(other.m_dXMin, other.m_dYMax);
	Update(other.m_dXMax, other.m_dYMax);
}

/**
 * Intersect
 * Methode : test d'intersection
 * @param const BoundingBox2D& other
 * @return bool
 */
bool BoundingBox2D::Intersect(const BoundingBox2D& other) const
{
	return (other.m_dXMin < m_dXMax) && (m_dXMin < other.m_dXMax) &&
		(other.m_dYMin < m_dYMax) && (m_dYMin < other.m_dYMax);
}

//test de distance
  /**
 * CloseTo
 * Methode : test de distance
 * @param const BoundingBox3D& other
 * @param double thresholdX
 * @param double thresholdY
 * @param double thresholdZ
 * @return bool
 */
bool BoundingBox2D::CloseTo(const BoundingBox2D& other, double thresholdX, double thresholdY) const
{
	return	other.m_dXMin - m_dXMax <= thresholdX && m_dXMin - other.m_dXMax <= thresholdX &&
		other.m_dYMin - m_dYMax <= thresholdY && m_dYMin - other.m_dYMax <= thresholdY;
}
