// -*- C++ -*-
// ****************************************************************************
// Class: GeometryTools2D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "BaseMathLib/geometry/GeometryTools2D.h"

#include <assert.h>
#include <math.h>
#include <float.h>

#include <cstring>
#include <algorithm>

using namespace BaseMathLib;

static double MY_DBL_EPSILON = pow(10.0, -6.0);

/**
 * mis en protected car ne doit contenir que des fonctions statiques
 */
GeometryTools2D::GeometryTools2D() {
}

/**
* intersectSegments
* Methode :
* segment : vecteur double (X1 Y1 X2 Y2)
* point : vecteur double (X Y)
* si p appartient aux segments s1 et s2 :
*         * p  = intersection des segments s1 et s2
* sinon :
*         * p = NaN
* Resultat : true si p appartient aux segments s1 et s2
*
* @param s1 double [4]
* @param s2 double [4]
* @param p double [2]
*
* @return TSegmentsIntersection
*/
TSegmentsIntersection GeometryTools2D::intersectSegments(double* s1, double* s2, double* p) {

	TSegmentsIntersection result = TSEGMENTS_UNKNOWN;

	result = intersectSegments(s1[0], s1[1], s1[2], s1[3], s2[0], s2[1], s2[2], s2[3], p);
	return result;
}

TSegmentsIntersection GeometryTools2D::intersectSegments(double s1_Ax, double s1_Ay, double s1_Bx, double s1_By,
	double s2_Ax, double s2_Ay, double s2_Bx, double s2_By,
	double* p)
{

	TSegmentsIntersection result = TSEGMENTS_UNKNOWN;
	p[0] = std::numeric_limits<double>::max();
	p[1] = std::numeric_limits<double>::max();

	//v�rifier d'abord que les segments ne sont pas �quivalents

	bool s1_A_equals_s2A = equals(s1_Ax, s2_Ax) && equals(s1_Ay, s2_Ay);
	bool s1_B_equals_s2B = equals(s1_Bx, s2_Bx) && equals(s1_By, s2_By);
	bool s1_A_equals_s2B = equals(s1_Ax, s2_Bx) && equals(s1_Ay, s2_By);
	bool s1_B_equals_s2A = equals(s1_Bx, s2_Ax) && equals(s1_By, s2_Ay);

	if ((s1_A_equals_s2A && s1_B_equals_s2B) ||
		(s1_B_equals_s2A && s1_A_equals_s2B)) {
		result = TSEGMENTS_EQUAL;
		p[0] = s1_Ax;
		p[1] = s1_Ay;
	}
	//not equals
	else {
		//calcul des parametres d'intersection des droites
		//vecteur double (UA, UB, VI, V2)
		/*static*/ double lineCrossparams[4];

		computeLinesCrossParam(s1_Ax, s1_Ay, s1_Bx, s1_By, s2_Ax, s2_Ay, s2_Bx,
			s2_By, lineCrossparams);
		//test points communs
		if (s1_A_equals_s2A || s1_A_equals_s2B) {
			result = TSEGMENTS_COMMONPOINT;
			p[0] = s1_Ax;
			p[1] = s1_Ay;
		}
		if (s1_B_equals_s2A || s1_B_equals_s2B) {
			result = TSEGMENTS_COMMONPOINT;
			p[0] = s1_Bx;
			p[1] = s1_By;
		}

		//if v2 = 0 --> droites paralleles
		//if v2 = 0  et v1 = 0 --> droites confondues
		if (equals(lineCrossparams[3], 0)) {
			bool colinear = equals(lineCrossparams[2], 0);

			if (colinear) {
				bool overlap = ((s1_Ax - s2_Ax) * (s1_Ax - s2_Bx) < 0) ||
					((s1_Bx - s2_Ax) * (s1_Bx - s2_Bx) < 0) ||
					((s2_Ax - s1_Ax) * (s2_Ax - s1_Bx) < 0) ||
					((s2_Bx - s1_Ax) * (s2_Bx - s1_Bx) < 0) ||
					((s1_Ay - s2_Ay) * (s1_Ay - s2_By) < 0) ||
					((s1_By - s2_Ay) * (s1_By - s2_By) < 0) ||
					((s2_Ay - s1_Ay) * (s2_Ay - s1_By) < 0) ||
					((s2_By - s1_Ay) * (s2_By - s1_By) < 0);
				if (result == TSEGMENTS_COMMONPOINT) {
					if (overlap) {
						result = TSEGMENTS_COMMONPOINT_OVERLAP;
					}
					else {
						result = TSEGMENTS_COMMONPOINT_COLINEAR;
					}
				}
				else {
					if (overlap) {
						result = TSEGMENTS_OVERLAP;
					}
					else {
						result = TSEGMENTS_COLINEAR;
					}
				}
			}
			else {
				result = TSEGMENTS_PARALLEL;
			}
		}
		else {

			if (result == TSEGMENTS_UNKNOWN) {
				result = TSEGMENTS_OTHER;
			}

			if (result != TSEGMENTS_COMMONPOINT) {
				//intersection si 0 <= ua et ub <= 1
				if (((lineCrossparams[0] >= 0.0) && (lineCrossparams[0] <= 1.0)) &&
					((lineCrossparams[1] >= 0.0) && (lineCrossparams[1] <= 1.0))) {
						{
							result = TSEGMENTS_CROSS;
							//xP = xS1 + u (xS2 - xS1)
							//yP = yS1 + u (yS2 - yS1)
							p[0] = (s1_Ax + lineCrossparams[0] * (s1_Bx - s1_Ax));
							p[1] = (s1_Ay + lineCrossparams[0] * (s1_By - s1_Ay));
						}
				}
			}
		}
	}
	return result;
}


/**
* intersectSegmentsSimple
* Methode :
* segment : vecteur double (X1 Y1 X2 Y2)
* point : vecteur double (X Y)
* si p appartient aux segments s1 et s2 :
*         * p  = intersection des segments s1 et s2
* sinon :
*         * p = NaN
* Resultat : true si p appartient aux segments s1 et s2
*
* @param s1 double [4]
* @param s2 double [4]
* @param p double [2]
*
* @return TSegmentsIntersection
*/
TSegmentsIntersection GeometryTools2D::intersectSegmentsSimple(double* s1, double* s2, double* p) {

	TSegmentsIntersection result = TSEGMENTS_UNKNOWN;

	result = intersectSegmentsSimple(s1[0], s1[1], s1[2], s1[3], s2[0], s2[1], s2[2], s2[3], p);
	return result;
}

TSegmentsIntersection GeometryTools2D::intersectSegmentsSimple(double s1_Ax, double s1_Ay, double s1_Bx, double s1_By,
	double s2_Ax, double s2_Ay, double s2_Bx, double s2_By,
	double* p)
{

	TSegmentsIntersection result = TSEGMENTS_UNKNOWN;
	p[0] = std::numeric_limits<double>::max();
	p[1] = std::numeric_limits<double>::max();

	//calcul des parametres d'intersection des droites
	//vecteur double (UA, UB, VI, V2)
	double lineCrossparams[4];
	computeLinesCrossParam(s1_Ax, s1_Ay, s1_Bx, s1_By, s2_Ax, s2_Ay, s2_Bx,
		s2_By, lineCrossparams);

	//intersection si 0 <= ua et ub <= 1
	double lineCrossparams0 = lineCrossparams[0];
	double lineCrossparams1 = lineCrossparams[1];
	if (lineCrossparams0 >= 0.0)
	{
		if (lineCrossparams0 <= 1.0)
		{
			if (lineCrossparams1 >= 0.0)
			{
				if (lineCrossparams1 <= 1.0)
				{
					result = TSEGMENTS_CROSS;
					//xP = xS1 + u (xS2 - xS1)
					//yP = yS1 + u (yS2 - yS1)
					p[0] = (s1_Ax + lineCrossparams0 * (s1_Bx - s1_Ax));
					p[1] = (s1_Ay + lineCrossparams0 * (s1_By - s1_Ay));
				}
			}
		}
	}

	return result;
}

/**
* intersectLines
 * Methode :
 * segment : vecteur double (X1 Y1 X2 Y2)
 * point : vecteur double (X Y)
 * si une intersection existe entre les droites (s1) et (s2) :
 *         * p = intersection des droites  (s1) et (s2)
 *         * p = s1(A) si S1 et S2 confondues
 * sinon :
 *         * p = NaN
 * Resultat : true si p appartient aux droites (s1) et (s2)
*
* @param s1 double [4]
* @param s2 double [4]
* @param p double [2]
*
* @return TLinesIntersection
*/
TLinesIntersection GeometryTools2D::intersectLines(double* s1, double* s2, double* p)
{
	TLinesIntersection result = TLINES_UNKNOWN;

	result = intersectLines(s1[0], s1[1], s1[2], s1[3], s2[0], s2[1], s2[2], s2[3], p);
	return result;
}
TLinesIntersection GeometryTools2D::intersectLines(double s1_Ax, double s1_Ay, double s1_Bx, double s1_By,
	double s2_Ax, double s2_Ay, double s2_Bx, double s2_By,
	double* p)
{
	TLinesIntersection result = TLINES_UNKNOWN;

	//calcul des parametres d'intersection des droites
	//vecteur double (UA, UB, VI, V2)
	double lineCrossparams[4];
	computeLinesCrossParam(s1_Ax, s1_Ay, s1_Bx, s1_By, s2_Ax, s2_Ay, s2_Bx, s2_By, lineCrossparams);

	//if v2 = 0 --> droites paralleles
	//if v2 = 0  et v1 = 0 --> droites confondues
	if (equals(lineCrossparams[3], 0)) {
		result = TLINES_PARALLEL;
		if (equals(lineCrossparams[2], 0)) {
			result = TLINES_EQUAL;
		}
		p[0] = std::numeric_limits<double>::max();
		p[1] = std::numeric_limits<double>::max();
	}
	else {
		result = TLINES_CROSS;

		//xP = xS1 + u (xS2 - xS1)
		//yP = yS1 + u (yS2 - yS1)
		p[0] = (s1_Ax + lineCrossparams[0] * (s1_Bx - s1_Ax));
		p[1] = (s1_Ay + lineCrossparams[0] * (s1_By - s1_Ay));
	}

	return result;
}

/**
 * computeLinesCrossParam
 * Methode : renseigne dans params les param�tres d'intersection des droites
 * segment : vecteur double (XA YA XB YB)
 * params : vecteur double (UA, UB, VI, V2)
*
* @param s1 double [4]
* @param s2 double [4]
* @param params double [4]
*
*/
void GeometryTools2D::computeLinesCrossParam(double s1_Ax, double s1_Ay, double s1_Bx, double s1_By,
	double s2_Ax, double s2_Ay, double s2_Bx, double s2_By,
	double* params) {

	double ua = -1;
	double ub = -1;
	double v1 = 0;
	double v2 = 0;

	//calcul du point le plus proche
	//ua = ((xT2-xT1)(yS1-yT1) - (yT2-yT1)(xS1-xT1)) /
	//      (yT2-yT1)(xS2-xS1) - (xT2-xT1)(yS2-yS1))

	//ub = ((xS2-xT1)(yS1-yT1) - (yS2-yT1)(xS1-xT1)) /
	//      (yT2-yT1)(xS2-xS1) - (xT2-xT1)(yS2-yS1))

	v1 = (s2_Bx - s2_Ax) * (s1_Ay - s2_Ay) - (s2_By - s2_Ay) * (s1_Ax - s2_Ax);
	v2 = (s2_By - s2_Ay) * (s1_Bx - s1_Ax) - (s2_Bx - s2_Ax) * (s1_By - s1_Ay);

	if (equals(v1, 0)) {
		ua = 0;
	}
	if (!equals(v2, 0)) {
		ua = v1 / v2;
	}

	v1 = (s1_Bx - s2_Ax) * (s1_Ay - s2_Ay) -
		(s1_By - s2_Ay) * (s1_Ax - s2_Ax);

	if (equals(v1, 0)) {
		ub = 0;
	}
	if (!equals(v2, 0)) {
		ub = v1 / v2;
	}

	params[0] = ua;
	params[1] = ub;
	params[2] = v1;
	params[3] = v2;
}

/**
* perpendicularOffset
 * Methode :
 * segment : vecteur double (XA YA XB YB)
 * point : vecteur double (X Y)
 * cr��e dans s_moved une copie du segment s perpendiculairement d�cal�e de l'offset off
 * RQ : le sens du d�placement du segment [AB] est tel que l'angle (BAB') > 0 avec B' le projet� de B
*
* @param s double [4]
* @param offset double
* @param s_moved double [4]
*
* @return bool
*/
void GeometryTools2D::perpendicularOffset(double* s, double offset, double* s_moved) {

	//d�calage point1

	//angle de rotation
	double a1 = atan2(s[3] - s[1], s[2] - s[0]);
	//taille segment
	double dist = sqrt((s[3] - s[1]) * (s[3] - s[1]) + (s[2] - s[0])*(s[2] - s[0]));

	//changement de repere
	s_moved[0] = s[0] + offset * sin(-a1);
	s_moved[1] = s[1] + offset * cos(-a1);

	//d�calage sym�trique du point 2
	s_moved[2] = s[2] + s_moved[0] - s[0];
	s_moved[3] = s[3] + s_moved[1] - s[1];
}

/**
* extractValues
*
* @param s double[]
*/
void GeometryTools2D::extractValues(double* s, int iBegin, int nbValues, double* res) {

	if (nbValues > 0)
	{
		for (int i = 0; i < nbValues; i++) {
			res[i] = s[iBegin + i];
		}
	}
}

/*/**
* equals
*
* @param p1 double[size1]
* @param size1
* @param p2 double[size2]
* @param size2
*/
bool GeometryTools2D::equals(double* p1, int size1, double* p2, int size2) {

	bool result = size1 == size2;
	for (int i = 0; i < size2 && result; i++)
	{
		result = equals(p1[i], p2[i]);
	}
	return result;
}

/**
* equals
*
* @param p1 double[]
* @param p2 double[]
* @param delta double
*/
bool GeometryTools2D::equals(double p1, double p2) {

	return equals(p1, p2, MY_DBL_EPSILON);
}
/**
* equals
*
* @param p1 double[]
* @param p2 double[]
* @param delta double
*/
bool GeometryTools2D::lessOrEquals(double p1, double p2) {

	return (p1 < p2) || equals(p1, p2, MY_DBL_EPSILON);
}

/**
* equals
*
* @param p1 double[]
* @param p2 double[]
* @param delta double
*/
bool GeometryTools2D::equals(double p1, double p2, double delta) {

	return abs(p1 - p2) <= delta;
}
/**
 * getSegmentDistance
 *
 * @param seg double[4]
 * @return double
 */
double GeometryTools2D::getSegmentDistance(double* seg) {

	double result = 0.0;

	result = sqrt((seg[0] - seg[2]) * (seg[0] - seg[2]) +
		(seg[1] - seg[3]) * (seg[1] - seg[3]));

	return result;
}
/**
* getSegmentDistance
*
* @param p1 double[2]
* @param p2 double[2]
* @return double
*/
double GeometryTools2D::getSegmentDistance(double* pt1, double* pt2) {

	double result = 0.0;

	result = sqrt((pt1[0] - pt2[0]) * (pt1[0] - pt2[0]) +
		(pt1[1] - pt2[1]) * (pt1[1] - pt2[1]));

	return result;
}

double GeometryTools2D::getDistancePointToLine(double* pt, double* seg) {
	double result = -1.0;
	double LineMag;
	double U;
	/*static*/ double intersection[2];

	LineMag = getSegmentDistance(seg);

	U = (((pt[0] - seg[0]) * (seg[2] - seg[0])) +
		((pt[1] - seg[1]) * (seg[3] - seg[1]))) /
		(LineMag * LineMag);

	/*   if (U < 0.0f || U > 1.0f) {
		 // closest point does not fall within the line segment
	   }
	   else */ {
		intersection[0] = seg[0] + U * (seg[2] - seg[0]);
		intersection[1] = seg[1] + U * (seg[3] - seg[1]);

		result = getSegmentDistance(pt, intersection);
	}
	return result;
}

/**
* pointInPolygon
*
* p : vecteur double (XA YA...XN YN)
* pt : vecteur double (2)
*
* @param p double[psize]
* @param psize
* @param pt double[2]
* @return bool
*/
bool GeometryTools2D::PointInPolygon(double*p, int psize, double* pt) {
	return PointInPolygon(p, psize, pt[0], pt[1]);
}

/**
 * is the point (px, py) in the polygon ?
 *
 * @param polygon double[]
 * @param px double
 * @param py double
 * @return bool
 */
bool GeometryTools2D::PointInPolygon(double* polygon, int psize, double px, double py) {

	int i, i_x, i_y, j, j_x, j_y;
	bool c = false;
	for (i = 0, j = psize - 1; i < psize; j = i++)
	{
		i_x = 2 * i;
		i_y = i_x + 1;
		j_x = 2 * j;
		j_y = j_x + 1;

		if (((polygon[i_y] > py) != (polygon[j_y] > py)) &&
			(px < (polygon[j_x] - polygon[i_x]) * (py - polygon[i_y]) / (polygon[j_y] - polygon[i_y]) + polygon[i_x]))
			c = !c;
	}

	return c;
}

/**
* is the point (px, py) close to the polygon ?
*
* @param polygon double[]
* @param px double
* @param py double
* @return bool
*/
bool GeometryTools2D::PointCloseToPolygon(double* polygon, int psize,
	double px, double py,
	double thresholdx, double thresholdy)
{

	int i, i_x, i_y, j, j_x, j_y;
	bool c = false;
	for (i = 0, j = psize - 1; i < psize; j = i++)
	{
		i_x = 2 * i;
		i_y = i_x + 1;
		j_x = 2 * j;
		j_y = j_x + 1;

		if (((polygon[i_y] > py) != (polygon[j_y] > py)) &&
			(px < (polygon[j_x] - polygon[i_x]) * (py - polygon[i_y]) / (polygon[j_y] - polygon[i_y]) + polygon[i_x]))
			c = !c;
	}

	return c;
}

/**
* PointsInPolygon
* Teste p_inside dans p. Un des points de l'un est-il inclus dans l'autre ?
* p_inside : vecteur double (XA YA...XN YN)
* p : vecteur double (XA' YA'...XN' YN')
*
* @param p_inside double[p_insidesize]
* @param p_insidesize
* @param p_inside double[psize]
* @param psize
* @return bool
*/
bool GeometryTools2D::PointsInPolygon(double* p_inside, int p_insidesize, double* p, int psize) {

	int	i = 0;
	/*static*/ double  pt[2];

	bool result = false;
	while (!result && i < p_insidesize)
	{
		extractValues(p_inside, i, 2, pt);
		result = PointInPolygon(p, psize, pt);
		i += 2;
	}

	return result;
}

/**
 * are Crossing segments ((xa1, ya1),(xb1, yb1)) and ((xa2, ya2),(xb2, yb2))
 *
 * @param xa double
 * @param ya double
 * @param xb double
 * @param yb double
 * @param xa2 double
 * @param ya2 double
 * @param xb2 double
 * @param yb2 double
 * @return bool
 */
bool GeometryTools2D::SegmentsCrossing(double xa1, double ya1,
	double xb1, double yb1,
	double xa2, double ya2,
	double xb2, double yb2,
	double& intersect_x, double& intersect_y) {
	bool result = false;
	// --------------------------------------------------------------------------
	// Le point I, intersection de AB et CD, est defini par:
	// 1) AI = s * AB
	// 2) CI = r * CD
	//
	// Les deux segments se coupent si s et r sont compris entre 0 et 1
	// --------------------------------------------------------------------------
	double DeltaX1 = (xb1 - xa1);
	double DeltaY1 = (yb1 - ya1);
	double DeltaX2 = (xb2 - xa2);
	double DeltaY2 = (yb2 - ya2);

	double denominateur = DeltaX1 * DeltaY2 - DeltaY1 * DeltaX2;

	if (denominateur != 0.0) {
		double r =
			((ya1 - ya2) * DeltaX2
				-
				(xa1 - xa2) * DeltaY2)
			/
			denominateur;
		if (0 <= r && r <= 1) {
			double s =
				((ya1 - ya2) * DeltaX1
					-
					(xa1 - xa2) * DeltaY1)
				/
				denominateur;
			result = (0 <= s && s <= 1);

			intersect_x = xa1 + ((xb1 - xa1) * r);
			intersect_y = ya1 + ((yb1 - ya1) * r);
		}
	}
	return result;
}


/**
* PolygonsCrossing
 * Methode :
 * p : vecteur double (XA YA...XN YN)
 * Teste les polygones s'intersectent
 * Renvoie true si les polygones s'intersectent
*
 * @param p1 double [size1]
 * @param size1 int
 * @param p2 double [size2]
 * @param size2 int
 * @return bool
*/
bool GeometryTools2D::PolygonsCrossing(double* p1, int size1, double* p2, int size2,
	double& intersect_x, double& intersect_y)
{
	bool result = false;

	double seg1[4];
	double seg2[4];

	int j = 0;
	for (j = 0; (j < size1) && !result; j += 2)
	{
		extractValues(p1, (j == 0) ? size1 - 2 : j - 2, 2, seg1);
		extractValues(p1, j, 2, seg1 + 2);

		for (int i = 0; (i < size2) && !result; i += 2) {

			extractValues(p2, (i == 0) ? size2 - 2 : i - 2, 2, seg2);
			extractValues(p2, i, 2, seg2 + 2);

			result = SegmentsCrossing(seg1[0], seg1[1], seg1[2], seg1[3],
				seg2[0], seg2[1], seg2[2], seg2[3],
				intersect_x, intersect_y);
		} //for
	}//for
	return result;
}

/**
  * hit
  *
  * @param polygon double[psize]
  * @param psize int
  * @param left double
  * @param top double
  * @param right double
  * @param bottom double
  * @return bool
  */
bool GeometryTools2D::PolygonIntersectBBox(double* polygon, int psize,
	double left, double top, double right, double bottom)
{
	bool result = false;

	// Variables locales
	int i;

	// --------------------------------------------------------------------------
	// On regarde si un point du polygone est dans le rectangle droit
	// --------------------------------------------------------------------------
	for (i = 0; !result && i < psize; i += 2) {
		result = (polygon[i] <= right) && (polygon[i] >= left) &&
			(polygon[i + 1] <= top) && (polygon[i + 1] >= bottom);
	}
	// --------------------------------------------------------------------------
	// On regarde si un point du rectangle droit est dans le polygone
	// --------------------------------------------------------------------------
	if (!result)
		result = PointInPolygon(polygon, psize, left, top);
	if (!result)
		result = PointInPolygon(polygon, psize, right, top);
	if (!result)
		result = PointInPolygon(polygon, psize, left, bottom);
	if (!result)
		result = PointInPolygon(polygon, psize, right, bottom);

	// --------------------------------------------------------------------------
	// On regarde s'il existe des intersections entre les segments du polygone
	// et ceux du rectangle droit
	// --------------------------------------------------------------------------
	if (!result) {

		int j;
		double xi, yi, xj, yj;

		for (i = 0, j = psize - 2; !result && i < psize; j = i,
			i += 2) {
			xi = polygon[i];
			yi = polygon[i + 1];

			xj = polygon[j];
			yj = polygon[j + 1];

			result = SegmentIntersectBBox(xi, yi, xj, yj, left, top, right, bottom);
		}

	}

	return result;
}

/**
* hitSegment with bounding box (left, top, right, bottom)
*
* @param xi double
* @param yi double
* @param xj double
* @param yj double
* @param left double
* @param top double
* @param right double
* @param bottom double
* @return bool
*/
bool GeometryTools2D::SegmentIntersectBBox(double xa, double ya, double xb, double yb,
	double left, double top, double right, double bottom) {
	bool result = false;
	double intersect_x = 0;
	double intersect_y = 0;

	result = SegmentsCrossing(xa, ya, xb, yb, left, top, left, bottom, intersect_x, intersect_y);
	if (!result)
		result = SegmentsCrossing(xa, ya, xb, yb, left, top, right, top, intersect_x, intersect_y);
	if (!result)
		result = SegmentsCrossing(xa, ya, xb, yb, right, bottom, right, top, intersect_x, intersect_y);
	if (!result)
		result = SegmentsCrossing(xa, ya, xb, yb, right, bottom, left, bottom, intersect_x, intersect_y);

	return result;
}

/**
 * BBoxIntersectBBox
 *
 * @param left1 double
 * @param top1 double
 * @param right1 double
 * @param bottom1 double
 * @param left2 double
 * @param top2 double
 * @param right2 double
 * @param bottom2 double
 * @return bool
 */
bool GeometryTools2D::BBoxIntersectBBox(double left1, double top1, double right1, double bottom1,
	double left2, double top2, double right2, double bottom2)
{
	bool result = false;

	result = (left2 < right1) && (left1 < right2) &&
		(bottom2 < top1) && (bottom1 < top2);
	return result;
}

/**
* GetPointsState
*	Determiner les statuts de chaque point du polygone (TPoint2DState)
*
* @param double * points
* @param TPoint2DState* states
* @param int size
* @return void
*/
void GeometryTools2D::GetPointsState(double * points, TPoint2DState* states, int size)
{
	double topMost = -std::numeric_limits<double>::max();
	double rightMost = -std::numeric_limits<double>::max();
	double bottomMost = std::numeric_limits<double>::max();
	double leftMost = std::numeric_limits<double>::max();

	int topMostIdx = -1;
	int rightMostIdx = -1;
	int bottomMostIdx = -1;
	int leftMostIdx = -1;

	//parcours de tous les points
	double x, y;
	int iPoint = 0;
	double *iter = points;
	double *end = points + size * 2;
	while (iter != end)
	{
		x = *iter;
		iter++;
		y = *iter;
		iter++;

		if (x > rightMost)
		{
			rightMost = x;
			rightMostIdx = iPoint;
		}
		if (x < leftMost)
		{
			leftMost = x;
			leftMostIdx = iPoint;
		}
		if (y > topMost)
		{
			topMost = y;
			topMostIdx = iPoint;
		}
		if (y < bottomMost)
		{
			bottomMost = y;
			bottomMostIdx = iPoint;
		}
		iPoint++;
	}

	if (size > 0)
	{
		states[topMostIdx] |= TPoint_TopMost;
		states[rightMostIdx] |= TPoint_RightMost;
		states[bottomMostIdx] |= TPoint_BottomMost;
		states[leftMostIdx] |= TPoint_LeftMost;
	}
}


/**
* AddMargins
*	Ajouter des marges en X et Y au polygone
*		si points_out est deja allou� a la bonne taille (size_out), pas de r�allocation
*
* @param double * points
* @param int size
* @param double xMargin
* @param double yMargin
* @param double ** points_out
* @param int& size_out
* @return void
*/
void GeometryTools2D::AddMargins(double * points, int size, double xMargin, double yMargin, double ** points_out, int& size_out)
{
	//pour une necessit� de fermeture g�ometrique de l'algo, on force une marge minimale infime
    xMargin = std::max<double>(0.01, xMargin);
    yMargin = std::max<double>(0.01, yMargin);

	//d�terminer l'�tat de chaque point
	TPoint2DState* pStates = new TPoint2DState[size];

	//init
	memset(pStates, TPoint_Undefined, size * sizeof(TPoint2DState));

	GetPointsState(points, pStates, size);

	//alloc optimis�e de 2 points en sortie par points en entr�e
	if (2 * size != size_out)
	{
		if (*points_out != NULL)
		{
			delete *points_out;
		}
		size_out = 2 * size;
		*points_out = new double[2 * size_out];
	}

	//necessit� d'une seconde passe
	bool need2ndStep = false;

	double* iterPointIn = points;
	double* iterPointOut = *points_out;
	TPoint2DState* iterStates = pStates;
	TPoint2DState* states_end = pStates + size;

	while (iterStates != states_end)
	{
		//si le point est bottommost, on ajoute 2 points (-xMargin, -yMargin) et (+xMargin, -yMargin)
		if (*iterStates & TPoint_BottomMost)
		{
			*iterPointOut = *iterPointIn - xMargin;
			*(iterPointOut + 1) = *(iterPointIn + 1) - yMargin;
			iterPointOut += 2;

			*iterPointOut = *iterPointIn + xMargin;
			*(iterPointOut + 1) = *(iterPointIn + 1) - yMargin;
			iterPointOut += 2;
		}
		//si le point est rightmost, on ajoute 2 points (+xMargin, -yMargin) et (+xMargin, +yMargin)
		if (*iterStates & TPoint_RightMost)
		{
			//ne pas doubler le point
			if (!(*iterStates & TPoint_BottomMost))
			{
				*iterPointOut = *iterPointIn + xMargin;
				*(iterPointOut + 1) = *(iterPointIn + 1) - yMargin;
				iterPointOut += 2;
			}
			*iterPointOut = *iterPointIn + xMargin;
			*(iterPointOut + 1) = *(iterPointIn + 1) + yMargin;
			iterPointOut += 2;
		}
		//si le point est topmost, on ajoute 2 points (+xMargin, +yMargin) et (-xMargin, +yMargin)
		if (*iterStates & TPoint_TopMost)
		{
			//ne pas doubler le point
			if (!(*iterStates & TPoint_RightMost))
			{
				*iterPointOut = *iterPointIn + xMargin;
				*(iterPointOut + 1) = *(iterPointIn + 1) + yMargin;
				iterPointOut += 2;
			}
			*iterPointOut = *iterPointIn - xMargin;
			*(iterPointOut + 1) = *(iterPointIn + 1) + yMargin;
			iterPointOut += 2;
		}
		//si le point est leftmost, on ajoute 2 points (-xMargin, +yMargin) et (-xMargin, -yMargin)
		if (*iterStates & TPoint_LeftMost)
		{
			//ne pas doubler le point
			if (!(*iterStates & TPoint_TopMost))
			{
				*iterPointOut = *iterPointIn - xMargin;
				*(iterPointOut + 1) = *(iterPointIn + 1) + yMargin;
				iterPointOut += 2;
			}

			//ne pas doubler le point
			if (!(*iterStates & TPoint_BottomMost))
			{
				*iterPointOut = *iterPointIn - xMargin;
				*(iterPointOut + 1) = *(iterPointIn + 1) - yMargin;
				iterPointOut += 2;
			}
		}

		//si l'�tat du point est ind�fini, on reserve le point en vue d'une seconde passe
		if (*iterStates == TPoint_Undefined)
		{
			//on stocke l'indice du point a reviser
			*iterStates = (TPoint2DState)(-(iterPointOut - *points_out)*0.5);

			//on reserve l'espace
			iterPointOut += 2;
			//necessit� d'une seconde passe
			need2ndStep = true;
		}

		iterStates++;
		iterPointIn += 2;
	}//	  while(iterStates != states_end)

	//necessit� d'une seconde passe
	if (need2ndStep)
	{
		//seconde passe pour les points a reviser
		iterStates = pStates;
		while (iterStates != states_end)
		{
			if (*iterStates <= 0)
			{
				int iPoint = -*iterStates;
				//point n-2
				int iPoint_NM2 = (iPoint >= 2) ? iPoint - 2 : size_out - 2;
				//point n+1
				int iPoint_NP1 = (iPoint + 1) % size_out;

				//le point manquant est l'intersection du segment pr�c�dent et du segment suivant
				intersectLines(*(points_out)+iPoint_NM2 * 2,
					*(points_out)+iPoint_NP1 * 2,
					*(points_out)+iPoint * 2);
			}
			iterStates++;
		}
	}

	delete pStates;
}


//*****************************************************************************
// Name : SetInRange
// Description : set the value in the interval [min, max]
// Parameters : * (in) double min
//              * (in) double max 
//              * (in) double value
// Return : void
//*****************************************************************************
double GeometryTools2D::SetInRange(double minRange, double maxRange, double value)
{
	double result = value;

	double tmp = minRange;
    minRange = std::min<double>(minRange, maxRange);
    maxRange = std::max<double>(tmp, maxRange);

	while (result < minRange)
	{
		result += maxRange - minRange;
	}
	while (result > maxRange)
	{
		result -= maxRange - minRange;
	}

	return result;
}