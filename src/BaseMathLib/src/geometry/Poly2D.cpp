// -*- C++ -*-
// ****************************************************************************
// Class: Poly2D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************
#include "BaseMathLib/geometry/Poly2D.h"
// ***************************************************************************
// Dependences
// ***************************************************************************

#include <algorithm>
#include <math.h>
#include <float.h>
#include <fstream>
#include "BaseMathLib/geometry/GeometryTools2D.h"

using namespace BaseMathLib;

#ifdef _TRACE_PERF
//#define _POLY2D_TRACE_PERF

#ifdef _POLY2D_TRACE_PERF
extern std::ofstream trace_ofs;
#endif
#endif

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
Poly2D::Poly2D()
{
	m_points = NULL;
	m_nbPoints = 0;
}

// Destructeur	
Poly2D::~Poly2D()
{
	Alloc(0);
}

// *********************************************************************
// M�thodes
// *********************************************************************

 /**
 * Alloc
 * Methode : allocation des points
 * @param void
 * @return void
 */
void Poly2D::Alloc(int nbPoints)
{
	if (m_nbPoints != nbPoints)
	{
		if (m_points != NULL)
		{
			delete m_points;
		}
		if (nbPoints > 0)
		{
			m_points = new double[2 * nbPoints];
		}
		else
		{
			m_points = NULL;
		}
		m_nbPoints = nbPoints;
	}
}

/**
* Intersect
* Methode : test d'intersection
* @param const Poly2D& other
* @return bool
*/
bool Poly2D::Intersect(Poly2D& other)
{
	bool result = false;

	//si les bounding box s'intersectent
	if (GetBoundingBox().Intersect(other.GetBoundingBox()))
	{
		//test des points de poly1 dans poly2
		result = GeometryTools2D::PointsInPolygon(GetPoints(), GetNbPoints(), other.GetPoints(), other.GetNbPoints());

		if (!result)
		{
			//test des points de poly2 dans poly1
			result = GeometryTools2D::PointsInPolygon(other.GetPoints(), other.GetNbPoints(), GetPoints(), GetNbPoints());
		}

		if (!result)
		{
			double intersect_x = 0;
			double intersect_y = 0;

			//test d'intersection des segments de poly1 et poly2
			result = GeometryTools2D::PolygonsCrossing(
				other.GetPoints(), other.GetNbPoints(),
				GetPoints(), GetNbPoints(), intersect_x, intersect_y);
		}
	}
	return result;
}

/**
* CloseTo
* Methode : test de distance
* @param const Poly2D& other
* @return bool
*/
bool Poly2D::CloseTo(Poly2D& other, double thresholdX, double thresholdY)
{
	bool result = false;

	/*
		//si les bounding box s'intersectent
		if(GetBoundingBox().CloseTo(other.GetBoundingBox(), thresholdX, thresholdY))
		{
			//test des points de poly1 dans poly2
			result = GeometryTools2D::PointsCloseToPolygon(GetPoints(), GetNbPoints(),
				other.GetPoints(), other.GetNbPoints(), thresholdX, thresholdY);

			if(!result)
			{
				//test des points de poly2 dans poly1
				result = GeometryTools2D::PointsCloseToPolygon(other.GetPoints(), other.GetNbPoints(),
					GetPoints(), GetNbPoints(), thresholdX, thresholdY);
			}
		}
	*/
	return result;
}

/**
* ComputeMinMaxIndex
* Methode : calcul du point minimal et maximal sur l'axe angulaire/X
* @param void
* @return double : area
*/
void Poly2D::ComputeMinMaxIndex(std::int32_t& minIndex, std::int32_t& maxIndex) const
{
	//recherche du point minimal et maximal sur l'axe angulaire/X
	minIndex = 0;
	maxIndex = 0;
	double minX = std::numeric_limits<double>::max();
	double maxX = -std::numeric_limits<double>::max();

	int nbPoints = GetNbPoints();

	for (int i = 0; i < nbPoints; i++)
	{
		double pointX = *GetPointX(i);
		if (pointX < minX)
		{
			minX = pointX;
			minIndex = i;
		}
		if (pointX > maxX)
		{
			maxX = pointX;
			maxIndex = i;
		}
	}

	int tmp = minIndex;
	minIndex = std::min<std::int32_t>(minIndex, maxIndex);
	maxIndex = std::max<std::int32_t>(maxIndex, tmp);
}

/**
* ComputeConvexOrthoArea
* Methode : calcul de la surface convexe en repere orthonorm�
* @param void
* @return double : area
*/
double Poly2D::ComputeConvexOrthoArea() const
{
	double result = 0;

	//recherche du point minimal et maximal sur l'axe X
	std::int32_t minIndex = 0;
	std::int32_t maxIndex = 0;
	ComputeMinMaxIndex(minIndex, maxIndex);

	//calcul d'aire global par soustraction de l'aire de l'enveloppe sup�rieure � l'aire de l'enveloppe inf�rieure
	//pour calculer l'aire en orthonorm� S = abs((x2-x1) * (y2+y1) * 0.5)
	double sumMinMax = 0.0;
	double sumMaxMin = 0.0;

	int nbPoints = GetNbPoints();
	int nbPointsMinMax = (maxIndex > minIndex) ? maxIndex - minIndex : (nbPoints - maxIndex) + minIndex;
	for (int i = 0, j = minIndex; i < nbPointsMinMax; i++, j = (j + 1) % nbPoints)
	{
		sumMinMax += fabs(0.5*	(*GetPointX((j + 1) % nbPoints) - *GetPointX(j))*
			(*GetPointY((j + 1) % nbPoints) + *GetPointY(j)));
	}
	int nbPointsMaxMin = (minIndex > maxIndex) ? maxIndex - minIndex : (nbPoints - maxIndex) + minIndex;
	for (int i = 0, j = maxIndex; i < nbPointsMaxMin; i++, j = (j + 1) % nbPoints)
	{
		sumMaxMin += fabs(0.5*	(*GetPointX((j + 1) % nbPoints) - *GetPointX(j))*
			(*GetPointY((j + 1) % nbPoints) + *GetPointY(j)));
	}
	result = fabs(sumMaxMin - sumMinMax);

	return result;
}

/**
* ComputeConvexPolarArea
* Methode : calcul de la surface convexe en repere polaire (Angle, Distance)
* @param void
* @return double : area
*/
double Poly2D::ComputeConvexPolarArea() const
{
	double result = 0;

	//recherche du point minimal et maximal sur l'axe angulaire
	std::int32_t minIndex = 0;
	std::int32_t maxIndex = 0;
	ComputeMinMaxIndex(minIndex, maxIndex);

	//calcul d'aire
	double sumMinMax = 0.0;
	double sumMaxMin = 0.0;

	//calcul d'aire global par soustraction de l'aire de l'enveloppe sup�rieure � l'aire de l'enveloppe inf�rieure
	//calcul de l'aire d'un triangle quelconque a partir de 2 cot�s adjacents a et b et de l'angle ab
	//S = 1/2 * a * b * sin(ab)

	int nbPoints = GetNbPoints();
	int nbPointsMinMax = (maxIndex > minIndex) ? maxIndex - minIndex : (nbPoints - maxIndex) + minIndex;
	for (int i = 0, j = minIndex; i < nbPointsMinMax; i++, j = (j + 1) % nbPoints)
	{
		double a = *GetPointY(j);
		double b = *GetPointY((j + 1) % nbPoints);
		double alpha = fabs(*GetPointX((j + 1) % nbPoints) - *GetPointX(j));

		sumMinMax += 0.5*a*b*sin(alpha);
	}
	int nbPointsMaxMin = (minIndex > maxIndex) ? maxIndex - minIndex : (nbPoints - maxIndex) + minIndex;
	for (int i = 0, j = maxIndex; i < nbPointsMaxMin; i++, j = (j + 1) % nbPoints)
	{
		double a = *GetPointY(j);
		double b = *GetPointY((j + 1) % nbPoints);
		double alpha = fabs(*GetPointX((j + 1) % nbPoints) - *GetPointX(j));

		sumMaxMin += 0.5*a*b*sin(alpha);
	}
	result = fabs(sumMaxMin - sumMinMax);

#ifdef _POLY2D_TRACE_PERF

	trace_ofs << "Min Index : " << minIndex << std::endl;
	trace_ofs << "Max Index : " << maxIndex << std::endl;
	trace_ofs << "Area  : " << result << std::endl;

	for (int i = 0; i < nbPoints; i++)
	{
		trace_ofs << "X : " << *GetPointX(i) << " Y : " << *GetPointY(i) << std::endl;
	}
	trace_ofs << std::endl;

#endif

	return result;
}
