cmake_minimum_required(VERSION 3.12)

project(BottomDetection)

basic_configure()

add_library(${PROJECT_NAME} SHARED ${MODULE_HEADERS} ${MODULE_SOURCES})
target_compile_definitions(${PROJECT_NAME} PRIVATE BOTTOMDETECTION_EXPORTS)

target_link_libraries(${PROJECT_NAME} PUBLIC M3DKernel Filtering)
target_include_directories(${PROJECT_NAME} PUBLIC ${INCLUDES_DIRS})

# Deploiement
if(WIN32)
install(TARGETS ${PROJECT_NAME}
        RUNTIME DESTINATION ${LIBRARY_DIR}
        COMPONENT ${PROJECT_NAME})
else(WIN32)
install(TARGETS ${PROJECT_NAME}
        LIBRARY DESTINATION ${LIBRARY_DIR}
        COMPONENT ${PROJECT_NAME})
endif(WIN32)
