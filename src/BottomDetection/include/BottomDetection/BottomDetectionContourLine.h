// -*- C++ -*-
// ****************************************************************************
// Class: BottomDetectionContourLine
//
// Description: D�tection de fond et lignes de niveau 

//
// Projet: MOVIES3D
// Auteur: Nicolas MENARD
// Date  : Octobre 2013
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "BottomDetection.h"
#include "BottomDetectionTypes.h"

#include <vector> 

class Sounder;
class ContourLineDef;
class FilterMgr;

// ***************************************************************************
// Declarations
// ***************************************************************************
class BOTTOMDETECTION_API BottomDetectionContourLine
	: public BottomDetectionAlgorithm
{
public:
	static const std::vector<int> s_AVAILABLE_CONTOUR_LEVELS;
	static const int s_MAX_SV_LEVEL_MAGIC_NUMBER;

	// structure contenant les isocourbes calcul�es
	struct sContourLine
	{
		// niveau de la ligne
		int iLevel;

		// indice de la ligne au dessus du fond
		int iUpperIndex;
		// indice de la ligne sous le fond
		int iLowerIndex;
	};

public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	BottomDetectionContourLine();

	// Destructeur
	virtual ~BottomDetectionContourLine(void);

	// Traitement
	virtual void ProcessBottom(PingFan* pFan, bool bChangeEditedPingsBottom, FilterMgr * pFilterMgr);

	// SounderChanged event
	virtual void SounderChanged(std::uint32_t sounderId);

	// description longue
	virtual std::string GetAlgorithmDesc();

	// Gestion de la configuration
	virtual bool Serialize(BaseKernel::MovConfig * movConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * movConfig);

	// m�thode utilitaire pour savoir si le niveau param�tr� doit �tre affich� ou non
	virtual bool DisplayLevel(int iLevel);

	// *********************************************************************
	// Accesseurs
	// *********************************************************************
	virtual std::vector<std::uint32_t> & GetSounderIDs();

	virtual std::vector<ContourLineDef *> & GetContourLinesDef();

	virtual int GetReferenceLevel();
	virtual void SetReferenceLevel(int iRefLevel);

	virtual bool GetMixedBottomDetermination();
	virtual void SetMixedBottomDetermination(bool bMixedDetermination);

	virtual bool GetDisplayMaximumEchoLine();
	virtual void SetDisplayMaximumEchoLine(bool bDisplayMaximumEchoLine);

	virtual double GetDefaultDepth();
	virtual void SetDefaultDepth(double dDefaultDepth);

	virtual const std::map<int, std::vector<sContourLine> > & GetContourLines(unsigned int sounderID, PingFan * pFan);

private:

	// *********************************************************************
	// M�thodes priv�es
	// *********************************************************************
	double getStdDevInHistory(unsigned int sounderID, int iChannel, double sampleSpacing);

	std::vector<int> Erode(const std::vector<int> & vectorToErode);

	// *********************************************************************
	// Donn�es priv�es
	// *********************************************************************

	// liste des IDs des sondeurs � traiter.
	unsigned int m_nbSounders;
	std::vector<std::uint32_t> m_SounderIDs;

	// d�finition des lignes de niveaux possibles
	unsigned int m_nbContourLines;
	std::vector<ContourLineDef *> m_tabContourLines;

	// niveau de r�f�rence
	int m_iReferenceLevel;

	// d�termination mixte ?
	bool m_bMixedDetermination;

	// affichage de la ligne d'�cho du fond max
	bool m_bDisplayMaximumEchoLine;

	// structure contenant les variables de travail pour un ping donn�
	struct sWorkingPingData
	{
		double dbCor1;
		double dbCor2;

		std::vector<double> dbMaxProf;

		std::vector<int>    iMaxProx;
	};

	// historique des 15 pings pr�c�dents, par sondeur
	std::map<unsigned int, std::vector<sWorkingPingData> > m_History;

	// historique des lignes de niveaux calcul�es, index�s par sondeur, ping, channel.
    std::map<unsigned int, std::map<std::uint64_t, std::map<int, std::vector<sContourLine> > > > m_ContourLines;

	// Evolution mantis 2238, valeur du fond par d�faut si le fond n'est pas trouv�
	double m_dDefaultDepth;
};

