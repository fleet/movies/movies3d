// -*- C++ -*-
// ****************************************************************************
// Class: BottomDetectionFixedBottom
//
// Description: On force le fond � une valeur fixe par rapport � la surface
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mars 2010
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "BottomDetection.h"

#include "BottomDetectionTypes.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
class BOTTOMDETECTION_API BottomDetectionFixedBottom
	: public BottomDetectionAlgorithmValue
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	BottomDetectionFixedBottom();

	// Destructeur
	virtual ~BottomDetectionFixedBottom(void);

	// Traitement
	virtual void ProcessBottom(PingFan* pFan, bool bChangeEditedPingsBottom, FilterMgr * pFilterMgr);

	// description longue
	virtual std::string GetAlgorithmDesc();

private:

	// *********************************************************************
	// Donn�es priv�es
	// *********************************************************************
};
