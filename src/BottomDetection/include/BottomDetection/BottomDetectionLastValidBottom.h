// -*- C++ -*-
// ****************************************************************************
// Class: BottomDetectionLastValidBottom
//
// Description: En cas de fond non reconnu, on r�cup�re le dernier fond
//              reconnu sur le channel concern�
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mars 2010
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "BottomDetection.h"

#include "BottomDetectionTypes.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
class BOTTOMDETECTION_API BottomDetectionLastValidBottom
	: public BottomDetectionAlgorithm
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	BottomDetectionLastValidBottom();

	// Destructeur
	virtual ~BottomDetectionLastValidBottom(void);

	// Traitement
	virtual void ProcessBottom(PingFan* pFan, bool bChangeEditedPingsBottom, FilterMgr * pFilterMgr);

	// description longue
	virtual std::string GetAlgorithmDesc();

private:

	// *********************************************************************
	// Donn�es priv�es
	// *********************************************************************
};
