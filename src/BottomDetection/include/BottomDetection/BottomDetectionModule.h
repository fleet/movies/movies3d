// -*- C++ -*-
// ****************************************************************************
// Class: BottomDetectionModule
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mars 2010
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "BottomDetection.h"
#include "BottomDetectionParameter.h"
#include "M3DKernel/module/ProcessModule.h"


// ***************************************************************************
// Declarations
// ***************************************************************************
class FilterMgr;
class BOTTOMDETECTION_API BottomDetectionModule : public ProcessModule
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	
	// Constructeur par d�faut
	MovCreateMacro(BottomDetectionModule);

	// Destructeur
	virtual ~BottomDetectionModule();

	// OTK - FAE219 - acc�s au filtres pour utilisation de leurs param�tres pour la d�tection du fond
	void setFilterMgr(FilterMgr * pFilterMgr);

	void PingFanAdded(PingFan *);
	void SounderChanged(std::uint32_t);
	void StreamClosed(const char *streamName);
	void StreamOpened(const char *streamName);

	// IPSIS - OTK - ajout accesseur sur param�tres pour la gestion des sorties
	BaseKernel::ParameterModule& GetParameterModule() { return m_parameter; }

	BottomDetectionParameter&	GetBottomDetectionParameter() { return m_parameter; }

protected:
	//Constructeur
	BottomDetectionModule();

	// param�tres du module de d�tection du fond
	BottomDetectionParameter	m_parameter;

private:
	FilterMgr * m_pFilterMgr;
};
