// -*- C++ -*-
// ****************************************************************************
// Class: BottomDetectionParameter
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mars 2010
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once


// ***************************************************************************
// Dependences
// ***************************************************************************
#include "BottomDetection.h"
#include "M3DKernel/parameter/ParameterModule.h"
#include "BottomDetectionTypes.h"

#include <vector>

class BottomDetectionContourLine;

// ***************************************************************************
// Declarations
// ***************************************************************************
class BOTTOMDETECTION_API BottomDetectionParameter :
	public BaseKernel::ParameterModule
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	BottomDetectionParameter(void);

	// Destructeur
	virtual ~BottomDetectionParameter(void);

	// gestion de la config
	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	// renvoi de l'algorithme activ�
	BottomDetectionAlgorithm* GetActivatedAlgo();
	void SetActivatedAlgo(BottomDetectionAlgorithmType type) { m_DetectionType = type; }

	// accesseurs
	const std::vector<BottomDetectionAlgorithm*>& GetAlgorithmDefinition() { return m_Algorithms; }
	BottomDetectionContourLine * GetContourLineAlgorithm();
	bool GetChangeEditedPingBottoms();
	void SetChangeEditedPingBottoms(bool bChangeEditedPingBottoms);

private:
	// *********************************************************************
	// Donn�es membres
	// *********************************************************************

	// type de l'algorithme de d�tection du fond utilis�
	BottomDetectionAlgorithmType m_DetectionType;

	// ensemble de la d�finition des algos de d�tection
	std::vector<BottomDetectionAlgorithm*> m_Algorithms;

	// FAE 192 - Option pour permettre ou non la modification de la sonde des pings edit�s
	bool m_bChangeEditedPingBottoms;
};
