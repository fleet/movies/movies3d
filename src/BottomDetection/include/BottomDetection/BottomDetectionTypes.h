// -*- C++ -*-
// ****************************************************************************
// Class: BottomDetectionAlgorithm
//
// Description: Classe de base pour les algos de detection du fond
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mars 2010
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

typedef enum BottomDetectionAlgorithmTypes
{
	eBDNone = 0,
	eBDLastValidBottom,
	eBDFixedBottom,
	eBDContourLine
} BottomDetectionAlgorithmType;

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "BottomDetection.h"
#include "M3DKernel/parameter/ParameterObject.h"
#include "M3DKernel/datascheme/PingFan.h"
#include <string>

// ***************************************************************************
// Declarations
// ***************************************************************************
class FilterMgr;
class BOTTOMDETECTION_API BottomDetectionAlgorithm
	: public BaseKernel::ParameterObject
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	BottomDetectionAlgorithm(std::string name, BottomDetectionAlgorithmType type);

	// Destructeur
	virtual ~BottomDetectionAlgorithm(void);

	// Gestion de la config
	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	// Traitement
	virtual void ProcessBottom(PingFan* pFan, bool bChangeEditedPingsBottom, FilterMgr * pFilterMgr) {};

	// SounderChanged event
	virtual void SounderChanged(std::uint32_t sounderId) {};

	// Accesseurs
	BottomDetectionAlgorithmType GetAlgorithmType() { return m_AlgorithmType; };
	std::string GetAlgorithmName() { return m_AlgorithmName; };
	virtual std::string GetAlgorithmDesc() { return GetAlgorithmName(); };


protected:

	// *********************************************************************
	// Donn�es prot�g�es
	// *********************************************************************
	std::string m_AlgorithmName;
	BottomDetectionAlgorithmType m_AlgorithmType;
};

// ****************************************************************************
// Class: BottomDetectionTypes
//
// Description: Algo de d�tection du fond n�cessitant une valeur pour
//              un param�tre.
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mars 2010
// Soci�t� : IPSIS
// ****************************************************************************
class BOTTOMDETECTION_API BottomDetectionAlgorithmValue
	: public BottomDetectionAlgorithm
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	BottomDetectionAlgorithmValue(std::string name, BottomDetectionAlgorithmType type);

	// Destructeur
	virtual ~BottomDetectionAlgorithmValue(void);

	// Gestion de la config
	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	// Accesseurs
	void SetValue(double value) { m_Value = value; };
	double GetValue() { return m_Value; };

	std::string GetAlgorithmName() { return m_AlgorithmName; };

protected:
	// *********************************************************************
	// Donn�es prot�g�es
	// *********************************************************************
	double m_Value;
};
