// -*- C++ -*-
// ****************************************************************************
// Class: ContourLineDef
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Janvier 2014
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "BottomDetection.h"

// d�pendances
#include "M3DKernel/parameter/ParameterDataType.h"
#include "M3DKernel/config/MovConfig.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
class BOTTOMDETECTION_API ContourLineDef :
	public BaseKernel::ParameterObject
{
public:
	// *********************************************************************
  // Constructeurs / Destructeur
  // *********************************************************************
	// Constructeur par d�faut
	ContourLineDef(void);

	// Destructeur
	virtual ~ContourLineDef(void);

	// *********************************************************************
  // M�thodes
  // *********************************************************************
	virtual bool Serialize(BaseKernel::MovConfig * movConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * movConfig);

	// *********************************************************************
	// Accesseurs
	// *********************************************************************
	virtual void SetLevel(int iLevel);
	virtual int GetLevel();

	virtual void SetCompute(bool bCompute);
	virtual bool GetCompute();

	virtual void SetDisplay(bool bDisplay);
	virtual bool GetDisplay();

private:
	// *********************************************************************
  // Variables membres
  // *********************************************************************
  // niveau associ� � la couche
	int m_iLevel;
	// ce niveau est-il � calculer ?
	bool m_bCompute;
	// ce niveau est-il � afficher ?
	bool m_bDisplay;
};
