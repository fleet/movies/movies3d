
#include "BottomDetection/BottomDetectionContourLine.h"

#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/PingFanContainer.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "Filtering/FilterMgr.h"
#include "Filtering/DataFilter.h"

#include "BottomDetection/ContourLineDef.h"

#include <math.h>
#include <float.h>
#include <algorithm>

using namespace BaseKernel;

using namespace std;

int contourLevels[] = { -50, -45, -40, -35, -30, -25, -20, -15, -10 };
const vector<int> BottomDetectionContourLine::s_AVAILABLE_CONTOUR_LEVELS = vector<int>(contourLevels, contourLevels + 9);
const int BottomDetectionContourLine::s_MAX_SV_LEVEL_MAGIC_NUMBER = 9999;


BottomDetectionContourLine::BottomDetectionContourLine()
	: BottomDetectionAlgorithm("ContourLine", eBDContourLine)
{
	// Construction de la config par d�faut
	m_nbSounders = 0;
	m_nbContourLines = (unsigned int)s_AVAILABLE_CONTOUR_LEVELS.size();
	for (size_t iContourLine = 0; iContourLine < s_AVAILABLE_CONTOUR_LEVELS.size(); iContourLine++)
	{
		ContourLineDef * pContourLine = new ContourLineDef;
		pContourLine->SetLevel(s_AVAILABLE_CONTOUR_LEVELS[iContourLine]);
		m_tabContourLines.push_back(pContourLine);
	}
	m_iReferenceLevel = -30;
	m_bDisplayMaximumEchoLine = true;
	m_bMixedDetermination = true;
	m_dDefaultDepth = 150;
}

BottomDetectionContourLine::~BottomDetectionContourLine(void)
{
	for (size_t i = 0; i < m_tabContourLines.size(); i++)
	{
		if (m_tabContourLines[i] != NULL)
		{
			delete m_tabContourLines[i];
			m_tabContourLines[i] = NULL;
		}
	}
}

void BottomDetectionContourLine::ProcessBottom(PingFan* pFan, bool bChangeEditedPingsBottom, FilterMgr * pFilterMgr)
{
	bool bChange = false;

	Sounder* pSound = pFan->getSounderRef();

	if (pSound)
	{
		// on ne traite pas les sondeurs qui ne doivent pas �tre trait�s !
		if (std::find(m_SounderIDs.begin(), m_SounderIDs.end(), pSound->m_SounderId) == m_SounderIDs.end())
		{
			return;
		}

		// R�cup�ration du filtre blind zone correspondant au type du sondeur
		double decTirDistance;
		if (dynamic_cast<SounderMulti*>(pSound))
		{
			decTirDistance = pFilterMgr->getBlindZoneFilterMulti()->getValue();
		}
		else
		{
			decTirDistance = pFilterMgr->getBlindZoneFilterMono()->getValue();
		}

		sWorkingPingData currentData;

		// pour chaque transducteur
		unsigned int nbTrans = pSound->GetTransducerCount();
		for (unsigned int iTrans = 0; iTrans < nbTrans; iTrans++)
		{
			Transducer * pTrans = pSound->GetTransducer(iTrans);

			// longueur du tir
			double sampleSpacing = pTrans->getBeamsSamplesSpacing();
			// OTK - FAE219 - on ne prend plus une marge de 4 longueurs d'impulsion mais on utilise
			// la distance d�finie pour les blindzone filters (cF. plus haut)
			//int decTir = (int)(4 * ceil(pTrans->m_pulseDuration / pTrans->m_timeSampleInterval));
			int decTir = (int)(decTirDistance / sampleSpacing);

			// Lecture et cr�ation de la matrice SV_Data
			MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(iTrans);

			BaseMathLib::Vector2I ampSize = pMemStruct->GetDataFmt()->getSize();
			const int beamNb = ampSize.x;
			const int echoNb = ampSize.y;

			// initialisation de la structure des variables de travail qui iront dans l'historique
			if (iTrans == 0)
			{
				currentData.iMaxProx.resize(beamNb*nbTrans, -1);
				currentData.dbMaxProf.resize(beamNb*nbTrans, -std::numeric_limits<double>::max());
			}

			map<int, vector<sContourLine> > & contourLinesForPing = m_ContourLines[pSound->m_SounderId][pFan->GetPingId()];

			// pour chaque channel ...
			for (int x = 0; x < beamNb; x++)
			{
				unsigned short softChannelId = pTrans->GetChannelId()[x];
				currentData.dbCor1 = pFan->GetNavAttitudeRef(softChannelId)->m_heaveMeter; //-0*tan(pitch);
				currentData.dbCor2 = cos(pFan->GetNavAttitudeRef(softChannelId)->m_pitchRad) * cos(pFan->GetNavAttitudeRef(softChannelId)->m_rollRad);

				// "hack" pour avoir un indice de channel dans le cas multi et mono faisceau et
				int iChannel = iTrans + x;

				// conteneur des lignes de contour � mettre � jour
				vector<sContourLine> & contourLinesForBeam = contourLinesForPing[iChannel];

				// 1- D�termination de la fin du tir, et exclusion du bullage �ventuel
				// ***********************************************************************
				int iDecTirForBeam = decTir;
				DataFmt * pVvalueOrigin = pMemStruct->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(x, 0));
				DataFmt * pVval = pVvalueOrigin;
				for (int y = 0; y < echoNb; y++)
				{
					if (*pVval++ < -3000)
					{
						// rmq. : on maximise par le nombre d'�chos pour ne pas avoir un index invalide
						iDecTirForBeam = min(echoNb - 1, max(iDecTirForBeam, y + 1));
						break;
					}
				}

				// 2- D�termination du Sv maximum du ping
				// *******************************************
				DataFmt dbSVMax = UNKNOWN_DB;
				int     iSVMax = -1;
				int     iMaxRec = echoNb - 1;
				for (int y = iDecTirForBeam + 1; y < echoNb; y++)
				{
					DataFmt pVvalue = *(pVvalueOrigin + y);
					if (iSVMax == -1)
					{
						iSVMax = y;
						dbSVMax = pVvalue;
					}
					else
					{
						if (pVvalue > dbSVMax)
						{
							dbSVMax = pVvalue;
							iSVMax = y;
						}
					}

					if (pVvalue > -20000)
					{
						iMaxRec = y;
					}
				}

				// 3- On examine la valeur de Sv_max.
				// *************************************
				int     iMaxProx = -1;
				DataFmt dbSVMaxProx = dbSVMax;
				bool bottomNotFound = true;
				if (dbSVMax < -3500)
				{
					// Pas de fond : on garde celui du ping pr�c�dent desfois qu'il y en ait eu un
					const vector<sWorkingPingData> & history = m_History[pSound->m_SounderId];
					if (!history.empty())
					{
						iMaxProx = history.back().iMaxProx[iChannel];
					}
				}
				else
				{
					iMaxProx = iSVMax;

					// dans le cas de la pr�sence d'un historique avec une valeur tr�s diff�rente,
					// on recherche plut�t pr�s de l'ancien iMaxProx.
					const vector<sWorkingPingData> & history = m_History[pSound->m_SounderId];
					if (!history.empty())
					{
						const sWorkingPingData & previousWorkingData = history.back();
						double currentValue = (iMaxProx + currentData.dbCor1 / sampleSpacing)*currentData.dbCor2;
						double oldIMaxProx = previousWorkingData.iMaxProx[iChannel];
						double oldValue = (oldIMaxProx + previousWorkingData.dbCor1 / sampleSpacing)*previousWorkingData.dbCor2;

						// calcul de l'�cart type de la profondeur max sur l'historique disponible
						double dbStdIR15 = getStdDevInHistory(pSound->m_SounderId, iChannel, sampleSpacing);

						if (fabs(currentValue - oldValue) > 3.0 * dbStdIR15)
						{
							// index d'�chantillon de m�me profondeur que le maximum du fond du ping pr�c�dent
							int iSameDepthIndex = (int)floor(oldValue / currentData.dbCor2 - currentData.dbCor1 / sampleSpacing);
							// recherche du max du ping dans un voisinage du max du ping pr�c�dent, large de 3x l'�cart-type (et apr�s le tir)
							int iNuu = (int)ceil(3.0*dbStdIR15 / sampleSpacing);
							int iUu = max(iDecTirForBeam, iSameDepthIndex - iNuu);
							bool bMorePlausibleMaxFound = false;
							for (int y = iUu; y <= min(echoNb - 1, iSameDepthIndex + iNuu); y++)
							{
								DataFmt pVvalue = pMemStruct->GetDataFmt()->GetValueToVoxel(x, y);
								if (pVvalue > -3500)
								{
									if (!bMorePlausibleMaxFound)
									{
										iMaxProx = y;
										dbSVMaxProx = pVvalue;
										bMorePlausibleMaxFound = true;
									}
									else
									{
										if (pVvalue > dbSVMaxProx)
										{
											iMaxProx = y;
											dbSVMaxProx = pVvalue;
										}
									}
								}
							}
						}
					}

					// D�termination des lignes de niveau
					// ***************************************
					contourLinesForBeam.reserve(m_tabContourLines.size() + 1);
					for (size_t iContourLine = 0; iContourLine < m_tabContourLines.size(); iContourLine++)
					{
						ContourLineDef * pContourLine = m_tabContourLines[iContourLine];
						if (pContourLine->GetCompute())
						{
							sContourLine newContourLine;
							newContourLine.iLevel = pContourLine->GetLevel();

							short contourLineLevel = 100 * pContourLine->GetLevel();
							DataFmt * pDataOrigin = pMemStruct->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(x, 0));
							DataFmt * pData;

							// on recherche la ligne au dessus du fond
							bool bFound = false;
							for (int y = iMaxProx; y >= 2 && !bFound; y--)
							{
								pData = pDataOrigin + (y - 2);
								if (*pData++ < contourLineLevel
									&& *pData++ < contourLineLevel
									&& *pData++ < contourLineLevel
									&& *pData++ < contourLineLevel)
								{
									newContourLine.iUpperIndex = min(echoNb - 1, y + 2);
									bFound = true;
								}
							}
							if (!bFound)
							{
								newContourLine.iUpperIndex = min(echoNb - 1, iDecTirForBeam);
							}

							// on recherche la ligne au dessous du fond
							bFound = false;
							for (int y = iMaxProx + 1; y < min(iMaxRec, echoNb - 1) && !bFound; y++)
							{
								pData = pDataOrigin + (y - 2);
								if (*pData++ < contourLineLevel
									&& *pData++ < contourLineLevel
									&& *pData++ < contourLineLevel
									&& *pData++ < contourLineLevel)
								{
									newContourLine.iLowerIndex = max(0, y - 3);
									bFound = true;
								}
							}
							if (!bFound)
							{
								newContourLine.iLowerIndex = iMaxRec;
							}

							// ajout de la ligne de contour dans la structure des r�sultats
							contourLinesForBeam.push_back(newContourLine);
						}
					} // fin du calcul des lignes de niveaux

					// D�termination du fond
					// ***************************************
					int iDetectedBottom = -1;
					if (!m_bMixedDetermination)
					{
						for (size_t iContourLine = 0; iContourLine < contourLinesForBeam.size(); iContourLine++)
						{
							if (contourLinesForBeam[iContourLine].iLevel >= m_iReferenceLevel)
							{
								iDetectedBottom = contourLinesForBeam[iContourLine].iUpperIndex;
								break;
							}
						}
					}
					else
					{
						// examen des d�calage entre les lignes ->modif nlb: on calcule le d�calage pour la ligne de r�f�rence aussi
						vector<int> decLines;
						decLines.reserve(contourLinesForBeam.size());
						size_t iContourLine;
						for (iContourLine = 0; iContourLine < (contourLinesForBeam.size() - 2) && contourLinesForBeam[iContourLine].iLevel <= m_iReferenceLevel; iContourLine++)
						{
							decLines.push_back(contourLinesForBeam[iContourLine + 1].iUpperIndex - contourLinesForBeam[iContourLine].iUpperIndex);
						}
						// pour la ligne de r�f�rence, on met 2 arbitrairement -> modif nlb: on ne met la valeur arbitraire que s'il n'y a pas de ligne suivante disponible
						if (contourLinesForBeam[iContourLine].iLevel <= m_iReferenceLevel)
						{
							decLines.push_back(2);
						}

						// on part de la fin et on prend la premi�re ligne qui a une espacement > 2 ou croissant par rapport � la ligne
						// d'avant
						int iBottomLine = -1;
						for (int iLine = (int)decLines.size() - 2; iLine >= 0; iLine--)
						{
							if (decLines[iLine] > 2 ||
								decLines[iLine] > decLines[iLine + 1])
							{
								iBottomLine = iLine;
								break;
							}
						}
						if (iBottomLine == -1)
						{
							iBottomLine = 0;
						}
						else
						{
							iBottomLine = min((int)contourLinesForBeam.size() - 1, iBottomLine + 1);
						}
						iDetectedBottom = contourLinesForBeam[iBottomLine].iUpperIndex;
					}

					// Ajout de la ligne de niveau SV max (apr�s la recherche du fond ci-dessus pour ne pas perturber l'algo)
					sContourLine contourLineMaxSV;
					contourLineMaxSV.iLevel = s_MAX_SV_LEVEL_MAGIC_NUMBER;
					contourLineMaxSV.iUpperIndex = contourLineMaxSV.iLowerIndex = iMaxProx;
					contourLinesForBeam.push_back(contourLineMaxSV);

					// positionnement du fond � partir de iDetectedBottom
					if (iDetectedBottom != -1)
					{
						bottomNotFound = false;
						// FAE 192 - on n'applique pas la modification si le ping a d�j� �t� �dit� manuellement
						if (bChangeEditedPingsBottom || pFan->getRefBeamDataObject(softChannelId, softChannelId)->m_tupleAttribute == 0)
						{
							bool bFound = true;
							// rmq. : on ne retranche pas 1 ici comme dans le script matlab (indexation � 0 en C++)
							pFan->setBottom(softChannelId, (std::int32_t)((double)(iDetectedBottom + pTrans->GetSampleOffset())*sampleSpacing*1000.0 + 0.5), bFound);
							bChange = true;
						}
					}
				} // fin de la boucle

				// NMD -  evolution mantis 2238
				if (bottomNotFound)
				{
					// FAE 192 - on n'applique pas la modification si le ping a d�j� �t� �dit� manuellement
					if (bChangeEditedPingsBottom || pFan->getRefBeamDataObject(softChannelId, softChannelId)->m_tupleAttribute == 0)
					{
						bool bFound = true;
						bChange = true;
						pFan->setBottom(softChannelId, m_dDefaultDepth * 1000.0, bFound);
					}
					else
					{
						double pouet = 0;
					}
				}

				// on positionne le iMaxProx et le dbMaxProf dans l'historique
				currentData.iMaxProx[iChannel] = iMaxProx;
				if (iMaxProx != -1)
				{
					currentData.dbMaxProf[iChannel] = (iMaxProx*sampleSpacing + currentData.dbCor1)*currentData.dbCor2;
				}
			} // fin de la boucle sur les channels
		} // fin de la boucle sur les transducteurs

		// Ajout des donn�es de travail � l'historique
		vector<sWorkingPingData> & sounderHistory = m_History[pSound->m_SounderId];
		sounderHistory.push_back(currentData);
		int nbUnusedData = (int)sounderHistory.size() - 15;
		if (nbUnusedData > 0)
		{
			sounderHistory.erase(sounderHistory.begin(), sounderHistory.begin() + nbUnusedData);
		}
	} // fi pSound != NULL

	if (bChange)
	{
		// mise � jour du computePingFan (ranges min et max)
		pFan->computeRanges();
	}

	// nettoyage de l'historique des lignes de contour.
	// pour chaque sondeur ...
    map<unsigned int, map<std::uint64_t, map<int, vector<sContourLine> > > >::iterator iterSounder;
	for (iterSounder = m_ContourLines.begin(); iterSounder != m_ContourLines.end(); iterSounder++)
	{
        map<std::uint64_t, map<int, vector<sContourLine> > >::iterator iterPing;
		for (iterPing = iterSounder->second.begin(); iterPing != iterSounder->second.end(); iterPing++)
		{
			if (!M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(iterPing->first, false))
			{
				// Si le ping n'est plus r�f�renc� par le container, on le supprime de l'historique.
				iterPing = iterSounder->second.erase(iterPing);
			}
			else
			{
				// Si le ping est toujours r�f�renc�, on ne va pas plus loin car ils sont ordonn�s par date, donc les suivants
				// seront toujours r�f�renc�s �galement
				break;
			}
		}
	}
}

double BottomDetectionContourLine::getStdDevInHistory(unsigned int sounderID, int iChannel, double sampleSpacing)
{
	const vector<sWorkingPingData> & history = m_History[sounderID];
	double dbMean = 0.0;
	int n = 0;
	for (size_t i = 0; i < history.size(); i++)
	{
		double dbMaxProf = history[i].dbMaxProf[iChannel];
		if (dbMaxProf > -std::numeric_limits<double>::max())
		{
			dbMean += dbMaxProf;
			n++;
		}
	}
	dbMean /= static_cast<double>(n);
	double dbStdDev = 0.0;
	for (size_t i = 0; i < history.size(); i++)
	{
		double dbMaxProf = history[i].dbMaxProf[iChannel];
		if (dbMaxProf > -std::numeric_limits<double>::max())
		{
			dbStdDev += (dbMaxProf - dbMean)*(dbMaxProf - dbMean);
		}
	}

	// on utilise l'�cart type calcul� uniquement si on a au moins 2 valeurs
	if (n > 1)
	{
		dbStdDev = sqrt(dbStdDev / static_cast<double>(n));
	}
	else
	{
		dbStdDev = 2.0 * sampleSpacing;
	}

	return dbStdDev;
}

vector<int> BottomDetectionContourLine::Erode(const vector<int> & vectorToErode)
{
	vector<int> result(vectorToErode.size(), 0);
	int sum;
	int size = (int)result.size();
	for (int i = 0; i < size; i++)
	{
		sum = 0;
		int jmax = min(size, i + 2);
		for (int j = max(0, i - 2); j < jmax; j++)
		{
			sum += vectorToErode[j];
		}
		if (sum == 4)
		{
			result[i] = 1;
		}
	}
	return result;
}

// SounderChanged event
void BottomDetectionContourLine::SounderChanged(std::uint32_t sounderId)
{
	// Nettoyage des historiques
	m_ContourLines.clear();
	m_History.clear();
}

// description longue
string BottomDetectionContourLine::GetAlgorithmDesc()
{
	string result = "Contour line";

	return result;
}

// Gestion de la configuration
bool BottomDetectionContourLine::Serialize(BaseKernel::MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, m_AlgorithmName.c_str());

	// s�rialisation des donn�es membres
	movConfig->SerializeData<int>(m_iReferenceLevel, eInt, "ReferenceLevel");
	movConfig->SerializeData<bool>(m_bDisplayMaximumEchoLine, eBool, "DisplayMaximumEchoLine");
	movConfig->SerializeData<bool>(m_bMixedDetermination, eBool, "MixedBottomDetermination");
	movConfig->SerializeData<double>(m_dDefaultDepth, eDouble, "DefaultBottom");

	// s�rialisation des sondeurs
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "Sounders");
	movConfig->SerializeData<unsigned int>((unsigned int)m_SounderIDs.size(), eUInt, "SoundersNumber");
	for (size_t i = 0; i < m_SounderIDs.size(); i++)
	{
		movConfig->SerializeData<std::uint32_t>(m_SounderIDs[i], eParameterObject, "SounderID");
	}
	movConfig->SerializePushBack();

	movConfig->SerializeData<unsigned int>(m_nbContourLines, eUInt, "ContourLinesNumber");
	for (size_t i = 0; i < m_tabContourLines.size(); i++)
	{
		m_tabContourLines[i]->Serialize(movConfig);
	}

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();

	return true;
}

bool BottomDetectionContourLine::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = true;
	// debut de la d�s�rialisation du module
	if (result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, m_AlgorithmName.c_str()))
	{
		// d�s�rialisation des donn�es membres
		result = movConfig->DeSerializeData<int>(&m_iReferenceLevel, eInt, "ReferenceLevel") && result;
		result = movConfig->DeSerializeData<bool>(&m_bDisplayMaximumEchoLine, eBool, "DisplayMaximumEchoLine") && result;
		result = movConfig->DeSerializeData<bool>(&m_bMixedDetermination, eBool, "MixedBottomDetermination") && result;
		result = movConfig->DeSerializeData<double>(&m_dDefaultDepth, eDouble, "DefaultBottom") && result;

		// d�s�rialisation des sondeurs   
		m_SounderIDs.clear();

		if (result = movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "Sounders") && result)
		{
			result = movConfig->DeSerializeData<unsigned int>(&m_nbSounders, eUInt, "SoundersNumber") && result;
			for (unsigned int i = 0; i < m_nbSounders; i++)
			{
				std::uint32_t sounderID;
				result = movConfig->DeSerializeData<std::uint32_t>(&sounderID, eUInt, "SounderID") && result;
				m_SounderIDs.push_back(sounderID);
			}
			movConfig->DeSerializePushBack();
		}

		result = movConfig->DeSerializeData<unsigned int>(&m_nbContourLines, eUInt, "ContourLinesNumber") && result;
		// r�initialisation du tableau m_tabContourLines
		for (size_t i = 0; i < m_tabContourLines.size(); i++)
		{
			delete m_tabContourLines[i];
		}
		m_tabContourLines.clear();

		// lecture de la d�finition des lignes de niveau
		ContourLineDef * pContourLineDef = 0;
		for (unsigned int i = 0; i < m_nbContourLines; i++)
		{
			pContourLineDef = new ContourLineDef();
			result = pContourLineDef->DeSerialize(movConfig) && result;
			m_tabContourLines.push_back(pContourLineDef);
		}

		// fin de la d�s�rialisation du module
		movConfig->DeSerializePushBack();
	}

	return result;
}

// m�thode utilitaire pour savoir si le niveau param�tr� doit �tre affich� ou non
bool BottomDetectionContourLine::DisplayLevel(int iLevel)
{
	bool bDisplay = false;
	if (iLevel == BottomDetectionContourLine::s_MAX_SV_LEVEL_MAGIC_NUMBER)
	{
		bDisplay = m_bDisplayMaximumEchoLine;
	}
	else
	{
		for (size_t iLine = 0; iLine < m_tabContourLines.size(); iLine++)
		{
			if (m_tabContourLines[iLine]->GetLevel() == iLevel)
			{
				bDisplay = m_tabContourLines[iLine]->GetDisplay();
				break;
			}
		}
	}
	return bDisplay;
}

std::vector<std::uint32_t> & BottomDetectionContourLine::GetSounderIDs()
{
	return m_SounderIDs;
}

std::vector<ContourLineDef *> & BottomDetectionContourLine::GetContourLinesDef()
{
	return m_tabContourLines;
}

int BottomDetectionContourLine::GetReferenceLevel()
{
	return m_iReferenceLevel;
}

void BottomDetectionContourLine::SetReferenceLevel(int iRefLevel)
{
	m_iReferenceLevel = iRefLevel;
}

bool BottomDetectionContourLine::GetMixedBottomDetermination()
{
	return m_bMixedDetermination;
}

void BottomDetectionContourLine::SetMixedBottomDetermination(bool bMixedDetermination)
{
	m_bMixedDetermination = bMixedDetermination;
}

bool BottomDetectionContourLine::GetDisplayMaximumEchoLine()
{
	return m_bDisplayMaximumEchoLine;
}

void BottomDetectionContourLine::SetDisplayMaximumEchoLine(bool bDisplayMaximumEchoLine)
{
	m_bDisplayMaximumEchoLine = bDisplayMaximumEchoLine;
}

double BottomDetectionContourLine::GetDefaultDepth()
{
	return m_dDefaultDepth;
}

void BottomDetectionContourLine::SetDefaultDepth(double dDefaultDepth)
{
	m_dDefaultDepth = dDefaultDepth;
}

const map<int, vector<BottomDetectionContourLine::sContourLine> > & BottomDetectionContourLine::GetContourLines(
	unsigned int sounderID, PingFan * pFan)
{
	return m_ContourLines[sounderID][pFan->GetPingId()];
}

