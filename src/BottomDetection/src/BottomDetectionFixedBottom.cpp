

#include "BottomDetection/BottomDetectionFixedBottom.h"

#include "M3DKernel/datascheme/Transducer.h"

#include <sstream>

using namespace std;

BottomDetectionFixedBottom::BottomDetectionFixedBottom()
	: BottomDetectionAlgorithmValue("FixedBottom", eBDFixedBottom)
{
	m_Value = 100; // 100m par d�faut...
}

BottomDetectionFixedBottom::~BottomDetectionFixedBottom(void)
{
}

void BottomDetectionFixedBottom::ProcessBottom(PingFan* pFan, bool bChangeEditedPingsBottom, FilterMgr * pFilterMgr)
{
	Sounder* pSound = pFan->getSounderRef();

	// pour chaque transducteur
	unsigned int nbTrans = pSound->GetTransducerCount();
	for (unsigned int iTrans = 0; iTrans < nbTrans; iTrans++)
	{
		Transducer * pTrans = pSound->GetTransducer(iTrans);
		double sampleSpacing = pTrans->getBeamsSamplesSpacing();

		// pour chaque channel
		MemoryStruct *pPolarMem = pFan->GetMemorySetRef()->GetMemoryStruct(iTrans);
		BaseMathLib::Vector2I size = pPolarMem->GetDataFmt()->getSize();
		int beamNb = size.x;
		int echoNb = size.y;
		for (int iBeam = 0; iBeam < beamNb; iBeam++)
		{
			unsigned short softChanId = pTrans->GetChannelId()[iBeam];
			// si le tuple ping n'a pas �t� �dit� manuellement...
			if (bChangeEditedPingsBottom || pFan->getRefBeamDataObject(softChanId, softChanId)->m_tupleAttribute == 0)
			{
				// pour avoir le numero d'�cho correspondant au fond voulu, 
				// il suffit d'interpoler le numero de l'�cho � partir 
				// des �chos extr�mes du faisceau (plus rapide que de boucler sur les �chos
				// pour trouver le bon)
				double depth1 = pSound->GetPolarToWorldCoord(pFan, iTrans, iBeam, 0).z;
				double depth2 = pSound->GetPolarToWorldCoord(pFan, iTrans, iBeam, echoNb - 1).z;
				double deltaDepth = depth2 - depth1;

				// cas du transducteur horizontal : fond non reconnu
				if (deltaDepth == 0.0)
				{
					pFan->setBottom(softChanId, FondNotFound, false);
				}
				else
				{
					// interpolation du fond
					double detectedBottomRange = ((m_Value - depth1) / deltaDepth)*(double)(echoNb - 1)*sampleSpacing;
					// OTK - FAE078 - si le fond interpol� ne se trouve pas entre depth1 et depth2 (cas des faisceaux tr�s d�point�s par exemple,
					// on met un fond non reconnu
					if (detectedBottomRange < depth1 || detectedBottomRange > depth2)
					{
						pFan->setBottom(softChanId, FondNotFound, false);
					}
					else
					{
						pFan->setBottom(softChanId, (std::int32_t)(detectedBottomRange*1000.0 + 0.5), true);
					}
				}
			}
		}
	}
	// mise � jour du computePingFan (ranges min et max)
	pFan->computeRanges();
}

// description longue
string BottomDetectionFixedBottom::GetAlgorithmDesc()
{
	ostringstream oss;
	oss << "Fixed Bottom : [" << GetValue() << "] m";
	return oss.str();
}
