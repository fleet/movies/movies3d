

#include "BottomDetection/BottomDetectionLastValidBottom.h"

#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/PingFanContainer.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

using namespace std;

BottomDetectionLastValidBottom::BottomDetectionLastValidBottom()
	: BottomDetectionAlgorithm("LastValidBottom", eBDLastValidBottom)
{
}

BottomDetectionLastValidBottom::~BottomDetectionLastValidBottom(void)
{
}

void BottomDetectionLastValidBottom::ProcessBottom(PingFan* pFan, bool bChangeEditedPingsBottom, FilterMgr * pFilterMgr)
{
	bool change = false;

	Sounder* pSound = pFan->getSounderRef();

	// pour chaque transducteur
	unsigned int nbTrans = pSound->GetTransducerCount();
	for (unsigned int iTrans = 0; iTrans < nbTrans; iTrans++)
	{
		Transducer * pTrans = pSound->GetTransducer(iTrans);

		// pour chaque channel
		MemoryStruct *pPolarMem = pFan->GetMemorySetRef()->GetMemoryStruct(iTrans);
		BaseMathLib::Vector2I size = pPolarMem->GetDataFmt()->getSize();
		int beamNb = size.x;
		int echoNb = size.y;
		for (int iBeam = 0; iBeam < beamNb; iBeam++)
		{
			unsigned short softChanId = pTrans->GetChannelId()[iBeam];
			// si le tuple ping n'a pas �t� �dit� manuellement...
			if (bChangeEditedPingsBottom || pFan->getRefBeamDataObject(softChanId, softChanId)->m_tupleAttribute == 0)
			{
				// si le fond n'est pas reconnu...
				bool bottomFound;
				std::uint32_t echoFondEval;
				std::int32_t bottomRange;
				pFan->getBottom(softChanId, echoFondEval, bottomRange, bottomFound);
				if (!bottomFound)
				{
					// r�cup�ration du dernier fond reconnu pour ce channel
					TimeObjectContainer* pCont = M3DKernel::GetInstance()->getObjectMgr()
						->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(pSound->m_SounderId);
					int nbPings = (int)pCont->GetObjectCount();
					bool found = false;
					for (int pingIdx = nbPings - 1; pingIdx >= 0 && !found; pingIdx--)
					{
						PingFan* lastFan = (PingFan*)pCont->GetDatedObject(pingIdx);
						lastFan->getBottom(softChanId, echoFondEval, bottomRange, bottomFound);
						// si le fond est d�tect�, on assigne cette valeur au fond du nouveau ping
						if (bottomFound)
						{
							found = true;
							change = true;
							pFan->setBottom(softChanId, bottomRange, found);
						}
					}
				}
			}
		}
	}
	if (change)
	{
		// mise � jour du computePingFan (ranges min et max)
		pFan->computeRanges();
	}
}

// description longue
string BottomDetectionLastValidBottom::GetAlgorithmDesc()
{
	string result = "Last Valid Bottom";

	return result;
}

