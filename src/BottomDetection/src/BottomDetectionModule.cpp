

#include "BottomDetection/BottomDetectionModule.h"

BottomDetectionModule::BottomDetectionModule()
	: ProcessModule("Bottom Detection Module")
	, m_pFilterMgr(nullptr)
{
}

BottomDetectionModule::~BottomDetectionModule()
{
}

void BottomDetectionModule::setFilterMgr(FilterMgr * pFilterMgr)
{
	m_pFilterMgr = pFilterMgr;
}

// Traitement du pingFan ajout�
void BottomDetectionModule::PingFanAdded(PingFan *p)
{
	if (!getEnable())
		return;

	if (m_parameter.GetActivatedAlgo())
	{
		m_parameter.GetActivatedAlgo()->ProcessBottom(p, m_parameter.GetChangeEditedPingBottoms(), m_pFilterMgr);
	}
}

// changement des propri�t�s d'un sondeur
void BottomDetectionModule::SounderChanged(std::uint32_t sounderId)
{
	for (size_t iAlgo = 0; iAlgo < m_parameter.GetAlgorithmDefinition().size(); iAlgo++)
	{
		m_parameter.GetAlgorithmDefinition()[iAlgo]->SounderChanged(sounderId);
	}
}

void BottomDetectionModule::StreamClosed(const char *streamName)
{
}

void BottomDetectionModule::StreamOpened(const char *streamName)
{
}
