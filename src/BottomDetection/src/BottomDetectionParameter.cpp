#include "BottomDetection/BottomDetectionParameter.h"
#include "BottomDetection/BottomDetectionLastValidBottom.h"
#include "BottomDetection/BottomDetectionFixedBottom.h"
#include "BottomDetection/BottomDetectionContourLine.h"

// d�pendances
#include "M3DKernel/config/MovConfig.h"


using namespace BaseKernel;

// constructeur
BottomDetectionParameter::BottomDetectionParameter(void) : ParameterModule("BottomDetectionParameter")
{
	m_Algorithms.push_back(new BottomDetectionAlgorithm("Disabled", eBDNone));
	m_Algorithms.push_back(new BottomDetectionLastValidBottom());
	m_Algorithms.push_back(new BottomDetectionFixedBottom());
	m_Algorithms.push_back(new BottomDetectionContourLine());

	m_DetectionType = eBDNone;
	m_bChangeEditedPingBottoms = false;
}

// Destructeur
BottomDetectionParameter::~BottomDetectionParameter(void)
{
	for (size_t i = 0; i < m_Algorithms.size(); i++)
	{
		delete m_Algorithms[i];
	}
}

// sauvegarde des param�tres dans le fichier de configuration associ�
bool BottomDetectionParameter::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	movConfig->SerializeData<int>(m_DetectionType, eInt, "EnabledDetectionType");
	for (size_t i = 0; i < m_Algorithms.size(); i++)
	{
		m_Algorithms[i]->Serialize(movConfig);
	}
	movConfig->SerializeData<bool>(m_bChangeEditedPingBottoms, eBool, "ChangeEditedPingBottoms");

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();

	return true;
}

// lecture des param�tres dans le fichier de configuration associ�
bool BottomDetectionParameter::DeSerialize(MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	// debut de la d�s�rialisation du module
	if (result)
	{
		int detecType = (int)eBDNone;
		result = result && movConfig->DeSerializeData<int>(&detecType, eInt, "EnabledDetectionType");
		if (result)
		{
			m_DetectionType = (BottomDetectionAlgorithmType)detecType;
		}
		for (size_t i = 0; i < m_Algorithms.size(); i++)
		{
			m_Algorithms[i]->DeSerialize(movConfig);
		}
		movConfig->DeSerializeData<bool>(&m_bChangeEditedPingBottoms, eBool, "ChangeEditedPingBottoms");
	}
	// fin de la d�s�rialisation du module
	movConfig->DeSerializePushBack();

	return result;
}

// renvoi de l'algorithme activ�
BottomDetectionAlgorithm* BottomDetectionParameter::GetActivatedAlgo()
{
	for (size_t i = 0; i < m_Algorithms.size(); i++)
	{
		if (m_Algorithms[i]->GetAlgorithmType() == m_DetectionType)
		{
			return m_Algorithms[i];
		}
	}

	return NULL;
}

BottomDetectionContourLine * BottomDetectionParameter::GetContourLineAlgorithm()
{
	return dynamic_cast<BottomDetectionContourLine*>(m_Algorithms[3]);
}

bool BottomDetectionParameter::GetChangeEditedPingBottoms()
{
	return m_bChangeEditedPingBottoms;
}

void BottomDetectionParameter::SetChangeEditedPingBottoms(bool bChangeEditedPingBottoms)
{
	m_bChangeEditedPingBottoms = bChangeEditedPingBottoms;
}
