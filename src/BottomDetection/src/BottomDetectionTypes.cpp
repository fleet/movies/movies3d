

#include "BottomDetection/BottomDetectionTypes.h"

#include "M3DKernel/config/MovConfig.h"

using namespace std;
using namespace BaseKernel;

BottomDetectionAlgorithm::BottomDetectionAlgorithm(string name, BottomDetectionAlgorithmType type)
	: BaseKernel::ParameterObject()
{
	m_AlgorithmName = name;
	m_AlgorithmType = type;
}

BottomDetectionAlgorithm::~BottomDetectionAlgorithm(void)
{
}

bool BottomDetectionAlgorithm::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, m_AlgorithmName.c_str());
	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	return true;
}

bool BottomDetectionAlgorithm::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = true;
	// debut de la d�s�rialisation du module
	if (result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, m_AlgorithmName.c_str()))
	{
		movConfig->DeSerializePushBack();
	}
	return result;
}

/////////////////////////////////////////////////////////////////////////////////

BottomDetectionAlgorithmValue::BottomDetectionAlgorithmValue(string name, BottomDetectionAlgorithmType type)
	: BottomDetectionAlgorithm(name, type)
{
	m_Value = 0.0;
}

BottomDetectionAlgorithmValue::~BottomDetectionAlgorithmValue(void)
{
}

bool BottomDetectionAlgorithmValue::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, m_AlgorithmName.c_str());

	movConfig->SerializeData<double>(m_Value, eDouble, "Value");

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	return true;
}

bool BottomDetectionAlgorithmValue::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = true;
	// debut de la d�s�rialisation du module
	if (result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, m_AlgorithmName.c_str()))
	{
		movConfig->DeSerializeData<double>(&m_Value, eDouble, "Value");

		movConfig->DeSerializePushBack();
	}
	return result;
}
