#include "BottomDetection/ContourLineDef.h"

using namespace BaseKernel;

ContourLineDef::ContourLineDef(void)
{
	m_iLevel = -9999;
	m_bCompute = true;
	m_bDisplay = true;
}

ContourLineDef::~ContourLineDef(void)
{
}

bool ContourLineDef::Serialize(BaseKernel::MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "ContourLineDef");

	// s�rialisation des donn�es membres
	movConfig->SerializeData<int>(m_iLevel, eInt, "Level");
	movConfig->SerializeData<bool>(m_bCompute, eBool, "Compute");
	movConfig->SerializeData<bool>(m_bDisplay, eBool, "Display");

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();

	return true;
}

bool ContourLineDef::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = true;
	// debut de la d�s�rialisation de l'objet
	result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "ContourLineDef");

	// d�s�rialisation des donn�es membres
	result = result && movConfig->DeSerializeData<int>(&m_iLevel, eInt, "Level");
	result = result && movConfig->DeSerializeData<bool>(&m_bCompute, eBool, "Compute");
	result = result && movConfig->DeSerializeData<bool>(&m_bDisplay, eBool, "Display");

	// fin de la d�s�rialisation de l'objet
	movConfig->DeSerializePushBack();

	return result;
}

// *********************************************************************
// Accesseurs
// *********************************************************************
void ContourLineDef::SetLevel(int iLevel)
{
	m_iLevel = iLevel;
}

int ContourLineDef::GetLevel()
{
	return m_iLevel;
}

void ContourLineDef::SetCompute(bool bCompute)
{
	m_bCompute = bCompute;
}

bool ContourLineDef::GetCompute()
{
	return m_bCompute;
}

void ContourLineDef::SetDisplay(bool bDisplay)
{
	m_bDisplay = bDisplay;
}

bool ContourLineDef::GetDisplay()
{
	return m_bDisplay;
}
