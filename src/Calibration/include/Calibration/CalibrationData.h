#pragma once
class CalibrationData
{
public:
	CalibrationData() : isInitialized(false) {}
	CalibrationData(double freq, double gain, double saCorr = 0, double beamAlong = 0, double beamAthwart = 0, double offsetAlong = 0, double offsetAthwart = 0)
		: Freq(freq)
		, Gain(gain)
		, SaCorr(saCorr)
		, BeamAlong(beamAlong)
		, BeamAthwart(beamAthwart)
		, OffsetAlong(offsetAlong)
		, OffsetAthwart(offsetAthwart)
		, isInitialized(true)
	{}

	double Freq, Gain, SaCorr, BeamAlong, BeamAthwart, OffsetAlong, OffsetAthwart;
	bool isInitialized;
};
