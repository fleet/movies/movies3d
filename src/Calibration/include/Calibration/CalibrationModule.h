#pragma once

#include "Calibration.h"
#include "CalibrationParameter.h"
#include "M3DKernel/module/ProcessModule.h"

class CALIBRATION_API CalibrationModule : public ProcessModule
{
public:
	MovCreateMacro(CalibrationModule);
	virtual ~CalibrationModule();

	void PingFanAdded(PingFan *);
	void SounderChanged(std::uint32_t);
	void StreamClosed(const char *streamName);
	void StreamOpened(const char *streamName);

	BaseKernel::ParameterModule& GetParameterModule() { return m_parameter;	}
	CalibrationParameter& GetCalibrationParameter() { return m_parameter; }

	/// Serialize the module configuration
	virtual bool Serialize(BaseKernel::MovConfig * movConfig);
	
	/// Deserialize the module configuration and activate/deactivation the module according to the configuration
	virtual bool DeSerialize(BaseKernel::MovConfig * movConfig);

	/**
	* \brief Retrieves the Sv values and add a gain interpolated from the specified calibration data.
	* \details If no calibration data is provided, the gain is taken from the channel.
	* The values are stored by range x frequency, i.e. sv(rangeIndex)
	*/
	std::vector<std::vector<double>> getSvValuesWithGain(const SpectralAnalysisDataObject * spectralAnalysisDataObject, SoftChannel* channel) const;

	/**
	* \brief Interpolates the gain from the calibration data.
	* \details If the provided frequency is outside the bounds of the calibration data, the returned value is "nan".
	*/
	double getInterpolatedCalibrationGain(const std::vector<CalibrationData>& calibration, SoftChannel* channel, double frequency) const;

	/**
	* \brief Computed interpolated gain the gain for a given channel for a list of frequencies
	* \details If the provided frequency is outside the bounds of the calibration data, the returned value is "nan".
	*/
	std::vector<double> getCalibrationGain(SoftChannel* channel, std::vector<double> & frequencies) const;
	
	double getSvCorr(const SoftChannel* channel, const std::vector<CalibrationData>& calibrationData, bool& found) const;
	double getTSCorr(const SoftChannel* channel, const std::vector<CalibrationData>& calibrationData, bool& found) const;

protected:
	CalibrationModule();
	
	CalibrationParameter m_parameter;
};
