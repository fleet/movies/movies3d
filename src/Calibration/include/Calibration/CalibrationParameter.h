#pragma once

#include "Calibration/Calibration.h"
#include "Calibration/CalibrationData.h"
#include "M3DKernel/parameter/ParameterModule.h"

class CALIBRATION_API CalibrationParameter : public BaseKernel::ParameterModule
{
public:
	CalibrationParameter();
	virtual ~CalibrationParameter(void);

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	bool isEnabled() const;
	void setEnabled(bool enabled);

	static bool parseCalibrationFile(const std::string& filepath, std::map<std::string, std::vector<CalibrationData>>& values, std::map<std::string, std::vector<CalibrationData>>& valuesCW);
	bool setCalibrationFile(const std::string& filepath) { m_CalibrationFile = filepath; return refreshCalibrationValues(); }
	const std::string& getCalibrationFile() { return m_CalibrationFile; }
	const std::map<std::string, std::vector<CalibrationData> >& getCalibrationValuesFM() const { return m_CalibrationValuesFM; }
	const std::map<std::string, std::vector<CalibrationData> >& getCalibrationValuesCW() const { return m_CalibrationValuesCW; }
	std::vector<CalibrationData> getCalibrationDataFM(const std::string& transducerName) const;
	std::vector<CalibrationData> getCalibrationDataCW(const std::string& transducerName) const;
	
private:
	bool m_enabled;
	std::string			m_CalibrationFile;
	std::map<std::string, std::vector<CalibrationData>> m_CalibrationValuesFM; 
	std::map<std::string, std::vector<CalibrationData>> m_CalibrationValuesCW;

	bool refreshCalibrationValues();
};
