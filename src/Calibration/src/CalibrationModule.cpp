#include "Calibration/CalibrationModule.h"
#include "Calibration/Calibration.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/utils/multithread/Synchronized.h"

#include "BaseMathLib/SignalUtils.h"

CalibrationModule::CalibrationModule()
	: ProcessModule("Calibration Module")
{
}

CalibrationModule::~CalibrationModule()
{
}

bool CalibrationModule::Serialize(BaseKernel::MovConfig * movConfig)
{
	m_parameter.setEnabled(this->getEnable());
	return m_parameter.Serialize(movConfig);
}

bool CalibrationModule::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool ok = m_parameter.DeSerialize(movConfig);
	if (ok)
	{
		this->setEnable(m_parameter.isEnabled());
	}
	return ok;
}

void CalibrationModule::PingFanAdded(PingFan *p)
{
	if (!getEnable())
		return;

	const int nbTransducers = p->GetMemorySetRef()->GetMemoryStructCount();
	for (int iTrans = 0; iTrans < nbTransducers; ++iTrans)
	{
		Transducer * pTrans = p->getSounderRef()->GetTransducer(iTrans);
		const unsigned int nombreCh = pTrans->m_numberOfSoftChannel;
		for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
		{
			auto channel = pTrans->getSoftChannelPolarX(softChan);
			
			const auto calibrationData = m_parameter.getCalibrationDataCW(channel->m_transNameShort);
			bool found = false;
			const double svCorr = getSvCorr(channel, calibrationData, found) * 100.0;
			const double tsCorr = getTSCorr(channel, calibrationData, found);
			if (found && pTrans->m_pulseShape != 2)
			{
				MemoryStruct *pMemStruct = p->GetMemorySetRef()->GetMemoryStruct(iTrans);
				MemoryObjectDataFmt * pData = pMemStruct->GetDataFmt();
				DataFmt * pDataFmt = pData->GetPointerToVoxel(BaseMathLib::Vector2I(softChan, 0));
				const int sizeY = pData->getSize().y;
				for (int y = 0; y < sizeY; ++y)
				{
					//Sv corrrection
					short svValue = *pDataFmt;
					if (svValue >= UNKNOWN_DB && svValue <= std::numeric_limits<short>::max())  // sv valide
					{
						double sv = svValue;
						sv -= svCorr;
						if (sv >= UNKNOWN_DB && sv <= std::numeric_limits<short>::max())  // sv corrigé valide
						{
							(*pDataFmt) = sv;
						}
						else
						{
							(*pDataFmt) = UNKNOWN_DB;
						}
					}
					++pDataFmt;
				}

				//TS correction pour les d�tections Simrad du fichier HAC
				M3DKernel *pKernel = M3DKernel::GetInstance();
				PingFanSingleTarget *pSingle = (PingFanSingleTarget *)pKernel->getObjectMgr()->GetPingFanContainer().GetSingleTargetContainer()->GetObjectWithDate(p->m_ObjectTime);
				if (pSingle)
				{
					for (unsigned int i = 0; i < pSingle->GetNumberOfTarget(); i++)
					{
						SingleTargetDataObject *pSingleTargetData = pSingle->GetTarget(i);
						SplitBeamPair ref;
						bool found = pKernel->getObjectMgr()->GetChannelParentSplitBeam(pSingleTargetData->m_parentSTId, ref);
						if (found)
						{
							for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataCount(); numTarget++)
							{
								if (numTarget < pSingleTargetData->GetSingleTargetDataCWCount())
								{
									SingleTargetData * singleTarget = &(pSingleTargetData->GetSingleTargetData(numTarget));
									SingleTargetDataCW * cwTS = static_cast<SingleTargetDataCW*>(singleTarget);
									cwTS->m_unCompensatedTS -= tsCorr;
									cwTS->m_compensatedTS -= tsCorr;
								}
							}
						}
					}
				}
			}
		}
	}
}

void CalibrationModule::SounderChanged(std::uint32_t sounderId)
{
	if (!getEnable())
		return;
	
	M3DKernel *pKern = M3DKernel::GetInstance();
	CurrentSounderDefinition &lsouderMgr = pKern->getObjectMgr()->GetPingFanContainer().m_sounderDefinition;
	Sounder *pSounder = lsouderMgr.GetSounderWithId(sounderId);
	const int nbTransducers = pSounder->GetTransducerCount();
	for (int iTrans = 0; iTrans < nbTransducers; ++iTrans)
	{
		Transducer * pTrans = pSounder->GetTransducer(iTrans);
		const unsigned int nombreCh = pTrans->m_numberOfSoftChannel;
		for (unsigned int iSoftChan = 0; iSoftChan < nombreCh; ++iSoftChan)
		{			
			auto pSoftChan = pTrans->getSoftChannelPolarX(iSoftChan);
			const auto calibrationDatas = m_parameter.getCalibrationDataCW(pSoftChan->m_transNameShort);
			
			// th�oriquement une seule valeur de calibration pour une fr�quence donn�e
			for (const auto & calib : calibrationDatas)
			{
				if (calib.Freq == pSoftChan->m_acousticFrequency)
				{
					pSoftChan->m_calibBeam3dBWidthAlongRad = DEG_TO_RAD(calib.BeamAlong);
					pSoftChan->m_calibBeam3dBWidthAthwartRad = DEG_TO_RAD(calib.BeamAthwart);
					pSoftChan->m_calibMainBeamAlongSteeringAngleRad = DEG_TO_RAD(calib.OffsetAlong);
					pSoftChan->m_calibMainBeamAthwartSteeringAngleRad = DEG_TO_RAD(calib.OffsetAthwart);
					pSoftChan->m_calibBeamGain = calib.Gain;
					pSoftChan->m_calibBeamSACorrection = calib.SaCorr;
					break;
				}
			}			
		}
	}
}

void CalibrationModule::StreamClosed(const char *streamName)
{

}

void CalibrationModule::StreamOpened(const char *streamName)
{
}

std::vector<std::vector<double>> CalibrationModule::getSvValuesWithGain(const SpectralAnalysisDataObject * spectralAnalysisDataObject, SoftChannel * channel) const
{
	assert(spectralAnalysisDataObject);

	auto svValuesWithgain = spectralAnalysisDataObject->svValues;
	if (svValuesWithgain.empty())
		return svValuesWithgain;
	
	const auto nfreq = spectralAnalysisDataObject->numberOfFrequencies();
	std::vector<double> frequencies(nfreq);
	for (auto freqIndex = 0; freqIndex < nfreq; ++freqIndex)
	{
		frequencies[freqIndex] = spectralAnalysisDataObject->frequencyForIndex(freqIndex);
	}

	std::vector<double> gainValues = getCalibrationGain(channel, frequencies);

	for (auto itRange = svValuesWithgain.begin(); itRange != svValuesWithgain.end(); ++itRange)
	{
		int freqIndex = 0;
		for (auto itFreq = itRange->begin(); itFreq != itRange->end(); ++itFreq)
		{
			auto gain = gainValues[freqIndex];

			*itFreq -= 2.0 * gain;
			++freqIndex;
		}
	}

	return svValuesWithgain;
}

double CalibrationModule::getInterpolatedCalibrationGain(const std::vector<CalibrationData>& calibration, SoftChannel * channel, double frequency) const
{
	if (calibration.empty() || frequency < calibration.front().Freq)
		return channel->m_beamGain + 20 * log10(frequency / channel->m_acousticFrequency);

	double prefCalibFreq = 0.0;
	double prevCalibGain = 0.0;

	for (auto calibrationValue : calibration)
	{
		double calibFreq = calibrationValue.Freq;
		double calibGain = calibrationValue.Gain;

		if (calibFreq >= frequency)
		{
			//Interpolatation
			return prevCalibGain + (frequency - prefCalibFreq) * ((calibGain - prevCalibGain) / (calibFreq - prefCalibFreq));
		}

		prevCalibGain = calibrationValue.Gain;
		prefCalibFreq = calibrationValue.Freq;
	}

	return nan("");
}

std::vector<double> CalibrationModule::getCalibrationGain(SoftChannel * channel, std::vector<double> & frequencies) const
{
	auto calibration = m_parameter.getCalibrationDataFM(channel->m_transNameShort);

	auto nfreq = frequencies.size();
	std::vector<double> gainValues(nfreq);

	for (auto freqIndex = 0; freqIndex < nfreq; ++freqIndex)
	{
		auto f = frequencies[freqIndex];
		auto gain = getInterpolatedCalibrationGain(calibration, channel, f);
		gainValues[freqIndex] = gain;
	}

	if (!calibration.empty())
	{
		// on lisse la courbe de gains pour calculer du Sv, on lisse sur l'�quivalent de 10 points de fr�quence de l'�talonnage
		auto firstFreq = frequencies[0];
		auto lastFreq = frequencies[nfreq - 1];
		auto smoothingSpan = (10.0 * (calibration.back().Freq - calibration.front().Freq) / calibration.size()) * (nfreq / (lastFreq - firstFreq));

		smoothed(gainValues, smoothingSpan);
	}

	return gainValues;
}


double CalibrationModule::getSvCorr(const SoftChannel* channel, const std::vector<CalibrationData>& calibrationData, bool& found) const
{
	//const auto calibrationData = m_parameter.getCalibrationDataCW(channel->m_transNameShort);
	const auto freq = channel->m_acousticFrequency;

	found = false;
	double svCorr = 0.0;

	// th�oriquement une seule valeur de calibration pour une fr�quence donn�e
	for (const auto & calib : calibrationData)
	{
		if (calib.Freq == freq)
		{
			found = true;
			svCorr = 2.0 *((calib.Gain - channel->m_beamGain) + (calib.SaCorr - channel->m_beamSACorrection));
			break;
		}
	}

	// Correction du svCorr en dB
	return svCorr;
}

double CalibrationModule::getTSCorr(const SoftChannel* channel, const std::vector<CalibrationData>& calibrationData, bool& found) const
{
	//const auto calibrationData = m_parameter.getCalibrationDataCW(channel->m_transNameShort);
	const auto freq = channel->m_acousticFrequency;

	found = false;
	double tsCorr = 0.0;

	// th�oriquement une seule valeur de calibration pour une fr�quence donn�e
	for (const auto & calib : calibrationData)
	{
		if (calib.Freq == freq)
		{
			found = true;
			tsCorr = 2.0 *(calib.Gain - channel->m_beamGain);
			break;
		}
	}

	// Correction du svCorr en dB
	return tsCorr;
}
