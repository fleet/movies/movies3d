#include "Calibration/CalibrationParameter.h"
#include "M3DKernel/config/MovConfig.h"

#include "M3DKernel/parameter/XmlChUtils.h"

#include <memory>

#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>

XERCES_CPP_NAMESPACE_USE

using namespace BaseKernel;

class CalibrationFileSAX2Handler : public xercesc::DefaultHandler
{
public:
	void startElement(
		const   XMLCh* const    uri,
		const   XMLCh* const    localname,
		const   XMLCh* const    qname,
		const   xercesc::Attributes&     attrs
	);

	void fatalError(const xercesc::SAXParseException&) {}

	void getCalibrationValuesFM(std::map<std::string, std::vector<CalibrationData>>& values) { values = m_CalibrationValuesFM; };
	void getCalibrationValuesCW(std::map<std::string, std::vector<CalibrationData>>& values) { values = m_CalibrationValuesCW; };

private:
	std::string m_currentTransducer;
	std::map<std::string, std::vector<CalibrationData>> m_CalibrationValuesFM;
	std::map<std::string, std::vector<CalibrationData>> m_CalibrationValuesCW;
};

void CalibrationFileSAX2Handler::startElement(
	const   XMLCh* const    uri,
	const   XMLCh* const    localname,
	const   XMLCh* const    qname,
	const   Attributes&     attrs
)
{
	if (std::u16string(qname) == u"Transducer")
	{
		auto nameAttributeIndex = attrs.getIndex(u"TransducerName");
		if (nameAttributeIndex >= 0)
			m_currentTransducer = XMLString::transcode(attrs.getValue(nameAttributeIndex));
	}
	else if (std::u16string(qname) == u"FrequencyPar" || std::u16string(qname) == u"FrequencyParCW")
	{
		double freq = 0;
		auto freqAttributeIndex = attrs.getIndex(u"Frequency");
		if (freqAttributeIndex >= 0)
			freq = XmlChUtils::XMLChToT<double>(attrs.getValue(freqAttributeIndex));

		double gain = 0;
		auto gainAttributeIndex = attrs.getIndex(u"Gain");
		if (gainAttributeIndex >= 0)
			gain = XmlChUtils::XMLChToT<double>(attrs.getValue(gainAttributeIndex));

		double saCorr = 0;
		auto saCorrAttributeIndex = attrs.getIndex(u"SaCorrection");
		if (saCorrAttributeIndex >= 0)
			saCorr = XmlChUtils::XMLChToT<double>(attrs.getValue(saCorrAttributeIndex));

		double beamAlong = 0;
		auto beamAlongAttributeIndex = attrs.getIndex(u"BeamWidthAlongship");
		if (beamAlongAttributeIndex >= 0)
			beamAlong = XmlChUtils::XMLChToT<double>(attrs.getValue(beamAlongAttributeIndex));

		double beamAthwart = 0;
		auto beamAthwartAttributeIndex = attrs.getIndex(u"BeamWidthAthwartship");
		if (beamAthwartAttributeIndex >= 0)
			beamAthwart = XmlChUtils::XMLChToT<double>(attrs.getValue(beamAthwartAttributeIndex));

		double angleAlong = 0;
		auto angleAlongAttributeIndex = attrs.getIndex(u"AngleOffsetAlongship");
		if (angleAlongAttributeIndex >= 0)
			angleAlong = XmlChUtils::XMLChToT<double>(attrs.getValue(angleAlongAttributeIndex));

		double angleAthwart = 0;
		auto angleAthwartAttributeIndex = attrs.getIndex(u"AngleOffsetAthwartship");
		if (angleAthwartAttributeIndex >= 0)
			angleAthwart = XmlChUtils::XMLChToT<double>(attrs.getValue(angleAthwartAttributeIndex));

		if (std::u16string(qname) == u"FrequencyPar")
			m_CalibrationValuesFM[m_currentTransducer].push_back(CalibrationData(freq, gain, saCorr, beamAlong, beamAthwart, angleAlong, angleAthwart));
		else
			m_CalibrationValuesCW[m_currentTransducer].push_back(CalibrationData(freq, gain, saCorr, beamAlong, beamAthwart, angleAlong, angleAthwart));
	}
}

CalibrationParameter::CalibrationParameter() 
	: BaseKernel::ParameterModule("CalibrationParameter")
	, m_enabled(true)
{
}

CalibrationParameter::~CalibrationParameter(void)
{
}

bool CalibrationParameter::Serialize(BaseKernel::MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	movConfig->SerializeData<bool>(m_enabled, eBool, "Enabled");
	movConfig->SerializeData<std::string>(m_CalibrationFile, eString, "CalibrationFile");

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	return true;
}

bool CalibrationParameter::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);

	// debut de la d�s�rialisation du module
	if (result)
	{
		result &= movConfig->DeSerializeData<bool>(&m_enabled, eBool, "Enabled");
		result &= movConfig->DeSerializeData<std::string>(&m_CalibrationFile, eString, "CalibrationFile");
		refreshCalibrationValues();
	}

	// fin de la d�s�rialisation du module
	movConfig->DeSerializePushBack();

	return result;
}


bool CalibrationParameter::isEnabled() const 
{
	return m_enabled;
}

void CalibrationParameter::setEnabled(bool enabled)
{
	m_enabled = enabled;
}

bool CalibrationParameter::parseCalibrationFile(const std::string& filepath,
	std::map<std::string, std::vector<CalibrationData>>& values,
	std::map<std::string, std::vector<CalibrationData>>& valuesCW)
{
	XMLPlatformUtils::Initialize();

	auto parser = std::unique_ptr<SAX2XMLReader>(XMLReaderFactory::createXMLReader());
	//parser->setFeature(XMLUni::fgSAX2CoreValidation, true);
	//parser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);   // optional

	auto defaultHandler = std::unique_ptr<CalibrationFileSAX2Handler>(new CalibrationFileSAX2Handler());
	parser->setContentHandler(defaultHandler.get());
	parser->setErrorHandler(defaultHandler.get());

	bool ok = true;
	try {
		parser->parse(filepath.c_str());
	}
	catch (...) {
		ok = false;
	}

	defaultHandler->getCalibrationValuesFM(values);
	defaultHandler->getCalibrationValuesCW(valuesCW);

	return ok;
}

bool CalibrationParameter::refreshCalibrationValues()
{
	return parseCalibrationFile(m_CalibrationFile, m_CalibrationValuesFM, m_CalibrationValuesCW);
}

std::vector<CalibrationData> CalibrationParameter::getCalibrationDataFM(const std::string& transducerName) const
{
	auto it = m_CalibrationValuesFM.find(transducerName);
	if (it != m_CalibrationValuesFM.cend())
		return it->second;

	return std::vector<CalibrationData>();
}

std::vector<CalibrationData> CalibrationParameter::getCalibrationDataCW(const std::string & transducerName) const
{
	auto it = m_CalibrationValuesCW.find(transducerName);
	if (it != m_CalibrationValuesCW.cend())
		return it->second;

	return std::vector<CalibrationData>();
}


