#pragma once
#include "Compensation/Compensation.h"
#include "M3DKernel/module/ProcessModule.h"
#include "CompensationParameter.h"
#include "CompensationTransducerPosition.h"
#include "OverLapComputer.h"
#include "rf/RFComputer.h"

class CalibrationModule;

class COMPENSATION_API CompensationModule : public ProcessModule, public PingFanConsumer
{
public:
	MovCreateMacro(CompensationModule);
	virtual ~CompensationModule();

	void setCalibrationModule(CalibrationModule * calibrationModule);
	inline CalibrationModule * getCalibrationModule() { return m_pCalibrationModule; }
	
	void PingFanAdded(PingFan *);
	void SounderChanged(std::uint32_t);
	void StreamClosed(const char *streamName);
	void StreamOpened(const char *streamName);

	CompensationParameter&	GetCompensationParameter() { return m_parameter; }
	CompensationTransducerPosition& GetCompensationTransducerPosition() { return m_transPosition; }

	void RunOverLapFilter(PROGRESS_CALLBACK progress, CANCEL_CALLBACK cancel);
	void RunFrequencyResponse(bool computeOverlap, PROGRESS_CALLBACK progressCB, CANCEL_CALLBACK cancelCB);

	// IPSIS - OTK - ajout accesseur sur param�tres pour la gestion des sorties
	BaseKernel::ParameterModule& GetParameterModule() { return m_parameter; };

	//IPSIS - FRE - ajout acces rf computer pour acc�der aux r�sultats
	RFComputer*			GetComputer(bool multiBeam) { return multiBeam ? &m_raComputer : &m_rfComputer; };

	// OTK - 16/10/2009 - pour utilisation en tant que traitement asynchrone du module
	virtual void WaitBeforeDeletingPing(std::uint64_t pingId);

protected:
	CompensationModule(void);

	void UpdateParameters();
	//Update compute ping window from shoal
	void UpdateOverlapParametersFromShoal(std::int32_t iShoal);

	CompensationParameter	          m_parameter;
	CompensationTransducerPosition  m_transPosition;

	OverLapComputer		              m_overLapComputer;
	RFComputer			                m_rfComputer;
	RFComputer			                m_raComputer;

	CalibrationModule * m_pCalibrationModule;
};
