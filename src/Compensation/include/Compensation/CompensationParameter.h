#pragma once
#include "Compensation/Compensation.h"
#include "M3DKernel/parameter/ParameterModule.h"
#include "TransducerPositionSet.h"
#include "OverLapParameter.h"
#include "Compensation/rf/RFParameter.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"

class COMPENSATION_API CompensationParameter : public BaseKernel::ParameterModule
{
public:
	CompensationParameter();
	virtual ~CompensationParameter(void);

	// IPSIS - OTK - ajout MovConfig
	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	TransducerPositionSet	m_transducerPositionSet;
	OverLapParameter		m_overlapParameter;
	RFParameter				m_rfParameter;

	CRecursiveMutex m_rfLock;
};
