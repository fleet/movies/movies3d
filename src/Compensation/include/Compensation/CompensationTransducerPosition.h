#pragma once
class PingFan;
class Transducer;
class TransducerPositionCompensation;
#include "Compensation/Compensation.h"
#include "CompensationParameter.h"

class COMPENSATION_API CompensationTransducerPosition
{
public:
	CompensationTransducerPosition(void);
	virtual ~CompensationTransducerPosition(void);

	void PingFanAdded(PingFan *, CompensationParameter&) {};
	void SounderChanged(std::uint32_t, CompensationParameter&);
	void StreamClosed(const char *streamName, CompensationParameter&) {};
	void StreamOpened(const char *streamName, CompensationParameter&) {};



	void ApplyParameter(CompensationParameter&);

private:
	void SaveHACFileValues(Transducer *pTrans, TransducerPositionCompensation *pCompens);
	void ApplyCompensation(Transducer *pTrans, TransducerPositionCompensation *pCompens);
	void RecomputeAllMatrix();

};
