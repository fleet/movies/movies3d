#pragma once
#include "Compensation/Compensation.h"

class COMPENSATION_API ElementSpace
{
public:
	ElementSpace(void);
	virtual ~ElementSpace(void);
	double coordX;
	double coordY;
	double coordZ;

	friend bool operator<(const ElementSpace &elem1, const ElementSpace &elem2)
	{
		if (elem1.coordX != elem2.coordX)
			return elem1.coordX < elem2.coordX;
		else if (elem1.coordY != elem2.coordY)
			return elem1.coordY < elem2.coordY;
		else return elem1.coordZ < elem2.coordZ;
	};


};

