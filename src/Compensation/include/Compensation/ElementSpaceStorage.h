#pragma once
#include "Compensation/Compensation.h"
#include <vector>
#include <set>
#include "BaseMathLib/Vector3.h"
#include "ElementSpace.h"

class COMPENSATION_API ElementSpaceStorage
{
public:
	ElementSpaceStorage(double gridDefinition);
	virtual ~ElementSpaceStorage(void);

	/// add one element
	inline void Add(ElementSpace &refVol)
	{
		m_myVolume.push_back(refVol);
		//		m_myVolume.insert(refVol);
	}
	/// empty the lists
	void Clear();

	/// After adding data perform clean up and other things
	void Finalize();

	/// return the number of element
	unsigned int Count();

	/// return the number of element in common
//	unsigned int EvaluateOverLap(ElementSpaceStorage &refCompare ); 
	double GetGridDefinition() { return m_gridDefinition; }

	BaseMathLib::Vector3D GetElementCoord(unsigned int index);

protected:
	double m_gridDefinition;
	//	std::vector<ElementSpace> m_myVolume;
	std::vector<ElementSpace> m_myVolume;
};