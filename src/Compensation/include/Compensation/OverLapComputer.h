#pragma once

#include <vector>
#include "Compensation/Compensation.h"
#include "OverLapParameter.h"
#include "BaseMathLib/Vector3.h"
#include "BaseMathLib/Box.h"
#include "ElementSpaceStorage.h"
#include "ReferenceSpaceStorage.h"
#include "M3DKernel/datascheme/PingFanIterator.h"
#include "OverLapStatitic.h"

#include "M3DKernel/algorithm/base/EchoAlgorithm.h"
#include "M3DKernel/utils/multithread/stack/ActionStack.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"

class PingFan;
class OverLapStatitic;

#ifndef WIN32
#define __stdcall
#else
// Exclure les en-t�tes Windows rarement utilis�s
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#endif


// OTK - 15/10/2009 - définition du callback de progression pour l'utilisation
// de module de traitement de façon asynchrone depuis l'IHM, par exemple.
typedef void(__stdcall *PROGRESS_CALLBACK)(const char*, int);
// OTK - 15/10/2009 - définition du callback d'annulation du traitement asynchrone
typedef bool(__stdcall *CANCEL_CALLBACK)(void);

class COMPENSATION_API OverLapComputer
{

public:
	OverLapComputer(void);
	~OverLapComputer(void);

	void PingFanAdded(PingFan *, OverLapParameter&, CANCEL_CALLBACK cancelCB);
	void SounderChanged(std::uint32_t, OverLapParameter &);
	void StreamClosed(const char *streamName, OverLapParameter&);
	void StreamOpened(const char *streamName, OverLapParameter&);

	void EvalutateRecover(OverLapParameter&, PROGRESS_CALLBACK progress, CANCEL_CALLBACK cancel);

	OverLapParameter& GetParameters() { return m_localParameter; };

	// add a ping fan as a filtered one
	void AddFilterData(PingFan *p, OverLapParameter& param, CANCEL_CALLBACK cancelCB);

	//reset multithread data
	void ResetActionStacks();

protected:

	//init multithread managment
	void InitActionStacks();

private:
	// evaluate if enable status or other parameter changed return true if status changed
	bool EvaluateParameter(OverLapParameter&, CANCEL_CALLBACK cancelCB);

	// evaluate the compute window
	void EvalutateWindow(OverLapParameter& param);

	// Compute the echo range on the channel matching the depth range
	void ComputeEchoRange(double minAngle, double maxAngle,
		double channelDepthOffset, double beamsSamplesSpacing,
		double minDepth, double maxDepth,
		int& minEcho, int& maxEcho);

	// add a ping fan as a reference 
	void AddReference(PingFan *p, OverLapParameter& param);
	// add a ping fan as a filtered one
	void AddFilterData(PingFan *p, OverLapParameter& param, OverLapStatitic &stat, CANCEL_CALLBACK cancelCB);

	// add an echo to the given space storage
	void AddEcho(ElementSpaceStorage &aStorage, double depthPolar, BaseMathLib::Vector3D aWorldCoord, BaseMathLib::Vector3D aWorldCoordTranducer, BaseMathLib::Box &aaBoudingBox);

	void ClearMemory();
	ReferenceSpaceStorage m_StoragerRef;
	OverLapParameter	m_localParameter;

	PingFanIterator m_pingFanIterator;

	//list of ActionStack to parallelize compute
	std::vector<ActionStack*> m_actionStackList;

	OverLapStatitic m_totalStat;

	CRecursiveMutex m_Lock;
};
