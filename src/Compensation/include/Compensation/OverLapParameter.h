#pragma once
#include "Compensation/Compensation.h"
#include "M3DKernel/parameter/ParameterModule.h"
#include "M3DKernel/parameter/ParameterObject.h"
#include "M3DKernel/DefConstants.h"

class COMPENSATION_API OverLapParameter : public BaseKernel::ParameterObject

{
public:
	OverLapParameter(void);
	// IPSIS - OTK - Ajout MovConfig
	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	virtual ~OverLapParameter(void);

	bool IsEnable() { return m_bIsEnable; };
	void SetEnable(bool a) { m_bIsEnable = a; };

	/// this will create a regular grid depending on depth (something like regularSpacing*(Z*sin(alpha))) 
	double	GetGridSpaceXY(double depthZ);
	void	SetGridSpaceXYAngleFactor(double alpha) { m_spaceXYAngleFactor = alpha; }
	double	GetGridSpaceXYAngleFactor() { return m_spaceXYAngleFactor; }



	double GetGridSpaceZ() { return m_gridSpaceZ; }
	void   SetGridSpaceZ(double a) { m_gridSpaceZ = a; }

	//nb core to NOT use
	int GetNbReservedCores() const { return m_nbReservedCores; };
	void SetNbReservedCores(int val) { m_nbReservedCores = val; };

	DataFmt GetReferenceThreshold() { return m_moduleReferenceThreshold; }
	void	SetReferenceThreshold(DataFmt a) { m_moduleReferenceThreshold = a; }
	DataFmt GetDataThreshold() { return m_moduleDataThreshold; }
	void	SetDataThreshold(DataFmt a) { m_moduleDataThreshold = a; }

	unsigned int GetReferenceWindowsPingCount() { return m_referenceWindowsPingCount; }
	void	SetReferenceWindowsPingCount(unsigned int a) { m_referenceWindowsPingCount = a; }

	bool IsVolumeParameterEqual(const OverLapParameter &refA)
	{
		return
			m_spaceXYAngleFactor == refA.m_spaceXYAngleFactor &&
			m_gridSpaceZ == refA.m_gridSpaceZ &&
			m_bIsEnable == refA.m_bIsEnable  &&
			m_elementaryUnitLenght == refA.m_elementaryUnitLenght &&
			m_moduleReferenceThreshold == refA.m_moduleReferenceThreshold //;
			&& m_moduleDataThreshold == refA.m_moduleDataThreshold //;
			&& m_pingFilteredStart == refA.m_pingFilteredStart
			&& m_pingFilteredStop == refA.m_pingFilteredStop
			&& m_depthMax == refA.m_depthMax
			&& m_depthMin == refA.m_depthMin
			&& m_referenceWindowsPingCount == refA.m_referenceWindowsPingCount;

	}



	//temporal window
	unsigned int m_pingFilteredStart;
	unsigned int m_pingFilteredStop;

	////use of ping ID instead of the ping index 
	//(to be used ONLY when the process is PAUSED)
	bool m_bUsePingId;

	//spatial depth window
	double m_depthMin;
	double m_depthMax;

	//spatial angular window
	double m_angleMin;
	double m_angleMax;

	unsigned int m_referenceWindowsPingCount;

protected:
	void BuildDefaultSettings();

	bool m_bIsEnable;

	// grid sampling definition in meter
	double m_gridSpaceZ;
	double m_spaceXYAngleFactor;

	// lenght in meter of an elementary unit use to recycle grid
	double m_elementaryUnitLenght;

	// we will considerer only data above threshold
	DataFmt		m_moduleReferenceThreshold;
	DataFmt		m_moduleDataThreshold;

	//void ComputeUnitCount();

	//nb core to NOT use
	int m_nbReservedCores;

};
