// -*- C++ -*-
// ****************************************************************************
// Class: OverLapStackAction
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Sept 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "M3DKernel/utils/multithread/stack/StackAction.h"
#include "M3DKernel/algorithm/base/EchoAlgorithm.h"

class ProgressInfo;
class OverLapComputer;
class OverLapParameter;
class PingFan;

// ***************************************************************************
// Declarations
// ***************************************************************************
enum EOverlapAction
{
	E_FILTERDATA_ACTION
};

class OverLapStackAction : public StackAction
{
public:
	// *********************************************************************
	// Constructors / Destructor
	// *********************************************************************
	// d�fault constructor
	OverLapStackAction();

	// Destructor
	virtual ~OverLapStackAction(void);

	// *********************************************************************
	// Accessors
	// *********************************************************************
	// *********************************************************************
	// Methods
	// *********************************************************************

	//process
	virtual void Process();

	static OverLapStackAction* CreateAction_FilterData(OverLapComputer* pOverLapComputer,
		PingFan* pPingFan,
		OverLapParameter* pParam,
		PROGRESS_CALLBACK progressCB,
		int progression,
		CANCEL_CALLBACK cancelCB);

private:

	// *********************************************************************
	// members
	// *********************************************************************

	EOverlapAction actionType;

	PROGRESS_CALLBACK progressCB;
	CANCEL_CALLBACK cancelCB;
	int progression;

	//data for filter data action
	OverLapComputer* pOverLapComputer;
	PingFan* pPingFan;
	OverLapParameter* pParam;
};
