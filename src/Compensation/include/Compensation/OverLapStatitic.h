#pragma once
#include "Compensation/Compensation.h"

#include <cinttypes>

class COMPENSATION_API OverLapStatitic
{
public:
	OverLapStatitic(void);
	~OverLapStatitic(void);

	void ResetStat()
	{
		m_echoProcessedCount = 0;
		m_echoRejectedCount = 0;
	}
	inline OverLapStatitic operator + (const OverLapStatitic& refA) const
	{
		OverLapStatitic a;
		a.m_echoProcessedCount = m_echoProcessedCount + refA.m_echoProcessedCount;
		a.m_echoRejectedCount = m_echoRejectedCount + refA.m_echoRejectedCount;
		return a;
	}

	std::uint64_t m_echoProcessedCount;
	std::uint64_t m_echoRejectedCount;


};
