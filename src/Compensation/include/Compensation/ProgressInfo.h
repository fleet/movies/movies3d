// -*- C++ -*-
// ****************************************************************************
// Class: ProgressInfo
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Aout 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

#include <string>
#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "Compensation/Compensation.h"

class COMPENSATION_API ProgressInfo
{
public:

	ProgressInfo();

	virtual void Reset();

	bool GetActive() const { return m_bActive;};
	const std::string& GetText() const { return m_text;};
	double GetProgress() const { return m_progress;};
	double GetCurrentStepRange() const { return m_currentStepRange;};

	void SetActive(bool val) { m_bActive = val;};
	void SetText(const std::string& val) { m_text = val;};
	void SetProgress(double val) { m_progress = val;};
	void SetCurrentStepRange(double val) { m_currentStepRange = val;};

	void IncrProgress(double val);

private :

	bool		m_bActive;
	std::string m_text;
	double		m_progress;
	double		m_currentStepRange;

    CRecursiveMutex m_lock;
};
