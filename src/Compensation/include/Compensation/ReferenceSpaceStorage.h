#pragma once
#include "Compensation/Compensation.h"


#include "BaseMathLib/Box.h"
#include "BaseMathLib/Vector3.h"
#include "ElementSpaceStorage.h"

#include <vector>
/// this class will store data for reference sounder
class COMPENSATION_API ReferenceEcho
{
public:
	ReferenceEcho(const BaseMathLib::Box &a, const BaseMathLib::Vector3D &center, const BaseMathLib::Vector3D &realCoord) :
		m_boundingBox(a), m_center(center), m_realCoord(realCoord)
	{
	}


	BaseMathLib::Box m_boundingBox;
	BaseMathLib::Vector3D m_center;
	BaseMathLib::Vector3D m_realCoord;
	/// check if the given vector is contained in this echo
	bool IsContained(BaseMathLib::Vector3D position, double toleranceEpaisseur);
};
class COMPENSATION_API ReferenceSpaceStorage
{
public:
	ReferenceSpaceStorage(void);
	~ReferenceSpaceStorage(void);

	unsigned int EvaluateOverLap(ElementSpaceStorage &refElement, double toleranceEpaisseur);
	/// empty the lists
	void Clear();
	unsigned int Count();
	void AddEcho(ReferenceEcho &ref);

private:
	bool CheckPoint(BaseMathLib::Vector3D &a, double toleranceEpaisseur);
	std::vector<ReferenceEcho> m_listEcho;
};