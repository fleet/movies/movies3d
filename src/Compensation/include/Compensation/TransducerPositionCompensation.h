#pragma once
#include "Compensation/Compensation.h"
#include "M3DKernel/parameter/ParameterObject.h"

#include <string>

class COMPENSATION_API TransducerPositionCompensation : public BaseKernel::ParameterObject
{
public:
	TransducerPositionCompensation(const char  *Name);
	virtual ~TransducerPositionCompensation(void);

	// IPSIS - OTK - Ajout MovConfig
	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	/// return true if names are equals
	bool checkTransName(std::string Name);

	std::string m_TransducerName;
	double m_headingRot;
	double m_pitchRot;
	double m_rollRot;
	double m_internalDelay;
	double m_sampleOffset;

	// OTK - FAE045 - choix possible entre les valeurs HAC et les valeurs manuelles
	bool m_bUseHACFileValues;
	double m_HACheadingRot;
	double m_HACpitchRot;
	double m_HACrollRot;
};
