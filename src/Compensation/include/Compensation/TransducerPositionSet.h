#pragma once
#include "Compensation/Compensation.h"
#include "M3DKernel/parameter/ParameterObject.h"
#include "TransducerPositionCompensation.h"

#include <vector>
class COMPENSATION_API TransducerPositionSet : public BaseKernel::ParameterObject
{
public:
	TransducerPositionSet(void);
	virtual ~TransducerPositionSet(void);

	// IPSIS - OTK - Ajout MovConfig
	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	TransducerPositionCompensation *GetTransducerPosition(const char *m_Name);

	unsigned int GetRecordCount();
	TransducerPositionCompensation* GetRecord(unsigned int idx);

private:
	std::vector<TransducerPositionCompensation*> m_RecordSet;

	TransducerPositionCompensation* CreateDefaultTransPosition(const char *m_Name);

	void BuildDefaultSettings();
};

