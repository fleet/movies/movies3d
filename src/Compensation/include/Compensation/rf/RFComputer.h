// -*- C++ -*-
// ****************************************************************************
// Class: RFComputer
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

#include <vector>
#include <map>
#include <string>
#include <set>

#include "Compensation/Compensation.h"
#include "RFParameter.h"
#include "Compensation/rf/RFResult.h"

#include "M3DKernel/algorithm/base/EchoAlgorithm.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/PingFanIterator.h"

#define WINDOW_SHOALID -1

class CalibrationModule;
class ShoalExtractionModule;

//results map
typedef std::map<std::string, std::vector<RFResult*>*> RFResultMap;

struct TSResult{
	std::vector<double> freq;
	std::vector<double> compensatedTS;
	std::vector<double> unCompensatedTS;
};

struct RFResults {
	RFResultMap unfiltered;
	RFResultMap filtered;
	TSResult ts;
} ;

namespace shoalextraction
{
	class ShoalData;
}

class COMPENSATION_API RFComputer
{
public:
	RFComputer(void);
	~RFComputer(void);

	// ****************************************************************************
	// Methods
	// ****************************************************************************
	void Evalutate(RFParameter& param, PROGRESS_CALLBACK progress,
		CANCEL_CALLBACK cancel, PingFanConsumer * pfconsumer);

	//reset the storage
	void Reset();


	// ****************************************************************************
	// Accessors
	// ****************************************************************************

	//results
	const std::map<std::int32_t, RFResults*> & GetResults() const { return m_results; }
	std::map<std::int32_t, RFResults*> & GetResults() { return m_results; }

	bool HasResults();
	bool IsMultiBeam() { return m_localParameter.GetIsMultiBeam(); }

	RFParameter& GetParameters() { return m_localParameter; };

	double GetRefEnergy(std::int32_t shoalId, bool filtered);

	void setCalibrationModule(CalibrationModule * calibrationModule) { m_pCalibrationModule = calibrationModule; }
	inline CalibrationModule * getCalibrationModule() { return m_pCalibrationModule; }

protected:

	// ****************************************************************************
	// Methods
	// ****************************************************************************

	//dump the result map
	void DumpResults(std::ofstream& outStream);

	// ****************************************************************************
	// General methods 

	//retrieve the result for the input (window : key <= 0 or shoal : key > 0)
	RFResults* GetResults(std::int32_t shoalId);

	//retrieve the result for the transducer (window : key <= 0 or shoal : key > 0)
	std::vector<RFResult*> GetResult(std::int32_t shoalId, const std::string & transducerName, bool filtered);

	//Sets the result for the transducer (window : key <= 0 or shoal : key > 0)
	void SetResult(std::int32_t shoalId, const std::string & transducerName, bool filtered, int index, RFResult* result);

	//free the result map
	void FreeResults(RFResults* pResults);

	// returns true if the transducer is multibeam
	bool IsMultiBeam(const std::string& transducerName);

	// ****************************************************************************
	// Compute Energy methods 

	//compute energy values
	void ComputeEnergy(PROGRESS_CALLBACK progress, CANCEL_CALLBACK cancel, PingFanConsumer * pfconsumer);

	//compute the energy in the window
	void EvalutateWindow(const std::set<std::string>& transducerList);
	void EvalutateWindowTransducer(const std::string & transducerName, bool useOverLap);
	void EvalutateWindowComponent(const std::string & componentName, bool useOverLap, int beamCount = 1);

	//compute the energy in the shoal
	void EvalutateShoal(const std::set<std::string>& transducerList,
		shoalextraction::ShoalData* pShoalData, CANCEL_CALLBACK cancel);
	void EvalutateShoalTransducer(const std::string & transducerName,
		shoalextraction::ShoalData* pShoalData,
		bool useOverLap);

	//create a shoal extraction module
	ShoalExtractionModule * CreateShoalExtractionModule();
	//delete a shoal extraction module
	void DeleteShoalExtractionModule(ShoalExtractionModule ** pModule);

	//compute a shoal extraction on input data
	void ComputeShoalExtraction(CANCEL_CALLBACK cancel);

	// Search the matching shoal in the list for thegiven transducer
	shoalextraction::ShoalData* MatchShoals(const shoalextraction::ShoalData* shoalRef,
		const std::vector<shoalextraction::ShoalData*>& shoalList,
		const std::string & transducerName);

	// ****************************************************************************
	// Compute RF Ratio methods 

	//compute energy values
	void ComputeRF();

	// compute the rf results for the given shoalId and the filter status
	//if shoalId <= 0, the data is treated only as a window
	void ComputeRF(std::int32_t shoalId, bool filtered);

	// compute the ts results for the given shoalId
	//if shoalId <= 0, the data is treated only as a window
	void ComputeTS(long shoalId);

private:

	//parameter
	RFParameter	m_localParameter;

	//results for each input (window : key <= 0 or shoal : key > 0)
	std::map<std::int32_t, RFResults*> m_results;

	//module d'extraction de bancs des donn�es 
	ShoalExtractionModule *m_pShoalExtractModule;

	//module d'extraction de bancs des donn�es filtr�es
	ShoalExtractionModule *m_pShoalExtractModuleFiltered;

	//module de calibration
	CalibrationModule * m_pCalibrationModule;

	PingFanIterator m_pingFanIterator;

	bool m_isMultiBeam;
	struct EnergyData
	{
		bool weightedEchoIntegration = false;
		double distanceTravelled = 0;
		double sumHeights = 0;
		double energy = 0;
		double pondEnergy = 0;
		std::uint32_t nbEcho = 0;
		std::uint32_t nbPings = 0;

		double frequency = -1.0;
		double channelAngle = -1.0;
	};

	void updateEnergyDataForPing(PingFan* pingFan, int transducerIndex, SoftChannel* channel, double beamsSamplesSpacing, bool useOverlap, DataFmt threshold, double depthMin, double depthMax, EnergyData& energyData, int iBeam);
	void updateEnergyDataForPingWithSpectralAnalysis(PingFan* pingFan, Transducer* transducer, SoftChannel* channel, DataFmt threshold, double depthMin, double depthMax, bool weightedEchoIntegration, std::vector<EnergyData>& energyData);
	void updateEnergyDataForEcho(DataFmt value, double beamsSamplesSpacing, double pingDistance, EnergyData& data);
	void computeEnergies(const EnergyData& data, float& energySV, float& energySA);
};
