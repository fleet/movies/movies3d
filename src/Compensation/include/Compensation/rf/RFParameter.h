// -*- C++ -*-
// ****************************************************************************
// Class: RFParameter
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

#include <vector>
#include <string>

#include "Compensation/Compensation.h"
#include "M3DKernel/parameter/ParameterModule.h"
#include "M3DKernel/parameter/ParameterObject.h"
#include "M3DKernel/DefConstants.h"

namespace shoalextraction
{
	class ShoalData;
}

class COMPENSATION_API RFParameter : public BaseKernel::ParameterObject

{
public:
	RFParameter(void);
	// IPSIS - OTK - Ajout MovConfig
	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	virtual ~RFParameter(void);


	// ****************************************************************************
	// Accessors
	// ****************************************************************************

	//use of SV instead of SA
	bool GetUseSV() const { return m_bUseSV; };
	void SetUseSV(bool val) { m_bUseSV = val; };

	//compute unfiltered data
	bool GetComputeUnfiltered() const { return m_bComputeUnfiltered; };
	void SetComputeUnfiltered(bool val) { m_bComputeUnfiltered = val; };

	//compute filtered data
	bool GetComputeFiltered() const { return m_bComputeFiltered; };
	void SetComputeFiltered(bool val) { m_bComputeFiltered = val; };

	//precision in begin distance value (in nautic miles)
	float GetPrecisionLog() const { return m_fPrecisionLog; };
	void SetPrecisionLog(float val) { m_fPrecisionLog = val; };

	//precision in depth (m)
	float GetPrecisionDepth() const { return m_fPrecisionDepth; };
	void SetPrecisionDepth(float val) { m_fPrecisionDepth = val; };

	//name of the reference transducer
	const std::string& GetRefTransducer() const { return m_sRefTransducer; };
	void SetRefTransducer(const std::string&  val) { m_sRefTransducer = val; };

	// Overlap threshold
	DataFmt		GetOverlapThreshold() const { return m_overlapThreshold; };
	void SetOverlapThreshold(DataFmt val) { m_overlapThreshold = val; };

	// Data threshold
	DataFmt		GetDataThreshold() const { return m_dataThreshold; };
	void SetDataThreshold(DataFmt val) { m_dataThreshold = val; };

	//temporal window
	unsigned int GetPingFilteredStart() const { return m_uiPingFilteredStart; };
	void SetPingFilteredStart(unsigned int pingId) { m_uiPingFilteredStart = pingId; };
	unsigned int GetPingFilteredStop() const { return m_uiPingFilteredStop; };
	void SetPingFilteredStop(unsigned int pingId) { m_uiPingFilteredStop = pingId; };

	//spatial depth window
	float GetDepthMin() const { return m_fDepthMin; };
	void SetDepthMin(float val) { m_fDepthMin = val; };
    float GetDepthMax() const { return m_fDepthMax; };
    void SetDepthMax(float val) { m_fDepthMax = val; };

	//shoals 
	std::vector<shoalextraction::ShoalData*>& GetShoals() { return m_pShoals; };

	//min nb of echoes in the shoal
	int GetMinEchoesNb() const { return m_iMinEchoesNb; };
	void SetMinEchoesNb(int val) { m_iMinEchoesNb = val; };


	//Vertical Integration Distance
	virtual float GetVerticalIntegrationDistance() const { return m_verticalIntegrationDistance; };
	virtual void SetVerticalIntegrationDistance(float d) { m_verticalIntegrationDistance = d; };

	//Along Integration Distance
	virtual float GetAlongIntegrationDistance() const { return m_alongIntegrationDistance; };
	virtual void SetAlongIntegrationDistance(float d) { m_alongIntegrationDistance = d; };

	//Across Integration Distance
	virtual float GetAcrossIntegrationDistance() const { return m_acrossIntegrationDistance; };
	virtual void SetAcrossIntegrationDistance(float d) { m_acrossIntegrationDistance = d; };

	//Across Integration Distance
	virtual bool GetIsMultiBeam() const { return m_isMultiBeam; };
	virtual void SetIsMultiBeam(bool isMultiBeam) { m_isMultiBeam = isMultiBeam; };

protected:
	virtual void BuildDefaultSettings();

private:

	// ****************************************************************************
	// Member Data
	// ****************************************************************************

	//use of SV instead of SA
	bool m_bUseSV;

	//compute unfiltered data
	bool m_bComputeUnfiltered;
	//compute filtered data
	bool m_bComputeFiltered;

	//name of the reference transducer
	std::string m_sRefTransducer;

	// Overlap threshold
	DataFmt		m_overlapThreshold;
	// Data threshold
	DataFmt		m_dataThreshold;

	//window RF 

	//temporal window
	unsigned int m_uiPingFilteredStart;
	unsigned int m_uiPingFilteredStop;

	//spatial depth window
	float m_fDepthMin;
	float m_fDepthMax;

	//shoal RF

	//shoals
	std::vector<shoalextraction::ShoalData*> m_pShoals;

	//*******************************************
	//shoal matching parameters

	//precision in begin distance value (in nautic miles)
	float m_fPrecisionLog;

	//precision in depth (m)
	float m_fPrecisionDepth;

	//min nb of echoes in the shoal
	int m_iMinEchoesNb;

	//******************************************
	// shoal extraction general parameters

	//Vertical Integration Distance
	float m_verticalIntegrationDistance;
	//Along Integration Distance
	float m_alongIntegrationDistance;
	//Across Integration Distance
	float m_acrossIntegrationDistance;

	//Indicates which sensor has to be used
	bool m_isMultiBeam;
};
