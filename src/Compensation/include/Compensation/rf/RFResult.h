// -*- C++ -*-
// ****************************************************************************
// Class: RFResult
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

#include <string>
#include "Compensation/Compensation.h"

class COMPENSATION_API RFResult
{
public:

	RFResult(void);
	virtual ~RFResult(void);

	// ****************************************************************************
	// Accessors
	// ****************************************************************************

	//energy SA
	float GetEnergySA() const { return m_fEnergySA; };
	void SetEnergySA(float val) { m_fEnergySA = val; };

	//energy SV
	float GetEnergySV() const { return m_fEnergySV; };
	void SetEnergySV(float val) { m_fEnergySV = val; };

	//n log
	float GetNLog() const { return m_fNLog; };
	void SetNLog(float val) { m_fNLog = val; };

	//echoes Nb
	int GetEchoesNb() const { return m_iEchoesNb; };
	void SetEchoesNb(int val) { m_iEchoesNb = val; };

	//rf ratio
	float GetRFRatio() const { return m_fRFRatio; };
	void SetRFRatio(float val) { m_fRFRatio = val; };

	//frequency
	float GetFrequency() const { return m_fFrequency; };
	void SetFrequency(float val) { m_fFrequency = val; };

	//shoal name
	const std::string& GetShoalName() const { return m_shoalName; };
	void SetShoalName(const std::string&  val) { m_shoalName = val; };

	//angle
	int GetAngle() const { return m_angle; };
	void SetAngle(int val) { m_angle = val; };

protected:

private:

	// ****************************************************************************
	// Member Data
	// ****************************************************************************

	//SA energy
	float m_fEnergySA;
	//SV energy
	float m_fEnergySV;

	//n log
	float m_fNLog;

	//echoes Nb
	int m_iEchoesNb;

	//rf ratio
	float m_fRFRatio;

	//frequency 
	float m_fFrequency;

	//shaol name
	std::string m_shoalName;

	//angle
	int m_angle;
};
