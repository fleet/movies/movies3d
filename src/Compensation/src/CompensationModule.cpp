

//shoalextraction

#include "ShoalExtraction/data/shoalstat.h"
#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/pingdata.h"

#include "Compensation/CompensationModule.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/utils/multithread/Synchronized.h"

CompensationModule::CompensationModule()
	: ProcessModule("Compensation Module")
	, m_pCalibrationModule(nullptr)
{
}

CompensationModule::~CompensationModule()
{
}

void CompensationModule::setCalibrationModule(CalibrationModule * calibrationModule)
{
	m_pCalibrationModule = calibrationModule; 
	m_rfComputer.setCalibrationModule(calibrationModule); 
	m_raComputer.setCalibrationModule(calibrationModule);
}

void CompensationModule::RunOverLapFilter(PROGRESS_CALLBACK progress, CANCEL_CALLBACK cancel)
{
	m_parameter.m_overlapParameter.SetEnable(true);
	m_overLapComputer.EvalutateRecover(m_parameter.m_overlapParameter, progress, cancel);
}


//*****************************************************************************
// Name : UpdateParameters
// Description : Update parameters
// Parameters : std::int32_t iShoal
// Return : void
//*****************************************************************************
void CompensationModule::UpdateParameters()
{
	// In order to allow pingfan container to be modified during compute
	// the search of the ping fans is done by pingid
	GetCompensationParameter().m_overlapParameter.m_bUsePingId = true;

	GetCompensationParameter().m_rfParameter.SetComputeFiltered(
		GetCompensationParameter().m_overlapParameter.IsEnable());

	//RF parameters
	GetCompensationParameter().m_rfParameter.SetDataThreshold(
		GetCompensationParameter().m_overlapParameter.GetDataThreshold());

	GetCompensationParameter().m_rfParameter.SetDepthMin(
		GetCompensationParameter().m_overlapParameter.m_depthMin);
	GetCompensationParameter().m_rfParameter.SetDepthMax(
		GetCompensationParameter().m_overlapParameter.m_depthMax);

	GetCompensationParameter().m_rfParameter.SetPingFilteredStart(
		GetCompensationParameter().m_overlapParameter.m_pingFilteredStart);
	GetCompensationParameter().m_rfParameter.SetPingFilteredStop(
		GetCompensationParameter().m_overlapParameter.m_pingFilteredStop);
}

//*****************************************************************************
// Name : UpdateOverlapParametersFromShoal
// Description : Update compute ping window from shoal
// Parameters : std::int32_t iShoal
// Return : void
//*****************************************************************************
void CompensationModule::UpdateOverlapParametersFromShoal(std::int32_t iShoal)
{
	//if the input is a shoal list, retrieve its data
	shoalextraction::ShoalData* pShoalData = m_parameter.m_rfParameter.GetShoals()[iShoal];
	int nbPings = pShoalData->GetPings().size();

	int minPing = pShoalData->GetPings()[0]->GetPingId();
	int maxPing = pShoalData->GetPings()[nbPings - 1]->GetPingId();

	GetCompensationParameter().m_overlapParameter.m_pingFilteredStart = minPing;
	GetCompensationParameter().m_overlapParameter.m_pingFilteredStop = maxPing;

	GetCompensationParameter().m_overlapParameter.m_depthMin = pShoalData->GetShoalStat()->GetMinDepth();
	GetCompensationParameter().m_overlapParameter.m_depthMax = pShoalData->GetShoalStat()->GetMaxDepth();
}

void CompensationModule::RunFrequencyResponse(bool computeOverlap, PROGRESS_CALLBACK progressCB, CANCEL_CALLBACK cancelCB)
{
	progressCB("Computing Overlap...", 0);

	//Update parameters
	UpdateParameters();

	if (computeOverlap)
	{
		//if several shoals are specified, work on several ping windows
		int nbShoals = m_parameter.m_rfParameter.GetShoals().size();

		if (nbShoals > 0)
		{
			//reset compute ping window
			GetCompensationParameter().m_overlapParameter.m_pingFilteredStart = 0;
			GetCompensationParameter().m_overlapParameter.m_pingFilteredStop = 0;

			for (int iShoal = 0; iShoal < nbShoals; iShoal++)
			{
				//Update compute ping window from shoal
				UpdateOverlapParametersFromShoal(iShoal);
				RunOverLapFilter(progressCB, cancelCB);
			}
		}
		else
		{
			RunOverLapFilter(progressCB, cancelCB);
		}
	}

	if (!cancelCB())
	{
		//Compute frequency response
		if (GetCompensationParameter().m_rfParameter.GetIsMultiBeam()) {
			m_raComputer.Reset();
			m_raComputer.Evalutate(GetCompensationParameter().m_rfParameter, progressCB, cancelCB, this);
			progressCB("r(a) Completed", 100);
		}
		else {
			m_rfComputer.Reset();
			m_rfComputer.Evalutate(GetCompensationParameter().m_rfParameter, progressCB, cancelCB, this);
			progressCB("r(f) Completed", 100);
		}
	}

	// pour ce module, lib�ration des pings globalement � la fin du traitement.
	// Pourrait �tre am�lior� si besoin en modifiant la structure de l'algo pour 
	// traiter les pings dans l'ordre (actuellement, on boucle sur les transducteurs puis les pings
	// ce qui emp�che la lib�ration pr�coce des premiers pings).
	CSynchronized sync(m_PingFanConsumerSync);

	SetLastComputedPing(std::numeric_limits<uint64_t>::max());
	m_PingFanConsumerSync.NotifyAll();

	//reset the default ping managment by ping index (for use with external tools)
	GetCompensationParameter().m_overlapParameter.m_bUsePingId = false;
}

// OTK - 16/10/2009 - pour utilisation en tant que traitement asynchrone du module
void CompensationModule::WaitBeforeDeletingPing(std::uint64_t pingId)
{
	// gestion concurrence entre cette m�thode et le thread associ� au traitement asynchrone
	CSynchronized sync(m_PingFanConsumerSync);

    std::uint64_t pingStart = GetLastComputedPing();
    std::uint64_t pingStop = GetCompensationParameter().m_rfParameter.GetPingFilteredStop();

	// early rejection test
	if (pingId < pingStart || pingId > pingStop)
	{
		return;
	}
	else
	{
		// attente du traitement du ping d'ID pingId (on suppose ici que les pings sont trait�s dans l'ordre.
		m_PingFanConsumerSync.Wait();
	}
}

void CompensationModule::PingFanAdded(PingFan *p)
{
	m_transPosition.PingFanAdded(p, m_parameter);
	//	m_overLapComputer.PingFanAdded(p,m_parameter.m_overlapParameter);
}


void CompensationModule::SounderChanged(std::uint32_t sounderId)
{
	m_transPosition.SounderChanged(sounderId, m_parameter);
	//	m_overLapComputer.SounderChanged(sounderId,m_parameter.m_overlapParameter);
}
void CompensationModule::StreamClosed(const char *streamName)
{
	m_transPosition.StreamClosed(streamName, m_parameter);
	//	m_overLapComputer.StreamClosed(streamName,m_parameter.m_overlapParameter);
}
void CompensationModule::StreamOpened(const char *streamName)
{
	m_transPosition.StreamOpened(streamName, m_parameter);
	//	m_overLapComputer.StreamOpened(streamName,m_parameter.m_overlapParameter);
}

