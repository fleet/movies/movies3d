
#include "Compensation/CompensationParameter.h"
#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

CompensationParameter::CompensationParameter() : BaseKernel::ParameterModule("CompensationParameter")
{
}

CompensationParameter::~CompensationParameter(void)
{
}

// IPSIS - OTK - Ajout MovConfig
bool CompensationParameter::Serialize(BaseKernel::MovConfig * movConfig)
{
	m_rfLock.Lock();

	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	movConfig->SerializeData<double>(m_overlapParameter.IsEnable(), eBool, "EnableOverlap");
	movConfig->SerializeData<double>(m_overlapParameter.GetReferenceThreshold(), eDouble, "ReferenceThreshold");
	movConfig->SerializeData<double>(m_overlapParameter.GetDataThreshold(), eDouble, "DataThreshold");
	movConfig->SerializeData<double>(m_overlapParameter.GetGridSpaceZ(), eDouble, "GridSpaceZ");
	movConfig->SerializeData<double>(m_overlapParameter.GetGridSpaceXYAngleFactor(), eDouble, "GridSpaceXYAngleFactor");
	movConfig->SerializeData<unsigned int>(m_overlapParameter.GetReferenceWindowsPingCount(), eUInt, "ReferenceWindowsPingCount");

	movConfig->SerializeData<double>(m_rfParameter.GetOverlapThreshold(), eDouble, "OverlapThreshold");
	movConfig->SerializeData<unsigned int>(m_rfParameter.GetMinEchoesNb(), eUInt, "MinEchoesNb");
	movConfig->SerializeData<double>(m_rfParameter.GetPrecisionLog(), eDouble, "PrecisionLog");
	movConfig->SerializeData<double>(m_rfParameter.GetPrecisionDepth(), eDouble, "PrecisionDepth");
	movConfig->SerializeData<bool>(m_rfParameter.GetUseSV(), eBool, "UseSV");
	movConfig->SerializeData<bool>(m_rfParameter.GetComputeUnfiltered(), eBool, "UseUnfiltered");
	movConfig->SerializeData<std::string>(m_rfParameter.GetRefTransducer(), eString, "RefTransducer");

	// OTK - 19/01/2010 - ajout de la sauvegarde/chargement des positions des transducteurs
	m_transducerPositionSet.Serialize(movConfig);

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	m_rfLock.Unlock();

	return true;
}

bool CompensationParameter::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	m_rfLock.Lock();
	
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	// debut de la d�s�rialisation du module
	if (result)
	{
		double d_val;
		unsigned int ui_val;
		bool b_val;
		std::string s_val;

		result = result && movConfig->DeSerializeData<bool>(&b_val, eBool, "EnableOverlap");
		m_overlapParameter.SetEnable(b_val);
		result = result && movConfig->DeSerializeData<double>(&d_val, eDouble, "ReferenceThreshold");
		m_overlapParameter.SetReferenceThreshold(d_val);
		result = result && movConfig->DeSerializeData<double>(&d_val, eDouble, "DataThreshold");
		m_overlapParameter.SetDataThreshold(d_val);
		result = result && movConfig->DeSerializeData<double>(&d_val, eDouble, "GridSpaceZ");
		m_overlapParameter.SetGridSpaceZ(d_val);
		result = result && movConfig->DeSerializeData<double>(&d_val, eDouble, "GridSpaceXYAngleFactor");
		m_overlapParameter.SetGridSpaceXYAngleFactor(d_val);
		result = result && movConfig->DeSerializeData<unsigned int>(&ui_val, eUInt, "ReferenceWindowsPingCount");
		m_overlapParameter.SetReferenceWindowsPingCount(ui_val);

		result = result && movConfig->DeSerializeData<double>(&d_val, eDouble, "OverlapThreshold");
		m_rfParameter.SetOverlapThreshold(d_val);
		result = result && movConfig->DeSerializeData<unsigned int>(&ui_val, eUInt, "MinEchoesNb");
		m_rfParameter.SetMinEchoesNb(ui_val);
		result = result && movConfig->DeSerializeData<double>(&d_val, eDouble, "PrecisionLog");
		m_rfParameter.SetPrecisionLog(d_val);
		result = result && movConfig->DeSerializeData<double>(&d_val, eDouble, "PrecisionDepth");
		m_rfParameter.SetPrecisionDepth(d_val);
		result = result && movConfig->DeSerializeData<bool>(&b_val, eBool, "UseSV");
		m_rfParameter.SetUseSV(b_val);
		result = result && movConfig->DeSerializeData<bool>(&b_val, eBool, "UseUnfiltered");
		m_rfParameter.SetComputeUnfiltered(b_val);
		result = result && movConfig->DeSerializeData<std::string>(&s_val, eString, "RefTransducer");
		m_rfParameter.SetRefTransducer(s_val);

		// OTK - 19/01/2010 - ajout de la sauvegarde/chargement des positions des transducteurs
		result = result && m_transducerPositionSet.DeSerialize(movConfig);
	}
	
	// fin de la d�s�rialisation du module
	movConfig->DeSerializePushBack();

	m_rfLock.Unlock();

	return result;
}
