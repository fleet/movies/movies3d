
#include "Compensation/CompensationTransducerPosition.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/M3DKernel.h"
CompensationTransducerPosition::CompensationTransducerPosition(void)
{
}

CompensationTransducerPosition::~CompensationTransducerPosition(void)
{
}

void CompensationTransducerPosition::SounderChanged(std::uint32_t sounderId, CompensationParameter	&refParameter)
{
	M3DKernel *pKern = M3DKernel::GetInstance();
	CurrentSounderDefinition &lsouderMgr = pKern->getObjectMgr()->GetPingFanContainer().m_sounderDefinition;

	Sounder *pSounder = lsouderMgr.GetSounderWithId(sounderId);

	for (unsigned int numTrans = 0; numTrans < pSounder->GetTransducerCount(); numTrans++)
	{
		Transducer *pTransducer = pSounder->GetTransducer(numTrans);
		TransducerPositionCompensation *p = refParameter.m_transducerPositionSet.GetTransducerPosition(pTransducer->m_transName);
		// OTK - FAE045 - sauvegarde des param�tres de compensation HAC avant �crasement �ventuel par les param�tres utilisateur
		SaveHACFileValues(pTransducer, p);
		ApplyCompensation(pTransducer, p);
	}
	RecomputeAllMatrix();
}

void CompensationTransducerPosition::SaveHACFileValues(Transducer *pTrans, TransducerPositionCompensation *pCompens)
{
	pCompens->m_HACheadingRot = pTrans->m_transRotationAngleRad;
	pCompens->m_HACpitchRot = pTrans->m_transFaceAlongAngleOffsetRad;
	pCompens->m_HACrollRot = pTrans->m_transFaceAthwarAngleOffsetRad;
}

void CompensationTransducerPosition::ApplyCompensation(Transducer *pTrans, TransducerPositionCompensation *pCompens)
{
	pTrans->SetInternalDelayTranslation(pCompens->m_internalDelay);
	pTrans->SetSampleOffset(pCompens->m_sampleOffset);
	if (pCompens->m_bUseHACFileValues)
	{
		pTrans->m_transRotationAngleRad = pCompens->m_HACheadingRot;
		pTrans->m_transFaceAlongAngleOffsetRad = pCompens->m_HACpitchRot;
		pTrans->m_transFaceAthwarAngleOffsetRad = pCompens->m_HACrollRot;
		pTrans->m_bUseAngleCustomValues = false;
	}
	else
	{
		pTrans->m_transRotationAngleRad = pCompens->m_headingRot;
		pTrans->m_transFaceAlongAngleOffsetRad = pCompens->m_pitchRot;
		pTrans->m_transFaceAthwarAngleOffsetRad = pCompens->m_rollRot;
		pTrans->m_bUseAngleCustomValues = true;
	}
}

void CompensationTransducerPosition::RecomputeAllMatrix()
{
	M3DKernel *pKern = M3DKernel::GetInstance();
	pKern->getObjectMgr()->GetPingFanContainer().m_sounderDefinition.ComputeMatrix();
}

void CompensationTransducerPosition::ApplyParameter(CompensationParameter &refParameter)
{
	M3DKernel *pKern = M3DKernel::GetInstance();

	CurrentSounderDefinition &lsounderMgr = pKern->getObjectMgr()->GetPingFanContainer().m_sounderDefinition;

	for (unsigned int numSounder = 0; numSounder < lsounderMgr.GetNbSounder(); numSounder++)
	{
		Sounder *pSounder = lsounderMgr.GetSounder(numSounder);
		for (unsigned int numTrans = 0; numTrans < pSounder->GetTransducerCount(); numTrans++)
		{
			Transducer *pTransducer = pSounder->GetTransducer(numTrans);
			TransducerPositionCompensation *p = refParameter.m_transducerPositionSet.GetTransducerPosition(pTransducer->m_transName);
			ApplyCompensation(pTransducer, p);
		}
	}
	RecomputeAllMatrix();
}
