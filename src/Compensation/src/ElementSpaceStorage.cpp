
#include "Compensation/ElementSpaceStorage.h"
#include <algorithm>

bool UDgreater(ElementSpace elem1, ElementSpace elem2)
{
	if (elem1.coordX != elem2.coordX)
		return elem1.coordX > elem2.coordX;
	else if (elem1.coordY != elem2.coordY)
		return elem1.coordY > elem2.coordY;
	else return elem1.coordZ > elem2.coordZ;
}

ElementSpaceStorage::ElementSpaceStorage(double gridDefinition)
{
	m_gridDefinition = gridDefinition;
}

ElementSpaceStorage::~ElementSpaceStorage(void)
{
}
void ElementSpaceStorage::Finalize()
{




}

void ElementSpaceStorage::Clear()
{
	m_myVolume.clear();
}
unsigned int ElementSpaceStorage::Count()
{
	return m_myVolume.size();
}



/*
unsigned int ElementSpaceStorage::EvaluateOverLap(ElementSpaceStorage &refCompare )
{
	ElementSpaceStorage &refMax=*this;
	ElementSpaceStorage &refMin=refCompare;
	unsigned int nbCommon=0;

	if(Count()< refCompare.Count())
	{
		refMax=refCompare;
		refMin=*this;
	}

	std::set<ElementSpace>::iterator mItrMax=refMax.m_myVolume.begin();

	while(mItrMax!=refMax.m_myVolume.end())
	{
		std::set<ElementSpace>::iterator i= refMin.m_myVolume.find(*mItrMax);
		if(i!=refMin.m_myVolume.end())
		{
			nbCommon++;
		}
		mItrMax++;
	}


	return nbCommon;
}*/

BaseMathLib::Vector3D ElementSpaceStorage::GetElementCoord(unsigned int index)
{
	BaseMathLib::Vector3D ret(0, 0, 0);
	if (index < Count())
	{
		ret.x = m_myVolume[index].coordX;
		ret.y = m_myVolume[index].coordY;
		ret.z = m_myVolume[index].coordZ;
	}
	return ret;
}
