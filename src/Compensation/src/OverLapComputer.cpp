
#include "Compensation/OverLapComputer.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/MemoryStruct.h"
#include "Compensation/ElementSpaceStorage.h"
#include "M3DKernel/datascheme/PingFanContainer.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/M3DKernel.h"

#include "Compensation/OverLapStackAction.h"

using namespace BaseMathLib;

#define _USE_WORKER_THREAD

namespace
{
	constexpr const char * LoggerName = "Compensation.OverLapComputer";
}

OverLapComputer::OverLapComputer(void)
{

}

OverLapComputer::~OverLapComputer(void)
{
	ClearMemory();

	//destroy action stacks
	std::vector<ActionStack*>::iterator iterStack = m_actionStackList.begin();
	while (iterStack != m_actionStackList.end())
	{
		delete *iterStack;
		iterStack++;
	}
}

bool OverLapComputer::EvaluateParameter(OverLapParameter&param, CANCEL_CALLBACK cancelCB)
{
	bool ret = false;
	if (!param.IsVolumeParameterEqual(m_localParameter))
	{
		m_localParameter = param;
		ret = true;
		this->ClearMemory();
		if (m_localParameter.IsEnable())
		{
			PingFanIterator windowedPingFanIterator;
			windowedPingFanIterator.Init(param.m_bUsePingId,
                (std::uint64_t)param.m_pingFilteredStart - m_localParameter.m_referenceWindowsPingCount,
                (std::uint64_t)param.m_pingFilteredStop + m_localParameter.m_referenceWindowsPingCount);
			windowedPingFanIterator.RBegin();

			while (!windowedPingFanIterator.REnd() && !cancelCB())
			{
				PingFan* pFan = *windowedPingFanIterator;
				if (pFan != NULL)
				{
					PingFanAdded(pFan, param, cancelCB);
				}
				++windowedPingFanIterator;
			}
		}
	}
	return ret;
}

void OverLapComputer::EvalutateRecover(OverLapParameter& param, PROGRESS_CALLBACK progress, CANCEL_CALLBACK cancel)
{
	m_Lock.Lock();

	unsigned int pingFanToCompute = 0;
	//early rejection test, if nothing to compute, we just skip processing
	m_pingFanIterator.Init(param.m_bUsePingId, param.m_pingFilteredStart, param.m_pingFilteredStop);
	m_pingFanIterator.RBegin();
	while (!m_pingFanIterator.REnd())
	{
		PingFan* pingFan = *m_pingFanIterator;
		if (pingFan != NULL)
		{
			if (!pingFan->getSounderRef()->m_isMultiBeam)
			{
				pingFanToCompute++;
			}
		}
		++m_pingFanIterator;
	}

	if (pingFanToCompute == 0)
		return;

	pingFanToCompute = 0;

	if (!cancel())
	{
		EvalutateWindow(param);
		EvaluateParameter(param, cancel);
		if (!m_localParameter.IsEnable())
		{
			M3D_LOG_WARN(LoggerName, "OverLapComputing is disable, cannot evaluate overlap");
			return;
		}

		//for each ping
		m_pingFanIterator.RBegin();

		int iStack = 0;
		InitActionStacks();

		while (!m_pingFanIterator.REnd() && !cancel())
		{
			OverLapStatitic pingStat;
			PingFan* pingFan = *m_pingFanIterator;
			if (pingFan != NULL)
			{
#ifndef _USE_WORKER_THREAD
				if (!pingFan->getSounderRef()->m_isMultiBeam)
				{
					AddFilterData(pingFan, param, pingStat, cancel);
				}
				progress("Computing Overlap...", (int)60.0*((double)pingFanToCompute / (param.m_pingFilteredStop - param.m_pingFilteredStart)));
#else
				if (!pingFan->getSounderRef()->m_isMultiBeam)
				{
					iStack = ++iStack % m_actionStackList.size();

					//multi_thread
					int progression = (int)60.0*((double)pingFanToCompute / (param.m_pingFilteredStop - param.m_pingFilteredStart));
					m_actionStackList[iStack]->ScheduleAction(
						OverLapStackAction::CreateAction_FilterData(this, pingFan, &param, progress, progression, cancel));
				}
#endif			
			}

			++m_pingFanIterator;
			pingFanToCompute++;
		}

#ifdef _USE_WORKER_THREAD

		//wait for all stacks to be empty
		std::vector<ActionStack*>::iterator iterStack = m_actionStackList.begin();
		while (iterStack != m_actionStackList.end())
		{
			(*iterStack)->WaitFinished();
			iterStack++;
		}
#endif

		if (!cancel())
		{
			M3D_LOG_INFO(LoggerName, Log::format("EvalutateRecover:: Processed [%d]", m_totalStat.m_echoProcessedCount));
		}
	}

	m_Lock.Unlock();
}

//*****************************************************************************
// Name : EvalutateWindow
// Description : Evaluate the window to be computed
// Parameters : * (in/out)  OverLapParameter& param
// Return : void
//*****************************************************************************
void OverLapComputer::EvalutateWindow(OverLapParameter& param)
{
	//for each ping
	m_pingFanIterator.RBegin();
	while (!m_pingFanIterator.REnd())
	{
		PingFan* pingFan = *m_pingFanIterator;
		if (pingFan != NULL)
		{
			Sounder* pSounder = pingFan->getSounderRef();
			if (!pSounder->m_isMultiBeam)
			{
				//for each transducer
				for (unsigned int numTrans = 0; numTrans < pSounder->GetTransducerCount(); numTrans++)
				{
					Transducer *pTrans = pSounder->GetTransducer(numTrans);
					double angleAthwart = pTrans->getSoftChannelPolarX(0)->m_mainBeamAthwartSteeringAngleRad;
					double angle3dBAthwart = pTrans->getSoftChannelPolarX(0)->m_beam3dBWidthAthwartRad;

                    param.m_angleMin = std::min(param.m_angleMin, angleAthwart - angle3dBAthwart);
                    param.m_angleMax = std::max(param.m_angleMax, angleAthwart + angle3dBAthwart);
				}
			}
		}
		++m_pingFanIterator;
	}
}

void OverLapComputer::PingFanAdded(PingFan *p, OverLapParameter& param, CANCEL_CALLBACK cancelCB)
{
	if (EvaluateParameter(param, cancelCB) || !m_localParameter.IsEnable())
		return;

	if (p != NULL)
	{
		if (p->getSounderRef()->m_isMultiBeam)
		{
			AddReference(p, param);
		}
	}
}
void OverLapComputer::SounderChanged(std::uint32_t, OverLapParameter&)
{
	ClearMemory();
};
void OverLapComputer::StreamClosed(const char *streamName, OverLapParameter&)
{
};
void OverLapComputer::StreamOpened(const char *streamName, OverLapParameter&)
{
};

//*****************************************************************************
// Name : ComputeEchoRange
// Description : Compute the echo range on the channel matching the depth range
// Parameters : * (in)  double minAngle
//              * (in)  double maxAngle
//              * (in)  double channelDepthOffset
//              * (in)  double beamsSamplesSpacing
//              * (in)  double minDepth
//              * (in)  double maxDepth
//              * (in/out)  int minEcho
//              * (in/out)  int maxEcho
// Return : void
//*****************************************************************************
void OverLapComputer::ComputeEchoRange(double minAngle, double maxAngle,
	double channelDepthOffset, double beamsSamplesSpacing,
	double minDepth, double maxDepth,
	int& minEcho, int& maxEcho)
{
	minDepth -= channelDepthOffset;
	maxDepth -= channelDepthOffset;

    double angleForMinDepth = std::max(fabs(minAngle), fabs(maxAngle));
    double angleForMaxDepth = std::min(fabs(minAngle), fabs(maxAngle));

	if (angleForMinDepth != 0)
	{
		minEcho = minDepth / cos(angleForMinDepth) / beamsSamplesSpacing - 1;
	}
	if (angleForMaxDepth != 0)
	{
		maxEcho = maxDepth / cos(angleForMaxDepth) / beamsSamplesSpacing + 1;
	}
}

void OverLapComputer::AddReference(PingFan *p, OverLapParameter& param)
{
	Sounder *pSounder = p->getSounderRef();

	for (unsigned int numTrans = 0; numTrans < pSounder->GetTransducerCount(); numTrans++) {
		Transducer *pTransducerRef = pSounder->GetTransducer(numTrans);

		//no global lock on MemoryStruct is required, assuming that MemoryStruct would only be over-resized
		MemoryStruct *pMemStruct = p->GetMemorySetRef()->GetMemoryStruct(numTrans);
		Vector2I memSize = pMemStruct->GetDataFmt()->getSize();
		unsigned int Xdim = memSize.x;
		unsigned int Ydim = memSize.y;

		Vector3D realCoordCenter = pSounder->GetSoftChannelCoordinateToWorldCoord(p, numTrans, 0, Vector3D(0, 0, 0));
		//Vector3D realCoordCenter=pSounder->GetBaseTransducerToWorldCoord(p,numTrans);

		Vector2I pos(0, 0);
		for (unsigned int i = 0; i < Xdim; i++)
		{
			pos.x = i;

			//filter reference data on angles with the computing window on each channel
			SoftChannel *pChannel = pTransducerRef->getSoftChannelPolarX(i);

			double angleAthwart = pChannel->m_mainBeamAthwartSteeringAngleRad;
			double angle3dBAthwart = pChannel->m_beam3dBWidthAthwartRad * 0.5;
			double angleMin = angleAthwart - angle3dBAthwart;
			double angleMax = angleAthwart + angle3dBAthwart;

			//if the channel belongs to the angular compute window
			if (angleMin < param.m_angleMax && param.m_angleMin < angleMax)
			{
				//compute the min and max echo belonging to the depth range for the channel
				int minEcho = 0;
				int maxEcho = 0;
				ComputeEchoRange(angleMin, angleMax, realCoordCenter.z, pTransducerRef->getBeamsSamplesSpacing(),
					param.m_depthMin, param.m_depthMax, minEcho, maxEcho);

                maxEcho = std::min(maxEcho, (int)Ydim - 1);
                minEcho = std::max(0, (int)minEcho);

				for (unsigned int j = minEcho; j <= maxEcho; j++)
				{
					pos.y = j;

					//we assume that MemoryStruct size could never decrease
					assert(pMemStruct->GetDataFmt()->getSize().y > j);

					DataFmt value = pMemStruct->GetDataFmt()->GetValueToVoxel_ThreadSafe(pos);
					if (value > param.GetReferenceThreshold())
					{
						Vector3D realCoord = pSounder->GetPolarToWorldCoord(p, numTrans, i, j);
						Box aaBox = pSounder->GetAxisAlignedBoundingBox(p, numTrans, i, j);
						ReferenceEcho a(aaBox, realCoordCenter, realCoord);
						this->m_StoragerRef.AddEcho(a);
					}
				}
			}
		}
	}
}

void OverLapComputer::AddEcho(ElementSpaceStorage &aStorage, double depthPolar, Vector3D aWorldCoord, Vector3D aWorldCoordTranducer, BaseMathLib::Box &aaBoundingBox)
{
	double distanceRef = aWorldCoord.distance(aWorldCoordTranducer);
	double xyInc = depthPolar*tan(m_localParameter.GetGridSpaceXYAngleFactor());

	//la subdivision en XY doit garder au moins 1 �chantillon
	ElementSpace mySpace;
	for (double x = xyInc*ceil(aaBoundingBox.m_minBox.x / xyInc); x <= aaBoundingBox.m_maxBox.x; x += xyInc)
	{
		for (double y = xyInc*ceil(aaBoundingBox.m_minBox.y / xyInc); y <= aaBoundingBox.m_maxBox.y; y += xyInc)
		{
			//on regarde l'appartenance � l'ellipse
			mySpace.coordX = x;
			mySpace.coordY = y;

			mySpace.coordZ = sqrt(distanceRef*distanceRef -
				(mySpace.coordX - aWorldCoordTranducer.x)*(mySpace.coordX - aWorldCoordTranducer.x) -
				(mySpace.coordY - aWorldCoordTranducer.y)*(mySpace.coordY - aWorldCoordTranducer.y)) +
				aWorldCoordTranducer.z;
			aStorage.Add(mySpace);
		}
	}
	//la subdivision en XY doit garder au moins 1 �chantillon
	if (aStorage.Count() == 0)
	{
		mySpace.coordX = aWorldCoord.x;
		mySpace.coordY = aWorldCoord.y;
		mySpace.coordZ = aWorldCoord.z - aWorldCoordTranducer.z;
		aStorage.Add(mySpace);
	}
}

// add a ping fan as a filtered one
void OverLapComputer::AddFilterData(PingFan *p, OverLapParameter& param, CANCEL_CALLBACK cancelCB)
{
	OverLapStatitic pingStat;
	AddFilterData(p, param, pingStat, cancelCB);

	m_totalStat = m_totalStat + pingStat;
}
void OverLapComputer::AddFilterData(PingFan *p, OverLapParameter& param, OverLapStatitic &stat, CANCEL_CALLBACK cancelCB)
{
	if (!m_localParameter.IsEnable())
		return;

	ElementSpaceStorage myStorage(param.GetGridSpaceZ());
	Sounder *pSounder = p->getSounderRef();
	for (unsigned int numTrans = 0; numTrans < pSounder->GetTransducerCount(); numTrans++) {
		Transducer *pTransducerRef = pSounder->GetTransducer(numTrans);

		Vector3D realCoordCenter = pSounder->GetSoftChannelCoordinateToWorldCoord(p, numTrans, 0, Vector3D(0, 0, 0));
		//Vector3D realCoordCenter=pSounder->GetBaseTransducerToWorldCoord(p,numTrans);

		//no global lock on MemoryStruct is required, assuming that MemoryStruct would only be over-resized
		MemoryStruct *pMemStruct = p->GetMemorySetRef()->GetMemoryStruct(numTrans);
		Vector2I memSize = pMemStruct->GetDataFmt()->getSize();
		unsigned int Xdim = memSize.x;
		unsigned int Ydim = memSize.y;

		Vector2I pos(0, 0);
		for (unsigned int i = 0; i < Xdim; i++)
		{
			pos.x = i;

			//filter reference data on angles with the computing window on each channel
			SoftChannel *pChannel = pTransducerRef->getSoftChannelPolarX(i);

			double angleAthwart = pChannel->m_mainBeamAthwartSteeringAngleRad;
			double angle3dBAthwart = pChannel->m_beam3dBWidthAthwartRad * 0.5;
			double angleMin = angleAthwart - angle3dBAthwart;
			double angleMax = angleAthwart + angle3dBAthwart;

			//compute the min and max echo belonging to the depth range for the channel
			int minEcho = 0;
			int maxEcho = 0;
			ComputeEchoRange(angleMin, angleMax, realCoordCenter.z, pTransducerRef->getBeamsSamplesSpacing(),
				param.m_depthMin, param.m_depthMax, minEcho, maxEcho);

            maxEcho = std::min(maxEcho, (int)Ydim - 1);
            minEcho = std::max(0, (int)minEcho);

			//reset overlap value to -1 if existing
	  //lock to prevent a concurrent resize by the reader thread
			pMemStruct->LockOverLap();
			if (pMemStruct->GetOverLap())
			{
				//Lock memory for safe access
				pMemStruct->GetOverLap()->Lock();
				short *pStart = pMemStruct->GetOverLap()->GetPointerToVoxel(Vector2I(i, minEcho));
				for (unsigned int k = 0; k <= maxEcho - minEcho; k++)
				{
					*pStart++ = -1;
				}
				pMemStruct->GetOverLap()->Unlock();
			}
			else
			{
				pMemStruct->AllocateOverlap();
			}
			pMemStruct->UnLockOverLap();

			for (unsigned int j = minEcho; j <= maxEcho && !cancelCB(); j++)
			{
				pos.y = j;

				//we assume that MemoryStruct size could never decrease
				assert(pMemStruct->GetDataFmt()->getSize().y > j);

				DataFmt value = pMemStruct->GetDataFmt()->GetValueToVoxel_ThreadSafe(pos);
				if (value > param.GetDataThreshold())
				{
					stat.m_echoProcessedCount++;
					myStorage.Clear();
					Vector3D realCoord = pSounder->GetPolarToWorldCoord(p, numTrans, i, j);
					Box aaBox = pSounder->GetAxisAlignedBoundingBox(p, numTrans, i, j);
					AddEcho(myStorage, (j + 0.5)*pTransducerRef->getBeamsSamplesSpacing(), realCoord, realCoordCenter, aaBox);
					myStorage.Finalize();

					unsigned int nbElementaryVolume = myStorage.Count();
					unsigned int nbElementaryRecovered = this->m_StoragerRef.EvaluateOverLap(myStorage, pTransducerRef->getBeamsSamplesSpacing());
					assert(nbElementaryRecovered <= nbElementaryVolume);

					if (nbElementaryVolume != 0)
					{
						short delta = nbElementaryRecovered * 100 / nbElementaryVolume;

						//lock memory access during write operation
						pMemStruct->GetOverLap()->Lock();
						*pMemStruct->GetOverLap()->GetPointerToVoxel(pos) = delta;
						pMemStruct->GetOverLap()->Unlock();
					}
					else
					{
						M3D_LOG_ERROR(LoggerName, "Nb volume elementaire null");
					}
				}
			}
		}
	}
}

void OverLapComputer::ClearMemory()
{
	m_StoragerRef.Clear();
}

//reset multithread data
void OverLapComputer::ResetActionStacks()
{
	std::vector<ActionStack*>::iterator iterStack = m_actionStackList.begin();
	while (iterStack != m_actionStackList.end())
	{
		(*iterStack)->Reset();
		iterStack++;
	}
}

int CountOne(int value)
{
	int result = 0;

	while (value != 0)
	{
		result += (value & 0x1);
		value = value >> 1;
	}

	return result;
}

int GetProcessorCoreNb()
{
	int result = -1;

#ifdef WIN32
	HANDLE processHandle = GetCurrentProcess();
	if (processHandle > 0)
	{
		ULONG_PTR processMask = 0;
		ULONG_PTR systemMask = 0;

		if (GetProcessAffinityMask(processHandle, &processMask, &systemMask))
		{
			result = CountOne(systemMask);
		}
	}
#endif

	return result;
}

//init multithread managment
void OverLapComputer::InitActionStacks()
{
	ResetActionStacks();

	//destroy action stacks
	std::vector<ActionStack*>::iterator iterStack = m_actionStackList.begin();
	while (iterStack != m_actionStackList.end())
	{
		delete *iterStack;
		iterStack++;
	}
	m_actionStackList.clear();

	int nbCore = GetProcessorCoreNb();
    int nbStack = std::max(1, nbCore - m_localParameter.GetNbReservedCores());

	m_Lock.Lock();
	for (int i = 0; i < nbStack; i++)
	{
		m_actionStackList.push_back(new ActionStack());
	}
	m_Lock.Unlock();
}
