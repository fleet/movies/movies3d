
#include "Compensation/OverLapParameter.h"
#include <math.h>
OverLapParameter::OverLapParameter(void) : BaseKernel::ParameterObject()
{
	BuildDefaultSettings();
}


OverLapParameter::~OverLapParameter(void)
{
}
void OverLapParameter::BuildDefaultSettings()
{
	m_bIsEnable = false;

	m_spaceXYAngleFactor = (DEG_TO_RAD(1));

	m_gridSpaceZ = 0.10;

	m_elementaryUnitLenght = 50.0;

	m_moduleDataThreshold = -7500;
	m_moduleReferenceThreshold = -6500;
	m_pingFilteredStart = 0;
	m_pingFilteredStop = 65535;

	//use of ping ID instead of the ping index 
	m_bUsePingId = false;

	//by default, do NOT reserve cores for other use
	m_nbReservedCores = 0;

	m_referenceWindowsPingCount = 10;

	//spatial depth window
	m_depthMin = 0;
	m_depthMax = 10000;

	//spatial angular window
	m_angleMin = DEG_TO_RAD(90);
	m_angleMax = DEG_TO_RAD(-90);

}

// IPSIS - OTK - Ajout MovConfig
bool OverLapParameter::Serialize(BaseKernel::MovConfig * MovConfig)
{
	return true;
}
bool OverLapParameter::DeSerialize(BaseKernel::MovConfig * MovConfig)
{
	return true;
}
double	OverLapParameter::GetGridSpaceXY(double depthZ)
{
	return MAX(floor(abs(depthZ*sin(m_spaceXYAngleFactor))), m_gridSpaceZ);
}
