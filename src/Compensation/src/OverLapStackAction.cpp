// -*- C++ -*-
// ****************************************************************************
// Class: OverLapStackAction
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Sept 2009
// Soci�t� : IPSIS
// ****************************************************************************

#include "Compensation/OverLapStackAction.h"

#include "Compensation/OverLapComputer.h"
#include "Compensation/OverLapParameter.h"

#include "M3DKernel/datascheme/PingFan.h"

class OverLapComputer;
class OverLapParameter;
class PingFan;

#include <assert.h>

// *********************************************************************
// Constructors / Destructor
// *********************************************************************
// d�fault constructor
OverLapStackAction::OverLapStackAction()
{
	actionType = E_FILTERDATA_ACTION;

	//data for filter data action
	pOverLapComputer = NULL;
	pPingFan = NULL;
	pParam = NULL;
}

// Destructor
OverLapStackAction::~OverLapStackAction(void)
{
}

// *********************************************************************
// Methods
// *********************************************************************

OverLapStackAction* OverLapStackAction::CreateAction_FilterData(OverLapComputer* pOverLapComputer,
	PingFan* pPingFan,
	OverLapParameter* pParam,
	PROGRESS_CALLBACK progressCB,
	int progression,
	CANCEL_CALLBACK cancelCB)
{
	OverLapStackAction* result = new OverLapStackAction();

	result->actionType = E_FILTERDATA_ACTION;
	result->pOverLapComputer = pOverLapComputer;
	result->pPingFan = pPingFan;
	result->pParam = pParam;
	result->progressCB = progressCB;
	result->progression = progression;
	result->cancelCB = cancelCB;

	return result;
}

//process
void OverLapStackAction::Process()
{
	switch (actionType)
	{
	case E_FILTERDATA_ACTION:
	{
		//perform the close action on the shoal extraction module
		pOverLapComputer->AddFilterData(pPingFan, *pParam, cancelCB);
		progressCB("Computing Overlap...", progression);
	}
	break;
	default:
		break;
	}
}
