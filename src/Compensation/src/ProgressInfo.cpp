// -*- C++ -*-
// ****************************************************************************
// Class: ProgressInfo
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Aout 2009
// Soci�t� : IPSIS
// ****************************************************************************
#include "Compensation/ProgressInfo.h"

//*****************************************************************************
// Name : ProgressInfo
// Description : constructor
//*****************************************************************************
ProgressInfo::ProgressInfo()
{
	Reset();
}

//*****************************************************************************
// Name : Reset
// Description : 
// Parameters : void
// Return : void
//*****************************************************************************
void ProgressInfo::Reset()
{
	m_bActive = true;
	m_text = "";
	m_progress = 0.0;
	m_currentStepRange = 100.0;
}

//*****************************************************************************
// Name : IncrProgress
// Description : double val
// Parameters : void
// Return : void
//*****************************************************************************
void ProgressInfo::IncrProgress(double val) 
{
	m_lock.Lock();
	m_progress += val;
    m_lock.Unlock();
}
