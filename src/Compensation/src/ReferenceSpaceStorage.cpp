
#include "Compensation/ReferenceSpaceStorage.h"

using namespace BaseMathLib;


bool ReferenceEcho::IsContained(BaseMathLib::Vector3D position, double toleranceEpaisseur)
{
	bool ret = false;
	if (this->m_boundingBox.IsContained(position))
	{
		double distanceRef = m_realCoord.distance(m_center);
		double dist = m_center.distance(position);
		if (abs(dist - distanceRef) <= toleranceEpaisseur)
			ret = true;
	}


	return ret;

}
ReferenceSpaceStorage::ReferenceSpaceStorage(void)
{
}

ReferenceSpaceStorage::~ReferenceSpaceStorage(void)
{
}

void ReferenceSpaceStorage::Clear()
{
	m_listEcho.clear();
}
unsigned int ReferenceSpaceStorage::Count()
{
	return m_listEcho.size();
}
void ReferenceSpaceStorage::AddEcho(ReferenceEcho &ref)
{
	m_listEcho.push_back(ref);
}

unsigned int ReferenceSpaceStorage::EvaluateOverLap(ElementSpaceStorage &refElement, double toleranceEpaisseur)
{
	unsigned int nbEchoContained = 0;
	for (unsigned int i = 0; i < refElement.Count(); i++)
	{
		BaseMathLib::Vector3D pos = refElement.GetElementCoord(i);
		if (CheckPoint(pos, toleranceEpaisseur))
			nbEchoContained++;

	}
	return nbEchoContained;
}
bool ReferenceSpaceStorage::CheckPoint(BaseMathLib::Vector3D &a, double toleranceEpaisseur)
{
	unsigned int echoListSize = Count();
	for (unsigned int listEchoIdx = 0; listEchoIdx < echoListSize; listEchoIdx++)
	{
		if (m_listEcho[listEchoIdx].IsContained(a, toleranceEpaisseur))
			return true;
	}
	return false;
}
