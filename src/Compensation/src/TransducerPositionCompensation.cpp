
#include "Compensation/TransducerPositionCompensation.h"

#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

TransducerPositionCompensation::TransducerPositionCompensation(const char *Name) : BaseKernel::ParameterObject()
{
	m_TransducerName = Name;
	m_headingRot = 0;
	m_pitchRot = 0;
	m_rollRot = 0;
	m_internalDelay = 0;
	m_sampleOffset = 0;
	m_bUseHACFileValues = true;
	m_HACheadingRot = 0;
	m_HACpitchRot = 0;
	m_HACrollRot = 0;
}

TransducerPositionCompensation::~TransducerPositionCompensation(void)
{
}

bool TransducerPositionCompensation::checkTransName(std::string Name)
{
	bool ret = !m_TransducerName.compare(Name);
	return ret;
}

// IPSIS - OTK - Ajout MovConfig
bool TransducerPositionCompensation::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "TransducerPosition");

	movConfig->SerializeData<bool>(m_bUseHACFileValues, eBool, "UseHACFileValues");

	movConfig->SerializeData<std::string>(m_TransducerName, eString, "TransducerName");
	movConfig->SerializeData<double>(m_headingRot, eDouble, "HeadingRot");
	movConfig->SerializeData<double>(m_pitchRot, eDouble, "PitchRot");
	movConfig->SerializeData<double>(m_rollRot, eDouble, "RollRot");
	movConfig->SerializeData<double>(m_internalDelay, eDouble, "InternalDelay");
	movConfig->SerializeData<double>(m_sampleOffset, eDouble, "SampleOffset");

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	return true;
}
bool TransducerPositionCompensation::DeSerialize(MovConfig * movConfig)
{
	bool result = true;
	// debut de la d�s�rialisation du module
	if (result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "TransducerPosition"))
	{
		result = result && movConfig->DeSerializeData<bool>(&m_bUseHACFileValues, eBool, "UseHACFileValues");

		result = result && movConfig->DeSerializeData<std::string>(&m_TransducerName, eString, "TransducerName");
		result = result && movConfig->DeSerializeData<double>(&m_headingRot, eDouble, "HeadingRot");
		result = result && movConfig->DeSerializeData<double>(&m_pitchRot, eDouble, "PitchRot");
		result = result && movConfig->DeSerializeData<double>(&m_rollRot, eDouble, "RollRot");
		result = result && movConfig->DeSerializeData<double>(&m_internalDelay, eDouble, "InternalDelay");
		result = result && movConfig->DeSerializeData<double>(&m_sampleOffset, eDouble, "SampleOffset");


		// fin de la d�s�rialisation du module
		movConfig->DeSerializePushBack();
	}

	return result;
}
