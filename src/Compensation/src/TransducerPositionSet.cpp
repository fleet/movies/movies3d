
#include "Compensation/TransducerPositionSet.h"
#include <assert.h>
#include "M3DKernel/DefConstants.h"
#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

TransducerPositionSet::TransducerPositionSet(void) : BaseKernel::ParameterObject()
{
	BuildDefaultSettings();
}

TransducerPositionSet::~TransducerPositionSet(void)
{
	for (unsigned int i = 0; i < m_RecordSet.size(); i++)
	{
		delete m_RecordSet[i];
	}
}

// IPSIS - OTK - Ajout MovConfig
bool TransducerPositionSet::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "TransducerPositionSet");

	unsigned int nbTrans = (unsigned int)m_RecordSet.size();
	movConfig->SerializeData<unsigned int>(nbTrans, eUInt, "NbTransducer");

	for (size_t i = 0; i < nbTrans; i++)
	{
		m_RecordSet[i]->Serialize(movConfig);
	}

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	return true;
}
bool TransducerPositionSet::DeSerialize(MovConfig * movConfig)
{
	bool result = true;
	// debut de la d�s�rialisation du module
	if (result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "TransducerPositionSet"))
	{
		// on supprime la d�finition existante avant d'ajouter les param�tres lus dans la config
		for (unsigned int i = 0; i < m_RecordSet.size(); i++)
		{
			delete m_RecordSet[i];
		}
		m_RecordSet.clear();

		unsigned int nbTrans = 0;
		result = result && movConfig->DeSerializeData<unsigned int>(&nbTrans, eUInt, "NbTransducer");

		for (unsigned int i = 0; i < nbTrans; i++)
		{
			TransducerPositionCompensation * pTransPosComp = new TransducerPositionCompensation("");
			result = result && pTransPosComp->DeSerialize(movConfig);
			m_RecordSet.push_back(pTransPosComp);
		}
		// fin de la d�s�rialisation du module
		movConfig->DeSerializePushBack();
	}

	return result;
}

TransducerPositionCompensation *TransducerPositionSet::GetTransducerPosition(const char *m_Name)
{
	for (unsigned int i = 0; i < m_RecordSet.size(); i++)
	{
		if (m_RecordSet[i]->checkTransName(m_Name))
		{
			return m_RecordSet[i];
		}
	}
	return CreateDefaultTransPosition(m_Name);
}


TransducerPositionCompensation* TransducerPositionSet::CreateDefaultTransPosition(const char *m_Name)
{
	TransducerPositionCompensation *pRet = new TransducerPositionCompensation(m_Name);
	m_RecordSet.push_back(pRet);
	return pRet;
}

unsigned int TransducerPositionSet::GetRecordCount() { return m_RecordSet.size(); }
TransducerPositionCompensation* TransducerPositionSet::GetRecord(unsigned int idx) { if (idx < GetRecordCount()) return m_RecordSet[idx]; else return NULL; }

void TransducerPositionSet::BuildDefaultSettings()
{
	TransducerPositionCompensation *p;

	p = GetTransducerPosition("ES18-11");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(0.54);
	p->m_rollRot = DEG_TO_RAD(-0.21);;
	p->m_internalDelay = -0.29;
	p->m_sampleOffset = -2;


	p = GetTransducerPosition("ES38B");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(0.49);
	p->m_rollRot = DEG_TO_RAD(0.20);
	p->m_internalDelay = -0.17;
	p->m_sampleOffset = -2;


	p = GetTransducerPosition("ES70-7C");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(-0.24);
	p->m_rollRot = DEG_TO_RAD(-0.22);
	p->m_internalDelay = -0.08;
	p->m_sampleOffset = -2;

	/*p=GetTransducerPosition("ES120-7");
	p->m_headingRot=0;
	p->m_pitchRot=DEG_TO_RAD(-0.28);
	p->m_rollRot=DEG_TO_RAD(-0.18);
	p->m_internalDelay=-0.03;
	p->m_sampleOffset=-2;*/

	p = GetTransducerPosition("ES120-7C");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(-0.28);
	p->m_rollRot = DEG_TO_RAD(-0.18);
	p->m_internalDelay = -0.03;
	p->m_sampleOffset = -2;


	p = GetTransducerPosition("ES200-7C");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(-0.60);
	p->m_rollRot = DEG_TO_RAD(0);
	p->m_internalDelay = -0.02;
	p->m_sampleOffset = -2;



	p = GetTransducerPosition("ME70");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(0);
	p->m_rollRot = DEG_TO_RAD(0);
	p->m_internalDelay = -0.11;
	p->m_sampleOffset = 0;


	p = GetTransducerPosition("ES70-11");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(0);
	p->m_rollRot = DEG_TO_RAD(-90);
	p->m_internalDelay = 0;
	p->m_sampleOffset = -2;

	// OTK - FAE069 - positions des transducteurs des tuples 2001
	p = GetTransducerPosition("EK5001");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(0.54);
	p->m_rollRot = DEG_TO_RAD(-0.21);;
	p->m_internalDelay = -0.29;
	p->m_sampleOffset = -2;


	p = GetTransducerPosition("EK5002");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(0.49);
	p->m_rollRot = DEG_TO_RAD(0.20);
	p->m_internalDelay = -0.17;
	p->m_sampleOffset = -2;


	p = GetTransducerPosition("EK5003");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(-0.24);
	p->m_rollRot = DEG_TO_RAD(-0.22);
	p->m_internalDelay = -0.08;
	p->m_sampleOffset = -2;

	p = GetTransducerPosition("EK5004");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(-0.28);
	p->m_rollRot = DEG_TO_RAD(-0.18);
	p->m_internalDelay = -0.03;
	p->m_sampleOffset = -2;


	p = GetTransducerPosition("EK5005");
	p->m_headingRot = 0;
	p->m_pitchRot = DEG_TO_RAD(-0.60);
	p->m_rollRot = DEG_TO_RAD(0);
	p->m_internalDelay = -0.02;
	p->m_sampleOffset = -2;


	/*p=GetTransducerPosition("ES18-11");
	p->m_headingRot=0;
	p->m_pitchRot=DEG_TO_RAD(0);
	p->m_rollRot=DEG_TO_RAD(0);;
	p->m_internalDelay=0;
	p->m_sampleOffset=-2;

	p=GetTransducerPosition("ES38B");
	p->m_headingRot=0;
	p->m_pitchRot=DEG_TO_RAD(0);
	p->m_rollRot=DEG_TO_RAD(0);
	p->m_internalDelay=0;
	p->m_sampleOffset=-2;

	p=GetTransducerPosition("ES70-7C");
	p->m_headingRot=0;
	p->m_pitchRot=DEG_TO_RAD(0);
	p->m_rollRot=DEG_TO_RAD(0);
	p->m_internalDelay=0;
	p->m_sampleOffset=-2;


	p=GetTransducerPosition("ES120-7C");
	p->m_headingRot=0;
	p->m_pitchRot=DEG_TO_RAD(0);
	p->m_rollRot=DEG_TO_RAD(0);
	p->m_internalDelay=0;
	p->m_sampleOffset=0-2;

	TransducerPositionCompensation *p2=GetTransducerPosition("ES120-7");
	p2->CopyData(*p);

	p=GetTransducerPosition("ES200-7C");
	p->m_headingRot=0;
	p->m_pitchRot=DEG_TO_RAD(0);
	p->m_rollRot=DEG_TO_RAD(0);
	p->m_internalDelay=0;
	p->m_sampleOffset=-2;

	p=GetTransducerPosition("TransName");
	p->m_headingRot=0;
	p->m_pitchRot=DEG_TO_RAD(0);
	p->m_rollRot=DEG_TO_RAD(0);
	p->m_internalDelay=0;
	p->m_sampleOffset=0;

	p2=GetTransducerPosition("ME70");
	p2->CopyData(*p);*/

}
