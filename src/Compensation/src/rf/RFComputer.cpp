// -*- C++ -*-
// ****************************************************************************
// Class: RFComputer
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Juillet 2009
// Soci�t� : IPSIS
// ****************************************************************************

#include "Compensation/rf/RFComputer.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/MemoryStruct.h"
#include "M3DKernel/datascheme/PingFanContainer.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/utils/multithread/Synchronized.h"

#include "Compensation/CompensationModule.h"
#include "Calibration/CalibrationModule.h"

#include "ShoalExtraction/ShoalExtractionModule.h"
#include "ShoalExtraction/data/channeldata.h"
#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/shoalstat.h"
#include "ShoalExtraction/data/pingdata.h"

using namespace BaseMathLib;
using namespace shoalextraction;

#ifdef _TRACE_PERF
std::ofstream trace_ofs("d:/tmp/rfcomputer/state.txt");
#endif

namespace
{
	constexpr const char * LoggerName = "Compensation.RFComputer";
}

RFComputer::RFComputer(void)
{
	m_pShoalExtractModule = NULL;
	m_pShoalExtractModuleFiltered = NULL;
	m_pCalibrationModule = NULL;
}

RFComputer::~RFComputer(void)
{
	Reset();

#ifdef _TRACE_PERF
	trace_ofs.close();
#endif
}

//*****************************************************************************
// Name : Reset
// Description : reset the storage
// Parameters : (in)	RFParameter& param
// Return : void
//*****************************************************************************
void RFComputer::Reset()
{
	std::map<std::int32_t, RFResults*>::iterator iterResult = m_results.begin();
	while (iterResult != m_results.end())
	{
		FreeResults(iterResult->second);
		delete iterResult->second;

		iterResult++;
	};
	m_results.clear();
}

//*****************************************************************************
// Name : FreeResults
// Description : free the result map
// Parameters : (in)	RFParameter& param
// Return : void
//*****************************************************************************
void RFComputer::FreeResults(RFResults* pResults)
{
	auto iterResult = pResults->filtered.begin();
	while (iterResult != pResults->filtered.end())
	{
		for (auto& val : *iterResult->second) {
			delete val;
		}
		iterResult++;
	};

	iterResult = pResults->unfiltered.begin();
	while (iterResult != pResults->unfiltered.end())
	{
		for (auto& val : *iterResult->second) {
			delete val;
		}
		iterResult++;
	};
}

//*****************************************************************************
// Name : HasResults
// Description : 
// Parameters : void
// Return : true if valid resuts
//*****************************************************************************
bool RFComputer::HasResults()
{
	bool result = false;

	//get the result for each input
	std::map<std::int32_t, RFResults*>::iterator iterResult = GetResults().begin();
	while (iterResult != GetResults().end() && !result)
	{
		RFResults* pResults = iterResult->second;

		result |= !pResults->unfiltered.empty() || !pResults->filtered.empty();

		iterResult++;
	}

	return result;
}

//*****************************************************************************
// Name : GetRefEnergy
// Description : get the energy of the rf result of the reference tranducer for the given shoalId
// Parameters : (in)	std::int32_t shoalId
//				(in)	bool filtered
// Return : double
//*****************************************************************************
double RFComputer::GetRefEnergy(std::int32_t shoalId, bool filtered)
{
	double refEnergy = 0.0;

	RFResult* pRefResult = NULL;
	if (m_isMultiBeam)
	{
		RFResults* pResult = GetResults()[shoalId];
		RFResultMap& map = (filtered) ? pResult->filtered : pResult->unfiltered;
		RFResultMap::iterator iterResult = map.begin();
		
		auto results = iterResult->second;
		auto idx = (int)std::floor((results->size() / 2.0));
		if(idx >= 0 && idx < results->size())
			pRefResult = results->at(idx);
	}
	else
	{
		//search the reference transducer
		std::vector<RFResult*> results = GetResult(shoalId, m_localParameter.GetRefTransducer(), filtered);
		pRefResult = (results.size() > 0) ? results[0] : NULL;
	}


	if (pRefResult != NULL)
	{
		bool useSV = m_localParameter.GetUseSV();
		//test de l'utilisation de SV (� convertir en lin�aire) au lieu de SA			
		refEnergy = useSV ? pow(10.0, pRefResult->GetEnergySV() / 10.0) : pRefResult->GetEnergySA();
	}

	return refEnergy;
}

//*****************************************************************************
// Name : DumpResults
// Description : dump the result map
// Parameters : (in)	std::ofstream& outStream
// Return : void
//*****************************************************************************
void RFComputer::DumpResults(std::ofstream& outStream)
{
	outStream << "\nRFComputer::Evalutate" << std::endl;
	std::map<std::int32_t, RFResults*>::iterator iterResults = m_results.begin();
	while (iterResults != m_results.end())
	{
		outStream << std::endl << "filtered " << std::endl;
		auto iterResult = iterResults->second->filtered.begin();
		while (iterResult != iterResults->second->filtered.end())
		{
			for (auto channel : *iterResult->second) {
				outStream << "Transducer " << iterResult->first.c_str() << "\t";
				outStream << "Freq = " << channel->GetFrequency() << "\t";
				outStream << "SA = " << channel->GetEnergySA() << "\t";
				outStream << "SV = " << channel->GetEnergySV() << "\t";
				outStream << "RF = " << channel->GetRFRatio() << std::endl;
			}
			iterResult++;
		};

		outStream << std::endl << "unfiltered " << std::endl;
		iterResult = iterResults->second->unfiltered.begin();
		while (iterResult != iterResults->second->unfiltered.end())
		{
			for (auto channel : *iterResult->second) {
				outStream << "Transducer " << iterResult->first.c_str() << "\t";
				outStream << "Freq = " << channel->GetFrequency() << "\t";
				outStream << "SA = " << channel->GetEnergySA() << "\t";
				outStream << "SV = " << channel->GetEnergySV() << "\t";
				outStream << "RF = " << channel->GetRFRatio() << std::endl;
			}
			iterResult++;
		};
		iterResults++;
	};

	outStream.flush();
}

//*****************************************************************************
// Name : Evalutate
// Description : Evaluate data for the given parameter
// Parameters : (in)	RFParameter& param
// Return : void
//*****************************************************************************
void RFComputer::Evalutate(RFParameter& param, PROGRESS_CALLBACK progress,
	CANCEL_CALLBACK cancel, PingFanConsumer * pfconsumer)
{
	m_isMultiBeam = param.GetIsMultiBeam();
	if (m_isMultiBeam) {
		progress("Computing r(a)...", -1);
	}
	else {
		progress("Computing r(f)...", -1);
	}

	m_localParameter = param;

	//compute energy values
	ComputeEnergy(progress, cancel, pfconsumer);

	if (!cancel())
	{
		//compute RF
		ComputeRF();

		if (m_isMultiBeam) {
			M3D_LOG_INFO(LoggerName, "Evalutate Angular Response:: Completed");
		}
		else {
			M3D_LOG_INFO(LoggerName, "Evalutate Frequency Response:: Completed");
		}

#ifdef _TRACE_PERF
		DumpResults(trace_ofs);
#endif
	}
}

// ****************************************************************************
// Methods
// ****************************************************************************

//*****************************************************************************
// Name : GetResult
// Description : retrieve the results for the input
// Parameters : (in)	std::int32_t shoalId (<= 0 --> window)
// Return : RFResults*
//*****************************************************************************
RFResults* RFComputer::GetResults(std::int32_t shoalId)
{
	//get the list of results for the input
	RFResults* results = NULL;

	std::map<std::int32_t, RFResults*>::iterator iterResult = GetResults().find(shoalId);
	if (iterResult != GetResults().end())
	{
		results = iterResult->second;
	}
	else
	{
		results = new RFResults();
		GetResults()[shoalId] = results;
	}

	return results;
}

//*****************************************************************************
// Name : GetResult
// Description : retrieve the result for the input and the transducer
// Parameters : (in)	std::int32_t shoalId (<= 0 --> window)
//				(in)	const std::string & transducerName
//				(in)	bool filtered
// Return : RFResult*
//*****************************************************************************
std::vector<RFResult*> RFComputer::GetResult(std::int32_t shoalId, const std::string & transducerName, bool filtered)
{
	std::vector<RFResult*> result;

	//get the list of results for the input
	RFResults* results = GetResults(shoalId);

	//get the result for the transducer
	auto& map = (filtered) ? results->filtered : results->unfiltered;
	auto iterResult = map.find(transducerName);

	if (iterResult != map.end())
	{
		result = *(*iterResult).second;
	}

	return result;
}

//*****************************************************************************
// Name : SetResult
// Description : set the result at the specified index for the input and the transducer
// Parameters : (in)	std::int32_t shoalId (<= 0 --> window)
//				(in)	const std::string & transducerName
//				(in)	bool filtered
//				(in)	int index: used when multiple results are associated to a transducer, put 0 if that's not the case
//				(in)	RFResult* result
//*****************************************************************************
void RFComputer::SetResult(std::int32_t shoalId, const std::string & transducerName, bool filtered, int index, RFResult* result)
{
	//get the list of results for the input
	RFResults* results = GetResults(shoalId);

	//get the result for the transducer
	auto& map = (filtered) ? results->filtered : results->unfiltered;

	if (map[transducerName] == nullptr) {
		map[transducerName] = new std::vector<RFResult*>();
	}

	auto resultVector = map[transducerName];

	while (index >= resultVector->size()) {
		resultVector->push_back(nullptr);
	}

	if ((*resultVector)[index] != nullptr) {
		delete (*resultVector)[index];
	}

	(*resultVector)[index] = result;	
}

//*****************************************************************************
// Name : ComputeEnergy
// Description : Generate the energy results for all mono beams
// Parameters : void
// Return : void
//*****************************************************************************
void RFComputer::ComputeEnergy(PROGRESS_CALLBACK progress, CANCEL_CALLBACK cancel,
	PingFanConsumer * pfconsumer)
{
	//use shoal data for input ?
	bool useShoal = (m_localParameter.GetShoals().size() > 0);

	if (!cancel())
	{
		//retrieve the list of transducers
		std::set<std::string> transducerList;



		//for each one of the 10 first ping, get the transducer name

	//m_pingFanIterator.Init(false, 0, 10);
	// NMD - FAE 127, si on se base uniquement sur le 10ers pings on ne recupere pas forcement tout les transducteurs...
		m_pingFanIterator.Init(true, m_localParameter.GetPingFilteredStart(), m_localParameter.GetPingFilteredStop());

		while (!m_pingFanIterator.End())
		{
			PingFan* pingFan = *m_pingFanIterator;

			if (pingFan != NULL)
			{
				Sounder* pSounder = pingFan->getSounderRef();

				//search the transducers
				for (size_t j = 0; j < pSounder->GetTransducerCount(); j++)
				{
					Transducer *pTrans = pSounder->GetTransducer(j);
					transducerList.insert(pTrans->m_transName);
				}
			}
			++m_pingFanIterator;
		}

		if (useShoal)
		{
			size_t nbShoal = m_localParameter.GetShoals().size();

			for (size_t i = 0; i < nbShoal && !cancel(); i++)
			{
				//get the input shoal
				ShoalData* pShoal = m_localParameter.GetShoals()[i];
				//evaluate eshoal
				EvalutateShoal(transducerList, pShoal, cancel);

				progress("Computing r(f)...", (int)(60.0 + 40.0*((double)i / nbShoal)));
			};
		}
		else
		{
			EvalutateWindow(transducerList);
		}
	}

	//clean data used for shoalextraction
	if (m_pShoalExtractModule != NULL)
	{
		DeleteShoalExtractionModule(&m_pShoalExtractModule);
	}
	if (m_pShoalExtractModuleFiltered != NULL)
	{
		DeleteShoalExtractionModule(&m_pShoalExtractModuleFiltered);
	}
}

//*****************************************************************************
// Name : IsMultiBeam
// Description : returns true if the transducer is multibeam
// Parameters : (in)	const std::string& transducerName
// Return : bool
//*****************************************************************************
bool RFComputer::IsMultiBeam(const std::string& transducerName)
{
	bool result = false;
	bool found = false;

	//for each ping
	m_pingFanIterator.Begin();
	while (!m_pingFanIterator.End() && !found)
	{
		PingFan* pingFan = *m_pingFanIterator;
		if (pingFan != NULL)
		{
			Sounder* pSounder = pingFan->getSounderRef();

			//search the transducer
			for (size_t j = 0; j < pSounder->GetTransducerCount() && !found; j++)
			{
				Transducer *pTrans = pSounder->GetTransducer(j);
				if (pTrans->m_transName == transducerName)
				{
					result = pSounder->m_isMultiBeam;
					found = true;
				}
			}
		}
		++m_pingFanIterator;
	}
	return result;
}

//*****************************************************************************
// Name : EvalutateWindow
// Description : Evaluate the results in the window
// Parameters : (in)	const std::set<std::string>& transducerList
// Return : void
//*****************************************************************************
void RFComputer::EvalutateWindow(const std::set<std::string>& transducerList)
{
	std::set<std::string>::const_iterator iterTrans = transducerList.begin();
	while (iterTrans != transducerList.end())
	{
		if (m_localParameter.GetComputeFiltered())
		{
			EvalutateWindowTransducer(*iterTrans, true);
		}

		if (m_localParameter.GetComputeUnfiltered())
		{
			EvalutateWindowTransducer(*iterTrans, false);
		}

		iterTrans++;
	}
}

//*****************************************************************************
// Name : EvalutateWindowTransducer
// Description : Evaluate the results in the window
// Parameters : (in)	const std::string& transducerName
//				(in)	bool useOverLap
// Return : void
//*****************************************************************************
void RFComputer::EvalutateWindowTransducer(const std::string& transducerName, bool useOverLap)
{
	auto isMultiBeamTransducer = IsMultiBeam(transducerName);
	if (isMultiBeamTransducer && m_isMultiBeam) {
		Transducer *pTrans = nullptr;
		auto channelCount = 0;
		//for each ping
		m_pingFanIterator.Begin();
		while (!m_pingFanIterator.End())
		{
			PingFan* pingFan = *m_pingFanIterator;
			if (pingFan != NULL)
			{
				Sounder* pSounder = pingFan->getSounderRef();

				//search the transducer
				for (size_t j = 0; j < pSounder->GetTransducerCount(); j++)
				{
					Transducer *pTrans = pSounder->GetTransducer(j);
					if (pTrans->m_transName == transducerName)
					{
						MemoryStruct *pPolarMem = pingFan->GetMemorySetRef()->GetMemoryStruct(j);
						channelCount = pPolarMem->GetDataFmt()->getSize().x;
						break;
					}
				}
				if (pTrans == nullptr) {
					++m_pingFanIterator;
				}
				else {
					break;
				}
			}

		}
		EvalutateWindowComponent(transducerName, useOverLap, channelCount);
	}
	else if (!isMultiBeamTransducer && !m_isMultiBeam) {
		EvalutateWindowComponent(transducerName, useOverLap);
	}
}

void RFComputer::EvalutateWindowComponent(const std::string & transducerName, bool useOverLap, int beamCount)
{
	std::vector<EnergyData> energyDataList;
	double depthMin = m_localParameter.GetDepthMin();
	double depthMax = m_localParameter.GetDepthMax();

	DataFmt threshold = 0;

	//for multi beam transducers, no overLap can be processed, so filtered == unfiltered
	bool tmpUseOverlap = useOverLap && beamCount == 1;

	if (tmpUseOverlap)
	{
		threshold = m_localParameter.GetOverlapThreshold();
	}
	else
	{
		threshold = m_localParameter.GetDataThreshold();
	}


	m_pingFanIterator.Init(true, m_localParameter.GetPingFilteredStart(), m_localParameter.GetPingFilteredStop());
	bool weightedEchoIntegration = M3DKernel::GetInstance()->GetRefKernelParameter().getWeightedEchoIntegration();
	static int angle = 0;
	SoftChannel *pChannel = nullptr;
	while (!m_pingFanIterator.End())
	{
		PingFan* pingFan = *m_pingFanIterator;
		if (pingFan != nullptr)
		{
			
			Transducer *pTrans = nullptr;
			int transducerIndex = 0;

			Sounder* sounder = pingFan->getSounderRef();
			for (size_t j = 0; j < sounder->GetTransducerCount(); j++)
			{
				if (sounder->GetTransducer(j)->m_transName == transducerName)
				{
					pTrans = sounder->GetTransducer(j);
					transducerIndex = j;
					break;
				}
			}
		
			if (pTrans)
			{
				pChannel = pTrans->getSoftChannelPolarX(0);

				if (pingFan->getSpectralAnalysisDataObject(pChannel->m_softChannelId) != nullptr)
				{
					updateEnergyDataForPingWithSpectralAnalysis(pingFan, pTrans, pChannel, threshold, depthMin, depthMax, weightedEchoIntegration, energyDataList);
				}
				else
				{
					energyDataList.resize(beamCount);
					for (auto beamIdx = 0; beamIdx < beamCount; beamIdx++) 
					{
						pChannel = pTrans->getSoftChannelPolarX(beamIdx);

						auto beamsSamplesSpacing = pTrans->getBeamsSamplesSpacing();
													
						EnergyData & energyData = energyDataList[beamIdx];
						energyData.weightedEchoIntegration = weightedEchoIntegration;
						energyData.channelAngle = pChannel->m_mainBeamAthwartSteeringAngleRad;
						energyData.frequency = pChannel->m_acousticFrequency;
												
						energyData.nbPings++;

						updateEnergyDataForPing(pingFan, transducerIndex, pChannel, beamsSamplesSpacing, tmpUseOverlap, threshold, depthMin, depthMax, energyData, beamIdx);
					}
				}
			}
		}

		++m_pingFanIterator;
	}//for each ping

	int index = 0;
	for (auto data : energyDataList)
	{
		//get the result for the transducer
		RFResult* pResult = new RFResult();

		pResult->SetFrequency((float)data.frequency);
		pResult->SetAngle(std::round(data.channelAngle / (2 * PI) * 360));
		pResult->SetEchoesNb(data.nbEcho);

		float sv = 0;
		float sa = 0;
		computeEnergies(data, sv, sa);

		if (sa > 0)
		{
			pResult->SetEnergySV(sv);
			pResult->SetEnergySA(sa);

			SetResult(WINDOW_SHOALID, transducerName, useOverLap, index, pResult);

			++index;
		}
	}
}

void RFComputer::updateEnergyDataForPing(PingFan* pingFan, int transducerIndex, SoftChannel* channel, double beamsSamplesSpacing, bool useOverlap, DataFmt threshold, double depthMin, double depthMax, EnergyData& energyData, int iBeam)
{
	Sounder* pSounder = pingFan->getSounderRef();

	double pingDistance = pingFan->getPingDistance();
	energyData.distanceTravelled += pingDistance;
	MemorySet *pMemSet = pingFan->GetMemorySetRef();
	MemoryStruct *pMem = pMemSet->GetMemoryStruct(transducerIndex);
	BaseMathLib::Vector2I	size = pMem->GetDataFmt()->getSize();

	// R�cup�ration (lock de la memoire)
	pMem->GetDataFmt()->Lock();
	if (useOverlap)
	{
		pMem->GetOverLap()->Lock();
	}

	// R�cup�ration donn�es �cho du faisceau unique
	DataFmt * pEchoStart = pMem->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(iBeam, 0));
	DataFmt * pOverLapStart = NULL;

	//si on utilise le filtrage
	if (useOverlap)
	{
		pOverLapStart = pMem->GetOverLap()->GetPointerToVoxel(BaseMathLib::Vector2I(iBeam, 0));
	}

	DataFmt value;
	for (int iEcho = 0; iEcho < size.y; iEcho++)
	{
		//si on utilise le filtrage
		if (useOverlap)
		{
			//utilisation de la valeur du recouvrement
			value = *(pOverLapStart + iEcho);
		}
		else
		{
			//utilisation de la valeur de l echo en DB
			value = *(pEchoStart + iEcho);
		}

		//on ne consid�re que les donn�es dont le recouvrement est sup�rieur au seuil
		if (threshold <= value && !(*(pMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(iBeam, iEcho)))))
		{
			//utilisation de la valeur de l echo en DB
			value = *(pEchoStart + iEcho);
			BaseMathLib::Vector3D echoPos = pSounder->GetPolarToGeoCoord(pingFan, transducerIndex, iBeam, iEcho);
			if (echoPos.z >= depthMin && echoPos.z <= depthMax)
			{
				updateEnergyDataForEcho(value, beamsSamplesSpacing, pingDistance, energyData);
			}
		}
	}

	// R�cup�ration (unlock de la memoire)
	pMem->GetDataFmt()->Unlock();
	if (useOverlap)
	{
		pMem->GetOverLap()->Unlock();
	}
}

void RFComputer::updateEnergyDataForPingWithSpectralAnalysis(PingFan* pingFan, Transducer* transducer, SoftChannel* channel, DataFmt threshold, double depthMin, double depthMax, bool weightedEchoIntegration, std::vector<EnergyData>& energyDataList)
{
	auto spectralAnalysisData = pingFan->getSpectralAnalysisDataObject(channel->m_softChannelId);
	if (spectralAnalysisData == nullptr)
		return;

	double pingDistance = pingFan->getPingDistance();
	auto nbRanges = spectralAnalysisData->numberOfRanges();
	auto nbFreq = spectralAnalysisData->numberOfFrequencies();

	energyDataList.resize(nbFreq);

	int rangeIndex = 0;
	for (auto frequencyValues : m_pCalibrationModule->getSvValuesWithGain(spectralAnalysisData, channel))
	{
		int freqIndex = 0;
		for (auto value : frequencyValues)
		{
			EnergyData& energyData = energyDataList[freqIndex];
			
			if (rangeIndex == 0)
			{
				energyData.distanceTravelled += pingDistance;
				energyData.nbPings++;
				energyData.weightedEchoIntegration = weightedEchoIntegration;

				if (energyData.frequency < 0)
					energyData.frequency = spectralAnalysisData->frequencyForIndex(freqIndex);
			}
			
			value *= 100.0;

			//on ne consid�re que les donn�es dont le recouvrement est sup�rieur au seuil
			if (threshold <= value)
			{
				//utilisation de la valeur de l echo en DB
				auto depth = spectralAnalysisData->rangeForIndex(rangeIndex);
				if (depth >= depthMin && depth <= depthMax)
				{
					updateEnergyDataForEcho(value, spectralAnalysisData->rangeInterval, pingDistance, energyData);
				}
			}

			++freqIndex;
		}

		++rangeIndex;
	}
}

void RFComputer::updateEnergyDataForEcho(DataFmt value, double beamsSamplesSpacing, double pingDistance, EnergyData& data)
{
	//calcul de l'�nergie de l'echo en lin�aire
	double echoEnergy = pow(10.0, value / 1000.0);

	//MaJ des stat
	if (data.weightedEchoIntegration)
	{
		data.energy += echoEnergy*beamsSamplesSpacing*pingDistance;
	}
	else
	{
		data.energy += echoEnergy;
		data.pondEnergy += echoEnergy*beamsSamplesSpacing;
	}

	data.sumHeights += beamsSamplesSpacing;
	++data.nbEcho;
}

void RFComputer::computeEnergies(const EnergyData& data, float& energySV, float& energySA)
{
	if (data.nbEcho > 0)
	{
		double Sv = 0.0;
		if (data.weightedEchoIntegration)
		{
			if (data.nbPings > 0)
			{
				double area = data.distanceTravelled*(data.sumHeights / (double)data.nbPings);
				if (area > 0)
				{
					Sv = 10.0 * log10(data.energy / area);
				}
			}
		}
		else
		{
			Sv = 10.0 * log10(data.energy / (double)data.nbEcho);
		}
		energySV = (float)Sv;
	}

	// sA (NASC) 
	if (data.nbPings > 0)
	{
		double sA = 0.0;
		if (data.weightedEchoIntegration)
		{
			if (data.distanceTravelled > 0.0)
			{
				sA = 1852.*1852.*4.*PI*data.energy / data.distanceTravelled;
			}
		}
		else
		{
			sA = 1852.*1852.*4.*PI*data.pondEnergy / data.nbPings;
		}

		energySA = (float)sA;
	}
}

//*****************************************************************************
// Name : CreateShoalExtractionModule
// Description : create a shoal extraction module
// Parameters : 
// Return : void
//*****************************************************************************
ShoalExtractionModule * RFComputer::CreateShoalExtractionModule()
{
	ShoalExtractionModule * pModule = NULL;

	pModule = ShoalExtractionModule::Create();
	MovRef(pModule);
	pModule->setEnable(true);

	return pModule;
}

//*****************************************************************************
// Name : DeleteShoalExtractionModule
// Description : delete a shoal extraction module
// Parameters : 
// Return : void
//*****************************************************************************
void RFComputer::DeleteShoalExtractionModule(ShoalExtractionModule ** pModule)
{
	if (*pModule != NULL)
	{
		MovUnRefDelete((*pModule));
		pModule = NULL;
	}
}

//*****************************************************************************
// Name : ComputeShoalExtraction
// Description : compute a shoal extraction on overlapped data
// Parameters : 
// Return : void
//*****************************************************************************
void RFComputer::ComputeShoalExtraction(CANCEL_CALLBACK cancel)
{
	if (m_localParameter.GetComputeUnfiltered())
	{
		//create a ShoalExtraction module
		DeleteShoalExtractionModule(&m_pShoalExtractModule);
		m_pShoalExtractModule = CreateShoalExtractionModule();

		//parameterize the extraction to take account only data value and make internal storage
		ShoalExtractionParameter& paramNoFilter = m_pShoalExtractModule->GetShoalExtractionParameter();
		paramNoFilter.SetUseOverlapFilter(false);
		// OTK - FAE213 - on garde finalement les seuils courants de l'extraction des bancs
		//paramNoFilter.SetThreshold((float)(m_localParameter.GetDataThreshold() / 100.0));
		paramNoFilter.SetUseOnlyCentralBeam(true);
		paramNoFilter.SetUseInternalShoalStorage(true);
		paramNoFilter.SetMinDepth(m_localParameter.GetDepthMin());
		paramNoFilter.SetMaxDepth(m_localParameter.GetDepthMax());
		paramNoFilter.SetMinHeight(0);
		paramNoFilter.SetMinLength(0);
		paramNoFilter.SetMinWidth(0);
		paramNoFilter.SetAlongIntegrationDistance(m_localParameter.GetAlongIntegrationDistance());
		paramNoFilter.SetAcrossIntegrationDistance(m_localParameter.GetAcrossIntegrationDistance());
		paramNoFilter.SetVerticalIntegrationDistance(m_localParameter.GetVerticalIntegrationDistance());
	}

	if (m_localParameter.GetComputeFiltered())
	{
		//create a ShoalExtraction module for Filtered Data
		DeleteShoalExtractionModule(&m_pShoalExtractModuleFiltered);
		m_pShoalExtractModuleFiltered = CreateShoalExtractionModule();

		//parameterize the extraction to take account on data overlap and make internal storage
		ShoalExtractionParameter& paramFilter = m_pShoalExtractModuleFiltered->GetShoalExtractionParameter();
		paramFilter.SetUseOverlapFilter(true);
		// OTK - FAE213 - on garde finalement les seuils courants de l'extraction des bancs
		//paramFilter.SetThreshold((float)(m_localParameter.GetOverlapThreshold() / 100.0));
		paramFilter.SetUseOnlyCentralBeam(true);
		paramFilter.SetUseInternalShoalStorage(true);
		paramFilter.SetMinDepth(m_localParameter.GetDepthMin());
		paramFilter.SetMaxDepth(m_localParameter.GetDepthMax());
		paramFilter.SetMinHeight(0);
		paramFilter.SetMinLength(0);
		paramFilter.SetMinWidth(0);
		paramFilter.SetAlongIntegrationDistance(m_localParameter.GetAlongIntegrationDistance());
		paramFilter.SetAcrossIntegrationDistance(m_localParameter.GetAcrossIntegrationDistance());
		paramFilter.SetVerticalIntegrationDistance(m_localParameter.GetVerticalIntegrationDistance());
	}

	//add every ping from the input shoal ping range
	//retrieve the number of transducers
	if (!cancel())
	{
		//for each ping
		m_pingFanIterator.Init(true, m_localParameter.GetPingFilteredStart(), m_localParameter.GetPingFilteredStop());
		while (!m_pingFanIterator.End() && !cancel())
		{
			PingFan* pingFan = *m_pingFanIterator;
			if (pingFan != NULL)
			{
				if (m_localParameter.GetComputeFiltered())
				{
					m_pShoalExtractModuleFiltered->PingFanAdded(pingFan);
				}
				if (m_localParameter.GetComputeUnfiltered())
				{
					m_pShoalExtractModule->PingFanAdded(pingFan);
				}
			}
			++m_pingFanIterator;
		}

		if (!cancel())
		{
			if (m_localParameter.GetComputeFiltered())
			{
				//force closing shoals
				m_pShoalExtractModuleFiltered->ForceCloseShoals();
			}

			if (m_localParameter.GetComputeUnfiltered())
			{
				m_pShoalExtractModule->ForceCloseShoals();
			}
		}
	}
}

//*****************************************************************************
// Name : EvalutateShoal
// Description : Evaluate the results in the shoal
// Parameters : (in)	const std::set<std::string>& transducerList
//				(in)	ShoalData* pShoalData
// Return : void
//*****************************************************************************
void RFComputer::EvalutateShoal(const std::set<std::string>& transducerList,
	ShoalData* pShoal,
	CANCEL_CALLBACK cancel)
{
	size_t nbPings = pShoal->GetPings().size();
	if (nbPings > 0)
	{
		//parameterize shoal extraction
		int pingMin = pShoal->GetPings()[0]->GetPingId();
		int pingMax = pShoal->GetPings()[nbPings - 1]->GetPingId();
		double depthMin = pShoal->GetShoalStat()->GetMinDepth();
		double depthMax = pShoal->GetShoalStat()->GetMaxDepth();

		m_localParameter.SetPingFilteredStart(pingMin);
		m_localParameter.SetPingFilteredStop(pingMax);
		m_localParameter.SetDepthMin((float)depthMin);
		m_localParameter.SetDepthMax((float)depthMax);

		//compute shoal extraction
		ComputeShoalExtraction(cancel);

		std::set<std::string>::const_iterator iterTrans = transducerList.begin();
		while (iterTrans != transducerList.end())
		{
			if (m_localParameter.GetComputeFiltered())
			{
				//evaluate shoal on each transducer
				EvalutateShoalTransducer(*iterTrans, pShoal, true);
			}

			if (m_localParameter.GetComputeUnfiltered())
			{
				EvalutateShoalTransducer(*iterTrans, pShoal, false);
			}
			iterTrans++;
		}
	}
}

//*****************************************************************************
// Name : EvalutateShoalTransducer
// Description : Evaluate the frequency response of the data in the shoal
// Parameters : (in)	int iTrans
//				(in)	ShoalData* pShoalData
//				(in)	bool useOverLap
// Return : void
//*****************************************************************************
void RFComputer::EvalutateShoalTransducer(const std::string & transducerName, ShoalData* pShoalData, bool useOverLap)
{
	//for multibeam transducers, there is no filtered data ... 
	//As the filter Reference is the multibeam transducer, we use the unfiltered data
	bool tmpOverLap = useOverLap && !IsMultiBeam(transducerName);

	//get the extracted shoals
	ShoalExtractionModule *pShoalExtractModule = (tmpOverLap) ? m_pShoalExtractModuleFiltered : m_pShoalExtractModule;

	if (pShoalExtractModule != NULL)
	{
		std::vector<ShoalData*>& shoalList = pShoalExtractModule->GetShoals();

		//match extracted shoals to the input one
		ShoalData* pMatchedShoal = MatchShoals(pShoalData, shoalList, transducerName);

		if (pMatchedShoal != NULL)
		{
			//create RFResult
			std::uint32_t shoalId = pShoalData->GetExternId();
			RFResult *pResult = new RFResult();

			char buffer[20];
            sprintf(buffer, "%u");
			pResult->SetShoalName(buffer);

			pResult->SetEchoesNb(pMatchedShoal->GetShoalStat()->GetNbEchos());

			// MVBS
			pResult->SetEnergySV((float)pMatchedShoal->GetShoalStat()->GetMeanSv());

			// sigma_ag (comme on normalise par le sigma_ag du transducteur de r�f�rence, 
	  // le rapport des sigma_ag est identique au rapport des sA (NASC)
			pResult->SetEnergySA((float)pMatchedShoal->GetShoalStat()->GetSigmaAg());

			double frequency = pMatchedShoal->GetPings()[0]->GetTransducers()[0]->GetChannels()[0]->GetAcousticFrequency();
			pResult->SetFrequency((float)frequency);

			SetResult(shoalId, transducerName, useOverLap, 0, pResult);
		}
	}
}

//*****************************************************************************
// Name : MatchShoals
// Description : Search the matching shoal in the list for thegiven transducer
// Parameters : (in)	const ShoalData* shoalRef
//				(in)	const std::vector<shoalextraction::ShoalData*>& shoalList
//				(in)	const std::string & transducerName
// Return : ShoalData*
//*****************************************************************************
ShoalData* RFComputer::MatchShoals(const ShoalData* shoalRef,
	const std::vector<ShoalData*>& shoalList,
	const std::string & transducerName)
{
	ShoalData* result = NULL;

	std::vector<ShoalData*>::const_iterator iterShoal = shoalList.begin();

	while (iterShoal != shoalList.end() && result == NULL)
	{
		ShoalData* pShoal = *iterShoal;

		//shoal matches if transducer names are equal
		bool match = transducerName == pShoal->GetTransName();

		if (match)
		{
			const BoundingBox3D& refBox = shoalRef->GetBbox();
			BoundingBox3D& testBox = pShoal->GetBbox();

			//shoal matches if :
			// X,Y center value < distance precision
			double distX = (testBox.GetXMax() + testBox.GetXMin() - refBox.GetXMax() - refBox.GetXMin()) * 0.5;
			double distY = (testBox.GetYMax() + testBox.GetYMin() - refBox.GetYMax() - refBox.GetYMin()) * 0.5;
			double distZ = (testBox.GetZMax() + testBox.GetZMin() - refBox.GetZMax() - refBox.GetZMin()) * 0.5;

			//conversion nm --> m
			match &= sqrt(distX*distX + distY*distY) < m_localParameter.GetPrecisionLog() * 1852;

			// depth value < depth precision
			match &= fabs(distZ) < m_localParameter.GetPrecisionDepth();

			//nb of echoes in the shoal > min nb
			match &= pShoal->GetShoalStat()->GetNbEchos() >= m_localParameter.GetMinEchoesNb();

			if (match)
			{
				result = pShoal;
			}
		}

		iterShoal++;
	}

	return result;
}

//*****************************************************************************
// Name : ComputeRF
// Description : compute the rf results
// Parameters : void
// Return : void
//*****************************************************************************
void RFComputer::ComputeRF()
{
	//for each input
	std::map<std::int32_t, RFResults*>::const_iterator iterRes = GetResults().begin();
	while (iterRes != GetResults().end())
	{
		if (m_localParameter.GetComputeFiltered())
		{
			ComputeRF(iterRes->first, true);
		}
		if (m_localParameter.GetComputeUnfiltered())
		{
			ComputeRF(iterRes->first, false);
		}
		ComputeTS(iterRes->first);
		iterRes++;
	}
}

//*****************************************************************************
// Name : ComputeRF
// Description : compute the rf results for the given input and the filter status
// Parameters : (in)	std::int32_t shoalId
//				(in)	bool filtered
// Return : void
//*****************************************************************************
void RFComputer::ComputeRF(std::int32_t shoalId, bool filtered)
{
	double refEnergy = GetRefEnergy(shoalId, filtered);
	if (refEnergy == 0)
	{
		refEnergy = 1.0;
	}

	bool useSV = m_localParameter.GetUseSV();
	RFResults* pResult = GetResults()[shoalId];
	RFResultMap& map = (filtered) ? pResult->filtered : pResult->unfiltered;
	RFResultMap::iterator iterResult = map.begin();

	while (iterResult != map.end())
	{
		for (auto pResult : *(iterResult->second))
		{
			double energy = useSV ? pow(10.0, pResult->GetEnergySV() / 10.0) : pResult->GetEnergySA();
				
			//compute the energy ratio to the reference transducer 
			pResult->SetRFRatio((float)(energy / refEnergy));
				
		}

		iterResult++;
	}
}


void RFComputer::ComputeTS(long shoalId)
{
	const double depthMin = m_localParameter.GetDepthMin();
	const double depthMax = m_localParameter.GetDepthMax();
	const unsigned int pingFilteredStart = m_localParameter.GetPingFilteredStart();
	const unsigned int pingFilteredStop = m_localParameter.GetPingFilteredStop();

	M3D_LOG_INFO(LoggerName, Log::format("Compute Mean TS, depth range [%f - %f], ping range [%d - %d]", depthMin, depthMax, pingFilteredStart, pingFilteredStop));

	struct TSPoint
	{
		double freq;
		double compensatedTSSum;
		double uncompensatedTSSum;
		int nbPoint;

		TSPoint()
			: compensatedTSSum(0.0)
			, uncompensatedTSSum(0.0)
			, nbPoint(0)
		{}
	};

	std::map<double, TSPoint> tsPoints;
	
	auto pKernel = M3DKernel::GetInstance();
	auto pSingleTargetContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetSingleTargetContainer();

	m_pingFanIterator.Init(true, pingFilteredStart, pingFilteredStop);
	for ( ; !m_pingFanIterator.End(); ++m_pingFanIterator)
	{
		auto pingFan = *m_pingFanIterator;
		if (pingFan == nullptr)
			continue;

		if (m_isMultiBeam != pingFan->getSounderRef()->m_isMultiBeam)
			continue;

		auto *pSingle = (PingFanSingleTarget*)pSingleTargetContainer->GetObjectWithDate(pingFan->m_ObjectTime);
		if (pSingle == nullptr)
			continue;

		for (unsigned int targetIdx = 0; targetIdx < pSingle->GetNumberOfTarget(); ++targetIdx)
		{
			auto pSingleTargetData = pSingle->GetTarget(targetIdx);
			auto transducer = pingFan->getSounderRef()->getTransducerForChannel(pSingleTargetData->m_parentSTId);
			if (transducer == nullptr)
			{
				M3D_LOG_WARN(LoggerName, Log::format("Compute Mean TS, transducer not found for channel %d !", pSingleTargetData->m_parentSTId));
				continue;
			}

			auto softChan = transducer->getSoftChannel(pSingleTargetData->m_parentSTId);
			if (softChan == nullptr)
			{
				M3D_LOG_WARN(LoggerName, Log::format("Compute Mean TS, channel %d not found !", pSingleTargetData->m_parentSTId));
				continue;
			}

			double compensateHeave = pingFan->getHeaveChan(softChan->m_softChannelId);
			for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataCWCount(); ++numTarget)
			{
				const auto & tsData = pSingleTargetData->GetSingleTargetDataCW(numTarget);

				auto freq = tsData.m_freq;
				if (m_isMultiBeam)
				{
					freq = std::round(softChan->m_mainBeamAthwartSteeringAngleRad / (2 * PI) * 360);
				}
				else if (freq <= 0)
				{
					freq = softChan->m_acousticFrequency;
				}

				BaseMathLib::Vector3D mSoftCoord(0, 0, tsData.m_targetRange);
				BaseMathLib::Vector3D fanCoords = softChan->GetMatrixSoftChan() * mSoftCoord;
				auto targetDepth = fanCoords.z + transducer->m_transDepthMeter + compensateHeave;

				if (targetDepth >= depthMin && targetDepth <= depthMax)
				{
					M3D_LOG_INFO(LoggerName, Log::format("Compute Mean TS, add CW target %d (ping : %d, depth : %f)", tsData.m_trackLabel, pingFan->m_computePingFan.m_pingId, targetDepth));
					auto & tsPoint = tsPoints[freq];
					tsPoint.freq = freq;
					tsPoint.compensatedTSSum += pow(10.0, tsData.m_compensatedTS * 0.1);
					tsPoint.uncompensatedTSSum += pow(10.0, tsData.m_unCompensatedTS * 0.1);
					tsPoint.nbPoint += 1;
				}
			}

			for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataFMCount(); ++numTarget)
			{
				const auto & tsData = pSingleTargetData->GetSingleTargetDataFM(numTarget);

				BaseMathLib::Vector3D mSoftCoord(0, 0, tsData.m_targetRange);
				BaseMathLib::Vector3D fanCoords = softChan->GetMatrixSoftChan() * mSoftCoord;
				auto targetDepth = fanCoords.z + transducer->m_transDepthMeter + compensateHeave;

				if (targetDepth >= depthMin && targetDepth <= depthMax)
				{
					M3D_LOG_INFO(LoggerName, Log::format("Compute Mean TS, add FM target %d (ping : %d, depth : %f)", tsData.m_trackLabel, pingFan->m_computePingFan.m_pingId, targetDepth));
					const auto nbFreq = tsData.m_freq.size();
					for (int freqIdx = 0; freqIdx < nbFreq; ++freqIdx)
					{
						const auto freq = tsData.m_freq[freqIdx];
						auto & tsPoint = tsPoints[freq];
						tsPoint.freq = freq;
						tsPoint.compensatedTSSum += pow(10.0, tsData.m_compensatedTS[freqIdx] * 0.1);
						tsPoint.uncompensatedTSSum += pow(10.0, tsData.m_unCompensatedTS[freqIdx] * 0.01);
						tsPoint.nbPoint += 1;
					}
				}
			}
		}
	}

	auto pResult = GetResults()[shoalId];
	for (auto it = tsPoints.cbegin(); it != tsPoints.cend(); ++it)
	{
		const auto & tsPoint = it->second;
		if (tsPoint.nbPoint > 0)
		{
			pResult->ts.freq.push_back(tsPoint.freq);
			pResult->ts.compensatedTS.push_back(tsPoint.compensatedTSSum / tsPoint.nbPoint);
			pResult->ts.unCompensatedTS.push_back(tsPoint.uncompensatedTSSum / tsPoint.nbPoint);
		}
	}
}
