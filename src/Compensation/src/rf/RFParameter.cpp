// -*- C++ -*-
// ****************************************************************************
// Class: RFParameter
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************


//shoalextraction
#include "ShoalExtraction/data/shoaldata.h"

#include "Compensation/rf/RFParameter.h"
#include <math.h>

RFParameter::RFParameter(void) : BaseKernel::ParameterObject()
{
	BuildDefaultSettings();
}

RFParameter::~RFParameter(void)
{
}
void RFParameter::BuildDefaultSettings()
{
	//use of SV instead of SA
	m_bUseSV = false;

	//compute unfiltered data
	m_bComputeUnfiltered = true;
	//compute filtered data
	m_bComputeFiltered = true;

	//precision in begin distance value (in nautic miles)
	m_fPrecisionLog = 0.005;

	//precision in depth (m)
	m_fPrecisionDepth = 5;

	// Overlap threshold
	m_overlapThreshold = 80.0;
	// Data threshold
	m_dataThreshold = -60.0;

	//window RF 

	m_uiPingFilteredStart = 0;
	m_uiPingFilteredStop = 65535;

	//spatial depth window
	m_fDepthMin = 0;
	m_fDepthMax = 10000;

	//shoal RF

	//min nb of echoes in the shoal
	m_iMinEchoesNb = 10;

	//ref transducer
	m_sRefTransducer = "ES38B";

	//Vertical Integration Distance
	m_verticalIntegrationDistance = 1.0;
	//Along Integration Distance
	m_alongIntegrationDistance = 0.0;
	//Across Integration Distance
	m_acrossIntegrationDistance = 0.0;
}

bool RFParameter::Serialize(BaseKernel::MovConfig * MovConfig)
{
	return true;
}
bool RFParameter::DeSerialize(BaseKernel::MovConfig * MovConfig)
{
	return true;
}
