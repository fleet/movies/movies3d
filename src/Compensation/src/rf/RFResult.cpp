// ****************************************************************************
// Class: RFResult
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************


// -*- C++ -*-
// ****************************************************************************
// Class: RFResult
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#include "Compensation/rf/RFResult.h"
#include <math.h>

RFResult::RFResult(void)
{
	//energy SA
	m_fEnergySA = 0;
	//energy SV
	m_fEnergySV = 0;

	//n log
	m_fNLog = 0;

	//echoes Nb
	m_iEchoesNb = 0;

	//rf ratio
	m_fRFRatio = 0;

	//frequency
	m_fFrequency = 1.0;

	//angle
	m_angle;
}


RFResult::~RFResult(void)
{
}
