#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EISupervised/EISupervisedExport.h"
#include "M3DKernel/parameter/ParameterObject.h"

#include <string>

// ***************************************************************************
// Representation de la definition d'une classification avec :
// - le nom de la classification
// - la description de la classification
// ***************************************************************************
class EISUPERVISED_API ClassificationDef :
	public BaseKernel::ParameterObject
{
public:
	ClassificationDef(void);
	~ClassificationDef(void);

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);

	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	int m_Id;

	std::string m_Name;

	std::string m_Description;
};
