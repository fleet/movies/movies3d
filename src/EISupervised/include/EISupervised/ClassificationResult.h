#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EISupervised/EISupervisedExport.h"
#include <set>
#include <cstdint>

// ***************************************************************************
// Declaration anticip�e
// ***************************************************************************
class EchoResult;
class ClassificationDef;

typedef std::set<EchoResult*> EchoResultSet;

namespace BaseMathLib
{
	class Poly2D;
}


// ***************************************************************************
// Represente le resultat de l'attribution d'une classsification � un ensemble d'echos
// ***************************************************************************
class EISUPERVISED_API IClassificationResult
{
public:
	IClassificationResult();
	virtual ~IClassificationResult() {};

	double GetEnergy(int classId) const;

	double GetRemainingEnergy() const;

	bool IsClassified() const;

	int GetClassId() const;

	void Reset();

	void IncNt();

	std::int32_t GetNt() const;

	std::int32_t GetNi(int classId) const;

	std::int32_t GetRemainingNi() const;


	double GetSumHeight() const;

	void IncSumHeight(const double & value);

	// Retourne la liste des �chos dans la couche
	const EchoResultSet & GetEchos() const;

	// Ajoute un �chos
	void AddEcho(EchoResult * echo);

	void ClassifyAs(int classId);

	double m_volume;

	double m_surface;

	double m_depth;

protected:

	std::int32_t m_nt;

	double m_sumHeight;

	// Liste des echos appartenant � la classification
	EchoResultSet m_echos;

	// Classification attribu�
	int m_classId;
};

// Resultat de la classification d'une couche
class LayerClassificationResult : public IClassificationResult
{
public:
	int m_layerIdx;

	double m_latitude;

	double m_longitude;
};

// Resultat de la classification d'un polygone
class PolygonClassificationResult : public IClassificationResult
{
public:
	BaseMathLib::Poly2D * m_pPolygon;
};
