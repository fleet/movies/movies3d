#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "EISupervised/EISupervisedExport.h"
#include <cstdint>
#include <map>

// ***************************************************************************
// Declaration anticip�e
// ***************************************************************************
class EISupervisedTransducer;
class EISupervisedResult;
class EISupervisedOutput;
class EISupervisedPolygonOutput;
class EISupervisedParameter;
class Transducer;

namespace BaseMathLib
{
	class Poly2D;
}

// ***************************************************************************
// Definition
// ***************************************************************************

typedef std::map<std::uint32_t, EISupervisedResult*> MapResultByEsu;
typedef std::map<std::uint32_t, EISupervisedOutput*> MapOutputsByEsu;


/// Echo integration supervis�e sur un transducteur
class EISupervisedChannel
{
public:
	EISupervisedChannel(EISupervisedTransducer * pParentTransducer);

	~EISupervisedChannel();

	void UpdateOutputSndSet(Transducer * pTrans, const double & soundVelocity);

	void AddEIResult(const std::uint32_t & esuId, const EISupervisedParameter & eiSupervisedParameter);

	// Ajoute le polygone pass� en param�tre, ne fait rien si le polygon n'est pas inclus dans une esu
	bool AddPolygon(BaseMathLib::Poly2D * polygon, std::int32_t minPing, std::int32_t maxPing);

	// Supprime le polygone pass� en param�tre
	void DeletePolygon(BaseMathLib::Poly2D * polygon, std::int32_t minPing, std::int32_t maxPing);

	// Calcule et stocke les energie des echos et leur appartenance dans la structure results
	void Compute(EISupervisedResult & results);

	// id du sondeur/transducteur/channel sur lequel l'ei supervised se d�roule
	int   m_softChannelId;
	int   m_iChannel;

	// Resultat par esu
	MapResultByEsu m_results;

	// Resultat par esu
	MapOutputsByEsu m_outputs;
	EISupervisedOutput * m_currentOutput;

	// Sorties par polygone
	std::map<BaseMathLib::Poly2D *, EISupervisedPolygonOutput*> m_polygonOutputs;

	// Transducteur parent
	EISupervisedTransducer * m_pParentTransducer;
};
