#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EISupervised/EISupervisedExport.h"

#include "EISupervised/EISupervisedResult.h"

// ***************************************************************************
// Resultat moyenn� de l'echo integration supervis�e sur une esu (ME70)
// ***************************************************************************
class EISUPERVISED_API EISupervisedMeanResult : public EISupervisedResult
{
public:
	EISupervisedMeanResult();

	virtual ~EISupervisedMeanResult();

	/////////////////////////////////
	// FONCTIONS DE CLASSIFICATION //
	/////////////////////////////////

	// Retourne le sA non attribu�
	virtual double GetRemainingSa() const;

	// Retourne le sA total
	virtual double GetTotalSa() const;

	// Retourne le sA d'une classification dans l'esu
	virtual double GetClassificationSa(int classId) const;

	// Retourne le sA pour une liste de layers et polygones
	virtual double GetSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const;

	// Retourne le sA non classifi�e pour une liste de layers et polygones
	virtual double GetFreeSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const;

	// Retourne le sA classifi�e pour une liste de layers et polygones
	virtual double GetClassifiedSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons, int classId) const;

	/////////////////
	// ACCESSEURS //
	////////////////

	void SetTotalSa(double dbSa);
	void SetRemainingSa(double dbSa);
	void SetClassifiedSa(const std::map<int, double> & classifiedSa);

private:

	double m_dbRemainingSa;
	double m_dbTotalSa;
	std::map<int, double> m_mapClassifiedSa;
};
