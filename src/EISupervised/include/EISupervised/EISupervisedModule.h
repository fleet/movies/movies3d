#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EISupervised/EISupervisedExport.h"
#include "M3DKernel/module/ProcessModule.h"

#include "EISupervised/EISupervisedParameter.h"
#include "EISupervised/EISupervisedResult.h"
#include "EISupervised/EISupervisedTransducer.h"
#include "EISupervised/EISupervisedChannel.h"

#include "EchoIntegration/LayerDef.h"

#include "BaseMathLib/geometry/Poly2D.h"

#include <set>

// ***************************************************************************
// Declaration anticip�e
// ***************************************************************************
class EchoIntegrationModule;
class EISupervisedOutput;

class EISupervisedMovFileOutput;
class EISupervisedPolygonFileOutput;

// ***************************************************************************
// Definition
// ***************************************************************************
typedef std::set<BaseMathLib::Poly2D*> PolygonSet;
typedef std::map<std::string, EISupervisedTransducer*> MapEITransducers;
typedef std::map<std::string, EISupervisedMovFileOutput*> MapEIOutputByTransducers;

// ***************************************************************************
// Module d'echo integration supervis�e
// ***************************************************************************
class EISUPERVISED_API EISupervisedModule : public ProcessModule
{
public:

	// Constructeur par d�faut
	MovCreateMacro(EISupervisedModule);

	// Destructeur
	virtual ~EISupervisedModule();

	virtual void setEnable(bool a);

	// Pour gestion de l'arr�t diff�r� de l'�choint�gration supervis�e
	virtual void DisableForNextESU();

	void PingFanAdded(PingFan *);

	void ESUStart(ESUParameter * pWorkingESU);

	void ESUEnd(ESUParameter * pWorkingESU, bool abort);

	void SounderChanged(std::uint32_t);

	void StreamClosed(const char *streamName);

	void StreamOpened(const char *streamName);


	BaseKernel::ParameterModule& GetParameterModule() { return m_parameter; };

	EISupervisedParameter&	GetEISupervisedParameter() { return m_parameter; }


	void SetEchoIntegrationModule(EchoIntegrationModule * pEchoIntegrationModule);
	
	// Retourne vrai si l'échointégration est activé pour le transducteur
	bool IsEISupervisedEnabled(const std::string & transName) const;

	// Retourne vrai si l'échointégration est activé pour le transducteur et le type de couche
	bool IsEISupervisedEnabled(const std::string & transName, Layer::Type layerType) const;

	// Retourne le resultat associ� � une esu pour la voie central du transducteur
	EISupervisedResult * GetCentralResultFromEsu(const std::string & transName, std::uint32_t esuId) const;

	// Retourne le resultat moyen sur tous les faisceaux pond�r� par le volume insonifi� associ� � une esu
	EISupervisedResult * GetMeanResultFromEsu(const std::string & transName, std::uint32_t esuId, const ClassificationVector& classifications,
		const std::vector<int> & layers, const std::vector<BaseMathLib::Poly2D*> & polygons,
		bool bUseVolumes) const;

	// Retourne les r�sultats associ�s � une esu pour toutes les voies du transducteur
	std::vector<EISupervisedResult*> GetResultsFromEsu(const std::string & transName, std::uint32_t esuId) const;

	// Retourne les resultats
	MapResultByEsu GetResults(const std::string & transName, const ClassificationVector& classifications, bool buseVolumes) const;

	// Retourne l'ensemble des polygones d�finis
	PolygonSet GetPolygons(const std::string & transName) const;

	// Ajoute le polygone pass� en param�tre, ne fait rien si le polygon n'est pas inclus dans une esu
	void AddPolygon(const std::string & transName, BaseMathLib::Poly2D * polygon);

	// Supprime le polygone pass� en param�tre
	void DeletePolygon(const std::string & transName, BaseMathLib::Poly2D * polygon);

	// Retourne l'esu d'appartenance du polygone
	std::uint32_t GetEsuForPolygon(const std::string & transName, BaseMathLib::Poly2D * polygon);

	// Reset des esu
	void Reset(const std::string & transName, const EsuIdVector & esuIdxVector);

	// Sauvegarde des r�sultats, retourne le nom du fichier
	void SaveResults(const std::string & transName, const EsuIdVector & esuIdVector,
		EISupervisedMovFileOutput * pFileOutput, bool bCreatedFileOutput, const std::vector<EISupervisedPolygonFileOutput*> & polygonFilesOutputs);

	// OTK - FAE198 - avertissement en cas d'�crasement des fichiers r�sultats
	EISupervisedMovFileOutput * GetDestinationFile(const std::string & transName, const EsuIdVector & esuIdVector, bool & bCreatedFileOutput,
		std::vector<EISupervisedPolygonFileOutput*> & polygonFileOutputs);


protected:

	// Constructeur
	EISupervisedModule();

	EISupervisedTransducer * getEISupervisedTransducer(const std::string & transName) const;

	void UpdateInitialized();

	EISupervisedParameter m_parameter;

	EchoIntegrationModule * m_pEchoIntegrationModule;

	// indique si le traitement doit s'arr�ter � la fin de l'ESU courant
	bool m_DisableNextESU;

	// indique si un ESU est encours
	bool m_ESURunning;

	bool m_initialized;

	// EI supervis�e par transducteur
	MapEITransducers m_eiSupervisedTransducers;

	// Sortie CSV par transducteur
	MapEIOutputByTransducers m_outputByTransducers;
};
