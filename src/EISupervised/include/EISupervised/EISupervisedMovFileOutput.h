#pragma once

#include "OutputManager/MovFileOutput.h"

class EISupervisedMovFileOutput : public MovFileOutput
{
public:
	EISupervisedMovFileOutput();

	EISupervisedMovFileOutput(
		std::string directoryPath, 
		std::string fileNamePrefix,
		int frequency,
		int id,
		bool prefixDate);

	virtual ~EISupervisedMovFileOutput();

	virtual void ConstructNewFileName();

	const std::string & getDirectoryPath() const { return m_directoryPath; }
	const std::string & getFileNamePrefix() const { return m_fileNamePrefix; }
	int getFrequency() const { return m_frequency; }
	int getId() const { return m_id; }

private:
	std::string m_directoryPath;
	std::string m_fileNamePrefix;
	int m_frequency;
	int m_id;
};
