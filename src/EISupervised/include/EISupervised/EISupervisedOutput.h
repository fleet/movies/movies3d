#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EISupervised/EISupervisedParameter.h"
#include "EchoIntegration/EchoIntegrationParameter.h"

#include "M3DKernel/module/ModuleOutput.h"


#include "MovNetwork/MvNetDataXMLSndset.h"
#include "MovNetwork/MvNetDataXMLShipnav.h"
#include "MovNetwork/MvNetDataXMLCellset.h"
#include "MovNetwork/MvNetDataXMLLayer.h"

class Transducer;
class EISupervisedResult;

class EISupervisedOutput : public ModuleOutput
{
public:

	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	  // Constructeur par d�faut
	MovCreateMacro(EISupervisedOutput);

	EISupervisedOutput();

	// Destructeur
	virtual ~EISupervisedOutput();

	const CMvNetDataXMLSndset & getSndSet() const;

	virtual void UpdateSndSet(
		Transducer * pTrans,
		unsigned short channelID,
		double soundVelocity
	);

	virtual void PrepareForSerialization(
		const EISupervisedParameter & eiSupervisedParameters,
		const EchoIntegrationParameter  & echoIntegrationParameter,
		EISupervisedResult * eiSupervisedResult,
		std::int32_t pingstart, std::int32_t pingend, HacTime timestart,
		HacTime timeend, double diststart, double distend,
		double roll, double pitch, double heave,
		double latitude, double longitude,
		double groundspeed, double groundcourse,
		double heading, double surfspeed,
		double driftcourse, double driftspeed, double altitude,
		unsigned int sounderID);

	virtual void CSVSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);

	virtual void CSVHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);

	const CMvNetDataXMLSndset & getSndSet() { return m_MvNetDataXMLSndSet; }
	const CMvNetDataXMLShipnav & getShipNav() { return m_MvNetDataXMLShipnav; }
	EISupervisedResult * getResult() { return m_eiSupervisedResult; }
	const ClassificationVector& getClassifications() const { return m_classifications; }

protected:

	virtual std::string CVSSerializeClassification(BaseKernel::TramaDescriptor & tramaDescriptor, const HacTime & now,
		ClassificationDef * classification, bool bTotal = false);

	// informations sur le channel
	CMvNetDataXMLSndset		m_MvNetDataXMLSndSet;

	// informations de navigation
	CMvNetDataXMLShipnav	m_MvNetDataXMLShipnav;

	// nombre de cellules
	unsigned int m_cellCount;

	// informations sur la cellule
	std::vector<CMvNetDataXMLCellset>	m_MvNetDataXMLCellset;
	CMvNetDataXMLCellset m_MvNetDataXMLCellsetTotal;

	// informations sur les couches
	std::vector<CMvNetDataXMLLayer>		m_MvNetDataXMLLayer;
	CMvNetDataXMLLayer m_MvNetDataXMLLayerTotal;

	// Classifications effectu�es
	EISupervisedResult * m_eiSupervisedResult;

	// informations de classificatons
	ClassificationVector m_classifications;

	// Descripteurs CSV
	static bool m_descInitialized;
	static sndsetDescriptor m_snddesc;
	static shipnavDescriptor m_shipnavdesc;
	static cellsetDescriptor m_cellsetdesc;
	static layerDescriptor m_layerdesc;
	static void ComputeDescriptors(BaseKernel::TramaDescriptor & tramaDescriptor);
};
