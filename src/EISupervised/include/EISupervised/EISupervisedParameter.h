#pragma once


// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EISupervised/EISupervisedExport.h"
#include "M3DKernel/parameter/ParameterModule.h"
#include "EchoIntegration/EchoIntegrationModule.h"
#include "ClassificationDef.h"

#include <map>
#include <vector>

// ***************************************************************************
// Paramètres du module d'echo integration supervisée
// ***************************************************************************
typedef	std::vector<ClassificationDef> ClassificationVector;

#include "M3DKernel/parameter/ParameterSerializable.h"

// Paramètre d'activation des couches pour un transducteur
struct TransducerLayerConf
{
	bool surface = false;
	bool distance = false;
	bool bottom = false;

	bool isActive() const { return surface || distance || bottom; }
};

class EISUPERVISED_API EISupervisedParameter :
	public BaseKernel::ParameterModule
{
public:
	EISupervisedParameter();

	bool Serialize(BaseKernel::MovConfig * movConfig) override;
	bool DeSerialize(BaseKernel::MovConfig * movConfig) override;
	
	void setEchoIntegrationParameter(EchoIntegrationParameter* echoIntegrationParameter);
	
	const std::map<std::string, TransducerLayerConf>& getTransducers() const;
	const TransducerLayerConf* getTransducerLayerConf(const std::string& transducerName) const;
	void clearTransducerLayerConf();
	void addTransducerLayerConf(const std::string& transducerName, const TransducerLayerConf& layerConf);

	const ClassificationVector& getClassifications() const;
	const ClassificationDef& getClassification(size_t index) const;
	void clearClassifications();
	void addClassification(const ClassificationDef& classDef);
	
	bool isAppendResults() const;
	bool isUseVolumicWeightedAvg() const;

	void setUseVolumicWeightedAvg(bool useVolumicWeightedAvg);
	void setAppendResults(bool bAppendResults);


private:
	EchoIntegrationParameter* m_echoIntegrationParameter;
	
	// Definitions des classifications déclarées
	ClassificationVector m_classifications;

	// Tranducteur utilisés
	std::map<std::string, TransducerLayerConf> m_transducerLayerConfMap;

	// Booleen pour savoir si on écrase les résultats ou non
	bool m_appendResults;

	// Booleen pour la pondération par le volume de chaque faisceau pour le calcul du Sa moyen multifaisceaux
	bool m_useVolumicWeightedAvg;
};
