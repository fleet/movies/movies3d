#pragma once

#include "EISupervised/EISupervisedMovFileOutput.h"

namespace BaseMathLib {
	class Poly2D;
}

class EISupervisedPolygonFileOutput : public EISupervisedMovFileOutput
{
public:
	EISupervisedPolygonFileOutput(
		std::string directoryPath,
		std::string fileNamePrefix,
		int frequency,
		int id,
		bool prefixDate,
		std::uint32_t esuId,
		std::uint32_t polygonId,
		BaseMathLib::Poly2D * polygon,
		const std::vector<size_t> & channelIndexes);

	virtual ~EISupervisedPolygonFileOutput();

	std::uint32_t getESUId();
	BaseMathLib::Poly2D * getPolygon();
	const std::vector<size_t> & getChannelIndexes() const;

protected:

	std::uint32_t m_esuId;
	std::uint32_t m_polygonId;
	BaseMathLib::Poly2D * m_polygon;
	std::vector<size_t> m_channelIndexes;
};
