#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EISupervised/EISupervisedOutput.h"

#include "BaseMathLib/geometry/Poly2D.h"

class PolygonClassificationResult;
class EISupervisedChannel;

class EISupervisedPolygonOutput : public EISupervisedOutput
{
public:

	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	EISupervisedPolygonOutput(BaseMathLib::Poly2D * polygon, std::int32_t minPing, std::int32_t maxPing, int transIdx, unsigned short softChanId,
		const EchoIntegrationParameter& echoIntegrationParams, EISupervisedOutput * pReferenceOutput, EISupervisedChannel * pCentralSupervisedChannel);

	// Destructeur
	virtual ~EISupervisedPolygonOutput();

	virtual void CSVSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor,
		PolygonClassificationResult * pPolygonResult);

private:
	std::string m_strCoordinates;

	int m_nbPings;

	EISupervisedOutput * m_pReferenceOutput;
};