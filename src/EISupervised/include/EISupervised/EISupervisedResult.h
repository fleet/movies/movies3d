#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EISupervised/EISupervisedExport.h"

#include <set>
#include <vector>

#include "EchoResult.h"
#include "PingResult.h"

// ***************************************************************************
// Declaration anticip�e
// ***************************************************************************
class ClassificationDef;
class IClassificationResult;

class LayerClassificationResult;
class PolygonClassificationResult;

namespace BaseMathLib
{
	class Poly2D;
}

typedef std::vector<IClassificationResult*> ClassificationResultVector;
typedef std::map<unsigned int, IClassificationResult*> LayersClassifications;

// ***************************************************************************
// Resultat de l'echo integration supervis�e sur une esu
// ***************************************************************************
class EISUPERVISED_API EISupervisedResult
{
public:
	EISupervisedResult();

	virtual ~EISupervisedResult();

	/////////////////////////////////
	// FONCTIONS DE CLASSIFICATION //
	/////////////////////////////////

	// Retourne la liste des echos int�grable
	EchoResult * AddEchoResult(int pingId, int echoId);

	// Ajoute un �cho int�grable
	EchoResult * GetEchoResult(int pingId, int echoId);

	// Retourne le nombre d'echos int�grable total
	std::int32_t GetTotalNi() const;

	// Retourne le sA non attribu�
	virtual double GetRemainingSa() const;

	// Retourne l'energie totale de l'esu
	double GetTotalEnergy() const;

	// Retourne le sA total
	virtual double GetTotalSa() const;

	// Supprime toute information de classification
	void Reset();

	// Retourne l'energie d'une classification dans l'esu
	double GetClassificationEnergy(int classId) const;

	//
	std::int32_t GetClassificationNi(int classId) const;

	// Retourne le sA d'une classification dans l'esu
	virtual double GetClassificationSa(int classId) const;

	// Retourne l'energie totale pour une liste de layers et polygones
	double GetEnergy(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const;

	// Retourne le sA pour une liste de layers et polygones
	virtual double GetSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const;

	// Retourne l'energie non classifi�e pour une liste de layers et polygones
	double GetFreeEnergy(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const;

	// Retourne le sA non classifi�e pour une liste de layers et polygones
	virtual double GetFreeSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const;

	// Retourne l'energie classifi�e pour une liste de layers et polygones
	double GetClassifiedEnergy(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons, int classId) const;

	// Retourne le sA classifi�e pour une liste de layers et polygones
	virtual double GetClassifiedSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons, int classId) const;


	// Attribue une classification aux echos appartenant a un banc / polygone / couche

	void SetClassificationForPolygon(BaseMathLib::Poly2D *  polygon, int classId);
	void SetClassificationForLayer(int layerIdx, int classId);

	// Reset de la classification aux echos appartenant a un banc / polygone / couche et classifi� en tant que

	void ResetClassificationForPolygon(BaseMathLib::Poly2D *  polygon, int classId);
	void ResetClassificationForLayer(int layerIdx, int classId);

	// Reclassification des echos appartenant a un banc / polygone / couche

	void ReclassifyPolygon(BaseMathLib::Poly2D *  polygon, int classId);
	void ReclassifyLayer(int layerIdx, int classId);


	void AddClassificationResult(IClassificationResult * classificationResult);

	LayerClassificationResult * GetLayerClassificationResult(int layerIdx) const;
	PolygonClassificationResult * GetPolygonClassificationResult(BaseMathLib::Poly2D *  pPolygon) const;

	// Supprime l'objet classiable associ� � un polygone
	void EraseClassificationForPolygon(BaseMathLib::Poly2D *  polygon);


	// Retourne la classification associ�e � un layer
	int GetLayerClassification(int layerIdx) const;

	// Retourne la classification associ�e � un layer
	int GetPolygonClassification(BaseMathLib::Poly2D *  polygon) const;



	/////////////////
	// ACCESSEURS //
	////////////////

	inline void SetPingRange(const std::int32_t & pingStart, const std::int32_t & pingEnd) { m_pingStart = pingStart; m_pingEnd = pingEnd; }

	inline std::int32_t GetPingStart() const { return m_pingStart; }
	//inline void SetPingStart(std::int32_t pingStart) { m_pingStart = pingStart; }

	inline std::int32_t GetPingEnd() const { return m_pingEnd; }
	//inline void SetPingEnd(std::int32_t pingEnd) { m_pingEnd = pingEnd; }

	inline double GetBottomDepth() const { return m_bottomDepth; }
	inline void SetBottomDepth(double bottomDepth) { m_bottomDepth = bottomDepth; }

	inline double GetEsuDistanceWidth() const { return m_esuDistanceWidth; }
	inline void SetEsuDistanceWidth(double esuDistWidth) { m_esuDistanceWidth = esuDistWidth; }

	inline double GetEsuSampleDepthSI() const { return m_esuSampleDepthSI; }
	inline void SetEsuSampleDepthSI(double esuSampleDepthSI) { m_esuSampleDepthSI = esuSampleDepthSI; }

    inline std::uint64_t GetNbPing() const { return m_nbPing; }
    inline void SetNbPing(std::uint64_t nbPing) { m_nbPing = nbPing; }

	inline void SetTotalSumHeight(const double & sumHeight) { m_esuSumHeight = sumHeight; }
	double GetTotalSumHeight() const { return m_esuSumHeight; }

	// Accesseurs sur le nombre total d'�chos
	void SetNt(const std::int32_t & nt);
	std::int32_t GetTotalNt() const;

	// Accesseurs sur l'�tat de sauvegarde
	bool IsAlreadySaved();
	void SetAlreadySaved(bool bAlreadySaved);

	double energy2sA(double energy) const;
	double energy2Sv(double energy, double sumHeight) const;

	// id du sondeur/transducteur/channel sur lequel l'ei supervised se d�roule
	int   m_sounderId;
	int   m_transIdx;
	int   m_softChannelId;

	double m_latitude;
	double m_longitude;
	double m_volume;
	double m_surface;

	// OTK - FAE166 - volume insonnifi� pour moyenne pond�r�e dans le cas multifaisceaux (diff�rent de m_volume)
	double m_insonifiedVolume;

private:
	// ping de debut et de fin de l'esu
	std::int32_t m_pingStart;
	std::int32_t m_pingEnd;

	// Nombre total d'echos
	std::int32_t m_nt;

	// Donn�es membres n�cessaires au calcul du sa et sv
    std::uint64_t m_nbPing;

	// fond courant du channel
	double m_bottomDepth;

	// information sur l'esu
	//double m_esuAverageDistPing;
	double m_esuSampleDepthSI;
	double m_esuDistanceWidth;
	double m_esuSumHeight;

	// Map des pings et echos
	MapPingResults m_pings;

	// Objets classifiables (couches / bancs /polygones)
	ClassificationResultVector m_results;

	// OTK - FAE 2057 - Flag indiquant si ce r�sultat a d�j� �t� sauvegard� ou non pour affichage en gris� dans
	// l'IHM le cas �ch�ant.
	bool m_bAlreadySaved;
};
