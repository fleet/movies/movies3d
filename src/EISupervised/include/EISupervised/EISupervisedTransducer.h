#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EISupervised/EISupervisedExport.h"
#include "M3DKernel/module/ProcessModule.h"

#include "EISupervised/EISupervisedParameter.h"
#include "EISupervised/EISupervisedResult.h"

#include "BaseMathLib/geometry/Poly2D.h"

#include <set>
#include <vector>

// ***************************************************************************
// Declaration anticip�e
// ***************************************************************************
class EchoIntegrationModule;
class EISupervisedChannel;
class EISupervisedMeanResult;

// ***************************************************************************
// Definition
// ***************************************************************************
typedef std::map<std::uint32_t, EISupervisedResult*> MapResultByEsu;

typedef std::set<BaseMathLib::Poly2D*> PolygonSet;

typedef std::vector<std::uint32_t> EsuIdVector;

struct EsuInfo
{
	EsuInfo();
	void Reset();
	void Update(PingFan * pFan);

	bool m_startFound;
	std::uint64_t m_pingstart;
	std::uint64_t m_pingend;

	HacTime			m_timestart;
	HacTime			m_timeend;

	double			m_diststart;
	double			m_distend;

	std::map<unsigned short, double> m_latitudes;
	std::map<unsigned short, double> m_longitudes;

	// Donn�es pour les trames ShipNav
	std::map<unsigned short, double> m_rolls;
	std::map<unsigned short, double> m_pitchs;
	std::map<unsigned short, double> m_heaves;
	double  m_groundspeed;
	std::map<unsigned short, double> m_groundcourses;
	std::map<unsigned short, double> m_headings;
	std::map<unsigned short, double> m_surfspeeds;
	double  m_driftcourse;
	double	m_driftspeed;
	double	m_altitude;
};

/// Echo integration supervis�e sur un transducteur
class EISupervisedTransducer
{
public:
	EISupervisedTransducer();

	~EISupervisedTransducer();

	void Initialize(Transducer * pTransducer);

	EchoIntegrationModule * m_pEchoIntegrationModule;

	std::string m_TransducerName;

	// Activation de l'EI supervisée pour les couches de surface pour le transducteur
	bool m_isSurfaceLayerEnabled = false;

	// Activation de l'EI supervisée pour les couches de distance pour le transducteur
	bool m_isDistanceLayerEnabled = false;
	
	// Activation de l'EI supervisée pour les couches de fond pour le transducteur
	bool m_isBottomLayerEnabled = false;

	EISupervisedChannel * getCentralSupervisedChannel() const;

	int getCentralSoftChannelId() const;

	int getCentralTransducerFrequency() const;

	// id du sondeur/transducteur/channel sur lequel l'ei supervised se d�roule
	int   m_sounderId;
	int   m_transIdx;
	bool  m_initialized;

	EsuInfo m_currentEsuInfo;

	std::vector<EISupervisedChannel*> m_Channels;

	void UpdateOutputSndSet(Transducer * pTrans, const double & soundVelocity);

	void AddEIResult(const std::uint32_t & esuId, const EISupervisedParameter & eiSupervisedParameter);

	// Retourne le resultat associ� � une esu pour la voie centrale du transducteur
	EISupervisedResult * GetCentralResultFromEsu(std::uint32_t esuId) const;

	// Retourne le resultat moyen sur toutes les voies associ� � une esu
	EISupervisedResult * GetMeanResultFromEsu(std::uint32_t esuId, const ClassificationVector& classifications,
		const std::vector<int> & layers, const std::vector<BaseMathLib::Poly2D*> & polygons,
		bool buseVolumes);

	// Retourne le resultat associ� � une esu pour toutes les voies du transducteur
	std::vector<EISupervisedResult*> GetResultsFromEsu(std::uint32_t esuId) const;

	// Retourne le resultat moyen sur toutes les voies pour toutes les ESU
	MapResultByEsu GetMeanResults(const ClassificationVector& classifications, bool buseVolumes);

	// Ajoute le polygone pass� en param�tre, ne fait rien si le polygon n'est pas inclus dans une esu
	void AddPolygon(BaseMathLib::Poly2D * polygon);

	// Supprime le polygone pass� en param�tre
	void DeletePolygon(BaseMathLib::Poly2D * polygon);

	// Retourne l'esu d'appartenance du polygone
	std::uint32_t GetEsuForPolygon(BaseMathLib::Poly2D *polygon);

	void Reset(const EsuIdVector & esuIdVector);

	// polygones
	PolygonSet m_polygons;

	// Map des r�sultats multifaisceaux moyen instanci� sp�cialement et � nettoyer
	MapResultByEsu m_meanResults;
};
