#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <cstdint>
#include <map>

/// ***************************************************************************
// Declaration anticipée
// ***************************************************************************
namespace BaseMathLib
{
	class Poly2D;
}

class IClassificationResult;

// ***************************************************************************
// Representation d'un echo avec :
// - son energie
// - son appartenance à un banc, un polygone, une couche de surface, une couche de fond, une couche de distance
// ***************************************************************************
class EchoResult
{
public:
	EchoResult();
	~EchoResult();

	// energie associée à l'écho
	double m_energy;

	// si l'écho est classifié
	bool isClassified() const;

	// sa classification 
	const IClassificationResult * parentClassification;

	// appartenance de l'echo
	int m_surfaceLayerIdx;
	int m_bottomLayerIdx;
	int m_distanceLayerIdx;

	BaseMathLib::Poly2D * m_pPolygon;
};

typedef std::map<std::int32_t, EchoResult> MapEchoResults;
