#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EchoResult.h"

// ***************************************************************************
// Repr�sentation d'un ping avec ses echos
// ***************************************************************************
class PingResult
{
public:
	PingResult() {};
	~PingResult() {};

	MapEchoResults m_echos;
};

typedef std::map<std::int32_t, PingResult> MapPingResults;
