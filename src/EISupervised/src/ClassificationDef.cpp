#include "EISupervised/ClassificationDef.h"

#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

ClassificationDef::ClassificationDef(void)
	: m_Id(-1)
{
}

ClassificationDef::~ClassificationDef(void)
{
}

bool ClassificationDef::Serialize(BaseKernel::MovConfig * movConfig)
{
	// debut de la s�rialisation de l objet
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "Class");

	// s�rialisation des donn�es membres
	movConfig->SerializeData<std::string>(m_Name, eString, "Name");
	movConfig->SerializeData<std::string>(m_Description, eString, "Description");

	// fin de la s�rialisation de l'objet
	movConfig->SerializePushBack();

	return true;
}

bool ClassificationDef::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = true;
	// debut de la d�s�rialisation de l'objet
	result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "Class");

	// d�s�rialisation des donn�es membres
	result = result && movConfig->DeSerializeData<std::string>(&m_Name, eString, "Name");
	result = result && movConfig->DeSerializeData<std::string>(&m_Description, eString, "Description");

	// fin de la d�s�rialisation de l'objet
	movConfig->DeSerializePushBack();

	return result;
}
