
#include "EISupervised/ClassificationResult.h"
#include "EISupervised/EchoResult.h"


template<class Filter, class Action, typename Param>
void doEchoFilter(const EchoResultSet & echos, Filter & f, Action & a, Param & p)
{
	for (EchoResultSet::const_iterator iterEcho = echos.begin(); iterEcho != echos.end(); ++iterEcho)
	{
		const EchoResult * currentEcho = *iterEcho;
		if (f(currentEcho))
		{
			a(currentEcho, p);
		}
	}
}

void sumEchoEnergy(const EchoResult * echo, double & energy)
{
	energy += echo->m_energy;
}


IClassificationResult::IClassificationResult()
{
	m_classId = -1;
	m_nt = 0;
	m_sumHeight = 0.0;

	m_volume = 0.0;
	m_surface = 0.0;
	m_depth = 0.0;
}

void IClassificationResult::Reset()
{
	for (std::set<EchoResult*>::const_iterator iterEcho = m_echos.begin(); iterEcho != m_echos.end(); iterEcho++)
	{
		(*iterEcho)->parentClassification = nullptr;
	}
	m_classId = -1;
}

const EchoResultSet & IClassificationResult::GetEchos() const
{
	return m_echos;
}

int IClassificationResult::GetClassId() const
{
	return m_classId;
}

bool IClassificationResult::IsClassified() const
{
	return m_classId != -1;
}

void IClassificationResult::IncNt()
{
	m_nt++;
}

std::int32_t IClassificationResult::GetNt() const
{
	return m_nt;
}

std::int32_t IClassificationResult::GetNi(int classId) const
{
	std::int32_t niresult = 0;

	struct {
		bool operator() (const EchoResult * echo) {
			bool bUseEcho = false;
			if (echo->parentClassification == parentClassification && parentClassification->GetClassId() == classId)
			{
				bUseEcho = true;
			}
			else
			{
				const PolygonClassificationResult * pPolygonRes = dynamic_cast<const PolygonClassificationResult*>(echo->parentClassification);
				if (pPolygonRes && pPolygonRes->GetClassId() == classId)
				{
					bUseEcho = true;
				}
			}
			return bUseEcho;
		}
		const IClassificationResult * parentClassification;
		int classId;
	} filter;
	filter.parentClassification = this;
	filter.classId = classId;

	struct {
		void operator() (const EchoResult * echo, std::int32_t & ni) {
			ni++;
		}
	}incNi;

	doEchoFilter(m_echos, filter, incNi, niresult);
	return niresult;
}

std::int32_t IClassificationResult::GetRemainingNi() const
{
	struct {
		bool operator() (const EchoResult * echo) {
			return !echo->isClassified();
		}
	}filter;

	struct {
		void operator() (const EchoResult * echo, std::int32_t & ni) {
			ni++;
		}
	}incNi;


	std::int32_t remainingNi = 0;
	doEchoFilter(m_echos, filter, incNi, remainingNi);

	return remainingNi;
}

double IClassificationResult::GetEnergy(int classId) const
{
	double energy = 0.0f;

	struct {
		bool operator() (const EchoResult * echo) {
			bool bUseEcho = false;
			if (echo->parentClassification == parentClassification && parentClassification->GetClassId() == classId)
			{
				bUseEcho = true;
			}
			else
			{
				const PolygonClassificationResult * pPolygonRes = dynamic_cast<const PolygonClassificationResult*>(echo->parentClassification);
				if (pPolygonRes && pPolygonRes->GetClassId() == classId)
				{
					bUseEcho = true;
				}
			}
			return bUseEcho;
		}
		const IClassificationResult * parentClassification;
		int classId;
	} filter;
	filter.parentClassification = this;
	filter.classId = classId;
	doEchoFilter(m_echos, filter, sumEchoEnergy, energy);
	return energy;
}

double IClassificationResult::GetRemainingEnergy() const
{
	double energy = 0.0f;

	struct {
		bool operator() (const EchoResult * echo) {
			return !echo->isClassified();
		}
	}filter;
	doEchoFilter(m_echos, filter, sumEchoEnergy, energy);

	return energy;
}

double IClassificationResult::GetSumHeight() const
{
	return m_sumHeight;
}

void IClassificationResult::IncSumHeight(const double & value)
{
	m_sumHeight += value;
}

void IClassificationResult::ClassifyAs(int classId)
{
	m_classId = classId;
	for (std::set<EchoResult*>::const_iterator iterEcho = m_echos.begin(); iterEcho != m_echos.end(); iterEcho++)
	{
		//if((*iterEcho)->parentClassification == NULL)
		{
			(*iterEcho)->parentClassification = this;
		}
	}
}

void IClassificationResult::AddEcho(EchoResult * echo)
{
	m_echos.insert(echo);
}
