
#include "EISupervised/EISupervisedChannel.h"

#include "EISupervised/EISupervisedResult.h"
#include "EISupervised/EISupervisedOutput.h"
#include "EISupervised/EISupervisedPolygonOutput.h"
#include "EISupervised/EISupervisedTransducer.h"
#include "EISupervised/ClassificationResult.h"

#include "BaseMathLib/geometry/GeometryTools2D.h"
#include "EchoIntegration/EchoIntegrationModule.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/TransformSet.h"
#include "M3DKernel/datascheme/SoftChannel.h"

namespace
{
	constexpr const char * LoggerName = "EISupervised.EISupervisedChannel";
}

EISupervisedChannel::EISupervisedChannel(EISupervisedTransducer * pParentTransducer)
{
	m_currentOutput = new EISupervisedOutput();
	m_pParentTransducer = pParentTransducer;
}

EISupervisedChannel::~EISupervisedChannel()
{
	// destruction des r�sultats des polygones
	for (std::map<BaseMathLib::Poly2D*, EISupervisedPolygonOutput*>::iterator it = m_polygonOutputs.begin(); it != m_polygonOutputs.end(); ++it)
	{
		delete it->second;
	}

	// destruction des r�sultats
	for (MapResultByEsu::const_iterator it = m_results.begin(); it != m_results.end(); ++it)
	{
		delete it->second;
	}

	// destruction des sorties
	for (MapOutputsByEsu::const_iterator it = m_outputs.begin(); it != m_outputs.end(); ++it)
	{
		delete it->second;
	}

	delete m_currentOutput;
}

void EISupervisedChannel::UpdateOutputSndSet(Transducer * pTrans, const double & soundVelocity)
{
	m_currentOutput->UpdateSndSet(pTrans, m_softChannelId, soundVelocity);
}

void EISupervisedChannel::AddEIResult(const std::uint32_t & esuId, const EISupervisedParameter & eiSupervisedParameter)
{
	// on cr�� la classe pour la gestion des r�sultats
	EISupervisedResult * pResult = new EISupervisedResult();
	pResult->m_sounderId = m_pParentTransducer->m_sounderId;
	pResult->m_transIdx = m_pParentTransducer->m_transIdx;
	pResult->m_softChannelId = m_softChannelId;
	pResult->SetPingRange(m_pParentTransducer->m_currentEsuInfo.m_pingstart, m_pParentTransducer->m_currentEsuInfo.m_pingend);

	// initialisation des r�sultats pas layer


	// calcul volume et surface
	size_t nbLayers = m_pParentTransducer->m_pEchoIntegrationModule->GetEchoIntegrationParameter().countLayersDef();
	for (size_t iLay = 0; iLay < nbLayers; iLay++)
	{
		LayerClassificationResult * layerClassificationResult = new LayerClassificationResult();
		layerClassificationResult->m_layerIdx = iLay;
		pResult->AddClassificationResult(layerClassificationResult);
	}

	m_results[esuId] = pResult;

	Compute(*pResult);

	// on cr�e la classe de sortie pour la sauvegarde CSV
	const EsuInfo & currentEsuInfo = m_pParentTransducer->m_currentEsuInfo;
	m_currentOutput->PrepareForSerialization(
		eiSupervisedParameter, m_pParentTransducer->m_pEchoIntegrationModule->GetEchoIntegrationParameter(),
		pResult,
		currentEsuInfo.m_pingstart, currentEsuInfo.m_pingend,
		currentEsuInfo.m_timestart, currentEsuInfo.m_timeend,
		currentEsuInfo.m_diststart, currentEsuInfo.m_distend,
		currentEsuInfo.m_rolls.at(m_softChannelId), currentEsuInfo.m_pitchs.at(m_softChannelId), currentEsuInfo.m_heaves.at(m_softChannelId),
		currentEsuInfo.m_latitudes.at(m_softChannelId), currentEsuInfo.m_longitudes.at(m_softChannelId),
		currentEsuInfo.m_groundspeed, currentEsuInfo.m_groundcourses.at(m_softChannelId),
		currentEsuInfo.m_headings.at(m_softChannelId), currentEsuInfo.m_surfspeeds.at(m_softChannelId), currentEsuInfo.m_driftcourse, currentEsuInfo.m_driftspeed,
		currentEsuInfo.m_altitude, m_pParentTransducer->m_sounderId);
	m_outputs[esuId] = m_currentOutput;

	m_currentOutput = new EISupervisedOutput();
}

void EISupervisedChannel::Compute(EISupervisedResult & results)
{
	M3DKernel * pKern = M3DKernel::GetInstance();
	pKern->Lock();

	const EchoIntegrationParameter& echoIntegrationParams = m_pParentTransducer->m_pEchoIntegrationModule->GetEchoIntegrationParameter();
	const double offsetBottom = echoIntegrationParams.getOffsetBottom();

	// calcul de l'offset de surface
	double offsetSurface = 0.0f;
	auto firstSurfaceLayer = echoIntegrationParams.getFirstLayer(Layer::Type::SurfaceLayer);
	if (firstSurfaceLayer) {
		offsetSurface = firstSurfaceLayer->GetMinDepth();
	}

	bool weightedEchoIntegration = pKern->GetRefKernelParameter().getWeightedEchoIntegration();
    std::uint64_t nbPing = 0;
	double distanceTravelled = 0.0;
	double esuDistanceWidth = 0;
	std::int32_t nt = 0;
	double sumHeight = 0;

	double meanBottomDepth = 0;

	double dbInsonifiedVolume = 0;

	// pour chaque ping
    const std::uint64_t pingstart = results.GetPingStart();
    const std::uint64_t pingend = results.GetPingEnd();
		
    for (std::uint64_t pingId = pingstart; pingId <= pingend; pingId++)
	{
		PingFan* pFan = pKern->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingId);
		if (pFan)
		{
			Sounder * pSound = pFan->getSounderRef();
			if (pSound->m_SounderId == m_pParentTransducer->m_sounderId && m_pParentTransducer->m_transIdx != -1 && m_pParentTransducer->m_sounderId != -1)
			{
				nbPing++;

				Transducer * pTransducer = pSound->GetTransducer(m_pParentTransducer->m_transIdx);
				
				unsigned short chanId = pTransducer->GetChannelId()[m_iChannel];

				auto beam = pFan->getBeam(chanId)->second;
				bool skipBeam = false;
				// Si le channel est passif, il n'est pas pris en compte dans le calcul
				if (beam->m_transMode == BeamDataObject::PassiveTransducerMode)
				{
					M3D_LOG_INFO(LoggerName, "EISupervized - Passive Ping found, will not be used.");
					skipBeam = true;
				}

				double beamsSamplesSpacing = pTransducer->getBeamsSamplesSpacing();

				double pingDistance = pFan->getPingDistance();
				distanceTravelled += pingDistance;

				MemoryStruct *pMem = pFan->GetMemorySetRef()->GetMemoryStruct(m_pParentTransducer->m_transIdx);
				BaseMathLib::Vector2I size = pMem->GetDataFmt()->getSize();

				// boucle sur les echos
				DataFmt * pStart = pMem->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(m_iChannel, 0));

				// r�cup�ration de la profondeur du fond verticale pour chaque channel
				std::uint32_t echoFondEval = 0;
				std::int32_t bottomRange = 0;
				bool found = false;
				pFan->getBottom(m_softChannelId, echoFondEval, bottomRange, found);

				// Position du fond dans le rep�re monde
				//BaseMathLib::Vector3D a(0,0,0);      	
				//a.z= (bottomRange / 1000.0 / pTransducer->getBeamsSamplesSpacing() + pTransducer->GetSampleOffset() ) * pTransducer->getBeamsSamplesSpacing();
				//  BaseMathLib::Vector3D vecBottom = pSound->GetSoftChannelCoordinateToWorldCoord(pFan, m_transIdx, 0, a);
				// Position du fond dans le rep�re antenne LB 25/03/2015 on travaille dans le rep�re antenne
				// pour l'EIsupervis� comme pour l'EI couche
				BaseMathLib::Vector3D vecBottom = pSound->GetPolarToGeoCoord(pFan, m_pParentTransducer->m_transIdx, m_iChannel, echoFondEval - pTransducer->GetSampleOffset());

				meanBottomDepth += vecBottom.z;

				TransformMap *pTransform = pSound->GetTransformSetRef()->GetTransformMap(m_pParentTransducer->m_transIdx);
				SoftChannel *pSoftChan = pTransducer->getSoftChannelPolarX(m_iChannel);
				double compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);

				std::vector<unsigned int> echoes(size.y, 0);

				// Pré-calculs de positions des échos dans le repère monde
				std::vector<BaseMathLib::Vector3D> echoPos(size.y);
				for (int iEcho = 0; iEcho < size.y; iEcho++) {
					echoes[iEcho] = iEcho;
				}
				pSound->GetPolarToGeoCoord(pFan, m_pParentTransducer->m_transIdx, m_iChannel, echoes, echoPos);

				// parcours de chaque echo pour lui attribuer une appartenance � un banc, un polygone ou une couche
				for (int iEcho = 0; iEcho < size.y; ++iEcho)
				{
					// calcul de l'energie de l'echo
					double energy = 0.0f;
					double pondEnergy = 0.0f;

					const double echoDepth = echoPos[iEcho].z;
					const double echoDistance = iEcho * beamsSamplesSpacing;
										
					//////////////////////////////////
					// CALCUL DE L'ENERGY DE L'ECHO //
					//////////////////////////////////

					double value = *(pStart + iEcho);
					char *pFilter = pMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(m_iChannel, iEcho));
					if (*pFilter)
						value = UNKNOWN_DB;

					const double echoDB = value / 100.0;

					int surfaceLayerIdx = -1;
					int bottomLayerIdx = -1;
					int distanceLayerIdx = -1;

					/////////////////////////////////////////////////////////////////////////////////////////////////
					// Recherche de l'appartenance de l'echos aux diff�rents �lements : couches / bancs / polygone //
					/////////////////////////////////////////////////////////////////////////////////////////////////

					const auto& layers = echoIntegrationParams.getLayersDefs();
					const auto nbLayers = layers.size();

					for (size_t iLayer = 0; iLayer < nbLayers; ++iLayer)
					{
						const auto & pLayerDef = layers[iLayer];
						switch (pLayerDef->GetLayerType())
						{
						case Layer::Type::SurfaceLayer:
							if (m_pParentTransducer->m_isSurfaceLayerEnabled)
							{
								// verification de l'appartenance de l'echo au range
								if (echoDepth >= offsetSurface && echoDepth <= (vecBottom.z - offsetBottom))
								{
									if (echoDepth >= pLayerDef->GetMinDepth() && echoDepth <= pLayerDef->GetMaxDepth())
									{
										IClassificationResult * classificationResult = results.GetLayerClassificationResult(iLayer);
										assert(classificationResult);

										surfaceLayerIdx = iLayer;

										classificationResult->IncNt();
										classificationResult->IncSumHeight(beamsSamplesSpacing);

										nt++;
										sumHeight += beamsSamplesSpacing;
									}
								}
							}
							break;

						case Layer::Type::BottomLayer:
							if (m_pParentTransducer->m_isBottomLayerEnabled)
							{
								// verification de l'appartenance de l'echo au range
								if (echoDepth >= offsetSurface && echoDepth <= (vecBottom.z - offsetBottom))
								{
									if (echoDepth >= pLayerDef->GetMinDepth(vecBottom.z) && echoDepth <= pLayerDef->GetMaxDepth(vecBottom.z))
									{
										IClassificationResult * classificationResult = results.GetLayerClassificationResult(iLayer);
										assert(classificationResult);

										bottomLayerIdx = iLayer;

										classificationResult->IncNt();
										classificationResult->IncSumHeight(beamsSamplesSpacing);
									}
								}
							}
							break;

						case Layer::Type::DistanceLayer:
							if (m_pParentTransducer->m_isDistanceLayerEnabled)
							{
								if (echoDistance >= pLayerDef->GetMinDepth() && echoDistance <= pLayerDef->GetMaxDepth())
								{
									IClassificationResult * classificationResult = results.GetLayerClassificationResult(iLayer);
									assert(classificationResult);

									distanceLayerIdx = iLayer;

									classificationResult->IncNt();
									classificationResult->IncSumHeight(beamsSamplesSpacing);
								}
							}
							break;
						}
					}
						
					if (!skipBeam && echoIntegrationParams.getLowThreshold() <= echoDB && echoDB <= echoIntegrationParams.getHighThreshold())
					{
						if (surfaceLayerIdx != -1 || bottomLayerIdx != -1 || distanceLayerIdx != -1)
						{
							auto pEchoResult = results.AddEchoResult(pingId, iEcho);

							// On ajoute l'�nergie par faisceau et par couche
							energy = 1000000.0 * pow(10.0, ((double)value) / 1000.0);

							// Ponderation par la distance parcourue
							if (weightedEchoIntegration)
							{
								energy *= beamsSamplesSpacing * pingDistance;
							}
							pEchoResult->m_energy = energy;

							if (surfaceLayerIdx != -1)
							{
								IClassificationResult * classificationResult = results.GetLayerClassificationResult(surfaceLayerIdx);
								pEchoResult->m_surfaceLayerIdx = surfaceLayerIdx;
								classificationResult->AddEcho(pEchoResult);
							}

							if (bottomLayerIdx != -1)
							{
								IClassificationResult * classificationResult = results.GetLayerClassificationResult(bottomLayerIdx);
								pEchoResult->m_bottomLayerIdx = bottomLayerIdx;
								classificationResult->AddEcho(pEchoResult);
							}

							if (distanceLayerIdx != -1)
							{
								IClassificationResult * classificationResult = results.GetLayerClassificationResult(distanceLayerIdx);
								pEchoResult->m_distanceLayerIdx = distanceLayerIdx;
								classificationResult->AddEcho(pEchoResult);
							}
						}
					}
				}

				///////////////////////////////
				// CALCUL DES INFOS DE L'ESU //
				///////////////////////////////
				
				bool hasSurfaceLayer = false;
				double globalMinDepth = 0;
				double globalMaxDepth = 0;

				// calcul volume et surface
				const auto& layers = echoIntegrationParams.getLayersDefs();
				const auto nbLayers = layers.size();

				for (size_t iLay = 0; iLay < nbLayers; ++iLay)
				{
					LayerClassificationResult * pLayerResult = (LayerClassificationResult*)results.GetLayerClassificationResult(iLay);

					const auto & layerDef = layers[iLay];

					double minDepth;
					double maxDepth;

					// OTK - FAE044 - dans le cas des couches en distance, le calcul est diff�rent
					if (layerDef->GetLayerType() != Layer::Type::DistanceLayer)
					{
						// on met � jour la profondeur moyenne de la couche au moment de la fin de l'ESU
						pLayerResult->m_depth = (layerDef->GetMaxDepth(vecBottom.z) + layerDef->GetMinDepth(vecBottom.z)) / 2;

						// on met � jour la position lat std::int32_t de la couche pour le dernier ping de l'ESU
						BaseMathLib::Vector3D layerPos = pSound->GetSoftChannelCoordinateToGeoCoord(
							pFan, m_pParentTransducer->m_transIdx, m_iChannel, BaseMathLib::Vector3D(0, 0, pLayerResult->m_depth));

						pLayerResult->m_latitude = layerPos.x;
						pLayerResult->m_longitude = layerPos.y;

						double offset = pTransducer->m_transDepthMeter + pFan->getHeaveChan(pSoftChan->m_softChannelId);
						minDepth = layerDef->GetMinDepth(vecBottom.z) - offset;
						maxDepth = layerDef->GetMaxDepth(vecBottom.z) - offset;
						
						if (hasSurfaceLayer)
						{
							globalMinDepth = std::min(globalMinDepth, minDepth);
							globalMaxDepth = std::max(globalMaxDepth, maxDepth);
						}
						else
						{
							hasSurfaceLayer = true;
							globalMinDepth = minDepth;
							globalMaxDepth = maxDepth;
						}

						minDepth = std::max(.0, minDepth);
						maxDepth = std::max(.0, maxDepth);

						// application du d�pointage transversal
						minDepth = minDepth / cos(pSoftChan->m_mainBeamAthwartSteeringAngleRad);
						maxDepth = maxDepth / cos(pSoftChan->m_mainBeamAthwartSteeringAngleRad);

					}
					else
					{
						// cas des couches en distance.
						unsigned int meanSampleNumber = (unsigned int)(((layerDef->GetMaxDepth() + layerDef->GetMinDepth()) / 2.0 + 0.5) 
							/ pTransducer->getBeamsSamplesSpacing());

						BaseMathLib::Vector3D layerPos = pSound->GetPolarToGeoCoord(pFan, m_pParentTransducer->m_transIdx, m_iChannel, meanSampleNumber);

						pLayerResult->m_latitude = layerPos.x;
						pLayerResult->m_longitude = layerPos.y;
						pLayerResult->m_depth = layerPos.z;

						minDepth = layerDef->GetMinDepth();
						maxDepth = layerDef->GetMaxDepth();
					}


					double volume = (PI / 3.)*tan(pSoftChan->m_beam3dBWidthAlongRad / 2.)
						*tan(pSoftChan->m_beam3dBWidthAthwartRad / 2.)
						*((maxDepth*maxDepth*maxDepth) - (minDepth*minDepth*minDepth));

					pLayerResult->m_volume = volume;
					pLayerResult->m_surface = volume / (maxDepth - minDepth);
				}

				if (hasSurfaceLayer)
				{
					double offset = pTransducer->m_transDepthMeter + pFan->getHeaveChan(pSoftChan->m_softChannelId);
                    double minDepth = std::max(0., globalMinDepth);
                    double maxDepth = std::max(.0, globalMaxDepth);

					double meanDepth = (maxDepth - minDepth) * 0.5;

					// on met � jour la position lat std::int32_t de la couche pour le dernier ping de l'ESU
					BaseMathLib::Vector3D layerPos = pSound->GetSoftChannelCoordinateToGeoCoord(
						pFan, m_pParentTransducer->m_transIdx, m_iChannel, BaseMathLib::Vector3D(0, 0, meanDepth));

					results.m_latitude = layerPos.x;
					results.m_longitude = layerPos.y;

					// OTK - FAE166 - ajout du d�pointage transversal pour le volume insonnifi� par le total des couches de surfaces
					// puisque l'approcimation faite (non prise en compte du d�pointage transversal) devient tr�s fausse dans le cas des 
					// voies multifaisceaux non centrales

					// application du d�pointage transversal
					minDepth = minDepth / cos(pSoftChan->m_mainBeamAthwartSteeringAngleRad);
					maxDepth = maxDepth / cos(pSoftChan->m_mainBeamAthwartSteeringAngleRad);

					double volume = (PI / 3.)*tan(pSoftChan->m_beam3dBWidthAlongRad / 2.)
						*tan(pSoftChan->m_beam3dBWidthAthwartRad / 2.)
						*((maxDepth*maxDepth*maxDepth) - (minDepth*minDepth*minDepth));

					results.m_volume = volume;
					results.m_surface = volume / (maxDepth - minDepth);

					results.SetEsuSampleDepthSI(beamsSamplesSpacing);

					// OTK - FAE166 - pour le calcul de moyen du sA sur tous les faisceaux, on utilise le volume insonifi�
					// sur l'ensemble de l'ESU en sommant donc le volume de chaque ping
					dbInsonifiedVolume += volume;
				}
			} // fin si le sondeur correspond
		} // fin si pingfan
	} // fin boucle sur les pings

	results.SetNbPing(nbPing);
	results.SetEsuDistanceWidth(distanceTravelled);
	results.SetNt(nt);
	results.SetTotalSumHeight(sumHeight);
	results.SetBottomDepth(meanBottomDepth / nbPing);

	results.m_insonifiedVolume = dbInsonifiedVolume;

	pKern->Unlock();
}

bool EISupervisedChannel::AddPolygon(BaseMathLib::Poly2D * polygon, std::int32_t minPing, std::int32_t maxPing)
{
	bool bAdded = false;
	//recherche de l'esu incluant le polygone
	EISupervisedResult * pResult = NULL;
	std::uint32_t esuId;
	for (MapResultByEsu::iterator iterResult = m_results.begin(); iterResult != m_results.end(); iterResult++)
	{
		EISupervisedResult * pTmpResult = iterResult->second;
		if (minPing >= pTmpResult->GetPingStart() && minPing <= pTmpResult->GetPingEnd())
		{
			pResult = pTmpResult;
			esuId = iterResult->first;
			break;
		}
	}

	if (pResult != NULL)
	{
		// Cr�ation de la classification
		PolygonClassificationResult * classificationResult = new PolygonClassificationResult();
		classificationResult->m_pPolygon = polygon;
		pResult->AddClassificationResult(classificationResult);
		bAdded = true;

		M3DKernel * pKern = M3DKernel::GetInstance();
		pKern->Lock();

		EchoIntegrationParameter& echoIntegrationParams = m_pParentTransducer->m_pEchoIntegrationModule->GetEchoIntegrationParameter();

		int sounderId = -1;
		int transId = -1;

		// OTK - FAE259 - gestion de l'�criture des fichiers pour les polygones
		EISupervisedPolygonOutput * pPolygonOutput = new EISupervisedPolygonOutput(polygon, minPing, maxPing, m_pParentTransducer->m_transIdx, m_softChannelId,
			echoIntegrationParams, m_outputs[esuId], m_pParentTransducer->getCentralSupervisedChannel());
		m_polygonOutputs[polygon] = pPolygonOutput;

		// recherche des echos appartenant au polygone
		// pour chaque ping
		int nbPings = 0;
        for (std::uint64_t pingId = minPing; pingId <= maxPing; pingId++)
		{
			PingFan* pFan = pKern->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingId);
			if (pFan)
			{
				Sounder * pSound = pFan->getSounderRef();
				// search sounder and transducer id
				if (sounderId == -1)
				{
					int transducerCount = pSound->GetTransducerCount();
					for (int iTrans = 0; iTrans < transducerCount; iTrans++)
					{
						if (pSound->GetTransducer(m_pParentTransducer->m_transIdx)->m_transName == m_pParentTransducer->m_TransducerName)
						{
							sounderId = pSound->m_SounderId;
							transId = iTrans;
							break;
						}
					}
				}

				if (pSound->m_SounderId == sounderId && transId != -1 && sounderId != -1)
				{
					MemoryStruct *pMem = pFan->GetMemorySetRef()->GetMemoryStruct(transId);
					BaseMathLib::Vector2I size = pMem->GetDataFmt()->getSize();

					Transducer * pTransducer = pSound->GetTransducer(m_pParentTransducer->m_transIdx);

					double dbMinDepthForPing = -1;
					double dbMaxDepthForPing = -1;
					double offset = pTransducer->m_transDepthMeter + pFan->getHeaveChan(m_softChannelId);

					// parcours de chaque echo pouvant appartenir au polygon
					for (int iEcho = 0; iEcho < size.y; iEcho++)
					{
						// verification qu'il appartient au polygone
						double depth = pSound->GetEchoDistance(pFan, m_pParentTransducer->m_transIdx, m_iChannel, iEcho);
						double tabPoint[2] = { pingId, depth };
						if (BaseMathLib::GeometryTools2D::PointInPolygon(polygon->GetPoints(), polygon->GetNbPoints(), tabPoint))
						{
							EchoResult * pEchoResult = pResult->GetEchoResult(pingId, iEcho);
							if (pEchoResult != NULL)
							{
								pEchoResult->m_pPolygon = polygon;
								classificationResult->AddEcho(pEchoResult);
							}

							// OTK - FAE 259 - pour pouvoir sortir le nt et le sa et sv dans les fichiers d�di�s aux polygones
							classificationResult->IncNt();
							classificationResult->IncSumHeight(pTransducer->getBeamsSamplesSpacing());

							// On calcule �galement le volume et l'aire insonifi� correspondant au polygone
							if (dbMinDepthForPing == -1)
							{
								dbMinDepthForPing = depth - offset;
							}
							dbMaxDepthForPing = depth - offset;
						}
					}

					// OTK - FAE 259 - calcul du volume et aire insonnifi�s par le polygone
					if (dbMinDepthForPing != -1)
					{
						dbMinDepthForPing = std::max<double>(0, dbMinDepthForPing);
						dbMaxDepthForPing = std::max<double>(0, dbMaxDepthForPing);

						if (dbMaxDepthForPing > dbMinDepthForPing)
						{
							SoftChannel * pSoftChan = pTransducer->getSoftChannel(m_softChannelId);
							double volume = (PI / 3.)*tan(pSoftChan->m_beam3dBWidthAlongRad / 2.)
								*tan(pSoftChan->m_beam3dBWidthAthwartRad / 2.)
								*((dbMaxDepthForPing*dbMaxDepthForPing*dbMaxDepthForPing) - (dbMinDepthForPing*dbMinDepthForPing*dbMinDepthForPing));
							classificationResult->m_volume += volume;
							classificationResult->m_surface += volume / (dbMaxDepthForPing - dbMinDepthForPing);
							nbPings++;
						}
					}
				}
			}
		}
		// OTK - FAE 259 - On normalise le volume et la surface du polygone par le nombre de pings ajout�s
		classificationResult->m_volume /= nbPings;
		classificationResult->m_surface /= nbPings;
		pKern->Unlock();
	}
	return bAdded;
}

void EISupervisedChannel::DeletePolygon(BaseMathLib::Poly2D *polygon, std::int32_t minPing, std::int32_t maxPing)
{
	//recherche de l'esu correspondante
	EISupervisedResult * pResult = NULL;

	for (MapResultByEsu::iterator iterResult = m_results.begin(); iterResult != m_results.end(); iterResult++)
	{
		EISupervisedResult * pTmpResult = iterResult->second;
		if (minPing >= pTmpResult->GetPingStart() && minPing <= pTmpResult->GetPingEnd())
		{
			pResult = pTmpResult;
			break;
		}
	}

	if (pResult != NULL)
	{
		// suppression de la classification
		pResult->EraseClassificationForPolygon(polygon);
	}
}

