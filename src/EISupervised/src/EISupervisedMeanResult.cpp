
#include "EISupervised/EISupervisedMeanResult.h"

EISupervisedMeanResult::EISupervisedMeanResult()
{
}

EISupervisedMeanResult::~EISupervisedMeanResult()
{
}

double EISupervisedMeanResult::GetRemainingSa() const
{
	return m_dbRemainingSa;
}

double EISupervisedMeanResult::GetTotalSa() const
{
	return m_dbTotalSa;
}

double EISupervisedMeanResult::GetClassificationSa(int classId) const
{
	double dbSa = 0;
	std::map<int, double>::const_iterator iter = m_mapClassifiedSa.find(classId);
	if (iter != m_mapClassifiedSa.end())
	{
		dbSa = iter->second;
	}
	return dbSa;
}

double EISupervisedMeanResult::GetSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const
{
	return m_dbTotalSa;
}

double EISupervisedMeanResult::GetFreeSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const
{
	return m_dbRemainingSa;
}

double EISupervisedMeanResult::GetClassifiedSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons, int classId) const
{
	return GetClassificationSa(classId);
}

void EISupervisedMeanResult::SetTotalSa(double dbSa)
{
	m_dbTotalSa = dbSa;
}
void EISupervisedMeanResult::SetRemainingSa(double dbSa)
{
	m_dbRemainingSa = dbSa;
}
void EISupervisedMeanResult::SetClassifiedSa(const std::map<int, double> & classifiedSa)
{
	m_mapClassifiedSa = classifiedSa;
}
