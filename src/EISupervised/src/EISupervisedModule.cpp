

#include "EISupervised/EISupervisedModule.h"
#include "EISupervised/EISupervisedOutput.h"
#include "EISupervised/EISupervisedTransducer.h"
#include "EISupervised/ClassificationResult.h"
#include "EISupervised/EISupervisedMovFileOutput.h"
#include "EISupervised/EISupervisedPolygonFileOutput.h"
#include "EISupervised/EISupervisedPolygonOutput.h"

#include "EchoIntegration/EchoIntegrationModule.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/parameter/TramaDescriptor.h"
#include "M3DKernel/utils/M3DStdUtils.h"

#include "BaseMathLib/geometry/GeometryTools2D.h"

#ifdef WIN32
#include <direct.h>
#endif

EISupervisedModule::EISupervisedModule()
	: ProcessModule("EISupervisedModule")
	, m_pEchoIntegrationModule(nullptr)
{
	m_ESURunning = false;
	m_DisableNextESU = false;

	m_initialized = false;

	setEnable(false);
}

EISupervisedModule::~EISupervisedModule(void)
{
	// destruction des ei supervis�e par transducteur
	for (MapEITransducers::const_iterator it = m_eiSupervisedTransducers.begin(); it != m_eiSupervisedTransducers.end(); ++it)
	{
		delete it->second;
	}

	// destruction des sorties CSV
	for (MapEIOutputByTransducers::const_iterator it = m_outputByTransducers.begin(); it != m_outputByTransducers.end(); ++it)
	{
		delete it->second;
	}
}

void EISupervisedModule::UpdateInitialized()
{
	bool allInitialzed = true;
	MapEITransducers::const_iterator it = m_eiSupervisedTransducers.begin();
	while (it != m_eiSupervisedTransducers.end())
	{
		if (it->second->m_initialized == false)
		{
			allInitialzed = false;
			break;
		}
		++it;
	}
	m_initialized = allInitialzed;
}

void EISupervisedModule::PingFanAdded(PingFan * pFan)
{
	if (getEnable() && m_ESURunning)
	{
		Sounder *pSounder = pFan->getSounderRef();
		if (!m_initialized)
		{
			int transducerCount = pSounder->GetTransducerCount();
			for (int iTrans = 0; iTrans < transducerCount; iTrans++)
			{
				Transducer * pTransducer = pSounder->GetTransducer(iTrans);
				EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(pTransducer->m_transName);
				if (pEISupervisedTransducer != NULL)
				{
					if (pEISupervisedTransducer->m_initialized == false)
					{
						pEISupervisedTransducer->m_sounderId = pSounder->m_SounderId;
						pEISupervisedTransducer->m_transIdx = iTrans;
						pEISupervisedTransducer->Initialize(pTransducer);
						UpdateInitialized();
					}
				}
			}
		}

		MapEITransducers::const_iterator it = m_eiSupervisedTransducers.begin();
		while (it != m_eiSupervisedTransducers.end())
		{
			if (pSounder->m_SounderId == it->second->m_sounderId)
			{
				it->second->m_currentEsuInfo.Update(pFan);
				Transducer * pTrans = pSounder->GetTransducer(it->second->m_transIdx);
				if (pTrans != NULL)
				{
					it->second->UpdateOutputSndSet(pTrans, pSounder->m_soundVelocity);
				}
			}
			++it;
		}
	}
}

void EISupervisedModule::SounderChanged(std::uint32_t)
{
}

void EISupervisedModule::StreamClosed(const char *streamName)
{
}

void EISupervisedModule::StreamOpened(const char *streamName)
{
}

void EISupervisedModule::setEnable(bool enable)
{
	// activation du module d'echo integration
	if (m_pEchoIntegrationModule != NULL)
	{
		m_pEchoIntegrationModule->setEnable(enable);
	}

	if (enable)
	{
		// destruction des ei supervis�e par transducteur
		for (MapEITransducers::const_iterator it = m_eiSupervisedTransducers.begin(); it != m_eiSupervisedTransducers.end(); ++it)
		{
			delete it->second;
		}
		m_eiSupervisedTransducers.clear();

		// destruction des sorties CSV
		for (MapEIOutputByTransducers::const_iterator it = m_outputByTransducers.begin(); it != m_outputByTransducers.end(); ++it)
		{
			delete it->second;
		}
		m_outputByTransducers.clear();

		m_initialized = false;

		// cr�ation des r�sultats pour chaque transducer
        for(const auto& transducerLayerConf : m_parameter.getTransducers())
		{
			const auto transName = transducerLayerConf.first;
        	const auto& layerConf = transducerLayerConf.second;
			if (m_eiSupervisedTransducers.find(transName) == m_eiSupervisedTransducers.end())
			{
				auto pEISupervisedTransducer = new EISupervisedTransducer;
				pEISupervisedTransducer->m_TransducerName = transName;
				pEISupervisedTransducer->m_pEchoIntegrationModule = m_pEchoIntegrationModule;
				pEISupervisedTransducer->m_isSurfaceLayerEnabled = layerConf.surface;
				pEISupervisedTransducer->m_isDistanceLayerEnabled = layerConf.distance;
				pEISupervisedTransducer->m_isBottomLayerEnabled = layerConf.bottom;
				m_eiSupervisedTransducers[transName] = pEISupervisedTransducer;
			}
		}
	}

	ProcessModule::setEnable(enable);
}

// indique au module qu'il doit se d�sactiver � la fin de l'ESU courant.
void EISupervisedModule::DisableForNextESU()
{
	if (!getEnable())
		return;

	m_DisableNextESU = true;
}

void EISupervisedModule::SetEchoIntegrationModule(EchoIntegrationModule * pEchoIntegrationModule)
{
	m_pEchoIntegrationModule = pEchoIntegrationModule;
	if (pEchoIntegrationModule != nullptr)
	{
		m_parameter.setEchoIntegrationParameter(&pEchoIntegrationModule->GetEchoIntegrationParameter());
	}
}

void EISupervisedModule::ESUStart(ESUParameter * pWorkingESU)
{
	// si le traitement doit s'arr�ter, on arr�te.
	if (m_DisableNextESU)
	{
		setEnable(false);
		m_DisableNextESU = false;
	}

	if (!getEnable())
	{
		return;
	}

	m_ESURunning = true;

	MapEITransducers::const_iterator it = m_eiSupervisedTransducers.begin();
	while (it != m_eiSupervisedTransducers.end())
	{
		it->second->m_currentEsuInfo.m_timestart = pWorkingESU->GetESUTime();
		it->second->m_currentEsuInfo.m_diststart = pWorkingESU->GetESUDistance();

		// on reset la classe pour la gestion des r�sultats et des sorties
		for (size_t i = 0; i < it->second->m_Channels.size(); i++)
		{
			EISupervisedChannel * pChannel = it->second->m_Channels[i];
			if (pChannel->m_currentOutput)
			{
				delete pChannel->m_currentOutput;
			}
			pChannel->m_currentOutput = new EISupervisedOutput();
		}

		++it;
	}
}


void EISupervisedModule::ESUEnd(ESUParameter *pWorkingESU, bool abort)
{
	if (!getEnable() || !m_ESURunning)
	{
		m_ESURunning = false;
		return;
	}

	m_ESURunning = false;

	if (!abort)
	{
		MapEITransducers::const_iterator it = m_eiSupervisedTransducers.begin();
		while (it != m_eiSupervisedTransducers.end())
		{
			it->second->AddEIResult(pWorkingESU->GetESUId(), m_parameter);
			it->second->m_currentEsuInfo.Reset();
			++it;
		}
	}
}

bool EISupervisedModule::IsEISupervisedEnabled(const std::string & transName) const
{
	const auto& layerConf = m_parameter.getTransducerLayerConf(transName);
	return layerConf != nullptr && layerConf->isActive();
}

bool EISupervisedModule::IsEISupervisedEnabled(const std::string & transName, Layer::Type layerType) const
{
	const auto& layerConf = m_parameter.getTransducerLayerConf(transName);
	if (layerConf == nullptr)
	{
		return false;
	}
	
	switch (layerType)
	{
	case Layer::Type::SurfaceLayer:
		return layerConf->surface;

	case Layer::Type::DistanceLayer:
		return layerConf->distance;

	case Layer::Type::BottomLayer:
		return layerConf->bottom;

	default:
		return false;
	}
}

EISupervisedTransducer * EISupervisedModule::getEISupervisedTransducer(const std::string & transName) const
{
	EISupervisedTransducer * pEISupervisedTransducer = NULL;
	MapEITransducers::const_iterator it = m_eiSupervisedTransducers.find(transName);
	if (it != m_eiSupervisedTransducers.end())
	{
		pEISupervisedTransducer = it->second;
	}
	return pEISupervisedTransducer;
}

void EISupervisedModule::AddPolygon(const std::string & transName, BaseMathLib::Poly2D * polygon)
{
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		pEISupervisedTransducer->AddPolygon(polygon);
	}
}

void EISupervisedModule::DeletePolygon(const std::string & transName, BaseMathLib::Poly2D *polygon)
{
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		pEISupervisedTransducer->DeletePolygon(polygon);
	}
}

std::uint32_t EISupervisedModule::GetEsuForPolygon(const std::string & transName, BaseMathLib::Poly2D *polygon)
{
	std::uint32_t esuId = -1;
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		esuId = pEISupervisedTransducer->GetEsuForPolygon(polygon);
	}
	return esuId;
}

MapResultByEsu EISupervisedModule::GetResults(const std::string & transName, const ClassificationVector& classifications,
	bool buseVolumes) const
{
	MapResultByEsu results;
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		results = pEISupervisedTransducer->GetMeanResults(classifications, buseVolumes);
	}
	return results;
}

PolygonSet EISupervisedModule::GetPolygons(const std::string & transName) const
{
	PolygonSet polygons;
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		polygons = pEISupervisedTransducer->m_polygons;
	}
	return polygons;
}

EISupervisedResult * EISupervisedModule::GetCentralResultFromEsu(const std::string & transName, std::uint32_t esuId) const
{
	EISupervisedResult * pResult = NULL;
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		pResult = pEISupervisedTransducer->GetCentralResultFromEsu(esuId);
	}
	return pResult;
}

EISupervisedResult * EISupervisedModule::GetMeanResultFromEsu(const std::string & transName, std::uint32_t esuId, const ClassificationVector& classifications,
	const std::vector<int> & layers, const std::vector<BaseMathLib::Poly2D*> & polygons,
	bool bUseVolumes) const
{
	EISupervisedResult * pResult = NULL;
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		pResult = pEISupervisedTransducer->GetMeanResultFromEsu(esuId, classifications, layers, polygons, bUseVolumes);
	}
	return pResult;
}

std::vector<EISupervisedResult*> EISupervisedModule::GetResultsFromEsu(const std::string & transName, std::uint32_t esuId) const
{
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		return pEISupervisedTransducer->GetResultsFromEsu(esuId);
	}
	return std::vector<EISupervisedResult*>();
}

void EISupervisedModule::Reset(const std::string & transName, const EsuIdVector & esuIdVector)
{
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		pEISupervisedTransducer->Reset(esuIdVector);
	}
}

void EISupervisedModule::SaveResults(const std::string & transName, const EsuIdVector & esuIdVector,
	EISupervisedMovFileOutput * pFileOutput, bool bCreatedFileOutput, const std::vector<EISupervisedPolygonFileOutput*> & polygonFilesOutputs)
{
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		// Descripteur de trames
		BaseKernel::TramaDescriptor & tramaDescriptor = m_parameter.GetTramaDescriptor();

		if (bCreatedFileOutput)
		{
			m_outputByTransducers[transName] = pFileOutput;

			// serialisation du header - on prend le premier output venu
			if (esuIdVector.size() > 0)
			{
				EsuIdVector::const_iterator it = esuIdVector.begin();
				EISupervisedChannel * pChannel = pEISupervisedTransducer->getCentralSupervisedChannel();
				if (pChannel)
				{
					EISupervisedOutput * output = pChannel->m_outputs[*it];
					output->CSVHeaderSerialize(pFileOutput, tramaDescriptor);
				}
			}
		}

        for(int esuId : esuIdVector)
		{
			// Sauvegarde des r�sultats de tous les faisceaux
			for (size_t iChan = 0; iChan < pEISupervisedTransducer->m_Channels.size(); iChan++)
			{
				EISupervisedOutput * output = pEISupervisedTransducer->m_Channels[iChan]->m_outputs[esuId];
				output->CSVSerialize(pFileOutput, tramaDescriptor);
			}

			// OTK - FAE259 - sauvegarde des coordonn�es des polygones
			// Ecriture des fichiers d�di�s aux polygones � sauvegarder pour l'ESU :
			for (size_t iPoly = 0; iPoly < polygonFilesOutputs.size(); iPoly++)
			{
				EISupervisedPolygonFileOutput * pPolyOutputFile = polygonFilesOutputs[iPoly];
				if (pPolyOutputFile->getESUId() == esuId)
				{
					for (size_t iChan = 0; iChan < pPolyOutputFile->getChannelIndexes().size(); iChan++)
					{
						EISupervisedChannel * pChannel = pEISupervisedTransducer->m_Channels[pPolyOutputFile->getChannelIndexes()[iChan]];
						EISupervisedPolygonOutput * output = pChannel->m_polygonOutputs[pPolyOutputFile->getPolygon()];
						PolygonClassificationResult * pPolygonResult = pChannel->m_results[esuId]->GetPolygonClassificationResult(pPolyOutputFile->getPolygon());

						// �criture des titres des colonnes 
						if (iChan == 0)
						{
							output->CSVHeaderSerialize(pPolyOutputFile, tramaDescriptor);
						}

						output->CSVSerialize(pPolyOutputFile, tramaDescriptor, pPolygonResult);
					}
				}
			}


			// OTK - FAE 2057 - on marque le r�sultat comme d�j� sauvegard� pour l'esu et le transducteur correspondant.
			EISupervisedResult * result = pEISupervisedTransducer->GetCentralResultFromEsu(esuId);
			if (result)
			{
				result->SetAlreadySaved(true);
			}
		}
	}
}

EISupervisedMovFileOutput * EISupervisedModule::GetDestinationFile(const std::string & transName, const EsuIdVector & esuIdVector, bool & bCreatedFileOutput,
	std::vector<EISupervisedPolygonFileOutput*> & polygonFileOutputs)
{
	bCreatedFileOutput = false;
	EISupervisedMovFileOutput * fileOutput = NULL;
	EISupervisedTransducer * pEISupervisedTransducer = getEISupervisedTransducer(transName);
	if (pEISupervisedTransducer != NULL)
	{
		// construction du chemin de sortie
		EISupervisedParameter & eiSupervisedParameter = GetEISupervisedParameter();

		// Descripteur de trames
		BaseKernel::TramaDescriptor & tramaDescriptor = m_parameter.GetTramaDescriptor();

		MapEIOutputByTransducers::iterator itOut = m_outputByTransducers.find(transName);
		if (itOut != m_outputByTransducers.end())
		{
			if (eiSupervisedParameter.isAppendResults())
			{
				fileOutput = itOut->second;
			}
			else
			{
				// on supprime la sortie existante
				delete itOut->second;
				m_outputByTransducers.erase(itOut);
			}
		}

		if (fileOutput == NULL)
		{
			BaseKernel::ParameterBroadcastAndRecord & broadcastAndRecordParameter = eiSupervisedParameter.GetParameterBroadcastAndRecord();
			BaseKernel::ParameterBroadcastAndRecord::TTramaType tramaType = BaseKernel::ParameterBroadcastAndRecord::CSV_TRAMA;
			
			// NMD FAE 2300 : ajout de la frequence et de l'id du channel dans le nom du fichier de sortie
			int transducerId = pEISupervisedTransducer->getCentralSoftChannelId();
			int frequency = pEISupervisedTransducer->getCentralTransducerFrequency();
			fileOutput = new EISupervisedMovFileOutput(broadcastAndRecordParameter.m_filePath, broadcastAndRecordParameter.m_filePrefix, frequency, transducerId, broadcastAndRecordParameter.m_prefixDate);
			bCreatedFileOutput = true;
		}

		// Cr�ation du r�pertoire des sortie pour les fichiers screenshots
		std::string screenshotsDirectoryPath = fileOutput->getDirectoryPath();
		if (screenshotsDirectoryPath.substr(screenshotsDirectoryPath.length() - 1, 1).compare("\\"))
		{
			screenshotsDirectoryPath.append("\\");
		}
		screenshotsDirectoryPath.append("screenshots");
		_mkdir(screenshotsDirectoryPath.c_str());

		// OTK - FAE259 - cr�ation des MovFileOutput pour les polygones. Contrairement au fichier "_man" classique, les fichiers polygones n'existent
		// forc�ment pas d�j� en mode append car ils ne sont associ�s qu'� un ESU.
		// On d�termine la liste des polygones qui sont classifi�s et qui correspondent aux ESUs � �crire
		std::map<std::uint32_t, std::uint32_t> polygonIDsByESU;
		for (PolygonSet::const_iterator iterPoly = pEISupervisedTransducer->m_polygons.begin(); iterPoly != pEISupervisedTransducer->m_polygons.end(); ++iterPoly)
		{
			// On ignore les polygones qui ne font pas partie des ESU � sauvegarder.
			std::uint32_t polygonESU = pEISupervisedTransducer->GetEsuForPolygon(*iterPoly);
			if (std::find(esuIdVector.begin(), esuIdVector.end(), polygonESU) != esuIdVector.end())
			{
				// Recherche des channels pour lesquels le polygon a �t� classifi�:
				std::vector<size_t> lstPolygonChannels;
				for (size_t iChan = 0; iChan < pEISupervisedTransducer->m_Channels.size(); iChan++)
				{
					EISupervisedResult * pResult = pEISupervisedTransducer->m_Channels[iChan]->m_results[polygonESU];
					if (pResult)
					{
						PolygonClassificationResult * pClassifiedResult = pResult->GetPolygonClassificationResult(*iterPoly);
						if (pClassifiedResult && pClassifiedResult->IsClassified())
						{
							lstPolygonChannels.push_back(iChan);
						}
					}
				}

				// Si on a au moins un channel avec des r�sultats pour le polygon, on pr�pare
				// le MovFileOutput qui servira � �crire le fichier pour le polygone
				if (!lstPolygonChannels.empty())
				{
					// Gestion d'un num�ro d'ID des polygon par ESU pour construire le nom des fichiers polygones
					std::uint32_t iPolygonID;
					std::map<std::uint32_t, std::uint32_t>::iterator iterPolyID = polygonIDsByESU.find(polygonESU);
					if (iterPolyID == polygonIDsByESU.end())
					{
						iPolygonID = 1;
						polygonIDsByESU[polygonESU] = iPolygonID;
					}
					else
					{
						iPolygonID = ++iterPolyID->second;
					}
					
					// Cr�ation du r�pertoire des sortie pour les fichiers CSV polygons
					std::string polygonsDirectoryPath = fileOutput->getDirectoryPath();
					if (polygonsDirectoryPath.substr(polygonsDirectoryPath.length() - 1, 1).compare("\\"))
					{
						polygonsDirectoryPath.append("\\");
					}
					polygonsDirectoryPath.append("polygons");
					_mkdir(polygonsDirectoryPath.c_str());

					EISupervisedPolygonFileOutput * pPolygonOutput = new EISupervisedPolygonFileOutput(
						polygonsDirectoryPath, fileOutput->getFileNamePrefix(), fileOutput->getFrequency(), fileOutput->getId(), fileOutput->getFileDatePrefix()
						, polygonESU, iPolygonID, *iterPoly, lstPolygonChannels);
					polygonFileOutputs.push_back(pPolygonOutput);
				}
			}
		}
	}

	return fileOutput;
}

