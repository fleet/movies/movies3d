
#include "EISupervised/EISupervisedMovFileOutput.h"

#include <sstream>

EISupervisedMovFileOutput::EISupervisedMovFileOutput()
	:MovFileOutput()
{
}

EISupervisedMovFileOutput::EISupervisedMovFileOutput(
	std::string directoryPath,
	std::string fileNamePrefix,
	int frequency,
	int id,
	bool prefixDate)
	: MovFileOutput()
	, m_directoryPath(directoryPath)
	, m_fileNamePrefix(fileNamePrefix)
	, m_frequency(frequency)
	, m_id(id)
{
	// chemin + rpefix des fichiers correspondant au flux
	// creation du chemin cible
	std::string filePrefix = directoryPath;
	if (filePrefix.substr(filePrefix.length() - 1, 1).compare("\\"))
	{
		filePrefix.append("\\");
	}
	filePrefix.append(fileNamePrefix);

	m_FilePrefix = filePrefix;

	// suffixe
	std::ostringstream oStrFileSuffix;
	oStrFileSuffix
		<< "id" << id
		<< "_" << frequency << "kHz"
		<< "_man";

	m_FileSuffix = oStrFileSuffix.str();

	// type de fichier 
	m_TramaType = BaseKernel::ParameterBroadcastAndRecord::CSV_TRAMA;

	// Prefixage de la date ?
	m_FileDatePrefix = prefixDate;

	// construction du nom complet du fichier.
	ConstructNewFileName();
}

EISupervisedMovFileOutput::~EISupervisedMovFileOutput()
{
}

void EISupervisedMovFileOutput::ConstructNewFileName()
{
	return MovFileOutput::ConstructNewFileName();
}