
#include "EISupervised/EISupervisedOutput.h"
#include "EISupervised/EISupervisedResult.h"
#include "EISupervised/ClassificationResult.h"
#include "M3DKernel/datascheme/Transducer.h"

#include "MovNetwork/MvNetDataXML.h"
#include "MovNetwork/MvNetDataXMLLayer.h"
#include "MovNetwork/MvNetDataXMLCellset.h"
#include "MovNetwork/MvNetDataXMLHeading.h"
#include "MovNetwork/MvNetDataXMLSndset.h"
#include "M3DKernel/datascheme/DateTime.h"

#include "M3DKernel/output/MovOutput.h"

#include <algorithm>
#include <time.h>

// Initialisation des variables statiques pour les descripteurs CSV
bool EISupervisedOutput::m_descInitialized = false;
sndsetDescriptor EISupervisedOutput::m_snddesc;
shipnavDescriptor EISupervisedOutput::m_shipnavdesc;
cellsetDescriptor EISupervisedOutput::m_cellsetdesc;
layerDescriptor EISupervisedOutput::m_layerdesc;

EISupervisedOutput::EISupervisedOutput()
{
}

EISupervisedOutput::~EISupervisedOutput()
{
}

const CMvNetDataXMLSndset & EISupervisedOutput::getSndSet() const
{
	return m_MvNetDataXMLSndSet;
}

void EISupervisedOutput::UpdateSndSet(
	Transducer * pTrans,
	unsigned short channelID,
	double soundVelocity)
{
	SoftChannel * pSoft = pTrans->getSoftChannel(channelID);

	// TRAMES SNDSET
	//**************
	m_MvNetDataXMLSndSet = CMvNetDataXMLSndset(pTrans->m_transName, pSoft, pTrans->m_pulseDuration);
	m_MvNetDataXMLSndSet.m_soundcelerity = soundVelocity;

	// TRAMES SHIPNAV
	//***************
	m_MvNetDataXMLShipnav.m_draught = pTrans->m_transDepthMeter * 10000;
}

void EISupervisedOutput::PrepareForSerialization(
	const EISupervisedParameter & eiSupervisedParameters,
	const EchoIntegrationParameter  & echoIntegrationParameter,
	EISupervisedResult * eiSupervisedResult,
	std::int32_t pingstart, std::int32_t pingend, HacTime timestart,
	HacTime timeend, double diststart, double distend,
	double roll, double pitch, double heave,
	double latitude, double longitude,
	double groundspeed, double groundcourse,
	double heading, double surfspeed,
	double driftcourse, double driftspeed, double altitude,
	unsigned int sounderID)
{
	m_classifications = eiSupervisedParameters.getClassifications();
	m_eiSupervisedResult = eiSupervisedResult;

	// Trame SndSet (donn�es du sondeur)
	m_MvNetDataXMLSndSet.m_sounderident = sounderID;
	m_MvNetDataXMLSndSet.m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;

	// Trame Shipnav (donn�es de navigation)
	m_MvNetDataXMLShipnav.m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;
	m_MvNetDataXMLShipnav.m_roll = roll;
	m_MvNetDataXMLShipnav.m_pitch = pitch;
	m_MvNetDataXMLShipnav.m_heave = heave;
	m_MvNetDataXMLShipnav.m_latitude = latitude;
	m_MvNetDataXMLShipnav.m_longitude = longitude;
	m_MvNetDataXMLShipnav.m_groundspeed = groundspeed;
	m_MvNetDataXMLShipnav.m_groundcourse = groundcourse;
	m_MvNetDataXMLShipnav.m_heading = heading;
	m_MvNetDataXMLShipnav.m_surfspeed = surfspeed;
	m_MvNetDataXMLShipnav.m_driftcourse = driftcourse;
	m_MvNetDataXMLShipnav.m_driftspeed = driftspeed;
	m_MvNetDataXMLShipnav.m_altitude = altitude;
	m_MvNetDataXMLShipnav.m_depth = eiSupervisedResult->GetBottomDepth();

	m_cellCount =
		echoIntegrationParameter.countLayersDef()
		+ 1; //couche totale

	m_MvNetDataXMLLayer.resize(m_cellCount);
	m_MvNetDataXMLCellset.resize(m_cellCount);

	bool hasSurfaceLayer = false;
	auto globalMinDepth = 0.0;
	auto globalMaxDepth = 0.0;

	const auto& layers = echoIntegrationParameter.getLayersDefs();
	const auto nbLayers = layers.size();

	// BOUCLE SUR LES COUCHES (Trames EILAYER et CELLSET)
	for (size_t iCell = 0; iCell < nbLayers; ++iCell)
	{
		CMvNetDataXMLLayer & mvNetDataXMLLayer = m_MvNetDataXMLLayer[iCell];
		CMvNetDataXMLCellset  & mvNetDataXMLCellset = m_MvNetDataXMLCellset[iCell];

		// Trame EILAYER
		mvNetDataXMLLayer.m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;

		// Trame CELLSET
		mvNetDataXMLCellset.m_cellindex = iCell;

		const auto & layerDef = layers[iCell];

		switch (layerDef->GetLayerType())
		{
		case Layer::Type::SurfaceLayer:			
			// type 0 pour couche de surface
			mvNetDataXMLCellset.m_celltype = CMvNetDataXMLCellset::kCellSurface;

			if (hasSurfaceLayer)
			{
				globalMinDepth = std::min<double>(globalMinDepth, layerDef->GetMinDepth(eiSupervisedResult->GetBottomDepth()));
				globalMaxDepth = std::max<double>(globalMaxDepth, layerDef->GetMaxDepth(eiSupervisedResult->GetBottomDepth()));
			}
			else
			{
				hasSurfaceLayer = true;
				globalMinDepth = layerDef->GetMinDepth(eiSupervisedResult->GetBottomDepth());
				globalMaxDepth = layerDef->GetMaxDepth(eiSupervisedResult->GetBottomDepth());
			}
			break;
			
		case Layer::Type::BottomLayer:
			// type 1 pour couche de fond
			mvNetDataXMLCellset.m_celltype = CMvNetDataXMLCellset::kCellBottom;
			break;
			
		case Layer::Type::DistanceLayer:
			mvNetDataXMLCellset.m_celltype = CMvNetDataXMLCellset::kCellDistance;
			break;
		}

		mvNetDataXMLCellset.m_depthstart = layerDef->GetMinDepth(eiSupervisedResult->GetBottomDepth());
		mvNetDataXMLCellset.m_depthend = layerDef->GetMaxDepth(eiSupervisedResult->GetBottomDepth());

		LayerClassificationResult * pLayerResult = eiSupervisedResult->GetLayerClassificationResult(iCell);
		if (pLayerResult != NULL)
		{
			mvNetDataXMLCellset.m_latitude = pLayerResult->m_latitude;
			mvNetDataXMLCellset.m_longitude = pLayerResult->m_longitude;
			mvNetDataXMLCellset.m_volume = pLayerResult->m_volume;
			mvNetDataXMLCellset.m_surface = pLayerResult->m_surface;
		}

		mvNetDataXMLCellset.m_pingstart = pingstart;
		mvNetDataXMLCellset.m_pingend = pingend;
		mvNetDataXMLCellset.m_timestart = timestart;
		mvNetDataXMLCellset.m_timeend = timeend;
		mvNetDataXMLCellset.m_diststart = diststart;
		mvNetDataXMLCellset.m_distend = distend;
		mvNetDataXMLCellset.m_threshldup = echoIntegrationParameter.getHighThreshold();
		mvNetDataXMLCellset.m_threshldlow = echoIntegrationParameter.getLowThreshold();
		mvNetDataXMLCellset.m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;
	}
	// TOTAL
	{
		unsigned int iCell = m_cellCount - 1;
		CMvNetDataXMLLayer & mvNetDataXMLLayer = m_MvNetDataXMLLayer[iCell];
		CMvNetDataXMLCellset & mvNetDataXMLCellset = m_MvNetDataXMLCellset[iCell];

		mvNetDataXMLCellset.m_cellindex = iCell;
		mvNetDataXMLCellset.m_celltype = CMvNetDataXMLCellset::kCellTotal;

		// cellset totale
		if (hasSurfaceLayer)
		{
			mvNetDataXMLCellset.m_depthstart = globalMinDepth;
			mvNetDataXMLCellset.m_depthend = globalMaxDepth;
			mvNetDataXMLCellset.m_latitude = eiSupervisedResult->m_latitude;
			mvNetDataXMLCellset.m_longitude = eiSupervisedResult->m_longitude;
			mvNetDataXMLCellset.m_volume = eiSupervisedResult->m_volume;
			mvNetDataXMLCellset.m_surface = eiSupervisedResult->m_surface;
		}

		mvNetDataXMLCellset.m_pingstart = pingstart;
		mvNetDataXMLCellset.m_pingend = pingend;
		mvNetDataXMLCellset.m_timestart = timestart;
		mvNetDataXMLCellset.m_timeend = timeend;
		mvNetDataXMLCellset.m_diststart = diststart;
		mvNetDataXMLCellset.m_distend = distend;
		mvNetDataXMLCellset.m_threshldup = echoIntegrationParameter.getHighThreshold();
		mvNetDataXMLCellset.m_threshldlow = echoIntegrationParameter.getLowThreshold();
		mvNetDataXMLCellset.m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;
	}

	// remplissage de la structure EILAYER totale 
	m_MvNetDataXMLLayerTotal.m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;
}

void EISupervisedOutput::ComputeDescriptors(BaseKernel::TramaDescriptor & tramaDescriptor)
{
	if (m_descInitialized == false)
	{
		// FAE 185 - le format du fichier EI supervis�e est � pr�sent le m�me que celui de l'EI par couches
		CMvNetDataXMLSndset::ComputeDescriptor(&tramaDescriptor, m_snddesc);
		CMvNetDataXMLShipnav::ComputeDescriptor(&tramaDescriptor, m_shipnavdesc);
		CMvNetDataXMLCellset::ComputeDescriptor(&tramaDescriptor, m_cellsetdesc);
		CMvNetDataXMLLayer::ComputeDescriptor(&tramaDescriptor, m_layerdesc);
		m_descInitialized = true;
	}
}

void EISupervisedOutput::CSVSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor)
{
	std::string str = "";

	// r�cup�ration de la date courante
	time_t rawtime;
	time(&rawtime);

	HacTime now((std::uint32_t)rawtime, 0);

	ComputeDescriptors(tramaDescriptor);

	// seralisation des r�sultats pour chaque classe 
	for (ClassificationVector::iterator iterClass = m_classifications.begin(); iterClass != m_classifications.end(); iterClass++)
	{
		ClassificationDef & classification = *iterClass;
		str.append(CVSSerializeClassification(tramaDescriptor, now, &classification));
	}

	// non classifi�
	str.append(CVSSerializeClassification(tramaDescriptor, now, NULL));

	// OTK - FAE 203 - ajout d'une ligne pour le total sur l'ensemble des couches
	str.append(CVSSerializeClassification(tramaDescriptor, now, NULL, true));

	movOutput->Write(str);
}

std::string EISupervisedOutput::CVSSerializeClassification(BaseKernel::TramaDescriptor & tramaDescriptor, const HacTime & now,
	ClassificationDef * classification, bool bTotal)
{
	int classId = classification ? classification->m_Id : -1;

	std::string str;

	// pour chaque cellule
	for (unsigned int iCell = 0; iCell < m_cellCount; iCell++)
	{
		// OTK - FAE203 - ajout de la ligne pour la couche totale et pour toutes les classes
		if (!bTotal || m_MvNetDataXMLCellset[iCell].m_celltype == CMvNetDataXMLCellset::kCellTotal)
		{
			str.append(CMvNetDataXML::ToStr(now));
			str.append(";");

			// trames SNDSET
			str.append(m_MvNetDataXMLSndSet.createCSV(m_snddesc));

			// trames SHIPNAV
			str.append(m_MvNetDataXMLShipnav.createCSV(m_shipnavdesc));

			// trames CELLSET
			str.append(m_MvNetDataXMLCellset[iCell].createCSV(m_cellsetdesc));

			// selon la classe en cours
			m_MvNetDataXMLLayer[iCell].m_sa = 0;
			m_MvNetDataXMLLayer[iCell].m_sv = 0;
			m_MvNetDataXMLLayer[iCell].m_ni = 0;
			m_MvNetDataXMLLayer[iCell].m_nt = 0;

			if (m_MvNetDataXMLCellset[iCell].m_celltype == CMvNetDataXMLCellset::kCellTotal)
			{
				double energy;
				if (bTotal)
				{
					energy = m_eiSupervisedResult->GetTotalEnergy();
					m_MvNetDataXMLLayer[iCell].m_ni = m_eiSupervisedResult->GetTotalNi();
				}
				else
				{
					energy = m_eiSupervisedResult->GetClassificationEnergy(classId);
					m_MvNetDataXMLLayer[iCell].m_ni = m_eiSupervisedResult->GetClassificationNi(classId);
				}
				m_MvNetDataXMLLayer[iCell].m_sa = m_eiSupervisedResult->energy2sA(energy);
				m_MvNetDataXMLLayer[iCell].m_sv = m_eiSupervisedResult->energy2Sv(energy, m_eiSupervisedResult->GetTotalSumHeight());
				m_MvNetDataXMLLayer[iCell].m_nt = m_eiSupervisedResult->GetTotalNt();
			}
			else
			{
				IClassificationResult * pResult = m_eiSupervisedResult->GetLayerClassificationResult(iCell);
				if (pResult != NULL)
				{
					if (classification == NULL)
					{
						// non classifi� dans la couche
						double energy = pResult->GetRemainingEnergy();
						m_MvNetDataXMLLayer[iCell].m_sa = m_eiSupervisedResult->energy2sA(energy);
						m_MvNetDataXMLLayer[iCell].m_sv = m_eiSupervisedResult->energy2Sv(energy, pResult->GetSumHeight());
						m_MvNetDataXMLLayer[iCell].m_ni = pResult->GetRemainingNi();
					}
					else
					{
						// classifi� dans la couche
						double energy = pResult->GetEnergy(classId);
						m_MvNetDataXMLLayer[iCell].m_sa = m_eiSupervisedResult->energy2sA(energy);
						m_MvNetDataXMLLayer[iCell].m_sv = m_eiSupervisedResult->energy2Sv(energy, pResult->GetSumHeight());
						m_MvNetDataXMLLayer[iCell].m_ni = pResult->GetNi(classId);
					}
					m_MvNetDataXMLLayer[iCell].m_nt = pResult->GetNt();
				}
			}

			// trames EILAYER
			str.append(m_MvNetDataXMLLayer[iCell].createCSV(m_layerdesc));

			// trame BottErr - r�cup�rer de l'EI par couches


			// Titres class
			if (classification == NULL)
			{
				if (bTotal)
				{
					str.append("Total");
				}
				else
				{
					str.append("Remaining");
				}
			}
			else
			{
				str.append(classification->m_Name);
			}

			str.append("\n");
		}
	}

	return str;
}

void EISupervisedOutput::CSVHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor)
{

	if (movOutput != NULL)
	{
		//Variables locales
		std::string header = "";

		// message description node cfg
		header += "MOVIES_EISupervised";

		// Titres sndset
        header += tramaDescriptor.GetTitlesByCat("MOVIES_EISupervised\\sndset");

		// Titres shipnav
        header += tramaDescriptor.GetTitlesByCat("MOVIES_EISupervised\\shipnav");

		// Titres cellset
        header += tramaDescriptor.GetTitlesByCat("MOVIES_EISupervised\\cellset");

		// Titres layer
        header += tramaDescriptor.GetTitlesByCat("MOVIES_EISupervised\\eilayer");

		// Titres class
		header += ";MOVIES_EISupervised\\class";

		header += "\n";

		movOutput->Write(header);
	}
}

