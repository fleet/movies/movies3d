
#include "EISupervised/EISupervisedParameter.h"

#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

EISupervisedParameter::EISupervisedParameter()
	: ParameterModule("EISupervisedParameter", eSupervised)
	,m_echoIntegrationParameter(nullptr),
	m_appendResults(true),
	m_useVolumicWeightedAvg(true)
{
	GetParameterBroadcastAndRecord().m_enableFileOutput = true;
}

bool EISupervisedParameter::Serialize(MovConfig * movConfig)
{
	// debut de la sérialisation du module
	movConfig->SerializeData(this, eParameterModule);

	movConfig->SerializeData<unsigned int>(static_cast<unsigned int>(m_classifications.size()), eUInt, "ClassificationNumber");

	for (auto& classification : m_classifications)
	{
		classification.Serialize(movConfig);
	}

	// Sauvegarde des transducteurs
	movConfig->SerializeData(reinterpret_cast<ParameterObject*>(this), eParameterObject, "Transducers");
	for (const auto & itTrans : m_transducerLayerConfMap)
	{
		movConfig->SerializeData(static_cast<ParameterObject*>(nullptr), eParameterObject, "Transducer");
		movConfig->SerializeData(itTrans.first, eString, "TransducerName");
		movConfig->SerializeData(itTrans.second.surface, eBool, "Surface");
		movConfig->SerializeData(itTrans.second.distance, eBool, "Distance");
		movConfig->SerializeData(itTrans.second.bottom, eBool, "Bottom");
		movConfig->SerializePushBack();

	}
	movConfig->SerializePushBack();
	

	GetParameterBroadcastAndRecord().Serialize(movConfig);

	movConfig->SerializeData<bool>(m_appendResults, eBool, "AppendResults");
	movConfig->SerializeData<bool>(m_useVolumicWeightedAvg, eBool, "UseVolumicWeightedAverage");

	// fin de la sérialisation du module
	movConfig->SerializePushBack();

	return true;
}

bool EISupervisedParameter::DeSerialize(MovConfig* movConfig)
{
	bool result = movConfig->DeSerializeData(this, eParameterModule);
	// debut de la désérialisation du module
	if (result)
	{
		// réinitialisation du tableau
		m_classifications.clear();
		m_transducerLayerConfMap.clear();

		unsigned int nbClassifications = 0;
		result = movConfig->DeSerializeData<unsigned int>(&nbClassifications, eUInt, "ClassificationNumber") && result;

		// lecture des classifications
		for (unsigned int i = 0; i < nbClassifications; i++)
		{
			ClassificationDef classification;
			classification.m_Id = static_cast<int>(i);
			result = classification.DeSerialize(movConfig) && result;
			m_classifications.push_back(classification);
		}

		// Lecture des transducteurs    
		result = result && movConfig->DeSerializeData(reinterpret_cast<ParameterObject*>(this), eParameterObject, "Transducers");
		if (result == true)
		{
			// Déserialize old data..
			while (result == true)
			{
				std::string name;
				result = movConfig->DeSerializeData<std::string>(&name, eString, "TransducerName");
				if (result)
				{
					TransducerLayerConf transducerLayerConf;
					transducerLayerConf.surface = true;
					transducerLayerConf.distance = true;
					transducerLayerConf.bottom = true;

					m_transducerLayerConfMap[name] = transducerLayerConf;
				}
			}

			// Déserialize new data
			result = true;
			while (result == true)
			{
				result = movConfig->DeSerializeData(nullptr, eParameterObject, "Transducer");
				if(result)
				{
					std::string name;
					movConfig->DeSerializeData<std::string>(&name, eString, "TransducerName");

					TransducerLayerConf transducerLayerConf;
					transducerLayerConf.surface = true;
					transducerLayerConf.distance = true;
					transducerLayerConf.bottom = true;

					movConfig->DeSerializeData<bool>(&transducerLayerConf.surface, eBool, "Surface");
					movConfig->DeSerializeData<bool>(&transducerLayerConf.distance, eBool, "Distance");
					movConfig->DeSerializeData<bool>(&transducerLayerConf.bottom, eBool, "Bottom");

					m_transducerLayerConfMap[name] = transducerLayerConf;
					movConfig->DeSerializePushBack();
				}
			}

			result = true;
			movConfig->DeSerializePushBack();
		}

		result = GetParameterBroadcastAndRecord().DeSerialize(movConfig) && result;

		movConfig->DeSerializeData<bool>(&m_appendResults, eBool, "AppendResults");

		result = movConfig->DeSerializeData<bool>(&m_useVolumicWeightedAvg, eBool, "UseVolumicWeightedAverage") && result;
	}

	// fin de la désérialisation du module
	movConfig->DeSerializePushBack();

	return result;
}

void EISupervisedParameter::setEchoIntegrationParameter(EchoIntegrationParameter* echoIntegrationParameter)
{
	m_echoIntegrationParameter = echoIntegrationParameter;
}

const ClassificationVector& EISupervisedParameter::getClassifications() const
{
	return m_classifications;
}

const ClassificationDef& EISupervisedParameter::getClassification(const size_t index) const
{
	return m_classifications.at(index);
}

void EISupervisedParameter::clearClassifications()
{
	m_classifications.clear();
}

void EISupervisedParameter::addClassification(const ClassificationDef& classDef)
{
	m_classifications.push_back(classDef);
}

const std::map<std::string, TransducerLayerConf>& EISupervisedParameter::getTransducers() const
{
	return m_transducerLayerConfMap;
}

const TransducerLayerConf* EISupervisedParameter::getTransducerLayerConf(const std::string& transducerName) const
{
	const auto& found = m_transducerLayerConfMap.find(transducerName);
	if (found != m_transducerLayerConfMap.cend())
	{
		return &found->second;
	}

	return nullptr;
}

void EISupervisedParameter::clearTransducerLayerConf()
{
	m_transducerLayerConfMap.clear();
	if (m_echoIntegrationParameter != nullptr)
	{
		m_echoIntegrationParameter->clearActiveTransducerNames();
	}
}

void EISupervisedParameter::addTransducerLayerConf(const std::string& transducerName, const TransducerLayerConf& layerConf)
{
	m_transducerLayerConfMap[transducerName] = layerConf;
	if (m_echoIntegrationParameter != nullptr)
	{
		m_echoIntegrationParameter->addActiveTransducerName(transducerName);
	}
}

bool EISupervisedParameter::isAppendResults() const
{
	return m_appendResults;
}

bool EISupervisedParameter::isUseVolumicWeightedAvg() const
{
	return m_useVolumicWeightedAvg;
}

void EISupervisedParameter::setUseVolumicWeightedAvg(const bool useVolumicWeightedAvg)
{
	m_useVolumicWeightedAvg = useVolumicWeightedAvg;
}

void EISupervisedParameter::setAppendResults(const bool bAppendResults)
{
	m_appendResults = bAppendResults;
}
