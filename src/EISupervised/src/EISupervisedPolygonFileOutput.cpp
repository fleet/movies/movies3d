
#include "EISupervised/EISupervisedPolygonFileOutput.h"

#include <sstream>


EISupervisedPolygonFileOutput::EISupervisedPolygonFileOutput(
	std::string directoryPath,
	std::string fileNamePrefix,
	int frequency,
	int id,
	bool prefixDate,
	std::uint32_t esuId,
	std::uint32_t polygonId,
	BaseMathLib::Poly2D * polygon,
	const std::vector<size_t> & channelIndexes) 
	: EISupervisedMovFileOutput(directoryPath, fileNamePrefix, frequency, id, prefixDate)
	, m_esuId(esuId)
	, m_polygonId(polygonId)
	, m_polygon(polygon)
	, m_channelIndexes(channelIndexes)
{
	// On ajoute au suffixe l'identifiant de l'ESU et l'ID du polygon dans l'ESU
	std::ostringstream oss;
	oss << m_FileSuffix << "_esu" << esuId << "_polygon" << polygonId;
	m_FileSuffix = oss.str();

	ConstructNewFileName();
}

EISupervisedPolygonFileOutput::~EISupervisedPolygonFileOutput()
{

}

std::uint32_t EISupervisedPolygonFileOutput::getESUId()
{
	return m_esuId;
}

BaseMathLib::Poly2D * EISupervisedPolygonFileOutput::getPolygon()
{
	return m_polygon;
}

const std::vector<size_t> & EISupervisedPolygonFileOutput::getChannelIndexes() const
{
	return m_channelIndexes;
}
