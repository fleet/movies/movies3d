
#include "EISupervised/EISupervisedPolygonOutput.h"

#include "EISupervised/EISupervisedChannel.h"
#include "EISupervised/EISupervisedOutput.h"
#include "EISupervised/EISupervisedResult.h"

#include "EISupervised/ClassificationResult.h"

#include "BaseMathLib/geometry/Poly2D.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"

#include "M3DKernel/output/MovOutput.h"

#include <time.h>

EISupervisedPolygonOutput::EISupervisedPolygonOutput(BaseMathLib::Poly2D * polygon, std::int32_t minPing, std::int32_t maxPing,
	int transIdx, unsigned short softChanId, const EchoIntegrationParameter& echoIntegrationParams,
	EISupervisedOutput * pReferenceOutput, EISupervisedChannel * pCentralSupervisedChannel) 
	: m_pReferenceOutput(pReferenceOutput)
{
	// rmq. : inutile de le locker car fait au niveau sup�rieur de l'appel
	M3DKernel * pKern = M3DKernel::GetInstance();

	m_MvNetDataXMLSndSet = pReferenceOutput->getSndSet();
	m_MvNetDataXMLShipnav = pReferenceOutput->getShipNav();

	m_MvNetDataXMLCellsetTotal.m_cellindex = 1;
	m_MvNetDataXMLCellsetTotal.m_celltype = 3;
	m_MvNetDataXMLCellsetTotal.m_threshldup = echoIntegrationParams.getHighThreshold();
	m_MvNetDataXMLCellsetTotal.m_threshldlow = echoIntegrationParams.getLowThreshold();

	// construction de la cha�ne des coordonn�es des points. On utilise la voie de r�f�rence pour avoir
	// une position lat/std::int32_t du point commune pour toutes les voies le cas �ch�ant
	std::ostringstream oss;
	oss << "\n" << "datetime;latitude;longitude;depth;" << "\n";

	for (int iPt = 0; iPt < polygon->GetNbPoints(); iPt++)
	{
		// r�cup�ration du ping du point :
		std::int32_t pingId = (std::int32_t)(*polygon->GetPointX(iPt));
		PingFan* pFan = pKern->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingId);

		assert(pFan); // on doit avoir encore les pings en m�moire.

		NavPosition * pPingPos = pFan->GetNavPositionRef(pCentralSupervisedChannel->m_softChannelId);

		oss << CMvNetDataXML::ToStr(pFan->m_ObjectTime) << ";"
			<< CMvNetDataXML::ToStr(pPingPos->m_lattitudeDeg) << ";"
			<< CMvNetDataXML::ToStr(pPingPos->m_longitudeDeg) << ";"
			<< CMvNetDataXML::ToStr(*polygon->GetPointY(iPt)) << ";\n";

		if (iPt == 0)
		{
			m_MvNetDataXMLCellsetTotal.m_depthstart = *polygon->GetPointY(iPt);
			m_MvNetDataXMLCellsetTotal.m_depthend = *polygon->GetPointY(iPt);
		}
		else
		{
			m_MvNetDataXMLCellsetTotal.m_depthstart = std::min<double>(*polygon->GetPointY(iPt), m_MvNetDataXMLCellsetTotal.m_depthstart);
			m_MvNetDataXMLCellsetTotal.m_depthend = std::max<double>(*polygon->GetPointY(iPt), m_MvNetDataXMLCellsetTotal.m_depthend);
		}
	}

	// Boucle sur les pings pour calculer le meanbottomdepth
	double meanBottomDepth = 0;
	m_nbPings = 0;
	bool channelIdxFound = false;
	unsigned int channelIdx = 0;
	for (std::int32_t pingId = minPing; pingId <= maxPing; pingId++)
	{
		PingFan* pFan = pKern->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingId);

		if (pFan)
		{
			// On ne prend pas en compte les pings associ�s aux sondeurs diff�rents de celui qui a le channel en cours de traitement
			Transducer * pTrans = pFan->getSounderRef()->getTransducerForChannel(softChanId);
			// On est oblig� de v�rifier que le transducteur contient bien la voie demand�e (cf. SounderMulti qui renvoie toujours le seul transducteur qu'il poss�de.
			if (pTrans && pTrans->getSoftChannel(softChanId)) 
			{
				if (!channelIdxFound)
				{
					for (size_t channelIndex = 0; channelIndex < pTrans->GetChannelId().size(); ++channelIndex)
					{
						if (pTrans->GetChannelId()[channelIndex] == softChanId)
						{
							channelIdx = channelIndex;
							channelIdxFound = true;
							break;
						}
					}

					// Pour le premier ping, on initialise les valeurs � produire
					m_MvNetDataXMLCellsetTotal.m_pingstart = (std::int32_t)pFan->m_computePingFan.m_pingId;
					m_MvNetDataXMLCellsetTotal.m_timestart = pFan->m_ObjectTime;
					m_MvNetDataXMLCellsetTotal.m_diststart = pFan->m_relativePingFan.m_cumulatedDistance / 1852.;
				}
				else
				{
					// Pour les pings suivants, on note les valeurs qui doivent correspondre au dernier ping de la cellule (polygone)
					m_MvNetDataXMLCellsetTotal.m_pingend = (std::int32_t)pFan->m_computePingFan.m_pingId;
					m_MvNetDataXMLCellsetTotal.m_timeend = pFan->m_ObjectTime;
					m_MvNetDataXMLCellsetTotal.m_distend = pFan->m_relativePingFan.m_cumulatedDistance / 1852.;
					m_MvNetDataXMLCellsetTotal.m_latitude = pFan->GetNavPositionRef(softChanId)->m_lattitudeDeg;
					m_MvNetDataXMLCellsetTotal.m_longitude = pFan->GetNavPositionRef(softChanId)->m_longitudeDeg;

					m_MvNetDataXMLShipnav.m_latitude = m_MvNetDataXMLCellsetTotal.m_latitude;
					m_MvNetDataXMLShipnav.m_longitude = m_MvNetDataXMLCellsetTotal.m_longitude;
					m_MvNetDataXMLShipnav.m_groundcourse = RAD_TO_DEG(pFan->GetNavAttributesRef(softChanId)->m_headingRad);
					m_MvNetDataXMLShipnav.m_heading = RAD_TO_DEG(pFan->GetNavAttributesRef(softChanId)->m_headingRad);
					m_MvNetDataXMLShipnav.m_roll = RAD_TO_DEG(pFan->GetNavAttitudeRef(softChanId)->m_rollRad);
					m_MvNetDataXMLShipnav.m_pitch = RAD_TO_DEG(pFan->GetNavAttitudeRef(softChanId)->m_pitchRad);
					m_MvNetDataXMLShipnav.m_heave = RAD_TO_DEG(pFan->GetNavAttitudeRef(softChanId)->m_heaveMeter);
					m_MvNetDataXMLShipnav.m_surfspeed = pFan->GetNavAttributesRef(softChanId)->m_speedMeter*3.6 / 1.852;

					m_MvNetDataXMLSndSet.m_acquisitionTime = pFan->m_ObjectTime.m_TimeCpu + pFan->m_ObjectTime.m_TimeFraction / 10000.0;
					m_MvNetDataXMLShipnav.m_acquisitionTime = pFan->m_ObjectTime.m_TimeCpu + pFan->m_ObjectTime.m_TimeFraction / 10000.0;
					m_MvNetDataXMLLayerTotal.m_acquisitionTime = pFan->m_ObjectTime.m_TimeCpu + pFan->m_ObjectTime.m_TimeFraction / 10000.0;
					m_MvNetDataXMLCellsetTotal.m_acquisitionTime = pFan->m_ObjectTime.m_TimeCpu + pFan->m_ObjectTime.m_TimeFraction / 10000.0;
				}

				assert(channelIdxFound);

				std::uint32_t echoFondEval = 0;
				std::int32_t bottomRange = 0;
				bool found = false;
				pFan->getBottom(softChanId, echoFondEval, bottomRange, found);

				BaseMathLib::Vector3D vecBottom = pFan->getSounderRef()->GetPolarToGeoCoord(pFan, transIdx, channelIdx, echoFondEval - pTrans->GetSampleOffset());
				meanBottomDepth += vecBottom.z;
				m_nbPings++;
			}
		}
	}
	meanBottomDepth /= m_nbPings;
	m_MvNetDataXMLShipnav.m_depth = meanBottomDepth;

	m_strCoordinates = oss.str();
}

EISupervisedPolygonOutput::~EISupervisedPolygonOutput()
{

}

void EISupervisedPolygonOutput::CSVSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor,
	PolygonClassificationResult * pPolygonResult)
{
	std::string str = "";

	// r�cup�ration de la date courante
	time_t rawtime;
	time(&rawtime);

	HacTime now((std::uint32_t)rawtime, 0);

	ComputeDescriptors(tramaDescriptor);

	str.append(CMvNetDataXML::ToStr(now));
	str.append(";");

	// trames SNDSET
	str.append(m_MvNetDataXMLSndSet.createCSV(m_snddesc));

	// trames SHIPNAV
	str.append(m_MvNetDataXMLShipnav.createCSV(m_shipnavdesc));

	// trames CELLSET
	m_MvNetDataXMLCellsetTotal.m_volume = pPolygonResult->m_volume;
	m_MvNetDataXMLCellsetTotal.m_surface = pPolygonResult->m_surface;
	str.append(m_MvNetDataXMLCellsetTotal.createCSV(m_cellsetdesc));

	int classId = pPolygonResult->GetClassId();

	assert(classId != -1); // on ne produit pas de fichier pour les polygones non classifi�s.

	double energy = pPolygonResult->GetEnergy(classId);

	m_MvNetDataXMLLayerTotal.m_sa = m_pReferenceOutput->getResult()->energy2sA(energy);
	m_MvNetDataXMLLayerTotal.m_sv = m_pReferenceOutput->getResult()->energy2Sv(energy, m_pReferenceOutput->getResult()->GetTotalSumHeight());

	m_MvNetDataXMLLayerTotal.m_ni = pPolygonResult->GetNi(classId);
	m_MvNetDataXMLLayerTotal.m_nt = pPolygonResult->GetNt();

	// trames EILAYER
	str.append(m_MvNetDataXMLLayerTotal.createCSV(m_layerdesc));

	// Titres class	
	std::string className;
    for(const auto& classDef : m_pReferenceOutput->getClassifications())
	{
		if (classDef.m_Id == classId)
		{
			className = classDef.m_Name;
		}
	}
	str.append(className);

	str.append("\n");

	str.append(m_strCoordinates);

	movOutput->Write(str);
}
