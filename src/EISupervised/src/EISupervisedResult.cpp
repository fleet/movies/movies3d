
#include "EISupervised/EISupervisedResult.h"
#include "EISupervised/ClassificationResult.h"

#include "M3DKernel/M3DKernel.h"

#include "MovNetwork/MvNetDataXML.h"

template<class Filter, class Action, typename Param>
void doEchoFilter(const MapPingResults & pings, Filter & f, Action & a, Param & p)
{
	for (MapPingResults::const_iterator iterPing = pings.begin(); iterPing != pings.end(); ++iterPing)
	{
		const PingResult & currentPing = iterPing->second;
		for (MapEchoResults::const_iterator iterEcho = currentPing.m_echos.begin(); iterEcho != currentPing.m_echos.end(); ++iterEcho)
		{
			const EchoResult & currentEcho = iterEcho->second;
			if (f(currentEcho))
			{
				a(currentEcho, p);
			}
		}
	}
}

void sumEchoEnergy(const EchoResult & echo, double & energy)
{
	energy += echo.m_energy;
}

EISupervisedResult::EISupervisedResult()
{
	m_nt = 0;
	m_nbPing = 0;
	m_esuSumHeight = 0.0;
	m_bAlreadySaved = false;

	m_latitude = -100.0;
	m_longitude = -200.0;
	m_volume = -1.0;
	m_surface = -1.0;
}

EISupervisedResult::~EISupervisedResult()
{
	// suppression des resultats
	for (ClassificationResultVector::iterator iterResult = m_results.begin(); iterResult != m_results.end(); iterResult++)
	{
		delete *iterResult;
	}
}

void EISupervisedResult::Reset()
{
	for (ClassificationResultVector::iterator iterResult = m_results.begin(); iterResult != m_results.end(); iterResult++)
	{
		IClassificationResult * pClassificationResult = *iterResult;
		pClassificationResult->Reset();
	}
}

double EISupervisedResult::GetRemainingSa() const
{
	double energy = GetClassificationEnergy(-1);
	return energy2sA(energy);
}

void EISupervisedResult::SetNt(const std::int32_t & nt)
{
	m_nt = nt;
}

std::int32_t EISupervisedResult::GetTotalNt() const
{
	return m_nt;
}

double EISupervisedResult::GetTotalEnergy() const
{
	double totalEnergy = 0.0f;

	struct {
		bool operator() (const EchoResult & echo) {
			return true;
		}
	} filter;

	doEchoFilter(m_pings, filter, sumEchoEnergy, totalEnergy);

	return totalEnergy;
}

double EISupervisedResult::GetTotalSa() const
{
	double totalEnergy = GetTotalEnergy();
	return energy2sA(totalEnergy);
}

std::int32_t EISupervisedResult::GetTotalNi() const
{
	std::int32_t echosCount = 0;
	for (MapPingResults::const_iterator iterPing = m_pings.begin(); iterPing != m_pings.end(); iterPing++)
	{
		const PingResult & currentPing = iterPing->second;
		echosCount += currentPing.m_echos.size();
	}
	return echosCount;
}

double EISupervisedResult::GetClassificationEnergy(int classId) const
{
	double classificationEnergy = 0.0;

	struct {
		bool operator() (const EchoResult & echo) {
			return (echo.parentClassification && echo.parentClassification->GetClassId() == classId)
				|| (!echo.parentClassification && classId==-1);
		}
		int classId;
	} filter;
	filter.classId = classId;

	struct {
		void operator () (const EchoResult & echo, double & energy) {
			energy += echo.m_energy;
		}
	} action;

	doEchoFilter(m_pings, filter, action, classificationEnergy);

	return classificationEnergy;
}

std::int32_t EISupervisedResult::GetClassificationNi(int classId) const
{
	std::int32_t remainingEi = 0;

	struct {
		bool operator() (const EchoResult & echo) {
			return (echo.parentClassification && echo.parentClassification->GetClassId() == classId)
				|| (!echo.parentClassification && classId==-1);
		}
		int classId;
	} filter;
	filter.classId = classId;

	struct {
		void operator () (const EchoResult & echo, std::int32_t & ei) {
			ei++;
		}
	} action;

	doEchoFilter(m_pings, filter, action, remainingEi);

	return remainingEi;
}

double EISupervisedResult::GetClassificationSa(int classId) const
{
	double classificationEnergy = GetClassificationEnergy(classId);
	return energy2sA(classificationEnergy);
}

double EISupervisedResult::GetEnergy(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const
{
	double energy = 0.0f;

	struct {
		bool operator() (const EchoResult & echo) {

			bool match = false;

			// parcours des layers
            for(int layerIdx  : _layersIdx) {
				if (layerIdx == echo.m_surfaceLayerIdx
					|| layerIdx == echo.m_bottomLayerIdx
					|| layerIdx == echo.m_distanceLayerIdx) {
					match = true;
					break;
				}
			}

			// parcours des polygones
            for(BaseMathLib::Poly2D * polygon : _polygons) {
				if (echo.m_pPolygon == polygon) {
					match = true;
					break;
				}
			}

			return match;
		}
		std::vector<int> _layersIdx;
		std::vector<BaseMathLib::Poly2D*> _polygons;
	} filter;

	filter._layersIdx = layersIdx;
	filter._polygons = polygons;

	doEchoFilter(m_pings, filter, sumEchoEnergy, energy);

	return energy;
}

double EISupervisedResult::GetSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const
{
	double energy = GetEnergy(layersIdx, polygons);
	return energy2sA(energy);
}

double EISupervisedResult::GetFreeEnergy(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const
{
	return GetClassifiedEnergy(layersIdx, polygons, -1);
}

double EISupervisedResult::GetFreeSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons) const
{
	double energy = GetClassifiedEnergy(layersIdx, polygons, -1);
	return energy2sA(energy);
}

double EISupervisedResult::GetClassifiedEnergy(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons, int classId) const
{
	double energy = 0.0f;

	struct {
		bool operator() (const EchoResult & echo) {

			int echoClassId = -1;
			if (echo.parentClassification != NULL) {
				echoClassId = echo.parentClassification->GetClassId();
			}

			if (echoClassId != _classId)
				return  false;

			bool match = false;

			// parcours des layers
            for(int layerIdx  : _layersIdx) {
				if (layerIdx == echo.m_surfaceLayerIdx
					|| layerIdx == echo.m_bottomLayerIdx
					|| layerIdx == echo.m_distanceLayerIdx) {
					match = true;
					break;
				}
			}

			// parcours des polygones
            for(BaseMathLib::Poly2D * polygon : _polygons) {
				if (echo.m_pPolygon == polygon) {
					match = true;
					break;
				}
			}

			return match;
		}
		int _classId;
		std::vector<int> _layersIdx;
		std::vector<BaseMathLib::Poly2D*> _polygons;
	} filter;

	filter._classId = classId;
	filter._layersIdx = layersIdx;
	filter._polygons = polygons;

	doEchoFilter(m_pings, filter, sumEchoEnergy, energy);

	return energy;
}

double EISupervisedResult::GetClassifiedSa(const std::vector<int> & layersIdx, const std::vector<BaseMathLib::Poly2D*> & polygons, int classId) const
{
	double energy = GetClassifiedEnergy(layersIdx, polygons, classId);
	return energy2sA(energy);
}

void EISupervisedResult::AddClassificationResult(IClassificationResult * classificationResult)
{
	m_results.push_back(classificationResult);
}

LayerClassificationResult * EISupervisedResult::GetLayerClassificationResult(int layerIdx) const
{
	LayerClassificationResult * pResult = NULL;

	for (ClassificationResultVector::const_iterator iterResult = m_results.begin(); iterResult != m_results.end(); iterResult++)
	{
		LayerClassificationResult * pLayerClassificationResult = dynamic_cast<LayerClassificationResult*>(*iterResult);
		if (pLayerClassificationResult != NULL)
		{
			if (pLayerClassificationResult->m_layerIdx == layerIdx)
			{
				pResult = pLayerClassificationResult;
				break;
			}
		}
	}

	return pResult;
}

PolygonClassificationResult * EISupervisedResult::GetPolygonClassificationResult(BaseMathLib::Poly2D * pPolygon) const
{
	PolygonClassificationResult * pResult = NULL;

	for (ClassificationResultVector::const_iterator iterResult = m_results.begin(); iterResult != m_results.end(); iterResult++)
	{
		PolygonClassificationResult * pPolygonClassificationResult = dynamic_cast<PolygonClassificationResult*>(*iterResult);
		if (pPolygonClassificationResult != NULL)
		{
			if (pPolygonClassificationResult->m_pPolygon == pPolygon)
			{
				pResult = pPolygonClassificationResult;
				break;
			}
		}
	}

	return pResult;
}

void EISupervisedResult::SetClassificationForPolygon(BaseMathLib::Poly2D * polygon, int classId)
{
	PolygonClassificationResult * pClassificationResult = (PolygonClassificationResult *)GetPolygonClassificationResult(polygon);
	assert(pClassificationResult != NULL);
	pClassificationResult->ClassifyAs(classId);
}

void EISupervisedResult::SetClassificationForLayer(int layerIdx, int classId)
{
	LayerClassificationResult * pClassificationResult = (LayerClassificationResult *)GetLayerClassificationResult(layerIdx);
	assert(pClassificationResult != NULL);
	pClassificationResult->ClassifyAs(classId);
}

void EISupervisedResult::ResetClassificationForPolygon(BaseMathLib::Poly2D *  polygon, int classId)
{
	IClassificationResult * pClassificationResult = GetPolygonClassificationResult(polygon);
	assert(pClassificationResult != NULL);
	pClassificationResult->Reset();
}

void EISupervisedResult::ResetClassificationForLayer(int layerIdx, int classId)
{
	IClassificationResult * pClassificationResult = GetLayerClassificationResult(layerIdx);
	assert(pClassificationResult != NULL);
	pClassificationResult->Reset();
}

void EISupervisedResult::ReclassifyPolygon(BaseMathLib::Poly2D *  polygon, int classId)
{
	IClassificationResult * pClassificationResult = GetPolygonClassificationResult(polygon);
	assert(pClassificationResult != NULL);
	pClassificationResult->ClassifyAs(classId);
}

void EISupervisedResult::ReclassifyLayer(int layerIdx, int classId)
{
	IClassificationResult * pClassificationResult = GetLayerClassificationResult(layerIdx);
	assert(pClassificationResult != NULL);
	pClassificationResult->ClassifyAs(classId);
}

int EISupervisedResult::GetLayerClassification(int layerIdx) const
{
	int classId = -1;

	// recherche si une classification existe deja pour le layer
	LayerClassificationResult * pClassificationResult = (LayerClassificationResult *)GetLayerClassificationResult(layerIdx);
	assert(pClassificationResult != NULL);
	if (pClassificationResult != NULL)
	{
		classId = pClassificationResult->GetClassId();
	}

	return classId;
}

int EISupervisedResult::GetPolygonClassification(BaseMathLib::Poly2D *  polygon) const
{
	int classId = -1;

	// recherche si une classification existe deja pour le polygon
	PolygonClassificationResult * pClassificationResult = (PolygonClassificationResult *)GetPolygonClassificationResult(polygon);
	assert(pClassificationResult != NULL);
	if (pClassificationResult != NULL)
	{
		classId = pClassificationResult->GetClassId();
	}

	return classId;
}


EchoResult * EISupervisedResult::AddEchoResult(int pingId, int echoId)
{
	EchoResult * pEchoResult = &m_pings[pingId].m_echos[echoId];

	// ajout de l'echo a la liste des echos non classifi�s
	//m_unClassifiedEchos.insert(pEchoResult);
	//m_esuNbSample++;

	return pEchoResult;
}

EchoResult * EISupervisedResult::GetEchoResult(int pingId, int echoId)
{
	EchoResult * pEchoResult = NULL;
	MapPingResults::iterator iterPing = m_pings.find(pingId);

	if (iterPing != m_pings.end())
	{
		MapEchoResults::iterator iterEcho = iterPing->second.m_echos.find(echoId);
		if (iterEcho != iterPing->second.m_echos.end())
		{
			pEchoResult = &(iterEcho->second);
		}
	}

	return pEchoResult;
}

void EISupervisedResult::EraseClassificationForPolygon(BaseMathLib::Poly2D *  polygon)
{
	for (ClassificationResultVector::const_iterator iterResult = m_results.begin(); iterResult != m_results.end(); iterResult++)
	{
		PolygonClassificationResult * pPolygonClassificationResult = dynamic_cast<PolygonClassificationResult*>(*iterResult);
		if (pPolygonClassificationResult != NULL)
		{
			if (pPolygonClassificationResult->m_pPolygon == polygon)
			{
				delete pPolygonClassificationResult;
				m_results.erase(iterResult);
				break;
			}
		}
	}
}


double EISupervisedResult::energy2sA(double energy) const
{
	//Variable locale
	//---------------
	double l_sA = 0.0;

	// Initialisations
	static const double coeffSa = 1852.*1852.*4.*PI / 1000000.0;

	M3DKernel * pKern = M3DKernel::GetInstance();
	pKern->Lock();
	bool isWeightedEchoIntegration = pKern->GetRefKernelParameter().getWeightedEchoIntegration();
	pKern->Unlock();

	if (isWeightedEchoIntegration)
	{
		if (m_esuDistanceWidth == 0)
		{
			l_sA = XMLBadValue;
		}
		else
		{
			l_sA = energy * coeffSa / m_esuDistanceWidth;
		}
	}
	else
	{
		int esuNbPing = m_pings.size();
		if (esuNbPing == 0)
		{
			l_sA = XMLBadValue;
		}
		else
		{
			l_sA = energy * coeffSa * m_esuSampleDepthSI / esuNbPing;
		}
	}

	return l_sA;
}

double EISupervisedResult::energy2Sv(double energy, double sumHeight) const
{
	//Variable locale
	//---------------
	double l_Sv;

	M3DKernel * pKern = M3DKernel::GetInstance();
	pKern->Lock();
	bool isWeightedEchoIntegration = pKern->GetRefKernelParameter().getWeightedEchoIntegration();
	pKern->Unlock();

	if (isWeightedEchoIntegration)
	{
		double area = m_esuDistanceWidth * sumHeight / m_nbPing;
		if (energy == 0 || area == 0)
		{
			l_Sv = XMLBadValue;
		}
		else
		{
			l_Sv = 10.0 * log10(energy * 1E-6 / area);
		}
	}
	else
	{
		if (energy == 0 || m_nt == 0)
		{
			l_Sv = XMLBadValue;
		}
		else
		{
			l_Sv = 10.0 * log10(energy * 1.E-6 / (double)m_nt);
		}
	}

	return l_Sv;
}


bool EISupervisedResult::IsAlreadySaved()
{
	return m_bAlreadySaved;
}

void EISupervisedResult::SetAlreadySaved(bool bAlreadySaved)
{
	m_bAlreadySaved = bAlreadySaved;
}

