

#include "EISupervised/EISupervisedModule.h"
#include "EISupervised/EISupervisedOutput.h"
#include "EISupervised/ClassificationResult.h"
#include "EISupervised/EISupervisedMovFileOutput.h"
#include "EISupervised/EISupervisedChannel.h"
#include "EISupervised/EISupervisedMeanResult.h"

#include "EchoIntegration/EchoIntegrationModule.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/parameter/TramaDescriptor.h"
#include "M3DKernel/datascheme/TransformSet.h"

#include "BaseMathLib/geometry/GeometryTools2D.h"

EISupervisedTransducer::EISupervisedTransducer()
{
	m_initialized = false;
}

EISupervisedTransducer::~EISupervisedTransducer()
{
	// destruction des polygones
	for (PolygonSet::iterator it = m_polygons.begin(); it != m_polygons.end(); ++it)
	{
		delete *it;
	}

	for (size_t i = 0; i < m_Channels.size(); i++)
	{
		delete m_Channels[i];
	}

	// destruction des r�sultats
	for (MapResultByEsu::const_iterator it = m_meanResults.begin(); it != m_meanResults.end(); ++it)
	{
		delete it->second;
	}
}

void EISupervisedTransducer::Initialize(Transducer * pTransducer)
{
	for (unsigned int iChan = 0; iChan < pTransducer->m_numberOfSoftChannel; iChan++)
	{
		EISupervisedChannel * pChannel = new EISupervisedChannel(this);
		pChannel->m_iChannel = iChan;
		pChannel->m_softChannelId = pTransducer->getSoftChannelPolarX(iChan)->m_softChannelId;
		m_Channels.push_back(pChannel);
	}
	m_initialized = true;
}

EsuInfo::EsuInfo(): m_latitudes{}, m_longitudes{}, m_rolls{}, m_pitchs{}, m_heaves{}, m_surfspeeds{}, m_groundcourses{}, m_headings{}
{
	Reset();
}

void EsuInfo::Reset()
{
	m_startFound = false;

	m_pingstart = 0;
	m_pingend = 0;

	m_timestart = HacTime(0, 0);
	m_timeend = HacTime(0, 0);

	m_diststart = 0;
	m_distend = 0;

	// Donn�es pour les trames ShipNav
	m_rolls.clear();
	m_pitchs.clear();
	m_heaves.clear();
	m_latitudes.clear();
	m_longitudes.clear();
	m_groundcourses.clear();
	m_headings.clear();
	m_surfspeeds.clear();
	m_groundspeed = -1;
	m_driftcourse = -1;
	m_driftspeed = -1;
	m_altitude = -10000000;
}

void EsuInfo::Update(PingFan * pFan)
{
	// update ping info
	if (m_startFound == false)
	{
		m_startFound = true;
		m_pingstart = pFan->m_computePingFan.m_pingId;
	}
	m_pingend = pFan->m_computePingFan.m_pingId;
	m_distend = pFan->m_relativePingFan.m_cumulatedDistance / 1852.;
	m_timeend = pFan->m_ObjectTime;

	// Donn�es de la trame ShipNav
	m_rolls.clear();
	m_pitchs.clear();
	m_heaves.clear();
	m_latitudes.clear();
	m_longitudes.clear();
	m_groundcourses.clear();
	m_headings.clear();
	m_surfspeeds.clear();
	for (auto id : pFan->GetChannelIds()) {
		m_rolls[id] = RAD_TO_DEG(pFan->GetNavAttitudeRef(id)->m_rollRad);
		m_pitchs[id] = RAD_TO_DEG(pFan->GetNavAttitudeRef(id)->m_pitchRad);
		m_heaves[id] = pFan->GetNavAttitudeRef(id)->m_heaveMeter;
		m_latitudes[id] = pFan->GetNavPositionRef(id)->m_lattitudeDeg;
		m_longitudes[id] = pFan->GetNavPositionRef(id)->m_longitudeDeg;
		m_surfspeeds[id] = pFan->GetNavAttributesRef(id)->m_speedMeter*3.6 / 1.852;
		m_groundcourses[id] = RAD_TO_DEG(pFan->GetNavAttributesRef(id)->m_headingRad);
		m_headings[id] = RAD_TO_DEG(pFan->GetNavAttributesRef(id)->m_headingRad);
	}
}

void EISupervisedTransducer::UpdateOutputSndSet(Transducer * pTrans, const double & soundVelocity)
{
	for (size_t iChannel = 0; iChannel < m_Channels.size(); iChannel++)
	{
		m_Channels[iChannel]->UpdateOutputSndSet(pTrans, soundVelocity);
	}
}

EISupervisedChannel * EISupervisedTransducer::getCentralSupervisedChannel() const
{
	EISupervisedChannel * pResult = NULL;
	if (!m_Channels.empty())
	{
		//rmq. : utilisation de la m�me formule pour d�terminer le faisceau central que dans le ShoalExtractionModule ...
		pResult = m_Channels[m_Channels.size() / 2];
	}
	return pResult;
}

int EISupervisedTransducer::getCentralSoftChannelId() const
{
	EISupervisedChannel * pChan = getCentralSupervisedChannel();
	return pChan ? pChan->m_softChannelId : -1;
}

int EISupervisedTransducer::getCentralTransducerFrequency() const
{
	EISupervisedChannel * pChan = getCentralSupervisedChannel();
	return pChan ? pChan->m_currentOutput->getSndSet().m_acousticFrequency : -1;
}

void EISupervisedTransducer::AddEIResult(const std::uint32_t & esuId, const EISupervisedParameter & eiSupervisedParameter)
{
	for (size_t iChannel = 0; iChannel < m_Channels.size(); iChannel++)
	{
		EISupervisedChannel * pChannel = m_Channels[iChannel];
		pChannel->AddEIResult(esuId, eiSupervisedParameter);
	}
}

void EISupervisedTransducer::AddPolygon(BaseMathLib::Poly2D * polygon)
{
	// recherche du min et du max (en pings) pour ce polygon polygon
	std::int32_t minPing = (std::int32_t)(*polygon->GetPointX(0));
	std::int32_t maxPing = (std::int32_t)(*polygon->GetPointX(0));
	for (int i = 1; i < polygon->GetNbPoints(); i++)
	{
		double ping = *polygon->GetPointX(i);
		if (ping < minPing) minPing = ping;
		if (ping > maxPing) maxPing = ping;
	}

	bool bAdded = false;
	for (size_t iChannel = 0; iChannel < m_Channels.size(); iChannel++)
	{
		if (m_Channels[iChannel]->AddPolygon(polygon, minPing, maxPing))
		{
			if (!bAdded)
			{
				bAdded = true;
				m_polygons.insert(polygon);
			}
		}
	}
}

void EISupervisedTransducer::DeletePolygon(BaseMathLib::Poly2D *polygon)
{
	// recherche du min et du max (en pings) pour ce polygon polygon
	std::int32_t minPing = (std::int32_t)(*polygon->GetPointX(0));
	std::int32_t maxPing = (std::int32_t)(*polygon->GetPointX(0));
	for (int i = 1; i < polygon->GetNbPoints(); i++)
	{
		double ping = *polygon->GetPointX(i);
		if (ping < minPing) minPing = ping;
		if (ping > maxPing) maxPing = ping;
	}

	for (size_t iChannel = 0; iChannel < m_Channels.size(); iChannel++)
	{
		m_Channels[iChannel]->DeletePolygon(polygon, minPing, maxPing);
	}

	// suppression du polygone
	m_polygons.erase(polygon);

	// suppression definitive du polygone
	delete polygon;
}

std::uint32_t EISupervisedTransducer::GetEsuForPolygon(BaseMathLib::Poly2D *polygon)
{
	// recherche du min et du max (en pings) pour ce polygon polygon
	std::int32_t minPing = (std::int32_t)(*polygon->GetPointX(0));
	std::int32_t maxPing = (std::int32_t)(*polygon->GetPointX(0));
	for (int i = 1; i < polygon->GetNbPoints(); i++)
	{
		double ping = *polygon->GetPointX(i);
		if (ping < minPing) minPing = ping;
		if (ping > maxPing) maxPing = ping;
	}

	//recherche de l'esu correspondante
	std::uint32_t esuId = -1;
	EISupervisedChannel * pCentralChannel = getCentralSupervisedChannel();
	if (pCentralChannel)
	{
		for (MapResultByEsu::iterator iterResult = pCentralChannel->m_results.begin(); iterResult != pCentralChannel->m_results.end(); ++iterResult)
		{
			EISupervisedResult * pTmpResult = iterResult->second;
			if (minPing >= pTmpResult->GetPingStart() && minPing <= pTmpResult->GetPingEnd())
			{
				esuId = iterResult->first;
				break;
			}
		}
	}
	return esuId;
}

EISupervisedResult * EISupervisedTransducer::GetCentralResultFromEsu(std::uint32_t esuId) const
{
	EISupervisedResult * pResult = NULL;
	EISupervisedChannel * pChannel = getCentralSupervisedChannel();
	if (pChannel)
	{
		MapResultByEsu::const_iterator iterFind = pChannel->m_results.find(esuId);
		if (iterFind != pChannel->m_results.end())
		{
			pResult = iterFind->second;
		}
	}
	return pResult;
}

EISupervisedResult * EISupervisedTransducer::GetMeanResultFromEsu(std::uint32_t esuId, const ClassificationVector& classifications,
	const std::vector<int> & layers, const std::vector<BaseMathLib::Poly2D*> & polygons,
	bool buseVolumes)
{
	EISupervisedResult * pResult = NULL;
	EISupervisedResult * centralResult = GetCentralResultFromEsu(esuId);
	if (m_Channels.size() == 1)
	{
		// Cas monofaisceau, pas de probl�me, on prend le seul r�sultat qu'on a
		pResult = centralResult;
	}
	else
	{
		// OTK - FAE166 - Cas multifaisceau !
		EISupervisedMeanResult * pMeanResult = new EISupervisedMeanResult();
		pResult = pMeanResult;

		std::vector<EISupervisedResult*> channelResults = GetResultsFromEsu(esuId);
		double dbTotalSa = 0;
		double dbRemainingSa = 0;
		double dbTotalCoeff = 0;
		std::map<int, double> classifiedSa;
		for (size_t iChan = 0; iChan < channelResults.size(); iChan++)
		{
			EISupervisedResult * pChanResult = channelResults[iChan];

			double dbCoeff = buseVolumes ? pChanResult->m_insonifiedVolume : 1.0;
			dbTotalSa += pChanResult->GetSa(layers, polygons) * dbCoeff;
			dbRemainingSa += pChanResult->GetFreeSa(layers, polygons) * dbCoeff;

			for (size_t iClass = 0; iClass < classifications.size(); iClass++)
			{
				const auto & classif = classifications[iClass];
				std::map <int, double > ::iterator iter = classifiedSa.find(classif.m_Id);
				if (iter != classifiedSa.end())
				{
					iter->second += pChanResult->GetClassifiedSa(layers, polygons, classif.m_Id) * dbCoeff;
				}
				else
				{
					classifiedSa[classif.m_Id] = pChanResult->GetClassifiedSa(layers, polygons, classif.m_Id) * dbCoeff;
				}
			}

			dbTotalCoeff += dbCoeff;
		}

		if (dbTotalCoeff > 0)
		{
			dbTotalSa = dbTotalSa / dbTotalCoeff;
			dbRemainingSa = dbRemainingSa / dbTotalCoeff;
			for (std::map<int, double>::iterator iter = classifiedSa.begin(); iter != classifiedSa.end(); ++iter)
			{
				iter->second /= dbTotalCoeff;
			}
		}

		pMeanResult->SetTotalSa(dbTotalSa);
		pMeanResult->SetRemainingSa(dbRemainingSa);
		pMeanResult->SetClassifiedSa(classifiedSa);

		// flag de sauvegarde d�j� effectu�e
		if (centralResult)
		{
			pResult->SetAlreadySaved(centralResult->IsAlreadySaved());
		}

		// Mise dans la map en supprimant �ventuellement l'ancien r�sultat
		MapResultByEsu::const_iterator iter = m_meanResults.find(esuId);
		if (iter != m_meanResults.end())
		{
			delete iter->second;
		}
		m_meanResults[esuId] = pResult;
	}

	return pResult;
}

std::vector<EISupervisedResult*> EISupervisedTransducer::GetResultsFromEsu(std::uint32_t esuId) const
{
	std::vector<EISupervisedResult*> result;
	for (size_t iChannel = 0; iChannel < m_Channels.size(); iChannel++)
	{
		EISupervisedChannel * pChannel = m_Channels[iChannel];
		MapResultByEsu::const_iterator iterFind = pChannel->m_results.find(esuId);
		if (iterFind != pChannel->m_results.end())
		{
			result.push_back(iterFind->second);
		}
	}
	return result;
}

MapResultByEsu EISupervisedTransducer::GetMeanResults(const ClassificationVector& classifications, bool buseVolumes)
{
	MapResultByEsu result;
	if (m_Channels.size() == 1)
	{
		// Cas monofaisceau, pas de probl�me, on prend le seul r�sultat qu'on a
		result = m_Channels.front()->m_results;
	}
	else
	{
		// OTK - FAE166 - Cas multifaisceau !
		EISupervisedChannel * pCentralChan = getCentralSupervisedChannel();
		if (pCentralChan)
		{
			// Pour chaque ESU d�finie pour le channel central ....
			for (std::map<std::uint32_t, EISupervisedResult*>::iterator esuIter = pCentralChan->m_results.begin(); esuIter != pCentralChan->m_results.end(); ++esuIter)
			{
				EISupervisedMeanResult * pMeanResult = new EISupervisedMeanResult();
				result[esuIter->first] = pMeanResult;

				std::vector<EISupervisedResult*> channelResults = GetResultsFromEsu(esuIter->first);
				double dbTotalSa = 0;
				double dbRemainingSa = 0;
				double dbTotalCoeff = 0;
				std::map<int, double> classifiedSa;
				for (size_t iChan = 0; iChan < channelResults.size(); iChan++)
				{
					EISupervisedResult * pChanResult = channelResults[iChan];

					double dbCoeff = buseVolumes ? pChanResult->m_insonifiedVolume : 1.0;
					dbTotalSa += pChanResult->GetTotalSa() * dbCoeff;
					dbRemainingSa += pChanResult->GetRemainingSa() * dbCoeff;

					for (size_t iClass = 0; iClass < classifications.size(); iClass++)
					{
						int classId = classifications[iClass].m_Id;
						std::map<int, double>::iterator iter = classifiedSa.find(classId);
						if (iter != classifiedSa.end())
						{
							iter->second += pChanResult->GetClassificationSa(classId) * dbCoeff;
						}
						else
						{
							classifiedSa[classId] = pChanResult->GetClassificationSa(classId) * dbCoeff;
						}
					}

					dbTotalCoeff += dbCoeff;
				}

				if (dbTotalCoeff > 0)
				{
					dbTotalSa = dbTotalSa / dbTotalCoeff;
					dbRemainingSa = dbRemainingSa / dbTotalCoeff;
					for (std::map<int, double>::iterator iter = classifiedSa.begin(); iter != classifiedSa.end(); ++iter)
					{
						iter->second /= dbTotalCoeff;
					}
				}

				pMeanResult->SetTotalSa(dbTotalSa);
				pMeanResult->SetRemainingSa(dbRemainingSa);
				pMeanResult->SetClassifiedSa(classifiedSa);

				// flag de sauvegarde d�j� effectu�e
				EISupervisedResult * centralResult = GetCentralResultFromEsu(esuIter->first);
				if (centralResult)
				{
					pMeanResult->SetAlreadySaved(centralResult->IsAlreadySaved());
				}

				// Mise dans la map en supprimant �ventuellement l'ancien r�sultat
				MapResultByEsu::const_iterator iter = m_meanResults.find(esuIter->first);
				if (iter != m_meanResults.end())
				{
					delete iter->second;
				}
				m_meanResults[esuIter->first] = pMeanResult;
			}
		}
	}

	return result;
}

void EISupervisedTransducer::Reset(const EsuIdVector & esuIdVector)
{
	for (EsuIdVector::const_iterator it = esuIdVector.begin(); it != esuIdVector.end(); ++it)
	{
		std::vector<EISupervisedResult*> results = GetResultsFromEsu(*it);
		for (size_t i = 0; i < results.size(); i++)
		{
			results[i]->Reset();
		}
	}
}
