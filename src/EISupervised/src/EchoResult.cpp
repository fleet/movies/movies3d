
#include "EISupervised/EchoResult.h"

EchoResult::EchoResult()
{
	m_energy = 0.0f;
	parentClassification = nullptr;
	m_surfaceLayerIdx = -1;
	m_bottomLayerIdx = -1;
	m_distanceLayerIdx = -1;
	m_pPolygon = nullptr;
}

EchoResult::~EchoResult()
{
}

bool EchoResult::isClassified() const
{
	return parentClassification != nullptr;
}
