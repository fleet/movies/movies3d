// -*- C++ -*-
// ****************************************************************************
// Class: EchoIntegrationChannelResult
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: K�vin DUGUE
// Date  : Avril 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EchoIntegration/EchoIntegrationExport.h"
#include "EchoIntegration/EchoIntegrationLayerResult.h"
#include "M3DKernel/datascheme/DateTime.h"

#include <vector>

class CMvNetDataXMLLayerBoterr;
class CMvNetDataXMLSndset;
class CMvNetDataXMLShipnav;

// ***************************************************************************
// Declarations
// ***************************************************************************
class ECHOINTEGRATION_API EchoIntegrationChannelResult
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	EchoIntegrationChannelResult(int nbLayer);

	// Destructeur
	~EchoIntegrationChannelResult(void);

	// renseigne les objets n�cessaires � la s�rialisation (d�finition des channels...)
	virtual void PrepareForSerialization(EchoIntegrationParameter & echoIntegrationParameter,
		std::int32_t pingstart, std::int32_t pingend, HacTime timestart,
		HacTime timeend, double diststart, double distend,
		double roll, double pitch, double heave,
		double latitude,
		double longitude,
		double groundspeed,
		double groundcourse, double heading, double surfspeed,
		double driftcourse, double driftspeed, double altitude,
		unsigned int sounderID);

	// tableau des r�sultats des diff�rentes couches par fr�quence
	std::vector<std::vector<EchoIntegrationLayerResult *> > m_tabLayerResult;
	const std::vector<std::vector<EchoIntegrationLayerResult *> > &getTabLayerResultFrequencies() const;
	const std::vector<EchoIntegrationLayerResult *> &getTabLayerResult() const;
	std::vector<double> m_tabFrequencies;

	void Initialize(const std::vector<double> &freqs);

	bool m_initialized;

	// r�sultats correspondant � l'erreur d'int�gration du fond
	EchoIntegrationLayerResult m_BotErr;

	// energie ajout�e � la couche de fond au ping pr�c�dent.
	double m_LastSum;

	// sA cumul� (ping � ping)
	double m_sA;

	// Donn�es membres n�cessaires au calcul du sa et sv
	std::uint64_t m_nbPing;

	// fond courant du channel
	double m_BottomDepth;

	//latitude et longitude courante du channel au fond
	double	m_latitude;
	double	m_longitude;

	// distance cumul�e pour l'EI pond�r�e
	double  m_ESUDistance;

	// Pour la s�rialisation
	CMvNetDataXMLLayer& getMvNetDataXMLLayerTotal();
	CMvNetDataXMLCellset& getMvNetDataXMLCellSetTotal();
	CMvNetDataXMLLayerBoterr& getMvNetDataXMLLayerBoterr();
	CMvNetDataXMLSndset& getMvNetDataXMLSndset();
	CMvNetDataXMLShipnav& getMvNetDataXMLShipnav();

private:
	// Objets permettant la s�rialisation des donn�es sur le channel
	// somme des r�sultats de toutes les couches
	std::unique_ptr<CMvNetDataXMLLayer>	m_MvNetDataXMLLayerTotal;
	std::unique_ptr<CMvNetDataXMLCellset> m_MvNetDataXMLCellsetTotal;
	// erreur de fond
	std::unique_ptr<CMvNetDataXMLLayerBoterr> m_MvNetDataXMLBotErr;
	// informations sur le channel
	std::unique_ptr<CMvNetDataXMLSndset> m_MvNetDataXMLSndSet;
	// informations de navigation
	std::unique_ptr<CMvNetDataXMLShipnav> m_MvNetDataXMLShipnav;
};
