// -*- C++ -*-
// ****************************************************************************
// Class: EchoIntegrationLayerResult
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: K�vin DUGUE
// Date  : Avril 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EchoIntegration/EchoIntegrationExport.h"

#include "EchoIntegration/EchoIntegrationParameter.h"

#include <memory>

class CMvNetDataXMLLayer;
class CMvNetDataXMLCellset;

// ***************************************************************************
// Declarations
// ***************************************************************************
class ECHOINTEGRATION_API EchoIntegrationLayerResult
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	EchoIntegrationLayerResult(void);

	// Destructeur
	~EchoIntegrationLayerResult(void);

	// renseigne les objets n�cessaires � la s�rialisation (r�sultats couches)
	virtual void PrepareForSerialization();

	// Renvoie le nombre d'échos
	unsigned int GetNt() const;

	// Permet d'incrémenter le nombre total d'échos
	void IncNt(std::int32_t nbSample = 1);

	// Renvoie le nombre total d'échos intégrés
	unsigned int GetNi() const;

	// Permet d'incrémenter le nombre total d'échos intégrés
	void IncNi(std::int32_t n = 1);

	// Retourne la hauteur totale
	double GetSumHeights() const;

	// Incrémente la hauteur totale
	void IncSumHeights(double h);

	// Permet d'ajouter une �nergie
	void AddEnergy(double energy);

	// Permet d'ajouter une �nergie pond�r�e du smapleSpacing
	void AddPondEnergy(double energy);

	// Renvoie l'�nergie
	double GetEnergy(void);

	// Renvoie l'�nergie pond�r�e du sampleSpacing
	double GetPondEnergy(void);

	double GetSa() const;

	double GetSv() const;

	void SetDepth(double depth);
	double GetDepth() const;

	void SetLatitude(double latitude);
	double GetLatitude() const;

	void SetLongitude(double longitude);
	double GetLongitude() const;
	
	void SetVolume(double volume);
	double GetVolume() const;

	void SetSurface(double surface);
	double GetSurface() const;

	// calcul du sA et Sv � partir de l'energie
	void Compute(std::int64_t nbPing, bool isWeighting, double distanceWidth);
	
	// Pour la sérialisation
	CMvNetDataXMLLayer& getMvNetDataXMLLayer();
	CMvNetDataXMLCellset& getMvNetDataXMLCellSet();

private:
	struct Impl;
	std::unique_ptr<Impl> impl;
};
