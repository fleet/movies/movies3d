// -*- C++ -*-
// ****************************************************************************
// Class: EchoIntegrationModule
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: K�vin DUGUE
// Date  : Avril 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EchoIntegration/EchoIntegrationExport.h"
#include "EchoIntegration/EchoIntegrationParameter.h"
#include "EchoIntegration/EchoIntegrationOutput.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/module/ProcessModule.h"

#include <map>

// ***************************************************************************
// Declarations
// ***************************************************************************
typedef	 std::map<std::uint32_t, EchoIntegrationOutput*> MapEchoIntegrationOutput;

class CalibrationModule;

class ECHOINTEGRATION_API EchoIntegrationModule : public ProcessModule
{
public:
	// *********************************************************************
  // Constructeurs / Destructeur
  // *********************************************************************
	// Constructeur par d�faut
	MovCreateMacro(EchoIntegrationModule);

	// Destructeur
	virtual ~EchoIntegrationModule();

	void setCalibrationModule(CalibrationModule * calibrationModule) { m_pCalibrationModule = calibrationModule; }
	inline CalibrationModule * getCalibrationModule() const { return m_pCalibrationModule; }

	void PingFanAdded(PingFan *);
	void SounderChanged(std::uint32_t);
	void StreamClosed(const char *streamName);
	void StreamOpened(const char *streamName);
	void ESUStart(ESUParameter * pWorkingESU);
	void ESUEnd(ESUParameter * pWorkingESU, bool abort);
	void DisableForNextESU();
	void onEnableStateChange() override;

	// IPSIS - OTK - ajout accesseur sur param�tres pour la gestion des sorties
	BaseKernel::ParameterModule& GetParameterModule() { return m_parameter; };

	// Conteneur des sorties du module en cours de construction (de l'ESU en cours).
	// la cl� utilis�e est l'ID du sondeur. (on a un EchoIntegrationOutput par sondeur)
	MapEchoIntegrationOutput m_EchoIntegrationOutputContainer;

	EchoIntegrationParameter&	GetEchoIntegrationParameter() { return m_parameter; }
	const EchoIntegrationParameter&	GetEchoIntegrationParameter() const { return m_parameter; }

	// Outputs
	std::vector<EchoIntegrationOutput*> GetOutputs() const;
	std::vector<EchoIntegrationOutput*> GetOutputs(tConsumerId consumerId) const;	

protected:
	//Constructeur
	EchoIntegrationModule();
	// param�tres du module d'�cho-int�gration
	EchoIntegrationParameter	m_parameter;

	// Donn�es n�cessaires � la s�rialisation des trames r�sultat
	//***********************************************************

	// d�but / fin d'ESU dans les 3 dimensions (temps, nb de pings, distance.
	std::int32_t				m_pingCurrent;
	std::int32_t				m_pingstart;
	std::int32_t				m_pingend;
	HacTime			m_timeCurrent;
	HacTime			m_timestart;
	HacTime			m_timeend;
	double			m_distCurrent;
	double			m_diststart;
	double			m_distend;
	std::map<unsigned short, double> m_latitudes;
	std::map<unsigned short, double> m_longitudes;

	// Donn�es pour les trames ShipNav
	std::map<unsigned short,double> m_rolls;
	std::map<unsigned short, double> m_pitchs;
	std::map<unsigned short, double> m_heaves;
	double m_groundspeed;
	std::map<unsigned short, double> m_groundcourses;
	std::map<unsigned short, double> m_headings;
	std::map<unsigned short, double> m_surfspeeds;
	double m_driftcourse;
	double	m_driftspeed;
	double	m_altitude;

	// indique si le traitement doit s'arr�ter � la fin de l'ESU courant
	bool				m_DisableNextESU;
	// indique si un ESU est encours
	bool				m_ESURunning;

private:
	// met a jour les caract�ristique d'un channel dans les objets servant a la serialisation des trames
	virtual void UpdateChannelParameters(CMvNetDataXMLSndset & mvNetDataXMLSndSet,
		CMvNetDataXMLShipnav& mvNetDataXMLShipnav,
		Transducer * pTrans, unsigned short channelID);

	// r�cup�re la liste des couches � une profondeur donn�e
	virtual void GetLayersForEcho(EchoIntegrationChannelResult * pChannelResult
		, double echoDepth, double bottomDepth,	double echoDistance
		, std::vector<EchoIntegrationLayerResult*> & result, unsigned int iFreq = 0);

	// nettoie les objets r�sultats cr��s mais non utilis�e (dernier ESU incomplet par exemple)
	void ClearWorkingResults();

	CalibrationModule * m_pCalibrationModule;
};
