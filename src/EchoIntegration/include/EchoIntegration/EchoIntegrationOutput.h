// -*- C++ -*-
// ****************************************************************************
// Class: EchoIntegrationOutput
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: K�vin DUGUE
// Date  : Avril 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EchoIntegration/EchoIntegrationExport.h"
#include "M3DKernel/module/ModuleOutput.h"
#include "EchoIntegration/EchoIntegrationChannelResult.h"
#include <vector>

// ***************************************************************************
// Declarations
// ***************************************************************************
class ECHOINTEGRATION_API EchoIntegrationOutput : public ModuleOutput
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	MovCreateMacro(EchoIntegrationOutput);
	EchoIntegrationOutput();

	// Destructeur
	virtual ~EchoIntegrationOutput();

	// Initialisation de donn�es membres
	virtual void Initialize(int nbChannel, int nblayer);

	// calcul du sa et sv en fin d'ESU � partir de l'energie
	virtual void Compute(bool isWeighting);

	// renseigne les objets n�cessaires � la s�rialisation (d�finition des couches, etc...)
	virtual void PrepareForSerialization(EchoIntegrationParameter & echoIntegrationParameter,
		std::int32_t pingstart, std::int32_t pingend, HacTime timestart,
		HacTime timeend, double diststart, double distend, std::map<unsigned short, double> & rolls,
		std::map<unsigned short, double> & pitchs, std::map<unsigned short, double> & heaves,
		std::map<unsigned short, double> & latitudes,
		std::map<unsigned short, double> & longitudes,
		double groundspeed,
		std::map<unsigned short, double> & groundcourses, std::map<unsigned short, double> & headings, std::map<unsigned short, double> & surfspeeds,
		double driftcourse, double driftspeed, double altitude);

	// red�finition des m�thodes de s�rialisation et de d�s�rialisation pour les sorties XML / CSV
	virtual void XMLSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);
	virtual void CSVSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);
	virtual void XMLHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);
	virtual void CSVHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);

public:
	/**my data are recorded here*/
	unsigned int m_SounderID;

	HacTime		m_timeEnd;

	// tableau des r�sultats des diff�rents channels
	std::vector<EchoIntegrationChannelResult *> m_tabChannelResult;

	// vrai si les vecteurs ont �t� initialis�s
	bool	m_initialized;
};
