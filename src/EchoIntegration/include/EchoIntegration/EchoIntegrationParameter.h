// -*- C++ -*-
// ****************************************************************************
// Class: LayerDefBottom
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: Kévin DUGUE
// Date  : Avril 2008
// Société : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include <memory>

#include "M3DKernel/parameter/ParameterModuleTransducerFiltered.h"
#include "EchoIntegration/LayerDef.h"

#include <vector>


// ***************************************************************************
// Declarations
// ***************************************************************************
class ECHOINTEGRATION_API EchoIntegrationParameter final : public BaseKernel::ParameterModuleTransducerFiltered
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par défaut
	EchoIntegrationParameter();
	~EchoIntegrationParameter();

	bool Serialize(BaseKernel::MovConfig * movConfig) override;
	bool DeSerialize(BaseKernel::MovConfig * movConfig) override;

	// Accesseurs
	double getLowThreshold() const;
	double getHighThreshold() const;
	double getOffsetBottom() const;
	bool isCustomLayersMode() const;

	void setLowThreshold(double lowThreshold);
	void setHighThreshold(double highThreshold);
	void setOffsetBottom(double offsetBottom);
	void setCustomLayersMode(bool customLayersMode);
	
	// Layers
	const std::vector<LayerDef*>& getLayersDefs() const;
	void clearLayersDef();
	size_t countLayersDef() const;
	const LayerDef* getLayerAt(size_t index) const;
	
	const LayerDef* addLayerDef(LayerDef* layer);
	const LayerDef* addLayerDef(const Layer::Type& layerType, double minDepth, double maxDepth);
	
	const LayerDef * getFirstLayer(Layer::Type layerType) const;
	int getFirstLayerIdx(Layer::Type layerType) const;

	// Récupération des indexs par type de couche
	std::map<Layer::Type, std::vector<size_t>> getAllLayerTypeIndexes() const;

	// Récupération des index pour un type de couche
	std::vector<size_t> getLayerTypeIndexes(const Layer::Type& type) const;

private:
	// threshold bas
	double m_lowThreshold;

	// threshold haut
	double m_highThreshold;

	// offsetBottom
	double m_offsetBottom;

	// tableau de définition des couches
	std::vector<LayerDef*> m_tabLayerDef;
	
	// OTK - 22/10/2009 - mode couches quelconques
	bool m_customLayersMode;

	// Méthode pour créer une nouvelle couche directement à partir de la désérialisation de la config	
	bool createLayer(BaseKernel::MovConfig * movConfig, const Layer::Type& layerType);
};

