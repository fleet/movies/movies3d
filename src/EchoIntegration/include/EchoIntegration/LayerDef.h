// -*- C++ -*-
// ****************************************************************************
// Class: LayerDef
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: K�vin DUGUE
// Date  : Avril 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "EchoIntegration/EchoIntegrationExport.h"
#include "M3DKernel/parameter/ParameterObject.h"

// dépendances
#include "M3DKernel/config/MovConfig.h"

#include <string>

class Layer {
public:
	enum class Type : uint8_t
	{
		SurfaceLayer = 0,
		BottomLayer,
		DistanceLayer
	};

	static std::vector<Type> allTypes()
	{
		return{ Type::SurfaceLayer, Type::BottomLayer, Type::DistanceLayer };
	}

	static std::string typeToString(const Type& type) noexcept {
		switch (type) {
		case Type::SurfaceLayer: return "SurfaceLayer";
		case Type::BottomLayer: return "BottomLayer";
		case Type::DistanceLayer: return "DistanceLayer";
		default: return {};
		}
	}

	static uint8_t getTypeValue(const Type type)
	{
		return static_cast<uint8_t>(type);
	}

	static Type getType(const uint8_t value)
	{
		for (const auto type : allTypes())
		{
			if (getTypeValue(type) == value)
			{
				return type;
			}
		}

		return Type::SurfaceLayer; /* Valeur par défaut ? */
	}
};

// ***************************************************************************
// Declarations
// ***************************************************************************
class ECHOINTEGRATION_API LayerDef final : public BaseKernel::ParameterObject
{
public:
	// *********************************************************************
  // Constructeurs / Destructeur
  // *********************************************************************
	// Constructeur par défaut
	explicit LayerDef(Layer::Type layerType, double start, double end);
	explicit LayerDef(Layer::Type layerType);
	LayerDef();

	// *********************************************************************
	// Méthodes
	// *********************************************************************
	bool Serialize(BaseKernel::MovConfig * movConfig) override;
	bool DeSerialize(BaseKernel::MovConfig * movConfig) override;
	
	// *********************************************************************
	// Accesseurs
	// *********************************************************************
	inline double GetStart() const { return m_start; }
	inline double GetEnd() const { return m_end; }
	inline double GetWidth() const { return m_end - m_start; }


	// Type de couche
	inline Layer::Type GetLayerType() const { return m_layerType; }


	// *********************************************************************
	// Calculs
	// *********************************************************************
	
	// Renvoie la profondeur minimale
	double GetMinDepth(double maxRange = 0) const;

	// Renvoie la profondeur maximale
	double GetMaxDepth(double maxRange = 0) const;

private:
	static bool LayerSerialize(LayerDef * layer,  BaseKernel::MovConfig * movConfig, const char * mainKey, const char * minDepthKey, const char * maxDepthKey);
	static bool LayerDeSerialize(LayerDef * layer, BaseKernel::MovConfig * movConfig, const char * mainKey, const char * minDepthKey, const char * maxDepthKey);
	
	// *********************************************************************
	// Variables membres
	// *********************************************************************
	// distance debut
	double m_start;

	// distance fin
	double m_end;

	// type de couche
	Layer::Type m_layerType;
};
