
#include "EchoIntegration/EchoIntegrationChannelResult.h"
#include "M3DKernel/M3DKernel.h"

#include "MovNetwork/MvNetDataXMLLayer.h"
#include "MovNetwork/MvNetDataXMLCellset.h"
#include "MovNetwork/MvNetDataXMLSndset.h"
#include "MovNetwork/MvNetDataXMLShipnav.h"
#include "MovNetwork/MvNetDataXMLLayerBoterr.h"

EchoIntegrationChannelResult::EchoIntegrationChannelResult(int nbLayer)
{
	m_tabLayerResult.resize(1);
	for (int j = 0; j < nbLayer; j++)
	{
		m_tabLayerResult[0].push_back(new EchoIntegrationLayerResult());
	}
	m_initialized = false;
	m_nbPing = 0;
	m_LastSum = 0;
	m_ESUDistance = 0;
	m_sA = 0;

	m_MvNetDataXMLLayerTotal = std::make_unique<CMvNetDataXMLLayer>();
	m_MvNetDataXMLCellsetTotal = std::make_unique<CMvNetDataXMLCellset>();
	m_MvNetDataXMLBotErr = std::make_unique<CMvNetDataXMLLayerBoterr>();
	m_MvNetDataXMLSndSet = std::make_unique<CMvNetDataXMLSndset>();
	m_MvNetDataXMLShipnav = std::make_unique<CMvNetDataXMLShipnav>();
}

EchoIntegrationChannelResult::~EchoIntegrationChannelResult(void)
{
	for (unsigned int i = 0; i < m_tabLayerResult.size(); i++)
	{
		for (unsigned int j = 0; j < m_tabLayerResult[i].size(); j++)
		{
			delete m_tabLayerResult[i][j];
		}
	}
}

// renseigne les objets n�cessaires � la s�rialisation (d�finition des channels...)
void EchoIntegrationChannelResult::PrepareForSerialization(EchoIntegrationParameter & echoIntegrationParameter,
	std::int32_t pingstart, std::int32_t pingend, HacTime timestart,
	HacTime timeend, double diststart, double distend,
	double roll, double pitch, double heave,
	double latitude,
	double longitude,
	double groundspeed,
	double groundcourse, double heading, double surfspeed,
	double driftcourse, double driftspeed, double altitude,
	unsigned int sounderID)
{

	// Trame SNDSET (donn�es sur le channel)
	m_MvNetDataXMLSndSet->m_sounderident = (std::int32_t)sounderID;
	m_MvNetDataXMLSndSet->m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;

	// Trame Shipnav (donn�es de navigation)
	m_MvNetDataXMLShipnav->m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;
	m_MvNetDataXMLShipnav->m_roll = roll;
	m_MvNetDataXMLShipnav->m_pitch = pitch;
	m_MvNetDataXMLShipnav->m_heave = heave;
	m_MvNetDataXMLShipnav->m_latitude = latitude;
	m_MvNetDataXMLShipnav->m_longitude = longitude;
	m_MvNetDataXMLShipnav->m_groundspeed = groundspeed;
	m_MvNetDataXMLShipnav->m_groundcourse = groundcourse;
	m_MvNetDataXMLShipnav->m_heading = heading;
	m_MvNetDataXMLShipnav->m_surfspeed = surfspeed;
	m_MvNetDataXMLShipnav->m_driftcourse = driftcourse;
	m_MvNetDataXMLShipnav->m_driftspeed = driftspeed;
	m_MvNetDataXMLShipnav->m_altitude = altitude;
	m_MvNetDataXMLShipnav->m_depth = m_BottomDepth;

	// r�sultats sur la somme des couches de surface :
	EchoIntegrationLayerResult * pLayerResultTotal = new EchoIntegrationLayerResult();

	// BOUCLE SUR LES COUCHES (Trames EILAYER et CELLSET)
	for (unsigned int i = 0; i < m_tabLayerResult.size(); i++)
	{
		for (unsigned int j = 0; j < m_tabLayerResult[i].size(); j++)
		{
			EchoIntegrationLayerResult * pLayerResult = m_tabLayerResult[i][j];
			// Trame EILAYER
			pLayerResult->PrepareForSerialization();
			pLayerResult->getMvNetDataXMLLayer().m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;
			// Trame CELLSET
			pLayerResult->getMvNetDataXMLCellSet().m_cellindex = j;
			const auto layerDef = echoIntegrationParameter.getLayerAt(j);

			// TODO gérer une retour NULL ?

			if (layerDef->GetLayerType() == Layer::Type::SurfaceLayer)
			{
				// type 0 pour couche de surface
				pLayerResult->getMvNetDataXMLCellSet().m_celltype = 0;
				pLayerResultTotal->AddEnergy(pLayerResult->GetEnergy());
				pLayerResultTotal->AddPondEnergy(pLayerResult->GetPondEnergy());
				pLayerResultTotal->IncNt(pLayerResult->GetNt());
				pLayerResultTotal->IncSumHeights(pLayerResult->GetSumHeights());
				pLayerResultTotal->IncNi(pLayerResult->GetNi());
				// le cellsettotal demarre au demarrage de la premiere couche de surface et fini � la derni�re
				if (j == 0)
				{
					m_MvNetDataXMLCellsetTotal->m_depthstart = layerDef->GetMinDepth(m_BottomDepth);
				}
				// TODO 288 ? on ne doit passer dans ce cas que si il n'y a que des couhes de surface...
				if (j == (m_tabLayerResult.size() - 1))
				{
					m_MvNetDataXMLCellsetTotal->m_depthend = layerDef->GetMaxDepth(m_BottomDepth);
				}
			}
			else if (layerDef->GetLayerType() == Layer::Type::BottomLayer)
			{
				// type 1 pour couche de fond
				pLayerResult->getMvNetDataXMLCellSet().m_celltype = 1;
			}
			// OTK - FAE044 - ajout du type 2 pour couche en distance au transducteur
			else if(layerDef->GetLayerType() == Layer::Type::DistanceLayer)
			{
				// type 2 pour couche en distance au transducteur
				pLayerResult->getMvNetDataXMLCellSet().m_celltype = 2;
			}

			const auto layerDepthstart = layerDef->GetMinDepth(m_BottomDepth);
			const auto layerDepthend = layerDef->GetMaxDepth(m_BottomDepth);
			pLayerResult->getMvNetDataXMLCellSet().m_depthstart = layerDepthstart;
			pLayerResult->getMvNetDataXMLCellSet().m_depthend = layerDepthend;
			pLayerResult->getMvNetDataXMLCellSet().m_pingstart = pingstart;
			pLayerResult->getMvNetDataXMLCellSet().m_pingend = pingend;
			pLayerResult->getMvNetDataXMLCellSet().m_timestart = timestart;
			pLayerResult->getMvNetDataXMLCellSet().m_timeend = timeend;
			pLayerResult->getMvNetDataXMLCellSet().m_diststart = diststart;
			pLayerResult->getMvNetDataXMLCellSet().m_distend = distend;
			pLayerResult->getMvNetDataXMLCellSet().m_threshldup = echoIntegrationParameter.getHighThreshold();
			pLayerResult->getMvNetDataXMLCellSet().m_threshldlow = echoIntegrationParameter.getLowThreshold();
			pLayerResult->getMvNetDataXMLCellSet().m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;

			// NMD - FAE 118
			pLayerResult->getMvNetDataXMLCellSet().m_latitude = pLayerResult->GetLatitude();
			pLayerResult->getMvNetDataXMLCellSet().m_longitude = pLayerResult->GetLongitude();
			pLayerResult->getMvNetDataXMLCellSet().m_volume = pLayerResult->GetVolume();
			pLayerResult->getMvNetDataXMLCellSet().m_surface = pLayerResult->GetSurface();
		}
	}

	// calcul des totaux
	pLayerResultTotal->Compute(m_nbPing, M3DKernel::GetInstance()->GetRefKernelParameter().getWeightedEchoIntegration(), m_ESUDistance);

	// remplissage de la structure EILAYER totale 
	m_MvNetDataXMLLayerTotal->m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;
	m_MvNetDataXMLLayerTotal->m_sa = pLayerResultTotal->GetSa();
	m_MvNetDataXMLLayerTotal->m_sv = pLayerResultTotal->GetSv();
	m_MvNetDataXMLLayerTotal->m_ni = pLayerResultTotal->GetNi();
	m_MvNetDataXMLLayerTotal->m_nt = pLayerResultTotal->GetNt();

	delete pLayerResultTotal;

	// remplissage de la structure CELLSET totale 
	m_MvNetDataXMLCellsetTotal->m_cellindex = static_cast<unsigned short>(echoIntegrationParameter.countLayersDef()); //echoIntegrationParameter.m_nbSurfaceLayers + echoIntegrationParameter.m_nbBottomLayers;
	m_MvNetDataXMLCellsetTotal->m_celltype = 4;
	m_MvNetDataXMLCellsetTotal->m_pingstart = pingstart;
	m_MvNetDataXMLCellsetTotal->m_pingend = pingend;
	m_MvNetDataXMLCellsetTotal->m_timestart = timestart;
	m_MvNetDataXMLCellsetTotal->m_timeend = timeend;
	m_MvNetDataXMLCellsetTotal->m_diststart = diststart;
	m_MvNetDataXMLCellsetTotal->m_distend = distend;
	m_MvNetDataXMLCellsetTotal->m_threshldup = echoIntegrationParameter.getHighThreshold();
	m_MvNetDataXMLCellsetTotal->m_threshldlow = echoIntegrationParameter.getLowThreshold();
	m_MvNetDataXMLCellsetTotal->m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;

	// remplissage de la structure BOTERR
	m_BotErr.Compute(m_nbPing, M3DKernel::GetInstance()->GetRefKernelParameter().getWeightedEchoIntegration(), m_ESUDistance);
	m_MvNetDataXMLBotErr->m_acquisitionTime = timeend.m_TimeCpu + timeend.m_TimeFraction / 10000.0;
	m_MvNetDataXMLBotErr->m_sa = m_BotErr.GetSa();
	m_MvNetDataXMLBotErr->m_ni = m_BotErr.GetNi();
}

const std::vector<std::vector<EchoIntegrationLayerResult*> >& EchoIntegrationChannelResult::getTabLayerResultFrequencies() const
{
	return m_tabLayerResult;
}

const  std::vector<EchoIntegrationLayerResult*>& EchoIntegrationChannelResult::getTabLayerResult() const
{
	return m_tabLayerResult[0];
}

void EchoIntegrationChannelResult::Initialize(const std::vector<double> &freqs)
{
	// TODO protection

	m_tabFrequencies = freqs;
	int nbFreq = freqs.size();
	m_tabLayerResult.resize(nbFreq);
	int nbLayer = m_tabLayerResult[0].size();
	for (int i = 1; i < nbFreq; i++)
	{
		for (int j = 0; j < nbLayer; j++)
		{
			m_tabLayerResult[i].push_back(new EchoIntegrationLayerResult());
		}
		m_tabLayerResult[i].size();
	}
	m_initialized = true;
}

CMvNetDataXMLLayer & EchoIntegrationChannelResult::getMvNetDataXMLLayerTotal() {
	return *m_MvNetDataXMLLayerTotal.get();
}

CMvNetDataXMLCellset & EchoIntegrationChannelResult::getMvNetDataXMLCellSetTotal() {
	return *m_MvNetDataXMLCellsetTotal.get();
}

CMvNetDataXMLLayerBoterr & EchoIntegrationChannelResult::getMvNetDataXMLLayerBoterr() {
	return *m_MvNetDataXMLBotErr.get();
}

CMvNetDataXMLSndset & EchoIntegrationChannelResult::getMvNetDataXMLSndset() {
	return *m_MvNetDataXMLSndSet.get();
}

CMvNetDataXMLShipnav & EchoIntegrationChannelResult::getMvNetDataXMLShipnav() {
	return *m_MvNetDataXMLShipnav.get();
}
