
#include "EchoIntegration/EchoIntegrationLayerResult.h"
#include "M3DKernel/DefConstants.h"

#include "MovNetwork/MvNetDataXMLLayer.h"
#include "MovNetwork/MvNetDataXMLCellset.h"

struct EchoIntegrationLayerResult::Impl
{
	unsigned int m_Ni = 0;
	unsigned int m_Nt = 0;		// nombre d'échos
	double m_Sv = XMLBadValue;
	double m_Sa = XMLBadValue;
	double m_energy = 0.0;		// énergie
	double m_pondenergy = 0.0;	// énergie pondérée du sampleSpacing

	// profondeur courante de la couche
	double m_Depth = 0.0;

	//latitude et longitude courante de la couche
	double	m_latitude = -100;
	double	m_longitude = -200;

	// NMD - FAE 118
	double  m_volume = -1;
	double  m_surface = -1;

	// pour calcul correct de l'aire de la couche si le sampleSpacing �volue
	double m_sumHeights = 0.0;

	// Pour la sérialisation
	CMvNetDataXMLLayer m_MvNetDataXMLLayer;
	CMvNetDataXMLCellset m_MvNetDataXMLCellset;
};

EchoIntegrationLayerResult::EchoIntegrationLayerResult()
	: impl(std::make_unique<EchoIntegrationLayerResult::Impl>())
{
}

EchoIntegrationLayerResult::~EchoIntegrationLayerResult()
{
}

// renseigne les objets nécessaires à la sérialisation (résultats couche)
void EchoIntegrationLayerResult::PrepareForSerialization()
{
	// TRAME EILAYER
	impl->m_MvNetDataXMLLayer.m_sa = impl->m_Sa;
	impl->m_MvNetDataXMLLayer.m_sv = impl->m_Sv;
	impl->m_MvNetDataXMLLayer.m_ni = impl->m_Ni;
	impl->m_MvNetDataXMLLayer.m_nt = impl->m_Nt;
}

unsigned int EchoIntegrationLayerResult::GetNt() const
{
	return impl->m_Nt;
}

void EchoIntegrationLayerResult::IncNt(std::int32_t nbSample)
{
	impl->m_Nt += nbSample;
}

unsigned int EchoIntegrationLayerResult::GetNi() const
{
	return impl->m_Ni;
}

void EchoIntegrationLayerResult::IncNi(std::int32_t n)
{
	impl->m_Ni += n;
}

double EchoIntegrationLayerResult::GetSumHeights() const
{
	return impl->m_sumHeights;
}

void EchoIntegrationLayerResult::IncSumHeights(double h)
{
	impl->m_sumHeights += h;
}

void EchoIntegrationLayerResult::AddEnergy(double energy)
{
	impl->m_energy += energy;
}

void EchoIntegrationLayerResult::AddPondEnergy(double energy)
{
	impl->m_pondenergy += energy;
}

double EchoIntegrationLayerResult::GetEnergy()
{
	return impl->m_energy;
}

double EchoIntegrationLayerResult::GetPondEnergy()
{
	return impl->m_pondenergy;
}

double EchoIntegrationLayerResult::GetSa() const
{
	return impl->m_Sa;
}

double EchoIntegrationLayerResult::GetSv() const
{
	return impl->m_Sv;
}

void EchoIntegrationLayerResult::SetDepth(double depth)
{
	impl->m_Depth = depth;
}

double EchoIntegrationLayerResult::GetDepth() const
{
	return impl->m_Depth;
}

void EchoIntegrationLayerResult::SetLatitude(double latitude)
{
	impl->m_latitude = latitude;
}

double EchoIntegrationLayerResult::GetLatitude() const
{
	return impl->m_latitude;
}

void EchoIntegrationLayerResult::SetLongitude(double longitude)
{
	impl->m_longitude = longitude;
}

double EchoIntegrationLayerResult::GetLongitude() const
{
	return impl->m_longitude;
}

void EchoIntegrationLayerResult::SetVolume(double volume)
{
	impl->m_volume = volume;
}

double EchoIntegrationLayerResult::GetVolume() const
{
	return impl->m_volume;
}

void EchoIntegrationLayerResult::SetSurface(double surface)
{
	impl->m_surface = surface;
}

double EchoIntegrationLayerResult::GetSurface() const
{
	return impl->m_surface;
}


// calcul du sa et sv en fonction de l energie
void EchoIntegrationLayerResult::Compute(std::int64_t nbPing, bool isWeighting, double distanceWidth)
{
	// Déclarations
	double energy = impl->m_energy;
	unsigned int nbSample = impl->m_Nt;

	// Initialisations
	double M_coeffSa = 1852.*4.*PI / 1000000.0;

	// si aucune échantillon intégré, sv = 0
	if (nbSample == 0 || energy == 0 || nbPing == 0)
	{
		impl->m_Sv = XMLBadValue;
	}
	else
	{
		if (isWeighting)
		{
			double area = distanceWidth*(impl->m_sumHeights / (double)nbPing);//(((double)nbSample/nbPing)*sampleSpacing);
			if (area == 0)
			{
				impl->m_Sv = XMLBadValue;
			}
			else
			{
				impl->m_Sv = 10.0 * log10(energy * 1E-6 / area);
			}
		}
		else
		{
			impl->m_Sv = 10.0 * log10(energy * 1.E-6 / (double)nbSample);
		}
	}


	double Sa;
	if (energy == 0 || nbPing == 0)
	{
		Sa = XMLBadValue;
	}
	else
	{
		if (isWeighting)
		{
			Sa = (float)energy;
			if (distanceWidth == 0)
			{
				Sa = XMLBadValue;
			}
			else
			{
				Sa *= M_coeffSa / (distanceWidth / 1852.);
			}
		}
		else
		{
			Sa = M_coeffSa * 1852. * impl->m_pondenergy; // * energy * sampleSpacing
			Sa /= nbPing;
		}
	}
	impl->m_Sa = Sa;
}

CMvNetDataXMLLayer & EchoIntegrationLayerResult::getMvNetDataXMLLayer() 
{
	return impl->m_MvNetDataXMLLayer;
}

CMvNetDataXMLCellset & EchoIntegrationLayerResult::getMvNetDataXMLCellSet() 
{
	return impl->m_MvNetDataXMLCellset;
}
