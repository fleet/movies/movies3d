
#include <algorithm>

#include "EchoIntegration/EchoIntegrationOutput.h"
#include "EchoIntegration/EchoIntegrationModule.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/NavAttributes.h"
#include "M3DKernel/datascheme/MemorySet.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/datascheme/SoftChannel.h"
#include "MovNetwork/MovNetwork.h"
#include "Calibration/CalibrationModule.h"

#include "MovNetwork/MvNetDataXMLSndset.h"
#include "MovNetwork/MvNetDataXMLShipnav.h"

namespace
{
	constexpr const char * LoggerName = "EchoIntergration.EchoIntegrationModule";
}

EchoIntegrationModule::EchoIntegrationModule() 
	: ProcessModule("EchoIntegration Module")
	, m_rolls{}, m_pitchs{}, m_heaves{}, m_latitudes{}, m_longitudes{}, m_groundcourses{}, m_headings{}, m_surfspeeds{}
{
	m_pCalibrationModule = NULL;
	// vecteur des r�sultats
	m_EchoIntegrationOutputContainer.clear();
	// donn�es n�cessaires � la s�rialisation
	m_pingCurrent = 0;
	m_pingstart = 0;
	m_pingend = 0;
	m_timeCurrent = HacTime(0, 0);
	m_timestart = HacTime(0, 0);
	m_timeend = HacTime(0, 0);
	m_distCurrent = 0;
	m_diststart = 0;
	m_distend = 0;
	// Donn�es pour les trames ShipNav
	m_groundspeed = 0;
	m_driftcourse = 0;
	m_driftspeed = 0;
	m_altitude = 0;
	setEnable(false);
	m_DisableNextESU = false;
	m_ESURunning = false;
}

EchoIntegrationModule::~EchoIntegrationModule(void)
{
	ClearWorkingResults();
}

// Traitement du pingFan ajout�
void EchoIntegrationModule::PingFanAdded(PingFan *p)
{
	if (getEnable() && m_ESURunning)
	{
		// indices de boucle
		unsigned int iChannel = 0;// channel
		unsigned int iTrans = 0;// transducteur
		unsigned int iLayer = 0;// couche

		Sounder *pSounder = p->m_pSounder;
		MemorySet *pMemSet = p->GetMemorySetRef();

		//for (auto transducerIndex = 0; transducerIndex < pSounder->GetTransducerCount(); ++transducerIndex)
		//{
		//	Transducer * pTrans = pSounder->GetTransducer(transducerIndex);

		//	auto channelIndex = 0;
		//	for (const auto &channelId : pTrans->GetChannelId())
		//	{
		//		SoftChannel * pSoftChan = pTrans->getSoftChannel(channelId);
		//		if (pSoftChan)
		//		{
		//			auto beam = p->getBeam(channelId)->second;
		//			//channelPingStatusMap[channelId] = beam->m_transMode;
		//			if (beam->m_transMode == BeamDataObject::PassiveTransducerMode)
		//			{
		//				M3D_LOG_INFO(LoggerName, "EchoIntegrationModule::PingFanAdded - Passive Ping found.");
		//				//return;
		//			}
		//		}
		//	}
		//}

		// Si au moins un transducteur du sondeur n'est pas éligible au traitement des écho intégration, on ignore le sondeur 
		const auto allTransducers = pSounder->GetAllTransducers();
		if (std::any_of(
			allTransducers.cbegin(),
			allTransducers.cend(),
			[&](const Transducer * transducer){ return !m_parameter.isTransducerNameActive(transducer->m_transName); }
			))
		{
			return;
		}
		
		bool weightedEchoIntegration = M3DKernel::GetInstance()->GetRefKernelParameter().getWeightedEchoIntegration();

		// cr�ation de la sortie correspondant au sondeur correspondant au pingfan
		// si celui ci n'existe pas deja
		if (m_EchoIntegrationOutputContainer[pSounder->m_SounderId] == NULL)
		{
			m_EchoIntegrationOutputContainer[pSounder->m_SounderId] = EchoIntegrationOutput::Create();
			MovRef(m_EchoIntegrationOutputContainer[pSounder->m_SounderId]);
		}

		//***************************************
		// Mise � jour informations n�cessaires 
		// � la s�rialisation des r�sultats
		//***************************************
		m_distCurrent = p->m_relativePingFan.m_cumulatedDistance / 1852.;
		m_pingCurrent = (std::int32_t)p->m_computePingFan.m_pingId;
		m_timeCurrent = p->m_ObjectTime;
		// Donn�es de la trame ShipNav
		for (const auto &id : p->GetChannelIds()) {
			m_rolls[id] = RAD_TO_DEG(p->GetNavAttitudeRef(id)->m_rollRad);
			m_pitchs[id] = RAD_TO_DEG(p->GetNavAttitudeRef(id)->m_pitchRad);
			m_heaves[id] = p->GetNavAttitudeRef(id)->m_heaveMeter;
			m_latitudes[id] = p->GetNavPositionRef(id)->m_lattitudeDeg;
			m_longitudes[id] = p->GetNavPositionRef(id)->m_longitudeDeg;
			m_surfspeeds[id] = p->GetNavAttributesRef(id)->m_speedMeter*3.6 / 1.852;
			m_groundcourses[id] = RAD_TO_DEG(p->GetNavAttributesRef(id)->m_headingRad);
			m_headings[id] = RAD_TO_DEG(p->GetNavAttributesRef(id)->m_headingRad);
		}
		m_groundspeed = -1; // not available
		m_driftcourse = -1;
		m_driftspeed = -1;
		m_altitude = -10000000;
		double pingDistance = p->getPingDistance();

		const auto nbLayers = m_parameter.countLayersDef();
		std::vector<EchoIntegrationLayerResult *> layerResults;
		const auto firstBottomLayerIdx = m_parameter.getFirstLayerIdx(Layer::Type::BottomLayer);

		//R�cup�ration de l'heure de l'ESU
		m_EchoIntegrationOutputContainer[pSounder->m_SounderId]->m_timeEnd = m_timeCurrent;

		//**************************
		// Boucle Transducteurs
		//**************************
		for (iTrans = 0; iTrans < pMemSet->GetMemoryStructCount(); iTrans++)
		{
			// Est-ce que le traitement doit être fait pour ce transducteur ?
			const auto pTrans = pSounder->GetTransducer(iTrans);
			if (!m_parameter.isTransducerNameActive(pTrans->m_transName))
			{
				continue;
			}			
			
			// On récupère l'espacement entre les échos
			double beamsSamplesSpacing = pTrans->getBeamsSamplesSpacing();

			// On r�cup�re la structure de donn�es associ�e au pingFan
			MemoryStruct *pMem = pMemSet->GetMemoryStruct(iTrans);
			BaseMathLib::Vector2I size = pMem->GetDataFmt()->getSize();

			// Initalisation des vecteurs r�sultat la premi�re fois
			if (!m_EchoIntegrationOutputContainer[pSounder->m_SounderId]->m_initialized)
			{
				m_EchoIntegrationOutputContainer[pSounder->m_SounderId]->Initialize(size.x + pMemSet->GetMemoryStructCount() - 1, (int)nbLayers);
			}

			//**************************
			// Boucle Channels
			//**************************
			
			for (iChannel = 0; iChannel < (unsigned int)size.x; iChannel++)
			{
				bool ignoreBeam = false;
				unsigned short chanId = pTrans->GetChannelId()[iChannel];
				SoftChannel * pSoftChan = pTrans->getSoftChannel(chanId);
				if (pSoftChan)
				{
					auto beam = p->getBeam(chanId)->second;
					if (!beam->m_isValid)
					{
						ignoreBeam = true;
					}

					// Si le channel est passif, il n'est pas pris en compte dans le calcul
					if (beam->m_transMode == BeamDataObject::PassiveTransducerMode)
					{
						M3D_LOG_INFO(LoggerName, "EchoIntegration - Passive Ping found, will not be used.");
						ignoreBeam = true;
					}
				}

				if (ignoreBeam)
					continue;

				EchoIntegrationChannelResult * pChannelResult = m_EchoIntegrationOutputContainer[pSounder->m_SounderId]->m_tabChannelResult[iChannel + iTrans];

				// relev� de tout un tas de car�ct�ristiques du chan pour les trames r�sultat
				UpdateChannelParameters(
					pChannelResult->getMvNetDataXMLSndset(),
					pChannelResult->getMvNetDataXMLShipnav(),
					pTrans, chanId);

				// NMD -  FAE 112 - ajout de la celerit� du son
				pChannelResult->getMvNetDataXMLSndset().m_soundcelerity = pSounder->m_soundVelocity;

				// mise � jour du sampleSpacing du faisceau, du nombre de ping parcouru dans l'ESU, de la distance
				pChannelResult->m_nbPing++;
				pChannelResult->m_ESUDistance += pingDistance;

				// R�cup�ration donn�es �cho de chaque faisceau
				DataFmt * pStart = pMem->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, 0));

				// r�cup�ration de la profondeur du fond verticale pour chaque channel
				std::uint32_t echoFondEval = 0;
				std::int32_t bottomRange = 0;
				bool found = false;
				p->getBottom(pTrans->GetChannelId()[iChannel], echoFondEval, bottomRange, found);
				// Position du fond dans le rep�re monde
				//BaseMathLib::Vector3D vecBottom = pSounder->GetSoftChannelCoordinateToWorldCoord(p,iTrans,iChannel,BaseMathLib::Vector3D(0,0,bottomRange/1000.0));
					// Position du fond dans le rep�re antenne LB 25/03/2015 on travaille dans le rep�re antenne
					// pour l'EIsupervis� comme pour l'EI couche
				BaseMathLib::Vector3D vecBottom = pSounder->GetPolarToGeoCoord(p, iTrans, iChannel, echoFondEval - pTrans->GetSampleOffset());


				// TODO : Diff�rencier FM/CW ?

				// D�tection de l'int�gration du fond.
				// on note pour chaque ping et chaque channel l'�nergie de la premi�re couche de fond
				// CDY - FE248 : D�plac� dans les boucles Range ou Echo
				double energyBeforeThisPing = 0;
				unsigned int NIBeforeThisPing = 0;
				
				double pingFanEnergy = 0.0;

				/**********************************
				 *                                *
				 *         Boucle Ranges          *
				 *                                *
				 **********************************/
				double sA = 0.0;
				auto spectre = p->getSpectralAnalysisDataObject(chanId);
				if (spectre)
				{
					std::vector<std::vector<double> > svValues = m_pCalibrationModule->getSvValuesWithGain(spectre, pSoftChan);
					if (!pChannelResult->m_initialized)
					{
						std::vector<double> freqs(spectre->numberOfFrequencies());
						//for_each(freqs.begin(), freqs.end(), spectre.frequencyForIndex);
						for (unsigned int i = 0; i < spectre->numberOfFrequencies(); i++)
						{
							freqs[i] = spectre->frequencyForIndex(i);
						}

						pChannelResult->Initialize(freqs);
					}

					for (unsigned int i = 0; i < spectre->numberOfFrequencies(); i++)
					{
						// Détection de l'intégration du fond.
						// on note pour chaque ping, chaque channel et chaque fr�quence l'�nergie de la premi�re couche de fond
						if (firstBottomLayerIdx != -1)
						{
							energyBeforeThisPing = pChannelResult->getTabLayerResultFrequencies()[i][firstBottomLayerIdx]->GetEnergy();
							NIBeforeThisPing = pChannelResult->getTabLayerResultFrequencies()[i][firstBottomLayerIdx]->GetNi();
						}

						double pingFanEnergy = 0.0;
						for (unsigned int j = 0; j < spectre->numberOfRanges(); j++)
						{
							double echoDistance = spectre->rangeForIndex(j) - spectre->firstRange;						
							
							// Calcul de la position dans le rep�re monde de l'Echo
							BaseMathLib::Vector3D echoPos(0.0, 0.0, echoDistance);
							BaseMathLib::Vector3D vecEcho = pSounder->GetSoftChannelCoordinateToGeoCoord(p, iTrans, iChannel, echoPos);

							//double value = spectre.svValues[j][i];
							//double value = svValues[j][i];

							double echoDB = svValues[j][i];
							double energy = 0.0;

							bool addEcho = false;

							if (m_parameter.getLowThreshold() <= echoDB && echoDB <= m_parameter.getHighThreshold())
							{
								addEcho = true;

								// On ajoute l'�nergie par faisceau et par couche
								energy = 1000000.0 * pow(10.0, echoDB/ 10.0);
								// L'energie peut �tre pond�r�e par la distance parcourue
								if (weightedEchoIntegration)
								{
									energy *= spectre->rangeInterval * pingDistance;
								}
							}

							double pondEnergy = energy*spectre->rangeInterval;

							//r�cup�ration de la liste des couches contenant cet écho
							GetLayersForEcho(pChannelResult, vecEcho.z, vecBottom.z, echoDistance, layerResults, i);

							if (layerResults.size() > 0 && addEcho)
							{
								pingFanEnergy += energy;
							}

							//**************************
							// Boucle Couches
							//**************************
							for (iLayer = 0; iLayer < layerResults.size(); iLayer++)
							{
								EchoIntegrationLayerResult * pLayerResult = layerResults[iLayer];

								// On incr�mente le nombre total d'�chos par faisceau et par couche 
								pLayerResult->IncNt();
								pLayerResult->IncSumHeights(spectre->rangeInterval);

								if (addEcho)
								{
									// On incr�mente le nombre d'�chos int�gr�s
									pLayerResult->IncNi();

									pLayerResult->AddEnergy(energy);

									// pour calcul du sA en non pond�r� par la distance parcourue
									pLayerResult->AddPondEnergy(pondEnergy);
								}
							}
						}
					}
					if (pChannelResult->m_nbPing != 0)
					{
						const double M_coeffSa = 1852.*4.*PI / 1000000.0;
						if (weightedEchoIntegration)
						{
							if (pingDistance == 0)
							{
								sA = XMLBadValue;
							}
							else
							{
								sA = (pingFanEnergy/ spectre->numberOfFrequencies()) * M_coeffSa / pingDistance;
							}
						}
						else
						{
							sA = M_coeffSa * (pingFanEnergy / spectre->numberOfFrequencies()) * spectre->rangeInterval;
						}
					}
					pChannelResult->m_sA += sA;
					p->setDeviation(chanId, pChannelResult->m_sA);
				}
				else
				{
					// ToDO : trouver la fr�quence unique (pSoftChan)
					if (!pChannelResult->m_initialized)
					{
						std::vector<double> freq(1);
						freq[0] = pSoftChan->m_acousticFrequency;
						pChannelResult->Initialize(freq);
					}

					//**************************
					// Boucle Echos
					//**************************
					std::vector<unsigned int> echoes(size.y, 0);

					// Pré-calculs de positions des échos dans le repère monde
					std::vector<BaseMathLib::Vector3D> vectEcho(size.y);
					for (int iEcho = 0; iEcho < size.y; iEcho++)
					{
						echoes[iEcho] = iEcho;
					}
					pSounder->GetPolarToGeoCoord(p, iTrans, iChannel, echoes, vectEcho);

					for (int iEcho = 0; iEcho < size.y; ++iEcho)
					{
						// Détection de l'intégration du fond.
						// on note pour chaque ping et chaque channel l'énergie de la première couche de fond
						if (firstBottomLayerIdx != -1)
						{
							energyBeforeThisPing = pChannelResult->getTabLayerResult()[firstBottomLayerIdx]->GetEnergy();
							NIBeforeThisPing = pChannelResult->getTabLayerResult()[firstBottomLayerIdx]->GetNi();
						}

						// distance de l'�cho au transducteur (pour les couches en distance au transducteur
						const double echoDistance = iEcho*beamsSamplesSpacing;
						
						// On calcule la valeur de l echo en DB
						double value = *(pStart + iEcho);
						char *pFilter = pMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, iEcho));
						if (*pFilter)
							value = UNKNOWN_DB;

						const double echoDB = value / 100.0;
						double energy = 0.0;

						bool addEcho = false;

						if (m_parameter.getLowThreshold() <= echoDB && echoDB <= m_parameter.getHighThreshold())
						{
							addEcho = true;

							// On ajoute l'�nergie par faisceau et par couche
							energy = 1000000.0 * pow(10.0, ((double)value) / 1000.0);
							// L'energie peut �tre pond�r�e par la distance parcourue
							if (weightedEchoIntegration)
							{
								energy *= beamsSamplesSpacing * pingDistance;
							}
						}

						double pondEnergy = energy*beamsSamplesSpacing;

						//r�cup�ration de la liste des couches contenant cet �cho
						GetLayersForEcho(pChannelResult, vectEcho[iEcho].z, vecBottom.z, echoDistance, layerResults);

						if (layerResults.size() > 0 && addEcho)
						{
							pingFanEnergy += energy;
						}

						//**************************
						// Boucle Couches
						//**************************
						for (iLayer = 0; iLayer < layerResults.size(); iLayer++)
						{
							EchoIntegrationLayerResult * pLayerResult = layerResults[iLayer];

							// On incrémente le nombre total d'échos par faisceau et par couche 
							pLayerResult->IncNt();
							pLayerResult->IncSumHeights(beamsSamplesSpacing);

							// D�termination du respect des seuils
							// OTK - FAE006 - on n'exclu pas les echos positifs
							//if(echoDB<=0)
							//{
							// OTK - 29/10/2009 - on prend les echos �gaux au seuil bas pour homog�n�it� avec l'extraction,
							// la RF, ...
							if (addEcho)
							{
								// On incr�mente le nombre d'�chos int�gr�s
								pLayerResult->IncNi();

								pLayerResult->AddEnergy(energy);

								// pour calcul du sA en non pond�r� par la distance parcourue
								pLayerResult->AddPondEnergy(pondEnergy);
							}
						} // fin boucle sur les couches
					} // fin boucle sur les echos
					if (pChannelResult->m_nbPing != 0)
					{
						const double M_coeffSa = 1852.*4.*PI / 1000000.0;
						if (weightedEchoIntegration)
						{
							if (pingDistance == 0)
							{
								sA = XMLBadValue;
							}
							else
							{
								sA = pingFanEnergy * M_coeffSa / pingDistance;
							}
						}
						else
						{
							sA = M_coeffSa * pingFanEnergy * beamsSamplesSpacing;
						}
					}
					pChannelResult->m_sA += sA;
					p->setDeviation(chanId, pChannelResult->m_sA);
				} // if/else - echos
				


				//************************************************
				// Calcul de l'erreur due � l'int�gration du fond
				//************************************************
				// on note pour chaque ping et chaque channel l'�nergie de la premi�re couche de fond
				double energyAfterThisPing = 0;
				unsigned int NIAfterThisPing = 0;
				
				if (firstBottomLayerIdx != -1)
				{
					energyAfterThisPing = pChannelResult->getTabLayerResult()[firstBottomLayerIdx]->GetEnergy();
					NIAfterThisPing = pChannelResult->getTabLayerResult()[firstBottomLayerIdx]->GetNi();
				}

				double echoMoyen = 0;
				if (NIAfterThisPing - NIBeforeThisPing > 0)
				{
					echoMoyen = (energyAfterThisPing - energyBeforeThisPing) / (NIAfterThisPing - NIBeforeThisPing);
				}

				double lastSum, sommeCouche;
				lastSum = pChannelResult->m_LastSum;
				sommeCouche = energyAfterThisPing - energyBeforeThisPing;


				// Test de l'int�gration du fond
				if (((lastSum > 0) && (sommeCouche > (lastSum * 10))) ||
					((lastSum == 0) && (echoMoyen > 4000000L)))	// 2000� 
				{
					pChannelResult->m_BotErr.IncNi(NIAfterThisPing - NIBeforeThisPing);
					pChannelResult->m_BotErr.AddEnergy(sommeCouche);
				}


				pChannelResult->m_LastSum = energyAfterThisPing - energyBeforeThisPing;
				//****************************************************
				// Fin calcul de l'erreur due � l'int�gration du fond
				//*****************************************************

			} // fin boucle sur les channels
		} // fin boucle transducteur		
	} // fin si getEnabled()
}


// D�marrage d'un ESU
void EchoIntegrationModule::ESUStart(ESUParameter * pWorkingESU)
{
	// si le traitement doit s'arr�ter, on arr�te.
	if (m_DisableNextESU)
	{
		setEnable(false);
		m_DisableNextESU = false;
	}

	if (!getEnable())
	{
		return;
	}

	m_ESURunning = true;

	// l'ESU a �t� utilis�e : on doit le savoir pour l'afficher, par exemple.
	pWorkingESU->SetUsedBySlice();

	m_pingstart = (std::int32_t)pWorkingESU->GetESUPingNumber();
	m_timestart = pWorkingESU->GetESUTime();
	m_diststart = pWorkingESU->GetESUDistance();
}

// Fin de l'ESU en cours
void EchoIntegrationModule::ESUEnd(ESUParameter * pWorkingESU, bool abort)
{
	// OTK - 02/04/2009 - si on n'�tait pas actif au d�but de l'ESU : on ne
	// doit pas s'occuper de la fin...
	if (!getEnable() || !m_ESURunning)
	{
		m_ESURunning = false;
		return;
	}

	m_ESURunning = false;

	// mise � jour des donn�es n�cessaires � la s�rialisation des r�sultats
	m_pingend = m_pingCurrent;
	m_timeend = m_timeCurrent;
	m_distend = m_distCurrent;

	bool weightedEchoIntegration = M3DKernel::GetInstance()->GetRefKernelParameter().getWeightedEchoIntegration();

	MapEchoIntegrationOutput::iterator res = m_EchoIntegrationOutputContainer.begin();
	while (res != m_EchoIntegrationOutputContainer.end())
	{
		EchoIntegrationOutput * pEchoIntegrationOutput = res->second;

		// si les donn�es sont existantes pour le sondeur courant 
		if (pEchoIntegrationOutput != NULL && pEchoIntegrationOutput->m_initialized)
		{
			// on note l'ID du sondeur correspondant pour pouvoir exploiter les r�sultats
			pEchoIntegrationOutput->m_SounderID = res->first;

			// OTK - 23/10/2009 - les calculs de profondeur et de position des couches sont 
			// faits ici en fin d'esu (inutile de le faire � chaque ping, puisque seul
			// le resultat du dernier n'est pas ecras�)

			// r�cup�ration du dernier ping pour ce sondeur
			SounderIndexedTimeContainer * pContainer = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();
			if (pContainer)
			{
				int nbPingFan = (int)pContainer->GetObjectCount();
				bool found = false;

				const auto & layers = m_parameter.getLayersDefs();
				const auto nbLayers = layers.size();

				// OTK - FAE024 - lors du esuend, on a deja ajout� au conteneur le premier ping de l'esu suivant.
				// on ignore donc ce ping dans notre boucle (d�marrage � nbPingFan-2)
				for (int pingFanIdx = nbPingFan - 2; pingFanIdx >= 0 && !found; pingFanIdx--)
				{
					PingFan * pFan = (PingFan*)pContainer->GetObjectWithIndex(pingFanIdx);
					Sounder * pSounder = pFan->getSounderRef();
					if (pSounder && pSounder->m_SounderId == pEchoIntegrationOutput->m_SounderID)
					{
						found = true;
						MemorySet * pMemSet = pFan->GetMemorySetRef();

						// calcul de la profondeur de chaque couche et chaque channel
						for (unsigned int iTrans = 0; iTrans < pMemSet->GetMemoryStructCount(); iTrans++)
						{
							Transducer *pTrans = pSounder->GetTransducer(iTrans);
							MemoryStruct *pMem = pMemSet->GetMemoryStruct(iTrans);
							BaseMathLib::Vector2I size = pMem->GetDataFmt()->getSize();
							for (unsigned int iChannel = 0; iChannel < (unsigned int)size.x; iChannel++)
							{
								SoftChannel * pSoftChan = pTrans->getSoftChannelPolarX(iChannel);
								std::uint32_t echoFondEval = 0;
								std::int32_t bottomRange = 0;
								bool found = false;
								pFan->getBottom(pTrans->GetChannelId()[iChannel], echoFondEval, bottomRange, found);
								BaseMathLib::Vector3D vecBottom = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, iTrans, iChannel, BaseMathLib::Vector3D(0, 0, bottomRange / 1000.0));
								EchoIntegrationChannelResult * pChannelResult = pEchoIntegrationOutput->m_tabChannelResult[iChannel + iTrans];
								pChannelResult->m_BottomDepth = vecBottom.z;
								for (int iFreq = 0; iFreq < pChannelResult->getTabLayerResultFrequencies().size(); iFreq++)
								{
									for (size_t iLay = 0; iLay<nbLayers; ++iLay)
									{										
										auto layer = layers[iLay];

										EchoIntegrationLayerResult * pLayerResult = pChannelResult->getTabLayerResultFrequencies()[iFreq][iLay];
										double bottom = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, iTrans, iChannel, BaseMathLib::Vector3D(0, 0, bottomRange / 1000.0)).z;

										double minDepth;
										double maxDepth;

										// OTK - FAE044 - dans le cas des couches en distance, le calcul est diff�rent
										if (layer->GetLayerType() != Layer::Type::DistanceLayer)
										{
											// on met � jour la profondeur moyenne de la couche au moment de la fin de l'ESU
											pLayerResult->SetDepth( (layer->GetMaxDepth(vecBottom.z) + layer->GetMinDepth(vecBottom.z)) / 2 );

											// on met � jour la position lat std::int32_t de la couche pour le dernier ping de l'ESU
											BaseMathLib::Vector3D layerPos = pSounder->GetSoftChannelCoordinateToGeoCoord(
												pFan, iTrans, iChannel, BaseMathLib::Vector3D(0, 0, pLayerResult->GetDepth()));
											pLayerResult->SetLatitude(layerPos.x);
											pLayerResult->SetLongitude(layerPos.y);

											double offset = pTrans->m_transDepthMeter + pFan->getHeaveChan(pSoftChan->m_softChannelId);
											minDepth = layer->GetMinDepth(bottom) - offset;
											maxDepth = layer->GetMaxDepth(bottom) - offset;
											minDepth = std::max(.0, minDepth);
											maxDepth = std::max(.0, maxDepth);
											// application du d�pointage transversal
											minDepth = minDepth / cos(pSoftChan->m_mainBeamAthwartSteeringAngleRad);
											maxDepth = maxDepth / cos(pSoftChan->m_mainBeamAthwartSteeringAngleRad);
										}
										else
										{
											// cas des couches en distance.
											unsigned int meanSampleNumber = (unsigned int)(((layer->GetMaxDepth() + layer->GetMinDepth()) / 2.0 + 0.5) 
												/ pTrans->getBeamsSamplesSpacing());
											BaseMathLib::Vector3D layerPos = pSounder->GetPolarToGeoCoord(pFan, iTrans, iChannel, meanSampleNumber);
											pLayerResult->SetLatitude(layerPos.x);
											pLayerResult->SetLongitude(layerPos.y);
											pLayerResult->SetDepth(layerPos.z);

											minDepth = layer->GetMinDepth();
											maxDepth = layer->GetMaxDepth();
										}

										pLayerResult->SetVolume( (PI / 3.)*tan(pSoftChan->m_beam3dBWidthAlongRad / 2.)*tan(pSoftChan->m_beam3dBWidthAthwartRad / 2.)
											*((maxDepth*maxDepth*maxDepth) - (minDepth*minDepth*minDepth)) );
										pLayerResult->SetSurface( pLayerResult->GetVolume() / (maxDepth - minDepth) );
									}
								}
							}
						}
					}
				}
			}

			// On calcule les valeurs de Sv et sA � partir de l'energie.
			pEchoIntegrationOutput->Compute(weightedEchoIntegration);
			// on pr�pare les objets n�cessaires � la s�rialisation des r�sultats
			// les donn�es pass�es ici en param�tres sont communes � tous les channels.
			pEchoIntegrationOutput->PrepareForSerialization(m_parameter, m_pingstart, m_pingend, m_timestart,
				m_timeend, m_diststart, m_distend, m_rolls, m_pitchs, m_heaves,
				m_latitudes, m_longitudes, m_groundspeed, m_groundcourses,
				m_headings, m_surfspeeds, m_driftcourse, m_driftspeed,
				m_altitude);

			// et on ajoute l'objet r�sultat au moduleOutputContainer
			// OTK - remarque : pas besoin de le lock, le lock global du kernel est deja la (l'EI par couches est toujorus synchrone
			m_outputContainer.AddOutput(pEchoIntegrationOutput);
		}
		res++;
	}
	// remise a zero du conteneur des sorties
	ClearWorkingResults();
}

// changement des propri�t�s d'un sondeur
void EchoIntegrationModule::SounderChanged(std::uint32_t sounderId)
{
	// l'ESU incomplet n'est pas s�rialis� : on nettoie le module
	MapEchoIntegrationOutput::iterator res = m_EchoIntegrationOutputContainer.begin();
	while (res != m_EchoIntegrationOutputContainer.end())
	{
		EchoIntegrationOutput * pEchoIntegrationOutput = res->second;
		MovUnRefDelete(pEchoIntegrationOutput);
		res++;
	}
	m_EchoIntegrationOutputContainer.clear();
}

void EchoIntegrationModule::StreamClosed(const char *streamName)
{

}
void EchoIntegrationModule::StreamOpened(const char *streamName)
{

}

void EchoIntegrationModule::ClearWorkingResults()
{
	MapEchoIntegrationOutput::iterator res = m_EchoIntegrationOutputContainer.begin();
	while (res != m_EchoIntegrationOutputContainer.end())
	{
		EchoIntegrationOutput * pEchoIntegrationOutput = res->second;
		MovUnRefDelete(pEchoIntegrationOutput);
		res++;
	}
	m_EchoIntegrationOutputContainer.clear();
}

// indique au module qu'il doit se d�sactiver � la fin de l'ESU courant.
void EchoIntegrationModule::DisableForNextESU()
{
	if (!getEnable())
		return;

	m_DisableNextESU = true;
}

// en cas de changement de l'activite du module, on doit en informer
// MovNetwork (qui �met cette information periodiquement)
void EchoIntegrationModule::onEnableStateChange()
{
	if (m_parameter.GetParameterBroadcastAndRecord().m_enableNetworkOutput)
	{
		MovNetwork::getInstance()->setEILayerEnabled(getEnable());
	}

	if (!getEnable())
	{
		// si le module est désactivé, on nettoie son contenu
		ClearWorkingResults();

		m_ESURunning = false;
		m_DisableNextESU = false;
	}
	else
	{
		// Vérification du paramétrage lors de la mise en route
		if (m_parameter.isEnableTransducerFilter() && m_parameter.getActiveTransducerNames().empty())
		{
			M3D_LOG_ERROR(LoggerName, "EchoIntegration - No Transducer selected for EchoIntegration");						
		}
	}
}

// met a jour les caract�ristique d'un channel dans les objets servant a la serialisation des trames
void EchoIntegrationModule::UpdateChannelParameters(CMvNetDataXMLSndset & mvNetDataXMLSndSet,
	CMvNetDataXMLShipnav & mvNetDataXMLShipnav,
	Transducer * pTrans, unsigned short channelID)
{
	SoftChannel * pSoft = pTrans->getSoftChannel(channelID);

	// TRAMES SNDSET
	//**************
	mvNetDataXMLSndSet = CMvNetDataXMLSndset(pTrans->m_transName, pSoft, pTrans->m_pulseDuration);

	// TRAMES SHIPNAV
	//***************
	mvNetDataXMLShipnav.m_draught = pTrans->m_transDepthMeter * 10000;
}

// r�cup�re la liste des couches � une profondeur donn�e
void EchoIntegrationModule::GetLayersForEcho(EchoIntegrationChannelResult * pChannelResult,
	double echoDepth, double bottomDepth,
	double echoDistance,
	std::vector<EchoIntegrationLayerResult*> & result,
	unsigned int iFreq)
{
	result.clear();

	const bool echoBeforeBottomOffset = echoDepth <= bottomDepth - m_parameter.getOffsetBottom();

	const auto& layers = m_parameter.getLayersDefs();
	const auto nbLayers = layers.size();

	if (echoBeforeBottomOffset)
	{
		if (m_parameter.isCustomLayersMode())
		{
			for (size_t layerIdx = 0; layerIdx < nbLayers; ++layerIdx)
			{
				const auto & pLayerDef = layers[layerIdx];
				// cas classique (couche de surface ou de fond)
				if (pLayerDef->GetLayerType() != Layer::Type::DistanceLayer)
				{
					if (echoDepth <= pLayerDef->GetMaxDepth(bottomDepth)
						&& pLayerDef->GetMinDepth(bottomDepth) <= echoDepth)
					{
						result.push_back(pChannelResult->getTabLayerResultFrequencies()[iFreq][layerIdx]);
					}
				}
				else
				{
					if (echoDistance <= pLayerDef->GetMaxDepth()
						&& pLayerDef->GetMinDepth() <= echoDistance)
					{
						result.push_back(pChannelResult->getTabLayerResultFrequencies()[iFreq][layerIdx]);
					}
				}
			}
		}
		else
		{
			bool surfaceLayerNotFound = true;
			bool bottomLayerNotFound = true;
			bool distanceLayerNotFound = true;

			for (size_t layerIdx = 0; layerIdx < nbLayers; ++layerIdx)
			{
				const auto & pLayerDef = layers[layerIdx];
				switch (pLayerDef->GetLayerType())
				{
				case Layer::Type::SurfaceLayer:
					if (surfaceLayerNotFound)
					{
						if (echoDepth <= pLayerDef->GetMaxDepth()	
							&& pLayerDef->GetMinDepth() <= echoDepth)
						{
							result.push_back(pChannelResult->getTabLayerResultFrequencies()[iFreq][layerIdx]);
							surfaceLayerNotFound = false;
						}
					}
					break;

				case Layer::Type::BottomLayer:
					if (bottomLayerNotFound)
					{
						if (echoBeforeBottomOffset
							&& echoDepth <= pLayerDef->GetMaxDepth(bottomDepth)
							&& pLayerDef->GetMinDepth(bottomDepth) <= echoDepth)
						{
							result.push_back(pChannelResult->getTabLayerResultFrequencies()[iFreq][layerIdx]);
							bottomLayerNotFound = false;
						}
					}
					break;

				case Layer::Type::DistanceLayer:
					if (distanceLayerNotFound)
					{
						if (echoDistance <= pLayerDef->GetMaxDepth()
							&& pLayerDef->GetMinDepth() <= echoDistance)
						{
							result.push_back(pChannelResult->getTabLayerResultFrequencies()[iFreq][layerIdx]);
							distanceLayerNotFound = false;
						}
					}
					break;
				}
			}
		}
	}
	else
	{
		// NMD - FAE 131 - m�me si l'echo est plus profond que le fond detect�e, on va verifier son appartenance au couche de distance
		if (m_parameter.isCustomLayersMode())
		{
			for (size_t layerIdx = 0; layerIdx < nbLayers; ++layerIdx)
			{
				const auto & pLayerDef = layers[layerIdx];
				// cas du layer de distance
				if (pLayerDef->GetLayerType() != Layer::Type::DistanceLayer)
				{
					if (echoDistance <= pLayerDef->GetMaxDepth()
						&& pLayerDef->GetMinDepth() <= echoDistance)
					{
						result.push_back(pChannelResult->getTabLayerResultFrequencies()[iFreq][layerIdx]);
					}
				}
			}
		}
		else
		{
			// cas du layer de distance
			for (size_t layerIdx = 0; layerIdx < nbLayers; ++layerIdx)
			{
				const auto & pLayerDef = layers[layerIdx];
				if (pLayerDef->GetLayerType() == Layer::Type::DistanceLayer)
				{
					if (echoDistance <= pLayerDef->GetMaxDepth()
						&& pLayerDef->GetMinDepth() <= echoDistance)
					{
						result.push_back(pChannelResult->getTabLayerResultFrequencies()[iFreq][layerIdx]);
					}
				}
			}
		}
	}
}

std::vector<EchoIntegrationOutput*> EchoIntegrationModule::GetOutputs() const
{
	return GetOutputContainer().getOutputs<EchoIntegrationOutput>();	
}

std::vector<EchoIntegrationOutput*> EchoIntegrationModule::GetOutputs(const tConsumerId consumerId) const
{
	return GetOutputContainer().getOutputs<EchoIntegrationOutput>(consumerId);
}