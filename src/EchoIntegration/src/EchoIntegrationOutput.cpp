
#include "EchoIntegration/EchoIntegrationOutput.h"
#include "MovNetwork/MvNetDataXMLLayer.h"
#include "MovNetwork/MvNetDataXMLCellset.h"
#include "MovNetwork/MvNetDataXML.h"
#include "MovNetwork/MvNetDataXMLHeading.h"
#include <math.h>
#include <time.h>

#include "M3DKernel/output/MovOutput.h"

#include "MovNetwork/MvNetDataXMLLayer.h"
#include "MovNetwork/MvNetDataXMLCellset.h"
#include "MovNetwork/MvNetDataXMLSndset.h"
#include "MovNetwork/MvNetDataXMLShipnav.h"
#include "MovNetwork/MvNetDataXMLLayerBoterr.h"

EchoIntegrationOutput::EchoIntegrationOutput(void)
{
	m_initialized = false;
	m_SounderID = 0;
}

EchoIntegrationOutput::~EchoIntegrationOutput(void)
{
	for (unsigned int i = 0; i < m_tabChannelResult.size(); i++)
	{
		delete m_tabChannelResult[i];
	}
}

// initialisation du vecteur des channels
void EchoIntegrationOutput::Initialize(int nbChannel, int nblayer)
{
	assert(!m_initialized);

	for (int i = 0; i < nbChannel; i++)
	{
		m_tabChannelResult.push_back(new EchoIntegrationChannelResult(nblayer));
	}

	m_initialized = true;
}

// calcul du sa et sv en fin d'ESU � partir de l'energie
void EchoIntegrationOutput::Compute(bool isWeighting)
{
	// Pour chaque faisceau
	for (unsigned int x = 0; x < m_tabChannelResult.size(); x++)
	{
		// Pour chaque frequence
		for (unsigned int i = 0; i < m_tabChannelResult[x]->getTabLayerResultFrequencies().size(); i++)
		{
			// Pour chaque couche
			for (unsigned int j = 0; j < m_tabChannelResult[x]->getTabLayerResultFrequencies()[i].size(); j++)
			{
				m_tabChannelResult[x]->getTabLayerResultFrequencies()[i][j]->Compute(m_tabChannelResult[x]->m_nbPing, isWeighting, m_tabChannelResult[x]->m_ESUDistance);
			}
		}
	}
}

// renseigne les objets n�cessaires � la s�rialisation (d�finition des channels, couches, etc...)
void EchoIntegrationOutput::PrepareForSerialization(EchoIntegrationParameter & echoIntegrationParameter,
	std::int32_t pingstart, std::int32_t pingend, HacTime timestart,
	HacTime timeend, double diststart, double distend, std::map<unsigned short, double> &rolls,
	std::map<unsigned short, double> & pitchs, std::map<unsigned short, double> &heaves,
	std::map<unsigned short, double> &latitudes,
	std::map<unsigned short, double> &longitudes,
	double groundspeed,
	std::map<unsigned short, double> &groundcourses, std::map<unsigned short, double> &headings, std::map<unsigned short, double> &surfspeeds,
	double driftcourse, double driftspeed, double altitude)
{
	unsigned short softChannelId;
	// Pour chaque channel...
	for (unsigned int i = 0; i < m_tabChannelResult.size(); i++)
	{
		softChannelId = (((m_tabChannelResult)[i])->getMvNetDataXMLSndset()).m_softChannelId;
		m_tabChannelResult[i]->PrepareForSerialization(echoIntegrationParameter, pingstart, pingend, timestart,
			timeend, diststart, distend, rolls[softChannelId], pitchs[softChannelId], heaves[softChannelId], latitudes[softChannelId], longitudes[softChannelId],
			groundspeed, groundcourses[softChannelId], headings[softChannelId], surfspeeds[softChannelId], driftcourse, driftspeed,
			altitude, m_SounderID);
	}
}

// red�finition des m�thode de s�rialisation et de d�s�rialisation
void EchoIntegrationOutput::XMLSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor)
{

	// Dans le cas du module d'�cho-int�gration, la trame XML a �mettre n'est pas la m�me sur le r�seau
	// ou dans un fichier. Ce n'est pas super beau...
	bool networkType = movOutput->isNetworkOutput();

	std::string str = "";
	// pour le mode network
	std::string strShipNav, strSndSet;

	// tableau des chaines de caract�res
	std::vector<std::string> vectStr;


	// OTK - FAE006 - on calcule une fois pour toute l'ESU les descripteurs de trames
	sndsetDescriptor snddesc;
	shipnavDescriptor shipnavdesc;
	cellsetDescriptor cellsetdesc;
	layerDescriptor layerDesc;
	botterrDescriptor botterrdesc;
	CMvNetDataXMLSndset::ComputeDescriptor(&tramaDescriptor, snddesc);
	CMvNetDataXMLShipnav::ComputeDescriptor(&tramaDescriptor, shipnavdesc);
	CMvNetDataXMLCellset::ComputeDescriptor(&tramaDescriptor, cellsetdesc);
	CMvNetDataXMLLayer::ComputeDescriptor(&tramaDescriptor, layerDesc);
	CMvNetDataXMLLayerBoterr::ComputeDescriptor(&tramaDescriptor, botterrdesc);

	for (unsigned int i = 0; i < m_tabChannelResult.size(); i++)
	{
		// OTK - 21/04/2009 - on laisse tomber la trame HEADING
		// TRAME HEADING (HEADER)
		/*CMvNetDataXMLHeading mvNetDataXMLHeading;
		// r�cup�ration de la date courante
		time_t rawtime;
		time ( &rawtime );

		HacTime now((std::uint32_t)rawtime, 0);

		mvNetDataXMLHeading.m_datetime = now;
		mvNetDataXMLHeading.m_acquisitionTime = now.m_TimeCpu + now.m_TimeFraction/10000.0;
		mvNetDataXMLHeading.m_surveyname = "Survey Name,Demo values";
		mvNetDataXMLHeading.m_comment = "Comment,Demo values";
		str = mvNetDataXMLHeading.createXMLSelectedOnly(&tramaDescriptor);
		vectStr.push_back(str);*/
		if (networkType)
		{
			//movOutput->AppendXML(vectStr);
			vectStr.clear();
		}

		EchoIntegrationChannelResult * pChannelResult = m_tabChannelResult[i];

		// trames SNDSET
		str = pChannelResult->getMvNetDataXMLSndset().createXMLSelectedOnly(snddesc);
		vectStr.push_back(str);
		if (networkType)
		{
			strSndSet = str;
		}

		// trames SHIPNAV
		str = pChannelResult->getMvNetDataXMLShipnav().createXMLSelectedOnly(shipnavdesc);
		vectStr.push_back(str);
		if (networkType)
		{
			strShipNav = str;
			vectStr.clear();
			vectStr.push_back(strShipNav);
			vectStr.push_back(strSndSet);
			movOutput->AppendXML(vectStr);
			vectStr.clear();
		}

		// parcours des couches (trames EILAYER et CELLSET)
		for (unsigned int j = 0; j < pChannelResult->getTabLayerResult().size(); j++)
		{
			str = pChannelResult->getTabLayerResult()[j]->getMvNetDataXMLCellSet().createXMLSelectedOnly(cellsetdesc);
			vectStr.push_back(str);
			if (networkType)
			{
				vectStr.push_back(strShipNav);
			}
			str = pChannelResult->getTabLayerResult()[j]->getMvNetDataXMLLayer().createXMLSelectedOnly(layerDesc);
			vectStr.push_back(str);
			if (networkType)
			{
				vectStr.push_back(strSndSet);
				movOutput->AppendXML(vectStr);
				vectStr.clear();
			}
		}

		// cellset total
		str = pChannelResult->getMvNetDataXMLCellSetTotal().createXMLSelectedOnly(cellsetdesc);
		vectStr.push_back(str);
		if (networkType)
		{
			vectStr.push_back(strShipNav);
		}

		// total des couches de surface
		str = pChannelResult->getMvNetDataXMLLayerTotal().createXMLSelectedOnly(layerDesc);
		vectStr.push_back(str);
		if (networkType)
		{
			vectStr.push_back(strSndSet);
			movOutput->AppendXML(vectStr);
			vectStr.clear();
		}

		// erreur due a l'int�gration du fond
		str = pChannelResult->getMvNetDataXMLLayerBoterr().createXMLSelectedOnly(botterrdesc);
		vectStr.push_back(str);
		if (networkType)
		{
			movOutput->AppendXML(vectStr);
			vectStr.clear();
		}

	}

	if (!networkType)
	{
		movOutput->AppendXML(vectStr);
	}
}



void EchoIntegrationOutput::CSVSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor)
{
	std::string str = "";

	// OTK - 21/04/2009 - on laisse tomber la trame HEADING
	//CMvNetDataXMLHeading mvNetDataXMLHeading;

	// r�cup�ration de la date courante
	time_t rawtime;
	time(&rawtime);

	HacTime now((std::uint32_t)rawtime, 0);

	/*mvNetDataXMLHeading.m_datetime = now;
	mvNetDataXMLHeading.m_acquisitionTime = now.m_TimeCpu + now.m_TimeFraction/10000.0;
	mvNetDataXMLHeading.m_surveyname = "Survey Name,Demo values";
	mvNetDataXMLHeading.m_comment = "Comment,Demo values";*/

	// OTK - FAE006 - on calcule une fois pour toute l'ESU les descripteurs de trames
	sndsetDescriptor snddesc;
	shipnavDescriptor shipnavdesc;
	cellsetDescriptor cellsetdesc;
	layerDescriptor layerdesc;
	botterrDescriptor botterrdesc;
	CMvNetDataXMLSndset::ComputeDescriptor(&tramaDescriptor, snddesc);
	CMvNetDataXMLShipnav::ComputeDescriptor(&tramaDescriptor, shipnavdesc);
	CMvNetDataXMLCellset::ComputeDescriptor(&tramaDescriptor, cellsetdesc);
	CMvNetDataXMLLayer::ComputeDescriptor(&tramaDescriptor, layerdesc);
	CMvNetDataXMLLayerBoterr::ComputeDescriptor(&tramaDescriptor, botterrdesc);

	// pour tous les channels
	for (unsigned int i = 0; i < m_tabChannelResult.size(); i++)
	{
		EchoIntegrationChannelResult * pChannelResult = m_tabChannelResult[i];

		// et pour toutes les couches
		for (unsigned int j = 0; j < pChannelResult->getTabLayerResult().size(); j++)
		{

			str.append(pChannelResult->getTabLayerResult()[j]->getMvNetDataXMLCellSet().ToStr(now));
			str.append(";");
			////str.append(mvNetDataXMLHeading.createCSV(&tramaDescriptor));

			// trames SNDSET
			str.append(pChannelResult->getMvNetDataXMLSndset().createCSV(snddesc));

			// trames SHIPNAV
			str.append(pChannelResult->getMvNetDataXMLShipnav().createCSV(shipnavdesc));

			str.append(pChannelResult->getTabLayerResult()[j]->getMvNetDataXMLCellSet().createCSV(cellsetdesc));
			str.append(pChannelResult->getTabLayerResult()[j]->getMvNetDataXMLLayer().createCSV(layerdesc));


			// erreur due a l'int�gration du fond
			str.append(pChannelResult->getMvNetDataXMLLayerBoterr().createCSV(botterrdesc));

			str.append("\n");
		}

		//FAE113 ajout couche total 

		str.append(pChannelResult->getMvNetDataXMLCellSetTotal().ToStr(now));
		str.append(";");
		////str.append(mvNetDataXMLHeading.createCSV(&tramaDescriptor));

		// trames SNDSET
		str.append(pChannelResult->getMvNetDataXMLSndset().createCSV(snddesc));

		// trames SHIPNAV
		str.append(pChannelResult->getMvNetDataXMLShipnav().createCSV(shipnavdesc));

		// cellset total
		str.append(pChannelResult->getMvNetDataXMLCellSetTotal().createCSV(cellsetdesc));

		// total des couches de surface
		str.append(pChannelResult->getMvNetDataXMLLayerTotal().createCSV(layerdesc));

		// erreur due a l'int�gration du fond
		str.append(pChannelResult->getMvNetDataXMLLayerBoterr().createCSV(botterrdesc));

		str.append("\n");
	}

	movOutput->Write(str);
}

void EchoIntegrationOutput::XMLHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor)
{
	std::string header = CMvNetDataXML::createXMLHeader("MOVIES_EILayer", 1, 0);
	header.append(CMvNetDataXML::createXMLFooter());
	movOutput->CreateXML(header);
}

void EchoIntegrationOutput::CSVHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor)
{
	//Variables locales
	std::string header = "";

	// message description node cfg
	header += "MOVIES_EILayer";

	//titre des colonnes
	// Titres header
	//header += tramaDescriptor.GetTitlesByCat("MOVIES_EILayer\\heading");

	// Titres sndset
	header += tramaDescriptor.GetTitlesByCat("MOVIES_EILayer\\sndset");

	// Titres shipnav
	header += tramaDescriptor.GetTitlesByCat("MOVIES_EILayer\\shipnav");

	// Titres cellset
	header += tramaDescriptor.GetTitlesByCat("MOVIES_EILayer\\cellset");

	// Titres layer
	header += tramaDescriptor.GetTitlesByCat("MOVIES_EILayer\\eilayer");

	// Titres botterr
	header += tramaDescriptor.GetTitlesByCat("MOVIES_EILayer\\boterr");

	header += "\n";

	movOutput->Write(header);
}
