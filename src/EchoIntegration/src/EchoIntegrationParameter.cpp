#include "EchoIntegration/EchoIntegrationParameter.h"

// dépendances
#include "M3DKernel/parameter/ParameterDataType.h"
#include "M3DKernel/config/MovConfig.h"

#include <algorithm>

using namespace BaseKernel;

namespace
{
	const auto PARAMETER_MODULE_NAME = "EchoIntegrationParameter";

	const auto RECORD_FILE_SUFFIX = "lay";
	
	const auto CUSTOM_LAYERS_MODE_KEY = "CustomLayersMode";
	const auto LOW_THRESHOLD_KEY = "LowThreshold";
	const auto HIGH_THRESHOLD_KEY = "HighThreshold";
	const auto OFFSET_BOTTOM_KEY = "OffsetBottom";
	const auto SURFACE_LAYER_NUMBER_KEY = "SurfaceLayerNumber";
	const auto BOTTOM_LAYER_NUMBER_KEY = "BottomLayerNumber";
	const auto DISTANCE_LAYER_NUMBER_KEY = "DistanceLayerNumber";
	
}

// constructeur
EchoIntegrationParameter::EchoIntegrationParameter() : ParameterModuleTransducerFiltered(PARAMETER_MODULE_NAME, eLayer)
{
	// Initialisation des seuils par défaut
	m_lowThreshold = -60;
	m_highThreshold = 0;
	m_offsetBottom = 0.5;

	// IPSIS - OTK - on ne crée qu'une couche de test par defaut
	addLayerDef(Layer::Type::SurfaceLayer, 0, 100);

	GetParameterBroadcastAndRecord().m_fileSuffix = RECORD_FILE_SUFFIX;

	// OTK - 22/10/2009 - mode "avancé" pour la définition des couches quelconques
	m_customLayersMode = false;
}

EchoIntegrationParameter::~EchoIntegrationParameter()
{
	clearLayersDef();
}

// sauvegarde des paramètres dans le fichier de configuration associé
bool EchoIntegrationParameter::Serialize(MovConfig * movConfig)
{	
	// Début de la sérialisation du module
	movConfig->SerializeData(this, eParameterModule);

	// OTK - 22/10/2009 - mode "avancé" pour la définition des couches quelconques
	movConfig->SerializeData<bool>(m_customLayersMode, eBool, CUSTOM_LAYERS_MODE_KEY);

	// sérialisation des données membres
	movConfig->SerializeData<double>(m_lowThreshold, eDouble, LOW_THRESHOLD_KEY);
	movConfig->SerializeData<double>(m_highThreshold, eDouble, HIGH_THRESHOLD_KEY);
	movConfig->SerializeData<double>(m_offsetBottom, eDouble, OFFSET_BOTTOM_KEY);

	unsigned int nbSurfaceLayers = 0;
	unsigned int nbBottomLayers = 0;
	unsigned int nbDistanceLayers = 0;
	for (const auto& layer : m_tabLayerDef)
	{
		switch (layer->GetLayerType())
		{
		case Layer::Type::SurfaceLayer:
			++nbSurfaceLayers;
			break;
		case Layer::Type::BottomLayer:
			++nbBottomLayers;
			break;
		case Layer::Type::DistanceLayer:
			++nbDistanceLayers;
			break;
		}
	}


	movConfig->SerializeData<unsigned int>(nbSurfaceLayers, eUInt, SURFACE_LAYER_NUMBER_KEY);
	movConfig->SerializeData<unsigned int>(nbBottomLayers, eUInt, BOTTOM_LAYER_NUMBER_KEY);
	movConfig->SerializeData<unsigned int>(nbDistanceLayers, eUInt, DISTANCE_LAYER_NUMBER_KEY);

	// attention, les couches de surface doivent etre avant les couches de fond
	for (const auto& layer : m_tabLayerDef)
	{
		layer->Serialize(movConfig);
	}

	GetParameterBroadcastAndRecord().Serialize(movConfig);
	
	// Sérialisation des transducteurs actifs
	SerializeActiveTransducers(movConfig);

	// fin de la sérialisation du module
	movConfig->SerializePushBack();

	return true;
}

// lecture des paramètres dans le fichier de configuration associé
bool EchoIntegrationParameter::DeSerialize(MovConfig * movConfig)
{	
	// Début de la désérialisation du module
	auto result = movConfig->DeSerializeData(this, eParameterModule);
	if (result)
	{		
		// OTK - 22/10/2009 - mode "avancé" pour la définition des couches quelconques
		result = movConfig->DeSerializeData<bool>(&m_customLayersMode, eBool, CUSTOM_LAYERS_MODE_KEY) && result;
		
		// désérialisation des données membres
		result = movConfig->DeSerializeData<double>(&m_lowThreshold, eDouble, LOW_THRESHOLD_KEY) && result;
		result = movConfig->DeSerializeData<double>(&m_highThreshold, eDouble, HIGH_THRESHOLD_KEY) && result;
		result = movConfig->DeSerializeData<double>(&m_offsetBottom, eDouble, OFFSET_BOTTOM_KEY) && result;

		unsigned int nbSurfaceLayers = 0;
		unsigned int nbBottomLayers = 0;
		unsigned int nbDistanceLayers = 0;
		result = movConfig->DeSerializeData<unsigned int>(&nbSurfaceLayers, eUInt, SURFACE_LAYER_NUMBER_KEY) && result;
		result = movConfig->DeSerializeData<unsigned int>(&nbBottomLayers, eUInt, BOTTOM_LAYER_NUMBER_KEY) && result;
		result = movConfig->DeSerializeData<unsigned int>(&nbDistanceLayers, eUInt, DISTANCE_LAYER_NUMBER_KEY) && result;

		// réinitialisation du tableau m_tabLayerDef
		clearLayersDef();

		// lecture des couches de surface
		for (unsigned int i = 0; i < nbSurfaceLayers; i++)
		{
			result &= createLayer(movConfig, Layer::Type::SurfaceLayer);
		}

		// lecture des couches de fond
		for (unsigned int i = 0; i < nbBottomLayers; i++)
		{
			result &= createLayer(movConfig, Layer::Type::BottomLayer);
		}

		// lecture des couches en distance au transducteur
		for (unsigned int i = 0; i < nbDistanceLayers; i++)
		{
			result &= createLayer(movConfig, Layer::Type::DistanceLayer);
		}

		result = GetParameterBroadcastAndRecord().DeSerialize(movConfig) && result;
		
		// Désérialisation des transducteurs actifs
		result &= DeSerializeActiveTransducers(movConfig);
	}

	// Fin de la désérialisation du module
	movConfig->DeSerializePushBack();

	return result;
}

const LayerDef * EchoIntegrationParameter::getFirstLayer(Layer::Type layerType) const
{
	const auto it = std::find_if(m_tabLayerDef.cbegin(), m_tabLayerDef.cend(), [layerType](auto layer) { return layer->GetLayerType() == layerType; });
	if (it != m_tabLayerDef.cend())
	{
		return *it;
	}
	return nullptr;
}

int EchoIntegrationParameter::getFirstLayerIdx(Layer::Type layerType) const
{
	const auto it = std::find_if(m_tabLayerDef.cbegin(), m_tabLayerDef.cend(), [layerType](auto layer) { return layer->GetLayerType() == layerType; });
	if (it != m_tabLayerDef.cend())
	{
		return std::distance(m_tabLayerDef.cbegin(), it);
	}
	return -1;
}

std::map<Layer::Type, std::vector<size_t>> EchoIntegrationParameter::getAllLayerTypeIndexes() const {

	std::map<Layer::Type, std::vector<size_t>> map;

	for (size_t i = 0; i < m_tabLayerDef.size(); ++i) {
		map[m_tabLayerDef[i]->GetLayerType()].push_back(i);
	}

	return map;
}

std::vector<size_t> EchoIntegrationParameter::getLayerTypeIndexes(const Layer::Type& type) const {
	return getAllLayerTypeIndexes()[type];
}

bool EchoIntegrationParameter::createLayer(MovConfig* movConfig, const Layer::Type& layerType)
{
	const auto layer = new LayerDef(layerType);
	const auto result = layer->DeSerialize(movConfig);
	m_tabLayerDef.push_back(layer);
	return result;
}

void EchoIntegrationParameter::clearLayersDef()
{
	std::for_each(m_tabLayerDef.begin(), m_tabLayerDef.end(), [](LayerDef* layeDef) { delete layeDef; });
	m_tabLayerDef.clear();
}

size_t EchoIntegrationParameter::countLayersDef() const
{
	return m_tabLayerDef.size();
}

const LayerDef* EchoIntegrationParameter::getLayerAt(const size_t index) const
{
	return index < m_tabLayerDef.size() ? m_tabLayerDef[index] : nullptr;
}

const LayerDef* EchoIntegrationParameter::addLayerDef(LayerDef* layer)
{
	m_tabLayerDef.push_back(layer);
	return m_tabLayerDef.back();
}

const LayerDef* EchoIntegrationParameter::addLayerDef(const Layer::Type& layerType, double minDepth, double maxDepth)
{
	return addLayerDef(new LayerDef(layerType, minDepth, maxDepth));	
}

double EchoIntegrationParameter::getLowThreshold() const
{
	return m_lowThreshold;
}

double EchoIntegrationParameter::getHighThreshold() const
{
	return m_highThreshold;
}

double EchoIntegrationParameter::getOffsetBottom() const
{
	return m_offsetBottom;
}

bool EchoIntegrationParameter::isCustomLayersMode() const
{
	return m_customLayersMode;
}

void EchoIntegrationParameter::setLowThreshold(const double lowThreshold)
{
	m_lowThreshold = lowThreshold;
}

void EchoIntegrationParameter::setHighThreshold(const double highThreshold)
{
	m_highThreshold = highThreshold;
}

void EchoIntegrationParameter::setOffsetBottom(const double offsetBottom)
{
	m_offsetBottom = offsetBottom;
}

void EchoIntegrationParameter::setCustomLayersMode(const bool customLayersMode)
{
	m_customLayersMode = customLayersMode;
}

const std::vector<LayerDef*>& EchoIntegrationParameter::getLayersDefs() const
{
	return m_tabLayerDef;
}
