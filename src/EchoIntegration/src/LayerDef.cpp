﻿#include "EchoIntegration/LayerDef.h"

namespace {
	const auto SURFACE_LAYER_KEY = "LayerDefSurface";	
	const auto SURFACE_LAYER_MIN_HEIGHT_KEY = "MinDepth";
	const auto SURFACE_LAYER_MAX_HEIGHT_KEY = "MaxDepth";

	const auto BOTTOM_LAYER_KEY = "LayerDefBottom";
	const auto BOTTOM_LAYER_MIN_HEIGHT_KEY = "MinHeight";
	const auto BOTTOM_LAYER_MAX_HEIGHT_KEY = "MaxHeight";

	const auto DISTANCE_LAYER_KEY = "LayerDefDistance";
	const auto DISTANCE_LAYER_MIN_HEIGHT_KEY = "MinDistance";
	const auto DISTANCE_LAYER_MAX_HEIGHT_KEY = "MaxDistance";
}

LayerDef::LayerDef(Layer::Type layerType, double start, double end)
	: m_start(start)
	, m_end(end)
	, m_layerType(layerType)
{	
}

LayerDef::LayerDef(Layer::Type layerType)
	: LayerDef(layerType, 0, std::numeric_limits<int>::max())
{
}

LayerDef::LayerDef()
	: LayerDef(Layer::Type::SurfaceLayer)
{
}

bool LayerDef::LayerSerialize(LayerDef* layer, BaseKernel::MovConfig* movConfig, const char* mainKey, const char* minDepthKey, const char* maxDepthKey)
{
	// debut de la sérialisation de l objet
	movConfig->SerializeData(static_cast<ParameterObject*>(layer), BaseKernel::eParameterObject, mainKey);

	// sérialisation des données membres
	movConfig->SerializeData<double>(layer->m_start, BaseKernel::eDouble, minDepthKey);
	movConfig->SerializeData<double>(layer->m_end, BaseKernel::eDouble, maxDepthKey);

	// fin de la sérialisation de l'objet
	movConfig->SerializePushBack();

	return true;
}

bool LayerDef::LayerDeSerialize(LayerDef* layer, BaseKernel::MovConfig* movConfig, const char* mainKey, const char* minDepthKey, const char* maxDepthKey)
{
	bool result = true;

	// debut de la désérialisation de l'objet
	result = result && movConfig->DeSerializeData(static_cast<ParameterObject*>(layer), BaseKernel::eParameterObject, mainKey);

	// désérialisation des données membres
	result = result && movConfig->DeSerializeData<double>(&layer->m_start, BaseKernel::eDouble, minDepthKey);
	result = result && movConfig->DeSerializeData<double>(&layer->m_end, BaseKernel::eDouble, maxDepthKey);

	// fin de la désérialisation de l'objet
	movConfig->DeSerializePushBack();

	return result;
}

bool LayerDef::Serialize(BaseKernel::MovConfig * movConfig)
{
	bool result = false;

	switch (m_layerType)
	{
	case Layer::Type::SurfaceLayer:
		result = LayerSerialize(this, movConfig, SURFACE_LAYER_KEY, SURFACE_LAYER_MIN_HEIGHT_KEY, SURFACE_LAYER_MAX_HEIGHT_KEY);
		break;

	case Layer::Type::BottomLayer:
		result = LayerSerialize(this, movConfig, BOTTOM_LAYER_KEY, BOTTOM_LAYER_MIN_HEIGHT_KEY, BOTTOM_LAYER_MAX_HEIGHT_KEY);
		break;

	case Layer::Type::DistanceLayer:
		result = LayerSerialize(this, movConfig, DISTANCE_LAYER_KEY, DISTANCE_LAYER_MIN_HEIGHT_KEY, DISTANCE_LAYER_MAX_HEIGHT_KEY);
		break;
	}

	return result;
}

bool LayerDef::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = false;
	
	switch (m_layerType)
	{
	case Layer::Type::SurfaceLayer:
		result = LayerDeSerialize(this, movConfig, SURFACE_LAYER_KEY, SURFACE_LAYER_MIN_HEIGHT_KEY, SURFACE_LAYER_MAX_HEIGHT_KEY);
		break;

	case Layer::Type::BottomLayer:
		result = LayerDeSerialize(this, movConfig, BOTTOM_LAYER_KEY, BOTTOM_LAYER_MIN_HEIGHT_KEY, BOTTOM_LAYER_MAX_HEIGHT_KEY);
		break;

	case Layer::Type::DistanceLayer:
		result = LayerDeSerialize(this, movConfig, DISTANCE_LAYER_KEY, DISTANCE_LAYER_MIN_HEIGHT_KEY, DISTANCE_LAYER_MAX_HEIGHT_KEY);
		break;
	}

	return result;
}

double LayerDef::GetMinDepth(const double maxRange) const
{
	return m_layerType == Layer::Type::BottomLayer
		? maxRange - m_end
		: m_start;
}

double LayerDef::GetMaxDepth(const double maxRange) const
{
	return m_layerType == Layer::Type::BottomLayer
		? maxRange - m_start
		: m_end;
}