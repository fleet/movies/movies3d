#pragma once

#include "FilteringExport.h"
#include "Filtering/DataFilter.h"

#include "M3DKernel/parameter/ParameterObject.h"

#include <map>
#include <string>

typedef std::vector<std::pair<int, int>> BeamIdRangeVector;
typedef std::map<std::string, BeamIdRangeVector> MapTransducerToBeamRange;

// ***************************************************************************
// Filtre de faisceaux par groupe (NMD - FAE091)
// ***************************************************************************
class FILTER_API BeamIdGroupFilter : public DataFilter
{
public:
	BeamIdGroupFilter();

	virtual ~BeamIdGroupFilter();

	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);

	// Retourne la plage des faisceaux filtr�s pour un transducteur donn�
	BeamIdRangeVector * GetBeamIdRangeVector(const std::string & transducerName);

	MapTransducerToBeamRange & GetMapBeamIdRangeVector() { return m_mapTransducerToBeamRange; }

	// Serialisation de la config du filtre
	bool Serialize(BaseKernel::MovConfig * movConfig);

	/// Deserialisation de la config du filtre
	bool DeSerialize(BaseKernel::MovConfig * movConfig);

private:
	MapTransducerToBeamRange m_mapTransducerToBeamRange;

	void BuildDefaultSettings();
};

