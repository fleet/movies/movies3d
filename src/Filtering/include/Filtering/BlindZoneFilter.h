#pragma once
#include "Filtering/DataFilter.h"

class FILTER_API BlindZoneFilter : public DataFilterValue
{
public:
	BlindZoneFilter(void);
	virtual ~BlindZoneFilter(void);
	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan) = 0;
	virtual const char* getValueName();
	void FilterArea(Sounder*pSound, PingFan *refFrame, unsigned int numFan);
};

class FILTER_API BlindZoneFilterMono : public BlindZoneFilter
{
public:
	BlindZoneFilterMono(void);
	virtual ~BlindZoneFilterMono(void) {};
	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);
};

class FILTER_API BlindZoneFilterMulti : public BlindZoneFilter
{
public:
	BlindZoneFilterMulti(void);
	virtual ~BlindZoneFilterMulti(void) {};
	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);
};
