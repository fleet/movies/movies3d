
/******************************************************************************/
/*	Project:	MOVIE 3d														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		GroundViewVolumePing.h												  */
/******************************************************************************/
#pragma once

#include "FilteringExport.h"

#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"

class FILTER_API DataFilter
{
public:
	DataFilter();
	~DataFilter();


	/// Appel methode de filtrage	
	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan) = 0;

	bool getEnable() { return m_Enable; }
	virtual void setEnable(bool a) { m_Enable = a; }
	virtual char* getName() { return m_Name; }
	virtual std::string GetShortName();
private:
	bool m_Enable;
	char *m_Name;
	char *m_CompleteName;
protected:
	void SetName(const char*);
	void SetCompleteName(const char*);
	char * getCompleteName() { return m_CompleteName; }
};
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

class FILTER_API DataFilterWithTolerance : public DataFilter
{
public:
	DataFilterWithTolerance() { m_tolerance = 0; };
	//	virtual bool isRejected(BaseMathLib::Vector2I &coordPolar,  DataFmt &valueDB, PingFan *refFrame, Transducer *pTransducer)=0;
	float getTolerance() { return m_tolerance; }
	void setTolerance(float a) { m_tolerance = a; }

	virtual char* getName();

	virtual const char* getToleranceName();

protected:
	float m_tolerance;
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

class  FILTER_API DataFilterBase : public DataFilter
{
public:
	DataFilterBase();
	//	virtual bool isRejected(BaseMathLib::Vector2I &coordPolar,  DataFmt &valueDB, PingFan *refFrame, Transducer *pTransducer);

};



/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

class FILTER_API DataFilterToleranceThreshold : public DataFilter
{
public:
	DataFilterToleranceThreshold() { m_tolerance = 0; m_threshold = -5300; };
	//	virtual bool isRejected(BaseMathLib::Vector2I &coordPolar,  DataFmt &valueDB, PingFan *refFrame, Transducer *pTransducer)=0;
	float getTolerance() { return m_tolerance; }
	void setTolerance(float a) { m_tolerance = a; }

	double getThreshold() { return m_threshold; }
	virtual void setThreshold(double a) { m_threshold = a; }

	virtual char* getName();

	virtual const char* getToleranceName();
	virtual const char* getThresholdName();

protected:
	float m_tolerance;
	double m_threshold;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

class FILTER_API DataFilterValue : public DataFilter
{
public:
	DataFilterValue() { m_value = 0; };
	//	virtual bool isRejected(BaseMathLib::Vector2I &coordPolar,  DataFmt &valueDB, PingFan *refFrame, Transducer *pTransducer)=0;

	double getValue() { return m_value; }
	void setValue(double a) { a < 0 ? m_value = -a : m_value = a; }

	virtual char* getName();

	virtual const char* getValueName();
protected:
	double m_value;
};


