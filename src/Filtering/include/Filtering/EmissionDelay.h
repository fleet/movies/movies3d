#pragma once
#include "M3DKernel/parameter/ParameterObject.h"
#include "FilteringExport.h"

#include <string>
#include <vector>

class FILTER_API EmissionDelay : public BaseKernel::ParameterObject
{
public:
	EmissionDelay(const char  *Name);
	virtual ~EmissionDelay(void);

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	/// return true if names are equals
	bool checkTransName(std::string Name);

	std::string m_TransducerName;
	// nombre de voies par groupes et d�lai associ�
	std::vector<std::pair<unsigned int, int>> m_Delays;
};
