#pragma once
#include "FilteringExport.h"
#include "M3DKernel/parameter/ParameterObject.h"

#include <vector>

class EmissionDelay;
class FILTER_API EmissionDelaySet : public BaseKernel::ParameterObject
{
public:
	EmissionDelaySet(void);
	virtual ~EmissionDelaySet(void);

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	EmissionDelay *GetEmissionDelay(const char *m_Name);

	unsigned int GetRecordCount();
	EmissionDelay* GetRecord(unsigned int idx);

private:
	std::vector<EmissionDelay*> m_RecordSet;

	EmissionDelay* CreateDefaultEmissionDelay(const char *m_Name);

	void BuildDefaultSettings();
};