
/******************************************************************************/
/*	Project:	MOVIE 3d														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		GroundViewFilter.h												  */
/******************************************************************************/
#ifndef _3D_VIEW_FILTER_MGR
#define _3D_VIEW_FILTER_MGR
#include "FilteringExport.h"
#include "FilterMgrParameter.h"

#include "BaseMathLib/Vector3.h"
#include "M3DKernel/module/ProcessModule.h"

#include <vector>

class DataFilter;
class DataFilterValue;
class PingFan;
class Transducer;
class SounderMgr;
class InterferenceFilter;
class FILTER_API FilterMgr : public ProcessModule, public BaseKernel::ParameterModule
{
public:
	MovCreateMacro(FilterMgr);

public:
	virtual void PingFanAdded(PingFan *pFan);
	virtual void SounderChanged(std::uint32_t sounderId);
	virtual void StreamClosed(const char *streamName) {};
	virtual void StreamOpened(const char *streamName) {};

	DataFilter* getFilter(unsigned int idx);
	InterferenceFilter* getInterferenceFilter();
	unsigned int getNbFilter();

	DataFilterValue* getBlindZoneFilterMono();
	DataFilterValue* getBlindZoneFilterMulti();

	// IPSIS - OTK - ajout accesseur sur param�tres pour la gestion des sorties
	BaseKernel::ParameterModule& GetParameterModule() { return m_parameter; };

	virtual bool Serialize(BaseKernel::MovConfig * movConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * movConfig);

	virtual void onEnableStateChange() override;

	~FilterMgr();

protected:
	FilterMgr();

	//	bool isRejected(BaseMathLib::Vector2I &coordPolar,  DataFmt &valueDB, PingFan *refFrame, Transducer *pTransducer);
	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);

	// IPSIS - OTK - ajout parametres (puisque tous les modules doivent pouvoir emettre sur le r�seau)
	FilterMgrParameter m_parameter;

private:
	static FilterMgr* m_pViewFilterMgr;

	std::vector<DataFilter*> m_vectFilterAlgo;

	// OTK - FAE219 - pour acc�s rapide aux param�tres des filtres blind zone
	DataFilterValue * m_BlindZoneFilterMono;
	DataFilterValue * m_BlindZoneFilterMulti;
};

#endif
