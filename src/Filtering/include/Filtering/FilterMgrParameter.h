// -*- C++ -*-
// ****************************************************************************
// Class: FilterMgrParameter
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: K�vin DUGUE
// Date  : Avril 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "FilteringExport.h"
#include "M3DKernel/parameter/ParameterModule.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
class FILTER_API FilterMgrParameter :
	public BaseKernel::ParameterModule
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	FilterMgrParameter(void);

	// Destructeur
	virtual ~FilterMgrParameter(void);

	virtual bool Serialize(BaseKernel::MovConfig * movConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * movConfig);
};
