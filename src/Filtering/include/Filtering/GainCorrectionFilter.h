#pragma once
#include "FilteringExport.h"
#include "Filtering/DataFilter.h"


class FILTER_API GainCorrectionFilter : public DataFilterToleranceThreshold
{
public:
	GainCorrectionFilter();
	virtual ~GainCorrectionFilter(void);

	virtual const char* getToleranceName();
	virtual const char* getThresholdName();

	virtual void setThreshold(double a) { m_threshold = a; }

	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);

};
