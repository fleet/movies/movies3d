#pragma once
#include "FilteringExport.h"
#include "Filtering/DataFilter.h"
#include "EmissionDelaySet.h"


class FILTER_API InterferenceFilter : public DataFilterToleranceThreshold
{
public:
	InterferenceFilter();
	virtual ~InterferenceFilter(void);

	virtual const char* getToleranceName();
	virtual const char* getThresholdName();

	virtual char* getName();

	virtual void setThreshold(double a) { m_threshold = a; }

	virtual int getProportion() { return m_Prop; }
	virtual int getHoleSize() { return m_HoleSize; }
	virtual int getWindowSize() { return m_WindowSize; }

	virtual void setProportion(int a) { m_Prop = a; }
	virtual void setHoleSize(int a) { m_HoleSize = a; }
	virtual void setWindowSize(int a) { m_WindowSize = a; }

	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);

	virtual EmissionDelaySet& GetEmissionDelaySet() { return m_EmissionDelaySet; }

private:
	// proportion minimale des voies devant pr�senter le parasite (en %)
	int m_Prop;
	// comparaison de l'�chantillon courant avec l'�chantillon m_HoleSize �chantillons avant
	int m_HoleSize;
	// taille de la fen�tre de moyennage en �chantillons
	int m_WindowSize;

	// param�tres de d�calage temporel � l'�mission
	EmissionDelaySet m_EmissionDelaySet;

};
