#pragma once
#include "Filtering/DataFilter.h"

class FILTER_API LowerFilter : public DataFilterValue
{
public:
	LowerFilter(void);
	virtual ~LowerFilter(void);
	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);
	virtual const char* getValueName();
	virtual void setValue(double a) { m_value = a; }
};
