#pragma once
#include "Filtering/DataFilter.h"

class FILTER_API SideLobeFilter : public DataFilterValue
{
public:
	SideLobeFilter(void);
	virtual ~SideLobeFilter(void);
	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);
	virtual const char* getValueName();
	virtual void setValue(double a) { m_value = a; }
};