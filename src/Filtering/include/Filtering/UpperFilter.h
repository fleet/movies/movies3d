#pragma once
#include "Filtering/DataFilter.h"

class FILTER_API UpperFilter : public DataFilterValue
{
public:
	UpperFilter(void);
	virtual ~UpperFilter(void);
	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);
	virtual const char* getValueName();
	virtual void setValue(double a) { m_value = a; }
};
