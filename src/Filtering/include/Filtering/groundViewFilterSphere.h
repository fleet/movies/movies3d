
/******************************************************************************/
/*	Project:	MOVIE 3d														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		VolumePing.h												  */
/******************************************************************************/
#pragma once

#include "FilteringExport.h"
#include "Filtering/DataFilter.h"
#include "BaseMathLib/Vector2.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////

class FILTER_API FilterSphereOffset : public DataFilterToleranceThreshold
{
public:
	FilterSphereOffset();
	//	virtual bool isRejected( BaseMathLib::Vector2I &coordPolar,  DataFmt &valueDB, PingFan *refFrame, Transducer *pTransducer);
	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);
	virtual std::string GetShortName();

};

class FILTER_API FilterSphereVariableOffset : public DataFilterToleranceThreshold
{
public:
	FilterSphereVariableOffset();
	//	virtual bool isRejected(BaseMathLib::Vector2I &coordPolar,  DataFmt &valueDB, PingFan *refFrame, Transducer *pTransducer);
	virtual void ProcessRejection(PingFan *refFrame, unsigned int numFan);
	virtual std::string GetShortName();

};
//
//class FILTER_API FilterSphereER60Pollution : public DataFilterToleranceThreshold
//{
//public:
//	FilterSphereER60Pollution();
////	virtual bool isRejected(BaseMathLib::Vector2I &coordPolar,  DataFmt &valueDB, PingFan *refFrame, Transducer *pTransducer);
//	virtual void ProcessRejection( PingFan *refFrame,unsigned int numFan);
//	
//};
