#include "Filtering/BeamIdGroupFilter.h"

#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

BeamIdGroupFilter::BeamIdGroupFilter()
	:DataFilter()
{
	SetName("Beam Id Group Filter");

	BuildDefaultSettings();
}

BeamIdGroupFilter::~BeamIdGroupFilter()
{
}

void BeamIdGroupFilter::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	Sounder *pSound = refFrame->getSounderRef();
	if (pSound)
	{
		unsigned int nbTrans = pSound->GetTransducerCount();
		for (unsigned int transNum = 0; transNum < nbTrans; transNum++)
		{
			Transducer *pTransducer = pSound->GetTransducer(transNum);


			//for (unsigned int beam=0; beam<pTransducer->m_numberOfSoftChannel; beam++)

			// recuperation du filtre associ�
			MapTransducerToBeamRange::const_iterator iterGroup = m_mapTransducerToBeamRange.find(pTransducer->m_transName);
			if (iterGroup != m_mapTransducerToBeamRange.end())
			{
				MemoryStruct *pPolarMem = refFrame->GetMemorySetRef()->GetMemoryStruct(transNum);
				int echoNb = pPolarMem->GetDataFmt()->getSize().y;

				for (BeamIdRangeVector::const_iterator iter = iterGroup->second.begin(); iter != iterGroup->second.end(); iter++)
				{
					int firstBeamId = iter->first;
					int lastBeamId = iter->second;

					//filtrage de tous les faisceaux concern�s
					for (int numBeam = firstBeamId; numBeam <= lastBeamId; numBeam++)
					{
						for (int iEcho = 0; iEcho < echoNb; iEcho++)
						{
							char * pFilter = pPolarMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(numBeam, iEcho));
							*(pFilter) = 1;
						}
					}
				}
			}
		}
	}
}

BeamIdRangeVector * BeamIdGroupFilter::GetBeamIdRangeVector(const std::string & transducerName)
{
	BeamIdRangeVector * pBeamIdRangeVector = NULL;

	MapTransducerToBeamRange::iterator iterMap = m_mapTransducerToBeamRange.find(transducerName);
	if (iterMap != m_mapTransducerToBeamRange.end())
	{
		pBeamIdRangeVector = &(iterMap->second);
	}

	return pBeamIdRangeVector;
}

void BeamIdGroupFilter::BuildDefaultSettings()
{
	m_mapTransducerToBeamRange.clear();
	m_mapTransducerToBeamRange["Generic MultiBeam Transducer"];
	m_mapTransducerToBeamRange["ME70"];
}

bool BeamIdGroupFilter::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "BeamIdRanges");

	unsigned int nbTrans = (unsigned int)m_mapTransducerToBeamRange.size();
	movConfig->SerializeData<unsigned int>(nbTrans, eUInt, "NbTransducer");

	for (MapTransducerToBeamRange::iterator iterTransducer = m_mapTransducerToBeamRange.begin(); iterTransducer != m_mapTransducerToBeamRange.end(); iterTransducer++)
	{
		// debut de la s�rialisation du module
		movConfig->SerializeData((ParameterObject*)this, eParameterObject, "TransducerBeamIdRange");
		movConfig->SerializeData<std::string>(iterTransducer->first, eString, "Transducer");

		unsigned int nbRange = iterTransducer->second.size();
		movConfig->SerializeData<unsigned int>(nbRange, eUInt, "NbRange");
		for (unsigned int i = 0; i < nbRange; i++)
		{
			movConfig->SerializeData((ParameterObject*)this, eParameterObject, "BeamIdRange");

			unsigned int firstBeam = iterTransducer->second[i].first;
			unsigned int lastBeam = iterTransducer->second[i].second;
			movConfig->SerializeData<unsigned int>(firstBeam, eUInt, "FirstBeam");
			movConfig->SerializeData<unsigned int>(lastBeam, eUInt, "LastBeam");

			movConfig->SerializePushBack();
		}

		movConfig->SerializePushBack();
	}

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	return true;
}

bool BeamIdGroupFilter::DeSerialize(MovConfig * movConfig)
{
	bool result = true;
	// debut de la d�s�rialisation du module
	if (result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "BeamIdRanges"))
	{
		BuildDefaultSettings();

		unsigned int nbTrans = 0;
		result = result && movConfig->DeSerializeData<unsigned int>(&nbTrans, eUInt, "NbTransducer");

		for (unsigned int iTrans = 0; iTrans < nbTrans; iTrans++)
		{
			result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "TransducerBeamIdRange");

			std::string transducerName;
			result = result && movConfig->DeSerializeData<std::string>(&transducerName, eString, "Transducer");

			unsigned int nbRange;
			result = result && movConfig->DeSerializeData<unsigned int>(&nbRange, eUInt, "NbRange");

			BeamIdRangeVector beamIdRangeVector;
			for (unsigned int iRange = 0; iRange < nbRange; iRange++)
			{
				result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "BeamIdRange");

				unsigned int firstBeam;
				unsigned int lastBeam;
				movConfig->DeSerializeData<unsigned int>(&firstBeam, eUInt, "FirstBeam");
				movConfig->DeSerializeData<unsigned int>(&lastBeam, eUInt, "LastBeam");

				beamIdRangeVector.push_back(std::make_pair(firstBeam, lastBeam));

				movConfig->DeSerializePushBack();
			}
			m_mapTransducerToBeamRange[transducerName] = beamIdRangeVector;

			movConfig->DeSerializePushBack();
		}

		// fin de la d�s�rialisation du module
		movConfig->DeSerializePushBack();
	}

	return result;
}
