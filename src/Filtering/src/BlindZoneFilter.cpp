
#include "Filtering/BlindZoneFilter.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/FanMemory.h"
#include "BaseMathLib/Vector3.h"

BlindZoneFilter::BlindZoneFilter(void)
{
	SetName("Blind Zone Filter");
}

BlindZoneFilter::~BlindZoneFilter(void)
{
}
void BlindZoneFilter::FilterArea(Sounder*pSound, PingFan *refFrame, unsigned int numFan)
{
	for (unsigned int transNum = 0; transNum < pSound->GetTransducerCount(); transNum++)
	{
		Transducer *pTransducer = pSound->GetTransducer(transNum);
		MemoryStruct *pPolarMem = refFrame->GetMemorySetRef()->GetMemoryStruct(transNum);
		for (unsigned int numBeam = 0; numBeam < pPolarMem->GetDataFmt()->getSize().x; numBeam++)
		{
			SoftChannel *p = pTransducer->getSoftChannelPolarX(numBeam);
			if (p)
			{
				double beamSample = pTransducer->getBeamsSamplesSpacing();
				unsigned int GoToEcho = ceil(this->m_value / beamSample);
				for (unsigned int numPolarY = 0; numPolarY < GoToEcho; numPolarY++)
				{
					BaseMathLib::Vector3I PolarCoor;
					PolarCoor.x = numBeam;
					PolarCoor.y = numPolarY;
					PolarCoor.z = numFan;

					//short *donnee=(short*)pPolarMem->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(numBeam,numPolarY));
					char  *Filter = pPolarMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(numBeam, numPolarY));
					*Filter = 1;
					//*donnee=MIN_DB;
				}
			}
		}
	}
}

const char* BlindZoneFilter::getValueName()
{
	return "OffsetMeters";
}

///////////////////////////////////////////////////////////////////////////
BlindZoneFilterMono::BlindZoneFilterMono(void)
{
	SetName("Blind Zone Filter Mono");
	this->m_value = 3.0f;
}
void BlindZoneFilterMono::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	Sounder *pSound = refFrame->getSounderRef();
	if (!pSound->m_isMultiBeam)
	{
		this->FilterArea(pSound, refFrame, numFan);
	}
}


///////////////////////////////////////////////////////////////////////////
BlindZoneFilterMulti::BlindZoneFilterMulti(void)
{
	SetName("Blind Zone Filter Multi");
	this->m_value = 15.0f;
}
void BlindZoneFilterMulti::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	Sounder *pSound = refFrame->getSounderRef();
	if (pSound->m_isMultiBeam)
	{
		this->FilterArea(pSound, refFrame, numFan);
	}
}
