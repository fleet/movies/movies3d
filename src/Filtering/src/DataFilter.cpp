#include "M3DKernel/DefConstants.h"
//
#include <sstream>

#include "Filtering/groundViewFilterGround.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/utils/M3DStdUtils.h"

using namespace BaseMathLib;


DataFilter::DataFilter()
{
	m_Name = NULL;
	m_CompleteName = NULL;
	SetName("DEFAULT FILTER");
	m_Enable = false;
}
DataFilter::~DataFilter()
{
	if (m_Name)
	{
		delete[] m_Name;
	}
	if (m_CompleteName)
	{
		delete[] m_CompleteName;
	}

}
void DataFilter::SetName(const char*aName)
{
	if (m_Name)
	{
		delete[] m_Name;
	}
	unsigned int size = strlen(aName);
	m_Name = new char[size + 4];
	strcpy_s(m_Name, size + 4, aName);
}
void DataFilter::SetCompleteName(const char*aName)
{
	if (m_CompleteName)
	{
		delete[] m_CompleteName;
	}
	unsigned int size = strlen(aName);
	m_CompleteName = new char[size + 4];
	strcpy_s(m_CompleteName, size + 4, aName);
}
// on renvoi le nom court du filtre, en supprimant les espaces
// pour avoir un nom de balise XML valide
std::string DataFilter::GetShortName()
{
	std::string name = DataFilter::getName();
	std::string result;

	for (unsigned int i = 0; i < name.length(); i++)
	{
		if (name[i] != ' ') result += name[i];
	}

	return result;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

DataFilterBase::DataFilterBase() : DataFilter()
{
	SetName("Base Threshold Filter ");
	this->setEnable(false);
}
/*bool DataFilterBase::isRejected( Vector2I &coordPolar, DataFmt &valueDB , PingFan *refFrame, Transducer *pTransducer)
{
	return false;
}*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
char* DataFilterWithTolerance::getName()
{
	std::string ret = DataFilter::getName();
	// cr�er un flux de sortie
	std::ostringstream oss;
	// �crire un nombre dans le flux
	oss << this->m_tolerance;
	// r�cup�rer une cha�ne de caract�res
	ret += " tolerance [" + oss.str() + "]";
	SetCompleteName(ret.c_str());
	return getCompleteName();
}
const char* DataFilterWithTolerance::getToleranceName()
{
	return "Tolerance";
}

char* DataFilterToleranceThreshold::getName()
{
	std::string ret = DataFilter::getName();
	// cr�er un flux de sortie
	std::ostringstream oss;

	// �crire un nombre dans le flux
	oss << this->m_tolerance;


	std::ostringstream oss2;

	// �crire un nombre dans le flux
	oss2 << this->m_threshold;

	// r�cup�rer une cha�ne de caract�res
	ret += getThresholdName();
	ret += " [" + oss2.str() + "] +" + getToleranceName() + " [" + oss.str() + "]";
	SetCompleteName(ret.c_str());
	return getCompleteName();
}
const char* DataFilterToleranceThreshold::getToleranceName()
{
	return "Tolerance";
}
const char* DataFilterToleranceThreshold::getThresholdName() {
	return "Threshold";
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
char* DataFilterValue::getName()
{
	std::string ret = DataFilter::getName();
	// cr�er un flux de sortie
	std::ostringstream oss;
	// �crire un nombre dans le flux
	oss << this->m_value;
	// r�cup�rer une cha�ne de caract�res
	ret += " " + std::string(getValueName());
	ret += "=[" + oss.str() + "]";
	SetCompleteName(ret.c_str());
	return getCompleteName();
}

const char* DataFilterValue::getValueName() { return "Value"; }

