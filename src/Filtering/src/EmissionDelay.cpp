
#include "Filtering/EmissionDelay.h"

#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;
using namespace std;

EmissionDelay::EmissionDelay(const char *Name) : BaseKernel::ParameterObject()
{
	m_TransducerName = Name;
	m_Delays.clear();
}

EmissionDelay::~EmissionDelay(void)
{
}

bool EmissionDelay::checkTransName(string Name)
{
	bool ret = !m_TransducerName.compare(Name);
	return ret;
}

// IPSIS - OTK - Ajout MovConfig
bool EmissionDelay::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "EmissionDelay");

	movConfig->SerializeData<string>(m_TransducerName, eString, "TransducerName");

	unsigned int nbGroup = (unsigned int)m_Delays.size();
	movConfig->SerializeData<unsigned int>(nbGroup, eUInt, "NbGroup");

	for (size_t i = 0; i < nbGroup; i++)
	{
		movConfig->SerializeData((ParameterObject*)this, eParameterObject, "DelayGroup");
		movConfig->SerializeData<unsigned int>(m_Delays[i].first, eUInt, "NbChan");
		movConfig->SerializeData<int>(m_Delays[i].second, eInt, "Delay");
		movConfig->SerializePushBack();
	}

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	return true;
}
bool EmissionDelay::DeSerialize(MovConfig * movConfig)
{
	bool result = true;

	// debut de la d�s�rialisation du module
	if (result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "EmissionDelay"))
	{
		result = result && movConfig->DeSerializeData<string>(&m_TransducerName, eString, "TransducerName");

		unsigned int nbGroup = 0;
		result = result && movConfig->DeSerializeData<unsigned int>(&nbGroup, eUInt, "NbGroup");

		// on supprime la d�finition existante avant d'ajouter les param�tres lus dans la config
		m_Delays.clear();

		for (unsigned int i = 0; i < nbGroup; i++)
		{
			result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "DelayGroup");
			unsigned int nbChan = 1;
			int delay = 0;
			result = result && movConfig->DeSerializeData<unsigned int>(&nbChan, eUInt, "NbChan");
			result = result && movConfig->DeSerializeData<int>(&delay, eInt, "Delay");
			m_Delays.push_back(make_pair(nbChan, delay));
			movConfig->DeSerializePushBack();
		}

		assert(m_Delays.size() == nbGroup);

		// fin de la d�s�rialisation du module
		movConfig->DeSerializePushBack();
	}

	return result;
}
