
#include "Filtering/EmissionDelaySet.h"
#include "M3DKernel/config/MovConfig.h"
#include "Filtering/EmissionDelay.h"

using namespace BaseKernel;
using namespace std;

EmissionDelaySet::EmissionDelaySet(void) : BaseKernel::ParameterObject()
{
	BuildDefaultSettings();
}

EmissionDelaySet::~EmissionDelaySet(void)
{
	for (unsigned int i = 0; i < m_RecordSet.size(); i++)
	{
		delete m_RecordSet[i];
	}
}


bool EmissionDelaySet::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "EmissionDelaySet");

	unsigned int nbTrans = (unsigned int)m_RecordSet.size();
	movConfig->SerializeData<unsigned int>(nbTrans, eUInt, "NbTransducer");

	for (size_t i = 0; i < nbTrans; i++)
	{
		m_RecordSet[i]->Serialize(movConfig);
	}

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	return true;
}
bool EmissionDelaySet::DeSerialize(MovConfig * movConfig)
{
	bool result = true;
	// debut de la d�s�rialisation du module
	if (result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "EmissionDelaySet"))
	{
		// on supprime la d�finition existante avant d'ajouter les param�tres lus dans la config
		for (unsigned int i = 0; i < m_RecordSet.size(); i++)
		{
			delete m_RecordSet[i];
		}
		m_RecordSet.clear();

		unsigned int nbTrans = 0;
		result = result && movConfig->DeSerializeData<unsigned int>(&nbTrans, eUInt, "NbTransducer");

		for (unsigned int i = 0; i < nbTrans; i++)
		{
			EmissionDelay * pDelay = new EmissionDelay("");
			result = result && pDelay->DeSerialize(movConfig);
			m_RecordSet.push_back(pDelay);
		}
		// fin de la d�s�rialisation du module
		movConfig->DeSerializePushBack();
	}

	return result;
}

EmissionDelay *EmissionDelaySet::GetEmissionDelay(const char *m_Name)
{
	for (unsigned int i = 0; i < m_RecordSet.size(); i++)
	{
		if (m_RecordSet[i]->checkTransName(m_Name))
		{
			return m_RecordSet[i];
		}
	}
	return CreateDefaultEmissionDelay(m_Name);
}


EmissionDelay* EmissionDelaySet::CreateDefaultEmissionDelay(const char *m_Name)
{
	EmissionDelay *pRet = new EmissionDelay(m_Name);
	m_RecordSet.push_back(pRet);
	return pRet;
}

unsigned int EmissionDelaySet::GetRecordCount() { return m_RecordSet.size(); }
EmissionDelay* EmissionDelaySet::GetRecord(unsigned int idx) { if (idx < GetRecordCount()) return m_RecordSet[idx]; else return NULL; }

void EmissionDelaySet::BuildDefaultSettings()
{
	EmissionDelay *p;

	//p=GetEmissionDelay("EM302");
	p = GetEmissionDelay("Generic MultiBeam Transducer");
	p->m_Delays.push_back(make_pair(42, 8));
	p->m_Delays.push_back(make_pair(45, 4));
	p->m_Delays.push_back(make_pair(57, 0));
	p->m_Delays.push_back(make_pair(57, 2));
	p->m_Delays.push_back(make_pair(45, 6));
	p->m_Delays.push_back(make_pair(42, 10));

	p = GetEmissionDelay("ME70");
	p->m_Delays.push_back(make_pair(1, 40));
	p->m_Delays.push_back(make_pair(2, 32));
	p->m_Delays.push_back(make_pair(2, 24));
	p->m_Delays.push_back(make_pair(2, 16));
	p->m_Delays.push_back(make_pair(2, 8));
	p->m_Delays.push_back(make_pair(4, 0));
	p->m_Delays.push_back(make_pair(2, 8));
	p->m_Delays.push_back(make_pair(2, 16));
	p->m_Delays.push_back(make_pair(2, 24));
	p->m_Delays.push_back(make_pair(2, 32));

}
