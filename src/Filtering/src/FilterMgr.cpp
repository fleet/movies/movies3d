
#include "BaseMathLib/Vector3.h"
#include "Filtering/FilterMgr.h"
#include "Filtering/groundViewFilterGround.h"
#include "Filtering/groundViewFilterSphere.h"
#include "Filtering/BlindZoneFilter.h"
#include "Filtering/GainCorrectionFilter.h"
#include "Filtering/UpperFilter.h"
#include "Filtering/LowerFilter.h"
#include "Filtering/SideLobeFilter.h"
#include "Filtering/InterferenceFilter.h"
#include "Filtering/BeamIdGroupFilter.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/M3DKernel.h"

using namespace BaseMathLib;
using namespace BaseKernel;

namespace
{
	constexpr const char * LoggerName = "Filtering.FilterMgr";
}

FilterMgr::FilterMgr() 
	: ProcessModule("Filter Module")
	, BaseKernel::ParameterModule("FilterMgrParameter")
{
	m_vectFilterAlgo.push_back(new FilterGround());
	m_vectFilterAlgo.push_back(new FilterGroundVariable());
	m_vectFilterAlgo.push_back(new FilterSphereOffset());
	m_vectFilterAlgo.push_back(new FilterSphereVariableOffset());
	m_BlindZoneFilterMulti = new BlindZoneFilterMulti();
	m_vectFilterAlgo.push_back(m_BlindZoneFilterMulti);
	m_BlindZoneFilterMono = new BlindZoneFilterMono();
	m_vectFilterAlgo.push_back(m_BlindZoneFilterMono);
	// OTK - FAE022 - ajout des nouveaux filtres
	m_vectFilterAlgo.push_back(new UpperFilter());
	m_vectFilterAlgo.push_back(new LowerFilter());
	m_vectFilterAlgo.push_back(new SideLobeFilter());

	m_vectFilterAlgo.push_back(new GainCorrectionFilter());
	m_vectFilterAlgo.push_back(new GainCorrectionFilter());
	m_vectFilterAlgo.push_back(new GainCorrectionFilter());

	// OTK - FAE077 - ajout du filtre d'interf�rences
	m_vectFilterAlgo.push_back(new InterferenceFilter());

	// NMD - FAE091 - Ajout filtre de groupe de faisceau par leur ID
	m_vectFilterAlgo.push_back(new BeamIdGroupFilter());

	for (unsigned int i = 0; i < getNbFilter(); i++)
	{
		getFilter(i)->setEnable(false);
	}
}

FilterMgr::~FilterMgr()
{
	while (m_vectFilterAlgo.size())
	{
		std::vector<DataFilter*>::iterator vectItr;
		vectItr = m_vectFilterAlgo.begin();
		if (*vectItr)
		{
			delete(*vectItr);
			m_vectFilterAlgo.erase(vectItr);
		}
	}
}
void FilterMgr::onEnableStateChange()
{
	for (unsigned int i = 0; i < getNbFilter(); i++)
		getFilter(i)->setEnable(getEnable());
}

unsigned int FilterMgr::getNbFilter()
{
	return m_vectFilterAlgo.size();
}

void FilterMgr::PingFanAdded(PingFan *pFan)
{
	if (!getEnable())
		return;

	M3DKernel *pKernel = M3DKernel::GetInstance();
	Sounder *p = pFan->getSounderRef();

	size_t numFan = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(p->m_SounderId)->GetObjectCount();
	if (numFan > 0)
	{
		if (pFan)
		{
			ProcessRejection(pFan, numFan - 1);
		}
	}
}

// OTK - FAE077 - ajout de param�tres par d�faut pour les transducteurs donc les d�lais d'�mission
// ne sont pas encore pr�sents dans la config
void FilterMgr::SounderChanged(std::uint32_t sounderId)
{
	InterferenceFilter* pFilter = getInterferenceFilter();
	if (pFilter)
	{
		M3DKernel *pKern = M3DKernel::GetInstance();
		CurrentSounderDefinition &lsouderMgr = pKern->getObjectMgr()->GetPingFanContainer().m_sounderDefinition;

		Sounder *pSounder = lsouderMgr.GetSounderWithId(sounderId);

		if (pSounder->m_isMultiBeam)
		{
			for (unsigned int numTrans = 0; numTrans < pSounder->GetTransducerCount(); numTrans++)
			{
				Transducer *pTransducer = pSounder->GetTransducer(numTrans);
				EmissionDelay *p = pFilter->GetEmissionDelaySet().GetEmissionDelay(pTransducer->m_transName);
			}
		}
	}
}

InterferenceFilter* FilterMgr::getInterferenceFilter()
{
	InterferenceFilter* interFilter = NULL;

	for (unsigned int i = 0; i < m_vectFilterAlgo.size(); i++)
	{
		DataFilter* pFilter = getFilter(i);

		interFilter = dynamic_cast<InterferenceFilter*>(pFilter);
		if (interFilter)
		{
			break;
		}
	}

	return interFilter;
}

DataFilter* FilterMgr::getFilter(unsigned int idx)
{
	if (idx > m_vectFilterAlgo.size() - 1)
		return NULL;
	return this->m_vectFilterAlgo[idx];
}

DataFilterValue* FilterMgr::getBlindZoneFilterMono()
{
	return m_BlindZoneFilterMono;
}

DataFilterValue* FilterMgr::getBlindZoneFilterMulti()
{
	return m_BlindZoneFilterMulti;
}

void FilterMgr::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	for (unsigned int i = 0; i < m_vectFilterAlgo.size(); i++)
	{
		DataFilter* pFilter = getFilter(i);

		if (pFilter&& pFilter->getEnable())
		{
			pFilter->ProcessRejection(refFrame, numFan);
		}
	}
}

// sauvegarde des param�tres dans le fichier de configuration associ�
bool FilterMgr::Serialize(BaseKernel::MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	// nombre de filtres � sauvegarder dans le config
	unsigned int filterNb = getNbFilter();
	movConfig->SerializeData<unsigned int>(filterNb, eUInt, "FilterNumber");

	// s�rialisation des donn�es des filtres et de leur �tat d'activation
	for (unsigned int i = 0; i < filterNb; i++)
	{
		DataFilter* p = getFilter(i);

		assert(p);

		std::string filterName = p->GetShortName();
		movConfig->SerializeData((ParameterObject*)this, eParameterObject, filterName.c_str());

		// pour tous les types de module, on s�rialise l'�tat
		movConfig->SerializeData<bool>(p->getEnable(), eBool, "Enabled");

		InterferenceFilter* pFilter = dynamic_cast<InterferenceFilter*> (p);
		if (pFilter)
		{
			// s�rialisation des parametres pour un InterferenceFilter
			movConfig->SerializeData<int>((int)pFilter->getTolerance(), eInt, pFilter->getToleranceName());
			movConfig->SerializeData<double>(pFilter->getThreshold(), eDouble, pFilter->getThresholdName());
			movConfig->SerializeData<int>(pFilter->getProportion(), eInt, "Proportion");
			movConfig->SerializeData<int>(pFilter->getHoleSize(), eInt, "HoleSize");
			movConfig->SerializeData<int>(pFilter->getWindowSize(), eInt, "WindowSize");

			// sauvegarde param�tres d�lais d'�mission
			pFilter->GetEmissionDelaySet().Serialize(movConfig);
		}
		else
		{
			BeamIdGroupFilter * pBeamIdGroupFilter = dynamic_cast<BeamIdGroupFilter*> (p);
			if (pBeamIdGroupFilter)
			{
				pBeamIdGroupFilter->Serialize(movConfig);
			}
			else
			{
				DataFilterWithTolerance* pFilter = dynamic_cast<DataFilterWithTolerance*> (p);
				if (pFilter)
				{
					// s�rialisation des parametres pour un DataFilterWithTolerance
					movConfig->SerializeData<float>(pFilter->getTolerance(), eFloat, pFilter->getToleranceName());
				}
				else
				{
					DataFilterToleranceThreshold* pFilter = dynamic_cast<DataFilterToleranceThreshold*> (p);
					if (pFilter)
					{
						// s�rialisation des parametres pour un DataFilterToleranceThreshold
						movConfig->SerializeData<float>(pFilter->getTolerance(), eFloat, pFilter->getToleranceName());
						movConfig->SerializeData<double>(pFilter->getThreshold(), eDouble, pFilter->getThresholdName());
					}
					else
					{
						DataFilterValue *pFilter = dynamic_cast<DataFilterValue*> (p);
						if (pFilter)
						{
							// s�rialisation des parametres pour un DataFilterValue
							movConfig->SerializeData<double>(pFilter->getValue(), eDouble, pFilter->getValueName());
						}
					}
				}
			}
		}

		movConfig->SerializePushBack();
	} // fin boucle sur les modules

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();

	return true;
}

// lecture des param�tres dans le fichier de configuration associ�
bool FilterMgr::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	// debut de la d�s�rialisation du module
	if (result)
	{
		// nombre de filtres � sauvegarder dans le config
		unsigned int filterNb = 0;
		result = result && movConfig->DeSerializeData<unsigned int>(&filterNb, eUInt, "FilterNumber");

		// d�s�rialisation des donn�es des filtres et de leur �tat d'activation
		for (unsigned int i = 0; i < filterNb; i++)
		{
			DataFilter* p = getFilter(i);

			assert(p);

			std::string filterName = p->GetShortName();
			if (p && (result = result && movConfig->DeSerializeData((ParameterObject*)this, eUInt, filterName.c_str())))
			{
				// pour tous les types de module, on d�s�rialise l'�tat
				bool enabled;
				result = result && movConfig->DeSerializeData<bool>(&enabled, eBool, "Enabled");

				InterferenceFilter* pFilter = dynamic_cast<InterferenceFilter*> (p);
				if (pFilter)
				{
					// d�s�rialisation des parametres pour un InterferenceFilter
					double value;
					int intvalue;
					result = result && movConfig->DeSerializeData<int>(&intvalue, eInt, pFilter->getToleranceName());
					if (result) pFilter->setTolerance((float)intvalue);
					result = result && movConfig->DeSerializeData<double>(&value, eDouble, pFilter->getThresholdName());
					if (result) pFilter->setThreshold(value);
					result = result && movConfig->DeSerializeData<int>(&intvalue, eInt, "Proportion");
					if (result) pFilter->setProportion(intvalue);
					result = result && movConfig->DeSerializeData<int>(&intvalue, eInt, "HoleSize");
					if (result) pFilter->setHoleSize(intvalue);
					result = result && movConfig->DeSerializeData<int>(&intvalue, eInt, "WindowSize");
					if (result) pFilter->setWindowSize(intvalue);

					// d�s�rialisation des param�tres de retard d'�mission
					result = result && pFilter->GetEmissionDelaySet().DeSerialize(movConfig);
				}
				else
				{
					BeamIdGroupFilter * pBeamIdGroupFilter = dynamic_cast<BeamIdGroupFilter*> (p);
					if (pBeamIdGroupFilter)
					{
						result = result && pBeamIdGroupFilter->DeSerialize(movConfig);
					}
					else
					{
						DataFilterWithTolerance* pFilter = dynamic_cast<DataFilterWithTolerance*> (p);
						if (pFilter)
						{
							// s�rialisation des parametres pour un DataFilterWithTolerance
							float tolerance;
							result = result && movConfig->DeSerializeData<float>(&tolerance, eFloat, pFilter->getToleranceName());
							pFilter->setTolerance(tolerance);
						}
						else
						{
							DataFilterToleranceThreshold* pFilter = dynamic_cast<DataFilterToleranceThreshold*> (p);
							if (pFilter)
							{
								// s�rialisation des parametres pour un DataFilterToleranceThreshold
								float tolerance;
								double threshold;
								result = result && movConfig->DeSerializeData<float>(&tolerance, eFloat, pFilter->getToleranceName());
								result = result && movConfig->DeSerializeData<double>(&threshold, eDouble, pFilter->getThresholdName());
								pFilter->setTolerance(tolerance);
								pFilter->setThreshold(threshold);
							}
							else
							{
								DataFilterValue *pFilter = dynamic_cast<DataFilterValue*> (p);
								if (pFilter)
								{
									// s�rialisation des parametres pour un DataFilterValue
									double value;
									result = result && movConfig->DeSerializeData<double>(&value, eDouble, pFilter->getValueName());
									pFilter->setValue(value);
								}
								else
								{
									assert(false);
									M3D_LOG_ERROR(LoggerName, "Chargement des param�tres d'un filtre de type inconnu.");
								}
							}
						}
					}
				}

				p->setEnable(enabled);

				movConfig->DeSerializePushBack();
			}
		} // fin boucle sur les modules
	}

	// fin de la sérialisation du module
	movConfig->DeSerializePushBack();

	return result;
}
