
#include "Filtering/GainCorrectionFilter.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/MemoryStruct.h"
#include <limits.h>
using namespace BaseMathLib;
GainCorrectionFilter::GainCorrectionFilter() : DataFilterToleranceThreshold()
{
	SetName("Gain Correction Filter");
	this->m_threshold = 0;
	this->m_tolerance = 0;
}

GainCorrectionFilter::~GainCorrectionFilter(void)
{
}

const char* GainCorrectionFilter::getToleranceName() { return "BeamId"; }
const char* GainCorrectionFilter::getThresholdName() { return "Value"; }

void GainCorrectionFilter::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	Sounder *pSound = refFrame->getSounderRef();
	if (pSound)
	{
		for (unsigned int i = 0; i < pSound->GetTransducerCount(); i++)
		{
			Transducer *pTransducer = pSound->GetTransducer(i);
			MemoryStruct *pPolarMem = refFrame->GetMemorySetRef()->GetMemoryStruct(i);
			for (unsigned int numBeam = 0; numBeam < pPolarMem->GetDataFmt()->getSize().x; numBeam++)
			{
				SoftChannel *p = pTransducer->getSoftChannelPolarX(numBeam);
				if (p)
				{
					if (p->m_softChannelId == this->m_tolerance)
					{
						for (unsigned int numPolarY = 0; numPolarY < pPolarMem->GetDataFmt()->getSize().y; numPolarY++)
						{
							Vector3I PolarCoor;
							PolarCoor.x = numBeam;
							PolarCoor.y = numPolarY;
							PolarCoor.z = numFan;

							short *donnee = (short*)pPolarMem->GetDataFmt()->GetPointerToVoxel(Vector2I(numBeam, numPolarY));
							// OTK - FAE056 - on passe par un double pour ne pas subir les d�bordements de capacit� du short
							double value = *donnee;
							value += this->m_threshold * 100;

							if (value < UNKNOWN_DB)
							{
								*donnee = UNKNOWN_DB;
							}
							else if (value > std::numeric_limits<short>::max())
							{
								*donnee = UNKNOWN_DB;
							}
							else
							{
								*donnee = (short)value;
							}
						}
					}
				}
			}
		}
	}
}
