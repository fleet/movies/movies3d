
#include "Filtering/InterferenceFilter.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/MemoryStruct.h"
#include "Filtering/EmissionDelay.h"

#include <sstream>

namespace
{
	constexpr const char * LoggerName = "Filtering.InterferenceFilter";
}

InterferenceFilter::InterferenceFilter() : DataFilterToleranceThreshold()
{
	SetName("Interference Filter");
	this->m_threshold = 10.0;
	this->m_tolerance = 3;
	this->m_Prop = 75;
	this->m_HoleSize = 5;
	this->m_WindowSize = 5;
}

InterferenceFilter::~InterferenceFilter(void)
{
}

const char* InterferenceFilter::getToleranceName() { return "Tolerance"; }
const char* InterferenceFilter::getThresholdName() { return "Threshold"; }

char* InterferenceFilter::getName()
{
	std::string ret = DataFilter::getName();
	ret += " ";
	// cr�er un flux de sortie
	std::ostringstream oss;

	// �crire un nombre dans le flux
	oss << this->m_tolerance;


	std::ostringstream oss2;

	// �crire un nombre dans le flux
	oss2 << this->m_threshold;

	// r�cup�rer une cha�ne de caract�res
	ret += getThresholdName();
	ret += " [" + oss2.str() + "] +" + getToleranceName() + " [" + oss.str() + "]";
	oss.str("");
	oss << this->m_Prop;
	ret += " + Proportion[" + oss.str() + "%]";
	SetCompleteName(ret.c_str());
	return getCompleteName();
}

void InterferenceFilter::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	Sounder *pSound = refFrame->getSounderRef();
	if (pSound && pSound->m_isMultiBeam)
	{
		std::uint32_t echoFondEval;
		std::int32_t bottomRange;
		bool found;
		unsigned int nbTrans = pSound->GetTransducerCount();
		for (unsigned int i = 0; i < nbTrans; i++)
		{
			Transducer *pTransducer = pSound->GetTransducer(i);
			MemoryStruct *pPolarMem = refFrame->GetMemorySetRef()->GetMemoryStruct(i);
			MemoryObjectDataFmt* pMem = pPolarMem->GetDataFmt();
			int nbBeams = pMem->getSize().x;
			int nbSamples = pMem->getSize().y;

			// construction du vecteur des d�calages temporel en fonction de la config
			std::vector<int> tx_dec;
			EmissionDelay* pDelay = this->m_EmissionDelaySet.GetEmissionDelay(pTransducer->m_transName);
			size_t nbGroups = pDelay->m_Delays.size();
			for (size_t i = 0; i < nbGroups; i++)
			{
				unsigned int groupSize = pDelay->m_Delays[i].first;
				int delay = pDelay->m_Delays[i].second;
				for (unsigned int j = 0; j < groupSize; j++)
				{
					tx_dec.push_back(delay);
				}
			}

			// si le nombre de d�lais est diff�rent, on compl�te par des zeros et on met un warning
			size_t paramSize = tx_dec.size();
			if (paramSize < pTransducer->m_numberOfSoftChannel)
			{
				for (unsigned int i = 0; i < pTransducer->m_numberOfSoftChannel - paramSize; i++)
				{
					tx_dec.push_back(0);
				}
				M3D_LOG_WARN(LoggerName, "InterferenceFilter emission delays missing : padding with zeros");
			}

			// d�calage maximum
			int max_tx = *std::max_element(tx_dec.begin(), tx_dec.end());
			int nbSamples2 = nbSamples - max_tx;

			// Conditions n�cessaires au filtrage
			if (nbSamples2 > 0)
			{
				// on s'arr�te � l'�cho de fond minimum entre les channels : quel est-il ?
				std::uint32_t firstBottomEcho = nbSamples - 1;
				for (int numBeam = 0; numBeam < nbBeams; numBeam++)
				{
					SoftChannel *p = pTransducer->getSoftChannelPolarX(numBeam);
					refFrame->getBottom(p->m_softChannelId, echoFondEval, bottomRange, found);
					if (found)
					{
                        firstBottomEcho = std::min(firstBottomEcho, echoFondEval);
					}
				}

				// ATTENTION : on stocke sv2f en lin�raire et sv3 en dB !
				// *********************************************************
				// cr�ation d'un objet m�moire de stockage des donn�es de travail (en double pr�cision pour pas avoir de pb d'arrondi)
				MemoryObjectDouble sv3;
				// affectation des donn�es initialis�es aux donn�es de travail corrig�es du d�calage tx_dec
				sv3.Allocate(*pMem, tx_dec);
				// cr�ation d'un objet m�moire de stockage des donn�es liss�es et retard�es
				MemoryObjectDouble sv2f;
				// affectation des donn�es initialis�es � 0 et de m�me taille que les donn�es de travail sv3
				sv2f.Allocate(nbBeams, nbSamples2, 0);

				// conversion de la tolerance en m�tres vers un nombre d'�chos
				int samples_tolerance = (int)m_tolerance; //(int)(0.5+m_tolerance/pTransducer->getBeamsSamplesSpacing());
				// idem pour la fen�tre et la taille du trou
				unsigned int holeSize = (int)m_HoleSize; //(int)(0.5+m_HoleSize/pTransducer->getBeamsSamplesSpacing());
				unsigned int windowSize = (int)m_WindowSize; //(int)(0.5+m_WindowSize/pTransducer->getBeamsSamplesSpacing());

				// flags permettant de m�moriser les d�passements sur m_tolech
				bool * det_tol = new bool[samples_tolerance*nbBeams];
				for (int i = 0; i < samples_tolerance*nbBeams; i++)
				{
					det_tol[i] = false;
				}

				// boucle sur les �chos
                int lastSample = std::min(nbSamples2, (int)firstBottomEcho - max_tx) - 1;
				double value;
				for (int numSample = (int)holeSize; numSample < lastSample; numSample++)
				{
					// lissage des donn�es m_HoleSize �chantillons avant
					for (int numBeam = 0; numBeam < nbBeams; numBeam++)
					{
						if (numSample <= (int)(holeSize + windowSize - 1))
						{
							value = sv2f.GetValueToVoxel(numBeam, numSample - 1) + pow(10.0, 0.0005*sv3.GetValueToVoxel(numBeam, numSample - holeSize)) / windowSize;
						}
						else
						{
							value = sv2f.GetValueToVoxel(numBeam, numSample - 1) + (pow(10.0, 0.0005*sv3.GetValueToVoxel(numBeam, numSample - holeSize)) - pow(10.0, 0.0005*sv3.GetValueToVoxel(numBeam, numSample - holeSize - windowSize))) / windowSize;
						}
						sv2f.SetValueToVoxel(numBeam, numSample, value);
					}

					// on d�tecte les voies pour lequels on a un d�passement
					int colindex = numSample % samples_tolerance;
					for (int i = 0; i < nbBeams; i++)
					{
						if (0.01*sv3.GetValueToVoxel(i, numSample) > (20.0*log10(sv2f.GetValueToVoxel(i, numSample)) + m_threshold))
						{
							det_tol[colindex*nbBeams + i] = true;
						}
						else
						{
							det_tol[colindex*nbBeams + i] = false;
						}
					}

					// pour chaque voie qui comporte une d�tection sur les samples_tolerance derniers �chos
					int nbdetec = 0;
					for (int numBeam = 0; numBeam < nbBeams; numBeam++)
					{
						bool found = false;
						for (int i = 0; i < samples_tolerance && !found; i++)
						{
							if (det_tol[i*nbBeams + numBeam])
							{
								nbdetec++;
								found = true;
							}
						}
					}

					// si on d�passe sur suffisamment de canaux...
					if ((double)nbdetec / nbBeams > 0.01*m_Prop)
					{
						// on remplace par la valeur liss�e sur tous les canaux
						for (int numBeam = 0; numBeam < nbBeams; numBeam++)
						{
							for (int i = 0; i < samples_tolerance + 1; i++)
							{
								// application de la nouvelle valeur dans la zone m�moire de travail
								double val = 2000.0*log10(sv2f.GetValueToVoxel(numBeam, numSample - i));
								sv3.SetValueToVoxel(numBeam, numSample - i, val);
								// application de la nouvelle valeur dans la m�morie r�elle du ping
								pMem->SetValueToVoxel(numBeam, numSample - i + max_tx - tx_dec[numBeam], (DataFmt)(val >= 0.5 ? ceil(val) : floor(val)));
							}
						}
					}
				}

				delete[] det_tol;
			}
		}
	}
}
