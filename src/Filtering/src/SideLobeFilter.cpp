
#include "Filtering/SideLobeFilter.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/FanMemory.h"
#include "BaseMathLib/Vector3.h"

SideLobeFilter::SideLobeFilter(void)
{
	SetName("Side Lobe Filter");
	setValue(5.0); // 5dB par d�faut
}

SideLobeFilter::~SideLobeFilter(void)
{
}

void SideLobeFilter::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	Sounder *pSound = refFrame->getSounderRef();
	if (pSound->m_isMultiBeam)
	{
		unsigned int nbTrans = pSound->GetTransducerCount();
		for (unsigned int transNum = 0; transNum < nbTrans; transNum++)
		{
			Transducer *pTransducer = pSound->GetTransducer(transNum);
			MemoryStruct *pPolarMem = refFrame->GetMemorySetRef()->GetMemoryStruct(transNum);
			BaseMathLib::Vector2I size = pPolarMem->GetDataFmt()->getSize();
			int beamNb = size.x;
			int echoNb = size.y;
			std::vector<DataFmt> echoMaxValue(echoNb, UNKNOWN_DB);

			// on commence par parcourir tous les �chos en notant le max pour un range donn�
			for (int numBeam = 0; numBeam < beamNb; numBeam++)
			{
				SoftChannel *p = pTransducer->getSoftChannelPolarX(numBeam);
				if (p)
				{
					DataFmt * pStart = pPolarMem->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(numBeam, 0));

					for (int iEcho = 0; iEcho < echoNb; iEcho++)
					{
                        echoMaxValue[iEcho] = std::max(echoMaxValue[iEcho], *(pStart + iEcho));
					}
				}
			}

			//on reboucle sur tous les channels en filtrant les donn�es inf�rieures � max-X dB
			for (int numBeam = 0; numBeam < beamNb; numBeam++)
			{
				SoftChannel *p = pTransducer->getSoftChannelPolarX(numBeam);
				if (p)
				{
					DataFmt * pStart = pPolarMem->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(numBeam, 0));
					char * pFilterStart = pPolarMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(numBeam, 0));

					for (int iEcho = 0; iEcho < echoNb; iEcho++)
					{
						if (*(pStart + iEcho) < (echoMaxValue[iEcho] - getValue()*100.))
						{
							*(pFilterStart + iEcho) = 1;
						}
					}
				}
			}
		}
	}
}

const char* SideLobeFilter::getValueName()
{
	return "Threshold";
}
