#include "Filtering/UpperFilter.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/FanMemory.h"
#include "BaseMathLib/Vector3.h"

UpperFilter::UpperFilter(void)
{
	SetName("Upper Filter");
}

UpperFilter::~UpperFilter(void)
{
}

void UpperFilter::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	Sounder *pSound = refFrame->getSounderRef();
	unsigned int nbTrans = pSound->GetTransducerCount();
	for (unsigned int transNum = 0; transNum < nbTrans; transNum++)
	{
		Transducer *pTransducer = pSound->GetTransducer(transNum);
		MemoryStruct *pPolarMem = refFrame->GetMemorySetRef()->GetMemoryStruct(transNum);
		int beamNb = pPolarMem->GetDataFmt()->getSize().x;
		for (int numBeam = 0; numBeam < beamNb; numBeam++)
		{
			SoftChannel *p = pTransducer->getSoftChannelPolarX(numBeam);
			if (p)
			{
				char * pStart = pPolarMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(numBeam, 0));
				BaseMathLib::Vector2I size = pPolarMem->GetDataFmt()->getSize();
				unsigned int echoNb = (unsigned int)size.y;
				bool filtered = false;
				bool finished = false;

				for (unsigned int iEcho = 0; iEcho < echoNb && !finished; iEcho++)
				{
					// Position dans le rep�re monde de l'Echo
					BaseMathLib::Vector3D vectEcho = pSound->GetPolarToGeoCoord(refFrame, transNum, numBeam, iEcho);
					if (vectEcho.z < getValue())
					{
						*(pStart + iEcho) = 1;
						filtered = true;
					}
					else if (filtered)
					{
						// si on a filtr� et qu'on est sorti de la zone filtr�e, on peut sortir
						finished = true;
					}
				}
			}
		}
	}
}

const char* UpperFilter::getValueName()
{
	return "Depth";
}
