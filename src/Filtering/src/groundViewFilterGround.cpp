#include "BaseMathLib/Vector3.h"
#include "M3DKernel/DefConstants.h"

#include "Filtering/groundViewFilterGround.h"

#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/FanMemory.h"
#include "M3DKernel/datascheme/TransformMap.h"
using namespace BaseMathLib;

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

FilterGround::FilterGround() : DataFilterWithTolerance()
{
	SetName("Floor Filter Simple");
	m_tolerance = 1.0f;
}
void FilterGround::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	std::uint32_t echoFondEval;
	std::int32_t bottomRange;
	bool found;
	Sounder *pSound = refFrame->getSounderRef();
	if (pSound)
	{
		for (unsigned int i = 0; i < pSound->GetTransducerCount(); i++)
		{
			Transducer *pTransducer = pSound->GetTransducer(i);
			MemoryStruct *pPolarMem = refFrame->GetMemorySetRef()->GetMemoryStruct(i);
			for (unsigned int numBeam = 0; numBeam < pPolarMem->GetDataFmt()->getSize().x; numBeam++)
			{
				SoftChannel *p = pTransducer->getSoftChannelPolarX(numBeam);
				if (p)
				{
					refFrame->getBottom(p->m_softChannelId, echoFondEval, bottomRange, found);
					if (found)
					{

						double angle = fabs(p->m_mainBeamAthwartSteeringAngleRad);
						float ouverture = p->m_beam3dBWidthAthwartRad;

						std::uint32_t echoTolerance = (std::uint32_t)(m_tolerance / (pTransducer->getBeamsSamplesSpacing())) / cos(p->m_mainBeamAthwartSteeringAngleRad);

						for (unsigned int numPolarY = 0; numPolarY < pPolarMem->GetDataFmt()->getSize().y; numPolarY++)
						{
							Vector3I PolarCoor;
							PolarCoor.x = numBeam;
							PolarCoor.y = numPolarY;
							PolarCoor.z = numFan;

							//short *donnee=(short*)pPolarMem->GetDataFmt()->GetPointerToVoxel(Vector2I(numBeam,numPolarY));
							//char *Filter=pPolarMem->GetFilterFlag()->GetPointerToVoxel(Vector2I(numBeam,numPolarY));
							//if(/**donnee==MIN_DB || */*Filter>0)
							//	continue;

							if (PolarCoor.y > (echoFondEval - echoTolerance))
							{

								while (numPolarY < pPolarMem->GetDataFmt()->getSize().y)
								{
									//*donnee++=MIN_DB;
									char *Filter = pPolarMem->GetFilterFlag()->GetPointerToVoxel(Vector2I(numBeam, numPolarY));
									*Filter = 1;
									numPolarY++;
								}


							}
						}
					}
				}
			}
		}
	}

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

FilterGroundVariable::FilterGroundVariable() : FilterGround()
{
	SetName("Floor Filter Variable");
	m_tolerance = 1.0f;
}
void FilterGroundVariable::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	std::uint32_t echoFondEval;
	std::int32_t bottomRange;
	bool found;
	Sounder *pSound = refFrame->getSounderRef();
	if (pSound)
	{
		for (unsigned int i = 0; i < pSound->GetTransducerCount(); i++)
		{
			Transducer *pTransducer = pSound->GetTransducer(i);
			MemoryStruct *pPolarMem = refFrame->GetMemorySetRef()->GetMemoryStruct(i);
			for (unsigned int numBeam = 0; numBeam < pPolarMem->GetDataFmt()->getSize().x; numBeam++)
			{
				SoftChannel *p = pTransducer->getSoftChannelPolarX(numBeam);
				if (p)
				{
					refFrame->getBottom(p->m_softChannelId, echoFondEval, bottomRange, found);
					if (found)
					{

						double angle = p->m_mainBeamAthwartSteeringAngleRad;

						// on estime la demi-ouverture sous laquelle est d�tect�e le fond avec la formule approxim�e permettant d'inverser la fonction de bessel du lobe en fonction d'un seuil � -6OdB				
						unsigned int nbEchoAutour = (pTransducer->m_pulseDuration / pTransducer->m_timeSampleInterval) / 2;
						double energy = 0;
						int realNbIntegratedEchos = 0;
						for (unsigned int numPolarY = 0; numPolarY < 2 * nbEchoAutour + 1; numPolarY++)
						{
							Vector3I PolarCoor;
							PolarCoor.x = numBeam;
							PolarCoor.y = numPolarY;
							PolarCoor.z = numFan;

							Vector2I echoPos(numBeam, echoFondEval - nbEchoAutour + numPolarY);

							// OTK - 13/04/2010 - on ne doit pas d�passer le dernier �cho de la zone m�moire (ni le premier, FAE073)
							if (echoPos.y >= 0 && echoPos.y < pPolarMem->GetDataFmt()->getSize().y)
							{
								energy += pow(10.0, ((double)*refFrame->GetMemorySetRef()->GetMemoryStruct(i)->GetDataFmt()->GetPointerToVoxel(echoPos)) / 1000.0);
								realNbIntegratedEchos++;
							}
						}

						energy = energy / realNbIntegratedEchos;
						double sv = 10 * log10(energy);

						float demi_ouverture = p->m_beam3dBWidthAthwartRad / 2;

						if (sv > -60)
							demi_ouverture = 0.44*demi_ouverture*pow((sv + 60), 0.45);


						//dans le cas multi on estime la pente du fond gr�ace aux faisceaux voisins
						double pente_locale = 0;

						if (pSound->m_isMultiBeam)
						{
							SoftChannel *p_prec;
							if (numBeam > 0)
								p_prec = pTransducer->getSoftChannelPolarX(numBeam - 1);
							else
								p_prec = p;

							SoftChannel *p_suiv;
							if (numBeam < pPolarMem->GetDataFmt()->getSize().x - 1)
								p_suiv = pTransducer->getSoftChannelPolarX(numBeam + 1);
							else
								p_suiv = p;

							std::int32_t bottomRange_prec = 0;
							std::uint32_t echoFondEval_prec = 0;
							bool found_prec = false;
							refFrame->getBottom(p_prec->m_softChannelId, echoFondEval_prec, bottomRange_prec, found_prec);
							std::int32_t bottomRange_suiv = 0;
							std::uint32_t echoFondEval_suiv = 0;
							bool found_suiv = false;
							refFrame->getBottom(p_suiv->m_softChannelId, echoFondEval_suiv, bottomRange_suiv, found_suiv);

							if (found_prec && found_suiv)
								pente_locale = atan((cos(p_suiv->m_mainBeamAthwartSteeringAngleRad)*echoFondEval_suiv - cos(p_prec->m_mainBeamAthwartSteeringAngleRad)*echoFondEval_prec) / (sin(p_suiv->m_mainBeamAthwartSteeringAngleRad)*echoFondEval_suiv - sin(p_prec->m_mainBeamAthwartSteeringAngleRad)*echoFondEval_prec));
							else if (found_prec)
								pente_locale = atan((cos(p->m_mainBeamAthwartSteeringAngleRad)*echoFondEval - cos(p_prec->m_mainBeamAthwartSteeringAngleRad)*echoFondEval_prec) / (sin(p->m_mainBeamAthwartSteeringAngleRad)*echoFondEval - sin(p_prec->m_mainBeamAthwartSteeringAngleRad)*echoFondEval_prec));
							else if (found_suiv)
								pente_locale = atan((cos(p_suiv->m_mainBeamAthwartSteeringAngleRad)*echoFondEval_suiv - cos(p->m_mainBeamAthwartSteeringAngleRad)*echoFondEval) / (sin(p_suiv->m_mainBeamAthwartSteeringAngleRad)*echoFondEval_suiv - sin(p->m_mainBeamAthwartSteeringAngleRad)*echoFondEval));
						}

						std::uint32_t echoTolerance = (std::uint32_t)(m_tolerance / (pTransducer->getBeamsSamplesSpacing()) / cos(p->m_mainBeamAthwartSteeringAngleRad));

						if (abs(angle) > demi_ouverture)
							echoTolerance += (echoFondEval*(sin(abs(angle)) - sin(abs(angle) - demi_ouverture))) / cos(abs(pente_locale));
						else
							echoTolerance += 0;


						for (unsigned int numPolarY = 0; numPolarY < pPolarMem->GetDataFmt()->getSize().y; numPolarY++)
						{
							Vector3I PolarCoor;
							PolarCoor.x = numBeam;
							PolarCoor.y = numPolarY;
							PolarCoor.z = numFan;

							//short *donnee=(short*)pPolarMem->GetDataFmt()->GetPointerToVoxel(Vector2I(numBeam,numPolarY));
							//char *Filter=pPolarMem->GetFilterFlag()->GetPointerToVoxel(Vector2I(numBeam,numPolarY));
							//if(/**donnee==MIN_DB*/ *Filter>0)
							//	continue;
							if (PolarCoor.y > (echoFondEval - echoTolerance))
							{
								while (numPolarY < pPolarMem->GetDataFmt()->getSize().y)
								{
									//*donnee++=MIN_DB;
									char *Filter = pPolarMem->GetFilterFlag()->GetPointerToVoxel(Vector2I(numBeam, numPolarY));
									*Filter = 1;
									numPolarY++;
								}

							}
						}
					}
				}
			}
		}
	}

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

