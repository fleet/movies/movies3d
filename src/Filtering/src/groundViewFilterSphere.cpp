

#include "BaseMathLib/Vector3.h"
#include "M3DKernel/DefConstants.h"



#include "Filtering/groundViewFilterSphere.h"



#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/FanMemory.h"
#include "M3DKernel/datascheme/TransformMap.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

using namespace BaseMathLib;

FilterSphereOffset::FilterSphereOffset() : DataFilterToleranceThreshold()
{
	SetName("Filter Sphere threshold under sphere");
	this->m_tolerance = 1.0f;
	this->m_threshold = -60;
}

// on renvoi le nom court du filtre, en supprimant les espaces
// pour avoir un nom de balise XML valide
std::string FilterSphereOffset::GetShortName()
{
	std::string result = "FilterSphereOffset";
	return result;
}

void FilterSphereOffset::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	Sounder *pSound = refFrame->getSounderRef();
	if (pSound)
	{
		for (unsigned int i = 0; i < pSound->GetTransducerCount(); i++)
		{
			Transducer *pTransducer = pSound->GetTransducer(i);
			MemoryStruct *pPolarMem = refFrame->GetMemorySetRef()->GetMemoryStruct(i);

			// NME - FAE 087 : on cherche le + petit faisceau par rapport au fond
			bool minEchoFound = false;
			std::uint32_t minEchoDemiSphere;
			unsigned int minBeam;
			for (unsigned int beam = 0; beam < pTransducer->m_numberOfSoftChannel; beam++)
			{
				std::uint32_t echoDemiSphere;
				std::int32_t bottomRange;
				bool found;
				refFrame->getBottom(pTransducer->getSoftChannelPolarX(beam)->getSoftwareChannelId(), echoDemiSphere, bottomRange, found);
				if (found)
				{
					if (minEchoFound)
					{
						if (minEchoDemiSphere > echoDemiSphere)
						{
							minEchoDemiSphere = echoDemiSphere;
							minBeam = beam;
						}
					}
					else
					{
						minEchoDemiSphere = echoDemiSphere;
						minBeam = beam;
						minEchoFound = true;
					}
				}
			}

			if (minEchoFound)
			{
				std::uint32_t echoTolerance = m_tolerance / (pTransducer->getBeamsSamplesSpacing());
				for (unsigned int numBeam = 0; numBeam < pPolarMem->GetDataFmt()->getSize().x; numBeam++)
				{
					SoftChannel *p = pTransducer->getSoftChannelVertical();
					if (p)
					{
						float lThres = m_threshold*(100.0);
						for (unsigned int numPolarY = 0; numPolarY < pPolarMem->GetDataFmt()->getSize().y; numPolarY++)
						{
							Vector3I PolarCoor;
							PolarCoor.x = numBeam;
							PolarCoor.y = numPolarY;
							PolarCoor.z = numFan;

							Vector2I coor(PolarCoor.x, PolarCoor.y);
							short *donnee = (short*)pPolarMem->GetDataFmt()->GetPointerToVoxel(Vector2I(numBeam, numPolarY));
							char  *Filter = pPolarMem->GetFilterFlag()->GetPointerToVoxel(Vector2I(numBeam, numPolarY));

							if (/**donnee==MIN_DB */*Filter > 0)
								continue;

							if (coor.y > (minEchoDemiSphere - echoTolerance) && *donnee < lThres)
							{	//*donnee=MIN_DB;
								*Filter = 1;
							}
						}
					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

FilterSphereVariableOffset::FilterSphereVariableOffset() : DataFilterToleranceThreshold()
{
	SetName("Filter the Sphere : +/- tolerance around the sphere");
	this->m_tolerance = 3.0;
	this->m_threshold = -50;
}

// on renvoi le nom court du filtre, en supprimant les espaces
// pour avoir un nom de balise XML valide
std::string FilterSphereVariableOffset::GetShortName()
{
	std::string result = "FilterSphereVariableOffset";
	return result;
}

void FilterSphereVariableOffset::ProcessRejection(PingFan *refFrame, unsigned int numFan)
{
	Sounder *pSound = refFrame->getSounderRef();
	if (pSound)
	{
		for (unsigned int i = 0; i < pSound->GetTransducerCount(); i++)
		{
			Transducer *pTransducer = pSound->GetTransducer(i);
			MemoryStruct *pPolarMem = refFrame->GetMemorySetRef()->GetMemoryStruct(i);
			float ctdeux = pTransducer->getBeamsSamplesSpacing();///vitesseson*interval/20000000.0;
			//on trouve le depointage max
			double DepMax = 0;

			// NME - FAE 087 : on cherche le + petit faisceau par rapport au fond
			bool minEchoFound = false;
			std::uint32_t minEchoDemiSphere;
			unsigned int minBeam;
			for (unsigned int beam = 0; beam < pTransducer->m_numberOfSoftChannel; beam++)
			{
                DepMax = std::max(DepMax, fabs(pTransducer->getSoftChannelPolarX(beam)->m_mainBeamAthwartSteeringAngleRad));

				std::uint32_t echoDemiSphere;
				std::int32_t bottomRange;
				bool found;
				refFrame->getBottom(pTransducer->getSoftChannelPolarX(beam)->getSoftwareChannelId(), echoDemiSphere, bottomRange, found);
				if (found)
				{
					if (minEchoFound)
					{
						if (minEchoDemiSphere > echoDemiSphere)
						{
							minEchoDemiSphere = echoDemiSphere;
							minBeam = beam;
						}
					}
					else
					{
						minEchoDemiSphere = echoDemiSphere;
						minBeam = beam;
						minEchoFound = true;
					}
				}
			}

			if (minEchoFound)
			{
				for (unsigned int numBeam = 0; numBeam < pPolarMem->GetDataFmt()->getSize().x; numBeam++)
				{
					SoftChannel *p = pTransducer->getSoftChannelVertical();
					SoftChannel *pCurrentTrans = pTransducer->getSoftChannelPolarX(numBeam);

					if (p)
					{
						std::uint32_t echoTolerance = (m_tolerance) / (pTransducer->getBeamsSamplesSpacing());
						float lThres = m_threshold*(100.0);

						//***************************************************
						//on filtre � -53dB dans la demi sph�re
						double sphere = lThres;
						for (unsigned int numPolarY = 0; numPolarY < pPolarMem->GetDataFmt()->getSize().y; numPolarY++)
						{
							Vector3I PolarCoor;
							PolarCoor.x = numBeam;
							PolarCoor.y = numPolarY;
							PolarCoor.z = numFan;

							Vector2I coor(PolarCoor.x, PolarCoor.y);
							short *donnee = (short*)pPolarMem->GetDataFmt()->GetPointerToVoxel(Vector2I(numBeam, numPolarY));
							char  *Filter = pPolarMem->GetFilterFlag()->GetPointerToVoxel(Vector2I(numBeam, numPolarY));

							if (/**donnee==MIN_DB */*Filter > 0)
								continue;



							if ((PolarCoor.y > minEchoDemiSphere - echoTolerance)
								&& (PolarCoor.y < minEchoDemiSphere + echoTolerance)
								&& *donnee < sphere)
							{
								//*donnee=MIN_DB;
								*Filter = 1;
							}
						}
					}
				}
			}
		}
	}

}
