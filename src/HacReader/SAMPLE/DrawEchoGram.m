function DrawEchoGram( ori, minValue, maxValue)

dB= createDbMap( minValue, maxValue);
rgB= createColorMap();

yi = interp1(dB(:,2),dB(:,1),ori, 'nearest');

colormap(rgB);
image(yi);

end

function db = createDbMap(minValue, maxValue)
% palette movies
inc=(minValue-maxValue)/16;
db=[
    [1 -200];
    [2 -2500];
    [3 -2750];
    [4 -3000];
    [5 -3250];
    [6 -3500];
    [7 -3750];
    [8 -4000];
    [9 -4250];
    [10 -4500];
    [11 -4750];
    [12 -5000];
    [13 -5250];
    [14 -5500];
    [15 -5750];
    [16 -6000];
    [17 -6250];
    [18 -18000];
    ];
db= [
    [1 minValue-inc];
    [2 minValue-2*inc];
    [3 minValue-3*inc];
    [4 minValue-4*inc];
    [5 minValue-5*inc];
    [6 minValue-6*inc];
    [7 minValue-7*inc];
    [8 minValue-8*inc];
    [9 minValue-9*inc];
    [10 minValue-10*inc];
    [11 minValue-11*inc];
    [12 minValue-12*inc];
    [13 minValue-13*inc];
    [14 minValue-14*inc];
    [15 minValue-15*inc];
    [16 minValue-16*inc];
    [17 maxValue-1];
    [18 -18000];
    ];

end
function rgb = createColorMap()
% palette movies
rgb= [
    [0 0 0 ];     % 0x00000000, // noir
    [128 0 0];	  % 0x00800000, // rouge fonce
    [172 0 0];	  % 0x00ac0000,
    [208 0 0];	  % 0x00d00000, // rouge
    [240 0 0];    % 0x00f00000, // orange
    [255 0 0];	  %  0x00ff0000, // jaune fonce
    [255 121 121]; % 0x00ff7979, // jaune
    [255 128 64];  % 0x00ff8040, // jaune clair
    [255 128 0];	  %  0x00ff8000, // vert clair
    [255 255 0];	  % 0x00ffff00, // vert
    [255 255 128]; % 0x00ffff80, // vert
    [0 255 0];	  % 0x0000ff00, // vert
    [125 255 128]; % 0x0080ff80,
    [0 128 192];	  % 0x000080c0, // bleu
    [0 128 255];	  % 0x000080ff, // bleu clair
    [128 255 255]; % 0x0080ffff,
    [192 192 192]; % 0x00C0C0C0, // gris clair
    [255 255 255]; %  0x00FFFFFF  /
    ];
rgb = rgb / 255.;
end
