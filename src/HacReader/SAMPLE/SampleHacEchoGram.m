function SampleHacRead()

% SampleHacRead is a simple sample reading a hac data and displaying it's
% echograms on an image window,
%
%
%
% Use this simple implementation to create your own .m processes
%

clear all;

%% we read the HAC file (see moReadWholeFile.m)
moReadWholeFile();



%% we draw the echograms
for numPingFan=0:moGetNumberOfPingFan()-1
    PingFan=moGetPingFan(numPingFan);
    PingId=PingFan.m_pingId;
    fanSounder = moGetFanSounder(PingId);
    % now we will loop on all trans
    figure(fanSounder.m_SounderId);
    i=0;
    indexSubPlot=1;
    % for each transducer get the ping Fan matrix
    for trans= fanSounder.m_transducer(1,:)
        PolarMatrix=moGetPolarMatrix(PingId,i);
        % draw the Polar matrix
        subplot(2,size(fanSounder.m_transducer,2),indexSubPlot)
        DrawEchoGram(PolarMatrix.m_Amplitude/100,0,-70);
        % draw the Screen matrix
        ScreenMatrix=moGetScreenMatrix(PingId,i);
        subplot(2,size(fanSounder.m_transducer,2),indexSubPlot+size(fanSounder.m_transducer,2))
        DrawEchoGram(ScreenMatrix/100,0,-70);
        i=i+1;
        indexSubPlot=indexSubPlot+1;
    end
    drawnow;
end



