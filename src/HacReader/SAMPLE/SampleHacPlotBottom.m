function SampleHacPlotBottom(pathHAC)

% SampleHacPlotBottom is a simple sample reading a hac data and displaying it's
% bottom on an image window,
%
%
%
% Use this simple implementation to create your own .m processes
%



%% we read the HAC file (see moReadWholeFile.m)
moReadWholeFile(pathHAC);



%% we read the bottom range for each ping and each transducer
LatitudeER60 = [];
LongitudeER60 = [];
DepthER60 = [];
LatitudeME70 = [];
LongitudeME70 = [];
DepthME70 = [];
pingCountER60 = 0;
pingCountME70 = 0;

for index= 0:moGetNumberOfPingFan()-1

    MX= moGetPingFan(index);
    SounderDesc =  moGetFanSounder(MX.m_pingId);
    if(SounderDesc.m_numberOfTransducer == 1)
        % ME70 case
        pingCountME70 = pingCountME70+1;
        TME70(:,pingCountME70)=[MX.m_meanTimeCPU,MX.m_meanTimeFraction];
    else
        % ER60 case
        pingCountER60 = pingCountER60+1;
        TER60(:,pingCountER60)=[MX.m_meanTimeCPU,MX.m_meanTimeFraction];
    end;

    for transducerIndex=1:SounderDesc.m_numberOfTransducer
        numberOfSoftChannel = SounderDesc.m_transducer(1,transducerIndex).m_numberOfSoftChannel;
        for beamIndex=1:numberOfSoftChannel
            if(numberOfSoftChannel > 1)
                % multibeam case
                BeamData=MX.beam_data(1,beamIndex);
                if(BeamData.m_bottomWasFound==1)
                    EchoEval=   floor( BeamData.m_bottomRange/SounderDesc.m_transducer.m_beamsSamplesSpacing);
                    Coord= moGetPolarToGeoCoord(MX.m_pingId,0,beamIndex-1,EchoEval);
                    LatitudeME70(beamIndex,pingCountME70)=Coord(1);
                    LongitudeME70(beamIndex,pingCountME70)=Coord(2);
                    DepthME70(beamIndex,pingCountME70)=Coord(3);
                else
                    LatitudeME70(beamIndex,pingCountME70)=MX.navigationPosition.m_lattitudeDeg;
                    LongitudeME70(beamIndex,pingCountME70)=MX.navigationPosition.m_longitudeDeg;
                    DepthME70(beamIndex,pingCountME70)=0;
                end;
            else
                % monobeam case
                BeamData=MX.beam_data(1,transducerIndex);
                if(BeamData.m_bottomWasFound==1)
                    EchoEval=   floor( BeamData.m_bottomRange/SounderDesc.m_transducer(transducerIndex).m_beamsSamplesSpacing);
                    Coord= moGetPolarToGeoCoord(MX.m_pingId,transducerIndex-1,beamIndex-1,EchoEval+2);
                    LatitudeER60(transducerIndex,pingCountER60)=Coord(1);
                    LongitudeER60(transducerIndex,pingCountER60)=Coord(2);
                    DepthER60(transducerIndex,pingCountER60)=Coord(3);
                else
                    LatitudeER60(transducerIndex,pingCountER60)=MX.navigationPosition.m_lattitudeDeg;
                    LongitudeER60(transducerIndex,pingCountER60)=MX.navigationPosition.m_longitudeDeg;
                    DepthER60(transducerIndex,pingCountER60)=0;
                end;
            end;
        end;
    end;
end;

%% we plot the bottom for each sounder

% ME70
if pingCountME70>0
    MaxX=max( reshape(LongitudeME70(:,:),1,[]));
    MinX=min( reshape(LongitudeME70(:,:),1,[]));

    MaxY=max(reshape(LatitudeME70(:,:),1,[]));
    MinY=min(reshape(LatitudeME70(:,:),1,[]));

    nbPoints = 1000;
    Precision = max((MaxX-MinX)/nbPoints,(MaxY-MinY)/nbPoints);
    XI=MinX:Precision:MaxX;
    YI=MinY:Precision:MaxY;
    figure;
    ZI = griddata( reshape(LongitudeME70(:,:),1,[]),reshape(LatitudeME70(:,:),1,[]),-reshape(DepthME70(:,:),1,[]),XI,YI');
    hold on;
    surf(XI,YI,ZI,'LineStyle','none');
    view(0,70);
    light
    lighting gouraud

      FileNameAscii = [pathHAC(1:size(pathHAC,2)-4),'_multi.csv'];
    fid = fopen(FileNameAscii,'w');

    nbpings = size(DepthME70,2);
    nbfaisceauME70 = size(DepthME70,01);
    fprintf(fid,'> %d\n', nbfaisceauME70);
    for j=1:nbpings
        for i=1:nbfaisceauME70
            if (DepthME70(i,j)>0)
                fprintf(fid,'%3.6f ',LatitudeME70(j));
                fprintf(fid,'%3.6f ',LongitudeME70(j));
                fprintf(fid,'%3.6f ',DepthME70(i,j));
                fprintf(fid,'%s\n',datestr((TME70(1,j)+TME70(2,j)/10000)/86400 +719529,'dd/mm/yyyy HH:MM:SS.FFF'));
            end;
        end;
    end;
end;

% ER60
if pingCountER60>0
    MaxX=max( reshape(LongitudeER60(:,:),1,[]));
    MinX=min( reshape(LongitudeER60(:,:),1,[]));

    MaxY=max(reshape(LatitudeER60(:,:),1,[]));
    MinY=min(reshape(LatitudeER60(:,:),1,[]));

    nbPoints = 1000;
    Precision = max((MaxX-MinX)/nbPoints,(MaxY-MinY)/nbPoints);
    XI=MinX:Precision:MaxX;
    YI=MinY:Precision:MaxY;
    figure;
    ZI = griddata( reshape(LongitudeER60(:,:),1,[]),reshape(LatitudeER60(:,:),1,[]),-reshape(DepthER60(:,:),1,[]),XI,YI');
    hold on;
    surf(XI,YI,ZI,'LineStyle','none');
    view(0,70);
    light
    lighting gouraud

    FileNameAscii = [pathHAC(1:size(pathHAC,2)-4),'_mono.csv'];
    fid = fopen(FileNameAscii,'w');

    nbpings = size(DepthER60,2);
    nbfaisceauER60 = size(DepthER60,1);
    fprintf(fid,'> %d\n', nbfaisceauER60);
    for j=1:nbpings
        for i=1:nbfaisceauER60
            if (DepthER60(i,j)>0)
                fprintf(fid,'%3.6f ',LatitudeER60(j));
                fprintf(fid,'%3.6f ',LongitudeER60(j));
                fprintf(fid,'%3.6f ',DepthER60(i,j));
                fprintf(fid,'%s\n',datestr((TER60(1,j)+TER60(2,j)/10000)/86400 +719529,'dd/mm/yyyy HH:MM:SS.FFF'));
            end;
        end;
    end;
end;