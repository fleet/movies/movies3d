function SampleHacPlotNavigation(path)

% SampleHacPlotNavigation is a simple sample reading a hac data and
% displaying it's navigation data on an image window,
%
%
%
% Use this simple implementation to create your own .m processes
%

%% we read the HAC file (see moReadWholeFile.m)
moReadWholeFile(path);



%% we read the navigation data for each ping and each transducer
PingNumberSounder1=0;
PingNumberSounder2=0;
DataSounder1 = [];
TimeSounder1 = [];
DataSounder2 = [];
TimeSounder2 = [];

for index= 0:moGetNumberOfPingFan()-1
    pingData = moGetPingFan(index);
    SounderDesc =  moGetFanSounder(pingData.m_pingId);
    if SounderDesc.m_SounderId == 1
        
        DataSounder1(:,PingNumberSounder1+1)=[
            pingData.navigationPosition.m_lattitudeDeg,
            pingData.navigationPosition.m_longitudeDeg
            pingData.navigationAttitude.pitchRad ,
            pingData.navigationAttitude.sensorHeaveMeter,
            pingData.navigationAttitude.rollRad ,
            pingData.navigationAttitude.yawRad ,
            pingData.navigationAttribute.m_headingRad,
            pingData.navigationAttribute.m_speedMeter,
            pingData.m_cumulatedDistance];
        TimeSounder1(:,PingNumberSounder1+1)=[pingData.navigationAttribute.m_meanTimeCPU,pingData.navigationAttribute.m_meanTimeFraction];
        PingNumberSounder1=PingNumberSounder1+1;
        
    elseif SounderDesc.m_SounderId == 2
        
        DataSounder2(:,PingNumberSounder2+1)=[
            pingData.navigationPosition.m_lattitudeDeg,
            pingData.navigationPosition.m_longitudeDeg
            pingData.navigationAttitude.pitchRad ,
            pingData.navigationAttitude.sensorHeaveMeter,
            pingData.navigationAttitude.rollRad ,
            pingData.navigationAttitude.yawRad ,
            pingData.navigationAttribute.m_headingRad,
            pingData.navigationAttribute.m_speedMeter,
            pingData.m_cumulatedDistance];
        TimeSounder2(:,PingNumberSounder2+1)=[pingData.navigationAttribute.m_meanTimeCPU,pingData.navigationAttribute.m_meanTimeFraction];
        PingNumberSounder2=PingNumberSounder2+1;
        
    end;
end;

%% we plot the data

% Position
figure;
subplot(2,1,1);
plot(DataSounder2(1,:), 'b');
title('ME70 Latitude');
xlabel('Ping Number'); ylabel('Latitude (�)')
subplot(2,1,2);
plot(DataSounder2(2,:), 'b');
title('ME70 Longitude');
xlabel('Ping Number'); ylabel('Longitude (�)')

% Pitch
figure;
subplot(2,2,1);
plot(DataSounder2(3,:), 'b');
title('Pitch');
xlabel('Ping Number'); ylabel('Pitch (rad)')
% Heave
subplot(2,2,2);
plot(DataSounder2(4,:), 'b');
title('Heave');
xlabel('Ping Number'); ylabel('Heave (m)')
% Roll
subplot(2,2,3);
plot(DataSounder2(5,:), 'b');
title('Roll');
xlabel('Ping Number'); ylabel('Roll (rad)')
% Heading
subplot(2,2,4);
plot(DataSounder2(7,:)*180/pi, 'b');
title('Heading');
xlabel('Ping Number'); ylabel('Heading (�)')
