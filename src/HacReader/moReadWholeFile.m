% moReadWholeFile reads a whole HAC File
%
% Ifremer Cponcelet 02/01/2008
% $Revision: 0.1 $

function moReadWholeFile(FileName)

if nargin==0 || isequal(FileName,'') ||  exist(FileName,'file')~=2
    PathName='';
    Name='';
	[Name,PathName,FilterIndex]=uigetfile('*.hac','Load HAC File');
    FileName=strcat(PathName,Name);
end
    
if exist(FileName,'file') ~= 2
    Errordlg('File does not exist')
else
   
    %% We modify the default kernel settings before reading the file
    %**********************************************************************
    
    % we get the current (default) kernel settings
    ParameterKernel= moKernelParameter();
    ParameterDef=moLoadKernelParameter(ParameterKernel);

    % we modify some of those settings
    ParameterDef.m_bIgnorePhaseData=1; % ignores ping angle data
    ParameterDef.m_bAutoLengthEnable=1; % we don't specify a maximum ping 
                                        % number to keep in memory
    % ParameterDef.m_nbPingFanMax=500;
    ParameterDef.m_bAutoDepthEnable=1 % we don't specify a maximum range
    % ParameterDef.m_MaxRange=150;
    % ParameterDef.m_bAutoDepthTolerance=10;
    % ParameterDef.m_ScreenPixelSizeX=1;
    % ParameterDef.m_ScreenPixelSizeY=0.2;
    % ParameterDef.m_bIgnorePingsWithNoNavigation=1;

    % we apply those new settings before reading
    moSaveKernelParameter(ParameterKernel,ParameterDef);

    clear Parameter;
    clear ParameterDef;
    
    
    %% We modify the default reader settings before reading the file
    %**********************************************************************
    
    % we get the current (default) reader settings
    ParameterR= moReaderParameter();
    ParameterDef=moLoadReaderParameter(ParameterR);

    % this parameter is the number of pingfan to read for each call to
    % the moReadChunk() function
    ParameterDef.m_chunkNumberOfPingFan=2;

    moSaveReaderParameter(ParameterR,ParameterDef);
    clear ParameterR;
    clear ParameterDef;

    %% We do the actuel reading operation : we read until the end of the
    %% file
    %**********************************************************************
    
    % open Hac file
    moOpenHac(FileName);
    
    %we read the whole file
    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1
        moReadChunk();
        FileStatus= moGetFileStatus();
    end;

    fprintf('End Read\n');
end;
end