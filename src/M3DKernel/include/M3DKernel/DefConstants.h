/******************************************************************************/
/*	Project:															  */
/*	Author(s):	C.Poncelet													  */
/*	File:		defconstants.h													  */
/******************************************************************************/


#ifndef DEF_CONSTANTS_H

#define DEF_CONSTANTS_H

enum SpeedType {
	SPTW = 1, //getSpeedThroughWater
	SPUSER
};

// OTK - FAE043 - possibilit� de fixer la position des pings
enum PositionType {
	POSITION_TUPLE = 1,
	POSITION_USER
};


enum SimradBeamType {
	// bit 0: single/split 
	// bit 1-3: ref. beam 
	// bit 4-6: splitbeam type 
	// bit 7: future use 
	// bit 8: ADCP BeamTypeSingle = 0, 

	//Single, SingleWide, SingleNarrow 
	BeamTypeSingle = 0x0,
	BeamTypeSplit = 0x1,			// 4 x quadrant type split beam 
	BeamTypeRef = 0x2,				//ME70 reference beam 
	BeamTypeRefB = 0x4,				//ME70 reference beam 
	BeamTypeSplit3 = 0x11,			//ES38-10... 
	BeamTypeSplit2 = 0x21,			//SideScan... 
	BeamTypeSplit3C = 0x31,
	BeamTypeSplit3CN = 0x41,		//Split3CenterNarrow ES38-7... 
	BeamTypeSplit3CW = 0x51,
	BeamTypeSplit4B = 0x61,			// 4 x half beam type ES split beam EC150-3C... 
	BeamTypeSplitFuture = 0x71,
	BeamTypeADCPSingle = 0x100,		// EC150 type ADCP Single 
	BeamTypeADCPSplit2 = 0x121,		// future EC150 ADCP Split2 
	BeamTypeSinglePassive = 0x10,
	BeamTypeLast = BeamTypeSinglePassive
};
const int HERMES_VERSION_INTRODUCING_BEAM_TYPES = 181;


typedef short DataFmt; // format des donnees
const int DataPrecision = sizeof(DataFmt) * 8;
const DataFmt MAX_VALUE = 0xFF; //0xFFFF

const double XMLBadValue = -9999999.9;

#define NB_CHANNEL_PER_GROUP 4

#define UNKNOWN_DB          -32768				 // valeur en centieme de dB pour un echo dont on n'a pas la donn�e
#define UNKNWON_ANGLE       -32768				 // valeur en centieme de � pour un echo dont on n'a pas la donn�e

#define OUTSIDE_GREY		200
#define DEFAULT_SPEED		4.0f
/*
#define DEFAULT_BEAMS_ANGLE_DEG		1.5f  // angle entre les faisceaux, par defaut
#define DEFAULT_BEAMS_SPACING		0.05f // 0.0146f espacement entre les echantillons (-> regles), par defaut
*/
#define DEFAULT_X_SPACING			10.0f  // resolution en x (taille pixels en m), par defaut
#define DEFAULT_Y_SPACING			5.0f  // resolution (taille pixels en m), par defaut


#define PI					(3.14159265359)
#define PI_D2				(PI / 2.0)
#define DEG_TO_RAD(a)		((a) * PI / 180.0)
#define RAD_TO_DEG(a)		((a) * 180.0 / PI)

#define INTERPOLE(a, b, f)	((a) * (1.0f - (f)) + (b) * (f))

#define ABS(x)				((x) < 0 ? -(x) : (x))
#define SQUARE(x)			((x) * (x))

#define MIN(a, b)			((a) < (b) ? (a) : (b))
#define MIN3(a, b, c)		(MIN(a, MIN(b, c)))
#define MIN4(a, b, c, d)	(MIN3(a, b, MIN(c, d)))
#define MAX(a, b)			((a) > (b) ? (a) : (b))
#define MAX3(a, b, c)		(MAX(a, MAX(b, c)))
#define MAX4(a, b, c, d)	(MAX3(a, b, MAX(c, d)))



/*

#define DEF_WAIT_TIME 100  // ms
#define DEF_WAIT_3D		1  // espacement raffraichissement 3D
#define DEF_AD1 255     // adresse et port ecoute de socket
#define DEF_AD2 255
#define DEF_AD3 255
#define DEF_AD4 255

#define HAC_FORMAT 172

*/
#endif