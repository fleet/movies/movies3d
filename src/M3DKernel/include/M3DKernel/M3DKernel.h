#pragma once
#include "M3DKernel/M3DKernelExport.h"


class MovReadService;
class HacObjectMgr;
class EchoAlgorithmRoot;
class WriteAlgorithm;

class ChunkEval;
class AlgorithmAutoLength;
class TimeShiftEval;
class MovESUMgr;

#include "M3DKernel/utils/stats/RecordedTimeCounter.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "M3DKernel/algorithm/internal/MatlabAlgoStore.h"
#include "M3DKernel/datascheme/UpdateEvent.h"
#include "M3DKernel/datascheme/KernelParameter.h"
#include "M3DKernel/datascheme/SpectralAnalysisParameters.h"

class M3DKERNEL_API M3DKernel
{
public:
	static M3DKernel* GetInstance();
	static void FreeMemory();

	void ClearObjectManager();
	HacObjectMgr* getObjectMgr() { return m_pHacObjectMgr; }
	MatlabAlgoStore m_MatlabAlgoStore;
	RecordedTimeCounter m_timeCounterRecord;

	KernelParameter & GetRefKernelParameter() { return m_kernelParameter; }
	void UpdateKernelParameter(KernelParameter& a);
	TimeShiftEval * getTimeShiftAlgorithm() { return m_pTimeShiftAlg; }

	SpectralAnalysisParameters& GetSpectralAnalysisParameters() { return m_spectralAnalysisParameters; }

	// IPSIS - OTK - Passage ESUManager en membre du HACLoader
	MovESUMgr* getMovESUManager() { return m_pMovESUMgr; }

	/// these algorithm will be called before anyone else
	EchoAlgorithmRoot * getInternalAlgorithmRoot() { return m_pInternalAlgorithmRoot; }

	// these are the regular algorithm
	EchoAlgorithmRoot * getEchoAlgorithmRoot() { return m_pEchoAlgorithmRoot; }

	/// this is the last called one
	EchoAlgorithmRoot * getWriteAlgorithmRoot() { return m_pWriteAlgorithmRoot; }

	void Lock() { m_KernelLock.Lock(); }
	void Unlock() { m_KernelLock.Unlock(); }

private:
	M3DKernel();
	virtual ~M3DKernel();
	CRecursiveMutex m_KernelLock;

	// singleton
	static M3DKernel* m_pKernelInstance;
	KernelParameter m_kernelParameter;
	SpectralAnalysisParameters m_spectralAnalysisParameters;

	HacObjectMgr		*m_pHacObjectMgr;

	EchoAlgorithmRoot	*m_pEchoAlgorithmRoot;
	EchoAlgorithmRoot	*m_pInternalAlgorithmRoot;
	EchoAlgorithmRoot	*m_pWriteAlgorithmRoot;
	TimeShiftEval		*m_pTimeShiftAlg;
	AlgorithmAutoLength *m_pAlgorithmAutoLength;

	// IPSIS - OTK - passage de MovESUManager en membre de HACLoader
	MovESUMgr * m_pMovESUMgr;
};