#pragma once
// Le bloc ifdef suivant est la fa�on standard de cr�er des macros qui facilitent l'exportation 
// � partir d'une DLL. Tous les fichiers contenus dans cette DLL sont compil�s avec le symbole KERNEL_EXPORTS
// d�fini sur la ligne de commande. Ce symbole ne doit pas �tre d�fini dans les projets
// qui utilisent cette DLL. De cette mani�re, les autres projets dont les fichiers sources comprennent ce fichier consid�rent les fonctions 
// KERNEL_API comme �tant import�es � partir d'une DLL, tandis que cette DLL consid�re les symboles
// d�finis avec cette macro comme �tant export�s.

#ifdef WIN32
#ifdef M3DKERNEL_EXPORTS
#define M3DKERNEL_API __declspec(dllexport)
#else
#define M3DKERNEL_API __declspec(dllimport)
#endif
#else
#define M3DKERNEL_API
#endif

#ifdef WIN32
#pragma warning( disable : 4244 ) 
#pragma warning( disable : 4267 ) 
#pragma warning( disable : 4251 ) 
#endif