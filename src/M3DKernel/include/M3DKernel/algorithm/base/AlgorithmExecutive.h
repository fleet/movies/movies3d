/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		AlgorithmExecutive.h												  */
/******************************************************************************/
#ifndef ECHO_ALGORITHM_EXECUTIVE
#define ECHO_ALGORITHM_EXECUTIVE

#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/base/MovObject.h"

#include <cstddef>
#include <vector>

class EchoAlgorithm;

class M3DKERNEL_API AlgorithmExecutiveValue
{
public:

	EchoAlgorithm* m_pAlgorithm;
	unsigned int m_Depth;

	AlgorithmExecutiveValue()
		: m_pAlgorithm(nullptr)
		, m_Depth(1)
	{
	}
	
	bool operator<(const AlgorithmExecutiveValue& value2) const
	{
		return m_Depth < value2.m_Depth;
	}
};

/*
* this class will maintain the hierachical organisation for algorithm
* Algorithm are organized in a hierachical tree, their position depends with resp to their input depth.
* Update of these algorithm will be done in a depth increasing order
*/
class M3DKERNEL_API AlgorithmExecutive : public MovObject
{
public:
	static AlgorithmExecutive* Create(EchoAlgorithm* producer)
	{
		return new AlgorithmExecutive(producer);
	}

	virtual ~AlgorithmExecutive();

	void AddAlgorithm(EchoAlgorithm * a_pAlgorithm);
	void RemoveAlgorithm(EchoAlgorithm * a_pAlgorithm);

	unsigned int getNumberOfExecutiveAlgorithm() const;
	const AlgorithmExecutiveValue & getExecutiveAlgorithm(unsigned int index) const;

private:
	std::vector<AlgorithmExecutiveValue> m_executiveList;
	bool HasAlgorithm(EchoAlgorithm *	a_pAlgorithm, unsigned int &index) const;
	void RemoveAll();

	void Sort();

protected:
	AlgorithmExecutive(EchoAlgorithm* producer);
};

#endif //ECHO_ALGORITHM_EXECUTIVE
