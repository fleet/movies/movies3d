/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		AlgorithmOutput.h												  */
/******************************************************************************/
#ifndef ECHO_ALGORITHM_OUTPUT
#define ECHO_ALGORITHM_OUTPUT

#include "M3DKernel/M3DKernelExport.h"

class EchoAlgorithm;

class M3DKERNEL_API AlgorithmOutput
{
public:
	AlgorithmOutput(EchoAlgorithm* producer);
	
	virtual ~AlgorithmOutput();

	EchoAlgorithm *getProducer() { return m_pProducer; }

private:
	EchoAlgorithm *m_pProducer;
};

#endif //ECHO_ALGORITHM_OUTPUT