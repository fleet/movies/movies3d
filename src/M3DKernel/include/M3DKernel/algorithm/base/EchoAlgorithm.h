/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		EchoAlgorithm.h												  */
/******************************************************************************/
#ifndef ECHO_ALGORITHM
#define ECHO_ALGORITHM

#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/algorithm/base/EchoAlgorithmInterface.h"
// IPSIS - OTK - passage de l'ESU courante en param�tres de ESUStart et end
#include "M3DKernel/datascheme/MovESUMgr.h"

#ifndef WIN32
#define __stdcall
#endif

// OTK - 15/10/2009 - d�finition du callback de progression pour l'utilisation
// de module de traitement de fa�on asynchrone depuis l'IHM, par exemple.
typedef void(__stdcall *PROGRESS_CALLBACK)(const char*, int);

// OTK - 15/10/2009 - d�finition du callback d'annulation du traitement asynchrone
typedef bool(__stdcall *CANCEL_CALLBACK)(void);

class AlgorithmOutput;
class AlgorithmExecutive;

class M3DKERNEL_API EchoAlgorithm : public EchoAlgorithmInterface
{
public:
	// Réglage de l'état de l'algorithme
	virtual void setEnable(bool enable) override;

	void ProcessPingFanAdded(PingFan *pFan);
	void ProcessSounderChanged(std::uint32_t sounderId);
	void ProcessSingleTargetAdded(std::uint32_t sounderId);
	void ProcessStreamClosed(const char *streamName);
	void ProcessStreamOpened(const char *streamName);
	void ProcessESUStarted(ESUParameter * pWorkingESU);
	void ProcessESUEnded(ESUParameter * pWorkingESU, bool abort);
	void ProcessTupleHeaderUpdate();

	const std::set<AlgorithmOutput*>& getInputs() const;
	const std::set<EchoAlgorithm *>& getChainedAlgorithms() const;

	void				AddInput(EchoAlgorithm* algo, bool addToExecutive = true);
	void				AddInput(AlgorithmOutput* algo, bool addToExecutive = true);
	void				RemoveInput(EchoAlgorithm* algo);
	void				RemoveInput(AlgorithmOutput* algo);
	AlgorithmOutput*	getOutPut() const { return m_pOutPut; }

	/// <summary>
	/// Callback appelé par le module entrant quand celui-ci change d'état
	/// </summary>
	/// <param name="algo">Module qui a changé d'état</param>
	virtual void		onInputModuleEnableStateChange(EchoAlgorithm* algo);

	AlgorithmExecutive* getExecutive();
	virtual ~EchoAlgorithm();

protected:
	EchoAlgorithm(const std::string & name);

private:
	std::set<AlgorithmOutput*> m_inputAlgorithms;

	// Algorithmes ayant cet algorithme en input
	std::set<EchoAlgorithm *> m_chainedOutputAlgorithms;

	AlgorithmExecutive* m_pExecutive;

	AlgorithmOutput*	m_pOutPut;
};

#endif //ECHO_ALGORITHM
