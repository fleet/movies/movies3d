/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		EchoAlgorithmInterface.h												  */
/******************************************************************************/
#ifndef ECHO_ALGORITHM_INTERFACE
#define ECHO_ALGORITHM_INTERFACE

#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/base/MovObject.h"
#include "M3DKernel/datascheme/DateTime.h"

#include <set>
#include <string>

class PingFan;
// IPSIS - OTK - passage de l'ESU courant dans les esuStart et end
class ESUParameter;

class M3DKERNEL_API EchoAlgorithmInterface : public MovObject
{
public:

	/**
	* Return the algorithm state : enabled or disabled
	*/
	bool getEnable() const;

	/**
	* Enable or disable the given algorithm
	*/
	virtual void setEnable(bool enable);

	/**
	* Set the start time for the module
	*/
	void setStartTime(HacTime* time);

	/**
	* Check if the module can be started and start it
	* Return true is the module has started
	*/
	bool tryStartModule(const HacTime & time);

	/**
	* FAE 193 : return the module's current enable count
	*/
	int getEnableCount() const { return m_EnableCount; }

	/**
	* Return the algorithm Name
	*/
	const std::string& getName() const { return m_Name; }

	/**
	* call when the enable state change
	*/
	virtual void onEnableStateChange();

protected:
	EchoAlgorithmInterface(const std::string& name);
	virtual ~EchoAlgorithmInterface();

	/**
	* called each time a new PingFan is added
	*
	* @param m_sounderId the id of the sounder having a new pingFan
	*
	*/
	virtual void PingFanAdded(PingFan* pFan) = 0;

	/***
	* initialisation step, will be called once a new sequence or sounder is created
	*
	* @param none
	*
	*/
	virtual void SounderChanged(std::uint32_t sounderId) = 0;

	/***
	*  will be called when and end of stream is encountered
	*
	* @param none
	*
	*/
	virtual void StreamClosed(const char* a_streamName) = 0;

	/***
	*  will be called when a new stream is opened
	*
	* @param none
	*
	*/
	virtual void StreamOpened(const char* a_streamName) = 0;

	/***
	*  will be called when a single target is Added
	*
	* @param none
	*
	*/
	virtual void SingleTargetAdded(std::uint32_t sounderId) {}

	/***
	*  will be called when an ESU starts
	*
	* @param none
	*
	*/
	virtual void ESUStart(ESUParameter* pWorkingESU) {}

	/***
	*  will be called when an ESU is finished
	*
	* @param none
	*
	*/
	virtual void ESUEnd(ESUParameter* pWorkingESU, bool abort) {}

	/***
	*  will be called when a tuple header in received
	*
	* @param none
	*
	*/
	virtual void TupleHeaderUpdate() {}

private:
	const std::string m_Name;
	HacTime* m_startTime;
	bool m_Enable;
	int m_EnableCount;
};

#endif //ECHO_ALGORITHM_INTERFACE