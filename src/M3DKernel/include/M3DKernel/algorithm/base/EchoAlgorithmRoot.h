/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		EchoAlgorithmRoot.h												  */
/******************************************************************************/
#ifndef ECHO_ALGORITHM_ROOT
#define ECHO_ALGORITHM_ROOT

#include "M3DKernel/M3DKernelExport.h"

#include "M3DKernel/algorithm/base/EchoAlgorithm.h"


class M3DKERNEL_API EchoAlgorithmRoot : public EchoAlgorithm
{
public:
	MovCreateMacro(EchoAlgorithmRoot);
	virtual ~EchoAlgorithmRoot();

protected:

	virtual void PingFanAdded(PingFan *pFan);
	virtual void SounderChanged(std::uint32_t sounderId);
	virtual void StreamClosed(const char *streamName);
	virtual void StreamOpened(const char *streamName);

	EchoAlgorithmRoot();
};

#endif //ECHO_ALGORITHM_ROOT