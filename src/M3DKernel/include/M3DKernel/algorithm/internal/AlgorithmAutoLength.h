/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		ChunkEval.h												  */
/******************************************************************************/

#ifndef AUTO_LENGTH_ALGO
#define AUTO_LENGTH_ALGO

#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/datascheme/DateTime.h"

#include "M3DKernel/algorithm/base/EchoAlgorithm.h"

class M3DKERNEL_API AlgorithmAutoLength : public EchoAlgorithm
{
public:
	MovCreateMacro(AlgorithmAutoLength);
	virtual void PingFanAdded(PingFan *pFan);
	virtual void SounderChanged(std::uint32_t sounderId);
	virtual void StreamClosed(const char *streamName);
	virtual void StreamOpened(const char *streamName);

	virtual ~AlgorithmAutoLength() {};
protected:
	AlgorithmAutoLength();
private:
};


#endif //AUTO_LENGTH_ALGO