#pragma once
#include "M3DKernel/M3DKernelExport.h"
#include <vector>

class EchoAlgorithm;

class M3DKERNEL_API MatlabAlgoStore
{
public:
	MatlabAlgoStore();
	~MatlabAlgoStore();

	void	RemoveAlgo(unsigned int index);
	unsigned int		AddAlgo(EchoAlgorithm *pNewAl);

	inline EchoAlgorithm *GetAlgorithm(unsigned int index);
private:
	std::vector<EchoAlgorithm*> m_CreatedAlgo;
};
