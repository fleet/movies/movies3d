#pragma once

#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/datascheme/DateTime.h"
#include "M3DKernel/algorithm/base/EchoAlgorithm.h"
#include "M3DKernel/datascheme/PingFan.h"

class M3DKERNEL_API TimeShiftEval : public EchoAlgorithm
{
public:
	MovCreateMacro(TimeShiftEval);
	virtual void PingFanAdded(PingFan *pFan);
	virtual void SounderChanged(std::uint32_t sounderId);
	virtual void StreamClosed(const char *streamName);
	virtual void StreamOpened(const char *streamName);

	~TimeShiftEval(void);

protected:
	TimeShiftEval(void);
	void EvaluateMeanMasterInterPing();

	/**tolerance in percent to activate a shifting*/
	double m_tolerance;

	std::uint32_t m_MasterSounderId;
	TimeElapse m_meanMasterInterPingTime;

	/// the delayed (i.e the expected sounder emit time) will be given by m_meanMasterInterPingTime/m_delayedMasterFrac; 
	/// currently 0.5*m_meanMasterInterPingTime;
	unsigned int m_delayedMasterFrac;
};
