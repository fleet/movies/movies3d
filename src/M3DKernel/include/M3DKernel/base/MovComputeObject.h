/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		MovObject.h												  */
/******************************************************************************/

#ifndef MOV_COMPUTE_OBJECT
#define MOV_COMPUTE_OBJECT


#include "M3DKernel/base/MovObject.h"
#include "M3DKernel/datascheme/Traverser.h"

class M3DKERNEL_API MovComputeObject : public MovObject
{
public:
	MovComputeObject();

	virtual void Update(Traverser &e) 
	{
		UpdateChildMember(e);
		if (m_bDirty)
		{
			Compute(e);
			m_bDirty = false;
		}
	}

	inline void DataChanged() { m_bDirty = true; }
	inline bool NeedCompute() const { return m_bDirty; }
	inline bool IsComplete() const { return m_bIsComplete; }
	
protected:
	virtual void UpdateChildMember(Traverser &e) {}
	virtual void Compute(Traverser &e) {}

	bool m_bDirty;
	bool m_bIsComplete;
	virtual ~MovComputeObject();
};

#endif //MOV_COMPUTE_OBJECT