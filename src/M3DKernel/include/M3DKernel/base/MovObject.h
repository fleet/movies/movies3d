/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		MovObject.h												  */
/******************************************************************************/

#ifndef MOV_OBJECT
#define MOV_OBJECT


#include "M3DKernel/M3DKernelExport.h"
#include "assert.h"
#include <limits>
#include <cinttypes>


// Macro to implement the standard form of the Create() method.
#define MovCreateMacro(thisClass) \
  static thisClass* Create() \
  { \
    return new thisClass; \
  } \


/**
* Base class for count referenced object
* Be carefull to prevent circular referencing
* If you got a kind of father to child relation ship, the father could ref the child,
* but the child should not ref the father. If you do so, none will be ever destroyed
*
*/
class M3DKERNEL_API MovObject
{
public:
	void lRef(MovObject* ptr);
	void Delete();

	std::uint64_t lUnRef(MovObject* ptr);
	
	void Release(MovObject ** pt);

private:
	MovObject* m_ptr;
	std::uint64_t m_refCount;

	// Disable copy
	MovObject(const MovObject&) {}
	MovObject operator=(const MovObject&) { return *this; }

protected:
	MovObject() : m_refCount(0), m_ptr(nullptr) {}

public:
	virtual ~MovObject();
};

#define MovRef(X) if(X) {X->lRef(X);}
#define MovUnRefDelete(X) 	if(X && X->lUnRef(X)<=0) { X->Delete(); X=0;}

/**
* Classe RAII pour la gestion des objet Movies3D
* Se charge de l'appel � MovRef et MovUnRefDelete
*/
template<typename T>
class MovObjectPtr
{
	T * m_object;
public:
	MovObjectPtr(T * object = nullptr)
		: m_object(object)
	{
		Ref();
	}

	MovObjectPtr(const MovObjectPtr & other)
		: m_object(other.m_object)
	{
		Ref();
	}

	MovObjectPtr & operator=(T * object)
	{
		UnRefDelete();
		m_object = object;
		Ref();
		return *this;
	}

	MovObjectPtr & operator=(const MovObjectPtr & other)
	{
		UnRefDelete();
		m_object = other.m_object;
		Ref();
		return *this;
	}

	~MovObjectPtr()
	{
		UnRefDelete();
	}

	bool operator==(T * object) const
	{
		return m_object == object;
	}

	bool operator==(const MovObjectPtr & other) const
	{
		return m_object == other.m_object;
	}

	bool operator!=(T * object) const
	{
		return m_object != object;
	}

	bool operator!=(const MovObjectPtr & other) const
	{
		return m_object != other.m_object;
	}

	T * operator->() const
	{
		return m_object;
	}

	T * get() const
	{
		return m_object;
	}

	operator bool() const
	{
		return m_object != nullptr;
	}

	bool operator!() const
	{
		return m_object == nullptr;
	}

	void Ref()
	{
		MovRef(m_object);
	}

	void UnRefDelete()
	{
		MovUnRefDelete(m_object);
	}

	static MovObjectPtr<T> make()
	{
		return MovObjectPtr<T>(T::Create());
	}
};



#endif //MOV_OBJECT
