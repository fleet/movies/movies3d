// -*- C++ -*-
// ****************************************************************************
// Class: MovConfig
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mai 2008
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

#include <string>
#include <cassert>

#include "M3DKernel/parameter/ParameterDataType.h"
#include "M3DKernel/parameter/ParameterModule.h"
#include "M3DKernel/parameter/ParameterObject.h"

#include "M3DKernel/config/MovConfigUtil.h"

// XERCES 
namespace xercesc_3_2 {
	class DOMDocument;
	class DOMNode;
	class XercesDOMParser;
}

namespace xercesc = xercesc_3_2;

namespace BaseKernel {

	// ***************************************************************************
	// Declarations
	// ***************************************************************************

	/// Interface principale pour la sauvegarde/chargement de la configuration
	class M3DKERNEL_API MovConfig
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par défaut
		MovConfig();

		// Destructeur
		virtual ~MovConfig();

		// *********************************************************************
		// Traitements
		// *********************************************************************

		// eParameterModule
		//----------------------
		virtual void SerializeData(ParameterModule* parameterModule, ParameterDataType paramType) = 0;
		virtual bool DeSerializeData(ParameterModule* parameterModule, ParameterDataType paramType) = 0;

		// eParameterObject
		//----------------------
		virtual void SerializeData(ParameterObject* parameterObject, ParameterDataType paramType, const char * name) = 0;
		virtual bool DeSerializeData(ParameterObject* parameterObject, ParameterDataType paramType, const char * name) = 0;

		virtual void Serialize(const std::string & str, ParameterDataType paramType, const char * name) = 0;
		virtual bool DeSerialize(std::string & str, ParameterDataType paramType, const char * name) = 0;

		// fonction template pour la serialisation de tous les types de base + Vector2 et Vector3
		//----------------------
		template <typename T>
		void SerializeData(T value, ParameterDataType paramType, const char * name)
		{
			std::string strValue = ToString(value);
			Serialize(strValue, paramType, name);
		}

		template <typename T>
		void SerializeData(T value, ParameterDataType paramType, std::string name)
		{
			SerializeData<T>(value, paramType, name.c_str());
		}

		// fonction template pour la deserialisation de tous les types de base
		//----------------------
		template <typename T>
		bool DeSerializeData(T * value, ParameterDataType paramType, const char * name)
		{
			std::string str;
			bool ok = DeSerialize(str, paramType, name);
			if (ok)
			{
				ok = FromString<T>(*value, str);
			}
			return ok;
		}

		template <typename T>
		bool DeSerializeData(T * value, ParameterDataType paramType, std::string name)
		{
			return DeSerializeData<T>(value, paramType, name.c_str());
		}

		// Fin de blocs
		//----------------------
		virtual void SerializePushBack() = 0;
		virtual void DeSerializePushBack() = 0;
	};


	/// Interface principale pour la sauvegarde/chargement de la configuration depuis un fichier XML
	class M3DKERNEL_API MovConfigFile : public MovConfig
	{
	public:
		/// Constructeur
		MovConfigFile();

		/// Destructeur
		virtual ~MovConfigFile();

		/// Gestion du chemin du fichier XML
		void SetFilePath(std::string path) { m_FilePath = path; }
		std::string GetFilePath() const { return m_FilePath; }

		// eParameterModule
		//----------------------
		virtual void SerializeData(ParameterModule* parameterModule, ParameterDataType paramType);
		virtual bool DeSerializeData(ParameterModule* parameterModule, ParameterDataType paramType);

		// eParameterObject
		//----------------------
		virtual void SerializeData(ParameterObject* parameterObject, ParameterDataType paramType, const char * name);
		virtual bool DeSerializeData(ParameterObject* parameterObject, ParameterDataType paramType, const char * name);

		virtual void Serialize(const std::string & str, ParameterDataType paramType, const char * name);
		virtual bool DeSerialize(std::string & str, ParameterDataType paramType, const char * name);

		// Fin de blocs
		//----------------------
		virtual void SerializePushBack();
		virtual void DeSerializePushBack();

		bool IsValid() const { return m_pCurrentDOMDocument != nullptr; }


	private:
		// chemin d'archivage de la configuration
		std::string m_FilePath;

		// DOMDocument courant. les paramétres en cours de sérialisation sont ajoutés dedans.
		// en fin de sérialisation, c'est ce document qui est écrit dans le fichier de configuration
		xercesc::DOMDocument * m_pCurrentDOMDocument;

		// DOMNode courant. Permet de savoir quel est l'élément "racine" courant pour 
		// l'ajout des noeuds
		xercesc::DOMNode * m_pCurrentDOMNode;

		// Parser XML
		xercesc::XercesDOMParser* m_pParser;
	};


	/// Interface principale pour la sauvegarde/chargement de la configuration depuis un dossier contenant des fichiers XML
	class M3DKERNEL_API MovConfigDirectory : public MovConfig
	{
	public:
		MovConfigDirectory();
		virtual ~MovConfigDirectory();

		// Gestion du répertoire courant de sauvegarde
		void SetPath(std::string path) { m_Path = path; }
		std::string GetPath() const { return m_Path; }

		// Gestion du préfixe
		void SetPrefix(std::string prefix) { m_Prefix = prefix; }
		std::string GetPrefix() const { return m_Prefix; }

		// eParameterModule
		//----------------------
		virtual void SerializeData(ParameterModule* parameterModule, ParameterDataType paramType);
		virtual bool DeSerializeData(ParameterModule* parameterModule, ParameterDataType paramType);

		// eParameterObject
		//----------------------
		virtual void SerializeData(ParameterObject* parameterObject, ParameterDataType paramType, const char * name);
		virtual bool DeSerializeData(ParameterObject* parameterObject, ParameterDataType paramType, const char * name);

		virtual void Serialize(const std::string & str, ParameterDataType paramType, const char * name);
		virtual bool DeSerialize(std::string & str, ParameterDataType paramType, const char * name);

		// Fin de blocs
		//----------------------
		virtual void SerializePushBack();
		virtual void DeSerializePushBack();

	private:

		// *********************************************************************
		// Traitements privés
		// *********************************************************************
		virtual std::string ConstructConfigurationFilePath(std::string parameterModuleName) const;

		// *********************************************************************
		// membres privés
		// *********************************************************************
		// préfixe associée à la configuration (spécifié par l'utilisateur)
		std::string m_Prefix;

		// chemin d'archivage de la configuration
		std::string m_Path;
		
		// nom de la configuration en cours d'�criture
		std::string m_ConfName;
		
		/// Fichier courant
		MovConfigFile * m_pCurrentFile;
	};
}
