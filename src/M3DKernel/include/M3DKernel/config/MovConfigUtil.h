// -*- C++ -*-
// ****************************************************************************
// Description: Fonction utilitaires pour MovConfig
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mai 2008
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include <sstream>
#include <limits>

namespace BaseKernel
{	
	// conversion des donn�es de base en string
	template <typename T>
	static std::string ToString(T aValue)
	{
		std::stringstream ss;
		// OTK - 19/01/2010 - precision maximum pour les doubles en particulier
		ss.precision(std::numeric_limits<T>::digits10 + 1);
		ss << aValue;
		return ss.str();
	}

	// conversion des string en donn�es de base
	template <typename T>
	static bool FromString(T &aValue, const std::string &aStr)
	{
		std::stringstream ss(aStr);
		ss >> aValue;
		return true;
	}
}
