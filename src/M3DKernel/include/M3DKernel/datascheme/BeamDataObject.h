#pragma once

#include "DatedObject.h"
#include "BeamDataSamples.h"

#include <string>
#include <map>

const std::int32_t FondNotFound = 2147483647;

class M3DKERNEL_API BeamDataObject : public MovObject
{
public:
	BeamDataObject();
	virtual ~BeamDataObject();
	bool            m_isValid;
	unsigned short  m_timeFraction; 	///Time Frac  (pr�cision � 0.0001s)
	std::uint32_t	m_timeCPU; 			/// Time CPU
	unsigned short  m_hacChannelId;     /// Unique identifier for this software data channel
	unsigned short  m_transMode;
	std::uint32_t	m_filePingNumber;

	enum TransducerMode
	{
		ActiveTransducerMode = 0, PassiveTransducerMode = 1
	};

	// to keep
	std::int32_t	m_bottomRange; // Detected bottom Range (Pr�cision 0.001 m)
	bool	m_bottomWasFound;

	double m_deviation;
	bool   m_hasDeviation;

	double m_compensateHeave;
	double m_compensateHeaveMovies;

	std::int32_t m_tupleAttribute;

	void push_back_power(double power);
	double computeAveragePower(bool& hasValue, int firstEcho = 0, int lastEcho = -1) const;
	void clearPowers() { m_power.clear(); }
	
	void setAverageNoiseLevel(const std::map<int, double> & averageNoiseLevel) { m_averageNoiseLevel = averageNoiseLevel; }
	const std::map<int, double> & getAverageNoiseLevel() const { return m_averageNoiseLevel; }

	void setNoiseRangeEchoNum(int num) { m_noiseRangeEchoNum = num; }
	int getNoiseRangeEchoNum() const { return m_noiseRangeEchoNum; }

	void setRefNoiseRangeEchoNum(int num) { m_refNoiseRangeEchoNum = num; }
	int getRefNoiseRangeEchoNum() const { return m_refNoiseRangeEchoNum; }

    void SetSamplesContainer(BeamDataSamples* samples) { m_pSamples = samples; MovRef(m_pSamples); }
    void RecoverSamplesMemory() { MovUnRefDelete(m_pSamples); m_pSamples = 0; }

	int capacity() const;
	void reserve(unsigned int);
	inline void push_back_amplitude(short a) { m_pSamples->m_amplitudes.push_back(a);	}
	inline void push_back_angle(short a) { m_pSamples->m_angles.push_back(a); }

	inline int size() const { return m_pSamples->m_amplitudes.size(); }
	inline void setAt(int index, short value) { m_pSamples->m_amplitudes[index] = value; }
	inline short getAt(int index) const { return m_pSamples->m_amplitudes[index]; }

	void SetPositionInFileRun(const std::string & fileName, std::uint32_t streamOffset);
	void GetPositionInFileRun(std::string & fileName, std::uint32_t & streamOffset);

	void SetRunFileName(const std::string& filename);
	const std::string& GetRunFileName() const;

private:
	BeamDataSamples* m_pSamples;
	friend  class PingFan;

	std::vector<double> m_power; //Power for each echo. Used by the NoiseLevel module to calculate the noise level
	std::map<int, double> m_averageNoiseLevel;

	int m_noiseRangeEchoNum = -1;
	int m_refNoiseRangeEchoNum = -1;

	// FAE1975 - ajout des infos de localisation du tuple correspondant dans le fichier lu afin de 
	// simplifier la modification du fichier d'origine.
	std::string m_runFileName;
	std::uint32_t m_streamPosInFile;
};

