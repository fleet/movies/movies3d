#pragma once

#include <vector>
#include "M3DKernel/base/MovObject.h"

class M3DKERNEL_API BeamDataSamples : public MovObject
{
public:
	BeamDataSamples();
	virtual ~BeamDataSamples();
	
private:
	std::vector<short> m_amplitudes;	/// donn�e decompressees 
	std::vector<short> m_angles;		// Phase for each echo
	
	friend  class PingFan;
	friend  class BeamDataObject;
};