/******************************************************************************/
/*	Project:	MOVIES 3D																											  */
/*	Author(s):	O. TONCK (IPSIS)																							*/
/*	File:		BeamWeight.h																											*/
/******************************************************************************/

#ifndef MOV_HAC_BEAMWEIGHT
#define MOV_HAC_BEAMWEIGHT

#include "DatedObject.h"
#include <vector>

class M3DKERNEL_API BeamWeight : public DatedObject
{
public:
	MovCreateMacro(BeamWeight);

protected:
	BeamWeight();

public:
	unsigned short			m_WeightID;
	std::vector<double> m_W;
	std::int32_t			m_tupleAttributes;
	virtual ~BeamWeight();

	bool IsEqual(const BeamWeight &B) const
	{
		return m_WeightID == B.m_WeightID
			&& m_tupleAttributes == B.m_tupleAttributes
			&& std::equal(m_W.begin(), m_W.end(), B.m_W.begin())
			;
	}

};

#endif //MOV_HAC_BEAMWEIGHT