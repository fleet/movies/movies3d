#pragma once
#include "M3DKernel/M3DKernelExport.h"

#include "DatedObject.h"
#include "BaseMathLib/Vector3.h"
#include "BaseMathLib/RotationMatrix.h"


/**
	this class is use to store the last known position offset between all ping fan and all sounder
	therefore it will be known to be last ship position
*/
class PingFan;

class M3DKERNEL_API ComputedNavigation
{
public:
	ComputedNavigation(void);
	virtual ~ComputedNavigation(void);
	void ComputeNavigation(PingFan* pFan, PingFan* pLastFan, double instantSpeed, bool temporalGap);
};
