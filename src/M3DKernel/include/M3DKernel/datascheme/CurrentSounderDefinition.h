#pragma once

#include "M3DKernel/datascheme/Sounder.h"
#include "FanMemory.h"
#include <vector>

/*
* This class will maintain a list of all the Sounder currently defined
*/
class M3DKERNEL_API CurrentSounderDefinition
{
public:
	CurrentSounderDefinition(void);
	virtual ~CurrentSounderDefinition(void);

	// add a Sounder to the Sounder list
	void AddSounder(Sounder *ref);
	// remove a Sounder given a Sounder pointer
	void RemoveSounder(Sounder *ref);
	/// remove a Sounder given its index
	void RemoveSounderIdx(unsigned int);

	// get the last Sounder given its sensor id
	Sounder *GetSounderWithId(std::uint32_t souderId);

	// OTK - FAE065 - renvoie l'ensemble des sondeurs d'un ID donn�
	std::vector<Sounder*> GetSoundersWithId(std::uint32_t souderId);

	Sounder *GetSounder(unsigned int SounderIndex) const;
	unsigned int GetNbSounder() const;

    FanMemory	*GetFanMemory(std::uint32_t idx);
	FanMemory	*GetLinkedFanMemory(std::uint32_t souderId);
	FanMemory	*GetLinkedFanMemory(Sounder *pSound);

	void CheckAllSoundersReaderParameters();

	// OTK - FAE065 - nettoyage des soundeurs
	void CleanSounders(Sounder* pSounder, PingFan* pFan);

	/**
	* Compute Matrix transformation for all sounder, transducer, channel
	*/
	void		ComputeMatrix();

protected:
	struct InternalAssociation
	{
		// OTK - FAE065 - une seule FanMemory pour plusieurs configuration d'un m�me sondeur
		std::vector<Sounder*> m_Sounders;
		FanMemory	*m_pFanMem;
	};
	std::vector<InternalAssociation> m_Stack;

private:
	// OTK - FAE065 - recuperation de l'association d'un sounderID donn�
	InternalAssociation* GetInternalAssociation(std::uint32_t sounderId);

	// Disable copy
	CurrentSounderDefinition(const CurrentSounderDefinition & other) {}
	CurrentSounderDefinition & operator=(const CurrentSounderDefinition & other) { return *this; }
};
