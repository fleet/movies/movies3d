/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		HacTime.h												  */
/******************************************************************************/

#ifndef DATE_TIME
#define DATE_TIME

#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/base/MovObject.h"
#include "TimeElapse.h"

#include <math.h>

class M3DKERNEL_API HacTime
{
public:
	HacTime(std::uint32_t	aTimeCpu, unsigned short	aTimeFraction)
		: m_TimeFraction(aTimeFraction)
		, m_TimeCpu(aTimeCpu)
	{
	}

	HacTime()
		: m_TimeFraction(0)
		, m_TimeCpu(0)
	{
	};

	void SetTimeAsSecond(double aSecond)
	{
		m_TimeCpu = (std::uint32_t)floor(aSecond);
		m_TimeFraction = (unsigned short)floor((aSecond - m_TimeCpu) * 10000);

	}
	virtual ~HacTime() {};

	/*
	* SetToNull:Set to the smallest date we can have
	*/
	void SetToNull()
	{
		m_TimeFraction = 0;
		m_TimeCpu = 0;
	}
	bool IsNull() const
	{
		return m_TimeFraction == 0 && m_TimeCpu == 0;
	}
	
	/*
	* to seconds
	*/
	double ToSeconds() const {
		return static_cast<double>(ToNanoseconds()) * 1e-9;
	}

	/*
	* to nanoseconds
	*/
	uint64_t ToNanoseconds() const {
		auto nanoseconds = 0.0;
		nanoseconds += m_TimeCpu * 1e9;
		nanoseconds += m_TimeFraction * 1e5;
		return static_cast<uint64_t>(nanoseconds);
	}

	/*
	* fraction of sec unit 0.0001 sec // practical range 0.99999 sec
	*/
	unsigned short	m_TimeFraction;
	/*
	* ansi C time (expect unsigned type ie up to year 2106)
	*/
	std::uint32_t	m_TimeCpu;


	void GetTimeDesc(char *, int len);
	void GetShortTimeDesc(char *, int len);
public:

	inline bool operator==(const HacTime& v) const { return v.m_TimeFraction == m_TimeFraction && v.m_TimeCpu == m_TimeCpu; }
	inline bool operator!=(const HacTime& v) const { return !operator==(v); }

	inline bool operator<=(const HacTime&v) const
	{
		return ((m_TimeCpu == v.m_TimeCpu && m_TimeFraction <= v.m_TimeFraction) || m_TimeCpu < v.m_TimeCpu);
		/*if (m_TimeCpu == v.m_TimeCpu)
		{
			return m_TimeFraction <= v.m_TimeFraction;
		}
		else
			return m_TimeCpu < v.m_TimeCpu;*/
	}

	inline bool operator<(const HacTime& v) const
	{
		return ((m_TimeCpu == v.m_TimeCpu  && m_TimeFraction < v.m_TimeFraction) || m_TimeCpu < v.m_TimeCpu);
		/*
		if (m_TimeCpu == v.m_TimeCpu)
		{
			return m_TimeFraction < v.m_TimeFraction;
		}
		else
			return m_TimeCpu < v.m_TimeCpu;
			*/
	}

	inline bool operator>(const HacTime& v) const
	{
		return ((m_TimeCpu == v.m_TimeCpu && m_TimeFraction > v.m_TimeFraction) || m_TimeCpu > v.m_TimeCpu);
		/*if (m_TimeCpu == v.m_TimeCpu)
		{
			return m_TimeFraction > v.m_TimeFraction;
		}
		else
			return m_TimeCpu > v.m_TimeCpu;*/
	}

	inline bool operator>=(const HacTime&v) const
	{
		return ((m_TimeCpu == v.m_TimeCpu && m_TimeFraction >= v.m_TimeFraction) || m_TimeCpu > v.m_TimeCpu);
		/*if (m_TimeCpu == v.m_TimeCpu)
		{
			return m_TimeFraction >= v.m_TimeFraction;
		}
		else
			return m_TimeCpu > v.m_TimeCpu;*/
	}

	HacTime& operator=(const HacTime& v)
	{
		m_TimeFraction = v.m_TimeFraction; m_TimeCpu = v.m_TimeCpu; return *this;
	}


};


TimeElapse M3DKERNEL_API operator-(const HacTime &v1, const HacTime &v2);
HacTime M3DKERNEL_API operator+(const HacTime &v1, const TimeElapse &v2);

#endif //DATE_TIME