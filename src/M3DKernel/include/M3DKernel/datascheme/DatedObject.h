/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		DatedObject.h												  */
/******************************************************************************/

#ifndef DATEDOBJECT_OBJECT
#define DATEDOBJECT_OBJECT

#include "M3DKernel/datascheme/HacObject.h"
#include "M3DKernel/datascheme/DateTime.h"

class M3DKERNEL_API DatedObject : public HacObject
{
public:
	HacTime	m_ObjectTime;
	bool m_bWritten;

protected:
	DatedObject();

public:
	virtual ~DatedObject();
};

#endif //DATEDOBJECT_OBJECT