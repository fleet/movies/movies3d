/******************************************************************************/
/*	Project:	MOVIES3D																											  */
/*	Author(s):	O. TONCK (IPSIS)																							*/
/*	File:		EsuContainer.h																										*/
/******************************************************************************/

#pragma once

#include "M3DKernel/M3DKernelExport.h"

#include <vector>

#include "ESUParameter.h"

class M3DKERNEL_API ESUContainer
{
public:

	/** get a ping Fan given its fanId*/
	ESUParameter* GetESUWithIdx(std::uint32_t ESUIdx);

	ESUParameter* GetESUWithPing(std::uint64_t pingId);

	ESUParameter * GetESUById(std::uint32_t EsuId);

	ESUParameter * GetMostRecentESU() const;

	ESUParameter * GetMostRecentClosedESU() const;

	size_t GetESUNb(void) const { return m_ESUContainer.size(); };
	void PopOldestESU();
	void ClearAll();
	void CleanUp(std::uint64_t oldestPingID);
	void AddESU(ESUParameter *a);

	void ResetDisplayEsuId();

private:
	std::vector<ESUParameter*> m_ESUContainer;
	std::uint32_t m_nextEsuId;
	std::uint32_t m_nextDisplayEsuId;

	// Disable copy
	ESUContainer(const ESUContainer & other);
	ESUContainer operator=(const ESUContainer & other);

public:
	virtual ~ESUContainer();
	ESUContainer();
};




