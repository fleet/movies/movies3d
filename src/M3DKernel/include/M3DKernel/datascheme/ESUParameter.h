/******************************************************************************/
/*	Project:	MOVIE3D																												  */
/*	Author(s):	O. TONCK																											*/
/*	File:		ESUParameter.h      																					  */
/******************************************************************************/

#ifndef ESU_PARAMETER
#define ESU_PARAMETER

#include "M3DKernel/base/MovObject.h"
#include "M3DKernel/M3DKernelExport.h"

#include "M3DKernel/datascheme/DateTime.h"

#include "M3DKernel/config/MovConfig.h"

/*
*	This class will define the ESU parameters
*   possible cut types are :
*		- By Distance (in miles)
*		- By Time (seconds)
*		- By Ping Number
*/
class M3DKERNEL_API ESUParameter : public BaseKernel::ParameterModule, public MovObject
{
public:

	// different cut types
	typedef enum {
		ESU_CUT_BY_DISTANCE = 0,
		ESU_CUT_BY_TIME,
		ESU_CUT_BY_PING_NB
	} TESUCutType;

	// Constructor
	MovCreateMacro(ESUParameter);
	ESUParameter();

	// M�thodes de s�rialisation et d�s�rialisation des param�tres
	virtual bool Serialize(BaseKernel::MovConfig * movConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * movConfig);

	void ResetData();

	// Accessors
	TESUCutType GetESUCutType() const { return m_ESUCutType; };
	double GetESUDistance() const { return m_Distance; };
	HacTime GetESUTime() const { return m_Time; };
	HacTime GetESUEndTime() { return m_EndTime; };
	inline const std::uint64_t & GetESUPingNumber() const { return m_PingNumber; };
	inline const std::uint64_t & GetESULastPingNumber() const { return m_LastPingNumber; };
	std::uint32_t GetSounderRef() { return m_RefSounderID; };
	bool GetUsedBySlice() { return m_EISliceUsed; };
	bool GetUsedByShoal() { return m_EIShoalUsed; };
	bool GetUsed() { return m_EIShoalUsed || m_EISliceUsed; };

	void SetESUCutType(TESUCutType value) { m_ESUCutType = value; };
	void SetESUDistance(double value) { m_Distance = value; };
	void SetESUTime(HacTime value) { m_Time = value; };
	void SetESUEndTime(HacTime value) { m_EndTime = value; };
	void SetESUPingNumber(std::uint64_t value) { m_PingNumber = value; };
	void SetESULastPingNumber(std::uint64_t value) { m_LastPingNumber = value; };
	void SetSounderRef(std::uint32_t value) { m_RefSounderID = value; };
	void SetUsedBySlice() { m_EISliceUsed = true; };
	void SetUsedByShoal() { m_EIShoalUsed = true; };

	TimeElapse	getMaxTemporalGap() const { return m_maxTemporalGap; }
	void		setMaxTemporalGap(TimeElapse a) { m_maxTemporalGap = a; }

	std::uint32_t GetESUId() { return m_esuId; }
	void SetESUId(std::uint32_t value) { m_esuId = value; }

	std::uint32_t GetDisplayESUId() { return m_displayedEsuId; }
	void SetDisplayESUId(std::uint32_t value) { m_displayedEsuId = value; }

private:
	// Esu Cut Type
	TESUCutType m_ESUCutType;

	// ESU Distance
	double m_Distance;

	/// ESU time
	HacTime m_Time;
	HacTime m_EndTime;

	// ESU ping number
	std::uint64_t m_PingNumber;

	//ESU last ping number
	std::uint64_t m_LastPingNumber;


	// EI Slice running ?
	bool m_EISliceUsed;

	// EI Shoal running ?
	bool m_EIShoalUsed;

	// OTK - 03/04/2009 - ajout du maxTemporalGap
	TimeElapse m_maxTemporalGap;

	// OTK - FAE064 - calcul des ESU par rapport � un sondeur de r�f�rence
	std::uint32_t m_RefSounderID;

	std::uint32_t m_esuId;

	// OTK - FAE201 - utilisation pour les sorties de l'EI supervis�e d'un num�ro d'EI qui red�marre apr�s un gap temporel
	std::uint32_t m_displayedEsuId;
};


#endif //ESU_PARAMETER
