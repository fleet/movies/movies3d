/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		Nav.h												  */
/******************************************************************************/

#ifndef MOV_ENVIRONNEMENT
#define MOV_ENVIRONNEMENT

#include "DatedObject.h"

#include <vector>

class M3DKERNEL_API  Measure
{
public:
	Measure();

	double ComputeAbsorptionCoefficient(double frequency) const;

	double				Pressure;
	double				Temperature;
	double				Conductivity;
	double				SpeedOfSound;
	double				DepthFromSurface;
	double				Salinity;
	std::uint32_t		SoundAbsorption;
};

class M3DKERNEL_API Environnement : public DatedObject 
{
public:
	MovCreateMacro(Environnement);

	unsigned short  m_SensorType;
	std::int32_t m_tupleAttributes;

	void AddMeasure(Measure a);
	unsigned int GetNumberOfMeasure() const;
	Measure* GetMeasure(unsigned int index) const;
	const std::vector<Measure> & GetMeasures() const { return m_Measure; }

protected:
	Environnement();

private:
	std::vector<Measure> m_Measure;
};

#endif //MOV_ENVIRONNEMENT