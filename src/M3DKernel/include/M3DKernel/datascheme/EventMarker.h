/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		EventMarker.h												  */
/******************************************************************************/

#ifndef MOV_HAC_EventMarker
#define MOV_HAC_EventMarker

#include "DatedObject.h"

class M3DKERNEL_API EventMarkerData : public DatedObject
{
public:
	std::int32_t m_tupleAttributes;
};

class M3DKERNEL_API EventMarker : public EventMarkerData
{
public:
	MovCreateMacro(EventMarker);
	char * m_Msg;
	void Allocate(std::uint32_t sizeMsg);

protected:
	EventMarker();

public:
	virtual ~EventMarker();
};

#endif //MOV_HAC_EventMarker