
#pragma once
/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		FanMemory.h												  */
/******************************************************************************/

#include "M3DKernel/base/MovObject.h"
#include "M3DKernel/datascheme/MemorySet.h"

#include <deque>

class M3DKERNEL_API FanMemory : public MovObject
{
public:
	MovCreateMacro(FanMemory);

protected:
	FanMemory();

public:
	virtual ~FanMemory();
	MemorySet * GetMemoryIndex(unsigned int idx);

	unsigned int GetMemoryLength();
	void  SetMemoryLength(unsigned int);

	// get the max Range use in the system.
	double GetMaxRange() { return m_maxRange; }
	
	// set the max Depth encountered 
	void SetMaxDepth(double maxDepth, unsigned int maxEchoNb);

	void ShiftOne();
	void CheckReaderParameter();

private:
	std::deque<MemorySet*> m_MemDeque;
	bool m_bIgnorePhase;
	double m_maxRange;
	unsigned int m_maxEchoNb;
};

