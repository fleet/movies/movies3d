/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		MovHacObject.h												  */
/******************************************************************************/

#ifndef MOV_HAC_OBJECT
#define MOV_HAC_OBJECT

#include "M3DKernel/base/MovComputeObject.h"

class M3DKERNEL_API HacObject : public MovComputeObject
{
public:
	virtual void Update(Traverser &e) 
	{
		UpdateChildMember(e);
		if (m_bDirty)
		{
			bool lastcomplete = IsComplete();
			Compute(e);
			if (IsComplete() && lastcomplete != IsComplete())
			{
				OnComplete(e);
			}
			m_bDirty = false;
		}
	}

protected:
	HacObject();
	virtual void OnComplete(Traverser &e) {}

public:
	virtual ~HacObject();
};

#endif //MOV_HAC_OBJECT