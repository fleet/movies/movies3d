/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		Nav.h												  */
/******************************************************************************/

#ifndef MOV_HAC_OBJECTMGR
#define MOV_HAC_OBJECTMGR


#include "M3DKernel/datascheme/HacObject.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "NavAttributes.h"
#include "TimeObjectContainer.h"
#include "Trawl.h"
#include "PingFanContainer.h"

#include <map>

/**
* this object will contain the currents working objects used trough read process
*
*
*/
typedef	 std::map<std::uint32_t, Sounder	*> MapSounder;
typedef	 std::map<unsigned short, TimeObjectContainer	*> MapNavAttitudeContainer;

struct SplitBeamPair
{
	std::uint32_t	m_sounderId;
	unsigned short	m_channelId;
};
typedef std::map<unsigned short, SplitBeamPair> MapSplitBeamChannel;
class EventMarker;
class SplitBeamParameter;

class M3DKERNEL_API HacObjectMgr : public MovComputeObject
{
public:
	MovCreateMacro(HacObjectMgr);

	Sounder	*GetWorkingSounder(std::uint32_t sounderId);
	MapSounder &GetWorkingSounderMap() { return m_WorkingSounder; }
	Sounder	*GetLastValidSounder(std::uint32_t sounderId);
	void		AddSounder(Sounder *);

	virtual ~HacObjectMgr();

	// retrieve a sounder containing a given channel @return true if succeed
	bool FindValidSounderContainingChannel(unsigned short ChannId, std::uint32_t & sounderId);
	bool FindWorkingSounderContainingChannel(unsigned short ChannId, std::uint32_t & sounderId);

	// this will manage preallocation of BeamDataObject stuff
	BeamDataObject* GetBeamDataObject();
	void PopBeamDataObject();
	void PushBeamDataObject(BeamDataObject*);

	// this will manage preallocation of BeamDataSamples stuff
	BeamDataSamples* GetBeamDataSamples();
	void PopBeamDataSamples();
	void PushBeamDataSamples(BeamDataSamples*);

	// this will manage preallocation of BeamDataObject stuff
	PhaseDataObject* GetPhaseDataObject();
	void PopPhaseDataObject();
	void PushPhaseDataObject(PhaseDataObject*);

	// this will manage preallocation of PhaseDataSamples stuff
	PhaseDataSamples* GetPhaseDataSamples();
	void PopPhaseDataSamples();
	void PushPhaseDataSamples(PhaseDataSamples*);

	/// add the given SplitBeamParameter to the right channel
	void AddSplitBeamParameter(SplitBeamParameter*, unsigned short channId);

	// sets and retrieve the last softChannel added
	void SetLastSoftChannel(SoftChannel * pChan);
	SoftChannel	*GetLastSoftChannel();

	// OTK - 21/12/2009 - lecture des fichiers HAC avec un mauvais nombre de channels
	// dans le tuple sondeur
	void CorrectChannelNumbers(Traverser &e);

	/***
		this will retrieve the channel and sounder Id associated with the splitBeamParameter id given
		@return true if found, false otherwise
	*/
	bool GetChannelParentSplitBeam(unsigned short aSplitBeamParameterChannelId, SplitBeamPair &ref);

	virtual void ComputePostRead(Traverser &e, bool bLastClose);

	CurrentSounderDefinition & GetSounderDefinition() { return m_pingFanContainer.m_sounderDefinition; };

	TimeObjectContainer*	GetNavAttitudeContainer(unsigned short attitudeSensorId);
	unsigned int			GetNumberOfNavAttitudeSensorId();
	unsigned short			GetNavAttitudeSensorId(unsigned int index);

	TimeObjectContainer*	GetNavAttributesContainer();
	TimeObjectContainer*	GetNavPositionContainer();
	TimeObjectContainer*	GetEventMarkerContainer();
	TimeObjectContainer*	GetEnvironnementContainer();

	// OTK - FAE037 - interpolation de la position des pings
	NavPosition* CreateInterpolatedPosition(const HacTime &time);
	NavAttitude* CreateInterpolatedAttitude(unsigned short attitudeSensorId, const HacTime &time);

	PingFanContainer & GetPingFanContainer() { return m_pingFanContainer; }

	/// function CheckTimeObject : remove object strictly older than the oldest ping fan found in sounders objects
	void CheckTimeObject();

	Trawl * GetTrawl() { return m_pTrawl; }

	// NME - FAE 86 - accesseurs au flag de pr�sence des informations de navigation
	void SetHasNavAttributes(bool hasNavAttributes);
	bool GetHasNavAttributes();

	// NME - FAE 86 - calcul des informations de navigations
	void ComputeNavAttributes(NavPosition * p);

public:
	PingFanContainer m_pingFanContainer;

protected:
	virtual void Compute(Traverser &e);
	virtual void UpdateChildMember(Traverser &e);
	HacObjectMgr();

private:
	void RemoveOldObject(HacTime &oldestDate);

	void ComputeSounder(Traverser &e);

	MapSounder	m_WorkingSounder;
	bool FindSounderContainingChannel(unsigned short ChannId, std::uint32_t & sounderId, MapSounder &refMap);

	MapNavAttitudeContainer		m_mapNavAttitudeContainer;
	std::vector<unsigned short> m_NavAttitudeSensorId;
	std::vector<BeamDataObject*>	m_pPreAllocBeamDataObject;
	std::vector<BeamDataSamples*>	m_pPreAllocBeamDataSamples;
	std::vector<PhaseDataObject*>	m_pPreAllocPhaseDataObject;
	std::vector<PhaseDataSamples*> m_pPreAllocPhaseDataSamples;

	MapSplitBeamChannel m_mapSplitBeamChannel;

	SoftChannel * m_pLastSoftChannel;

	TimeObjectContainer	*m_pEventMarker;
	TimeObjectContainer *m_pNavAttributes;
	TimeObjectContainer *m_pNavPosition;
	TimeObjectContainer *m_pEnvironnement;
	Trawl				*m_pTrawl;

	// NME - FAE 86 - flag de pr�sence des informations de navigation
	bool m_HasNavAttributes;

	// Derni�re position de navigation utilis�e pour estimer les infos de navigation
	NavPosition * m_pLastNavPosition;

	// Derni�re position de navigation ajout�e
	NavPosition * m_pPreviousNavPosition;

	// compteur de nouvelles positions GPS
	int m_deltaPos;

	// liste des derni�res valeurs de vitesses
	std::list<double> m_sTabSpeed;

	// Sauvegarde des valeurs custom de l'environnement
	bool m_isUsingCustomEnvironmentValues;
	double m_customSpeedOfSound;
	double m_customSalinity;
};

#endif //MOV_HAC_OBJECTMGR
