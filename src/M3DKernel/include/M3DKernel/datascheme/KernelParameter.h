#pragma once
#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/DefConstants.h"
#include "M3DKernel/parameter/ParameterModule.h"
#include "BaseMathLib/Vector3.h"
#include "M3DKernel/parameter/ParameterProjection.h"

class M3DKERNEL_API KernelParameter : public BaseKernel::ParameterModule
{
public:
	KernelParameter();
	void			ResetData();

	// config
	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	// memory managagement
	int				getNbPingFanMax() { return 	m_nbPingFanMax; }
	void			setNbPingFanMax(int a) { m_nbPingFanMax = a; }
	bool			getAutoLengthEnable() { return m_bAutoLengthEnable; }
	void			setAutoLengthEnable(bool a) { m_bAutoLengthEnable = a; }


	// get and set methods return the number of sample in Y polar coord
	bool			getAutoDepthEnable() { return m_bAutoDepthEnable; }
	void			setAutoDepthEnable(bool a) { m_bAutoDepthEnable = a; }
	float			getAutoDepthTolerance() { return m_bAutoDepthTolerance; }
	void			setAutoDepthTolerance(float a) { m_bAutoDepthTolerance = a; }

	// screen transform settings
	bool				getCustomSampling() { return m_CustomSampling; }
	double			getScreenPixelSizeX() { return m_ScreenPixelSizeX; }
	double			getScreenPixelSizeY() { return m_ScreenPixelSizeY; }
	void			setCustomSampling(bool a) { m_CustomSampling = a; }
	void			setScreenPixelSizeX(double a) { m_ScreenPixelSizeX = a; }
	void 			setScreenPixelSizeY(double a) { m_ScreenPixelSizeY = a; }


	/// ground things
	float			getMaxRange() { return m_MaxRange; }
	void			setMaxRange(float a) { m_MaxRange = a; }

	/// speed management
	SpeedType		getSpeedUsed(void) const { return m_SpeedUsed; }
	void			setSpeedUsed(SpeedType a) { m_SpeedUsed = a; };
	void			setUserSpeedValue(float a) { m_UserSpeed = a; }
	float			getUserSpeedValue(void) const { return m_UserSpeed; }

	/// position management
	PositionType	getPositionUsed(void) const { return m_PositionUsed; }
	void			setPositionUsed(PositionType a) { m_PositionUsed = a; };
	void			setUserPositionValue(BaseMathLib::Vector3D a) { m_UserPosition = a; }
	BaseMathLib::Vector3D	getUserPositionValue(void) const { return m_UserPosition; }



	///	read Parameter
	bool			getIgnoreIncompletePings() const { return m_bIgnoreIncompletePings; }
	void			setIgnoreIncompletePings(bool a) { m_bIgnoreIncompletePings = a; }

	bool			getIgnorePhase() const { return m_bIgnorePhaseData; }
	void			setIgnorePhase(bool a) { m_bIgnorePhaseData = a; }

	bool			getIgnorePingsWithNoNavigation() const { return m_bIgnorePingsWithNoNavigation; }
	void			setIgnorePingsWithNoNavigation(bool a) { m_bIgnorePingsWithNoNavigation = a; }

	bool			getIgnorePingsWithNoPosition() const { return m_bIgnorePingsWithNoPosition; }
	void			setIgnorePingsWithNoPosition(bool a) { m_bIgnorePingsWithNoPosition = a; }

	bool			getIgnoreAsynchronousChannels() const { return m_bIgnoreAsynchronousChannels; }
	void			setIgnoreAsynchronousChannels(bool a) { m_bIgnoreAsynchronousChannels = a; }

	bool			getInterpolatePingPosition() const { return m_bInterpolatePingPosition; }
	void			setInterpolatePingPosition(bool a) { m_bInterpolatePingPosition = a; }

	bool			getInterpolateAttitudes() const { return m_bInterpolateAttitudes; }
	void			setInterpolateAttitudes(bool a) { m_bInterpolateAttitudes = a; }

	bool			getKeepQuadrantsInMemory() const { return m_bKeepQuadrantsInMemory; }
	void			setKeepQuadrantsInMemory(bool a) { m_bKeepQuadrantsInMemory = a; }

	// time shift correction ?
	bool			getCorrectSounderTimeShift() const { return m_bCorrectSounderTimeShift; }
	void			setCorrectSounderTimeShift(bool a) { m_bCorrectSounderTimeShift = a; }

	bool			getLogPingFanInterTime() const { return m_logPingFanInterTime; }
	void			setLogPingFanInterTime(bool a) { m_logPingFanInterTime = a; }

	bool			getWeightedEchoIntegration() const { return m_bWeightedEchoIntegration; }
	void			setWeightedEchoIntegration(bool a) { m_bWeightedEchoIntegration = a; }

	int       getInitWindowWidth() { return m_InitWindowWidth; }
	void      setInitWindowWidth(int a) { m_InitWindowWidth = a; }

	int       getInitWindowHeight() { return m_InitWindowHeight; }
	void      setInitWindowHeight(int a) { m_InitWindowHeight = a; }

	std::vector<BaseKernel::ParameterProjection>& getProjections() { return m_Projections; }
	int       getSelectedProjection() { return m_SelectedProjection; }
	void      setSelectedProjection(int a) { m_SelectedProjection = a; }


	void			setTemperature(double value) { m_temperature = value; }
	double			getTemperature() const { return m_temperature; }

	void			setSalinity(double value) { m_salinity = value; }
	double			getSalinity() const { return m_salinity; }

	void			setSpeedOfSound(double value) { m_speedOfSound = value; }
	double			getSpeedOfSound() const { return m_speedOfSound; }

	void			useCustomEnvironmentValues(bool value) { m_useCustomEnvironmentValues = value; }
	bool			isUsingCustomEnvironmentValues() const { return m_useCustomEnvironmentValues; }

private:
	float m_MaxRange;

	float m_UserSpeed;
	SpeedType m_SpeedUsed;

	// OTK - FAE043 - option de positionnement fixe
	BaseMathLib::Vector3D m_UserPosition;
	PositionType m_PositionUsed;


	bool			m_logPingFanInterTime;
	// OTK - 23/03/2009 - ajout du mode d'�chantillonnage automatique
	bool			m_CustomSampling;
	// size definition of a single pixel (in meter)
	double		m_ScreenPixelSizeX;
	// size definition of a single pixel (in meter)
	double		m_ScreenPixelSizeY;

	unsigned int	m_nbPingFanMax;
	bool			m_bAutoLengthEnable;
	bool			m_bAutoDepthEnable;
	float			m_bAutoDepthTolerance;

	bool			m_bIgnoreIncompletePings;
	bool			m_bIgnorePhaseData;
	bool			m_bIgnorePingsWithNoNavigation;
	bool			m_bIgnorePingsWithNoPosition;  // NMD - 06/11/2012 -  FAE 120, ajout du param�tre pour ignorer les pings sans position
	bool			m_bIgnoreAsynchronousChannels; // FAE082
	bool			m_bInterpolatePingPosition;
	bool			m_bInterpolateAttitudes;
	bool			m_bCorrectSounderTimeShift;

	bool			m_bKeepQuadrantsInMemory;

	bool      m_bWeightedEchoIntegration;

	// OTK - FAE021 - prise en compte de la taille des visu 2D d�s le chargement du premier ping
	// (pour l'IHM uniquement)
	int       m_InitWindowWidth;
	int       m_InitWindowHeight;

	// OTK - FAE055 - syst�mes de projection
	int       m_SelectedProjection;
	std::vector<BaseKernel::ParameterProjection> m_Projections;

	double			m_temperature;
	double			m_speedOfSound;
	double			m_salinity;
	bool			m_useCustomEnvironmentValues;
};
