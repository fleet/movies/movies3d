#pragma once

#include "M3DKernel/DefConstants.h"
#include "M3DKernel/datascheme/HacObject.h"
#include "M3DKernel/datascheme/PhaseDataObject.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"

#include "BaseMathLib/Vector3.h"

#include <algorithm>
#include <cstring>

template  <typename T> 
class M3DKERNEL_API MemoryObject : public MovObject
{
public:
	MovCreateMacro(MemoryObject);
	inline void Allocate(unsigned int sizeX, unsigned int sizeY, T fillValue)
	{
		Free();
		assert(sizeX);
		assert(sizeY);
		m_pData = new T[sizeX*sizeY];
		m_SizeX = sizeX;
		m_SizeY = sizeY;
		T *pStart = m_pData;
		for (unsigned int i = 0; i < m_SizeX; i++)
		{
			for (unsigned int j = 0; j < m_SizeY; j++)
			{
				*pStart++ = fillValue;
			}
		}
	}
	// OTK - FAE077 - permet d'initialiser un conteneur m�moire � partir des valeurs d'un autre 
	// en appliquant �ventuellement un d�calage d'�chantillons propre � chaque  channel.
	// on gagne en performances plutot que de faire une remiere allocation par d�faut
	// pour une reaffectation de chaque valeur !
	template <typename B>
	inline void Allocate(MemoryObject<B>& source, const std::vector<int>& tx_dec)
	{
		Free();

		unsigned int sourceSizeY = source.getSize().y;
		unsigned int sourceSizeX = source.getSize().x;

		assert(sourceSizeX);
		assert(sourceSizeY);

		// d�calage maximum
		int max_tx = *std::max_element(tx_dec.begin(), tx_dec.end());

		assert(((int)sourceSizeY - max_tx) > 0);

		m_SizeX = sourceSizeX;
		m_SizeY = sourceSizeY - max_tx;
		m_pData = new T[m_SizeX*m_SizeY];
		T *pStart = m_pData;
		B *pSourceStart;
		B *pSourceOrigin = source.GetData();
		for (unsigned int i = 0; i < m_SizeX; i++)
		{
			pSourceStart = pSourceOrigin + ((int)(i*sourceSizeY) + max_tx - tx_dec[i]);
			for (unsigned int j = 0; j < m_SizeY; j++)
			{
				*pStart++ = *pSourceStart++;
			}
		}
	}
	inline void Reallocate(unsigned int sizeX, unsigned int sizeY, T fillValue)
	{
		if (sizeX == m_SizeX && sizeY == m_SizeY)
			return;
		T *p = new T[sizeX*sizeY];
		if (p)
		{
			T *pStart;
			int diffSizeY = sizeY - m_SizeY;
			for (unsigned int i = 0; i < sizeX; i++)
			{
				if (i < m_SizeX)
				{
					std::memcpy(p + i*sizeY, m_pData + i*m_SizeY, std::min<unsigned int>(m_SizeY, sizeY) * sizeof(T));
					if (diffSizeY > 0)
					{
						pStart = p + i*sizeY + m_SizeY;
						for (int j = 0; j < diffSizeY; j++)
						{
							*pStart++ = fillValue;
						}
					}
				}
				else
				{
					pStart = p + i*sizeY;
					for (unsigned int j = 0; j < sizeY; j++)
					{
						*pStart++ = fillValue;
					}
				}
			}

			//FRE - 17/09/2009 - Lock memory for safe access
			Lock();

			m_SizeX = sizeX;
			m_SizeY = sizeY;
			Free();
			m_pData = p;

			//FRE - 17/09/2009 - Unlock memory for safe access
			Unlock();
		}
		else
		{
			M3D_LOG_ERROR("M3DKernel.datascheme.MemoryObject", "Reallocate::No more Memory avalaible");
		}
	}

	inline void SetDefault(unsigned int sizeX, unsigned int sizeY, T fillValue)
	{
		//FRE - 17/09/2009 - Lock memory for safe access
		Lock();

		assert(sizeX <= m_SizeX && sizeY <= m_SizeY);

		T *pStart = m_pData;
		for (unsigned int i = 0; i < sizeX; i++)
		{
			for (unsigned int j = 0; j < sizeY; j++)
			{
				*pStart++ = fillValue;
			}
		}

		//FRE - 17/09/2009 - Unlock memory for safe access
		Unlock();
	}

	virtual ~MemoryObject()
	{
		Free();
	}
	inline T* GetData()
	{
		return m_pData;
	}
	inline T* GetPointerToVoxel(const BaseMathLib::Vector2I& pos)
	{
		return &(m_pData[pos.x*m_SizeY + pos.y]);
	}
	inline T GetValueToVoxel(int x, int y)
	{
		return m_pData[x*m_SizeY + y];
	}
	inline void SetValueToVoxel(int x, int y, T value)
	{
		m_pData[x*m_SizeY + y] = value;
	}
	inline T GetValueToVoxel_ThreadSafe(BaseMathLib::Vector2I pos)
	{
		//FRE - 17/09/2009 - Lock memory for safe access
		Lock();

		T result = *GetPointerToVoxel(pos);

		//FRE - 17/09/2009 - Unlock memory for safe access
		Unlock();

		return result;
	}

	BaseMathLib::Vector2I getSize()
	{
		return BaseMathLib::Vector2I(m_SizeX, m_SizeY);
	}

	inline void Lock() { m_Lock.Lock(); }
	inline void Unlock() { m_Lock.Unlock(); }

	MemoryObject()
		: m_SizeX(0)
		, m_SizeY(0)
		, m_pData(NULL)
	{
	}

private:
	CRecursiveMutex m_Lock;

	unsigned int m_SizeX;
	unsigned int m_SizeY;

	T *m_pData;

	void Free()
	{
		if (m_pData)
			delete[] m_pData;
		m_pData = NULL;
	}

};
template class M3DKERNEL_API MemoryObject<DataFmt>;
template class M3DKERNEL_API MemoryObject<Phase>;
//template class M3DKERNEL_API MemoryObject<short>;
template class M3DKERNEL_API MemoryObject<char>;
template class M3DKERNEL_API MemoryObject<double>;

typedef MemoryObject<DataFmt>	MemoryObjectDataFmt;
typedef MemoryObject<Phase>		MemoryObjectPhase;
typedef MemoryObject<short>		MemoryOverlap;
typedef MemoryObject<char>		MemoryFiltered;
typedef MemoryObject<double>	MemoryObjectDouble;
