#pragma once
#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/base/MovObject.h"
#include "TransducerIndexed.h"
#include "M3DKernel/datascheme/MemoryStruct.h"

class M3DKERNEL_API MemorySet : public MovObject
{

public:
	MovCreateMacro(MemorySet);

	virtual ~MemorySet(void);
	inline MemoryStruct* GetMemoryStruct(unsigned int Index) { return m_IndexedMemory.GetObject(Index); }
	unsigned int  GetMemoryStructCount();

	void SetMaxRange(double m_range);
	void UpdateIgnorePhaseFlag();

	void		SetSounder(Sounder	*);

protected:
	TransducerIndexed<MemoryStruct> m_IndexedMemory; /// ptr to a transducer indexed table of memoryStruct
	MemorySet(void);
};
