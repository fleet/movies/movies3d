#pragma once

#include "M3DKernel/datascheme/MemoryObject.h"
class Transducer;

class M3DKERNEL_API MemoryStruct : public MovObject
{
public:
	MovCreateMacro(MemoryStruct);
	~MemoryStruct();
	inline MemoryObjectDataFmt*	GetDataFmt() { return m_pAmpData; }
	inline MemoryObjectDataFmt*	GetAngleDataFmt() { return m_pAngleData; }
	inline MemoryObjectPhase*	GetPhase() { return m_pPhase; }
	inline MemoryOverlap*		GetOverLap() { return m_pOverLap; }
	inline MemoryFiltered*		GetFilterFlag() { return m_pFilterFlag; }

	void SetMaxRange(double range);
	void CleanFilterAndOverlap(double range);
	void UpdateIgnorePhaseFlag();

	// OTK - 06/01/2010 - pas d'allocation syst�matique de l'overlap
	// ajout m�thode pour allouer � la demande de l'overlapcomputer
	void AllocateOverlap();
	void LockOverLap() { m_OverLapLock.Lock(); };
	void UnLockOverLap() { m_OverLapLock.Unlock(); };
	unsigned int GetSizeDepthUsed() { return m_SizeDepthUsed; };

	Transducer	*GetTransducer() { return m_pTransducer; };
	void		SetTransducer(Transducer	*);

protected:
	void Reallocate(bool ignorePhase, unsigned int sizeX, unsigned int sizeY);
	void Allocate(bool ignorePhase, unsigned int sizeX, unsigned int sizeY);

private:
	unsigned int		m_SizeDepthUsed;
	Transducer			*m_pTransducer;
	MemoryObjectDataFmt *m_pAmpData;
	MemoryObjectDataFmt *m_pAngleData;
	MemoryObjectPhase	*m_pPhase;
	MemoryOverlap		*m_pOverLap;
	MemoryFiltered		*m_pFilterFlag;
	
	MemoryStruct();

	// OTK - 06/01/2010 - ajout lock pour concurrence entre
	// le redimensionnement de la zone par le thread de lecture
	// et l'utilisation des donn�es de la zone par d'�ventuels
	// threads de traitement
	CRecursiveMutex m_OverLapLock;
	int m_OverlapEmptySizeX;
	int m_OverlapEmptySizeY;
};