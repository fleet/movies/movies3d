/******************************************************************************/
/*	Project:	MOVIE3D																												  */
/*	Author(s):	O. TONCK																											*/
/*	File:		MovESUMgr.h      																					  */
/******************************************************************************/

#ifndef MOV_ESU_MGR
#define MOV_ESU_MGR

#include "M3DKernel/base/MovObject.h"
#include "M3DKernel/M3DKernelExport.h"


#include "PingFanContainer.h"

#include "ESUParameter.h"
#include "ESUContainer.h"


/*
*	This static class contains the properties of the ESU cut strategy
*/
class M3DKERNEL_API MovESUMgr : public MovObject
{
public:
	MovCreateMacro(MovESUMgr);
	MovESUMgr();

	// Accesseurs
	ESUParameter&   GetRefParameter() { return m_Parameter; };
	ESUParameter*   GetWorkingESU() { return m_pWorkingESU; };
	ESUParameter*   GetMostRecentClosedESU() { return m_ESUContainer.GetMostRecentClosedESU(); }
	ESUParameter&   GetRefCurrentPosition() { return m_CurrentPosition; };
	ESUContainer&   GetESUContainer() { return m_ESUContainer; };

	// traitements //

	// initialisation des donn�es
	virtual void ResetData();

	// teste l'appartenance � lESU courant et d�clenche les �v�nements 
	// ESUStart et ESUEnd
	virtual void ManageESU(PingFanContainer * pPingFanContainer, Traverser &e);

	// Met a jour la position courante
	virtual void SetCurrentPosition(double distance, HacTime time
		, std::uint64_t pingID, std::uint32_t sounderID);

	// force la fin de l'ESU courant et le d�but du prochain
	virtual void ForceNewESU(bool bProcessClosedESU = false);

	// OTK - FAE064 - utilisation d'un sondeur de r�f�rence poru le calcul des ESU
	virtual void SetSounderRef(std::uint32_t sounderID);

	virtual ~MovESUMgr();

private:
	// Creation nouvel ESU
	virtual void CreateNewESU(bool continuous, PingFanContainer * pPingFanContainer);

	// V�rifie si le pingFan courant appartient � l'ESU en cours
	virtual bool CurrentPingFanIsInCurrentESU();

	// Param�tres de d�coupage des ESU
	ESUParameter	m_Parameter;

	// Param�tres de l'ESU courant (ping de d�but, date de d�but ou distance de d�but)
	ESUParameter  * m_pWorkingESU;
	
	// Position courante
	ESUParameter	m_CurrentPosition;
	
	// si positionn� a vrai, le prochain passage 
	// de ManageESU clos l'ESu courant et en d�marre un nouveau
	bool m_ForceRenew;
	
	// Indique si l'ESU dont la fin est forc�e doit faire l'objet de traitement ou non pour l'EI supervis�e (FAE 1978)
	bool m_ProcessClosedESU;
	
	// Indique si le point de d�part de l'ESU doit �tre mis � jour avec les donn�es du prochain ping du sondeur de r�f�rence
	bool m_bUpdateCurrentESUStart;

	// conteneur des ESU pass�es pour �ventuelle utilisation en visu 2D/3D
	ESUContainer m_ESUContainer;
};

#endif //MOV_ESU_MGR
