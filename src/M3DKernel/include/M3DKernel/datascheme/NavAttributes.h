/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		Nav.h												  */
/******************************************************************************/

#ifndef MOV_HAC_NAV
#define MOV_HAC_NAV

#include "DatedObject.h"

#include <string>
#include <list>

enum TupleObjectSourceType
{
	eTupleSource,
	eInterpSource,
	eDefaultSource
};

/// angles are in degrees
class M3DKERNEL_API NavPosition : public DatedObject 
{
public:
	MovCreateMacro(NavPosition);

	TupleObjectSourceType m_source;
	double m_longitudeDeg;
	double m_lattitudeDeg;
	std::uint32_t   m_gpsTime;
	unsigned short  m_positionningSystem;
	std::int32_t			m_tupleAttributes;

protected:
	NavPosition() 
		: DatedObject() 
	{ 
		m_longitudeDeg = 0; 
		m_lattitudeDeg = 0; 
		m_tupleAttributes = 1; 
		m_source = eDefaultSource; 
	}

	//	float m_yaw;
};

/// angles are in radians, and heave is in meters
class M3DKERNEL_API NavAttitude : public DatedObject
{
public:
	MovCreateMacro(NavAttitude);

	TupleObjectSourceType m_source;
	double m_pitchRad;
	double m_rollRad;
	double m_heaveMeter;
	double m_yawRad;
	bool	 m_bDefaultYaw;
	bool m_heaveCM;

	unsigned short  m_AttitudeSensorId;
	std::int32_t			m_tupleAttributes;

protected:

	NavAttitude() 
		: DatedObject() 
	{ 
		m_pitchRad = 0; 
		m_rollRad = 0; 
		m_yawRad = 0; 
		m_heaveMeter = 0; 
		m_bDefaultYaw = false; 
		m_heaveCM = false;  
		m_tupleAttributes = 1; 
		m_source = eDefaultSource; 
	}
};


/// angles are in radians, and speed is in meters/s

class M3DKERNEL_API NavAttributes : public DatedObject
{
public:
	MovCreateMacro(NavAttributes);

	unsigned short	m_NavigationSystem;
	double			m_headingRad;
	double			m_speedMeter;
	std::int32_t			m_tupleAttributes;
	bool      m_bIsEstimated;
	
	// Calcul de la vitesse entre deux positions de navigations dat�es
	static double GetSpeed(NavPosition * pNavPosition1, NavPosition * pNavPosition2);

	// Calcul de la vitesse � partir des positions de navigations donn�es
	static double GetSpeed(std::list<NavPosition *> & navPositionList);

	// Calcul de la distance entre deux positions de navigations
	static double GetDistance(NavPosition * pNavPosition1, NavPosition * pNavPosition2);

	// Calcul du cap entre 2 positions
	static double GetHeading(NavPosition * pNavPosition1, NavPosition * pNavPosition2);

protected:

	NavAttributes() 
		: DatedObject()
	{
		m_headingRad = 0;
		m_speedMeter = 0;
		m_NavigationSystem = 0;
		m_tupleAttributes = 1;
		m_bIsEstimated = false;
	}
};


#endif //MOV_HAC_NAV
