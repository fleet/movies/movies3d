#pragma once

#include <vector>
#include "PhaseDataSamples.h"

class M3DKERNEL_API PhaseDataObject : public MovObject
{
public:
	PhaseDataObject(void);
	~PhaseDataObject(void);

    void SetSamplesContainer(PhaseDataSamples* samples) { m_pSamples = samples; MovRef(m_pSamples); }
    void RecoverSamplesMemory() { MovUnRefDelete(m_pSamples); m_pSamples = 0; }

	int capacity();
	void reserve(unsigned int);
    void push_back(const Phase &);
	int size();
	void setAt(int index, short along, short athwart);

	unsigned short		m_timeFraction; 	///Time Frac  (pr�cision � 0.0001s)
	std::uint32_t		m_timeCPU; 			/// Time CPU
	unsigned short		m_hacChannelId;     /// Unique identifier for this software data channel
	unsigned short		m_transMode;
	std::uint32_t		m_filePingNumber;
	std::int32_t				m_detectedBottomRange;
	std::int32_t			tupleAttributes;

private:
	PhaseDataSamples* m_pSamples;
	friend class PingFan;
};
