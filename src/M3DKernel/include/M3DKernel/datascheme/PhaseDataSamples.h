#pragma once

#include <cmath>
#include <vector>
#include "M3DKernel/base/MovObject.h"
#include "M3DKernel/DefConstants.h"

struct M3DKERNEL_API Phase
{
public:
	Phase() : AthwartValue(UNKNWON_ANGLE), AlongValue(UNKNWON_ANGLE) {}

	Phase(short athwart, short along) : AthwartValue(athwart), AlongValue(along) {}

	inline double GetAthwart() const { return DEG_TO_RAD((double)AthwartValue*0.01); }

	inline double GetAlong() const { return DEG_TO_RAD((double)AlongValue*0.01); }

	inline void SetAthwart(double athwart) { AthwartValue = (short)floor(0.5 + RAD_TO_DEG(athwart)*100.0); }

	inline void SetAlong(double along) { AlongValue = (short)floor(0.5 + RAD_TO_DEG(along)*100.0); }

	inline void SetValues(double along, double athwart) { AlongValue = (short)(along * 100.0); AthwartValue = (short)(athwart * 100.0); }

	inline void SetRawValues(short along, short athwart) { AlongValue = along; AthwartValue = athwart; }

	inline short GetRawAthwartValue() const { return AthwartValue; }

	inline short GetRawAlongValue() const { return AlongValue; }

	// theese values as stored in 1/100th of degrees
private:
	short AthwartValue;
	short AlongValue;
};

class M3DKERNEL_API PhaseDataSamples : public MovObject
{
public:
	PhaseDataSamples();
	virtual ~PhaseDataSamples();

private:
	std::vector<Phase>	m_sampleData;  /// donn�e decompressees 
	friend  class PingFan;
	friend  class PhaseDataObject;
};
