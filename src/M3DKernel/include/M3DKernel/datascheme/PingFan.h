/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		PingFan.h												  */
/******************************************************************************/

#ifndef MOV_HAC_PINGFAN
#define MOV_HAC_PINGFAN

#include <map>
#include <vector>


class Sounder;
class Transducer;
class NavAttributes;
class NavAttitude;
class NavPosition;
class MemoryStruct;

#include "M3DKernel/datascheme/BeamDataObject.h"
#include "M3DKernel/datascheme/PhaseDataObject.h"
#include "M3DKernel/datascheme/FanMemory.h"
#include "M3DKernel/datascheme/MemorySet.h"
#include "M3DKernel/datascheme/DatedObject.h"
#include "M3DKernel/datascheme/MemoryObject.h"
#include "M3DKernel/datascheme/SpectralAnalysisDataObject.h"

#include "BaseMathLib/RotationMatrix.h"
#include "BaseMathLib/Vector3.h"

#include <complex>

class QuadrantDataObject
{
public:
	// Samples for each quadrants
	std::vector < std::vector < std::complex<float> >> samples;
};
typedef	 std::map<unsigned short, BeamDataObject	*> MapBeamDataObject;
typedef	 std::map<unsigned short, PhaseDataObject	*> MapPhaseDataObject;
typedef	 std::map<unsigned short, SpectralAnalysisDataObject*> MapSpectralAnalysisDataObject;
typedef  std::map<unsigned short, QuadrantDataObject*> MapQuadrantDataObject;

class M3DKERNEL_API ComputePingFan
{
public:
	ComputePingFan() { m_maxRange = 0; m_maxRangeFound = false; }

	std::uint64_t 			m_pingId;
	std::uint32_t					m_filePingId; // OTK - 02/03/2009 - ajout du pingID fichier pour GOTO.
//	std::uint32_t 			m_hMinSec;

	std::int32_t			m_maxRange; // Detected bottom Range (Pr�cision 0.001 m)
	std::int32_t			m_minRange; // Detected bottom Range (Pr�cision 0.001 m)
	bool			m_maxRangeFound;

	void getBottomMaxRangeMeter(double &fond, bool &found) const
	{
		std::int32_t fondL;
		getBottomMaxRangeAsLong(fondL, found);
		fond = fondL*0.001;
	}

	void getBottomMaxRangeAsLong(std::int32_t &fond, bool &found) const
	{
		fond = m_maxRange;
		found = m_maxRangeFound;
	}
};

// data relative to the others pings
class M3DKERNEL_API ComputeRelativePingFan
{
public:
	ComputeRelativePingFan() { m_cumulatedNav.x = m_cumulatedNav.y = m_cumulatedNav.z = 0; };

	BaseMathLib::Vector3D m_cumulatedNav;
	double m_cumulatedDistance;
};

class M3DKERNEL_API PingFan : public DatedObject
{
public:
	MovCreateMacro(PingFan);

	void OnBeamChange();

	inline Sounder*	getSounderRef() const { return m_pSounder; };
	void		SetSounderRef(Sounder	*pSounder);

	FanMemory*	GetFanMemoryRef() { return m_pFanMemory; };
    void		SetFanMemoryRef(FanMemory* p) { MovUnRefDelete(m_pFanMemory); m_pFanMemory = p; MovRef(p); };

	inline MemorySet*	GetMemorySetRef() { return m_pMemorySet; };
    void		SetMemorySetRef(MemorySet* p) { MovUnRefDelete(m_pMemorySet); m_pMemorySet = p; MovRef(p); };
	
	NavAttitude* GetNavAttitudeRef(unsigned short channelId);

	NavAttributes* GetNavAttributesRef(unsigned short channelId);

	NavPosition* GetNavPositionRef(unsigned short channelId);
	
	void AddBeamDataObject(BeamDataObject*);
	void AddPhaseDataObject(PhaseDataObject*);
	void AddSpectralAnalysisDataObject(unsigned short chanId, SpectralAnalysisDataObject*);
	void AddQuadrantDataObject(unsigned short chanId, QuadrantDataObject*);

	BeamDataObject* getRefBeamDataObject(const unsigned short chanId, const unsigned short virtualChanId);
	PhaseDataObject* getRefPhaseDataObject(const unsigned short chanId, const unsigned short virtualChanId);
	SpectralAnalysisDataObject * getSpectralAnalysisDataObject(const unsigned short chanId);
	QuadrantDataObject * getQuadrantDataObject(const unsigned short chanId);
	
	void CopyData(MemoryStruct*, Transducer *);
	void ClearData();
	
	std::uint64_t GetPingId() { return m_computePingFan.m_pingId; }
	std::uint32_t GetFilePingId() { return m_computePingFan.m_filePingId; }

	void getBottom(const unsigned short chanId, std::uint32_t	&m_echoFondEval, std::int32_t &bottomRange, bool &found);
	// OTK - FAE050 - pour modification du fond d�tect� par le module de d�tection du fond interne � M3D
	void setBottom(const unsigned short chanId, std::int32_t bottomRange, bool found);
	std::map<int, double> getAverageNoiseLevel(const unsigned short chanId);

	int getNoiseEchoNum(const unsigned short chanId, bool &found);
	int getRefNoiseEchoNum(const unsigned short chanId, bool &found);

	void setDeviation(const unsigned short chanId, double deviation);
	void getDeviation(const unsigned short chanId, double &deviation, bool &found);

	void computeRanges();
	double getHeaveChan(const unsigned short chanId);
	double getHeaveChanMovies(const unsigned short chanId);

    BaseMathLib::Matrix3&		GetMatrixNav(unsigned short channelId);
    BaseMathLib::Matrix3&		GetHeadingMatrixNav(unsigned short channelId);
    BaseMathLib::Vector3D&		GetTranslationNav(unsigned short channelId);

	PingFan* getPreviousPingFan();
	double getPingDistance();

	// NMD - FAE 111, ajout d'une fonction permettant de recuperer un faisceau en fonction du chanId (virtuel ou non)
	// Si le faisceau n'est pas trouv�  dans la map d'association m_BeamDataObject, on recherhe alors au niveau du sondeur
	MapBeamDataObject::iterator getBeam(const unsigned short chanId);
	inline const MapBeamDataObject & getBeams() const { return m_BeamDataObject; }

	MapPhaseDataObject::iterator getPhase(const unsigned short chanId);
	inline const MapPhaseDataObject & getPhases() const { return m_PhaseDataObject; }
	
	// Renvoie l'identifiant du Channel le plus t�t dans le temps.
	unsigned short GetFirstChannelId() { return m_pFirstChannelId; }
	std::vector<unsigned short> GetChannelIds () const { return m_pChannelIds; }

protected:
	PingFan();
	virtual void	Compute(Traverser &e);
	virtual void	OnComplete(Traverser &e);

	void computeHeave();

private:
	MapBeamDataObject	m_BeamDataObject;
	MapPhaseDataObject	m_PhaseDataObject;
	MapSpectralAnalysisDataObject m_SpectralAnalysisDataObject;
	MapQuadrantDataObject m_QuadrantDataObject;
	
	void SetNavPositionRef(unsigned short channelId, NavPosition * pNavPosition);
	void SetNavAttitudeRef(unsigned short channelId, NavAttitude * pNavAttitude);
	void SetNavAttributesRef(unsigned short channelId, NavAttributes * pNavAttributes);

	std::map<unsigned short, BaseMathLib::Matrix3> m_MatrixNavs;
	std::map<unsigned short, BaseMathLib::Matrix3> m_HeadingMatrixNavs;
	std::map<unsigned short, BaseMathLib::Vector3D> m_TranslationNavs;

	std::map<unsigned short, NavPosition*> m_pNavPositions;
	std::map<unsigned short, NavAttributes*> m_pNavAttributes;
	std::map<unsigned short, NavAttitude*> m_pNavAttitudes;
	unsigned short m_pFirstChannelId;
	HacTime m_pFirstChannelDate;
	std::vector<unsigned short> m_pChannelIds;

public:
	virtual ~PingFan();
	void			ComputeMatrix();
	void			ComputeMatrixHeading();

	Sounder		*m_pSounder;
	FanMemory	*m_pFanMemory;
	MemorySet	*m_pMemorySet;

	ComputePingFan m_computePingFan;
	ComputeRelativePingFan m_relativePingFan;
};

#endif //MOV_HAC_PINGFAN
