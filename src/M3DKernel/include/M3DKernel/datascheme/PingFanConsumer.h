// -*- C++ -*-
// ****************************************************************************
// Class: PingFanConsumer
//
// Description: Interface permettant au noyau d'attendre que les algorithmes
// de traitement asynchrone sur les pings (comme la r�ponse fr�quentielle)
// aient termin� d'utiliser un pingfan avant de le supprimer.
//
// le PingFanConsumer peut au choix faire attendre le noyau, ou bien abandonner
// son traitement avant de laisser le noyau supprimer le PingFan.
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Octobre 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "M3DKernel/M3DKernelExport.h"

#include "M3DKernel/utils/multithread/Monitor.h"

#include <cstdint>

// ***************************************************************************
// Declarations
// ***************************************************************************

class M3DKERNEL_API PingFanConsumer
{
public:
	// *********************************************************************
  // Constructeurs / Destructeur
  // *********************************************************************
	// Constructeur par d�faut
	PingFanConsumer();

	// Destructeur
	virtual ~PingFanConsumer(void);

	virtual void WaitBeforeDeletingPing(std::uint64_t pingId) = 0;

	virtual void SetLastComputedPing(std::uint64_t pingId) { m_LastComputedPing = pingId; };
	virtual std::uint64_t GetLastComputedPing() { return m_LastComputedPing; };

	virtual CMonitor* GetMonitor() { return &m_PingFanConsumerSync; };

protected:
	// Moniteur permettant la synchronisation de la m�thode WaitBeforeDeletingPing
	// entre le thread des traitements synchrone et le thread du traitement asynchrone
	CMonitor  m_PingFanConsumerSync;

private:
	// ping jusqu'auquel on peut d�truire les donn�es sans gener le pingfanconsumer.
	std::uint64_t                m_LastComputedPing;
};
