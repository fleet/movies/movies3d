/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		PingFanContainer.h												  */
/******************************************************************************/

#pragma once

#include "M3DKernel/datascheme/CurrentSounderDefinition.h"

#include "M3DKernel/datascheme/TimeObjectContainer.h"
#include "M3DKernel/datascheme/HacObject.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/PingFanSingleTarget.h"
#include "M3DKernel/datascheme/ComputedNavigation.h"
#include "M3DKernel/datascheme/SounderIndexedTimeContainer.h"
#include "M3DKernel/datascheme/PingFanConsumer.h"

#include "M3DKernel/utils/multithread/Monitor.h"

// OTK - 15/01/2010 - un maxrange pour chaque sondeur
typedef	 std::map<std::uint32_t, float> MapMaxRange;
class M3DKERNEL_API PingFanContainer
{
public:

	/** get a ping Fan given its fanId*/
	PingFan* GetPingFanWithId(std::uint64_t fanId, bool bWarn = true);

	size_t GetNbFan(void) const { return m_pingFanContainer->GetObjectCount(); };
	SounderIndexedTimeContainer* GetPingFanContainer() { return m_pingFanContainer; };
	SounderIndexedTimeContainer* GetSingleTargetContainer() { return m_singleTargetContainer; };

	/** get a FanIndex given its id , return false if not found*/
	bool FindFanIndexWithId(std::uint64_t fanId, unsigned int &refIndex, bool bWarn);
	
	void CheckReaderParameter();
	float GetMaxRangeUsed(); // range max tous sondeurs confondus
	float GetMaxRangeUsed(std::uint32_t sounderID, TimeObjectContainer *pTimed); // range max par sondeur
	unsigned int GetMaxEchoNbUsed(std::uint32_t sounderID); // range max en nb d'�chos par sondeur
	void RemoveAllFan();

	// NMD - 10/04/2011 - Ajout de la fonction pour supprimer les pings associ�s � un sondeur
	void RemoveAllFan(std::uint32_t sounderId);

	// NMD - FAE 114 - 19/03/2012 - Suppression d'un ping par son identifiant
	bool RemovePing(unsigned int pingId);
	
	// Remove all single targets
	void RemoveAllSingleTargets();

	PingFanSingleTarget* GetPingFanSingleTarget(const HacTime &ref, std::uint32_t sounderId) const;
	void PushSingleTarget(SingleTargetDataObject *pTarget);
	void AddPingFan(PingFan *pFan);

	CurrentSounderDefinition m_sounderDefinition;

	void AddPingFanConsumer(PingFanConsumer* consumer);
	void RemovePingFanConsumer(PingFanConsumer* consumer);

	// NMD - FE 94 - Ajout d'une fonction de mise a jour du sondeur de r�f�rence pour les pings
	void UpdateRefSounder(Sounder * pNewSounder);

private:
	void WaitForPingFanConsumers(std::uint64_t pingID, bool kernelLocked = true);
	std::vector<PingFanConsumer*> m_PingFanConsumers;
	CMonitor  m_PingFanConsumersMonitor;

	ComputedNavigation m_computedNavigation;

	SounderIndexedTimeContainer			*m_pingFanContainer;
	SounderIndexedTimeContainer			*m_singleTargetContainer;


	// OTK - 15/01/2010 - 1 rangemax par sondeur
	//float m_maxRangeUsed;
	MapMaxRange m_maxRangeUsed;

	std::uint64_t m_nextInternalpingId;

	/// length
	unsigned int m_nbRecord;
	
public:
	virtual ~PingFanContainer();
	PingFanContainer();

private:

	// Disable copy
	PingFanContainer(const PingFanContainer & other) {}
	PingFanContainer & operator=(const PingFanContainer & other) { return *this; }
};




