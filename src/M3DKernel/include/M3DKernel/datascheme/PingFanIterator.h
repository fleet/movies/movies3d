/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	F.RACAPE													  */
/*	File:		PingFanIterator.h											  */
/******************************************************************************/

#pragma once

#include "M3DKernel/M3DKernelExport.h"
#include <cstdint>

class PingFan;
class PingFanContainer;

class M3DKERNEL_API PingFanIterator
{
public:

	PingFanIterator(void);
	virtual ~PingFanIterator();

	/** Methods */

	//initialisation of iterator (forward)
	void Init(bool m_bUsePingId	/** Use of ping Id instead of ping Index */,
        std::int64_t m_ulMinRange /** iterator range min */,
        std::int64_t m_ulMaxRange /** iterator range max */);

	//start forward iterator
	void Begin();
	//check for forward iterator ending
	bool End();

	//start backward iterator
	void RBegin();
	//check for backward iterator ending
	bool REnd();

	PingFanIterator& operator++();
	PingFan* operator*();



private:

	/** Members */

	/** Use of ping Id instead of ping Index */
	bool m_bUsePingId;

	/** iterator range min */
	std::int64_t m_ulMinRange;

	/** iterator range max */
	std::int64_t m_ulMaxRange;

	/** iterator position */
	std::int64_t m_ulPos;

	/** iterator increment */
	int m_inc;
public:


};




