#pragma once
#include "M3DKernel/datascheme/DatedObject.h"
#include "M3DKernel/datascheme/SingleTargetDataObject.h"

#include <vector>

class M3DKERNEL_API PingFanSingleTarget : public DatedObject
{
	PingFanSingleTarget(void);
	virtual ~PingFanSingleTarget(void);
public:
	MovCreateMacro(PingFanSingleTarget);

	std::uint32_t	m_pingNumber;

	void AddTarget(SingleTargetDataObject*);
	unsigned int GetNumberOfTarget();
	SingleTargetDataObject* GetTarget(unsigned int targetNum);

private:
	std::vector<SingleTargetDataObject*> m_targetList;

};