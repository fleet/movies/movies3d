/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		Platform.h												  */
/******************************************************************************/

#ifndef MOV_HAC_PLATFORM
#define MOV_HAC_PLATFORM

#include "M3DKernel/datascheme/HacObject.h"
#include "BaseMathLib/Vector3.h"

class M3DKERNEL_API Platform : public HacObject
{
public:
	MovCreateMacro(Platform);

	// OTK - FAE067 - op�rateurs de recopie des sondeurs
	virtual void Copy(const Platform &other);

protected:
	Platform();

public:
	void SetAttitudeOffset(BaseMathLib::Vector3D aOffset) { m_Offset = aOffset; DataChanged(); };

	double GetAlongShipOffset() { return m_Offset.x; }
	double GetAthwartShipOffset() { return m_Offset.y; }
	double GetDephtOffset() { return m_Offset.z; }

	BaseMathLib::Vector3D& GetOffset() { return m_Offset; }

	unsigned short	m_attitudeSensorId;
	std::uint32_t	m_timeCpu;
	unsigned short	m_timeFrac;
	unsigned short	m_platformType;

	bool IsEqual(const Platform &B) const;

protected:
	BaseMathLib::Vector3D m_Offset;

public:
	virtual ~Platform();
};

#endif //MOV_HAC_PLATFORM
