#pragma once

#include "M3DKernel/base/MovObject.h"

#include <complex>
#include <vector>

class M3DKERNEL_API SignalFilteringObject : public MovObject
{
public:
	MovCreateMacro(SignalFilteringObject);
	virtual ~SignalFilteringObject();

	virtual void Copy(const SignalFilteringObject & other);

	bool IsEqual(const SignalFilteringObject &B) const;

	unsigned short m_FilterID;
	unsigned short m_DecimationFactor;
	std::vector<std::complex<float>> m_Coefficients;
	
protected:
	SignalFilteringObject();
};
