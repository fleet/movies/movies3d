#pragma once

#include "M3DKernel/M3DKernelExport.h"

#include "BaseMathLib/Vector3.h"
#include <vector>

class M3DKERNEL_API SingleTargetData
{
public:
	double m_targetRange;
	std::uint64_t m_trackLabel = 0;
	double m_AlongShipAngleRad;
	double m_AthwartShipAngleRad;

	// Return target position in beam coordinate system
	BaseMathLib::Vector3D GetTargetPositionSoftChannel() const;
};

class M3DKERNEL_API SingleTargetDataCW : public SingleTargetData
{
public:
	double m_freq = -1;
	double m_compensatedTS;
	double m_unCompensatedTS;
};

class M3DKERNEL_API SingleTargetDataFM : public SingleTargetData
{
public:
	std::vector<double> m_freq;
	std::vector<double> m_compensatedTS;
	std::vector<double> m_unCompensatedTS;
	double m_medianTS;
};