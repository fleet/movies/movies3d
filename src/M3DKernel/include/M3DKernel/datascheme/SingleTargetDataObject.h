#pragma once
#include "M3DKernel/base/MovObject.h"
#include "M3DKernel/datascheme/DatedObject.h"
#include "M3DKernel/datascheme/SingleTargetData.h"
#include "BaseMathLib/Vector3.h"

#include <vector>
class SoftChannel;
class Sounder;
class PingFan;
class M3DKERNEL_API SingleTargetDataObject : public MovObject
{
	SingleTargetDataObject(void);
	virtual ~SingleTargetDataObject(void);

public:
	MovCreateMacro(SingleTargetDataObject);

	HacTime			m_PingTime;
	unsigned short  m_parentSTId;
	std::uint32_t	m_pingNumber;
	double			m_selectStartRange;
	double			m_selectEndRange;
	double			m_detectedBottom;
	std::int32_t			m_tupleAttributes;

	/// Return target position in beam coordinate system
	BaseMathLib::Vector3D GetTargetPositionSoftChannelCoord(unsigned int numTarget) const;
	
	SingleTargetData & GetSingleTargetData(unsigned int i);
	SingleTargetDataCW& GetSingleTargetDataCW(unsigned int i);
	SingleTargetDataFM& GetSingleTargetDataFM(unsigned int i);

	unsigned int	  GetSingleTargetDataCount() const;
	unsigned int	  GetSingleTargetDataCWCount() const;
	unsigned int	  GetSingleTargetDataFMCount() const;

	void			  PushSingleTargetDataCW(SingleTargetDataCW &ref);
	void			  PushSingleTargetDataFM(SingleTargetDataFM &ref);
	
	void SetSounderRef(Sounder *pSounder);
	Sounder *GetSounderRef() { return m_pSounder; }

private:
	Sounder *m_pSounder;
	std::vector<SingleTargetDataCW> m_targetListCW;
	std::vector<SingleTargetDataFM> m_targetListFM;

};