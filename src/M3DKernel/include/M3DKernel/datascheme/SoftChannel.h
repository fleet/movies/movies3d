/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		SoftChannel.h												  */
/******************************************************************************/

#ifndef MOV_HAC_SOFTCHANNEL
#define MOV_HAC_SOFTCHANNEL

#include "M3DKernel/datascheme/HacObject.h"
#include "M3DKernel/datascheme/Platform.h"
#include "M3DKernel/datascheme/Threshold.h"
#include "M3DKernel/datascheme/BeamWeight.h"
#include "BaseMathLib/RotationMatrix.h"
#include "M3DKernel/datascheme/SplitBeamParameter.h"
#include "M3DKernel/datascheme/HacTupleDef.h"

#include <map>

class TransmitSignalObject;
class Transducer;
class SoftChannel;

class M3DKERNEL_API SoftChannelData : public HacObject
{
protected:
	HacTupleDef *m_pRefTupleDef;

public:
	SoftChannelData();
	virtual ~SoftChannelData();

	void SetTupleUserData(HacTupleDef *pRefTupleDef);
	HacTupleDef* GetTupleUserData();

	unsigned short	m_tupleType;
	unsigned short	m_softChannelId;
	char			m_channelName[48];
	char			m_transNameShort[30];
	unsigned short	m_dataType;
	unsigned short	m_beamType;
	std::uint32_t	m_acousticFrequency;
	std::uint32_t	m_startFrequency;
	std::uint32_t	m_endFrequency;
	std::uint32_t	m_startSample;
	double			m_mainBeamAlongSteeringAngleRad;
	double			m_mainBeamAthwartSteeringAngleRad;
	std::uint32_t	m_absorptionCoef;
	std::uint32_t	m_absorptionCoefHAC;
	std::uint32_t	m_transmissionPower;
	double			m_beamAlongAngleSensitivity;
	double			m_beamAthwartAngleSensitivity;
	double			m_beam3dBWidthAlongRad;
	double			m_beam3dBWidthAthwartRad;
	double			m_beamEquTwoWayAngle;
	double			m_beamGain;
	double			m_beamSACorrection;
	double			m_bottomDetectionMinDepth;
	double			m_bottomDetectionMaxDepth;
	double			m_bottomDetectionMinLevel;
	unsigned short	m_AlongTXRXWeightId;
	unsigned short	m_AthwartTXRXWeightId;
	unsigned short	m_SplitBeamAlongTXRXWeightId;
	unsigned short	m_SplitBeamAthwartTXRXWeightId;
	std::uint32_t	m_bandWidth;

	// OTK - 01/06/2009 - flag permettant de savoir si des valeurs ont �t� renseign�es par d�faut
	// afin de le prendre en compte au moment de la r�-�criture du fichier HAC
	bool m_bDefaultAlongAngleOffset;
	bool m_bDefaultAthwartAngleOffset;
	
	// NMD
	double			m_calibMainBeamAlongSteeringAngleRad;
	double			m_calibMainBeamAthwartSteeringAngleRad;
	double			m_calibBeam3dBWidthAlongRad;
	double			m_calibBeam3dBWidthAthwartRad;
	double			m_calibBeamGain;
	double			m_calibBeamSACorrection;
};

class M3DKERNEL_API SoftChannelComputeData
{
public:
	SoftChannelComputeData();
	virtual ~SoftChannelComputeData();

	unsigned int	m_PolarId;
	bool			m_isReferenceBeam;
	short			m_groupId;
	bool			m_isMultiBeam;

	void InitTransmitSignalObject(Transducer* transducer, SoftChannel* softChannel, const std::vector<float>& signalReal = {}, const std::vector<float>& signalImag = {});

	TransmitSignalObject * GetTransmitSignalObject(Transducer * pTrans, SoftChannel * pSoftChan);

private:
	TransmitSignalObject * m_pTransmitSignalObject;
};

typedef	std::map<unsigned short, BeamWeight *> MapBeamWeight;

class M3DKERNEL_API SoftChannel : public SoftChannelData
{
public:
	MovCreateMacro(SoftChannel);
	virtual ~SoftChannel();

	Threshold*	GetThreshold() { return m_pThreshold; }
	void		SetThreshold(Threshold*p);

	SplitBeamParameter*	GetSplitBeamParameter() { return m_pSplit; }
	void		SetSplitBeamParameter(SplitBeamParameter*p);

	MapBeamWeight&	GetBeamWeights();
	void		SetBeamWeight(unsigned short wID, BeamWeight*p);

	unsigned short getSoftwareChannelId() { return m_softChannelId; }

	void setSoftwareVirtualChannelId(unsigned short virtualId) { m_virtualSoftChannelId = virtualId; }
	unsigned short getSoftwareVirtualChannelId() { return m_virtualSoftChannelId; }

	SoftChannelComputeData m_softChannelComputeData;

	inline BaseMathLib::Matrix3&	GetMatrixSoftChan() { return m_matrixSoftChan; };

	void ComputeMatrix();

	bool IsEqual(const SoftChannel &B) const;

protected:
	SoftChannel();
	virtual void Compute(Traverser &e);
	virtual void OnComplete(Traverser &e);
	virtual void UpdateChildMember(Traverser &e);

	Threshold * m_pThreshold;
	SplitBeamParameter * m_pSplit;
	
	// OTK - 26/05/2009 - ajout des parametres du tuple 2210
	MapBeamWeight m_BeamWeights;

	BaseMathLib::Matrix3 m_matrixSoftChan;

	// NMD - ajout de l'id du canal virtuel associ�
	unsigned int m_virtualSoftChannelId;
};

class M3DKERNEL_API SoftChannelMulti : public SoftChannel
{
public:
	MovCreateMacro(SoftChannelMulti);

protected:
	SoftChannelMulti();
};

#endif //MOV_HAC_SOFTCHANNEL
