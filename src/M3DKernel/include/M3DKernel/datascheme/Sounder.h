/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		Sounder.h												  */
/******************************************************************************/

#ifndef MOV_HAC_SOUNDER
#define MOV_HAC_SOUNDER

#include <vector>

#include "M3DKernel/datascheme/HacObject.h"
#include "BaseMathLib/Vector3.h"
#include "BaseMathLib/Box.h"
#include "M3DKernel/datascheme/TimeElapse.h"
#include "M3DKernel/utils/overloadable.h"

class Transducer;
class SoftChannel;
class SingleTargetDataObject;
class TransformSet;
class BeamDataObject;
class PhaseDataObject;
class SpectralAnalysisDataObject;
class QuadrantDataObject;

class M3DKERNEL_API SounderData : public HacObject
{
public:
	// OTK - FAE067 - op�rateurs de recopie des sondeurs
	virtual void Copy(const SounderData &other)
	{
		m_tupleType = other.m_tupleType;
		m_SounderId = other.m_SounderId;
		m_soundVelocity = other.m_soundVelocity;
		m_triggerMode = other.m_triggerMode;
		m_numberOfTransducer = other.m_numberOfTransducer;
		m_pingInterval = other.m_pingInterval;
		m_isMultiBeam = other.m_isMultiBeam;
		m_computedHermesVersion = -1;
		m_heaveCM = false;
	}

	virtual SounderData &operator= (const SounderData &other)
	{
		Copy(other);
		return *this;
	}

	virtual void SetRemarks(const char * remarks);
	virtual int GetHermesVersionFromRemarks();
	virtual bool IsHeaveCM();

	virtual const char * GetRemarks();

	std::uint32_t m_SounderId;
	
	/// Vitesse du son 
	overloadable<double>  m_soundVelocity;

	/// Nombre de canaux logiciels associ�s au sondeur 
	unsigned short m_triggerMode;
	unsigned short m_numberOfTransducer;
	double m_pingInterval;
	bool m_isMultiBeam;
	unsigned short m_tupleType;

protected:
	char m_remarks[40];
	int m_computedHermesVersion;
	bool m_heaveCM;
};

class M3DKERNEL_API SounderComputeData
{
public:
	SounderComputeData() { m_nbBeamPerFan = 0; }

	// OTK - FAE067 - op�rateurs de recopie des sondeurs
	virtual void Copy(const SounderComputeData &other)
	{
		m_nbBeamPerFan = other.m_nbBeamPerFan;
		m_timeShift = other.m_timeShift;
	}

	virtual SounderComputeData &operator= (const SounderComputeData &other)
	{
		Copy(other);
		return *this;
	}

	unsigned int m_nbBeamPerFan;

	TimeElapse m_timeShift;
};

class M3DKERNEL_API Sounder : public SounderData
{
public:
	MovCreateMacro(Sounder);

	// OTK - FAE067 - op�rateurs de recopie des sondeurs
	virtual Sounder * Clone() const;
	virtual void Copy(const Sounder &other, bool copyTransducers = true);
	virtual Sounder &operator= (const Sounder &other)
	{
		Copy(other);
		return *this;
	}

	void addTransducer(Transducer *p);
	Transducer *GetTransducer(unsigned int idx) const;
	unsigned int GetTransducerCount() const;

	SoftChannel* GetSoftChannelByIndex(const size_t& index) const;
	size_t GetSoftChannelCount() const;

	const std::vector<Transducer*>& GetAllTransducers() const;

	// NMD - FAE 105 - recherche d'un transducteur par son nom
	Transducer*	 GetTransducerWithName(const char * transName) const;

	SounderComputeData& getSounderComputeData() { return m_sounderComputeData; };

	virtual SoftChannel* getSoftChannelPolarX(unsigned int polarId);
	virtual SoftChannel* getSoftChannel(unsigned short channelId);
	virtual unsigned short getTransducerIndexForChannel(unsigned short channelId) const;
	virtual Transducer*	 getTransducerForChannel(unsigned short channelId) const;

	void AddBeamDataObject(BeamDataObject * beamData
		, PhaseDataObject * phaseData = nullptr
		, SpectralAnalysisDataObject * spectralAnalysisDataObject = nullptr
		, QuadrantDataObject * quadrantDataObject = nullptr);
	
	void AddPhaseDataObject(PhaseDataObject * phaseData);
	
	void ComputePostRead(Traverser &e, bool bLastClose);

	virtual bool IsSequencing();

	/// transformation methods

	/**
	* The purpose of this function is to
	* @parameters :	the polar coordonates
	* @return values : Vector3D the double coordonates in world
	*/
	virtual BaseMathLib::Box GetAxisAlignedBoundingBox(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, unsigned int depthPolar);

	/*
	* Return the distance of an echo with the transducer depth and the heave compensation
	*/
	virtual double GetEchoDistance(PingFan* pFan, unsigned int numTransducer, unsigned int softChannel, unsigned int depthPolar) const;

	/**
	* @parameters : Vector3I the polar coordonates
	* @return values : Vector3D the double coordonates in world
	*/
	virtual BaseMathLib::Vector3D GetPolarToWorldCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, unsigned int depthPolar);
	virtual BaseMathLib::Vector3D GetPolarToGeoCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, unsigned int depthPolar);
	virtual void GetPolarToGeoCoord(PingFan* pFan, unsigned int numTransducer, unsigned int softChannel, const std::vector<unsigned int> & depthPolar, std::vector<BaseMathLib::Vector3D> & res);
	virtual BaseMathLib::Vector3D GetPolarToCartesianCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, unsigned int depthPolar);

	/**
	Transform point in channel coordinate system to worldCoordinate
	*/
	virtual BaseMathLib::Vector3D GetSoftChannelCoordinateToWorldCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, const BaseMathLib::Vector3D &refCoord);
	virtual BaseMathLib::Vector3D GetSoftChannelCoordinateToGeoCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, const BaseMathLib::Vector3D &refCoord);
	virtual void GetSoftChannelCoordinateToGeoCoord(PingFan* pFan, unsigned int numTransducer, unsigned int softChannel, const std::vector<BaseMathLib::Vector3D> & refCoord, std::vector<BaseMathLib::Vector3D> & res);

	virtual BaseMathLib::Vector3D GetSoftChannelCoordinateToCartesianCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, const BaseMathLib::Vector3D &refCoord);

	//	virtual BaseMathLib::Vector2I GetPolarToScreenCoord( BaseMathLib::Vector2I coord);
	virtual BaseMathLib::Vector3D GetSoftChannelCoordinateToWorldCoord(Transducer *transducer, SoftChannel *pSoft, PingFan *pFan, const BaseMathLib::Vector3D &refCoord);
	virtual BaseMathLib::Vector3D GetSoftChannelCoordinateToGeoCoord(Transducer *transducer, SoftChannel *pSoft, PingFan *pFan, const BaseMathLib::Vector3D &refCoord);
	virtual void GetSoftChannelCoordinateToGeoCoord(Transducer* transducer, SoftChannel* pSoft, PingFan* pFan, const std::vector<BaseMathLib::Vector3D> & refCoord, std::vector<BaseMathLib::Vector3D> & res);
	
	virtual BaseMathLib::Vector3D GetSoftChannelCoordinateToCartesianCoord(Transducer *transducer, SoftChannel *pSoft, PingFan *pFan, const BaseMathLib::Vector3D &refCoord);

public:

	virtual void GetBeamOrientation(PingFan *pFan, double &Roll, double &Pitch, unsigned int numBeam);

	/**
	* Compute Matrix transformation for all sounder, transducer, channel
	*/
	void ComputeMatrix();

	bool IsEqual(const Sounder &SounderB) const;

protected:
	Sounder();
	virtual void Compute(Traverser &e);
	virtual void UpdateChildMember(Traverser &e);
	virtual void OnComplete(Traverser &e);
	virtual void FinalizePingFanProcessing(Traverser &e, PingFan * pingFan);
	
	PingFan * GetWorkPingFan();

private:
	std::vector<Transducer*> m_transducerList;
	// OTK - FAE065 - on associe les tables de transformation � un objet sondeur.
	// on porura avori donc plusieurs tables de transformation pour une m�me zone m�moire
	TransformSet			*m_pTransformSet;

public:

	// OTK - FAE065 - association des tables de transformation avec un objet sondeur
	TransformSet* GetTransformSetRef() { return m_pTransformSet; };

	virtual ~Sounder();

	PingFan					*m_pWorkingPingFan;
	PingFan					*m_pPendingPingFan; // OTK - FAE214 - lecture diff�r�e d'un ping pour bien avoir toutes les infos du ping process�
	PingFan                 *m_pIncompletePingFan;

	SounderComputeData m_sounderComputeData;
};

class M3DKERNEL_API SounderEk500 : public Sounder
{
public:
	MovCreateMacro(SounderEk500);
	// OTK - FAE067 - op�rateurs de recopie des sondeurs
	virtual Sounder * Clone() const;
	virtual void Copy(const SounderEk500 &other, bool copyTransducers = false)
	{
		Sounder::Copy(other, copyTransducers);

		pingMode = other.pingMode;
		transmitPower = other.transmitPower;
		noiseMargin = other.noiseMargin;
		sampleRange = other.sampleRange;
		superLayerType = other.superLayerType;
		superLayerNumber = other.superLayerNumber;
		superLayerRange = other.superLayerRange;
		superLayerStart = other.superLayerStart;
		superLayerMargin = other.superLayerMargin;
		superLayerThreshold = other.superLayerThreshold;
		ek500version = other.ek500version;
	}

protected:
	SounderEk500() {}

public:
	unsigned short  pingMode;
	unsigned short  transmitPower;
	unsigned short	noiseMargin;
	unsigned short  sampleRange;
	unsigned short	superLayerType;
	unsigned short	superLayerNumber;
	unsigned short	superLayerRange;
	std::int32_t			superLayerStart;
	unsigned short	superLayerMargin;
	unsigned short  superLayerThreshold;
	std::uint32_t   ek500version;
};

class M3DKERNEL_API SounderSBES : public Sounder
{
public:
	MovCreateMacro(SounderSBES);

	// OTK - FAE067 - op�rateurs de recopie des sondeurs
	virtual Sounder * Clone() const;
	virtual void Copy(const SounderSBES &other, bool copyTransducers = false)
	{
		Sounder::Copy(other, copyTransducers);
		m_pingMode = other.m_pingMode;
	}

	unsigned short  m_pingMode;

protected:	
	SounderSBES() {}
};

class M3DKERNEL_API SounderER60 : public SounderSBES
{
public:
	MovCreateMacro(SounderER60);

	virtual Sounder * Clone() const;

protected:
	SounderER60() {}
};

class M3DKERNEL_API SounderEK80 : public SounderSBES
{
public:
	MovCreateMacro(SounderEK80);

	virtual Sounder * Clone() const;

	virtual bool IsSequencing();

protected:
	SounderEK80() {}
};

class M3DKERNEL_API SounderGeneric : public Sounder
{
public:
	MovCreateMacro(SounderGeneric);

	// OTK - FAE067 - op�rateurs de recopie des sondeurs
	virtual Sounder * Clone() const;

	virtual void Copy(const SounderGeneric &other, bool copyTransducers = false)
	{
		Sounder::Copy(other, copyTransducers);
		m_IsSequencing = other.m_IsSequencing;
	}

	virtual SounderGeneric &operator= (const SounderGeneric &other)
	{
		Copy(other, false);
		return *this;
	}

	virtual bool IsSequencing() { return m_IsSequencing; }
	void setIsSequencing(bool isSequencing) { m_IsSequencing = isSequencing;  }

protected:
	SounderGeneric() { m_IsSequencing = false; }

	bool m_IsSequencing;
};

#endif //MOV_HAC_SOUNDER
