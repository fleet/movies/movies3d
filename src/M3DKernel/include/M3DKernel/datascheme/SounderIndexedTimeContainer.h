#pragma once
#include "M3DKernel/M3DKernelExport.h"
#include "TimeObjectContainer.h"

#include <map>

typedef	 std::map<std::uint32_t, TimeObjectContainer *> MapSounderIndexedContainer;

class M3DKERNEL_API SounderIndexedTimeContainer : public MovObject
{
public:
	MovCreateMacro(SounderIndexedTimeContainer);

	virtual ~SounderIndexedTimeContainer(void);

	DatedObject* GetObjectWithIndex(size_t idx);
	DatedObject*  GetObjectWithDate(HacTime fanTime);
	DatedObject* GetOldestObject();

	size_t GetObjectCount(void) const;

	TimeObjectContainer *GetSounderIndexedContainer(std::uint32_t sounderId);
	MapSounderIndexedContainer	&GetSounderIndexedMap() { return m_sounderIndexedContainer; };

	void AddObject(DatedObject *pObject, std::uint32_t sounderId);
	void RemoveObject(DatedObject *pObject, std::uint32_t sounderId);

	void RemoveAllObjects();

	void Sort();

protected:
	SounderIndexedTimeContainer(void);

	TimeObjectContainer			*m_allObjectContainer;
	MapSounderIndexedContainer	m_sounderIndexedContainer;
};
