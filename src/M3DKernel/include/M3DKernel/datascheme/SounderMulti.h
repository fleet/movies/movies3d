#pragma once
#include "M3DKernel/datascheme/Sounder.h"

class M3DKERNEL_API SounderMulti : public Sounder
{
public:
	MovCreateMacro(SounderMulti);
protected:
	SounderMulti();

public:
	virtual Sounder * Clone() const;

	virtual SoftChannel* getSoftChannel(unsigned short channelId);
	virtual SoftChannel* getSoftChannelPolarX(unsigned int polarId);
	virtual Transducer*	 getTransducerForChannel(unsigned short channelId);

	/// transformation methods see base class for definition

	//	virtual BaseMathLib::Vector2I GetPolarToScreenCoord( BaseMathLib::Vector2I coord);
#if DEPRECATED
	virtual BaseMathLib::Vector3D GetBaseTransducerToWorldCoord(PingFan *pFan, unsigned int numTransducer);
#endif
	virtual BaseMathLib::Vector3D GetSoftChannelCoordinateToWorldCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, const BaseMathLib::Vector3D &refCoord);
	virtual BaseMathLib::Vector3D GetSoftChannelCoordinateToGeoCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, const BaseMathLib::Vector3D &refCoord);
	virtual BaseMathLib::Vector3D GetSoftChannelCoordinateToGeoCoord(Transducer *pTrans, SoftChannel *pSoftChannel, PingFan *pFan, const BaseMathLib::Vector3D &refCoord);
	virtual void GetSoftChannelCoordinateToGeoCoord(Transducer* transducer, SoftChannel* pSoft, PingFan* pFan, const std::vector<BaseMathLib::Vector3D>& refCoord, std::vector<BaseMathLib::Vector3D>& res);

	virtual BaseMathLib::Vector3D GetSoftChannelCoordinateToWorldCoord(Transducer *pTrans, SoftChannel *pSoftChannel, PingFan *pFan, const BaseMathLib::Vector3D &refCoord);


	virtual void GetBeamOrientation(PingFan *pFan, double &Roll, double &Pitch, unsigned int numBeam);

};