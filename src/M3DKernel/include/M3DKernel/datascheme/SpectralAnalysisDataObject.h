#pragma once

#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "SpectralAnalysisParameters.h"

#include <vector>

class M3DKERNEL_API SpectralAnalysisDataObject
{
public:
	SpectralAnalysisDataObject() {}

	/**
	* \brief Sv values without gain.
	* \details The values are stored by range x frequency, i.e. sv(rangeIndex, freqIndex) = svValues[rangeIndex][freqIndex].
	*/
	std::vector<std::vector<double>> svValues;

	std::vector<double> svValuesForFrequencyIndex(int freqIndex) const;
	std::vector<double> svValuesForRangeIndex(int rangeIndex) const;

	double sv(int rangeIndex, int freqIndex) const;


	
	double firstFrequency = 0;		//In Hz
	double lastFrequency = 100;		//In Hz
	double frequencyInterval = 1;	//In Hz

	double firstRange = 0;			//In meters
	double lastRange = 100;			//In meters
	double rangeInterval = 1;		//In meters

	double frequencyForIndex(int freqIndex) const { return firstFrequency + frequencyInterval * freqIndex; }
	double rangeForIndex(int rangeIndex) const { return firstRange + rangeInterval * rangeIndex; }

	unsigned int numberOfRanges() const { return svValues.size(); }
	unsigned int numberOfFrequencies() const { return svValues.empty() ? 0 : svValues.front().size(); }
};

