#pragma once

#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/parameter/ParameterModule.h"

class M3DKERNEL_API SpectralAnalysisParameters : public BaseKernel::ParameterModule
{
public:

	SpectralAnalysisParameters();
	~SpectralAnalysisParameters();

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	void setPulseCompressionEnabled(bool value) { m_EK80_FM_PulseCompression = value; }
	bool isPulseCompressionEnabled() const { return m_EK80_FM_PulseCompression; }

	void setSpectralAnalysisEnabled(bool value) { m_spectralAnalysis = value; }
	bool isSpectralAnalysisEnabled() const { return m_spectralAnalysis; }
	
	void setFrequencyResolution(int value) { m_FrequencyResolution = value; }
	int getFrequencyResolution() const { return m_FrequencyResolution; }


private:
	bool			m_EK80_FM_PulseCompression;

	bool			m_spectralAnalysis;
	int				m_FrequencyResolution;		//hertz
};

