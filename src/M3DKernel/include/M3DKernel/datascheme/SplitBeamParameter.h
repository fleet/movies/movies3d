#pragma once
#include "M3DKernel/datascheme/HacObject.h"
#include "M3DKernel/datascheme/DateTime.h"
class M3DKERNEL_API SplitBeamParameter :
	public HacObject
{
public:
	MovCreateMacro(SplitBeamParameter);


	virtual ~SplitBeamParameter(void);

	HacTime	m_startLogTime;

	unsigned short m_splitBeamParameterChannelId;
	double m_minimumValue;
	double m_minimumEchoLength;
	double m_maximumEchoLenght;
	double m_maximumGainCompensation;
	double m_maximumPhaseCompensation;

	char   m_Remark[30];
	std::int32_t   m_attribute;

protected:
	SplitBeamParameter(void);
};
