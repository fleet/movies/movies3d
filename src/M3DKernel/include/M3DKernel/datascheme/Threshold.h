/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		Threshold.h												  */
/******************************************************************************/

#ifndef MOV_HAC_THRESHOLD
#define MOV_HAC_THRESHOLD

#include "DatedObject.h"

class M3DKERNEL_API Threshold : public DatedObject
{
public:
	MovCreateMacro(Threshold);

protected:
	Threshold();

public:

	unsigned short  m_SoftwareChannelID;
	double			m_TVGMaxRange;
	double			m_TVGMinRange;
	unsigned short  m_TVTEvaluationMode;
	unsigned short  m_TVTEvaluationInterval;
	unsigned short  m_TVTEvaluationNumberOfPing;
	std::uint32_t	m_TVTStartPingNumber;
	double			m_TVTParameter;
	double			m_TVTAmpParameter;
	std::int32_t			m_tupleAttributes;
	virtual ~Threshold();

	bool IsEqual(const Threshold &B) const
	{
		return m_SoftwareChannelID == B.m_SoftwareChannelID
			&& m_TVGMaxRange == B.m_TVGMaxRange
			&& m_TVGMinRange == B.m_TVGMinRange
			&& m_TVTEvaluationMode == B.m_TVTEvaluationMode
			&& m_TVTEvaluationInterval == B.m_TVTEvaluationInterval
			&& m_TVTEvaluationNumberOfPing == B.m_TVTEvaluationNumberOfPing
			&& m_TVTStartPingNumber == B.m_TVTStartPingNumber
			&& m_TVTParameter == B.m_TVTParameter
			&& m_TVTAmpParameter == B.m_TVTAmpParameter
			&& m_tupleAttributes == B.m_tupleAttributes;
	}

};

#endif //MOV_HAC_THRESHOLD