#pragma once
#include "M3DKernel/M3DKernelExport.h"
#include <cstdint>

class M3DKERNEL_API TimeElapse
{
public:
    TimeElapse(std::int64_t m_time);
	TimeElapse(void);
	virtual ~TimeElapse(void);

	bool operator==(const TimeElapse& v)
	{
		return v.m_timeElapse == m_timeElapse;
	}
	bool operator<(const TimeElapse& v)
	{
		return m_timeElapse < v.m_timeElapse;
	}
    bool operator>(const TimeElapse& v)
	{
		return m_timeElapse > v.m_timeElapse;
	}
	TimeElapse& operator=(const TimeElapse& v)
	{
		m_timeElapse = v.m_timeElapse;  return *this;
	}

public:
    std::int64_t m_timeElapse; /// time elapsed as 1/10000 s
};

TimeElapse M3DKERNEL_API operator+(const TimeElapse &v1, const TimeElapse &v2);
