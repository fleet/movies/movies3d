/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		Nav.h												  */
/******************************************************************************/

#ifndef TIME_OBJECT_CONTAINER
#define TIME_OBJECT_CONTAINER

#include "M3DKernel/M3DKernelExport.h"
#include <vector>
#include <deque>
#include <map>

#include "DatedObject.h"

/**
* TimeObjectContainer container for the dated object get trough the read process
* Note that object contained will be maintained in a sorted order, in respect with < operator
*
*/
class M3DKERNEL_API TimeObjectContainer : public MovObject
{
public:
	MovCreateMacro(TimeObjectContainer);
	virtual ~TimeObjectContainer();

protected:
	TimeObjectContainer();

public:
	/** Add an object at the right position	*/
	void AddObject(DatedObject *);

	/** This function will remove all object having their datetime stricly less than the one specified **/
	void RemoveOldObject(const HacTime &maxDate);

	/** This function will remove the given according to it's index*/
	void RemoveObject(unsigned int idx);

	/** This function will remove the given object */
	void RemoveObject(DatedObject *);

	/** Remove all objects	*/
	void RemoveAllObjects();

	/** Get an object given its index */
	inline DatedObject* GetDatedObject_unsafe(unsigned int idx) const { return m_mainContainer[idx]; }
	DatedObject* GetDatedObject(unsigned int idx) const;

	DatedObject* GetFirstDatedObject() const;
	DatedObject* GetLastDatedObject() const;
	DatedObject* GetOldestDatedObject() const;

	/** Get the number of object stored*/
	size_t GetObjectCount() const;

	/** Sort all store*/
	void Sort();
		
	// Find object with ObjectTime equal to seekDate 
	DatedObject * FindDatedObject(const HacTime &seekDate) const;

	// Find objects with ObjectTime equal to seekDate 
	std::vector<DatedObject *> FindDatedObjects(const HacTime &seekDate) const;

	// Find first objects in container with ObjectTime smaller than seekDate (lower_bound)
	DatedObject * FindPreviousDatedObject(const HacTime & seekDate) const;

	// Find first objects in container with ObjectTime smaller than seekDate (lower_bound)
	std::vector<DatedObject*> FindPreviousDatedObjects(const HacTime & seekDate) const;
	
	// Find first objects in container with ObjectTime greater than seekDate (upper_bound)
	DatedObject * FindNextDatedObject(const HacTime & seekDate) const;

	// Find first objects in container with ObjectTime greater than seekDate (upper_bound)
	std::vector<DatedObject*> FindNextDatedObjects(const HacTime & seekDate) const;

	// Find first objects in container with ObjectTime closest than seekDate
	DatedObject * FindClosestDatedObject(const HacTime &seekDate) const;

	// Find first objects in container with ObjectTime  closest than seekDate
	std::vector<DatedObject*> FindClosestDatedObjects(const HacTime & seekDate) const;
	
	// Get access to dated objects container
	const std::map<HacTime, std::vector<DatedObject *> > & datedContainer() const { return m_datedContainer; }

private:

	std::deque<DatedObject *> m_mainContainer;

	// Conteneur alternatif tri� par date pour acc�s rapide aux �l�ments par date
	std::map<HacTime, std::vector<DatedObject *> > m_datedContainer;
};

#endif //TIME_OBJECT_CONTAINER