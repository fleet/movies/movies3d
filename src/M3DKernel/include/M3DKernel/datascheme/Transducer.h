/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		Transducer.h												  */
/******************************************************************************/

#pragma once

#include "M3DKernel/datascheme/HacObject.h"
#include "Platform.h"
#include "BaseMathLib/Vector3.h"
#include "BaseMathLib/RotationMatrix.h"

#include <map>
#include <vector>

class SoftChannel;
class TransformMap;
class PingFan;
class SignalFilteringObject;

class M3DKERNEL_API TransData : public HacObject
{
public:
	// OTK - FAE067 - opérateurs de recopie des transducteurs
	void Copy(const TransData &other)
	{
		memcpy(m_transName, other.m_transName, sizeof(m_transName));
		memcpy(m_transSoftVersion, other.m_transSoftVersion, sizeof(m_transSoftVersion));
		m_pulseDuration = other.m_pulseDuration;
		m_pulseForm = other.m_pulseForm;
		m_pulseShape = other.m_pulseShape;
		m_pulseSlope = other.m_pulseSlope;
		m_timeSampleInterval = other.m_timeSampleInterval;
		m_frequencyBeamSpacing = other.m_frequencyBeamSpacing;
		m_frequencySpaceShape = other.m_frequencySpaceShape;
		m_transPower = other.m_transPower;
		m_transDepthMeter = other.m_transDepthMeter;
		m_platformId = other.m_platformId;
		m_transShape = other.m_transShape;
		m_transFaceAlongAngleOffsetRad = other.m_transFaceAlongAngleOffsetRad;
		m_transFaceAthwarAngleOffsetRad = other.m_transFaceAthwarAngleOffsetRad;
		m_transRotationAngleRad = other.m_transRotationAngleRad;
		m_numberOfSoftChannel = other.m_numberOfSoftChannel;
		m_bDefaultRotation = other.m_bDefaultRotation;
		m_bDefaultFaceAlongAngleOffset = other.m_bDefaultFaceAlongAngleOffset;
		m_bDefaultFaceAthwartAngleOffset = other.m_bDefaultFaceAthwartAngleOffset;

		m_bUseAngleCustomValues = other.m_bUseAngleCustomValues;
	}

	TransData &operator= (const TransData &other)
	{
		Copy(other);
		return *this;
	}

public:
	TransData();
	char m_transName[80];
	char m_transSoftVersion[30];
	std::uint32_t m_pulseDuration;
	unsigned short m_pulseShape; // EK80
	double m_pulseSlope;         // EK80
	unsigned short m_pulseForm;  // pulse form 
	double	 m_timeSampleInterval; // time sample interval micro seconds
	unsigned short m_frequencyBeamSpacing;
	unsigned short m_frequencySpaceShape;
	std::uint32_t m_transPower;
	double m_transDepthMeter;
	unsigned short  m_platformId;
	unsigned short m_transShape;
	double m_transFaceAlongAngleOffsetRad;
	double m_transFaceAthwarAngleOffsetRad;
	double m_transRotationAngleRad;
	unsigned int m_numberOfSoftChannel;

	std::uint32_t m_initialWBTSamplingFrequency;
	std::uint32_t m_transducerImpedance;
	std::uint32_t m_WBTImpedance;

	// OTK - 01/06/2009 - flag permettant de savoir si des valeurs ont été renseignées par défaut
	// afin de le prendre en compte au moment de la ré-écriture du fichier HAC
	bool m_bDefaultRotation;
	bool m_bDefaultFaceAlongAngleOffset;
	bool m_bDefaultFaceAthwartAngleOffset;

	// NMD - FE 096 - flag permettant de savoir si les angles du transducteur ont été écrasé par les "custom values"
	bool m_bUseAngleCustomValues;
};

typedef	 std::map<unsigned short, SoftChannel	*> MapSoftChannel;

class M3DKERNEL_API Transducer : public TransData
{
public:
	MovCreateMacro(Transducer)

	// OTK - FAE067 - opérateurs de recopie des sondeurs
	void Copy(const Transducer &other);
	Transducer &operator= (const Transducer &other)
	{
		Copy(other);
		return *this;
	}

	Platform * GetPlatform() const { return m_pPlatform; }
	void SetPlatform(Platform* platform);

	SignalFilteringObject * GetFPGAFilter() const { return m_pFPGAFilter; }
	void SetFPGAFilter(SignalFilteringObject* fpgaFilter);
	SignalFilteringObject *	GetApplicationFilter() const { return m_pApplicationFilter; }
	void SetApplicationFilter(SignalFilteringObject* applicationFilter);

	SoftChannel* getSoftChannel(unsigned short channelId) const;
	void AddSoftChannel(SoftChannel* softChannel);
	SoftChannel* getSoftChannelPolarX(unsigned int channelIndex) const;
	SoftChannel* getSoftChannelVertical() const;

	SoftChannel* getSoftChannelByName(const char * name) const;

	// NMD - FAE 105 - ajout d'un soft channel deja existant mais avec un autre id
	void AddVirtualSoftChannel(SoftChannel* softChannel, unsigned short virtualId);

	const std::vector<unsigned short> & GetChannelId() const;

protected:
	Transducer();
	void Compute(Traverser &traverser) override;
	void OnComplete(Traverser &traverser) override;
	void UpdateChildMember(Traverser &traverser) override;

public:
	~Transducer() override;
	void ComputeMatrix();

	double getBeamsSamplesSpacing() const { return m_beamsSamplesSpacing; }
	void setBeamSamplesSpacing(const double beamSamplesSpacing) { m_beamsSamplesSpacing = beamSamplesSpacing; }
	double computeSampleSpacing(double soundVelocity) const;

	// Translation due to internal delay of hardware, set as a number of sample spacing
	double GetInternalDelayTranslation() const { return m_internalDelayTranslation; }
	void SetInternalDelayTranslation(const double internalDelayTranslation) { m_internalDelayTranslation = internalDelayTranslation; }

	// Translation due to internal delay of hardware, set as a number of sample spacing
	double GetSampleOffset() const { return m_sampleOffset; }
	void SetSampleOffset(const double sampleOffset) { m_sampleOffset = sampleOffset; }

	const BaseMathLib::Vector3D & GetTransducerTranslation() const { return m_transducerTranslation; }
	const BaseMathLib::Matrix3 & GetTransducerRotation() const { return m_transducerRotation; }

	bool IsEqual(const Transducer &otherTransducer) const;

protected:
	Platform *m_pPlatform;

	double m_beamsSamplesSpacing;

	BaseMathLib::Vector3D m_transducerTranslation;
	BaseMathLib::Matrix3 m_transducerRotation;
	double m_internalDelayTranslation;
	double m_sampleOffset;

	SignalFilteringObject * m_pFPGAFilter;
	SignalFilteringObject * m_pApplicationFilter;

private:
	MapSoftChannel m_softChannel;

	// NMD - FAE 105 - stockage des soft channels en double avec un id différents
	MapSoftChannel m_virtualSoftChannel;

	/// contain a list of the ids 
	std::vector<unsigned short> m_idChannel;
};


