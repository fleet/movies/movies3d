#pragma once
#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/datascheme/Sounder.h"
#include <vector>

template <typename T> 
class M3DKERNEL_API TransducerIndexed
{
public:

	TransducerIndexed(void) {}

	~TransducerIndexed(void)
	{
		for (unsigned int i = 0; i < GetObjectCount(); i++)
		{
			T* ptr = GetObject(i);
			MovUnRefDelete(ptr);
		}
		m_ObjectList.clear();
	}
	
	inline T* GetObject(unsigned int index) { return m_ObjectList[index]; }
	
	size_t GetObjectCount() { return m_ObjectList.size(); };

	void SetSounder(Sounder *pSound)
	{
		ReAllocate(pSound->GetTransducerCount());
		for (size_t i = 0; i < GetObjectCount(); i++)
		{
			GetObject(i)->SetTransducer(pSound->GetTransducer(i));
		}
	}

private:
	
	void ReAllocate(unsigned int nbTransducer)
	{
		// ajouts �ventuels 
		while (m_ObjectList.size() < nbTransducer)
		{
			T* ptr = T::Create();
			m_ObjectList.push_back(ptr);
			MovRef(ptr);
		}
		// retraits �ventuels
		while (m_ObjectList.size() > nbTransducer)
		{
			MovUnRefDelete(m_ObjectList[0]);
			m_ObjectList.erase(m_ObjectList.begin());
		}
	}

	std::vector<T*> m_ObjectList;
};

