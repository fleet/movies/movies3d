/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		TransformMap.h												  */
/******************************************************************************/

#ifndef MOV_HAC_TRANSFORM
#define MOV_HAC_TRANSFORM

#include "M3DKernel/DefConstants.h"

#include "M3DKernel/datascheme/HacObject.h"
#include "BaseMathLib/Vector3.h"
//#include "UCharArray2D.h"
class Transducer;
class SoftChannel;
class M3DKERNEL_API TransformMap : public MovObject
{
public:
	MovCreateMacro(TransformMap);

protected:
	TransformMap();


public:

	void		Update();

	Transducer	*GetTransducer() { return m_pTransducer; };
	void		SetTransducer(Transducer	*);
	void CheckReaderParameter();

	/*
	* Check if parameters changed, and propagate downward if necessary
	*/
	void SetMaxRange(double m_range);


	virtual ~TransformMap();

public:

	void computeTable(void);
	bool posIsIn(BaseMathLib::Vector2I pos);

	int getIndex(BaseMathLib::Vector2I pos)
	{
		return *getPointerToIndex(pos);
	};
	int getIndex(unsigned int x, unsigned int y)
	{
		return *getPointerToIndex(x, y);
	}

	int getNbIndexes(void);
	int* getPointerToIndex(BaseMathLib::Vector2I pos);
	int* getPointerToIndex();
	int* getPointerToIndex(unsigned int x, unsigned int y);
	int* getPointerToChannelIndex();
	int* getPointerToChannelIndex(unsigned int x, unsigned int y);

	BaseMathLib::Vector2I getCartesianSize2(void);
	BaseMathLib::Vector2D getRealOrigin2(void);
	BaseMathLib::Vector2D getRealSize2(void);


	//	Coord. Convertion Methods
	//	*************************

	BaseMathLib::Vector2I polarToCartesian2(const BaseMathLib::Vector2I &p);
	BaseMathLib::Vector2D polarToReal2(const BaseMathLib::Vector2I &p);
	BaseMathLib::Vector2D cartesianToPolar2(const BaseMathLib::Vector2D &c);
	BaseMathLib::Vector2D cartesianToReal2(const BaseMathLib::Vector2D &c);
	inline double cartesianToRealY(double c) const { return m_p1.y + (c * m_fact.y); }
	inline double cartesianToRealX(double c) const { return m_p1.x + (c * m_fact.x); }
	BaseMathLib::Vector2D realToPolar2(const BaseMathLib::Vector2D &r);
	BaseMathLib::Vector2I realToCartesian2(const BaseMathLib::Vector2D &r);
	BaseMathLib::Vector2D realToCartesian2Double(const BaseMathLib::Vector2D &r);

	inline double realToCartesianY(double r) const { return (r - m_p1.y) / m_fact.y; }

	double getXSpacing(void);
	double getYSpacing(void);
	BaseMathLib::Vector2D getDelta();
	double calcFaisceau(double angl);
	int NumFaisceau(double angl);
	int FastNumFaisceau(double angl, SoftChannel ** tabChannel);

	// OTK - 23/03/2009 - passage en public pour modification sur redimentionnement
	void setXYSpacing(double newSpacingX, double newSpacingY);


	//	Shape Methods
	//	*************
	int getMinX(int y);
	int getMaxX(int y);
	int getMinY(int x);
	int getMaxY(int x);
	double getAngle1(void);
	double getAngle2(void);


private:
	Transducer*	m_pTransducer;



	void setPolarSize(BaseMathLib::Vector2I size);

	bool m_mustBeComputed;
	BaseMathLib::Vector2I m_polarSize;
	double m_xSpacing;
	double m_ySpacing;


	BaseMathLib::Vector2D m_p1;
	BaseMathLib::Vector2D m_p2;
	BaseMathLib::Vector2I m_tableSize;
	BaseMathLib::Vector2D m_invTableSize;

	BaseMathLib::Vector2D m_delta;
	BaseMathLib::Vector2D m_fact;


	int* m_indexes;
	// OTK - FAE018 - ajout du numero de faisceau pour chaque pixel (le fastnumfaisceau est 
	// encore trop lent pour un calcul du numero de faisceau de chaque pixel � chaque ping)
	int* m_channelIndexes;

	double getTotalAngle(void);
	double getRadius(void);


	void transformeLine2(
		unsigned char* dst,
		unsigned char* src,
		int * p,
		unsigned int sy,
		unsigned int dataSize,
		DataFmt outside);

};

#endif //MOV_HAC_TRANSFORM