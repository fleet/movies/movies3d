#pragma once
#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/base/MovObject.h"
#include "TransformMap.h"
#include "TransducerIndexed.h"

class M3DKERNEL_API TransformSet : public MovObject
{
public:
	MovCreateMacro(TransformSet);

	virtual ~TransformSet(void);

	TransformMap* GetTransformMap(unsigned int Index);

	void SetMaxRange(double m_range, unsigned int maxEchoNb);

	Sounder	*GetSounder() { return m_pSounder; };
	void		SetSounder(Sounder	*);

	void CheckReaderParameter();

private:
	TransducerIndexed<TransformMap> m_IndexedMap; /// ptr to a transducer indexed table of memoryStruct
	TransformSet(void);
	Sounder			*m_pSounder;
	double m_maxRange;
	unsigned int m_maxEchoNb;
};
