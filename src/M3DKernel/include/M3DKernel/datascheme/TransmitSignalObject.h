#pragma once

#include "M3DKernel/base/MovObject.h"

#include <complex>
#include <vector>

class Transducer;
class SoftChannel;

class M3DKERNEL_API TransmitSignalObject : public MovObject
{
public:
	MovCreateMacro(TransmitSignalObject);
	virtual ~TransmitSignalObject();

	void InitSignals(const std::vector<float>& signalReal, const std::vector<float>& signalImag);

	void Compute(Transducer * transducer, SoftChannel * channel, const bool& recomputeSignals = true);

	// Performs pulse compression in FM mode
	void PerformPulseCompression(std::vector<std::complex<float>> & complexSamples, float freqResolution, int nbQuadrants);

	std::vector<std::complex<float>> GetFilt() const;

	double GetEffectivePulseLength() const;

	double GetSignalSquaredNorm();

	// -- Is result of TransmitSignal is valid
	bool IsValid() const;

protected:
	TransmitSignalObject();

	// --- Signals
	void ComputeSignals(Transducer * const transducer, SoftChannel * const channel);
	std::vector<std::complex<float>> m_Signal;

	// --- FM Signals
	void ComputeFMSignals();
	// flipped and conjugated signal (computed once in FM mode only)
	std::vector<std::complex<float>> m_SignalConjugate;

	// squared norm (computed once in FM mode only)
	float m_SignalSquaredNorm;

	// --- Effective Pulse Length
	void ComputeEffectivePulseLength(Transducer * const transducer);
	double m_EffectivePulseLength;

	float m_SamplingFrequency;
};
