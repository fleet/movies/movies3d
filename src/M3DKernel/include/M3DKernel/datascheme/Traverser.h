/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		HacNav.h												  */
/******************************************************************************/

#pragma once

#include "M3DKernel/base/MovObject.h"
#include "UpdateEvent.h"

class HACSequence;
class HacObjectMgr;
class EchoAlgorithmRoot;
class EchoAlgorithm;
class MovViewFilterMgr;
class WriteAlgorithm;
/// this will be used to pass data through the processing process
class PingFan;
// IPSIS - OTK - on passe l'ESU courante aux m�thodes ESUStart et ESUEnd
class ESUParameter;

class M3DKERNEL_API Traverser : public MovObject
{
public:
	Traverser();

	HacObjectMgr		*m_pHacObjectMgr;
	EchoAlgorithmRoot	*m_pEchoAlgorithmRoot;
	EchoAlgorithmRoot	*m_pInternalAlgorithmRoot;
	EchoAlgorithmRoot	*m_pWriteAlgorithmRoot;
	virtual ~Traverser();

	void PingFanAdded(PingFan *pFan);
	void SounderChanged(std::uint32_t sounderId);
	void SingleTargetAdded(std::uint32_t sounderId);
	void StreamClosed(const char *streamName);
	void StreamOpened(const char *streamName);
	void TupleHeaderUpdate();

	// IPSIS - OTK - ajout evenements ESUStart et ESUEnd
	void ESUStarted(ESUParameter * pWorkingESU);
	void ESUEnded(ESUParameter * pWorkingESU, bool abort);

	UpdateEvent& getUpdateEvent() { return m_UpdateEvent; }

private:
	UpdateEvent m_UpdateEvent;
};

