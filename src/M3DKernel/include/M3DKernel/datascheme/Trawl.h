/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	S. TOGNI													  */
/*	File:		HACTrawl.h										  */
/******************************************************************************/
#ifndef HAC_TRAWL
#define HAC_TRAWL
#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/base/MovComputeObject.h"
#include "BaseMathLib/Vector3.h"

#include "DatedObject.h"
#include "TimeObjectContainer.h"

#include <vector>
#include <map>


#include "M3DKernel/base/MovObject.h"

class M3DKERNEL_API TrawlPositionAttitude : public DatedObject
{
public:
	MovCreateMacro(TrawlPositionAttitude);
	
	//*Angles are in radians, and lenght in meters*/
	double m_HeadRopeAlt;
	double m_PortAlongShipAngle;
	double m_PortArthwartAngle;
	double m_VOpen;
	double m_HOpen;
	double m_HOpenDoor;
	double m_HeadRopeAltitude;
	double m_FootRopeAltitude;
	unsigned short m_FootRopeOnBottom;

	double m_AlongShipSpeedOnFootRope;
	double m_AthwartShipSpeedOnFootRope;
	double m_Temperature;
	unsigned short m_TrawlFill1;
	unsigned short m_TrawlFill2;
	unsigned short m_TrawlFill3;
	unsigned short m_TrawlFill4;
	std::int32_t m_tupleAttributes;

private:
	TrawlPositionAttitude();

};
class M3DKERNEL_API TrawlSensorPositionId
{
public:
	TrawlSensorPositionId() {
		m_distanceId = m_depthId = 0;
	}
	unsigned short m_distanceId;
	unsigned short m_depthId;
	
	bool operator==(const TrawlSensorPositionId& v) const
	{
		return v.m_distanceId == m_distanceId && v.m_depthId == m_depthId;
	}

	bool operator<(const TrawlSensorPositionId& v) const
	{
		if (m_depthId == v.m_depthId)
		{
			return m_distanceId < v.m_distanceId;
		}
		else
			return m_depthId < v.m_depthId;
	}

	bool operator>(const TrawlSensorPositionId& v) const
	{
		if (m_depthId == v.m_depthId)
		{
			return m_distanceId > v.m_distanceId;
		}
		else
			return m_depthId > v.m_depthId;
	}

	TrawlSensorPositionId& operator=(const TrawlSensorPositionId& v)
	{
		m_distanceId = v.m_distanceId; m_depthId = v.m_depthId; return *this;
	}
};


class M3DKERNEL_API TrawlPlatformPosition : public DatedObject
{
public:
	MovCreateMacro(TrawlPlatformPosition);

	BaseMathLib::Vector3D m_Offset;
	std::int32_t			tupleAttributes;
	TrawlSensorPositionId m_SensorId;

protected:
	TrawlPlatformPosition()
	{
	}
};

class M3DKERNEL_API DynamicPlatformPosition : public DatedObject
{
public:
	MovCreateMacro(DynamicPlatformPosition);
	
	TrawlSensorPositionId	m_SensorId;
	BaseMathLib::Vector3D					m_Offset;
	unsigned short			m_PlatformType;
	unsigned short			m_DistanceSensorType;
	unsigned short			m_DepthSensorType;
	std::int32_t					tupleAttributes;
	unsigned short			m_TransChannelId;

protected:
	DynamicPlatformPosition()
	{
	}
};

typedef	 std::map<TrawlSensorPositionId, TimeObjectContainer	*> MapTrawlPlatformPosition;

class M3DKERNEL_API Trawl : public MovComputeObject
{
public:
	MovCreateMacro(Trawl);
	//	Constructors / Destructor
	//	*************************
	virtual ~Trawl();
	
	TimeObjectContainer *GetPlatformPositionContainer(TrawlSensorPositionId attitudeSensorId);

	unsigned int			GetNumberOfPlatformPositionSensorId();
	TrawlSensorPositionId			GetPlatformPositionSensorId(unsigned int index);

	TimeObjectContainer *GetDynamicPlatformPositionContainer(TrawlSensorPositionId attitudeSensorId);

	unsigned int			GetNumberOfDynamicPlatformPositionSensorId();
	TrawlSensorPositionId			GetDynamicPlatformPositionSensorId(unsigned int index);
	TimeObjectContainer *GetTrawlPositionAttitudeContainer() { return m_pTrawlPositionAttitudeContainer; };
	
	void RemoveOldObject(HacTime &oldestDate);

protected:
	Trawl();

	/// calcul les coordonn�es d'un carr� correspondant � l'entr�e du chalut
//	void ComputeQuadCoord();

private:

	TimeObjectContainer *m_pTrawlPositionAttitudeContainer;

	MapTrawlPlatformPosition m_TrawlPlatformPosition;
	std::vector<TrawlSensorPositionId> m_TrawlPlatformPositionSensorId;
	MapTrawlPlatformPosition m_DynamicPlatformPosition;
	std::vector<TrawlSensorPositionId> m_DynamicPlatformPositionSensorId;
};

#endif //HAC_TRAWL
