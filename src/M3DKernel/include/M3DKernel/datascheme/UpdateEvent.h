
#ifndef HAC_UPDATE_EVENT
#define HAC_UPDATE_EVENT
#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/utils/stats/TimeCounter.h"

class M3DKERNEL_API UpdateEvent
{
public:
	UpdateEvent() { Reset(); }
	void Reset() {
		m_FrameAdded = false;
		m_SequenceChanged = false;
		m_TimeOutReached = false;
		m_EndSignatureReached = false;
		//		m_StreamChanged=false;

		m_readTime.Reset();
		m_computeTime.Reset();
	}

	bool m_FrameAdded;
	bool m_SequenceChanged;
	bool m_TimeOutReached;
	bool m_EndSignatureReached;


	// tell if we change file or connection 
//	bool m_StreamChanged;


	/// stats
	TimeCounter m_readTime;
	TimeCounter m_computeTime;


};

#endif