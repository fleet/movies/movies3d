#pragma once
#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/base/MovObject.h"
#include "M3DKernel/DefConstants.h"

// IPSIS - OTK - ajout du passage aux m�thodes serialize et deserialize
// du flux de sortie a utiliser
#include "M3DKernel/parameter/TramaDescriptor.h"

class MovOutput;

class M3DKERNEL_API ModuleOutput : public MovObject
{
public:
	MovCreateMacro(ModuleOutput);
	~ModuleOutput(void);
	virtual void XMLSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);
	virtual void CSVSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);
	virtual void XMLHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);
	virtual void CSVHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);

protected:

	ModuleOutput(void);

};
