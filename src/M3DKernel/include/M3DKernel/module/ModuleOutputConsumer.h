#pragma once
#include "M3DKernel/M3DKernelExport.h"
#include "ModuleOutputContainer.h"

//class ModuleOutput;

class M3DKERNEL_API ModuleOutputConsumer
{
public:
	ModuleOutputConsumer(ModuleOutputContainer&);
	~ModuleOutputConsumer(void);

	void Flush();
	ModuleOutput*	GetOutPut(unsigned int i);
	unsigned int	GetOutPutCount();

	/**use by OutputContainer*/
	unsigned int	GetConsumerId() { return m_consumerId; };

private:
	tConsumerId m_consumerId;
	ModuleOutputContainer &m_refContainer;
};

