#pragma once
#include <algorithm>
#include <iterator>

#include "M3DKernel/M3DKernelExport.h"

#include <vector>
#include <map>
#include "M3DKernel/module/ModuleOutput.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"

typedef unsigned int tConsumerId;

typedef	 std::map<tConsumerId, unsigned int> IndexConsumer;

class M3DKERNEL_API ModuleOutputContainer
{
public:
	ModuleOutputContainer(void);
	~ModuleOutputContainer(void);

	/** interface used by module to add a few outputs*/
	void AddOutput(ModuleOutput*);

	/** interface used by consumers */
	unsigned int GetOutPutCount(tConsumerId aConsumerId) const;
	ModuleOutput* GetOutPut(tConsumerId, unsigned int i) const;
	
	/// Get all ouputs
	const std::vector<ModuleOutput*>& getOutputs() const;
	template<typename T>
	std::vector<T*> getOutputs() const;
	
	/// Get all ouputs by consumer
	std::vector<ModuleOutput*> getOutputs(tConsumerId consumerId) const;
	template<typename T>
	std::vector<T*> getOutputs(tConsumerId consumerId) const;

	///this function is called when a consumer has read all data avalaible
	void Flush(tConsumerId aConsumerId);

	tConsumerId	ReserveConsumerId();
	void			RemoveConsumer(tConsumerId);

	void Lock() { m_Lock.Lock(); }
	void Unlock() { m_Lock.Unlock(); }

private:
	/**function called to remove output no longer needed*/
	void CleanUp();
	std::vector<ModuleOutput*> m_moduleOutputList;
	IndexConsumer	m_mapConsumerIndex;
	bool GetIndexForConsumer(tConsumerId a, unsigned int &index) const;

	// OTK - 21/10/2009 - pour synchroniser les acc�s au container
	CRecursiveMutex m_Lock;

	// Cast des données de sortie
	template<typename T>
	static std::vector<T*> castOutputs(const std::vector<ModuleOutput*>& outputs);
	template<typename T>
	static T* castOutput(ModuleOutput* output);
};

template <typename T>
std::vector<T*> ModuleOutputContainer::getOutputs() const
{
	return castOutputs<T>(getOutputs());
}

template <typename T>
std::vector<T*> ModuleOutputContainer::getOutputs(const tConsumerId consumerId) const
{
	return castOutputs<T>(getOutputs(consumerId));
}

template <typename T>
std::vector<T*> ModuleOutputContainer::castOutputs(const std::vector<ModuleOutput*>& outputs)
{
	std::vector<T*> castedOutputs;
	std::transform(outputs.cbegin(), outputs.cend(), std::back_inserter(castedOutputs), &castOutput<T>);
	return castedOutputs;
}

template <typename T>
T* ModuleOutputContainer::castOutput(ModuleOutput* output)
{
	return dynamic_cast<T*>(output);
}
