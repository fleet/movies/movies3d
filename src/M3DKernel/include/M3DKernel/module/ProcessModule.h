#pragma once

#include "M3DKernel/algorithm/base/EchoAlgorithm.h"
#include "M3DKernel/module/ModuleOutputContainer.h"
#include "M3DKernel/module/ModuleOutputConsumer.h"
#include "M3DKernel/parameter/ParameterModule.h"

class M3DKERNEL_API ProcessModule :	public EchoAlgorithm
{
public:

	virtual void PingFanAdded(PingFan *pFan) = 0;
	virtual void SounderChanged(std::uint32_t sounderId) = 0;
	virtual void StreamClosed(const char *streamName) = 0;
	virtual void StreamOpened(const char *streamName) = 0;

	// IPSIS - OTK - on doit pouvoir d�sactiver un module de traitement
	// � la fin de l'ESU suivant
	virtual void DisableForNextESU() {}
	virtual ~ProcessModule();

	const ModuleOutputContainer& GetOutputContainer() const { return m_outputContainer; }
	ModuleOutputContainer& GetOutputContainer() { return m_outputContainer; }

	ModuleOutputConsumer*	GetMatlabModuleConsumer();
	ModuleOutputConsumer*	GetOutputMgrConsumer();

	// les modules doivent �galement disposer d'un accesseur vers
	// leur param�tres poru que le outputManager puisse
	// savoir comment �mettre les r�sultats
	virtual BaseKernel::ParameterModule& GetParameterModule() = 0;

protected:
	ProcessModule(const std::string& name);
	ModuleOutputContainer m_outputContainer;
	ModuleOutputConsumer *m_pMatlabConsumer;
	ModuleOutputConsumer *m_pOutputMgrConsumer;
};
