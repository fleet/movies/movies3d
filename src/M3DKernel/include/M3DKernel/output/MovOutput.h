// -*- C++ -*-
// ****************************************************************************
// Class: MovOutput
//
// Description: Classe abstraite d�finissant un flux de sortie pour 
// le module d'archivage fichier et r�seau OutputManager.
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mai 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once
#pragma warning( disable : 4275 ) 

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "M3DKernel/M3DKernelExport.h"
#include <string>
#include <vector>


// ***************************************************************************
// Declarations
// ***************************************************************************
class MovOutput
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	
	// Constructeur par d�faut
	MovOutput() = default;

	// Destructeur
	virtual ~MovOutput() = default;

	// *********************************************************************
	// Accesseurs
	// *********************************************************************
	virtual bool isNetworkOutput() = 0;

	// *********************************************************************
	// Traitements
	// *********************************************************************
	virtual void Write(std::string str) = 0;
	virtual void CreateXML(std::string str) = 0;
	virtual void AppendXML(std::vector<std::string> strvect) = 0;

};
