// -*- C++ -*-
// ****************************************************************************
// Class: ParameterBroadcastAndRecord
//
// Description: Classe contenant l'ensemble des param�tres propres � un module
// pour la'rchivage de ses r�sultats (ou le broadcast r�seau).
// Il ne s'agit pas des param�tres globaux a tous les modules qui sont
// contenus dans le OutputModuleParameter.
//
// Projet: MOVIES3D
// Auteur: K�vin DUGUE
// Date  : Avril 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "M3DKernel/parameter/ParameterObject.h"
#include "M3DKernel/parameter/TramaDescriptor.h"

#include <string>
#include <vector>

namespace BaseKernel {

	// ***************************************************************************
	// Declarations
	// ***************************************************************************

	class M3DKERNEL_API ParameterBroadcastAndRecord :
		public ParameterObject
	{

	public:

		// enum des diff�rents types d'export de trames :
		// - XML
		// - CSV
		// - NetCDF
		typedef enum {
			XML_TRAMA = 0,
			CSV_TRAMA,
			NETCDF_TRAMA
		} TTramaType;

		// enum des diff�rents types de d�coupage des fichiers :
		// - taille
		// - dur�e
		typedef enum {
			SIZE_CUT = 0,
			TIME_CUT
		} TCutType;

		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		ParameterBroadcastAndRecord(void);
		ParameterBroadcastAndRecord(DictionnaryType type);
		// Destructeur
		virtual ~ParameterBroadcastAndRecord(void);

		virtual bool Serialize(MovConfig * movConfig);
		virtual bool DeSerialize(MovConfig * movConfig);

		// *********************************************************************
		// Donn�es
		// *********************************************************************

		bool m_enableFileOutput; // archivage fichier activ� ou non
		bool m_enableNetworkOutput; // archivage r�seau activ� ou non
		TTramaType m_tramaType; // trames CSV ou XML ou ...

		// Param�tres contenant la s�lection des trames � sauvegarder
		TramaDescriptor m_tramaDescriptor;

		TCutType		 m_cutType;
		unsigned int m_fileMaxSize;
		unsigned int m_fileMaxTime; // en secondes
		std::string  m_filePrefix;
		std::string	 m_filePath;
		std::string	 m_fileSuffix; // LAY, SHL ...
		bool m_prefixDate; // prefixage ou non de la date dans le nom du fichier

	};
}