#pragma once
namespace BaseKernel
{

	enum ParameterDataType
	{
		eBool,
		eInt,
		eUInt,
		eShort,
		eUShort,
		eFloat,
		eDouble,
		eVector3D,
		eVector3I,
		eVector3UI,
		eVector2D,
		eVector2I,
		eVector2UI,

		eString, // std::string

		eParameterModule,
		eParameterObject,

	};

}
