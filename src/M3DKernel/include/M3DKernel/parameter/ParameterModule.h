#pragma once
#include "M3DKernel/parameter/ParameterSerializable.h"
// IPSIS - OTK - ajout ParameterBoradCastAndRecord
// afin que chaque module puisse s�rialiser sur le r�seau
#include "M3DKernel/parameter/ParameterBroadcastAndRecord.h"
#include "M3DKernel/parameter/TramaDescriptor.h"

#include <string.h>
namespace BaseKernel
{

	class M3DKERNEL_API ParameterModule : public ParameterSerializable
	{
	public:
		ParameterModule(const char *aName);
		ParameterModule(const char *aName, DictionnaryType type);
		virtual ~ParameterModule(void);

		std::string& GetName() { return m_Name; }
		void		SetName(std::string name) { m_Name = name; }

		// IPSIS - OTK - ajout ParameterBoradCastAndRecord pour chaque module		
		ParameterBroadcastAndRecord& GetParameterBroadcastAndRecord() { return m_ParameterBroadcastAndRecord; };
		TramaDescriptor& GetTramaDescriptor() { return m_ParameterBroadcastAndRecord.m_tramaDescriptor; };



	private:
		std::string m_Name;

		// IPSIS - OTK - ajout ParameterBoradCastAndRecord pour chaque module
		ParameterBroadcastAndRecord m_ParameterBroadcastAndRecord;

	};
}