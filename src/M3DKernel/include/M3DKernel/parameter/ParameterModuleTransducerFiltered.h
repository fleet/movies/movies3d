﻿#pragma once
#include <set>

#include "ParameterModule.h"

namespace BaseKernel
{
    class M3DKERNEL_API ParameterModuleTransducerFiltered : public ParameterModule
    {
    public:
        explicit ParameterModuleTransducerFiltered(const char *aName);
        explicit ParameterModuleTransducerFiltered(const char *aName, DictionnaryType type);
        
        // Sérialisation
        bool SerializeActiveTransducers(MovConfig * movConfig) const;
        bool DeSerializeActiveTransducers(MovConfig * movConfig);

        // Activation du filtre
        void setEnableTransducerFilter(bool enable);
        bool isEnableTransducerFilter() const;

        // Transducers
        void addActiveTransducerName(const std::string& transducerName);
        void addActiveTransducerNames(const std::set<std::string>& transducerNames);

        void removeActiveTransducerName(const std::string& transducerName);
        void removeActiveTransducerNames(const std::set<std::string>& transducerNames);

        void clearActiveTransducerNames();
	
        const std::set<std::string>& getActiveTransducerNames() const;
        bool isTransducerNameActive(const std::string& transducerName) const;

    private:
        // Filtre par transducteur activé
        bool m_enableTransducerFilter;
        
        // Transducteurs concernés
        std::set<std::string> m_activeTransducerNames;
    };
}