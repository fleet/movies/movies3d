#pragma once
#include "M3DKernel/parameter/ParameterSerializable.h"


namespace BaseKernel
{

	class M3DKERNEL_API ParameterObject : public ParameterSerializable
	{
	public:

		ParameterObject(void) : ParameterSerializable()
		{
		}

		virtual ~ParameterObject(void)
		{
		}
	};
}