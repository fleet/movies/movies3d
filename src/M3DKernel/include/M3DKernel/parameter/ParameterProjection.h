// -*- C++ -*-
// ****************************************************************************
// Class: ParameterProjection
//
// Description: Classe regroupant les param�tres propores � une projection
// conique conforme.
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Juin 2010
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "M3DKernel/parameter/ParameterObject.h"

#include <string>

namespace BaseKernel {

	// ***************************************************************************
	// Declarations
	// ***************************************************************************

	class M3DKERNEL_API ParameterProjection :
		public ParameterObject
	{

	public:

		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		ParameterProjection(void);
		// Destructeur
		virtual ~ParameterProjection(void);

		virtual bool Serialize(MovConfig * movConfig);
		virtual bool DeSerialize(MovConfig * movConfig);

		// *********************************************************************
		// Donn�es
		// *********************************************************************

		std::string m_ProjectionName;
		double m_Eccentricity;
		double m_SemiMajorAxis;
		double m_FirstParal;
		double m_SecondParal;
		double m_LongMeridOrigin;
		double m_X0;
		double m_Y0;
		double m_ScaleFactor;

	};
}