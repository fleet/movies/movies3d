#pragma once

#include "M3DKernel/M3DKernelExport.h"

namespace BaseKernel
{
	class MovConfig;
	class M3DKERNEL_API ParameterSerializable
	{
	public:
		ParameterSerializable();
		virtual ~ParameterSerializable();

		// IPSIS - OTK - passage de MovConfig en param�tre
		virtual bool Serialize(MovConfig * pMovConfig) = 0;
        virtual bool DeSerialize(MovConfig * pMovConfig) = 0;
	};
}
