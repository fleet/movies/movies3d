#pragma once
#include "M3DKernel/parameter/ParameterDataType.h"
#include "M3DKernel/M3DKernelExport.h"

namespace BaseKernel
{

	class M3DKERNEL_API ParameterStream
	{
	public:
		ParameterStream(void);
		virtual ~ParameterStream(void);


		/// read the config from a file
		void ReadFile(const char *);

		/// write the config to a file
		void OpenFile(const char *);

		void CloseFile();


		void SerializeData(const void *	donnee1, ParameterDataType dataType, const char *dataName);
		void DeSerializeData(void *		donnee1, ParameterDataType dataType, const char *dataName);
	};
};
