#pragma once
#include "M3DKernel/parameter/ParameterSerializable.h"
namespace BaseKernel
{

	class M3DKERNEL_API ParameterTable :
		public ParameterSerializable
	{
	public:
		ParameterTable(void);
		virtual ~ParameterTable(void);
	};
};