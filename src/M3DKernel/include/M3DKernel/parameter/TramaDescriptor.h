// -*- C++ -*-
// ****************************************************************************
// Class: TramaDescriptor
//
// Description: Classe d�crivant un ensemble de trames XML associ�es � un 
// boul�en indiquant si la trame correspondant est s�lectionn�e ou non.
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mai 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// D�pendances
#include <vector>
#include <string>
#include <map>

#include "M3DKernel/parameter/ParameterObject.h"

namespace BaseKernel {

	// classe serialisable contenant l'identifiant d'un noeud XML ou CSV
	// et un boolen indiquant si ce neoud doit �tre sauvegard� ou non.
	class M3DKERNEL_API TramaNode : public ParameterObject
	{
	public:

		TramaNode() {};
		virtual ~TramaNode() {};

		virtual bool Serialize(MovConfig * movConfig);
		virtual bool DeSerialize(MovConfig * movConfig);

		bool wanted;
		bool needed;
		std::string xmlNodeName;
		std::string xmlNodeDesc;
	};


	enum DictionnaryType
	{
		eUndefined = -1,
		eLayer = 0,
		eShoal,
		eSupervised,
	};

	class M3DKERNEL_API TramaDescriptor : public BaseKernel::ParameterObject
	{
	public:


		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************

		// constructeurs
		TramaDescriptor();
		TramaDescriptor(DictionnaryType type);

		// destructeur
		virtual ~TramaDescriptor();


		virtual bool Serialize(MovConfig * movConfig);
		virtual bool DeSerialize(MovConfig * movConfig);

		// *********************************************************************
		// Traitements
		// *********************************************************************

		// construit l'arbre des noeuds en fonction du dictionnaire
        void ParseDictionnary();
		// ajoute un noeud a la config
		void AddNode(std::string nodeText, std::string nodeDesc, bool nodeWanted,
			bool needed = false, size_t parentNodeIndex = 0);
		// renvoie vrai si le noeud XML sp�cifi� est "wanted"
		bool IsWanted(std::string stringID);
		// renvoie vrai si le noeud XML sp�cifi� est "needed"
		bool IsNeeded(std::string stringID);
		// Positionne le booleen associ� au noeud XML identifi� par stringID
		void SetWanted(std::string stringID, bool value);
		// construit l'ent�te CSV en fonction de l'arbre des noeuds XML
        std::string	GetTitlesByCat(std::string catID, bool filterEcho = false);

		std::string GetValuesByCat(const std::string & catID, const std::map<std::string, std::string> & values, bool filterEcho = false) const;


		// *********************************************************************
		// Donn�es
		// *********************************************************************
		// tableau des trames � archiver
		std::vector<TramaNode>	m_xmlNodesArray;
		// indique si les trames choisies ont �t� configur�es ou non
		// si non, on archive toutes les trames.
		bool										m_Configured;
        std::string                     m_DictionnaryPath;
		// dictionnaire � utiliser
		DictionnaryType					m_DicoType;

	};
}
