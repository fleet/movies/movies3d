﻿#pragma once

#include "M3DKernel/M3DKernelExport.h"
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/util/XMLString.hpp>
#include <sstream>
#include <codecvt>

XERCES_CPP_NAMESPACE_USE

namespace BaseKernel
{
	class M3DKERNEL_API XmlChUtils
	{
	public:
		/// Convertit un XmlCh en un autre type
		template <typename T>
		static T XMLChToT(const XMLCh* xmlCh) {
			T res;			
			std::string str = XMLString::transcode(xmlCh);
			std::stringstream stream(str);
			stream >> res;
			return res;
		}
	};
}
