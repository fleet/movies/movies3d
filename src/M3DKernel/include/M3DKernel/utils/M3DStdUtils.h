#pragma once

#include "M3DKernel/M3DKernelExport.h"

#include <string>
#include <cstring>
#include <ctime>
#include <limits>

#ifndef WIN32
#include <sys/types.h>
#include <sys/stat.h>

namespace {

	inline char* strcpy_s(char* restrict_dest, size_t destsz, const char* restrict_src)
	{
		return strcpy(restrict_dest, restrict_src);
	}

	inline int _stricmp(const char *s1, const char *s2)
	{
		return strcmp(s1, s2);
	}

	inline void _itoa_s(int value, char * buffer, size_t size, int radix)
	{
		sprintf(buffer, "%d", value);
	}

	inline int _mkdir(const char *path)
	{
		return mkdir(path, 0755);
	}

}

#endif
