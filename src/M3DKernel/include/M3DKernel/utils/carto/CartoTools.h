// -*- C++ -*-
// ****************************************************************************
// Class: CCartoTools
//
// Description: M�thodes utilitaires pour les calculs de projection et 
// transformation de coordonn�es g�ographiques en coordonn�es cart�siennes
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Juin 2010
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "M3DKernel/M3DKernelExport.h"

#include "BaseMathLib/Vector2.h"
#include "BaseMathLib/Vector3.h"

#include <string>

// ***************************************************************************
// Declarations
// ***************************************************************************

class KernelParameter;
class M3DKERNEL_API CCartoTools
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	CCartoTools();

	// Destructeur
	virtual ~CCartoTools();


	// *********************************************************************
	// Traitements publics
	// *********************************************************************
	// Initialisation et pr�calculs en fonction des param�tres de projection
	static void Initialize(KernelParameter& kernelParams);

	static void GetGeoCoords(double latitude, double longitude,
		double x, double y, double &resultingLat, double &resultingLong);

	static void GetGeoCoords(const BaseMathLib::Vector3D & refCoords
		, const std::vector<BaseMathLib::Vector3D> & offsetCoords
		, std::vector<BaseMathLib::Vector3D>& resCoords);

	static void GetCartesianCoords(double latitude, double longitude,
		double &x, double &y, bool translation = true);

	static bool IsKnownOrigin() { return s_bKnownOrigin; };
	static void InitializeOrigin() { s_bKnownOrigin = false; s_Origin.x = 0.0; s_Origin.y = 0.0; };
	static void SetOrigin(double latitude, double longitude);

	static std::string LattitudeToString(double lattitude);
	static std::string LongitudeToString(double longitude);


private:



	// *********************************************************************
	// Donn�es statiques
	// *********************************************************************

	// translation d'origine pour la pr�cision OpenGl (float)
	static bool s_bKnownOrigin;
	static BaseMathLib::Vector2D s_Origin;

	// param�tres de l'ellipse
	static double s_A; // demi grand axe
	static double s_E; // excentricit�
	static double s_E2; // excentricit� au carr�
	static double s_DegToRad;

	// param�tres de la projection
	static double s_L1;
	static double s_L2;
	static double s_G0;
	static double s_X0;
	static double s_Y0;
	static double s_RK;
	static double s_N1;
	static double s_N2;
	static double s_ISO1;
	static double s_ISO2;
	static double s_L12;
	static double s_R0;
	static double s_N;
	static double s_C;
};
