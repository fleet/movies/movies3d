#ifdef WIN32

// -*- C++ -*-
// ****************************************************************************
// Class: Movies3DEnv
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Septembre 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

#include <string>
#include "M3DKernel/M3DKernelExport.h"

class M3DKERNEL_API Movies3DEnv
{
public:

	static Movies3DEnv* GetInstance() {
		if (m_pInstance == NULL)
		{
			m_pInstance = new Movies3DEnv();
			m_pInstance->Init();
		}
		return m_pInstance;
	};

	//get Movies3D env : last cfg
	virtual std::string GetLastConfig();
	//set Movies3D env : last cfg
	virtual void SetLastConfig(const std::string& dir);

	//get Movies3D env : last load directory
	virtual std::string GetLastLoadDir();
	//set Movies3D env : last load directory
	virtual void SetLastLoadDir(const std::string& dir);

protected:

	//initialise the Movies3D Env
	virtual void Init();
private:

	static Movies3DEnv* m_pInstance;

	//members
	std::string m_regKeyEnv;

	std::string m_regSubKey_LastCfg;
	std::string m_regSubKey_LastLoadDir;
};

#endif
