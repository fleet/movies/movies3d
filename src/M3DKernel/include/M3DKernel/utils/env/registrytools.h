#ifdef WIN32

//*****************************************************************************
// File : Registrytools.h
//
// Description : Some useful functions tools to manipulate registry
//
// Date : 29/01/2007
// Author : FRE
//*****************************************************************************

// Exclure les en-t�tes Windows rarement utilis�s
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <string>

//*****************************************************************************
// Registry tools
//*****************************************************************************

  //Delete a key in the registry
std::int32_t DeleteKey(
	HKEY hKeyParent,
	const char* lpszKeyChild);

std::int32_t DeleteKey(
	struct HKEY__ * pkeyRoot, //HKEY_LOCAL_MACHINE, HKEY_CURRENT_USER... 
	const char* szKey,
	const char* lpszKeyChild);

//Add a key in the registry
BOOL AddKey(
	struct HKEY__ * pkeyRoot, //HKEY_LOCAL_MACHINE, HKEY_CURRENT_USER...
	const char* szKey,
	const char* szSubkey,
	const char* szValue);

//Set a key in the registry
BOOL SetKeyValue(
	struct HKEY__ * pkeyRoot, //HKEY_LOCAL_MACHINE, HKEY_CURRENT_USER...
	const char* szKey,
	const char* szNamedValue,
	const char* szValue);

//Get a key from the registry
BOOL GetKeyValue(
	struct HKEY__ * pkeyRoot, //HKEY_LOCAL_MACHINE, HKEY_CURRENT_USER...
	const char* szKey,
	const char* szNamedValue,
	unsigned char** szValue,
	std::uint32_t* valueSize);

BOOL GetKeyValue(
	struct HKEY__ * pkeyRoot, //HKEY_LOCAL_MACHINE, HKEY_CURRENT_USER...
	const char* szKey,
	const char* szNamedValue,
	std::string& szValue);

#endif
