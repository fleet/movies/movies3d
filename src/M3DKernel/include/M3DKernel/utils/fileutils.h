#ifndef FILEUTILS_H
#define FILEUTILS_H

#include "M3DKernel/M3DKernelExport.h"
#include <string>

namespace fileutils {

	/// Retourne la taille d'une fichier en octet
	M3DKERNEL_API std::uint32_t getFileSize(const std::string & filePath);
}

#endif
