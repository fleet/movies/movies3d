#ifndef LOGINTERFACE
#define LOGINTERFACE

#include <string>
#include <memory>

#include "M3DKernel/M3DKernelExport.h"

namespace Log
{
	/// Log level
	typedef int LogLevel;

	/// No Log level
	const LogLevel NO_LOG_LEVEL = -1;

	/// Log level for trace message
	const LogLevel TRACE_LOG_LEVEL = 0;

	/// Log level for debug message
	const LogLevel DEBUG_LOG_LEVEL = 10000;

	/// Log level for info message
	const LogLevel INFO_LOG_LEVEL = 20000;

	/// Log level for info message
	const LogLevel HAC_LOG_LEVEL = 25000;

	/// Log level for warn message
	const LogLevel WARN_LOG_LEVEL = 30000;

	/// Log level for error message
	const LogLevel ERROR_LOG_LEVEL = 40000;

	/// Log level for fatal message
	const LogLevel FATAL_LOG_LEVEL = 50000;

	/**
	* @brief Structure permettant la représentation d'un message de log
	*/
	struct LoggerItem
	{
		std::string source;
		int level;
		std::uint64_t timestamp;
		std::string message;
	};

	/**
	* @brief Fonction utilitaire permettant le formatage de chaine de caractère
	*/
	template<typename ... Args>
	std::string format(const std::string& format, Args ... args)
	{
		std::size_t size = std::snprintf(nullptr, 0, format.c_str(), args ...) + 1;
		std::unique_ptr<char[]> buf(new char[size]);
		std::snprintf(buf.get(), size, format.c_str(), args ...);
		return std::string(buf.get(), buf.get() + size - 1);
	}

    /**
    * @class ILogger
    * @brief Interface for logging class
    */
    class ILogger
    {
    public:
        virtual ~ILogger() {}

        /**
        * @brief Log message
        * @param logLevel Message level
        * @param msg Message
        * @param file Source file name : don't use directly, use Macro __FILE__instead
        * @param line Line in source file : don't use directly, use Macro __LINE__instead
        */
        virtual void log(LogLevel logLevel, const std::string & msg, const char * file = nullptr, int line = -1) = 0;

		/**
        * @brief Return is a log level is enabled
        * @param logLevel The log level to test
        * @return True if the log level is enabled, false otherwise
        */
        virtual bool isLevelEnabled(LogLevel logLevel) const = 0;

		/**
        * @brief Get a child logger
        * @param loggerName Name of the child logger
        * @return The child logger
        */
        virtual std::shared_ptr<Log::ILogger> getLogger(const std::string & loggerName) const = 0;
    };


    /**
    * @class ILoggerFactory
    * @brief Interface for class creating loggers
    */
    class ILoggerFactory
    {
    public:

        /**
        * @brief Get logger by name
        * @param loggerName Name for the logger
        * @return A shared pointer to the logger
        */
        virtual std::shared_ptr<Log::ILogger> getLogger(const std::string & loggerName) = 0;
    };
}

/// Typedef for logger (using shared pointer)
typedef std::shared_ptr<Log::ILogger> M3DLogger;

/// Usefull macro to log message
#define M3D_LOG(logger, loglevel, message) Log::Utils::log(logger, loglevel, message, __FILE__, __LINE__)

/// Usefull macro to test if trace level is enabled for a logger
#define IS_TRACE_ENABLED(logger) (logger->isLevelEnabled(Log::TRACE_LOG_LEVEL))

/// Usefull macro to log message as trace
#define M3D_LOG_TRACE(logger, message) M3D_LOG(logger, Log::TRACE_LOG_LEVEL, message)

/// Usefull macro to log message as debug
#define M3D_LOG_DEBUG(logger, message) M3D_LOG(logger, Log::DEBUG_LOG_LEVEL, message)

#define IS_DEBUG_ENABLED(logger) (logger->isLevelEnabled(Log::DEBUG_LOG_LEVEL))

/// Usefull macro to log message as information
#define M3D_LOG_INFO(logger, message) M3D_LOG(logger, Log::INFO_LOG_LEVEL, message)

/// Usefull macro to log message as information
#define M3D_LOG_HAC(logger, message) M3D_LOG(logger, Log::HAC_LOG_LEVEL, message)

/// Usefull macro to log message as warning
#define M3D_LOG_WARN(logger, message) M3D_LOG(logger, Log::WARN_LOG_LEVEL, message)

/// Usefull macro to log message as error
#define M3D_LOG_ERROR(logger, message) M3D_LOG(logger, Log::ERROR_LOG_LEVEL, message)

/// Usefull macro to log message as fatal
#define M3D_LOG_FATAL(logger, message) M3D_LOG(logger, Log::FATAL_LOG_LEVEL, message)

namespace Log
{
	/// Usefull class to log message with loggerName or logger
	class M3DKERNEL_API Utils
	{
	public:
		static void log(const std::string & loggerName, Log::LogLevel logLevel, const std::string & msg, const char * file = nullptr, int line = -1);

		static void log(M3DLogger logger, Log::LogLevel logLevel, const std::string & msg, const char * file = nullptr, int line = -1);
	};
}

#endif
