#ifndef LOGAPPENDER
#define LOGAPPENDER

#include "M3DKernel/M3DKernelExport.h"

#include "M3DKernel/utils/log/ILogger.h"

namespace Log {

	class ILogAppender
	{
	public:
		virtual void append(const LoggerItem & logItem) = 0;
	};

	class M3DKERNEL_API LogAppender
	{
	public:
		// Singleton
		static LogAppender* GetInstance();
		static void FreeMemory();

		void setDefaultAppender(ILogAppender * appender);

		void registerAppender(ILogAppender * appender);
				
		void unregisterAppender(ILogAppender * appender);
		
		// Reset le syst�me d'alerte
		void resetCheckLevelMessage(const int level = NO_LOG_LEVEL);

		// R�cup�re l'alerte de log ayant le niveau le plus haut
		int maxAlertLevel();
		
	private:
		// Constructeur
		LogAppender();

		//Destructeur
		virtual ~LogAppender();
		
		// Impl permettant de cacher l'impl�mentation log4cplus
		class Impl;
		Impl * impl;

		// Instance singleton
		static LogAppender* m_logAppenderInstance;
	};
}
#endif
