#ifndef LOGGERFACTORY
#define LOGGERFACTORY

#include <string>
#include <memory>

#include <M3DKernel/utils/log/ILogger.h>
#include "M3DKernel/M3DKernelExport.h"

namespace Log
{
    class M3DKERNEL_API LoggerFactory : public ILoggerFactory
    {
    public:
        void initialize(const std::string & propertiesFilePath);

        std::shared_ptr<Log::ILogger> getLogger(const std::string & loggerName);

        static Log::LoggerFactory * GetInstance();

		static void FreeMemory();

    protected:
        static LoggerFactory * m_instance;

        LoggerFactory();
        virtual ~LoggerFactory();
    };
}

#define M3D_GETLOGGER(LoggerName) Log::LoggerFactory::GetInstance()->getLogger(LoggerName)

#endif // LOGGERFACTORY
