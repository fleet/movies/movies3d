﻿#pragma once
#include <algorithm>

class MathUtils
{
public:
    static bool floatingPointEquals(const double x, const double y, const double epsilon = std::numeric_limits<double>::epsilon())
    {
        const auto max = std::max( { 1.0, std::abs(x) , std::abs(y) } ) ;
        return std::abs(x - y) <= epsilon * max ;        
    }
    
    static bool floatingPointEquals(const float x, const float y, const float epsilon = std::numeric_limits<float>::epsilon())
    {
        const auto max = std::max( { 1.0f, std::abs(x) , std::abs(y) } ) ;
        return std::abs(x - y) <= epsilon * max ;        
    }
};
