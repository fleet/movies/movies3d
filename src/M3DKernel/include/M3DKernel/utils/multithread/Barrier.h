#pragma once

#include "M3DKernel/M3DKernelExport.h"


class M3DKERNEL_API Barrier
{
	struct Impl;
	Impl * impl;


	// Disable copy
	Barrier(const Barrier &) = delete;
	Barrier & operator=(const Barrier &) = delete;

public:

	/// Default constructor,  initial state : unlocked(false)
	Barrier();

	/// Destructor, switch to state unlocked(false)
	~Barrier();

	/// Set the state of the barrier to the given state
	void setState(bool state);

	/// Wait until the state of the barrier is equal to the given state
	void wait(bool state);
	
	/// Convenience method to set the state of the barrier to 'true' (locked)
	void lock();

	/// Convenience method to set the state of the barrier to 'false' (unlocked)
	void unlock();

	/// Convenience method to wait until the barrier is locked
	void waitLock();
	
	/// Convenience method to wait until the barrier is unlocked
	void waitUnlock();

	/// Query the state of the barrier, true:locked, false:unlock
	bool isLocked() const;
};

class BarrierLocker
{
	Barrier & b;

	// Disable copy
	BarrierLocker() = delete;
	BarrierLocker(const BarrierLocker &) = delete;
	BarrierLocker & operator=(const BarrierLocker &) = delete;

public:
	BarrierLocker(Barrier & _b);
	~BarrierLocker();
};