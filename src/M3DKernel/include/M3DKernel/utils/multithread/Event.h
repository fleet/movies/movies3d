#pragma once

#include "M3DKernel/M3DKernelExport.h"

// Exclure les en-t�tes Windows rarement utilis�s
#define WIN32_LEAN_AND_MEAN	
#define NOMINMAX

// ***************************************************************************
// Dependences
// ***************************************************************************

// ***************************************************************************
// Declarations
// ***************************************************************************

// ***************************************************************************
// Interface de la classe CEvent
// ***************************************************************************
class M3DKERNEL_API CEvent
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	CEvent();

	// Destructeur
	~CEvent();

	void Post();
	void Wait();
	void Reset();

private:

	// Disable copy
	CEvent(const CEvent & other) {}
	CEvent & operator=(const CEvent & other) { return *this; }

	// *********************************************************************
	// Donn�es
	// *********************************************************************
	class Impl;
	Impl * impl;
};
