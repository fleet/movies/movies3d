#pragma once

#include "M3DKernel/M3DKernelExport.h"

class M3DKERNEL_API KernelLocker
{
public:
	KernelLocker();
	~KernelLocker();

private:
	KernelLocker(const KernelLocker & other) {}
	KernelLocker & operator=(const KernelLocker & other) { return *this; }
};