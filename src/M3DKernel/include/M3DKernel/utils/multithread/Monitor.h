#pragma once
#include "M3DKernel/M3DKernelExport.h"

#include "Event.h"

#include <cstdint>

// ***************************************************************************
// Declarations
// ***************************************************************************

// ***************************************************************************
// Interface de la classe CMonitor
// ***************************************************************************
class M3DKERNEL_API CMonitor
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	CMonitor();

	// Destructeur
	virtual ~CMonitor();

	void Wait();
	void Wait(std::int32_t);
	void Notify();
	void NotifyAll();

	void Lock(bool);
	void Unlock();

private:

	void ValidateMutexOwner(const char*) const;

	class Impl;
	Impl * impl;
};
