#pragma once
#include "M3DKernel/M3DKernelExport.h"

#include "M3DKernel/utils/multithread/RecursiveMutex.h"

/**
* Classe permettant d'acquire le lock sur une CriticalSectionLocker lors du constructeur, et de la lib�rer � la destrution
*/
class M3DKERNEL_API CRecursiveMutexLocker
{
public:
	CRecursiveMutexLocker(CRecursiveMutex *  mutex);
	~CRecursiveMutexLocker();

private:
	CRecursiveMutex* m_mutex;

	// Disable copy
	CRecursiveMutexLocker(const CRecursiveMutexLocker &) {}
	CRecursiveMutexLocker & operator=(const CRecursiveMutexLocker &) { return *this; }
};
