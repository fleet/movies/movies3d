#pragma once
#include "M3DKernel/M3DKernelExport.h"

class CRecursiveMutexImpl;
// ***************************************************************************
// Declarations
// ***************************************************************************

// ***************************************************************************
// Interface de la classe CRecursiveMutex
// ***************************************************************************
class M3DKERNEL_API CRecursiveMutex
{
public:
	friend class CMonitor;
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	CRecursiveMutex();

	// Destructeur
	~CRecursiveMutex();

	void Lock() const;
	void Unlock() const;

private:
	CRecursiveMutexImpl* impl;

	// Disable copy
	CRecursiveMutex(const CRecursiveMutex &) {}
	CRecursiveMutex & operator=(const CRecursiveMutex &) { return *this;  }
};
