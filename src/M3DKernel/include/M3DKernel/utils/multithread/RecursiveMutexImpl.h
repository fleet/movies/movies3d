#pragma once
#include "M3DKernel/M3DKernelExport.h"

#include <mutex>

// ***************************************************************************
// Declarations
// ***************************************************************************

// ***************************************************************************
// Interface de la classe CRecursiveMutexImpl
// ***************************************************************************
class M3DKERNEL_API CRecursiveMutexImpl
{
public:
	std::recursive_mutex mutex;
};
