#pragma once
#include "M3DKernel/M3DKernelExport.h"

#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "Monitor.h"

// ***************************************************************************
// Declarations
// ***************************************************************************

// ***************************************************************************
// Interface de la classe CSynchronized
// ***************************************************************************
class CRecursiveMutex;
class CRecursiveMutex;
class CMonitor;
class M3DKERNEL_API CSynchronized
{
public:
	typedef enum { REC_MUTEX, MONITOR } TLockType;

	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeurs
	CSynchronized(CRecursiveMutex&       recMutex);
	CSynchronized(CMonitor&              monitor);

	// Destructeur
	virtual ~CSynchronized();

protected:

	// *********************************************************************
	// Traitements prot�g�s
	// *********************************************************************

private:

	// *********************************************************************
	// Donn�es
	// *********************************************************************
	TLockType             m_LockType;
	CRecursiveMutex       *m_pRecMutex;
	CMonitor              *m_pMonitor;
};
