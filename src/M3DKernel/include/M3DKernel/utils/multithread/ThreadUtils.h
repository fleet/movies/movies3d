#pragma once

#include "M3DKernel/M3DKernelExport.h"

#include <thread>

class M3DKERNEL_API ThreadUtils
{
public:
	static bool isThreadAlive(std::thread & t);
};

