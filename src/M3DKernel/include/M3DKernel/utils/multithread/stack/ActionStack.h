// -*- C++ -*-
// ****************************************************************************
// Class: ActionStack
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Sept 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once
#include "M3DKernel/M3DKernelExport.h"

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <list>
#include "M3DKernel/utils/multithread/Event.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "M3DKernel/utils/multithread/stack/StackAction.h"

// ***************************************************************************
// Declarations
// ***************************************************************************

class M3DKERNEL_API ActionStack
{
public:
	// *********************************************************************
	// Constructors / Destructor
	// *********************************************************************
	// Default constructor
	ActionStack();

	// Destructor
	virtual ~ActionStack(void);

	// *********************************************************************
	// Accessors
	// *********************************************************************

	// Get extern synchro event
	CEvent& GetExternSynchroEvent() { return m_externSynchroEvent; };

	//  Get intern synchro event
	CEvent& GetInternSynchroEvent() { return m_internSynchroEvent; };

	// Get input event
	CEvent& GetInputEvent() { return m_inputEvent; };

	// Has remaining actions
	virtual bool HasScheduledActions() const;

	// Get nb remaining actions
	virtual int GetStackSize() const;

	virtual bool GetEnabled() const { return m_bEnabled; };
	virtual void Enable() { m_bEnabled = true; };
	virtual void Disable() { m_bEnabled = false; };

	// *********************************************************************
	// Methods
	// *********************************************************************

	//add action
	virtual void ScheduleAction(StackAction* pAction);

	//return the first Action
	virtual StackAction* PopAction();

	//wait the stack size is under the limit
	virtual void WaitStackLimit(int stackLimit);

	//wait every treatment in the stack is finished
	virtual void WaitFinished();

	//Pause the execution of the input for synchro purpose
	virtual void PauseExecution();

	//Resume the execution of the input for synchro purpose
	virtual void ResumeExecution();

	// Reset
	virtual void Reset();
	
	/// Check if thread is active
	virtual bool IsAlive();

	/// Chech if action stack is active
	virtual bool IsActive();

	// Launch the worker thread
	virtual void Launch();

	/// Stop the worker thread, and wait for end
	void Stop();

	/// Method tio run in thread
	unsigned int Run();
	
protected:

	// *********************************************************************
	// Accessors
	// *********************************************************************
	std::list<StackAction*>& GetActionStack() { return m_actionStack; };
	const std::list<StackAction*>& GetActionStack() const { return m_actionStack; };

	//get stack size event
	CEvent& GetStackSizeEvent() { return m_stackSizeEvent; };


private:
	class Impl;
	Impl * impl;

	// Disable copy
	ActionStack(const ActionStack &) {}
	ActionStack & operator=(const ActionStack &) { return *this; }

	std::list<StackAction*> m_actionStack;

	CEvent m_inputEvent;
	CEvent m_stackSizeEvent;
	CEvent m_externSynchroEvent;
	CEvent m_internSynchroEvent;

	CRecursiveMutex m_lock;

	bool m_bEnabled;
};

