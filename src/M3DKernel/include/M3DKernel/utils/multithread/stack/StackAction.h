// -*- C++ -*-
// ****************************************************************************
// Class: StackAction
//
// Description: Base classe for action to be stacked
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Sept 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once
#include "M3DKernel/M3DKernelExport.h"

// ***************************************************************************
// Dependences
// ***************************************************************************

// ***************************************************************************
// Declarations
// ***************************************************************************
class M3DKERNEL_API StackAction
{
public:
	// *********************************************************************
	// Constructors / Destructor
	// *********************************************************************
	// d�fault constructor
	StackAction();

	// Destructor
	virtual ~StackAction(void);

	// *********************************************************************
	// Accessors
	// *********************************************************************

	// *********************************************************************
	// Methods
	// *********************************************************************

	//process
	virtual void Process() = 0;

private:
};

