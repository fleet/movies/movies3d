#ifndef OVERLOADABLE_H
#define OVERLOADABLE_H

// Classe utiliatire pour permettre de surcharge une valeur par une valeur custom
template<typename T>
struct overloadable
{
	/// Valeur orignale
	T originalValue;

	/// Valeur originale ou surchargée
	T overloadValue;

	inline operator T() const { return overloadValue; };

	inline T get() const { return overloadValue; }

	inline void setValue(const T & val)
	{
		originalValue = val;
		overloadValue = val;
	}

	inline overloadable<T> operator=(const T & val)
	{
		setValue(val);
		return *this;
	}

	inline overloadable<T> operator=(const overloadable<T> & other)
	{
		originalValue = other.originalValue;
		overloadValue = other.overloadValue;
		return *this;
	}
};


#endif