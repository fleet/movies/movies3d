// -*- C++ -*-
// ****************************************************************************
// Class: CSocketCore
//
// Description: Classe de base permettant de centraliser la gestion
// des ressources li�es � l'utilisation des sockets
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Avril 2010
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "M3DKernel/M3DKernelExport.h"

// ***************************************************************************
// Declarations
// ***************************************************************************

class M3DKERNEL_API CSocketCore
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	CSocketCore();

	// Destructeur
	virtual ~CSocketCore();

private:
    class Impl;
    Impl * impl;

	// Initialisation de l'api socket
	void InitializeSocketAPI();

	// Nettoyage de l'api
	void CleanSocketAPI();
};

