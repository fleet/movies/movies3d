#pragma once
#include "M3DKernel/M3DKernelExport.h"
#include "M3DKernel/utils/stats/TimeCountStat.h"
#include <map>
#include <string>
#include "M3DKernel/utils/multithread/RecursiveMutex.h"

/// this class is a named time counter container
typedef std::map<std::string, TimeCountStat> MapNameTimeCounter;
class M3DKERNEL_API RecordedTimeCounter
{
public:
	RecordedTimeCounter();
	virtual ~RecordedTimeCounter(void);

	void AddTimeCounter(const char *Name, TimeCounter &refCount);
	void ClearAll();
	size_t GetObjectCount(void) const;
	bool GetObjectDescWithIndex(unsigned int idx, char*Name, unsigned int maxDescLen, TimeCountStat &TimeDesc);
private:
	MapNameTimeCounter m_mapNamed;
	CRecursiveMutex m_Lock;
};
