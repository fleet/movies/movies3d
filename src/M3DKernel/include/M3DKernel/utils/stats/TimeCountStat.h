#pragma once

#include "M3DKernel/utils/stats/TimeCounter.h"
#include "M3DKernel/M3DKernelExport.h"

class M3DKERNEL_API TimeCountStat
{
public:
	TimeCountStat();
	TimeCountStat(TimeCount ref);
	~TimeCountStat();

	void SetTime(TimeCount a);
	TimeCount m_Min;
	TimeCount m_Max;
	TimeCount m_Time;
};
