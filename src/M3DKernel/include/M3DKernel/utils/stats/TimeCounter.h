#pragma once

#include "M3DKernel/M3DKernelExport.h"
typedef int TimeCount;

class M3DKERNEL_API TimeCounter
{
public:
	TimeCounter();
	TimeCounter(const TimeCounter&src);

	~TimeCounter();

	void StartCount();
	void Reset();

	TimeCount StopCount();

	/*
	* Renvoi le temps ecoule en mseconds
	*/
	TimeCount getTime() const;

	TimeCounter& operator=(const TimeCounter&src);

private:
	class Impl;
	Impl * impl;
};
