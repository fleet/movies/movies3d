#pragma once

#include "M3DKernel/M3DKernelExport.h"

#include <chrono>
#include <string>

namespace timeutils {
	// Date Format
	constexpr const char* DATE_FORMAT = "%Y-%m-%dT%H:%M:%SZ";

	/// Converti un std::chrono::time_point en String UTC
	M3DKERNEL_API std::string timePointToStringUTC(const std::chrono::time_point<std::chrono::system_clock>& date, const char* dateFormat = DATE_FORMAT);

	/// Converti un std::chrono::time_point en String Local
	M3DKERNEL_API std::string timePointToStringLocal(const std::chrono::time_point<std::chrono::system_clock>& date, const char* dateFormat = DATE_FORMAT);

	/// Converti un String UTC en std::chrono::time_point
	M3DKERNEL_API std::chrono::time_point<std::chrono::system_clock> timePointFromStringUTC(const std::string& dateStr, const char* dateFormat = DATE_FORMAT);

	/// Converti un String Local en std::chrono::time_point
	M3DKERNEL_API std::chrono::time_point<std::chrono::system_clock> timePointFromStringLocal(const std::string& dateStr, const char* dateFormat = DATE_FORMAT);

	/// Converti un tm (UTC) en time_t
	time_t utcTimeT(const tm& utcTm);

	/// Calcul du nombre d'ann�e bisextiles depuis l'an 0
	unsigned int countLeapYearsFrom0(const unsigned int& year);

	/// Calcul du nombre d'ann�e bisextiles depuis l'an 0
	unsigned int countLeapYearsFromEpoch(const unsigned int& year);

	/// Calcul du nombre d'ann�es bisextiles entre 2 dates
	unsigned int countLeapYears(const unsigned int & startYear, const unsigned int & endYear);

}
