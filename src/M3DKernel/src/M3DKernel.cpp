// kernel.cpp�: d�finit le point d'entr�e pour l'application DLL.
//
#ifdef _MANAGED
#pragma managed(push, off)
#endif
#include "M3DKernel/M3DKernel.h"

#ifdef WIN32
// Exclure les en-t�tes Windows rarement utilis�s
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		M3DKernel::FreeMemory();
		break;
	}
	return TRUE;
}

#else

__attribute__((destructor)) void dllUnload()
{
	M3DKernel::FreeMemory();
}

#endif

#include "M3DKernel/parameter/ParameterObject.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/utils/log/LoggerFactory.h"
#include "M3DKernel/utils/log/LogAppender.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/NavAttributes.h"
#include "M3DKernel/algorithm/base/EchoAlgorithmRoot.h"
#include "M3DKernel/algorithm/internal/AlgorithmAutoLength.h"
#include "M3DKernel/algorithm/internal/TimeShiftEval.h"
#include "M3DKernel/datascheme/MovESUMgr.h"
#include "M3DKernel/utils/carto/CartoTools.h"

M3DKernel* M3DKernel::m_pKernelInstance = 0;

M3DKernel* M3DKernel::GetInstance()
{
	if (!m_pKernelInstance)
	{
		m_pKernelInstance = new M3DKernel();
	}
	return m_pKernelInstance;
}

void M3DKernel::FreeMemory()
{
	if (m_pKernelInstance)
	{
		delete m_pKernelInstance;
		m_pKernelInstance = NULL;
	}
}

M3DKernel::M3DKernel()
{
	// Initialisation du système de log
	Log::LoggerFactory::GetInstance();
	Log::LogAppender::GetInstance();

	m_pHacObjectMgr = HacObjectMgr::Create();
    MovRef(m_pHacObjectMgr);

	m_pEchoAlgorithmRoot = EchoAlgorithmRoot::Create();
    MovRef(m_pEchoAlgorithmRoot);

	m_pInternalAlgorithmRoot = EchoAlgorithmRoot::Create();
    MovRef(m_pInternalAlgorithmRoot);

	m_pWriteAlgorithmRoot = EchoAlgorithmRoot::Create();
    MovRef(m_pWriteAlgorithmRoot);

	m_pAlgorithmAutoLength = AlgorithmAutoLength::Create();
	m_pAlgorithmAutoLength->AddInput(m_pInternalAlgorithmRoot);
    MovRef(m_pAlgorithmAutoLength);


	m_pTimeShiftAlg = TimeShiftEval::Create();
	m_pTimeShiftAlg->AddInput(m_pInternalAlgorithmRoot);
    MovRef(m_pTimeShiftAlg);

	// IPSIS - OTK - passage de l'ESU manager en membre de HACLoader
	m_pMovESUMgr = MovESUMgr::Create();
    MovRef(m_pMovESUMgr);

	// initialisation de l'outil de projection avec les param�tres de projection par d�faut
	CCartoTools::Initialize(GetRefKernelParameter());
}

M3DKernel::~M3DKernel()
{
    MovUnRefDelete(m_pAlgorithmAutoLength);
    MovUnRefDelete(m_pTimeShiftAlg);

    MovUnRefDelete(m_pWriteAlgorithmRoot);
    MovUnRefDelete(m_pEchoAlgorithmRoot);
    MovUnRefDelete(m_pInternalAlgorithmRoot);

    MovUnRefDelete(m_pHacObjectMgr);

	// IPSIS - OTK - passage de l'ESU manager en membre de HACLoader
    MovUnRefDelete(m_pMovESUMgr);

	Log::LogAppender::FreeMemory();
	Log::LoggerFactory::FreeMemory();
}

void M3DKernel::ClearObjectManager()
{
    MovUnRefDelete(m_pHacObjectMgr);
	m_pHacObjectMgr = HacObjectMgr::Create();
    MovRef(m_pHacObjectMgr);
}

void M3DKernel::UpdateKernelParameter(KernelParameter& a)
{
	Lock();
	this->GetRefKernelParameter() = a;
	// mise � jour des param�tres de projection
	CCartoTools::Initialize(a);
	Unlock();
}


#ifdef _MANAGED
#pragma managed(pop)
#endif
