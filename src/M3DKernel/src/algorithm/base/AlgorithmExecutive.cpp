/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		AlgorithmExecutive.cpp											  */
/******************************************************************************/
#include "M3DKernel/algorithm/base/AlgorithmExecutive.h"

#include "M3DKernel/algorithm/base/EchoAlgorithm.h"
#include "M3DKernel/algorithm/base/AlgorithmOutput.h"

#include <algorithm>

AlgorithmExecutive::AlgorithmExecutive(EchoAlgorithm* producer)
{
	assert(producer);
	AddAlgorithm(producer);
}

AlgorithmExecutive::~AlgorithmExecutive()
{
}

bool AlgorithmExecutive::HasAlgorithm(EchoAlgorithm * a_pAlgorithm, unsigned int &index) const
{
	for (unsigned int i = 0; i < m_executiveList.size(); ++i)
	{
		if (a_pAlgorithm == m_executiveList[i].m_pAlgorithm)
		{
			index = i;
			return true;
		}
	}
	return false;
}

void AlgorithmExecutive::RemoveAlgorithm(EchoAlgorithm * a_pAlgorithm)
{
	std::vector<EchoAlgorithm *> algoList;
	for (unsigned int i = 0; i < m_executiveList.size(); ++i)
	{
		auto executiveAlgo = m_executiveList[i].m_pAlgorithm;
		if (a_pAlgorithm != executiveAlgo)
		{
			// Retrait de la connexion si elle existe
			executiveAlgo->RemoveInput(a_pAlgorithm);
			algoList.push_back(executiveAlgo);
		}
	}
	RemoveAll();
	for (unsigned int i = 0; i < algoList.size(); ++i)
	{
		AddAlgorithm(algoList[i]);
	}
}

void AlgorithmExecutive::AddAlgorithm(EchoAlgorithm * a_pAlgorithm)
{
	if (a_pAlgorithm == nullptr)
		return;
	
	/// check if parents where added
	for (const auto& outputAlgo : a_pAlgorithm->getInputs())
	{
		if (outputAlgo)
		{
			unsigned int index;
			bool hasAlgo = HasAlgorithm(outputAlgo->getProducer(), index);
			if (!hasAlgo)
			{
				AddAlgorithm(outputAlgo->getProducer());
			}
		}
	}

	/// now add ourself
	unsigned int Depth = 1;
	for (const auto& outputAlgo : a_pAlgorithm->getInputs())
	{
		if (outputAlgo)
		{
			unsigned int index;
			bool hasAlgo = HasAlgorithm(outputAlgo->getProducer(), index);
			if (hasAlgo)
			{
				Depth = std::max<unsigned int>(m_executiveList[index].m_Depth + 1, Depth);
			}
			else
			{
				assert(0);
			}
		}
		else
		{
			//	assert(0);
		}
	}

	AlgorithmExecutiveValue pValue;
	pValue.m_Depth = Depth;
	pValue.m_pAlgorithm = a_pAlgorithm;

	unsigned int index;
	bool hasAlgo = HasAlgorithm(a_pAlgorithm, index);

	// already added
	if (hasAlgo)
	{
		m_executiveList[index] = pValue;
	}
	else 
	{
		m_executiveList.push_back(pValue);
	}

	Sort();
}

void AlgorithmExecutive::RemoveAll()
{
	m_executiveList.clear();
}

void AlgorithmExecutive::Sort()
{
	std::sort(m_executiveList.begin(), m_executiveList.end());
}

unsigned int AlgorithmExecutive::getNumberOfExecutiveAlgorithm() const
{
	return m_executiveList.size();
}

const AlgorithmExecutiveValue & AlgorithmExecutive::getExecutiveAlgorithm(unsigned int index) const
{
	return m_executiveList[index];
}
