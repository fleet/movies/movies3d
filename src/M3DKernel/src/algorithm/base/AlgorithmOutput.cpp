/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		AlgorithmOutput.cpp											  */
/******************************************************************************/
#include "M3DKernel/algorithm/base/AlgorithmOutput.h"

#include <cassert>

AlgorithmOutput::AlgorithmOutput(EchoAlgorithm* producer)
	: m_pProducer(producer)
{
	assert(producer);
}

AlgorithmOutput::~AlgorithmOutput()
{
}
