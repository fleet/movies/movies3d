/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		EchoAlgorithm.cpp											  */
/******************************************************************************/
#include "M3DKernel/algorithm/base/EchoAlgorithm.h"
#include "M3DKernel/algorithm/base/AlgorithmOutput.h"
#include "M3DKernel/algorithm/base/AlgorithmExecutive.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/utils/log/ILogger.h"

EchoAlgorithm::EchoAlgorithm(const std::string & name)
	: EchoAlgorithmInterface(name)
	, m_pExecutive(nullptr)
{
	m_pOutPut = new AlgorithmOutput(this);
}

EchoAlgorithm::~EchoAlgorithm()
{
	/// no add our connection to executive 
	AlgorithmExecutive* p = getExecutive();
	p->RemoveAlgorithm(this);

	// Retrait des liens avec les algorithmes inférieurs (copie de la liste des chained outputs, car RemoveInput altère cette liste)
	for (auto& chainedAlgo : std::set<EchoAlgorithm*>(getChainedAlgorithms())) {
		chainedAlgo->RemoveInput(this);
	}

	// Retrait des liens avec les algorithmes supérieurs (copie de la liste des inputs, car RemoveInput altère cette liste)
	for (auto& inputAlgo : std::set<AlgorithmOutput*>(getInputs()))
	{
		RemoveInput(inputAlgo);
	}

	if (m_pExecutive)
		MovUnRefDelete(m_pExecutive);

	if (m_pOutPut)
		delete m_pOutPut;
}

void EchoAlgorithm::setEnable(bool enable) 
{
	const auto stateHasChanged = getEnable() != enable;
	EchoAlgorithmInterface::setEnable(enable);

	if (stateHasChanged) 
	{
		// Notify other modules
		for (auto& chainedAlgo : m_chainedOutputAlgorithms)
		{
			chainedAlgo->onInputModuleEnableStateChange(this);
		}
	}
}

void EchoAlgorithm::onInputModuleEnableStateChange(EchoAlgorithm * algo)
{
}

void EchoAlgorithm::ProcessPingFanAdded(PingFan *pFan)
{
	bool needNewEsu = false;

	auto executive = getExecutive();
	const auto nbAlgos = executive->getNumberOfExecutiveAlgorithm();

	for (unsigned int idx = 0; idx < nbAlgos; ++idx)
	{
		const auto & v = executive->getExecutiveAlgorithm(idx);
		if (v.m_pAlgorithm)
		{
			if (v.m_pAlgorithm->getEnable() == false && v.m_pAlgorithm->tryStartModule(pFan->m_ObjectTime))
			{
				needNewEsu = true;
			}
		}
	}

	if (needNewEsu)
	{
		MovESUMgr * pMovESUMgr = M3DKernel::GetInstance()->getMovESUManager();
		pMovESUMgr->ForceNewESU();
	}

	for (unsigned int idx = 0; idx < nbAlgos; ++idx)
	{
		const auto & v = executive->getExecutiveAlgorithm(idx);
		if (v.m_pAlgorithm)
		{
			if (v.m_pAlgorithm->getEnable())
			{
				TimeCounter	timeCountPingFanAdded;
				timeCountPingFanAdded.StartCount();
				v.m_pAlgorithm->PingFanAdded(pFan);
				timeCountPingFanAdded.StopCount();
				std::string nameDesc = v.m_pAlgorithm->getName();
				nameDesc += " PingAdded";
				M3DKernel::GetInstance()->m_timeCounterRecord.AddTimeCounter(nameDesc.c_str(), timeCountPingFanAdded);
			}
		}
	}
}

void EchoAlgorithm::ProcessSounderChanged(std::uint32_t sounderId)
{
	for (unsigned int ind = 0; ind < getExecutive()->getNumberOfExecutiveAlgorithm(); ind++)
	{
		AlgorithmExecutiveValue v = getExecutive()->getExecutiveAlgorithm(ind);
		if (v.m_pAlgorithm)
		{
			v.m_pAlgorithm->SounderChanged(sounderId);
		}
	}
}

void EchoAlgorithm::ProcessSingleTargetAdded(std::uint32_t sounderId)
{
	for (unsigned int ind = 0; ind < getExecutive()->getNumberOfExecutiveAlgorithm(); ind++)
	{
		AlgorithmExecutiveValue v = getExecutive()->getExecutiveAlgorithm(ind);
		if (v.m_pAlgorithm)
		{
			v.m_pAlgorithm->SingleTargetAdded(sounderId);
		}
	}
}

void EchoAlgorithm::ProcessStreamClosed(const char *streamName)
{
	for (unsigned int ind = 0; ind < getExecutive()->getNumberOfExecutiveAlgorithm(); ind++)
	{
		AlgorithmExecutiveValue v = getExecutive()->getExecutiveAlgorithm(ind);
		if (v.m_pAlgorithm)
		{
			v.m_pAlgorithm->StreamClosed(streamName);
		}
	}
}

void EchoAlgorithm::ProcessStreamOpened(const char *streamName)
{
	for (unsigned int ind = 0; ind < getExecutive()->getNumberOfExecutiveAlgorithm(); ind++)
	{
		AlgorithmExecutiveValue v = getExecutive()->getExecutiveAlgorithm(ind);
		if (v.m_pAlgorithm)
		{
			v.m_pAlgorithm->StreamOpened(streamName);
		}
	}
}

void EchoAlgorithm::ProcessESUStarted(ESUParameter * pWorkingESU)
{
	for (unsigned int ind = 0; ind < getExecutive()->getNumberOfExecutiveAlgorithm(); ind++)
	{
		AlgorithmExecutiveValue v = getExecutive()->getExecutiveAlgorithm(ind);
		if (v.m_pAlgorithm)
		{
			v.m_pAlgorithm->ESUStart(pWorkingESU);
		}
	}
}

void EchoAlgorithm::ProcessESUEnded(ESUParameter * pWorkingESU, bool abort)
{
	for (unsigned int ind = 0; ind < getExecutive()->getNumberOfExecutiveAlgorithm(); ind++)
	{
		AlgorithmExecutiveValue v = getExecutive()->getExecutiveAlgorithm(ind);
		if (v.m_pAlgorithm)
		{
			v.m_pAlgorithm->ESUEnd(pWorkingESU, abort);
		}
	}
}

void EchoAlgorithm::ProcessTupleHeaderUpdate()
{
	for (unsigned int ind = 0; ind < getExecutive()->getNumberOfExecutiveAlgorithm(); ind++)
	{
		AlgorithmExecutiveValue v = getExecutive()->getExecutiveAlgorithm(ind);
		if (v.m_pAlgorithm)
		{
			v.m_pAlgorithm->TupleHeaderUpdate();
		}
	}
}

const std::set<AlgorithmOutput*>& EchoAlgorithm::getInputs() const 
{
	return m_inputAlgorithms;
}

const std::set<EchoAlgorithm *>& EchoAlgorithm::getChainedAlgorithms() const 
{
	return m_chainedOutputAlgorithms;
}

void EchoAlgorithm::AddInput(EchoAlgorithm* algo, bool addToExecutive)
{
	AddInput(algo->getOutPut(), addToExecutive);
}

void EchoAlgorithm::AddInput(AlgorithmOutput * algo, bool addToExecutive) 
{
	if (!algo)
	{
		M3D_LOG_WARN("M3DKernel.algorithm.EchoAlgorithm", "Connecting null output to algorithm");
	}

	m_inputAlgorithms.insert(algo);
	algo->getProducer()->m_chainedOutputAlgorithms.insert(this);

	/// no add our connection to executive 
	if (addToExecutive) 
	{
		AlgorithmExecutive* p = getExecutive();
		p->AddAlgorithm(this);
	}
}

void EchoAlgorithm::RemoveInput(EchoAlgorithm* algo)
{
	RemoveInput(algo->getOutPut());
}

void EchoAlgorithm::RemoveInput(AlgorithmOutput * algo) 
{
	algo->getProducer()->m_chainedOutputAlgorithms.erase(this);
	m_inputAlgorithms.erase(algo);
}

AlgorithmExecutive* EchoAlgorithm::getExecutive()
{
	if (m_pExecutive) {
		return m_pExecutive;
	}

	if (!m_inputAlgorithms.empty())
	{
		m_pExecutive = (*m_inputAlgorithms.begin())->getProducer()->getExecutive();
		MovRef(m_pExecutive);
		return m_pExecutive;		
	}

	m_pExecutive = AlgorithmExecutive::Create(this);
	MovRef(m_pExecutive);
	return m_pExecutive;
}

