/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		EchoAlgorithmInterface.cpp											  */
/******************************************************************************/
#include "M3DKernel/algorithm/base/EchoAlgorithmInterface.h"
#include "M3DKernel/utils/M3DStdUtils.h"
#include "M3DKernel/utils/log/ILogger.h"

EchoAlgorithmInterface::EchoAlgorithmInterface(const std::string& name)
	: m_Name(name)
	, m_Enable(true)
	, m_EnableCount(0)
	, m_startTime(nullptr)
{
}

EchoAlgorithmInterface::~EchoAlgorithmInterface()
{
}

bool EchoAlgorithmInterface::getEnable() const
{
	return m_Enable;
}

void EchoAlgorithmInterface::setEnable(bool enable)
{
	const bool stateChange = (m_Enable != enable);
	m_Enable = enable;
	if (stateChange) 
	{
		M3D_LOG_INFO("EchoAlgorithmInterface", Log::format("%s du module %s", enable ? "activation" : "désactivation", m_Name.c_str()));
		if (m_Enable) {
			++m_EnableCount;
		}
		onEnableStateChange();
	}
}

void EchoAlgorithmInterface::onEnableStateChange() 
{
}

void EchoAlgorithmInterface::setStartTime(HacTime* time)
{
	if (m_startTime != nullptr) {
		delete m_startTime;
	}
	m_startTime = time;
}

bool EchoAlgorithmInterface::tryStartModule(const HacTime & time)
{
	if (m_startTime != nullptr && *m_startTime <= time)
	{
		delete m_startTime;
		m_startTime = nullptr;
		setEnable(true);
		return true;
	}
	return false;
}
