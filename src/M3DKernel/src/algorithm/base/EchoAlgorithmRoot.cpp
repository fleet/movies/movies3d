/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		EchoAlgorithmRoot.cpp											  */
/******************************************************************************/

#include "M3DKernel/algorithm/base/EchoAlgorithmRoot.h"

EchoAlgorithmRoot::EchoAlgorithmRoot()
	: EchoAlgorithm("AlgorithmRoot")
{
}

EchoAlgorithmRoot::~EchoAlgorithmRoot()
{
}

void EchoAlgorithmRoot::PingFanAdded(PingFan *pFan)
{
}

void EchoAlgorithmRoot::SounderChanged(std::uint32_t sounderId)
{
}

void EchoAlgorithmRoot::StreamClosed(const char *streamName)
{
}

void EchoAlgorithmRoot::StreamOpened(const char *streamName)
{
}