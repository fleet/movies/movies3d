#include "M3DKernel/algorithm/internal/AlgorithmAutoLength.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/PingFanContainer.h"

AlgorithmAutoLength::AlgorithmAutoLength()
	: EchoAlgorithm("AutoLength Module")
{
}

void AlgorithmAutoLength::PingFanAdded(PingFan *pFan)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	if (pKernel->GetRefKernelParameter().getAutoLengthEnable())
	{		
		unsigned int numMax = pKernel->GetRefKernelParameter().getNbPingFanMax();
		unsigned int numRead = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(pFan->m_pSounder->m_SounderId)->GetObjectCount();
		if (numRead >= numMax) 
		{
			pKernel->GetRefKernelParameter().setNbPingFanMax(numMax + 1);
		}
	}
}

void AlgorithmAutoLength::SounderChanged(std::uint32_t sounderId)
{
}
void AlgorithmAutoLength::StreamClosed(const char *streamName)
{
}
void AlgorithmAutoLength::StreamOpened(const char *streamName)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	if (pKernel->GetRefKernelParameter().getAutoLengthEnable())
	{
		unsigned int numMax = 3;
		for (unsigned int i = 0; i < pKernel->getObjectMgr()->GetSounderDefinition().GetNbSounder(); i++)
		{
			std::uint32_t sounderId = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(i)->m_SounderId;
			unsigned int numRead = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(sounderId)->GetObjectCount();
			numMax = MAX(numMax, numRead);
		}
		pKernel->GetRefKernelParameter().setNbPingFanMax(numMax);
	}
}
