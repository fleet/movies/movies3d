#include "M3DKernel/algorithm/internal/MatlabAlgoStore.h"
#include "M3DKernel/algorithm/base/EchoAlgorithm.h"

#include "M3DKernel/utils/log/ILogger.h"

namespace
{
	constexpr const char * LoggerName = "M3DKernel.algorithm.internal.MatlabAlgoStore";
}

EchoAlgorithm *MatlabAlgoStore::GetAlgorithm(unsigned int index)
{
	if (index < m_CreatedAlgo.size())
		return m_CreatedAlgo[index];
	return NULL;
}

void MatlabAlgoStore::RemoveAlgo(unsigned int index)
{
	if (index >= m_CreatedAlgo.size())
	{
		M3D_LOG_ERROR(LoggerName, "Index out of Range");
		return;
	}
	EchoAlgorithm *pNewAl = m_CreatedAlgo[index];
	m_CreatedAlgo[index] = NULL;
	MovUnRefDelete(pNewAl);
}

MatlabAlgoStore::MatlabAlgoStore()
{
}

MatlabAlgoStore::~MatlabAlgoStore()
{
	for (unsigned int i = 0; i < m_CreatedAlgo.size(); i++)
	{
		RemoveAlgo(i);
	}
}

unsigned int MatlabAlgoStore::AddAlgo(EchoAlgorithm *pNewAl)
{
	MovRef(pNewAl);
	unsigned int id = m_CreatedAlgo.size();
	m_CreatedAlgo.push_back(pNewAl);
	return id;
}
