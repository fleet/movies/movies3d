#include "M3DKernel/algorithm/internal/TimeShiftEval.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/utils/log/ILogger.h"

namespace
{
	constexpr const char * LoggerName = "M3DKernel.algorithm.internal.TimeShiftEval";
}

TimeShiftEval::TimeShiftEval()
	: EchoAlgorithm("TimeShiftEval")
{
	m_MasterSounderId = 0;
	m_delayedMasterFrac = 2;
	m_meanMasterInterPingTime = TimeElapse(0);
	m_tolerance = 0.7;
	this->setEnable(false);
}

TimeShiftEval::~TimeShiftEval()
{
}

void TimeShiftEval::PingFanAdded(PingFan *pFan)
{
	if (!getEnable())
		return;

	M3DKernel *pKernel = M3DKernel::GetInstance();
	if (pFan->m_pSounder->m_SounderId != m_MasterSounderId)
	{
		Sounder *p = pFan->m_pSounder;
		TimeObjectContainer *pTimeFan = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(pFan->m_pSounder->m_SounderId);

		Sounder *pSounderMaster = pKernel->getObjectMgr()->GetLastValidSounder(m_MasterSounderId);
		TimeObjectContainer *pTimeMaster = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(m_MasterSounderId);
		if (pSounderMaster)
		{
			unsigned int numFanMaster = pTimeMaster->GetObjectCount();
			if (numFanMaster > 0)
			{
				PingFan *pingFanMaster = (PingFan*)pTimeMaster->GetDatedObject(numFanMaster - 1);
				HacTime datePingFanMaster = pingFanMaster->m_ObjectTime;


				unsigned int numFan = pTimeFan->GetObjectCount();
				if (m_meanMasterInterPingTime > 0 && numFan > 2)
				{
					PingFan *pFan = (PingFan *)pTimeFan->GetDatedObject(numFan - 1);
					PingFan *pFanPrevious = (PingFan *)pTimeFan->GetDatedObject(numFan - 2);

					if (pFan)
					{
						if (p->m_sounderComputeData.m_timeShift.m_timeElapse != 0 &&
							pFanPrevious->m_ObjectTime > pingFanMaster->m_ObjectTime &&
							pFan->m_ObjectTime > pFanPrevious->m_ObjectTime
							)
						{
							M3D_LOG_WARN(LoggerName, "hole in multibeam pingFan");
							return;
						}

						/// check time 
						HacTime myDate = pFan->m_ObjectTime;
						TimeElapse elapsedTime;

						//elapse time 
						elapsedTime = datePingFanMaster - myDate;

						if ((/*elapsedTime > m_meanMasterInterPingTime && */elapsedTime.m_timeElapse > 0)
							|| (elapsedTime.m_timeElapse < 0 && elapsedTime.m_timeElapse < -m_meanMasterInterPingTime.m_timeElapse))
						{
							TimeElapse correction;

							/// on attribue � la nouvelle date la date du pingME70+ le temps interping
							correction = elapsedTime + TimeElapse(m_meanMasterInterPingTime.m_timeElapse / m_delayedMasterFrac); ;

							M3D_LOG_WARN(LoggerName, "Probable Time Shift detected");
							M3D_LOG_WARN(LoggerName, "Correcting time shift");
							M3D_LOG_WARN(LoggerName, Log::format("Correction is %f s\n", correction.m_timeElapse / 10000.0));

							p->m_sounderComputeData.m_timeShift = p->m_sounderComputeData.m_timeShift + correction;

						}

					}
				}
			}
		}
	}
	else 
	{
		EvaluateMeanMasterInterPing();
	}
}

void TimeShiftEval::EvaluateMeanMasterInterPing()
{
	unsigned int nbSample = 10;
	M3DKernel *pKernel = M3DKernel::GetInstance();
	Sounder *pSounderMaster = pKernel->getObjectMgr()->GetLastValidSounder(m_MasterSounderId);
	TimeObjectContainer *pTime = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(m_MasterSounderId);
	if (!pSounderMaster)
		return;
	
	unsigned int numFan = pTime->GetObjectCount();
	if (numFan > 1)
	{
		PingFan *pFan = (PingFan *)pTime->GetDatedObject(numFan - 1);
		PingFan *pFanPrevious = (PingFan *)pTime->GetDatedObject(numFan - 2);
		TimeElapse elapsedTime = pFan->m_ObjectTime - pFanPrevious->m_ObjectTime;
		if (m_meanMasterInterPingTime.m_timeElapse != 0)
		{
			if (elapsedTime.m_timeElapse < 0)
			{
				M3D_LOG_ERROR(LoggerName, "Error PingFan time is decreasing going to past ??");
				elapsedTime.m_timeElapse = -elapsedTime.m_timeElapse;
			}
			m_meanMasterInterPingTime = (((elapsedTime) < (m_meanMasterInterPingTime)) ? (elapsedTime) : (m_meanMasterInterPingTime));

		}
		else
		{
			m_meanMasterInterPingTime = elapsedTime;
		}
	}
}

void TimeShiftEval::SounderChanged(std::uint32_t sounderId)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	Sounder *p = pKernel->getObjectMgr()->GetLastValidSounder(sounderId);
	if (!p->m_isMultiBeam)
	{
		m_MasterSounderId = sounderId;
	}
}

void TimeShiftEval::StreamClosed(const char *streamName)
{
}

void TimeShiftEval::StreamOpened(const char *streamName)
{
	m_meanMasterInterPingTime.m_timeElapse = 0;
}
