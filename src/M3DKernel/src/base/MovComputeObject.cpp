#include "M3DKernel/base/MovComputeObject.h"

MovComputeObject::MovComputeObject()
	: MovObject()
	, m_bDirty(true)
	, m_bIsComplete(false)
{
}

MovComputeObject::~MovComputeObject()
{
}
