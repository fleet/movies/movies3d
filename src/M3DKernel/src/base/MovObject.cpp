#include "M3DKernel/base/MovObject.h"

MovObject::~MovObject()
{
	assert(m_refCount == 0);
}

void MovObject::lRef(MovObject* ptr)
{
	m_ptr = ptr;
	if (m_ptr != 0)
	{
		assert(m_refCount < (std::numeric_limits<short>::max() - 1));
		m_refCount++;
	}
}

std::uint64_t MovObject::lUnRef(MovObject* ptr)
{
	if (m_ptr == 0)
		return 1;
	m_refCount--;
	assert(m_refCount >= 0);
	return m_refCount;
}

void MovObject::Delete()
{
	delete this;
}

void MovObject::Release(MovObject ** pt)
{
	if (pt && *pt)
	{
		MovUnRefDelete((*pt))
		(*pt) = 0;
	}
}

#pragma message(" _________________________________________________ ")
#pragma message("!                                                 !")
#pragma message("! Passer le construteur par recopie en private,   !")
#pragma message("! ainsi pour assurer de n'avoir que des pointeurs !")
#pragma message("!_________________________________________________!")
