﻿#include "M3DKernel/config/MovConfig.h"

#include "M3DKernel/utils/log/ILogger.h"

// XERCES
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLDouble.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

XERCES_CPP_NAMESPACE_USE

using namespace BaseKernel;

namespace
{
	constexpr const char * LoggerName = "M3DKernel.config.MovConfig";
}

namespace BaseKernel
{
	namespace details
	{
		template<typename T>
		struct Transcoder
		{
			static void FromXMLch(T * aValue, const XMLCh &aStr)
			{
				char * xmlch = XMLString::transcode(&aStr);
				std::string str(xmlch);
				XMLString::release(&xmlch);
				std::stringstream ss(str);
				ss >> *aValue;
			}
		};

		// OTK - FAE042 - spécialisation de la méthode FromXMLch dans le cas
		// des chaines de caractères (conservation des espaces en début de chaine)
		template<>
		struct Transcoder<std::string>
		{
			static void FromXMLch(std::string * aValue, const XMLCh &aStr)
			{
				char * xmlch = XMLString::transcode(&aStr);
				*aValue = xmlch;
				XMLString::release(&xmlch);
			}
		};
	}

	// conversion des XMLch en données de base
	template <typename T>
	static void FromXMLch(T * aValue, const XMLCh &aStr)
	{
		details::Transcoder<T>::FromXMLch(aValue, aStr);
	}
	
	static xercesc::DOMDocument * CreateDOMDocument()
	{
		static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
		auto impl = DOMImplementationRegistry::getDOMImplementation(gLS);
		return impl->createDocument();
	}

	static void WriteDOMDocument(xercesc::DOMDocument * doc, const std::string & filePath)
	{
		// creation du DOMWriter
		static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
		auto impl = DOMImplementationRegistry::getDOMImplementation(gLS);
		DOMLSSerializer* theSerializer = ((DOMImplementationLS*)impl)->createLSSerializer();
		// Pour avoir un fichier formattÃ© (sinon tout est sur une ligne
		// et donc pas trÃ¨s lisible avec un editeur de texte basique)
		theSerializer->setNewLine(XMLString::transcode("\r\n"));
		theSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);

		//définition de la sortie
		XMLCh tempStr[255];
		XMLString::transcode(filePath.c_str(), tempStr, 254);

		// Ecriture du fichier de configuration
		try
		{
			theSerializer->writeToURI(doc, tempStr);
		}
		catch (const XMLException& toCatch)
		{
			std::string str = "MovConfig  XMLException : ";
			char * xmlch = XMLString::transcode(toCatch.getMessage());
			str.append(xmlch);
			XMLString::release(&xmlch);
			M3D_LOG_WARN(LoggerName, str.c_str());
		}
		catch (const DOMException& toCatch)
		{
			std::string str = "MovConfig  DOMException : ";
			char * xmlch = XMLString::transcode(toCatch.getMessage());
			str.append(xmlch);
			XMLString::release(&xmlch);
			M3D_LOG_WARN(LoggerName, str.c_str());
		}
		catch (...)
		{
			// unexpected exception
			M3D_LOG_WARN(LoggerName, "MovConfig  Unexpected Exception");
		}
	}
}

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

class XercesSystem
{
public:
	XercesSystem()
	{
	}

	void Initialize()
	{
		// initialisation de Xerces
		try 
		{
			XMLPlatformUtils::Initialize();
		}
		catch (const XMLException&) 
		{
			M3D_LOG_WARN(LoggerName, "MovConfig  Error XML Initialisation");
		}
	}

	~XercesSystem()
	{
		// Désallocation du système xerces
		XMLPlatformUtils::Terminate();
	}
};

static XercesSystem xercesSystem;

// Constructeur par défaut
MovConfig::MovConfig()
{
	xercesSystem.Initialize();
}

// Destructeur
MovConfig::~MovConfig()
{
}

// *********************************************************************
// MovConfigFile
// *********************************************************************
MovConfigFile::MovConfigFile()
	: MovConfig()
	, m_FilePath("C:\\M3D_config.xml")
	, m_pCurrentDOMDocument(nullptr)
	, m_pCurrentDOMNode(nullptr)
	, m_pParser(nullptr)
{
}

MovConfigFile::~MovConfigFile()
{
}

// eParameterModule
void MovConfigFile::SerializeData(ParameterModule* parameterModule, ParameterDataType paramType)
{
	assert(paramType == eParameterModule);

	if (m_pCurrentDOMDocument != nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigFile  SerializeData failed, document already open");
		return;
	}

	m_pCurrentDOMDocument = CreateDOMDocument();

	// conversion du nom du parameterModule en XMLCh
	XMLCh tempStr[100];
	XMLString::transcode(parameterModule->GetName().c_str(), tempStr, 99);
	m_pCurrentDOMNode = m_pCurrentDOMDocument->appendChild(m_pCurrentDOMDocument->createElement(tempStr));
}

bool MovConfigFile::DeSerializeData(ParameterModule* parameterModule, ParameterDataType paramType)
{
	assert(paramType == eParameterModule);
	
	if (m_pCurrentDOMDocument != nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigFile  DeSerializeData failed, document already open");
		return false;
	}
	
	// on renvoie faux si on n'a pas trouvé l'item dans la config
	bool result = false;
	try 
	{
		std::string filePath = GetFilePath();
		
		// Parser XML
		m_pParser = new XercesDOMParser();
		m_pParser->parse(filePath.c_str());
		m_pCurrentDOMDocument = m_pParser->getDocument();

		if (m_pCurrentDOMDocument)
		{
			m_pCurrentDOMNode = m_pCurrentDOMDocument->getDocumentElement();
			result = true;
		}
		else
		{
			M3D_LOG_WARN(LoggerName, Log::format("MovConfig  Fichier de configuration introuvable : %s", filePath.c_str()));
		}
	}
	catch (const XMLException& ex) 
	{
		std::string str = "MovConfig  XMLException : ";
		char * xmlch = XMLString::transcode(ex.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_WARN(LoggerName, str.c_str());
	}
	catch (const DOMException& ex) 
	{
		std::string str = "MovConfig  DOMException : ";
		char * xmlch = XMLString::transcode(ex.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_WARN(LoggerName, str.c_str());
	}
	catch (...) 
	{
		M3D_LOG_WARN(LoggerName, "MovConfig  UnkownException");
	}

	return result;
}

// eParameterObject
void MovConfigFile::SerializeData(ParameterObject* parameterObject, ParameterDataType paramType, const char * name)
{
	// Ã  priori un parameterObject ne peut etre serializÃ© que dans un ParameterModule
	if (m_pCurrentDOMDocument == nullptr || m_pCurrentDOMNode == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigFile  SerializeData failed, no document");
		return;
	}

	assert(m_pCurrentDOMDocument);
	assert(m_pCurrentDOMNode);

	// conversion du nom du parameterObject en XMLCh
	XMLCh tempStr[100];
	XMLString::transcode(name, tempStr, 99);
	// creation du nouveau noeud
	DOMNode * domNode = m_pCurrentDOMDocument->createElement(tempStr);
	// ajout du noeud dans l'arbre
	m_pCurrentDOMNode->appendChild(domNode);
	// on travail a present sur ce nouveau noeud jusqu'au pushback
	m_pCurrentDOMNode = domNode;
}

bool MovConfigFile::DeSerializeData(ParameterObject* parameterObject, ParameterDataType paramType, const char * name)
{
	if (m_pCurrentDOMDocument == nullptr || m_pCurrentDOMNode == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigFile  DeSerializeData failed, no document");
		return false;
	}

	assert(m_pCurrentDOMDocument);
	assert(m_pCurrentDOMNode);

	// on renvoie faux si on n'a pas trouvé l'item dans la config
	bool result = false;

	XMLCh * xmlCh = XMLString::transcode(name);

	// parcours des noeuds fils pour voir si on trouve la donnée qui nous intèresse
	auto nodeList = m_pCurrentDOMNode->getChildNodes();
	const unsigned int nbNodes = nodeList->getLength();
	for (unsigned int i = 0; i < nbNodes; ++i)
	{
		auto node = nodeList->item(i);
		if (node->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			if (XMLString::compareString(node->getNodeName(), xmlCh) == 0)
			{
				m_pCurrentDOMNode = node;
				result = true;
				break;
			}
		}
	}
	XMLString::release(&xmlCh);

	return result;
}

void MovConfigFile::Serialize(const std::string & str, ParameterDataType paramType, const char * name)
{
	if (m_pCurrentDOMDocument == nullptr || m_pCurrentDOMNode == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigFile  SerializeData failed, no document");
		return;
	}

	assert(m_pCurrentDOMDocument);
	assert(m_pCurrentDOMNode);

	// Creation du noeud correspondant à la donnée
	XMLCh tempStr[100];
	XMLString::transcode(name, tempStr, 99);
	DOMNode * pDOMNode = m_pCurrentDOMDocument->createElement(tempStr);

	// affectation de la valeur
	XMLString::transcode(str.c_str(), tempStr, 99);
	pDOMNode->setTextContent(tempStr);

	m_pCurrentDOMNode->appendChild(pDOMNode);
}

bool MovConfigFile::DeSerialize(std::string & str, ParameterDataType paramType, const char * name)
{
	if (m_pCurrentDOMDocument == nullptr || m_pCurrentDOMNode == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigFile  DeSerializeData failed, no document");
		return false;
	}

	assert(m_pCurrentDOMDocument);
	assert(m_pCurrentDOMNode);

	// on renvoi vrai si l'item a été trouvé dans la config
	bool result = false;

	XMLCh * xmlCh = XMLString::transcode(name);

	// parcours des noeuds fils pour voir si on trouve la donnée qui nous intèresse
	auto nodeList = m_pCurrentDOMNode->getChildNodes();
	const unsigned int nbNodes = nodeList->getLength();
	for (unsigned int i = 0; i < nbNodes; ++i)
	{
		auto node = nodeList->item(i);
		if (node->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			if (XMLString::compareString(node->getNodeName(), xmlCh) == 0)
			{
				// lecture affectation de la valeur
				FromXMLch(&str, *nodeList->item(i)->getTextContent());
				result = true;

				m_pCurrentDOMNode->removeChild(node);

				break;
			}
		}
	}

	XMLString::release(&xmlCh);

	
	return result;
}

void MovConfigFile::SerializePushBack()
{
	if (m_pCurrentDOMDocument == nullptr || m_pCurrentDOMNode == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigFile  SerializePushBack failed, no document");
		return;
	}

	assert(m_pCurrentDOMDocument);
	assert(m_pCurrentDOMNode);

	// S'il s'agit du dernier niveau, on écrit le fichier
	if (m_pCurrentDOMNode == m_pCurrentDOMDocument->getDocumentElement())
	{
		// Ecriture du document
		std::string filePath = GetFilePath();
		WriteDOMDocument(m_pCurrentDOMDocument, filePath);

		// liberation des ressources
		m_pCurrentDOMDocument = nullptr;
		m_pCurrentDOMNode = nullptr;
	}
	else
	{
		// si ce n'est pas le dernier élément, c'est qu'on fait un pushback sur un eParameterObject,
		// on remonte donc d'un cran
		m_pCurrentDOMNode = m_pCurrentDOMNode->getParentNode();
	}
}

void MovConfigFile::DeSerializePushBack()
{
	if (m_pCurrentDOMDocument == nullptr)
	{
		return;
	}

	if (m_pCurrentDOMNode == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigFile  DeSerializePushBack failed, current node not valid !");
		return;
	}

	assert(m_pCurrentDOMDocument);
	assert(m_pCurrentDOMNode);

	// S'il s'agit du dernier niveau, on a fini d'écrire le module,
	// on prépare donc la prochaine lecture / écriture
	if (m_pCurrentDOMNode == m_pCurrentDOMDocument->getDocumentElement())
	{
		m_pCurrentDOMDocument = nullptr;
		m_pCurrentDOMNode = nullptr;
	}
	else
	{
		DOMNode * nodeTmp = m_pCurrentDOMNode;
		m_pCurrentDOMNode = m_pCurrentDOMNode->getParentNode();
		m_pCurrentDOMNode->removeChild(nodeTmp);
	}
}

// *********************************************************************
// MovConfigDirectory
// *********************************************************************

MovConfigDirectory::MovConfigDirectory()
	: MovConfig()
	, m_Path("C:")
	, m_Prefix("M3D_config_")
	, m_pCurrentFile(nullptr)
{
}

MovConfigDirectory::~MovConfigDirectory()
{
	if (m_pCurrentFile != nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigDirectory  destructor, file still open");
		delete m_pCurrentFile;
		m_pCurrentFile = nullptr;
	}
}


// *********************************************************************
// Traitements
// *********************************************************************

// eParameterModule
//----------------------
void MovConfigDirectory::SerializeData(ParameterModule* parameterModule, ParameterDataType paramType)
{
	assert(paramType == eParameterModule);

	if (m_pCurrentFile != nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigDirectory  SerializeData, file already open");
		delete m_pCurrentFile;
	}

	m_pCurrentFile = new MovConfigFile();
	std::string filePath = ConstructConfigurationFilePath(parameterModule->GetName());
	m_pCurrentFile->SetFilePath(filePath);
	m_pCurrentFile->SerializeData(parameterModule, paramType);
}


bool MovConfigDirectory::DeSerializeData(ParameterModule* parameterModule, ParameterDataType paramType)
{
	assert(paramType == eParameterModule);

	if (m_pCurrentFile != nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigDirectory  DeSerializeData, file already open");
		delete m_pCurrentFile;
	}

	m_pCurrentFile = new MovConfigFile();
	std::string filePath = ConstructConfigurationFilePath(parameterModule->GetName());
	m_pCurrentFile->SetFilePath(filePath);
	return m_pCurrentFile->DeSerializeData(parameterModule, paramType);
}

// eParameterObject
//----------------------
void MovConfigDirectory::SerializeData(ParameterObject* parameterObject, ParameterDataType paramType, const char * name)
{
	if (m_pCurrentFile == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigDirectory  SerializeData failed, no file");
		return;
	}

	m_pCurrentFile->SerializeData(parameterObject, paramType, name);
}

bool MovConfigDirectory::DeSerializeData(ParameterObject* parameterObject, ParameterDataType paramType, const char * name)
{
	if (m_pCurrentFile == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigDirectory  DeSerializeData failed, no file");
		return false;
	}

	return m_pCurrentFile->DeSerializeData(parameterObject, paramType, name);
}

void MovConfigDirectory::Serialize(const std::string & str, ParameterDataType paramType, const char * name)
{
	if (m_pCurrentFile == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigDirectory  Serialize failed, no file");
		return;
	}

	m_pCurrentFile->Serialize(str, paramType, name);
}

bool MovConfigDirectory::DeSerialize(std::string & str, ParameterDataType paramType, const char * name)
{
	if (m_pCurrentFile == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigDirectory  DeSerialize failed, no file");
		return false;
	}

	return m_pCurrentFile->DeSerialize(str, paramType, name);
}


// Fin de blocs
//----------------------
void MovConfigDirectory::SerializePushBack()
{
	if (m_pCurrentFile == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigDirectory  SerializePushBack failed, no file");
		return;
	}

	m_pCurrentFile->SerializePushBack();

	if (!m_pCurrentFile->IsValid())
	{
		delete m_pCurrentFile;
		m_pCurrentFile = nullptr;
	}
}

void MovConfigDirectory::DeSerializePushBack()
{
	if (m_pCurrentFile == nullptr)
	{
		M3D_LOG_WARN(LoggerName, "MovConfigDirectory  DeSerializePushBack failed, no file");
		return;
	}

	m_pCurrentFile->DeSerializePushBack();

	if (!m_pCurrentFile->IsValid())
	{
		delete m_pCurrentFile;
		m_pCurrentFile = nullptr;
	}
}

// *********************************************************************
// Traitements privÃ©s
// *********************************************************************
std::string MovConfigDirectory::ConstructConfigurationFilePath(std::string parameterModuleName) const
{
	std::string result;
	result = m_Path + "/" + m_Prefix + "/" + parameterModuleName + ".xml";
	return result;
}
