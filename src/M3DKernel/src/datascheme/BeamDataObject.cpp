#include "M3DKernel/datascheme/BeamDataObject.h"

BeamDataObject::BeamDataObject(void)
{
	m_isValid = true;
	m_timeFraction = 0;
	m_timeCPU = 0;
	m_hacChannelId = 0;
	m_bottomRange = FondNotFound;
	m_bottomWasFound = false;
	m_hasDeviation = false;
	m_compensateHeave = 0;
	m_compensateHeaveMovies = 0;
	m_pSamples = NULL;
}

BeamDataObject::~BeamDataObject(void)
{
	MovUnRefDelete(m_pSamples);
}

int BeamDataObject::capacity() const
{
	return m_pSamples->m_amplitudes.capacity();
}

void BeamDataObject::reserve(unsigned int a)
{
	m_pSamples->m_amplitudes.reserve(a);
	m_pSamples->m_angles.reserve(a);
}

void BeamDataObject::SetPositionInFileRun(const std::string & fileName, std::uint32_t streamOffset)
{
	m_runFileName = fileName;
	m_streamPosInFile = streamOffset;
}

void BeamDataObject::GetPositionInFileRun(std::string & fileName, std::uint32_t & streamOffset)
{
	fileName = m_runFileName;
	streamOffset = m_streamPosInFile;
}

void BeamDataObject::SetRunFileName(const std::string & filename) {
	m_runFileName = filename;
}

const std::string & BeamDataObject::GetRunFileName() const {
	return m_runFileName;
}

void BeamDataObject::push_back_power(double power)
{ 
	if (m_power.empty())
		m_power.reserve(m_pSamples->m_amplitudes.capacity());

	m_power.push_back(power); 
}

double BeamDataObject::computeAveragePower(bool& hasValue, int firstEcho, int lastEcho) const
{ 
	hasValue = false;
	if (m_power.empty() || firstEcho > m_power.size() - 1)
		return 0.0;

	if (lastEcho < 0 || lastEcho > m_power.size() - 1)
		lastEcho = m_power.size() - 1;

	auto avg = 0.0;
	int nb = 0;
	auto lastIt = m_power.cbegin() + lastEcho + 1;
	for (auto it = m_power.cbegin() + firstEcho; it != lastIt; ++it)
	{
		avg += *it;
		++nb;
	}

	hasValue = nb > 0;

	if (hasValue)
		avg /= nb;

	return avg;
}
