#include "M3DKernel/datascheme/ComputedNavigation.h"

#include <memory.h>
#include <math.h>

#include "M3DKernel/datascheme/PingFan.h"

using namespace BaseMathLib;

ComputedNavigation::ComputedNavigation(void)
{
}

ComputedNavigation::~ComputedNavigation(void)
{
}

void ComputedNavigation::ComputeNavigation(PingFan* pFan, PingFan* pLastFan, double instantSpeed, bool temporalGap)
{
	/*
	if (isnan(instantSpeed))
	{
		instantSpeed = 0;
	}
	*/

	double cumulatedDistance = 0.0;
	Vector3D position(0, 0, 0);
	if (pLastFan != NULL)
	{
		position = pLastFan->m_relativePingFan.m_cumulatedNav;
		cumulatedDistance = pLastFan->m_relativePingFan.m_cumulatedDistance;
		TimeElapse InterPings = pFan->m_ObjectTime - pLastFan->m_ObjectTime;
		// OTK - 03/04/2009 - on g�re � pr�sent ca comme dans Movies+ : un saut temporel et pas en distance.
		//on met une s�curit� sur la distance parcourue entre deux pings dans le cas d'un saut temporel dans les donn�es
		//double Length=min(100,InterPings.m_timeElapse/10000.0*instantSpeed);
		if (temporalGap)
		{
			cumulatedDistance = 0;
		}
		else
		{
			Vector3D displacementV;
			double Length = InterPings.m_timeElapse / 10000.0*instantSpeed;
			displacementV = pLastFan->GetHeadingMatrixNav(pFan->GetFirstChannelId())*Vector3D(Length, 0, 0);
			position = displacementV + position;
			cumulatedDistance = Length + cumulatedDistance;
		}
	}

	pFan->m_relativePingFan.m_cumulatedNav = position;
	pFan->m_relativePingFan.m_cumulatedDistance = cumulatedDistance;
}
