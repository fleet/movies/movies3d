#include "M3DKernel/datascheme/CurrentSounderDefinition.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/TransformSet.h"
#include "M3DKernel/datascheme/PingFan.h"

CurrentSounderDefinition::CurrentSounderDefinition()
{

}
CurrentSounderDefinition::~CurrentSounderDefinition()
{
	while (GetNbSounder())
		RemoveSounderIdx((unsigned int)0);
}

void CurrentSounderDefinition::AddSounder(Sounder *ref)
{
	// OTK - FAE065 - si une association avec le m�me sounderID existe, on y ajoute
	// le nouveau sondeur. Sinon on cr�e une nouvelle association
	InternalAssociation* pAssoc = GetInternalAssociation(ref->m_SounderId);
	if (pAssoc == NULL)
	{
		// cr�ation de la nouvelle association
		InternalAssociation a;

		a.m_Sounders.push_back(ref);
		a.m_pFanMem = FanMemory::Create();
		a.m_pFanMem->SetMemoryLength(M3DKernel::GetInstance()->GetRefKernelParameter().getNbPingFanMax());

		MovRef(a.m_pFanMem);

		m_Stack.push_back(a);
	}
	else
	{
		// ajout du nouveau sondeur � l'association existante
		pAssoc->m_Sounders.push_back(ref);
	}

	MovRef(ref);
}
void CurrentSounderDefinition::RemoveSounder(Sounder *ref)
{
	std::vector<InternalAssociation>::iterator itr = m_Stack.begin();
	bool done = false;
	while (!done && itr != m_Stack.end())
	{
		size_t nbSound = (*itr).m_Sounders.size();
		for (size_t i = 0; i < nbSound && !done; i++)
		{
			if ((*itr).m_Sounders[i] == ref)
			{
				MovUnRefDelete(ref);
				(*itr).m_Sounders.erase((*itr).m_Sounders.begin() + i);
				if (nbSound == 1)
				{
					MovUnRefDelete((*itr).m_pFanMem);
					m_Stack.erase(itr);
				}
				// pour sortir des boucles
				done = true;
			}
		}
		if (!done)
		{
			itr++;
		}
	}
}
void CurrentSounderDefinition::RemoveSounderIdx(unsigned int SounderIndex)
{
	Sounder*p = GetSounder(SounderIndex);
	if (p)
	{
		RemoveSounder(p);
	}
}

Sounder *CurrentSounderDefinition::GetSounderWithId(std::uint32_t souderId)
{
	Sounder * result = NULL;
	InternalAssociation* pAssoc = GetInternalAssociation(souderId);
	if (pAssoc)
	{
		result = pAssoc->m_Sounders[pAssoc->m_Sounders.size() - 1];
	}
	return result;
}
// OTK - FAE065 - renvoie l'ensemble des sondeurs d'un ID donn�
std::vector<Sounder*> CurrentSounderDefinition::GetSoundersWithId(std::uint32_t souderId)
{
	std::vector<Sounder*> result;

	InternalAssociation* pAssoc = GetInternalAssociation(souderId);
	if (pAssoc != NULL)
	{
		size_t nbSounder = pAssoc->m_Sounders.size();
		for (size_t i = 0; i < nbSounder; i++)
		{
			result.push_back(pAssoc->m_Sounders[i]);
		}
	}

	return result;
}

FanMemory	*CurrentSounderDefinition::GetFanMemory(std::uint32_t idx) {
    if (idx < GetNbSounder())
        return m_Stack[idx].m_pFanMem;
    else
        return NULL;
}

FanMemory	*CurrentSounderDefinition::GetLinkedFanMemory(std::uint32_t souderId)
{
	FanMemory * result = NULL;
	InternalAssociation* pAssoc = GetInternalAssociation(souderId);
	if (pAssoc)
	{
		result = pAssoc->m_pFanMem;
	}
	return result;
}
FanMemory	*CurrentSounderDefinition::GetLinkedFanMemory(Sounder *pSound)
{
	FanMemory * result = NULL;
	std::vector<InternalAssociation>::iterator itr = m_Stack.begin();
	bool done = false;
	while (!done && itr != m_Stack.end())
	{
		size_t nbSound = (*itr).m_Sounders.size();
		for (size_t i = 0; i < nbSound && !done; i++)
		{
			if ((*itr).m_Sounders[i] == pSound)
			{
				result = (*itr).m_pFanMem;
				// pour sortir des boucles
				done = true;
			}
		}
		itr++;
	}
	return result;
}

Sounder *CurrentSounderDefinition::GetSounder(unsigned int SounderIndex) const
{
	if (SounderIndex < GetNbSounder())
		return m_Stack[SounderIndex].m_Sounders[m_Stack[SounderIndex].m_Sounders.size() - 1];
	else
		return NULL;
}

unsigned int CurrentSounderDefinition::GetNbSounder() const
{
	return (unsigned int)m_Stack.size();
}

void	CurrentSounderDefinition::ComputeMatrix() {
	std::vector<InternalAssociation>::iterator itr = m_Stack.begin();
	while (itr != m_Stack.end())
	{
		size_t nbSounder = (*itr).m_Sounders.size();
		for (size_t i = 0; i < nbSounder; i++)
		{
			(*itr).m_Sounders[i]->ComputeMatrix();
		}
		itr++;
	}
}

// OTK - FAE065 - recuperation de l'association d'un sounderID donn�
CurrentSounderDefinition::InternalAssociation* CurrentSounderDefinition::GetInternalAssociation(std::uint32_t sounderId)
{
	InternalAssociation* result = NULL;

	std::vector<InternalAssociation>::iterator itr = m_Stack.begin();
	while (itr != m_Stack.end() && !result)
	{
		if ((*itr).m_Sounders[0]->m_SounderId == sounderId)
		{
			result = &(*itr);
		}
		itr++;
	}

	return result;
}

// OTK - FAE065 - ajustement de la taille des transformmaps sur changement des param�tres Reader
void CurrentSounderDefinition::CheckAllSoundersReaderParameters()
{
	unsigned int nbAssocs = GetNbSounder();
	for (unsigned int i = 0; i < nbAssocs; i++)
	{
		size_t nbSounders = m_Stack[i].m_Sounders.size();
		for (size_t j = 0; j < nbSounders; j++)
		{
			m_Stack[i].m_Sounders[j]->GetTransformSetRef()->CheckReaderParameter();
		}
	}
}

// OTK - FAE065 - nettoyage des soundeurs
void CurrentSounderDefinition::CleanSounders(Sounder* pSounder, PingFan* pFan)
{
	// s'il n'y a plus de ping ou que le sondeur pSounder n'est pas celui associ�
	// au plus ancien PingFan, on l'enl�ve de la liste
	if (pFan == NULL || pSounder != pFan->getSounderRef())
	{
		RemoveSounder(pSounder);
	}
}
