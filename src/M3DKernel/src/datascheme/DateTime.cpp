#include "M3DKernel/datascheme/DateTime.h"
#include "M3DKernel/utils/M3DStdUtils.h"

#include <stdio.h>
#include <ctime>

TimeElapse operator-(const HacTime &v1, const HacTime &v2)
{
	std::int64_t cpuDiff = (std::int64_t)v1.m_TimeCpu - (std::int64_t)v2.m_TimeCpu;
	std::int64_t fracDiff = (std::int64_t)v1.m_TimeFraction - (std::int64_t)v2.m_TimeFraction;
	return TimeElapse(cpuDiff * 10000 + fracDiff);
}

HacTime operator+(const HacTime &v1, const TimeElapse &v2)
{

	HacTime ret(0, 0);
	std::int64_t totalTime = ((std::int64_t)v1.m_TimeCpu) * 10000 + v1.m_TimeFraction + v2.m_timeElapse;
	if (totalTime > 0)
	{
		ret.m_TimeFraction = (unsigned short)(totalTime - 10000 * (totalTime / 10000));
		ret.m_TimeCpu = (std::uint32_t)(totalTime / 10000);
	}
	return ret;
}

void HacTime::GetTimeDesc(char *dest, int len)
{
	time_t time = this->m_TimeCpu;
	tm t;
#ifdef WIN32
	gmtime_s(&t, &time);
#else
	gmtime_r(&time, &t);
#endif
	sprintf(dest, "%d/%02d/%02d %02d:%02d:%02d.%04d", t.tm_year + 1900, t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec, this->m_TimeFraction);
}

void HacTime::GetShortTimeDesc(char *dest, int len)
{
	time_t time = this->m_TimeCpu;
	tm t;
#ifdef WIN32
	gmtime_s(&t, &time);
#else
	gmtime_r(&time, &t);
#endif
	sprintf(dest, "%02d:%02d:%02d.%04d", t.tm_hour, t.tm_min, t.tm_sec, this->m_TimeFraction);
}
