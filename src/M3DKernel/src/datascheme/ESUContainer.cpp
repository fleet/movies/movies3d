#include "M3DKernel/datascheme/ESUContainer.h"

ESUContainer::~ESUContainer()
{
	// d�r�f�rencement des ESU stock�es
	ClearAll();
}

ESUContainer::ESUContainer()
{
	m_ESUContainer.clear();
	m_nextEsuId = 1;
	m_nextDisplayEsuId = 1;
}

void ESUContainer::ResetDisplayEsuId()
{
	m_nextDisplayEsuId = 1;
}

void ESUContainer::AddESU(ESUParameter* pESU)
{
	MovRef(pESU);
	pESU->SetESUId(m_nextEsuId++);
	pESU->SetDisplayESUId(m_nextDisplayEsuId++);
	m_ESUContainer.push_back(pESU);
}

ESUParameter* ESUContainer::GetESUWithIdx(std::uint32_t ESUIdx)
{
	return m_ESUContainer[ESUIdx];
}

ESUParameter* ESUContainer::GetESUById(std::uint32_t ESUId)
{
	ESUParameter * esu = NULL;

	// vu que les esus sont ordonn�es par ID croissant avec un increment de 1
	// on doit pouvoir calculer la position de l'ESU recherhc� dans le contenuer
	// a partir de son id et du 1er id dans le contenuer
	if (m_ESUContainer.size() > 0)
	{
		ESUParameter * currentEsu = m_ESUContainer[0];
		int d = ESUId - currentEsu->GetESUId();
		if (d >= 0 && d < m_ESUContainer.size())
		{
			esu = m_ESUContainer[d];

			// �a ne devrait pas arriver !
			assert(esu->GetESUId() == ESUId);
		}
	}

	return esu;
}

ESUParameter * ESUContainer::GetMostRecentESU() const
{
	ESUParameter * pEsu = NULL;

	if (m_ESUContainer.size() > 0)
	{
		pEsu = m_ESUContainer.back();
	}

	return pEsu;
}

ESUParameter * ESUContainer::GetMostRecentClosedESU() const
{	
	for (auto it = m_ESUContainer.crbegin(); it != m_ESUContainer.crend(); ++it)
	{
		ESUParameter * esu = *it;
		HacTime currentESUEndTime = esu->GetESUTime();
		bool isWorkingESU = currentESUEndTime.IsNull();
		if (!isWorkingESU)
		{
			return esu;
		}
	}
	return NULL;
}

ESUParameter* ESUContainer::GetESUWithPing(std::uint64_t pingId)
{
	ESUParameter * pEsu = NULL;
	// les pings sont class�s par ordre croissant, de m�me que les esus
	for(ESUParameter *p : m_ESUContainer)
	{
		if (pingId < p->GetESUPingNumber())
		{
			break;
		}
		else if (pingId < p->GetESULastPingNumber())
		{
			pEsu = p;
			break;
		}
	}
	return pEsu;
}

void ESUContainer::PopOldestESU()
{
	ESUParameter *p = m_ESUContainer[0];
	MovUnRefDelete(p);
	m_ESUContainer.erase(m_ESUContainer.begin());
}

void ESUContainer::ClearAll()
{
	// d�r�f�rencement des ESU stock�es
	while (m_ESUContainer.size())
	{
		PopOldestESU();
	}
}

void ESUContainer::CleanUp(std::uint64_t oldestPingID)
{
	bool done = false;
	while (m_ESUContainer.size() && !done)
	{
		ESUParameter *p = m_ESUContainer[0];

		// si l'ESU contient que des pings du pass�, on l'enl�ve
		if (p->GetESULastPingNumber() < oldestPingID)
		{
			MovUnRefDelete(p);
			m_ESUContainer.erase(m_ESUContainer.begin());
		}
		else
		{
			// comme les ESU sont dans l'ordre, pas la peine de continuer si
			// on a trouv� un ESU qui est encore en m�moire
			done = true;
		}
	}
}


