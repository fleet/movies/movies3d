#include "M3DKernel/datascheme/ESUParameter.h"

using namespace BaseKernel;

ESUParameter::ESUParameter() :
	BaseKernel::ParameterModule("ESUParameter")
{
	ResetData();
	m_RefSounderID = 0;
}

void ESUParameter::ResetData()
{
	m_ESUCutType = ESU_CUT_BY_DISTANCE;
	m_Distance = 0.0;
	m_Time.SetTimeAsSecond(0);
	m_PingNumber = 0;
	m_LastPingNumber = 0;
	m_EISliceUsed = false;
	m_EIShoalUsed = false;
	m_maxTemporalGap = TimeElapse(10000 * 100); // 100s
	//m_RefSounderID = 0; pas de reset pour ce paramètre : il est repositionné
}

// Méthodes de sérialisation et désérialisation des paramètres
bool ESUParameter::Serialize(MovConfig * movConfig)
{
	// debut de la sérialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	// sérialisation des données membres
	movConfig->SerializeData<unsigned int>(m_ESUCutType, eUInt, "ESUCutType");
	movConfig->SerializeData<double>(m_Distance, eDouble, "Distance");
	movConfig->SerializeData<unsigned int>((unsigned int)m_PingNumber, eUInt, "PingNumber");
	movConfig->SerializeData<int>(m_Time.m_TimeCpu, eInt, "Time");
	movConfig->SerializeData<unsigned int>((unsigned int)(m_maxTemporalGap.m_timeElapse / 10000), eUInt, "MaxTemporalGap");

	movConfig->SerializePushBack();

	return true;
}

bool ESUParameter::DeSerialize(MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	// debut de la désérialisation du module
	if (result)
	{
		// désérialisation des données membres
		unsigned int ESUCutType;
		unsigned int PingNumber;
		unsigned int TimeCPU;
		result = result && movConfig->DeSerializeData<unsigned int>(&ESUCutType, eUInt, "ESUCutType");
		result = result && movConfig->DeSerializeData<double>(&m_Distance, eDouble, "Distance");
		result = result && movConfig->DeSerializeData<unsigned int>(&PingNumber, eUInt, "PingNumber");
		result = result && movConfig->DeSerializeData<unsigned int>(&TimeCPU, eInt, "Time");
		unsigned int temporalGap = 100;
		result = result && movConfig->DeSerializeData<unsigned int>(&temporalGap, eUInt, "MaxTemporalGap");
		m_maxTemporalGap.m_timeElapse = temporalGap * 10000;

		m_ESUCutType = (TESUCutType)ESUCutType;
		m_PingNumber = PingNumber;
		m_Time.m_TimeFraction = 0;
		m_Time.m_TimeCpu = TimeCPU;
	}
	movConfig->DeSerializePushBack();
	return result;
}