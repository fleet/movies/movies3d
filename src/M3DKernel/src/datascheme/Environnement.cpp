#include "M3DKernel/datascheme/Environnement.h"
#include "M3DKernel/utils/log/ILogger.h"

namespace
{
	constexpr const char * LoggerName = "M3DKernel.datascheme.Environnement";
}

Measure::Measure()
	: Pressure(0)
	, Temperature(0)
	, Conductivity(0)
	, SpeedOfSound(0)
	, DepthFromSurface(0)
	, Salinity(0)
	, SoundAbsorption(0)
{
}

Environnement::Environnement() 
	: DatedObject()
{
}

void Environnement::AddMeasure(Measure a)
{
	m_Measure.push_back(a);
}

Measure* Environnement::GetMeasure(unsigned int index) const
{
	if (index < GetNumberOfMeasure())
	{
		return const_cast<Measure*>(&(m_Measure[index]));
	}
	M3D_LOG_ERROR(LoggerName, "Environnement  GetMesure index ouf of bounds");
	return nullptr;
}

unsigned int Environnement::GetNumberOfMeasure() const
{
	return (unsigned int)m_Measure.size();
}

double Measure::ComputeAbsorptionCoefficient(double frequency) const
{
	double f = frequency / 1e3;
	double t = Temperature;
	double s = Salinity;
	double d = DepthFromSurface;
	double ph = 8;
	double c = SpeedOfSound;

	double a1 = (8.86 / c)*pow(10, 0.78*ph - 5);
	double p1 = 1;
	double f1 = 2.8*sqrt(s / 35)*pow(10, 4 - 1245 / (t + 273));

	double a2 = 21.44*(s / c)*(1 + 0.025*t);
	double p2 = 1 - 1.37e-4*d + 6.62e-9*d*d;
	double f2 = 8.17*pow(10, 8 - 1990 / (t + 273)) / (1 + 0.0018*(s - 35));

	double p3 = 1 - 3.83e-5*d + 4.9e-10*d*d;

	double a3l = 4.937e-4 - 2.59e-5*t + 9.11e-7*t*t - 1.5e-8*t*t*t;
	double a3h = 3.964e-4 - 1.146e-5*t + 1.45e-7*t*t - 6.5e-10*t*t*t;
	double a3 = t <= 20 ? a3l : a3h;

	double a = f*f*(a1*p1*f1 / (f1*f1 + f*f) + a2*p2*f2 / (f2*f2 + f*f) + a3*p3);

	return a / 1e3;
}
