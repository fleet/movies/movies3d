#include "M3DKernel/datascheme/EventMarker.h"

EventMarker::~EventMarker()
{
	if (m_Msg)
		delete [] m_Msg;
}

EventMarker::EventMarker()
	: m_Msg(NULL)
{
}

void EventMarker::Allocate(std::uint32_t sizeMsg)
{
	if (m_Msg)
		delete [] m_Msg;
	m_Msg = new char[sizeMsg + 1];
	m_Msg[sizeMsg] = 0;
}