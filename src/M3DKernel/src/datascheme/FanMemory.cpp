#include "M3DKernel/datascheme/FanMemory.h"
#include "M3DKernel/DefConstants.h"
#include "M3DKernel/datascheme/TransformSet.h"
#include "M3DKernel/M3DKernel.h"

using namespace BaseMathLib;

FanMemory::FanMemory()
{
	m_bIgnorePhase = M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePhase();
	m_maxRange = 100;
	m_maxEchoNb = 0;
}

FanMemory::~FanMemory()
{
	while (m_MemDeque.size() > 0)
	{
		MemorySet*pObj = m_MemDeque.front();
		m_MemDeque.pop_front();
        MovUnRefDelete(pObj);
	}
}

void FanMemory::ShiftOne()
{
	MemorySet*pObj = m_MemDeque.front();
	m_MemDeque.pop_front();
	m_MemDeque.push_back(pObj);
}

unsigned int FanMemory::GetMemoryLength()
{
	return m_MemDeque.size();
}

void FanMemory::SetMaxDepth(double maxDepth, unsigned int maxEchoNb)
{
	if (maxDepth != m_maxRange || m_maxEchoNb != maxEchoNb)
	{
		for (unsigned int idx = 0; idx < GetMemoryLength(); idx++)
			GetMemoryIndex(idx)->SetMaxRange(maxDepth);
		m_maxRange = maxDepth;
		m_maxEchoNb = maxEchoNb;
	}
}

void FanMemory::SetMemoryLength(unsigned int len)
{
	// decrease
	while (m_MemDeque.size() > len)
	{
		MemorySet*pObj = m_MemDeque.front();
		m_MemDeque.pop_front();
        MovUnRefDelete(pObj);
	}

	// increase ?
	while (m_MemDeque.size() < len)
	{
		MemorySet*pObj = MemorySet::Create();
        MovRef(pObj);
		m_MemDeque.push_back(pObj);
	}
}

MemorySet* FanMemory::GetMemoryIndex(unsigned int index)
{
	return m_MemDeque[index];
}

void FanMemory::CheckReaderParameter()
{
	bool ignoreP = M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePhase();
	if (m_bIgnorePhase != ignoreP)
	{
		m_bIgnorePhase = ignoreP;
		for (unsigned int i = 0; i < m_MemDeque.size(); i++)
		{
			GetMemoryIndex(i)->UpdateIgnorePhaseFlag();
		}
	}
}
