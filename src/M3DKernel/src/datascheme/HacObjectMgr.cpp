#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/datascheme/TransformMap.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/EventMarker.h"
#include "M3DKernel/datascheme/Environnement.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/MovESUMgr.h"


// IPSIS - OTK - prise en compte du changement d'ESU
// en cas de changement de config accoustique
#include "M3DKernel/M3DKernel.h"

#include "M3DKernel/utils/carto/CartoTools.h"

// NME -  FAE 86
#define NB_SPEED_SAMPLE 10  // nombre de vitesse conserv�e pour le calcul de la vitesse liss�e
#define NB_AVERAGE_POS 20   // echantillonage des positions pour le calcul des infos de navigation
 
namespace
{
	constexpr const char * LoggerName = "M3DKernel.datascheme.HacObjectMgr";
}

HacObjectMgr::HacObjectMgr()
{
	m_pNavAttributes = TimeObjectContainer::Create();
	MovRef(m_pNavAttributes);

	m_pNavPosition = TimeObjectContainer::Create();
	MovRef(m_pNavPosition);

	m_pEventMarker = TimeObjectContainer::Create();
	MovRef(m_pEventMarker);

	m_pEnvironnement = TimeObjectContainer::Create();
	MovRef(m_pEnvironnement);
	
	m_pTrawl = Trawl::Create();
	MovRef(m_pTrawl);

	m_pLastSoftChannel = NULL;

	// OTK - FAE041 - sur ouverture d'un flux HAC, r�initialisation de l'origine 3D
	CCartoTools::InitializeOrigin();

	// NME - FAE 086 reinitialisation du flag de la presence des attributs de navigation
	m_HasNavAttributes = false;
	m_pLastNavPosition = NULL;
	m_pPreviousNavPosition = NULL;
	m_deltaPos = 0;

	m_isUsingCustomEnvironmentValues = false;
}

HacObjectMgr::~HacObjectMgr()
{
	MovUnRefDelete(m_pLastSoftChannel);

	this->m_pingFanContainer.RemoveAllFan();
	MovUnRefDelete(m_pNavAttributes);
	MovUnRefDelete(m_pNavPosition);
	MovUnRefDelete(m_pEventMarker);
	MovUnRefDelete(m_pEnvironnement);
	/// clean Working sounder
	MapSounder::iterator res = m_WorkingSounder.begin();
	while (res != m_WorkingSounder.end())
	{
		if (res->second != NULL)
			MovUnRefDelete(res->second);
		res++;
	}
	m_WorkingSounder.clear();
	/// clean Valid sounder

	/// Clean m_ValidAttitudeSensor 
	{
		MapNavAttitudeContainer::iterator itr = m_mapNavAttitudeContainer.begin();
		while (itr != m_mapNavAttitudeContainer.end())
		{
			if (itr->second != NULL)
				MovUnRefDelete(itr->second);
			itr++;
		}
		m_mapNavAttitudeContainer.clear();

	}
	while (m_pPreAllocBeamDataObject.size())
	{
		PopBeamDataObject();
	}
	while (m_pPreAllocBeamDataSamples.size())
	{
		PopBeamDataSamples();
	}
	while (m_pPreAllocPhaseDataObject.size())
	{
		PopPhaseDataObject();
	}
	while (m_pPreAllocPhaseDataSamples.size())
	{
		PopPhaseDataSamples();
	}

	MovUnRefDelete(m_pTrawl);
}

BeamDataObject* HacObjectMgr::GetBeamDataObject()
{
	if (!m_pPreAllocBeamDataObject.size())
	{
		PushBeamDataObject(new BeamDataObject());
	}
	// on associe � cet objet un vecteur pr�allou� pour recevoir les �chantillons
	BeamDataObject* beam = m_pPreAllocBeamDataObject[m_pPreAllocBeamDataObject.size() - 1];
	beam->SetSamplesContainer(GetBeamDataSamples());
	PopBeamDataSamples();
	return beam;
}

BeamDataSamples* HacObjectMgr::GetBeamDataSamples()
{
	if (!m_pPreAllocBeamDataSamples.size())
	{
		PushBeamDataSamples(new BeamDataSamples());
	}
	return m_pPreAllocBeamDataSamples[m_pPreAllocBeamDataSamples.size() - 1];
}

PhaseDataObject* HacObjectMgr::GetPhaseDataObject()
{
	if (!m_pPreAllocPhaseDataObject.size())
	{
		PushPhaseDataObject(new PhaseDataObject());
	}
	// on associe � cet objet un vecteur pr�allou� pour recevoir les �chantillons
	PhaseDataObject* phase = m_pPreAllocPhaseDataObject[m_pPreAllocPhaseDataObject.size() - 1];
	phase->SetSamplesContainer(GetPhaseDataSamples());
	PopPhaseDataSamples();
	return phase;
}

PhaseDataSamples* HacObjectMgr::GetPhaseDataSamples()
{
	if (!m_pPreAllocPhaseDataSamples.size())
	{
		PushPhaseDataSamples(new PhaseDataSamples());
	}
	return m_pPreAllocPhaseDataSamples[m_pPreAllocPhaseDataSamples.size() - 1];
}

void HacObjectMgr::PopBeamDataObject()
{
	size_t beamDataObjectNb = m_pPreAllocBeamDataObject.size();
	if (beamDataObjectNb)
	{
		MovUnRefDelete(m_pPreAllocBeamDataObject[beamDataObjectNb - 1]);
		m_pPreAllocBeamDataObject.pop_back();
	}
}

void HacObjectMgr::PopBeamDataSamples()
{
	size_t beamDataSamplesNb = m_pPreAllocBeamDataSamples.size();
	if (beamDataSamplesNb)
	{
		MovUnRefDelete(m_pPreAllocBeamDataSamples[beamDataSamplesNb - 1]);
		m_pPreAllocBeamDataSamples.pop_back();
	}
}

void HacObjectMgr::PopPhaseDataObject()
{
	size_t phaseDataObjectNb = m_pPreAllocPhaseDataObject.size();
	if (phaseDataObjectNb)
	{
		MovUnRefDelete(m_pPreAllocPhaseDataObject[phaseDataObjectNb - 1]);
		m_pPreAllocPhaseDataObject.pop_back();
	}
}

void HacObjectMgr::PopPhaseDataSamples()
{
	size_t phaseDataSamplesNb = m_pPreAllocPhaseDataSamples.size();
	if (phaseDataSamplesNb)
	{
		MovUnRefDelete(m_pPreAllocPhaseDataSamples[phaseDataSamplesNb - 1]);
		m_pPreAllocPhaseDataSamples.pop_back();
	}
}

void HacObjectMgr::PushBeamDataObject(BeamDataObject*p)
{
	MovRef(p);
	m_pPreAllocBeamDataObject.push_back(p);
}

void HacObjectMgr::PushBeamDataSamples(BeamDataSamples*p)
{
	MovRef(p);
	m_pPreAllocBeamDataSamples.push_back(p);
}

void HacObjectMgr::PushPhaseDataObject(PhaseDataObject*p)
{
	MovRef(p);
	m_pPreAllocPhaseDataObject.push_back(p);
}

void HacObjectMgr::PushPhaseDataSamples(PhaseDataSamples*p)
{
	MovRef(p);
	m_pPreAllocPhaseDataSamples.push_back(p);
}

void HacObjectMgr::ComputePostRead(Traverser &e, bool bLastClose)
{
	/// update des sessions
	for (unsigned int idx = 0; idx < m_pingFanContainer.m_sounderDefinition.GetNbSounder(); idx++)
	{
		Sounder *p = m_pingFanContainer.m_sounderDefinition.GetSounder(idx);
		if (p)
		{
			p->Update(e);
			p->ComputePostRead(e, bLastClose);
		}
	}
}

void HacObjectMgr::UpdateChildMember(Traverser &e)
{
	for (unsigned idx = 0; idx < m_pingFanContainer.m_sounderDefinition.GetNbSounder(); idx++)
	{
		Sounder *p = m_pingFanContainer.m_sounderDefinition.GetSounder(idx);
		if (p)
		{
			p->Update(e);
		}
	}

	// Vérification si de nouveaux sondeurs doivent être créés
	auto kernelParameters = M3DKernel::GetInstance()->GetRefKernelParameter();


	bool envHasChanged = false;
	if (m_isUsingCustomEnvironmentValues)
	{
		if (kernelParameters.isUsingCustomEnvironmentValues())
		{
			envHasChanged = m_customSpeedOfSound != kernelParameters.getSpeedOfSound()
				|| m_customSalinity != kernelParameters.getSalinity();
		}
		else
		{
			envHasChanged = true;		
		}
	}
	else
	{
		if (kernelParameters.isUsingCustomEnvironmentValues())
		{
			envHasChanged = true;
		}
	}
	m_isUsingCustomEnvironmentValues = kernelParameters.isUsingCustomEnvironmentValues();
	m_customSpeedOfSound = kernelParameters.getSpeedOfSound();
	m_customSalinity = kernelParameters.getSalinity();

	if (envHasChanged)
	{
		M3D_LOG_INFO(LoggerName, "Custom environnement has changed, recreate sounder !");
		
		for (unsigned idx = 0; idx < m_pingFanContainer.m_sounderDefinition.GetNbSounder(); idx++)
		{
			Sounder *p = m_pingFanContainer.m_sounderDefinition.GetSounder(idx);
			if (p && m_WorkingSounder.find(p->m_SounderId) == m_WorkingSounder.cend())
			{
				MovObjectPtr<Sounder> clone = p->Clone();
				AddSounder(clone.get());
			}
		}
	}

	MapSounder::iterator res = m_WorkingSounder.begin();
	while (res != m_WorkingSounder.end())
	{
		if (res->second != NULL)
			res->second->Update(e);
		res++;
	}

	// we force an update 
	this->m_pingFanContainer.CheckReaderParameter();
	DataChanged();
}

void HacObjectMgr::Compute(Traverser &e)
{
	m_bIsComplete = true;
	ComputeSounder(e);
}

void HacObjectMgr::ComputeSounder(Traverser &e)
{
	MapSounder::iterator res = m_WorkingSounder.begin();
	while (res != m_WorkingSounder.end())
	{
		if (res->second != NULL)
		{
			Sounder *p = res->second;
			if (p->IsComplete() && !p->NeedCompute())
			{
				// On applique la nouvelle sound speed
				auto kernelParameters = M3DKernel::GetInstance()->GetRefKernelParameter();
				if (kernelParameters.isUsingCustomEnvironmentValues())
				{				
					M3D_LOG_WARN(LoggerName, "Use custom environnement value for sounder");

					p->m_soundVelocity.overloadValue = kernelParameters.getSpeedOfSound();

					// on corrige le beam sample spacing
					const int nbTransducer = p->GetTransducerCount();
					for (int transIdx = 0; transIdx < nbTransducer; ++transIdx)
					{
						Transducer * trans = p->GetTransducer(transIdx);
						double beamSampleSpacing = trans->computeSampleSpacing(p->m_soundVelocity);
						trans->setBeamSamplesSpacing(beamSampleSpacing);
					}

					TimeObjectContainer * pContainer = GetEnvironnementContainer();
					Environnement * pEnv = (Environnement*)pContainer->GetLastDatedObject();

					if (pEnv && pEnv->GetNumberOfMeasure() > 0)
					{
						Measure envMeasure = *pEnv->GetMeasure(0);
						envMeasure.Temperature = kernelParameters.getTemperature();
						envMeasure.Salinity = kernelParameters.getSalinity();
						envMeasure.SpeedOfSound = kernelParameters.getSpeedOfSound();

						// on corrige le absorption coeff
						const int nbTransducer = p->GetTransducerCount();
						for (int transIdx = 0; transIdx < nbTransducer; ++transIdx)
						{
							Transducer * trans = p->GetTransducer(transIdx);
							const std::vector<unsigned short> channels = trans->GetChannelId();
							for(unsigned short channelId : channels)
							{
								SoftChannel * softChannel = trans->getSoftChannel(channelId);
								softChannel->m_absorptionCoef = envMeasure.ComputeAbsorptionCoefficient(softChannel->m_acousticFrequency) * 1e7;
							}
						}
					}
				}

				// on enleve celui ci et recommence
				m_WorkingSounder.erase(res);
				Sounder *pSou = m_pingFanContainer.m_sounderDefinition.GetSounderWithId(p->m_SounderId);
				if (!pSou || !pSou->IsEqual(*p))
				{
					this->m_pingFanContainer.m_sounderDefinition.AddSounder(p);

					// NMD - FE 94 - update du sondeur de r�f�rence pour les pings deja lu si on change de type de sondeur
					if (pSou != NULL && pSou->m_isMultiBeam != p->m_isMultiBeam)
						this->m_pingFanContainer.UpdateRefSounder(p);

					e.SounderChanged(p->m_SounderId);
					// IPSIS - OTK - en cas de reconfiguration sondeur,
					// on force le démarrage d'un nouvel ESU.
					//LB 23/05/2011 on ne force plus le redémarrage de l'ESU
					M3DKernel::GetInstance()->getMovESUManager()->ForceNewESU();
				}
				else if (pSou->m_soundVelocity != p->m_soundVelocity)
				{
					// OTK - 04/01/2010 - on repasse en mise à jour simple car l'ajout d'un nouveau sondeur
					// provoque la création d'une zone mémoire supplémentaire pour les pings ce qui provoque
					// des pics de consommation mémoire (et éventuellement des plantages).
					// si on veut pouvoir conserver la valeur de célérité pour chaque ping (utile seulement 
					// pour les calculs à postériori, par exemple la réponse fréquentielle), il faudra se repencher
					// sur l'architecture du noyau de Movies3D (pas trivial).
					//pSou->UpdateCelerity(p->m_soundVelocity);
					// OTK - 09/10/2009 - dans ce cas, on n'envoi pas de sounderchanged,
					// mais on crée tout de même un nouvel objet sondeur
					// pour que les pings plus anciens conservent une référence
					// vers un sondeur avec l'ancienne valeur de soundvelocity
					this->m_pingFanContainer.m_sounderDefinition.AddSounder(p);
				}
				MovUnRefDelete(p);
				res = m_WorkingSounder.begin();
			}
			else 
			{
				res++;
			}
		}
		else
		{
			res++;
		}
	}
}

Sounder	*HacObjectMgr::GetWorkingSounder(std::uint32_t sounderId)
{
	MapSounder::iterator res = m_WorkingSounder.find(sounderId);

	if (res != m_WorkingSounder.end())
	{
		return res->second;
	}
	return NULL;
}

void HacObjectMgr::SetLastSoftChannel(SoftChannel * pChan)
{
	MovUnRefDelete(m_pLastSoftChannel);
	m_pLastSoftChannel = pChan;
	MovRef(pChan);
}

SoftChannel	*HacObjectMgr::GetLastSoftChannel()
{
	return m_pLastSoftChannel;
}

void HacObjectMgr::AddSounder(Sounder *a)
{
	if (!a)
		return;

	//if(this->GetSounderDefinition().GetSounderWithId(a->m_SounderId))
	//{
	//	M3D_LOG_WARN(LoggerName, "Find sounder already defined ignoring");	
	//	return;
	//}
	MapSounder::iterator res = m_WorkingSounder.find(a->m_SounderId);

	if (res != m_WorkingSounder.end())
	{
		MovUnRefDelete(res->second);
		res->second = a;
		MovRef(a);
	}
	else
	{
		m_WorkingSounder.insert(MapSounder::value_type(a->m_SounderId, a));
		MovRef(a);
	}
}

TimeObjectContainer *HacObjectMgr::GetNavAttitudeContainer(unsigned short attitudeSensorId)
{
	MapNavAttitudeContainer::iterator res = m_mapNavAttitudeContainer.find(attitudeSensorId);

	if (res != m_mapNavAttitudeContainer.end())
	{
		return res->second;
	}
	TimeObjectContainer *pObj = TimeObjectContainer::Create();
	MovRef(pObj);
	m_mapNavAttitudeContainer.insert(MapNavAttitudeContainer::value_type(attitudeSensorId, pObj));
	m_NavAttitudeSensorId.push_back(attitudeSensorId);
	return pObj;
}

Sounder	*HacObjectMgr::GetLastValidSounder(std::uint32_t sounderId)
{
	return m_pingFanContainer.m_sounderDefinition.GetSounderWithId(sounderId);
}

bool HacObjectMgr::FindValidSounderContainingChannel(unsigned short ChannId, std::uint32_t & sounderId)
{
	bool found = false;
	if (m_pingFanContainer.m_sounderDefinition.GetNbSounder() != 0)
	{
		for (int i = m_pingFanContainer.m_sounderDefinition.GetNbSounder() - 1; i >= 0; i--)
		{
			if (m_pingFanContainer.m_sounderDefinition.GetSounder(i) && m_pingFanContainer.m_sounderDefinition.GetSounder(i)->getSoftChannel(ChannId) != NULL)
			{
				sounderId = m_pingFanContainer.m_sounderDefinition.GetSounder(i)->m_SounderId;
				return true;
			}
		}
	}
	return found;
}

bool HacObjectMgr::FindWorkingSounderContainingChannel(unsigned short ChannId, std::uint32_t & sounderId)
{
	return FindSounderContainingChannel(ChannId, sounderId, m_WorkingSounder);
}

bool HacObjectMgr::FindSounderContainingChannel(unsigned short ChannId, std::uint32_t & sounderId, MapSounder &refMap)
{
	MapSounder::iterator res = refMap.begin();

	while (res != refMap.end())
	{
		SoftChannel*p = res->second->getSoftChannel(ChannId);
		if (p != NULL)
		{
			sounderId = res->second->m_SounderId;
			return true;
		}
		res++;
	}
	return false;
}

bool HacObjectMgr::GetChannelParentSplitBeam(unsigned short aSplitBeamParameterChannelId, SplitBeamPair &ref)
{
	bool found = false;
	MapSplitBeamChannel::iterator itr = m_mapSplitBeamChannel.find(aSplitBeamParameterChannelId);
	if (itr != m_mapSplitBeamChannel.end())
	{
		ref = itr->second;
		found = true;
	}
	return found;
}

void HacObjectMgr::AddSplitBeamParameter(SplitBeamParameter *aSplitbeam, unsigned short channId)
{
	std::uint32_t sounderId;
	Sounder	*pSounder = NULL;
	if (FindValidSounderContainingChannel(channId, sounderId))
	{
		pSounder = this->GetLastValidSounder(sounderId);
	}
	else
	{
		if (FindWorkingSounderContainingChannel(channId, sounderId))
			pSounder = this->GetWorkingSounder(sounderId);
	}

	if (pSounder)
	{
		Transducer *pTrans = pSounder->getTransducerForChannel(channId);
		if (pTrans)
		{
			SoftChannel *pChan = pTrans->getSoftChannel(channId);
			if (pChan)
			{
				pChan->SetSplitBeamParameter(aSplitbeam);
				SplitBeamPair myPair;
				myPair.m_channelId = channId;
				myPair.m_sounderId = pSounder->m_SounderId;

				m_mapSplitBeamChannel.insert(MapSplitBeamChannel::value_type(aSplitbeam->m_splitBeamParameterChannelId, myPair));
			}
		}
		assert(pTrans);
	}
}

void HacObjectMgr::CheckTimeObject()
{
	// remove old objects 
	HacTime olderTime(std::numeric_limits<std::uint32_t>::max(), std::numeric_limits<unsigned short>::max());
	for (unsigned int idx = 0; idx < m_pingFanContainer.m_sounderDefinition.GetNbSounder(); idx++)
	{
		Sounder *p = m_pingFanContainer.m_sounderDefinition.GetSounder(idx);
		if (p)
		{
			if (GetPingFanContainer().GetNbFan())
			{
				// OTK - FAE179 - correction d'un probl�me de suppression de datedobjects ici si les pings ne sont pas chronologiques dans le fichier d'entr�e
				PingFan *pingfan = (PingFan *)GetPingFanContainer().GetPingFanContainer()->GetOldestObject();
				if (pingfan->m_ObjectTime < olderTime)
				{
					olderTime = pingfan->m_ObjectTime;
				}
			}

			if (p->m_pWorkingPingFan)
			{
				if (p->m_pWorkingPingFan->m_ObjectTime < olderTime)
				{
					olderTime = p->m_pWorkingPingFan->m_ObjectTime;
				}
			}

			if(p->m_pIncompletePingFan)
			{
				if (p->m_pIncompletePingFan->m_ObjectTime < olderTime)
				{
					olderTime = p->m_pIncompletePingFan->m_ObjectTime;
				}
			}

			if(p->m_pPendingPingFan)
			{
				if (p->m_pPendingPingFan->m_ObjectTime < olderTime)
				{
					olderTime = p->m_pPendingPingFan->m_ObjectTime;
				}
			}
		}
	}
	/// check date for working pingFan
	if (olderTime < HacTime(std::numeric_limits<std::uint32_t>::max(), std::numeric_limits<unsigned short>::max()))
	{
		RemoveOldObject(olderTime);
	}
}

// OTK - 21/12/2009 - lecture des fichiers HAC avec un mauvais nombre de channels
// dans le tuple sondeur
void HacObjectMgr::CorrectChannelNumbers(Traverser &e)
{
	bool updateNeeded = false;
	MapSounder::iterator res = m_WorkingSounder.begin();
	while (res != m_WorkingSounder.end())
	{
		if (res->second != NULL)
		{
			unsigned int nbTrans = res->second->GetTransducerCount();
			if (nbTrans != 0)
			{
				if (res->second->m_numberOfTransducer != nbTrans)
				{
					M3D_LOG_WARN(LoggerName, "The SounderTuple's ""number of software channel"" is incoherent with the real number of software channel tuples");
					updateNeeded = true;
					res->second->m_numberOfTransducer = nbTrans;
				}

				// utilisation d'un tuple platform par defaut si on n'en n'a pas recu pour compatibilit� avec certains fichiers HAC Movies+
				for (unsigned int i = 0; i < nbTrans; i++)
				{
					if (res->second->GetTransducer(i)->GetPlatform() == NULL)
					{
						Platform *p = Platform::Create();
						res->second->GetTransducer(i)->SetPlatform(p);
						res->second->GetTransducer(i)->Update(e);
						updateNeeded = true;
					}
				}
				res->second->DataChanged();
			}
		}
		res++;
	}
	if (updateNeeded)
	{
		this->Update(e);
	}
}

void HacObjectMgr::RemoveOldObject(HacTime &oldestDate)
{
	m_pEventMarker->RemoveOldObject(oldestDate);
	m_pNavAttributes->RemoveOldObject(oldestDate);
	m_pNavPosition->RemoveOldObject(oldestDate);
	MapNavAttitudeContainer::iterator itr = m_mapNavAttitudeContainer.begin();
	while (itr != m_mapNavAttitudeContainer.end())
	{
		if (itr->second != NULL)
			itr->second->RemoveOldObject(oldestDate);
		itr++;
	}
	m_pTrawl->RemoveOldObject(oldestDate);
}

unsigned int HacObjectMgr::GetNumberOfNavAttitudeSensorId()
{
	return m_NavAttitudeSensorId.size();
}

unsigned short HacObjectMgr::GetNavAttitudeSensorId(unsigned int index)
{
	if (index < GetNumberOfNavAttitudeSensorId())
		return m_NavAttitudeSensorId[index];
	return 0;
}

// OTK - FAE037 - interpolation de la position des pings
NavPosition* HacObjectMgr::CreateInterpolatedPosition(const HacTime &time)
{
	const auto & positions = m_pNavPosition->datedContainer();
	if (positions.size() < 2)
		return nullptr;

	const auto beg = positions.cbegin();
	const auto end = positions.cend();
	const auto rbeg = positions.rbegin();
	const auto rend = positions.rend();
	
	auto it1 = positions.lower_bound(time);
	auto it2 = it1;
	if (it1 != end)
	{
		if (it1 == beg)
		{
			++it2;
		}
		else
		{
			--it1;
		}
	}
	else
	{
		it1--;
		it2 = it1;
		it1--;
	}

	// On recherche dans le conteneur de donn�es les attitudes avant et apr�s le plus proche temporellement
	// On recherche deux valeurs avant et apr�s pour pouvoir �ventuellement extrapoler
	// On recherche donc : pPrevPos2 < pPrevPos1 < time < pNextPos1 < pNextPos2

	NavPosition* pPrevPos1 = nullptr;
	NavPosition* pPrevPos2 = nullptr;
	NavPosition* pNextPos1 = nullptr;
	NavPosition* pNextPos2 = nullptr;

	TimeElapse pPrevPos1Delta;
	TimeElapse pPrevPos2Delta;
	TimeElapse pNextPos1Delta;
	TimeElapse pNextPos2Delta;
	
	// Recherche des informations pr�c�dentes temporellement
	for (auto it = std::make_reverse_iterator(it1); it != rend; ++it)
	{
		NavPosition* pPos = (NavPosition*) it->second.front();
		if (pPos->m_source == eTupleSource)
		{
			if (pPrevPos1 == nullptr)
			{
				pPrevPos1 = pPos;
				pPrevPos1Delta = time - pPrevPos1->m_ObjectTime;
			}
			else
			{
				pPrevPos2 = pPos;
				pPrevPos2Delta = time - pPrevPos2->m_ObjectTime;
				break;
			}
		}
	}

	// Recherche des informations suivantes temporellement
	for (auto it = it2; it != end; ++it)
	{
		NavPosition* pPos = (NavPosition*)it->second.front();
		if (pPos->m_source == eTupleSource)
		{
			if (pNextPos1 == nullptr)
			{
				pNextPos1 = pPos;
				pNextPos1Delta = pNextPos1->m_ObjectTime - time;
			}
			else
			{
				pNextPos2 = pPos;
				pNextPos2Delta = pNextPos2->m_ObjectTime - time;
				break;
			}
		}
	}
	
	NavPosition* pos1 = pPrevPos1;
	NavPosition* pos2 = pNextPos1;
	double delta1 = -pPrevPos1Delta.m_timeElapse;
	double delta2 = pNextPos1Delta.m_timeElapse;

	// Recherche du meilleur couple pour l'interpolation
	if (pPrevPos1 != nullptr && pNextPos1 != nullptr)
	{
		// on utilise le min des deux delta comme reference
		if (pPrevPos1Delta < pNextPos1Delta)
		{
			if (pPrevPos2 != nullptr)
			{
				if (pNextPos1Delta > pPrevPos2Delta)
				{
					pos1 = pPrevPos2;
					delta1 = -pPrevPos2Delta.m_timeElapse;

					pos2 = pPrevPos1;
					delta2 = -pPrevPos1Delta.m_timeElapse;
				}
			}
		}
		else
		{
			if (pNextPos2 != nullptr)
			{
				if (pPrevPos1Delta > pNextPos2Delta)
				{
					pos1 = pNextPos1;
					delta1 = pNextPos1Delta.m_timeElapse;

					pos2 = pNextPos2;
					delta2 = pNextPos2Delta.m_timeElapse;
				}
			}
		}
	}
	else
	{
		if (pPrevPos1 != nullptr && pPrevPos2 != nullptr)
		{
			pos1 = pPrevPos2;
			delta1 = -pPrevPos2Delta.m_timeElapse;

			pos2 = pPrevPos1;
			delta2 = -pPrevPos1Delta.m_timeElapse;
		}
		else if(pNextPos1 != nullptr && pNextPos2 != nullptr)
		{
			pos1 = pNextPos1;
			delta1 = pNextPos1Delta.m_timeElapse;

			pos2 = pNextPos2;
			delta2 = pNextPos2Delta.m_timeElapse;
		}
	}

	NavPosition* interp = NULL;

	if (pos1 != nullptr && pos2 != nullptr)
	{
		interp = NavPosition::Create();
		interp->m_source = eInterpSource;
		interp->m_gpsTime = time.m_TimeCpu;
		interp->m_ObjectTime.m_TimeCpu = time.m_TimeCpu;
		interp->m_ObjectTime.m_TimeFraction = time.m_TimeFraction;
				
		const double r1 = delta2 / (delta2 - delta1);
		const double r2 = delta1 / (delta1 - delta2);

		interp->m_lattitudeDeg = r1 * pos1->m_lattitudeDeg + r2 * pos2->m_lattitudeDeg;
		interp->m_longitudeDeg = r1 * pos1->m_longitudeDeg + r2 * pos2->m_longitudeDeg;
	}
	
	return interp;
}

NavAttitude* HacObjectMgr::CreateInterpolatedAttitude(unsigned short attitudeSensorId, const HacTime &time)
{
	const auto & attitudes = GetNavAttitudeContainer(attitudeSensorId)->datedContainer();
	if (attitudes.size() < 2)
		return nullptr;

	const auto beg = attitudes.cbegin();
	const auto end = attitudes.cend();
	const auto rbeg = attitudes.rbegin();
	const auto rend = attitudes.rend();

	auto it1 = attitudes.lower_bound(time);
	auto it2 = it1;
	if (it1 != end)
	{
		if (it1 == beg)
		{
			++it2;
		}
		else
		{
			--it1;
		}
	}
	else
	{
		it1--;
		it2 = it1;
		it1--;
	}

	// On recherche dans le conteneur de donn�es les attitudes avant et apr�s le plus proche temporellement
	// On recherche deux valeurs avant et apr�s pour pouvoir �ventuellement extrapoler
	// On recherche donc : pPrevPos2 < pPrevPos1 < time < pNextPos1 < pNextPos2

	NavAttitude* pPrevPos1 = nullptr;
	NavAttitude* pPrevPos2 = nullptr;
	NavAttitude* pNextPos1 = nullptr;
	NavAttitude* pNextPos2 = nullptr;

	TimeElapse pPrevPos1Delta;
	TimeElapse pPrevPos2Delta;
	TimeElapse pNextPos1Delta;
	TimeElapse pNextPos2Delta;

	// Recherche des informations pr�c�dentes temporellement
	for (auto it = std::make_reverse_iterator(it1); it != rend; ++it)
	{
		NavAttitude* pPos = (NavAttitude*)it->second.front();
		if (pPos->m_source == eTupleSource)
		{
			if (pPrevPos1 == nullptr)
			{
				pPrevPos1 = pPos;
				pPrevPos1Delta = time - pPrevPos1->m_ObjectTime;
			}
			else
			{
				pPrevPos2 = pPos;
				pPrevPos2Delta = time - pPrevPos2->m_ObjectTime;
				break;
			}
		}
	}

	// Recherche des informations suivantes temporellement
	for (auto it = it2; it != end; ++it)
	{
		NavAttitude* pPos = (NavAttitude*)it->second.front();
		if (pPos->m_source == eTupleSource)
		{
			if (pNextPos1 == nullptr)
			{
				pNextPos1 = pPos;
				pNextPos1Delta = pNextPos1->m_ObjectTime - time;
			}
			else
			{
				pNextPos2 = pPos;
				pNextPos2Delta = pNextPos2->m_ObjectTime - time;
				break;
			}
		}
	}

	NavAttitude* pos1 = pPrevPos1;
	NavAttitude* pos2 = pNextPos1;
	double delta1 = -pPrevPos1Delta.m_timeElapse;
	double delta2 = pNextPos1Delta.m_timeElapse;

	// Recherche du meilleur couple pour l'interpolation
	if (pPrevPos1 != nullptr && pNextPos1 != nullptr)
	{
		// on utilise le min des deux delta comme reference
		if (pPrevPos1Delta < pNextPos1Delta)
		{
			if (pPrevPos2 != nullptr)
			{
				if (pNextPos1Delta > pPrevPos2Delta)
				{
					pos1 = pPrevPos2;
					delta1 = -pPrevPos2Delta.m_timeElapse;

					pos2 = pPrevPos1;
					delta2 = -pPrevPos1Delta.m_timeElapse;
				}
			}
		}
		else
		{
			if (pNextPos2 != nullptr)
			{
				if (pPrevPos1Delta > pNextPos2Delta)
				{
					pos1 = pNextPos1;
					delta1 = pNextPos1Delta.m_timeElapse;

					pos2 = pNextPos2;
					delta2 = pNextPos2Delta.m_timeElapse;
				}
			}
		}
	}
	else
	{
		if (pPrevPos1 != nullptr && pPrevPos2 != nullptr)
		{
			pos1 = pPrevPos2;
			delta1 = -pPrevPos2Delta.m_timeElapse;

			pos2 = pPrevPos1;
			delta2 = -pPrevPos1Delta.m_timeElapse;
		}
		else if (pNextPos1 != nullptr && pNextPos2 != nullptr)
		{
			pos1 = pNextPos1;
			delta1 = pNextPos1Delta.m_timeElapse;

			pos2 = pNextPos2;
			delta2 = pNextPos2Delta.m_timeElapse;
		}
	}

	NavAttitude* interp = nullptr;

	if(pos1 != nullptr && pos2 != nullptr)
	{
		interp = NavAttitude::Create();
		interp->m_source = eInterpSource;
		interp->m_bDefaultYaw = (delta1 < delta2) ? pos1->m_bDefaultYaw : pos2->m_bDefaultYaw;
		interp->m_ObjectTime.m_TimeCpu = time.m_TimeCpu;
		interp->m_ObjectTime.m_TimeFraction = time.m_TimeFraction;
		interp->m_AttitudeSensorId = attitudeSensorId;

		const double r1 = delta2 / (delta2 - delta1);
		const double r2 = delta1 / (delta1 - delta2);

		interp->m_pitchRad   = r1 * pos1->m_pitchRad   + r2 * pos2->m_pitchRad;
		interp->m_rollRad    = r1 * pos1->m_rollRad    + r2 * pos2->m_rollRad;
		interp->m_heaveMeter = r1 * pos1->m_heaveMeter + r2 * pos2->m_heaveMeter;
		interp->m_yawRad     = r1 * pos1->m_yawRad     + r2 * pos2->m_yawRad;
	}
	
	return interp;
}

TimeObjectContainer*	HacObjectMgr::GetNavAttributesContainer() { return m_pNavAttributes; }
TimeObjectContainer*	HacObjectMgr::GetNavPositionContainer() { return m_pNavPosition; }
TimeObjectContainer*	HacObjectMgr::GetEventMarkerContainer() { return m_pEventMarker; }
TimeObjectContainer*	HacObjectMgr::GetEnvironnementContainer() { return m_pEnvironnement; }

void HacObjectMgr::SetHasNavAttributes(bool hasNavAttributes) { m_HasNavAttributes = hasNavAttributes; }

bool HacObjectMgr::GetHasNavAttributes() { return m_HasNavAttributes; }

void HacObjectMgr::ComputeNavAttributes(NavPosition * p)
{
	// FAE 86 - ajout du calcul des informations de navigation
	if (!m_HasNavAttributes)
	{
		if (!M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePingsWithNoNavigation())
		{
			// verification si on a une nouvelle position GPS
			if (m_pPreviousNavPosition == NULL)
			{
				m_deltaPos++;
				m_pPreviousNavPosition = p;
				m_pLastNavPosition = p;
			}
			else if (p->m_ObjectTime > m_pPreviousNavPosition->m_ObjectTime)
			{
				m_deltaPos++;
				m_pPreviousNavPosition = p;
			}

			if (m_deltaPos >= NB_AVERAGE_POS)
			{
				// calcul de la vitesse
				double speed;
				if (m_pLastNavPosition != NULL)
				{
					speed = NavAttributes::GetSpeed(m_pLastNavPosition, p);
				}
				else
				{
					speed = 0.0f;
				}

				if (m_sTabSpeed.size() == NB_SPEED_SAMPLE)
					m_sTabSpeed.pop_back();

				m_sTabSpeed.push_front(speed);

				// lissage de la vitesse aves les valeurs sauvegard�es
				double sumSpeed = 0.0f;
				int validSpeed = 0;
				for (std::list<double>::iterator iter = m_sTabSpeed.begin(); iter != m_sTabSpeed.end(); iter++)
				{
					speed = *iter;
					if (speed > 0 && speed < 30000)
					{
						sumSpeed += speed;
						validSpeed++;
					}
				}
				if (validSpeed != 0)
					speed = sumSpeed / validSpeed;
				else
					speed = 0.0f;

				NavAttributes * computedNavAttributes = NavAttributes::Create();
				MovRef(computedNavAttributes);
				computedNavAttributes->m_ObjectTime = p->m_ObjectTime;
				computedNavAttributes->m_speedMeter = speed * 1.852 / 3.6;
				computedNavAttributes->m_headingRad = NavAttributes::GetHeading(m_pLastNavPosition, p);
				computedNavAttributes->m_bIsEstimated = true;
				this->GetNavAttributesContainer()->AddObject(computedNavAttributes);
				MovUnRefDelete(computedNavAttributes);

				// reinitialisation du compteur
				m_deltaPos = 0;
				m_pLastNavPosition = p;
			}
		}
	}
}
