#include "M3DKernel/datascheme/KernelParameter.h"
#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;


KernelParameter::KernelParameter() : ParameterModule("KernelParameter")
{
	ResetData();
}


void KernelParameter::ResetData()
{
	m_ScreenPixelSizeX = DEFAULT_X_SPACING;
	m_ScreenPixelSizeY = DEFAULT_Y_SPACING;
	m_CustomSampling = false; // par défaut, échantillonnage automatique
	m_InitWindowWidth = -1;
	m_InitWindowHeight = -1;
	m_nbPingFanMax = 500;//NBFRAME_MAX;
#ifdef _DEBUG
	m_nbPingFanMax = 100;
#endif
	m_MaxRange = 150;
	m_UserSpeed = 1;
	m_SpeedUsed = SPTW;
	m_UserPosition = BaseMathLib::Vector3D(0.0, 0.0, 0.0);
	m_PositionUsed = POSITION_TUPLE;
	m_bAutoLengthEnable = false;
	m_bAutoDepthEnable = true;
	m_bAutoDepthTolerance = 10;

	m_bIgnoreIncompletePings = true;
	m_bIgnorePhaseData = false;
	m_bIgnorePingsWithNoNavigation = false; // 09/09/2011 - FAE 107
	m_bIgnorePingsWithNoPosition = true;
	m_bIgnoreAsynchronousChannels = true;
	m_bInterpolatePingPosition = true;
	m_bInterpolateAttitudes = true;
	m_bKeepQuadrantsInMemory = false;

	m_logPingFanInterTime = false;

	m_bCorrectSounderTimeShift = true;

	m_bWeightedEchoIntegration = true;

	m_SelectedProjection = 0;
	m_Projections.clear();
	m_Projections.push_back(ParameterProjection());

	m_temperature = 12.6;
	m_speedOfSound = 1494.572;
	m_salinity = 35.4;
	m_useCustomEnvironmentValues = false;
}

// sauvegarde des paramètres dans le fichier de configuration associé
bool KernelParameter::Serialize(MovConfig * movConfig)
{
	// debut de la sérialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	movConfig->SerializeData<float>(m_MaxRange, eFloat, "MaxRange");
	movConfig->SerializeData<float>(m_UserSpeed, eFloat, "UserSpeed");
	movConfig->SerializeData<int>((int)m_SpeedUsed, eUInt, "SpeedUsed");
	movConfig->SerializeData<double>(m_UserPosition.x, eDouble, "UserPositionLat");
	movConfig->SerializeData<double>(m_UserPosition.y, eDouble, "UserPositionLong");
	movConfig->SerializeData<double>(m_UserPosition.z, eDouble, "UserPositionDepth");
	movConfig->SerializeData<int>((int)m_PositionUsed, eUInt, "PositionUsed");
	movConfig->SerializeData<bool>(m_logPingFanInterTime, eBool, "LogPingFanInterTime");
	movConfig->SerializeData<bool>(m_CustomSampling, eBool, "CustomSampling");
	movConfig->SerializeData<double>(m_ScreenPixelSizeX, eDouble, "ScreenPixelSizeX");
	movConfig->SerializeData<double>(m_ScreenPixelSizeY, eDouble, "ScreenPixelSizeY");
	movConfig->SerializeData<unsigned int>(m_nbPingFanMax, eUInt, "NbPingFanMax");
	movConfig->SerializeData<bool>(m_bAutoLengthEnable, eBool, "AutoLengthEnable");
	movConfig->SerializeData<bool>(m_bAutoDepthEnable, eBool, "AutoDepthEnable");
	movConfig->SerializeData<float>(m_bAutoDepthTolerance, eFloat, "AutoDepthTolerance");
	movConfig->SerializeData<bool>(m_bIgnoreIncompletePings, eBool, "IgnoreIncompletePings");
	movConfig->SerializeData<bool>(m_bIgnorePhaseData, eBool, "IgnorePhaseData");
	movConfig->SerializeData<bool>(m_bCorrectSounderTimeShift, eBool, "CorrectSounderTimeShift");
	movConfig->SerializeData<bool>(m_bIgnorePingsWithNoNavigation, eBool, "IgnorePingsWithNoNavigation");
	movConfig->SerializeData<bool>(m_bIgnorePingsWithNoPosition, eBool, "IgnorePingsWithNoPosition");
	movConfig->SerializeData<bool>(m_bIgnoreAsynchronousChannels, eBool, "IgnoreAsynchronousChannels"); // FAE082
	movConfig->SerializeData<bool>(m_bInterpolatePingPosition, eBool, "InterpolatePingPosition");
	movConfig->SerializeData<bool>(m_bWeightedEchoIntegration, eBool, "WeightedEchoIntegration");
	movConfig->SerializeData<bool>(m_bInterpolateAttitudes, eBool, "InterpolateAttitudes");
	movConfig->SerializeData<bool>(m_bKeepQuadrantsInMemory, eBool, "KeepQuadrantsInMemory");

	// systemes de projection
	movConfig->SerializeData<int>((int)m_Projections.size(), eInt, "ProjectionNumber");
	movConfig->SerializeData<int>(m_SelectedProjection, eInt, "SelectedProjectionIndex");
	for (size_t i = 0; i < m_Projections.size(); i++)
	{
		m_Projections[i].Serialize(movConfig);
	}

	//Environment
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "Environment");
	movConfig->SerializeData<double>(m_temperature, eDouble, "Temperature");
	movConfig->SerializeData<double>(m_speedOfSound, eDouble, "SpeedOfSound");
	movConfig->SerializeData<double>(m_salinity, eDouble, "Salinity");
	movConfig->SerializeData<bool>(m_useCustomEnvironmentValues, eBool, "UseCustomEnvironment");
	movConfig->SerializePushBack();

	// fin de la sérialisation du module
	movConfig->SerializePushBack();

	return true;
}

// lecture des paramètres dans le fichier de configuration associé
bool KernelParameter::DeSerialize(MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	// debut de la désérialisation du module
	if (result)
	{
		result = result && movConfig->DeSerializeData<float>(&m_MaxRange, eFloat, "MaxRange");
		result = result && movConfig->DeSerializeData<float>(&m_UserSpeed, eFloat, "UserSpeed");
		int speedUsed = (int)m_SpeedUsed;
		result = result && movConfig->DeSerializeData<int>(&speedUsed, eUInt, "SpeedUsed");
		m_SpeedUsed = (SpeedType)speedUsed;
		result = result && movConfig->DeSerializeData<double>(&m_UserPosition.x, eDouble, "UserPositionLat");
		result = result && movConfig->DeSerializeData<double>(&m_UserPosition.y, eDouble, "UserPositionLong");
		result = result && movConfig->DeSerializeData<double>(&m_UserPosition.z, eDouble, "UserPositionDepth");
		int positionUsed = (int)m_PositionUsed;
		result = result && movConfig->DeSerializeData<int>(&positionUsed, eUInt, "PositionUsed");
		m_PositionUsed = (PositionType)positionUsed;
		result = result && movConfig->DeSerializeData<bool>(&m_logPingFanInterTime, eBool, "LogPingFanInterTime");
		result = result && movConfig->DeSerializeData<bool>(&m_CustomSampling, eBool, "CustomSampling");
		result = result && movConfig->DeSerializeData<double>(&m_ScreenPixelSizeX, eDouble, "ScreenPixelSizeX");
		result = result && movConfig->DeSerializeData<double>(&m_ScreenPixelSizeY, eDouble, "ScreenPixelSizeY");
		result = result && movConfig->DeSerializeData<unsigned int>(&m_nbPingFanMax, eUInt, "NbPingFanMax");
		result = result && movConfig->DeSerializeData<bool>(&m_bAutoLengthEnable, eBool, "AutoLengthEnable");
		result = result && movConfig->DeSerializeData<bool>(&m_bAutoDepthEnable, eBool, "AutoDepthEnable");
		result = result && movConfig->DeSerializeData<float>(&m_bAutoDepthTolerance, eFloat, "AutoDepthTolerance");
		result = result && movConfig->DeSerializeData<bool>(&m_bIgnoreIncompletePings, eBool, "IgnoreIncompletePings");
		result = result && movConfig->DeSerializeData<bool>(&m_bIgnorePhaseData, eBool, "IgnorePhaseData");
		result = result && movConfig->DeSerializeData<bool>(&m_bCorrectSounderTimeShift, eBool, "CorrectSounderTimeShift");
		result = result && movConfig->DeSerializeData<bool>(&m_bIgnorePingsWithNoNavigation, eBool, "IgnorePingsWithNoNavigation");
		result = result && movConfig->DeSerializeData<bool>(&m_bIgnorePingsWithNoPosition, eBool, "IgnorePingsWithNoPosition");
		result = result && movConfig->DeSerializeData<bool>(&m_bInterpolatePingPosition, eBool, "InterpolatePingPosition");
		result = result && movConfig->DeSerializeData<bool>(&m_bWeightedEchoIntegration, eBool, "WeightedEchoIntegration");
		result = result && movConfig->DeSerializeData<bool>(&m_bKeepQuadrantsInMemory, eBool, "KeepQuadrantsInMemory");

		int projNb = 0;
		result = result && movConfig->DeSerializeData<int>(&projNb, eInt, "ProjectionNumber");
		result = result && movConfig->DeSerializeData<int>(&m_SelectedProjection, eInt, "SelectedProjectionIndex");
		m_Projections.clear();

		if (projNb <= 0)
		{
			m_Projections.push_back(ParameterProjection());
		}
		else
		{
			for (int i = 0; i < projNb; i++)
			{
				ParameterProjection proj;
				result = result && proj.DeSerialize(movConfig);
				m_Projections.push_back(proj);
			}
		}

		// OTK - FAE082
		result = result && movConfig->DeSerializeData<bool>(&m_bIgnoreAsynchronousChannels, eBool, "IgnoreAsynchronousChannels");

		//Attitude
		result = result && movConfig->DeSerializeData<bool>(&m_bInterpolateAttitudes, eBool, "InterpolateAttitudes");

		//Environment
		if (movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "Environment"))
		{
			result = result && movConfig->DeSerializeData<double>(&m_temperature, eDouble, "Temperature");
			result = result && movConfig->DeSerializeData<double>(&m_speedOfSound, eDouble, "SpeedOfSound");
			result = result && movConfig->DeSerializeData<double>(&m_salinity, eDouble, "Salinity");
			result = result && movConfig->DeSerializeData<bool>(&m_useCustomEnvironmentValues, eBool, "UseCustomEnvironment");

			movConfig->DeSerializePushBack();
		}
	}

	// fin de la désérialisation du module
	movConfig->DeSerializePushBack();

	return result;
}


