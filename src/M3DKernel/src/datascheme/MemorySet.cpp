#include "M3DKernel/datascheme/MemorySet.h"

typedef TransducerIndexed<MemoryStruct> MemoryStructTransducerIndexed;

MemorySet::MemorySet(void)
{
}

MemorySet::~MemorySet(void)
{
}

void MemorySet::SetMaxRange(double m_range)
{
	for (unsigned int i = 0; i < m_IndexedMemory.GetObjectCount(); i++)
	{
		m_IndexedMemory.GetObject(i)->SetMaxRange(m_range);
	}
}

void MemorySet::SetSounder(Sounder *p)
{
	this->m_IndexedMemory.SetSounder(p);
}

void MemorySet::UpdateIgnorePhaseFlag()
{
	for (unsigned int i = 0; i < m_IndexedMemory.GetObjectCount(); i++)
	{
		m_IndexedMemory.GetObject(i)->UpdateIgnorePhaseFlag();
	}
}

unsigned int  MemorySet::GetMemoryStructCount()
{
	return m_IndexedMemory.GetObjectCount();
}
