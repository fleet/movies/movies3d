#include "M3DKernel/datascheme/MemoryStruct.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/M3DKernel.h"

MemoryStruct::MemoryStruct()
{
	m_pAmpData = NULL;
	m_pAngleData = NULL;
	m_pPhase = NULL;
	m_pOverLap = NULL;
	m_pFilterFlag = NULL;
	
	m_SizeDepthUsed = 100;
}

MemoryStruct::~MemoryStruct()
{
	MovUnRefDelete(m_pAmpData);
	MovUnRefDelete(m_pAngleData);
	MovUnRefDelete(m_pPhase);
	MovUnRefDelete(m_pOverLap);
	MovUnRefDelete(m_pFilterFlag);
}

void MemoryStruct::SetMaxRange(double range)
{
	unsigned int DepthY = (unsigned int)(range / GetTransducer()->getBeamsSamplesSpacing());
	bool ignorePhase = M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePhase();
	Reallocate(ignorePhase, GetTransducer()->m_numberOfSoftChannel, DepthY);
	m_SizeDepthUsed = DepthY;
}

void MemoryStruct::CleanFilterAndOverlap(double range)
{
	unsigned int sizeX = GetTransducer()->m_numberOfSoftChannel;
	unsigned int sizeY = (unsigned int)(range / GetTransducer()->getBeamsSamplesSpacing());

	// OTK - 06/01/2010 - pas d'allocation syst�matique de l'overlap
	m_OverLapLock.Lock();
	if (m_pOverLap)
	{
		m_OverlapEmptySizeX = sizeX;
		m_OverlapEmptySizeY = sizeY;
		m_pOverLap->SetDefault(sizeX, sizeY, -1);
	}
	m_OverLapLock.Unlock();

	m_pFilterFlag->SetDefault(sizeX, sizeY, 0);
}

void MemoryStruct::SetTransducer(Transducer	*pTrans)
{
	m_pTransducer = pTrans;
}

void MemoryStruct::UpdateIgnorePhaseFlag()
{
	bool ignorePhase = M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePhase();
	if (ignorePhase)
	{
		MovUnRefDelete(m_pPhase);
	}
	else {
		if (!m_pPhase)
		{
			if (GetDataFmt())
			{
				Reallocate(ignorePhase, GetDataFmt()->getSize().x, GetDataFmt()->getSize().y);
			}
		}
	}
}

void MemoryStruct::Reallocate(bool ignorePhase, unsigned int sizeX, unsigned int sizeY)
{
	if (!m_pAmpData)
	{
		Allocate(ignorePhase, sizeX, sizeY);
	}
	else
	{
		if (ignorePhase)
		{
			MovUnRefDelete(m_pPhase);
		}
		else
		{
			Phase vide;
			if (!m_pPhase)
			{
				m_pPhase = MemoryObjectPhase::Create();
				MovRef(m_pPhase);
				m_pPhase->Allocate(sizeX, sizeY, vide);
			}
			else
				m_pPhase->Reallocate(sizeX, sizeY, vide);
		}
		m_pAmpData->Reallocate(sizeX, sizeY, UNKNOWN_DB);
		m_pAngleData->Reallocate(sizeX, sizeY, UNKNOWN_DB);
		// OTK - 06/01/2010 - pas d'allocation syst�matique de l'overlap
		m_OverLapLock.Lock();
		m_OverlapEmptySizeX = sizeX;
		m_OverlapEmptySizeY = sizeY;
		if (m_pOverLap != NULL)
		{
			m_pOverLap->Lock();
			m_pOverLap->Reallocate(m_OverlapEmptySizeX, m_OverlapEmptySizeY, -1);
			m_pOverLap->Unlock();
		}
		m_OverLapLock.Unlock();
		m_pFilterFlag->Reallocate(sizeX, sizeY, 0);
	}
}

void MemoryStruct::Allocate(bool ignorePhase, unsigned int sizeX, unsigned int sizeY)
{
	MovUnRefDelete(m_pAmpData);
	MovUnRefDelete(m_pAngleData);
	MovUnRefDelete(m_pPhase);
	MovUnRefDelete(m_pFilterFlag);
	
	m_pAmpData = MemoryObjectDataFmt::Create();
	m_pAngleData = MemoryObjectDataFmt::Create();
	m_pFilterFlag = MemoryFiltered::Create();

	MovRef(m_pAmpData);
	MovRef(m_pAngleData);
	MovRef(m_pFilterFlag);

	m_OverLapLock.Lock();
	m_OverlapEmptySizeX = sizeX;
	m_OverlapEmptySizeY = sizeY;
	m_OverLapLock.Unlock();

	Phase vide;
	m_pAmpData->Allocate(sizeX, sizeY, UNKNOWN_DB);
	m_pAngleData->Allocate(sizeX, sizeY, UNKNOWN_DB);
	m_pFilterFlag->Allocate(sizeX, sizeY, 0);
	if (!ignorePhase)
	{
		m_pPhase = MemoryObjectPhase::Create();
		MovRef(m_pPhase);
		m_pPhase->Allocate(sizeX, sizeY, vide);
	}
}

void MemoryStruct::AllocateOverlap()
{
	m_OverLapLock.Lock();
	if (m_pOverLap == NULL)
	{
		m_pOverLap = MemoryOverlap::Create();
		MovRef(m_pOverLap);
		m_pOverLap->Lock();
		m_pOverLap->Allocate(m_OverlapEmptySizeX, m_OverlapEmptySizeY, -1);
		m_pOverLap->Unlock();
	}
	m_OverLapLock.Unlock();
}
