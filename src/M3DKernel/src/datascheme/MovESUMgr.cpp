#include "M3DKernel/datascheme/MovESUMgr.h"

using namespace BaseKernel;

namespace
{
	constexpr const char * LoggerName = "M3DKernel.datascheme.MovESUMgr";
}

MovESUMgr::MovESUMgr()
{
	m_pWorkingESU = 0;
	m_Parameter.ResetData();
	// Parametrage par defaut a 0.1 Miles,
	// 10 minutes ou 100 pings au cas ou
	// on ne charge pas de configuration
	m_Parameter.SetESUDistance(0.1);
	HacTime time(600, 0);
	m_Parameter.SetESUTime(time);
	m_Parameter.SetESUPingNumber(100);
	ResetData();
}

MovESUMgr::~MovESUMgr()
{
	if (m_pWorkingESU)
		MovUnRefDelete(m_pWorkingESU);
}


// ***********************************************************
// Traitements
// ***********************************************************

// initialisation des donn�es
void MovESUMgr::ResetData()
{
	m_ForceRenew = false;
	m_ProcessClosedESU = false;
	m_bUpdateCurrentESUStart = false;
	m_CurrentPosition.ResetData();
	if (m_pWorkingESU)
		MovUnRefDelete(m_pWorkingESU);
}

// Gestion des ESU
void MovESUMgr::ManageESU(PingFanContainer * pPingFanContainer, Traverser &e)
{
	// on nettoie les ESU dont on n'a plus les pings en m�moire. Pour cela
	// on r�cup�re le num�ro du ping le plus vieux.
	std::uint64_t oldestPingID = 1;
	PingFan * pFirstFan = (PingFan*)pPingFanContainer->GetPingFanContainer()->GetObjectWithIndex(0);
	if (pFirstFan)
	{
		oldestPingID = pFirstFan->GetPingId();
	}
	m_ESUContainer.CleanUp(oldestPingID);

	// 1er cas : pas d'ESU en cours : c'est le premier passage : on cr�e le nouvel ESU
	if (m_pWorkingESU == NULL)
	{
		CreateNewESU(false, pPingFanContainer);
		// broadCast de l'�v�nement de d�but d'ESU
		e.ESUStarted(m_pWorkingESU);
		m_ForceRenew = false;
		m_ProcessClosedESU = false;
		m_bUpdateCurrentESUStart = false;
	}
	// 2eme cas : un ESU est en cours. On doit y mettre un terme si
	// la condition de d�passement est remplie par le nouveau pingFan.
	else if (m_ForceRenew || !CurrentPingFanIsInCurrentESU())
	{
		// sauvgarde des infos de fin de l'esu
		m_pWorkingESU->SetESULastPingNumber(m_CurrentPosition.GetESUPingNumber());
		m_pWorkingESU->SetESUEndTime(m_CurrentPosition.GetESUTime());

		// Ajout de l'ancien ESU dans le conteneur des ESU
		bool bProcessESU = !m_ForceRenew || m_ProcessClosedESU;
		if (bProcessESU)
		{
			m_ESUContainer.AddESU(m_pWorkingESU);
		}

		// broadCast de l'�v�nement de fin d'ESU
		e.ESUEnded(m_pWorkingESU, !bProcessESU);

		// Creation du nouvel ESU
		CreateNewESU(!m_ForceRenew, pPingFanContainer);

		// broadCast de l'�v�nement de d�but d'ESU
		e.ESUStarted(m_pWorkingESU);
		m_ForceRenew = false;
		m_ProcessClosedESU = false;
	}
	// Pour corriger un probl�me de dimension d'ESU si l'ESU a �t� cr�� avec un premier ping 
	// qui n'appartient pas au sondeur de r�f�rence pour les ESU.
	else if (m_bUpdateCurrentESUStart && m_CurrentPosition.GetSounderRef() == m_pWorkingESU->GetSounderRef())
	{
		m_pWorkingESU->SetESUDistance(m_CurrentPosition.GetESUDistance());
		m_pWorkingESU->SetESUTime(m_CurrentPosition.GetESUTime());
		m_bUpdateCurrentESUStart = false;
	}
}

// Creation nouvel ESU
void MovESUMgr::CreateNewESU(bool continuous, PingFanContainer * pPingFanContainer)
{
	// OTK - FAE064 - si le sondeur de r�f�rence n'existe pas, on prend le premier de la liste
	HacTime lastESUTime;
	double lastESUDist;
	std::uint32_t lastSounderRef = m_Parameter.GetSounderRef();

	if (m_pWorkingESU)
	{
		lastESUDist = m_Parameter.GetESUDistance() + m_pWorkingESU->GetESUDistance();
		TimeElapse timeShift;
		timeShift.m_timeElapse = m_Parameter.GetESUTime().m_TimeFraction + m_Parameter.GetESUTime().m_TimeCpu * 10000;
		lastESUTime = m_pWorkingESU->GetESUTime() + timeShift;
		lastSounderRef = m_pWorkingESU->GetSounderRef();
	}

	MovUnRefDelete(m_pWorkingESU);
	m_pWorkingESU = ESUParameter::Create();
	// OTK - FAE064 - pour le nouvel ESU de travail, on se base sur le sondeur de r�f�rence du reader,
	// ou a d�faut du premier sondeur venu
	if (pPingFanContainer->m_sounderDefinition.GetSounderWithId(m_Parameter.GetSounderRef()) == NULL)
	{
		// pour ne pas changer de sondeur de r�f�rence � chaque SounderChanged, on prend en priorit� le m�me que pr�c�demment
		if (pPingFanContainer->m_sounderDefinition.GetSounderWithId(lastSounderRef) != NULL)
		{
			m_pWorkingESU->SetSounderRef(lastSounderRef);
		}
		else
		{
			m_pWorkingESU->SetSounderRef(pPingFanContainer->m_sounderDefinition.GetSounder(0)->m_SounderId);
		}
		M3D_LOG_WARN(LoggerName, Log::format("ESU Reference Sounder not found, using sounder[%d]", m_pWorkingESU->GetSounderRef()));
	}
	else
	{
		m_pWorkingESU->SetSounderRef(m_Parameter.GetSounderRef());
	}
	MovRef(m_pWorkingESU);

	// renseignement du type de d�coupage en fonction des param�tres de movies
	m_pWorkingESU->SetESUCutType(m_Parameter.GetESUCutType());

	m_bUpdateCurrentESUStart = false;
	// renseignement des coordonn�es de d�part de l'ESU
	// s'il s'agit d'un ESU forc� ou du premier ESU, on prend les coordonn�es courantes
	if (!continuous)
	{
		m_pWorkingESU->SetESUDistance(m_CurrentPosition.GetESUDistance());
		m_pWorkingESU->SetESUTime(m_CurrentPosition.GetESUTime());

		if (m_CurrentPosition.GetSounderRef() != m_pWorkingESU->GetSounderRef())
		{
			m_bUpdateCurrentESUStart = true;
		}
	}
	// s'il s'agit d'un ESU dans la continuit� du pr�c�dent, on prend les valeurs limites
	else
	{
		if (m_Parameter.GetESUCutType() == ESUParameter::ESU_CUT_BY_DISTANCE)
		{
			m_pWorkingESU->SetESUDistance(lastESUDist);
			m_pWorkingESU->SetESUTime(m_CurrentPosition.GetESUTime());
		}
		else if (m_Parameter.GetESUCutType() == ESUParameter::ESU_CUT_BY_TIME)
		{
			m_pWorkingESU->SetESUTime(lastESUTime);
			m_pWorkingESU->SetESUDistance(m_CurrentPosition.GetESUDistance());
		}
		else// if(m_Parameter.GetESUCutType() == ESU_PARAMETER::ESU_CUT_BY_PING_NB)
		{
			m_pWorkingESU->SetESUTime(m_CurrentPosition.GetESUTime());
			m_pWorkingESU->SetESUDistance(m_CurrentPosition.GetESUDistance());
		}
	}
	m_pWorkingESU->SetESUPingNumber(m_CurrentPosition.GetESUPingNumber());
}

// V�rifie si le pingFan courant appartient � l'ESU en cours
// CAD : on suppose que le temps est toujours croissant, donc
// test � sens unique.
// on g�re les cas de trous dans les donn�es avec le maxTemporalGap.
bool MovESUMgr::CurrentPingFanIsInCurrentESU()
{
	bool result = false;

	// OTK - FAE064 - on ne se base que sur les pings du sondeur de r�f�rence
	if (m_CurrentPosition.GetSounderRef() != m_pWorkingESU->GetSounderRef())
	{
		result = true;
	}
	else
	{
		double distance = m_CurrentPosition.GetESUDistance();
		HacTime time = m_CurrentPosition.GetESUTime();
		std::uint64_t pingID = m_CurrentPosition.GetESUPingNumber();

		TimeElapse timeShift;

		// en fonction du type de d�coupage, on v�rifie la bonne condition
		switch (m_pWorkingESU->GetESUCutType())
		{
		case ESUParameter::ESU_CUT_BY_DISTANCE:
			if (distance <= (m_Parameter.GetESUDistance() + m_pWorkingESU->GetESUDistance()))
			{
				result = true;
			}
			break;
		case ESUParameter::ESU_CUT_BY_TIME:
			// conversion du HacTime de param�trage de la dur�e des ESU
			// en timeElapse.
			timeShift.m_timeElapse = m_Parameter.GetESUTime().m_TimeFraction + m_Parameter.GetESUTime().m_TimeCpu * 10000;
			if (!(time > (m_pWorkingESU->GetESUTime() + timeShift)))
			{
				result = true;
			}
			break;
		case ESUParameter::ESU_CUT_BY_PING_NB:
			if (pingID < (m_Parameter.GetESUPingNumber() + m_pWorkingESU->GetESUPingNumber()))
			{
				result = true;
			}
			break;
		default:
			assert(false); // Mode de d�coupage des ESU inconnu
			break;
		}
	}
	return result;
}

// Met a jour la position courante
void MovESUMgr::SetCurrentPosition(double distance, HacTime time, std::uint64_t pingID,
	std::uint32_t sounderID)
{
	// test mis en place pour corriger des probl�mes en cas d'informations incoh�rentes
  // envoy�es par le sondeur qui n'est pas le sondeur de r�f�rence (ex. : un sondeur
  // envoie une distance parcourue de 0 car gap temporel sur ce sondeur, cf. m�thode ComputedNavigation::ComputeNavigation)
	if (!m_pWorkingESU || m_pWorkingESU->GetSounderRef() == sounderID)
	{
		// attention la distance pass�e est en unit�s SI alors que l'unit� de d�coupe des ESU est le mile
		m_CurrentPosition.SetESUDistance((distance / 1.852) / 1000.);
		m_CurrentPosition.SetESUTime(time);
		m_CurrentPosition.SetESUPingNumber(pingID);
	}
	m_CurrentPosition.SetSounderRef(sounderID);
}

// force la fin de l'ESU courant et le d�but du prochain
void MovESUMgr::ForceNewESU(bool bProcessClosedESU)
{
	// on doit le stopper au prochain manageESU.
	m_ForceRenew = true;
	m_ProcessClosedESU = bProcessClosedESU;
}

// OTK - FAE064 - utilisation d'un sondeur de r�f�rence pour le calcul des ESU
void MovESUMgr::SetSounderRef(std::uint32_t sounderID)
{
	m_Parameter.SetSounderRef(sounderID);
}
