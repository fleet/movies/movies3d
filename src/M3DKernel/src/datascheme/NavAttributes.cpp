#include "M3DKernel/datascheme/NavAttributes.h"

double NavAttributes::GetHeading(NavPosition * pNavPosition1, NavPosition * pNavPosition2)
{
	// variables locales
	double demi_latitude;
	double diff_longitude;
	double vecteur_est;
	double vecteur_nord;

	double longitudeDeg1 = pNavPosition1->m_longitudeDeg;
	double longitudeDeg2 = pNavPosition2->m_longitudeDeg;
	double latitudeDeg1 = pNavPosition1->m_lattitudeDeg;
	double latitudeDeg2 = pNavPosition2->m_lattitudeDeg;

	double cap;

	double deg_to_rad = 3.14159265367989 / 180.;

	if (longitudeDeg1 == 0 && latitudeDeg1 == 0)
	{
		cap = 0;
	}
	else
	{
		/************************************************/
		/* calcul du cap (0, 360) du point 1 au point 2 */
		/************************************************/

		demi_latitude = (latitudeDeg1 + latitudeDeg2) / 2.;
		diff_longitude = longitudeDeg2 - longitudeDeg1;

		if (diff_longitude > 180) diff_longitude -= 360.;
		if (diff_longitude < -180) diff_longitude += 360.;

		vecteur_est = 60. * diff_longitude * cos(demi_latitude*deg_to_rad);
		vecteur_nord = 60. * (latitudeDeg2 - latitudeDeg1);

		if (vecteur_nord == 0)
		{
			if (vecteur_est >= 0)
				cap = 90.;
			else
				cap = 270.;
		}
		else
		{
			cap = atan(vecteur_est / vecteur_nord) / deg_to_rad;
			if (vecteur_nord < 0)
				cap += 180.;
			else if (cap < 0)
				cap += 360.;
		}
	}
	return cap * deg_to_rad;
}

double NavAttributes::GetSpeed(std::list<NavPosition*> & navPositionsList)
{
	double speed = 0;

	short countValid = 0;
	double	speedSum = 0.0;

	double navPositionCount = navPositionsList.size();

	if (navPositionCount >= 2)
	{
		std::list<NavPosition*>::iterator iter = navPositionsList.begin();
		NavPosition * pNavPosition1 = *iter;
		NavPosition * pNavPosition2;
		for (iter++; iter != navPositionsList.end(); iter++)
		{
			pNavPosition2 = *iter;
			double speedTmp = GetSpeed(pNavPosition1, pNavPosition2);

			//Suppression des vitesse nulles et > 30 nds
			if (speedTmp > 0 && speedTmp < 30000)
			{
				speedSum += speedTmp;
				countValid++;
			}

			pNavPosition1 = pNavPosition2;
		}

		if (countValid > 0)
			speed = speedSum /= countValid;
	}

	return speed;
}

double NavAttributes::GetSpeed(NavPosition * pNavPosition1, NavPosition * pNavPosition2)
{
	TimeElapse timeElpase;

	if (pNavPosition2->m_ObjectTime > pNavPosition1->m_ObjectTime)
		timeElpase = pNavPosition2->m_ObjectTime - pNavPosition1->m_ObjectTime;
	else
		timeElpase = pNavPosition1->m_ObjectTime - pNavPosition2->m_ObjectTime;

	double distance = GetDistance(pNavPosition1, pNavPosition2);
	double speed_en_double;

	if (timeElpase > 0)
		speed_en_double = 36000000.* (distance / (timeElpase.m_timeElapse));
	else
		speed_en_double = 99000.;

	return  speed_en_double;
}

double 	NavAttributes::GetDistance(NavPosition * pNavPosition1, NavPosition * pNavPosition2)
{
	// variables locales
	double pi = 3.1415926535;
	double N1;
	double N2;
	double E1;
	double E2;
	double distance;
	double a_cos;

	if (pNavPosition1->m_longitudeDeg == 0 && pNavPosition1->m_lattitudeDeg == 0)
	{
		distance = 0;
	}
	else
	{
		// les lat et long sont transmises en 10 E-6 deg
		// la distance en milles nautiques entre 2 points est donn�e par
		// D = 6370000/1852 arc cos( sin N1 * sin N2 
		//				   + cos N1 * cos N2 *	cos (E2 - E1))
		// les fonctions sin et cos recoivent des radians en entr�e

		N1 = double(pNavPosition1->m_lattitudeDeg);
		//N1 /= 1000000;
		N1 = (N1*pi) / 180;
		N2 = double(pNavPosition2->m_lattitudeDeg);
		//N2 /= 1000000;
		N2 = (N2*pi) / 180;
		E1 = double(pNavPosition1->m_longitudeDeg);
		//E1 /= 1000000;
		E1 = (E1*pi) / 180;
		E2 = double(pNavPosition2->m_longitudeDeg);
		//E2 /= 1000000;
		E2 = (E2*pi) / 180;

		a_cos = acos(sin(N1)*sin(N2) + cos(N1)*cos(N2)*cos(E2 - E1));
		distance = 6370000 * a_cos / 1852;
	}

	return distance;

} // Fin CMvLatiLong::GetDistance