#include "M3DKernel/datascheme/PhaseDataObject.h"

PhaseDataObject::PhaseDataObject(void)
{
	m_timeFraction = 0;
	m_timeCPU = 0;
	m_hacChannelId = 0;
	m_transMode = 0;
	m_filePingNumber = 0;
}

PhaseDataObject::~PhaseDataObject(void)
{
	// NMD - FAE 117 - correction fuite m�moire
	MovUnRefDelete(m_pSamples);
}

int PhaseDataObject::capacity()
{
	return m_pSamples->m_sampleData.capacity();
}
void PhaseDataObject::reserve(unsigned int a)
{
	m_pSamples->m_sampleData.reserve(a);
}
void PhaseDataObject::push_back(const Phase & a)
{
	m_pSamples->m_sampleData.push_back(a);
}
int PhaseDataObject::size()
{
	return m_pSamples->m_sampleData.size();
}
void PhaseDataObject::setAt(int index, short along, short athwart)
{
	m_pSamples->m_sampleData[index].SetRawValues(along, athwart);
}
