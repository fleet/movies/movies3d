#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/NavAttributes.h"
#include <time.h>
#include <limits.h>
#include <math.h>
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/BackWardConst.h"

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "M3DKernel.datascheme.PingFan";
}

PingFan::~PingFan()
{
	MovUnRefDelete(m_pSounder);
	for (auto& it : m_pNavAttitudes)
	{
		MovUnRefDelete(it.second);
	}
	for (auto &it : m_pNavPositions)
	{
		MovUnRefDelete(it.second);
	}
	for (auto &it : m_pNavAttributes)
	{
		MovUnRefDelete(it.second);
	}

	SetMemorySetRef(NULL);
	SetFanMemoryRef(NULL);
	MapBeamDataObject::iterator res = m_BeamDataObject.begin();
	while (res != m_BeamDataObject.end())
	{
		if (res->second != NULL)
		{
			if (res->second->m_pSamples != NULL)
			{
				res->second->m_pSamples->m_amplitudes.clear();
				res->second->m_pSamples->m_angles.clear();
				M3DKernel::GetInstance()->getObjectMgr()->PushBeamDataSamples(res->second->m_pSamples);
				res->second->RecoverSamplesMemory();
			}
			M3DKernel::GetInstance()->getObjectMgr()->PushBeamDataObject(res->second);
			MovUnRefDelete(res->second);
		}
		res++;
	}
	m_BeamDataObject.clear();
	MapPhaseDataObject::iterator itr = m_PhaseDataObject.begin();
	while (itr != m_PhaseDataObject.end())
	{
		if (itr->second != NULL)
		{
			if (itr->second->m_pSamples != NULL)
			{
				itr->second->m_pSamples->m_sampleData.clear();
				M3DKernel::GetInstance()->getObjectMgr()->PushPhaseDataSamples(itr->second->m_pSamples);
				itr->second->RecoverSamplesMemory();
			}
			M3DKernel::GetInstance()->getObjectMgr()->PushPhaseDataObject(itr->second);
			MovUnRefDelete(itr->second);
		}
		itr++;
	}

	m_PhaseDataObject.clear();
	
	for (auto it : m_SpectralAnalysisDataObject)
	{
		delete it.second;
	}

	for (auto it : m_QuadrantDataObject)
	{
		delete it.second;
	}
}

PingFan::PingFan() 
	: m_pNavAttitudes{}
	, m_pNavPositions{}
	, m_pNavAttributes{}
	, m_pFirstChannelId{0}
{
	m_pSounder = NULL;
	m_ObjectTime.m_TimeCpu = std::numeric_limits<uint32_t>::max();
	m_ObjectTime.m_TimeFraction = std::numeric_limits<short>::max();
	m_pFanMemory = NULL;
	m_pMemorySet = NULL;
}

void PingFan::Compute(Traverser &e)
{
	m_bIsComplete = false;
	if (m_pSounder)
	{
		assert(m_pSounder->IsComplete());

		if (m_BeamDataObject.size() == m_pSounder->m_sounderComputeData.m_nbBeamPerFan && m_pSounder->m_sounderComputeData.m_nbBeamPerFan > 0)
		{
			for (const auto &it : m_BeamDataObject)
			{
				auto channelId = it.first;
				HacTime beamTime{ it.second->m_timeCPU, it.second->m_timeFraction };

				// OTK - 15/06/2009 - on associe � un pingfan les donn�es de navigation les plus r�centes mais dont la 
				// date est inf�rieure � la date du ping.
				NavAttributes* pAttr = NULL;
				size_t navCount = e.m_pHacObjectMgr->GetNavAttributesContainer()->GetObjectCount();

				for (size_t i = 1; i <= navCount && !pAttr; i++)
				{
					NavAttributes* pTempAttr = (NavAttributes*)e.m_pHacObjectMgr->GetNavAttributesContainer()->GetDatedObject(
						e.m_pHacObjectMgr->GetNavAttributesContainer()->GetObjectCount() - i);

					// si la nav a une date inf�rieur ou �gale au ping, on prend celle la
					if (!(pTempAttr->m_ObjectTime > beamTime))
					{
						pAttr = pTempAttr;
					}
				}

				// si on n'a pas de navigation respectant la chronologie, on prend la derniere en date en la datant
				// avec la date du ping (pour que l'�ventuel fichier HAC �crit produise le m�me r�sultat au d�codage)
				if (!pAttr)
				{
					M3D_LOG_WARN(LoggerName, "Could Not find chronological NavAttributes, Using non-chronological navigation");
					pAttr = (NavAttributes*)e.m_pHacObjectMgr->GetNavAttributesContainer()->GetDatedObject(
						e.m_pHacObjectMgr->GetNavAttributesContainer()->GetObjectCount() - 1);

					// modification de la date pour le fichier HAC en sortie
					if (pAttr)
					{
						MovRef(pAttr);
						e.m_pHacObjectMgr->GetNavAttributesContainer()->RemoveObject(pAttr);
						pAttr->m_ObjectTime = beamTime;
						e.m_pHacObjectMgr->GetNavAttributesContainer()->AddObject(pAttr);
						MovUnRefDelete(pAttr);
					}
					// seulement si on a l'option pour ne pas ignorer les pings sans donn�e de nav
					else if (!M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePingsWithNoNavigation())
					{
						M3D_LOG_WARN(LoggerName, "Could Not find NavAttributes, Using Default Value");
						NavAttributes *defaultNavAttributes = NavAttributes::Create();
						MovRef(defaultNavAttributes);
						defaultNavAttributes->m_bIsEstimated = true;
						defaultNavAttributes->m_ObjectTime = beamTime;
						defaultNavAttributes->m_speedMeter = 10. * 1.852 / 3.6; // 10nm par defaut : on se base sur la vitesse pour le calcul de la distance
						e.m_pHacObjectMgr->GetNavAttributesContainer()->AddObject(defaultNavAttributes);
						pAttr = defaultNavAttributes;
						MovUnRefDelete(defaultNavAttributes);
					}
				}

				// OTK - 01/06/2009 - les tuples position sont dat�s comme les pings
				NavPosition* pPos = (NavPosition*)e.m_pHacObjectMgr->GetNavPositionContainer()->FindDatedObject(beamTime);

				// OTK - FAE037 - Si la date exacte n'est pas trouv�e et que l'option d'interpolation est activ�e, on interpole,
				// sinon, on prend le dernier tuple position re�u
				if (!pPos)
				{
					// tentative d'interpolation si on a suffisamment (au moins 2) vrais tuples navigation
					if (M3DKernel::GetInstance()->GetRefKernelParameter().getInterpolatePingPosition())
					{
						pPos = e.m_pHacObjectMgr->CreateInterpolatedPosition(beamTime);
						e.m_pHacObjectMgr->GetNavPositionContainer()->AddObject(pPos);
					}

					// si on n'a pas interpol�, on prend la derni�re position s'il y en a une
					if (!pPos)
					{
						pPos = (NavPosition*)e.m_pHacObjectMgr->GetNavPositionContainer()->GetDatedObject(
							e.m_pHacObjectMgr->GetNavPositionContainer()->GetObjectCount() - 1);
					}

					// OTK - 01/06/2009 - en d�but de fichier, on n'a pas forcement de position : on en prend une par defaut.
					// OTK - FAE037 - si on n'a pas trouv� de position et qu'on choisi de ne pas ignorer les pings sans navigation,
					// on prend un NavPosition par d�faut
			//if(!pPos && !Kernel::GetInstance()->GetRefKernelParameter().getIgnorePingsWithNoNavigation())

			// NMD - FAE 120 - ajout du param�tre permettant d'ignorer les pings sans position
					if (!pPos && !M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePingsWithNoPosition())
					{
						M3D_LOG_WARN(LoggerName, "Could Not find NavPosition, Using Default Value");
						NavPosition *defaultNavPosition = NavPosition::Create();
						MovRef(defaultNavPosition);
						defaultNavPosition->m_ObjectTime = beamTime;
						e.m_pHacObjectMgr->GetNavPositionContainer()->AddObject(defaultNavPosition);
						pPos = defaultNavPosition;
						MovUnRefDelete(defaultNavPosition);
					}
				}

				SetNavAttributesRef(channelId, pAttr);

				SetNavPositionRef(channelId, pPos);

				Transducer *pTrans = m_pSounder->getTransducerForChannel(channelId);
				if (pTrans && pTrans->GetPlatform())
				{
					unsigned short m_ss = pTrans->GetPlatform()->m_attitudeSensorId;
					TimeObjectContainer *pContainer = e.m_pHacObjectMgr->GetNavAttitudeContainer(m_ss);
					NavAttitude *pNavAtt = (NavAttitude *)pContainer->FindDatedObject(beamTime);
					if (!pNavAtt)
					{
						if (M3DKernel::GetInstance()->GetRefKernelParameter().getInterpolateAttitudes())
						{
							pNavAtt = e.m_pHacObjectMgr->CreateInterpolatedAttitude(m_ss, beamTime);
							pContainer->AddObject(pNavAtt);
						}

						if (!pNavAtt)
						{
							//Interpolation failed or disabled
							pNavAtt = (NavAttitude*)pContainer->GetDatedObject(pContainer->GetObjectCount() - 1);
							if (pNavAtt) {
								M3D_LOG_WARN(LoggerName, "Could Not find NavAttitude with the same date Using an older NavAttitude");
							}
							else
							{
								M3D_LOG_WARN(LoggerName, "Could Not find NavAttitude with the same date Using Default Value");
								NavAttitude *defaultNavAtt = NavAttitude::Create();
								MovRef(defaultNavAtt);
								defaultNavAtt->m_ObjectTime = beamTime;
								// OTK - FAE179 - probl�me de m_AttitudeSensorId non initialis�e ici
								defaultNavAtt->m_AttitudeSensorId = m_ss;
								pContainer->AddObject(defaultNavAtt);
								pNavAtt = defaultNavAtt;
								MovUnRefDelete(defaultNavAtt);
							}
						}
					}
					SetNavAttitudeRef(channelId, pNavAtt);
				}
			}

			auto beamCount = m_BeamDataObject.size();
			if (m_pNavPositions.size() == beamCount && m_pNavAttitudes.size() == beamCount &&  m_pNavAttributes.size() == beamCount)
			{

				// now check for phase
				if (m_PhaseDataObject.size() > 0)
				{
					if (m_PhaseDataObject.size() == m_BeamDataObject.size())
					{
						m_bIsComplete = true;
					}
				}
				else if (m_PhaseDataObject.size() == 0)
					// no phase at all in the file 
				{
					m_bIsComplete = true;
				}
			}
		}
	}
}

void PingFan::OnComplete(Traverser &e)
{
	OnBeamChange();
}

void PingFan::OnBeamChange()
{
	MapBeamDataObject::iterator i = m_BeamDataObject.begin();
	/// compute heave
	computeHeave();

	std::uint32_t echoLu = 0;
	i = m_BeamDataObject.begin();
	while (i != m_BeamDataObject.end())
	{
		std::int32_t fond = i->second->m_bottomRange;
		/*
		Transducer *pTrans=m_pSounder->getTransducerForChannel(i->second->m_hacChannelId);
		std::uint32_t echoFond=(std::uint32_t) (fond/1000.0/pTrans->getBeamsSamplesSpacing());
		i->second->m_echoFondEval=echoFond;
		*/
		i->second->m_bottomWasFound = (fond != FondNotFound);
		i++;
	}

	computeRanges();
}

void PingFan::computeRanges()
{
	m_computePingFan.m_maxRangeFound = false;
	m_computePingFan.m_maxRange = 0;
	m_computePingFan.m_minRange = LONG_MAX;
	MapBeamDataObject::iterator i = m_BeamDataObject.begin();
	while (i != m_BeamDataObject.end())
	{
		std::int32_t beamRange = i->second->m_bottomRange;
		unsigned short beamId = i->second->m_hacChannelId;
		if (i->second->m_bottomWasFound)
		{
			Transducer *pTrans = m_pSounder->getTransducerForChannel(beamId);
			m_computePingFan.m_maxRange = std::max<std::int32_t>(m_computePingFan.m_maxRange, beamRange);
			m_computePingFan.m_minRange = std::min<std::int32_t>(m_computePingFan.m_minRange, beamRange);
			m_computePingFan.m_maxRangeFound = true;
		}
		i++;
	}
}

void PingFan::CopyData(MemoryStruct *pMem, Transducer *pTrans)
{
	unsigned int echoLu = 0;
	if (!pTrans)
		return;

	Vector2I size = pMem->GetDataFmt()->getSize();
	MapBeamDataObject::iterator i = m_BeamDataObject.begin();
	while (i != m_BeamDataObject.end())
	{
		SoftChannel *p = pTrans->getSoftChannel(i->second->m_hacChannelId);
		if (p)
		{
			unsigned int x = p->m_softChannelComputeData.m_PolarId;
			BeamDataObject* BeamDataObject = i->second;
			unsigned int offsetMemory = p->m_startSample;
			int sizeYToCpy = std::min<int>(BeamDataObject->m_pSamples->m_amplitudes.size() + offsetMemory, pMem->GetDataFmt()->getSize().y);
			DataFmt *pdata = pMem->GetDataFmt()->GetPointerToVoxel(Vector2I(x, 0));
			DataFmt *pangledata = pMem->GetAngleDataFmt()->GetPointerToVoxel(Vector2I(x, 0));
			for (unsigned int ct = 0; ct < offsetMemory; ct++)
			{
				*(pdata + ct) = UNKNOWN_DB;
				*(pangledata + ct) = UNKNOWN_DB;
			}
			for (unsigned int ct = 0; ct < sizeYToCpy - offsetMemory; ct++)
			{
				*(pdata + ct + offsetMemory) = *(BeamDataObject->m_pSamples->m_amplitudes.begin() + ct);
			}
			int sizeYToCpy_angle = std::min<int>(BeamDataObject->m_pSamples->m_angles.size() + offsetMemory, pMem->GetAngleDataFmt()->getSize().y);
			for (unsigned int ct = 0; ct < sizeYToCpy_angle - offsetMemory; ct++)
			{
				*(pangledata + ct + offsetMemory) = *(BeamDataObject->m_pSamples->m_angles.begin() + ct);
			}
			// on lib�re la m�moire des donn�e brutes pour les prochaines ping tuples
			BeamDataObject->m_pSamples->m_amplitudes.clear();
			BeamDataObject->m_pSamples->m_angles.clear();
			M3DKernel::GetInstance()->getObjectMgr()->PushBeamDataSamples(BeamDataObject->m_pSamples);
			BeamDataObject->RecoverSamplesMemory();
			//memcpy(pdata,(void*)&(*BeamDataObject->m_sampleData.begin()),sizeYToCpy*sizeof(DataFmt));	
			if (pMem->GetDataFmt()->getSize().y > sizeYToCpy + offsetMemory)
			{
				DataFmt * pStart = pMem->GetDataFmt()->GetPointerToVoxel(Vector2I(x, sizeYToCpy + offsetMemory));
				for (unsigned int j = 0; j < pMem->GetDataFmt()->getSize().y - (sizeYToCpy + offsetMemory); j++)
				{
					*pStart++ = UNKNOWN_DB;
				}
			}
			if (pMem->GetAngleDataFmt()->getSize().y > sizeYToCpy_angle + offsetMemory)
			{
				DataFmt * pStart = pMem->GetAngleDataFmt()->GetPointerToVoxel(Vector2I(x, sizeYToCpy_angle + offsetMemory));
				for (unsigned int j = 0; j < pMem->GetAngleDataFmt()->getSize().y - (sizeYToCpy_angle + offsetMemory); j++)
				{
					*pStart++ = UNKNOWN_DB;
				}
			}
		}
		i++;
	}

	if (pMem->GetPhase())
	{
		// copy phase
		Vector2I size = pMem->GetPhase()->getSize();
		MapPhaseDataObject::iterator i = m_PhaseDataObject.begin();
		while (i != m_PhaseDataObject.end())
		{
			SoftChannel *p = pTrans->getSoftChannel(i->second->m_hacChannelId);
			if (p)
			{
				unsigned int x = p->m_softChannelComputeData.m_PolarId;
				PhaseDataObject* phaseDataObject = i->second;
				int sizeYToCpy = std::min<int>(phaseDataObject->m_pSamples->m_sampleData.size(), pMem->GetPhase()->getSize().y);
				Phase *pdata = pMem->GetPhase()->GetPointerToVoxel(Vector2I(x, 0));
				for (unsigned int ct = 0; ct < sizeYToCpy; ct++)
				{
					*(pdata + ct) = *(phaseDataObject->m_pSamples->m_sampleData.begin() + ct);
				}
				// on lib�re la m�moire des donn�e brutes pour les prochaines ping tuples
				phaseDataObject->m_pSamples->m_sampleData.clear();
				M3DKernel::GetInstance()->getObjectMgr()->PushPhaseDataSamples(phaseDataObject->m_pSamples);
				phaseDataObject->RecoverSamplesMemory();

				if (pMem->GetPhase()->getSize().y > sizeYToCpy)
				{
					Phase * pStart = pMem->GetPhase()->GetPointerToVoxel(Vector2I(x, sizeYToCpy));
					Phase emptyValue;
					for (unsigned int j = 0; j < pMem->GetPhase()->getSize().y - sizeYToCpy; j++)
					{
						*pStart++ = emptyValue;
					}
				}
			}
			i++;
		}
	}
}

void PingFan::SetSounderRef(Sounder	*pSounder)
{
	if (pSounder)
	{
		MovUnRefDelete(m_pSounder);
		m_pSounder = pSounder;
		MovRef(m_pSounder);
	}
	DataChanged();
}

void PingFan::SetNavAttitudeRef(unsigned short channelId, NavAttitude *pNavAttitude)
{
	if (!pNavAttitude) {
		if (m_pNavAttitudes.count(channelId) == 1) {
			MovUnRefDelete(m_pNavAttitudes[channelId]);
			m_pNavAttitudes.erase(channelId);
		}
		return;
	}
	if (m_pNavAttitudes.count(channelId) == 1) {
		if (m_pNavAttitudes[channelId] == pNavAttitude) {
			return;
		}
		else {
			MovUnRefDelete(m_pNavAttitudes[channelId]);
		}
	}
	m_pNavAttitudes[channelId] = pNavAttitude;
	MovRef(pNavAttitude);
	DataChanged();
}

void PingFan::SetNavAttributesRef(unsigned short channelId, NavAttributes *pNavAttributes)
{
	if (!pNavAttributes) {
		if (m_pNavAttributes.count(channelId) == 1) {
			MovUnRefDelete(m_pNavAttributes[channelId]);
			m_pNavAttributes.erase(channelId);
		}
		return;
	}
	if (m_pNavAttributes.count(channelId) == 1) {
		if (m_pNavAttributes[channelId] == pNavAttributes) {
			return;
		}
		else {
			MovUnRefDelete(m_pNavAttributes[channelId]);
		}
	}
	m_pNavAttributes[channelId] = pNavAttributes;
	MovRef(pNavAttributes);
	DataChanged();
}

void PingFan::SetNavPositionRef(unsigned short channelId, NavPosition *pNavPosition)
{
	if (!pNavPosition) {
		if (m_pNavPositions.count(channelId) == 1) {
			MovUnRefDelete(m_pNavPositions[channelId]);
			m_pNavPositions.erase(channelId);
		}
		return;
	}
	if (m_pNavPositions.count(channelId) == 1) {
		if (m_pNavPositions[channelId] == pNavPosition) {
			return;
		}
		else {
			MovUnRefDelete(m_pNavPositions[channelId]);
		}
	}
	m_pNavPositions[channelId] = pNavPosition;
	MovRef(pNavPosition);
	DataChanged();
}

void PingFan::AddPhaseDataObject(PhaseDataObject*p)
{
	if (p)
	{
		MapPhaseDataObject::iterator res = m_PhaseDataObject.find(p->m_hacChannelId);

		if (res != m_PhaseDataObject.end())
		{
			// OTK - FAE082 - test sur la date optionnel
			if (M3DKernel::GetInstance()->GetRefKernelParameter().getIgnoreAsynchronousChannels() && !m_pSounder->IsSequencing())
			{
				HacTime myDate(p->m_timeCPU, p->m_timeFraction);
				if (!(myDate == this->m_ObjectTime))
				{
					ClearData();
				}
			}
		}

		MovRef(p);
		m_PhaseDataObject.insert(MapPhaseDataObject::value_type(p->m_hacChannelId, p));
	}

	//LB 12/11/2008 on se sert de l'heure des donn�es d'amplitude pour l'heure du ping fan
	// OTK - 01/06/2009 - la date est � priori rigoureusement la m�me, donc pas besoin de redater
	/*if(m_PhaseDataObject.size()>0)
	{
		this->m_ObjectTime.m_TimeFraction=m_PhaseDataObject.begin()->second->m_timeFraction;
		this->m_ObjectTime.m_TimeCpu=m_PhaseDataObject.begin()->second->m_timeCPU;; 	/// Time CPU
		this->m_computePingFan.m_filePingId = m_BeamDataObject.begin()->second->m_filePingNumber;
	}*/

	DataChanged();
}

void PingFan::AddBeamDataObject(BeamDataObject*p)
{
	if (p)
	{
		MapBeamDataObject::iterator first = m_BeamDataObject.begin();
		if (first != m_BeamDataObject.end())
		{
			// OTK - FAE082 - test sur la date optionnel
			if (M3DKernel::GetInstance()->GetRefKernelParameter().getIgnoreAsynchronousChannels() && !m_pSounder->IsSequencing())
			{
				HacTime myDate(p->m_timeCPU, p->m_timeFraction);
				// La date des BeamDataObject ek80 n'est pas la m�me pour tous les BeamDataObject : avoir une date diff�rente n'est pas un cas d'erreur.
				auto isEK80 = dynamic_cast<SounderEK80*> (m_pSounder) != nullptr;
				if (!(myDate == this->m_ObjectTime) && !isEK80)
				{
					ClearData();
				}
			}
		}

		auto currentDate = HacTime(p->m_timeCPU, p->m_timeFraction);
		if (m_pChannelIds.size() == 0 || currentDate < m_pFirstChannelDate) {
			m_pFirstChannelId = p->m_hacChannelId;
			m_pFirstChannelDate = currentDate;
		}

		MovRef(p);
		m_pChannelIds.push_back(p->m_hacChannelId);
		m_BeamDataObject.insert(MapBeamDataObject::value_type(p->m_hacChannelId, p));

		if (m_BeamDataObject.size() > 0)
		{
			this->m_ObjectTime.m_TimeFraction = m_BeamDataObject.begin()->second->m_timeFraction;
			this->m_ObjectTime.m_TimeCpu = m_BeamDataObject.begin()->second->m_timeCPU; 	/// Time CPU
			this->m_computePingFan.m_filePingId = m_BeamDataObject.begin()->second->m_filePingNumber;
		}
	}

	DataChanged();
}

void PingFan::AddSpectralAnalysisDataObject(unsigned short chanId, SpectralAnalysisDataObject*p)
{
	if (p)
	{
		m_SpectralAnalysisDataObject[chanId] = p;
	}
}

void PingFan::AddQuadrantDataObject(unsigned short chanId, QuadrantDataObject*p)
{
	if (p)
	{
		m_QuadrantDataObject[chanId] = p;
	}
}

NavAttitude * PingFan::GetNavAttitudeRef(unsigned short channelId)
{
	NavAttitude * result = nullptr;
	if (m_pNavAttitudes.count(channelId) == 1) {
		result = m_pNavAttitudes.at(channelId);
	}
	return result;
}

NavAttributes * PingFan::GetNavAttributesRef(unsigned short channelId)
{
	NavAttributes * result = nullptr;
	if (m_pNavAttributes.count(channelId) == 1) {
		result = m_pNavAttributes.at(channelId);
	}
	return result;
}

NavPosition * PingFan::GetNavPositionRef(unsigned short channelId)
{
	NavPosition * result = nullptr;
	if (m_pNavPositions.count(channelId) == 1) {
		result = m_pNavPositions.at(channelId);
	}
	return result;
}

// OTK - 01/06/2009 - factorisation du nettoyage du ping courant
void PingFan::ClearData()
{
	M3D_LOG_WARN(LoggerName, Log::format("Incomplete Fan date [%d.%d], deleting...", m_ObjectTime.m_TimeCpu, m_ObjectTime.m_TimeFraction));

	// Nettoyage de l'amplitude
	MapBeamDataObject::iterator res = m_BeamDataObject.begin();
	while (res != m_BeamDataObject.end())
	{
		if (res->second != NULL)
		{
			if (res->second->m_pSamples != NULL)
			{
				res->second->m_pSamples->m_amplitudes.clear();
				res->second->m_pSamples->m_angles.clear();
				M3DKernel::GetInstance()->getObjectMgr()->PushBeamDataSamples(res->second->m_pSamples);
				res->second->RecoverSamplesMemory();
			}
			M3DKernel::GetInstance()->getObjectMgr()->PushBeamDataObject(res->second);
			MovUnRefDelete(res->second);
		}
		res++;
	}
	m_BeamDataObject.clear();

	// Nettoyage de la phase
	MapPhaseDataObject::iterator res2 = m_PhaseDataObject.begin();
	while (res2 != m_PhaseDataObject.end())
	{
		if (res2->second != NULL)
		{
			if (res2->second->m_pSamples != NULL)
			{
				res2->second->m_pSamples->m_sampleData.clear();
				M3DKernel::GetInstance()->getObjectMgr()->PushPhaseDataSamples(res2->second->m_pSamples);
				res2->second->RecoverSamplesMemory();
			}
			M3DKernel::GetInstance()->getObjectMgr()->PushPhaseDataObject(res2->second);
			MovUnRefDelete(res2->second);
		}
		res2++;
	}
	m_PhaseDataObject.clear();

	// Nettoyage des données d'analyse spectrale
	for (auto it : m_SpectralAnalysisDataObject)
	{
		delete it.second;
	}
	m_SpectralAnalysisDataObject.clear();

	// Nettoyage des quadrants
	for (auto it : m_QuadrantDataObject)
	{
		delete it.second;
	}
	m_QuadrantDataObject.clear();
}

BeamDataObject* PingFan::getRefBeamDataObject(const unsigned short chanId, const unsigned short virtualChanId)
{
	BeamDataObject*ret = NULL;
	if (m_pSounder)
	{
		MapBeamDataObject::iterator res = m_BeamDataObject.find(chanId);
		if (res != m_BeamDataObject.end())
		{
			ret = res->second;
		}
		else
		{
			res = m_BeamDataObject.find(virtualChanId);
			if (res != m_BeamDataObject.end())
			{
				ret = res->second;
			}
			else
			{
				M3D_LOG_INFO(LoggerName, "getBeamDataObject::No Beam Found");
			}
		}

	}
	return ret;
}

PhaseDataObject* PingFan::getRefPhaseDataObject(const unsigned short chanId, const unsigned short virtualChanId)
{
	PhaseDataObject*ret = NULL;
	if (m_pSounder)
	{
		MapPhaseDataObject::iterator res = m_PhaseDataObject.find(chanId);
		if (res != m_PhaseDataObject.end())
		{
			ret = res->second;
		}
		else
		{
			res = m_PhaseDataObject.find(virtualChanId);
			if (res != m_PhaseDataObject.end())
			{
				ret = res->second;
			}
			else
			{
				M3D_LOG_INFO(LoggerName, "getPhaseDataObject::No Beam Found");
			}
		}

	}
	return ret;
}

SpectralAnalysisDataObject * PingFan::getSpectralAnalysisDataObject(const unsigned short chanId)
{
	SpectralAnalysisDataObject * ret = nullptr;
	auto it = m_SpectralAnalysisDataObject.find(chanId);
	if (it != m_SpectralAnalysisDataObject.end())
	{
		ret = it->second;
	}
	return ret;
}

QuadrantDataObject * PingFan::getQuadrantDataObject(const unsigned short chanId)
{
	QuadrantDataObject * ret = nullptr;
	auto it = m_QuadrantDataObject.find(chanId);
	if (it != m_QuadrantDataObject.end())
	{
		ret = it->second;
	}
	return ret;
}

void PingFan::computeHeave()
{
	assert(this->m_pNavAttitudes.size() == this->m_BeamDataObject.size());
	assert(this->m_pSounder);

	SounderMulti* pSounderMulti = dynamic_cast<SounderMulti*> (m_pSounder);
	if (pSounderMulti)
	{
		Platform *pPlatform = m_pSounder->GetTransducer(0)->GetPlatform();
#ifdef BEFORE_IBTS_08
		double heaveCompenseMovies = this->m_pNavAttitude->m_heaveMeter;

		double l_alongship_offset = pPlatform->GetAlongShipOffset();
		double l_athwartship_offset = pPlatform->GetAthwartShipOffset();
		double l_vertical_offset = pPlatform->GetDephtOffset();
		double l_pitch = m_pNavAttitude->m_pitchRad;
		double l_roll = m_pNavAttitude->m_rollRad;
		double  heave = heaveCompenseMovies - ((-1.0 * sin(l_pitch)*l_alongship_offset) +
			cos(l_pitch)*sin(l_roll)*l_athwartship_offset +
			(cos(l_pitch)*cos(l_roll) - 1.0)*l_vertical_offset);

		MapBeamDataObject::iterator i = m_BeamDataObject.begin();
		while (i != m_BeamDataObject.end())
		{

			double heaveCompenseNew = (double)heave - sin(l_pitch)*cos(l_roll)*l_alongship_offset +
				sin(l_roll)*l_athwartship_offset +
				(cos(l_pitch)*cos(l_roll) - 1.0)*l_vertical_offset;

			i->second->m_compensateHeave = heaveCompenseNew;


			i->second->m_compensateHeaveMovies = heaveCompenseMovies;
			i++;
		}
		this->m_pNavAttitude->m_heaveMeter = heave;
#else

		MapBeamDataObject::iterator i = m_BeamDataObject.begin();
		while (i != m_BeamDataObject.end())
		{
			double l_alongship_offset = pPlatform->GetAlongShipOffset();
			double l_athwartship_offset = pPlatform->GetAthwartShipOffset();
			double l_vertical_offset = pPlatform->GetDephtOffset();
			double l_pitch = m_pNavAttitudes.at(i->first)->m_pitchRad;
			double l_roll = m_pNavAttitudes.at(i->first)->m_rollRad;
			double heaveCompense = (double)m_pNavAttitudes.at(i->first)->m_heaveMeter
				- 1.0 * sin(l_pitch)*cos(l_roll)*l_alongship_offset +
				sin(l_roll)*l_athwartship_offset +
				(cos(l_pitch)*cos(l_roll) - 1.0)*l_vertical_offset;

			i->second->m_compensateHeave = heaveCompense;
			i++;
		}

#endif
	}
	SounderSBES* pSounderSBES = dynamic_cast<SounderSBES*> (m_pSounder);
	SounderEk500* pSounderEk500 = dynamic_cast<SounderEk500*> (m_pSounder);
	SounderGeneric* pSounderGeneric = dynamic_cast<SounderGeneric*> (m_pSounder);
	if (pSounderSBES || pSounderEk500 || pSounderGeneric)
	{

		MapBeamDataObject::iterator i = m_BeamDataObject.begin();
		while (i != m_BeamDataObject.end())
		{
			unsigned short beamId = i->second->m_hacChannelId;
			Platform *pPlatform = m_pSounder->getTransducerForChannel(beamId)->GetPlatform();
			NavAttitude* pNavAttitude = this->m_pNavAttitudes.at(i->first);
			assert(pPlatform);
			assert(pNavAttitude);
			if (pPlatform&&pNavAttitude)
			{

				double l_alongship_offset = pPlatform->GetAlongShipOffset();
				double l_athwartship_offset = pPlatform->GetAthwartShipOffset();
				double l_vertical_offset = pPlatform->GetDephtOffset();
				double l_pitch = pNavAttitude->m_pitchRad;
				double l_roll = pNavAttitude->m_rollRad;
				double l_result = (double)pNavAttitude->m_heaveMeter
					- 1.0 * sin(l_pitch)*cos(l_roll)*l_alongship_offset +
					sin(l_roll)*l_athwartship_offset +
					(cos(l_pitch)*cos(l_roll) - 1.0)*l_vertical_offset;
				i->second->m_compensateHeave = l_result;


				l_alongship_offset = floor(pPlatform->GetAlongShipOffset() * 100) / 100.0;
				l_athwartship_offset = floor(pPlatform->GetAthwartShipOffset() * 100) / 100.0;
				l_vertical_offset = floor(pPlatform->GetDephtOffset() * 100) / 100.00;
				l_pitch = pNavAttitude->m_pitchRad;
				l_roll = pNavAttitude->m_rollRad;
				double  heaveMovies = (double)pNavAttitude->m_heaveMeter - 1.0 * sin(l_pitch)*l_alongship_offset +
					cos(l_pitch)*sin(l_roll)*l_athwartship_offset + (cos(l_pitch)*cos(l_roll) - 1.0)*l_vertical_offset;
				i->second->m_compensateHeaveMovies = ((short)floor(heaveMovies*100.0)) / 100.0;

			}
			i++;
		}

	}
	return;
}

MapBeamDataObject::iterator PingFan::getBeam(const unsigned short chanId)
{
	MapBeamDataObject::iterator res = m_BeamDataObject.find(chanId);
	if (res == m_BeamDataObject.end()) {
		if (m_pSounder) {
			Transducer * pTrans = m_pSounder->getTransducerForChannel(chanId);
			if (pTrans) {
				SoftChannel * pSoftChannel = pTrans->getSoftChannel(chanId);
				unsigned short _chanId = pSoftChannel->getSoftwareChannelId();
				if (_chanId == chanId)
					_chanId = pSoftChannel->getSoftwareVirtualChannelId();
				res = m_BeamDataObject.find(_chanId);
			}
		}
	}
	return res;
}

MapPhaseDataObject::iterator PingFan::getPhase(const unsigned short chanId)
{
	MapPhaseDataObject::iterator res = m_PhaseDataObject.find(chanId);
	if (res == m_PhaseDataObject.end()) {
		if (m_pSounder) {
			Transducer * pTrans = m_pSounder->getTransducerForChannel(chanId);
			if (pTrans) {
				SoftChannel * pSoftChannel = pTrans->getSoftChannel(chanId);
				unsigned short _chanId = pSoftChannel->getSoftwareChannelId();
				if (_chanId == chanId)
					_chanId = pSoftChannel->getSoftwareVirtualChannelId();
				res = m_PhaseDataObject.find(_chanId);
			}
		}
	}
	return res;
}

void PingFan::getBottom(const unsigned short chanId, std::uint32_t	&echoFondEval, std::int32_t &bottomRange, bool &found)
{
	found = false;
	if (m_pSounder)
	{
		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			//LB si le fond n'est pas detect� en renvoie 0 pour ne pas avoir de probl�mes en EI
			if (res->second->m_bottomWasFound)
			{
				bottomRange = res->second->m_bottomRange;
				Transducer *pTrans = m_pSounder->getTransducerForChannel(res->second->m_hacChannelId);
				echoFondEval = (std::uint32_t)(bottomRange  * 1e-3 / pTrans->getBeamsSamplesSpacing() + 0.5);

				found = true;
			}
			else
			{
				echoFondEval = 0;
			}
		}
		else
		{
			M3D_LOG_INFO(LoggerName, "getBottom::No Beam Found");
		}
	}
}

// OTK - FAE050 - pour modification du fond d�tect� par le module de d�tection du fond interne � M3D
void PingFan::setBottom(const unsigned short chanId, std::int32_t bottomRange, bool found)
{
	if (m_pSounder)
	{
		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			res->second->m_bottomWasFound = found;
			if (found)
			{
				res->second->m_bottomRange = bottomRange;
			}
			else
			{
				res->second->m_bottomRange = FondNotFound;
			}
		}
		else
		{
			M3D_LOG_INFO(LoggerName, "setBottom::No Beam Found");
		}
	}
}

/*
double PingFan::getAveragePower(const unsigned short chanId, bool &found, int firstEcho, int lastEcho)
{
	found = false;
	double power = 0.0;
	if (m_pSounder)
	{
		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			power = res->second->computeAveragePower(found, firstEcho, lastEcho);
		}
		else
		{
			M3D_LOG_INFO(LoggerName, "getAveragePower::No Beam Found");
		}
	}

	return power;
}

double PingFan::getAverageNoiseLevel(const unsigned short chanId, bool &found)
{
	found = false;
	double noiseLevel = 0.0;
	if (m_pSounder)
	{
		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			noiseLevel = res->second->getAverageNoiseLevel(found);
		}
		else
		{
			M3D_LOG_INFO(LoggerName, "getAverageNoiseLevel::No Beam Found");
		}
	}

	return noiseLevel;
}
*/

std::map<int, double> PingFan::getAverageNoiseLevel(const unsigned short chanId)
{
	std::map<int, double> averageNoiseLevel;
	if (m_pSounder)
	{
		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			averageNoiseLevel = res->second->getAverageNoiseLevel();
		}
		else
		{
			M3D_LOG_INFO(LoggerName, "getAverageNoiseLevel::No Beam Found");
		}
	}
	return averageNoiseLevel;
}

int PingFan::getNoiseEchoNum(const unsigned short chanId, bool &found)
{
	found = false;
	int echo = -1;
	if (m_pSounder)
	{
		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			echo = res->second->getNoiseRangeEchoNum();
		}
		else
		{
			M3D_LOG_INFO(LoggerName, "getNoiseEchoNum::No Beam Found");
		}
	}

	found = echo >= 0;

	return echo;
}

int PingFan::getRefNoiseEchoNum(const unsigned short chanId, bool &found)
{
	found = false;
	int echo = -1;
	if (m_pSounder)
	{
		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			echo = res->second->getRefNoiseRangeEchoNum();
		}
		else
		{
			M3D_LOG_INFO(LoggerName, "getNoiseEchoNum::No Beam Found");
		}
	}
	found = echo >= 0;
	return echo;
}

void PingFan::setDeviation(const unsigned short chanId, double deviation)
{
	if (m_pSounder)
	{
		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			res->second->m_hasDeviation = true;
			res->second->m_deviation = deviation;
		}
		else
		{
			M3D_LOG_INFO(LoggerName, "setDeviation::No Beam Found");
		}
	}
}


void PingFan::getDeviation(const unsigned short chanId, double &deviation, bool &found)
{
	found = false;
	if (m_pSounder)
	{
		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			if (res->second->m_hasDeviation)
			{
				deviation = res->second->m_deviation;
				Transducer *pTrans = m_pSounder->getTransducerForChannel(res->second->m_hacChannelId);
				found = true;
			}
		}
		else
		{
			M3D_LOG_INFO(LoggerName, "getDeviation::No Beam Found");
		}
	}
}

double PingFan::getHeaveChan(const unsigned short chanId)
{
	double heave = 0;
	if (m_pSounder)
	{

		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			heave = res->second->m_compensateHeave;
		}
		else {
			M3D_LOG_INFO(LoggerName, "getHeaveChan::No Beam Found");
		}

	}
	return heave;
}

double PingFan::getHeaveChanMovies(const unsigned short chanId)
{
	double heave = 0;
	if (m_pSounder)
	{

		MapBeamDataObject::iterator res = getBeam(chanId);
		if (res != m_BeamDataObject.end())
		{
			heave = res->second->m_compensateHeaveMovies;
		}
		else {
			M3D_LOG_INFO(LoggerName, "getHeaveChan::No Beam Found");
		}

	}
	return heave;
}

BaseMathLib::Matrix3 & PingFan::GetMatrixNav(unsigned short channelId) { return m_MatrixNavs.at(channelId); }

BaseMathLib::Matrix3 & PingFan::GetHeadingMatrixNav(unsigned short channelId) { return m_HeadingMatrixNavs.at(channelId); }

BaseMathLib::Vector3D & PingFan::GetTranslationNav(unsigned short channelId) { return m_TranslationNavs.at(channelId); }

void PingFan::ComputeMatrix()
{
	for (const auto & it : m_BeamDataObject) {
		auto channelId = it.first;
		if (m_HeadingMatrixNavs[channelId] == Matrix3::IDENTITY) {
			ComputeMatrixHeading();
		}

		m_MatrixNavs[channelId].CreateRotationMatrix(this->GetNavAttributesRef(channelId)->m_headingRad, GetNavAttitudeRef(channelId)->m_pitchRad, GetNavAttitudeRef(channelId)->m_rollRad);
		m_TranslationNavs[channelId] = BaseMathLib::Vector3D(0, 0, GetNavAttitudeRef(channelId)->m_heaveMeter) + m_relativePingFan.m_cumulatedNav;
	}
}

void PingFan::ComputeMatrixHeading()
{
	for (const auto & it : m_BeamDataObject) {
		auto channelId = it.first;
		m_HeadingMatrixNavs[channelId].CreateRotationMatrix(this->GetNavAttributesRef(channelId)->m_headingRad, 0, 0);
	}
}

// renvoie le ping pr�c�dent pour un sondeur donn�
PingFan* PingFan::getPreviousPingFan()
{
	PingFan * result = NULL;
	Sounder* pSound = this->getSounderRef();
	if (pSound)
	{
		TimeObjectContainer* pCont = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(pSound->m_SounderId);
		bool found = false;
		if (pCont)
		{
			size_t nbPingFan = pCont->GetObjectCount();
			for (int i = (int)nbPingFan - 1; i >= 0 && !found; i--)
			{
				PingFan * pFan = (PingFan*)pCont->GetDatedObject(i);
				if (pFan)
				{
					if (pFan->m_computePingFan.m_pingId < this->m_computePingFan.m_pingId)
					{
						found = true;
						result = pFan;
					}
				}
			}
		}
	}
	return result;
}

double PingFan::getPingDistance()
{
	double pingDistance;
	PingFan * lastFan = this->getPreviousPingFan();
	if (lastFan)
	{
		pingDistance = this->m_relativePingFan.m_cumulatedDistance - lastFan->m_relativePingFan.m_cumulatedDistance;
		if (pingDistance < 0)
		{
			pingDistance = 0;
		}
	}
	else
	{
		pingDistance = 0.0;
	}

	return pingDistance;
}
