// -*- C++ -*-
// ****************************************************************************
// Class: PingFanConsumer
//
// Description: Interface permettant au noyau d'attendre que les algorithmes
// de traitement asynchrone sur les pings (comme la r�ponse fr�quentielle)
// aient termin� d'utiliser un pingfan avant de le supprimer.
//
// le PingFanConsumer peut au choix faire attendre le noyau, ou bien abandonner
// son traitement avant de laisser le noyau supprimer le PingFan.
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Octobre 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "M3DKernel/datascheme/PingFanConsumer.h"

// ***************************************************************************
// Declarations
// ***************************************************************************

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************
// Constructeur par d�faut
PingFanConsumer::PingFanConsumer()
{
}

// Destructeur
PingFanConsumer::~PingFanConsumer()
{
}