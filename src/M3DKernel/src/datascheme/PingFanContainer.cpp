#include "M3DKernel/datascheme/PingFanContainer.h"
#include "M3DKernel/datascheme/NavAttributes.h"

#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/TransformSet.h"
/*
#include "M3DKernel/datascheme/SoftChannel.h"
*/
// IPSIS - OTK - ajout ESUManager
#include "M3DKernel/datascheme/MovESUMgr.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/utils/multithread/Synchronized.h"

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "M3DKernel.datascheme.PingFanContainer";
}

PingFanContainer::~PingFanContainer()
{
	MovUnRefDelete(m_pingFanContainer);
	MovUnRefDelete(m_singleTargetContainer);
}

PingFanContainer::PingFanContainer()
{
	m_nextInternalpingId = 1;
	
	m_singleTargetContainer = SounderIndexedTimeContainer::Create();
	MovRef(m_singleTargetContainer);

	m_pingFanContainer = SounderIndexedTimeContainer::Create();
	MovRef(m_pingFanContainer);

	m_nbRecord = 0;
}

void PingFanContainer::AddPingFan(PingFan *pFan)
{
	assert(pFan);
	assert(pFan->IsComplete());

	Sounder *pSounder = pFan->m_pSounder;

	BaseMathLib::Vector3D lastLenght;
	HacTime lastTime;
	bool temporalGap = false;
	PingFan *pLastFan = NULL;
	// OTK - FAE064 - prise en compte de l'�cart temporel entre pings de m�me sondeur (pour pallier
	// aux d�synchronisations temporelles entre sondeurs)
	TimeObjectContainer * pPingFanContainer = m_pingFanContainer->GetSounderIndexedContainer(pSounder->m_SounderId);
	if (pPingFanContainer != NULL && pPingFanContainer->GetObjectCount() > 0)
	{
		pLastFan = (PingFan *)pPingFanContainer->GetDatedObject(pPingFanContainer->GetObjectCount() - 1);
		lastLenght = pLastFan->m_relativePingFan.m_cumulatedNav;
		lastTime = pLastFan->m_ObjectTime;

		// OTK - 03/04/2009 - ajout de la gestion du maxTemporalGap
		TimeElapse timeElapsed = pFan->m_ObjectTime - lastTime;
		TimeElapse maxTemporalgap = M3DKernel::GetInstance()->getMovESUManager()->GetRefParameter().getMaxTemporalGap();
		std::int64_t timeElapsedAbsolute = timeElapsed.m_timeElapse > 0 ? timeElapsed.m_timeElapse : -timeElapsed.m_timeElapse;
		if (timeElapsedAbsolute > maxTemporalgap.m_timeElapse)
		{
			// FAE 1978 - on essaye de d�tecter au mieux les cas dans lesquels on souhaite pouvoir traiter tout
			// de m�me les donn�es de l'ESU : cas de l'arr�t puis red�marrage de l'acquisition avec red�marrage de
			// la num�rotation des pings.
			// rmq. : le test ne porte pas sur un num�ro de ping fichier �gal � un car le sondeur de r�f�rence pour les ESU
			// peut ne pas �tre associ� au ping num�ro 1 du fichier (cas multisondeur). On peut tout de m�me v�rifier que le num�ro
			// du ping lu dans le nouveau fichier est inf�rieur au pr�c�dent, et que le gap temporel est dans le bon sens (chronologique).
			bool bProcessClosedESU = pFan->m_computePingFan.m_filePingId < pLastFan->m_computePingFan.m_filePingId
				&& timeElapsed.m_timeElapse > 0;
			M3DKernel::GetInstance()->getMovESUManager()->ForceNewESU(bProcessClosedESU);
			// OTK - FAE201 - utilisation pour les sorties de l'EI supervis�e d'un num�ro d'EI qui red�marre apr�s un gap temporel
			M3DKernel::GetInstance()->getMovESUManager()->GetESUContainer().ResetDisplayEsuId();
			M3D_LOG_WARN(LoggerName, "Temporal Gap Detected !");
			temporalGap = true;
		}

		if (M3DKernel::GetInstance()->GetRefKernelParameter().getLogPingFanInterTime())
		{
			M3D_LOG_WARN(LoggerName, Log::format("Ping Fan interTime : %d", timeElapsed.m_timeElapse));
		}
	}

	pFan->ComputeMatrixHeading();

	SpeedType type = M3DKernel::GetInstance()->GetRefKernelParameter().getSpeedUsed();
	double speed = 0.0f;
	switch (type)
	{

	case SPTW:
		if (pLastFan)
		{
			speed = pLastFan->GetNavAttributesRef(pLastFan->GetFirstChannelId())->m_speedMeter;
		}
		break;
	case SPUSER:
	default:
		speed = M3DKernel::GetInstance()->GetRefKernelParameter().getUserSpeedValue();
		break;
	}

	// OTK - FAE043 - mode de positionnement fixe
	PositionType posType = M3DKernel::GetInstance()->GetRefKernelParameter().getPositionUsed();
	switch (posType)
	{
	case POSITION_TUPLE:
	{
		// pas de traitement sur la position associ�e au ping
	}
	break;
	case POSITION_USER:
	{
		// on force la position � celle indiqu�e dans les param�tres
		Vector3D fixedPos = M3DKernel::GetInstance()->GetRefKernelParameter().getUserPositionValue();
		pFan->GetNavPositionRef(pFan->GetFirstChannelId())->m_lattitudeDeg = fixedPos.x;
		pFan->GetNavPositionRef(pFan->GetFirstChannelId())->m_longitudeDeg = fixedPos.y;
	}
	break;
	default:
	{
		M3D_LOG_ERROR(LoggerName, "Incorrect Position Source Type");
		assert(0);
	}
	break;
	}

	m_computedNavigation.ComputeNavigation(pFan, pLastFan, speed, temporalGap);

	pFan->ComputeMatrix();
	// OTK - FAE043 - profondeur fixe
	if (posType == POSITION_USER)
	{
		pFan->GetTranslationNav(pFan->GetFirstChannelId()).z += M3DKernel::GetInstance()->GetRefKernelParameter().getUserPositionValue().z;
	}
	pFan->m_computePingFan.m_pingId = m_nextInternalpingId;
	m_nextInternalpingId++;

	// IPSIS - OTK - Les donn�es distance, temps et PingID sont calcul�es / mises � jour
	// on les transmet � l'ESUManager
	M3DKernel::GetInstance()->getMovESUManager()->SetCurrentPosition(pFan->m_relativePingFan.m_cumulatedDistance,
		pFan->m_ObjectTime, pFan->GetPingId(), pSounder->m_SounderId);


	FanMemory *pFanMem = this->m_sounderDefinition.GetLinkedFanMemory(pSounder);
	pFan->SetFanMemoryRef(pFanMem);

	TimeObjectContainer *pTimed = m_pingFanContainer->GetSounderIndexedContainer(pSounder->m_SounderId);
	while (pTimed->GetObjectCount() > m_nbRecord - 1)
	{
		PingFan *pDeleteFan = (PingFan *)pTimed->GetDatedObject(0);
		Sounder *pDeletSounder = pDeleteFan->getSounderRef();

		// OTK - 15/10/2009 - attente �ventuelle des traitements asynchrones sur ce ping avant sa suppression
		WaitForPingFanConsumers(pDeleteFan->GetPingId());

		// OTK - FAE062 - pas la peine de shifter les vieilles FanMemory, par contre on doit shifter celle pour laquelle
		// on ajoute le nouveau pingfan.
		pFanMem->ShiftOne();
		// OTK - FAE003 - Fuite m�moire - lorsqu'on d�truit un pingFan, on d�truit �galement les single targets associ�es (sinon elles ne sont jamais lib�r�es)
		PingFanSingleTarget* pDeleteSingleTargets = GetPingFanSingleTarget(pDeleteFan->m_ObjectTime, pSounder->m_SounderId);
		m_singleTargetContainer->RemoveObject(pDeleteSingleTargets, pSounder->m_SounderId);

		m_pingFanContainer->RemoveObject(pDeleteFan, pDeleteFan->m_pSounder->m_SounderId);

		// OTK - FAE065 - il faut nettoyer les vecteurs de sondeurs associ�s
		// � la fanMemory en cas de suppression d'un ping (le dernier a r�f�rencer le sondeur....)
		m_sounderDefinition.CleanSounders(pDeletSounder, (PingFan*)pTimed->GetDatedObject(0));
	}

	m_pingFanContainer->AddObject(pFan, pSounder->m_SounderId);
	/*	SoftChannel *pSoftChan=pSounder->getSoftChannelPolarX(0);
	pSoftChan->getSoftwareChannelId();

	M3D_LOG_INFO(LoggerName, "Add PingFan file Id for chan[%d]: %d date[%d:%d]",pSoftChan->getSoftwareChannelId(),pFan->getRefBeamDataObject(pSoftChan->getSoftwareChannelId())->m_filePingNumber,
	pFan->m_ObjectTime.m_TimeCpu,
	pFan->m_ObjectTime.m_TimeFraction);
	*/
	// OTK - 15/01/2010 - utilisation d'un maxrange par sondeur
	double maxRange = GetMaxRangeUsed(pSounder->m_SounderId, pTimed);
	// OTK - 13/03/2009 - on doit v�rifier le nombre max d'�chos (qui peut varier m�me a maxrange constant
	// en fonction de la c�l�rit� du son)
	unsigned int maxEchoNb = GetMaxEchoNbUsed(pSounder->m_SounderId);
	size_t nbPingsForSounder = pTimed->GetObjectCount();
	pFanMem->SetMaxDepth(maxRange, maxEchoNb);
	// OTK - FAE065 - application de la nouvelle dimension aux transformMaps
	// de l'ensemble des sondeurs associ�s � la zone m�moire
	std::vector<Sounder*> sounders = m_sounderDefinition.GetSoundersWithId(pSounder->m_SounderId);
	size_t nbSounder = sounders.size();
	for (size_t i = 0; i < nbSounder; i++)
	{
		sounders[i]->GetTransformSetRef()->SetMaxRange(maxRange, maxEchoNb);
	}

	MemorySet *pMemSet = pFanMem->GetMemoryIndex(nbPingsForSounder - 1);
	pMemSet->SetSounder(pSounder);
	pMemSet->SetMaxRange(pFanMem->GetMaxRange());
	for (unsigned int i = 0; i < pSounder->GetTransducerCount(); i++)
	{
		MemoryStruct *pMemStruct = pMemSet->GetMemoryStruct(i);
		pFan->CopyData(pMemStruct, pSounder->GetTransducer(i));
		pMemStruct->CleanFilterAndOverlap(maxRange);
	}
	pFan->SetMemorySetRef(pMemSet);
}


PingFanSingleTarget * PingFanContainer::GetPingFanSingleTarget(const HacTime &ref, std::uint32_t sounderId) const
{
	TimeObjectContainer * container = m_singleTargetContainer->GetSounderIndexedContainer(sounderId);
	if (container == NULL)
		return NULL;
	return (PingFanSingleTarget*)container->FindDatedObject(ref);
}

void PingFanContainer::PushSingleTarget(SingleTargetDataObject *pTarget)
{
	PingFanSingleTarget *p = (PingFanSingleTarget *)this->m_singleTargetContainer->GetObjectWithDate(pTarget->m_PingTime);
	if (!p)
	{
		p = PingFanSingleTarget::Create();
		MovRef(p);
		p->m_ObjectTime = pTarget->m_PingTime;
		p->m_pingNumber = pTarget->m_pingNumber;
		this->m_singleTargetContainer->AddObject(p, pTarget->GetSounderRef()->m_SounderId);
		MovUnRefDelete(p);
	}
	p->AddTarget(pTarget);
}

bool PingFanContainer::FindFanIndexWithId(std::uint64_t fanId, unsigned int &refIndex, bool bWarn)
{
	bool retValue = false;
	// 
	if (GetNbFan()) {

		std::uint64_t fanFirstId = ((PingFan*)m_pingFanContainer->GetObjectWithIndex(0))->GetPingId();
		std::uint64_t fanLastId = ((PingFan*)m_pingFanContainer->GetObjectWithIndex(m_pingFanContainer->GetObjectCount() - 1))->GetPingId();

		if (fanId - fanFirstId >= 0 && fanId - fanFirstId < GetNbFan())
		{
			size_t index = (size_t)(fanId - fanFirstId);
			if (fanId == ((PingFan*)m_pingFanContainer->GetObjectWithIndex(index))->GetPingId())
			{
				refIndex = index;
				retValue = true;
				return retValue;
			}
		}
		if (!retValue && bWarn)
		{
			M3D_LOG_WARN(LoggerName, "FindFanIndexWithId index may be wrong");
		}

		// we check if we need to parse from start to stop or from stop to start
		if (abs((int)(fanId - fanFirstId)) < abs((int)(fanId - fanLastId)))
		{
			for (unsigned int i = 0; i < GetNbFan(); i++)
			{
				if (((PingFan*)m_pingFanContainer->GetObjectWithIndex(i))->GetPingId() == fanId)
				{
					refIndex = i;
					retValue = true;
					return retValue;
				}
			}
		}
		else
		{
			for (int i = GetNbFan() - 1; i >= 0; i--)
			{
				if (((PingFan*)m_pingFanContainer->GetObjectWithIndex(i))->GetPingId() == fanId)
				{
					refIndex = i;
					retValue = true;
					return retValue;
				}
			}

		}
	}
	return retValue;
}

PingFan* PingFanContainer::GetPingFanWithId(std::uint64_t fanId, bool bWarn)
{
	unsigned int refIndex = 0;
	if (FindFanIndexWithId(fanId, refIndex, bWarn))
	{
		return ((PingFan*)m_pingFanContainer->GetObjectWithIndex(refIndex));
	}
	return NULL;
}

float PingFanContainer::GetMaxRangeUsed()
{
	float result = 0;
	bool found = false;

	MapMaxRange::iterator it = m_maxRangeUsed.begin();
	while (it != m_maxRangeUsed.end())
	{
		found = true;
		result = std::max<float>(result, it->second);
		it++;
	}

	if (!found)
	{
		result = M3DKernel::GetInstance()->GetRefKernelParameter().getMaxRange();
	}

	return result;
}

float PingFanContainer::GetMaxRangeUsed(std::uint32_t sounderID, TimeObjectContainer *pTimed)
{
	std::int32_t maxRangeL = 0;
	bool OneBottomWasFound = false;

	if (M3DKernel::GetInstance()->GetRefKernelParameter().getAutoDepthEnable())
	{
		const size_t nbFans = pTimed->GetObjectCount();
		for (int i = 0; i < nbFans; ++i)
		{
			std::int32_t maxFrameRangeL;
			bool found = false;
			((PingFan*)pTimed->GetDatedObject_unsafe(i))->m_computePingFan.getBottomMaxRangeAsLong(maxFrameRangeL, found);
			if (found)
			{
				if (maxFrameRangeL > maxRangeL) maxRangeL = maxFrameRangeL;
				OneBottomWasFound = found;
			}
		}
	}

	if (OneBottomWasFound)
	{
		double myMaxRange = maxRangeL*0.001 + M3DKernel::GetInstance()->GetRefKernelParameter().getAutoDepthTolerance();

		float shift = (int)((myMaxRange - m_maxRangeUsed[sounderID]) / M3DKernel::GetInstance()->GetRefKernelParameter().getAutoDepthTolerance());
		if (myMaxRange > m_maxRangeUsed[sounderID])
		{
			shift = std::max<float>(1.0f, shift);
		}
		if (shift != 0)
		{
			m_maxRangeUsed[sounderID] = m_maxRangeUsed[sounderID] + shift * M3DKernel::GetInstance()->GetRefKernelParameter().getAutoDepthTolerance();
		}
	}
	else
	{
		m_maxRangeUsed[sounderID] = M3DKernel::GetInstance()->GetRefKernelParameter().getMaxRange();
	}

	return m_maxRangeUsed[sounderID];
}

unsigned int PingFanContainer::GetMaxEchoNbUsed(std::uint32_t sounderID)
{
	unsigned int maxEchoNb = 0;
	std::vector<Sounder*> sounders = m_sounderDefinition.GetSoundersWithId(sounderID);
	// on recup�re le samplespacing minimum sur tous les sondeurs
	size_t nbSounder = sounders.size();
	for (size_t i = 0; i < nbSounder; i++)
	{
		Sounder * pSound = sounders[i];
		int nbTrans = pSound->GetTransducerCount();
		for (int j = 0; j < nbTrans; j++)
		{
			double sampleSpacing = pSound->GetTransducer(j)->getBeamsSamplesSpacing();
			unsigned int maxFrameEchoNb = (unsigned int)(m_maxRangeUsed[sounderID] / sampleSpacing);
			maxEchoNb = std::max<unsigned int>(maxEchoNb, maxFrameEchoNb);
		}
	}

	return maxEchoNb;
}

void PingFanContainer::RemoveAllFan()
{
	while (m_pingFanContainer->GetObjectCount())
	{
		PingFan *pFan = (PingFan *)m_pingFanContainer->GetObjectWithIndex(0);

		// OTK - 15/10/2009 - attente �ventuelle des traitements asynchrones sur ce ping avant sa suppression
		WaitForPingFanConsumers(pFan->GetPingId(), false);

		// OTK - FAE003 - Fuite m�moire - lorsqu'on d�truit un pingFan, on d�truit �galement les single targets associ�es (sinon elles ne sont jamais lib�r�es)
		Sounder *pSounder = pFan->m_pSounder;
		PingFanSingleTarget* pDeleteSingleTargets = GetPingFanSingleTarget(pFan->m_ObjectTime, pSounder->m_SounderId);
		m_singleTargetContainer->RemoveObject(pDeleteSingleTargets, pSounder->m_SounderId);
		m_pingFanContainer->RemoveObject(pFan, pFan->m_pSounder->m_SounderId);

		// OTK - FAE065 - il faut nettoyer les vecteurs de sondeurs associ�s
		// � la fanMemory en cas de suppression d'un ping (le dernier a r�f�rencer le sondeur....)
		m_sounderDefinition.CleanSounders(pSounder, (PingFan*)m_pingFanContainer->GetSounderIndexedContainer(pSounder->m_SounderId)->GetDatedObject(0));
	}
}

void PingFanContainer::RemoveAllFan(std::uint32_t sounderId)
{
	for (int index = 0; index < m_pingFanContainer->GetObjectCount(); )
	{
		PingFan *pFan = (PingFan *)m_pingFanContainer->GetObjectWithIndex(index);

		if (pFan->getSounderRef()->m_SounderId == sounderId)
		{
			WaitForPingFanConsumers(pFan->GetPingId(), false);

			Sounder *pSounder = pFan->m_pSounder;
			PingFanSingleTarget* pDeleteSingleTargets = GetPingFanSingleTarget(pFan->m_ObjectTime, pSounder->m_SounderId);
			m_singleTargetContainer->RemoveObject(pDeleteSingleTargets, pSounder->m_SounderId);
			m_pingFanContainer->RemoveObject(pFan, pFan->m_pSounder->m_SounderId);

			m_sounderDefinition.CleanSounders(pSounder, (PingFan*)m_pingFanContainer->GetSounderIndexedContainer(pSounder->m_SounderId)->GetDatedObject(0));
		}
		else
		{
			index++;
		}
	}
}

bool PingFanContainer::RemovePing(unsigned int pingId) {

	bool success = false;
	for (int index = 0; index < m_pingFanContainer->GetObjectCount(); index++)
	{
		PingFan *pFan = (PingFan *)m_pingFanContainer->GetObjectWithIndex(index);
		if (pingId == pFan->GetPingId()) {
			WaitForPingFanConsumers(pFan->GetPingId(), false);

			Sounder *pSounder = pFan->m_pSounder;
			PingFanSingleTarget* pDeleteSingleTargets = GetPingFanSingleTarget(pFan->m_ObjectTime, pSounder->m_SounderId);
			m_singleTargetContainer->RemoveObject(pDeleteSingleTargets, pSounder->m_SounderId);
			m_pingFanContainer->RemoveObject(pFan, pFan->m_pSounder->m_SounderId);

			success = true;

			break;
		}
	}
	return success;
}

void PingFanContainer::RemoveAllSingleTargets()
{
	m_singleTargetContainer->RemoveAllObjects();
}

void PingFanContainer::UpdateRefSounder(Sounder * pNewSounder)
{
	// sauvegarde des fans
	std::vector<PingFan*> pingFanSave;
	for (int index = 0; index < m_pingFanContainer->GetObjectCount(); index++)
	{
		PingFan *pFan = (PingFan *)m_pingFanContainer->GetObjectWithIndex(index);
		if (pFan->getSounderRef()->m_SounderId == pNewSounder->m_SounderId)
		{
			pingFanSave.push_back(pFan);
			pFan->ClearData();
			MovRef(pFan);
		}
	}

	// suppression de la relation fan/sondeur
	RemoveAllFan(pNewSounder->m_SounderId);

	// ajout des fans au nouveau sondeur
	for (std::vector<PingFan*>::iterator iter = pingFanSave.begin(); iter != pingFanSave.end(); iter++)
	{
		PingFan *pFan = *iter;
		pFan->SetSounderRef(pNewSounder);
		AddPingFan(pFan);

		MovUnRefDelete(pFan);
	}
}

void PingFanContainer::CheckReaderParameter()
{
	if (m_nbRecord != M3DKernel::GetInstance()->GetRefKernelParameter().getNbPingFanMax())
	{
		m_nbRecord = M3DKernel::GetInstance()->GetRefKernelParameter().getNbPingFanMax();
		for (unsigned int i = 0; i < this->m_sounderDefinition.GetNbSounder(); i++)
		{
            m_sounderDefinition.GetFanMemory(i)->SetMemoryLength(m_nbRecord);
            m_sounderDefinition.GetFanMemory(i)->CheckReaderParameter();
		}
		m_sounderDefinition.CheckAllSoundersReaderParameters();

		MapSounderIndexedContainer::iterator res = m_pingFanContainer->GetSounderIndexedMap().begin();
		while (res != m_pingFanContainer->GetSounderIndexedMap().end())
		{
			TimeObjectContainer *pTimed = res->second;			
			while (pTimed->GetObjectCount() > m_nbRecord)
			{

				PingFan *pFan = (PingFan *)pTimed->GetDatedObject(0);

				// OTK - 15/10/2009 - attente �ventuelle des traitements asynchrones sur ce ping avant sa suppression
				WaitForPingFanConsumers(pFan->GetPingId());

				Sounder *pSounder = pFan->m_pSounder;
				pFan->GetFanMemoryRef()->ShiftOne();
				// OTK - FAE003 - Fuite m�moire - lorsqu'on d�truit un pingFan, on d�truit �galement les single targets associ�es (sinon elles ne sont jamais lib�r�es)
				PingFanSingleTarget* pDeleteSingleTargets = GetPingFanSingleTarget(pFan->m_ObjectTime, pSounder->m_SounderId);
				m_singleTargetContainer->RemoveObject(pDeleteSingleTargets, pSounder->m_SounderId);

				m_pingFanContainer->RemoveObject(pFan, pFan->m_pSounder->m_SounderId);

				// OTK - FAE065 - il faut nettoyer les vecteurs de sondeurs associ�s
				// � la fanMemory en cas de suppression d'un ping (le dernier a r�f�rencer le sondeur....)
				m_sounderDefinition.CleanSounders(pSounder, (PingFan*)m_pingFanContainer->GetSounderIndexedContainer(pSounder->m_SounderId)->GetDatedObject(0));
			}
			res++;

		}
	}
	else
	{
		for (unsigned int i = 0; i < this->m_sounderDefinition.GetNbSounder(); i++)
		{
            m_sounderDefinition.GetFanMemory(i)->CheckReaderParameter();
		}
		m_sounderDefinition.CheckAllSoundersReaderParameters();
	}
}

// attente des pingFanConsumers avant suppression du ping d'ID pingID
void PingFanContainer::WaitForPingFanConsumers(std::uint64_t pingID, bool kernelLocked)
{
	CSynchronized sync(m_PingFanConsumersMonitor);

	// pendant l'attente, on lib�re le verou global du noyau pour �viter les interblocages.
	// on peut le faire car pendant l'attente, le thread de lecture ne fait rien si ce n'est attendre.
	if (kernelLocked)
	{
		M3DKernel::GetInstance()->Unlock();
	}

	size_t nbConsumers = m_PingFanConsumers.size();
	for (size_t i = 0; i < nbConsumers; i++)
	{
		m_PingFanConsumers[i]->WaitBeforeDeletingPing(pingID);
	}

	if (kernelLocked)
	{
		M3DKernel::GetInstance()->Lock();
	}
}

// ajoute un consommateur de pings
void PingFanContainer::AddPingFanConsumer(PingFanConsumer* consumer)
{
	CSynchronized sync(m_PingFanConsumersMonitor);

	m_PingFanConsumers.push_back(consumer);
}

// supprime un consommateur de pings
void PingFanContainer::RemovePingFanConsumer(PingFanConsumer* consumer)
{
	CSynchronized sync(m_PingFanConsumersMonitor);

	size_t nbConsumers = m_PingFanConsumers.size();
	bool erased = false;
	for (size_t i = 0; i < nbConsumers && !erased; i++)
	{
		if (m_PingFanConsumers[i] == consumer)
		{
			m_PingFanConsumers.erase(m_PingFanConsumers.begin() + i);
			erased = true;
		}
	}
}

