#include "M3DKernel/datascheme/PingFanIterator.h"
#include "M3DKernel/datascheme/PingFanContainer.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

using namespace BaseMathLib;

PingFanIterator::PingFanIterator()
{
	/** Use of ping Id instead of ping Index */
	m_bUsePingId = false;

	/** iterator range min */
	m_ulMinRange = 0;

	/** iterator range max */
	m_ulMaxRange = 0;

	/** iterator position */
	m_ulPos = 0;

	/** iterator increment (default : forward)*/
	m_inc = 1;
}

PingFanIterator::~PingFanIterator()
{
}

//*****************************************************************************
// Name : UpdateOverlapParametersFromShoal
// Description : Update compute ping window from shoal
// Parameters : std::int32_t iShoal
// Return : void
//*****************************************************************************
void PingFanIterator::Init(bool bUsePingId	/** Use of ping Id instead of ping Index */,
	std::int64_t ulMinRange /** iterator range min */,
	std::int64_t ulMaxRange /** iterator range max */)
{

	m_bUsePingId = bUsePingId;
	m_ulMinRange = ulMinRange;
	m_ulMaxRange = ulMaxRange;

	Begin();
}

void PingFanIterator::Begin()
{
	m_ulPos = m_ulMinRange;

	//forward direction;
	m_inc = 1;
}

bool PingFanIterator::End()
{
	return m_ulPos > m_ulMaxRange;
}

void PingFanIterator::RBegin()
{
	m_ulPos = m_ulMaxRange;

	//backward direction;
	m_inc = -1;
}

bool PingFanIterator::REnd()
{
	return m_ulPos < m_ulMinRange;
}

PingFanIterator& PingFanIterator::operator++()
{
	//increment iterator according to the iterator direction
	m_ulPos += m_inc;

	return *this;
}

PingFan* PingFanIterator::operator*()
{
	PingFan*  result = NULL;

	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();

	PingFanContainer &fanContainer = pKernel->getObjectMgr()->GetPingFanContainer();

	//use ping id
	if (m_bUsePingId)
	{
		result = fanContainer.GetPingFanWithId(m_ulPos);
	}
	//use ping index
	else
	{
		int nbPing = fanContainer.GetPingFanContainer()->GetObjectCount();
		if (m_ulPos >= 0 && nbPing > m_ulPos)
		{
			result = (PingFan*)fanContainer.GetPingFanContainer()->GetObjectWithIndex(m_ulPos);
		}
	}
	pKernel->Unlock();

	return result;
}
