#include "M3DKernel/datascheme/PingFanSingleTarget.h"

PingFanSingleTarget::PingFanSingleTarget(void)
{
}

PingFanSingleTarget::~PingFanSingleTarget(void)
{
	for (unsigned int i = 0; i < m_targetList.size(); i++)
	{
		MovUnRefDelete(m_targetList[i]);
	}
}

void PingFanSingleTarget::AddTarget(SingleTargetDataObject*p)
{
	MovRef(p);
	m_targetList.push_back(p);
}

unsigned int PingFanSingleTarget::GetNumberOfTarget()
{
	return m_targetList.size();
}

SingleTargetDataObject* PingFanSingleTarget::GetTarget(unsigned int targetNum)
{
	if (targetNum < GetNumberOfTarget())
		return m_targetList[targetNum];
	return NULL;
}
