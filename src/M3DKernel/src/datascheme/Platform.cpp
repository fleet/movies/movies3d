#include "M3DKernel/datascheme/Platform.h"

Platform::~Platform()
{
}

Platform::Platform()
{
	// OTK - FAE047 - si un sondeur n'a pas de tuple platform,
	// on en utilise un par d�faut qui ne sera pas utilis�.
	// On doit initialiser m_attitudSensorId en particulier 
	// sinon on s'expose au risque d'une fuite m�moire
	// en cr�ant des conteneurs vides d'objets NavAttitudes pour diff�rentes
	// valeurs al�atoires de m_attitudeSensorId.
	m_attitudeSensorId = -1;
	m_timeCpu = 0;
	m_timeFrac = 0;
	m_platformType = 0;
}

void Platform::Copy(const Platform &other)
{
	m_attitudeSensorId = other.m_attitudeSensorId;
	m_timeCpu = other.m_timeCpu;
	m_timeFrac = other.m_timeFrac;
	m_platformType = other.m_platformType;
	m_Offset = other.m_Offset;
}

bool Platform::IsEqual(const Platform &B) const
{
	return m_attitudeSensorId == B.m_attitudeSensorId
		&& B.m_Offset == m_Offset
		&& m_platformType == B.m_platformType;
}

