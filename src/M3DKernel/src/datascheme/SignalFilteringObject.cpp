#include "M3DKernel/datascheme/SignalFilteringObject.h"

SignalFilteringObject::SignalFilteringObject()
	: m_FilterID(0)
	, m_DecimationFactor(0)
{
}

SignalFilteringObject::~SignalFilteringObject()
{
}

void SignalFilteringObject::Copy(const SignalFilteringObject & other)
{
	m_DecimationFactor = other.m_DecimationFactor;
	m_FilterID = other.m_FilterID;
	m_Coefficients = other.m_Coefficients;
}

bool SignalFilteringObject::IsEqual(const SignalFilteringObject &B) const
{
	return B.m_DecimationFactor == m_DecimationFactor
		//&& B.m_FilterID==m_FilterID peut importe si l'ID du filtre est diff�rent
		&& B.m_Coefficients == m_Coefficients;
}