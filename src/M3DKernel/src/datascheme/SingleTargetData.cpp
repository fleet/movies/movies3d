#include "M3DKernel/datascheme/SingleTargetData.h"

BaseMathLib::Vector3D SingleTargetData::GetTargetPositionSoftChannel() const
{
	BaseMathLib::Vector3D vec(0.0, 0.0, m_targetRange);

	vec.z = vec.z*(cos(m_AlongShipAngleRad)*cos(m_AthwartShipAngleRad));
	vec.x = vec.z*tan(m_AlongShipAngleRad);
	vec.y = vec.z*tan(m_AthwartShipAngleRad);

	return vec;
}