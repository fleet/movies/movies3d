#include "M3DKernel/datascheme/SingleTargetDataObject.h"

#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/NavAttributes.h"
#include "M3DKernel/BackWardConst.h"

SingleTargetDataObject::SingleTargetDataObject(void)
{
	m_pSounder = NULL;
}

SingleTargetDataObject::~SingleTargetDataObject(void)
{
	MovUnRefDelete(m_pSounder);
}

void SingleTargetDataObject::SetSounderRef(Sounder *pSounder)
{
	MovUnRefDelete(m_pSounder);
	MovRef(pSounder);
	m_pSounder = pSounder;
}

unsigned int SingleTargetDataObject::GetSingleTargetDataCount() const
{
	return m_targetListFM.size() + m_targetListCW.size();
}

unsigned int SingleTargetDataObject::GetSingleTargetDataCWCount() const 
{ 
	return m_targetListCW.size(); 
}

unsigned int SingleTargetDataObject::GetSingleTargetDataFMCount() const 
{ 
	return m_targetListFM.size(); 
}

SingleTargetData& SingleTargetDataObject::GetSingleTargetData(unsigned int i)
{
	if (i < m_targetListCW.size())
		return GetSingleTargetDataCW(i);
	else
		return GetSingleTargetDataFM(i - m_targetListCW.size());
}

SingleTargetDataCW& SingleTargetDataObject::GetSingleTargetDataCW(unsigned int i)
{
#ifdef BEFORE_IBTS_08
	if (pFan->getSounderRef()->m_isMultiBeam)
	{
		double heave = pFan->GetNavAttitudeRef()->m_heaveMeter;
		double compensatedHeave = pFan->getHeaveChan(this->m_parentSTId);
		Transducer *pTrans = pFan->getSounderRef()->GetTransducer(0);
		SoftChannel *pChan = pFan->getSounderRef()->getSoftChannel(m_parentSTId);
		double RangeSimrad = m_targetListCW[i].m_targetRange + (pTrans->GetPlatform()->GetDephtOffset() + compensatedHeave);
		double cosPhi = cos(pChan->m_mainBeamAthwartSteeringAngleRad);
		double Range = RangeSimrad - (pTrans->GetPlatform()->GetDephtOffset() + compensatedHeave) / cosPhi;
		double oldRange = m_targetListCW[i].m_targetRange;
		m_targetListCW[i].m_targetRange = Range;

	}
	else
	{
		//nothing to do
	}
#endif

	return m_targetListCW[i];
}

SingleTargetDataFM & SingleTargetDataObject::GetSingleTargetDataFM(unsigned int i)
{
	return m_targetListFM[i];
}

BaseMathLib::Vector3D SingleTargetDataObject::GetTargetPositionSoftChannelCoord(unsigned int numTarget) const
{
	if (numTarget < m_targetListCW.size())
		return m_targetListCW[numTarget].GetTargetPositionSoftChannel();
	else
		return m_targetListFM[numTarget - m_targetListCW.size()].GetTargetPositionSoftChannel();
}

void SingleTargetDataObject::PushSingleTargetDataCW(SingleTargetDataCW &ref) 
{
	m_targetListCW.push_back(ref); 
}

void SingleTargetDataObject::PushSingleTargetDataFM(SingleTargetDataFM &ref)
{ 
	m_targetListFM.push_back(ref);
}
