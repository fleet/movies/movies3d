#include "M3DKernel/datascheme/SoftChannel.h"
#include "BaseMathLib/RotationMatrix.h"
#include "M3DKernel/datascheme/TransmitSignalObject.h"
#include "M3DKernel/utils/M3DStdUtils.h"

using namespace BaseMathLib;

SoftChannelData::SoftChannelData() 
	: HacObject()
	, m_pRefTupleDef(NULL)
{
	strcpy_s(m_channelName, 48, "Unavalaible");
	strcpy_s(m_transNameShort, 30, "Unavalaible");

	m_startFrequency = 0;
	m_endFrequency = 0;

	m_bDefaultAlongAngleOffset = false;
	m_bDefaultAthwartAngleOffset = false;
}

void SoftChannelData::SetTupleUserData(HacTupleDef *pRefTupleDef)
{
	if (m_pRefTupleDef != NULL)
	{
		delete m_pRefTupleDef;
	}
	m_pRefTupleDef = pRefTupleDef;
}

HacTupleDef* SoftChannelData::GetTupleUserData() { return m_pRefTupleDef; }

SoftChannelData::~SoftChannelData()
{
	SetTupleUserData(NULL);
}


SoftChannelComputeData::SoftChannelComputeData() 
{
	m_PolarId = 0; 
	m_isReferenceBeam = false; 
	m_groupId = 0; 
	m_pTransmitSignalObject = NULL; 
}

SoftChannelComputeData::~SoftChannelComputeData()
{
	MovUnRefDelete(m_pTransmitSignalObject);
}

void SoftChannelComputeData::InitTransmitSignalObject(Transducer * transducer, SoftChannel * softChannel, const std::vector<float>& signalReal, const std::vector<float>& signalImag) {
	m_pTransmitSignalObject = TransmitSignalObject::Create();
	m_pTransmitSignalObject->InitSignals(signalReal, signalImag);
	m_pTransmitSignalObject->Compute(transducer, softChannel, false /*do not recompute signals*/);
}

TransmitSignalObject * SoftChannelComputeData::GetTransmitSignalObject(Transducer * pTrans, SoftChannel * pSoftChan)
{
	//LB 08/07/2021 cas différent de EK80 on n'a pas de transmit signal
	if (pSoftChan->m_startFrequency == 0)
		return NULL;

	if (!m_pTransmitSignalObject)
	{
		InitTransmitSignalObject(pTrans, pSoftChan);
	}
	return m_pTransmitSignalObject;
}

SoftChannel::~SoftChannel()
{
	MovUnRefDelete(m_pThreshold);
	MovUnRefDelete(m_pSplit);
	MapBeamWeight::iterator iter = m_BeamWeights.begin();
	while (iter != m_BeamWeights.end())
	{
		MovUnRefDelete(iter->second);
		iter++;
	}
}

SoftChannel::SoftChannel() 
	: m_pThreshold(NULL)
	, m_pSplit(NULL)
	, m_virtualSoftChannelId(-1)
{
}

void SoftChannel::Compute(Traverser &e)
{
	m_bIsComplete = false;
	if (m_acousticFrequency != 0)
	{
		m_bIsComplete = true;
	}
}

void SoftChannel::OnComplete(Traverser &e)
{
	ComputeMatrix();
}

void SoftChannel::ComputeMatrix()
{
	m_matrixSoftChan.CreateRotationMatrix(0, m_mainBeamAlongSteeringAngleRad, -m_mainBeamAthwartSteeringAngleRad);
}

void SoftChannel::SetThreshold(Threshold*p)
{
	MovUnRefDelete(m_pThreshold);
	m_pThreshold = p;
	MovRef(m_pThreshold);
}

void SoftChannel::SetSplitBeamParameter(SplitBeamParameter*p)
{
	MovUnRefDelete(m_pSplit);
	m_pSplit = p;
	MovRef(m_pSplit);
}
MapBeamWeight&	SoftChannel::GetBeamWeights()
{
	return m_BeamWeights;
}
void SoftChannel::SetBeamWeight(unsigned short wID, BeamWeight*p)
{
	MovUnRefDelete(m_BeamWeights[wID]);
	m_BeamWeights[wID] = p;
	MovRef(m_BeamWeights[wID]);
}

SoftChannelMulti::SoftChannelMulti()
{
}

bool SoftChannel::IsEqual(const SoftChannel &B) const
{
	bool ret = true;
	m_softChannelId == B.m_softChannelId &&
		m_dataType == B.m_dataType &&
		m_beamType == B.m_beamType &&
		m_acousticFrequency == B.m_acousticFrequency &&
		m_startFrequency == B.m_startFrequency &&
		m_endFrequency == B.m_endFrequency &&
		// OTK - FAE064 - ne pas �mettre de SounderChanged si le startSample change
		//m_startSample==B.m_startSample &&
		m_mainBeamAlongSteeringAngleRad == B.m_mainBeamAlongSteeringAngleRad &&
		m_mainBeamAthwartSteeringAngleRad == B.m_mainBeamAthwartSteeringAngleRad &&
		m_absorptionCoef == B.m_absorptionCoef &&
		m_transmissionPower == B.m_transmissionPower &&
		m_beamAlongAngleSensitivity == B.m_beamAlongAngleSensitivity &&
		m_beamAthwartAngleSensitivity == B.m_beamAthwartAngleSensitivity &&
		m_beam3dBWidthAlongRad == B.m_beam3dBWidthAlongRad &&
		m_beam3dBWidthAthwartRad == B.m_beam3dBWidthAthwartRad &&
		m_beamEquTwoWayAngle == B.m_beamEquTwoWayAngle &&
		m_beamGain == B.m_beamGain &&
		m_beamSACorrection == B.m_beamSACorrection &&
		// OTK - FAE064 - ne pas �mettre de SounderChanged sur modification de le fen�tre de fond
		//m_bottomDetectionMinDepth==B.m_bottomDetectionMinDepth &&
		//m_bottomDetectionMaxDepth==B.m_bottomDetectionMaxDepth &&
		m_bottomDetectionMinLevel == B.m_bottomDetectionMinLevel &&
		m_AlongTXRXWeightId == B.m_AlongTXRXWeightId &&
		m_AthwartTXRXWeightId == B.m_AthwartTXRXWeightId &&
		m_SplitBeamAlongTXRXWeightId == B.m_SplitBeamAlongTXRXWeightId &&
		m_SplitBeamAthwartTXRXWeightId == B.m_SplitBeamAthwartTXRXWeightId &&
		m_bandWidth == B.m_bandWidth;
	if (m_pThreshold != NULL && B.m_pThreshold != NULL)
		ret = ret && m_pThreshold->IsEqual(*(B.m_pThreshold));
	else
		ret = ret && m_pThreshold == B.m_pThreshold;

	ret = ret && (m_BeamWeights.size() == B.m_BeamWeights.size());
	MapBeamWeight::const_iterator iter = m_BeamWeights.begin();
	while (ret && iter != m_BeamWeights.end())
	{
		MapBeamWeight::const_iterator iter2 = B.m_BeamWeights.find(iter->first);
		ret = ret && iter2 != B.m_BeamWeights.end() && iter->second->IsEqual(*(iter2->second));
		iter++;
	}
	return ret;
}

void SoftChannel::UpdateChildMember(Traverser &e) 
{
	if (m_pThreshold)
		m_pThreshold->Update(e);

	if (m_pSplit)
		m_pSplit->Update(e);

	MapBeamWeight::iterator iter = m_BeamWeights.begin();
	while (iter != m_BeamWeights.end())
	{
		iter->second->Update(e);
		iter++;
	}
}
