#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/Transducer.h"

#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/NavAttributes.h"
#include "BaseMathLib/RotationMatrix.h"
#include "M3DKernel/datascheme/TransformMap.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/PingFanSingleTarget.h"
#include "M3DKernel/datascheme/TransformSet.h"
#include "M3DKernel/datascheme/Environnement.h"

// ISPIS - OTK - ajout de la gestion des ESU
#include "M3DKernel/datascheme/MovESUMgr.h"
#include "M3DKernel/M3DKernel.h"

#include "M3DKernel/utils/carto/CartoTools.h"

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "M3DKernel.datascheme.Sounder";
}

void SounderData::SetRemarks(const char * remarks)
{
	// NMD - FAE 110 - correction du plantage en 32bits
	// on ne copie que les 40 premiers caracteres de remarks car le champs
	strncpy(this->m_remarks, remarks, 40);

	// extraction du num�ro de version HERMES
	m_computedHermesVersion = -1;
	std::string stdRemarks = m_remarks;
	size_t foundHERMES = stdRemarks.find("HERMES");
	if (foundHERMES != std::string::npos)
	{
		stdRemarks = stdRemarks.substr(foundHERMES + 6);
		stdRemarks.erase(std::remove(stdRemarks.begin(), stdRemarks.end(), '.'), stdRemarks.end());

		// cr�er un flux � partir de la cha�ne � convertir
		std::istringstream iss(stdRemarks);
		iss >> m_computedHermesVersion;
	}

	//le heave est il en cm pour gestion immersion sondeur
	m_heaveCM = false;
	size_t foundHeave = stdRemarks.find("HeaveCM");
	if (foundHeave != std::string::npos)
	{
		m_heaveCM = true;
	}
}

int SounderData::GetHermesVersionFromRemarks()
{
	return m_computedHermesVersion;
}

bool SounderData::IsHeaveCM()
{
	return m_heaveCM;
}
const char * SounderData::GetRemarks()
{
	return m_remarks;
}

Sounder::~Sounder()
{
	for (unsigned int i = 0; i < m_transducerList.size(); i++)
	{
		MovUnRefDelete(m_transducerList[i]);
	}

	if (m_pPendingPingFan)
		MovUnRefDelete(m_pPendingPingFan);

	if (m_pIncompletePingFan)
		MovUnRefDelete(m_pIncompletePingFan);

	if (m_pWorkingPingFan)
		MovUnRefDelete(m_pWorkingPingFan);

	if (m_pTransformSet)
		MovUnRefDelete(m_pTransformSet);
}

Sounder::Sounder()
	: m_pIncompletePingFan(nullptr)
	, m_pPendingPingFan(nullptr)
	, m_pWorkingPingFan(nullptr)
	, m_pTransformSet(nullptr)
{
}

Sounder * Sounder::Clone() const
{
	auto s = Sounder::Create();
	s->Copy(*this, true);
	return s;
}

// OTK - FAE067 - recopie des sondeurs/transducteurs/channels
void Sounder::Copy(const Sounder &other, bool copyTransducers)
{
	// h�ritage
	SounderData::Copy(other);

	// copie des transducteurs si voulu (on ne le fait pas dans le cas mono faisceau
	// car on veut changer la d�finition du transducteur de toutes fa�ons
	if (copyTransducers)
	{
		size_t nbTransducer = other.m_transducerList.size();
		for (size_t i = 0; i < nbTransducer; i++)
		{
			Transducer* pTrans = Transducer::Create();
			*pTrans = *other.m_transducerList[i];
			m_transducerList.push_back(pTrans);
			MovRef(pTrans);
		}
	}
}

unsigned int Sounder::GetTransducerCount() const
{
	return (unsigned int)m_transducerList.size();
}

SoftChannel* Sounder::GetSoftChannelByIndex(const size_t& index) const {
	auto count = 0;

	for (const auto& transducer : m_transducerList) {
		for (const auto& channelId : transducer->GetChannelId()) {
			if (count == index) {
				return transducer->getSoftChannel(channelId);
			}
			++count;
		}
	}

	return nullptr;
}

size_t Sounder::GetSoftChannelCount() const {
	size_t nbSoft = 0;
	for (const auto& transducer : m_transducerList) {
		nbSoft += transducer->m_numberOfSoftChannel;
	}

	return nbSoft;
}

const std::vector<Transducer*>& Sounder::GetAllTransducers() const {
	return m_transducerList;
}

Transducer *Sounder::GetTransducer(unsigned int idx) const
{
	if (idx < GetTransducerCount())
		return m_transducerList[idx];
	else return NULL;
}

Transducer *Sounder::GetTransducerWithName(const char * transName) const
{
	Transducer * pTrans = NULL;
	for (unsigned int i = 0; i < m_transducerList.size(); i++)
	{
		if (strcmp(m_transducerList[i]->m_transName, transName) == 0)
		{
			pTrans = m_transducerList[i];
			break;
		}
	}
	return pTrans;
}

void Sounder::addTransducer(Transducer *p)
{
	if (p != NULL)
	{
		m_transducerList.push_back(p);
		MovRef(p);
	}
}

void Sounder::ComputeMatrix()
{
	for (unsigned int i = 0; i < m_transducerList.size(); i++)
	{
		m_transducerList[i]->ComputeMatrix();
	}
}

void Sounder::AddBeamDataObject(BeamDataObject * beamData
	, PhaseDataObject * phaseData
	, SpectralAnalysisDataObject * spectralAnalysisDataObject
	, QuadrantDataObject * quadrantDataObject)
{
	bool processIncompletePingFan = (M3DKernel::GetInstance()->GetRefKernelParameter().getIgnoreIncompletePings() == false);
	if (processIncompletePingFan)
	{
		auto workingPingFan = GetWorkPingFan();
		const auto & beams = workingPingFan->getBeams();

		auto it = beams.find(beamData->m_hacChannelId);
		if (it != beams.end())
		{
			// complete pingFan
			m_pIncompletePingFan = workingPingFan;
			m_pWorkingPingFan = NULL;
		}
	}

	auto workingPingFan = GetWorkPingFan();
	workingPingFan->AddBeamDataObject(beamData);
	if (phaseData)
	{
		workingPingFan->AddPhaseDataObject(phaseData);
	}

	if (spectralAnalysisDataObject)
	{
		workingPingFan->AddSpectralAnalysisDataObject(beamData->m_hacChannelId, spectralAnalysisDataObject);
	}

	if (quadrantDataObject)
	{
		workingPingFan->AddQuadrantDataObject(beamData->m_hacChannelId, quadrantDataObject);
	}
}

void Sounder::AddPhaseDataObject(PhaseDataObject * phaseData)
{
	// OTK - FAE214 - on regarde si la phase ne serait pas plutôt associée au ping en attente avant de 
	// l'associer au ping en cours de construction (peut arriver si les tuples phase sont �crits apr�s les tuples amplitude)

	PingFan * pFan = nullptr;
	if (m_pPendingPingFan
		&& (m_pPendingPingFan->GetFilePingId() == phaseData->m_filePingNumber))
	{
		pFan = m_pPendingPingFan;
	}
	else
	{
		pFan = GetWorkPingFan();
	}
	pFan->AddPhaseDataObject(phaseData);
}

PingFan	*Sounder::GetWorkPingFan()
{
	if (m_pWorkingPingFan == nullptr)
	{
		/// si on ne l'a pas trouvée on en crée une
		PingFan *pingFan = PingFan::Create();
		MovRef(pingFan);
		pingFan->SetSounderRef(this);
		m_pWorkingPingFan = pingFan;
	}
	return m_pWorkingPingFan;
}

void Sounder::FinalizePingFanProcessing(Traverser & e, PingFan * pingFan)
{
	// Envoi du pingfan en attente le cas �ch�ant
	assert(pingFan->IsComplete());

	if (!CCartoTools::IsKnownOrigin())
	{
		NavPosition* pPos = pingFan->GetNavPositionRef(pingFan->GetFirstChannelId());
		assert(pPos);
		// on ne prend pas en compte les coordonn�es par defaut (0;0), sinon le shift ne sert � rien
		if (pPos->m_source != eDefaultSource)
		{
			CCartoTools::SetOrigin(pPos->m_lattitudeDeg, pPos->m_longitudeDeg);
		}
	}

	//
	auto kernelParameters = M3DKernel::GetInstance()->GetRefKernelParameter();
	if (kernelParameters.isUsingCustomEnvironmentValues())
	{
		const MapBeamDataObject & beams = pingFan->getBeams();
		for (MapBeamDataObject::const_iterator itBeam = beams.cbegin(); itBeam != beams.cend(); ++itBeam)
		{
			int channelId = itBeam->first;
			BeamDataObject * beam = itBeam->second;
			SoftChannel * softChannel = getSoftChannel(channelId);
			Transducer * trans = getTransducerForChannel(channelId);
			if (trans && softChannel && beam)
			{
				// db / km
				const double alpha_new = softChannel->m_absorptionCoef* 1e-7;
				const double alpha_old = softChannel->m_absorptionCoefHAC* 1e-7;
				
				const double beamsSamplesSpacing_new = trans->getBeamsSamplesSpacing();
				const double beamsSamplesSpacing_old = trans->computeSampleSpacing(m_soundVelocity.originalValue);

				const int nbValues = beam->size();
				for (int idx = 0; idx < nbValues; ++idx)
				{
					short svValue = beam->getAt(idx);
					if (svValue > UNKNOWN_DB && svValue <= std::numeric_limits<short>::max())  // sv valide
					{
						double sv = svValue;

						const double R_old = beamsSamplesSpacing_old * (idx + 0.5);
						const double R_new = beamsSamplesSpacing_new* (idx + 0.5);
						sv += ((20 * log10(R_new) + 2.0 * (alpha_new*R_new)) - (20 * log10(R_old) + 2.0 * (alpha_old*R_old))) * 100;

						if (sv > UNKNOWN_DB && sv <= std::numeric_limits<short>::max())  // sv corrigé valide
						{
							svValue = sv;
						}
						else
						{
							svValue = UNKNOWN_DB;
						}

						beam->setAt(idx, svValue);
					}
				}
				
				// Adjust bottom
				if (beam->m_bottomWasFound)
				{
					beam->m_bottomRange *= beamsSamplesSpacing_new / beamsSamplesSpacing_old;
				}
			}
		}
	}

	// Detection des transducteurs partiellement passifs 
	bool isPassive = false;
	const MapBeamDataObject & beams = pingFan->getBeams();
	for (auto itBeam = beams.cbegin(); itBeam != beams.cend(); ++itBeam)
	{
		if (itBeam->second->m_transMode == BeamDataObject::PassiveTransducerMode)
		{
			isPassive = true;
			break;
		}
	}

	if (isPassive)
	{
		for (auto itBeam = beams.cbegin(); itBeam != beams.cend(); ++itBeam)
		{
			itBeam->second->m_transMode = BeamDataObject::PassiveTransducerMode;
		}
	}

	e.m_pHacObjectMgr->m_pingFanContainer.AddPingFan(pingFan);
	// IPSIS - OTK - gestion des ESU
	// on ne repositionne l'ESU manager que sur 
	// completion d'un PingFan, pour avoir une date, une distance et un nombre
	// de pings parcouru correct
	M3DKernel::GetInstance()->getMovESUManager()->ManageESU(&e.m_pHacObjectMgr->m_pingFanContainer, e);

	e.PingFanAdded(pingFan);
}

void Sounder::ComputePostRead(Traverser &e, bool bLastClose)
{
	// OTK - FAE214 - si un ping fan est en attente, on en d�clenche le traitement si:
	// - soit un ping fan a commencé à être lu (m_pWorkingPingFan existe)
	// - soit la fin du run a été atteinte (sinon on rate le dernier ping du run)
	if (m_pPendingPingFan && (m_pWorkingPingFan != nullptr || bLastClose))
	{
		// Envoi du pingfan en attente le cas échéant
		FinalizePingFanProcessing(e, m_pPendingPingFan);
		MovUnRefDelete(m_pPendingPingFan);
		m_pPendingPingFan = nullptr;
	}

	// Traitement des pings incomplets
	if (m_pIncompletePingFan)
	{
		bool processPhase = (M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePhase() == false);

		// récupération d'une date valide
		unsigned short  timeFraction = 0;	
		std::uint32_t	timeCPU = 0;
		const auto & beams = m_pIncompletePingFan->getBeams();
		if (!beams.empty())
		{
			auto firstBeam = beams.cbegin();
			timeFraction = firstBeam->second->m_timeFraction;
			timeCPU = firstBeam->second->m_timeCPU;
		}

		// Construction de la liste des channels du 
		for (unsigned int i = 0; i < GetTransducerCount(); i++)
		{
			auto transducer = GetTransducer(i);
			auto transducerChannels = transducer->GetChannelId();

			/// Vérification pour chaque channel si un objet "BeamData" est présent, et création de BeamData vide si manquant
			for (auto channel : transducerChannels)
			{
				const auto & beams = m_pIncompletePingFan->getBeams();
				auto it = beams.find(channel);
				if (it == beams.end())
				{
					MovObjectPtr<BeamDataObject> beamDataObject = M3DKernel::GetInstance()->getObjectMgr()->GetBeamDataObject();
					M3DKernel::GetInstance()->getObjectMgr()->PopBeamDataObject();
					beamDataObject->m_isValid = false;
					beamDataObject->m_hacChannelId = channel;
					beamDataObject->m_timeFraction = timeFraction;
					beamDataObject->m_timeCPU = timeCPU;

					m_pIncompletePingFan->AddBeamDataObject(beamDataObject.get());
				}
				
				if (processPhase)
				{
					const auto & phases = m_pIncompletePingFan->getPhases();
					auto it = phases.find(channel);
					if (it == phases.end())
					{
						MovObjectPtr<PhaseDataObject> phaseDataObject = M3DKernel::GetInstance()->getObjectMgr()->GetPhaseDataObject();
						M3DKernel::GetInstance()->getObjectMgr()->PopPhaseDataObject();
						phaseDataObject->m_hacChannelId = channel;
						phaseDataObject->m_timeFraction = timeFraction;
						phaseDataObject->m_timeCPU = timeCPU;


						m_pIncompletePingFan->AddPhaseDataObject(phaseDataObject.get());
					}
				}
			}
		}

		m_pIncompletePingFan->Update(e);
		if(m_pIncompletePingFan->IsComplete())
		{
			FinalizePingFanProcessing(e, m_pIncompletePingFan);
		}
		else
		{
			M3D_LOG_INFO(LoggerName, "Dropping incomplete ping fan....");
		}
		MovUnRefDelete(m_pIncompletePingFan);
		m_pIncompletePingFan = nullptr;
	}

	// Traitement du pingfan en cours de construction
	if (m_pWorkingPingFan != NULL)
	{
		m_pWorkingPingFan->Update(e);
		if (m_pWorkingPingFan->IsComplete())
		{
			// On bascule le ping en cours de construction vers le ping en attente d'envoi
			m_pPendingPingFan = m_pWorkingPingFan;
			m_pWorkingPingFan = NULL;
		}
	}
}

void Sounder::Compute(Traverser &e)
{
	m_bIsComplete = false;
	if (m_numberOfTransducer == m_transducerList.size())
	{
		m_bIsComplete = true;
		for (unsigned int i = 0; i < m_transducerList.size(); i++)
		{
			if (m_transducerList[i] != NULL)
			{
				if (m_transducerList[i]->NeedCompute() || !m_transducerList[i]->IsComplete())
					m_bIsComplete = false;
			}
		}
		OnComplete(e);
	}
}

void Sounder::OnComplete(Traverser &e)
{
	m_sounderComputeData.m_nbBeamPerFan = 0;

	for (auto transducer : m_transducerList)
	{
		if (transducer != nullptr)
		{
			double beamSampleSpacing = transducer->computeSampleSpacing(m_soundVelocity);
			transducer->setBeamSamplesSpacing(beamSampleSpacing);
			m_sounderComputeData.m_nbBeamPerFan += transducer->m_numberOfSoftChannel;
		}
	}

	// OTK - FAE065 - on associe les transformtables au sondeur
	if (m_pTransformSet)
	{
		// si on en avait deja mis en place dans un onComplete, on supprime les anciennes
		MovUnRefDelete(m_pTransformSet);
	}
	m_pTransformSet = TransformSet::Create();
	MovRef(m_pTransformSet);
	m_pTransformSet->SetSounder(this);
}

void Sounder::UpdateChildMember(Traverser &e)
{
	bool forceChange = false;
	for (unsigned int i = 0; i < m_transducerList.size(); i++)
	{
		if (m_transducerList[i] != NULL)
		{
			forceChange |= m_transducerList[i]->NeedCompute();
			m_transducerList[i]->Update(e);
		}
	}
	if (forceChange)
	{
		DataChanged();
	}
	if (m_pWorkingPingFan)
		m_pWorkingPingFan->Update(e);
}

SoftChannel* Sounder::getSoftChannel(unsigned short channelId)
{
	for (unsigned int i = 0; i < GetTransducerCount(); i++)
	{
		SoftChannel *p = GetTransducer(i)->getSoftChannel(channelId);
		if (p)
			return p;
	}

	return NULL;
}

SoftChannel* Sounder::getSoftChannelPolarX(unsigned int polarId)
{
	if (GetTransducerCount() > polarId)
	{
		return GetTransducer(polarId)->getSoftChannelPolarX(0);
	}
	else
	{
		return NULL;
	}
}

unsigned short Sounder::getTransducerIndexForChannel(unsigned short channelId) const {
	for (auto i = 0; i < GetTransducerCount(); ++i) {
		if (GetTransducer(i)->getSoftChannel(channelId)) {
			return i;
		}
	}
	return -1;
}

Transducer*	 Sounder::getTransducerForChannel(unsigned short channelId) const
{
	const auto transducerIndex = getTransducerIndexForChannel(channelId);
	if (transducerIndex != -1) {
		return GetTransducer(transducerIndex);
	}

	return NULL;
}

bool Sounder::IsSequencing()
{
	return false;
}

bool SounderEK80::IsSequencing()
{
	return m_pingMode == 2 || m_pingMode == 4;
}
#if DEPRECATED
BaseMathLib::Vector3D Sounder::GetBaseTransducerToWorldCoord(PingFan *pFan, unsigned int numTransducer)
{
	Vector3D ret;

	Transducer *transducer = GetTransducer(numTransducer);
	SoftChannel *pSoft = NULL;


	if (pFan && transducer)
	{

		ret = pFan->GetMatrixNav()*transducer->GetTransducerTranslation() + pFan->GetTranslationNav();
		return ret;
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "One data (PingFan, transducer  ) not found check your polarCoord");

	}
	return ret;
}
#endif

BaseMathLib::Vector3D Sounder::GetSoftChannelCoordinateToWorldCoord(Transducer *transducer, SoftChannel *pSoft, PingFan *pFan, const BaseMathLib::Vector3D &refCoord)
{
	//LB 10/07/2008 le rep�re monde est translat� sur le niveau moyen de la mer
	Vector3D ret(0, 0, transducer->m_transDepthMeter - transducer->GetTransducerTranslation().z);
	if (pFan && transducer && pSoft)
	{
#pragma message(" ________________________________________________ ")
#pragma message("!                                                !")
#pragma message("! Optimisation possible ??????                   !")
#pragma message("! transRotation()*pSoft->GetMatrixSoftChan       !")
#pragma message("!________________________________________________!")

		Vector3D delay(0, 0, transducer->GetInternalDelayTranslation());

		ret = ret + pFan->GetMatrixNav(pSoft->getSoftwareChannelId())*(
			transducer->GetTransducerRotation()* pSoft->GetMatrixSoftChan()* (refCoord + delay)
			+ transducer->GetTransducerTranslation()) + pFan->GetTranslationNav(pSoft->getSoftwareChannelId());
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "One data (PingFan, transducer or SoftChannel) not found check your polarCoord");

	}
	return ret;
}

BaseMathLib::Vector3D Sounder::GetSoftChannelCoordinateToGeoCoord(Transducer *transducer, SoftChannel *pSoft, PingFan *pFan, const BaseMathLib::Vector3D &refCoord)
{
	Vector3D relativeVector(0, 0, transducer->m_transDepthMeter - transducer->GetTransducerTranslation().z + pFan->GetTranslationNav(pSoft->getSoftwareChannelId()).z);
	Vector3D shipGeoPosition(pFan->GetNavPositionRef(pSoft->getSoftwareChannelId())->m_lattitudeDeg, pFan->GetNavPositionRef(pSoft->getSoftwareChannelId())->m_longitudeDeg, 0);
	Vector3D ret(0, 0, 0);

	if (pFan && transducer && pSoft)
	{
		relativeVector = relativeVector 
			+ pFan->GetMatrixNav(pSoft->getSoftwareChannelId())*(transducer->GetTransducerRotation()* pSoft->GetMatrixSoftChan()* refCoord 
				+ transducer->GetTransducerTranslation());
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "One data (PingFan, transducer or SoftChannel) not found check your polarCoord");
	}

	// depth
	ret.z = relativeVector.z;

	// lat std::int32_t
	CCartoTools::GetGeoCoords(shipGeoPosition.x, shipGeoPosition.y,
		relativeVector.x, relativeVector.y, ret.x, ret.y);

	return ret;
}

void Sounder::GetSoftChannelCoordinateToGeoCoord(Transducer* transducer, SoftChannel* pSoft, PingFan* pFan, const std::vector<BaseMathLib::Vector3D> & refCoord, std::vector<BaseMathLib::Vector3D>& res)
{
	if (pFan && transducer && pSoft)
	{
		const size_t n = refCoord.size();
		std::vector<BaseMathLib::Vector3D> relativeVector(n);

		BaseMathLib::Vector3D baseRelativeVector(0, 0, transducer->m_transDepthMeter - transducer->GetTransducerTranslation().z + pFan->GetTranslationNav(pSoft->getSoftwareChannelId()).z);
		
		const auto matrixNav = pFan->GetMatrixNav(pSoft->getSoftwareChannelId());
		const auto transducerTranslation = transducer->GetTransducerTranslation();
		const auto refCoordFact = transducer->GetTransducerRotation() * pSoft->GetMatrixSoftChan();
		for (size_t i = 0; i < n; ++i)
		{
			relativeVector[i] = baseRelativeVector 
				+ matrixNav * (refCoordFact * refCoord[i] + transducerTranslation);
		}

		// depth
		for (size_t i = 0; i < n; ++i)
		{
			res[i].z = relativeVector[i].z;
		}

		BaseMathLib::Vector3D shipGeoPosition(pFan->GetNavPositionRef(pSoft->getSoftwareChannelId())->m_lattitudeDeg, pFan->GetNavPositionRef(pSoft->getSoftwareChannelId())->m_longitudeDeg, 0);
		CCartoTools::GetGeoCoords(shipGeoPosition, relativeVector, res);
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "One data (PingFan, transducer or SoftChannel) not found check your polarCoord");
	}
}

BaseMathLib::Vector3D Sounder::GetSoftChannelCoordinateToCartesianCoord(Transducer *transducer, SoftChannel *pSoft, PingFan *pFan, const BaseMathLib::Vector3D &refCoord)
{
	Vector3D geocoords = GetSoftChannelCoordinateToGeoCoord(transducer, pSoft, pFan, refCoord);

	CCartoTools::GetCartesianCoords(geocoords.x, geocoords.y, geocoords.x, geocoords.y);

	return geocoords;
}

BaseMathLib::Vector3D Sounder::GetSoftChannelCoordinateToWorldCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, const BaseMathLib::Vector3D &refCoord)
{
	Transducer *transducer = GetTransducer(numTransducer);
	SoftChannel *pSoft = NULL;
	if (transducer)
		pSoft = transducer->getSoftChannelPolarX(softChannel);


	return	GetSoftChannelCoordinateToWorldCoord(transducer, pSoft, pFan, refCoord);
}

Vector3D Sounder::GetPolarToWorldCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, unsigned int depthPolar)
{
	Transducer *transducer = GetTransducer(numTransducer);
	Vector3D a(0, 0, 0);

	if (transducer)
	{
		a.z = (depthPolar + transducer->GetSampleOffset())*transducer->getBeamsSamplesSpacing();
	}
	return 	GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, a);
}

// the return value is a 3D vector (latitude,longitude,depth)
Vector3D Sounder::GetPolarToGeoCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, unsigned int depthPolar)
{
	Vector3D pos(0.0, 0.0, 0.0);
	Transducer *transducer = GetTransducer(numTransducer);
	if (transducer)
	{
		pos.z = (depthPolar + transducer->GetSampleOffset())*transducer->getBeamsSamplesSpacing() + transducer->GetInternalDelayTranslation();
	}

	return GetSoftChannelCoordinateToGeoCoord(pFan, numTransducer, softChannel, pos);
}

void Sounder::GetPolarToGeoCoord(PingFan* pFan, unsigned int numTransducer, unsigned int softChannel, const std::vector<unsigned int> & depthPolar, std::vector<BaseMathLib::Vector3D> & res)
{
	const size_t n = depthPolar.size();

	std::vector<Vector3D> pos(n);
	Transducer* transducer = GetTransducer(numTransducer);
	if (transducer)
	{
		for (size_t i = 0; i < n; ++i)
		{
			pos[i].z = (depthPolar[i] + transducer->GetSampleOffset()) * transducer->getBeamsSamplesSpacing() + transducer->GetInternalDelayTranslation();
		}
	}

	GetSoftChannelCoordinateToGeoCoord(pFan, numTransducer, softChannel, pos, res);
}

// converts the return value of GetPolarToGeoCoord to cartesian coordinates (x,y,depth)
Vector3D Sounder::GetPolarToCartesianCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, unsigned int depthPolar)
{
	Vector3D geocoords = GetPolarToGeoCoord(pFan, numTransducer, softChannel, depthPolar);

	CCartoTools::GetCartesianCoords(geocoords.x, geocoords.y, geocoords.x, geocoords.y);

	return geocoords;
}

// the return value is a 3D vector (latitude,longitude,depth)
BaseMathLib::Vector3D Sounder::GetSoftChannelCoordinateToGeoCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, const BaseMathLib::Vector3D &refCoord)
{
	Transducer *transducer = GetTransducer(numTransducer);
	SoftChannel *pSoft = NULL;
	if (transducer)
		pSoft = transducer->getSoftChannelPolarX(softChannel);

	return GetSoftChannelCoordinateToGeoCoord(transducer, pSoft, pFan, refCoord);
}

void Sounder::GetSoftChannelCoordinateToGeoCoord(PingFan* pFan, unsigned int numTransducer, unsigned int softChannel, const std::vector<BaseMathLib::Vector3D> & refCoord, std::vector<BaseMathLib::Vector3D> & res)
{
	Transducer* transducer = GetTransducer(numTransducer);
	SoftChannel* pSoft = NULL;
	if (transducer)
		pSoft = transducer->getSoftChannelPolarX(softChannel);

	GetSoftChannelCoordinateToGeoCoord(transducer, pSoft, pFan, refCoord, res);
}

// converts the return value of GetPolarToGeoCoord to cartesian coordinates (x,y,depth)
Vector3D Sounder::GetSoftChannelCoordinateToCartesianCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, const BaseMathLib::Vector3D &refCoord)
{
	Transducer *transducer = GetTransducer(numTransducer);
	SoftChannel *pSoft = NULL;
	if (transducer)
		pSoft = transducer->getSoftChannelPolarX(softChannel);

	return GetSoftChannelCoordinateToCartesianCoord(transducer, pSoft, pFan, refCoord);
}

void Sounder::GetBeamOrientation(PingFan *pFan, double &Roll, double &Pitch, unsigned int numBeam)
{
	Roll = 0;
	Pitch = 0;

	Transducer *transducer = GetTransducer(numBeam);
	SoftChannel *pSoft = NULL;
	if (transducer)
		pSoft = transducer->getSoftChannelPolarX(0);

	if (pFan && transducer && pSoft)
	{
		Pitch = pFan->GetNavAttitudeRef(pSoft->getSoftwareChannelId())->m_pitchRad
			+ transducer->m_transFaceAlongAngleOffsetRad + pSoft->m_mainBeamAlongSteeringAngleRad;
		Roll = pFan->GetNavAttitudeRef(pSoft->getSoftwareChannelId())->m_rollRad - transducer->m_transFaceAthwarAngleOffsetRad
			+ pSoft->m_mainBeamAthwartSteeringAngleRad;
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "One data (PingFan, transducer or SoftChannel) not found check your polarCoord");
	}
}
BaseMathLib::Box Sounder::GetAxisAlignedBoundingBox(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, unsigned int depthPolar)
{
	BaseMathLib::Vector3D pointA(0, 0, 0), pointB(0, 0, 0);
	Transducer *transducer = GetTransducer(numTransducer);
	SoftChannel *pSoft = NULL;
	if (transducer)
		pSoft = transducer->getSoftChannelPolarX(softChannel);
	double	alongShipAngleRad = pSoft->m_beam3dBWidthAlongRad / 2;
	double	athwartShipAngleRad = pSoft->m_beam3dBWidthAthwartRad / 2;

	double Za = (depthPolar + transducer->GetSampleOffset() + 0.5)*transducer->getBeamsSamplesSpacing() + transducer->GetInternalDelayTranslation();
	//		pointB.z=(depthPolar+transducer->GetSampleOffset()-0.5)*transducer->getBeamsSamplesSpacing()+transducer->GetInternalDelayTranslation();
	double Zb = Za - transducer->getBeamsSamplesSpacing();


	pointA.z = Za / (cos(alongShipAngleRad)*cos(athwartShipAngleRad));
	pointA.x = pointA.z*tan(alongShipAngleRad);
	pointA.y = pointA.z*tan(athwartShipAngleRad);
	pointB.z = Zb / (cos(alongShipAngleRad)*cos(athwartShipAngleRad));
	pointB.x = pointB.z*tan(alongShipAngleRad) / (cos(alongShipAngleRad)*cos(athwartShipAngleRad));
	pointB.y = pointB.z*tan(athwartShipAngleRad) / (cos(alongShipAngleRad)*cos(athwartShipAngleRad));

	Vector3D A = GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, pointA);
	Vector3D B = GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, pointB);
	BaseMathLib::Box ret(A, B);

	pointA.x = pointA.z*tan(-alongShipAngleRad) / (cos(-alongShipAngleRad)*cos(athwartShipAngleRad));
	pointA.y = pointA.z*tan(athwartShipAngleRad);
	pointB.x = pointB.z*tan(-alongShipAngleRad);
	pointB.y = pointB.z*tan(athwartShipAngleRad);
	A = GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, pointA);
	B = GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, pointB);

	ret.Extends(BaseMathLib::Box(A, B));

	pointA.x = pointA.z*tan(-alongShipAngleRad);
	pointA.y = pointA.z*tan(-athwartShipAngleRad);
	pointB.x = pointB.z*tan(-alongShipAngleRad);
	pointB.y = pointB.z*tan(-athwartShipAngleRad);
	A = GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, pointA);
	B = GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, pointB);

	ret.Extends(BaseMathLib::Box(A, B));

	pointA.x = pointA.z*tan(+alongShipAngleRad);
	pointA.y = pointA.z*tan(-athwartShipAngleRad);
	pointB.x = pointB.z*tan(+alongShipAngleRad);
	pointB.y = pointB.z*tan(-athwartShipAngleRad);
	A = GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, pointA);
	B = GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, pointB);

	pointA.x = pointA.z*tan(+alongShipAngleRad);
	pointA.y = pointA.z*tan(-athwartShipAngleRad);
	pointB.x = pointB.z*tan(+alongShipAngleRad);
	pointB.y = pointB.z*tan(-athwartShipAngleRad);


	ret.Extends(BaseMathLib::Box(A, B));


	pointA.z = Za;
	pointB.z = Zb;

	A = GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, pointA);
	B = GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, softChannel, pointB);

	ret.Extends(BaseMathLib::Box(A, B));

	return ret;
}

double Sounder::GetEchoDistance(PingFan* pFan, unsigned int numTransducer, unsigned int softChannel, unsigned int depthPolar) const
{
	double depth = 0.0;
	Transducer* transducer = GetTransducer(numTransducer);
	if (transducer)
	{
		SoftChannel* pSoft = transducer->getSoftChannelPolarX(softChannel);
		if (pSoft)
		{
			depth = (depthPolar + transducer->GetSampleOffset()) * transducer->getBeamsSamplesSpacing()
				+ transducer->GetInternalDelayTranslation()
				+ transducer->m_transDepthMeter
				+ pFan->getHeaveChan(pSoft->m_softChannelId);
		}
	}
	return depth;
}

bool Sounder::IsEqual(const Sounder &SounderB) const
{
	bool ret = true;
	ret &= (m_SounderId == SounderB.m_SounderId
		//		&& m_soundVelocity == SounderB.m_soundVelocity
		&& m_triggerMode == SounderB.m_triggerMode
		&& m_numberOfTransducer == SounderB.m_numberOfTransducer
		// OTK - FAE064 - ne pas �mettre de sounderchanged sur changement de la dur�e interping
		//		&& m_pingInterval == SounderB.m_pingInterval
		&& m_isMultiBeam == SounderB.m_isMultiBeam
		&& m_transducerList.size() == SounderB.m_transducerList.size());
	
	if (!ret)
		return ret;
	
	for (unsigned int i = 0; i < this->m_transducerList.size(); i++)
	{
		// OTK - FAE053 - le test d'�galit� des param�tres des transducteurs
		// n'�tait pas fait correctement
		ret &= m_transducerList[i]->IsEqual(*(SounderB.m_transducerList[i]));
	}
	return ret;
}

Sounder * SounderEk500::Clone() const
{
	auto s = SounderEk500::Create();
	s->Copy(*this, true);
	return s;
}

Sounder * SounderSBES::Clone() const
{
	auto s = SounderSBES::Create();
	s->Copy(*this, true);
	return s;
}

Sounder * SounderER60::Clone() const
{
	auto s = SounderER60::Create();
	s->Copy(*this, true);
	return s;
}

Sounder * SounderEK80::Clone() const
{
	auto s = SounderEK80::Create();
	s->Copy(*this, true);
	return s;
}

Sounder * SounderGeneric::Clone() const
{
	auto s = SounderGeneric::Create();
	s->Copy(*this, true);
	return s;
}