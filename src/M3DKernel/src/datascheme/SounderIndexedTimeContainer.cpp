#include "M3DKernel/datascheme/SounderIndexedTimeContainer.h"

SounderIndexedTimeContainer::SounderIndexedTimeContainer(void)
{
	m_allObjectContainer = TimeObjectContainer::Create();
	MovRef(m_allObjectContainer);
}

SounderIndexedTimeContainer::~SounderIndexedTimeContainer(void)
{
	MovUnRefDelete(m_allObjectContainer);
	MapSounderIndexedContainer::iterator res = m_sounderIndexedContainer.begin();
	while (res != m_sounderIndexedContainer.end())
	{
		MovUnRefDelete(res->second);
		res++;
	}
	m_sounderIndexedContainer.clear();
}

TimeObjectContainer *SounderIndexedTimeContainer::GetSounderIndexedContainer(std::uint32_t sounderId)
{
	MapSounderIndexedContainer::iterator res = m_sounderIndexedContainer.find(sounderId);
	if (res != m_sounderIndexedContainer.end())
	{
		return res->second;
	}
	else
	{
		TimeObjectContainer *pRef = TimeObjectContainer::Create();
		MovRef(pRef);
		m_sounderIndexedContainer.insert(MapSounderIndexedContainer::value_type(sounderId, pRef));
		return pRef;
	}
}
void SounderIndexedTimeContainer::AddObject(DatedObject *pObject, std::uint32_t sounderId)
{
	m_allObjectContainer->AddObject(pObject);
	GetSounderIndexedContainer(sounderId)->AddObject(pObject);
}

void SounderIndexedTimeContainer::RemoveObject(DatedObject *pObject, std::uint32_t sounderId)
{
	GetSounderIndexedContainer(sounderId)->RemoveObject(pObject);
	m_allObjectContainer->RemoveObject(pObject);
}

void SounderIndexedTimeContainer::RemoveAllObjects()
{	
	for (auto it = m_sounderIndexedContainer.cbegin(); it != m_sounderIndexedContainer.cend(); ++it)
	{
		it->second->RemoveAllObjects();
	}
	m_sounderIndexedContainer.clear();
	m_allObjectContainer->RemoveAllObjects();
}

size_t SounderIndexedTimeContainer::GetObjectCount(void) const
{
	return m_allObjectContainer->GetObjectCount();
}

DatedObject* SounderIndexedTimeContainer::GetObjectWithIndex(size_t idx)
{
	return m_allObjectContainer->GetDatedObject(idx);
}

DatedObject*  SounderIndexedTimeContainer::GetObjectWithDate(HacTime fanTime)
{
	return this->m_allObjectContainer->FindDatedObject(fanTime);
}

DatedObject* SounderIndexedTimeContainer::GetOldestObject()
{
	return m_allObjectContainer->GetOldestDatedObject();
}

void SounderIndexedTimeContainer::Sort()
{
	m_allObjectContainer->Sort();
	MapSounderIndexedContainer::iterator res = m_sounderIndexedContainer.begin();
	while (res != m_sounderIndexedContainer.end())
	{
		res->second->Sort();
		res++;
	}
}