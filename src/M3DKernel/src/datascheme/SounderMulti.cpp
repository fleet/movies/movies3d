#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/Transducer.h"

#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/NavAttributes.h"
#include "BaseMathLib/RotationMatrix.h"
#include "M3DKernel/datascheme/TransformMap.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/utils/carto/CartoTools.h"

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "M3DKernel.datascheme.SounderMulti";
}

SounderMulti::SounderMulti() 
	: Sounder()
{
}

Sounder * SounderMulti::Clone() const
{
	auto s = SounderMulti::Create();
	s->Copy(*this, true);
	return s;
}

SoftChannel* SounderMulti::getSoftChannel(unsigned short channelId)
{
	if (GetTransducerCount() == 1)
	{
		return GetTransducer(0)->getSoftChannel(channelId);
	}
	else
	{
		assert(0);
		return NULL;
	}
}

SoftChannel* SounderMulti::getSoftChannelPolarX(unsigned int polarId)
{
	if (GetTransducerCount() == 1)
	{
		return GetTransducer(0)->getSoftChannelPolarX(polarId);
	}
	else
	{
		return NULL;
	}
}

Transducer*	 SounderMulti::getTransducerForChannel(unsigned short channelId)
{
	if (GetTransducerCount() == 1)
	{
		return GetTransducer(0);
	}
	else
	{
		assert(0);
		return NULL;
	}
}

BaseMathLib::Vector3D SounderMulti::GetSoftChannelCoordinateToWorldCoord(Transducer *transducer, SoftChannel *pSoft, PingFan *pFan, const BaseMathLib::Vector3D &refCoord)
{
	//LB 10/07/2008 le rep�re monde est translat� sur le niveau moyen de la mer
	Vector3D ret(0, 0, transducer->m_transDepthMeter - transducer->GetTransducerTranslation().z);
	if (pFan && transducer && pSoft)
	{
#pragma message(" ________________________________________________ ")
#pragma message("!                                                !")
#pragma message("! Optimisation possible ??????                   !")
#pragma message("! transRotation()*pSoft->GetMatrixSoftChan       !")
#pragma message("!________________________________________________!")
		Vector3D delay(0, 0, transducer->GetInternalDelayTranslation());
		ret = ret + pFan->GetMatrixNav(pSoft->getSoftwareChannelId())*transducer->GetTransducerTranslation() + pFan->GetTranslationNav(pSoft->getSoftwareChannelId())
			+ pFan->GetHeadingMatrixNav(pSoft->getSoftwareChannelId())*transducer->GetTransducerRotation()*pSoft->GetMatrixSoftChan()*(refCoord + delay)
			;
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "One data (PingFan, transducer or SoftChannel) not found check you polarCoord");
	}

	return ret;
}

BaseMathLib::Vector3D SounderMulti::GetSoftChannelCoordinateToWorldCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, const BaseMathLib::Vector3D &refCoord)
{
	Transducer *transducer = GetTransducer(numTransducer);
	SoftChannel *pSoft = transducer->getSoftChannelPolarX(softChannel);
	return GetSoftChannelCoordinateToWorldCoord(transducer, pSoft, pFan, refCoord);
}

// the return value is a 3D vector (latitude,longitude,depth)
BaseMathLib::Vector3D SounderMulti::GetSoftChannelCoordinateToGeoCoord(PingFan *pFan, unsigned int numTransducer, unsigned int softChannel, const BaseMathLib::Vector3D &refCoord)
{
	Transducer *transducer = GetTransducer(numTransducer);
	SoftChannel *pSoft = NULL;
	if (transducer)
		pSoft = transducer->getSoftChannelPolarX(softChannel);

	return GetSoftChannelCoordinateToGeoCoord(transducer, pSoft, pFan, refCoord);
}

BaseMathLib::Vector3D SounderMulti::GetSoftChannelCoordinateToGeoCoord(Transducer *transducer, SoftChannel *pSoft, PingFan *pFan, const BaseMathLib::Vector3D &refCoord)
{
	Vector3D relativeVector(0, 0, transducer->m_transDepthMeter - transducer->GetTransducerTranslation().z + pFan->GetTranslationNav(pSoft->getSoftwareChannelId()).z);
	Vector3D shipGeoPosition(pFan->GetNavPositionRef(pSoft->getSoftwareChannelId())->m_lattitudeDeg, pFan->GetNavPositionRef(pSoft->getSoftwareChannelId())->m_longitudeDeg, 0);
	Vector3D ret(0, 0, 0);

	if (pFan && transducer && pSoft)
	{
		relativeVector = relativeVector + pFan->GetMatrixNav(pSoft->getSoftwareChannelId())*transducer->GetTransducerTranslation()
			+ pFan->GetHeadingMatrixNav(pSoft->getSoftwareChannelId())*transducer->GetTransducerRotation()*pSoft->GetMatrixSoftChan()*refCoord;
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "One data (PingFan, transducer or SoftChannel) not found check your polarCoord");
	}

	// depth
	ret.z = relativeVector.z;

	// lat long
	CCartoTools::GetGeoCoords(shipGeoPosition.x, shipGeoPosition.y,
		relativeVector.x, relativeVector.y, ret.x, ret.y);

	return ret;
}

void SounderMulti::GetSoftChannelCoordinateToGeoCoord(Transducer* transducer, SoftChannel* pSoft, PingFan* pFan, const std::vector<BaseMathLib::Vector3D>& refCoord, std::vector<BaseMathLib::Vector3D>& res)
{
	if (pFan && transducer && pSoft)
	{
		const size_t n = refCoord.size();
		std::vector<BaseMathLib::Vector3D> relativeVector(n);

		BaseMathLib::Vector3D baseRelativeVector(0, 0, transducer->m_transDepthMeter - transducer->GetTransducerTranslation().z + pFan->GetTranslationNav(pSoft->getSoftwareChannelId()).z);

		const auto refCoordOffset = pFan->GetMatrixNav(pSoft->getSoftwareChannelId()) * transducer->GetTransducerTranslation();
		baseRelativeVector = baseRelativeVector + refCoordOffset;

		const auto refCoordFact = pFan->GetHeadingMatrixNav(pSoft->getSoftwareChannelId()) * transducer->GetTransducerRotation() * pSoft->GetMatrixSoftChan();
		for (size_t i = 0; i < n; ++i)
		{
			relativeVector[i] = baseRelativeVector + refCoordFact * refCoord[i];
		}

		// depth
		for (size_t i = 0; i < n; ++i)
		{
			res[i].z = relativeVector[i].z;
		}

		Vector3D shipGeoPosition(pFan->GetNavPositionRef(pSoft->getSoftwareChannelId())->m_lattitudeDeg, pFan->GetNavPositionRef(pSoft->getSoftwareChannelId())->m_longitudeDeg, 0);
		CCartoTools::GetGeoCoords(shipGeoPosition, relativeVector, res);
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "One data (PingFan, transducer or SoftChannel) not found check your polarCoord");
	}
}

void SounderMulti::GetBeamOrientation(PingFan *pFan, double &Roll, double &Pitch, unsigned int numBeam)
{
	Roll = 0;
	Pitch = 0;

	Transducer *transducer = GetTransducer(0);
	SoftChannel *pSoft = NULL;
	if (transducer)
		pSoft = transducer->getSoftChannelPolarX(numBeam);

	if (transducer && pSoft)
	{
		Pitch = transducer->m_transFaceAlongAngleOffsetRad + pSoft->m_mainBeamAlongSteeringAngleRad;
		Roll = transducer->m_transFaceAthwarAngleOffsetRad
			- pSoft->m_mainBeamAthwartSteeringAngleRad;
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "One data (PingFan, transducer or SoftChannel) not found check your polarCoord");
	}
}