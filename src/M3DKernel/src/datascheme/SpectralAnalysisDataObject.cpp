#include "M3DKernel/datascheme/SpectralAnalysisDataObject.h"

std::vector<double> SpectralAnalysisDataObject::svValuesForFrequencyIndex(int freqIndex) const
{
	if (freqIndex < 0 || freqIndex > numberOfFrequencies())
		return std::vector<double>();

	std::vector<double> results(numberOfRanges());

	int i = 0;
	for (auto valuesPerRanges : svValues)
	{
		results[i] = valuesPerRanges[freqIndex];
		++i;
	}

	return results;
}

std::vector<double> SpectralAnalysisDataObject::svValuesForRangeIndex(int rangeIndex) const
{
	if (rangeIndex < 0 || rangeIndex > numberOfRanges())
		return std::vector<double>();

	return svValues[rangeIndex];
}

double SpectralAnalysisDataObject::sv(int rangeIndex, int freqIndex) const
{
	return svValues[rangeIndex][freqIndex];
}