#include "M3DKernel/datascheme/SpectralAnalysisParameters.h"
#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;
using namespace xercesc;

SpectralAnalysisParameters::SpectralAnalysisParameters() : ParameterModule("SpectralAnalysisParameter")
{
	m_EK80_FM_PulseCompression = true;
	m_spectralAnalysis = false;
	m_FrequencyResolution = 2000; //temp value
}


SpectralAnalysisParameters::~SpectralAnalysisParameters()
{
}

// sauvegarde des paramètres dans le fichier de configuration associé
bool SpectralAnalysisParameters::Serialize(MovConfig * movConfig)
{
	// debut de la sérialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	movConfig->SerializeData<bool>(m_EK80_FM_PulseCompression, eBool, "PulseCompression");
	movConfig->SerializeData<bool>(m_spectralAnalysis, eBool, "SpectralAnalysis");
	movConfig->SerializeData<int>(m_FrequencyResolution, eUInt, "FrequencyResolution");

	// fin de la sérialisation du module
	movConfig->SerializePushBack();

	return true;
}


// lecture des paramètres dans le fichier de configuration associé
bool SpectralAnalysisParameters::DeSerialize(MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	// debut de la désérialisation du module
	if (result)
	{
		result = result && movConfig->DeSerializeData<bool>(&m_EK80_FM_PulseCompression, eBool, "PulseCompression");
		result = result && movConfig->DeSerializeData<bool>(&m_spectralAnalysis, eBool, "SpectralAnalysis");
		result = result && movConfig->DeSerializeData<int>(&m_FrequencyResolution, eUInt, "FrequencyResolution");
	}
	// fin de la désérialisation du module
	movConfig->DeSerializePushBack();

	return result;
}
