#include "M3DKernel/datascheme/SplitBeamParameter.h"
#include <string>


SplitBeamParameter::SplitBeamParameter(void)
{
	m_startLogTime.SetToNull();
	m_splitBeamParameterChannelId = 0;
	m_minimumValue = 0;
	m_minimumEchoLength = 0;
	m_maximumEchoLenght = 10;
	m_maximumGainCompensation = 6;
	m_maximumPhaseCompensation = 10;

    sprintf(m_Remark, "DefaultAlgorithm");
	m_attribute = 1;
}

SplitBeamParameter::~SplitBeamParameter(void)
{
}
