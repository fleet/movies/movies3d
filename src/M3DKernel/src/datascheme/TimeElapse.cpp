#include "M3DKernel/datascheme/TimeElapse.h"
#include <cstdint>

TimeElapse::TimeElapse(void)
{
	m_timeElapse = 0;
}

TimeElapse::TimeElapse(std::int64_t m_time)
{
	m_timeElapse = m_time;
}

TimeElapse::~TimeElapse(void)
{
}

TimeElapse operator+(const TimeElapse &v1, const TimeElapse &v2)
{
	return TimeElapse(v1.m_timeElapse + v2.m_timeElapse);
}
