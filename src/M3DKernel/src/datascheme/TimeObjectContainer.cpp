#include "M3DKernel/datascheme/TimeObjectContainer.h"
#include <algorithm>

struct DatedObjectComparator
{
	bool operator() (const DatedObject * o1, const DatedObject * o2) const
	{
		return o1->m_ObjectTime < o2->m_ObjectTime;
	}

	bool operator() (const HacTime & hacTime, const DatedObject * datedObject) const
	{
		return hacTime < datedObject->m_ObjectTime;
	}

	bool operator() (const DatedObject * datedObject, const HacTime & hacTime) const
	{
		return datedObject->m_ObjectTime < hacTime;
	}
};

TimeObjectContainer::~TimeObjectContainer()
{
	RemoveAllObjects();
}

TimeObjectContainer::TimeObjectContainer()
{
}

void TimeObjectContainer::AddObject(DatedObject *pObject)
{
	if (pObject)
	{
		MovRef(pObject);

		m_mainContainer.push_back(pObject);
		m_datedContainer[pObject->m_ObjectTime].push_back(pObject);
	}
}

void TimeObjectContainer::RemoveOldObject(const HacTime &maxDate)
{
	// we remove the oldest object if the next one is less than the date we are looking for
	while (m_datedContainer.size() > 1 && (++m_datedContainer.begin())->first < maxDate)
	{
		// Suppression du conteneur principal
		const std::vector<DatedObject*> & vect = m_datedContainer.begin()->second;
        for (DatedObject* p : vect)
		{
            auto it = std::find(m_mainContainer.begin(), m_mainContainer.end(), p);

			assert(it != m_mainContainer.end());

			MovUnRefDelete(p);
			m_mainContainer.erase(it);
		}

		// Suppression du conteneur tri� par date
		m_datedContainer.erase(m_datedContainer.begin());
	}
}

void TimeObjectContainer::RemoveObject(DatedObject *p)
{
	if (p == NULL)
		return;

	// Suppression du conteneur index� par date
	std::map<HacTime, std::vector<DatedObject*> >::iterator itMap = m_datedContainer.find(p->m_ObjectTime);
	if (itMap != m_datedContainer.end())
	{
		std::vector<DatedObject*> & vect = itMap->second;
		std::vector<DatedObject*>::const_iterator it = std::find(vect.begin(), vect.end(), p);
		if (it != vect.end())
		{
			vect.erase(it);
			if (vect.empty())
			{
				m_datedContainer.erase(itMap);
			}
		}
	}

	// Suppression du conteneur principal
    auto it = std::find(m_mainContainer.begin(), m_mainContainer.end(), p);
	if (it != m_mainContainer.end())
	{
		MovUnRefDelete(p);
		m_mainContainer.erase(it);
	}
}

void TimeObjectContainer::RemoveObject(unsigned int idx)
{
	DatedObject *p = GetDatedObject(idx);
    if (p == nullptr)
		return;

	// Suppression du conteneur index� par date
	std::map<HacTime, std::vector<DatedObject*> >::iterator itMap = m_datedContainer.find(p->m_ObjectTime);
	if (itMap != m_datedContainer.end())
	{
		std::vector<DatedObject*> & vect = itMap->second;
		std::vector<DatedObject*>::const_iterator it = std::find(vect.begin(), vect.end(), p);
		if (it != vect.end())
		{
			vect.erase(it);
			if (vect.empty())
			{
				m_datedContainer.erase(itMap);
			}
		}
	}

	// Suppression du conteneur principal
	m_mainContainer.erase(m_mainContainer.begin() + idx);
	MovUnRefDelete(p);
}

void TimeObjectContainer::RemoveAllObjects()
{
	for (DatedObject * pObj : m_mainContainer)
	{
		MovUnRefDelete(pObj);
	}
	m_mainContainer.clear();
	m_datedContainer.clear();
}

DatedObject* TimeObjectContainer::FindDatedObject(const HacTime &seekDate) const
{
	//rmq. : on ne teste pas le caract�re non vide de itMap->second car on nettoie la map si on supprime le dernier �l�ment d'un vecteur pour une date donn�e
	// Et pour rester coh�rent avec la version "historique", on renvoie l'�lement correspondant � la date ajout� en dernier.
	const auto itMap = m_datedContainer.find(seekDate);
	return (itMap != m_datedContainer.end()) ? itMap->second.back() : nullptr;
}

std::vector<DatedObject*> TimeObjectContainer::FindDatedObjects(const HacTime &seekDate) const
{	
	const auto itMap = m_datedContainer.find(seekDate);
	return (itMap != m_datedContainer.end()) ? itMap->second : std::vector<DatedObject*>();
}

DatedObject* TimeObjectContainer::FindPreviousDatedObject(const HacTime &seekDate) const
{
	const auto itMap = m_datedContainer.lower_bound(seekDate);
	return (itMap != m_datedContainer.end()) ? itMap->second.front() : nullptr;
}

std::vector<DatedObject*> TimeObjectContainer::FindPreviousDatedObjects(const HacTime &seekDate) const
{
	const auto itMap = m_datedContainer.lower_bound(seekDate);
	return (itMap != m_datedContainer.end()) ? itMap->second : std::vector<DatedObject*>();
}

DatedObject* TimeObjectContainer::FindNextDatedObject(const HacTime &seekDate) const
{
	//rmq. : on ne teste pas le caract�re non vide de itMap->second car on nettoie la map si on supprime le dernier �l�ment d'un vecteur pour une date donn�e
	// Et pour rester coh�rent avec la version "historique", on renvoie l'�lement correspondant � la date ajout� en dernier.
	const auto itMap = m_datedContainer.upper_bound(seekDate);
	return (itMap != m_datedContainer.end()) ? itMap->second.back() : nullptr;
}

std::vector<DatedObject*> TimeObjectContainer::FindNextDatedObjects(const HacTime &seekDate) const
{
	const auto itMap = m_datedContainer.upper_bound(seekDate);
	return (itMap != m_datedContainer.end()) ? itMap->second : std::vector<DatedObject*>();
}

DatedObject* TimeObjectContainer::FindClosestDatedObject(const HacTime &seekDate) const
{
	const auto itDown = m_datedContainer.lower_bound(seekDate);
	const auto itUp = m_datedContainer.upper_bound(seekDate);

	if (itDown == m_datedContainer.cend())
	{
		if (itUp == m_datedContainer.cend())
		{
			if (m_datedContainer.empty())
			{
				return nullptr;
			}
			else
			{
				return m_datedContainer.rbegin()->second.back();
			}
		}
		else
		{
			return itUp->second.back();
		}
	}
	else
	{
		if (itUp == m_datedContainer.cend())
		{
			return itDown->second.back();
		}
		else
		{
			// get closeset
			if ((seekDate - itDown->first) < (itUp->first - seekDate))
			{
				return itDown->second.back();
			}
			else
			{
				return itUp->second.back();
			}
		}
	}
}

std::vector<DatedObject*> TimeObjectContainer::FindClosestDatedObjects(const HacTime &seekDate) const
{
	const auto itDown = m_datedContainer.lower_bound(seekDate);
	const auto itUp = m_datedContainer.upper_bound(seekDate);

	if (itDown == m_datedContainer.cend())
	{
		if (itUp == m_datedContainer.cend())
		{
			if (m_datedContainer.empty())
			{
				return std::vector<DatedObject*>();
			}
			else
			{
				return m_datedContainer.rbegin()->second;
			}
		}
		else
		{
			return itUp->second;
		}
	}
	else
	{
		if (itUp == m_datedContainer.cend())
		{
			return itDown->second;
		}
		else
		{
			// get closeset
			if ((seekDate - itDown->first) < (itUp->first - seekDate))
			{
				return itDown->second;
			}
			else
			{
				return itUp->second;
			}
		}
	}
}

DatedObject* TimeObjectContainer::GetDatedObject(unsigned int idx) const
{
	return (idx < m_mainContainer.size() && idx >= 0) ? m_mainContainer[idx] : nullptr;
}

DatedObject* TimeObjectContainer::GetFirstDatedObject() const
{
	return (!m_mainContainer.empty()) ? m_mainContainer.front() : nullptr;
}

DatedObject* TimeObjectContainer::GetLastDatedObject() const
{
	return (!m_mainContainer.empty()) ? m_mainContainer.back() : nullptr;
}

DatedObject* TimeObjectContainer::GetOldestDatedObject() const
{
	return (!m_datedContainer.empty()) ? m_datedContainer.begin()->second.front() : nullptr;
}

/** Get the number of object stored*/
size_t TimeObjectContainer::GetObjectCount() const
{
	return m_mainContainer.size();
}

void TimeObjectContainer::Sort()
{
	std::sort(m_mainContainer.begin(), m_mainContainer.end(), DatedObjectComparator());
}
