#include "M3DKernel/datascheme/Transducer.h"

#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/SignalFilteringObject.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/utils/M3DStdUtils.h"
#include "M3DKernel/utils/mathutils.h"

using namespace BaseMathLib;

namespace
{
	constexpr const char * LOGGER_NAME = "M3DKernel.datascheme.Transducer";
}

TransData::TransData() : HacObject(), m_transName(""), m_transSoftVersion(""), m_pulseDuration(0), m_pulseShape(0), m_pulseSlope(0), m_pulseForm(0), m_timeSampleInterval(0),
                         m_frequencyBeamSpacing(0),
                         m_frequencySpaceShape(0),
                         m_transPower(0),
                         m_transDepthMeter(0),
                         m_platformId(0),
                         m_transShape(0),
                         m_transFaceAlongAngleOffsetRad(0),
                         m_transFaceAthwarAngleOffsetRad(0),
                         m_transRotationAngleRad(0),
                         m_numberOfSoftChannel(0),
                         m_initialWBTSamplingFrequency(0), m_transducerImpedance(0),
                         m_WBTImpedance(0),
                         m_bDefaultRotation(false),
                         m_bDefaultFaceAlongAngleOffset(false),
                         m_bDefaultFaceAthwartAngleOffset(false),
                         m_bUseAngleCustomValues(false)
{
	strcpy_s(m_transName, sizeof(m_transName), "Unavailable");
	strcpy_s(m_transSoftVersion, sizeof(m_transSoftVersion), "Unavailable");
}

Transducer::~Transducer()
{
	auto res = m_softChannel.begin();
	while (res != m_softChannel.end())
	{
		if (res->second != nullptr)
		{
			MovUnRefDelete(res->second)
		}
		++res;
	}
	MovUnRefDelete(m_pPlatform)
	MovUnRefDelete(m_pFPGAFilter)
	MovUnRefDelete(m_pApplicationFilter)
}

Transducer::Transducer()
: m_beamsSamplesSpacing(0)
{
	m_pPlatform = nullptr;
	m_internalDelayTranslation = 0;
	m_sampleOffset = 0;
	m_pFPGAFilter = nullptr;
	m_pApplicationFilter = nullptr;
}

// OTK - FAE067 - recopie des sondeurs/transducteurs/channels
void Transducer::Copy(const Transducer &other)
{
	// héritage
	TransData::Copy(other);

	// copie
	if (other.m_pPlatform != nullptr)
	{
		Platform* pPlatform = Platform::Create();
		pPlatform->Copy(*other.m_pPlatform);
		SetPlatform(pPlatform);
	}
	if (other.m_pFPGAFilter != nullptr)
	{
		SignalFilteringObject* pFPGAFilter = SignalFilteringObject::Create();
		pFPGAFilter->Copy(*other.m_pFPGAFilter);
		SetFPGAFilter(pFPGAFilter);
	}
	if (other.m_pApplicationFilter != nullptr)
	{
		SignalFilteringObject* pApplicationFilter = SignalFilteringObject::Create();
		pApplicationFilter->Copy(*other.m_pApplicationFilter);
		SetFPGAFilter(pApplicationFilter);
	}
	m_beamsSamplesSpacing = other.m_beamsSamplesSpacing;
	m_transducerTranslation = other.m_transducerTranslation;
	m_transducerRotation = other.m_transducerRotation;
	m_internalDelayTranslation = other.m_internalDelayTranslation;
	m_sampleOffset = other.m_sampleOffset;
	m_idChannel = other.m_idChannel;

	m_initialWBTSamplingFrequency = other.m_initialWBTSamplingFrequency;
	m_transducerImpedance = other.m_transducerImpedance;
	m_WBTImpedance = other.m_WBTImpedance;

	// OTK - Important : les channels ne sont pas recopies (on récupère juste les référence)
	// Une vraie recopie est inutile puisque la recopie des sondeurs intervient
	// dans le cas où on récupère un nouvel objet channel à mettre à jour.
	// Les channels non mis à jour peuvent donc être partagés entre les différentes définitions de sondeurs.
	// les channels mis à jour sont de toute façon reconstruits.
	// on doit tout de même références les objets channels pour pas qu'ils soient détruits
	// à la destruction des autres sondeurs
	auto iter = other.m_softChannel.cbegin();
	while (iter != other.m_softChannel.cend())
	{
		AddSoftChannel(iter->second);
		++iter;
	}
}

void Transducer::Compute(Traverser &traverser)
{
	m_bIsComplete = m_pPlatform != nullptr &&
		m_softChannel.size() == m_numberOfSoftChannel;
}

void Transducer::SetPlatform(Platform* platform)
{
	MovUnRefDelete(m_pPlatform)
	m_pPlatform = platform;
	MovRef(m_pPlatform)
	DataChanged();
}

void Transducer::SetFPGAFilter(SignalFilteringObject* fpgaFilter)
{
	MovUnRefDelete(m_pFPGAFilter)
	m_pFPGAFilter = fpgaFilter;
	MovRef(m_pFPGAFilter)
	DataChanged();
}

void Transducer::SetApplicationFilter(SignalFilteringObject* applicationFilter)
{
	MovUnRefDelete(m_pApplicationFilter)
	m_pApplicationFilter = applicationFilter;
	MovRef(m_pApplicationFilter)
	DataChanged();
}

void Transducer::OnComplete(Traverser &traverser)
{
	m_idChannel.clear();
	for (const auto& softChannel : m_softChannel)
	{
		softChannel.second->m_softChannelComputeData.m_PolarId = static_cast<unsigned int>(m_idChannel.size());
		m_idChannel.push_back(softChannel.first);
	}
	ComputeMatrix();
}

double Transducer::computeSampleSpacing(const double soundVelocity) const
{
	return soundVelocity * m_timeSampleInterval / 2000000.0;

#pragma message(" _______________________________________________ ")
#pragma message("!                                               !")
#pragma message("!          RAJOUTER LE GROUP ID                 !")
#pragma message("!_______________________________________________!")
}

const std::vector<unsigned short> & Transducer::GetChannelId() const
{
	return m_idChannel;
}

void Transducer::AddSoftChannel(SoftChannel* softChannel)
{
	if (softChannel)
	{
		const auto res = m_softChannel.find(softChannel->getSoftwareChannelId());

		if (res != m_softChannel.end())
		{
			auto previousChannel = res->second;
			/// hou la on a un nouveau on efface l'ancien
			MovUnRefDelete(previousChannel)
			res->second = softChannel;
		}
		else
		{
			m_softChannel.insert(MapSoftChannel::value_type(softChannel->getSoftwareChannelId(), softChannel));
		}
		MovRef(softChannel)
	}
}

SoftChannel* Transducer::getSoftChannel(const unsigned short channelId) const
{
	auto res = m_softChannel.find(channelId);
	if (res != m_softChannel.end())
	{
		return res->second;
	}

	// NMD - FAE 105 - recherche parmis les canaux virtuels 
	res = m_virtualSoftChannel.find(channelId);
	if (res != m_virtualSoftChannel.end())
	{
		return res->second;
	}

	return nullptr;
}

SoftChannel* Transducer::getSoftChannelByName(const char * name) const
{
	for (const auto& softChannel : m_softChannel)
	{
		if (strcmp(softChannel.second->m_channelName, name) == 0)
		{
			return softChannel.second;
		}
	}
	return nullptr;
}

void Transducer::AddVirtualSoftChannel(SoftChannel* softChannel, unsigned short virtualId)
{
	if (softChannel)
	{
		const auto& res = m_virtualSoftChannel.find(virtualId);

		if (res != m_virtualSoftChannel.end())
		{
			SoftChannel* previousChannel = res->second;
			MovUnRefDelete(previousChannel)
			res->second = softChannel;
		}
		else
		{
			m_virtualSoftChannel.insert(MapSoftChannel::value_type(virtualId, softChannel));
		}
		softChannel->setSoftwareVirtualChannelId(virtualId);
		MovRef(softChannel)
	}
}

SoftChannel* Transducer::getSoftChannelPolarX(const unsigned int channelIndex) const
{
	if (channelIndex < m_idChannel.size())
	{
		const auto& chanId = m_idChannel[channelIndex];
		return getSoftChannel(chanId);
	}
	
	M3D_LOG_WARN(LOGGER_NAME, "getSoftChannelPolarX:: Channel Not found");
	return nullptr;
}

SoftChannel* Transducer::getSoftChannelVertical() const
{
	double minAbsoluteAngle = abs(getSoftChannelPolarX(0)->m_mainBeamAthwartSteeringAngleRad - m_transFaceAthwarAngleOffsetRad);
	unsigned int indexVertical = 0;
	for (unsigned int i = 1; i < m_numberOfSoftChannel; i++)
	{
		if (abs(getSoftChannelPolarX(i)->m_mainBeamAthwartSteeringAngleRad - m_transFaceAthwarAngleOffsetRad) < minAbsoluteAngle)
		{
			minAbsoluteAngle = abs(getSoftChannelPolarX(i)->m_mainBeamAthwartSteeringAngleRad - m_transFaceAthwarAngleOffsetRad);
			indexVertical = i;
		}

	}

	return getSoftChannelPolarX(indexVertical);
}

void Transducer::ComputeMatrix()
{
	m_transducerTranslation = BaseMathLib::Vector3D(GetPlatform()->GetAlongShipOffset(), GetPlatform()->GetAthwartShipOffset(), GetPlatform()->GetDephtOffset());
	m_transducerRotation.CreateRotationMatrix(m_transRotationAngleRad, m_transFaceAlongAngleOffsetRad, m_transFaceAthwarAngleOffsetRad);

	for (const auto& softChannel : m_softChannel)
	{
		softChannel.second->ComputeMatrix();
	}
}

void Transducer::UpdateChildMember(Traverser &traverser)
{
	if (m_pPlatform)
	{
		m_pPlatform->Update(traverser);
	}

	for (const auto& softChannel : m_softChannel)
	{
		softChannel.second->Update(traverser);
	}
}

bool Transducer::IsEqual(const Transducer &otherTransducer) const
{
	auto ret = m_pulseDuration == otherTransducer.m_pulseDuration &&
		m_pulseForm == otherTransducer.m_pulseForm &&
		m_pulseShape == otherTransducer.m_pulseShape &&
		MathUtils::floatingPointEquals(m_pulseSlope, otherTransducer.m_pulseSlope) &&
		MathUtils::floatingPointEquals(m_timeSampleInterval, otherTransducer.m_timeSampleInterval) &&
		m_frequencyBeamSpacing == otherTransducer.m_frequencyBeamSpacing &&
		m_frequencySpaceShape == otherTransducer.m_frequencySpaceShape &&
		m_transPower == otherTransducer.m_transPower &&
		MathUtils::floatingPointEquals(m_transDepthMeter, otherTransducer.m_transDepthMeter) &&
		m_platformId == otherTransducer.m_platformId &&
		m_transShape == otherTransducer.m_transShape &&
		m_numberOfSoftChannel == otherTransducer.m_numberOfSoftChannel &&
		m_initialWBTSamplingFrequency == otherTransducer.m_initialWBTSamplingFrequency &&
		m_transducerImpedance == otherTransducer.m_transducerImpedance &&
		m_WBTImpedance == otherTransducer.m_WBTImpedance;

	// NMD - 26/05/11 - FE 096
	// On ne vérifie pas les valeurs d'angles des transducteurs si le module de compensation ne les ecrase pas par les valeurs "custom"
	if (!m_bUseAngleCustomValues)
	{
		ret &= MathUtils::floatingPointEquals(m_transRotationAngleRad, otherTransducer.m_transRotationAngleRad) &&
			MathUtils::floatingPointEquals(m_transFaceAlongAngleOffsetRad, otherTransducer.m_transFaceAlongAngleOffsetRad) &&
			MathUtils::floatingPointEquals(m_transFaceAthwarAngleOffsetRad, otherTransducer.m_transFaceAthwarAngleOffsetRad);
	}

	ret &= m_pPlatform->IsEqual(*(otherTransducer.m_pPlatform));
	if (m_pFPGAFilter)
	{
		ret &= otherTransducer.m_pFPGAFilter && m_pFPGAFilter->IsEqual(*otherTransducer.m_pFPGAFilter);
	}
	else
	{
		ret &= !otherTransducer.m_pFPGAFilter;
	}
	if (m_pApplicationFilter)
	{
		ret &= otherTransducer.m_pApplicationFilter && m_pApplicationFilter->IsEqual(*otherTransducer.m_pApplicationFilter);
	}
	else
	{
		ret &= !otherTransducer.m_pApplicationFilter;
	}
	
	if (!ret)
	{
		return ret;
	}
	
	for (unsigned int i = 0; i < m_numberOfSoftChannel; i++)
	{
		const auto& softChannel = getSoftChannelPolarX(i);
		const auto& otherSoftChannel = otherTransducer.getSoftChannelPolarX(i);

		ret &= softChannel->IsEqual(*otherSoftChannel);
	}
	return ret;
}
