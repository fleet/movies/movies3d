#include "M3DKernel/datascheme/TransformMap.h"
#include "M3DKernel/utils/log/ILogger.h"

#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/M3DKernel.h"

#include <algorithm>

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "M3DKernel.datascheme.TransformMap";
}

TransformMap::~TransformMap()
{
	delete[] m_indexes;
	delete[] m_channelIndexes;
}

TransformMap::TransformMap() :
	m_mustBeComputed(true),
	m_polarSize(0),
	m_xSpacing(DEFAULT_X_SPACING),
	m_ySpacing(DEFAULT_Y_SPACING),
	m_p1(0.0f),
	m_p2(0.0f),
	m_tableSize(0),
	m_indexes(NULL),
	m_channelIndexes(NULL)
{
	m_pTransducer = NULL;
}

void TransformMap::Update()
{
	if (m_mustBeComputed)
		computeTable();
}

void TransformMap::SetTransducer(Transducer *p)
{
	m_pTransducer = p;
}

void TransformMap::CheckReaderParameter()
{
	// OTK - 23/03/2009 - ajout mode d'�chantillonnage automatique
	if (M3DKernel::GetInstance()->GetRefKernelParameter().getCustomSampling())
	{
		setXYSpacing(
			M3DKernel::GetInstance()->GetRefKernelParameter().getScreenPixelSizeX(),
			M3DKernel::GetInstance()->GetRefKernelParameter().getScreenPixelSizeY());
	}

	computeTable();
}

void TransformMap::SetMaxRange(double m_range)
{
	setPolarSize(Vector2I(m_pTransducer->m_numberOfSoftChannel, m_range / m_pTransducer->getBeamsSamplesSpacing()));

	// OTK - 23/03/2009 - ajout mode d'�chantillonnage automatique
	if (M3DKernel::GetInstance()->GetRefKernelParameter().getCustomSampling())
	{
		setXYSpacing(
			M3DKernel::GetInstance()->GetRefKernelParameter().getScreenPixelSizeX(),
			M3DKernel::GetInstance()->GetRefKernelParameter().getScreenPixelSizeY());
	}

	computeTable();
}

void TransformMap::setPolarSize(Vector2I newSize)
{
	if (newSize == m_polarSize)
		return;
	m_polarSize = newSize;
	m_mustBeComputed = true;
}

void TransformMap::setXYSpacing(double newSpacingX, double newSpacingY)
{
	if (newSpacingX == m_xSpacing && newSpacingY == m_ySpacing)
		return;
	m_xSpacing = newSpacingX;
	m_ySpacing = newSpacingY;
	m_mustBeComputed = true;
}

void TransformMap::computeTable(void)
{
	if (!m_mustBeComputed || m_polarSize.y <= 0)
		return;

	M3D_LOG_INFO(LoggerName, "Compute Transformation Table ...");
	if (getAngle1() < 0.0f)
		m_p1.x = sin(getAngle1()) * getRadius();
	else
		m_p1.x = 0.0f;
	if (getAngle2() > PI_D2)
		m_p1.y = cos(getAngle2()) * getRadius();
	else
		m_p1.y = 0.0f;

	if (getAngle2() < PI_D2)
		m_p2.x = sin(getAngle2()) * getRadius();
	else
		m_p2.x = getRadius();
	if (getAngle1() > 0.0f)
		m_p2.y = cos(getAngle1()) * getRadius();
	else
		m_p2.y = getRadius();


	m_delta.x = m_p2.x - m_p1.x;
	m_delta.y = m_p2.y - m_p1.y;
	
	// OTK - FAE021 - en mode automatique, on limite la taille de la transformmap � la taille 
	// des fen�tres IHM
	int initWindowWidth = M3DKernel::GetInstance()->GetRefKernelParameter().getInitWindowWidth();
	int initWindowHeight = M3DKernel::GetInstance()->GetRefKernelParameter().getInitWindowHeight();
	if (initWindowWidth != -1 && initWindowHeight != -1
		&& !M3DKernel::GetInstance()->GetRefKernelParameter().getCustomSampling()
		&& m_indexes == NULL)
	{
		m_xSpacing = m_delta.x / (double)initWindowWidth;
		m_ySpacing = m_delta.y / (double)initWindowHeight;
	}
	// fin FAE021

	m_tableSize.x = std::max<int>((int)((m_delta.x) / (m_xSpacing)), 1);
	m_tableSize.y = std::max<int>((int)((m_delta.y) / (m_ySpacing)), 1);

	m_invTableSize.x = 1.0 / (double)m_tableSize.x;
	m_invTableSize.y = 1.0 / (double)m_tableSize.y;

	m_fact.x = m_delta.x * m_invTableSize.x;
	m_fact.y = m_delta.y * m_invTableSize.y;

	int nbIndexes = getNbIndexes();
	assert(nbIndexes > 0);

	delete[] m_indexes;
	m_indexes = new int[nbIndexes];
	assert(m_indexes);

	delete[] m_channelIndexes;
	m_channelIndexes = new int[nbIndexes];
	assert(m_channelIndexes);

	memset(m_indexes, -1, nbIndexes * sizeof(int));
	memset(m_channelIndexes, -1, nbIndexes * sizeof(int));

	const double invBeamSampleSpacing = 1.0 / m_pTransducer->getBeamsSamplesSpacing();

	double* px = new double[m_tableSize.x];
	double* px2 = new double[m_tableSize.x];
	for (int x = m_tableSize.x - 1; x >= 0; x--)
	{
		double fx = m_p1.x + (m_fact.x * (x + 0.5f));
		fx *= invBeamSampleSpacing;
		px[x] = fx;
		px2[x] = fx * fx;
	}

	// NMD : attention au probl�me d'overflow si y est tr�s grand, y*y d�passer la valeur d'un int..
	// double y2 = m_polarSize.y*m_polarSize.y;
	double y2 = pow(m_polarSize.y, 2);

	int nombreCh = this->m_pTransducer->m_numberOfSoftChannel;
	SoftChannel** softChannels = new SoftChannel*[nombreCh];

	for (int softChan = 0; softChan < nombreCh; softChan++)
	{
		softChannels[softChan] = this->m_pTransducer->getSoftChannelPolarX(softChan);
	}

	for (int y = m_tableSize.y - 1; y >= 0; y--)
	{
		int* p = (int*)getPointerToIndex(0, y);
		int* channelP = (int*)getPointerToChannelIndex(0, y);
		double dy = m_p1.y + (m_fact.y * (y + 0.5f));
		dy *= invBeamSampleSpacing;
		double dy2 = dy * dy;
		//double rollAdd = m_roll + m_beamsAngle;   //   depointage pris dans tableau ( d�plac� car d�pend de x)

		for (int x = m_tableSize.x - 1; x >= 0; x--)
		{
			double r2 = px2[x] + dy2;

			if (r2 < y2)
			{
				//	double fb = (atan2f(px[x], dy) + rollAdd) / m_beamsAngle;   //  remplacer par fonction recherche faisceau

				int b = FastNumFaisceau(atan2(px[x], dy), softChannels);
				if (b >= 0 && b < m_polarSize.x)
				{
					//p[x] = //r * m_polarSize.x + b;
					p[x] = (int)sqrt(r2) + b*m_polarSize.y;
					channelP[x] = b;
				}
			}
		}
	}

	delete[] softChannels;

	delete[] px2;
	delete[] px;

	m_mustBeComputed = false;
}

bool TransformMap::posIsIn(Vector2I polarPos)
{
	return (polarPos >= 0 && polarPos < m_polarSize);
}

int TransformMap::getNbIndexes(void)
{
	return m_tableSize.x*m_tableSize.y;
}

int* TransformMap::getPointerToIndex(unsigned int x, unsigned int y)
{
	//	assert(pos >= 0 && pos < m_tableSize);
	return m_indexes + y * m_tableSize.x + x;
}

int* TransformMap::getPointerToIndex()
{
	//	assert(pos >= 0 && pos < m_tableSize);
	return m_indexes;
}

int* TransformMap::getPointerToChannelIndex(unsigned int x, unsigned int y)
{
	//	assert(pos >= 0 && pos < m_tableSize);
	return m_channelIndexes + y * m_tableSize.x + x;
}

int* TransformMap::getPointerToChannelIndex()
{
	return m_channelIndexes;
}

int* TransformMap::getPointerToIndex(Vector2I pos)
{
	//	assert(pos >= 0 && pos < m_tableSize);
	return getPointerToIndex(pos.x, pos.y);
}

Vector2I TransformMap::getCartesianSize2(void)
{
	return m_tableSize;
}

Vector2D TransformMap::getRealOrigin2(void)
{
	return m_p1;
}

Vector2D TransformMap::getRealSize2(void)
{
	return m_p2 - m_p1;
}

double TransformMap::getXSpacing(void)
{
	return m_xSpacing;
}

double TransformMap::getYSpacing(void)
{
	return m_ySpacing;
}

Vector2D TransformMap::getDelta()
{
	return m_delta;
}


/******************************************************************************/
/*	Coord. Convertion Methods												  */
/*****************************************************************************/
Vector2I TransformMap::polarToCartesian2(const Vector2I &p)
{
	return realToCartesian2(polarToReal2(p));
}

Vector2D TransformMap::polarToReal2(const Vector2I &p)
{
	double angle;
	int nombreCh = m_pTransducer->m_numberOfSoftChannel;
	int faisceau = (int)p.x;
	if (faisceau >= 0 && faisceau < this->m_pTransducer->m_numberOfSoftChannel)
	{
		angle = this->m_pTransducer->getSoftChannelPolarX(faisceau)->m_mainBeamAthwartSteeringAngleRad;//-m_roll
	}
	else
		angle = 0;
	/*
	if((faisceau>=0)&&(faisceau<nombreCh-1))
	{
	angle=m_BeamsAngles[faisceau]+((p.x)-faisceau)*(m_BeamsAngles[faisceau]-m_BeamsAngles[faisceau+1]) - m_roll;
	}
	else
	{
	angle = p.x * m_beamsAngle - m_roll;  //  trouver valeur angle plus proche de valeur souhait�e
	}
	*/
	double radius = p.y * this->m_pTransducer->getBeamsSamplesSpacing();
	return Vector2D(sin(angle), cos(angle)) * radius;
}


Vector2D TransformMap::cartesianToPolar2(const Vector2D &c)
{
	return realToPolar2(cartesianToReal2(c));
}

Vector2D TransformMap::cartesianToReal2(const Vector2D &c)
{
	Vector2D cartSize(m_tableSize.x, m_tableSize.y);
	return m_p1 + (c * (m_p2 - m_p1)) / cartSize;
}

Vector2D TransformMap::realToPolar2(const Vector2D &r)
{
	//	double beam = (r.angle() + m_roll) / m_beamsAngle;    //  fonction recherche du faisceau (valeur la plus proche de l'angle)
	double beam = calcFaisceau(atan2(r.x, r.y));
	// IPSIS - FRE - 23/07/2009 correction calcul radius
	//double radius = r.squaredLength() / this->m_pTransducer->getBeamsSamplesSpacing();
	double radius = r.length() / this->m_pTransducer->getBeamsSamplesSpacing();
	return Vector2D(beam, radius);
}

Vector2I TransformMap::realToCartesian2(const Vector2D &r)
{
	Vector2D cartSize(getCartesianSize2().x, getCartesianSize2().y);
	Vector2D ret = (r - m_p1) * cartSize / getRealSize2();
	return Vector2I((int)ret.x, (int)ret.y);
}

Vector2D TransformMap::realToCartesian2Double(const Vector2D &r)
{
	Vector2D cartSize(getCartesianSize2().x, getCartesianSize2().y);
	return (r - m_p1) * cartSize / getRealSize2();
}

int TransformMap::NumFaisceau(double angl)
{
	int nombreCh = this->m_pTransducer->m_numberOfSoftChannel;
	SoftChannel * soft0 = this->m_pTransducer->getSoftChannelPolarX(0);
	if (angl < soft0->m_mainBeamAthwartSteeringAngleRad - soft0->m_beam3dBWidthAthwartRad / 2)
		return -1;

	//FRE - Disble result if angle if beyond last beam angle
	SoftChannel * softLast = this->m_pTransducer->getSoftChannelPolarX(nombreCh - 1);
	if (angl > softLast->m_mainBeamAthwartSteeringAngleRad + softLast->m_beam3dBWidthAthwartRad / 2)
		return -1;

	//	if(angl > m_BeamsAngles[nombreCh-1] + (m_BeamsWidth[nombreCh-1])
	//		return nombreCh;
	int faisceau = 0;
	double largeur;
	while (faisceau < nombreCh)
	{
		SoftChannel * soft = this->m_pTransducer->getSoftChannelPolarX(faisceau);
		if (faisceau < nombreCh - 1)
			largeur = (this->m_pTransducer->getSoftChannelPolarX(faisceau + 1)->m_mainBeamAthwartSteeringAngleRad - soft->m_mainBeamAthwartSteeringAngleRad) / 2;
		else
			//	largeur = (m_BeamsAngles[faisceau] - m_BeamsAngles[faisceau-1])/2;
			largeur = soft->m_beam3dBWidthAthwartRad / 2;


		if (angl < soft->m_mainBeamAthwartSteeringAngleRad + largeur)
		{
			return faisceau;
		}
		faisceau++;
	}
	return faisceau;
}

// OTK - 27/03/2009 - m�thode rapide pour optimiser le computetable
int TransformMap::FastNumFaisceau(double angl, SoftChannel ** tabChannel)
{
	int nombreCh = this->m_pTransducer->m_numberOfSoftChannel;
	SoftChannel * soft0 = tabChannel[0];
	if (angl < soft0->m_mainBeamAthwartSteeringAngleRad - soft0->m_beam3dBWidthAthwartRad / 2)
		return -1;

	//FRE - Disble result if angle if beyond last beam angle
	SoftChannel * softLast = tabChannel[nombreCh - 1];
	if (angl > softLast->m_mainBeamAthwartSteeringAngleRad + softLast->m_beam3dBWidthAthwartRad / 2)
		return -1;

	//	if(angl > m_BeamsAngles[nombreCh-1] + (m_BeamsWidth[nombreCh-1])
	//		return nombreCh;
	int faisceau = 0;
	double largeur;
	while (faisceau < nombreCh)
	{
		if (faisceau < nombreCh - 1)
			largeur = (tabChannel[faisceau + 1]->m_mainBeamAthwartSteeringAngleRad - tabChannel[faisceau]->m_mainBeamAthwartSteeringAngleRad) / 2;
		else
			//	largeur = (m_BeamsAngles[faisceau] - m_BeamsAngles[faisceau-1])/2;
			largeur = tabChannel[faisceau]->m_beam3dBWidthAthwartRad / 2;


		if (angl < tabChannel[faisceau]->m_mainBeamAthwartSteeringAngleRad + largeur)
		{
			return faisceau;
		}
		faisceau++;
	}
	return faisceau;
}

//  calcul du faisceau correspondant � un angle avec partie d�cimale pour ecart avec faisceau
double TransformMap::calcFaisceau(double angl)
{
	double faisceau = 0.0;
	int num = 0;
	int nombreCh = m_pTransducer->m_numberOfSoftChannel;
	// calcul angle plus proche
	if (nombreCh == 1)
		return 0;

	int entier = NumFaisceau(angl);

	faisceau = 1.0 * entier;

	if ((entier >= 0) && (entier < nombreCh))
	{
		double cetAngle = this->m_pTransducer->getSoftChannelPolarX(entier)->m_mainBeamAthwartSteeringAngleRad;

		// calcul rapport ecart / largeur faisceau

		double ecart = angl - cetAngle;

		// 3 cas ? debut faisceau 0
		// entre 2 faisceaux
		// entre d�but et fin dernier faisceau 
		if (((entier > 0) && (entier < nombreCh - 1)) || ((entier == 0) && (ecart > 0)) || ((entier == nombreCh - 1) && (ecart < 0)))
		{
			if (ecart > 0)
				faisceau += ecart / (this->m_pTransducer->getSoftChannelPolarX(entier + 1)->m_mainBeamAthwartSteeringAngleRad - this->m_pTransducer->getSoftChannelPolarX(entier)->m_mainBeamAthwartSteeringAngleRad);
			else
				faisceau += ecart / (this->m_pTransducer->getSoftChannelPolarX(entier)->m_mainBeamAthwartSteeringAngleRad - this->m_pTransducer->getSoftChannelPolarX(entier - 1)->m_mainBeamAthwartSteeringAngleRad);
		}
	}

	return faisceau;
}

/******************************************************************************/
/*	Shape Methods															  */
/******************************************************************************/
int TransformMap::getMinX(int y)
{
	Vector2I p;
	p.y = y;
	for (p.x = 0; p.x < m_tableSize.x; p.x++)
	{
		if (getIndex(p) >= 0)
			break;
	}
	return p.x;
}

int TransformMap::getMaxX(int y)
{
	Vector2I p;
	p.y = y;
	for (p.x = m_tableSize.x - 1; p.x >= 0; p.x--)
	{
		if (getIndex(p) >= 0)
			break;
	}
	return p.x;
}

int TransformMap::getMinY(int x)
{
	Vector2I p;
	p.x = x;
	for (p.y = 0; p.y < m_tableSize.y; p.y++)
	{
		if (getIndex(p) >= 0)
			break;
	}
	return p.y;
}

int TransformMap::getMaxY(int x)
{
	Vector2I p;
	p.x = x;
	for (p.y = m_tableSize.y - 1; p.y >= 0; p.y--)
	{
		if (getIndex(p) >= 0)
			break;
	}
	return p.y;
}

/******************************************************************************/
/*	Transformation															  */
/******************************************************************************/
/*void
TransformMap::transformeArray2D(UCharArray2D* result,
UCharArray2D& source,
DataFmt outside)
{
transformeArray2D(result, source, 0, m_tableSize, outside);
}



void
TransformMap::transformeArray2D(UCharArray2D* result,
UCharArray2D& source,
Vector2I pos, Vector2I size,
DataFmt outside)
{
assert(source.getSize() == m_polarSize);
assert(pos >= 0 && size >= 0 && pos + size <= m_tableSize);
assert(result->getSize() == size);

// parcours les lignes
for(int y = size.y - 1; y >= 0; y--)
{
Vector2I p(0, y);

transformeLine2(
result->getPointerToPixel(p),
source.getPointerToPixel(0),
getPointerToIndex(pos + p),
size.x,
source.getDataSize(),
outside);
}
}
*/

void TransformMap::transformeLine2(unsigned char* dst,
	unsigned char* src,
	int * p,
	unsigned int sx,
	unsigned int dataSize,
	DataFmt outside)
{
	int x;
	for (x = sx - 1; p[x] < 0; x--) // contour droit du multi-faisceaux
	{
		memcpy(&(dst[x*dataSize]), &outside, dataSize);
		if (x == 0)
			return;
	}

	for (; *p < 0; x--) // contour gauche du multi-faisceaux
	{
		memcpy(dst, &outside, dataSize);
		dst += dataSize;
		p++;
	}

	p += x;

	for (; x >= 0; x--) // echos des faisceaux pour la ligne donnee
	{
		memcpy(&(dst[x*dataSize]), &(src[(*p)*dataSize]), dataSize);
		p--;
	}
}


/******************************************************************************/
/*	Private Methods															  */
/******************************************************************************/
double TransformMap::getTotalAngle(void)
{
	return this->m_pTransducer->getSoftChannelPolarX(m_polarSize.x - 1)->m_mainBeamAthwartSteeringAngleRad 
		- this->m_pTransducer->getSoftChannelPolarX(0)->m_mainBeamAthwartSteeringAngleRad 
		+ this->m_pTransducer->getSoftChannelPolarX(0)->m_beam3dBWidthAthwartRad / 2 
		+ this->m_pTransducer->getSoftChannelPolarX(m_polarSize.x - 1)->m_beam3dBWidthAthwartRad / 2;

	//	return m_beamsAngle * m_polarSize.x;    //   min steering - max steering +  2 moiti�s largeurs faisceaux

}

double TransformMap::getAngle1(void)
{
	return this->m_pTransducer->getSoftChannelPolarX(0)->m_mainBeamAthwartSteeringAngleRad 
		- this->m_pTransducer->getSoftChannelPolarX(0)->m_beam3dBWidthAthwartRad / 2;
}

double TransformMap::getAngle2(void)
{
	/*
	// Semble �tre �quivalent au code suivant :
	return this->m_pTransducer->getSoftChannelPolarX(m_polarSize.x - 1)->m_mainBeamAthwartSteeringAngleRad
		+ this->m_pTransducer->getSoftChannelPolarX(m_polarSize.x - 1)->m_beam3dBWidthAthwartRad / 2;
	*/
	return getAngle1() + getTotalAngle();
}

double TransformMap::getRadius(void)
{
	return m_polarSize.y * m_pTransducer->getBeamsSamplesSpacing();
}