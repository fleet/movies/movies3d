#include "M3DKernel/datascheme/TransformSet.h"

TransformSet::TransformSet(void)
{
	m_pSounder = NULL;
	m_maxRange = 100;
	m_maxEchoNb = 0;
}

TransformSet::~TransformSet(void)
{
}
TransformMap* TransformSet::GetTransformMap(unsigned int Index)
{
	return m_IndexedMap.GetObject(Index);
}
void TransformSet::SetMaxRange(double maxDepth, unsigned int maxEchoNb)
{
	if (maxDepth != m_maxRange || m_maxEchoNb != maxEchoNb)
	{
		for (unsigned int i = 0; i < m_IndexedMap.GetObjectCount(); i++)
		{
			m_IndexedMap.GetObject(i)->SetMaxRange(maxDepth);
		}
		m_maxRange = maxDepth;
		m_maxEchoNb = maxEchoNb;
	}
}
void TransformSet::CheckReaderParameter()
{
	for (unsigned int i = 0; i < m_IndexedMap.GetObjectCount(); i++)
	{
		m_IndexedMap.GetObject(i)->CheckReaderParameter();
	}
}
void TransformSet::SetSounder(Sounder *p)
{
	m_pSounder = p;

	this->m_IndexedMap.SetSounder(p);
}

