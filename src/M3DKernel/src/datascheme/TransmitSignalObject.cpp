#include "M3DKernel/datascheme/TransmitSignalObject.h"

#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/SignalFilteringObject.h"
#include "M3DKernel/DefConstants.h"
#include "M3DKernel/utils/mathutils.h"

#include "BaseMathLib/SignalUtils.h"

#include <algorithm>
#include <float.h>

namespace {
	const constexpr double SAMPLE_FREQUENCY = 1.5e6;
}

TransmitSignalObject::TransmitSignalObject()
{
}

void TransmitSignalObject::ComputeSignals(Transducer * const transducer, SoftChannel * const channel) 
{
	m_Signal.clear();

	double pulseLength = ((double)transducer->m_pulseDuration) / 1000000;
	int nSamples = (int)floor(pulseLength * SAMPLE_FREQUENCY);
	int nShapingSamples = (int)floor(transducer->m_pulseSlope*nSamples);
	const std::vector<float> windowFunction = hanning(2 * nShapingSamples);

	std::vector<std::complex<float>> transmitSignal(nSamples, 1);
	if (nShapingSamples > 0) {
		for (int i = 0; i < nShapingSamples; ++i) {
			transmitSignal[i].real(windowFunction[i]);
		}

		for (int i = 0; i < nShapingSamples; ++i) {
			transmitSignal[nSamples - nShapingSamples + i].real(windowFunction[nShapingSamples + i]);
		}
	}

	// Chirp
	double beta = (channel->m_endFrequency - channel->m_startFrequency) / pulseLength;
	double t;
	for (int i = 0; i < nSamples; i++) {
		t = i / SAMPLE_FREQUENCY;
		transmitSignal[i] *= (float)cos(2 * PI * (beta / 2 * t * t + channel->m_startFrequency * t));
	}

	// Filtre FPGA
	MovObjectPtr<SignalFilteringObject> pFPGAFilter = transducer->GetFPGAFilter();
	if (pFPGAFilter != nullptr) {
		if (pFPGAFilter->m_DecimationFactor == 0) {
			pFPGAFilter = nullptr;
		}
	}

	if (pFPGAFilter == nullptr) {
		pFPGAFilter = SignalFilteringObject::Create();
		pFPGAFilter->m_DecimationFactor = 1;
	}

	std::vector<std::complex<float>> filter1Res;
	conv2Complex(transmitSignal, pFPGAFilter->m_Coefficients, filter1Res);

	// decimation
	std::vector<std::complex<float>> filter2In;
	filter2In.reserve(filter1Res.size() / pFPGAFilter->m_DecimationFactor + 1);
	for (size_t i = 0; i < filter1Res.size(); i += pFPGAFilter->m_DecimationFactor) {
		filter2In.push_back(filter1Res[i]);
	}

	// Filtre Application EK80
	MovObjectPtr<SignalFilteringObject> pApplicationFilter = transducer->GetApplicationFilter();
	if (pApplicationFilter != nullptr) {
		if (pApplicationFilter->m_DecimationFactor == 0) {
			pApplicationFilter = nullptr;
		}
	}
	if (pApplicationFilter == nullptr) {
		pApplicationFilter = SignalFilteringObject::Create();
		pApplicationFilter->m_DecimationFactor = 1;
	}

	std::vector<std::complex<float>> filter2Res;
	conv2Complex(filter2In, pApplicationFilter->m_Coefficients, filter2Res);

	// decimation
	m_Signal.reserve(filter2Res.size() / pApplicationFilter->m_DecimationFactor + 1);
	for (size_t i = 0; i < filter2Res.size(); i += pApplicationFilter->m_DecimationFactor) {
		m_Signal.push_back(filter2Res[i]);
	}
}

void TransmitSignalObject::ComputeFMSignals() {
	// Calcul de l'�quivalent de flipud(conj(transmitSignal)) et norm(transmitSignal)^2
	// du code matlab une fois pour toutes ici
	m_SignalConjugate.resize(m_Signal.size());
	m_SignalSquaredNorm = 0;

	auto * pDest = &m_SignalConjugate[0];
	auto * pOrig = &m_Signal[m_Signal.size() - 1];
	for (size_t i = 0; i < m_Signal.size(); i++) {
		*pDest = std::conj(*pOrig--);
		m_SignalSquaredNorm += std::norm(*pDest);
		pDest++;
	}
}

void TransmitSignalObject::ComputeEffectivePulseLength(Transducer * const transducer) {

	if (transducer->m_pulseShape == 2) {
		// FM mode
		std::vector<std::complex<float>> autoCorrRes;
		conv2Complex(m_Signal, m_SignalConjugate, autoCorrRes);
		float maxTransmitSignalPower = -std::numeric_limits<float>::max();
		float transmitSignalPowerSum = 0;
		float dbTmp;
		for (size_t i = 0; i < autoCorrRes.size(); i++) {
			dbTmp = pow(autoCorrRes[i].real() / m_SignalSquaredNorm, 2.0f) + pow(autoCorrRes[i].imag() / m_SignalSquaredNorm, 2.0f);
			transmitSignalPowerSum += dbTmp;
			maxTransmitSignalPower = std::max<float>(maxTransmitSignalPower, dbTmp);
		}

		m_EffectivePulseLength = transducer->m_timeSampleInterval / 1000000.0 * transmitSignalPowerSum / maxTransmitSignalPower;
	}
	else {
		// CW mode
		float maxTransmitSignalPower = -std::numeric_limits<float>::max();
		float transmitSignalPowerSum = 0;
		float dbTmp;
		for (size_t i = 0; i < m_Signal.size(); i++) {
			dbTmp = pow(m_Signal[i].real(), 2.0f) + pow(m_Signal[i].imag(), 2.0f);
			transmitSignalPowerSum += dbTmp;
			maxTransmitSignalPower = std::max<float>(maxTransmitSignalPower, dbTmp);
		}

		m_EffectivePulseLength = (1 / SAMPLE_FREQUENCY) 
			* transducer->GetFPGAFilter()->m_DecimationFactor /* TODO la valeur lors d'une lecture NetCDF n'est pas issue du fichier lu mais construite � partir du type de transducteur */
			* transducer->GetApplicationFilter()->m_DecimationFactor /* TODO la valeur lors d'une lecture NetCDF n'est pas issue du fichier lu mais construite � partir du type de transducteur */
			* transmitSignalPowerSum / maxTransmitSignalPower;
	}
}

TransmitSignalObject::~TransmitSignalObject()
{
}

void TransmitSignalObject::InitSignals(const std::vector<float>& signalReal, const std::vector<float>& signalImag) 
{
	const int nreal = signalReal.size();
	const int nimag = signalImag.size();

	if (nreal == nimag)
	{
		m_Signal.resize(nreal);
		for (int i = 0; i < nreal; ++i)
		{
			m_Signal[i] = std::complex<float>(signalReal[i], signalImag[i]);
		}
	}
	else
	{
		if (nimag < nreal)
		{
			m_Signal.resize(nreal);
			for (int i = 0; i < nimag; ++i)
			{
				m_Signal[i] = std::complex<float>(signalReal[i], signalImag[i]);
			}

			for (int i = nimag; i < nreal; ++i)
			{
				m_Signal[i] = std::complex<float>(signalReal[i], 0.0f);
			}
		}
		else
		{
			m_Signal.resize(nimag);
			for (int i = 0; i < nreal; ++i)
			{
				m_Signal[i] = std::complex<float>(signalReal[i], signalImag[i]);
			}

			for (int i = nimag; i < nimag; ++i)
			{
				m_Signal[i] = std::complex<float>(0.0f, signalImag[i]);
			}
		}
	}
}

void TransmitSignalObject::Compute(Transducer * transducer, SoftChannel * channel, const bool& recomputeSignals)
{
	m_SamplingFrequency = 1 / (transducer->m_timeSampleInterval / 1000000.0);

	// Signals
	if (recomputeSignals || m_Signal.empty()) {
		ComputeSignals(transducer, channel);
	}

	// FM Signals
	if (transducer->m_pulseShape == 2) {
		ComputeFMSignals();
	}

	// Effective Pulse Length
	ComputeEffectivePulseLength(transducer);
}

double TransmitSignalObject::GetEffectivePulseLength() const
{
	return m_EffectivePulseLength;
}

double TransmitSignalObject::GetSignalSquaredNorm()
{
	return m_SignalSquaredNorm;
}

bool TransmitSignalObject::IsValid() const {
	return !std::isnan(GetEffectivePulseLength()) && !MathUtils::floatingPointEquals(GetEffectivePulseLength(), 0.0);
}

void TransmitSignalObject::PerformPulseCompression(std::vector<std::complex<float>> & complexSamples, float freqResolution, int nbQuadrants)
{
	pulseCompression(m_Signal, complexSamples, m_SignalSquaredNorm, m_SamplingFrequency, freqResolution, nbQuadrants);
}

std::vector<std::complex<float>> TransmitSignalObject::GetFilt() const
{
	const int size = m_SignalConjugate.size();
	
	const double invNorm = 1.0 / m_SignalSquaredNorm;
	std::vector<std::complex<float>> filt = m_SignalConjugate;
	for (int i = 0; i < size; ++i)
	{
		filt[i] *= invNorm;
	}

	return filt;
}