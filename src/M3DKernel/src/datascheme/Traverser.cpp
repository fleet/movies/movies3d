#include "M3DKernel/datascheme/Traverser.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/algorithm/base/EchoAlgorithmRoot.h"
#include "M3DKernel/M3DKernel.h"

namespace
{
	constexpr const char * LoggerName = "M3DKernel.datascheme.Traverser";
}

Traverser::Traverser()
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	m_pHacObjectMgr = pKernel->getObjectMgr();

	m_pEchoAlgorithmRoot = pKernel->getEchoAlgorithmRoot();
	m_pInternalAlgorithmRoot = pKernel->getInternalAlgorithmRoot();
	m_pWriteAlgorithmRoot = pKernel->getWriteAlgorithmRoot();

	MovRef(m_pHacObjectMgr);
	MovRef(m_pEchoAlgorithmRoot);
	MovRef(m_pInternalAlgorithmRoot);
	MovRef(m_pWriteAlgorithmRoot);
}

Traverser::~Traverser()
{
	MovUnRefDelete(m_pHacObjectMgr);
	MovUnRefDelete(m_pEchoAlgorithmRoot);
	MovUnRefDelete(m_pWriteAlgorithmRoot);
	MovUnRefDelete(m_pInternalAlgorithmRoot);
}

void Traverser::PingFanAdded(PingFan *pFan)
{
	m_UpdateEvent.m_FrameAdded = true;
	m_pInternalAlgorithmRoot->ProcessPingFanAdded(pFan);
	m_pEchoAlgorithmRoot->ProcessPingFanAdded(pFan);
	m_pWriteAlgorithmRoot->ProcessPingFanAdded(pFan);
}

void Traverser::SounderChanged(std::uint32_t sounderId)
{
	m_UpdateEvent.m_SequenceChanged = true;
	m_pInternalAlgorithmRoot->ProcessSounderChanged(sounderId);
	m_pEchoAlgorithmRoot->ProcessSounderChanged(sounderId);
	m_pWriteAlgorithmRoot->ProcessSounderChanged(sounderId);
}

void Traverser::SingleTargetAdded(std::uint32_t sounderId)
{
	m_pInternalAlgorithmRoot->ProcessSingleTargetAdded(sounderId);
	m_pEchoAlgorithmRoot->ProcessSingleTargetAdded(sounderId);
	m_pWriteAlgorithmRoot->ProcessSingleTargetAdded(sounderId);
}

void Traverser::StreamOpened(const char *streamName)
{
	//m_UpdateEvent.m_StreamChanged=true;
	m_pInternalAlgorithmRoot->ProcessStreamOpened(streamName);
	m_pEchoAlgorithmRoot->ProcessStreamOpened(streamName);
	m_pWriteAlgorithmRoot->ProcessStreamOpened(streamName);
}

void Traverser::StreamClosed(const char *streamName)
{
	//m_UpdateEvent.m_StreamChanged=true;
	m_pInternalAlgorithmRoot->ProcessStreamClosed(streamName);
	m_pEchoAlgorithmRoot->ProcessStreamClosed(streamName);
	m_pWriteAlgorithmRoot->ProcessStreamClosed(streamName);
}

void Traverser::TupleHeaderUpdate()
{
	m_pInternalAlgorithmRoot->ProcessTupleHeaderUpdate();
	m_pEchoAlgorithmRoot->ProcessTupleHeaderUpdate();
	m_pWriteAlgorithmRoot->ProcessTupleHeaderUpdate();
}

// IPSIS - OTK - ajout �v�nements ESUStart et ESUEnd
void Traverser::ESUStarted(ESUParameter * pWorkingESU)
{
	M3D_LOG_INFO(LoggerName, "New ESU Started...");
	m_pInternalAlgorithmRoot->ProcessESUStarted(pWorkingESU);
	m_pEchoAlgorithmRoot->ProcessESUStarted(pWorkingESU);
	m_pWriteAlgorithmRoot->ProcessESUStarted(pWorkingESU);
}

// IPSIS - OTK - ajout �v�nements ESUStart et ESUEnd
void Traverser::ESUEnded(ESUParameter * pWorkingESU, bool abort)
{
	M3D_LOG_INFO(LoggerName, "ESU Ended.");
	m_pInternalAlgorithmRoot->ProcessESUEnded(pWorkingESU, abort);
	m_pEchoAlgorithmRoot->ProcessESUEnded(pWorkingESU, abort);
	m_pWriteAlgorithmRoot->ProcessESUEnded(pWorkingESU, abort);
}
