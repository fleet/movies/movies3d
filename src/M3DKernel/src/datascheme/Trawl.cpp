/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	S. TOGNI													  */
/*	File:		 HACTrawl.cpp										  */
/******************************************************************************/
#include "M3DKernel/datascheme/Trawl.h"
#include <algorithm>
#include <math.h>

#include "M3DKernel/DefConstants.h"
#include "M3DKernel/M3DKernel.h"

TrawlPositionAttitude::TrawlPositionAttitude()
{
	m_HeadRopeAlt = 0;
	m_PortAlongShipAngle = 0;
	m_PortArthwartAngle = 0;
	m_VOpen = 10;
	m_HOpen = 10;
	m_HOpenDoor = 10;
}



/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/
Trawl::Trawl() : MovComputeObject()
{
	//	m_absoluteOffset=Float3(0,0,-10);
	m_pTrawlPositionAttitudeContainer = TimeObjectContainer::Create();
	MovRef(m_pTrawlPositionAttitudeContainer);
}

Trawl::~Trawl()
{
	MovUnRefDelete(m_pTrawlPositionAttitudeContainer);
	{
		MapTrawlPlatformPosition::iterator itr = m_TrawlPlatformPosition.begin();
		while (itr != m_TrawlPlatformPosition.end())
		{
			if (itr->second != NULL)
				MovUnRefDelete(itr->second);
			itr++;
		}
		m_TrawlPlatformPosition.clear();

	}
	{
		MapTrawlPlatformPosition::iterator itr = m_DynamicPlatformPosition.begin();
		while (itr != m_DynamicPlatformPosition.end())
		{
			if (itr->second != NULL)
				MovUnRefDelete(itr->second);
			itr++;
		}
		m_DynamicPlatformPosition.clear();

	}
}

void Trawl::RemoveOldObject(HacTime &oldestDate)
{
	m_pTrawlPositionAttitudeContainer->RemoveOldObject(oldestDate);
	MapTrawlPlatformPosition::iterator itr = m_DynamicPlatformPosition.begin();
	while (itr != m_DynamicPlatformPosition.end())
	{
		if (itr->second != NULL)
			itr->second->RemoveOldObject(oldestDate);
		itr++;
	}
	itr = m_TrawlPlatformPosition.begin();
	while (itr != m_TrawlPlatformPosition.end())
	{
		if (itr->second != NULL)
			itr->second->RemoveOldObject(oldestDate);
		itr++;
	}
	return;
}

/*
void Trawl::ComputeQuadCoord()
{
// calcul les coordonn�es r�elles de 4 points correspondant � l'entr�e du chalut.

  m_Center=m_absoluteOffset;
  m_Center.y=m_PositionAttitude.getPosition();
  m_Open.x=m_PositionAttitude.getHOpen();
  m_Open.y=m_PositionAttitude.getVOpen();

	// check for default values

	  /// ie magouille pour pallier les defaults capteur
	  if(m_Open.x==0)
	  m_Open.x=50;
	  if(m_Open.y==0)
	  m_Open.y=20;

		if(m_Center.z==0)
		m_Center.z=-50;
		m_Center.y+=m_Open.y/2.0f;

		  m_Center.z=-cos(DEG_TO_RAD(30))*m_PositionAttitude.getPosition()-176.0f;


			  }
*/

TimeObjectContainer *Trawl::GetPlatformPositionContainer(TrawlSensorPositionId attitudeSensorId)
{
	MapTrawlPlatformPosition::iterator res = m_TrawlPlatformPosition.find(attitudeSensorId);

	if (res != m_TrawlPlatformPosition.end())
	{
		return res->second;
	}
	TimeObjectContainer *pObj = TimeObjectContainer::Create();
	MovRef(pObj);
	m_TrawlPlatformPosition.insert(MapTrawlPlatformPosition::value_type(attitudeSensorId, pObj));
	m_TrawlPlatformPositionSensorId.push_back(attitudeSensorId);
	return pObj;
}
TimeObjectContainer *Trawl::GetDynamicPlatformPositionContainer(TrawlSensorPositionId attitudeSensorId)
{
	MapTrawlPlatformPosition::iterator res = m_DynamicPlatformPosition.find(attitudeSensorId);

	if (res != m_DynamicPlatformPosition.end())
	{
		return res->second;
	}
	TimeObjectContainer *pObj = TimeObjectContainer::Create();
	MovRef(pObj);
	m_DynamicPlatformPosition.insert(MapTrawlPlatformPosition::value_type(attitudeSensorId, pObj));
	m_DynamicPlatformPositionSensorId.push_back(attitudeSensorId);

	return pObj;
}



TrawlSensorPositionId			Trawl::GetDynamicPlatformPositionSensorId(unsigned int index)
{
	TrawlSensorPositionId defaultRet;
	if (index < GetNumberOfDynamicPlatformPositionSensorId())
		return m_DynamicPlatformPositionSensorId[index];
	return defaultRet;
}


unsigned int			Trawl::GetNumberOfDynamicPlatformPositionSensorId()
{
	return m_DynamicPlatformPositionSensorId.size();
}

TrawlSensorPositionId			Trawl::GetPlatformPositionSensorId(unsigned int index)
{
	TrawlSensorPositionId defaultRet;
	if (index < GetNumberOfPlatformPositionSensorId())
		return m_TrawlPlatformPositionSensorId[index];
	return defaultRet;
}

unsigned int			Trawl::GetNumberOfPlatformPositionSensorId()
{
	return m_TrawlPlatformPositionSensorId.size();
}
