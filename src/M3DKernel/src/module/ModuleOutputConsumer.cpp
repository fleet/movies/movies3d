#include "M3DKernel/module/ModuleOutputConsumer.h"
#include "M3DKernel/module/ModuleOutput.h"
#include <assert.h>

ModuleOutputConsumer::ModuleOutputConsumer(ModuleOutputContainer &ref) : m_refContainer(ref)
{
	this->m_consumerId = m_refContainer.ReserveConsumerId();
}

ModuleOutputConsumer::~ModuleOutputConsumer(void)
{
	m_refContainer.RemoveConsumer(this->m_consumerId);
}

ModuleOutput* ModuleOutputConsumer::GetOutPut(unsigned int i)
{
	return m_refContainer.GetOutPut(m_consumerId, i);
}
unsigned int ModuleOutputConsumer::GetOutPutCount()
{
	return m_refContainer.GetOutPutCount(m_consumerId);
}
void ModuleOutputConsumer::Flush()
{
	m_refContainer.Flush(m_consumerId);
}
