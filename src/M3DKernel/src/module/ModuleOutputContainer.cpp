#include "M3DKernel/module/ModuleOutputContainer.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/DefConstants.h"

namespace
{
	constexpr const char * LoggerName = "M3DKernel.module.ModuleOutputContainer";
}

ModuleOutputContainer::ModuleOutputContainer(void)
{
}

ModuleOutputContainer::~ModuleOutputContainer(void)
{
	Lock();
	IndexConsumer::iterator res = m_mapConsumerIndex.begin();
	while (res != m_mapConsumerIndex.end())
	{
		Flush(res->first);
		res++;
	}
	m_mapConsumerIndex.clear();
	Unlock();
}
unsigned int	ModuleOutputContainer::ReserveConsumerId()
{
	tConsumerId avalaibleId = 1;
	IndexConsumer::iterator res = m_mapConsumerIndex.find(avalaibleId);
	while (res != m_mapConsumerIndex.end())
	{
		avalaibleId++;
		res = m_mapConsumerIndex.find(avalaibleId);
	}
	m_mapConsumerIndex.insert(IndexConsumer::value_type(avalaibleId, 0));
	return avalaibleId;
}
void ModuleOutputContainer::RemoveConsumer(tConsumerId a)
{
	IndexConsumer::iterator res = m_mapConsumerIndex.find(a);
	if (res != m_mapConsumerIndex.end())
	{
		m_mapConsumerIndex.erase(res);
	}
	CleanUp();
}


void ModuleOutputContainer::AddOutput(ModuleOutput *a)
{
	if (m_mapConsumerIndex.size() > 0)
	{
		MovRef(a);
		m_moduleOutputList.push_back(a);
	}
}

void ModuleOutputContainer::Flush(tConsumerId aConsumerId)
{
	IndexConsumer::iterator res = m_mapConsumerIndex.find(aConsumerId);
	if (res != m_mapConsumerIndex.end())
	{
		res->second = m_moduleOutputList.size();
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "Unknown consumer index");
	}
	CleanUp();
}
unsigned int ModuleOutputContainer::GetOutPutCount(const tConsumerId aConsumerId) const
{
	unsigned int index;
	if (GetIndexForConsumer(aConsumerId, index))
	{
		return static_cast<unsigned int>(m_moduleOutputList.size()) - index;
	}
	
	return 0;
}
ModuleOutput* ModuleOutputContainer::GetOutPut(const tConsumerId aConsumerId, const unsigned int i) const
{
	unsigned int index;
	if (GetIndexForConsumer(aConsumerId, index) && m_moduleOutputList.size() > index)
	{
		return m_moduleOutputList[i + index];
	}
	
	return nullptr;
}

const std::vector<ModuleOutput*>& ModuleOutputContainer::getOutputs() const
{
	return m_moduleOutputList;
}

std::vector<ModuleOutput*> ModuleOutputContainer::getOutputs(const tConsumerId consumerId) const
{
	std::vector<ModuleOutput*> results;
	const auto count = GetOutPutCount(consumerId);
	
	for (unsigned int i = 0; i < count; ++i)
	{
		results.push_back(GetOutPut(consumerId, i));		
	}
	
	return results;
}

bool ModuleOutputContainer::GetIndexForConsumer(const tConsumerId a, unsigned int &index) const
{
	const auto res = m_mapConsumerIndex.find(a);
	if (res != m_mapConsumerIndex.end())
	{
		assert(res->second <= m_moduleOutputList.size());
		index = res->second;
		return true;
	}
	M3D_LOG_ERROR(LoggerName, "Not Registered consumer");
	return false;
}

void ModuleOutputContainer::CleanUp()
{
	// get min index
	unsigned int minIndex = 0;
	bool firstIndex = true;
	IndexConsumer::iterator res = m_mapConsumerIndex.begin();
	while (res != m_mapConsumerIndex.end())
	{
		if (firstIndex)
		{
			minIndex = res->second;
		}
		else
		{
			minIndex = MIN(minIndex, res->second);
		}
		res++;
	}

	for (unsigned int i = 0; i < minIndex; i++)
	{
		ModuleOutput *p = *(m_moduleOutputList.begin());
		MovUnRefDelete(p);
		m_moduleOutputList.erase(m_moduleOutputList.begin());

	}
	// shift remaining index;
	if (minIndex > 0)
	{
		IndexConsumer::iterator res = m_mapConsumerIndex.begin();
		while (res != m_mapConsumerIndex.end())
		{
			//IPSIS - FRE - correct capacity bottom overflow for unsigned int
			//res->second=MAX(res->second-minIndex,0);
			res->second = (res->second > minIndex) ? res->second - minIndex : 0;
			res++;
		}
	}

}