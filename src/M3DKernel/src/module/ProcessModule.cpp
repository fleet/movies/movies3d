#include "M3DKernel/module/ProcessModule.h"

ProcessModule::ProcessModule(const std::string & name)
	: EchoAlgorithm(name)
	, m_pMatlabConsumer(nullptr)
	, m_pOutputMgrConsumer(nullptr)
{
}

ProcessModule::~ProcessModule()
{
	if (m_pMatlabConsumer)
	{
		GetOutputContainer().RemoveConsumer(m_pMatlabConsumer->GetConsumerId());
		delete (m_pMatlabConsumer);
	}

	if (m_pOutputMgrConsumer)
	{
		GetOutputContainer().RemoveConsumer(m_pOutputMgrConsumer->GetConsumerId());
		delete (m_pOutputMgrConsumer);
	}
}

ModuleOutputConsumer * ProcessModule::GetMatlabModuleConsumer()
{
	if (!m_pMatlabConsumer)
	{
		m_pMatlabConsumer = new ModuleOutputConsumer(GetOutputContainer());
	}
	return m_pMatlabConsumer;
}

ModuleOutputConsumer*	ProcessModule::GetOutputMgrConsumer()
{
	if (!m_pOutputMgrConsumer)
	{
		m_pOutputMgrConsumer = new ModuleOutputConsumer(GetOutputContainer());
	}
	return m_pOutputMgrConsumer;
}
