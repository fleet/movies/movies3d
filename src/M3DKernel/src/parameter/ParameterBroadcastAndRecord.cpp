#include "M3DKernel/parameter/ParameterBroadcastAndRecord.h"

// d�pendances
#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

ParameterBroadcastAndRecord::ParameterBroadcastAndRecord(void)
{
	// Initialisation valeurs par d�faut
	m_tramaType = CSV_TRAMA;
	m_enableFileOutput = true;
	m_enableNetworkOutput = false;
	m_filePrefix = "results";
	m_filePath = "C:\\";
	m_cutType = SIZE_CUT;
	m_fileMaxSize = 5;
	m_fileMaxTime = 3600;
	m_prefixDate = true;
}

ParameterBroadcastAndRecord::ParameterBroadcastAndRecord(DictionnaryType type)
	: m_tramaDescriptor(type)
{
	// Initialisation valeurs par d�faut
	m_tramaType = CSV_TRAMA;
	m_enableFileOutput = true;
	m_enableNetworkOutput = false;
	m_filePrefix = "results";
	m_filePath = "C:\\";
	m_cutType = SIZE_CUT;
	m_fileMaxSize = 5;
	m_fileMaxTime = 3600;
	m_prefixDate = true;
}

ParameterBroadcastAndRecord::~ParameterBroadcastAndRecord(void)
{
}


bool ParameterBroadcastAndRecord::Serialize(MovConfig * movConfig)
{
	// D�but de la s�rialisation des param�tres du ParameterObject
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "BroadcastAndRecord");

	movConfig->SerializeData<bool>(m_enableFileOutput, eBool, "EnableFileOutput");
	movConfig->SerializeData<bool>(m_enableNetworkOutput, eBool, "EnableNetworkOutput");
	movConfig->SerializeData<unsigned int>(m_tramaType, eUInt, "TramaFormat");

	movConfig->SerializeData<unsigned int>(m_cutType, eUInt, "FileCutStrategy");
	movConfig->SerializeData<unsigned int>(m_fileMaxSize, eUInt, "FileMaxSize");
	movConfig->SerializeData<unsigned int>(m_fileMaxTime, eInt, "FileMaxTime");
	movConfig->SerializeData<std::string>(m_filePrefix, eString, "FilePrefix");
	movConfig->SerializeData<std::string>(m_filePath, eString, "FilePath");

	movConfig->SerializeData<bool>(m_prefixDate, eBool, "DatePrefix");

	// Descripteur des trames a archiver
	m_tramaDescriptor.Serialize(movConfig);

	movConfig->SerializePushBack();

	return true;
}


bool ParameterBroadcastAndRecord::DeSerialize(MovConfig * movConfig)
{
	bool result = true;

	// D�but de la d�s�rialisation des param�tres du ParameterObject
	result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "BroadcastAndRecord");

	result = result && movConfig->DeSerializeData<bool>(&m_enableFileOutput, eBool, "EnableFileOutput");
	result = result && movConfig->DeSerializeData<bool>(&m_enableNetworkOutput, eBool, "EnableNetworkOutput");
	unsigned int cutType, tramaType;
	result = result && movConfig->DeSerializeData<unsigned int>(&tramaType, eUInt, "TramaFormat");

	result = result && movConfig->DeSerializeData<unsigned int>(&cutType, eUInt, "FileCutStrategy");
	m_cutType = (TCutType)cutType;
	m_tramaType = (TTramaType)tramaType;
	result = result && movConfig->DeSerializeData<unsigned int>(&m_fileMaxSize, eUInt, "FileMaxSize");
	result = result && movConfig->DeSerializeData<unsigned int>(&m_fileMaxTime, eUInt, "FileMaxTime");
	result = result && movConfig->DeSerializeData<std::string>(&m_filePrefix, eString, "FilePrefix");
	result = result && movConfig->DeSerializeData<std::string>(&m_filePath, eString, "FilePath");

	result = result && movConfig->DeSerializeData<bool>(&m_prefixDate, eBool, "DatePrefix");

	// Descripteur des trames a archiver
	result = result && m_tramaDescriptor.DeSerialize(movConfig);

	movConfig->DeSerializePushBack();

	return result;
}