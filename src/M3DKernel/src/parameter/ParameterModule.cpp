#include "M3DKernel/parameter/ParameterModule.h"

using namespace BaseKernel;

ParameterModule::ParameterModule(const char *aName) 
	: ParameterSerializable()
	, m_Name("")
{
	SetName(aName);
}

ParameterModule::ParameterModule(const char *aName, DictionnaryType type)
	: ParameterSerializable(),
	m_Name(""),
	m_ParameterBroadcastAndRecord(type)
{
	SetName(aName);
}

ParameterModule::~ParameterModule(void)
{
}
