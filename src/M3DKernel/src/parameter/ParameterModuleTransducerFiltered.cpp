﻿#include "M3DKernel/parameter/ParameterModuleTransducerFiltered.h"

#include <numeric>

#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

namespace
{
    const auto ENABLE_TRANSDUCERS_FILTER_KEY = "EnableTransducerFilter";
    const auto ACTIVE_TRANSDUCERS_KEY = "ActiveTransducerNames";

    const auto ACTIVE_TRANSDUCERS_DELIMITER = ";";
}

ParameterModuleTransducerFiltered::ParameterModuleTransducerFiltered(const char* aName)
    : ParameterModule(aName), m_enableTransducerFilter(false)
{
}

ParameterModuleTransducerFiltered::ParameterModuleTransducerFiltered(const char* aName, const DictionnaryType type)
    : ParameterModule(aName, type), m_enableTransducerFilter(false)
{
}

bool ParameterModuleTransducerFiltered::SerializeActiveTransducers(MovConfig* movConfig) const
{
    movConfig->SerializeData<bool>(m_enableTransducerFilter, eString, ENABLE_TRANSDUCERS_FILTER_KEY);

    std::string joinedTransducerNames;
    for (const auto &name : m_activeTransducerNames)
    {
        joinedTransducerNames += name + ACTIVE_TRANSDUCERS_DELIMITER;
    }
	if (!joinedTransducerNames.empty()) 
	{
		joinedTransducerNames.pop_back();
	}
    movConfig->SerializeData<std::string>(joinedTransducerNames, eString, ACTIVE_TRANSDUCERS_KEY);
    
    return true;
}

bool ParameterModuleTransducerFiltered::DeSerializeActiveTransducers(MovConfig* movConfig)
{
    // Actif ?
    auto result = movConfig->DeSerializeData<bool>(&m_enableTransducerFilter, eString, ENABLE_TRANSDUCERS_FILTER_KEY);
    
    // Récupération de la liste conjointe des nomgs de transducteurs dans la config
    std::string joinedTransducerNames;
    result &= movConfig->DeSerializeData<std::string>(&joinedTransducerNames, eString, ACTIVE_TRANSDUCERS_KEY);

    // Séparation des différents noms
    if (result)
    {        
        size_t pos;
        while ((pos = joinedTransducerNames.find(ACTIVE_TRANSDUCERS_DELIMITER)) != std::string::npos) {
            const auto name = joinedTransducerNames.substr(0, pos);
            m_activeTransducerNames.insert(name);
            joinedTransducerNames.erase(0, pos + std::string(ACTIVE_TRANSDUCERS_DELIMITER).length());
        }

        // Si la chaine ne se termine pas par le délimiteur, on récupère tout de même le dernier nom
        if (!joinedTransducerNames.empty())
        {
            m_activeTransducerNames.insert(joinedTransducerNames);
        }
    }

    return result;
}

void ParameterModuleTransducerFiltered::setEnableTransducerFilter(const bool enable)
{
    m_enableTransducerFilter = enable;
}

bool ParameterModuleTransducerFiltered::isEnableTransducerFilter() const
{
    return m_enableTransducerFilter;
}

void ParameterModuleTransducerFiltered::addActiveTransducerName(const std::string& transducerName)
{
    m_activeTransducerNames.insert(transducerName);
}

void ParameterModuleTransducerFiltered::addActiveTransducerNames(const std::set<std::string>& transducerNames)
{
    m_activeTransducerNames.insert(transducerNames.cbegin(), transducerNames.cend());
}

void ParameterModuleTransducerFiltered::removeActiveTransducerName(const std::string& transducerName)
{
    m_activeTransducerNames.erase(transducerName);
}

void ParameterModuleTransducerFiltered::removeActiveTransducerNames(const std::set<std::string>& transducerNames)
{
    m_activeTransducerNames.erase(transducerNames.cbegin(), transducerNames.cend());
}

void ParameterModuleTransducerFiltered::clearActiveTransducerNames()
{
    m_activeTransducerNames.clear();
}

const std::set<std::string>& ParameterModuleTransducerFiltered::getActiveTransducerNames() const
{
    return m_activeTransducerNames;
}

bool ParameterModuleTransducerFiltered::isTransducerNameActive(const std::string& transducerName) const
{
    return !m_enableTransducerFilter || m_activeTransducerNames.find(transducerName) != m_activeTransducerNames.end();
}