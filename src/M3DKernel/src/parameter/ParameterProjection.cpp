#include "M3DKernel/parameter/ParameterProjection.h"

// dépendances
#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;
using namespace std;

ParameterProjection::ParameterProjection(void)
{
	m_ProjectionName = "Lambert93";
	m_Eccentricity = 0.08181919104281098;
	m_SemiMajorAxis = 6378137.0;
	m_FirstParal = 44.0;
	m_SecondParal = 49.0;
	m_LongMeridOrigin = 3.0;
	m_X0 = 700000.0;
	m_Y0 = 6600000.0;
	m_ScaleFactor = 1.0;
}

ParameterProjection::~ParameterProjection(void)
{
}


bool ParameterProjection::Serialize(MovConfig * movConfig)
{
	// Début de la sérialisation des paramètres du ParameterObject
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "ProjectionParameters");

	movConfig->SerializeData<string>(m_ProjectionName, eString, "ProjectionName");
	movConfig->SerializeData<double>(m_Eccentricity, eDouble, "Eccentricity");
	movConfig->SerializeData<double>(m_SemiMajorAxis, eDouble, "SemiMajorAxis");
	movConfig->SerializeData<double>(m_FirstParal, eDouble, "FirstParal");
	movConfig->SerializeData<double>(m_SecondParal, eDouble, "SecondParal");
	movConfig->SerializeData<double>(m_LongMeridOrigin, eDouble, "LongMeridOrigin");
	movConfig->SerializeData<double>(m_X0, eDouble, "X0");
	movConfig->SerializeData<double>(m_Y0, eDouble, "Y0");
	movConfig->SerializeData<double>(m_ScaleFactor, eDouble, "ScaleFactor");

	movConfig->SerializePushBack();

	return true;
}


bool ParameterProjection::DeSerialize(MovConfig * movConfig)
{
	bool result = true;

	result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "ProjectionParameters");

	// Début de la désérialisation des paramètres du ParameterObject
	result = result && movConfig->DeSerializeData<string>(&m_ProjectionName, eString, "ProjectionName");
	result = result && movConfig->DeSerializeData<double>(&m_Eccentricity, eDouble, "Eccentricity");
	result = result && movConfig->DeSerializeData<double>(&m_SemiMajorAxis, eDouble, "SemiMajorAxis");
	result = result && movConfig->DeSerializeData<double>(&m_FirstParal, eDouble, "FirstParal");
	result = result && movConfig->DeSerializeData<double>(&m_SecondParal, eDouble, "SecondParal");
	result = result && movConfig->DeSerializeData<double>(&m_LongMeridOrigin, eDouble, "LongMeridOrigin");
	result = result && movConfig->DeSerializeData<double>(&m_X0, eDouble, "X0");
	result = result && movConfig->DeSerializeData<double>(&m_Y0, eDouble, "Y0");
	result = result && movConfig->DeSerializeData<double>(&m_ScaleFactor, eDouble, "ScaleFactor");

	movConfig->DeSerializePushBack();

	return result;
}