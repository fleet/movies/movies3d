#include "M3DKernel/parameter/TramaDescriptor.h"

// dépendances
#include "M3DKernel/config/MovConfig.h"
#include "M3DKernel/utils/log/ILogger.h"

// XERCES
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/framework/MemBufFormatTarget.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>

// Constantes : nom des dictionnaires XML
#define EILayerDictionnaryFileName "desc_eilayer.xml"
#define EIShoalDictionnaryFileName "desc_eishoal.xml"
#define EISupervisedDictionnaryFileName "desc_eisupervised.xml"

#ifdef WIN32
#include "windows.h"
#endif

using namespace BaseKernel;

XERCES_CPP_NAMESPACE_USE

namespace
{
	constexpr const char * LoggerName = "M3DKernel.parameter.TramaDescriptor";
}

TramaDescriptor::TramaDescriptor()
    : TramaDescriptor(eUndefined)
{
}

TramaDescriptor::TramaDescriptor(DictionnaryType type)
{
	m_DicoType = type;
	m_xmlNodesArray.resize(0);
	m_Configured = false;

#ifdef WIN32
    // R�cup�ration du chemin de l'ex�cutable
    char Buffer[1024];
    GetModuleFileNameA(NULL, Buffer, sizeof(Buffer));
    std::string execPath = Buffer;
    size_t pos = execPath.rfind('\\');
    m_DictionnaryPath = execPath.substr(0, pos + 1);
#endif
}

TramaDescriptor::~TramaDescriptor()
{
	m_xmlNodesArray.clear();
}

// renvoie vrai si le noeud XML sp�cifi� est "wanted"
bool TramaDescriptor::IsWanted(std::string stringID)
{
	int nodeNumber = (int)m_xmlNodesArray.size();
	bool result = false;
	for (int i = 0; i < nodeNumber && !result; i++)
	{
		if (m_xmlNodesArray.at(i).wanted && !m_xmlNodesArray.at(i).xmlNodeName.compare(stringID))
		{
			result = true;
		}
	}

	// par d�faut, on coche toutes les trames
	if (!m_Configured)
	{
		result = true;
	}

	return result;
}

// renvoie vrai si le noeud XML sp�cifi� est "needed"
bool TramaDescriptor::IsNeeded(std::string stringID)
{
	int nodeNumber = (int)m_xmlNodesArray.size();
	bool result = false;
	for (int i = 0; i < nodeNumber && !result; i++)
	{
		if (m_xmlNodesArray.at(i).needed && !m_xmlNodesArray.at(i).xmlNodeName.compare(stringID))
		{
			result = true;
		}
	}

	return result;
}


// Positionne le booleen associ� au noeud XML identifi� par stringID
void TramaDescriptor::SetWanted(std::string stringID, bool value)
{
	int nodeNumber = (int)m_xmlNodesArray.size();
	for (int i = 0; i < nodeNumber; i++)
	{
		if (!m_xmlNodesArray[i].xmlNodeName.compare(stringID))
		{
			((m_xmlNodesArray[i]).wanted) = value;
			return;
		}
	}
}

// m�thode utilitaire permettant de cr�er les titres des colonnes CSV
std::string TramaDescriptor::GetTitlesByCat(std::string catID, bool filterEcho)
{
	// si l'arbre n'est pas initialis�, on doit le faire
	if (!m_Configured)
	{
        ParseDictionnary();
	}

	std::string l_sResult = "";


	unsigned int nbElts = (unsigned int)m_xmlNodesArray.size();
	for (unsigned int i = 0; i < nbElts; i++)
	{
		// On ne conserve que les �l�ments commencant par catID et qu'on a souscrit
		if (m_xmlNodesArray[i].xmlNodeName.find(catID) != -1 && m_xmlNodesArray[i].wanted)
		{
			// OTK - 28/04/2009 - pour la trame Extraction, on filtre les echos poru ne pas les avoir dans le CSV quelquesoit
			// l'�tat de l'option save echo data.
			if (!(filterEcho &&
				(m_xmlNodesArray[i].xmlNodeName.find("echolat") != -1
					|| m_xmlNodesArray[i].xmlNodeName.find("echolong") != -1
					|| m_xmlNodesArray[i].xmlNodeName.find("echodepth") != -1)))
			{
				l_sResult = l_sResult + ";" + m_xmlNodesArray[i].xmlNodeName;
			}
		}
	}

	return l_sResult;
}

std::string TramaDescriptor::GetValuesByCat(const std::string & catID, const std::map<std::string, std::string> & values, bool filterEcho) const
{
	std::string l_sResult = "";

	if (m_Configured)
	{
		unsigned int nbElts = (unsigned int)m_xmlNodesArray.size();
		for (unsigned int i = 0; i < nbElts; i++)
		{
			// On ne conserve que les �l�ments commencant par catID et qu'on a souscrit
			const std::string & currentNode = m_xmlNodesArray[i].xmlNodeName;

			int catIDIdx = currentNode.find(catID);
			if (catIDIdx != -1 && m_xmlNodesArray[i].wanted)
			{
				if (!(filterEcho &&
					(currentNode.find("echolat") != -1
						|| currentNode.find("echolong") != -1
						|| currentNode.find("echodepth") != -1)))
				{
					std::string shortNode = currentNode.substr(catIDIdx);

					const std::map<std::string, std::string>::const_iterator itValue = values.find(shortNode);
					if (itValue != values.end())
					{
						l_sResult = l_sResult + ";" + itValue->second;
					}
					else
					{
						l_sResult = l_sResult + ";";
					}
				}
			}
		}
	}

	return l_sResult;
}


bool TramaDescriptor::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation du parameterObject
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "TramaDescriptor");

	movConfig->SerializeData<bool>(m_Configured, eBool, "TramaConfigured");

	movConfig->SerializeData<unsigned int>((unsigned int)m_xmlNodesArray.size(), eUInt, "TramaNumber");

	for (unsigned int i = 0; i < m_xmlNodesArray.size(); i++)
	{
		m_xmlNodesArray[i].Serialize(movConfig);
	}

	movConfig->SerializePushBack();

	return true;
}


bool TramaDescriptor::DeSerialize(MovConfig * movConfig)
{
	bool result = true;

	result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "TramaDescriptor");

	result = result && movConfig->DeSerializeData<bool>(&m_Configured, eBool, "TramaConfigured");

	m_xmlNodesArray.clear();

	unsigned int tramaNumber = 0;
	result = result && movConfig->DeSerializeData<unsigned int>(&tramaNumber, eUInt, "TramaNumber");

	for (unsigned int i = 0; i < tramaNumber; i++)
	{
		TramaNode tramaNode;
		result = result && tramaNode.DeSerialize(movConfig);
		m_xmlNodesArray.push_back(tramaNode);
	}

	movConfig->DeSerializePushBack();

	return result;
}


bool TramaNode::Serialize(MovConfig * movConfig)
{
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "TramaNode");

	movConfig->SerializeData<std::string>(xmlNodeName, eString, "TramaID");
	movConfig->SerializeData<std::string>(xmlNodeDesc, eString, "TramaDesc");
	movConfig->SerializeData<bool>(wanted, eBool, "TramaWanted");
	movConfig->SerializeData<bool>(needed, eBool, "TramaNeeded");

	movConfig->SerializePushBack();

	return true;
}


bool TramaNode::DeSerialize(MovConfig * movConfig)
{
	bool result = true;
	result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "TramaNode");

	result = result && movConfig->DeSerializeData<std::string>(&xmlNodeName, eString, "TramaID");
	result = result && movConfig->DeSerializeData<std::string>(&xmlNodeDesc, eString, "TramaDesc");
	result = result && movConfig->DeSerializeData<bool>(&wanted, eBool, "TramaWanted");
	result = result && movConfig->DeSerializeData<bool>(&needed, eBool, "TramaNeeded");

	movConfig->DeSerializePushBack();

	return result;
}

// ***************************************************************************
/**
 * Construit l'arbre des trames a archiver ou non en fonction des trames du
 * dictionnaire associ�.
 *
 * @date   29/05/2008 - Olivier Tonck(IPSIS) - Cr�ation
 *				 31/03/2009 - Olivier Tonck(IPSIS) - D�placement dans TramaDescriptor
 */
 // ***************************************************************************
 // d�plac� ici car on doit avoir acces au dictionnaire m�me sans passer par l'IHM
void TramaDescriptor::ParseDictionnary()
{
	DOMImplementation *impl = NULL;
	DOMLSParser *parser = NULL;
	DOMDocument* pDoc = NULL;
	std::string nodeStringID;

	// Initialize the XML4C2 system
	try
	{
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException&)
	{
		M3D_LOG_ERROR(LoggerName, "TramaDescriptorForm::ParseDictionnary  Error XML initialization");
		return;
	}

	// Instantiate the DOM parser.
	static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
	impl = DOMImplementationRegistry::getDOMImplementation(gLS);
	parser = ((DOMImplementationLS*)impl)->createLSParser(DOMImplementationLS::MODE_SYNCHRONOUS, 0);

	const char * gMemBufId = "descquery";

	try
	{
		std::string dictionnayName;
		switch ((int)m_DicoType)
		{
		case eLayer:
			dictionnayName = EILayerDictionnaryFileName;
			break;
		case eShoal:
			dictionnayName = EIShoalDictionnaryFileName;
			break;
		case eSupervised:
			dictionnayName = EISupervisedDictionnaryFileName;
			break;
		default:
			assert(false); // type de dictionnaire non d�fini !
			break;
		}
        pDoc = parser->parseURI((m_DictionnaryPath + dictionnayName).c_str());
		if (pDoc == NULL)
		{
			M3D_LOG_WARN(LoggerName, "TramaDescriptorForm::ParseDictionnary  Empty dictionnary");
			delete parser;
			return;
		}

		// XML nodes name
		const std::string node_frame = "frame";
		const std::string node_data = "data";
		const std::string node_attr_name = "name";
		const std::string node_attr_broadcast_name = "broadcastname";
		const std::string node_attr_long_name = "long_name";
		const std::string node_attr_cardinalitymin = "cardinalitymin";

		DOMAttr *pAttribNode = NULL;

		DOMNode * pNodeRoot = pDoc->getDocumentElement();
		DOMNamedNodeMap * pAttribsRoot = pNodeRoot->getAttributes();
		XMLCh * xmlch = XMLString::transcode("name");
		DOMAttr* pAttribNodeRoot = (DOMAttr*)pAttribsRoot->getNamedItem(xmlch);
		XMLString::release(&xmlch);
		char * xmlch2 = XMLString::transcode(pAttribNodeRoot->getValue());
		std::string rootName = xmlch2;
		XMLString::release(&xmlch2);
		AddNode(rootName, "", IsWanted(rootName));

		//Pour tous les noeuds <frame>, on cr�e un item dans notre arbre
		//--------------------------------------------------------------

		// recuperation de la liste des trames
		xmlch = XMLString::transcode(node_frame.c_str());
		DOMNodeList* pListNode = pDoc->getElementsByTagName(xmlch);
		XMLString::release(&xmlch);
		// si aucune trame dans le dictionnaire, on arr�te
		if (pListNode->getLength() == 0)
		{
			M3D_LOG_ERROR(LoggerName, "TramaDescriptorForm::ParseDictionnary  No frame in dictionnary");
			delete parser;
			return;
		}

		std::string parentName;
		// on boucle sur la liste des trames et on cr�e l'arbre associ�
		for (unsigned int i = 0; i < pListNode->getLength(); i++)
		{
			//r�cup�ration du noeud
			DOMNode * pNode = pListNode->item(i);
			//r�cuperation des attributes du noeud
			DOMNamedNodeMap * pAttribs = pNode->getAttributes();
			//r�cuperation de l'attribut "name" de la trame
			xmlch = XMLString::transcode(node_attr_name.c_str());
			pAttribNode = (DOMAttr*)pAttribs->getNamedItem(xmlch);
			XMLString::release(&xmlch);
			if (pAttribNode != NULL)
			{
				xmlch2 = XMLString::transcode(pAttribNode->getValue());
				nodeStringID = xmlch2;
				XMLString::release(&xmlch2);
				nodeStringID = "\\" + nodeStringID;
				nodeStringID = rootName + nodeStringID;
				size_t parentNodeIndex = m_xmlNodesArray.size();
				AddNode(nodeStringID, "", IsWanted(nodeStringID));

				parentName = nodeStringID;
				//ajout des �l�ments des trames
				DOMNodeList* pListElts = pNode->getChildNodes();
				//boucle sur les �l�ments
				for (unsigned int j = 0; j < pListElts->getLength(); j++)
				{
					DOMNode * pElt = pListElts->item(j);
					//si on est bien dans un element de la trame (balise <data>)
					xmlch = XMLString::transcode(node_data.c_str());
					if (XMLString::equals(pElt->getNodeName(), xmlch))
					{
						XMLString::release(&xmlch);
						DOMNamedNodeMap * pAttribsElt = pElt->getAttributes();
						xmlch = XMLString::transcode(node_attr_broadcast_name.c_str());
						DOMAttr* pAttribElt = (DOMAttr*)pAttribsElt->getNamedItem(xmlch);
						XMLString::release(&xmlch);
						std::string nameDescr = "";
						if (pAttribElt != NULL)
						{
							xmlch2 = XMLString::transcode(pAttribElt->getValue());
							nodeStringID = xmlch2;
							XMLString::release(&xmlch2);
							nodeStringID = "\\" + nodeStringID;
							nodeStringID = parentName + nodeStringID;


							// r�cup�ration de la description longue
							xmlch = XMLString::transcode(node_attr_long_name.c_str());
							pAttribElt = (DOMAttr*)pAttribsElt->getNamedItem(xmlch);
							XMLString::release(&xmlch);
							if (pAttribElt != NULL)
							{
								xmlch2 = XMLString::transcode(pAttribElt->getValue());
								nameDescr = xmlch2;
								XMLString::release(&xmlch2);
							}

							// r�cup�ration de la cardinalit� minimum
							bool needed = false;
							xmlch = XMLString::transcode(node_attr_cardinalitymin.c_str());
							pAttribElt = (DOMAttr*)pAttribsElt->getNamedItem(xmlch);
							XMLString::release(&xmlch);
							if (pAttribElt != NULL)
							{
								xmlch2 = XMLString::transcode(pAttribElt->getValue());
								if (atoi(xmlch2) > 0)
								{
									needed = true;
								}
								XMLString::release(&xmlch2);
							}

							AddNode(nodeStringID, nameDescr, IsWanted(nodeStringID), needed, parentNodeIndex);
						}
						pAttribElt = NULL;
					}
					else
					{
						XMLString::release(&xmlch);
					}
				}

			}
			pAttribNode = NULL;
		}

		m_Configured = true;
	}
    catch (const xercesc::OutOfMemoryException&)
	{
		M3D_LOG_ERROR(LoggerName, "TramaDescriptorForm::ParseDictionnary  Out of memory exception");
		delete parser;
		return;
	}
	catch (const XMLException& e)
	{
		std::string str = "TramaDescriptorForm::ParseDictionnary  XMLException : ";
		char * xmlch = XMLString::transcode(e.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_ERROR(LoggerName, str.c_str());
		delete parser;
		return;
	}

	delete parser;

	// Terminates the XML4C2 system
	try
	{
		XMLPlatformUtils::Terminate();
	}
	catch (const XMLException&)
	{
		M3D_LOG_ERROR(LoggerName, "TramaDescriptorForm::ParseDictionnary  Error XML termination");
		return;
	}

	return;
}

// ajout d'un noeud a la config
void TramaDescriptor::AddNode(std::string nodeText, std::string nodeDesc, bool nodeWanted,
	bool needed, size_t parentNodeIndex)
{
	// ajout du noeud lui m�me
	TramaNode xmlNode;
	xmlNode.wanted = nodeWanted;
	xmlNode.needed = needed;
	if (needed)
	{
		// noeud racine
		m_xmlNodesArray[0].needed = true;

		// noeud parent
		m_xmlNodesArray[parentNodeIndex].needed = true;
	}
	xmlNode.xmlNodeName = nodeText;
	xmlNode.xmlNodeDesc = nodeDesc;

	m_xmlNodesArray.push_back(xmlNode);
}
