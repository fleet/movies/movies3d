// -*- C++ -*-
// ****************************************************************************
// Class: CCartoTools
//
// Description: M�thodes utilitaires pour les calculs de projection et 
// transformation de coordonn�es g�ographiques en coordonn�es cart�siennes
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Juin 2010
// Soci�t� : IPSIS
// ****************************************************************************
#include "M3DKernel/utils/carto/CartoTools.h"

#include "M3DKernel/DefConstants.h"
#include "M3DKernel/datascheme/KernelParameter.h"
#include "M3DKernel/parameter/ParameterProjection.h"

#include <sstream>
#include <iomanip>

#include <math.h>

bool CCartoTools::s_bKnownOrigin = false;
BaseMathLib::Vector2D CCartoTools::s_Origin(0, 0);

double CCartoTools::s_A; // demi grand eaxe
double CCartoTools::s_E; // excentricit�
double CCartoTools::s_E2; // excentricit� au carr�
double CCartoTools::s_DegToRad = PI / 180.0;
const char degunit = '°';

// paramètres de projection
double CCartoTools::s_L1;
double CCartoTools::s_L2;
double CCartoTools::s_G0;
double CCartoTools::s_X0;
double CCartoTools::s_Y0;
double CCartoTools::s_RK;
double CCartoTools::s_N1;
double CCartoTools::s_N2;
double CCartoTools::s_ISO1;
double CCartoTools::s_ISO2;
double CCartoTools::s_L12;
double CCartoTools::s_R0;
double CCartoTools::s_N;
double CCartoTools::s_C;

using namespace BaseKernel;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************
CCartoTools::CCartoTools()
{
}

CCartoTools::~CCartoTools()
{
}

// *********************************************************************
// Traitements
// *********************************************************************

// initialisations et pr�-calculs en fonction des param�tres de projection
void CCartoTools::Initialize(KernelParameter& kernelParams)
{
	int selectedProjection = kernelParams.getSelectedProjection();
	ParameterProjection params = kernelParams.getProjections()[selectedProjection];
	s_A = params.m_SemiMajorAxis;
	s_E = params.m_Eccentricity;
	s_E2 = s_E*s_E;

	s_L1 = params.m_FirstParal*s_DegToRad;
	s_L2 = params.m_SecondParal*s_DegToRad;
	s_G0 = params.m_LongMeridOrigin*s_DegToRad;
	s_X0 = params.m_X0;
	s_Y0 = params.m_Y0;
	s_RK = params.m_ScaleFactor;
	s_N1 = s_A / sqrt(1.0 - (s_E2*sin(s_L1)*sin(s_L1)));
	s_N2 = s_A / sqrt(1.0 - (s_E2*sin(s_L2)*sin(s_L2)));
	s_ISO1 = tan(s_L1 / 2.0 + PI / 4.0)*pow(((1.0 - s_E*sin(s_L1)) / (1.0 + s_E*sin(s_L1))), (s_E / 2.0));
	s_ISO2 = tan(s_L2 / 2.0 + PI / 4.0)*pow(((1.0 - s_E*sin(s_L2)) / (1.0 + s_E*sin(s_L2))), (s_E / 2.0));
	s_L12 = (s_L1 + s_L2) / 2.0;
	s_R0 = s_A / sqrt(1.0 - s_E2*sin(s_L12)*sin(s_L12)) / sin(s_L12) * cos(s_L12) * s_RK;
	s_N = log((s_N2*cos(s_L2)) / (s_N1 * cos(s_L1))) / log(s_ISO1 / s_ISO2);
	s_C = s_N1 * cos(s_L1) / s_N * pow(s_ISO1, s_N);
}

// calcul des coordonn�es g�ographiques en fonction de coordonn�es de r�f�rence
// et d'un vecteur cartesien relatif � ces coordonn�es de r�f�rence.
void CCartoTools::GetGeoCoords(double latitude, double longitude,
	double x, double y,
	double &resultingLat, double &resultingLong)
{
	double sinlat = sin(DEG_TO_RAD(latitude));
	double N = s_A / sqrt(1 - s_E2*sinlat);
	double ro = N * (1 - s_E2) / (1 - s_E2*sinlat*sinlat);

	resultingLat = latitude + RAD_TO_DEG(x / ro);
	resultingLong = longitude + RAD_TO_DEG(y / (N*cos(DEG_TO_RAD(latitude))));
}

void CCartoTools::GetGeoCoords(const BaseMathLib::Vector3D& refCoords
	, const std::vector<BaseMathLib::Vector3D>& offsetCoords
	, std::vector<BaseMathLib::Vector3D>& resCoords)
{
	const double sinlat = sin(DEG_TO_RAD(refCoords.x));
	const double N = s_A / sqrt(1 - s_E2 * sinlat);
	const double ro = N * (1 - s_E2) / (1 - s_E2 * sinlat * sinlat);

	const double xFact = RAD_TO_DEG(1.0 / ro);
	const double yFact = RAD_TO_DEG(1.0 / (N * cos(DEG_TO_RAD(refCoords.x))));

	const size_t n = offsetCoords.size();
	for (size_t i = 0; i < n; ++i)
	{
		resCoords[i].x = refCoords.x + offsetCoords[i].x * xFact;
		resCoords[i].y = refCoords.y + offsetCoords[i].y * yFact;
	}
}

// projection conique conforme (lat std::int32_t vers x y)
void CCartoTools::GetCartesianCoords(double latitude, double longitude,
	double &x, double &y, bool translation)
{

	double G = longitude*s_DegToRad;
	double L = latitude*s_DegToRad;
	double ISO = tan(PI / 4.0 + L / 2.0) * pow(((1.0 - s_E*sin(L)) / (1.0 + s_E*sin(L))), (s_E / 2.0));

	double IsoN = pow(ISO, s_N);
	double NG_G0 = s_N*(G - s_G0);

	y = s_C * sin(NG_G0) / IsoN + s_X0;
	x = s_R0 - s_C * cos(NG_G0) / IsoN + s_Y0;

	// OTK - FAE041 - translation de l'origine
	if (s_bKnownOrigin && translation)
	{
		double cartesianoriginx, cartesianoriginy;
		GetCartesianCoords(s_Origin.x, s_Origin.y, cartesianoriginx, cartesianoriginy, false);
		x -= cartesianoriginx;
		y -= cartesianoriginy;
	}
}

// positionnement de la translation d'origine
void CCartoTools::SetOrigin(double latitude, double longitude)
{
	s_Origin.x = latitude;
	s_Origin.y = longitude;
	s_bKnownOrigin = true;
}

std::string CCartoTools::LattitudeToString(double lattitude)
{
	std::ostringstream latString;

	char sign = 'N';
	if (lattitude < 0)
	{
		sign = 'S';
		lattitude = -lattitude;
	}

	short latDeg = (short)lattitude;
	double latMin = 60.*(lattitude - (double)latDeg);
	short latMin2 = (short)latMin;
	latMin = 10000.*(latMin - (double)latMin2);

	latString << std::setfill('0');
	latString << std::setw(2) << latDeg << degunit
		<< std::setw(2) << latMin2 << "."
		<< std::setw(4) << (short)latMin
		<< std::setw(1) << sign;

	return latString.str();
}

std::string CCartoTools::LongitudeToString(double longitude)
{
	std::ostringstream longString;

	char sign = 'E';
	if (longitude < 0)
	{
		sign = 'W';
		longitude = -longitude;
	}
	short longDeg = (short)longitude;
	double longMin = 60.*(longitude - (double)longDeg);
	short longMin2 = (short)longMin;
	longMin = 10000.*(longMin - (double)longMin2);

	longString << std::setfill('0');
	longString << std::setw(3) << longDeg << degunit
		<< std::setw(2) << longMin2 << "."
		<< std::setw(4) << (short)longMin
		<< std::setw(1) << sign;

	return longString.str();
}
