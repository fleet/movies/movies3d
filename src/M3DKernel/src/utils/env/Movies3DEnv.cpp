#ifdef WIN32

// -*- C++ -*-
// ****************************************************************************
// Class: Movies3DEnv
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Septembre 2009
// Soci�t� : IPSIS
// ****************************************************************************

#include "M3DKernel/utils/env/Movies3DEnv.h"
#include "M3DKernel/utils/env/registrytools.h"

//static module variables
static HKEY__ *m_pKeyRoot = HKEY_LOCAL_MACHINE;

//static class variables
Movies3DEnv* Movies3DEnv::m_pInstance = NULL;

//*****************************************************************************
// Name : Init
// Description : initialise the Movies3D Env
// Parameters : void
// Return : void
//*****************************************************************************
void Movies3DEnv::Init()
{
	//init member variables
	m_regKeyEnv = "SOFTWARE\\IFREMER\\Moviews3D\\Environment";

	m_regSubKey_LastCfg = "Last config";
	m_regSubKey_LastLoadDir = "Last load dir";

	//add key if needed
	AddKey(m_pKeyRoot, m_regKeyEnv.c_str(), m_regSubKey_LastCfg.c_str(), "");
	AddKey(m_pKeyRoot, m_regKeyEnv.c_str(), m_regSubKey_LastLoadDir.c_str(), "");
}

//*****************************************************************************
// Name : GetLastConfig
// Description : get Movies3D env : last cfg
// Parameters : void
// Return : std::string
//*****************************************************************************
std::string Movies3DEnv::GetLastConfig()
{
	std::string result;

	GetKeyValue(m_pKeyRoot, m_regKeyEnv.c_str(), m_regSubKey_LastCfg.c_str(), result);

	return result;
}

//*****************************************************************************
// Name : SetLastConfig
// Description : set Movies3D env : last cfg
// Parameters : const std::string& dir
// Return : void
//*****************************************************************************
void Movies3DEnv::SetLastConfig(const std::string& dir)
{
	SetKeyValue(m_pKeyRoot, m_regKeyEnv.c_str(), m_regSubKey_LastCfg.c_str(), dir.c_str());
}

//*****************************************************************************
// Name : GetLastLoadDir
// Description : get Movies3D env : last load directory
// Parameters : void
// Return : std::string
//*****************************************************************************
std::string Movies3DEnv::GetLastLoadDir()
{
	std::string result;
	GetKeyValue(m_pKeyRoot, m_regKeyEnv.c_str(), m_regSubKey_LastLoadDir.c_str(), result);
	return result;
}

//*****************************************************************************
// Name : SetLastLoadDir
// Description : set Movies3D env : last load directory
// Parameters : const std::string& dir
// Return : void
//*****************************************************************************
void Movies3DEnv::SetLastLoadDir(const std::string& dir)
{
	SetKeyValue(m_pKeyRoot, m_regKeyEnv.c_str(), m_regSubKey_LastLoadDir.c_str(), dir.c_str());
}

#endif
