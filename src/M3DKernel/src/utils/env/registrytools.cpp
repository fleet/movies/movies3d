#ifdef WIN32
//*****************************************************************************
// File : Registrytools.cpp
//
// Description : Some useful functions tools to manipulate registry
//
// Date : 29/01/2007
// Author : FRE
//*****************************************************************************
#undef UNICODE

#include "M3DKernel/utils/env/registrytools.h"

#include <stdlib.h>

#include <assert.h>

#include <fstream>
#include <strstream>
#include <string>

#undef LPCWSTR
#undef LPWSTR

#define LPCWSTR LPCSTR
#define LPWSTR LPSTR 


//*****************************************************************************
// Name : DeleteKey
// Description : delete a key in the registry
//
// Date : 29/01/2007
// Author : FRE
//*****************************************************************************
std::int32_t DeleteKey(HKEY hKeyParent, const char* lpszKeyChild)
{
	HKEY hKeyChild;
	std::int32_t lRes = RegOpenKeyEx(hKeyParent, (LPCWSTR)lpszKeyChild, 0, KEY_ALL_ACCESS, &hKeyChild);
	if (lRes != ERROR_SUCCESS) return lRes;
	FILETIME time;
	char szBuffer[256];
	DWORD dwSize = 256;
	while (RegEnumKeyEx(hKeyChild, 0, (LPWSTR)szBuffer, &dwSize, NULL, NULL, NULL, &time) == S_OK)
	{
		lRes = DeleteKey(hKeyChild, szBuffer);
		if (lRes != ERROR_SUCCESS)
		{
			RegCloseKey(hKeyChild);
			return lRes;
		}
		dwSize = 256;
	}
	RegCloseKey(hKeyChild);
	return RegDeleteKey(hKeyParent, (LPCWSTR)lpszKeyChild);
}

//*****************************************************************************
// Name : DeleteKey
// Description : delete a key in the registry
//
// Date : 29/01/2007
// Author : FRE
//*****************************************************************************
std::int32_t DeleteKey(struct HKEY__ * pkeyRoot, const char* szKey, const char* lpszKeyChild)
{
	HKEY hKey;
	std::int32_t lret = FALSE;

	lret = ::RegOpenKeyEx(pkeyRoot, (LPCWSTR)szKey, 0, KEY_ALL_ACCESS, &hKey);

	if (lret == ERROR_SUCCESS)
	{
		lret = DeleteKey(hKey, lpszKeyChild);
	}
	RegCloseKey(hKey);

	return lret;
}

//*****************************************************************************
// Name : AddKey
// Description : add a key in the registry
//
// Date : 29/01/2007
// Author : FRE
//*****************************************************************************
BOOL AddKey(struct HKEY__ * pkeyRoot, const char* szKey, const char* szSubkey, const char* szValue)
{
	HKEY hKey;
	char szKeyBuf[1024];
	strcpy(szKeyBuf, szKey);
	if (szSubkey != NULL)
	{
		strcat(szKeyBuf, "\\");
		strcat(szKeyBuf, szSubkey);
	}
	std::int32_t lResult = RegCreateKeyEx(pkeyRoot, (LPCWSTR)szKeyBuf, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);

	if (lResult != ERROR_SUCCESS) {
		return FALSE;
	}
	if (szValue != NULL) {
		RegSetValueEx(hKey, NULL, 0, REG_SZ, (BYTE *)szValue, strlen(szValue) + 1);
	}
	RegCloseKey(hKey);
	return TRUE;
}

//*****************************************************************************
// Name : SetKeyValue
// Description : set the value for a key in the registry
//
// Date : 29/01/2007
// Author : FRE
//*****************************************************************************
BOOL SetKeyValue(struct HKEY__ * pkeyRoot, const char* szKey, const char* szNamedValue, const char* szValue)
{
	HKEY hKey;
	char szKeyBuf[1024];
	strcpy(szKeyBuf, szKey);
	std::int32_t lResult = RegOpenKeyEx(pkeyRoot, (LPCWSTR)szKeyBuf, 0, KEY_SET_VALUE, &hKey);
	if (lResult != ERROR_SUCCESS) return FALSE;
	if (szValue != NULL)
		RegSetValueEx(hKey, (LPCWSTR)szNamedValue, 0, REG_SZ, (BYTE*)szValue, strlen(szValue) + 1);
	RegCloseKey(hKey);
	return TRUE;
}

//*****************************************************************************
// Name : GetKeyValue
// Description : get the value from a key in the registry
//
// Date : 29/01/2007
// Author : FRE
//*****************************************************************************
BOOL GetKeyValue(struct HKEY__ * pkeyRoot, const char* szKey, const char* szNamedValue, unsigned char** szValue, std::uint32_t* valueSize)
{
	HKEY hKey;

	assert(pkeyRoot != NULL);
	assert(szKey != NULL);
	assert(szNamedValue != NULL);
	assert(szValue != NULL);
	assert(valueSize != NULL);


	BOOL lret = ::RegOpenKeyEx(pkeyRoot, (LPCWSTR)szKey, 0, KEY_READ, &hKey);

	if (lret == ERROR_SUCCESS)
	{
		DWORD dwVarType = REG_SZ;
		unsigned char szKeyBuf[1024];
		*valueSize = sizeof(szKeyBuf);

		lret = ::RegQueryValueEx(hKey, (LPCWSTR)szNamedValue, NULL, &dwVarType, szKeyBuf, (LPDWORD)valueSize);

		if (lret == ERROR_SUCCESS)
		{
			if (*szValue != NULL)
			{
				free(szValue);
			}
			*szValue = (unsigned char*)malloc(*valueSize + 1);
			memcpy(*szValue, szKeyBuf, *valueSize + 1);
		}

		::RegCloseKey(hKey);
	}

	return lret == ERROR_SUCCESS;
}

//*****************************************************************************
// Name : GetKeyValue
// Description : get the value from a key in the registry
//
// Date : 29/01/2007
// Author : FRE
//*****************************************************************************
BOOL GetKeyValue(struct HKEY__ * pkeyRoot, const char* szKey, const char* szNamedValue, std::string& szValue)
{
	BOOL lret = FALSE;

	unsigned char* pRegValue = NULL;
	std::uint32_t regValueSize = 0;

	if (GetKeyValue(pkeyRoot, szKey, szNamedValue, &pRegValue, &regValueSize))
	{
		if (pRegValue != NULL)
		{
			szValue = (char *)pRegValue;

			if (pRegValue != NULL)
			{
				delete pRegValue;
				pRegValue = NULL;
			}

			lret = TRUE;
		}
	}
	return lret;
}

#endif