﻿#include "M3DKernel/utils/fileutils.h"

// d�pendances
#include <sys/stat.h>

// renvoie la taille du fichier courant utilis� par le flux
std::uint32_t fileutils::getFileSize(const std::string & filePath)
{
	std::uint32_t result = 0;
	struct stat statbuf;

	if (stat(filePath.c_str(), &statbuf) != -1) {
		result = statbuf.st_size;
	}

	return result;
}