#include "M3DKernel/utils/log/LogAppender.h"

#include "M3DKernel/utils/log/ILogger.h"

#include "log4cplus/logger.h"
#include "log4cplus/appender.h"
#include "log4cplus/spi/loggingevent.h"

#include <list>
#include <set>

using namespace Log;

LogAppender* LogAppender::m_logAppenderInstance = nullptr;

class InternalAppender : public log4cplus::Appender
{
public:
	ILogAppender* defaultAppender = nullptr;
	std::list<ILogAppender*> appenders;

	std::set<LogLevel> logAlerts;

	virtual ~InternalAppender() override
	{
		destructorImpl();
	}

	void close() override
	{
	}

	void append(const log4cplus::spi::InternalLoggingEvent& event)
	{
		Log::LoggerItem item;
		item.level = event.getLogLevel();
		item.timestamp = event.getTimestamp().time_since_epoch().count() / 1000;
		item.source = event.getLoggerName();
		item.message = event.getMessage();

		if (defaultAppender)
		{
			defaultAppender->append(item);
		}

		for (auto appender : appenders)
		{
			appender->append(item);
		}

		// Ajout un niveau d'alerte pour ce niveau de log
		logAlerts.emplace(item.level);
	}
};

class LogAppender::Impl
{
public:
	InternalAppender* appender;
	log4cplus::SharedAppenderPtr log4plus_appender;
};

LogAppender* LogAppender::GetInstance()
{
	if (!m_logAppenderInstance)
	{
		m_logAppenderInstance = new LogAppender();
	}
	return m_logAppenderInstance;
}

void LogAppender::FreeMemory()
{
	delete m_logAppenderInstance;
	m_logAppenderInstance = nullptr;
}

LogAppender::LogAppender()
{
	impl = new LogAppender::Impl();
	impl->appender = new InternalAppender;
	impl->log4plus_appender = impl->appender;

	auto root = log4cplus::Logger::getRoot();
	root.addAppender(impl->log4plus_appender);
}

LogAppender::~LogAppender()
{
	auto root = log4cplus::Logger::getRoot();
	root.removeAppender(impl->log4plus_appender);
}

void Log::LogAppender::setDefaultAppender(ILogAppender * appender)
{
	impl->appender->defaultAppender = appender;
}

void Log::LogAppender::registerAppender(ILogAppender * appender)
{
	impl->appender->appenders.push_back(appender);
}

void Log::LogAppender::unregisterAppender(ILogAppender * appender)
{
	impl->appender->appenders.remove(appender);
}

void Log::LogAppender::resetCheckLevelMessage(const int level)
{
	if (level == NO_LOG_LEVEL)
	{
		impl->appender->logAlerts.clear();
	}
	else
	{
		impl->appender->logAlerts.erase(level);
	}
}

int Log::LogAppender::maxAlertLevel()
{
	if (impl->appender->logAlerts.empty())
	{
		return NO_LOG_LEVEL;
	}
	return *impl->appender->logAlerts.rbegin();
}
