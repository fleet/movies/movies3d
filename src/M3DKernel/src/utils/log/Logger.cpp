#include "M3DKernel/utils/log/LoggerFactory.h"

#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/helpers/stringhelper.h>
#include <log4cplus/loggingmacros.h>

namespace Log
{
    class Logger : public ILogger
    {
    public:
        Logger(const log4cplus::Logger & log4cplusLogger)
            : m_logger(log4cplusLogger)
        {
        }

        virtual ~Logger()
        {
        }

        void log(int logLevel, const std::string & msg, const char * file, int line)
        {
            log4cplus::LogLevel ll = logLevel;
            m_logger.log(ll, msg, file, line);
        }

        bool isLevelEnabled(int logLevel) const
        {
            return m_logger.isEnabledFor(logLevel);
        }

        std::shared_ptr<Log::ILogger> getLogger(const std::string & loggerName) const
        {
            log4cplus::tstring completeLogggerName =
                    m_logger.getName() + LOG4CPLUS_C_STR_TO_TSTRING(".") + loggerName;
            log4cplus::Logger log4cplusLogger = m_logger.getInstance(completeLogggerName);
            std::shared_ptr<Log::ILogger> logger = std::make_shared<Logger>(log4cplusLogger);
            return logger;
        }

    private:
        log4cplus::Logger m_logger;
    };
}

static log4cplus::tstring const empty_str;
static log4cplus::tstring const hacevent_str(LOG4CPLUS_TEXT("HAC "));

static log4cplus::tstring const & ToStringMethod(log4cplus::LogLevel ll)
{
	return (ll == Log::HAC_LOG_LEVEL) ? hacevent_str : empty_str;
}

Log::LoggerFactory::LoggerFactory()
{
	log4cplus::initialize();
	log4cplus::getLogLevelManager().pushToStringMethod(ToStringMethod);
	log4cplus::helpers::LogLog::getLogLog()->setInternalDebugging(false);
	log4cplus::BasicConfigurator::doConfigure();
}

Log::LoggerFactory::~LoggerFactory()
{
	log4cplus::Logger::shutdown();
}

Log::LoggerFactory * Log::LoggerFactory::m_instance = nullptr; //new Log::LoggerFactory();

Log::LoggerFactory * Log::LoggerFactory::GetInstance()
{
	if(m_instance == nullptr)
	{
		m_instance = new Log::LoggerFactory();
	}
    return m_instance;
}

void Log::LoggerFactory::FreeMemory()
{
	delete m_instance;
	m_instance = nullptr;
}

void Log::LoggerFactory::initialize(const std::string & propertiesFilePath)
{
    try
    {
        log4cplus::PropertyConfigurator::doConfigure( propertiesFilePath);
    }
    catch(...)
    {
        log4cplus::Logger root = log4cplus::Logger::getRoot();
        LOG4CPLUS_FATAL(root, "Exception occured...");
    }
}

std::shared_ptr<Log::ILogger> Log::LoggerFactory::getLogger(const std::string& loggerName)
{
    log4cplus::Logger log4cplusLogger = log4cplus::Logger::getInstance(loggerName);
	return std::make_shared<Log::Logger>(log4cplusLogger);
}

void Log::Utils::log(const std::string & loggerName, Log::LogLevel logLevel, const std::string & msg, const char * file, int line)
{
	auto logger = Log::LoggerFactory::GetInstance()->getLogger(loggerName);
	logger->log(logLevel, msg, file, line);
}

void Log::Utils::log(M3DLogger logger, Log::LogLevel logLevel, const std::string & msg, const char * file, int line)
{
	logger->log(logLevel, msg, file, line);
}