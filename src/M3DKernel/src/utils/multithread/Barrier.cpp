#include "M3DKernel/utils/multithread/Barrier.h"

#include <atomic>
#include <condition_variable>

struct Barrier::Impl
{
	std::atomic_bool state;
	std::condition_variable cond;
};

Barrier::Barrier()
	: impl { new Barrier::Impl }
{
	unlock();
}

Barrier::~Barrier()
{
	unlock();
	delete impl;
}

void Barrier::setState(bool state)
{
	impl->state = state;
	impl->cond.notify_all();
}

void Barrier::wait(bool state)
{
	if (impl->state == state)
		return;

	std::mutex m;
	std::unique_lock<std::mutex> lk(m);
	impl->cond.wait(lk, [this, state] { return (impl->state == state); });
}

void Barrier::lock()
{
	setState(true);
}

void Barrier::unlock()
{
	setState(false);
}

void Barrier::waitUnlock()
{
	wait(false);
}

void Barrier::waitLock()
{
	wait(true);
}

bool Barrier::isLocked() const
{
	return impl->state;
}


BarrierLocker::BarrierLocker(Barrier & _b)
	: b(_b)
{
	b.lock();
}

BarrierLocker::~BarrierLocker()
{
	b.unlock();
}