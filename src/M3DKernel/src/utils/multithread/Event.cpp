#include "M3DKernel/utils/multithread/Event.h"

#include <mutex>
#include <atomic>
#include <condition_variable>

class CEvent::Impl
{
public:
    std::atomic_bool state;
	std::mutex mutex;
	std::condition_variable cond;
};

// ***************************************************************************
// Constructeurs / Destructeur
// ***************************************************************************

// ***************************************************************************
// Nom : Constructeur
// Description : Constructeur par defaut
// ***************************************************************************
CEvent::CEvent()
	: impl(new CEvent::Impl)
{
    impl->state = false;
}

// ***************************************************************************
// Nom : Destructeur
// Description : Detruit les donnees de l'objets courants
// ***************************************************************************
CEvent::~CEvent()
{
	Post();
	delete impl;
}

// ***************************************************************************
/**
 * <Description d�taill�e de la m�thode>
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CEvent::Post()
{
	impl->state = true;
	impl->cond.notify_all();
}

// ***************************************************************************
/**
 * <Description d�taill�e de la m�thode>
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CEvent::Wait()
{
	if (impl->state) {
		return;
	}

	std::unique_lock<std::mutex> lock(impl->mutex);
	impl->cond.wait(lock, [this] { return impl->state == true; });
}

// ***************************************************************************
/**
 * <Description d�taill�e de la m�thode>
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CEvent::Reset()
{
	impl->state = false;
}