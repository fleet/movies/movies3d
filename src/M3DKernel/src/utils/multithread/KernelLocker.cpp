#include "M3DKernel/utils/multithread/KernelLocker.h"
#include "M3DKernel/M3DKernel.h"

KernelLocker::KernelLocker()
{
	M3DKernel::GetInstance()->Lock();
}

KernelLocker::~KernelLocker()
{
	M3DKernel::GetInstance()->Unlock();
}