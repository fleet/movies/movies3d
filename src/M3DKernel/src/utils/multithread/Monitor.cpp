#include "M3DKernel/utils/multithread/Monitor.h"

#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "M3DKernel/utils/multithread/RecursiveMutexImpl.h"

// STL C++
#include <cassert>
#include <condition_variable>

class CMonitor::Impl
{
public:
	std::condition_variable_any cond;
	CRecursiveMutex mutex;
};

// ***************************************************************************
// Constructeurs / Destructeur
// ***************************************************************************

// ***************************************************************************
// Nom : Constructeur
// Description : Constructeur par defaut
// ***************************************************************************
CMonitor::CMonitor()
	: impl(new CMonitor::Impl)
{
}

// ***************************************************************************
// Nom : Destructeur
// Description : Detruit les donnees de l'objets courants
// ***************************************************************************
CMonitor::~CMonitor()
{
	delete impl;
}

// ***************************************************************************
/**
 * <Description d�taill�e de la m�thode>
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CMonitor::Wait()
{
	ValidateMutexOwner("wait(std::int32_t)");

	std::unique_lock<std::recursive_mutex> lock(impl->mutex.impl->mutex);
	impl->cond.wait(lock);
}

// ***************************************************************************
/**
 * <Description d�taill�e de la m�thode>
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CMonitor::Wait(std::int32_t timeout)
{
	ValidateMutexOwner("wait(std::int32_t)");

	std::unique_lock<std::recursive_mutex> lock(impl->mutex.impl->mutex);
	impl->cond.wait_for(lock, std::chrono::seconds(timeout));
}

// ***************************************************************************
/**
 * <Description d�taill�e de la m�thode>
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CMonitor::Notify()
{
	ValidateMutexOwner("notify");
	impl->cond.notify_one();
}

// ***************************************************************************
/**
 * <Description d�taill�e de la m�thode>
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CMonitor::NotifyAll()
{
	ValidateMutexOwner("notifyAll");
	impl->cond.notify_all();
}

// ***************************************************************************
/**
 * <Description d�taill�e de la m�thode>
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CMonitor::ValidateMutexOwner(const char*) const
{
	// TODO MUTEX
	//assert(m_MonMutex.GetOwner() == CThreadId::Self());
}

// ***************************************************************************
/**
 * <Description d�taill�e de la m�thode>
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CMonitor::Lock(bool)
{
	impl->mutex.Lock();
}

// ***************************************************************************
/**
 * <Description d�taill�e de la m�thode>
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CMonitor::Unlock()
{
	impl->mutex.Unlock();
}
