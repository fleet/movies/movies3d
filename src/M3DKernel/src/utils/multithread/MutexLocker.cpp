#include "M3DKernel/utils/multithread/MutexLocker.h"

CRecursiveMutexLocker::CRecursiveMutexLocker(CRecursiveMutex *  mutex)
	: m_mutex(mutex)
{
	m_mutex->Lock();
}

// ***************************************************************************
// Nom : Destructeur
// Description : Detruit les donnees de l'objets courants
// ***************************************************************************
CRecursiveMutexLocker::~CRecursiveMutexLocker()
{
	m_mutex->Unlock();
}
