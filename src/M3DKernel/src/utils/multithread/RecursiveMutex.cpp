#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "M3DKernel/utils/multithread/RecursiveMutexImpl.h"

#include <mutex>

// ***************************************************************************
// Constructeurs / Destructeur
// ***************************************************************************

// ***************************************************************************
// Nom : Constructeur
// Description : Constructeur par defaut
// ***************************************************************************
CRecursiveMutex::CRecursiveMutex()
	: impl(new CRecursiveMutexImpl)
{
}

// ***************************************************************************
// Nom : Destructeur
// Description : Detruit les donnees de l'objets courants
// ***************************************************************************
CRecursiveMutex::~CRecursiveMutex()
{
	delete impl;
}

// ***************************************************************************
// Traitements
// ***************************************************************************
// ***************************************************************************
/**
 * Lock du r�cursive mutex
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CRecursiveMutex::Lock() const
{
	impl->mutex.lock();
}

// ***************************************************************************
/**
 * Unlock du r�cursive mutex
 *
 * @date   16/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CRecursiveMutex::Unlock() const
{
	impl->mutex.unlock();
}
