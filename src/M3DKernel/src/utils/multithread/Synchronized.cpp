#include "M3DKernel/utils/multithread/Synchronized.h"

// STL C++
#include <cassert>

// ***************************************************************************
// Constructeurs / Destructeur
// ***************************************************************************

// ***************************************************************************
// Nom : Constructeur
// Description : Constructeur par defaut
// ***************************************************************************
// Constructeurs

CSynchronized::CSynchronized(CRecursiveMutex& recMutex)
	: m_pRecMutex(&recMutex), m_LockType(REC_MUTEX)
{
	m_pRecMutex->Lock();
}

CSynchronized::CSynchronized(CMonitor&        monitor)
	: m_pMonitor(&monitor), m_LockType(MONITOR)
{
	monitor.Lock(false);
}

// ***************************************************************************
// Nom : Destructeur
// Description : Detruit les donnees de l'objets courants
// ***************************************************************************
CSynchronized::~CSynchronized()
{
	switch (m_LockType) {
	case REC_MUTEX:
		m_pRecMutex->Unlock();
		break;
	case MONITOR:
		m_pMonitor->Unlock();
		break;
	default:
		assert(false);
	};
}
