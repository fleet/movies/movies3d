#include "M3DKernel/utils/multithread/ThreadUtils.h"

#ifdef  WIN32
#include <windows.h>
#include <processthreadsapi.h>
#endif //  WIN32

bool ThreadUtils::isThreadAlive(std::thread & t)
{
#ifdef WIN32
	bool alive = false;
	DWORD threadExitCode;
	auto res = GetExitCodeThread(t.native_handle(), &threadExitCode);
	if (res == TRUE)
	{
		if (threadExitCode == STILL_ACTIVE)
		{
			alive = true;
		}
	}
	return alive;
#else
	return t.joinable();
#endif
}