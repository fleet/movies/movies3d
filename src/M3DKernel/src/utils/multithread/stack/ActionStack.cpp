// -*- C++ -*-
// ****************************************************************************
// Class: ActionStack
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Sept 2009
// Soci�t� : IPSIS
// ****************************************************************************

#include <thread>
#include "M3DKernel/utils/multithread/stack/ActionStack.h"
#include "M3DKernel/utils/multithread/Barrier.h"
#include "M3DKernel/utils/multithread/ThreadUtils.h"

#include <assert.h>
#include <thread>


class ActionStack::Impl
{
public:
	Barrier barrier;
	std::thread thread;
};

// *********************************************************************
// Constructors / Destructor
// *********************************************************************
// d�fault constructor
ActionStack::ActionStack()
	: impl(new ActionStack::Impl)
{
	GetInternSynchroEvent().Post();
	GetExternSynchroEvent().Post();
	GetStackSizeEvent().Reset();
	m_bEnabled = true;
}

// Destructor
ActionStack::~ActionStack(void)
{
	Stop();
	delete impl;
}

// *********************************************************************
// Methods
// *********************************************************************

//add action to process
void ActionStack::ScheduleAction(StackAction* pAction)
{
	if (pAction != NULL && GetEnabled())
	{
		m_lock.Lock();
		GetActionStack().push_back(pAction);
		m_lock.Unlock();

		if (!IsAlive())
		{
			Launch();
		}
		//signal action to thread
		GetInputEvent().Post();
	}
}

//return the first Action
StackAction* ActionStack::PopAction()
{
	m_lock.Lock();

	assert(HasScheduledActions());
	StackAction* result = GetActionStack().front();
	GetActionStack().pop_front();

	m_lock.Unlock();

	//notify the stack size decreased
	GetStackSizeEvent().Post();
	
	return result;
}

//wait the stack size is under the limit
void ActionStack::WaitStackLimit(int stackLimit)
{
	if (IsAlive())
	{
		//wait while the size of the stack is over the limit
		while (GetStackSize() > stackLimit)
		{
			//wait the next stack event
			GetStackSizeEvent().Wait();
			GetStackSizeEvent().Reset();
		}
	}
}

//wait every treatment in the stack is finished
void ActionStack::WaitFinished()
{
	if (IsAlive())
	{
		//wait all the stacked actions are processed
		WaitStackLimit(0);
		//pause the stack to ensure the current action is finished
		PauseExecution();
		//resume stack execution
		ResumeExecution();
	}
}

bool ActionStack::IsAlive()
{
	return impl->barrier.isLocked();
}

bool ActionStack::IsActive()
{
	return impl->barrier.isLocked();
}

//get nb remaining actions
int ActionStack::GetStackSize() const
{
	int result = 0;

	m_lock.Lock();
	result = GetActionStack().size();
	m_lock.Unlock();

	return result;
}

//has remaining actions
bool ActionStack::HasScheduledActions() const
{
	return GetStackSize() > 0;
}

//Pause the execution of the input for synchro purpose
void ActionStack::PauseExecution()
{
	if (IsAlive())
	{
		//reset external synchro to make the thread wait
		GetExternSynchroEvent().Reset();

		if (impl->thread.get_id() != std::this_thread::get_id())
		{
			if (impl->thread.joinable()) 
			{
				GetInternSynchroEvent().Wait();
			}
		}
	}
}

//Resume the execution of the input for synchro purpose
void ActionStack::ResumeExecution()
{
	//reset external synchro to make the thread wait
	GetExternSynchroEvent().Post();
}

//*****************************************************************************
// Name : ThreadFunction
// Description : 
// Parameters : void*
// Return : void
//*****************************************************************************
unsigned int ThreadFunction(void* pArg)
{
	ActionStack* pStack = (ActionStack*)pArg;
	return pStack->Run();
}

unsigned int ActionStack::Run()
{
	BarrierLocker l(impl->barrier);

	//loop while stack is active
	while (this->IsActive())
	{
		this->GetInputEvent().Reset();

		//process all scheduled actions
		while (this->HasScheduledActions() && this->IsActive())
		{
			//reset intern synchro
			this->GetInternSynchroEvent().Reset();

			//get next action
			StackAction* pAction = this->PopAction();

			if (pAction != NULL)
			{
				try
				{
					pAction->Process();
				}
				catch (...)
				{
				}

				delete pAction;
			}

			//signal intern synchro
			this->GetInternSynchroEvent().Post();

			//check extern synchro is ok
			this->GetExternSynchroEvent().Wait();
		}

		//wait for actions to be scheduled
		this->GetInputEvent().Wait();
	}

	return 0;
}

//launch the worker thread
void ActionStack::Launch()
{
	GetInternSynchroEvent().Post();
	GetExternSynchroEvent().Post();
	GetInputEvent().Reset();
	GetStackSizeEvent().Reset();
	impl->thread = std::thread(&ThreadFunction, this);
	impl->thread.detach();
}

void ActionStack::Stop()
{
	// Ask for thread end
	bool isAlive = false;
	if (impl->barrier.isLocked())
	{
		isAlive = ThreadUtils::isThreadAlive(impl->thread);
	}

	if(isAlive)
	{
		impl->barrier.setState(false);
		GetInputEvent().Post();
		GetExternSynchroEvent().Post();

		// Wait thread end (join not working in matlab context, so use detach instead and use an event to ensure that th thread is finished)
		impl->barrier.waitUnlock();
	}
}


//reset
void ActionStack::Reset()
{
	//reset internal data
	m_lock.Lock();
	GetActionStack().clear();
	m_lock.Unlock();

	//wait the current action is performed (otherwise locks may remain locked)
	PauseExecution();
	//resume
	ResumeExecution();

	//release waiting threads
	GetInternSynchroEvent().Post();
	GetStackSizeEvent().Post();
}
