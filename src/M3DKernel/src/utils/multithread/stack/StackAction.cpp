// -*- C++ -*-
// ****************************************************************************
// Class: StackAction
//
// Description: Base classe for action to be stacked
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Sept 2009
// Soci�t� : IPSIS
// ****************************************************************************

#include "M3DKernel/utils/multithread/stack/StackAction.h"

// *********************************************************************
// Constructors / Destructor
// *********************************************************************
StackAction::StackAction()
{

}

// Destructor
StackAction::~StackAction(void)
{}
