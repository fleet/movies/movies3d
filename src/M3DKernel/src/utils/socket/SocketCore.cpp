#include "M3DKernel/utils/socket/SocketCore.h"

#include "M3DKernel/utils/multithread/RecursiveMutex.h"

#ifdef WIN32
#include <winsock2.h>
// Exclure les en-têtes Windows rarement utilisés
#define WIN32_LEAN_AND_MEAN

class CSocketCore::Impl
{
public:
    CRecursiveMutex  s_Lock;
    int              s_InitializeCounter = 0;
    WORD             s_wVersionRequested;
    WSADATA          s_SUSData;
};

// ***************************************************************************
// Constructeurs / Destructeur
// ***************************************************************************
CSocketCore::CSocketCore()
    : impl(new CSocketCore::Impl)
{
	InitializeSocketAPI();
}

CSocketCore::~CSocketCore()
{
	CleanSocketAPI();
    delete impl;
}

// ***************************************************************************
// M�thodes statiques
// ***************************************************************************
//int                   CSocketCore::impl->s_InitializeCounter = 0;
//WORD                  CSocketCore::s_wVersionRequested;
//WSADATA               CSocketCore::s_SUSData;
//CRecursiveMutex CSocketCore::s_Lock;

void CSocketCore::InitializeSocketAPI()
{
	impl->s_Lock.Lock();

	if (impl->s_InitializeCounter == 0)
	{
		impl->s_wVersionRequested = MAKEWORD(2, 2);
		int res = WSAStartup(impl->s_wVersionRequested, &impl->s_SUSData);
	}

	impl->s_InitializeCounter++;

	impl->s_Lock.Unlock();
}

void CSocketCore::CleanSocketAPI()
{
	impl->s_Lock.Lock();

	impl->s_InitializeCounter--;
	if (impl->s_InitializeCounter == 0)
	{
		WSACleanup();
	}

	impl->s_Lock.Unlock();
}

#else

CSocketCore::CSocketCore()
{
}

CSocketCore::~CSocketCore()
{
}

void CSocketCore::InitializeSocketAPI()
{
}

void CSocketCore::CleanSocketAPI()
{
}

#endif
