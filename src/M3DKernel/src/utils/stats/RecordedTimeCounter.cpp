#include "M3DKernel/utils/stats/RecordedTimeCounter.h"
#include "M3DKernel/utils/M3DStdUtils.h"

RecordedTimeCounter::RecordedTimeCounter(void)
{
}

RecordedTimeCounter::~RecordedTimeCounter(void)
{
	ClearAll();
}
void RecordedTimeCounter::ClearAll(void)
{
	m_mapNamed.clear();
}
void RecordedTimeCounter::AddTimeCounter(const char *Name, TimeCounter &refCount)
{
	m_Lock.Lock();
	std::string l_name = Name;
	MapNameTimeCounter::iterator res = m_mapNamed.find(l_name);
	if (res != m_mapNamed.end())
	{

		res->second.SetTime(refCount.getTime());
	}
	else
	{
		TimeCountStat a(refCount.getTime());
		m_mapNamed.insert(MapNameTimeCounter::value_type(l_name, a));
	}
	m_Lock.Unlock();
}
size_t RecordedTimeCounter::GetObjectCount(void) const
{
	m_Lock.Lock();
	size_t ret = m_mapNamed.size();
	m_Lock.Unlock();
	return ret;
}
bool RecordedTimeCounter::GetObjectDescWithIndex(unsigned int idx, char*Name, unsigned int maxDescLen, TimeCountStat &TimeDesc)
{
	m_Lock.Lock();
	MapNameTimeCounter::iterator res = m_mapNamed.begin();
	for (size_t i = 0; i < idx; i++)
		res++;
	if (res != m_mapNamed.end())
	{
		TimeDesc = res->second;
		strcpy_s(Name, maxDescLen, res->first.c_str());
		m_Lock.Unlock();
		return true;
	}
	return false;
	m_Lock.Unlock();
}
