#include "M3DKernel/utils/stats/TimeCountStat.h"
#include <algorithm>

TimeCountStat::TimeCountStat() {}

TimeCountStat::TimeCountStat(TimeCount ref) 
{ 
	m_Time = m_Max = m_Min = ref; 
}

TimeCountStat::~TimeCountStat() {}

void TimeCountStat::SetTime(TimeCount a) 
{ 
	m_Time = a; 
	m_Max = std::max<TimeCount>(m_Time, m_Max); 
	m_Min = std::min<TimeCount>(m_Time, m_Min); 
}
