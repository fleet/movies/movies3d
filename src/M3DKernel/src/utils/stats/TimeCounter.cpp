#include "M3DKernel/utils/stats/TimeCounter.h"

#include <chrono>

class TimeCounter::Impl
{
public:
	TimeCount m_mseconds;
	std::chrono::steady_clock::time_point start;
};

TimeCounter::TimeCounter()
  : impl(new TimeCounter::Impl)
{
	impl->m_mseconds = 0;
}

TimeCounter::TimeCounter(const TimeCounter&src)
	: TimeCounter()
{
	impl->m_mseconds = src.impl->m_mseconds;
	impl->start = src.impl->start;
}

TimeCounter::~TimeCounter()
{
	delete impl;
}

void TimeCounter::StartCount()
{
	impl->start = std::chrono::steady_clock::now();
}

void TimeCounter::Reset()
{
	impl->m_mseconds = 0;
}


TimeCount TimeCounter::StopCount()
{
	auto end = impl->start = std::chrono::steady_clock::now();
	impl->m_mseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - impl->start).count();
	return impl->m_mseconds;
}

TimeCount TimeCounter::getTime() const
{
	return impl->m_mseconds;
}

TimeCounter& TimeCounter::operator=(const TimeCounter& src)
{
	impl->m_mseconds = src.getTime();
	impl->start = src.impl->start;
	return *this;
}
