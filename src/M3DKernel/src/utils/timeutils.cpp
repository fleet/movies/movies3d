﻿#include "M3DKernel/utils/timeutils.h"

#include <iomanip>
#include <sstream>
#include <time.h>
#include "M3DKernel/utils/M3DStdUtils.h"

namespace {
	constexpr unsigned int daysFromJanuaryFirst[12] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };
}

std::string timeutils::timePointToStringUTC(const std::chrono::time_point<std::chrono::system_clock>& date, const char* dateFormat) {
	const auto time = std::chrono::system_clock::to_time_t(date);
	std::tm utcTm = {};
#ifdef WIN32
	gmtime_s(&utcTm, &time);
#else
	gmtime_r(&time, &utcTm);
#endif

	std::stringstream ss;
	ss << std::put_time(&utcTm, dateFormat);
	return ss.str();
}

std::string timeutils::timePointToStringLocal(const std::chrono::time_point<std::chrono::system_clock>& date, const char * dateFormat) {
	const auto time = std::chrono::system_clock::to_time_t(date);
	std::tm localTime = {};
#ifdef WIN32
	localtime_s(&localTime, &time);
#else
	localtime_r(&time, &localTime);
#endif

	std::stringstream ss;
	ss << std::put_time(&localTime, dateFormat);
	return ss.str();
}

std::chrono::time_point<std::chrono::system_clock> timeutils::timePointFromStringUTC(const std::string & dateStr, const char* dateFormat) {
	std::tm utcTm = {};
	std::stringstream ss(dateStr);
	ss >> std::get_time(&utcTm, dateFormat);

	const auto utcTm_t = utcTimeT(utcTm);
	return std::chrono::system_clock::from_time_t(utcTm_t);
}

std::chrono::time_point<std::chrono::system_clock> timeutils::timePointFromStringLocal(const std::string & dateStr, const char * dateFormat) {
	std::tm localTime = {};
	std::stringstream ss(dateStr);
	ss >> std::get_time(&localTime, dateFormat);

	const auto utcTm_t = std::mktime(&localTime);
	return std::chrono::system_clock::from_time_t(utcTm_t);
}

time_t timeutils::utcTimeT(const tm & utcTm) {
	const auto daysInYear = daysFromJanuaryFirst[utcTm.tm_mon] + utcTm.tm_mday - 1;
	const auto daysFromEpoch = 
		(utcTm.tm_year - 70) * 365 + 
		countLeapYearsFromEpoch(1900 + utcTm.tm_year - (utcTm.tm_mon > 2 ? 0 : 1)) + 
		daysInYear;
	return daysFromEpoch * (60 * 60 * 24) + utcTm.tm_hour * (60 * 60) + utcTm.tm_min * 60 + utcTm.tm_sec;
}

unsigned int timeutils::countLeapYearsFrom0(const unsigned int & year) {
	if (year < 0) {
		return 0;
	}
	return year / 4 - year / 100 + year / 400;
}

unsigned int timeutils::countLeapYearsFromEpoch(const unsigned int & year) {
	return countLeapYears(1970, year);
}

unsigned int timeutils::countLeapYears(const unsigned int & startYear, const unsigned int & endYear) {
	if (endYear <= startYear) {
		return 0;
	}
	return countLeapYearsFrom0(endYear) - countLeapYearsFrom0(startYear);
}
