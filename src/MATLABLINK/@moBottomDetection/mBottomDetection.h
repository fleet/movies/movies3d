#pragma once

#include "mCommon.h"
#include "BottomDetection/BottomDetectionModule.h"

static void mSetBottomDetectionParameterDesc(BottomDetectionModule* pModule, const mxArray* input)
{
	mxArray* pDef = mxGetField(input, 0, "m_bIsEnable");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		pModule->setEnable(value);
	}
	const std::vector<BottomDetectionAlgorithm*>& algos = pModule->GetBottomDetectionParameter().GetAlgorithmDefinition();
	int nbEnabledAlgos = 0;
	for (size_t i = 0; i < algos.size(); i++)
	{
		char name[255];
		sprintf(name, "%s_%d", "BottomDetectionAlgorithmState", i);
		mxArray* pDef = mxGetField(input, 0, name);
		if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
		{
			mexErrMsgTxt("Bad Structure");
		}
		else
		{
			double value = *mxGetPr(pDef);
			bool enabled = (bool)value;
			if (enabled)
			{
				nbEnabledAlgos++;
				if (nbEnabledAlgos == 1)
				{
					pModule->GetBottomDetectionParameter().SetActivatedAlgo(algos[i]->GetAlgorithmType());
				}
				else
				{
					mexErrMsgTxt("Only one Bottom Detection algorithm can be enabled.");
				}
			}
		}
	}
	if (nbEnabledAlgos == 0)
	{
		mexWarnMsgTxt("No Bottom Detection algorithm enabled. Using HAC bottom.");
		pModule->GetBottomDetectionParameter().SetActivatedAlgo(eBDNone);
	}
}

static mxArray* mGetBottomDetectionParameterDesc(BottomDetectionModule* pModule)
{
	const char* field_names[] = {
	  "m_bIsEnable",
	};
	mwSize dims[2] = { 1, 1 };

	mxArray* Array = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	mxArray* field_value;

	int field = mxGetFieldNumber(Array, "m_bIsEnable");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pModule->getEnable();

	mxSetFieldByNumber(Array, 0, field, field_value);

	const std::vector<BottomDetectionAlgorithm*>& algos = pModule->GetBottomDetectionParameter().GetAlgorithmDefinition();
	for (size_t i = 0; i < algos.size(); i++)
	{
		char name[255];
		sprintf(name, "%s_%d", "BottomDetectionAlgorithmState", i);
		field = mxAddField(Array, name);
		field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
		bool enabled = pModule->GetBottomDetectionParameter().GetActivatedAlgo() == algos[i];
		*mxGetPr(field_value) = enabled;
		mxSetFieldByNumber(Array, 0, field, field_value);

		sprintf(name, "%s_%d", "BottomDetectionAlgorithmDesc", i);
		field = mxAddField(Array, name);
		field_value = mxCreateString(algos[i]->GetAlgorithmName().c_str());
		mxSetFieldByNumber(Array, 0, field, field_value);
	}

	return Array;
}
