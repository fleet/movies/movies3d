function p = moBottomDetection(a)
if nargin == 0
   p.Enable=1;
   p = class(p,'moBottomDetection');

   
elseif isa(a,'moBottomDetection')
   p = a;
else
   p.Enable=1;
   p = class(p,'moBottomDetection');

end
