#include "mBottomDetection.h"
#include "mM3DKernel.h"

#include "ModuleManager/ModuleManager.h"
#include "BottomDetection/BottomDetectionModule.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	Init();
	CModuleManager* pInstance = CModuleManager::getInstance();
	plhs[0] = mGetBottomDetectionParameterDesc(pInstance->GetBottomDetectionModule());
}
