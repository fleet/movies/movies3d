#include "mBottomDetection.h"
#include "mM3DKernel.h"

#include "ModuleManager/ModuleManager.h"
#include "BottomDetection/BottomDetectionModule.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	Init();
	CModuleManager* pInstance = CModuleManager::getInstance();
	if (nrhs != 2) {
		mexErrMsgTxt("Two Input argument needed : the struct object, and the parameter definition structure");
	}

	if (mxIsClass(prhs[0], "moBottomDetection") != 1)
		mexErrMsgTxt("Input 0 Bad Class Argument");

	if (mxIsStruct(prhs[1]) != 1)
		mexErrMsgTxt("Input 1 Bad structure argument");

	mSetBottomDetectionParameterDesc(pInstance->GetBottomDetectionModule(), prhs[1]);
}
