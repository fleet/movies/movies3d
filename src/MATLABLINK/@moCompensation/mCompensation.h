#pragma once

#include "mCommon.h"

#include "M3DKernel/module/ModuleOutputConsumer.h"
#include "Compensation/CompensationModule.h"

static  void mSetCompensationParameterDesc(OverLapParameter& refParam, const mxArray* input)
{
	mxArray* pDef = mxGetField(input, 0, "m_bIsEnable");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		refParam.SetEnable(value);
	}
	pDef = mxGetField(input, 0, "m_gridSpaceZ");

	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		refParam.SetGridSpaceZ(value);
	}

	pDef = mxGetField(input, 0, "m_moduleReferenceThreshold");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		refParam.SetReferenceThreshold((DataFmt)(value * 100));
	}

	pDef = mxGetField(input, 0, "m_moduleDataThreshold");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		refParam.SetDataThreshold((DataFmt)(value * 100));
	}

	pDef = mxGetField(input, 0, "m_pingFilteredStart");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		refParam.m_pingFilteredStart = value;
	}

	pDef = mxGetField(input, 0, "m_pingFilteredStop");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		refParam.m_pingFilteredStop = value;
	}

	pDef = mxGetField(input, 0, "m_referenceWindowsPingCount");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		refParam.m_referenceWindowsPingCount = value;
	}

	pDef = mxGetField(input, 0, "m_spaceXYAngleFactor");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		refParam.SetGridSpaceXYAngleFactor(DEG_TO_RAD(value));
	}
}

static mxArray* mGetCompensationParameterDesc(OverLapParameter& refParam)
{
	const char* field_names[] = {
		 "m_bIsEnable",
		 "m_gridSpaceZ",
		 "m_spaceXYAngleFactor",

		 "m_moduleReferenceThreshold",
		 "m_moduleDataThreshold",
		 "m_pingFilteredStart",
		 "m_pingFilteredStop",
		 "m_referenceWindowsPingCount"
	};
	mwSize dims[2] = { 1, 1 };

	mxArray* Array = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	mxArray* field_value;

	int field = mxGetFieldNumber(Array, "m_bIsEnable");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = refParam.IsEnable();
	mxSetFieldByNumber(Array, 0, field, field_value);

	field = mxGetFieldNumber(Array, "m_gridSpaceZ");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = refParam.GetGridSpaceZ();
	mxSetFieldByNumber(Array, 0, field, field_value);

	field = mxGetFieldNumber(Array, "m_moduleReferenceThreshold");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = refParam.GetReferenceThreshold() / 100.0;
	mxSetFieldByNumber(Array, 0, field, field_value);

	field = mxGetFieldNumber(Array, "m_moduleDataThreshold");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = refParam.GetDataThreshold() / 100.0;
	mxSetFieldByNumber(Array, 0, field, field_value);

	field = mxGetFieldNumber(Array, "m_pingFilteredStart");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = refParam.m_pingFilteredStart;
	mxSetFieldByNumber(Array, 0, field, field_value);

	field = mxGetFieldNumber(Array, "m_pingFilteredStop");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = refParam.m_pingFilteredStop;
	mxSetFieldByNumber(Array, 0, field, field_value);

	field = mxGetFieldNumber(Array, "m_referenceWindowsPingCount");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = refParam.m_referenceWindowsPingCount;
	mxSetFieldByNumber(Array, 0, field, field_value);

	field = mxGetFieldNumber(Array, "m_spaceXYAngleFactor");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = RAD_TO_DEG(refParam.GetGridSpaceXYAngleFactor());
	mxSetFieldByNumber(Array, 0, field, field_value);

	return Array;
}
