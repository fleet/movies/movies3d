function p = moCompensation(a)
if nargin == 0
   p.Enable=1;
   p = class(p,'moCompensation');

   
elseif isa(a,'moCompensation')
   p = a;
else
   p.Enable=1;
   p = class(p,'moCompensation');

end
