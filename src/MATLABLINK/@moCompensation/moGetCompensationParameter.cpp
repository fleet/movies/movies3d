#include "mCompensation.h"
#include "mM3DKernel.h"

#include "ModuleManager/ModuleManager.h"
#include "Compensation/CompensationModule.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	Init();
	CModuleManager* pInstance = CModuleManager::getInstance();
	plhs[0] = mGetCompensationParameterDesc(pInstance->GetCompensationModule()->GetCompensationParameter().m_overlapParameter);
}
