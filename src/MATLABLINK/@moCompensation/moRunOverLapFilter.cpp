#include "mCompensation.h"
#include "mM3DKernel.h"

#include "ModuleManager/ModuleManager.h"
#include "Compensation/CompensationModule.h"

void __stdcall progressCB(const char*, int)
{
  // pas de remont�e de progression sous matlab. (on pourrait rajouter une sortie texte...)
}

bool __stdcall cancelCB()
{
  // pas d'annulation possible sous MatLab.
  return false;
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray*prhs[] )
{
	Init();
	CModuleManager *pInstance=CModuleManager::getInstance();
	pInstance->GetCompensationModule()->RunOverLapFilter((PROGRESS_CALLBACK)(void*)progressCB, (CANCEL_CALLBACK)(void*)cancelCB);
}
