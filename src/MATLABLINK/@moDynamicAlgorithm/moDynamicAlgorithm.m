function p = moDynamicAlgorithm(a)
if nargin == 0
   p.AlgId=0;
   p = class(p,'moDynamicAlgorithm');
   p.AlgId=moCreateAlgo(p);
elseif isa(a,'moDynamicAlgorithm')
   p = a;
end