#include "HacAlg.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	char* input_buf;
	Init();

	if (nrhs != 2) {
		mexErrMsgTxt("Two Input argument needed : the source object, and the port number");
	}
	if (mxIsClass(prhs[0], "moDynamicAlgorithm") != 1)
		mexErrMsgTxt("Input 0 Bad Class Argument");

	if (mxIsDouble(prhs[1]) != 1)
		mexErrMsgTxt("Input 0 Bad Class Argument");

	double port;
	memcpy(&port, mxGetData(prhs[1]), mxGetElementSize(prhs[1]));


	mxArray* pa;
	int field_num = mxGetFieldNumber(prhs[0], "AlgId");
	pa = mxGetFieldByNumber(prhs[0], 0, field_num);

	double id;
	memcpy(&id, mxGetData(pa), mxGetElementSize(pa));

	setInputPortToRoot(id, port);
}
