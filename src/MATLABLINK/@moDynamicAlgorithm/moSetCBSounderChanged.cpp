#include "HacAlg.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	char* input_buf;
	Init();

	if (nrhs != 2) {
		mexErrMsgTxt("Two Input argument needed : the  object, and the CB Name");
	}
	if (mxIsClass(prhs[0], "moDynamicAlgorithm") != 1)
		mexErrMsgTxt("Input 0 Bad Class Argument");
	//// input must be a string 
	if (mxIsChar(prhs[1]) != 1)
		mexErrMsgTxt("Input 1 must be a string.");
	//// input must be a row vector 
	if (mxGetM(prhs[1]) != 1)
		mexErrMsgTxt("Input 1 must be a row vector.");
	/// get the length of the input string 

	/// copy the string data from prhs[0] into a C string input_ buf.    /
	input_buf = mxArrayToString(prhs[1]);
	if (input_buf == NULL)
		mexErrMsgTxt("Could not convert input to string.");

	/*
	number_of_fields = mxGetNumberOfFields(prhs[0]);
	mexPrintf("This object contains the following fields:\n");
	mexPrintf("name\t\tclass\t\tvalue\n");
	mexPrintf("-------------------------------------\n");
	for (field_num=0; field_num<number_of_fields; field_num++){
		mxArray *pa;
		mexPrintf("%s", mxGetFieldNameByNumber(prhs[0], field_num));
		pa = mxGetFieldByNumber(prhs[0], 0, field_num);
		mexPrintf("\t\t%s\t\t", mxGetClassName(pa));
		mexCallMATLAB(0, NULL, 1, &pa, "disp");
	}
	*/
	mxArray* pa;
	int field_num = mxGetFieldNumber(prhs[0], "AlgId");
	pa = mxGetFieldByNumber(prhs[0], 0, field_num);

	double id;
	memcpy(&id, mxGetData(pa), mxGetElementSize(pa));

	DynamicMatlabAlgo* p = (DynamicMatlabAlgo*)M3DKernel::GetInstance()->m_MatlabAlgoStore.GetAlgorithm((unsigned int)id);
	if (p)
	{
		p->SetCBSounder(input_buf);
	}
	else
	{
		mexErrMsgTxt("Algorithm Not Found, check id");
	}
	mxFree(input_buf);
}
