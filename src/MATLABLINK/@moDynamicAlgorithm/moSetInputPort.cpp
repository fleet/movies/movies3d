#include "HacAlg.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	Init();
	if (nrhs != 3) {
		mexErrMsgTxt("Three Input argument needed : the source object, the producer Object and the port number");
	}
	if (mxIsClass(prhs[0], "moDynamicAlgorithm") != 1)
		mexErrMsgTxt("Input 0 Bad Class Argument");

	if (mxIsClass(prhs[1], "moDynamicAlgorithm") != 1)
		mexErrMsgTxt("Input 1 Bad Class Argument");

	if (mxIsDouble(prhs[2]) != 1)
		mexErrMsgTxt("Input 2 Bad Class Argument");

	double port;
	memcpy(&port, mxGetData(prhs[2]), mxGetElementSize(prhs[2]));


	mxArray* pa;
	int field_num = mxGetFieldNumber(prhs[0], "AlgId");
	pa = mxGetFieldByNumber(prhs[0], 0, field_num);

	double SourceId;
	memcpy(&SourceId, mxGetData(pa), mxGetElementSize(pa));

	field_num = mxGetFieldNumber(prhs[1], "AlgId");
	pa = mxGetFieldByNumber(prhs[1], 0, field_num);

	double ProducerId;
	memcpy(&ProducerId, mxGetData(pa), mxGetElementSize(pa));

	setInputPort(SourceId, port, ProducerId);
}
