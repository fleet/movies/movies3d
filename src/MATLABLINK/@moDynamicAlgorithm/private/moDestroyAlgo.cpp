
#include "HacAlg.h"
#include "Hac.h"

/* Input Arguments */



/* Output Arguments */

/* Output Arguments */


void mexFunction( int nlhs, mxArray *plhs[],
int nrhs, const mxArray*prhs[] )

{
    Init();
    
    if (nrhs != 1) {
        mexErrMsgTxt("One Input argument needed");
    }
    if ( mxIsClass(prhs[0],"moDynamicAlgorithm") != 1)
        mexErrMsgTxt("Input 0 Bad Class Argument");
    
    mxArray *pa;
    int field_num=mxGetFieldNumber(prhs[0], "AlgId");
    pa = mxGetFieldByNumber(prhs[0], 0, field_num);

    double id;
    memcpy(&id, mxGetData(pa), mxGetElementSize(pa));
 
    
    
    unsigned int num=id;
   
    removeAlgo(num);
     
    return;
    
}
