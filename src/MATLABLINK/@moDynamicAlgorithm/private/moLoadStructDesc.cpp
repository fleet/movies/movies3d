#include "HacAlg.h"
#include "Hac.h"

/* Input Arguments */



/* Output Arguments */

/* Output Arguments */


void mexFunction( int nlhs, mxArray *plhs[],
int nrhs, const mxArray*prhs[] )

{
    
    
    unsigned char *start_of_pr;
    
  if (nrhs != 1) {
        mexErrMsgTxt("1 Input argument needed : the  object");
    }
    if ( mxIsClass(prhs[0],"moDynamicAlgorithm") != 1)
        mexErrMsgTxt("Input 0 Bad Class Argument");
    //// input must be a string 
    
 
    Init();
    mxArray *pa;
    int field_num=mxGetFieldNumber(prhs[0], "AlgId");
    pa = mxGetFieldByNumber(prhs[0], 0, field_num);

    double id;
    memcpy(&id, mxGetData(pa), mxGetElementSize(pa));
 
    
    
    DynamicMatlabAlgo *p=(DynamicMatlabAlgo *) M3DKernel::GetInstance()->m_MatlabAlgoStore.GetAlgorithm((unsigned int) id);
    if(p)
    {
        plhs[0] =p->getDesc();
    }
    else
    {
         mexErrMsgTxt("Algorithm Not Found, check id");
    }
    
    return;
    
}
