#pragma once

#include "mCommon.h"
#include "mM3DKernel.h"

#include "ModuleManager/ModuleManager.h"
#include "EchoIntegration/EchoIntegrationOutput.h"
#include "M3DKernel/module/ModuleOutputConsumer.h"
#include "M3DKernel/datascheme/CurrentSounderDefinition.h"
#include "EchoIntegration/EchoIntegrationModule.h"
#include "EchoIntegration/LayerDef.h"

static void CreateMatlabConsumer()
{
	CModuleManager *pInstance = CModuleManager::getInstance();
	ModuleOutputConsumer *pConsumer = pInstance->GetEchoIntegrationModule()->GetMatlabModuleConsumer();
}

// r?cup?ration des r?sultats du module
static mxArray * getOutPutDesc()
{
	unsigned int x;///
	unsigned int j;///

	CModuleManager *pInstance = CModuleManager::getInstance();
	EchoIntegrationModule *pEIModule = pInstance->GetEchoIntegrationModule();
	ModuleOutputConsumer *pEIConsumer = pEIModule->GetMatlabModuleConsumer();

	mxArray * retDesc = NULL;
	if (!pEIModule || !pEIConsumer)
		return NULL;

	unsigned int count = pEIModule->GetOutputContainer().GetOutPutCount(pEIConsumer->GetConsumerId());

	const char *field_names[] = { "m_Ni","m_Nt","m_Sv","m_Sa","m_latitude","m_longitude","m_Depth","m_time", "m_freq" };
	mwSize dims[2] = { 1, count };

	M3D_LOG_INFO("MatlabLink", Log::format("Count: %d, dims[0] = %d, dims[1] = %d", count, dims[0], dims[1]));
	retDesc = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	//retDesc=   mxCreateStructMatrix (2, 2, NUMBER_OF_FIELDS, field_names);


	for (unsigned int i = 0; i < count; i++)
	{
		EchoIntegrationOutput *pOut = (EchoIntegrationOutput*)pEIModule->GetOutputContainer().GetOutPut(pEIConsumer->GetConsumerId(), i);

		//////////
		// On r?cup?re le nombre de faisceaux et d'?chos
		M3D_LOG_INFO("MatlabLink", Log::format("Count: %d/%d", i, count));
		unsigned int nbChannel;
		unsigned int nbLayer;

		double ni;
		double nt;
		double sv;
		double sa;
		double latitude;
		double longitude;
		double depth;

		nbChannel = pOut->m_tabChannelResult.size();
		nbLayer = pEIModule->GetEchoIntegrationParameter().countLayersDef();
		M3D_LOG_INFO("MatlabLink", Log::format("nbChannels: %d, nbLayers: %d", nbChannel, nbLayer));


		mwSize fieldDims[2] = { 1, nbChannel };
		mxArray *field_value1 = mxCreateCellArray(2, fieldDims);
		mxArray *field_value2 = mxCreateCellArray(2, fieldDims);
		mxArray *field_value3 = mxCreateCellArray(2, fieldDims);
		mxArray *field_value4 = mxCreateCellArray(2, fieldDims);
		mxArray *field_value5 = mxCreateCellArray(2, fieldDims);
		mxArray *field_value6 = mxCreateCellArray(2, fieldDims);
		mxArray *field_value7 = mxCreateCellArray(2, fieldDims);
		mxArray *field_value8 = mxCreateDoubleMatrix(1, 1, mxREAL);
		mxArray *field_value9 = mxCreateCellArray(2, fieldDims);


		// Pour chaque faisceau
		for (x = 0; x < nbChannel; x++)
		{
			int k = 0;
			int nbFreq = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies().size();
			M3D_LOG_INFO("MatlabLink", Log::format("nbFreq: %d", nbFreq));

			mxArray * ni_array = mxCreateDoubleMatrix(nbLayer, nbFreq, mxREAL);
			mxArray * nt_array = mxCreateDoubleMatrix(nbLayer, nbFreq, mxREAL);
			mxArray * sv_array = mxCreateDoubleMatrix(nbLayer, nbFreq, mxREAL);
			mxArray * sa_array = mxCreateDoubleMatrix(nbLayer, nbFreq, mxREAL);
			mxArray * lat_array = mxCreateDoubleMatrix(nbLayer, 1, mxREAL);
			mxArray * long_array = mxCreateDoubleMatrix(nbLayer, 1, mxREAL);
			mxArray * depth_array = mxCreateDoubleMatrix(nbLayer, 1, mxREAL);
			mxArray * freq_array = mxCreateDoubleMatrix(1, nbFreq, mxREAL);

			for (int f = 0; f < nbFreq; f++)
			{
				*(mxGetPr(freq_array) + f) = pOut->m_tabChannelResult[x]->m_tabFrequencies[f];
				// Pour chaque couche
				for (j = 0; j < nbLayer; j++)
				{
					ni = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[f][j]->GetNi();
					nt = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[f][j]->GetNt();
					sv = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[f][j]->GetSv();
					sa = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[f][j]->GetSa();

					*(mxGetPr(ni_array) + k) = ni;
					*(mxGetPr(nt_array) + k) = nt;
					*(mxGetPr(sv_array) + k) = sv;
					*(mxGetPr(sa_array) + k) = sa;

					if (f == 0)
					{
						latitude = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[f][j]->GetLatitude();
						longitude = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[f][j]->GetLongitude();
						depth = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[f][j]->GetDepth();
						*(mxGetPr(lat_array) + k) = latitude;
						*(mxGetPr(long_array) + k) = longitude;
						*(mxGetPr(depth_array) + k) = depth;
					}
					k++;
				}
			}
			mxSetCell(field_value1, x, ni_array);
			mxSetCell(field_value2, x, nt_array);
			mxSetCell(field_value3, x, sv_array);
			mxSetCell(field_value4, x, sa_array);
			mxSetCell(field_value5, x, lat_array);
			mxSetCell(field_value6, x, long_array);
			mxSetCell(field_value7, x, depth_array);
			mxSetCell(field_value9, x, freq_array);

		}
		*mxGetPr(field_value8) = pOut->m_timeEnd.m_TimeCpu + pOut->m_timeEnd.m_TimeFraction / 10000.0;
		mxSetFieldByNumber(retDesc, i, 0, field_value1);
		mxSetFieldByNumber(retDesc, i, 1, field_value2);
		mxSetFieldByNumber(retDesc, i, 2, field_value3);
		mxSetFieldByNumber(retDesc, i, 3, field_value4);
		mxSetFieldByNumber(retDesc, i, 4, field_value5);
		mxSetFieldByNumber(retDesc, i, 5, field_value6);
		mxSetFieldByNumber(retDesc, i, 6, field_value7);
		mxSetFieldByNumber(retDesc, i, 7, field_value8);
		mxSetFieldByNumber(retDesc, i, 8, field_value9);

		//////////
	}
	pEIModule->GetOutputContainer().Flush(pEIConsumer->GetConsumerId());
	return retDesc;
}

// r?cup?ration d'une matrice d?finissant les couches
static mxArray * getLayersDesc()
{
	CModuleManager *pInstance = CModuleManager::getInstance();
	EchoIntegrationModule *pEIModule = pInstance->GetEchoIntegrationModule();

	mxArray * retDesc = NULL;
	if (!pEIModule)
		return NULL;

	const auto& layers = pEIModule->GetEchoIntegrationParameter().getLayersDefs();
	const auto count = layers.size();

	const char *field_names[] = { "StartDepth","StopDepth","LayerType" };
	mwSize dims[2] = { 1, count };
	retDesc = mxCreateStructArray(1, dims, NUMBER_OF_FIELDS, field_names);
	
	mxArray *field_value1 = mxCreateDoubleMatrix(1, count, mxREAL);
	mxArray *field_value2 = mxCreateDoubleMatrix(1, count, mxREAL);
	mxArray *field_value3 = mxCreateDoubleMatrix(1, count, mxREAL);

	// Pour chaque couche
	for (size_t j = 0; j < count; ++j)
	{
		auto & layerDef = layers[j];

		// si c'est une couche de surface
		switch (layerDef->GetLayerType())
		{
		case Layer::Type::SurfaceLayer:
			*(mxGetPr(field_value1) + j) = layerDef->GetStart();
			*(mxGetPr(field_value2) + j) = layerDef->GetEnd();
			*(mxGetPr(field_value3) + j) = 0; // 0 pour surface
			break;

		case Layer::Type::BottomLayer:
			*(mxGetPr(field_value1) + j) = layerDef->GetStart();
			*(mxGetPr(field_value2) + j) = layerDef->GetEnd();
			*(mxGetPr(field_value3) + j) = 1; // 1 pour fond
			break;

		case Layer::Type::DistanceLayer:
			*(mxGetPr(field_value1) + j) = layerDef->GetStart();
			*(mxGetPr(field_value2) + j) = layerDef->GetEnd();
			*(mxGetPr(field_value3) + j) = 2; // 2 pour distance
			break;
		}
	}

	mxSetFieldByNumber(retDesc, 0, 0, field_value1);
	mxSetFieldByNumber(retDesc, 0, 1, field_value2);
	mxSetFieldByNumber(retDesc, 0, 2, field_value3);

	return retDesc;
}


// r?cup?ration d'une matrice contenant le volume de l'ensemble des couches pour chaque faisceau
static mxArray * getLayersVolumes()
{
	CModuleManager *pInstance = CModuleManager::getInstance();
	EchoIntegrationModule *pEIModule = pInstance->GetEchoIntegrationModule();

	M3DKernel * pKern = M3DKernel::GetInstance();

	mxArray * retDesc = NULL;
	if (!pEIModule)
		return NULL;

	// couches
	const auto& layers = pEIModule->GetEchoIntegrationParameter().getLayersDefs();
	const auto nbLayers = layers.size();

	const char *field_names[] = { "Sounders" };
	mwSize dims[2] = { 1, nbLayers };
	retDesc = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

	// nombre de sondeurs
	unsigned int nbSounders = pKern->getObjectMgr()->GetSounderDefinition().GetNbSounder();

	for (size_t layerIdx = 0; layerIdx < nbLayers; ++layerIdx)
	{
		// cr?ation du tableau des volumes du sondeur pour la couche layerIdx
		const char *field_names_sounder[] = { "Channels" };
		mwSize dims[2] = { 1, nbSounders };
		mxArray * sounderDesc = mxCreateStructArray(2, dims, 1, field_names_sounder);

		auto & layerDef = layers[layerIdx];

		for (unsigned int sounderIdx = 0; sounderIdx < nbSounders; sounderIdx++)
		{
			Sounder * pSound = pKern->getObjectMgr()->GetSounderDefinition().GetSounder(sounderIdx);
			unsigned int nbTrans = pSound->GetTransducerCount();

			// creation du tableau des volumes pour chaque channel
			unsigned int globalChannelNb = 0;
			for (unsigned int i = 0; i < nbTrans; i++)
			{
				globalChannelNb += pSound->GetTransducer(i)->m_numberOfSoftChannel;
			}

			mxArray *channelDesc = mxCreateDoubleMatrix(1, globalChannelNb, mxREAL);

			globalChannelNb = 0;

			for (unsigned int transIdx = 0; transIdx < nbTrans; transIdx++)
			{
				Transducer * pTrans = pSound->GetTransducer(transIdx);
				unsigned int nbChannel = pTrans->m_numberOfSoftChannel;

				for (unsigned int channelIdx = 0; channelIdx < nbChannel; channelIdx++)
				{
					double volume = 0.0;
					SoftChannel * pSoftChan = pTrans->getSoftChannelPolarX(channelIdx);
					// r?cup?ration de la sonde pour le dernier ping
		  // OTK - 09/03/2010 - en cas d'absence de pings pour un sondeur, on ne d?clenche pas d'erreur bloquante, mais simplement
		  // un warning (le cas o? un sondeur n'?met pas ne doit pas bloquer l'utilisation des autres sondeurs)
		  // le volume sera nul pour les sondeurs sans ping.
					TimeObjectContainer *pCont = pKern->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(pSound->m_SounderId);
					if (!pCont)
					{
						mexWarnMsgTxt("No PingFan Container...");
					}
					else
					{
						PingFan * lastPingFan = (PingFan*)pCont->GetDatedObject(pCont->GetObjectCount() - 1);
						if (!lastPingFan)
						{
							mexWarnMsgTxt("No PingFan In PingFan Container...");
						}
						else
						{
							std::uint32_t echoFondEval = 0;
							std::int32_t bottomRange = 0;
							bool found = false;
							lastPingFan->getBottom(pTrans->GetChannelId()[channelIdx], echoFondEval, bottomRange, found);
							double bottom = pSound->GetSoftChannelCoordinateToWorldCoord(lastPingFan, transIdx, channelIdx, BaseMathLib::Vector3D(0, 0, bottomRange / 1000.0)).z;

							double minDepth, maxDepth;
							// cas des couches d?finies en distance au transducteur
							if (layerDef->GetLayerType() == Layer::Type::DistanceLayer)
							{
								minDepth = layerDef->GetMinDepth(0);
								maxDepth = layerDef->GetMaxDepth(0);
							}
							else
							{
								// calcul des profondeurs r?f?renc?es transducteur et d?point?es
								double offset = pTrans->m_transDepthMeter + lastPingFan->getHeaveChan(pSoftChan->m_softChannelId);
								minDepth = layerDef->GetMinDepth(bottom) - offset;
								maxDepth = layerDef->GetMaxDepth(bottom) - offset;
								minDepth = std::max<double>(0.0, minDepth);
								maxDepth = std::max<double>(0, maxDepth);
								// application du d?pointage transversal
								minDepth = minDepth / cos(pSoftChan->m_mainBeamAthwartSteeringAngleRad);
								maxDepth = maxDepth / cos(pSoftChan->m_mainBeamAthwartSteeringAngleRad);
							}
							// calcul du volume - la profondeur est deja d?finie par rapport au transducteur, et d?point?e
							volume = (PI / 3.)*tan(pSoftChan->m_beam3dBWidthAlongRad / 2.)*tan(pSoftChan->m_beam3dBWidthAthwartRad / 2.)
								*((maxDepth*maxDepth*maxDepth) - (minDepth*minDepth*minDepth));
						}
					}

					// remplissage du tableau r?sultat
					*(mxGetPr(channelDesc) + globalChannelNb) = volume;

					globalChannelNb++;
				}

			}

			int field = mxGetFieldNumber(sounderDesc, "Channels");
			mxSetFieldByNumber(sounderDesc, sounderIdx, field, channelDesc);
		}

		int field = mxGetFieldNumber(retDesc, "Sounders");
		mxSetFieldByNumber(retDesc, layerIdx, field, sounderDesc);
	}

	return retDesc;
}
