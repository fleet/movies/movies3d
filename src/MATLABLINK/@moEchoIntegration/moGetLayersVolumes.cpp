#include "mEchoIntegration.h"
#include "mM3DKernel.h"

#include "EchoIntegration/EchoIntegrationModule.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	Init();
	plhs[0] = getLayersVolumes();
}
