#include "mEchoIntegration.h"
#include "mM3DKernel.h"

#include "ModuleManager/ModuleManager.h"
#include "EchoIntegration/EchoIntegrationModule.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	unsigned char* start_of_pr;

	Init();
	CreateMatlabConsumer();

	CModuleManager* pInstance = CModuleManager::getInstance();
	pInstance->GetEchoIntegrationModule()->setEnable(false);
}
