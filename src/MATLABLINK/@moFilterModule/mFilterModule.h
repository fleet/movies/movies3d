#pragma once

#include "mM3DKernel.h"

#include "Filtering/FilterMgr.h"
#include "Filtering/DataFilter.h"

static void mSetFilterModuleParameterDesc(FilterMgr* pModule, const mxArray* input)
{
	mxArray* pDef = mxGetField(input, 0, "m_bIsEnable");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		pModule->setEnable(value);
	}
	for (unsigned int i = 0; i < pModule->getNbFilter(); i++)
	{

		char name[255];
		sprintf(name, "%s_%d", "SubFilterState", i);
		mxArray* pDef = mxGetField(input, 0, name);
		if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
		{
			mexErrMsgTxt("Bad Structure");
		}
		else
		{
			double value = *mxGetPr(pDef);
			pModule->getFilter(i)->setEnable(value);
		}
	}
}

static mxArray* mGetFilterModuleParameterDesc(FilterMgr* pModule)
{
	const char* field_names[] = {
		 "m_bIsEnable",
	};
	mwSize dims[2] = { 1, 1 };

	mxArray* Array = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	mxArray* field_value;

	int field = mxGetFieldNumber(Array, "m_bIsEnable");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pModule->getEnable();

	mxSetFieldByNumber(Array, 0, field, field_value);
	for (unsigned int i = 0; i < pModule->getNbFilter(); i++)
	{
		char name[255];
		sprintf(name, "%s_%d", "SubFilterState", i);
		field = mxAddField(Array, name);
		field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
		*mxGetPr(field_value) = pModule->getFilter(i)->getEnable();
		mxSetFieldByNumber(Array, 0, field, field_value);

		sprintf(name, "%s_%d", "SubFilterDesc", i);
		field = mxAddField(Array, name);
		field_value = mxCreateString(pModule->getFilter(i)->getName());
		mxSetFieldByNumber(Array, 0, field, field_value);
	}

	return Array;
}
