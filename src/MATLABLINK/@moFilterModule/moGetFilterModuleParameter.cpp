#include "mFilterModule.h"

#include "ModuleManager/ModuleManager.h"
#include "Filtering/FilterMgr.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	Init();
	CModuleManager* pInstance = CModuleManager::getInstance();
	plhs[0] = mGetFilterModuleParameterDesc(pInstance->GetFilterModule());
}
