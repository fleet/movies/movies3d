#include "mFilterModule.h"

#include "ModuleManager/ModuleManager.h"
#include "Filtering/FilterMgr.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	Init();
	CModuleManager* pInstance = CModuleManager::getInstance();
	if (nrhs != 2) {
		mexErrMsgTxt("Two Input argument needed : the struct object, and the parameter definition structure");
	}

	Init();

	if (mxIsClass(prhs[0], "moFilterModule") != 1)
		mexErrMsgTxt("Input 0 Bad Class Argument");

	if (mxIsStruct(prhs[1]) != 1)
		mexErrMsgTxt("Input 1 Bad structure argument");

	mSetFilterModuleParameterDesc(pInstance->GetFilterModule(), prhs[1]);
}
