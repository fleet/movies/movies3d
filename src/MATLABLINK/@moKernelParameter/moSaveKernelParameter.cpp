#include "mM3DKernel.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	unsigned char* start_of_pr;

	if (nrhs != 2) {
		mexErrMsgTxt("Two Input argument needed : the struct object, and the parameter definition structure");
	}

	Init();

	if (mxIsClass(prhs[0], "moKernelParameter") != 1)
		mexErrMsgTxt("Input 0 Bad Class Argument");

	if (mxIsStruct(prhs[1]) != 1)
		mexErrMsgTxt("Input 1 Bad structure argument");

	M3DKernel* pInst = M3DKernel::GetInstance();
	KernelParameter localParam = pInst->GetRefKernelParameter();

	mxArray* pDef = mxGetField(prhs[1], 0, "m_nbPingFanMax");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setNbPingFanMax(value);
	}
	pDef = mxGetField(prhs[1], 0, "m_bAutoLengthEnable");

	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setAutoLengthEnable(value != 0);
	}

	pDef = mxGetField(prhs[1], 0, "m_bAutoDepthEnable");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setAutoDepthEnable((bool)value);
	}
	pDef = mxGetField(prhs[1], 0, "m_bAutoDepthTolerance");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setAutoDepthTolerance(value);
	}
	pDef = mxGetField(prhs[1], 0, "m_ScreenPixelSizeX");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setScreenPixelSizeX(value);
	}
	pDef = mxGetField(prhs[1], 0, "m_ScreenPixelSizeY");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setScreenPixelSizeY(value);
	}
	pDef = mxGetField(prhs[1], 0, "m_MaxRange");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setMaxRange(value);
	}

	pDef = mxGetField(prhs[1], 0, "m_bIgnorePhaseData");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setIgnorePhase(value);
	}

	pDef = mxGetField(prhs[1], 0, "m_bIgnorePingsWithNoNavigation");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setIgnorePingsWithNoNavigation(value);
	}

	pDef = mxGetField(prhs[1], 0, "m_bIgnorePingsWithNoPosition");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setIgnorePingsWithNoPosition(value);
	}

	pInst->UpdateKernelParameter(localParam);
}
