function p = moReaderParameter(a)
if nargin == 0
   p.read=true;
    p = class(p,'moReaderParameter');
elseif isa(a,'moReaderParameter')
   p = a;
else
   p.read=1;
   p = class(p,'moReaderParameter');
end