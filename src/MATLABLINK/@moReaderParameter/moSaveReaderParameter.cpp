#include "mReader.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	unsigned char* start_of_pr;

	if (nrhs != 2) {
		mexErrMsgTxt("Two Input argument needed : the struct object, and the parameter definition structure");
	}

	Init();

	if (mxIsClass(prhs[0], "moReaderParameter") != 1)
		mexErrMsgTxt("Input 0 Bad Class Argument");

	if (mxIsStruct(prhs[1]) != 1)
		mexErrMsgTxt("Input 1 Bad structure argument");

	ReaderCtrl* pRead = ReaderCtrl::getInstance();
	ReaderParameter localParam = pRead->GetReaderParameter();

	mxArray* pDef = mxGetField(prhs[1], 0, "m_chunkUseTimeStamp");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.m_ChunckDef.m_bIsUseTimeDef = value;
	}
	pDef = mxGetField(prhs[1], 0, "m_chunkTimeStamp");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.m_ChunckDef.m_TimeElapsed = value;
	}
	pDef = mxGetField(prhs[1], 0, "m_chunkSounderId");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.m_ChunckDef.m_SounderId = value;
	}
	pDef = mxGetField(prhs[1], 0, "m_chunkNumberOfPingFan");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.m_ChunckDef.m_NumberOfFanToRead = value;
	}

	pDef = mxGetField(prhs[1], 0, "m_sortDataBeforeWrite");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		double value = *mxGetPr(pDef);
		localParam.setSortDataBeforeWrite(value);
	}

	pRead->UpdateReaderParameter(localParam);
}
