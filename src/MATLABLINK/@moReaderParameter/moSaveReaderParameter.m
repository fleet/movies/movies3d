% moSaveReaderParameter record the given the structure to memory, apply its settings
%   moSaveReaderParameter(s1,s2)
%       s1 the parameter object
%       s2 the option structure to record
% Ifremer Cponcelet 07/30/2007
% $Revision: 0.1 $
