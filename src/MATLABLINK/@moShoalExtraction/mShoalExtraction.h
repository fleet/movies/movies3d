#pragma once

#include "mCommon.h"

#include "M3DKernel/module/ModuleOutputConsumer.h"
#include "ModuleManager/ModuleManager.h"
#include "ShoalExtraction/ShoalExtractionModule.h"

static void CreateMatlabConsumer()
{
	CModuleManager* pInstance = CModuleManager::getInstance();
	ModuleOutputConsumer* pConsumer = pInstance->GetShoalExtractionModule()->GetMatlabModuleConsumer();
}

// récupération des paramètres du module
static mxArray* getExtractionParameterDesc(ShoalExtractionModule* pEIModule)
{
	mxArray* retDesc = NULL;

	const char* field_names[] = { "m_verticalIntegrationDistance","m_alongIntegrationDistance","m_acrossIntegrationDistance",
		"m_threshold","m_maxThreshold","m_minLength","m_minWidth","m_minHeight","m_recordPath","m_saveEchoes" };
	mwSize dims[2] = { 1, 1 };
	retDesc = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

	int field = mxGetFieldNumber(retDesc, "m_verticalIntegrationDistance");
	mxSetFieldByNumber(retDesc, 0, field, mxCreateDoubleScalar(pEIModule->GetShoalExtractionParameter().GetVerticalIntegrationDistance()));

	field = mxGetFieldNumber(retDesc, "m_alongIntegrationDistance");
	mxSetFieldByNumber(retDesc, 0, field, mxCreateDoubleScalar(pEIModule->GetShoalExtractionParameter().GetAlongIntegrationDistance()));

	field = mxGetFieldNumber(retDesc, "m_acrossIntegrationDistance");
	mxSetFieldByNumber(retDesc, 0, field, mxCreateDoubleScalar(pEIModule->GetShoalExtractionParameter().GetAcrossIntegrationDistance()));

	field = mxGetFieldNumber(retDesc, "m_threshold");
	mxSetFieldByNumber(retDesc, 0, field, mxCreateDoubleScalar(pEIModule->GetShoalExtractionParameter().GetThreshold()));

	field = mxGetFieldNumber(retDesc, "m_maxThreshold");
	mxSetFieldByNumber(retDesc, 0, field, mxCreateDoubleScalar(pEIModule->GetShoalExtractionParameter().GetMaxThreshold()));

	field = mxGetFieldNumber(retDesc, "m_minLength");
	mxSetFieldByNumber(retDesc, 0, field, mxCreateDoubleScalar(pEIModule->GetShoalExtractionParameter().GetMinLength()));

	field = mxGetFieldNumber(retDesc, "m_minWidth");
	mxSetFieldByNumber(retDesc, 0, field, mxCreateDoubleScalar(pEIModule->GetShoalExtractionParameter().GetMinWidth()));

	field = mxGetFieldNumber(retDesc, "m_minHeight");
	mxSetFieldByNumber(retDesc, 0, field, mxCreateDoubleScalar(pEIModule->GetShoalExtractionParameter().GetMinHeight()));

	field = mxGetFieldNumber(retDesc, "m_recordPath");
	mxSetFieldByNumber(retDesc, 0, field, mxCreateString(pEIModule->GetShoalExtractionParameter().GetRecordPath().c_str()));

	field = mxGetFieldNumber(retDesc, "m_saveEchoes");
	mxSetFieldByNumber(retDesc, 0, field, mxCreateDoubleScalar(pEIModule->GetShoalExtractionParameter().GetSaveEchos()));

	return retDesc;
}