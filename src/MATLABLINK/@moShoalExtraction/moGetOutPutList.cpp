#include "mShoalExtraction.h"
#include "mM3DKernel.h"

#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/shoalstat.h"
#include "ShoalExtraction/data/pingdata.h"
#include "ShoalExtraction/data/pingshoalstat.h"
#include "OutputManager/OutputManagerModule.h"

#include "ModuleManager/ModuleManager.h"
#include "ShoalExtraction/ShoalExtractionModule.h"


// r�cup�ration des infos geographiques d'un echo
static mxArray* getEchoDesc(ShoalExtractionOutput* pOut)
{
	mxArray* retDesc = NULL;

	const char* field_names[] = { "m_latitude","m_longitude","m_depth","m_db","m_channelNb" };
	mwSize dims[2] = { 1, pOut->m_pClosedShoal->GetShoalStat()->GetNbEchos() };
	retDesc = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

	std::vector<shoalextraction::PingData*>::iterator iterPing = pOut->m_pClosedShoal->GetPings().begin();
	int index = 0;
	while (iterPing != pOut->m_pClosedShoal->GetPings().end())
	{
		shoalextraction::PingData* pPingData = *iterPing;

		std::vector<shoalextraction::EchoInfo>::iterator iterEcho = pPingData->GetShoalStat()->GetEchoList().begin();
		while (iterEcho != pPingData->GetShoalStat()->GetEchoList().end())
		{
			int field = mxGetFieldNumber(retDesc, "m_latitude");
			mxSetFieldByNumber(retDesc, index, field, mxCreateDoubleScalar(iterEcho->position.x));

			field = mxGetFieldNumber(retDesc, "m_longitude");
			mxSetFieldByNumber(retDesc, index, field, mxCreateDoubleScalar(iterEcho->position.y));

			field = mxGetFieldNumber(retDesc, "m_depth");
			mxSetFieldByNumber(retDesc, index, field, mxCreateDoubleScalar(iterEcho->position.z));

			field = mxGetFieldNumber(retDesc, "m_db");
			mxSetFieldByNumber(retDesc, index, field, mxCreateDoubleScalar(iterEcho->energy));

			field = mxGetFieldNumber(retDesc, "m_channelNb");
			mxSetFieldByNumber(retDesc, index, field, mxCreateDoubleScalar(iterEcho->channelNb));

			index++;
			iterEcho++;
		}

		iterPing++;
	}

	return retDesc;
}

// OTK - FAE025 - r�cup�ration des infos geographiques du contour
static mxArray* getContourDesc(ShoalExtractionOutput* pOut)
{
	mxArray* retDesc = NULL;

	const char* field_names[] = { "m_latitude","m_longitude","m_depth","m_facet" };

	// nombre de points
	size_t nbPoints = 0;
	const std::vector<std::vector<double>>& strips = pOut->m_pClosedShoal->GetGPSContourStrips();
	size_t stripNumber = strips.size();
	for (size_t i = 0; i < stripNumber; i++)
	{
		nbPoints += strips[i].size() / 3; // 3 coordonn�es par point.
	}

	mwSize dims[2] = { 1, nbPoints };
	retDesc = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

	int index = 0;

	for (size_t i = 0; i < stripNumber; i++)
	{
		const std::vector<double>& strip = strips[i];
		size_t nbPoints = strip.size() / 3; // 3 doubles par point (X,Y,Z)

		for (size_t j = 0; j < nbPoints; j++)
		{
			int field = mxGetFieldNumber(retDesc, "m_latitude");
			mxSetFieldByNumber(retDesc, index, field, mxCreateDoubleScalar(strip[j * 3]));

			field = mxGetFieldNumber(retDesc, "m_longitude");
			mxSetFieldByNumber(retDesc, index, field, mxCreateDoubleScalar(strip[j * 3 + 1]));

			field = mxGetFieldNumber(retDesc, "m_depth");
			mxSetFieldByNumber(retDesc, index, field, mxCreateDoubleScalar(strip[j * 3 + 2]));

			field = mxGetFieldNumber(retDesc, "m_facet");
			mxSetFieldByNumber(retDesc, index, field, mxCreateDoubleScalar(i));

			index++;
		}
	}

	return retDesc;
}

// r�cup�ration des r�sultats du module
static mxArray* getOutPutDesc()
{
	CModuleManager* pInstance = CModuleManager::getInstance();
	ShoalExtractionModule* pEIModule = pInstance->GetShoalExtractionModule();
	ModuleOutputConsumer* pEIConsumer = pEIModule->GetMatlabModuleConsumer();

	mxArray* retDesc = NULL;
	if (!pEIModule || !pEIConsumer)
		return NULL;

	// attente de la fin de l'ex�cution du module (extraction asynchrone)
	pEIModule->WaitExtractionActions();
	// OTK - 14/12/2009 - on provoque un flush du OutputManager pour cr�er les flux CSV/XML des derniers r�sultats asynchrones,
	// et pour lib�rer la m�moire associ�e
	pInstance->GetOutputManagerModule()->PingFanAdded(NULL);

	unsigned int count = pEIModule->GetOutputContainer().GetOutPutCount(pEIConsumer->GetConsumerId());

	const char* field_names[] = { "m_parameter", "m_shoalId", "m_sounderId", "m_transId", "m_minDepth","m_maxDepth","m_minDtBot","m_maxDtBot",
		"m_length","m_width","m_height",
	"m_MVBS", "m_sigma_ag", "m_MVBS_weighted", "m_echoesNb", "m_echoData", "m_contourPoints", "m_volume", "m_startTS", "m_endTS", "m_energyCenterLat",
	"m_energyCenterLong", "m_energyCenterDepth", "m_geoCenterLat", "m_geoCenterLong", "m_geoCenterDepth", "m_firstPointLat",
	"m_firstPointLong", "m_firstPointDepth", "m_lastPointLat",	"m_lastPointLong", "m_lastPointDepth", "m_minAngle", "m_maxAngle" };
	mwSize dims[2] = { 1, count };
	retDesc = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

	for (unsigned int i = 0; i < count; i++)
	{
		ShoalExtractionOutput* pOut = (ShoalExtractionOutput*)pEIModule->GetOutputContainer().GetOutPut(pEIConsumer->GetConsumerId(), i);

		//////////
		// On r�cup�re les parametres d'extraction
		int field = mxGetFieldNumber(retDesc, "m_parameter");
		mxSetFieldByNumber(retDesc, i, field, getExtractionParameterDesc(pEIModule));

		field = mxGetFieldNumber(retDesc, "m_shoalId");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetExternId()));

		field = mxGetFieldNumber(retDesc, "m_sounderId");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetSounderId()));

		field = mxGetFieldNumber(retDesc, "m_transId");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetTransId()));

		field = mxGetFieldNumber(retDesc, "m_minDepth");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetMinDepth()));

		field = mxGetFieldNumber(retDesc, "m_maxDepth");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetMaxDepth()));

		field = mxGetFieldNumber(retDesc, "m_minDtBot");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetMinBottomDistance()));

		field = mxGetFieldNumber(retDesc, "m_maxDtBot");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetMaxBottomDistance()));

		field = mxGetFieldNumber(retDesc, "m_length");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetLength()));

		field = mxGetFieldNumber(retDesc, "m_width");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetWidth()));

		field = mxGetFieldNumber(retDesc, "m_height");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetHeigth()));

		field = mxGetFieldNumber(retDesc, "m_MVBS");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetMeanSv()));

		field = mxGetFieldNumber(retDesc, "m_sigma_ag");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetSigmaAg()));

		field = mxGetFieldNumber(retDesc, "m_MVBS_weighted");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetMeanPondSv()));

		field = mxGetFieldNumber(retDesc, "m_echoesNb");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetNbEchos()));

		// Sous-ensemble contenant les positions des echos
		if (pEIModule->GetShoalExtractionParameter().GetSaveEchos())
		{
			field = mxGetFieldNumber(retDesc, "m_echoData");
			mxSetFieldByNumber(retDesc, i, field, getEchoDesc(pOut));
		}

		// OTK - FAE025 - ajout des points de contour
		if (pEIModule->GetShoalExtractionParameter().GetSaveContour())
		{
			field = mxGetFieldNumber(retDesc, "m_contourPoints");
			mxSetFieldByNumber(retDesc, i, field, getContourDesc(pOut));
		}

		field = mxGetFieldNumber(retDesc, "m_volume");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetVolume()));

		field = mxGetFieldNumber(retDesc, "m_startTS");
		HacTime ts = pOut->m_pClosedShoal->GetShoalStat()->GetBeginTime();
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar((double)ts.m_TimeCpu + (double)ts.m_TimeFraction * 0.0001));

		field = mxGetFieldNumber(retDesc, "m_endTS");
		ts = pOut->m_pClosedShoal->GetShoalStat()->GetEndTime();
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar((double)ts.m_TimeCpu + (double)ts.m_TimeFraction * 0.0001));

		field = mxGetFieldNumber(retDesc, "m_energyCenterLat");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetGravityCenter().x));

		field = mxGetFieldNumber(retDesc, "m_energyCenterLong");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetGravityCenter().y));

		field = mxGetFieldNumber(retDesc, "m_energyCenterDepth");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetGravityCenter().z));

		field = mxGetFieldNumber(retDesc, "m_geoCenterLat");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetGeographicCenter().x));

		field = mxGetFieldNumber(retDesc, "m_geoCenterLong");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetGeographicCenter().y));

		field = mxGetFieldNumber(retDesc, "m_geoCenterDepth");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetGeographicCenter().z));

		field = mxGetFieldNumber(retDesc, "m_firstPointLat");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetFirstPoint().x));

		field = mxGetFieldNumber(retDesc, "m_firstPointLong");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetFirstPoint().y));

		field = mxGetFieldNumber(retDesc, "m_firstPointDepth");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetFirstPoint().z));

		field = mxGetFieldNumber(retDesc, "m_lastPointLat");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetLastPoint().x));

		field = mxGetFieldNumber(retDesc, "m_lastPointLong");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetLastPoint().y));

		field = mxGetFieldNumber(retDesc, "m_lastPointDepth");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetLastPoint().z));

		field = mxGetFieldNumber(retDesc, "m_minAngle");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetAcrossAngleMin()));

		field = mxGetFieldNumber(retDesc, "m_maxAngle");
		mxSetFieldByNumber(retDesc, i, field, mxCreateDoubleScalar(pOut->m_pClosedShoal->GetShoalStat()->GetAcrossAngleMax()));
	}
	pEIModule->GetOutputContainer().Flush(pEIConsumer->GetConsumerId());
	return retDesc;
}

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	Init();
	CreateMatlabConsumer();
	plhs[0] = getOutPutDesc();
}
