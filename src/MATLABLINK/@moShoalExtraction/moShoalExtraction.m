function p = moShoalExtraction(a)
if nargin == 0
   p.Enable=1;
   p = class(p,'moShoalExtraction');
   p.Enable=  moGetShoalExtractionEnabled(p);
   %moSetShoalExtractionEnabled(p);

elseif isa(a,'moShoalExtraction')
   p = a;
else
   p.Enable=1;
   p = class(p,'moShoalExtraction');
   p.Enable=  moGetShoalExtractionEnabled(p);
   %moSetShoalExtractionEnabled(p);
end
