#ifndef MAT_HACALG_H
#define MAT_HACALG_H

#include "mM3DKernel.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/PingFan.h"

#include "M3DKernel/algorithm/base/EchoAlgorithm.h"
#include "M3DKernel/algorithm/base/EchoAlgorithmRoot.h"
#include "M3DKernel/algorithm/base/AlgorithmExecutive.h"

#include <vector>

static void CreateCallerData(std::uint32_t sounderId, const char *MyFieldName)
{    
    const char *field_names[] = {
        MyFieldName,
    };

    mwSize dims[2] = {1, 1};
    mxArray *myArray = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

    mxArray *field_value;
        
    int field = mxGetFieldNumber(myArray,MyFieldName);
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = sounderId;
    mxSetFieldByNumber(myArray,0,field,field_value)  ;
    mexEvalString("global CallerVariables");
    int status=mexPutVariable("global","CallerVariables",myArray);
    if (status==1){
        mexPrintf("Variable %s\n", "CallerVariables");
        mexErrMsgTxt("Could not put variable in global workspace.\n");
    }
}

static void CreateCallerDataFan(unsigned __int64 fanId)
{
	CreateCallerData(fanId,"m_FanIdChanged");
}

static void CreateCallerDataSounder(std::uint32_t sounderId)
{
	CreateCallerData(sounderId,"m_SounderIdChanged");
}

static void RemoveCallerData()
{
    mxArray *array_ptr;
    array_ptr = mexGetVariable("global", "CallerVariables");
       
    /* Destroy array */
    mxDestroyArray(array_ptr); 
    mexEvalString("clear CallerVariables");
}

class DynamicMatlabAlgo : public EchoAlgorithm
{
    std::map<unsigned int, AlgorithmOutput*> m_inputList;

public:
	MovCreateMacro(DynamicMatlabAlgo);
    
    unsigned int GetNumberOfInputPort() const
    {
        return m_inputList.empty() ? 0 : (m_inputList.cend()->first + 1);
    }

    void AddInputPort(AlgorithmOutput* pOutPut, unsigned int portNumber)
    {
        if (!pOutPut)
        {
            M3D_LOG_WARN("M3DKernel.algorithm.EchoAlgorithm", "Connecting null output to algorithm");
        }

        m_inputList[portNumber] = pOutPut;

        AddInput(pOutPut);
    }

	virtual void PingFanAdded( PingFan *pFan)
    {
        if(m_CBPingFanAdded)
        {
			CreateCallerDataFan( pFan->m_computePingFan.m_pingId);
            mexEvalString(m_CBPingFanAdded);
            RemoveCallerData();
        }
    }

	virtual void SounderChanged(std::uint32_t sounderId)
    {
        if(m_CBSounderChanged)
        {
            CreateCallerDataSounder( sounderId);
            mexEvalString(m_CBSounderChanged);
            RemoveCallerData();
        }        
    }

	virtual void StreamClosed(const char *streamName)
	{
		if(m_CBStreamClosed)
        {
            mexEvalString(m_CBStreamClosed);
        }
	}

	virtual void StreamOpened(const char *streamName)
	{
		if(m_CBStreamOpened)
        {
            mexEvalString(m_CBStreamOpened);
        }
	}
	
	virtual ~DynamicMatlabAlgo()
    {
        if(m_CBSounderChanged)
            delete m_CBSounderChanged;
        if(m_CBStreamOpened)
            delete m_CBStreamOpened;
        if(m_CBStreamClosed)
            delete m_CBStreamClosed;
        if(m_CBPingFanAdded)
            delete m_CBPingFanAdded;
    }

    void SetCBSounder(char *CBName)
    {
        SetCB(CBName,&m_CBSounderChanged);
    }
   
    void SetCBPingFan(char *CBName)
    {
        SetCB(CBName,&m_CBPingFanAdded);
    }

    void SetCBStreamClosed(char *CBName)
    {
        SetCB(CBName,&m_CBStreamClosed);
    }

    void SetCBStreamOpened(char *CBName)
    {
        SetCB(CBName,&m_CBStreamOpened);
    }
    
    mxArray *getDesc()
    {
        const char *field_names[] = {
            "m_CBSounderChanged",
            "m_CBPingFanAdded",
            "m_CBStreamOpened",
            "m_CBStreamClosed"
       }   ;
        mwSize dims[2] = {1, 1};
        mxArray * Array =  mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
        mxArray *field_value;
        int field; 
        
        if(m_CBSounderChanged)
        {
            field = mxGetFieldNumber(Array,"m_CBSounderChanged");
            field_value = mxCreateString(m_CBSounderChanged);
            mxSetFieldByNumber(Array,0,field,field_value);
        }

        if(m_CBPingFanAdded)
        {
            field = mxGetFieldNumber(Array,"m_CBPingFanAdded");
            field_value = mxCreateString(m_CBPingFanAdded);
            mxSetFieldByNumber(Array,0,field,field_value);
        }

        if(m_CBStreamOpened)
        {
            field = mxGetFieldNumber(Array,"m_CBStreamOpened");
            field_value = mxCreateString(m_CBStreamOpened);
            mxSetFieldByNumber(Array,0,field,field_value);
        }

		if(m_CBStreamClosed)
        {
            field = mxGetFieldNumber(Array,"m_CBStreamClosed");
            field_value = mxCreateString(m_CBStreamClosed);
            mxSetFieldByNumber(Array,0,field,field_value);
        }

        return Array;
    }
    
    ///
    unsigned int m_algorithmID;

protected:
	DynamicMatlabAlgo()
        : EchoAlgorithm("DynamicMatlabAlgo")
    {
        m_CBSounderChanged=NULL;
        m_CBStreamClosed=NULL;
		m_CBStreamOpened=NULL;
        m_CBPingFanAdded=NULL;    
    }

private:
    
    /// these contain the names of matlab script files called when one event occurs
    char *m_CBSounderChanged;
    char *m_CBStreamOpened;
    char *m_CBStreamClosed;
    char *m_CBPingFanAdded;
    
    void SetCB(char *CBName,char **Dest)
    {
        if(*Dest)
        {
            delete *Dest;
            *Dest=NULL;
        }
        if(CBName)
        {
            unsigned int lenght=strlen(CBName);
            if(lenght>0 ){
                *Dest=new char[lenght+1];
                strcpy_s(*Dest,lenght+1,CBName);
            }
        }
    }
};

static int createAlgo()
{
 	M3DKernel *pKernel= M3DKernel::GetInstance();
    DynamicMatlabAlgo *pNewAl=DynamicMatlabAlgo::Create();
	pNewAl->AddInputPort(pKernel->getEchoAlgorithmRoot()->getOutPut(),0);
    return pKernel->m_MatlabAlgoStore.AddAlgo(pNewAl);
}

static void setInputPort(unsigned int AlgorithmSrc, unsigned int portNumber, unsigned int Producer)
{
	M3DKernel *pKernel= M3DKernel::GetInstance();
    DynamicMatlabAlgo *pSrc=(DynamicMatlabAlgo *)pKernel->m_MatlabAlgoStore.GetAlgorithm(AlgorithmSrc);
    DynamicMatlabAlgo *pProducer=(DynamicMatlabAlgo *)pKernel->m_MatlabAlgoStore.GetAlgorithm(Producer);
    if(!pSrc)
    {
		M3D_LOG_ERROR("MatlabLink", "Unkwnown source Algorithm");
        return;
    }
    
    if(!pProducer)
    {
		M3D_LOG_ERROR("MatlabLink", "Unkwnown pProducer Algorithm");
        return;
    }
    pSrc->AddInputPort(pProducer->getOutPut(), portNumber);
}

static void setInputPortToRoot(unsigned int AlgorithmSrc, unsigned int portNumber)
{
	M3DKernel *pKernel= M3DKernel::GetInstance();
    DynamicMatlabAlgo *pSrc=(DynamicMatlabAlgo *)pKernel->m_MatlabAlgoStore.GetAlgorithm(AlgorithmSrc);
    if(!pSrc)
    {
		M3D_LOG_ERROR("MatlabLink", "Unkwnown source Algorithm");
        return;
    }
    
    pSrc->AddInputPort(pKernel->getEchoAlgorithmRoot()->getOutPut(),portNumber);
}

static void removeAlgo(unsigned int num)
{
    DynamicMatlabAlgo *pSrc=(DynamicMatlabAlgo *)M3DKernel::GetInstance()->m_MatlabAlgoStore.GetAlgorithm(num);
    for(unsigned int i=0;i<pSrc->GetNumberOfInputPort();i++)
    {
        setInputPortToRoot(num,i);
    }
	M3DKernel::GetInstance()->m_MatlabAlgoStore.RemoveAlgo(num);
}

#endif
