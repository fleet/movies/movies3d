#pragma once

#include "mex.h"

#define NUMBER_OF_FIELDS_X(X) (sizeof(X)/sizeof(*X))
#define NUMBER_OF_FIELDS NUMBER_OF_FIELDS_X(field_names)

void *GetAndCheckField(const mxArray *aStructDef,const char *aFieldName,mxClassID classId,int idx=0)
{
   void *ret=0;
    mxArray *pDef= mxGetField(aStructDef, idx , aFieldName);
    if ( pDef == NULL || mxGetClassID(pDef) != classId)
    {
        mexErrMsgTxt("Bad Structure");
    }
    else
    {
        ret=mxGetPr(pDef);
    }
    return ret;
}

double *GetFieldDouble(const mxArray *aStructDef,const char *aFieldName,int idx=0)
{
    return (double*)GetAndCheckField(aStructDef,aFieldName,mxDOUBLE_CLASS,idx);
}