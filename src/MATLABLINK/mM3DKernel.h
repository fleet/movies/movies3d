#ifndef MAT_M3DKERNEL_H
#define MAT_M3DKERNEL_H

#include "mCommon.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/utils/log/LogAppender.h"
#include "M3DKernel/utils/log/LoggerFactory.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/PingFanContainer.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"

#include "M3DKernel/datascheme/TransformSet.h"

class MatlabAppender : public Log::ILogAppender
{
	virtual void append(const Log::LoggerItem & logItem) override
	{
		if (logItem.level >= Log::ERROR_LOG_LEVEL)
		{			
			mexErrMsgTxt(logItem.message.c_str());
		}
		else if (logItem.level >= Log::WARN_LOG_LEVEL)
		{
			mexWarnMsgTxt(logItem.message.c_str());
		}
		else if (logItem.level >= Log::INFO_LOG_LEVEL)
		{
			mexPrintf("%s\n", logItem.message.c_str());
		}
	}
};

MatlabAppender matlabAppender;

static void Init()
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	Log::LogAppender* appender = Log::LogAppender::GetInstance();
	appender->setDefaultAppender(&matlabAppender);
}

static mxArray * getSplitBeamParameterDesc(SoftChannel *pChan)
{
	SplitBeamParameter *pParameter=pChan->GetSplitBeamParameter();
	mxArray * retArray=NULL;
	if(pParameter)
	{
		const char *field_names[] = {"m_startLogTimeCPU","m_startLogTimeFraction","m_splitBeamParameterChannelId",
			"m_minimumValue", "m_minimumEchoLength","m_maximumEchoLenght","m_maximumGainCompensation","m_maximumPhaseCompensation"};
		mwSize dims[2] = {1, 1 };
		retArray=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

		int field = mxGetFieldNumber(retArray,"m_startLogTimeCPU");
		mxArray *field_value;
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pParameter->m_startLogTime.m_TimeCpu;
		mxSetFieldByNumber(retArray,0,field,field_value);

		field = mxGetFieldNumber(retArray,"m_startLogTimeFraction");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pParameter->m_startLogTime.m_TimeFraction;
		mxSetFieldByNumber(retArray,0,field,field_value);

		field = mxGetFieldNumber(retArray,"m_splitBeamParameterChannelId");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pParameter->m_splitBeamParameterChannelId;
		mxSetFieldByNumber(retArray,0,field,field_value);

		field = mxGetFieldNumber(retArray,"m_minimumValue");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pParameter->m_minimumValue;
		mxSetFieldByNumber(retArray,0,field,field_value);

		field = mxGetFieldNumber(retArray,"m_minimumEchoLength");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pParameter->m_minimumEchoLength;
		mxSetFieldByNumber(retArray,0,field,field_value);

		field = mxGetFieldNumber(retArray,"m_maximumEchoLenght");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pParameter->m_maximumEchoLenght;
		mxSetFieldByNumber(retArray,0,field,field_value);

		field = mxGetFieldNumber(retArray,"m_maximumGainCompensation");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pParameter->m_maximumGainCompensation;
		mxSetFieldByNumber(retArray,0,field,field_value);

		field = mxGetFieldNumber(retArray,"m_maximumPhaseCompensation");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pParameter->m_maximumPhaseCompensation;
		mxSetFieldByNumber(retArray,0,field,field_value);

	}
	return retArray;
}

static mxArray* getPolarToWorldCoord(unsigned __int64 pingFanId, unsigned int numTransducer, unsigned int numBeam, unsigned int DepthPolar)
{
	mxArray *ret=NULL;
	const mwSize dims[]={3,1};

	M3DKernel *pKernel= M3DKernel::GetInstance();
	PingFan *pFan=pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingFanId);
	if(pFan!=NULL)
	{
		auto realCoord=pFan->getSounderRef()->GetPolarToWorldCoord(pFan,numTransducer,numBeam,DepthPolar);
		ret= mxCreateNumericArray(2,dims,mxDOUBLE_CLASS,mxREAL);
		double *start_of_pr = (double *)mxGetData(ret);
		*(start_of_pr)= realCoord.x;
		*(start_of_pr+1)= realCoord.y;
		*(start_of_pr+2)= realCoord.z;
		return ret;

	}else
	{
		mexErrMsgTxt("Sounder Not Found");
	}
	return ret;
}

static mxArray* getPolarToGeoCoord(std::uint64_t pingFanId, unsigned int numTransducer, unsigned int numBeam, unsigned int DepthPolar)
{
	mxArray *ret=NULL;
	const mwSize dims[]={3,1};

	M3DKernel *pKernel= M3DKernel::GetInstance();
	PingFan *pFan=pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingFanId);
	if(pFan!=NULL)
	{
		auto geoCoord=pFan->getSounderRef()->GetPolarToGeoCoord(pFan,numTransducer,numBeam,DepthPolar);
		ret= mxCreateNumericArray(2,dims,mxDOUBLE_CLASS,mxREAL);
		double *start_of_pr = (double *)mxGetData(ret);
		*(start_of_pr)= geoCoord.x;
		*(start_of_pr+1)= geoCoord.y;
		*(start_of_pr+2)= geoCoord.z;
		return ret;

	}else
	{
		mexErrMsgTxt("Sounder Not Found");
	}
	return ret;
}

static mxArray* getChannelToWordCoord(unsigned __int64 pingFanId, unsigned int numTransducer, unsigned int numBeam, BaseMathLib::Vector3D &channelCoord)
{
	mxArray *ret=NULL;
	const mwSize dims[]={3,1};

	M3DKernel *pKernel= M3DKernel::GetInstance();
	PingFan *pFan=pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingFanId);
	if(pFan!=NULL)
	{

		auto realCoord=pFan->getSounderRef()->GetSoftChannelCoordinateToWorldCoord(pFan,numTransducer,numBeam,channelCoord);
		ret= mxCreateNumericArray(2,dims,mxDOUBLE_CLASS,mxREAL);
		double *start_of_pr = (double *)mxGetData(ret);
		*(start_of_pr)= realCoord.x;
		*(start_of_pr+1)= realCoord.y;
		*(start_of_pr+2)= realCoord.z;
		return ret;

	}else
	{
		mexErrMsgTxt("Sounder Not Found");
	}
	return ret;
}

static mxArray *getKernelParameterDesc()
{
	const char *field_names[] = {
		"m_nbPingFanMax",
		"m_bAutoLengthEnable",
		"m_bAutoDepthEnable",
		"m_bAutoDepthTolerance",
		"m_ScreenPixelSizeX",
		"m_ScreenPixelSizeY",
		"m_MaxRange",
		"m_bIgnorePhaseData",
		"m_bIgnorePingsWithNoNavigation",
		"m_bIgnorePingsWithNoPosition"
	};

	mwSize dims[2] = {1, 1};
	KernelParameter param= M3DKernel::GetInstance()->GetRefKernelParameter();
	mxArray * Array=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	mxArray *field_value;

	int field = mxGetFieldNumber(Array,"m_nbPingFanMax");
	field_value =mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = param.getNbPingFanMax();
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_bAutoLengthEnable");
	field_value =mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = param.getAutoLengthEnable();
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_bAutoDepthEnable");
	field_value =mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = param.getAutoDepthEnable();
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_bAutoDepthTolerance");
	field_value =mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = param.getAutoDepthTolerance();
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_ScreenPixelSizeX");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = param.getScreenPixelSizeX();
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_ScreenPixelSizeY");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = param.getScreenPixelSizeY();
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_MaxRange");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = param.getMaxRange();
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_bIgnorePhaseData");
	field_value =mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = param.getIgnorePhase();
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_bIgnorePingsWithNoNavigation");
	field_value =mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = param.getIgnorePingsWithNoNavigation();
	mxSetFieldByNumber(Array,0,field,field_value);  

	field = mxGetFieldNumber(Array,"m_bIgnorePingsWithNoPosition");
	field_value =mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = param.getIgnorePingsWithNoPosition();
	mxSetFieldByNumber(Array,0,field,field_value);

	return Array;
}

static mxArray * getWholeSoftChannelDesc(Transducer *pTransducer)
{
	if(pTransducer==NULL)
		return NULL;

	const char *field_names[] = {
		"m_PolarId",
		"m_isReferenceBeam",
		"m_groupId",
		"m_isMultiBeam",
		"m_softChannelId",
		"m_channelName",
		"m_dataType",
		"m_beamType",
		"m_acousticFrequency",
		"m_startFrequency",
		"m_endFrequency",
		"m_startSample",
		"m_mainBeamAlongSteeringAngleRad",
		"m_mainBeamAthwartSteeringAngleRad",
		"m_absorptionCoef",
		"m_transmissionPower",
		"m_beamAlongAngleSensitivity",
		"m_beamAthwartAngleSensitivity",
		"m_beam3dBWidthAlongRad",
		"m_beam3dBWidthAthwartRad",
		"m_beamEquTwoWayAngle",
		"m_beamGain",
		"m_beamSACorrection",
		"m_bottomDetectionMinDepth",
		"m_bottomDetectionMaxDepth",
		"m_bottomDetectionMinLevel",
		"m_AlongTXRXWeightId",
		"m_AthwartTXRXWeightId",
		"m_SplitBeamAlongTXRXWeightId",
		"m_SplitBeamAthwartTXRXWeightId",
		"m_bandWidth",
		"m_singleTargetParameter"
	};
	mwSize dims[2] = {1, pTransducer->m_numberOfSoftChannel };
	mxArray * ArrayChannel=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	for(unsigned int i=0;i< pTransducer->m_numberOfSoftChannel;i++)
	{
		SoftChannel*pChan =pTransducer->getSoftChannelPolarX(i);

		int field = mxGetFieldNumber(ArrayChannel,"m_PolarId");
		mxArray *field_value;
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_softChannelComputeData.m_PolarId;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_isReferenceBeam");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_softChannelComputeData.m_isReferenceBeam;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_groupId");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_softChannelComputeData.m_groupId;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_isMultiBeam");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_softChannelComputeData.m_isMultiBeam;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_softChannelId");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_softChannelId;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_channelName");
		field_value = mxCreateString(pChan->m_channelName);
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_dataType");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_dataType;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_beamType");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_beamType;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_acousticFrequency");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_acousticFrequency;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_startFrequency");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_startFrequency;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_endFrequency");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_endFrequency;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_startSample");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_startSample;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_mainBeamAlongSteeringAngleRad");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_mainBeamAlongSteeringAngleRad;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_mainBeamAthwartSteeringAngleRad");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_mainBeamAthwartSteeringAngleRad;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_absorptionCoef");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_absorptionCoef;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_transmissionPower");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_transmissionPower;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_beamAlongAngleSensitivity");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_beamAlongAngleSensitivity;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_beamAthwartAngleSensitivity");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_beamAthwartAngleSensitivity;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_beam3dBWidthAlongRad");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_beam3dBWidthAlongRad;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_beam3dBWidthAthwartRad");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_beam3dBWidthAthwartRad;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_beamEquTwoWayAngle");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_beamEquTwoWayAngle;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_beamGain");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_beamGain;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_beamSACorrection");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_beamSACorrection;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_bottomDetectionMinDepth");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_bottomDetectionMinDepth;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_bottomDetectionMaxDepth");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_bottomDetectionMaxDepth;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_bottomDetectionMinLevel");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_bottomDetectionMinLevel;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_AlongTXRXWeightId");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_AlongTXRXWeightId;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_AthwartTXRXWeightId");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_AthwartTXRXWeightId;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_SplitBeamAlongTXRXWeightId");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_SplitBeamAlongTXRXWeightId;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);
		field = mxGetFieldNumber(ArrayChannel,"m_SplitBeamAthwartTXRXWeightId");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_SplitBeamAthwartTXRXWeightId;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"m_bandWidth");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pChan->m_bandWidth;
		mxSetFieldByNumber(ArrayChannel,i,field,field_value);


		field = mxGetFieldNumber(ArrayChannel,"m_singleTargetParameter");
		mxSetFieldByNumber(ArrayChannel,i,field,getSplitBeamParameterDesc(pChan));
	}
	return ArrayChannel;
}

static mxArray * getPlatformDesc(Transducer *pTransducer)
{
	if(pTransducer && pTransducer->GetPlatform())
	{
		int field = 0;
		mxArray *field_value;
		const char *field_names[] = { "along_ship_offset",
			"athwart_ship_offset",
			"depth_offset"
		};
		mwSize dims[2] = {1, 1 };
		mxArray * ArrayChannel=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
		field = mxGetFieldNumber(ArrayChannel,"along_ship_offset");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->GetPlatform()->GetAlongShipOffset();
		mxSetFieldByNumber(ArrayChannel,0,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"athwart_ship_offset");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->GetPlatform()->GetAthwartShipOffset();
		mxSetFieldByNumber(ArrayChannel,0,field,field_value);

		field = mxGetFieldNumber(ArrayChannel,"depth_offset");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->GetPlatform()->GetDephtOffset();
		mxSetFieldByNumber(ArrayChannel,0,field,field_value);
		return ArrayChannel;
	}
	return NULL;
}

static mxArray * getWholeTransducerDesc(Sounder *pSounder)
{
	if(pSounder==NULL)
		return NULL;
	const char *field_names[] = {
		"m_transName",
		"m_transSoftVersion",
		"m_pulseDuration",
		"m_pulseShape",
		"m_pulseSlope",
		"m_pulseForm",
		"m_timeSampleInterval",
		"m_frequencyBeamSpacing",
		"m_frequencySpaceShape",
		"m_transPower",
		"m_transDepthMeter",
		"m_platformId",
		"m_transShape",
		"m_transFaceAlongAngleOffsetRad",
		"m_transFaceAthwarAngleOffsetRad",
		"m_transRotationAngleRad",
		"m_numberOfSoftChannel",
		"m_beamsSamplesSpacing",
		"m_SoftChannel",
		"m_pPlatform"
	};
	mwSize dims[2] = {1, pSounder->m_numberOfTransducer };
	mxArray * ArrayTrans=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	for(int i=0;i< pSounder->m_numberOfTransducer;i++)
	{
		int field;
		mxArray *field_value;
		Transducer *pTransducer=pSounder->GetTransducer(i);
		if(!pTransducer)
			continue;

		field = mxGetFieldNumber(ArrayTrans,"m_transName");
		field_value = mxCreateString(pTransducer->m_transName);
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_transSoftVersion");
		field_value = mxCreateString(pTransducer->m_transSoftVersion);
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_pulseDuration");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_pulseDuration;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_pulseShape");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_pulseShape;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_pulseSlope");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_pulseSlope;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_pulseForm");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_pulseForm;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_timeSampleInterval");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_timeSampleInterval;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_frequencyBeamSpacing");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_frequencyBeamSpacing;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_frequencySpaceShape");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_frequencySpaceShape;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_transPower");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_transPower;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_transDepthMeter");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_transDepthMeter;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_platformId");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_platformId;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_transShape");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_transShape;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_transFaceAlongAngleOffsetRad");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_transFaceAlongAngleOffsetRad;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_transFaceAthwarAngleOffsetRad");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_transFaceAthwarAngleOffsetRad;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_transRotationAngleRad");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_transRotationAngleRad;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_numberOfSoftChannel");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->m_numberOfSoftChannel;
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_beamsSamplesSpacing");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pTransducer->getBeamsSamplesSpacing();
		mxSetFieldByNumber(ArrayTrans,i,field,field_value);

		field = mxGetFieldNumber(ArrayTrans,"m_SoftChannel");
		mxSetFieldByNumber(ArrayTrans,i,field,getWholeSoftChannelDesc(pTransducer));

		field = mxGetFieldNumber(ArrayTrans,"m_pPlatform");
		mxSetFieldByNumber(ArrayTrans,i,field,getPlatformDesc(pTransducer));

	}
	return ArrayTrans;
}
/*
static mxArray* getPolarToReal(std::uint32_t numSensor, Float2 polarCoord)
{
mxArray *ret=NULL;
Float2 realCoord;
realCoord.x=0;
realCoord.y=0;
Kernel *pKernel=Kernel::GetInstance();
SessionMgr& sessionMgr=pKernel->getObjectMgr()->getSessionMgr();
TransformMap*pTransform=sessionMgr.GetLastSession(numSensor);
if(pTransform!=NULL)
{
realCoord= pTransform->polarToReal2(polarCoord);

}else
{
mexErrMsgTxt("No Active Session Found");
}
const mwSize dims[]={2,1};
ret= mxCreateNumericArray(2,dims,mxDOUBLE_CLASS,mxREAL);
double *start_of_pr = (double *)mxGetData(ret);
*(start_of_pr)= realCoord.x;
*(start_of_pr+1)= realCoord.y;
return ret;
}*/
/*
* create a single sounder desc, if array is given, set this desc in this array at a given place
*/

static mxArray * getSounderDesc(Sounder *pSounder)
{
	const char *field_names[] = {
		"m_SounderId",
		"m_soundVelocity",
		"m_triggerMode",
		"m_numberOfTransducer",
		"m_pingInterval",
		"m_isMultiBeam",
		"m_nbBeamPerFan",
		"m_transducer"
	};

	mxArray * ArraySounder;

	mwSize dims[2] = {1, 1 };
	ArraySounder =   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	int index=0;
	int field = mxGetFieldNumber(ArraySounder,"m_SounderId");
	mxArray *field_value;
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = pSounder->m_SounderId;
	mxSetFieldByNumber(ArraySounder,index,field,field_value);

	field = mxGetFieldNumber(ArraySounder,"m_soundVelocity");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = pSounder->m_soundVelocity;
	mxSetFieldByNumber(ArraySounder,index,field,field_value);

	field = mxGetFieldNumber(ArraySounder,"m_triggerMode");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = pSounder->m_triggerMode;
	mxSetFieldByNumber(ArraySounder,index,field,field_value);

	field = mxGetFieldNumber(ArraySounder,"m_numberOfTransducer");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = pSounder->m_numberOfTransducer;
	mxSetFieldByNumber(ArraySounder,index,field,field_value);

	field = mxGetFieldNumber(ArraySounder,"m_pingInterval");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = pSounder->m_pingInterval;
	mxSetFieldByNumber(ArraySounder,index,field,field_value);

	field = mxGetFieldNumber(ArraySounder,"m_isMultiBeam");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = pSounder->m_isMultiBeam;
	mxSetFieldByNumber(ArraySounder,index,field,field_value);

	field = mxGetFieldNumber(ArraySounder,"m_nbBeamPerFan");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = pSounder->m_sounderComputeData.m_nbBeamPerFan;
	mxSetFieldByNumber(ArraySounder,index,field,field_value);

	field = mxGetFieldNumber(ArraySounder,"m_transducer");
	mxSetFieldByNumber(ArraySounder,index,field,getWholeTransducerDesc(pSounder));

	return ArraySounder;
}

static mxArray * getSounderDesc(CurrentSounderDefinition &aSounderMgr, std::uint32_t SounderId, bool single )
{
	const char *field_names[] = {
		"m_SounderId",
		"m_soundVelocity",
		"m_triggerMode",
		"m_numberOfTransducer",
		"m_pingInterval",
		"m_isMultiBeam",
		"m_nbBeamPerFan",
		"m_transducer"
	};
	mxArray * ArraySounder;
	unsigned int nbSounder=1;
	Sounder *pSounder=NULL;
	if(!single)
	{
		nbSounder=aSounderMgr.GetNbSounder();
	}
	mwSize dims[2] = {1, nbSounder };
	ArraySounder =   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	for(unsigned int index=0;index<nbSounder;index++)
	{
		if(single)
		{
			pSounder=aSounderMgr.GetSounderWithId(SounderId);
		}else
		{
			pSounder=aSounderMgr.GetSounder(index);
		}
		if(!pSounder)
			continue;
		int field = mxGetFieldNumber(ArraySounder,"m_SounderId");
		mxArray *field_value;
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pSounder->m_SounderId;
		mxSetFieldByNumber(ArraySounder,index,field,field_value);

		field = mxGetFieldNumber(ArraySounder,"m_soundVelocity");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pSounder->m_soundVelocity;
		mxSetFieldByNumber(ArraySounder,index,field,field_value);

		field = mxGetFieldNumber(ArraySounder,"m_triggerMode");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pSounder->m_triggerMode;
		mxSetFieldByNumber(ArraySounder,index,field,field_value);

		field = mxGetFieldNumber(ArraySounder,"m_numberOfTransducer");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pSounder->m_numberOfTransducer;
		mxSetFieldByNumber(ArraySounder,index,field,field_value);

		field = mxGetFieldNumber(ArraySounder,"m_pingInterval");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pSounder->m_pingInterval;
		mxSetFieldByNumber(ArraySounder,index,field,field_value);

		field = mxGetFieldNumber(ArraySounder,"m_isMultiBeam");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pSounder->m_isMultiBeam;
		mxSetFieldByNumber(ArraySounder,index,field,field_value);

		/*    field = mxGetFieldNumber(ArraySounder,"m_maxBottomDepth");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pSounder->m_sounderComputeData.GetPingFanContainer().m_maxBottomDepth;
		mxSetFieldByNumber(ArraySounder,0,field,field_value);
		*/

		field = mxGetFieldNumber(ArraySounder,"m_nbBeamPerFan");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pSounder->m_sounderComputeData.m_nbBeamPerFan;
		mxSetFieldByNumber(ArraySounder,index,field,field_value);

		field = mxGetFieldNumber(ArraySounder,"m_transducer");
		mxSetFieldByNumber(ArraySounder,index,field,getWholeTransducerDesc(pSounder));

	}
	return ArraySounder;
}

static mxArray* getPingFanMatrixScreen(unsigned __int64 fanId, unsigned int transIndex )
{
	mxArray *ret=NULL;
	unsigned int Xdim=0;
	unsigned int Ydim=0;

	M3DKernel *pKernel= M3DKernel::GetInstance();
	PingFan *pingFan=pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(fanId);

	Transducer *pTrans=pingFan->getSounderRef()->GetTransducer(transIndex);
	if(!pTrans)
	{
		mexErrMsgTxt("Transducer not found Check Index");
	}
	if(pingFan)
	{
		TransformMap *pTransform=pingFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transIndex);
		auto memSize=pTransform->getCartesianSize2();
		Xdim=memSize.x;
		Ydim=memSize.y;
		const mwSize dims[]={Ydim,Xdim};

		ret= mxCreateNumericArray(2,dims,mxDOUBLE_CLASS,mxREAL);
		double *start_of_pr = (double *)mxGetData(ret);
		MemoryStruct *pMemStruct=(MemoryStruct *)pingFan->GetMemorySetRef()->GetMemoryStruct(transIndex);
		if(!pMemStruct)
			mexErrMsgTxt("Memory Struct not found Check transducer index");
		short *value=pMemStruct->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(0,0));
		char *Filter=pMemStruct->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(0,0));
		double m_compensateHeave=0;
		SoftChannel *pSoftChan=pTrans->getSoftChannelPolarX(0);
		if(pSoftChan)
		{
			m_compensateHeave= pingFan->getHeaveChan(pSoftChan->m_softChannelId);
		}
		else
		{
			M3D_LOG_ERROR("MatlabLink", "Channel Not Found");
		}
		double prof = pTrans->m_transDepthMeter;	// transducer depth en cm

		int offset=(prof+m_compensateHeave)/pTransform->getYSpacing();

		for(unsigned int j = 0; j< Ydim;j++)
		{
			for(unsigned int i=0; i< Xdim;i++)
			{
				BaseMathLib::Vector3D pos(i,j);

				//   short value=*((short*)pTrans->getTransformMap()->getPolarMemory()->GetPointerToVoxel(pos));

				int index=-1;
				if(j-offset>0)
					index=pTransform->getIndex(BaseMathLib::Vector2I(i,j-offset));
				short myValue=(index < 0 ) ? 0 : value[index];
				char myFilter=(index<0) ? 0 : Filter[index];
				if(myFilter>0)
					*(start_of_pr+i*Ydim+j)=UNKNOWN_DB;
				else
					*(start_of_pr+i*Ydim+j)=myValue;
			}
		}
	}
	else
	{
		mexErrMsgTxt("PingFan not found check id");
	}

	return ret;
}

#endif
