#ifndef MAT_NAVATTITUDE_H
#define MAT_NAVATTITUDE_H

#include "mCommon.h"

static mxArray * getNavAttitudeDesc(NavAttitude *pNav)
{
    if(!pNav)
        return NULL;
    const char *field_names[] = {"m_meanTimeCPU", "m_meanTimeFraction","pitchRad","rollRad","yawRad","sensorHeaveMeter"};
    mwSize dims[2] = {1, 1 };
    mxArray * ArrayNav=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
    int field;
    mxArray *field_value;
    
    field = mxGetFieldNumber(ArrayNav,"m_meanTimeFraction");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_ObjectTime.m_TimeFraction;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"m_meanTimeCPU");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_ObjectTime.m_TimeCpu;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"pitchRad");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_pitchRad;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"rollRad");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) =  pNav->m_rollRad;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    
    field = mxGetFieldNumber(ArrayNav,"yawRad");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) =  pNav->m_yawRad;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"sensorHeaveMeter");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) =  pNav->m_heaveMeter;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    return ArrayNav;
}
static void setNavAttitudeDesc(NavAttitude *pNav,const mxArray *myStruct)
{
    if(!pNav)
        return;
    if(!myStruct)
        return;
    pNav->m_ObjectTime.m_TimeFraction=*GetFieldDouble(myStruct,"m_meanTimeFraction",0);
    pNav->m_ObjectTime.m_TimeCpu=*GetFieldDouble(myStruct,"m_meanTimeCPU",0);
    
    pNav->m_pitchRad=*GetFieldDouble(myStruct,"pitchRad",0);
    pNav->m_rollRad=*GetFieldDouble(myStruct,"rollRad",0);
    
    pNav->m_yawRad=*GetFieldDouble(myStruct,"yawRad",0);
    
    pNav->m_heaveMeter=*GetFieldDouble(myStruct,"sensorHeaveMeter",0);
    
}

#endif