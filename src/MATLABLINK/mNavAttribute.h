#ifndef MAT_NAVATTRIBUTE_H
#define MAT_NAVATTRIBUTE_H

#include "mCommon.h"

static mxArray * getNavAttributesDesc(NavAttributes *pNav)
{
    if(!pNav)
        return NULL;
    const char *field_names[] = {"m_meanTimeFraction","m_meanTimeCPU","m_navigationSystem","m_headingRad","m_speedMeter" };
    mwSize dims[2] = {1, 1 };
    mxArray * ArrayNav=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
    int field;
    mxArray *field_value;
    
    field = mxGetFieldNumber(ArrayNav,"m_meanTimeFraction");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_ObjectTime.m_TimeFraction;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"m_meanTimeCPU");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_ObjectTime.m_TimeCpu;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"m_navigationSystem");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_NavigationSystem;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"m_headingRad");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_headingRad;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"m_speedMeter");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_speedMeter;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    return ArrayNav;    
}

static void setNavAttributesDesc(NavAttributes *pNav,const mxArray *myStruct)
{
    if(!pNav)
        return;
    if(!myStruct)
        return;
    pNav->m_ObjectTime.m_TimeFraction=*GetFieldDouble(myStruct,"m_meanTimeFraction",0);
    pNav->m_ObjectTime.m_TimeCpu=*GetFieldDouble(myStruct,"m_meanTimeCPU",0);
    
    pNav->m_NavigationSystem=*GetFieldDouble(myStruct,"m_navigationSystem",0);
    pNav->m_headingRad=*GetFieldDouble(myStruct,"m_headingRad",0);
    pNav->m_speedMeter=*GetFieldDouble(myStruct,"m_speedMeter",0);
}

#endif