#ifndef MAT_NAVPOSITION_H
#define MAT_NAVPOSITION_H

#include "mCommon.h"

static mxArray * getNavPositionDesc(NavPosition *pNav)
{
    if(!pNav)
        return NULL;
    const char *field_names[] = {"m_meanTimeCPU","m_meanTimeFraction","m_longitudeDeg","m_lattitudeDeg","m_gpsTime", "m_positionningSystem" };
    mwSize dims[2] = {1, 1 };
    mxArray * ArrayNav=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
    int field;
    mxArray *field_value;
    
    field = mxGetFieldNumber(ArrayNav,"m_meanTimeFraction");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_ObjectTime.m_TimeFraction;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"m_meanTimeCPU");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_ObjectTime.m_TimeCpu;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"m_longitudeDeg");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_longitudeDeg;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"m_lattitudeDeg");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_lattitudeDeg;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"m_gpsTime");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_gpsTime;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    
    field = mxGetFieldNumber(ArrayNav,"m_positionningSystem");
    field_value = mxCreateDoubleMatrix(1,1,mxREAL);
    *mxGetPr(field_value) = pNav->m_positionningSystem;
    mxSetFieldByNumber(ArrayNav,0,field,field_value);
    return ArrayNav;
}
static void  setNavPositionDesc(NavPosition *pNav,const mxArray *myStruct)
{
    if(!pNav)
        return;
    if(!myStruct)
        return;
    pNav->m_ObjectTime.m_TimeFraction=*GetFieldDouble(myStruct,"m_meanTimeFraction",0);
    pNav->m_ObjectTime.m_TimeCpu=*GetFieldDouble(myStruct,"m_meanTimeCPU",0);
    
    pNav->m_longitudeDeg=*GetFieldDouble(myStruct,"m_longitudeDeg",0);
    pNav->m_lattitudeDeg=*GetFieldDouble(myStruct,"m_lattitudeDeg",0);
    pNav->m_gpsTime=*GetFieldDouble(myStruct,"m_gpsTime",0);
    pNav->m_positionningSystem=  *GetFieldDouble(myStruct,"m_positionningSystem",0);
}

#endif
