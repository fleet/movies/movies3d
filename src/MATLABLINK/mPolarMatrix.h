#ifndef MAT_MATRIXPOLAR_H
#define MAT_MATRIXPOLAR_H

#include "mM3DKernel.h"

#include "Calibration/CalibrationModule.h"

static mxArray* getPingFanMatrixPolar(unsigned __int64 FanId,  unsigned int transIdx )
{
	mxArray *ret=NULL;
	unsigned int Xdim=0;
	unsigned int Ydim=0;
	const char *field_names[] = {
		"m_Amplitude",
		"m_Overlap",
		"m_AthwartAngle",
		"m_AlongAngle",
	};
	mwSize dims[2] = {1, 1 };
	ret  =   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

	M3DKernel *pKernel= M3DKernel::GetInstance();

	PingFan *pingFan=(PingFan *)pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(FanId);
	Transducer *pTrans=pingFan->getSounderRef()->GetTransducer(transIdx);
	if(pTrans!=NULL)
	{
		/* populate the real part of the created array */
		if(pingFan)
		{
			TransformMap *pTransMap=pingFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transIdx);
			MemoryStruct *pMemStruct=(MemoryStruct *)pingFan->GetMemorySetRef()->GetMemoryStruct(transIdx);
			BaseMathLib::Vector2I memSize=pMemStruct->GetDataFmt()->getSize();
			Xdim=memSize.x;
			Ydim=memSize.y;
			const mwSize dims[]={Ydim,Xdim};

			mxArray *Ampl= mxCreateNumericArray(2,dims,mxDOUBLE_CLASS,mxREAL);
			mxArray *Athw=NULL;
			mxArray *Along=NULL;
			mxArray *Overlap=NULL;

			double *start_of_pr       = (double *)mxGetData(Ampl);
			double *start_of_prAthw  =NULL;
			double *start_of_prAlong  =NULL;
			double *start_of_prOverlap = NULL;
			if(pMemStruct->GetPhase())
			{
				Athw= mxCreateNumericArray(2,dims,mxDOUBLE_CLASS,mxREAL);
				Along= mxCreateNumericArray(2,dims,mxDOUBLE_CLASS,mxREAL);
				start_of_prAthw   = (double *)mxGetData(Athw);
				start_of_prAlong  = (double *)mxGetData(Along);
			}
			if(pMemStruct->GetOverLap())
			{
				Overlap= mxCreateNumericArray(2,dims,mxDOUBLE_CLASS,mxREAL);
				start_of_prOverlap= (double*) mxGetData(Overlap);
			}

			for(unsigned int i=0; i< Xdim;i++)
			{
				for(unsigned int j = 0; j< Ydim;j++)
				{
					BaseMathLib::Vector2I pos(i,j);
					short value=*(pMemStruct->GetDataFmt()->GetPointerToVoxel(pos));
					char Filter=*(pMemStruct->GetFilterFlag()->GetPointerToVoxel(pos));
					if(Filter>0)
						value=UNKNOWN_DB;
					*(start_of_pr+i*Ydim+j)=value;

					if(pMemStruct->GetPhase())
					{
						Phase valueAngle=
							*((Phase*)pMemStruct->GetPhase()->GetPointerToVoxel(pos));
						*(start_of_prAthw+i*Ydim+j)=valueAngle.GetAthwart();
						*(start_of_prAlong+i*Ydim+j)=valueAngle.GetAlong();
					}
					if(pMemStruct->GetOverLap())
					{
						short valueO=*((short*)pMemStruct->GetOverLap()->GetPointerToVoxel(pos));
						*(start_of_prOverlap+i*Ydim+j)=valueO;
					}
				}
			}

			int field ;

			field = mxGetFieldNumber(ret,"m_Amplitude");
			mxSetFieldByNumber(ret,0,field,Ampl);
			if(Athw)
			{
				field = mxGetFieldNumber(ret,"m_AthwartAngle");
				mxSetFieldByNumber(ret,0,field,Athw);
			}
			if(Along)
			{
				field = mxGetFieldNumber(ret,"m_AlongAngle");
				mxSetFieldByNumber(ret,0,field,Along);
			}
			if(Overlap)
			{
				field = mxGetFieldNumber(ret,"m_Overlap");
				mxSetFieldByNumber(ret,0,field,Overlap);
			}
		}
		else
		{
			mexErrMsgTxt("PingFan not found check index");
		}
	} 
	else
	{
		mexErrMsgTxt("Transducer not found check index");
	}
	
	return ret;
}

static void setPingFanMatrixPolar(unsigned int FanId, unsigned int transIdx,const mxArray* matrixAmp, const mxArray* matrixAngleAthw, const mxArray* matrixAngleAlong)
{

	M3DKernel *pKernel= M3DKernel::GetInstance();
	PingFan *pFan=(PingFan*)pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(FanId);
	Transducer *pTrans=pFan->getSounderRef()->GetTransducer(transIdx);

	if(!pFan)
	{
		mexErrMsgTxt("PingFan not found check index");
	}
	if(!pTrans)
	{
		mexErrMsgTxt("Transducer not found check index");
	}
	TransformMap *pTransMap=pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transIdx);
	MemoryStruct *pMemStruct=(MemoryStruct *)pFan->GetMemorySetRef()->GetMemoryStruct(transIdx);
	BaseMathLib::Vector2I memSize=pMemStruct->GetDataFmt()->getSize();
	int Xdim=memSize.x;
	int Ydim=memSize.y;

	const mwSize *dimsMatrix=mxGetDimensions(matrixAmp);

	if(dimsMatrix[0]==Ydim && dimsMatrix[1]==Xdim)
	{
		double *start_of_prAmp = NULL;
		double *start_of_prAthw = NULL;
		double *start_of_prAlon = NULL;


		if(matrixAmp)
			start_of_prAmp =(double *)mxGetData(matrixAmp);
		if(matrixAngleAthw && matrixAngleAlong)
		{
			start_of_prAthw = (double *)mxGetData(matrixAngleAthw);
			start_of_prAlon = (double *)mxGetData(matrixAngleAlong);
		}
		for(int i=0; i< Xdim;i++)
		{
			for(int j = 0; j< Ydim;j++)
			{
				BaseMathLib::Vector2I pos(i,j);
				MemoryObjectDataFmt *pAmp=pMemStruct->GetDataFmt();
				if(pAmp)
				{
					short *pValue=((short*)pAmp->GetPointerToVoxel(pos));
					*pValue=*(start_of_prAmp+i*Ydim+j);
				}
				MemoryObjectPhase *pMemPhase=pMemStruct->GetPhase();
				if(pMemPhase && matrixAngleAthw && matrixAngleAlong )
				{
					Phase *pValue=pMemPhase->GetPointerToVoxel(pos);
					pValue->SetAthwart(*(start_of_prAthw+i*Ydim+j));
					pValue->SetAlong(*(start_of_prAlon+i*Ydim+j));
				}
			}
		}
	}
	else
	{
		mexErrMsgTxt("Matrix size is different from PolarMemorySize");
	}
}


#endif