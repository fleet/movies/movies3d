#ifndef MAT_READER_H
#define MAT_READER_H

#include "mCommon.h"
#include "mM3DKernel.h"

#include "Reader/ReaderCtrl.h"
#include "Reader/MovReadService.h"

static mxArray *getFileRunDesc(MovReadService*pService)
{
	if(!pService)
		return NULL;
	const char *field_names[] = {
		"m_CurrentFileIdx",
		"m_FileList",
	}   ;
	mwSize dims[2] = {1, 1};
	mxArray * Array=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	mxArray *field_value;

	int field = mxGetFieldNumber(Array,"m_CurrentFileIdx");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = pService->GetFileRun().GetCurrentFileIdx();
	mxSetFieldByNumber(Array,0,field,field_value);

	if(pService->GetFileRun().GetFileCount()>0)
	{
		// first get Max Str Length
		unsigned int maxLenght=0;
		for(unsigned int i=0;i<pService->GetFileRun().GetFileCount();i++)
		{
			std::string fileName = pService->GetFileRun().GetFileName(i);
			maxLenght = std::max<unsigned int>(maxLenght, strlen(fileName.c_str()));
		}

		char* *tmp=new char* [pService->GetFileRun().GetFileCount()];
		for(unsigned int i=0;i<pService->GetFileRun().GetFileCount();i++)
		{
			tmp[i]=new char[maxLenght+1];
			memset(tmp[i],0,(maxLenght+1)*sizeof(char));
			std::string fileName = pService->GetFileRun().GetFileName(i);
			strcpy_s(tmp[i],maxLenght+1, fileName.c_str());
		}

		mxArray *fileList= mxCreateCharMatrixFromStrings((mwSize)pService->GetFileRun().GetFileCount(), (const char **)tmp); 
		for(unsigned int i=0;i<pService->GetFileRun().GetFileCount();i++)
		{
			delete [] (tmp[i]);
		}

		delete [] tmp;
		field = mxGetFieldNumber(Array,"m_FileList");
		mxSetFieldByNumber(Array,0,field,fileList);
	}
	return Array;
}

static mxArray *getStreamDesc()
{
	const char *field_names[] = {
		"m_StreamClosed",
		"m_StreamDescName",
		"m_FileRun"
	}   ;
	mwSize dims[2] = {1, 1};
	mxArray * Array=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	M3DKernel *pKernel= M3DKernel::GetInstance();
	ReaderCtrl *pRead=ReaderCtrl::getInstance();

	mxArray *field_value;

	if(pRead->GetActiveService())
	{
		int field = mxGetFieldNumber(Array,"m_StreamClosed");
		field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = pRead->GetActiveService()->IsClosed();
		mxSetFieldByNumber(Array,0,field,field_value);

		field = mxGetFieldNumber(Array,"m_StreamDescName");
		field_value = mxCreateString(pRead->GetActiveService()->getStreamDesc());
		mxSetFieldByNumber(Array,0,field,field_value);

		field = mxGetFieldNumber(Array,"m_FileRun");		
		mxSetFieldByNumber(Array,0,field,getFileRunDesc(pRead->GetActiveService()));
	}

	return Array;
}

static mxArray * getReaderParameterDesc()
{
	const char *field_names[] = {
		"m_chunkUseTimeStamp",
		"m_chunkTimeStamp",
		"m_chunkSounderId",
		"m_chunkNumberOfPingFan",
		"m_sortDataBeforeWrite"
	};

	mwSize dims[2] = {1, 1};
	ReaderCtrl *pRead=ReaderCtrl::getInstance();
	ReaderParameter Readparam=pRead->GetReaderParameter();

	mxArray * Array=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
	mxArray *field_value;

	int field = mxGetFieldNumber(Array,"m_chunkUseTimeStamp");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = Readparam.m_ChunckDef.m_bIsUseTimeDef;
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_chunkTimeStamp");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = Readparam.m_ChunckDef.m_TimeElapsed;
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_chunkSounderId");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = Readparam.m_ChunckDef.m_SounderId;
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_chunkNumberOfPingFan");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = Readparam.m_ChunckDef.m_NumberOfFanToRead;
	mxSetFieldByNumber(Array,0,field,field_value);

	field = mxGetFieldNumber(Array,"m_sortDataBeforeWrite");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = Readparam.getSortDataBeforeWrite();
	mxSetFieldByNumber(Array,0,field,field_value);

	return Array;
}

#endif
