#pragma once

#include "mM3DKernel.h"

#include "TSAnalysis/TSAnalysisModule.h"

static mxArray * getSingleTargetFanDesc(PingFanSingleTarget *pSingle,PingFan* pFan, TSAnalysisModule * pTSModule)
{
	const char *field_names[] = {"m_meanTimeCPU","m_meanTimeFraction","m_pingId", "m_splitBeamData" };
	mwSize dims[2] = {1, 1 };
	mxArray * singleTargetDesc=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

	int field = mxGetFieldNumber(singleTargetDesc,"m_pingId");
	mxArray *field_value;
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) =pFan->m_computePingFan.m_pingId;
	mxSetFieldByNumber(singleTargetDesc,0,field,field_value);

	field = mxGetFieldNumber(singleTargetDesc,"m_meanTimeFraction");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = pFan->m_ObjectTime.m_TimeFraction;
	mxSetFieldByNumber(singleTargetDesc,0,field,field_value);

	field = mxGetFieldNumber(singleTargetDesc,"m_meanTimeCPU");
	field_value = mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(field_value) = pFan->m_ObjectTime.m_TimeCpu;
	mxSetFieldByNumber(singleTargetDesc,0,field,field_value);

	field = mxGetFieldNumber(singleTargetDesc,"m_splitBeamData");

	{
		const char *field_names[] = { "m_parentSTId", "m_selectStartRange", "m_selectEndRange","m_detectedBottom","m_targetList"};
		int num=0;
		if(pSingle)
			num=pSingle->GetNumberOfTarget();
		
		mwSize dims[2] = {1,num};

		mxArray * arrayChan=   mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
		mxSetFieldByNumber(singleTargetDesc,0,field,arrayChan);
		
		if(pSingle)
		{
			const std::map<unsigned __int64, TSTrack*> & mapTracks = pTSModule->getTracks();
			for(unsigned int chan=0;chan<pSingle->GetNumberOfTarget();chan++)
			{
				SingleTargetDataObject *pSingleTargetData=pSingle->GetTarget(chan);

				int field = mxGetFieldNumber(arrayChan,"m_parentSTId");
				mxArray *field_value;
				field_value = mxCreateDoubleMatrix(1,1,mxREAL);
				*mxGetPr(field_value) = pSingleTargetData->m_parentSTId;
				mxSetFieldByNumber(arrayChan,chan,field,field_value);

				field = mxGetFieldNumber(arrayChan,"m_selectStartRange");
				field_value = mxCreateDoubleMatrix(1,1,mxREAL);
				*mxGetPr(field_value) = pSingleTargetData->m_selectStartRange;
				mxSetFieldByNumber(arrayChan,chan,field,field_value);

				field = mxGetFieldNumber(arrayChan,"m_selectEndRange");
				field_value = mxCreateDoubleMatrix(1,1,mxREAL);
				*mxGetPr(field_value) = pSingleTargetData->m_selectEndRange;
				mxSetFieldByNumber(arrayChan,chan,field,field_value);

				field = mxGetFieldNumber(arrayChan,"m_detectedBottom");
				field_value = mxCreateDoubleMatrix(1,1,mxREAL);
				*mxGetPr(field_value) = pSingleTargetData->m_detectedBottom;
				mxSetFieldByNumber(arrayChan,chan,field,field_value);

				field = mxGetFieldNumber(arrayChan,"m_targetList");

				const char *field_namesTarget[] = { "m_targetRange",
					"m_compensatedTS",
					"m_unCompensatedTS",
					"m_AlongShipAngleRad",
					"m_AthwartShipAngleRad",
					"m_positionBeamCoord",
					"m_positionWorldCoord",
					"m_positionGeoCoord",
					"m_trackLabel",
				};
				mwSize dimsTarget[2] = {1,pSingleTargetData->GetSingleTargetDataCWCount() };
				mxArray * arrayTarget=   mxCreateStructArray(2, dimsTarget, NUMBER_OF_FIELDS_X(field_namesTarget), field_namesTarget);
				mxSetFieldByNumber(arrayChan,chan,field,arrayTarget);

				SplitBeamPair ref;
				M3DKernel *pKernel= M3DKernel::GetInstance();
				bool found=pKernel->getObjectMgr()->GetChannelParentSplitBeam(pSingleTargetData->m_parentSTId,ref);
				// OTK - FAE076 - on ne liste que les single targets correspondant au sondeur du ping en question
				// (sinon, en cas particulier de deux pings de sondeurs diff?rents dat?s ? l'identique : ca plante)
				Sounder *pSounder=pFan->getSounderRef();
				if(found && ref.m_sounderId == pSounder->m_SounderId)
				{
					Transducer *pTransducer=pSounder->getTransducerForChannel(ref.m_channelId);
					SoftChannel *pChannel=pTransducer->getSoftChannel(ref.m_channelId);
					for(unsigned int target=0;target<pSingleTargetData->GetSingleTargetDataCWCount();target++)
					{
						const SingleTargetDataCW & data = pSingleTargetData->GetSingleTargetDataCW(target);
						int field = mxGetFieldNumber(arrayTarget,"m_targetRange");
						mxArray *field_value;
						field_value = mxCreateDoubleMatrix(1,1,mxREAL);
						*mxGetPr(field_value) = data.m_targetRange;
						mxSetFieldByNumber(arrayTarget,target,field,field_value);

						field = mxGetFieldNumber(arrayTarget,"m_compensatedTS");
						field_value = mxCreateDoubleMatrix(1,1,mxREAL);
						*mxGetPr(field_value) = data.m_compensatedTS;
						mxSetFieldByNumber(arrayTarget,target,field,field_value);

						field = mxGetFieldNumber(arrayTarget,"m_unCompensatedTS");
						field_value = mxCreateDoubleMatrix(1,1,mxREAL);
						*mxGetPr(field_value) = data.m_unCompensatedTS;
						mxSetFieldByNumber(arrayTarget,target,field,field_value);

						field = mxGetFieldNumber(arrayTarget,"m_AlongShipAngleRad");
						field_value = mxCreateDoubleMatrix(1,1,mxREAL);
						*mxGetPr(field_value) = data.m_AlongShipAngleRad;
						mxSetFieldByNumber(arrayTarget,target,field,field_value);

						field = mxGetFieldNumber(arrayTarget,"m_AthwartShipAngleRad");
						field_value = mxCreateDoubleMatrix(1,1,mxREAL);
						*mxGetPr(field_value) = data.m_AthwartShipAngleRad;
						mxSetFieldByNumber(arrayTarget,target,field,field_value);


						BaseMathLib::Vector3D mSoftCoord=data.GetTargetPositionSoftChannel();

						const mwSize myDims[]={3,1};
						mxArray  *pCoord= mxCreateNumericArray(2,myDims,mxDOUBLE_CLASS,mxREAL);
						double *start_of_pr = (double *)mxGetData(pCoord);
						*(start_of_pr)= mSoftCoord.x;
						*(start_of_pr+1)= mSoftCoord.y;
						*(start_of_pr+2)= mSoftCoord.z;

						field = mxGetFieldNumber(arrayTarget,"m_positionBeamCoord");

						mxSetFieldByNumber(arrayTarget,target,field,pCoord);

						BaseMathLib::Vector3D mWorldCoord=
							pSounder->GetSoftChannelCoordinateToWorldCoord(pTransducer,pChannel,pFan,mSoftCoord);
						pCoord= mxCreateNumericArray(2,myDims,mxDOUBLE_CLASS,mxREAL);
						start_of_pr = (double *)mxGetData(pCoord);
						*(start_of_pr)= mWorldCoord.x;
						*(start_of_pr+1)= mWorldCoord.y;
						*(start_of_pr+2)= mWorldCoord.z;
						field = mxGetFieldNumber(arrayTarget,"m_positionWorldCoord");
						mxSetFieldByNumber(arrayTarget, target, field, pCoord);

						BaseMathLib::Vector3D mGeoCoord =
							pSounder->GetSoftChannelCoordinateToGeoCoord(pTransducer, pChannel, pFan, mSoftCoord);
						pCoord = mxCreateNumericArray(2, myDims, mxDOUBLE_CLASS, mxREAL);
						start_of_pr = (double *)mxGetData(pCoord);
						*(start_of_pr) = mGeoCoord.x;
						*(start_of_pr + 1) = mGeoCoord.y;
						*(start_of_pr + 2) = mGeoCoord.z;
						field = mxGetFieldNumber(arrayTarget, "m_positionGeoCoord");
						mxSetFieldByNumber(arrayTarget,target,field,pCoord);

						// OTK - FAE214 - ajout du label de la track ?ventuelle
						field = mxGetFieldNumber(arrayTarget, "m_trackLabel");
						field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
						unsigned __int64 trackLabel = 0;
						std::map<unsigned __int64, TSTrack*>::const_iterator iter = mapTracks.find(data.m_trackLabel);
						if (iter != mapTracks.end())
						{
							// On filtre les tracks retenues
							if (pTSModule->IsValidTrack(iter->second))
							{
								trackLabel = data.m_trackLabel;
							}
						}
						pTSModule->getTracks();
						*mxGetPr(field_value) = trackLabel;
						mxSetFieldByNumber(arrayTarget, target, field, field_value);
					}
				}
			}
		}
	}
	return singleTargetDesc;
}


static mxArray * getSingleTargetFMFanDesc(PingFanSingleTarget *pSingle, PingFan* pFan, TSAnalysisModule * pTSModule)
{
	const char *field_names[] = { "m_meanTimeCPU","m_meanTimeFraction","m_pingId", "m_splitBeamData" };
	mwSize dims[2] = { 1, 1 };
	mxArray * singleTargetDesc = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

	int field = mxGetFieldNumber(singleTargetDesc, "m_pingId");
	mxArray *field_value;
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pFan->m_computePingFan.m_pingId;
	mxSetFieldByNumber(singleTargetDesc, 0, field, field_value);

	field = mxGetFieldNumber(singleTargetDesc, "m_meanTimeFraction");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pFan->m_ObjectTime.m_TimeFraction;
	mxSetFieldByNumber(singleTargetDesc, 0, field, field_value);

	field = mxGetFieldNumber(singleTargetDesc, "m_meanTimeCPU");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pFan->m_ObjectTime.m_TimeCpu;
	mxSetFieldByNumber(singleTargetDesc, 0, field, field_value);

	field = mxGetFieldNumber(singleTargetDesc, "m_splitBeamData");
	
	{
		const char *field_names[] = { "m_parentSTId", "m_selectStartRange", "m_selectEndRange","m_detectedBottom","m_targetList" };
		const  int numberOfTarget = (pSingle) ?  pSingle->GetNumberOfTarget() : 0;

		mwSize dims[2] = { 1, numberOfTarget };
		mxArray * arrayChan = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
		mxSetFieldByNumber(singleTargetDesc, 0, field, arrayChan);

		if (pSingle)
		{
			const std::map<unsigned __int64, TSTrack*> & mapTracks = pTSModule->getTracks();
			for (int chan = 0; chan<numberOfTarget; ++chan)
			{
				SingleTargetDataObject *pSingleTargetData = pSingle->GetTarget(chan);

				int field = mxGetFieldNumber(arrayChan, "m_parentSTId");
				mxArray *field_value;
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = pSingleTargetData->m_parentSTId;
				mxSetFieldByNumber(arrayChan, chan, field, field_value);

				field = mxGetFieldNumber(arrayChan, "m_selectStartRange");
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = pSingleTargetData->m_selectStartRange;
				mxSetFieldByNumber(arrayChan, chan, field, field_value);

				field = mxGetFieldNumber(arrayChan, "m_selectEndRange");
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = pSingleTargetData->m_selectEndRange;
				mxSetFieldByNumber(arrayChan, chan, field, field_value);

				field = mxGetFieldNumber(arrayChan, "m_detectedBottom");
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = pSingleTargetData->m_detectedBottom;
				mxSetFieldByNumber(arrayChan, chan, field, field_value);

				field = mxGetFieldNumber(arrayChan, "m_targetList");

				const char *field_namesTarget[] = { 
					"m_targetRange",
					"m_freq",
					"m_compensatedTS",
					"m_unCompensatedTS",
					"m_AlongShipAngleRad",
					"m_AthwartShipAngleRad",
					"m_medianTS",
					"m_positionBeamCoord",
					"m_positionWorldCoord",
					"m_positionGeoCoord",
					"m_trackLabel",
				};

				mwSize dimsTarget[2] = { 1,pSingleTargetData->GetSingleTargetDataFMCount() };
				mxArray * arrayTarget = mxCreateStructArray(2, dimsTarget, NUMBER_OF_FIELDS_X(field_namesTarget), field_namesTarget);
				mxSetFieldByNumber(arrayChan, chan, field, arrayTarget);

				SplitBeamPair ref;
				M3DKernel *pKernel = M3DKernel::GetInstance();
				bool found = pKernel->getObjectMgr()->GetChannelParentSplitBeam(pSingleTargetData->m_parentSTId, ref);
				// OTK - FAE076 - on ne liste que les single targets correspondant au sondeur du ping en question
				// (sinon, en cas particulier de deux pings de sondeurs diff?rents dat?s ? l'identique : ca plante)
				Sounder *pSounder = pFan->getSounderRef();
				if (found && ref.m_sounderId == pSounder->m_SounderId)
				{
					Transducer *pTransducer = pSounder->getTransducerForChannel(ref.m_channelId);
					SoftChannel *pChannel = pTransducer->getSoftChannel(ref.m_channelId);

					for (unsigned int target = 0; target < pSingleTargetData->GetSingleTargetDataFMCount(); target++)
					{
						const SingleTargetDataFM &data = pSingleTargetData->GetSingleTargetDataFM(target);
						int field = mxGetFieldNumber(arrayTarget, "m_targetRange");
						mxArray *field_value;
						field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
						*mxGetPr(field_value) = data.m_targetRange;
						mxSetFieldByNumber(arrayTarget, target, field, field_value);

						const int nbFreqs = data.m_freq.size();
						const mwSize array_dims[] = { nbFreqs };

						mxArray * array_freq = mxCreateNumericArray(1, array_dims, mxDOUBLE_CLASS, mxREAL);
						mxArray * array_compensatedTS = mxCreateNumericArray(1, array_dims, mxDOUBLE_CLASS, mxREAL);
						mxArray * array_unCompensatedTS = mxCreateNumericArray(1, array_dims, mxDOUBLE_CLASS, mxREAL);
						mxArray * array_alongShipAngleRad = mxCreateNumericArray(1, array_dims, mxDOUBLE_CLASS, mxREAL);
						mxArray * array_athwartShipAngleRad = mxCreateNumericArray(1, array_dims, mxDOUBLE_CLASS, mxREAL);

						double * freq = (double *)mxGetData(array_freq);
						double * compensatedTS = (double *)mxGetData(array_compensatedTS);
						double * unCompensatedTS = (double *)mxGetData(array_unCompensatedTS);
						double * alongShipAngleRad = (double *)mxGetData(array_alongShipAngleRad);
						double * athwartShipAngleRad = (double *)mxGetData(array_athwartShipAngleRad);
						
						for (int i = 0; i < nbFreqs; ++i)
						{
							freq[i] = data.m_freq[i];
							compensatedTS[i] = data.m_compensatedTS[i];
							unCompensatedTS[i] = data.m_unCompensatedTS[i];
						}

						int field_freq = mxGetFieldNumber(arrayTarget, "m_freq");
						mxSetFieldByNumber(arrayTarget, target, field_freq, array_freq);
						
						int field_compensatedTS = mxGetFieldNumber(arrayTarget, "m_compensatedTS");
						mxSetFieldByNumber(arrayTarget, target, field_compensatedTS, array_compensatedTS);

						int field_unCompensatedTS = mxGetFieldNumber(arrayTarget, "m_unCompensatedTS");
						mxSetFieldByNumber(arrayTarget, target, field_unCompensatedTS, array_unCompensatedTS);

						field = mxGetFieldNumber(arrayTarget, "m_AlongShipAngleRad");
						field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
						*mxGetPr(field_value) = data.m_AlongShipAngleRad;
						mxSetFieldByNumber(arrayTarget, target, field, field_value);

						field = mxGetFieldNumber(arrayTarget, "m_AthwartShipAngleRad");
						field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
						*mxGetPr(field_value) = data.m_AthwartShipAngleRad;
						mxSetFieldByNumber(arrayTarget, target, field, field_value);

						field = mxGetFieldNumber(arrayTarget, "m_medianTS");
						field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
						*mxGetPr(field_value) = data.m_medianTS;
						mxSetFieldByNumber(arrayTarget, target, field, field_value);

						BaseMathLib::Vector3D mSoftCoord = data.GetTargetPositionSoftChannel();

						const mwSize myDims[] = { 3,1 };
						mxArray  *pCoord = mxCreateNumericArray(2, myDims, mxDOUBLE_CLASS, mxREAL);
						double *start_of_pr = (double *)mxGetData(pCoord);
						*(start_of_pr) = mSoftCoord.x;
						*(start_of_pr + 1) = mSoftCoord.y;
						*(start_of_pr + 2) = mSoftCoord.z;

						field = mxGetFieldNumber(arrayTarget, "m_positionBeamCoord");
						mxSetFieldByNumber(arrayTarget, target, field, pCoord);

						BaseMathLib::Vector3D mWorldCoord =
							pSounder->GetSoftChannelCoordinateToWorldCoord(pTransducer, pChannel, pFan, mSoftCoord);
						pCoord = mxCreateNumericArray(2, myDims, mxDOUBLE_CLASS, mxREAL);
						start_of_pr = (double *)mxGetData(pCoord);
						*(start_of_pr) = mWorldCoord.x;
						*(start_of_pr + 1) = mWorldCoord.y;
						*(start_of_pr + 2) = mWorldCoord.z;
						field = mxGetFieldNumber(arrayTarget, "m_positionWorldCoord");			
						mxSetFieldByNumber(arrayTarget, target, field, pCoord);

						BaseMathLib::Vector3D mGeoCoord =
							pSounder->GetSoftChannelCoordinateToGeoCoord(pTransducer, pChannel, pFan, mSoftCoord);
						pCoord = mxCreateNumericArray(2, myDims, mxDOUBLE_CLASS, mxREAL);
						start_of_pr = (double *)mxGetData(pCoord);
						*(start_of_pr) = mGeoCoord.x;
						*(start_of_pr + 1) = mGeoCoord.y;
						*(start_of_pr + 2) = mGeoCoord.z;
						field = mxGetFieldNumber(arrayTarget, "m_positionGeoCoord");
						mxSetFieldByNumber(arrayTarget, target, field, pCoord);

						// OTK - FAE214 - ajout du label de la track ?ventuelle
						field = mxGetFieldNumber(arrayTarget, "m_trackLabel");
						field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
						unsigned __int64 trackLabel = 0;
						std::map<unsigned __int64, TSTrack*>::const_iterator iter = mapTracks.find(data.m_trackLabel);
						if (iter != mapTracks.end())
						{
							// On filtre les tracks retenues
							if (pTSModule->IsValidTrack(iter->second))
							{
								trackLabel = data.m_trackLabel;
							}
						}
						pTSModule->getTracks();
						*mxGetPr(field_value) = trackLabel;
						mxSetFieldByNumber(arrayTarget, target, field, field_value);

					}
				}
			}
		}
	}
	return singleTargetDesc;
}