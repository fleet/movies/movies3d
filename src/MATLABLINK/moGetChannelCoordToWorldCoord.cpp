#include "mM3DKernel.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	unsigned char* start_of_pr;
	size_t bytes_to_copy;
	(void)nlhs; (void)nrhs; (void)prhs;  /* unused parameters */

	if (nrhs != 4) {
		mexErrMsgTxt("4 Input argument needed : the FanId,TransducerIndex,BeamIndex, and a row vector containing [X Y Z] the position in beam coordinate  ");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a number (double in fact).");
	if (mxIsDouble(prhs[1]) != 1)
		mexErrMsgTxt("Input 2 must be a number (double in fact).");
	if (mxIsDouble(prhs[2]) != 1)
		mexErrMsgTxt("Input 3 must be a number (double in fact).");
	if (mxIsDouble(prhs[3]) != 1)
		mexErrMsgTxt("Input 4 must be a number (double in fact).");

	double vector[3];
	const mwSize* dimsMatrix = mxGetDimensions(prhs[3]);
	if (dimsMatrix[0] != 3 || dimsMatrix[1] != 1)
		mexErrMsgTxt("Input must be a row vector[3]");
	double* startPtr = (double*)mxGetData(prhs[3]);
	for (int i = 0; i < 3; i++)
		vector[i] = *(startPtr + i);

	vector[0] = *startPtr;

	double pingFanId;
	memcpy(&pingFanId, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	double numTransducer;
	memcpy(&numTransducer, mxGetData(prhs[1]), mxGetElementSize(prhs[1]));

	double numBeam;
	memcpy(&numBeam, mxGetData(prhs[2]), mxGetElementSize(prhs[2]));

	BaseMathLib::Vector3D a(vector[0], vector[1], vector[2]);

	Init();

	plhs[0] = getChannelToWordCoord((unsigned __int64)pingFanId, (unsigned int)numTransducer, (unsigned int)numBeam, a);
}
