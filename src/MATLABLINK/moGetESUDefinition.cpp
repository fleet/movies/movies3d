#include "mM3DKernel.h"

#include "M3DKernel/datascheme/MovESUMgr.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	// récupération du module de gestion des ESU
	MovESUMgr* pMovESUMgr = M3DKernel::GetInstance()->getMovESUManager();

	if (pMovESUMgr == NULL)
		return;

	// creation du tableau de sortie
	const char* field_names[] = { "ESUCutType","ESULength" };
	mwSize dims[2] = { 1, 1 };
	mxArray* retDesc = mxCreateStructArray(1, dims, NUMBER_OF_FIELDS, field_names);

	mxArray* pType = mxCreateDoubleMatrix(1, 1, mxREAL);
	mxArray* pValue = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(pType) = (unsigned int)pMovESUMgr->GetRefParameter().GetESUCutType();
	switch (pMovESUMgr->GetRefParameter().GetESUCutType())
	{
	case ESUParameter::ESU_CUT_BY_DISTANCE:
		*mxGetPr(pValue) = pMovESUMgr->GetRefParameter().GetESUDistance();
		break;
	case ESUParameter::ESU_CUT_BY_PING_NB:
		*mxGetPr(pValue) = pMovESUMgr->GetRefParameter().GetESUPingNumber();
		break;
	case ESUParameter::ESU_CUT_BY_TIME:
		*mxGetPr(pValue) = pMovESUMgr->GetRefParameter().GetESUTime().m_TimeCpu;
		break;
	}

	mxSetField(retDesc, 0, "ESUCutType", pType);
	mxSetField(retDesc, 0, "ESULength", pValue);

	plhs[0] = retDesc;
}
