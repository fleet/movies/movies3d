#include "mM3DKernel.h"

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray*prhs[])
{    
    unsigned char *start_of_pr;
    (void) nlhs; (void) nrhs; (void) prhs;  /* unused parameters */
    
    if (nrhs != 1) {
        mexErrMsgTxt("One Input argument needed : the PingFan id");
    }    
    
   /* input must be a number */
    if ( mxIsDouble(prhs[0]) != 1)
        mexErrMsgTxt("Input 1 must be a number (double in fact).");
    
    double fanId;
    memcpy(&fanId, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));
    
	M3DKernel *pKernel= M3DKernel::GetInstance();
    
    PingFan *pFan=pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(fanId);
	if(pFan)
		plhs[0]=getSounderDesc(pFan->getSounderRef() );
    else
		mexErrMsgTxt("Ping Fan not found in memory, check the PingFan id");    
}
