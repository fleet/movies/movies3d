#include "mReader.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	// resultat
	mxArray* retDesc = NULL;

	// initialisations ...
	Init();

	ReaderCtrl* pRead = ReaderCtrl::getInstance();

	// récupération du fileRun courant
	if (pRead->IsFileService())
	{
		//récupération des limites du run
		HacTime minTime, maxTime;
		std::uint32_t minPing, maxPing;
		pRead->GetLimits(minTime, minPing, maxTime, maxPing);

		// formatage de la structure résultat
		const char* field_names[] = { "MinTime", "MinPingID","MaxTime","MaxPingID" };
		mwSize dims[2] = { 1, 1 };
		retDesc = mxCreateStructArray(1, dims, NUMBER_OF_FIELDS, field_names);

		mxArray* field_value;
		int field = mxGetFieldNumber(retDesc, "MinTime");
		field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
		*mxGetPr(field_value) = (double)minTime.m_TimeCpu + (double)minTime.m_TimeFraction * 0.0001;
		mxSetFieldByNumber(retDesc, 0, field, field_value);

		field = mxGetFieldNumber(retDesc, "MinPingID");
		field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
		*mxGetPr(field_value) = minPing;
		mxSetFieldByNumber(retDesc, 0, field, field_value);

		field = mxGetFieldNumber(retDesc, "MaxTime");
		field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
		*mxGetPr(field_value) = (double)maxTime.m_TimeCpu + (double)maxTime.m_TimeFraction * 0.0001;
		mxSetFieldByNumber(retDesc, 0, field, field_value);

		field = mxGetFieldNumber(retDesc, "MaxPingID");
		field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
		*mxGetPr(field_value) = maxPing;
		mxSetFieldByNumber(retDesc, 0, field, field_value);
	}
	else
	{
		mexErrMsgTxt("No FileRun loaded.");
	}

	plhs[0] = retDesc;
}
