#include "mM3DKernel.h"

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray*prhs[] )
{
    if (nrhs != 0) {
        mexErrMsgTxt("No Input argument needed ");
    }
    
    Init();
        
	unsigned int numPinFan= M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetNbFan();
	mxArray * p=mxCreateDoubleMatrix(1,1,mxREAL);
	*mxGetPr(p) = numPinFan;
	plhs[0] = p;
}
