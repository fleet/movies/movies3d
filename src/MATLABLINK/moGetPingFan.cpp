#include "mM3DKernel.h"
#include "mNavAttribute.h"
#include "mNavPosition.h"
#include "mNavAttitude.h"
#include "mSingleTarget.h"

#include "ModuleManager/ModuleManager.h"

static mxArray* getPingFanDesc(PingFan* pingFan)
{
	if (!pingFan)
		return NULL;

	const char* field_names[] = {
		"m_meanTimeCPU","m_meanTimeFraction","m_pingId","m_pingHacId","m_sounderId", "m_cumulatedNav", "m_cumulatedDistance"
		,"m_maxRange", "m_maxRangeWasFound"
		,"beam_data","singleTarget" };

	mwSize dims[2] = { 1, 1 };
	mxArray* ArraySounder = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

	int field = mxGetFieldNumber(ArraySounder, "m_pingId");
	mxArray* field_value;
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pingFan->m_computePingFan.m_pingId;
	mxSetFieldByNumber(ArraySounder, 0, field, field_value);

	field = mxGetFieldNumber(ArraySounder, "m_pingHacId");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pingFan->m_computePingFan.m_filePingId;
	mxSetFieldByNumber(ArraySounder, 0, field, field_value);

	field = mxGetFieldNumber(ArraySounder, "m_cumulatedNav");
	field_value = mxCreateDoubleMatrix(1, 3, mxREAL);
	*mxGetPr(field_value) = pingFan->m_relativePingFan.m_cumulatedNav.x;
	*(mxGetPr(field_value) + 1) = pingFan->m_relativePingFan.m_cumulatedNav.y;
	*(mxGetPr(field_value) + 2) = pingFan->m_relativePingFan.m_cumulatedNav.z;
	mxSetFieldByNumber(ArraySounder, 0, field, field_value);

	field = mxGetFieldNumber(ArraySounder, "m_cumulatedDistance");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = (pingFan->m_relativePingFan.m_cumulatedDistance / 1.852) / 1000.0;
	mxSetFieldByNumber(ArraySounder, 0, field, field_value);

	field = mxGetFieldNumber(ArraySounder, "m_maxRange");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pingFan->m_computePingFan.m_maxRange * 0.001;
	mxSetFieldByNumber(ArraySounder, 0, field, field_value);

	field = mxGetFieldNumber(ArraySounder, "m_maxRangeWasFound");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pingFan->m_computePingFan.m_maxRangeFound;
	mxSetFieldByNumber(ArraySounder, 0, field, field_value);

	field = mxGetFieldNumber(ArraySounder, "m_sounderId");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pingFan->getSounderRef()->m_SounderId;
	mxSetFieldByNumber(ArraySounder, 0, field, field_value);

	field = mxGetFieldNumber(ArraySounder, "m_meanTimeFraction");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pingFan->m_ObjectTime.m_TimeFraction;
	mxSetFieldByNumber(ArraySounder, 0, field, field_value);

	field = mxGetFieldNumber(ArraySounder, "m_meanTimeCPU");
	field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
	*mxGetPr(field_value) = pingFan->m_ObjectTime.m_TimeCpu;
	mxSetFieldByNumber(ArraySounder, 0, field, field_value);

	HacTime currentPingFanTime = pingFan->m_ObjectTime;
	PingFanSingleTarget* pSingle = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanSingleTarget(currentPingFanTime, pingFan->getSounderRef()->m_SounderId);
	field = mxGetFieldNumber(ArraySounder, "singleTarget");

	if (pSingle)
	{
		mxSetFieldByNumber(ArraySounder, 0, field, getSingleTargetFanDesc(pSingle, pingFan, CModuleManager::getInstance()->GetTSAnalysisModule()));
	}
	else
	{
		mxSetFieldByNumber(ArraySounder, 0, field, NULL);
	}


	{
		field = mxGetFieldNumber(ArraySounder, "beam_data");
		const char* field_names[] = { "m_hacChannelId"
			, "m_timeCPU", "m_timeFraction"
			, "navigationAttribute", "navigationPosition", "navigationAttitude"
			, "m_bottomRange", "m_bottomWasFound","m_compensateheave","m_compensateHeaveMovies", "m_transMode" };
		unsigned int sizeBeam = pingFan->getSounderRef()->m_sounderComputeData.m_nbBeamPerFan;
		/// hop la pas bon,
		mwSize dims[2] = { 1,sizeBeam };
		mxArray* ArrayBeam = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);
		unsigned int numTable = 0;
		for (int i = 0; i < pingFan->getSounderRef()->m_numberOfTransducer; i++)
		{
			Transducer* pTrans = pingFan->getSounderRef()->GetTransducer(i);
			for (unsigned int chan = 0; chan < pTrans->m_numberOfSoftChannel; chan++)
			{
				SoftChannel* pChan = pTrans->getSoftChannelPolarX(chan);


				BeamDataObject* beamData = pingFan->getRefBeamDataObject(pChan->getSoftwareChannelId(), pChan->getSoftwareVirtualChannelId());
				int field = mxGetFieldNumber(ArrayBeam, "m_hacChannelId");
				mxArray* field_value;
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = beamData->m_hacChannelId;
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				field = mxGetFieldNumber(ArrayBeam, "m_timeCPU");
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = beamData->m_timeCPU;
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				field = mxGetFieldNumber(ArrayBeam, "m_timeFraction");
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = beamData->m_timeFraction;
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				auto channelId = pChan->getSoftwareChannelId();

				field = mxGetFieldNumber(ArrayBeam, "navigationAttribute");
				field_value = getNavAttributesDesc(pingFan->GetNavAttributesRef(channelId));
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				field = mxGetFieldNumber(ArrayBeam, "navigationPosition");
				field_value = getNavPositionDesc(pingFan->GetNavPositionRef(channelId));
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				field = mxGetFieldNumber(ArrayBeam, "navigationAttitude");
				field_value = getNavAttitudeDesc(pingFan->GetNavAttitudeRef(channelId));
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				field = mxGetFieldNumber(ArrayBeam, "m_bottomRange");
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = beamData->m_bottomRange * 0.001;
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				field = mxGetFieldNumber(ArrayBeam, "m_bottomWasFound");
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = beamData->m_bottomWasFound;
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				field = mxGetFieldNumber(ArrayBeam, "m_compensateheave");
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = pingFan->getHeaveChan(beamData->m_hacChannelId);
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				field = mxGetFieldNumber(ArrayBeam, "m_compensateHeaveMovies");
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = pingFan->getHeaveChanMovies(beamData->m_hacChannelId);
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				// FE M3D-246
				field = mxGetFieldNumber(ArrayBeam, "m_transMode");
				field_value = mxCreateDoubleMatrix(1, 1, mxREAL);
				*mxGetPr(field_value) = beamData->m_transMode;
				mxSetFieldByNumber(ArrayBeam, numTable, field, field_value);

				numTable++;
			}
		}
		mxSetFieldByNumber(ArraySounder, 0, field, ArrayBeam);

	}
	return ArraySounder;
}

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	if (nrhs != 1) {
		mexErrMsgTxt("One Input argument needed :  the pingFan index looked for ");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a number (double in fact).");

	double FanNum;
	memcpy(&FanNum, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	Init();

	if (FanNum < M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetNbFan())
	{
		PingFan* pingFan = (PingFan*)M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(FanNum);
		plhs[0] = getPingFanDesc(pingFan);
	}
	else
	{
		mexErrMsgTxt("PingFan index is greater than PingFan Num");
	}
}
