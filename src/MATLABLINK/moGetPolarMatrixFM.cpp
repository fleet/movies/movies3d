#include "mM3DKernel.h"

#include "Calibration/CalibrationModule.h"
#include "ModuleManager/ModuleManager.h"

static mxArray* getPingFanMatrixPolarFM(unsigned __int64 FanId, unsigned int transIdx)
{
	mxArray* ret = NULL;
	unsigned int Xdim = 0;
	unsigned int Ydim = 0;
	unsigned int Fdim = 0;
	const char* field_names[] = { "m_Amplitude", "m_Ranges", "m_Frequencies" };
	mwSize dims[2] = { 1, 1 };
	ret = mxCreateStructArray(2, dims, NUMBER_OF_FIELDS, field_names);

	M3DKernel* pKernel = M3DKernel::GetInstance();

	auto& params = pKernel->GetSpectralAnalysisParameters();
	if (!params.isSpectralAnalysisEnabled())
	{
		mexErrMsgTxt("Spectral analysis is not enabled...");
		return ret;
	}

	PingFan* pingFan = (PingFan*)pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(FanId);
	if (pingFan == NULL)
	{
		mexErrMsgTxt("PingFan not found check index");
		return ret;
	}

	Transducer* pTrans = pingFan->getSounderRef()->GetTransducer(transIdx);
	if (pTrans == NULL)
	{
		mexErrMsgTxt("Transducer not found check index");
		return ret;
	}

	/* populate the real part of the created array */
	TransformMap* pTransMap = pingFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transIdx);
	MemoryStruct* pMemStruct = (MemoryStruct*)pingFan->GetMemorySetRef()->GetMemoryStruct(transIdx);
	BaseMathLib::Vector2I memSize = pMemStruct->GetDataFmt()->getSize();

	auto channelId = pTrans->GetChannelId().front();

	Xdim = 1;
	Ydim = 0;
	Fdim = 0;

	auto spectralAnalysisDataObject = pingFan->getSpectralAnalysisDataObject(channelId);
	if (spectralAnalysisDataObject != nullptr)
	{
		Ydim = spectralAnalysisDataObject->numberOfRanges();
		Fdim = spectralAnalysisDataObject->numberOfFrequencies();
	}

	CalibrationModule* calibrationModule = CModuleManager::getInstance()->GetCalibrationModule();

	const mwSize ampl_dims[] = { Fdim, Ydim, Xdim };
	mxArray* ampl = mxCreateNumericArray(3, ampl_dims, mxDOUBLE_CLASS, mxREAL);
	double* start_of_ampl = (double*)mxGetData(ampl);

	const mwSize ranges_dims[] = { Ydim, Xdim };
	mxArray* ranges = mxCreateNumericArray(2, ranges_dims, mxDOUBLE_CLASS, mxREAL);
	double* start_of_ranges = (double*)mxGetData(ranges);

	const mwSize freq_dims[] = { Fdim, Xdim };
	mxArray* freq = mxCreateNumericArray(2, freq_dims, mxDOUBLE_CLASS, mxREAL);
	double* start_of_freq = (double*)mxGetData(freq);

	if (spectralAnalysisDataObject != nullptr)
	{
		for (unsigned int i = 0; i < Xdim; i++)
		{
			for (unsigned int k = 0; k < Fdim; ++k)
			{
				start_of_freq[i * Fdim + k] = spectralAnalysisDataObject->frequencyForIndex(k);
			}

			for (unsigned int j = 0; j < Ydim; j++)
			{
				start_of_ranges[i * Ydim + j] = spectralAnalysisDataObject->rangeForIndex(j);
			}

			const std::vector<std::vector<double>> svValues = calibrationModule->getSvValuesWithGain(spectralAnalysisDataObject, pTrans->getSoftChannel(channelId));

			for (unsigned int j = 0; j < Ydim; j++)
			{
				for (unsigned int k = 0; k < Fdim; ++k)
				{
					start_of_ampl[i * Fdim * Ydim + j * Fdim + k] = svValues[j][k];
				}
			}
		}
	}

	int ampl_field = mxGetFieldNumber(ret, "m_Amplitude");
	mxSetFieldByNumber(ret, 0, ampl_field, ampl);

	int range_field = mxGetFieldNumber(ret, "m_Ranges");
	mxSetFieldByNumber(ret, 0, range_field, ranges);

	int freq_field = mxGetFieldNumber(ret, "m_Frequencies");
	mxSetFieldByNumber(ret, 0, freq_field, freq);

	return ret;
}


void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	if (nrhs != 2) {
		mexErrMsgTxt("Two Input argument needed : the pingFan id, the Transducer Index");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a number (double in fact).");
	if (mxIsDouble(prhs[1]) != 1)
		mexErrMsgTxt("Input 2 must be a number (double in fact).");

	double FanId;
	memcpy(&FanId, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	double transIdx;
	memcpy(&transIdx, mxGetData(prhs[1]), mxGetElementSize(prhs[1]));

	Init();

	/* create a 2-by-2 array of unsigned 16-bit integers */
	plhs[0] = getPingFanMatrixPolarFM(FanId, transIdx);
}
