% moGetPolarToGeo return geographic coordinate from Polar coordinates
%   moGetPolarToGeo(s1,s2,s3,s4) 
%       s1  the FanId
%		s2  transducer Index
%		s3  beam Index
%		s4	Depth Polar i.e number of sample
%
% Ifremer LBerger 20/11/2008
% $Revision: 0.1 $
