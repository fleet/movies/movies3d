#include "mM3DKernel.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	unsigned char* start_of_pr;
	size_t bytes_to_copy;
	(void)nlhs; (void)nrhs; (void)prhs;  /* unused parameters */

	if (nrhs != 4) {
		mexErrMsgTxt("4 Input argument needed : the FanId,TransducerIndex,BeamIndex,DepthPolar");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a number (double in fact).");
	if (mxIsDouble(prhs[1]) != 1)
		mexErrMsgTxt("Input 2 must be a number (double in fact).");
	if (mxIsDouble(prhs[2]) != 1)
		mexErrMsgTxt("Input 3 must be a number (double in fact).");
	if (mxIsDouble(prhs[3]) != 1)
		mexErrMsgTxt("Input 4 must be a number (double in fact).");

	double pingFanId;
	memcpy(&pingFanId, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	double numTransducer;
	memcpy(&numTransducer, mxGetData(prhs[1]), mxGetElementSize(prhs[1]));

	double numBeam;
	memcpy(&numBeam, mxGetData(prhs[2]), mxGetElementSize(prhs[2]));

	double DepthPolar;
	memcpy(&DepthPolar, mxGetData(prhs[3]), mxGetElementSize(prhs[3]));

	Init();

	plhs[0] = getPolarToWorldCoord((unsigned __int64)pingFanId, (unsigned int)numTransducer, (unsigned int)numBeam, (unsigned int)DepthPolar);
}
