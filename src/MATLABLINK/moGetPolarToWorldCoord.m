% moGetPolarToWorld return world coordinate from Polar coordinates
%   moGetPolarToWorld(s1,s2,s3,s4) 
%       s1  the FanId
%		s2  transducer Index
%		s3  beam Index
%		s4	Depth Polar i.e number of sample
%
% Ifremer Cponcelet 07/30/2007
% $Revision: 0.1 $
