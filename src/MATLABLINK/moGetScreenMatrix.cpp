#include "mM3DKernel.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	if (nrhs != 2) {
		mexErrMsgTxt("Three Input argument needed : the PingFan Id , the Transducer Index,");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a number (double in fact).");
	if (mxIsDouble(prhs[1]) != 1)
		mexErrMsgTxt("Input 2 must be a number (double in fact).");


	double PingFanNum;
	memcpy(&PingFanNum, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	double transIdx;
	memcpy(&transIdx, mxGetData(prhs[1]), mxGetElementSize(prhs[1]));

	Init();

	/* create a 2-by-2 array of unsigned 16-bit integers */
	plhs[0] = getPingFanMatrixScreen(PingFanNum, transIdx);
}
