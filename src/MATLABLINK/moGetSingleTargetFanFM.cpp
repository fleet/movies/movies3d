#include "mSingleTarget.h"

#include "ModuleManager/ModuleManager.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray*prhs[])
{
	if (nrhs != 1) {
		mexErrMsgTxt("One input argument needed :   the pingFan id looked for ");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1) {
		mexErrMsgTxt("Input 1 must be a number (double in fact).");
	}

	double FanNum;
	memcpy(&FanNum, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	Init();
	
	M3DKernel *pKernel = M3DKernel::GetInstance();

	PingFan *pFan = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(FanNum);
	if (pFan)
	{
		HacTime currentPingFanTime = pFan->m_ObjectTime;
		PingFanSingleTarget *pSingle = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanSingleTarget(currentPingFanTime, pFan->getSounderRef()->m_SounderId);
		plhs[0] = getSingleTargetFMFanDesc(pSingle, pFan, CModuleManager::getInstance()->GetTSAnalysisModule());
	}
	else
	{
		mexErrMsgTxt("Ping Fan not found in memory, check the PingFan id");
	}
}
