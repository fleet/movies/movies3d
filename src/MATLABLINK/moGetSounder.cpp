#include "mM3DKernel.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	unsigned char* start_of_pr;
	size_t bytes_to_copy;
	(void)nlhs; (void)nrhs; (void)prhs;  /* unused parameters */

	if (nrhs != 1) {
		mexErrMsgTxt("One Input argument needed : the sounder id");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a number (double in fact).");

	double sounderId;
	memcpy(&sounderId, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	M3DKernel* pKernel = M3DKernel::GetInstance();
	plhs[0] = getSounderDesc(pKernel->getObjectMgr()->GetPingFanContainer().m_sounderDefinition, sounderId, true);
}
