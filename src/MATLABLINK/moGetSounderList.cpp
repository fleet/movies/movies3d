#include "mM3DKernel.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	unsigned char* start_of_pr;
	size_t bytes_to_copy;
	(void)nlhs; (void)nrhs; (void)prhs;  /* unused parameters */

	M3DKernel* pKernel = M3DKernel::GetInstance();
	plhs[0] = getSounderDesc(pKernel->getObjectMgr()->GetPingFanContainer().m_sounderDefinition, 0, false);
}
