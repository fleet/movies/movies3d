#include "mM3DKernel.h"
#include "mReader.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray*prhs[])
{
	if (nrhs != 1) {
		mexErrMsgTxt("1 Input argument needed : the target time");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a number (double in fact).");

	double targetTime;
	memcpy(&targetTime, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	// initialisations ...
	Init();

	ReaderCtrl *pRead = ReaderCtrl::getInstance();
	M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().RemoveAllSingleTargets();

	GoToTarget target;
	// on ne propose pas le goto par ping (a cause des ambiguités si plusieurs sondeurs)
	target.byPing = false;
	target.targetTime.m_TimeCpu = (unsigned long)targetTime;
	target.targetTime.m_TimeFraction = (unsigned short)((targetTime - (double)(target.targetTime.m_TimeCpu))*10000.0);
	pRead->GoTo(target);
}
