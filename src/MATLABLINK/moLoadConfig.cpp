#include "mM3DKernel.h"
#include "ModuleManager/ModuleManager.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	char* input_buf;
	mwSize buflen;
	// Check for proper number of arguments    
	if (nrhs != 1) {
		mexErrMsgTxt("One Input argument needed : folder path");
	}

	// input must be a string
	if (mxIsChar(prhs[0]) != 1) {
		mexErrMsgTxt("Input must be a string.");
	}

	// input must be a row vector
	if (mxGetM(prhs[0]) != 1) {
		mexErrMsgTxt("Input 1 must be a row vector.");
	}

	// get the length of the input string 
	buflen = (mxGetM(prhs[0]) * mxGetN(prhs[0])) + 1;

	// copy the string data from prhs[0] into a C string input_ buf
	input_buf = mxArrayToString(prhs[0]);
	if (input_buf == NULL) {
		mexErrMsgTxt("Could not convert input to string.");
	}

	// Initialize Movies3D
	Init();

	std::string str;
	if (CModuleManager::getInstance()->IsTreatmentEnabled(str)) {
		mexErrMsgTxt("Could not load config while treatment(s) running :");
		mexErrMsgTxt(str.c_str());
	}

	std::string path, prefix;
	std::string workingStr = input_buf;

	// on doit d�couper le chemin complet du dossier de configuration
	// pour avoir le chemin d'une part et le nom de la configuration d'autre part.
	int pos = workingStr.find_last_of("/");
	path = workingStr.substr(0, pos);
	prefix = workingStr.substr(pos + 1);

	// changement du r�pertoire de configuration
	CModuleManager::getInstance()->ChangeConfigurationCurrentDirectory(path, prefix);

	// chargement de la configuration (NULL car pas de parametres d'affichage a loader)
	CModuleManager::getInstance()->LoadConfiguration({});
}
