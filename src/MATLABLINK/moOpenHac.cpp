#include "mReader.h"

#include "Reader/MovFileRun.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	char* input_buf;
	mwSize buflen;
	/* Check for proper number of arguments */

	if (nrhs != 1) {
		mexErrMsgTxt("One Input argument needed : file Name");
	}
	/* input must be a string */
	if (mxIsChar(prhs[0]) != 1)
		mexErrMsgTxt("Input must be a string.");

	/* input must be a row vector */
	if (mxGetM(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a row vector.");
	/* get the length of the input string */
	buflen = (mxGetM(prhs[0]) * mxGetN(prhs[0])) + 1;

	/* copy the string data from prhs[0] into a C string input_ buf.    */
	input_buf = mxArrayToString(prhs[0]);

	if (input_buf == NULL)
		mexErrMsgTxt("Could not convert input to string.");

	/* input must be a number */
	Init();

	/* Do the actual computations in a subroutine */

	ReaderCtrl* pRead = ReaderCtrl::getInstance();

	MovFileRun mFileRun;
	mFileRun.SetSourceName(input_buf, ".hac");

	pRead->OpenFileStream(mFileRun);
	pRead->ReadSounderConfig();
}
