#include "mReader.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	Init();
	ReaderCtrl* pRead = ReaderCtrl::getInstance();

	pRead->SyncReadChunk();
	ChunckEventList ref;
	pRead->WaitEndChunck(ref);
}
