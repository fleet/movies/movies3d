% moReadWholeFile a whole File and increase memory storage to keep all
% pingFan read
%
% Ifremer Cponcelet 02/01/2008
% $Revision: 0.1 $

function moReadWholeFile(FileName)
Z=moFilterModule;
A=moGetFilterModuleParameter(Z);
A.m_bIsEnable=1;
moSetFilterModuleParameter(Z,A);

if isequal(FileName,'') ||  exist(FileName,'file')~=2
    PathName='';
    Name='';
	[Name,PathName,FilterIndex]=uigetfile('*.hac','Load HAC File');
    FileName=strcat(PathName,Name);
end
    
if exist(FileName,'file') ~= 2
    Errordlg('File does not exist')
    
else
    
    % define our chunk
    % define our chunk
    % we define a chunk of  ping for the given sounderId
    ParameterKernel= moKernelParameter();
    ParameterDef=moLoadKernelParameter(ParameterKernel);

    ParameterDef.m_bIgnorePhaseData=1
    ParameterDef.m_bAutoLengthEnable=1;
    ParameterDef.m_bAutoDepthEnable=1;

    moSaveKernelParameter(ParameterKernel,ParameterDef);

    clear Parameter;
    clear ParameterDef;


    % open Hac file
    moOpenHac(FileName);
    %we read the whole file
    FileStatus= moGetFileStatus;
    while FileStatus.m_StreamClosed < 1
        moReadChunk();
        FileStatus= moGetFileStatus();
    end;

    fprintf('End Read\n');
end;
end