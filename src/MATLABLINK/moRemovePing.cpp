#include "mM3DKernel.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	unsigned char* start_of_pr;
	size_t bytes_to_copy;
	(void)nlhs; (void)nrhs; (void)prhs;  /* unused parameters */

	if (nrhs != 1) {
		mexErrMsgTxt("One Input argument needed : the ping id");
	}

	double pingId;
	memcpy(&pingId, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	Init();

	M3DKernel* pKernel = M3DKernel::GetInstance();
	PingFanContainer& pingFanContainer = pKernel->getObjectMgr()->GetPingFanContainer();
	bool bSuccess = pingFanContainer.RemovePing(pingId);
	if (bSuccess == false) {
		mexWarnMsgTxt("Unable to delete ping from given id");
	}
}
