#include "mM3DKernel.h"

#include "mNavAttribute.h"
#include "mNavPosition.h"
#include "mNavAttitude.h"

void setPingFanDesc(PingFan* pingFan, const mxArray* myStruct)
{
	if (!pingFan)
		mexErrMsgTxt("NULL PingFan");
	if (!myStruct)
		mexErrMsgTxt("Bad Structure");

	pingFan->m_computePingFan.m_pingId = *GetFieldDouble(myStruct, "m_pingId", 0);

	double* ptr = GetFieldDouble(myStruct, "m_cumulatedNav", 0);

	pingFan->m_relativePingFan.m_cumulatedNav.x = *ptr;
	pingFan->m_relativePingFan.m_cumulatedNav.y = *(ptr + 1);
	pingFan->m_relativePingFan.m_cumulatedNav.z = *(ptr + 2);

	pingFan->m_relativePingFan.m_cumulatedDistance = *GetFieldDouble(myStruct, "m_cumulatedDistance", 0) / 1.852 / 1000.0;

	pingFan->m_computePingFan.m_maxRange = *GetFieldDouble(myStruct, "m_maxRange", 0) / 0.001;

	pingFan->m_computePingFan.m_maxRangeFound = *GetFieldDouble(myStruct, "m_maxRangeWasFound", 0);

	pingFan->m_ObjectTime.m_TimeFraction = *GetFieldDouble(myStruct, "m_meanTimeFraction", 0);
	pingFan->m_ObjectTime.m_TimeCpu = *GetFieldDouble(myStruct, "m_meanTimeCPU", 0);

	unsigned int sizeBeam = pingFan->getSounderRef()->m_sounderComputeData.m_nbBeamPerFan;
	mxArray* pDefChan = mxGetField(myStruct, 0, "beam_data");
	unsigned int numTable = 0;

	for (int i = 0; i < pingFan->getSounderRef()->m_numberOfTransducer; i++)
	{
		Transducer* pTrans = pingFan->getSounderRef()->GetTransducer(i);
		for (unsigned int chan = 0; chan < pTrans->m_numberOfSoftChannel; chan++)
		{
			SoftChannel* pChan = pTrans->getSoftChannelPolarX(chan);
			//		    pDef= mxGetField(pDefChan, numTable , "beam_data");

			BeamDataObject* beamData = pingFan->getRefBeamDataObject(pChan->getSoftwareChannelId(), pChan->getSoftwareVirtualChannelId());
			beamData->m_bottomWasFound = *GetFieldDouble(pDefChan, "m_bottomWasFound", numTable);
			if (beamData->m_bottomWasFound)
			{
				double myBottomRange = *GetFieldDouble(pDefChan, "m_bottomRange", numTable);
				beamData->m_bottomRange = floor(myBottomRange / 0.001);
			}
			else
			{
				beamData->m_bottomRange = FondNotFound;
			}

			auto channelId = pChan->getSoftwareChannelId();
			mxArray* pDef = mxGetField(myStruct, 0, "navigationAttribute");
			setNavAttributesDesc(pingFan->GetNavAttributesRef(channelId), pDef);

			pDef = mxGetField(myStruct, 0, "navigationPosition");
			setNavPositionDesc(pingFan->GetNavPositionRef(channelId), pDef);

			pDef = mxGetField(myStruct, 0, "navigationAttitude");
			setNavAttitudeDesc(pingFan->GetNavAttitudeRef(channelId), pDef);

			numTable++;
		}
		//	numTable++;
	}
	pingFan->OnBeamChange();
	pingFan->ComputeMatrix();
}

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	if (nrhs != 2) {
		mexErrMsgTxt("Two Input argument needed :   the pingFan ID, and the pingFan structure ");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a number (double in fact).");

	if (mxIsStruct(prhs[1]) != 1)
		mexErrMsgTxt("Input 2 Bad structure argument");

	double FanNum;
	memcpy(&FanNum, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	const mxArray* myStruct = prhs[1];

	Init();

	PingFan* pingFan = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(FanNum);
	if (pingFan)
		setPingFanDesc(pingFan, myStruct);
	else
		mexErrMsgTxt("Ping Fan not in memory check Id");
}
