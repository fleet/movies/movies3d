#include "mPolarMatrix.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	if (nrhs != 3) {
		mexErrMsgTxt("3 Input argument needed : the PingFanId, the Transducer Index and the matrix structure");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a number (double in fact).");
	if (mxIsDouble(prhs[1]) != 1)
		mexErrMsgTxt("Input 2 must be a number (double in fact).");

	double PingFanNum;
	memcpy(&PingFanNum, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	double transIdx;
	memcpy(&transIdx, mxGetData(prhs[1]), mxGetElementSize(prhs[1]));

	if (mxIsStruct(prhs[2]) != 1)
		mexErrMsgTxt("Input 4 Bad structure argument");

	mxArray* pAmp = NULL;
	mxArray* pPhaseAthw = NULL;
	mxArray* pPhaseAlon = NULL;

	mxArray* pDef = mxGetField(prhs[2], 0, "m_Amplitude");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		mexErrMsgTxt("Bad Structure");
	}
	else
	{
		pAmp = pDef;
	}

	pDef = mxGetField(prhs[2], 0, "m_AthwartAngle");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		// mexErrMsgTxt("Bad Structure");
	}
	else
	{
		pPhaseAthw = pDef;
	}

	pDef = mxGetField(prhs[2], 0, "m_AlongAngle");
	if (pDef == NULL || mxGetClassID(pDef) != mxDOUBLE_CLASS)
	{
		// mexErrMsgTxt("Bad Structure");
	}
	else
	{
		pPhaseAlon = pDef;
	}
#pragma message("___________________________________________________________________") 
#pragma message("                                                                   ") 
#pragma message("                                                                   ") 
#pragma message("                                                                   ") 
#pragma message("                                                                   ") 
#pragma message("Voir pour rajouter l'ecriture depuis matlab du taux de recouvrement") 
#pragma message("                                                                   ") 
#pragma message("                                                                   ") 
#pragma message("                                                                   ") 
#pragma message("                                                                   ") 
#pragma message("                                                                   ") 
#pragma message("                                                                   ") 
#pragma message("___________________________________________________________________") 

	Init();

	setPingFanMatrixPolar(PingFanNum, transIdx, pAmp, pPhaseAthw, pPhaseAlon);
}
