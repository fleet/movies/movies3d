% moSetPolarMatrix save to memory a given polar Matrix 
%   getPingFan(s1,s2,s3) 
%       s1 the PingFan id
%       s2 the transducer index
%       s3 the polar matrix structure to write
%
% Ifremer Cponcelet 07/30/2007
% $Revision: 0.1 $
