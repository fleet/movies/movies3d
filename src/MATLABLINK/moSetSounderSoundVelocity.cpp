#include "mM3DKernel.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	if (nrhs != 2) {
		mexErrMsgTxt("2 Input argument needed : the sounder Id and the offset value");
	}

	/* input must be a number */
	if (mxIsDouble(prhs[0]) != 1)
		mexErrMsgTxt("Input 1 must be a number (double in fact).");
	if (mxIsDouble(prhs[1]) != 1)
		mexErrMsgTxt("Input 2 must be a number (double in fact).");

	double sounderId;
	memcpy(&sounderId, mxGetData(prhs[0]), mxGetElementSize(prhs[0]));

	double value;
	memcpy(&value, mxGetData(prhs[1]), mxGetElementSize(prhs[1]));

	Init();

	M3DKernel* pInst = M3DKernel::GetInstance();
	Sounder* pSound = pInst->getObjectMgr()->GetSounderDefinition().GetSounderWithId(sounderId);
	pSound->m_soundVelocity = value;
}
