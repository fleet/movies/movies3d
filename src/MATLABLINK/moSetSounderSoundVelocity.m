% moSetSounderSoundVelocity Replace the current value for sound velocity used to compute 
%   moSetSounderSoundVelocity(s1,s2) 
%       s1 the sounder id
%       s2 the sound velocity value
%
% Ifremer LBerger 14/05/2011
% $Revision: 0.1 $
