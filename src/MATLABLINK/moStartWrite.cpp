#include "mReader.h"
#include "Reader/WriteAlgorithm/WriteAlgorithm.h"

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	char* DestDirBuffer = NULL;
	char* FileNamePrefix = NULL;
	mwSize buflen;

	/* Check for proper number of arguments */
	if (nrhs != 2 && nrhs != 0) {
		mexErrMsgTxt("Either Zero or Two Input argument needed : the dest directory and the file Name prefix");
	}

	if (nrhs != 0)
	{
		/* input must be a string */
		if (mxIsChar(prhs[0]) != 1)
			mexErrMsgTxt("Input must be a string.");

		/* input must be a row vector */
		if (mxGetM(prhs[0]) != 1)
			mexErrMsgTxt("Input 1 must be a row vector.");
		/* get the length of the input string */
		buflen = (mxGetM(prhs[0]) * mxGetN(prhs[0])) + 1;

		/* copy the string data from prhs[0] into a C string input_ buf.    */
		DestDirBuffer = mxArrayToString(prhs[0]);

		if (DestDirBuffer == NULL)
			mexErrMsgTxt("Could not convert input to string.");

		/* input must be a string */
		if (mxIsChar(prhs[1]) != 1)
			mexErrMsgTxt("Input must be a string.");

		/* input must be a row vector */
		if (mxGetM(prhs[1]) != 1)
			mexErrMsgTxt("Input 1 must be a row vector.");
		/* get the length of the input string */
		buflen = (mxGetM(prhs[1]) * mxGetN(prhs[1])) + 1;

		/* copy the string data from prhs[0] into a C string input_ buf.    */
		FileNamePrefix = mxArrayToString(prhs[1]);

		if (FileNamePrefix == NULL)
			mexErrMsgTxt("Could not convert input to string.");
	}

	Init();

	/* Do the actual computations in a subroutine */

	ReaderCtrl* pRead = ReaderCtrl::getInstance();
	WriteAlgorithm* pW = pRead->GetWriteAlgorithm();
	pW->CloseFile();
	if (FileNamePrefix)
	{
		pW->SetFilePrefix(FileNamePrefix);
	}
	if (DestDirBuffer)
	{
		std::string name(DestDirBuffer);
		name += '\\';
		pW->SetDirectory(name.c_str());
	}
	pW->SetOutputType(WriteAlgorithm::OutputType::Hac);
	pW->setEnable(true);
}
