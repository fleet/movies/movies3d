#include "mReader.h"
#include "Reader/WriteAlgorithm/WriteAlgorithm.h"


void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	char* input_buf;
	mwSize buflen;

	/* input must be a number */
	Init();

	/* Do the actual computations in a subroutine */
	ReaderCtrl* pRead = ReaderCtrl::getInstance();
	WriteAlgorithm* pW = pRead->GetWriteAlgorithm();
	pW->CloseFile();
}
