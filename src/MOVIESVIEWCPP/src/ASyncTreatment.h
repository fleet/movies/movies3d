#pragma once


using namespace System::Runtime::InteropServices;
static std::string netStr2CppStr(String ^ ns)
{
	char* str = (char*)Marshal::StringToHGlobalAnsi(ns).ToPointer();
	std::string ret(str);
	Marshal::FreeHGlobal(IntPtr(str));
	return ret;
}
static String^ cppStr2NetStr(const char* s)
{
	return gcnew String(s);
}

namespace MOVIESVIEWCPP {
	// enumeration des traitements asynchrones possibles deuis l'IHM Movies3D
	enum TreatmentType
	{
		eRF, // r�ponse fr�quentielle
		eRA, // r�ponse angulaire
		eUnknown
	};

	ref class ASyncTreatment
	{
	public:
		ASyncTreatment(TreatmentType type) { mType = type; };
		TreatmentType mType;
	};

	ref class ASyncTreatmentProgresStatus
	{
	public:
		ASyncTreatmentProgresStatus(String^ stateDesc, int percentage)
		{
			StateDesc = stateDesc;
			Percentage = percentage;
		}

		String^ StateDesc;
		int Percentage;
	};
}