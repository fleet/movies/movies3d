#include "AVIWriter.h"

#include <windows.h>
#include <vfw.h>

using namespace MOVIESVIEWCPP;

// ***************************************************************************
// Constructeur / Destructeur
// ***************************************************************************
AVIWriter::AVIWriter()
{
	InitializeAVIAPI();

	m_pFileHandle = new int(0);
}

AVIWriter::~AVIWriter()
{
	CleanAVIAPI();

	delete m_pFileHandle;
}

// ***************************************************************************
// méthodes statiques
// ***************************************************************************

// Initialisation de l'api AVI windows
void AVIWriter::InitializeAVIAPI()
{
	if (s_InitializeCounter == 0)
	{
		AVIFileInit();
	}
	s_InitializeCounter++;
}

// Nettoyage de l'api AVI windows
void AVIWriter::CleanAVIAPI()
{
	s_InitializeCounter--;
	if (s_InitializeCounter == 0)
	{
		AVIFileExit();
	}
}


// ***************************************************************************
// Traitements publiques
// ***************************************************************************
int AVIWriter::StartAVI(System::String^ fileName)
{
	int result = 0;

	// conversion en const char * nécessaire
	System::IntPtr ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(fileName);
	const char* strFile = static_cast<const char*>(ip.ToPointer());

	result = AVIFileOpen((PAVIFILE*)m_pFileHandle, strFile, OF_WRITE | OF_CREATE, 0L);

	System::Runtime::InteropServices::Marshal::FreeHGlobal(ip);

	// on ne configure pour la suite qu'en cas de succès
	if (result == 0)
	{
		// définition des caractéristiques de la video
		AVISTREAMINFO strhdr;
		memset(&strhdr, 0, sizeof(strhdr));
		strhdr.fccType = streamtypeVIDEO;// stream type
		strhdr.fccHandler = 0;
		strhdr.dwScale = 1;
		strhdr.dwRate = 15;
		strhdr.dwQuality = (DWORD)-1;
		// TODO - passer la taille de la video en membre
		/*strhdr.dwSuggestedBufferSize = height_ * stride_;
		strhdr.rect_bottom           = height_;
		strhdr.rect_right            = width_;

		AVIFileCreateStream((PAVIFILE*)m_pFileHandle,  // file pointer
							&this->Internals->Stream,  // returned stream pointer
							&strhdr);      // stream header

	*/

	}

	return result;
}

void AVIWriter::AddBitmap(System::Drawing::Bitmap^ bitmap)
{
	// TODO
}

void AVIWriter::StopAVI()
{
	// TODO
}
