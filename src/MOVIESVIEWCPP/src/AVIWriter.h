// -*- C++ -*-
// ****************************************************************************
// Class: AVIWriter
//
// Description: Classe permettant la cr�ation de vid�os AVI � partir d'une s�rie
// de bitmaps
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Avril 2010
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace MOVIESVIEWCPP {
	ref class AVIWriter
	{
	public:

		// ***************************************************************************
		// Constructeur / Destructeur
		// ***************************************************************************

		AVIWriter();

		virtual ~AVIWriter();

		// ***************************************************************************
		// m�thodes statiques
		// ***************************************************************************
	private:
		// Initialisation de l'api AVI windows
		static void InitializeAVIAPI();

		// Nettoyage de l'api AVI windows
		static void CleanAVIAPI();


	public:
		// ***************************************************************************
		// Traitements publiques
		// ***************************************************************************

		int StartAVI(System::String^ fileName);

		void AddBitmap(System::Drawing::Bitmap^ bitmap);

		void StopAVI();

	private:

		// ***************************************************************************
		// Donn�es membres priv�es
		// ***************************************************************************
		static int s_InitializeCounter = 0;

		// handle du fichier AVI cible
		int *m_pFileHandle;

	};
}