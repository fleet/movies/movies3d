#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de AboutForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class AboutForm : public System::Windows::Forms::Form
	{
	public:
        AboutForm();

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~AboutForm()
		{
			if (components)
			{
				delete components;
			}
		}
  private: System::Windows::Forms::Label^  labelVersion;
  private: System::Windows::Forms::Label^  labelCopyRight;
  protected: 

  private: System::Windows::Forms::Label^  label1;
  private: System::Windows::Forms::PictureBox^  pictureBoxLogo;

  private: System::Windows::Forms::Label^  labelMovies3D;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
      System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(AboutForm::typeid));
      this->labelVersion = (gcnew System::Windows::Forms::Label());
      this->labelCopyRight = (gcnew System::Windows::Forms::Label());
      this->label1 = (gcnew System::Windows::Forms::Label());
      this->pictureBoxLogo = (gcnew System::Windows::Forms::PictureBox());
      this->labelMovies3D = (gcnew System::Windows::Forms::Label());
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBoxLogo))->BeginInit();
      this->SuspendLayout();
      // 
      // labelVersion
      // 
      this->labelVersion->AutoSize = true;
      this->labelVersion->Location = System::Drawing::Point(129, 39);
      this->labelVersion->Name = L"labelVersion";
      this->labelVersion->Size = System::Drawing::Size(51, 13);
      this->labelVersion->TabIndex = 0;
      this->labelVersion->Text = L"Version : ";
      // 
      // labelCopyRight
      // 
      this->labelCopyRight->AutoSize = true;
      this->labelCopyRight->Location = System::Drawing::Point(42, 73);
      this->labelCopyRight->Name = L"labelCopyRight";
      this->labelCopyRight->Size = System::Drawing::Size(145, 13);
      this->labelCopyRight->TabIndex = 1;
      this->labelCopyRight->Text = L"Copyright (C) IFREMER 2017";
      // 
      // label1
      // 
      this->label1->AutoSize = true;
      this->label1->Location = System::Drawing::Point(177, 39);
      this->label1->Name = L"label1";
      this->label1->Size = System::Drawing::Size(31, 13);
      this->label1->TabIndex = 2;
      this->label1->Text = L"2.2.12";
      // 
      // pictureBoxLogo
      // 
      this->pictureBoxLogo->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBoxLogo.Image")));
      this->pictureBoxLogo->Location = System::Drawing::Point(12, 12);
      this->pictureBoxLogo->Name = L"pictureBoxLogo";
      this->pictureBoxLogo->Size = System::Drawing::Size(73, 45);
      this->pictureBoxLogo->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
      this->pictureBoxLogo->TabIndex = 3;
      this->pictureBoxLogo->TabStop = false;
      // 
      // labelMovies3D
      // 
      this->labelMovies3D->AutoSize = true;
      this->labelMovies3D->Location = System::Drawing::Point(141, 18);
      this->labelMovies3D->Name = L"labelMovies3D";
      this->labelMovies3D->Size = System::Drawing::Size(58, 13);
      this->labelMovies3D->TabIndex = 4;
      this->labelMovies3D->Text = L"Movies 3D";
      // 
      // AboutForm
      // 
      this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
      this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
      this->ClientSize = System::Drawing::Size(228, 92);
      this->Controls->Add(this->labelMovies3D);
      this->Controls->Add(this->pictureBoxLogo);
      this->Controls->Add(this->label1);
      this->Controls->Add(this->labelCopyRight);
      this->Controls->Add(this->labelVersion);
      this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
      this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
      this->MaximizeBox = false;
      this->MinimizeBox = false;
      this->Name = L"AboutForm";
      this->Text = L"About Movies3D";
      (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBoxLogo))->EndInit();
      this->ResumeLayout(false);
      this->PerformLayout();

    }
#pragma endregion
	};
}
