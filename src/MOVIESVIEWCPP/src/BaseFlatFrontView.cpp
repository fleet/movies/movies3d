#include "BaseFlatFrontView.h"
#include "DisplayView.h"
#include "DisplayParameter.h"
#include "utils.h"
#include "ColorPaletteEcho.h"
#include "Reader/ReaderCtrl.h"
#include <math.h>

#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "ModuleManager/ModuleManager.h"
#include "EchoIntegration/EchoIntegrationModule.h"
#include "M3DKernel/datascheme/TransformSet.h"

#include "ModuleManager/ModuleManager.h"
#include "Calibration/CalibrationModule.h"

#include "TSAnalysis/TSAnalysisModule.h"
#include "TSAnalysis/TSTrack.h"

#include "IViewSelectionObserver.h"

#include "BottomDetection/BottomDetectionContourLine.h"
#include "BottomDetection/BottomDetectionModule.h"
#include "BottomDetection/ContourLineDef.h"

using namespace MOVIESVIEWCPP;

#define FREQUENCY_SPECTROGRAM_OPTION "Frequency Spectrogram"

BaseFlatFrontView::BaseFlatFrontView()
	:BaseFlatView()
{
	m_ViewDesc = "MOVIES 3D : 2D Front View";
	HistoricHScrollBar->Visible = false;
	buttonSpectralView->Visible = false;

	// disable split on front views
	SplitEnabled = false;

	m_cursorPen->Color = Color::Green;
	m_cursorPen->Width = 2.0f;
	m_cursorPen->DashStyle = System::Drawing::Drawing2D::DashStyle::Dash;

	ModeChanged += gcnew BaseFlatView::SelectionModeChangedHandler(this, &BaseFlatFrontView::OnModeChanged);
}

System::Void BaseFlatFrontView::DrawNow()
{
	BaseFlatView::DrawNow();

	if (m_bmp->IsValid())
	{
		// UPDATE STRECTH RATIO
		if (m_bFullyStretched)
		{
			const double verticalRatio = (double)FlatPictureBox->Height / (double)m_bmp->Height();
			const double horizontalRatio = (double)FlatPictureBox->Width / (double)m_bmp->Width();
			m_StretchRatio = std::min(horizontalRatio, verticalRatio);
		}
		else
		{
			m_StretchRatio = 1;
		}
	}
	else
	{
		m_StretchRatio = 1;
	}
}

struct LineSorter
{
	bool operator()(const BaseMathLib::Vector2I & p1, const BaseMathLib::Vector2I & p2) const
	{
		return p1.x < p2.x;
	}
};

System::Void BaseFlatFrontView::DrawEchoLine(Graphics^  e, std::vector<BaseMathLib::Vector2I> echoesLine)
{
	TransducerFlatFrontView * refFront = (TransducerFlatFrontView *)GetTransducerView();
	if (refFront)
	{
		std::sort(echoesLine.begin(), echoesLine.end(), LineSorter());

		int startDepth = (int)refFront->GetStartPixel();
		int lineWidth = m_bottomPen->Width;

		int width = FlatPictureBox->Width;
		int nbCols = refFront->GetWidth();
		double realDisplayWidth = GetRealDisplayWidth();
		const double xRatio = realDisplayWidth / nbCols;
		const double xOffset = (width - realDisplayWidth)  * 0.5;

		int nbRows = refFront->GetHeight();
		double realDisplayHeight = GetRealDisplayHeight();
		const double yRatio = realDisplayHeight / nbRows;
		const double yOffset = 0.5 - lineWidth * 0.5;

		const int nbPoints = echoesLine.size();
		if (nbPoints > 1)
		{
			BaseMathLib::Vector2I echoPos = echoesLine[0];
			int x1 = (int)floor(xOffset + 0.5 + echoPos.x * xRatio);
			int y1 = (int)floor(yOffset + (1 + echoPos.y - startDepth) * yRatio);
			int x2 = x1;
			int y2 = y1;

			for (int i = 1; i < nbPoints; ++i)
			{
				echoPos = echoesLine[i];
				x2 = (int)floor(xOffset + 0.5 + echoPos.x * xRatio);
				y2 = (int)floor(yOffset + (1 + echoPos.y - startDepth) * yRatio);

				e->DrawLine(m_bottomPen, x1, y1, x2, y2);

				x1 = x2;
				y1 = y2;
			}
		}
	}
}

System::Void BaseFlatFrontView::DrawBottom(Graphics^  e)
{
	TransducerFlatFrontView * refFront = (TransducerFlatFrontView *)GetTransducerView();
	if (refFront)
	{
		DrawEchoLine(e, refFront->GetBottomPoints());
	}
}

System::Void BaseFlatFrontView::DrawContourLines(Graphics^  e)
{
	TransducerFlatFrontView * refFront = (TransducerFlatFrontView *)GetTransducerView();
	if (refFront)
	{
		const auto & contourLines = refFront->GetContourLines();
		for (auto iterLine = contourLines.cbegin(); iterLine != contourLines.cend(); iterLine++)
		{
			DrawEchoLine(e, *iterLine);
		}
	}
}

System::Void BaseFlatFrontView::DrawRefNoise(Graphics^  e)
{
	TransducerFlatFrontView * refFront = (TransducerFlatFrontView *)GetTransducerView();
	if (refFront)
	{
		auto echoesLine = refFront->GetRefNoisePoints();
		DrawNoisePoints(e, echoesLine, m_refNoisePen, m_refNoiseBrush);
	}
}

System::Void BaseFlatFrontView::DrawNoise(Graphics^  e)
{
	TransducerFlatFrontView * refFront = (TransducerFlatFrontView *)GetTransducerView();
	if (refFront)
	{
		auto echoesLine = refFront->GetNoisePoints();
		DrawNoisePoints(e, echoesLine, m_noisePen, m_noiseBrush);
	}
}

System::Void BaseFlatFrontView::DrawNoisePoints(Graphics^ e, std::vector<BaseMathLib::Vector2I> echoesLine, Pen^ pen, Brush^ brush)
{
	TransducerFlatFrontView * refFront = (TransducerFlatFrontView *)GetTransducerView();
	if (refFront)
	{
		std::sort(echoesLine.begin(), echoesLine.end(), LineSorter());

		int startDepth = (int)refFront->GetStartPixel();
		int lineWidth = m_noisePen->Width;

		int height = FlatPictureBox->Height;
		int width = FlatPictureBox->Width;

		int nbCols = refFront->GetWidth();
		double realDisplayWidth = GetRealDisplayWidth();
		const double xRatio = realDisplayWidth / nbCols;
		const double xOffset = (width - realDisplayWidth)  * 0.5;

		int nbRows = refFront->GetHeight();
		double realDisplayHeight = GetRealDisplayHeight();
		const double yRatio = realDisplayHeight / nbRows;
		const double yOffset = 0.5 - lineWidth * 0.5;

		System::Collections::Generic::List<System::Drawing::Point> ^ noisePoints = gcnew System::Collections::Generic::List<System::Drawing::Point>();

		const int nbPoints = echoesLine.size();

		if (nbPoints > 0)
		{
			array<System::Drawing::Point> ^pts = gcnew array<System::Drawing::Point>(nbPoints + 4);

			for (int i = 0; i < nbPoints; ++i)
			{
				BaseMathLib::Vector2I  echoPos = echoesLine[i];
				pts[i + 2].X = (int)floor(xOffset + 0.5 + echoPos.x * xRatio);
				pts[i + 2].Y = (int)floor(yOffset + (1 + echoPos.y - startDepth) * yRatio);
			}

			pts[0].X = 0;
			pts[0].Y = height;

			pts[1].X = 0;
			pts[1].Y = pts[2].Y;

			pts[nbPoints + 2].X = width;
			pts[nbPoints + 2].Y = pts[nbPoints + 1].Y;

			pts[nbPoints + 3].X = width;
			pts[nbPoints + 3].Y = height;

			e->FillPolygon(brush, pts);
			e->DrawPolygon(pen, pts);
		}
	}
}

System::Void BaseFlatFrontView::DrawShoalId(Graphics^  e)
{
}

System::Void BaseFlatFrontView::DrawCursor(Graphics^  e)
{
	if (mCursorCenterXpercent >= 0 && mCursorCenterXpercent <= 100)
	{
		// Create coordinates of points that define line.

		int x = (int)round(FlatPictureBox->ClientSize.Width*this->mCursorCenterXpercent / 100.0);
		int y1 = 0;
		int y2 = FlatPictureBox->ClientSize.Height;
		e->DrawLine(m_cursorPen, x, y1, x, y2);
	}
}

System::Void	BaseFlatFrontView::DrawImage(Graphics^  e)
{
	// si on n'a pas d'image r�elle on ne colorie pas tout en noir, ce n'est pas tr�s joli
	if (m_bRealPicture)
		e->Clear(Color::Black);

	e->InterpolationMode = System::Drawing::Drawing2D::InterpolationMode::NearestNeighbor;
	e->PixelOffsetMode = System::Drawing::Drawing2D::PixelOffsetMode::Half; // problemes sinon si gros zoom																	
																		
	// soit on �tire l'image pour la faire rentrer, soit non
	if (DisplayParameter::getInstance()->GetStrech())
	{
		m_bFullyStretched = true;
		m_bmp->Render(e, this->FlatPictureBox->ClientRectangle);
	}
	else
	{
		m_bFullyStretched = false;
		System::Drawing::Rectangle rect;

		/*if (GetHorizontalStretchRatio() < GetVerticalStretchRatio())
		{
			// dans ce cas, on peut �liminer les marges sur les cot�s
			rect.X = 0;
			rect.Y = 0;
			rect.Width = (int)(0.5 + m_bmp->Width() * m_StretchRatio);
			rect.Height = (int)(0.5 + m_bmp->Height() * m_StretchRatio);
		}
		else
		{
			// dans ce cas, on peut �liminer la marge du bas
			rect.X = (int)round(GetHorizontalMargin());
			rect.Y = 0;
			rect.Height = (int)(0.5 + m_bmp->Height() * m_StretchRatio);
			rect.Width = (int)(0.5 + m_bmp->Width() * m_StretchRatio);
		}
		*/

		// dans ce cas, on peut �liminer la marge du bas
		rect.X = (int)round(GetHorizontalMargin());
		rect.Y = 0;
		rect.Height = (int)(0.5 + m_bmp->Height() * m_StretchRatio);
		rect.Width = (int)(0.5 + m_bmp->Width() * m_StretchRatio);

		m_bmp->Render(e, rect);
	}
}

System::Void  BaseFlatFrontView::CursorHasChanged(double value) {
	// OTK - 02/04/2009 - blindage pour �viter le plantage si on clique sans avoir charg� de fichier
	if (m_viewContainer->SideViewContainer->GetView(0))
	{
		m_NeedRedraw = true;

		// on doit adapter la position du curseur en fonction du zoom
		TransducerFlatFrontView * ref = (TransducerFlatFrontView *)GetTransducerView();
		if (ref)
		{
			double realCursorPercent = 100.*(0.01*ConvertStretchToFlatPercent(value)*ref->GetWidth() + ref->GetStartHPixel()) / (double)ref->GetTotalRange();
			m_DataCursorPercent = realCursorPercent;
			m_viewContainer->SideViewContainer->SetActiveSlide(realCursorPercent, GetCurrentIndex());

			UpdateCursor();
			this->FlatPictureBox->Refresh();
		}
	}
}

System::Void  BaseFlatFrontView::UpdateCursor()
{
	if (m_viewContainer->SideViewContainer->GetView(0))
	{
		// on doit adapter la position du curseur en fonction du zoom
		TransducerFlatFrontView * ref = (TransducerFlatFrontView *)GetTransducerView();
		if (ref)
		{
			double realActiveSlide = m_viewContainer->SideViewContainer->GetActiveSlide(GetCurrentIndex());
			realActiveSlide = (realActiveSlide*(double)ref->GetTotalRange() / 100. - ref->GetStartHPixel()) / (0.01*ref->GetWidth());
			SetCursorPosition(ConvertFlatToStretchPercent(realActiveSlide));
		}
	}
}

System::String^ BaseFlatFrontView::FormatEchoInformation(int ScreenPosX, int ScreenPosY)
{
	System::String ^result = "";
	TransducerFlatFrontView * ref = (TransducerFlatFrontView *)GetTransducerView();
	if (ref)
	{
		// on passe la position de la souris sur l'image en % de la taille de l'image
		double posX, posY;
		// +0.5 pour tenir compte de l'interpolation de l'image en nearestNeighboor
		posX = ((double)ScreenPosX - GetHorizontalMargin() + 0.5) / (GetRealDisplayWidth());
		posY = ((double)ScreenPosY + 0.5) / (GetRealDisplayHeight());

		if (posX >= 0 && posX <= 1 && posY >= 0 && posY <= 1)
		{
			result = ref->FormatEchoData(posX, posY);
		}
	}

	if (result->Equals(""))
	{
		result = "No Data";
	}

	return result;
}

// mise � jour de la table de transformation en fonction de la taille de la fen�tre
System::Void BaseFlatFrontView::UpdateTransformTable()
{
	TransducerFlatFrontView * ref = (TransducerFlatFrontView *)GetTransducerView();
	if (ref)
	{
		// OTK - 26/10/2009 - maintenant que les hauteurs des deux vues 2D ne sont plus forc�ment identiques
		// (fen�tres non dock�es), on doit prendre le max des deux hauteurs pour assurer une r�solution 
		// id�ale pour les deux visus
		// OTK - 27/10/2009 - la vue 2D de face est redimentionn�e avant la vue de profil : on doit dans ce cas
		// ne pas tenir compte de la vieille taille de la vue de profil
		int maxHeight = 0;
		for each(auto view in ViewManager->SiblingFlatViews(this))
		{
			maxHeight = Math::Max(maxHeight, view->GetFlatPictureBox()->Height);
		}

		int maxWidth = 0;
		for each(auto view in ViewManager->SiblingFlatFrontViews(this))
		{
			maxWidth = Math::Max(maxWidth, view->GetFlatPictureBox()->Width);
		}

		ref->UpdateTransformMap(maxWidth, maxHeight, DisplayParameter::getInstance()->GetStrech());
	}
}

// mise � jour de la table de transformation en fonction de la taille de la fen�tre
System::Void BaseFlatFrontView::UpdateTransformTable(int index)
{
	TransducerFlatFrontView * ref = (TransducerFlatFrontView *)GetTransducerView();

	if (ref)
	{
		// OTK - 26/10/2009 - maintenant que les hauteurs des deux vues 2D ne sont plus forc�ment identiques
		// (fen�tres non dock�es), on doit prendre le max des deux hauteurs pour assurer une r�solution 
		// id�ale pour les deux visus
		//int maxHeight = Math::Max(FlatPictureBox->Height, m_OtherView->GetFlatPictureBox()->Height);
		int maxHeight = 0;
		for each(auto view in ViewManager->SiblingFlatViews(this))
		{
			maxHeight = Math::Max(maxHeight, view->GetFlatPictureBox()->Height);
		}

		int maxWidth = 0;
		for each(auto view in ViewManager->SiblingFlatFrontViews(this))
		{
			maxWidth = Math::Max(maxWidth, view->GetFlatPictureBox()->Width);
		}

		ref->UpdateTransformMap(maxWidth, maxHeight, DisplayParameter::getInstance()->GetStrech());
	}
}

System::Void BaseFlatFrontView::OnZoom()
{
	DisplayParameter * pDisplay = DisplayParameter::getInstance();
	pDisplay->SetZoomActivated(true);

	// zoom suivant l'axe horizontal
	// m�morisation du zoom horizontal courant
	ZoomSection ^zoom = gcnew ZoomSection();
	zoom->horizontalZoom = gcnew RMinRMax();
	zoom->horizontalZoom->RMin = pDisplay->GetMinFrontHorizontalRatio();
	zoom->horizontalZoom->RMax = pDisplay->GetMaxFrontHorizontalRatio();

	// pourcentage de la largeur totale affich�e actuellement
	double currentWidthpercent = pDisplay->GetMaxFrontHorizontalRatio() - pDisplay->GetMinFrontHorizontalRatio();

	// nouveau pourcentage du point de droite de la selection
	double widthPercentMin = ((double)m_DrawingRegion.X - GetHorizontalMargin()) / GetRealDisplayWidth();
	if (widthPercentMin < 0)
	{
		widthPercentMin = 0;
	}

	// nouveau pourcentage du point de gauche de la selection
	double widthPercentMax = ((double)m_DrawingRegion.X - GetHorizontalMargin() + (double)m_DrawingRegion.Width) / GetRealDisplayWidth();
	if (widthPercentMax > 1)
	{
		widthPercentMax = 1;
	}
	DisplayParameter::getInstance()->SetMinMaxFrontHorizontalRatio(
		zoom->horizontalZoom->RMin + widthPercentMin*currentWidthpercent
		, zoom->horizontalZoom->RMin + widthPercentMax*currentWidthpercent);

	// VERTICAL ZOOM
	double currentMinDepth = pDisplay->GetCurrentMinDepth();
	double currentMaxDepth = pDisplay->GetCurrentMaxDepth();
	double currentHeightMeters = (currentMaxDepth - currentMinDepth)*GetVerticalStretchRatio();

	// point superieur
	double heightPercent = (double)(m_DrawingRegion.Y) / (double)FlatPictureBox->Height;
	double newMinDepth = currentMinDepth + heightPercent*currentHeightMeters;
	// On ne permet pas de "d�zoomer en zoomant", provoque des plantage si on d�zoom trop loin
	newMinDepth = std::max(newMinDepth, currentMinDepth);
	pDisplay->SetCurrentMinDepth(newMinDepth);

	// point inf�rieur
	heightPercent = ((double)m_DrawingRegion.Y + (double)m_DrawingRegion.Height) / (double)FlatPictureBox->Height;
	double newMaxDepth = currentMinDepth + heightPercent*currentHeightMeters;
	// On ne permet pas de "d�zoomer en zoomant", provoque des plantage si on d�zoom trop loin
	newMaxDepth = std::min(newMaxDepth, currentMaxDepth);
	pDisplay->SetCurrentMaxDepth(newMaxDepth);
		
	zoom->verticalZoom = gcnew RMinRMax();
	zoom->verticalZoom->RMin = currentMinDepth;
	zoom->verticalZoom->RMax = currentMaxDepth;

	m_ZoomStack.Push(zoom);
	
	AdjustCursorPosition();
}

System::Void BaseFlatFrontView::ZoomOut()
{
	DisplayParameter * pDisplay = DisplayParameter::getInstance();

	// annulation du zoom actif
	if (m_ZoomStack.Count > 1)
	{
		ZoomSection^ prevZoom = (ZoomSection^)m_ZoomStack.Pop();
		pDisplay->SetZoomActivated(true);
		pDisplay->SetCurrentMinDepth(prevZoom->verticalZoom->RMin);
		pDisplay->SetCurrentMaxDepth(prevZoom->verticalZoom->RMax);

		if (prevZoom->horizontalZoom != nullptr)
		{
			pDisplay->SetMinFrontHorizontalRatio(prevZoom->horizontalZoom->RMin);
			pDisplay->SetMaxFrontHorizontalRatio(prevZoom->horizontalZoom->RMax);
		}
	}
	else
	{
		if (m_ZoomStack.Count > 0)
		{
			m_ZoomStack.Pop();
		}
		pDisplay->SetZoomActivated(false);
		pDisplay->SetMinMaxSideHorizontalRatio(0, 1);
		pDisplay->SetMinMaxFrontHorizontalRatio(0, 1);
	}

	AdjustCursorPosition();
}

//mapping d'un point en coord ecran vers coord r�elles
PointF BaseFlatFrontView::MapScreenToRealCoord(PointF screenCoord)
{
	//gestion des coordonn�es verticales dans la classe parente
	PointF result = BaseFlatView::MapScreenToRealCoord(screenCoord);

	//gestion des coordonn�es verticales
	double xRealMin = DisplayParameter::getInstance()->GetMinFrontHorizontalRatio();
	double xRealMax = DisplayParameter::getInstance()->GetMaxFrontHorizontalRatio();

	//dans cette vue, l'axe X repr�sente le	 depointage transversal
	//on renvoie le pourcentage normalis� dans la matrice de d�pointage transversal
	result.X = xRealMin + screenCoord.X * (xRealMax - xRealMin) / (FlatPictureBox->Width);

	return result;
}

//mapping d'un point en coord r�elles vers coord �cran
PointF BaseFlatFrontView::MapRealToScreenCoord(PointF realCoord)
{
	//gestion des coordonn�es verticales dans la classe parente
	PointF result = BaseFlatView::MapRealToScreenCoord(realCoord);

	//gestion des coordonn�es verticales
	double xRealMin = DisplayParameter::getInstance()->GetMinFrontHorizontalRatio();
	double xRealMax = DisplayParameter::getInstance()->GetMaxFrontHorizontalRatio();

	//dans cette vue, l'axe X repr�sente le	 depointage transversal
	//on renvoie le pourcentage normalis� dans la matrice de d�pointage transversal
	result.X = (realCoord.X - xRealMin) * ((double)FlatPictureBox->Width / GetHorizontalStretchRatio()) / (xRealMax - xRealMin) + GetHorizontalMargin();

	return result;
}


System::Void  BaseFlatFrontView::OnRegionSelection()
{
	// notification de l'observeur de selection
}

System::Void MOVIESVIEWCPP::BaseFlatFrontView::AdjustCursorPosition()
{
	TransducerFlatFrontView * ref = (TransducerFlatFrontView *)GetTransducerView();
	if (!ref)
		return;

	auto cursorCenterPos = ((m_DataCursorPercent *(double)ref->GetTotalRange() / 100.) - ref->GetStartHPixel()) * 100. / ref->GetWidth();
	double displayedCursorPercent = ConvertFlatToStretchPercent(cursorCenterPos);

	if (m_viewContainer->SideViewContainer->GetView(0))
	{
		m_NeedRedraw = true;
		TransducerFlatFrontView * ref = (TransducerFlatFrontView *)GetTransducerView();
		if (ref)
		{
			auto cursorCenterPos = ((m_DataCursorPercent *(double)ref->GetTotalRange() / 100.) - ref->GetStartHPixel()) * 100. / ref->GetWidth();
			double displayedCursorPercent = ConvertFlatToStretchPercent(cursorCenterPos);

			double realCursorPercent = 100.*(0.01*ConvertStretchToFlatPercent(displayedCursorPercent)*ref->GetWidth() + ref->GetStartHPixel()) / (double)ref->GetTotalRange();
			m_viewContainer->SideViewContainer->SetActiveSlide(realCursorPercent, GetCurrentIndex());

			UpdateCursor();
		}
	}
}

System::Void MOVIESVIEWCPP::BaseFlatFrontView::OnTransducerSelectedIndexChanged()
{
	if (GetTransducerView())
	{
		buttonSpectralView->Visible = true;
		updateSpectralView();
	}
	else
	{
		buttonSpectralView->Visible = false;
	}

	auto viewCount = m_viewContainer->FrontViewContainer->ViewCount();
	auto currentIndex = GetCurrentIndex();
	// La fen�tre de l'EchoIntegration ne doit pas provoquer de repositionnement du curseur (cas currentIndex >= viewCount)
	if (viewCount > 0 && currentIndex < viewCount) {
		TransducerFlatFrontView * ref = (TransducerFlatFrontView *)GetTransducerView();
		m_DataCursorPercent = ref->GetFanCenter();
		AdjustCursorPosition();
	}
}

PingFan * BaseFlatFrontView::GetCurrentPingFan()
{
	PingFan *pFan = NULL;
	TransducerFlatFrontView * refViewFront = (TransducerFlatFrontView *)GetTransducerView();
	if (refViewFront)
	{
		double activeCursor = refViewFront->GetCursorOffset();
		TimeObjectContainer *pCont = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(refViewFront->GetSounderId());
		if (pCont)
		{
			int Index = pCont->GetObjectCount() - 1 - activeCursor;
			if (Index < 0)
			{
				Index = 0;
			}
			pFan = (PingFan *)pCont->GetDatedObject(Index);
		}
	}
	return pFan;
}

System::Void  BaseFlatFrontView::OnShoalSelection(PointF pt)
{
	if (GetViewSelectionObserver() != nullptr)
	{
		// notification de l'observeur de selection
		//pour la vue de face, le depointage transversal correspond � l'axe X et la hauteur � l'axe Y
		PointF realXDepth = MapScreenToRealCoord(pt);

		//get the ping fan
		M3DKernel *pKernel = M3DKernel::GetInstance();
		pKernel->Lock();
		PingFan * pFan = GetCurrentPingFan();
		pKernel->Unlock();

		unsigned int transducerListIdx = GetCurrentIndex();
		unsigned int transducerSounderIdx = m_viewContainer->FrontViewContainer->GetView(transducerListIdx)->GetTransducerIndex();

		if (pFan != NULL)
		{
			int	ping = pFan->GetPingId();

			if (transducerSounderIdx < pFan->GetMemorySetRef()->GetMemoryStructCount())
			{
				MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(transducerSounderIdx);
				TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transducerSounderIdx);
				Transducer *pTrans = pFan->getSounderRef()->GetTransducer(transducerSounderIdx);
				double m_compensateHeave = 0;
				SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
				if (pSoftChan)
				{
					m_compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
				}
				BaseMathLib::Vector2I memSize = pTransform->getCartesianSize2();

				//convert cartesianX to realX
				double realX = pTransform->cartesianToRealX(realXDepth.X * memSize.x);
				//substract heave to depth to get real Depth
				double realY = realXDepth.Y - m_compensateHeave - pTrans->m_transDepthMeter;

				//convert real Coord to polar Coord
				if (realY >= 0)
				{
					BaseMathLib::Vector2D realCoord(realX, realY);
					BaseMathLib::Vector2D polarCoord = pTransform->realToPolar2(realCoord);
					//arrondi de la coordonn�e polaire x pour obtenir le faisceau exact 
					//(sans compensation de l'angle dans le faisceau)
					polarCoord.x = (int)(polarCoord.x + 0.5);

					if (polarCoord.x >= 0 && polarCoord.y >= 0)
					{
						//notify observer
						GetViewSelectionObserver()->OnSelectionPoint(
							gcnew ViewSelectionPolarPoint(ping, polarCoord.x, polarCoord.y), GetTransducer());
					}
					else
					{
						GetViewSelectionObserver()->OnSelectionError();
					}
				}
				else
				{
					GetViewSelectionObserver()->OnSelectionError();
				}
			}
		}
	}
}

// OTK - FAE039 - dessin de la dimension horizontale del'image
System::Void BaseFlatFrontView::DrawDistanceGrid(Graphics^  e)
{
	TransducerFlatFrontView* ref = (TransducerFlatFrontView*)GetTransducerView();
	if (ref)
	{
		// r�cup�ration de la largeur r�elle de l'image
		double imageSize = ref->GetHorizontalMeters();

		String^ str = String::Format("Width : {0:0.00}m", imageSize);
		SizeF^ size = e->MeasureString(str, m_defaultFont);
		int margin = 2;
		int x = FlatPictureBox->Width - size->Width - margin + 1;
		int y = margin;

		// dessin du rectangle englobant en blanc
		e->FillRectangle(Brushes::White, System::Drawing::Rectangle(x, y, size->Width, size->Height + margin));
		e->DrawString(str, m_defaultFont, Brushes::Red, x + margin, y + margin);
	}
}

// OTK - FAE214 - dessin des single targets
System::Void BaseFlatFrontView::DrawSingleTargets(Graphics^  e)
{
	//get the ping fan
	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	PingFan * pFan = GetCurrentPingFan();
	if (pFan)
	{
		PingFanSingleTarget *pSingle = (PingFanSingleTarget *)pKernel->getObjectMgr()->GetPingFanContainer().GetSingleTargetContainer()->GetObjectWithDate(pFan->m_ObjectTime);
		if (pSingle)
		{
			Transducer * pTrans = pFan->getSounderRef()->GetTransducerWithName(netStr2CppStr(GetTransducer()).c_str());
			if (pTrans)
			{
				unsigned int transducerSounderIdx = m_viewContainer->FrontViewContainer->GetView(GetCurrentIndex())->GetTransducerIndex();
				if (transducerSounderIdx < pFan->GetMemorySetRef()->GetMemoryStructCount())
				{
					auto displayParameters = DisplayParameter::getInstance();
					auto palette = displayParameters->GetCurrent2DEchoPalette();

					TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transducerSounderIdx);
					if (pTransform)
					{
						TSDisplayType tsDisplayType = DisplayParameter::getInstance()->GetTSDisplayType();
						TSAnalysisModule * pTSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
						const std::map<std::uint64_t, TSTrack*> & mapTracks = pTSModule->getTracks();
						for (unsigned int i = 0; i < pSingle->GetNumberOfTarget(); i++)
						{
							SingleTargetDataObject *pSingleTargetData = pSingle->GetTarget(i);
							SplitBeamPair ref;
							bool found = pKernel->getObjectMgr()->GetChannelParentSplitBeam(pSingleTargetData->m_parentSTId, ref);
							if (found)
							{
								SoftChannel * pSoftChan = pTrans->getSoftChannel(pSingleTargetData->m_parentSTId);
								// Il faut que la target soit sur une voie du transducteur de la vue 2D courante
								if (pSoftChan)
								{
									System::Drawing::Brush ^labelBrush = gcnew System::Drawing::SolidBrush(System::Drawing::Color::Black);
									double m_compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
									const std::map<std::uint64_t, TSTrack*> & mapTracks = pTSModule->getTracks();
									for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataCWCount(); numTarget++)
									{
										BaseMathLib::Vector3D mSoftCoord(0, 0, pSingleTargetData->GetSingleTargetDataCW(numTarget).m_targetRange);
										BaseMathLib::Vector3D fanCoords = pSoftChan->GetMatrixSoftChan() * mSoftCoord;
										fanCoords.z += pTrans->m_transDepthMeter + m_compensateHeave;

										if (fanCoords.z >= DisplayParameter::getInstance()->GetCurrentMinDepth()
											&& fanCoords.z <= DisplayParameter::getInstance()->GetCurrentMaxDepth())
										{
											const SingleTargetDataCW & tsData = pSingleTargetData->GetSingleTargetDataCW(numTarget);

											bool bIsTracked = false;
											if (tsData.m_trackLabel > 0)
											{
												std::map<std::uint64_t, TSTrack*>::const_iterator iterTrack = mapTracks.find(tsData.m_trackLabel);
												// On n'affiche que les tracks qui respectent les crit�res
												if (iterTrack != mapTracks.end() && pTSModule->IsValidTrack(iterTrack->second))
												{
													bIsTracked = true;
												}
											}

											if (tsDisplayType == eTSDisplayAll
												|| (tsDisplayType == eTSDisplayTracked && bIsTracked)
												|| (tsDisplayType == eTSDisplayUntracked && !bIsTracked))
											{
												BaseMathLib::Vector2D realSize = pTransform->getRealSize2();
												PointF targetPos;
												// Le y de mSoftCoord correspond � l'axe X de la vue 2D de face
												targetPos.X = (realSize.x / 2.0 + fanCoords.y) / realSize.x;
												targetPos.Y = fanCoords.z;
												PointF screenPos = MapRealToScreenCoord(targetPos);

												if (screenPos.X > 0 && screenPos.Y > 0 && screenPos.X < FlatPictureBox->Width && screenPos.Y < FlatPictureBox->Height)
												{
													unsigned int color = palette->GetColor(tsData.m_compensatedTS * 100);
													System::Drawing::Brush ^brush = gcnew System::Drawing::SolidBrush(System::Drawing::Color::FromArgb(color));

													// Calcul de la taille en pixel d'un �chantillon (taille minimum 5 pixels pour qu'on le voit)
													targetPos.Y += pTrans->getBeamsSamplesSpacing();
													PointF screenPos2 = MapRealToScreenCoord(targetPos);
													float echoSize = std::max<float>(5.0, screenPos2.Y - screenPos.Y);

													// Dessin du cercle
													e->FillEllipse(brush, screenPos.X - echoSize / 2.0, screenPos.Y - echoSize / 2.0, echoSize, echoSize);

													// Dessin du label de la track �ventuelle
													if (bIsTracked)
													{
														// Dessin du label avec un rectangle blanc pour bien voir avec la palette noir et blanc
														String^ tracklabel = Convert::ToString(tsData.m_trackLabel);
														SizeF^ size = e->MeasureString(tracklabel, m_defaultFont);

														e->FillRectangle(Brushes::White, screenPos.X + echoSize / 2.0, screenPos.Y + echoSize / 2.0, size->Width, size->Height);
														e->DrawString(tracklabel, m_defaultFont, labelBrush, screenPos.X + echoSize / 2.0, screenPos.Y + echoSize / 2.0);
													}
												}
											}
										}
									}
									for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataFMCount(); numTarget++)
									{
										BaseMathLib::Vector3D mSoftCoord(0, 0, pSingleTargetData->GetSingleTargetDataFM(numTarget).m_targetRange);
										BaseMathLib::Vector3D fanCoords = pSoftChan->GetMatrixSoftChan() * mSoftCoord;
										fanCoords.z += pTrans->m_transDepthMeter + m_compensateHeave;

										if (fanCoords.z >= DisplayParameter::getInstance()->GetCurrentMinDepth()
											&& fanCoords.z <= DisplayParameter::getInstance()->GetCurrentMaxDepth())
										{
											const SingleTargetDataFM & tsData = pSingleTargetData->GetSingleTargetDataFM(numTarget);

											bool bIsTracked = false;
											if (tsData.m_trackLabel > 0)
											{
												std::map<std::uint64_t, TSTrack*>::const_iterator iterTrack = mapTracks.find(tsData.m_trackLabel);
												// On n'affiche que les tracks qui respectent les crit�res
												if (iterTrack != mapTracks.end() && pTSModule->IsValidTrack(iterTrack->second))
												{
													bIsTracked = true;
												}
											}

											if (tsDisplayType == eTSDisplayAll
												|| (tsDisplayType == eTSDisplayTracked && bIsTracked)
												|| (tsDisplayType == eTSDisplayUntracked && !bIsTracked))
											{
												BaseMathLib::Vector2D realSize = pTransform->getRealSize2();
												PointF targetPos;
												// Le y de mSoftCoord correspond � l'axe X de la vue 2D de face
												targetPos.X = (realSize.x / 2.0 + fanCoords.y) / realSize.x;
												targetPos.Y = fanCoords.z;
												PointF screenPos = MapRealToScreenCoord(targetPos);

												if (screenPos.X > 0 && screenPos.Y > 0 && screenPos.X < FlatPictureBox->Width && screenPos.Y < FlatPictureBox->Height)
												{
													unsigned int color = palette->GetColor(tsData.m_medianTS * 100);
													System::Drawing::Brush ^brush = gcnew System::Drawing::SolidBrush(System::Drawing::Color::FromArgb(color));

													// Calcul de la taille en pixel d'un �chantillon (taille minimum 5 pixels pour qu'on le voit)
													targetPos.Y += pTrans->getBeamsSamplesSpacing();
													PointF screenPos2 = MapRealToScreenCoord(targetPos);
													float echoSize = std::max<float>(5.0, screenPos2.Y - screenPos.Y);

													// Dessin du cercle
													e->FillEllipse(brush, screenPos.X - echoSize / 2.0, screenPos.Y - echoSize / 2.0, echoSize, echoSize);

													// Dessin du label de la track �ventuelle
													if (bIsTracked)
													{
														// Dessin du label avec un rectangle blanc pour bien voir avec la palette noir et blanc
														String^ tracklabel = Convert::ToString(tsData.m_trackLabel);
														SizeF^ size = e->MeasureString(tracklabel, m_defaultFont);

														e->FillRectangle(Brushes::White, screenPos.X + echoSize / 2.0, screenPos.Y + echoSize / 2.0, size->Width, size->Height);
														e->DrawString(tracklabel, m_defaultFont, labelBrush, screenPos.X + echoSize / 2.0, screenPos.Y + echoSize / 2.0);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	pKernel->Unlock();
}

void BaseFlatFrontView::OnModeChanged()
{
	if (m_spectrogramControl)
	{
		m_spectrogramControl->spectrogram->setBoxZoomModeEnabled(m_bZoom);
	}
}

System::Void BaseFlatFrontView::buttonSpectralView_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	updateSpectralView();
}

void BaseFlatFrontView::updateSpectralView()
{
	if (buttonSpectralView->Checked)
	{
		if (m_spectrogramControl == nullptr)
		{
			m_spectrogramControl = gcnew InternalSpectrogramControl();
			m_spectrogramInternalControlIndex = AddInternalControl(m_spectrogramControl);
			m_spectrogramControl->setEnabled(true);
			OnModeChanged();
		}

		showInternalControl(m_spectrogramInternalControlIndex);
		m_spectrogramControl->setEnabled(true);
		m_spectrogramControl->transducerChanged(GetTransducerView()->GetSounderId(), GetTransducerView()->GetTransducerIndex());
	}
	else
	{
		showInternalControl(0);

		if (m_spectrogramControl != nullptr)
		{
			m_spectrogramControl->setEnabled(false);
		}
	}
}

int BaseFlatFrontView::getCurrentPingOffset()
{
	int offset = 0;
	auto refViewFront = (TransducerFlatFrontView *)GetTransducerView();
	if (refViewFront)
	{
		offset = refViewFront->GetCursorOffset();
	}
	return offset;
}

void InternalSpectrogramControl::pingFanAdded()
{
	if (m_sounderId < 0 || m_transducerIndex < 0 || !m_enabled)
		return;

	M3DKernel *pKernel = M3DKernel::GetInstance();
	TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(m_sounderId);
	if (pCont)
	{
		int Index = pCont->GetObjectCount() - 1 - m_cursorOffset;
		if (Index < 0)
		{
			Index = 0;
		}
		PingFan *lastFan = (PingFan *)pCont->GetDatedObject(Index);
		if (lastFan)
		{
			Transducer *pTrans = lastFan->getSounderRef()->GetTransducer(m_transducerIndex);
			auto channelId = pTrans->GetChannelId().front();

			SoftChannel * pSoftChan = pTrans->getSoftChannel(channelId);

			auto data = lastFan->getSpectralAnalysisDataObject(channelId);

			CalibrationModule* calibrationModule = CModuleManager::getInstance()->GetCalibrationModule();
			if (data)
			{
				//get the Sv values with the gains from the calibration file
				auto svValues = calibrationModule->getSvValuesWithGain(data, pSoftChan);
				auto minFreq = data->firstFrequency;
				auto maxFreq = data->lastFrequency;
				double additionalFrequenciesOnAxis = (maxFreq - minFreq) / 20.0;

				double frequencyFactor = 1000.0; //to display the frequencies in kHz 
				double minDepth = DisplayParameter::getInstance()->GetCurrentMinDepth();
				double maxDepth = DisplayParameter::getInstance()->GetCurrentMaxDepth();

				spectrogram->setAxisXDataRange(minFreq / frequencyFactor, data->frequencyInterval / frequencyFactor);
				//spectrogram->setAxisY(data.firstRange, data.lastRange, data.firstRange, data.rangeInterval);
				spectrogram->setAxisYDataRange(data->firstRange, data->rangeInterval);
				spectrogram->setData(svValues, 100.0);

				//double prof = pTrans->m_transDepthMeter;
				double prof = 0;
				double offset = pTrans->GetSampleOffset() * pTrans->getBeamsSamplesSpacing();
				double compensateHeave = 0.0;
				if (pSoftChan)
				{
					compensateHeave = lastFan->getHeaveChan(pSoftChan->m_softChannelId);
				}
				spectrogram->setDepthOffset(prof + compensateHeave + offset);


				spectrogram->setAxisXDisplayRange((minFreq - additionalFrequenciesOnAxis) / frequencyFactor, (maxFreq + additionalFrequenciesOnAxis) / frequencyFactor);
				spectrogram->setAxisYDisplayRange(minDepth, maxDepth);
			}
			else
			{
				spectrogram->clear();
			}
		}
	}
}

void InternalSpectrogramControl::parametersUpdated()
{
	double minDepth = DisplayParameter::getInstance()->GetCurrentMinDepth();
	double maxDepth = DisplayParameter::getInstance()->GetCurrentMaxDepth();
	spectrogram->setAxisYDisplayRange(minDepth, maxDepth);

	pingFanAdded();
}

void InternalSpectrogramControl::transducerChanged(std::uint32_t sounderId, unsigned int transducerIndex)
{
	if ((sounderId != m_sounderId || transducerIndex != m_transducerIndex) && m_enabled)
	{
		m_sounderId = sounderId;
		m_transducerIndex = transducerIndex;
		pingFanAdded();
	}
}

void InternalSpectrogramControl::PingCursorOffsetChanged(int offset)
{
	if (offset != m_cursorOffset)
	{
		m_cursorOffset = offset;
		pingFanAdded();
	}
}

