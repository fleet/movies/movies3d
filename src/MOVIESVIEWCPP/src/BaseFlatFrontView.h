#pragma once

#include "BaseFlatView.h"
#include "DisplayViewContainer.h"
#include "SpectrogramControl.h"

namespace MOVIESVIEWCPP 
{
	public ref class InternalSpectrogramControl : public InternalControl
	{
	public:
		InternalSpectrogramControl() : InternalControl(gcnew SpectrogramControl()) { spectrogram = (SpectrogramControl^)Controls[0]; }
		virtual void pingFanAdded() override;
		virtual void parametersUpdated() override;
		virtual void transducerChanged(std::uint32_t sounderId, unsigned int transducerIndex) override;
		virtual void PingCursorOffsetChanged(int offset) override;

		SpectrogramControl^ spectrogram;

		void setEnabled(bool value) { m_enabled = value; }

	private:
		std::uint32_t m_sounderId;
		unsigned int m_transducerIndex;
		int m_cursorOffset = 0;
		bool m_enabled = false;
	};

	public ref class BaseFlatFrontView : public BaseFlatView
	{
	public:
		BaseFlatFrontView();
		
	public:	   virtual System::Void	DrawNow() override;
	protected: virtual System::Void DrawBottom(Graphics^  e) override;
	protected: virtual System::Void DrawContourLines(Graphics^  e) override;
	protected: virtual System::Void BaseFlatFrontView::DrawRefNoise(Graphics^  e) override;
	protected: virtual System::Void BaseFlatFrontView::DrawNoise(Graphics^  e) override;
	protected: virtual System::Void BaseFlatFrontView::DrawNoisePoints(Graphics^  e, std::vector<BaseMathLib::Vector2I> points, Pen^ pen, Brush^ brush);
	protected: virtual System::Void DrawEchoLine(Graphics^  e, std::vector<BaseMathLib::Vector2I> echoesLine);
	protected: 
		virtual System::Void DrawCursor(Graphics^  e) override;
		virtual System::Void DrawShoalId(Graphics^  e) override;
		virtual System::Void DrawSingleTargets(Graphics^  e) override;
		virtual System::Void DrawDistanceGrid(Graphics^  e) override;
		virtual System::Void DrawImage(Graphics^  e) override;
		virtual TransducerContainerView ^GetFlatView() override { return m_viewContainer->FrontViewContainer;	}
		virtual System::Void AdjustCursorPosition() override; ///< Positionne le curseur relativement � m_DataCursorPercent
		virtual System::Void CursorHasChanged(double value) override;
		virtual System::Void UpdateTransformTable() override;
		virtual System::Void UpdateTransformTable(int index) override;
		virtual System::String^ FormatEchoInformation(int ScreenPosX, int ScreenPosY) override;
		virtual System::Void OnZoom() override;
		virtual System::Void ZoomOut() override;
		virtual System::Void UpdateCursor() override;

				   // selection d'une region
		virtual System::Void  OnRegionSelection() override;
				   // selection d'un point
		virtual System::Void  OnShoalSelection(PointF pt) override;
				   //mapping d'un point en coord ecran vers coord r�elles
		virtual PointF MapScreenToRealCoord(PointF screenCoord) override;
				   //mapping d'un point en coord r�elles vers coord �cran
		virtual PointF MapRealToScreenCoord(PointF realCoord) override;
				   // Remet le curseur � 50%
		virtual System::Void OnTransducerSelectedIndexChanged() override;
		PingFan * GetCurrentPingFan();

		void OnModeChanged();

		virtual System::Void buttonSpectralView_CheckedChanged(System::Object^  sender, System::EventArgs^  e) override;

	public: int getCurrentPingOffset() override;

	private:

		// Position du curseur relative aux donn�es
		double m_DataCursorPercent;
		InternalSpectrogramControl^ m_spectrogramControl = nullptr;
		int m_spectrogramInternalControlIndex = -1;

		void updateSpectralView();
	};
}
