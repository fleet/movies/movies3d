﻿#include "displayparameter.h"
#include "BaseFlatSideView.h"
#include "TransducerContainerFlatView.h"
#include "GdiWrapper.h"
#include "utils.h"
#include "MoviesException.h"
#include <math.h>

#include "DisplayView.h"
#include "IViewSelectionObserver.h"
#include "colorlist.h"
#include "ColorPaletteEIClass.h"

#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/utils/multithread/KernelLocker.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "ModuleManager/ModuleManager.h"
#include "M3DKernel/datascheme/TransformSet.h"

#include "EISupervised/EISupervisedModule.h"
#include "EISupervised/ClassificationResult.h"

#include "BaseMathLib/geometry/GeometryTools2D.h"

#include "ReaderCtrlManaged.h"

#include "BottomDetection/BottomDetectionContourLine.h"
#include "BottomDetection/BottomDetectionModule.h"
#include "BottomDetection/ContourLineDef.h"

#include "EISupervisedSelection.h"
#include "TransducerImageFlatView.h"
#include "ColorPaletteEcho.h"

#include "EchoIntegration/EchoIntegrationModule.h"

#include "TSAnalysis/TSAnalysisModule.h"
#include "TSAnalysis/TSTrack.h"

using namespace MOVIESVIEWCPP;

namespace
{
	constexpr const char * LoggerName = "MoviesView.View.2DViews.BaseFlatSideView";
}

BaseFlatSideView::BaseFlatSideView()
	:BaseFlatView()
{
	mReaderCount = 0;
	mMaxDelay = 0;
	mShowDelayInfo = false;
	buttonHistoric->Visible = true;
	buttonHistoric->CheckedChanged += gcnew System::EventHandler(this, &BaseFlatSideView::buttonHistoric_CheckedChanged);
	buttonSpectralView->Visible = false;

	m_bBottomCorrectionMode = false;
	m_bEISupervisedSelectionMode = false;
	m_bPolygonSelectionMode = false;

	m_eiSelectionManager = gcnew EISelectionManager();

	m_SelectionContextMenuStrip = gcnew System::Windows::Forms::ContextMenuStrip();
	m_SelectionContextMenuStrip->ItemClicked += gcnew System::Windows::Forms::ToolStripItemClickedEventHandler(this, &BaseFlatSideView::m_SelectionContextMenuStrip_ItemClicked);

	TransducerChanged += gcnew BaseFlatView::TransducerChangedHandler(this, &BaseFlatSideView::OnTransducerChanged);

	m_EISupervisedMenuStrip = gcnew EISupervisedToolStripMenu();
	m_EISupervisedMenuStrip->DeletePolygonClicked += gcnew EISupervisedToolStripMenu::DeletePolygonHandler(this, &BaseFlatSideView::m_EISupervisedMenuStrip_DeletePolygonClicked);
	m_EISupervisedMenuStrip->AddEnergyClicked += gcnew EISupervisedToolStripMenu::OperationEnergyHandler(this, &BaseFlatSideView::m_EISupervisedMenuStrip_AddEnergyClicked);
	m_EISupervisedMenuStrip->SubstractEnergyClicked += gcnew EISupervisedToolStripMenu::OperationEnergyHandler(this, &BaseFlatSideView::m_EISupervisedMenuStrip_SubstractEnergyClicked);
	m_EISupervisedMenuStrip->ReclassifyEnergyClicked += gcnew EISupervisedToolStripMenu::OperationEnergyHandler(this, &BaseFlatSideView::m_EISupervisedMenuStrip_ReclasssifyClicked);

	m_ViewDesc = "MOVIES 3D : 2D Side View";

	historicScrollHandler = gcnew System::Windows::Forms::ScrollEventHandler(this, &BaseFlatSideView::HistoricHScrollBar_Scroll);
	this->HistoricHScrollBar->Scroll += historicScrollHandler;
	m_OffsetHistoric = 0;
	m_OffsetHistoricReference = 0;
	// OTK - FAE058 - barre de défilement activée par défaut
	buttonHistoric->Checked = true;

	// Pour le mode de correction manuel du fond
	m_BottomCorrectionMenuStrip = gcnew System::Windows::Forms::ContextMenuStrip();
	System::Windows::Forms::ToolStripMenuItem ^ applyBottomCorrectionItem = gcnew System::Windows::Forms::ToolStripMenuItem("Apply bottom correction");
	applyBottomCorrectionItem->Click += gcnew System::EventHandler(this, &BaseFlatSideView::OnBottomCorrectionApply);
	m_BottomCorrectionMenuStrip->Items->Add(applyBottomCorrectionItem);
	System::Windows::Forms::ToolStripMenuItem ^ cancelBottomCorrectionItem = gcnew System::Windows::Forms::ToolStripMenuItem("Cancel");
	cancelBottomCorrectionItem->Click += gcnew System::EventHandler(this, &BaseFlatSideView::OnBottomCorrectionCancel);
	m_BottomCorrectionMenuStrip->Items->Add(cancelBottomCorrectionItem);
	m_bIgnoreNextMouseUp = false;

	// Pour la suppression des échos d'un polygone
	m_PolygonDeleteMenuStrip = gcnew System::Windows::Forms::ContextMenuStrip();
	System::Windows::Forms::ToolStripMenuItem ^ applyPolygonDeleteItem = gcnew System::Windows::Forms::ToolStripMenuItem("Delete polygon contents");
	applyPolygonDeleteItem->Click += gcnew System::EventHandler(this, &BaseFlatSideView::OnPolygonDeleteApply);
	m_PolygonDeleteMenuStrip->Items->Add(applyPolygonDeleteItem);
	System::Windows::Forms::ToolStripMenuItem ^ cancelPolygonDeleteItem = gcnew System::Windows::Forms::ToolStripMenuItem("Cancel");
	cancelPolygonDeleteItem->Click += gcnew System::EventHandler(this, &BaseFlatSideView::OnPolygonDeleteCancel);
	m_PolygonDeleteMenuStrip->Items->Add(cancelPolygonDeleteItem);
	m_polygonToDelete = NULL;


	// INITIALISATION DES PINCEAUX
	m_3DAreaPen = gcnew Pen(Color::Green, 2.0f);
	m_3DAreaPen->DashStyle = System::Drawing::Drawing2D::DashStyle::Dash;

	m_gridPen = gcnew Pen(Color::Black, 1.0f);
	
	m_deviationPen = gcnew Pen(Color::Red, 1.0f);

	m_esuPen = gcnew Pen(Color::Red, 4.0f);
	m_esuPen->DashStyle = Drawing2D::DashStyle::Dash;

	m_surfaceLayerPen = gcnew Pen(Color::LightGreen, 1.0f);
	m_bottomLayerPen = gcnew Pen(Color::Purple, 1.0f);
	m_distanceLayerPen = gcnew Pen(Color::Orange, 1.0f);
	m_polygonPen = gcnew Pen(Color::Black, 1.0f);

	m_selectionWindowPen = gcnew Pen(Color::BlueViolet, 1.0f);
	m_selectionWindowPen->DashStyle = Drawing2D::DashStyle::Dash;

	m_clearBrush = gcnew SolidBrush(Color::White);
	m_blackBrush = gcnew SolidBrush(Color::Black);

	m_distanceGridBrush = SystemBrushes::ControlText;

	// INITIALISATION DES FONTS
	m_fontForSelectedShoal = gcnew System::Drawing::Font(m_defaultFont->FontFamily, 10, System::Drawing::FontStyle::Bold);
	m_fontForShoal = gcnew System::Drawing::Font(m_defaultFont->FontFamily, 10);
}

BaseFlatSideView::~BaseFlatSideView()
{
	if (m_polygonToDelete)
	{
		delete m_polygonToDelete;
		m_polygonToDelete = NULL;
	}
}

System::Void BaseFlatSideView::buttonHistoric_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
	// le suspendLayout ne fonctionne pas correctement...
	GDIWrapper::BeginUpdate(this->Handle);
	if (buttonHistoric->Checked)
	{
		m_OffsetHistoric = m_OffsetHistoricReference;
		if ((HistoricHScrollBar->Maximum - HistoricHScrollBar->LargeChange) - m_OffsetHistoric < HistoricHScrollBar->Minimum)
		{
			m_OffsetHistoric = (HistoricHScrollBar->Maximum - HistoricHScrollBar->LargeChange) - HistoricHScrollBar->Minimum;
		}
		if ((HistoricHScrollBar->Maximum - HistoricHScrollBar->LargeChange) - m_OffsetHistoric > HistoricHScrollBar->Maximum)
		{
			m_OffsetHistoric = (HistoricHScrollBar->Maximum - HistoricHScrollBar->LargeChange) - HistoricHScrollBar->Maximum;
		}
		this->HistoricHScrollBar->Visible = true;
	}
	else
	{
		this->HistoricHScrollBar->Visible = false;
		m_OffsetHistoric = 0;
	}
	GDIWrapper::EndUpdate(this->Handle);
	this->Refresh();
}

void BaseFlatSideView::UpdatePensAndBrushes()
{
	BaseFlatView::UpdatePensAndBrushes();
	auto displayParameter = DisplayParameter::getInstance();
	displayParameter->Lock();

	auto esuPen = displayParameter->GetEsuGridPen();

	m_esuPen->Width = esuPen.width;
	m_esuPen->Color = Color::FromArgb(esuPen.color);

	displayParameter->Unlock();
}

System::Void BaseFlatSideView::DrawShoalId(Graphics^  e)
{
	System::Drawing::Font^ font = nullptr;

	TransducerFlatView * pSideView = (TransducerFlatView *)GetTransducerView();

	if (pSideView != NULL)
	{
		std::vector<std::uint64_t> pingIds;
		std::vector<unsigned int> xVect;

		// on récupère la profondeur min et max
		double minDepth = DisplayParameter::getInstance()->GetCurrentMinDepth();
		double maxDepth = DisplayParameter::getInstance()->GetCurrentMaxDepth();
		double height = GetRealDisplayHeight();
		int nbPings = pSideView->GetWidth();

		std::map<std::uint64_t, ShoalAreaInfo> & mapShoalInfo = pSideView->GetShoalAreaInfo();
		std::map<std::uint64_t, ShoalAreaInfo>::iterator iterShoalInfo = mapShoalInfo.begin();
		while (iterShoalInfo != mapShoalInfo.end())
		{
			int y1 = (int)(height* (iterShoalInfo->second.depthMin - minDepth) / (maxDepth - minDepth));
			int y2 = (int)(height* (iterShoalInfo->second.depthMax - minDepth) / (maxDepth - minDepth));

			pingIds.clear();
			pingIds.push_back(iterShoalInfo->second.pingMin);
			pingIds.push_back(iterShoalInfo->second.pingMax + 3);
			pSideView->GetScreenPixelsFromPingIDs(pingIds, xVect);

			if (xVect.size() > 1)
			{
				int x1, x2;
				if (Zoomed)
				{
					x1 = (int)floor(FlatPictureBox->ClientSize.Width*((double)xVect[0] / (double)nbPings));
					x2 = (int)ceil(FlatPictureBox->ClientSize.Width*((double)xVect[1] / (double)nbPings));
				}
				else
				{
					unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
					unsigned int realNbPings = pSideView->GetLastAddedIndex() + 1;
					if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
					{
						// image plaquée à gauche, on affiche tous les pings   
						x1 = xVect[0] * nbPixelsPerPing;
						x2 = xVect[1] * nbPixelsPerPing;
					}
					else
					{
						// image plaquée à droite
						x1 = (m_OffsetHistoric + xVect[0] - (realNbPings - FlatPictureBox->Width / nbPixelsPerPing))*nbPixelsPerPing;
						x2 = (m_OffsetHistoric + xVect[1] - (realNbPings - FlatPictureBox->Width / nbPixelsPerPing))*nbPixelsPerPing;
					}
				}

				if (x2 > 0)
				{
					std::uint64_t shoalId = iterShoalInfo->first;

					System::Drawing::SolidBrush^ brush = gcnew System::Drawing::SolidBrush(
						ColorList::GetInstance()->GetColor(shoalId));

					font = ((iterShoalInfo->second.selected)) ? m_fontForSelectedShoal : m_fontForShoal;
					e->DrawString(Convert::ToString(shoalId), font, brush, x2, y1);
				}
			}
			iterShoalInfo++;
		}
	}
}

System::Void BaseFlatSideView::DrawCursor(Graphics^  e)
{
	bool show3dCursor = true;
	// Create coordinates of points that define line.
	TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
	if (ref)
	{
		int cursorPos = ref->GetLastAddedIndex() - m_OffsetCursor;
		int imageSize = m_bmp->Width();
		if (Zoomed)
		{
			mCursorCenterXpercent = (double)(cursorPos - (int)ref->GetStartHPixel()) / (double)ref->GetWidth();
		}
		else
		{
			unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
			unsigned int realNbPings = ref->GetLastAddedIndex() + 1;
			if (FlatPictureBox->Width >= nbPixelsPerPing*realNbPings)
			{
				// image plaquée à gauche, on affiche tous les pings  
				mCursorCenterXpercent = (double)(cursorPos*nbPixelsPerPing) / (double)FlatPictureBox->ClientSize.Width;
			}
			else
			{
				// image plaquée à droite
				mCursorCenterXpercent = 1.0 - (double)((m_OffsetCursor - m_OffsetHistoric)*nbPixelsPerPing) / (double)FlatPictureBox->ClientSize.Width;
			}
		}
		mCursorCenterXpercent *= 100.0;

		if (mCursorCenterXpercent >= 0 && mCursorCenterXpercent <= 100)
		{
			int x = (int)floor(0.5 + FlatPictureBox->ClientSize.Width*this->mCursorCenterXpercent / 100.0);
			int y1 = 0;
			int y2 = FlatPictureBox->ClientSize.Height;
			e->DrawLine(m_cursorPen, x, y1, x, y2);

			// Now Draw 3d area
			if (show3dCursor)
			{
				int x1, x2;
				int y1 = 0;
				int y2 = FlatPictureBox->ClientSize.Height;
				if (Zoomed)
				{
					x1 = (int)floor(0.5 + FlatPictureBox->Width*ref->GetFirstPoint3DAdded());
					x2 = (int)floor(0.5 + FlatPictureBox->Width*ref->GetLastPoint3DAdded());
				}
				else
				{
					unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
					unsigned int realNbPings = ref->GetLastAddedIndex() + 1;
					if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
					{
						// image plaquée à gauche, on affiche tous les pings   
						double windowPercent = (double)m_bmp->Width() / ((double)FlatPictureBox->Width / (double)nbPixelsPerPing);
						x1 = (int)floor(0.5 + FlatPictureBox->Width*windowPercent*ref->GetFirstPoint3DAdded());
						x2 = (int)floor(0.5 + FlatPictureBox->Width*windowPercent*ref->GetLastPoint3DAdded());
					}
					else
					{
						// image plaquée à droite
						unsigned int nbDisplayedPing = FlatPictureBox->Width / nbPixelsPerPing;
						x1 = (int)floor(0.5 + (ref->GetFirstPoint3DAdded()*m_bmp->Width() - realNbPings + nbDisplayedPing + m_OffsetHistoric)*nbPixelsPerPing);
						x2 = (int)floor(0.5 + (ref->GetLastPoint3DAdded()*m_bmp->Width() - realNbPings + nbDisplayedPing + 1 + m_OffsetHistoric)*nbPixelsPerPing);
					}
					x2 += nbPixelsPerPing;
				}

				e->DrawRectangle(m_3DAreaPen, x1, y1, x2 - x1, y2 - 1);
			}
		}
	}
}

// dessin de la grille des distances
System::Void BaseFlatSideView::DrawDistanceGrid(Graphics^  e)
{
	if (DisplayParameter::getInstance()->GetDistanceGrid())
	{
		TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
		if (ref)
		{
			// on récupére les indices des pings affichés pour lesquels on doit tracer une ligne
			std::vector<DistanceGridLine> distanceIndexes = ref->GetDistanceIndexes();
			int nbLines = distanceIndexes.size();
			int nbPings = ref->GetWidth();

			int y1 = 0;
			int y2 = FlatPictureBox->ClientSize.Height;

			int lastX;

			// on laisse une marge entre le bas de la picturebox et le texte
			int bottomMargin = 3;
			// et entre la ligne et son label
			int leftmargin = 3;

			for (int i = 0; i < nbLines; i++)
			{
				int x;
				if (Zoomed)
				{
					x = (int)floor(0.5 + (double)FlatPictureBox->ClientSize.Width*((double)distanceIndexes[i].PingIndex / (double)nbPings));
				}
				else
				{
					unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
					unsigned int realNbPings = ref->GetLastAddedIndex() + 1;
					if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
					{
						// image plaquée à gauche, on affiche tous les pings   
						x = distanceIndexes[i].PingIndex*nbPixelsPerPing;
					}
					else
					{
						// image plaquée à droite
						x = (m_OffsetHistoric + distanceIndexes[i].PingIndex - (realNbPings - FlatPictureBox->Width / nbPixelsPerPing))*nbPixelsPerPing;
					}
				}

				// tracé d'une ligne
				GDIWrapper::DrawXORLine(e, m_gridPen, x, y1, x, y2);

				// Dessin du label de distance
				// on détermine le rectangle englobant le texte
				String^ str = Convert::ToString(distanceIndexes[i].MilesFraction / 1000.) + " nm";
				SizeF^ size = e->MeasureString(str, m_defaultFont);

				// si on n'a pas la place, on ne dessine pas le texte
				if (size->Height + bottomMargin < y2)
				{
					System::Drawing::Font^ font = m_defaultFont;
					// si on est trop pres de la ligne suivante, on rétréci la taille
					if (i > 0)
					{
						int nbPixelsInterLines = lastX - x;

						if (size->Width + leftmargin + 1 > nbPixelsInterLines)
						{
							double ratio = (double)(size->Height) / (double)(size->Width);
							size->Width = nbPixelsInterLines - 1 - leftmargin;
							int oldHeight = size->Height;
							size->Height = size->Width*ratio;
							double shrinkFactor = (double)size->Height / (double)oldHeight;
							font = gcnew System::Drawing::Font(m_defaultFont->FontFamily, std::max(1.0, m_defaultFont->SizeInPoints*shrinkFactor));
						}
					}

					e->FillRectangle(Brushes::White, System::Drawing::Rectangle(x + leftmargin, y2 - size->Height - bottomMargin, size->Width, size->Height));
					e->DrawString(str, font, m_distanceGridBrush, x + leftmargin, y2 - size->Height - bottomMargin);
				}

				lastX = x;
			}
		}
	}
}

// dessin de la grille des ESUs
System::Void BaseFlatSideView::DrawESUGrid(Graphics^  e)
{
	// seulement si un traitement utilisant les ESU est en cours
	M3DKernel * pKern = M3DKernel::GetInstance();
	pKern->Lock();

	TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
	if (ref)
	{
		std::vector<std::uint64_t> pingIds;
		size_t ESUNumber = pKern->getMovESUManager()->GetESUContainer().GetESUNb();
		// pour chaque ESU, on dessine les limites
		for (size_t i = 0; i < ESUNumber; i++)
		{
			ESUParameter * esu = pKern->getMovESUManager()->GetESUContainer().GetESUWithIdx(i);
			if (esu->GetUsed())
			{
				pingIds.push_back(esu->GetESUPingNumber());
				pingIds.push_back(esu->GetESULastPingNumber());
			}
		}

		// on fait de même pour l'ESU en cours le cas échéant
		ESUParameter * currentESU = pKern->getMovESUManager()->GetWorkingESU();
		if (currentESU && currentESU->GetUsed())
		{
			pingIds.push_back(currentESU->GetESUPingNumber());
		}

		// on récupère les indices des pings a tracer sur l'image
		if (pingIds.size())
		{
			std::vector<unsigned int> pingsToDraw;
			ref->GetScreenPixelsFromPingIDs(pingIds, pingsToDraw);

			// on effectue le tracé
			int y1 = 0;
			int y2 = FlatPictureBox->ClientSize.Height;
			int nbPings = ref->GetWidth();
			for (size_t i = 0; i < pingsToDraw.size(); i++)
			{
				int x;
				if (Zoomed)
				{
					x = (int)floor(FlatPictureBox->ClientSize.Width*((double)pingsToDraw[i] / (double)nbPings));
				}
				else
				{
					unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
					unsigned int realNbPings = ref->GetLastAddedIndex() + 1;
					if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
					{
						// image plaquée à gauche, on affiche tous les pings   
						x = pingsToDraw[i] * nbPixelsPerPing;
					}
					else
					{
						// image plaquée à droite
						x = (m_OffsetHistoric + pingsToDraw[i] - (realNbPings - FlatPictureBox->Width / nbPixelsPerPing))*nbPixelsPerPing;
					}
				}
				e->DrawLine(m_esuPen, x, y1, x, y2);
			}
		}
	}
	pKern->Unlock();
}

// dessin des couches de surface et de fond
System::Void BaseFlatSideView::DrawLayersContour(Graphics^  e)
{
	M3DKernel * pKern = M3DKernel::GetInstance();
	pKern->Lock();

	TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
	if (ref)
	{
		std::uint64_t minVisiblePing = GetPingIdFromCursorPos(0.0f, true);
		std::uint64_t maxVisiblePing = GetPingIdFromCursorPos(100.0f, true);

		size_t ESUNumber = pKern->getMovESUManager()->GetESUContainer().GetESUNb();
		// pour chaque ESU, on dessine les limites si l'EI par couches était activée durant l'ESU
		for (size_t i = 0; i < ESUNumber; i++)
		{
			ESUParameter * esu = pKern->getMovESUManager()->GetESUContainer().GetESUWithIdx(i);
			if (esu->GetUsedBySlice())
			{
				std::uint64_t pingNumber = esu->GetESUPingNumber();
				std::uint64_t lastPingNumber = esu->GetESULastPingNumber();

				// verification de l'intervalle de l'ESU avant de la dessiner
				if (pingNumber < maxVisiblePing && lastPingNumber > minVisiblePing)
				{
					if (pingNumber < minVisiblePing)
						pingNumber = minVisiblePing;

					if (lastPingNumber > maxVisiblePing)
						lastPingNumber = maxVisiblePing;

					DrawLayersContour(e, ref, esu->GetESUId(), pingNumber, lastPingNumber);
				}
			}
		}

		// on fait de même pour l'ESU en cours le cas échéant
		ESUParameter * currentESU = pKern->getMovESUManager()->GetWorkingESU();
		if (currentESU && currentESU->GetUsedBySlice())
		{
			std::uint64_t pingNumber = currentESU->GetESUPingNumber();
			std::uint64_t lastPingNumber = pKern->getMovESUManager()->GetRefCurrentPosition().GetESUPingNumber();

			// verification de l'intervalle de l'ESU avant de la dessiner
			if (pingNumber < maxVisiblePing && lastPingNumber > minVisiblePing)
			{
				if (pingNumber < minVisiblePing)
					pingNumber = minVisiblePing;

				if (lastPingNumber > maxVisiblePing)
					lastPingNumber = maxVisiblePing;

				DrawLayersContour(e, ref, -1, pingNumber, lastPingNumber);
			}
		}
	}
	pKern->Unlock();
}


Pen ^ ClassificationPen(int classId)
{	
	Pen ^pen;
	if (classId != -1)
	{
		CModuleManager * pModuleMgr = CModuleManager::getInstance();
		EISupervisedModule * pEISupervisedModule = pModuleMgr->GetEISupervisedModule();
		const EISupervisedParameter & eiSupervisedParameter = pEISupervisedModule->GetEISupervisedParameter();

		ColorPaletteEIClass colorPaletteEIClass = DisplayParameter::getInstance()->GetColorPaletteEIClass();

		//recherche de l'index de la classe
		int iClassIdx = 0;
		for(const auto& classDef : eiSupervisedParameter.getClassifications())
		{
			if (classDef.m_Id == classId)
			{
				pen = gcnew Pen(Color::FromArgb(colorPaletteEIClass.getARGBColorClassification(iClassIdx)));
				break;
			}
			++iClassIdx;
		}
	}
	return  pen;
}

System::Void BaseFlatSideView::DrawSurfaceLayer(Graphics^ e
	, int x1Esu, int x2Esu
	, const double & layerMinDepth, const double & layerMaxDepth
	, Pen ^ layerPen
	, bool drawSelection, Pen^ selectionPen)
{
	double minDepth = DisplayParameter::getInstance()->GetCurrentMinDepth();
	double maxDepth = DisplayParameter::getInstance()->GetCurrentMaxDepth();
	double height = GetRealDisplayHeight();

	int y1 = (int)(height*(layerMinDepth - minDepth) / (maxDepth - minDepth));
	int y2 = (int)(height*(layerMaxDepth - minDepth) / (maxDepth - minDepth));

	e->DrawLine(layerPen, x1Esu, y1, x2Esu, y1);
	e->DrawLine(layerPen, x1Esu, y2, x2Esu, y2);

	// affichage de la zone en hachure
	if (drawSelection)
	{
		int x = x1Esu;
		if (x % 2 != 0) x += 1;
		for (; x < x2Esu; x += 2)
		{
			e->DrawLine(selectionPen, x, y1, x, y2);
		}
	}
}

System::Void BaseFlatSideView::DrawOffsetLayer(Graphics^ e
	, bool isBottomLayer
	, int x1Esu, int x2Esu
	, const double & layerMinDepth, const double & layerMaxDepth
	, Pen ^ layerPen
	, bool drawSelection, Pen^ selectionPen)
{
	double minDepth = DisplayParameter::getInstance()->GetCurrentMinDepth();
	double maxDepth = DisplayParameter::getInstance()->GetCurrentMaxDepth();
	double height = GetRealDisplayHeight();
	double realDisplayHeight = GetRealDisplayHeight();

	TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
	int nbColumns = ref->GetWidth();
	int nbRows = ref->GetHeight();
	int width = FlatPictureBox->Width;
	int startDepth = (int)ref->GetStartPixel();

	auto * offsetValues = isBottomLayer
		? ref->GetBottomValues()
		: ref->GetSurfaceValues();

	int lineWidth = floor(0.5 + realDisplayHeight / (double)nbRows);

	if (Zoomed)
	{
		for (int i = 0; i < nbColumns; i++)
		{
			int offset = offsetValues[i];
			if (offset != -1)
			{
				int x1 = (int)floor(0.5 + (double)(i*width) / (double)nbColumns);
				int x2 = (int)floor(0.5 + (double)((i + 1)*width) / (double)nbColumns);

				if (x1 < x2Esu && x2 > x1Esu)
				{
					if (x1 < x1Esu) x1 = x1Esu;
					if (x2 > x2Esu) x2 = x2Esu;

					int y = (int)floor(0.5 + (double)((1 + offset - startDepth)*realDisplayHeight) / (double)nbRows - (double)lineWidth / 2.0);
					int y1 = y + (int)(realDisplayHeight*layerMinDepth / (maxDepth - minDepth));
					int y2 = y + (int)(realDisplayHeight*layerMaxDepth / (maxDepth - minDepth));

					e->DrawLine(layerPen, x1, y1, x2, y1);
					e->DrawLine(layerPen, x1, y2, x2, y2);

					// dessin des hachures
					if (drawSelection)
					{
						int x = x1;
						if (x % 2 != 0) x += 1;
						for (; x < x2; x += 2)
						{
							e->DrawLine(selectionPen, x, y1, x, y2);
						}
					}
				}
			}
		}
	}
	else
	{
		unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
		unsigned int realNbPings = ref->GetLastAddedIndex() + 1;
		if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
		{
			// image plaquée à gauche, on affiche tous les pings 
			for (int i = 0; i < nbColumns; i++)
			{
				int offset = offsetValues[i];
				if (offset != -1)
				{
					int x1 = i*nbPixelsPerPing;
					int x2 = x1 + nbPixelsPerPing;

					if (x1 < x2Esu && x2 > x1Esu)
					{
						if (x1 < x1Esu) x1 = x1Esu;
						if (x2 > x2Esu) x2 = x2Esu;

						int y = (int)floor(0.5 + (double)((1 + offset - startDepth)*realDisplayHeight) / (double)nbRows - (double)lineWidth / 2.0);
						int y1 = y + (int)(realDisplayHeight*layerMinDepth / (maxDepth - minDepth));
						int y2 = y + (int)(realDisplayHeight*layerMaxDepth / (maxDepth - minDepth));

						e->DrawLine(layerPen, x1, y1, x2, y1);
						e->DrawLine(layerPen, x1, y2, x2, y2);

						// dessin des hachures
						if (drawSelection)
						{
							int x = x1;
							if (x % 2 != 0) x += 1;
							for (; x < x2; x += 2)
							{
								e->DrawLine(selectionPen, x, y1, x, y2);
							}
						}
					}
				}
			}
		}
		else
		{
			// image plaquée à droite
			int nbPingsToDraw = width / nbPixelsPerPing;
			for (int i = 0; i < nbPingsToDraw; i++)
			{
				int botIndex = realNbPings - 1 - i - m_OffsetHistoric;
				if (botIndex >= 0 && botIndex < nbColumns)
				{
					int offset = offsetValues[realNbPings - 1 - i - m_OffsetHistoric];
					if (offset != -1)
					{
						int x1 = width - i*nbPixelsPerPing;
						int x2 = x1 + nbPixelsPerPing;

						if (x1 < x2Esu && x2 > x1Esu)
						{
							if (x1 < x1Esu) x1 = x1Esu;
							if (x2 > x2Esu) x2 = x2Esu;

							int y = (int)floor(0.5 + (double)((1 + offset - startDepth)*realDisplayHeight) / (double)nbRows - (double)lineWidth / 2.0);
							int y1 = y + (int)(realDisplayHeight*layerMinDepth / (maxDepth - minDepth));
							int y2 = y + (int)(realDisplayHeight*layerMaxDepth / (maxDepth - minDepth));

							e->DrawLine(layerPen, x1, y1, x2, y1);
							e->DrawLine(layerPen, x1, y2, x2, y2);

							// dessin des hachures
							if (drawSelection)
							{
								int x = x1;
								if (x % 2 != 0) x += 1;
								for (; x < x2; x += 2)
								{
									e->DrawLine(selectionPen, x, y1, x, y2);
								}
							}
						}
					}
				}
			}
		}
	}
}

// dessin du contour des couches de surface et de fond
System::Void  BaseFlatSideView::DrawLayersContour(Graphics^  e,
	TransducerFlatView* ref, std::uint32_t currentEsuId,
	std::uint64_t pingStart, std::uint64_t pingEnd)
{
	// récupération des coordonnées image des pings délimitant l'ESU
	std::vector<unsigned int> pingsToDraw;
	std::vector<std::uint64_t> pingsIDsToDraw;
	pingsIDsToDraw.push_back(pingStart);
	pingsIDsToDraw.push_back(pingEnd);
	ref->GetScreenPixelsFromPingIDs(pingsIDsToDraw, pingsToDraw);
	if (pingsToDraw.size() <= 1)
		return;

	// on récupère la profondeur min et max
	double minDepth = DisplayParameter::getInstance()->GetCurrentMinDepth();
	double maxDepth = DisplayParameter::getInstance()->GetCurrentMaxDepth();
	double height = GetRealDisplayHeight();

	int x1Esu, x2Esu;
	int nbPings = ref->GetWidth();
	if (Zoomed)
	{
		x1Esu = (int)floor(FlatPictureBox->ClientSize.Width*((double)pingsToDraw[0] / (double)nbPings));
		x2Esu = (int)floor(FlatPictureBox->ClientSize.Width*((double)pingsToDraw[1] / (double)nbPings));
	}
	else
	{
		unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
		unsigned int realNbPings = ref->GetLastAddedIndex() + 1;
		if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
		{
			// image plaquée à gauche, on affiche tous les pings   
			x1Esu = pingsToDraw[0] * nbPixelsPerPing;
			x2Esu = pingsToDraw[1] * nbPixelsPerPing;
		}
		else
		{
			// image plaquée à droite
			x1Esu = (m_OffsetHistoric + pingsToDraw[0] - (realNbPings - FlatPictureBox->Width / nbPixelsPerPing))*nbPixelsPerPing;
			x2Esu = (m_OffsetHistoric + pingsToDraw[1] - (realNbPings - FlatPictureBox->Width / nbPixelsPerPing))*nbPixelsPerPing;
		}
	}

	// récupération des paramètres d'EI pour avoir la définition des couches
	EchoIntegrationParameter& params = CModuleManager::getInstance()->GetEchoIntegrationModule()->GetEchoIntegrationParameter();

	EISupervisedModule * pEISupervisedModule = CModuleManager::getInstance()->GetEISupervisedModule();
	std::string transName = netStr2CppStr(GetTransducer());
	EISupervisedResult * pEISupervisedEsuResult = pEISupervisedModule->GetCentralResultFromEsu(transName, currentEsuId);
	
	bool isSurfaceLayerEnable = pEISupervisedModule->IsEISupervisedEnabled(transName, Layer::Type::SurfaceLayer);
	bool isBottomLayerEnable = pEISupervisedModule->IsEISupervisedEnabled(transName, Layer::Type::BottomLayer);
	bool isDistanceLayerEnable = pEISupervisedModule->IsEISupervisedEnabled(transName, Layer::Type::DistanceLayer);

	const auto& layers = params.getLayersDefs();
	const auto nbLayers = layers.size();

	for (size_t iLayer = 0; iLayer < nbLayers; ++iLayer)
	{
		const auto & layer = layers[iLayer];

		bool drawSelection = false;

		Pen^ layerPen = nullptr;
		switch (layer->GetLayerType())
		{
		case Layer::Type::SurfaceLayer:
			layerPen = m_surfaceLayerPen;
			break;
		case Layer::Type::BottomLayer:
			layerPen = m_bottomLayerPen;
			break;
		case Layer::Type::DistanceLayer:
			layerPen = m_distanceLayerPen;
			break;
		}

		Pen^ selectionPen = nullptr;
		if (mShowEchoTypes)
		{
			if (m_eiSelectionManager->isSelected(iLayer, layer->GetLayerType(), currentEsuId))
			{
				drawSelection = true;
				selectionPen = layerPen;
			}
			else
			{
				// NMD - FAE 123 - couleur des éléments classifiés (Movies+ 210)
				if (pEISupervisedEsuResult != NULL)
				{
					const int layerClassId = pEISupervisedEsuResult->GetLayerClassification(iLayer);
					Pen ^classificationPen = ClassificationPen(layerClassId);
					if (classificationPen != nullptr)
					{
						selectionPen = classificationPen;
						drawSelection = true;
					}
				}
			}
		}
		
		int nbColumns = ref->GetWidth();
		auto bottomValues = ref->GetBottomValues();
		auto surfaceValues = ref->GetSurfaceValues();
		int width = FlatPictureBox->Width;

		switch (layer->GetLayerType())
		{
		case Layer::Type::SurfaceLayer:
			if (isSurfaceLayerEnable)
			{
				DrawSurfaceLayer(e, x1Esu, x2Esu
					, layer->GetMinDepth(), layer->GetMaxDepth()
					, layerPen, drawSelection, selectionPen);
			}
			break;

		case Layer::Type::BottomLayer:
			if (isBottomLayerEnable)
			{
				DrawOffsetLayer(e, true, x1Esu, x2Esu
					, layer->GetMinDepth(), layer->GetMaxDepth()
					, layerPen, drawSelection, selectionPen);
			}
			break;

		case Layer::Type::DistanceLayer:
			if (isDistanceLayerEnable)
			{
				DrawOffsetLayer(e, false, x1Esu, x2Esu
					, layer->GetMinDepth(), layer->GetMaxDepth()
					, layerPen, drawSelection, selectionPen);
			}
			break;
		}
	}
}

System::Void BaseFlatSideView::DrawDeviation(Graphics^  e)
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		DrawDeviationLine(e, refSide->GetDeviationValues());
	}
}

// méthode de dessin d'une ligne, telle que la ligne de fond, ou les lignes de contour.
System::Void BaseFlatSideView::DrawDeviationLine(Graphics^  e, const int * deviationsLine)
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		int nbColumns = refSide->GetWidth();
		int nbRows = refSide->GetHeight();
		int width = FlatPictureBox->Width;
		double realDisplayHeight = GetRealDisplayHeight();
		int lineWidth = floor(0.5 + realDisplayHeight / (float)nbRows);
		int startDepth = (int)refSide->GetStartPixel();
		m_deviationPen->Width = lineWidth;

		// le dessin du fond dépend des différents cas de figure et paramètres d'affichage :   
		if (Zoomed)
		{
			const double xRatio = (double)width / (double)nbColumns;
			for (int i = 0; i < nbColumns; ++i)
			{
				int deviationValue = deviationsLine[i];
				if (deviationValue > 0)
				{
					int x1 = (int)floor(0.5 + i * xRatio);
					int x2 = (int)floor(0.5 + (i + 1) * xRatio);
					e->DrawLine(m_deviationPen, x1, deviationValue, x2, deviationValue);
				}
			}
		}
		else
		{
			unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
			unsigned int realNbPings = refSide->GetLastAddedIndex() + 1;
			if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
			{
				// image plaquée à gauche, on affiche tous les pings 
				for (int i = 0; i < nbColumns; ++i)
				{
					int deviationValue = deviationsLine[i];
					if (deviationValue > 0)
					{
						int x1 = i*nbPixelsPerPing;
						int x2 = x1 + nbPixelsPerPing;
						e->DrawLine(m_deviationPen, x1, deviationValue, x2, deviationValue);
					}
				}
			}
			else
			{
				// image plaquée à droite
				int nbPingsToDraw = width / nbPixelsPerPing;
				int realNbPings = refSide->GetLastAddedIndex() + 1;
				for (int i = 0; i < nbPingsToDraw; ++i)
				{
					int echoIndex = realNbPings - 1 - i - m_OffsetHistoric;
					if (echoIndex >= 0 && echoIndex < nbColumns)
					{
						int deviationValue = deviationsLine[echoIndex];
						if (deviationValue > 0)
						{
							int x1 = width - i*nbPixelsPerPing;
							int x2 = x1 - nbPixelsPerPing;
							e->DrawLine(m_deviationPen, x1, deviationValue, x2, deviationValue);
						}
					}
				}
			}
		}
	}
}


System::Void BaseFlatSideView::DrawImage(Graphics^  e)
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		e->InterpolationMode = System::Drawing::Drawing2D::InterpolationMode::NearestNeighbor;
		e->PixelOffsetMode = System::Drawing::Drawing2D::PixelOffsetMode::Half;

		Rectangle dstRect;
		Rectangle srcRect;

		// on récupère la portion de l'image source à afficher (en fonction du nb de pixels par ping
		// et de la largeur de la fenêtre)
		unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
		unsigned int realNbPings = refSide->GetLastAddedIndex() + 1;
		// cas ou on cale les pings à gauche de la fenêtre (seulement si la fenêtre est assez large)
		if (Zoomed)
		{
			dstRect.Width = FlatPictureBox->Width;
			srcRect.Width = m_bmp->Width();
			srcRect.Height = m_bmp->Height();
		}
		else
		{
			if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
			{
				//source
				srcRect.Width = realNbPings;
				srcRect.Height = m_bmp->Height();
				//destination
				dstRect.Width = nbPixelsPerPing*realNbPings;
				dstRect.Height = FlatPictureBox->ClientRectangle.Height;
			}
			else
			{
				// cas ou on cale les pings à droite de la fenêtre
				int nbPingsToDraw = FlatPictureBox->Width / nbPixelsPerPing;
				int nbPixelsToClear = FlatPictureBox->Width - nbPixelsPerPing*nbPingsToDraw;
				if (nbPixelsToClear > 0)
				{
					e->FillRectangle(m_clearBrush, 0, 0, nbPixelsToClear, FlatPictureBox->Height);
				}
				srcRect.X = realNbPings - nbPingsToDraw - m_OffsetHistoric;
				srcRect.Width = nbPingsToDraw;
				srcRect.Height = m_bmp->Height();
				//destination
				dstRect.X = FlatPictureBox->Width - nbPingsToDraw*nbPixelsPerPing;
				dstRect.Width = nbPingsToDraw*nbPixelsPerPing;
				dstRect.Height = FlatPictureBox->Height;
			}
		}

		if (DisplayParameter::getInstance()->GetStrech())
		{
			m_bFullyStretched = true;
			dstRect.Height = this->FlatPictureBox->ClientRectangle.Height;

			m_bmp->Render(e, srcRect, dstRect);
		}
		else
		{
			m_bFullyStretched = false;

			// si il n'y a pas de correspondance de taille de fenêtre ou de transducteur sélectionné,
			// aucune raison de ne pas étirer verticalement le profil.

			MOVIESVIEWCPP::BaseFlatFrontView ^ dockedFlatFrontView = ViewManager->DockedFlatFrontView;
			if (this->Docked && dockedFlatFrontView != nullptr
				&& !dockedFlatFrontView->IsUserControlVisible()
				&& dockedFlatFrontView->GetSounderId() == this->GetSounderId()
				&& dockedFlatFrontView->GetTransducerIndex() == this->GetTransducerIndex())
			{

				if (m_bmp->IsValid())
				{
					m_StretchRatio = dockedFlatFrontView->GetStretchRatio();
				}
				else
				{
					m_StretchRatio = 1;
				}

				dstRect.Height = (int)(0.5 + m_bmp->Height()*dockedFlatFrontView->GetStretchRatio());

				// on met une bande noire en dessous du profil
				int nbpingDisplayed = 1 + refSide->GetLastDisplayedIndex() - refSide->GetStartHPixel();
				if (nbpingDisplayed > 0)
				{
					int blackWidth;
					if (Zoomed)
					{
						double filledRatio = (double)nbpingDisplayed / (double)m_bmp->Width();
						blackWidth = (int)(dstRect.Width*filledRatio + 0.5);
					}
					else
					{
						blackWidth = nbPixelsPerPing*nbpingDisplayed;
					}
					e->FillRectangle(m_blackBrush, 0, dstRect.Height, blackWidth, FlatPictureBox->Height - dstRect.Height);
				}
			}
			else
			{
				if (m_bmp->IsValid())
				{
					m_StretchRatio = (double)FlatPictureBox->Height / (double)m_bmp->Height();
				}
				else
				{
					m_StretchRatio = 1;
				}
				dstRect.Height = (int)(0.5 + m_bmp->Height()*m_StretchRatio);
			}

			m_bmp->Render(e, srcRect, dstRect);
		}
	}
}

System::Void BaseFlatSideView::DrawSelectionWindow(Graphics^  e)
{
	if (m_pViewSelectionObserver != nullptr
		&& m_pViewSelectionObserver->GetSelection() != nullptr
		&& !m_pViewSelectionObserver->IsShoalSelection())
	{
		// seulement si un traitement utilisant les ESU est en cours
		M3DKernel * pKern = M3DKernel::GetInstance();
		pKern->Lock();

		TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
		if (ref)
		{
			std::vector<unsigned int> pingsToDraw;
			std::vector<std::uint64_t> pingIds;
			pingIds.push_back((std::uint64_t)m_pViewSelectionObserver->GetSelection()->min->ping);
			pingIds.push_back((std::uint64_t)m_pViewSelectionObserver->GetSelection()->max->ping);
			ref->GetScreenPixelsFromPingIDs(pingIds, pingsToDraw);

			if (pingsToDraw.size() == 2)
			{
				double depthMin = m_pViewSelectionObserver->GetSelection()->min->depth;
				double depthMax = m_pViewSelectionObserver->GetSelection()->max->depth;
				double minDepth = DisplayParameter::getInstance()->GetCurrentMinDepth();
				double maxDepth = DisplayParameter::getInstance()->GetCurrentMaxDepth();
				int y = (int)(GetRealDisplayHeight()*(depthMin - minDepth) / (maxDepth - minDepth));
				int height = (int)(GetRealDisplayHeight()*(depthMax - minDepth) / (maxDepth - minDepth)) - y;

				// on effectue le tracé
				int nbPings = ref->GetWidth();
				int x, width;
				if (Zoomed)
				{
					x = (int)floor(0.5 + FlatPictureBox->ClientSize.Width*((double)pingsToDraw[0] / (double)nbPings));
					width = (int)floor(0.5 + FlatPictureBox->ClientSize.Width*((double)(pingsToDraw[1] - pingsToDraw[0]) / (double)nbPings));
				}
				else
				{
					unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
					unsigned int realNbPings = ref->GetLastAddedIndex() + 1;
					if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
					{
						// image plaquée à gauche, on affiche tous les pings   
						x = pingsToDraw[0] * nbPixelsPerPing;
						width = (1 + pingsToDraw[1] - pingsToDraw[0])*nbPixelsPerPing;
					}
					else
					{
						// image plaquée à droite
						x = (m_OffsetHistoric + pingsToDraw[0] - (realNbPings - FlatPictureBox->Width / nbPixelsPerPing))*nbPixelsPerPing;
						width = (1 + pingsToDraw[1] - pingsToDraw[0])*nbPixelsPerPing;
					}
				}

				Rectangle rect(x, y, width, height);

				e->DrawRectangle(m_selectionWindowPen, rect);

				// on commence par récupérer le transducteur de référence
				unsigned int sounderNb = pKern->getObjectMgr()->GetSounderDefinition().GetNbSounder();
				bool transFound = false;
				Transducer* pRefTransducer;
				unsigned int sounderID;
				unsigned int transIdx;
				for (unsigned int i = 0; i < sounderNb && !transFound; i++)
				{
					Sounder* pSounder = pKern->getObjectMgr()->GetSounderDefinition().GetSounder(i);

					// NMD -FAE 128 : ok pour le multi-faisceau ?
					//if(!pSounder->m_isMultiBeam)
					{
						for (unsigned int j = 0; j < pSounder->GetTransducerCount() && !transFound; j++)
						{
							Transducer *pTrans = pSounder->GetTransducer(j);
							String^ transName = gcnew String(pTrans->m_transName);

							// NMD -FAE 128 : on calcul le sA du transducteur visualisé et non plus du transducteur de référence
							//if(transName->Equals(m_pViewSelectionObserver->GetTransducerRefName()))
							if (transName->Equals(GetTransducer()))
							{
								pRefTransducer = pTrans;
								transIdx = j;
								sounderID = pSounder->m_SounderId;
								transFound = true;
							}
						}
					}
				}

				// si on a trouvé le transducteur de référence, on echo-intégre les échos de ce transducteur sur la 
				// fenêtre de sélection
				if (transFound)
				{
					bool weightedEchoIntegration = pKern->GetRefKernelParameter().getWeightedEchoIntegration();
					double energy = 0.0;
					unsigned int nbPing = 0;
					double distanceTravelled = 0.0;
					DataFmt threshold = m_pViewSelectionObserver->GetDataThreshold();
					// pour chaque ping de la fenêtre

					int iBeam = (pRefTransducer->m_numberOfSoftChannel - 1) / 2;

					for (std::uint64_t pingId = pingIds[0]; pingId <= pingIds[1]; pingId++)
					{
						PingFan* pFan = pKern->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingId);
						if (pFan)
						{
							Sounder * pSound = pFan->getSounderRef();
							if (pSound->m_SounderId == sounderID)
							{
								double beamsSamplesSpacing = pSound->GetTransducer(transIdx)->getBeamsSamplesSpacing();
								// calcul de la distance parcourue par différence avec le ping précédent
								double pingDistance = pFan->getPingDistance();
								distanceTravelled += pingDistance;
								nbPing++;
								MemoryStruct *pMem = pFan->GetMemorySetRef()->GetMemoryStruct(transIdx);
								BaseMathLib::Vector2I size = pMem->GetDataFmt()->getSize();

								// NMD -FAE 128 : ok pour le multi-faisceau ?
								//assert(size.x == 1); // on est en mono

								// boucle sur les echos
								DataFmt * pStart = pMem->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(iBeam, 0));
								for (int iEcho = 0; iEcho < size.y; iEcho++)
								{
									double value = *(pStart + iEcho);
									// on passe à la suite si on ne respecte pas le seuil
									if (value >= threshold)
									{
										// si l'echo n'est pas filtré
										if (!(*(pMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(iBeam, iEcho)))))
										{
											BaseMathLib::Vector3D echoPos = pSound->GetPolarToGeoCoord(pFan, transIdx, iBeam, iEcho);
											if (echoPos.z >= depthMin && echoPos.z <= depthMax)
											{
												if (weightedEchoIntegration)
												{
													energy += pow(10.0, value / 1000.0)*beamsSamplesSpacing*pingDistance;
												}
												else
												{
													energy += pow(10.0, value / 1000.0)*beamsSamplesSpacing;
												}
											}
										}
									}
								}
							}
						}
					}
					// calcul du sA (NASC) 
					double sA = 0.0;
					if (nbPing > 0)
					{
						if (weightedEchoIntegration)
						{
							if (distanceTravelled > 0.0)
							{
								sA = 1852.*1852.*4.*PI*energy / distanceTravelled;
							}
						}
						else
						{
							sA = 1852.*1852.*4.*PI*energy / nbPing;
						}
					}

					int margin = 1; //px
					String^ str = String::Format("sA = {0:0.00}", sA);
					SizeF^ size = e->MeasureString(str, m_defaultFont);
					System::Drawing::SolidBrush^ brush = gcnew System::Drawing::SolidBrush(Color::White);
					// si on a plus de place à droite qu'à gauche on le met a droite
					int offset = 0;
					if ((FlatPictureBox->Width - (x + width)) < x)
					{
						offset = -(1 + width + 4 * margin + size->Width);
					}
					e->FillRectangle(Brushes::BlueViolet, System::Drawing::Rectangle(offset + x + width + margin, y, size->Width + 2 * margin, size->Height + 2 * margin));
					e->DrawString(str, m_defaultFont, brush, offset + x + width + 2 * margin, y + margin);
				}
			}
		}

		pKern->Unlock();
	}
}

std::uint64_t BaseFlatSideView::GetPingIdFromCursorPos(double cursorCenterXpercent, bool bGetlastPingIfTooFar)
{
	std::uint64_t result = -1;

	// TODONMD : voir si on peut pas remplacer directement par
	// TransducerFlatView * ref = (TransducerFlatView *) GetTransducerView();
	// if(ref)

	// OTK - 02/04/2009 - blindage pour éviter le plantage si on clique sans avoir chargé de fichier
	if (m_viewContainer->SideViewContainer->GetView(0))
	{
		TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
		int desiredOffsetIndex;
		if (Zoomed)
		{
			desiredOffsetIndex = cursorCenterXpercent*m_bmp->Width() / 100;
		}
		else
		{
			unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
			unsigned int realNbPings = ref->GetLastAddedIndex() + 1;
			if (FlatPictureBox->Width >= nbPixelsPerPing*realNbPings)
			{
				// image plaquée à gauche, on affiche tous les pings  
				desiredOffsetIndex = (cursorCenterXpercent / 100.0)*FlatPictureBox->Width / nbPixelsPerPing;
			}
			else
			{
				// image plaquée à droite
				desiredOffsetIndex = realNbPings - m_OffsetHistoric - (1.0 - cursorCenterXpercent / 100.0)*FlatPictureBox->Width / nbPixelsPerPing;
			}
		}
		int FinalOffset = ref->GetLastAddedIndex() - ref->GetStartHPixel() - desiredOffsetIndex;
		int OffsetCursor = 0;
		int localOffsetCursor = FinalOffset;
		if (FinalOffset > 0)
		{
			OffsetCursor = FinalOffset;
		}
		else
		{
			OffsetCursor = 0;
		}
		if (bGetlastPingIfTooFar && localOffsetCursor < 0)
		{
			localOffsetCursor = 0;
		}
		// mise à jour de la vue 3D : ajustement de l'offset afin de visualiser le ping sélectionné
		M3DKernel *pKernel = M3DKernel::GetInstance();
		pKernel->Lock();
		// 1 - récupération de l'ID du dernier ping global
		size_t numberOfFan = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectCount();
		PingFan *pFan = (PingFan *)pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(numberOfFan - 1);
		if (pFan)
		{
			// 2 - récupération de l'ID du ping pointé par le curseur
			TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(ref->GetSounderId());
			if (pCont)
			{
				int Index = pCont->GetObjectCount() - 1 - localOffsetCursor;
				if (Index < 0)
				{
					if (bGetlastPingIfTooFar)
					{
						Index = 0;
					}
					else
					{
						pKernel->Unlock();
						return -1;
					}
				}

				// NMD - FAE104 verification si le ping existes
				PingFan *cursorFan = (PingFan *)pCont->GetDatedObject(Index);
				if (cursorFan)
				{
					result = cursorFan->GetPingId();
				}
			}
		}
		pKernel->Unlock();
	}

	return result;
}


System::Void  BaseFlatSideView::CursorHasChanged(double value)
{
	if (m_bPolygonSelectionMode || m_bEISupervisedSelectionMode || m_bBottomCorrectionMode)
		return;


	// TODONMD : voir si on peut pas remplacer directement par
	// TransducerFlatView * ref = (TransducerFlatView *) GetTransducerView();
	// if(ref)

	// OTK - 02/04/2009 - blindage pour éviter le plantage si on clique sans avoir chargé de fichier
	if (m_viewContainer->SideViewContainer->GetView(0))
	{
		m_NeedRedraw = true;

		TransducerFlatView * currentView = (TransducerFlatView *)GetTransducerView();
		int desiredOffsetIndex;
		if (Zoomed)
		{
			desiredOffsetIndex = value*m_bmp->Width() / 100;
		}
		else
		{
			unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
			unsigned int realNbPings = currentView->GetLastAddedIndex() + 1;
			if (FlatPictureBox->Width >= nbPixelsPerPing*realNbPings)
			{
				// image plaquée à gauche, on affiche tous les pings  
				desiredOffsetIndex = (value / 100.0)*FlatPictureBox->Width / nbPixelsPerPing;
			}
			else
			{
				// image plaquée à droite
				desiredOffsetIndex = realNbPings - m_OffsetHistoric - (1.0 - value / 100.0)*FlatPictureBox->Width / nbPixelsPerPing;
			}
		}
		int FinalOffset = currentView->GetLastAddedIndex() - currentView->GetStartHPixel() - desiredOffsetIndex;
		if (FinalOffset > 0)
		{
			m_OffsetCursor = FinalOffset;
		}
		else
		{
			m_OffsetCursor = 0;
		}
		// mise à jour de la vue 3D : ajustement de l'offset afin de visualiser le ping sélectionné
		M3DKernel *pKernel = M3DKernel::GetInstance();
		pKernel->Lock();

		// 1 - récupération de l'ID du dernier ping global
		std::uint64_t lastPingId, cursorPingId;
		size_t numberOfFan = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectCount();
		PingFan *pFan = (PingFan *)pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(numberOfFan - 1);
		if (pFan)
		{
			lastPingId = pFan->GetPingId();
			// 2 - récupération de l'ID du ping pointé par le curseur
			TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(currentView->GetSounderId());
			if (pCont)
			{
				int Index = pCont->GetObjectCount() - 1 - m_OffsetCursor;
				if (Index < 0)
				{
					Index = 0;
				}
				PingFan *cursorFan = (PingFan *)pCont->GetDatedObject(Index);
				cursorPingId = cursorFan->GetPingId();
			}
		}
		pKernel->Unlock();

		// 3 - mise à jour de l'offset de la vue 3D
		// OTK - FAE038 - on centre la fourchette de visu des pings 3D sur le ping cliqué en 2D
		int semiWidth = DisplayParameter::getInstance()->GetPingFanLength() / 2;
		ViewManager->VolumicView->UpdateShiftPingFan( std::max<int>(0, (int)lastPingId - (int)cursorPingId - semiWidth));

		DisplayedPingOffsetChanged(currentView->GetSounderId(), m_OffsetCursor);
	}
}

System::String^ BaseFlatSideView::FormatEchoInformation(int ScreenPosX, int ScreenPosY)
{
	System::String ^result = "";

	TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
	if (ref)
	{
		// on passe la position de la souris sur l'image en % de la taille de l'image
		// +0.5 pour tenir compte de l'interpolation de l'image en nearestNeighboor
		double posY = ((double)ScreenPosY + 0.5) / (GetRealDisplayHeight());
		unsigned int pingFanIdx;
		if (Zoomed)
		{
			double posX = ((double)ScreenPosX + 0.5) / (this->FlatPictureBox->Size.Width);
			pingFanIdx = (unsigned int)floor(posX*(ref->GetStopHPixel() - ref->GetStartHPixel()) + ref->GetStartHPixel());
		}
		else
		{
			unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
			unsigned int realNbPings = ref->GetLastAddedIndex() + 1;
			if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
			{
				// image plaquée à gauche, on affiche tous les pings 
				pingFanIdx = ScreenPosX / nbPixelsPerPing;
			}
			else
			{
				// image plaquée à droite
				pingFanIdx = realNbPings - m_OffsetHistoric - ceil((double)(FlatPictureBox->Width - ScreenPosX) / nbPixelsPerPing);
			}
		}

		if (posY >= 0 && posY <= 1)
		{
			result = ref->FormatEchoData(pingFanIdx, posY);
		}
	}

	if (result->Equals(""))
	{
		result = "No Data";
	}

	return result;
}

// mise à jour de la table de transformation en fonction de la taille de la fenêtre
System::Void BaseFlatSideView::UpdateTransformTable()
{
	TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
	if (ref)
	{
		// OTK - 26/10/2009 - maintenant que les hauteurs des deux vues 2D ne sont plus forcément identiques
		// (fenêtres non dockées), on doit prendre le max des deux hauteurs pour assurer une résolution 
		// idéale pour les deux visus
		int maxHeight = 0;
		for each(auto view in ViewManager->SiblingFlatViews(this))
		{
			maxHeight = Math::Max(maxHeight, view->GetFlatPictureBox()->Height);
		}

		int maxWidth = 0;
		for each(auto view in ViewManager->SiblingFlatFrontViews(this))
		{
			maxWidth = Math::Max(maxWidth, view->GetFlatPictureBox()->Width);
		}

		ref->UpdateTransformMap(maxWidth, maxHeight, DisplayParameter::getInstance()->GetStrech());
	}
}

// mise à jour de la table de transformation en fonction de la taille de la fenêtre
System::Void BaseFlatSideView::UpdateTransformTable(int index)
{
	TransducerFlatView* ref = this->m_viewContainer->SideViewContainer->GetView(index);
	if (ref)
	{
		// OTK - 26/10/2009 - maintenant que les hauteurs des deux vues 2D ne sont plus forcément identiques
		// (fenêtres non dockées), on doit prendre le max des deux hauteurs pour assurer une résolution 
		// idéale pour les deux visus
		int maxHeight = 0;
		for each(auto view in ViewManager->SiblingFlatViews(this))
		{
			maxHeight = Math::Max(maxHeight, view->GetFlatPictureBox()->Height);
		}

		int maxWidth = 0;
		for each(auto view in ViewManager->SiblingFlatFrontViews(this))
		{
			maxWidth = Math::Max(maxWidth, view->GetFlatPictureBox()->Width);
		}

		ref->UpdateTransformMap(maxWidth, maxHeight, DisplayParameter::getInstance()->GetStrech());
	}
}

//mapping d'un point en coord ecran vers coord réelles
PointF BaseFlatSideView::MapScreenToRealCoord(PointF screenCoord)
{
	//gestion des coordonnées verticales dans la classe parente
	PointF result = BaseFlatView::MapScreenToRealCoord(screenCoord);

	//dans cette vue, l'axe X représente les numéros de ping
	double cursorCenterXpercent = (100.0*screenCoord.X) / FlatPictureBox->Width;

	result.X = GetPingIdFromCursorPos(cursorCenterXpercent, true);

	return result;
}

bool BaseFlatSideView::Zoomed::get() 
{ 
	return DisplayParameter::getInstance()->IsZoomActivated();
}

System::Void BaseFlatSideView::OnZoom()
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		DisplayParameter * pDisplay = DisplayParameter::getInstance();
		bool isZoomed = pDisplay->IsZoomActivated();
		pDisplay->SetZoomActivated(true);

		// zoom suivant l'axe horizontal
		// mémorisation du zoom horizontal courant

		ZoomSection ^zoom = gcnew ZoomSection();
		zoom->horizontalZoom = gcnew RMinRMax();
		
		// HORIZONTAL ZOOM
		// pourcentage de la largeur totale affichée actuellement
		double currentWidthpercent, rightWidthPercent, leftWidthPercent;
		double newCursorPos;

		// cas particulier du premier zoom (n pixels par ping)
		if (isZoomed)
		{
			zoom->horizontalZoom->RMin = pDisplay->GetMinSideHorizontalRatio();
			zoom->horizontalZoom->RMax = pDisplay->GetMaxSideHorizontalRatio();
			rightWidthPercent = (double)m_DrawingRegion.X / (double)FlatPictureBox->Width;
			leftWidthPercent = (double)(FlatPictureBox->Width - (m_DrawingRegion.X + m_DrawingRegion.Width)) / (double)FlatPictureBox->Width;
			currentWidthpercent = zoom->horizontalZoom->RMax - zoom->horizontalZoom->RMin;
			
			// nouveau pourcentage du point de la selection
			pDisplay->SetMinMaxSideHorizontalRatio(zoom->horizontalZoom->RMin + rightWidthPercent*currentWidthpercent, zoom->horizontalZoom->RMax - leftWidthPercent*currentWidthpercent);
			newCursorPos = 100.*(1. - leftWidthPercent);
		}
		else
		{
			zoom->horizontalZoom->RMin = 0.0;
			zoom->horizontalZoom->RMax = 1.0;
			unsigned int nbPixelsPerPing = pDisplay->GetPixelsPerPing();
			unsigned int totalNbPings = refSide->GetWidth();
			unsigned int realNbPings = refSide->GetLastAddedIndex() + 1;
			if (FlatPictureBox->Width >= nbPixelsPerPing*realNbPings)
			{
				// image plaquée à gauche, on affiche tous les pings   
				rightWidthPercent = (double)m_DrawingRegion.X / (double)nbPixelsPerPing / (double)totalNbPings;
				leftWidthPercent = ((double)m_DrawingRegion.X + (double)m_DrawingRegion.Width) / (double)nbPixelsPerPing / (double)totalNbPings;
			}
			else
			{
				// image plaquée à droite
				rightWidthPercent = ((double)realNbPings - (double)m_OffsetHistoric - ((double)(FlatPictureBox->Width - m_DrawingRegion.X) / (double)nbPixelsPerPing)) / (double)totalNbPings;
				leftWidthPercent = ((double)realNbPings - (double)m_OffsetHistoric - ((double)(FlatPictureBox->Width - (m_DrawingRegion.X + m_DrawingRegion.Width)) / (double)nbPixelsPerPing)) / (double)totalNbPings;
			}
			// nouveau pourcentage du point de la selection
			pDisplay->SetMinMaxSideHorizontalRatio(rightWidthPercent, leftWidthPercent);
			newCursorPos = 100.*leftWidthPercent;
		}
		
		// VERTICAL ZOOM
		double currentMinDepth = pDisplay->GetCurrentMinDepth();
		double currentMaxDepth = pDisplay->GetCurrentMaxDepth();
		double currentHeightMeters = (currentMaxDepth - currentMinDepth)*GetVerticalStretchRatio();

		// point superieur
		double heightPercent = (double)(m_DrawingRegion.Y) / (double)FlatPictureBox->Height;
		double newMinDepth = currentMinDepth + heightPercent*currentHeightMeters;
		// On ne permet pas de "dézoomer en zoomant", provoque des plantage si on dézoom trop loin
		newMinDepth = std::max(newMinDepth, currentMinDepth);
		pDisplay->SetCurrentMinDepth(newMinDepth);

		// point inférieur
		heightPercent = ((double)m_DrawingRegion.Y + (double)m_DrawingRegion.Height) / (double)FlatPictureBox->Height;
		double newMaxDepth = currentMinDepth + heightPercent*currentHeightMeters;
		// On ne permet pas de "dézoomer en zoomant", provoque des plantage si on dézoom trop loin
		newMaxDepth = std::min(newMaxDepth, currentMaxDepth);
		pDisplay->SetCurrentMaxDepth(newMaxDepth);

		zoom->verticalZoom = gcnew RMinRMax();
		zoom->verticalZoom->RMin = currentMinDepth;
		zoom->verticalZoom->RMax = currentMaxDepth;


		m_ZoomStack.Push(zoom);
		UpdateHistoricSCrollBar();

		// on met à jour le parametre pingfandelayed de la vue 3D pour qu'elle se positionne comme il faut
		//CursorHasChanged(newCursorPos);
	}
}

System::Void BaseFlatSideView::ZoomOut()
{
	DisplayParameter * pDisplay = DisplayParameter::getInstance();
	double widthPercent = 100.0;

	if (m_ZoomStack.Count > 1)
	{
		ZoomSection^ prevZoom = (ZoomSection^)m_ZoomStack.Pop();
		pDisplay->SetZoomActivated(true);
		pDisplay->SetCurrentMinDepth(prevZoom->verticalZoom->RMin);
		pDisplay->SetCurrentMaxDepth(prevZoom->verticalZoom->RMax);

		if (prevZoom->horizontalZoom != nullptr)
		{
			pDisplay->SetMinSideHorizontalRatio(prevZoom->horizontalZoom->RMin);
			pDisplay->SetMaxSideHorizontalRatio(prevZoom->horizontalZoom->RMax);

			// on replace le curseur à l'extrémité droite de la fenêtre
			if (Zoomed)
			{
				double currentMin = pDisplay->GetMinSideHorizontalRatio();
				double currentMax = pDisplay->GetMaxSideHorizontalRatio();
				widthPercent *= (prevZoom->horizontalZoom->RMax - currentMin) / (currentMax - currentMin);
			}
		}
	}
	else
	{
		if (m_ZoomStack.Count > 0)
		{
			m_ZoomStack.Pop();
		}
		pDisplay->SetZoomActivated(false);
		pDisplay->SetMinMaxSideHorizontalRatio(0, 1);
		pDisplay->SetMinMaxFrontHorizontalRatio(0, 1);
	}

	UpdateHistoricSCrollBar();

	//CursorHasChanged(widthPercent);
}

// dessin du fond. cette ligne de fond n'est plus intégrée à l'image pour ne pas dépendre
// de la taille des pixels de l'image une fois redimensionnée et conserver un fond
// bien visible quelle que soit la taille de la fenêtre de l'échogramme.
System::Void BaseFlatSideView::DrawBottom(Graphics^  e)
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		DrawEchoLine(e, refSide->GetBottomValues());
	}
}

System::Void BaseFlatSideView::DrawRefNoise(Graphics^  e)
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		auto echoesLine = refSide->GetRefNoiseValues();
		DrawNoisePoints(e, echoesLine, m_refNoisePen, m_refNoiseBrush);
	}
}

System::Void BaseFlatSideView::DrawNoise(Graphics^  e)
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		auto echoesLine = refSide->GetNoiseValues();
		DrawNoisePoints(e, echoesLine, m_noisePen, m_noiseBrush);
	}
}

System::Void BaseFlatSideView::DrawNoisePoints(Graphics^  e, int *echoesLine, Pen^ pen, Brush^ brush)
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		int nbColumns = refSide->GetWidth();
		int nbRows = refSide->GetHeight();
		int width = FlatPictureBox->Width;
		double realDisplayHeight = GetRealDisplayHeight();
		int startDepth = (int)refSide->GetStartPixel();

		int lineWidth = m_noisePen->Width;
		int lastVLine = -1;

		const double yOffset = 0.5 - lineWidth * 0.5;
		const double yRatio = realDisplayHeight / nbRows;

		System::Collections::Generic::List<System::Drawing::Point> ^ noisePoints = gcnew System::Collections::Generic::List<System::Drawing::Point>();

		// le dessin du fond dépend des différents cas de figure et paramètres d'affichage :   
		if (Zoomed)
		{
			const double xRatio = (double)width / (double)nbColumns;
			for (int i = 0; i < nbColumns; i++)
			{
				int echoPos = echoesLine[i];
				if (echoPos != -1)
				{
					int x = (int)floor(0.5 + i * xRatio);
					int y = (int)floor(yOffset + (1 + echoPos - startDepth)*yRatio);

					if (noisePoints->Count == 0)
					{
						noisePoints->Add(System::Drawing::Point(x, nbRows));
					}
					else
					{
						int lastY = noisePoints[noisePoints->Count - 1].Y;
						noisePoints->Add(System::Drawing::Point(x, lastY));
					}
					noisePoints->Add(System::Drawing::Point(x, y));
				}
			}

			if (noisePoints->Count != 0)
			{
				int x = (int)floor(0.5 + nbColumns * xRatio);;
				int y = noisePoints[noisePoints->Count - 1].Y;
				noisePoints->Add(System::Drawing::Point(x, y));
				noisePoints->Add(System::Drawing::Point(x, nbRows));
			}
		}
		else
		{
			unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
			unsigned int realNbPings = refSide->GetLastAddedIndex() + 1;
			if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
			{
				// image plaquée à gauche, on affiche tous les pings 
				for (int i = 0; i < nbColumns; i++)
				{
					int echoPos = echoesLine[i];
					if (echoPos != -1)
					{
						int y = (int)floor(yOffset + (1 + echoPos - startDepth)*yRatio);
						int x = i*nbPixelsPerPing;

						if (noisePoints->Count == 0)
						{
							noisePoints->Add(System::Drawing::Point(x, nbRows));
						}
						else
						{
							int lastY = noisePoints[noisePoints->Count - 1].Y;
							noisePoints->Add(System::Drawing::Point(x, lastY));
						}
						noisePoints->Add(System::Drawing::Point(x, y));
					}
				}

				if (noisePoints->Count != 0)
				{
					int x = nbPixelsPerPing*realNbPings;
					int y = noisePoints[noisePoints->Count - 1].Y;
					noisePoints->Add(System::Drawing::Point(x, y));
					noisePoints->Add(System::Drawing::Point(x, nbRows));
				}
			}
			else
			{
				// image plaquée à droite
				int nbPingsToDraw = width / nbPixelsPerPing;
				int realNbPings = refSide->GetLastAddedIndex() + 1;
				for (int i = nbPingsToDraw - 1; i >= 0; --i)
				{
					// OTK - FAE057 - correction plantage en cas de dezoom en cours de lecture/acquisition
					int echoIndex = realNbPings - 1 - i - m_OffsetHistoric;
					if (echoIndex >= 0 && echoIndex < nbColumns)
					{
						int echoPos = echoesLine[echoIndex];
						if (echoPos != -1)
						{
							int x = width - i*nbPixelsPerPing;
							int y = (int)floor(yOffset + (1 + echoPos - startDepth)*yRatio);

							if (noisePoints->Count == 0)
							{
								noisePoints->Add(System::Drawing::Point(x, nbRows));
							}
							else
							{
								int lastY = noisePoints[noisePoints->Count - 1].Y;
								noisePoints->Add(System::Drawing::Point(x, lastY));
							}
							noisePoints->Add(System::Drawing::Point(x, y));
						}
					}
				}

				if (noisePoints->Count != 0)
				{
					int x = nbPixelsPerPing*realNbPings;
					int y = noisePoints[noisePoints->Count - 1].Y;
					noisePoints->Add(System::Drawing::Point(x, y));
					noisePoints->Add(System::Drawing::Point(x, nbRows));
				}
			}
		}

		if (noisePoints->Count != 0)
		{
			array<System::Drawing::Point> ^pts = noisePoints->ToArray();
			e->FillPolygon(brush, pts);
			e->DrawPolygon(pen, pts);
		}
	}
}

System::Void BaseFlatSideView::DrawContourLines(Graphics^  e)
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		const auto & contourLines = refSide->GetContourLines();
		for (auto iterLine = contourLines.cbegin(); iterLine != contourLines.cend(); iterLine++)
		{
			const std::vector<int> & line = *iterLine;
			DrawEchoLine(e, line.data());
		}
	}
}

// méthode de dessin d'une ligne, telle que la ligne de fond, ou les lignes de contour.
System::Void BaseFlatSideView::DrawEchoLine(Graphics^  e, const int * echoesLine)
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		int nbColumns = refSide->GetWidth();
		int nbRows = refSide->GetHeight();
		int width = FlatPictureBox->Width;
		double realDisplayHeight = GetRealDisplayHeight();
		//int lineWidth = floor(0.5 + realDisplayHeight / (double)nbRows);
		int startDepth = (int)refSide->GetStartPixel();
		//m_bottomPen->Width = lineWidth;
		//m_bottomPen->Width = lineWidth;

		int lineWidth = m_bottomPen->Width;

		const double yOffset = 0.5 - lineWidth * 0.5;
		const double yRatio = realDisplayHeight / nbRows;

		// le dessin du fond dépend des différents cas de figure et paramètres d'affichage :   
		if (Zoomed)
		{
			int echoPos;
			const double xRatio = (double)width / (double)nbColumns;

			for (int i = 0; i < nbColumns; i++)
			{
				echoPos = echoesLine[i];
				if (echoPos != -1)
				{
					int x1 = (int)floor(0.5 + i * xRatio);
					int x2 = (int)floor(0.5 + (i + 1) * xRatio);
					int y = (int)floor(yOffset + (1 + echoPos - startDepth)*yRatio);
					e->DrawLine(m_bottomPen, x1, y, x2, y);
				}
			}
		}
		else
		{
			unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
			unsigned int realNbPings = refSide->GetLastAddedIndex() + 1;
			if (FlatPictureBox->ClientRectangle.Width >= nbPixelsPerPing*realNbPings)
			{
				// image plaquée à gauche, on affiche tous les pings 
				int echoPos;
				for (int i = 0; i < nbColumns; i++)
				{
					echoPos = echoesLine[i];
					if (echoPos != -1)
					{
						int x1 = i*nbPixelsPerPing;
						int x2 = x1 + nbPixelsPerPing;
						int y = (int)floor(yOffset + (1 + echoPos - startDepth)*yRatio);
						e->DrawLine(m_bottomPen, x1, y, x2, y);
					}
				}
			}
			else
			{
				// image plaquée à droite
				int nbPingsToDraw = width / nbPixelsPerPing;
				int realNbPings = refSide->GetLastAddedIndex() + 1;
				int echoPos, echoIndex;
				for (int i = 0; i < nbPingsToDraw; i++)
				{
					// OTK - FAE057 - correction plantage en cas de dezoom en cours de lecture/acquisition
					echoIndex = realNbPings - 1 - i - m_OffsetHistoric;
					if (echoIndex >= 0 && echoIndex < nbColumns)
					{
						echoPos = echoesLine[echoIndex];
						if (echoPos != -1)
						{
							int x1 = width - i*nbPixelsPerPing;
							int x2 = x1 - nbPixelsPerPing;
							int y = (int)floor(yOffset + (1 + echoPos - startDepth)*yRatio);
							e->DrawLine(m_bottomPen, x1, y, x2, y);
						}
					}
				}
			}
		}
	}
}

System::Void  BaseFlatSideView::OnRegionSelection()
{
	if (GetViewSelectionObserver() != nullptr)
	{
		// notification de l'observeur de selection
		// pour la vue de coté, les pings correspondent à l'axe X et la hauteur à l'axe Y
		PointF topLeft(m_DrawingRegion.X, m_DrawingRegion.Y);
		PointF bottomRight(m_DrawingRegion.X + m_DrawingRegion.Width,
			m_DrawingRegion.Y + m_DrawingRegion.Height);

		PointF topLeftReal = MapScreenToRealCoord(topLeft);
		PointF bottomRightReal = MapScreenToRealCoord(bottomRight);

		ViewSelectionPoint^ minPoint = gcnew ViewSelectionPoint(topLeftReal.X, topLeftReal.Y);
		ViewSelectionPoint^ maxPoint = gcnew ViewSelectionPoint(bottomRightReal.X, bottomRightReal.Y);

		ViewSelectionRange ^range = gcnew ViewSelectionRange();
		range->min = minPoint;
		range->max = maxPoint;

		GetViewSelectionObserver()->OnSelectionRange(range, false);
	}
}

System::Void  BaseFlatSideView::OnShoalSelection(PointF pt)
{
	// on verifie qu'on ne fait pas en mode dessin de polygone ou selection de l'echo integration supervisée
	if (m_bPolygonSelectionMode || m_bEISupervisedSelectionMode || m_bBottomCorrectionMode)
		return;

	if (GetViewSelectionObserver() != nullptr)
	{
		// notification de l'observeur de selection
		//pour la vue de coté, les pings correspondent à l'axe X et la hauteur à l'axe Y
		PointF realPingDepth = MapScreenToRealCoord(pt);

		//get the cartesian X and Y coord from the active slide

		//get the ping fan
		M3DKernel *pKernel = M3DKernel::GetInstance();
		pKernel->Lock();
		int ping = realPingDepth.X;
		PingFan *pFan = (PingFan *)pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(ping);

		//retrieve cartesian X
		unsigned int transducerListIdx = GetCurrentIndex();
		unsigned int transducerSounderIdx = m_viewContainer->SideViewContainer->GetView(transducerListIdx)->GetTransducerIndex();

		if (pFan != NULL)
		{
			if (transducerSounderIdx < pFan->GetMemorySetRef()->GetMemoryStructCount())
			{
				MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(transducerSounderIdx);
				TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transducerSounderIdx);
				Transducer *pTrans = pFan->getSounderRef()->GetTransducer(transducerSounderIdx);
				double m_compensateHeave = 0;
				SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
				if (pSoftChan)
				{
					m_compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
				}
				BaseMathLib::Vector2I memSize = pTransform->getCartesianSize2();
				double activeSlide = this->m_viewContainer->SideViewContainer->GetActiveSlide(transducerListIdx);
				int xCartesian = floor(memSize.x * activeSlide / 100.0);

				//convert cartesianX to realX
				double realX = pTransform->cartesianToRealX(xCartesian);
				//substract heave to depth to get real Depth
				double realY = realPingDepth.Y - m_compensateHeave - pTrans->m_transDepthMeter;

				//convert real Coord to polar Coord
				if (realY >= 0)
				{
					BaseMathLib::Vector2D realCoord(realX, realY);
					BaseMathLib::Vector2D polarCoord = pTransform->realToPolar2(realCoord);

					if (polarCoord.x >= 0 && polarCoord.y >= 0)
					{
						//notify observer
						//arrondi de la coordonnée polaire x pour obtenir le faisceau exact 
						//(sans compensation de l'angle dans le faisceau)
						polarCoord.x = (int)(polarCoord.x + 0.5);
						GetViewSelectionObserver()->OnSelectionPoint(
							gcnew ViewSelectionPolarPoint(ping, polarCoord.x, polarCoord.y), GetTransducer());
					}
					else
					{
						GetViewSelectionObserver()->OnSelectionError();
					}
				}
				else
				{
					GetViewSelectionObserver()->OnSelectionError();
				}
			}
		}
		pKernel->Unlock();
	}
}

void BaseFlatSideView::DelayChanged(ASyncDelayChangedStatus^ status)
{
	if (mShowDelayInfo)
	{
		if (status->ExtractionCount == -1)
		{
			// on sauvegarde le nouveau readerCount et maxDelay
			mReaderCount = status->ReaderCount;
			mMaxDelay = status->MaxDelay + 1;
		}
		else if (status->ReaderCount == -1)
		{
			int delay = (int)mReaderCount - (int)status->ExtractionCount;
			progressBar->Maximum = mMaxDelay;
			if (delay >= progressBar->Minimum && delay <= progressBar->Maximum)
			{
				progressBar->Value = delay;
			}
			// on traite le nouveau extractionCount
			if (delay > mMaxDelay / 20)
			{
				progressBar->Visible = true;
				labelProgress->Visible = true;
			}
			else if (delay <= 1)
			{
				progressBar->Visible = false;
				labelProgress->Visible = false;
			}
		}
		else
		{
			assert(0); // l'une des deux valeurs doit être -1
		}
	}
}

// mise à jour du retard de l'extraction de banc sur la vue de 
void BaseFlatSideView::UpdateShoalExtractionDelay(std::uint32_t readerCount, std::uint32_t extractionCount,
	std::uint32_t maxDelay)
{
	ASyncDelayChangedStatus^ status = gcnew ASyncDelayChangedStatus(readerCount, extractionCount, maxDelay);
	this->BeginInvoke(gcnew DelayChangedDelegate(this, &BaseFlatSideView::DelayChanged), status);
}

void BaseFlatSideView::UpdateShoalExtractionDelayVisible(bool bVisible)
{
	if (bVisible)
	{
		mShowDelayInfo = true;
	}
	else
	{
		mShowDelayInfo = false;
		progressBar->Visible = false;
		labelProgress->Visible = false;
	}
}

int BaseFlatSideView::GetDataWidth()
{
	int width;
	TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
	if (Zoomed)
	{
		width = FlatPictureBox->Width*(((double)ref->GetLastDisplayedIndex() + 1 - ref->GetStartHPixel()) / (double)(m_bmp->Width()));
	}
	else
	{

		int nbpingDisplayed = 1 + ref->GetLastDisplayedIndex() - ref->GetStartHPixel();
		width = nbpingDisplayed*DisplayParameter::getInstance()->GetPixelsPerPing();
	}
	return width;
}

System::Void BaseFlatSideView::UpdateHistoricSCrollBar()
{
	TransducerFlatView * refSide = (TransducerFlatView *)GetTransducerView();
	if (refSide)
	{
		auto display = DisplayParameter::getInstance();

		display->Lock();
		unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();
		auto horizontalSideRatio = display->GetMaxSideHorizontalRatio() - display->GetMinSideHorizontalRatio();
		display->Unlock();

		auto realNbPings = refSide->GetLastAddedIndex() + 1;
		auto displayablePingsUnzoomed = FlatPictureBox->Width / nbPixelsPerPing;

		if (Zoomed)
		{
			// on peut scroller
			HistoricHScrollBar->Enabled = true;

			auto kernel = M3DKernel::GetInstance();
			kernel->Lock();

			auto displayedPingsZoomed = Math::Round(horizontalSideRatio *
				kernel->GetRefKernelParameter().getNbPingFanMax());

			kernel->Unlock();

			// dimensionnement de la scrollbar

			HistoricHScrollBar->SmallChange = Math::Max(1.0, 0.01 * displayedPingsZoomed);
			HistoricHScrollBar->LargeChange = Math::Max(1.0, 0.10 * displayedPingsZoomed);

			auto changeOverhead = HistoricHScrollBar->LargeChange - 1;

			HistoricHScrollBar->Minimum = 0;

			if (displayablePingsUnzoomed < realNbPings)
			{
				HistoricHScrollBar->Maximum = realNbPings - displayedPingsZoomed + changeOverhead;
			}
			else {
				HistoricHScrollBar->Maximum = displayablePingsUnzoomed - displayedPingsZoomed + changeOverhead;
			}

			HistoricHScrollBar->Value = Math::Max(0, Math::Min(HistoricHScrollBar->Maximum - changeOverhead,
				(int)Math::Round(display->GetMinSideHorizontalRatio() * kernel->GetRefKernelParameter().getNbPingFanMax())));

			auto test = refSide->GetStartHPixel();
		}
		else
		{
			if (displayablePingsUnzoomed < realNbPings)
			{
				// on peut scroller
				HistoricHScrollBar->Enabled = true;

				// dimensionnement de la scrollbar
				HistoricHScrollBar->SmallChange = Math::Max(1.0, 0.01 * displayablePingsUnzoomed);
				HistoricHScrollBar->LargeChange = Math::Max(1.0, 0.10 * displayablePingsUnzoomed);
				
				HistoricHScrollBar->Minimum = 0;

				auto changeOverhead = HistoricHScrollBar->LargeChange - 1;

				HistoricHScrollBar->Maximum = realNbPings - displayablePingsUnzoomed + changeOverhead;
				HistoricHScrollBar->Value = Math::Max(0, Math::Min(HistoricHScrollBar->Maximum - changeOverhead,
					(int)(realNbPings - displayablePingsUnzoomed - m_OffsetHistoric)));
			}
			else
			{
				// on ne peut pas scroller
				HistoricHScrollBar->Enabled = false;
			}
		}
	}
}

System::Void BaseFlatSideView::PingFanAdded()
{
	// appel à la méthode mère
	BaseFlatView::PingFanAdded();

	// mise à jour de la scrollbar
	UpdateHistoricSCrollBar();
}

System::Void BaseFlatSideView::FlatPictureBox_Resize(System::Object^  sender, System::EventArgs^  e)
{
	// mise à jour de la scrollbar
	UpdateHistoricSCrollBar();

	// appel à la méthode mère
	BaseFlatView::FlatPictureBox_Resize(sender, e);
}

System::Void BaseFlatSideView::HistoricHScrollBar_Scroll(System::Object^ sender, System::Windows::Forms::ScrollEventArgs^ e)
{
	if (e->NewValue == e->OldValue) {
		return;
	}

	if (Zoomed)
	{
		auto oldZone = Rectangle(m_DrawingRegion.X, m_DrawingRegion.Y, m_DrawingRegion.Width, m_DrawingRegion.Height);
		auto display = DisplayParameter::getInstance();
		auto minRatio = display->GetMinSideHorizontalRatio();
		auto maxRatio = display->GetMaxSideHorizontalRatio();
		auto pingMax = M3DKernel::GetInstance()->GetRefKernelParameter().getNbPingFanMax();
		//auto horizontalNumberOfPings = Math::Round((display->GetMaxSideHorizontalRatio() - display->GetMinSideHorizontalRatio()) * Kernel::GetInstance()->GetRefKernelParameter().getNbPingFanMax());
		
		auto displayedPings = (maxRatio - minRatio)* pingMax;
		auto pixelWidth = FlatPictureBox->Width / displayedPings;

		auto shift = Math::Round(pixelWidth * (e->NewValue - e->OldValue));
		
		m_DrawingRegion = System::Drawing::Rectangle(shift, 0, FlatPictureBox->Width, FlatPictureBox->Height / GetVerticalStretchRatio());

		OnZoom();

		// La fonction OnZoom ajoute un nouvel élément qu'on ne souhaite pas conserver dans le cas où on "zoome latéralement"
		m_ZoomStack.Pop();

		UpdateHistoricSCrollBar();
	}
	else 
	{
		m_OffsetHistoric = (HistoricHScrollBar->Maximum - HistoricHScrollBar->LargeChange) - e->NewValue;
		this->FlatPictureBox->Refresh();

		// FAE 175 - synchronization des ascenseurs horizontaux
		OffsetHistoricChange(m_OffsetHistoric);
	}
}

void BaseFlatSideView::SetOffsetHistoricReference(int offsetReference)
{
	int targetHistoricOffset = offsetReference;
	if ((HistoricHScrollBar->Maximum - HistoricHScrollBar->LargeChange) - targetHistoricOffset < HistoricHScrollBar->Minimum)
	{
		targetHistoricOffset = (HistoricHScrollBar->Maximum - HistoricHScrollBar->LargeChange) - HistoricHScrollBar->Minimum;
	}
	if ((HistoricHScrollBar->Maximum - HistoricHScrollBar->LargeChange) - targetHistoricOffset > HistoricHScrollBar->Maximum)
	{
		targetHistoricOffset = (HistoricHScrollBar->Maximum - HistoricHScrollBar->LargeChange) - HistoricHScrollBar->Maximum;
	}
	m_OffsetHistoricReference = targetHistoricOffset;
	if (m_OffsetHistoricReference != m_OffsetHistoric && HistoricHScrollBar->Visible)
	{
		m_OffsetHistoric = targetHistoricOffset;
		this->HistoricHScrollBar->Scroll -= historicScrollHandler;
		HistoricHScrollBar->Value = (HistoricHScrollBar->Maximum - HistoricHScrollBar->LargeChange) - m_OffsetHistoric;
		this->HistoricHScrollBar->Scroll += historicScrollHandler;
		this->FlatPictureBox->Refresh();
	}
}


void BaseFlatSideView::SetEchotypesVisibility(bool bVisible)
{
	mShowEchoTypes = bVisible;
}

System::Void BaseFlatSideView::DrawOnGraphics(Graphics^ e)
{
	BaseFlatView::DrawOnGraphics(e);

	DrawPolygons(e);

	DrawSelectionPolygon(e);

	DrawBottomCorrectionPolyline(e);
}

System::Void BaseFlatSideView::DrawSelectionPolygon(Graphics^  e)
{
	if (m_bPolygonSelectionMode && m_SelectionPolygon.Count > 0)
	{
		Pen^ pen = gcnew Pen(Color::Black);
		System::Drawing::Point pt1 = m_SelectionPolygon[0];
		for (int iPt = 1; iPt < m_SelectionPolygon.Count; iPt++)
		{
			GDIWrapper::DrawXORLine(e, pen, pt1.X, pt1.Y, m_SelectionPolygon[iPt].X, m_SelectionPolygon[iPt].Y);
			pt1 = m_SelectionPolygon[iPt];
		}

		// dessin de la position de la souris
		GDIWrapper::DrawXORLine(e, pen, pt1.X, pt1.Y, m_CurrentPoint.X, m_CurrentPoint.Y);

		if (m_SelectionPolygon.Count > 1)
			GDIWrapper::DrawXORLine(e, pen, m_CurrentPoint.X, m_CurrentPoint.Y, m_SelectionPolygon[0].X, m_SelectionPolygon[0].Y);
	}
}

System::Void BaseFlatSideView::DrawBottomCorrectionPolyline(Graphics^  e)
{
	if (m_bBottomCorrectionMode && m_BottomCorrectionPolyLine.Count > 0)
	{
		Pen^ pen = gcnew Pen(Color::Black);
		System::Drawing::Point pt1 = m_BottomCorrectionPolyLine[0];
		for (int iPt = 1; iPt < m_BottomCorrectionPolyLine.Count; iPt++)
		{
			GDIWrapper::DrawXORLine(e, pen, pt1.X, pt1.Y, m_BottomCorrectionPolyLine[iPt].X, m_BottomCorrectionPolyLine[iPt].Y);
			pt1 = m_BottomCorrectionPolyLine[iPt];
		}

		// dessin de la position de la souris si elle se trouve sur des pings
		if (GetPingIdFromCursorPos((100.0*m_CurrentPoint.X) / FlatPictureBox->Width, false) != -1)
		{
			GDIWrapper::DrawXORLine(e, pen, pt1.X, pt1.Y, m_CurrentPoint.X, m_CurrentPoint.Y);
		}
	}
}

System::Void BaseFlatSideView::OnPolygonDeleteCancel(System::Object^  sender, System::EventArgs ^e)
{
	if (m_polygonToDelete)
	{
		delete m_polygonToDelete;
		m_polygonToDelete = NULL;
	}
	m_SelectionPolygon.Clear();
}

System::Void BaseFlatSideView::OnPolygonDeleteApply(System::Object^  sender, System::EventArgs ^e)
{
	if (m_polygonToDelete)
	{
		m_correctionManager->DeletePolygon(GetTransducerView()->GetSounderId(), GetTransducerView()->GetTransducerIndex(), m_polygonToDelete);

		// Pour rafraichissement des toutes les vues longitudinales concernées
		this->SounderChanged();

		// suppression du polygone supprimé
		delete m_polygonToDelete;
		m_polygonToDelete = NULL;
	}
	m_SelectionPolygon.Clear();
}

System::Void BaseFlatSideView::OnBottomCorrectionCancel(System::Object^  sender, System::EventArgs ^e)
{
	m_BottomCorrectionPolyLine.Clear();
}

System::Void BaseFlatSideView::OnBottomCorrectionApply(System::Object^  sender, System::EventArgs ^e)
{
	// On ajoute le dernier point non validé mais à prendre en compte
	if (GetPingIdFromCursorPos((100.0*m_CurrentPoint.X) / FlatPictureBox->Width, false) != -1)
	{
		m_BottomCorrectionPolyLine.Add(Point(m_CurrentPoint));
	}

	for (int iPoint = 1; iPoint < m_BottomCorrectionPolyLine.Count; iPoint++)
	{
		Point pt1(m_BottomCorrectionPolyLine[iPoint - 1].X, m_BottomCorrectionPolyLine[iPoint - 1].Y);
		Point pt2(m_BottomCorrectionPolyLine[iPoint].X, m_BottomCorrectionPolyLine[iPoint].Y);

		// récupération des numéros de ping
		std::uint64_t pingId1 = GetPingIdFromCursorPos((100.0*pt1.X) / FlatPictureBox->Width, false);
		std::uint64_t pingId2 = GetPingIdFromCursorPos((100.0*pt2.X) / FlatPictureBox->Width, false);

		// on ne traite pas les segments qui ne sont pas entièrement sur des pings
		if (pingId1 != -1 && pingId2 != -1)
		{
			std::uint64_t minPingId = MIN(pingId1, pingId2);
			std::uint64_t maxPingId = MAX(pingId1, pingId2);

			int sens;
			if (pingId1 <= pingId2)
			{
				sens = 1;
			}
			else
			{
				sens = -1;
			}

			std::vector<PingFan*> pingFans;
			for (std::uint64_t pingId = pingId1; pingId <= maxPingId && pingId >= minPingId; pingId += sens)
			{
				PingFan *pFan = (PingFan *)M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingId);
				if (pFan && pFan->m_pSounder->m_SounderId == GetTransducerView()->GetSounderId())
				{
					pingFans.push_back(pFan);
				}
			}

			// on interpole le pixel de la ligne pour chaque pingfan
			for (size_t iPing = 0; iPing < pingFans.size(); iPing++)
			{
				int pixelY;
				if (pingFans.size() == 1)
				{
					pixelY = (pt1.Y + pt2.Y) / 2;
				}
				else
				{
					pixelY = pt1.Y + iPing * ((double)(pt2.Y - pt1.Y)) / (pingFans.size() - 1);
				}

				int beamIndex, echoIndex;
				if (GetPingEchoAndBeam(pingFans[iPing], pixelY, beamIndex, echoIndex))
				{
					Transducer * pTrans = GetTransducerView()->GetTransducer();
					std::int32_t bottomRange = (std::int32_t)(1000.0 * pTrans->getBeamsSamplesSpacing() * (echoIndex + pTrans->GetSampleOffset()) + 0.5);
					m_correctionManager->CorrectBottom(pingFans[iPing], pTrans->GetChannelId()[beamIndex], bottomRange);
				}
			}

			// Pour rafraichissement des toutes les vues longitudinales concernées
			this->SounderChanged();
		}
	}

	m_BottomCorrectionPolyLine.Clear();
}

void PolygonToArray(BaseMathLib::Poly2D * polygon, List<PointF> % tmpPolygon,
	const std::uint64_t & minVisiblePing, const std::uint64_t & maxVisiblePing)
{
	const int nbPoints = polygon->GetNbPoints();
	for (int i = 0; i != nbPoints; i++)
	{
		double * _pt1 = polygon->GetPoint(i);
		double * _pt2 = polygon->GetPoint(((i + 1) == nbPoints) ? 0 : i + 1);

		PointF pt1;
		pt1.X = _pt1[0];
		pt1.Y = _pt1[1];

		PointF pt2;
		pt2.X = _pt2[0];
		pt2.Y = _pt2[1];

		// on passe le couple de points si les 2 points ne sont pas visibles
		if ((pt1.X < minVisiblePing && pt2.X < minVisiblePing)
			|| (pt1.X > maxVisiblePing && pt2.X > maxVisiblePing))
		{
			continue;
		}

		bool bAddPt2 = false;
		if (pt1.X < minVisiblePing || pt1.X > maxVisiblePing
			|| pt2.X < minVisiblePing || pt2.X > maxVisiblePing)
		{
			double a = (pt2.Y - pt1.Y) / (pt2.X - pt1.X);
			double b = pt1.Y - a * pt1.X;

			if (pt1.X < minVisiblePing)
			{
				pt1.X = minVisiblePing;
				pt1.Y = minVisiblePing * a + b;
			}
			else if (pt1.X > maxVisiblePing)
			{
				pt1.X = maxVisiblePing;
				pt1.Y = maxVisiblePing * a + b;
			}

			if (pt2.X < minVisiblePing)
			{
				bAddPt2 = true;
				pt2.X = minVisiblePing;
				pt2.Y = minVisiblePing * a + b;
			}
			else if (pt2.X > maxVisiblePing)
			{
				bAddPt2 = true;
				pt2.X = maxVisiblePing;
				pt2.Y = maxVisiblePing * a + b;
			}
		}
		tmpPolygon.Add(pt1);
		if (bAddPt2)
			tmpPolygon.Add(pt2);
	}
}

System::Void BaseFlatSideView::DrawPolygons(Graphics^  e)
{
	// Recuperation des polygones déja définis
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	EISupervisedModule * pEISupervisedModule = pModuleMgr->GetEISupervisedModule();

	std::string transName = netStr2CppStr(GetTransducer());
	PolygonSet polygons = pEISupervisedModule->GetPolygons(transName);

	if (polygons.size() > 0)
	{
		TransducerFlatView * pSideView = (TransducerFlatView *)GetTransducerView();

		//gestion des coordonnées verticales
		double yRealMin = DisplayParameter::getInstance()->GetCurrentMinDepth();
		double yRealMax = DisplayParameter::getInstance()->GetCurrentMaxDepth();
		int nbPings = pSideView->GetWidth();
		double currentHeightMeters = (yRealMax - yRealMin)*GetVerticalStretchRatio();

		std::uint64_t minVisiblePing = GetPingIdFromCursorPos(0.0f, true);
		std::uint64_t maxVisiblePing = GetPingIdFromCursorPos(100.0f, true);

		double xFact = 1.0f;
		double xDelta = 0.0f;

		double yFact = (double)FlatPictureBox->Height / currentHeightMeters;
		double yDelta = -yRealMin;

		if (Zoomed)
		{
			xFact = (double)FlatPictureBox->ClientSize.Width / nbPings;
		}
		else
		{
			unsigned int realNbPings = pSideView->GetLastAddedIndex() + 1;
			unsigned int nbPixelsPerPing = DisplayParameter::getInstance()->GetPixelsPerPing();

			xFact = nbPixelsPerPing;

			if (FlatPictureBox->ClientRectangle.Width < nbPixelsPerPing*realNbPings)
			{
				// image plaquée à droite
				xDelta = (double)(FlatPictureBox->Width) / nbPixelsPerPing + m_OffsetHistoric - realNbPings;
			}
		}

		//Pen^ blackPen = gcnew Pen(Color::Black, 1.00f);

		std::vector<std::uint64_t> pingIds;
		std::vector<unsigned int> xVect;

		std::vector<POINT> vectPoints;

		// Récuperation des paramètres de l'écho-integration supervisée
		const EISupervisedParameter & eiSupervisedParameter = pEISupervisedModule->GetEISupervisedParameter();
		ColorPaletteEIClass colorPaletteEIClass = DisplayParameter::getInstance()->GetColorPaletteEIClass();

		ESUContainer & esuContainer = M3DKernel::GetInstance()->getMovESUManager()->GetESUContainer();

		for (PolygonSet::iterator iterPoly = polygons.begin(); iterPoly != polygons.end(); iterPoly++)
		{
			int polygonEsu = -1;

			BaseMathLib::Poly2D * polygon = *iterPoly;
			List<PointF> tmpPolygon;
			PolygonToArray(polygon, tmpPolygon, minVisiblePing, maxVisiblePing);

			const int nbDrawPoints = tmpPolygon.Count;
			if (nbDrawPoints > 0)
			{
				array<PointF> ^ drawnPolygon = gcnew array<PointF>(nbDrawPoints);
				for (int i = 0; i != nbDrawPoints; i++)
				{
					PointF pt = tmpPolygon[i];
					pingIds.clear();
					xVect.clear();
					pingIds.push_back(pt.X);
					pSideView->GetScreenPixelsFromPingIDs(pingIds, xVect);

					drawnPolygon[i] = PointF((xVect[0] + xDelta) * xFact, (pt.Y + yDelta) * yFact);
				}

				// dessin de la selection
				bool drawSelection = false;
				Pen^ penSelection = m_polygonPen;
				if (m_eiSelectionManager->isSelected(m_eiSelectionManager->CurrentEsuId, polygon))
				{
					drawSelection = true;
				}
				else
				{
					// NMD - FAE 123 - couleur des éléments classifiés (Movies+ 210)
					std::uint32_t esu = pEISupervisedModule->GetEsuForPolygon(transName, polygon);
					EISupervisedResult * pEISupervisedEsuResult = pEISupervisedModule->GetCentralResultFromEsu(transName, esu);
					if (pEISupervisedEsuResult != NULL)
					{
						const int polygonClassId = pEISupervisedEsuResult->GetPolygonClassification(polygon);
						Pen ^classificationPen = ClassificationPen(polygonClassId);
						if (classificationPen != nullptr)
						{
							penSelection = classificationPen;
							drawSelection = true;
						}
					}
				}

				DrawPolygon(e, drawnPolygon, penSelection, drawSelection);
			}
		}
	}
}

System::Void BaseFlatSideView::OnTransducerChanged(String ^ transducerName)
{
	m_eiSelectionManager->clearSelection();
}

System::Void BaseFlatSideView::EnterEISupervisedSelectionMode()
{
	m_bEISupervisedSelectionMode = true;
	LeaveSelectionMode();
	LeaveZoomMode();
	LeavePolygonSelectionMode();
	LeaveBottomCorrectionMode();
}

System::Void BaseFlatSideView::LeaveEISupervisedSelectionMode()
{
	m_bEISupervisedSelectionMode = false;
	ClearSelection();
}

System::Void BaseFlatSideView::EnterPolygonSelectionMode(CorrectionManager *correctionMgr)
{
	m_bPolygonSelectionMode = true;
	m_correctionManager = correctionMgr;
	LeaveSelectionMode();
	LeaveZoomMode();
	LeaveEISupervisedSelectionMode();
	LeaveBottomCorrectionMode();
}

System::Void BaseFlatSideView::LeavePolygonSelectionMode()
{
	m_bPolygonSelectionMode = false;
	m_SelectionPolygon.Clear();
}

System::Void BaseFlatSideView::EnterBottomCorrectionMode(CorrectionManager *correctionMgr)
{
	m_bBottomCorrectionMode = true;
	m_correctionManager = correctionMgr;
	LeaveSelectionMode();
	LeaveZoomMode();
	LeaveEISupervisedSelectionMode();
	LeavePolygonSelectionMode();
}

System::Void BaseFlatSideView::LeaveBottomCorrectionMode()
{
	m_bBottomCorrectionMode = false;
	m_BottomCorrectionPolyLine.Clear();
}

System::Void BaseFlatSideView::CheckEISupervisedForCurrentTransducer()
{
	if (!IsEISupervisedForCurrentTransducer())
	{
		throw gcnew MoviesException("EI supervised not actived for selected transducer");
	}
}

bool BaseFlatSideView::IsEISupervisedForCurrentTransducer()
{
	bool result = true;
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	EISupervisedModule * pModule = pModuleMgr->GetEISupervisedModule();
	std::string transName = netStr2CppStr(GetTransducer());
	if (pModule->IsEISupervisedEnabled(transName) == false)
	{
		result = false;
	}
	return result;
}

System::Void BaseFlatSideView::FlatPictureBox_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	BaseFlatView::FlatPictureBox_MouseUp(sender, e);

	if (m_bIgnoreNextMouseUp)
	{
		m_bIgnoreNextMouseUp = false;
		return;
	}

	try
	{
		// mode definition de polygone
		if (m_bPolygonSelectionMode)
		{
			if (e->Button == System::Windows::Forms::MouseButtons::Left || e->Button == System::Windows::Forms::MouseButtons::Right)
			{
				CModuleManager * pModuleMgr = CModuleManager::getInstance();
				EISupervisedModule * pModule = pModuleMgr->GetEISupervisedModule();
				bool bIsSupervisedEIEnabled = IsEISupervisedForCurrentTransducer() && pModule->getEnable();

				// si on n'est pas en mode EI supervisée mais en mode suppression d'échos dans un polygone, il faut vérifier que :
				// - on n'est pas en mode acquisition
				// - on est en pause
				// - on est sur un monofaisceau
				if (!bIsSupervisedEIEnabled)
				{
					// Vérification du mode acquisition ou rejeu
					if (!ReaderCtrl::getInstance()->IsFileService())
					{
						return;
					}

					// Vérification du mode pause
					if (!ReaderCtrlManaged::Instance()->IsPause())
					{
						return;
					}

					// Vérification du sondeur monofaisceau
					Sounder * pSound = M3DKernel::GetInstance()->getObjectMgr()->GetSounderDefinition().GetSounderWithId(GetTransducerView()->GetSounderId());
					if (pSound->m_isMultiBeam)
					{
						return;
					}
				}


				if (e->Button == System::Windows::Forms::MouseButtons::Left
					|| e->Button == System::Windows::Forms::MouseButtons::Right && m_SelectionPolygon.Count >= 2)
				{
					//ajout d'un point au polygone
					Point pt(e->X, e->Y);
					if (pt.X < 0) pt.X = 0;
					if (pt.X > FlatPictureBox->Width) pt.X = FlatPictureBox->Width;
					if (pt.Y < 0) pt.Y = 0;
					if (pt.Y > FlatPictureBox->Height) pt.Y = FlatPictureBox->Height;
					m_SelectionPolygon.Add(pt);
				}

				if (e->Button == System::Windows::Forms::MouseButtons::Right && m_SelectionPolygon.Count >= 3)
				{
					// sauvegarde du polygon en cours de dessin current polygon
					// conversion coordonnée ecran vers coordonnées réelles
					BaseMathLib::Poly2D * polygon = new BaseMathLib::Poly2D();
					polygon->Alloc(m_SelectionPolygon.Count);

					double * points = new double[m_SelectionPolygon.Count * 2];
					int count = 0;

					for each(Point pt in m_SelectionPolygon)
					{
						PointF realPoint = MapScreenToRealCoord(pt);
						points[count] = realPoint.X;
						count++;
						points[count] = realPoint.Y;
						count++;
					}

					polygon->SetPoints(points, m_SelectionPolygon.Count);

					if (bIsSupervisedEIEnabled)
					{
						m_SelectionPolygon.Clear();
						this->FlatPictureBox->Invalidate();

						M3DKernel * pKernel = M3DKernel::GetInstance();
						KernelLocker kernelLocker;
						auto & esuContainer = pKernel->getMovESUManager()->GetESUContainer();
						
						for (int i = 1; i < polygon->GetNbPoints(); ++i)
						{
							ESUParameter * pEsu1 = esuContainer.GetESUWithPing(*polygon->GetPointX(i-1));
							ESUParameter * pEsu2 = esuContainer.GetESUWithPing(*polygon->GetPointX(i));
							if (pEsu1 == nullptr || pEsu2 == nullptr)
							{
								throw gcnew MoviesException("Invalid polygon : some points don't belong to an ESU...");
							}
							if (pEsu1->GetESUId() != pEsu2->GetESUId())
							{
								throw gcnew MoviesException("Invalid polygon : points don't belong to the same ESU...");
							}
						}
									
						std::string transName = netStr2CppStr(GetTransducer());

						// ajout du polygon 
						CModuleManager * pModuleMgr = CModuleManager::getInstance();
						EISupervisedModule * pModule = pModuleMgr->GetEISupervisedModule();
						pModule->AddPolygon(transName, polygon);

						// mise a jour de la fenetre des resultats
						OnEISupervisedResultChanged();
					}
					else
					{
						// Cas de la sélection d'un polygone pour suppression des échos correspondants
						Point menuLocation;
						menuLocation.X = e->X;
						menuLocation.Y = e->Y;
						m_PolygonDeleteMenuStrip->AutoSize = true;
						m_PolygonDeleteMenuStrip->Show(this, menuLocation, System::Windows::Forms::ToolStripDropDownDirection::BelowRight);
						m_polygonToDelete = polygon;
					}

				}

				this->FlatPictureBox->Invalidate();
			}
		}
		// mode attribution d'une classification à la selection courante
		else if (m_bEISupervisedSelectionMode && e->Button == System::Windows::Forms::MouseButtons::Right)
		{
			CheckEISupervisedForCurrentTransducer();

			CModuleManager * pModuleMgr = CModuleManager::getInstance();
			EISupervisedModule * pModule = pModuleMgr->GetEISupervisedModule();

			std::vector<int> layers;
			std::vector<BaseMathLib::Poly2D*> polygons;
			List<ISelection^> ^ currentSelection = m_eiSelectionManager->currentSelection();
			bool deletePolygonEnable = false;
			for each (ISelection ^selection in currentSelection)
			{
				PolygonSelection ^ polygonSelection = dynamic_cast<PolygonSelection^>(selection);
				if (polygonSelection)
				{
					deletePolygonEnable = true;
					polygons.push_back(polygonSelection->Polygon);
				}

				LayerSelection ^ layerSelection = dynamic_cast<LayerSelection^>(selection);
				if (layerSelection)
				{
					layers.push_back(layerSelection->LayerIndex);
				}
			}

			// recuperation des resultats de l'echo-integration supervisée pour l'esu
			std::string transName = netStr2CppStr(GetTransducer());
			EISupervisedParameter & params = pModule->GetEISupervisedParameter();
			EISupervisedResult * pResult = pModule->GetMeanResultFromEsu(transName, m_eiSelectionManager->CurrentEsuId,
				params.getClassifications(), layers, polygons, params.isUseVolumicWeightedAvg());
			if (pResult != NULL)
			{
				String ^ description = "";
				double selectionEnergy = 0;
				double remainingEnergy = 0;

				if (m_eiSelectionManager->CurrentEsuId != -1)
				{
					std::vector<std::string> classificationNames;
					std::vector<double> classificationEnergies;

					if (currentSelection->Count > 0)
					{
						if (currentSelection->Count == 1)
						{
							description = currentSelection[0]->Description;
						}
						else
						{
							description = System::String::Format("{0} élements", currentSelection->Count);
						}

						selectionEnergy = pResult->GetSa(layers, polygons);
						remainingEnergy = pResult->GetFreeSa(layers, polygons);

						for(const auto& classDef : params.getClassifications())
						{
							classificationNames.push_back(classDef.m_Name);
							classificationEnergies.push_back(pResult->GetClassifiedSa(layers, polygons, classDef.m_Id));
						}
					}

					deletePolygonEnable &= currentSelection->Count == 1;

					m_EISupervisedMenuStrip->InitMenu(remainingEnergy, classificationNames, classificationEnergies);
					m_EISupervisedMenuStrip->DeletePolygonVisible = deletePolygonEnable;
					m_EISupervisedMenuStrip->SelectionDescription = System::String::Format(description + " ({0:0.00})", selectionEnergy);

					// affichage du menu d'affectation de la classification
					Point menuLocation;
					menuLocation.X = e->X;
					menuLocation.Y = e->Y;
					m_EISupervisedMenuStrip->Show(this, menuLocation, System::Windows::Forms::ToolStripDropDownDirection::BelowRight);
				}
			}
		}

		// mode selection
		else if (m_bEISupervisedSelectionMode && e->Button == System::Windows::Forms::MouseButtons::Left)
		{
			CheckEISupervisedForCurrentTransducer();

			bool bAltKeyPressed = this->ModifierKeys.HasFlag(System::Windows::Forms::Keys::Alt);
			bool bCtrlKeyPressed = this->ModifierKeys.HasFlag(System::Windows::Forms::Keys::Control);
			bool bShiftKeyPressed = this->ModifierKeys.HasFlag(System::Windows::Forms::Keys::Shift);
			
			Point pt(e->X, e->Y);

			// transformation du point en coordonnées réelles
			PointF realPt = MapScreenToRealCoord(pt);
			double tabRealPt[2] = { realPt.X, realPt.Y };

			// Liste des objets sous la souris
			List<ISelection^> selectionList;

			M3DKernel * pKernel = M3DKernel::GetInstance();
			pKernel->Lock();

			// Récuperation de l'esu courante
			std::uint32_t esuId = -1;
			ESUParameter * pEsu = pKernel->getMovESUManager()->GetESUContainer().GetESUWithPing(realPt.X);
			if (pEsu) {
				esuId = pEsu->GetESUId();
			}

			// Recuperation des polygones déja définis
			CModuleManager * pModuleMgr = CModuleManager::getInstance();
			EISupervisedModule * pEISupervisedModule = pModuleMgr->GetEISupervisedModule();

			std::string transName = netStr2CppStr(GetTransducer());
			PolygonSet polygons = pEISupervisedModule->GetPolygons(transName);

			// verification des polygones    
			for (PolygonSet::iterator iterPoly = polygons.begin(); iterPoly != polygons.end(); iterPoly++)
			{
				BaseMathLib::Poly2D * polygon = *iterPoly;

				bool pointInside = BaseMathLib::GeometryTools2D::PointInPolygon(polygon->GetPoints(), polygon->GetNbPoints(), tabRealPt);
				if (pointInside)
				{
					PolygonSelection ^ polygonSelection = gcnew PolygonSelection(polygon, esuId);
					selectionList.Add(polygonSelection);
				}
			}

			// Si touche ALT enfoncé, on sélectionne en priorité les polygones
			if (bAltKeyPressed == false || selectionList.Count == 0)
			{
				// recuperation de la valeur du fond (pour les couches de fond et pour les bancs)
				PointF realBottomPoint;
				BaseMathLib::Vector2D realPolarCoord;
				bool success = GetRealPoint(realPt, realBottomPoint, realPolarCoord);

				// verification des layers
				TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
				EchoIntegrationParameter& params = CModuleManager::getInstance()->GetEchoIntegrationModule()->GetEchoIntegrationParameter();
				
				// recuperation de la valeur du fond
				double bottomValue;
				bool found;
				PingFan* pFan = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(realPt.X);
				pFan->m_computePingFan.getBottomMaxRangeMeter(bottomValue, found);
				bottomValue += ref->GetTransducer()->m_transDepthMeter;

				// TODO MUTUALISER LE COMPENSATE HEAVE
				double compensateHeave = 0;
				unsigned int transducerListIdx = GetCurrentIndex();
				unsigned int transducerSounderIdx = m_viewContainer->SideViewContainer->GetView(transducerListIdx)->GetTransducerIndex();
				Transducer *pTrans = pFan->getSounderRef()->GetTransducer(transducerSounderIdx);
				SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
				if (pSoftChan)
				{
					compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
				}
				double echoDistance = realPt.Y - compensateHeave - pTrans->m_transDepthMeter;

				bool isSurfaceLayerEnable = pEISupervisedModule->IsEISupervisedEnabled(transName, Layer::Type::SurfaceLayer);
				bool isBottomLayerEnable = pEISupervisedModule->IsEISupervisedEnabled(transName, Layer::Type::BottomLayer);
				bool isDistanceLayerEnable = pEISupervisedModule->IsEISupervisedEnabled(transName, Layer::Type::DistanceLayer);

				const auto& layers = params.getLayersDefs();
				const auto nbLayers = layers.size();

				for (size_t iLayer = 0; iLayer < nbLayers; ++iLayer)
				{
					const auto & layer = layers[iLayer];
					bool addLayerToSelection = false;
					switch (layer->GetLayerType())
					{
					case Layer::Type::SurfaceLayer:
						if (isSurfaceLayerEnable)
						{
							addLayerToSelection = (realPt.Y > layer->GetMinDepth() && realPt.Y < layer->GetMaxDepth());
						}
						break;

					case Layer::Type::BottomLayer:
						if (isBottomLayerEnable)
						{
							addLayerToSelection = (realPt.Y > layer->GetMinDepth(bottomValue) && realPt.Y < layer->GetMaxDepth(bottomValue));
						}
						break;

					case Layer::Type::DistanceLayer:
						if (isDistanceLayerEnable)
						{
							addLayerToSelection = (echoDistance > layer->GetMinDepth() && echoDistance < layer->GetMaxDepth());
						}
						break;
					}

					if (addLayerToSelection)
					{
						LayerSelection ^ selection = gcnew LayerSelection(esuId, layer->GetLayerType(), iLayer);
						selectionList.Add(selection);
					}
				}
			}

			pKernel->Unlock();

			EISelectionManager::eSelectionMode mode = EISelectionManager::eSelectionMode::ReplaceSelection;
			if (bCtrlKeyPressed)
			{
				mode = EISelectionManager::eSelectionMode::AddToCurrentSelection;
			}
			else if (bShiftKeyPressed)
			{
				mode = EISelectionManager::eSelectionMode::ExpandCurrentSelection;
			}

			if (selectionList.Count > 0)
			{
				if (selectionList.Count == 1)
				{
					// selection de l'élément
					m_eiSelectionManager->addToSelection(selectionList[0], mode);
					this->FlatPictureBox->Invalidate();
				}
				else
				{
					// ajout des elements au menu
					m_SelectionContextMenuStrip->SuspendLayout();
					m_SelectionContextMenuStrip->Items->Clear();
					for each(ISelection ^ selection in selectionList)
					{
						m_SelectionContextMenuStrip->Items->Add(gcnew SelectionToolStripItem(selection, mode));
					}

					m_SelectionContextMenuStrip->ResumeLayout();

					// affichage du menu de selection
					Point menuLocation;
					menuLocation.X = e->X;
					menuLocation.Y = e->Y;
					m_SelectionContextMenuStrip->AutoSize = true;
					m_SelectionContextMenuStrip->Show(this, menuLocation, System::Windows::Forms::ToolStripDropDownDirection::BelowRight);
				}
			}
			else if (selectionList.Count == 0)
			{
				ClearSelection();
			}
		}
		// Mode correction du fond (tracé d'une polyligne)
		else if (m_bBottomCorrectionMode)
		{
			if (e->Button == System::Windows::Forms::MouseButtons::Left)
			{
				// ajout d'un point à la polyligne si le point est sur un ping
				Point pt(e->X, e->Y);
				if (pt.X < 0) pt.X = 0;
				if (pt.X > FlatPictureBox->Width) pt.X = FlatPictureBox->Width;
				if (pt.Y < 0) pt.Y = 0;
				if (pt.Y > FlatPictureBox->Height) pt.Y = FlatPictureBox->Height;
				if (GetPingIdFromCursorPos((100.0*pt.X) / FlatPictureBox->Width, false) != -1)
				{
					m_BottomCorrectionPolyLine.Add(pt);
				}
			}
			else if (e->Button == System::Windows::Forms::MouseButtons::Right && m_BottomCorrectionPolyLine.Count > 0)
			{
				// Affichage du menu
				Point menuLocation;
				menuLocation.X = e->X;
				menuLocation.Y = e->Y;
				m_BottomCorrectionMenuStrip->AutoSize = true;
				m_BottomCorrectionMenuStrip->Show(this, menuLocation, System::Windows::Forms::ToolStripDropDownDirection::BelowRight);
			}
		}
	}
	catch (MoviesException^ ex)
	{
		MessageBox::Show(ex->Message);
	}
}

System::Void BaseFlatSideView::FlatPictureBox_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	BaseFlatView::FlatPictureBox_MouseMove(sender, e);

	// affichage du curseur
	if (m_bPolygonSelectionMode || m_bBottomCorrectionMode)
	{
		m_CurrentPoint.X = e->X;
		m_CurrentPoint.Y = e->Y;

		// pour dessin de la zone de selection
		this->FlatPictureBox->Invalidate();
	}
}

System::Void BaseFlatSideView::FlatPictureBox_MouseDoubleClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	// affichage du curseur
	if (m_bBottomCorrectionMode && e->Button == System::Windows::Forms::MouseButtons::Left
		&& m_BottomCorrectionPolyLine.Count <= 1)
	{
		if (m_BottomCorrectionPolyLine.Count == 1)
		{
			m_BottomCorrectionPolyLine.Clear();
		}

		int beamIndex, echoIndex;
		std::uint64_t pingId;
		if (GetPingEchoAndBeam(e->X, e->Y, beamIndex, echoIndex, pingId))
		{
			Transducer * pTrans = GetTransducerView()->GetTransducer();
			std::int32_t bottomRange = (std::int32_t)(1000.0 * pTrans->getBeamsSamplesSpacing() * (echoIndex + pTrans->GetSampleOffset()) + 0.5);
			PingFan *pFan = (PingFan *)M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingId);
			m_correctionManager->CorrectBottom(pFan, pTrans->GetChannelId()[beamIndex], bottomRange);

			// Pour rafraichissement des toutes les vues longitudinales concernées
			this->SounderChanged();
		}

		// Pour gérer la concurrence du double click avec le mouseup
		m_bIgnoreNextMouseUp = true;
	}
}

bool BaseFlatSideView::GetPingEchoAndBeam(PingFan * pFan, int mouseY, int & beamIndex, int & echoIndex)
{
	bool bFound = false;

	if (pFan)
	{
		// récupération du numéro d'écho et de channel pointé
		TransducerFlatView * ref = (TransducerFlatView *)GetTransducerView();
		TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(ref->GetTransducerIndex());
		if (pTransform != NULL)
		{
			// on passe la position de la souris sur l'image en % de la taille de l'image
			// +0.5 pour tenir compte de l'interpolation de l'image en nearestNeighboor
			double posY = ((double)mouseY + 0.5) / (GetRealDisplayHeight());

			BaseMathLib::Vector2I memSize = pTransform->getCartesianSize2();
			int pixelY = (int)floor((ref->GetStopPixel() - ref->GetStartPixel())*posY + ref->GetStartPixel());
			int pixelX = (unsigned int)floor(memSize.x*ref->GetActiveSlide() / 100.0);

			Transducer * pTrans = ref->GetTransducer();
			double compensateHeave = 0;
			SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
			if (pSoftChan)
			{
				compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
			}
			double prof = pTrans->m_transDepthMeter;
			int offset = (int)((prof + compensateHeave) / pTransform->getYSpacing()) + pTrans->GetSampleOffset();
			int echoDepth = pixelY - offset;

			int numFaisceau = *pTransform->getPointerToChannelIndex(pixelX, echoDepth);
			if (numFaisceau >= 0 && numFaisceau < (int)pTrans->m_numberOfSoftChannel)
			{
				BaseMathLib::Vector2D polarCoord = pTransform->cartesianToPolar2(BaseMathLib::Vector2D(pixelX, echoDepth));
				echoIndex = polarCoord.y;
				beamIndex = numFaisceau;
				bFound = true;
			}
		}
	}
	return bFound;
}

bool BaseFlatSideView::GetPingEchoAndBeam(int mouseX, int mouseY, int & beamIndex, int & echoIndex, std::uint64_t & pingId)
{
	bool bFound = false;

	// récupération du ping pointé
	double cursorCenterXpercent = (100.0*mouseX) / FlatPictureBox->Width;
	std::uint64_t pingNumber = GetPingIdFromCursorPos(cursorCenterXpercent, false);

	PingFan *pFan = (PingFan *)M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingNumber);
	if (pFan)
	{
		pingId = pFan->GetPingId();
		bFound = GetPingEchoAndBeam(pFan, mouseY, beamIndex, echoIndex);
	}
	return bFound;
}

void BaseFlatSideView::ClearSelection()
{
	m_eiSelectionManager->clearSelection();
	this->FlatPictureBox->Invalidate();
}

System::Void BaseFlatSideView::m_SelectionContextMenuStrip_ItemClicked(System::Object^  sender, System::Windows::Forms::ToolStripItemClickedEventArgs^ e)
{
	SelectionToolStripItem ^item = dynamic_cast<SelectionToolStripItem^>(e->ClickedItem);
	if (item != nullptr)
	{
		m_eiSelectionManager->addToSelection(item->Selection, item->SelectionMode);
		this->FlatPictureBox->Invalidate();
	}
}

void BaseFlatSideView::m_EISupervisedMenuStrip_DeletePolygonClicked(/*System::Object^  sender, System::EventArgs^ e*/)
{
	// suppression du polygon
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	EISupervisedModule * pModule = pModuleMgr->GetEISupervisedModule();
	for each(ISelection ^selection in m_eiSelectionManager->currentSelection())
	{
		PolygonSelection ^ polygonSelection = dynamic_cast<PolygonSelection ^>(selection);
		if (polygonSelection != nullptr)
		{
			std::string transName = netStr2CppStr(GetTransducer());
			pModule->DeletePolygon(transName, polygonSelection->Polygon);
		}
	}
	ClearSelection();

	// mise a jour de la fenetre des resultats
	OnEISupervisedResultChanged();

	m_EISupervisedMenuStrip->Hide();
}

void BaseFlatSideView::m_EISupervisedMenuStrip_AddEnergyClicked(int classIdx)
{

	if (m_eiSelectionManager->CurrentEsuId != -1)
	{
		EISupervisedModule * pModule = CModuleManager::getInstance()->GetEISupervisedModule();

		// recuperation de la classification associée par son nom
		const auto parameter = pModule->GetEISupervisedParameter();
		const auto classId = parameter.getClassification(classIdx).m_Id;

		std::string transName = netStr2CppStr(GetTransducer());
		std::vector<EISupervisedResult*> eiSupervisedResults = pModule->GetResultsFromEsu(transName, m_eiSelectionManager->CurrentEsuId);

		for each(ISelection ^selection in m_eiSelectionManager->currentSelection())
		{
			PolygonSelection ^polygonSelection = dynamic_cast<PolygonSelection ^> (selection);
			if (polygonSelection != nullptr)
			{
				for (size_t iRes = 0; iRes < eiSupervisedResults.size(); iRes++)
				{
					eiSupervisedResults[iRes]->SetClassificationForPolygon(polygonSelection->Polygon, classId);
				}
			}

			LayerSelection ^layerSelection = dynamic_cast<LayerSelection ^> (selection);
			if (layerSelection != nullptr)
			{
				for (size_t iRes = 0; iRes < eiSupervisedResults.size(); iRes++)
				{
					eiSupervisedResults[iRes]->SetClassificationForLayer(layerSelection->LayerIndex, classId);
				}
			}
		}

		// update result form
		OnEISupervisedResultChanged();
	}
}

void BaseFlatSideView::m_EISupervisedMenuStrip_SubstractEnergyClicked(int classIdx)
{
	if (m_eiSelectionManager->CurrentEsuId != -1)
	{
		EISupervisedModule * pModule = CModuleManager::getInstance()->GetEISupervisedModule();

		// recuperation de la classification associée par son nom
		EISupervisedParameter parameter = pModule->GetEISupervisedParameter();
		const auto classId = parameter.getClassification(classIdx).m_Id;

		std::string transName = netStr2CppStr(GetTransducer());
		std::vector<EISupervisedResult*> eiSupervisedResults = pModule->GetResultsFromEsu(transName, m_eiSelectionManager->CurrentEsuId);

		for each(ISelection ^selection in m_eiSelectionManager->currentSelection())
		{
			PolygonSelection ^polygonSelection = dynamic_cast<PolygonSelection ^> (selection);
			if (polygonSelection != nullptr)
			{
				for (size_t iRes = 0; iRes < eiSupervisedResults.size(); iRes++)
				{
					eiSupervisedResults[iRes]->ResetClassificationForPolygon(polygonSelection->Polygon, classId);
				}
			}

			LayerSelection ^layerSelection = dynamic_cast<LayerSelection ^> (selection);
			if (layerSelection != nullptr)
			{
				for (size_t iRes = 0; iRes < eiSupervisedResults.size(); iRes++)
				{
					eiSupervisedResults[iRes]->ResetClassificationForLayer(layerSelection->LayerIndex, classId);
				}
			}
		}

		// update result form
		OnEISupervisedResultChanged();
	}
}

void BaseFlatSideView::m_EISupervisedMenuStrip_ReclasssifyClicked(int classIdx)
{
	if (m_eiSelectionManager->CurrentEsuId != -1)
	{
		EISupervisedModule * pModule = CModuleManager::getInstance()->GetEISupervisedModule();

		// recuperation de la classification associée par son nom
		EISupervisedParameter parameter = pModule->GetEISupervisedParameter();
		const auto classId = parameter.getClassification(classIdx).m_Id;

		std::string transName = netStr2CppStr(GetTransducer());
		std::vector<EISupervisedResult*> eiSupervisedResults = pModule->GetResultsFromEsu(transName, m_eiSelectionManager->CurrentEsuId);

		for each(ISelection ^selection in m_eiSelectionManager->currentSelection())
		{
			PolygonSelection ^polygonSelection = dynamic_cast<PolygonSelection ^> (selection);
			if (polygonSelection != nullptr)
			{
				for (size_t iRes = 0; iRes < eiSupervisedResults.size(); iRes++)
				{
					eiSupervisedResults[iRes]->ReclassifyPolygon(polygonSelection->Polygon, classId);
				}
			}

			LayerSelection ^layerSelection = dynamic_cast<LayerSelection ^> (selection);
			if (layerSelection != nullptr)
			{
				for (size_t iRes = 0; iRes < eiSupervisedResults.size(); iRes++)
				{
					eiSupervisedResults[iRes]->ReclassifyLayer(layerSelection->LayerIndex, classId);
				}
			}
		}

		// update result form
		OnEISupervisedResultChanged();
	}
}

bool BaseFlatSideView::GetRealPoint(PointF realPingPoint, PointF & realPoint, BaseMathLib::Vector2D & realPolarCoord)
{
	bool success = false;

	//get the cartesian X and Y coord from the active slide

	//get the ping fan
	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	int ping = realPingPoint.X;
	PingFan *pFan = (PingFan *)pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(ping);

	//retrieve cartesian X
	unsigned int transducerListIdx = GetCurrentIndex();
	unsigned int transducerSounderIdx = m_viewContainer->SideViewContainer->GetView(transducerListIdx)->GetTransducerIndex();

	if (pFan != NULL)
	{
		if (transducerSounderIdx < pFan->GetMemorySetRef()->GetMemoryStructCount())
		{
			MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(transducerSounderIdx);
			TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transducerSounderIdx);
			Transducer *pTrans = pFan->getSounderRef()->GetTransducer(transducerSounderIdx);
			double m_compensateHeave = 0;
			SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
			if (pSoftChan)
			{
				m_compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
			}
			BaseMathLib::Vector2I memSize = pTransform->getCartesianSize2();
			double activeSlide = this->m_viewContainer->SideViewContainer->GetActiveSlide(transducerListIdx);
			int xCartesian = floor(memSize.x * activeSlide / 100.0);

			//convert cartesianX to realX
			realPoint.X = pTransform->cartesianToRealX(xCartesian);
			//substract heave to depth to get real Depth
			realPoint.Y = realPingPoint.Y - m_compensateHeave - pTrans->m_transDepthMeter;

			if (realPoint.Y >= 0)
			{
				BaseMathLib::Vector2D realCoord(realPoint.X, realPoint.Y);
				realPolarCoord = pTransform->realToPolar2(realCoord);

				if (realPolarCoord.x >= 0 && realPolarCoord.y >= 0)
				{
					//notify observer
					//arrondi de la coordonnée polaire x pour obtenir le faisceau exact 
					//(sans compensation de l'angle dans le faisceau)
					realPolarCoord.x = (int)(realPolarCoord.x + 0.5);

					success = true;
				}
			}
		}
	}

	pKernel->Unlock();

	return success;
}


void BaseFlatSideView::SaveEIView(TransducerFlatView * refView, String ^transducer, std::uint32_t esuId, String ^outPath)
{
	M3DKernel * pKernel = M3DKernel::GetInstance();
	pKernel->Lock();

	std::string transName = netStr2CppStr(transducer);

	std::vector<Sounder*> sounders = pKernel->getObjectMgr()->GetSounderDefinition().GetSoundersWithId(refView->GetSounderId());
	assert(sounders.size() == 1);
	TransformMap *pTransform = sounders[0]->GetTransformSetRef()->GetTransformMap(refView->GetTransducerIndex());

	ESUParameter * esu = pKernel->getMovESUManager()->GetESUContainer().GetESUById(esuId);
	if (esu != NULL)
	{
		CModuleManager * pModuleMgr = CModuleManager::getInstance();

		EISupervisedModule * pEISupervisedModule = pModuleMgr->GetEISupervisedModule();

		const EISupervisedParameter & eiSupervisedParameter = pEISupervisedModule->GetEISupervisedParameter();
		ColorPaletteEIClass colorPaletteEIClass = DisplayParameter::getInstance()->GetColorPaletteEIClass();

		std::uint64_t pingstart = esu->GetESUPingNumber();
		std::uint64_t pingend = esu->GetESULastPingNumber();

		TransducerImageFlatView view(
			refView->GetSounderId(), refView->GetTransducerIndex()
			, pingstart, pingend);

		bool isValid = view.ComputeImageData();
		if (isValid)
		{
			int y = 0;
			int h = view.GetHeight();

			int x1 = view.GetStartHPixel();
			int x2 = view.GetStopHPixel();
			int w = x2 - x1;

			Bitmap ^ bmp = gcnew Bitmap(w, h);
			Graphics ^g = Graphics::FromImage(bmp);

			// *********************************************************
			// Dessin des pixels de l'image
			// *********************************************************
			g->DrawImage(view.GetImage(), 0, 0, Rectangle(0, 0, w, h), GraphicsUnit::Pixel);

			// *********************************************************
			// Dessin du fond
			// *********************************************************
			for (int i = x1; i < x2; i++)
			{
				int bottom = view.GetBottomValue(i);
				if (bottom != -1)
				{
					bmp->SetPixel(i - x1, bottom, Color::Black);
				}
			}

			EISupervisedResult * pEISupervisedEsuResult = pEISupervisedModule->GetCentralResultFromEsu(transName, esuId);


			// *********************************************************
			// Couches de surface
			// *********************************************************
			EchoIntegrationParameter& eiParams = CModuleManager::getInstance()->GetEchoIntegrationModule()->GetEchoIntegrationParameter();

			bool bFisrtSurfaceLayer = true;
			bool bFirstBottomLayer = true;
			bool bFirstDistanceLayer = true;

			Pen^ surfaceLayerPen = gcnew Pen(Color::LightGreen, 1.0f);
			Pen^ bottomLayerPen = gcnew Pen(Color::Purple, 1.0f);
			Pen^ distanceLayerPen = gcnew Pen(Color::Orange, 1.0f);


			const auto& layers = eiParams.getLayersDefs();
			const auto nbLayers = layers.size();

			bool isSurfaceLayerEnable = pEISupervisedModule->IsEISupervisedEnabled(transName, Layer::Type::SurfaceLayer);
			bool isBottomLayerEnable = pEISupervisedModule->IsEISupervisedEnabled(transName, Layer::Type::BottomLayer);
			bool isDistanceLayerEnable = pEISupervisedModule->IsEISupervisedEnabled(transName, Layer::Type::DistanceLayer);

			for (size_t iLayer = 0; iLayer < nbLayers; ++iLayer)
			{
				auto & layer = layers[iLayer];
				
				// dessin de la selection
				bool drawSelection = false;
				Pen^ layerPen = nullptr;

				if (pEISupervisedEsuResult != NULL)
				{
					const int layerClassId = pEISupervisedEsuResult->GetLayerClassification(iLayer);
					Pen ^classificationPen = ClassificationPen(layerClassId);
					if (classificationPen != nullptr)
					{
						layerPen = classificationPen;
						drawSelection = true;
					}
				}
				
				switch (layer->GetLayerType())
				{
				case Layer::Type::SurfaceLayer:
					if(isSurfaceLayerEnable)
					{
						BaseMathLib::Vector2I v1 = pTransform->realToCartesian2(BaseMathLib::Vector2D(0, layer->GetMinDepth()));
						BaseMathLib::Vector2I v2 = pTransform->realToCartesian2(BaseMathLib::Vector2D(0, layer->GetMaxDepth()));
						if (bFisrtSurfaceLayer)
						{
							if (v1.y >= 0 && v1.y < h)
							{
								g->DrawLine(surfaceLayerPen, 0, v1.y, w, v1.y);
							}
						}
						if (v2.y >= 0 && v2.y < h)
						{
							g->DrawLine(surfaceLayerPen, 0, v2.y, w, v2.y);
						}

						// affichage de la zone en hachure
						if (drawSelection)
						{
							int y1 = std::max(0, std::min(h, v1.y));
							int y2 = std::max(0, std::min(h, v2.y));
							for (int x = 1; x < w; x += 2)
							{
								g->DrawLine(layerPen, x, y1, x, y2);
							}
						}
						bFisrtSurfaceLayer = false;
					}
					break;

				case Layer::Type::BottomLayer:
					if (isBottomLayerEnable)
					{
						for (int i = x1; i < x2; i++)
						{
							int bottom = view.GetBottomValue(i);
							if (bottom != -1)
							{
								double bottomValue = pTransform->cartesianToRealY(bottom);
								BaseMathLib::Vector2I v1 = pTransform->realToCartesian2(BaseMathLib::Vector2D(0, bottomValue + layer->GetMaxDepth()));
								BaseMathLib::Vector2I v2 = pTransform->realToCartesian2(BaseMathLib::Vector2D(0, bottomValue + layer->GetMinDepth()));

								if (bFirstBottomLayer)
								{
									if (v1.y >= 0 && v1.y < h)
									{
										bmp->SetPixel(i - x1, v1.y, bottomLayerPen->Color);
									}
								}

								if (v2.y >= 0 && v2.y < h)
								{
									bmp->SetPixel(i - x1, v2.y, bottomLayerPen->Color);
								}

								// Affichage de la zone de hachure
								if (drawSelection && i % 2 == 0)
								{
									int y1 = std::max(0, std::min(h, v1.y));
									int y2 = std::max(0, std::min(h, v2.y));
									g->DrawLine(layerPen, i - x1, y1, i - x1, y2);
								}
							}
						}
						bFirstBottomLayer = false;
					}
					break;

				case Layer::Type::DistanceLayer:
					if (isDistanceLayerEnable)
					{
						for (int i = x1; i < x2; i++)
						{
							int surface = view.GetSurfaceValue(i);
							if (surface != -1)
							{
								double surfaceValue = pTransform->cartesianToRealY(surface);
								BaseMathLib::Vector2I v1 = pTransform->realToCartesian2(BaseMathLib::Vector2D(0, surfaceValue + layer->GetMinDepth()));
								BaseMathLib::Vector2I v2 = pTransform->realToCartesian2(BaseMathLib::Vector2D(0, surfaceValue + layer->GetMaxDepth()));

								if (bFirstDistanceLayer)
								{
									if (v1.y >= 0 && v1.y < h)
									{
										bmp->SetPixel(i - x1, v1.y, distanceLayerPen->Color);
									}
								}

								if (v2.y >= 0 && v2.y < h)
								{
									bmp->SetPixel(i - x1, v2.y, distanceLayerPen->Color);
								}

								// Affichage de la zone de hachure
								if (drawSelection && i % 2 == 0)
								{
									int y1 = std::max(0, std::min(h, v1.y - 1));
									int y2 = std::max(0, std::min(h, v2.y + 1));
									g->DrawLine(layerPen, i - x1, y1, i - x1, y2);
								}
							}
						}
						bFirstDistanceLayer = false;
					}
					break;
				}
			}
			
			// *********************************************************
			// Polygones
			// *********************************************************

			Pen^ blackPen = gcnew Pen(Color::Black, 1.00f);
			PolygonSet polygons = pEISupervisedModule->GetPolygons(transName);
			for (PolygonSet::iterator iterPoly = polygons.begin(); iterPoly != polygons.end(); iterPoly++)
			{
				BaseMathLib::Poly2D * polygon = *iterPoly;
				List<PointF> tmpPolygon;
				PolygonToArray(polygon, tmpPolygon, pingstart, pingend);

				const int nbDrawPoints = tmpPolygon.Count;
				if (nbDrawPoints > 0)
				{
					array<PointF> ^ drawnPolygon = gcnew array<PointF>(nbDrawPoints);
					for (int i = 0; i != nbDrawPoints; i++)
					{
						PointF pt = tmpPolygon[i];

						unsigned int pixel = view.GetPixelFromPingId(pt.X);
						BaseMathLib::Vector2I v = pTransform->realToCartesian2(BaseMathLib::Vector2D(0, pt.Y));

						drawnPolygon[i] = PointF(pixel - x1, v.y);
					}

					// dessin de la selection
					bool drawSelection = false;
					Pen^ penSelection = blackPen;

					// Vérification si le polygone est classifié
					std::uint32_t polygonEsu = pEISupervisedModule->GetEsuForPolygon(transName, polygon);
					if (pEISupervisedEsuResult != NULL && polygonEsu == esuId)
					{
						const int polygonClassId = pEISupervisedEsuResult->GetPolygonClassification(polygon);
						Pen ^classificationPen = ClassificationPen(polygonClassId);
						if (classificationPen != nullptr)
						{
							penSelection = classificationPen;
							drawSelection = true;
						}
					}
					DrawPolygon(g, drawnPolygon, penSelection, drawSelection);
				}
			}

			try
			{
				bmp->Save(outPath + ".png", Imaging::ImageFormat::Png);
			}
			catch (System::Runtime::InteropServices::ExternalException ^ )
			{
				M3D_LOG_ERROR(LoggerName, "An external erreur occured while saving the graphical result of the EI supervised : please check that the destination folder exists and is writable.");
			}
			catch (...)
			{
				M3D_LOG_ERROR(LoggerName, "An unknown erreur occured while saving the graphical result of the EI supervised.");
			}
		}
	}

	pKernel->Unlock();
}

void BaseFlatSideView::DrawPolygon(Graphics^ graphics, array<PointF> ^ polygon, Pen^ pen, bool hatched)
{
	if (hatched)
	{
		Brush ^ brush = gcnew Drawing2D::HatchBrush(
			Drawing2D::HatchStyle::LightUpwardDiagonal,
			pen->Color, Color::FromArgb(0, 0, 0, 0));
		graphics->FillPolygon(brush, polygon);
	}
	graphics->DrawPolygon(pen, polygon);
}

// OTK - FAE214 - dessin des single targets
struct singleTargetPoint {
	double screenY;
	double tsValue;
	std::uint64_t trackId;
};

System::Void BaseFlatSideView::DrawSingleTargets(Graphics^  e)
{
	TransducerFlatView * pSideView = (TransducerFlatView *)GetTransducerView();
	if (pSideView != NULL && !pSideView->isColoredEchogram())
	{
		M3DKernel *pKernel = M3DKernel::GetInstance();
		pKernel->Lock();
		
		auto displayParameters = DisplayParameter::getInstance();
		auto palette = displayParameters->GetCurrent2DEchoPalette();
		TSDisplayType tsDisplayType = displayParameters->GetTSDisplayType();
		unsigned int transducerSounderIdx = m_viewContainer->FrontViewContainer->GetView(GetCurrentIndex())->GetTransducerIndex();
		std::string transName = netStr2CppStr(GetTransducer());
		std::uint64_t minVisiblePing = GetPingIdFromCursorPos(0.0f, true);
		std::uint64_t maxVisiblePing = GetPingIdFromCursorPos(100.0f, true);

		if (minVisiblePing == -1 || maxVisiblePing == -1)
		{
			pKernel->Unlock();
			return;
		}

		float echoSize = -1;
		float trackLineWidth = 1;

		// Echos simples a dessiner, par numéro de ping dans l'image (avec numéro de pixel vertical et valeur de TS compensé)
		std::map<unsigned int, std::vector<singleTargetPoint> > mapSingleTargets;

		unsigned int pingImageIndex = -1;
		for (std::uint64_t iPing = minVisiblePing; iPing <= maxVisiblePing; iPing++)
		{
			PingFan * pFan = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(iPing);
			if (pFan)
			{
				Transducer * pTrans = pFan->getSounderRef()->GetTransducerWithName(transName.c_str());
				if (pTrans)
				{
					if (echoSize == -1)
					{
						double realToScreenRatio = FlatPictureBox->Height / ((pSideView->GetStopPixelDepth() - pSideView->GetStartPixelDepth())*GetVerticalStretchRatio());
						echoSize = pTrans->getBeamsSamplesSpacing() * realToScreenRatio;
						trackLineWidth = (pTrans->getBeamsSamplesSpacing()*pTrans->m_pulseDuration / pTrans->m_timeSampleInterval) * realToScreenRatio;
					}
					pingImageIndex++;
					PingFanSingleTarget *pSingle = (PingFanSingleTarget *)pKernel->getObjectMgr()->GetPingFanContainer().GetSingleTargetContainer()->GetObjectWithDate(pFan->m_ObjectTime);
					if (pSingle)
					{
						if (transducerSounderIdx < pFan->GetMemorySetRef()->GetMemoryStructCount())
						{
							TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transducerSounderIdx);
							if (pTransform)
							{
								BaseMathLib::Vector2I memSize = pTransform->getCartesianSize2();
								int transMapX = (unsigned int)floor(memSize.x*pSideView->GetActiveSlide() / 100.0);
								for (unsigned int i = 0; i < pSingle->GetNumberOfTarget(); i++)
								{
									SingleTargetDataObject *pSingleTargetData = pSingle->GetTarget(i);
									SplitBeamPair ref;
									bool found = pKernel->getObjectMgr()->GetChannelParentSplitBeam(pSingleTargetData->m_parentSTId, ref);
									if (found)
									{
										SoftChannel * pSoftChan = pTrans->getSoftChannel(pSingleTargetData->m_parentSTId);
										if (pSoftChan)
										{
											double m_compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
											for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataCount(); numTarget++)
											{
												const SingleTargetData * singleTarget = &(pSingleTargetData->GetSingleTargetData(numTarget));
												BaseMathLib::Vector3D mSoftCoord(0, 0, singleTarget->m_targetRange);
												BaseMathLib::Vector3D fanCoords = pSoftChan->GetMatrixSoftChan() * mSoftCoord;
												fanCoords.z += pTrans->m_transDepthMeter + m_compensateHeave;

												int offset = (int)floor((pTrans->m_transDepthMeter + m_compensateHeave) / pTransform->getYSpacing()) + pTrans->GetSampleOffset();
												int echoDepth = (int)(round(fanCoords.z / pTransform->getYSpacing()) - offset);

												if (transMapX >= 0 && transMapX < memSize.x && echoDepth >= 0 && echoDepth < memSize.y)
												{
													int numFaisceau = *pTransform->getPointerToChannelIndex(transMapX, echoDepth);
													if (numFaisceau >= 0 && numFaisceau < (int)pTrans->m_numberOfSoftChannel && pTrans->getSoftChannelPolarX(numFaisceau) == pSoftChan)
													{
														if (fanCoords.z >= DisplayParameter::getInstance()->GetCurrentMinDepth() 
															&& fanCoords.z <= DisplayParameter::getInstance()->GetCurrentMaxDepth())
														{
															PointF echoPos;
															echoPos.Y = fanCoords.z;
															PointF screenPos = MapRealToScreenCoord(echoPos);

															singleTargetPoint ptToDraw;
															ptToDraw.screenY = screenPos.Y;
															double singleEchoValue;
															if (numTarget < pSingleTargetData->GetSingleTargetDataCWCount())
															{
																const SingleTargetDataCW * cwTS = static_cast<const SingleTargetDataCW*>(singleTarget);
																singleEchoValue = cwTS->m_compensatedTS;
															}
															else
															{
																const SingleTargetDataFM * fmTS = static_cast<const SingleTargetDataFM*>(singleTarget);
																singleEchoValue = fmTS->m_medianTS;
															}

															ptToDraw.tsValue = singleEchoValue;
															ptToDraw.trackId = singleTarget->m_trackLabel;

															// On note le depth en pixels, le compTS, et le numéro de ping dans l'image !
															mapSingleTargets[pingImageIndex].push_back(ptToDraw);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		// On utilise un diametre min de 5 pixels pour bien voir les échos
		echoSize = std::max<float>(echoSize, 5.0);
		// et au moins un pixel de large pour la track
		trackLineWidth = std::max<float>(trackLineWidth, 1.0);

		TSAnalysisModule * pTSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
		const std::map<std::uint64_t, TSTrack*> & mapTracks = pTSModule->getTracks();
		int nbPings = pSideView->GetWidth();
		std::map<std::uint64_t, unsigned int> mapLastTrackColor;
		std::map<std::uint64_t, std::pair<double, double> > mapTracksLastPoint;
		std::map<unsigned int, std::vector<singleTargetPoint> >::const_iterator iter;
		for (iter = mapSingleTargets.begin(); iter != mapSingleTargets.end(); ++iter)
		{
			double xScreen;
			if (Zoomed)
			{
				xScreen = (int)round(FlatPictureBox->ClientSize.Width*((double)iter->first + 0.5) / (double)nbPings);
			}
			else
			{
				xScreen = ((double)iter->first + 0.5)*DisplayParameter::getInstance()->GetPixelsPerPing();
			}

			for (size_t iY = 0; iY < iter->second.size(); iY++)
			{
				const singleTargetPoint & tsPoint = iter->second[iY];

				bool bIsTrack = false;
				if (tsPoint.trackId > 0)
				{
					std::map<std::uint64_t, TSTrack*>::const_iterator iterTrack = mapTracks.find(tsPoint.trackId);
					if (iterTrack != mapTracks.end() && pTSModule->IsValidTrack(iterTrack->second))
					{
						bIsTrack = true;
					}
				}

				if (tsDisplayType == eTSDisplayAll
					|| (tsDisplayType == eTSDisplayTracked && bIsTrack)
					|| (tsDisplayType == eTSDisplayUntracked && !bIsTrack))
				{
					if (xScreen > 0 && tsPoint.screenY > 0 && xScreen < FlatPictureBox->Width && tsPoint.screenY < FlatPictureBox->Height)
					{
						unsigned int color = palette->GetColor(tsPoint.tsValue * 100);
						System::Drawing::Brush ^brush = gcnew System::Drawing::SolidBrush(System::Drawing::Color::FromArgb(color));

						// Dessin du segment de track éventuel
						std::map<std::uint64_t, std::pair<double, double> >::const_iterator iterLastPt = mapTracksLastPoint.find(tsPoint.trackId);
						if (iterLastPt != mapTracksLastPoint.end())
						{
							System::Drawing::Pen ^trackPen = gcnew System::Drawing::Pen(System::Drawing::Color::FromArgb(color), 1);
							e->DrawLine(trackPen, (float)iterLastPt->second.first, iterLastPt->second.second - trackLineWidth / 2.f, xScreen, tsPoint.screenY - trackLineWidth / 2.f);
							e->DrawLine(trackPen, (float)iterLastPt->second.first, iterLastPt->second.second + trackLineWidth / 2.f, xScreen, tsPoint.screenY + trackLineWidth / 2.f);
						}

						// Dessin du point
						e->FillEllipse(brush, xScreen - echoSize / 2.0, tsPoint.screenY - echoSize / 2.0, echoSize, echoSize);

						// MAJ du dernier point de la track le cas échéant pour préparer le dessin du prochain segment
						if (bIsTrack)
						{
							// Dessin de la ligne verticale d'ouverture de la track pour la première cible simple affichée pour cette track
							if (mapTracksLastPoint.find(tsPoint.trackId) == mapTracksLastPoint.end())
							{
								System::Drawing::Pen ^trackPen = gcnew System::Drawing::Pen(System::Drawing::Color::FromArgb(color), 1);
								e->DrawLine(trackPen, (float)xScreen, tsPoint.screenY + trackLineWidth / 2.f, xScreen, tsPoint.screenY - trackLineWidth / 2.f);
							}
							mapTracksLastPoint[tsPoint.trackId] = std::make_pair(xScreen, tsPoint.screenY);
						}
						mapLastTrackColor[tsPoint.trackId] = color;
					}
				}
			}
		}

		// dessin des identifiants de tracks pour les derniers points qui restent et pour lesquels
		// on a au moins deux points
		std::map<std::uint64_t, std::pair<double, double> >::const_iterator iterDisplayedTracks;
		System::Drawing::Brush ^brush = gcnew System::Drawing::SolidBrush(System::Drawing::Color::Black);
		for (iterDisplayedTracks = mapTracksLastPoint.begin(); iterDisplayedTracks != mapTracksLastPoint.end(); ++iterDisplayedTracks)
		{
			std::map<std::uint64_t, TSTrack*>::const_iterator iterTrack = mapTracks.find(iterDisplayedTracks->first);
			// On n'affiche que les tracks retenues
			if (iterTrack != mapTracks.end() && pTSModule->IsValidTrack(iterTrack->second))
			{
				// Dessin de la fermeture de la track
				unsigned int color = mapLastTrackColor[iterTrack->first];
				System::Drawing::Pen ^trackPen = gcnew System::Drawing::Pen(System::Drawing::Color::FromArgb(color), 1);
				e->DrawLine(trackPen, (float)iterDisplayedTracks->second.first, iterDisplayedTracks->second.second + trackLineWidth / 2.f,
					iterDisplayedTracks->second.first, iterDisplayedTracks->second.second - trackLineWidth / 2.f);


				// Dessin du label avec un rectangle blanc pour bien voir avec la palette noir et blanc
				String^ tracklabel = Convert::ToString(iterDisplayedTracks->first);
				SizeF^ size = e->MeasureString(tracklabel, m_fontForShoal);

				e->FillRectangle(Brushes::White, iterDisplayedTracks->second.first + echoSize / 2.0, iterDisplayedTracks->second.second + echoSize / 2.0, size->Width, size->Height);
				e->DrawString(tracklabel, m_fontForShoal, brush,
					iterDisplayedTracks->second.first + echoSize / 2.0, iterDisplayedTracks->second.second + echoSize / 2.0);
			}
		}

		pKernel->Unlock();
	}
}

int BaseFlatSideView::getCurrentPingOffset()
{
	return m_OffsetCursor;
}