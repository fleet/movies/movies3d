#pragma once
#include "BaseFlatView.h"
#include "BaseVolumicView.h"
#include "Reader/ManualCorrection/CorrectionManager.h"

#include "BaseMathLib/geometry/Poly2D.h"

#include "EISupervisedToolStripMenu.h"
//#include "EISupervisedSelection.h"

//class shoalextraction::ShoalData;

namespace MOVIESVIEWCPP {

	ref class EISelectionManager;

	public ref class ASyncDelayChangedStatus
	{
	public:
		ASyncDelayChangedStatus(std::uint32_t readerCount, std::uint32_t extractionCount, std::uint32_t maxDelay)
		{
			ReaderCount = readerCount;
			ExtractionCount = extractionCount;
			MaxDelay = maxDelay;
		};

		std::uint32_t ReaderCount, ExtractionCount, MaxDelay;
	};

	public ref class BaseFlatSideView : public BaseFlatView
	{
	public:
		BaseFlatSideView();

		virtual ~BaseFlatSideView();

		property bool Zoomed { bool get();  }


	protected: virtual System::Void DrawOnGraphics(Graphics^ e) override;
	protected: virtual System::Void DrawBottom(Graphics^  e) override;
	protected: virtual System::Void DrawRefNoise(Graphics^  e) override;
	protected: virtual System::Void DrawNoise(Graphics^  e) override;
	protected: virtual System::Void DrawNoisePoints(Graphics ^ e, int * echoesLine, Pen ^pen, Brush^brush);
	protected: virtual System::Void DrawContourLines(Graphics^  e) override;
	protected: virtual System::Void DrawDeviation(Graphics^  e) override;
	protected: virtual System::Void DrawDeviationLine(Graphics^  e, const int * deviationsLine);
	protected: virtual System::Void DrawEchoLine(Graphics^  e, const int * echoesLine);
	protected: virtual System::Void DrawCursor(Graphics^  e) override;
	protected: virtual System::Void DrawShoalId(Graphics^  e) override;
	protected: virtual System::Void DrawDistanceGrid(Graphics^  e) override;
	protected: virtual System::Void DrawESUGrid(Graphics^  e) override;
	protected: virtual System::Void DrawLayersContour(Graphics^  e) override;
	protected: virtual System::Void DrawImage(Graphics^  e) override;
	protected: virtual System::Void DrawSelectionWindow(Graphics^  e) override;
	protected: virtual System::Void DrawSelectionPolygon(Graphics^  e);
	protected: virtual System::Void DrawBottomCorrectionPolyline(Graphics^  e);
	protected: virtual System::Void DrawSingleTargets(Graphics^  e) override;
	protected: virtual System::Void HistoricHScrollBar_Scroll(System::Object^ sender, System::Windows::Forms::ScrollEventArgs^ e) override;
	protected: virtual TransducerContainerView ^GetFlatView() override
	{
		return m_viewContainer->SideViewContainer;
	}
	protected: virtual System::Void CursorHasChanged(double value) override;
	protected: virtual System::Void UpdateTransformTable() override;
	protected: virtual System::Void UpdateTransformTable(int index) override;
	protected: virtual int GetDataWidth() override;
	protected: virtual System::String^ FormatEchoInformation(int ScreenPosX, int ScreenPosY) override;
	protected: virtual System::Void OnZoom() override;
	protected: virtual System::Void ZoomOut() override;
	public: int m_OffsetCursor;
	public: int m_OffsetHistoric;
	public: int m_OffsetHistoricReference;
	public: virtual System::Void PingFanAdded() override;

	public: virtual void UpdatePensAndBrushes() override;

			 // dessin du contour des couches de surface et de fond
	private: System::Void DrawLayersContour(Graphics^  e, TransducerFlatView* ref, std::uint32_t currentEsuId, std::uint64_t pingStart, std::uint64_t pingEnd);


	private: System::Void DrawSurfaceLayer(Graphics^ e
		, int x1Esu, int x2Esu
		, const double & layerMinDepth, const double & layerMaxDepth
		, Pen ^ layerPen
		, bool drawSelection, Pen^ selectionPen);
			 
	private: System::Void DrawOffsetLayer(Graphics^ e, bool isBottomLayer
		, int x1Esu, int x2Esu
		, const double & layerMinDepth, const double & layerMaxDepth
		, Pen ^ layerPen
		, bool drawSelection, Pen^ selectionPen);
			
			 /*
	private: System::Void DrawBottomLayers(Graphics^ e, TransducerFlatView* ref, std::uint32_t currentEsuId, int x1, int x2, int bottom, int x1Esu, int x2Esu);

	private: System::Void DrawDistanceLayers(Graphics^ e, TransducerFlatView* ref, std::uint32_t currentEsuId, int x1, int x2, int offset, int x1Esu, int x2Esu);
	*/

			 // selection d'une region
	protected: virtual System::Void  OnRegionSelection() override;
			   // selection d'un point
	protected: virtual System::Void  OnShoalSelection(PointF pt) override;

			   //mapping d'un point en coord ecran vers coord r�elles
	protected: virtual PointF MapScreenToRealCoord(PointF screenCoord) override;
	protected: std::uint64_t GetPingIdFromCursorPos(double cursorCenterXpercent, bool bGetlastPingIfTooFar);

			   // mise � jour du retard de l'extraction de banc sur la vue de profil
	public: void UpdateShoalExtractionDelayVisible(bool bVisible);
	public: void UpdateShoalExtractionDelay(std::uint32_t readerCount,
		std::uint32_t extractionCount, std::uint32_t maxDelay);
	public: void ShowDelayInfo() { mShowDelayInfo = true; };
	public: void HideDelayInfo() { mShowDelayInfo = false; progressBar->Visible = false; labelProgress->Visible = false; };
	private: delegate void DelayChangedDelegate(ASyncDelayChangedStatus^ status);
	private: System::Void DelayChanged(ASyncDelayChangedStatus^ status);

	private: System::Void buttonHistoric_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	public: virtual System::Void UpdateHistoricSCrollBar() override;
	protected: virtual System::Void FlatPictureBox_Resize(System::Object^  sender, System::EventArgs^  e) override;

	public: delegate void OffsetHistoricChangeHandler(int offset);
	public: OffsetHistoricChangeHandler ^ OffsetHistoricChange;

	public: delegate void DisplayedPingOffsetChangeHandler(int sounderId, int offset);
	public: DisplayedPingOffsetChangeHandler ^ DisplayedPingOffsetChanged;

	public: void SetOffsetHistoricReference(int offsetReference);

	public: void SetEchotypesVisibility(bool bVisible);

	public: int getCurrentPingOffset() override;

	public:
		System::Void OnTransducerChanged(String ^ transducerName);
		System::Void EnterEISupervisedSelectionMode();
		System::Void LeaveEISupervisedSelectionMode();
		System::Void EnterPolygonSelectionMode(CorrectionManager *correctionMgr);
		System::Void LeavePolygonSelectionMode();
		System::Void EnterBottomCorrectionMode(CorrectionManager *correctionMgr);
		System::Void LeaveBottomCorrectionMode();

	protected: System::Void FlatPictureBox_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) override;
	protected: System::Void FlatPictureBox_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) override;
	protected: System::Void FlatPictureBox_MouseDoubleClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);

	private:
		std::uint32_t mReaderCount;
		std::uint32_t mMaxDelay;
		bool mShowDelayInfo;

		// Mode de correction manuelle du fond
		bool m_bBottomCorrectionMode;

		// Mode selection d'objet de l'echo integration supervis�e 
		bool m_bEISupervisedSelectionMode;

		// Mode dessin de polygone
		bool m_bPolygonSelectionMode;

		// Mode affichage des echotypes
		bool mShowEchoTypes;

		// Sauvegarde de la position courante de la souris pour dessiner le polygone en cours
		System::Drawing::Point m_CurrentPoint;

		// Polygone en cours de dessin
		System::Collections::Generic::List<Point> m_SelectionPolygon;

		// V�rifie si l'EI supervis�e est activ�e pour le transducteur courant, si non l�ve une exception
		System::Void CheckEISupervisedForCurrentTransducer();

		// V�rifie si l'EI supervis�e est activ�e pour le transducteur courant ou non
		bool IsEISupervisedForCurrentTransducer();

		// Menu contextuel affich� lors de la selection de plusieurs items
		System::Windows::Forms::ContextMenuStrip ^ m_SelectionContextMenuStrip;

		// Evenement sur le clic du menu contextuel de selection
		System::Void m_SelectionContextMenuStrip_ItemClicked(System::Object^  sender, System::Windows::Forms::ToolStripItemClickedEventArgs^ e);

		// Gestionnaire des objets selectionn�es pour l'EI supervis�e
		EISelectionManager ^ m_eiSelectionManager;

		// Gestionnaire des corrections manuelles du fond ou des �chos
		CorrectionManager * m_correctionManager;

		// Menu contextuel affich� pour valider ou annuler une suppression d'�chos
		System::Windows::Forms::ContextMenuStrip ^ m_PolygonDeleteMenuStrip;

		// Sur annulation d'une suppression d'�chos dans un polygone
		System::Void OnPolygonDeleteCancel(System::Object^  sender, System::EventArgs ^e);

		// Sur application d'une suppression d'�chos dans un polygone
		System::Void OnPolygonDeleteApply(System::Object^  sender, System::EventArgs ^e);

		// Pointeur vers le polygone � supprimer
		BaseMathLib::Poly2D * m_polygonToDelete;

		// Polygone en cours de dessin
		System::Collections::Generic::List<Point> m_BottomCorrectionPolyLine;

		// Menu contextuel affich� pour valider ou annuler une correction de fond de type polyline
		System::Windows::Forms::ContextMenuStrip ^ m_BottomCorrectionMenuStrip;

		// Sur annulation d'une polyligne de correction du fond
		System::Void OnBottomCorrectionCancel(System::Object^  sender, System::EventArgs ^e);

		// Sur application d'une polyligne de correction du fond
		System::Void OnBottomCorrectionApply(System::Object^  sender, System::EventArgs ^e);

		// Utilis� pour g�rer la concurrence entre le clic simple et le double click pour la correction manuelle du fond
		bool m_bIgnoreNextMouseUp;

		bool GetRealPoint(PointF realPingPoint, PointF & realPoint, BaseMathLib::Vector2D & realPolarCoord);

		bool GetPingEchoAndBeam(PingFan * pFan, int mouseY, int & beamIndex, int & echoIndex);
		bool GetPingEchoAndBeam(int mouseX, int mouseY, int & beamIndex, int & echoIndex, std::uint64_t & pingId);

		// Menu contextuel d'attribution d'une classification � la selection courante
		EISupervisedToolStripMenu ^ m_EISupervisedMenuStrip;

		// Evenement sur le clic de l'item permettant d'effacer le polygone selectionn�
		void m_EISupervisedMenuStrip_DeletePolygonClicked(/*System::Object^  sender, System::EventArgs^ e*/);
		void m_EISupervisedMenuStrip_AddEnergyClicked(int classIdx);
		void m_EISupervisedMenuStrip_SubstractEnergyClicked(int classIdx);
		void m_EISupervisedMenuStrip_ReclasssifyClicked(int classIdx);

		// Dessin d'un polygone
		static void DrawPolygon(Graphics^ graphics, array<PointF> ^ polygon, Pen^ pen, bool hatched);

	public:
		// Efface la selection courante
		void ClearSelection();

		delegate void EISupervisedResultChanged();
		event EISupervisedResultChanged ^ OnEISupervisedResultChanged;

		delegate void PolygonSelectionFinishedHandler();
		event PolygonSelectionFinishedHandler ^ OnPolygonSelectionFinished;

		// Sauvegarde graphique dans un fichier
		static void SaveEIView(TransducerFlatView * refView, String^transudcer, std::uint32_t esudIdx, String ^outPath);

	protected:
		// Dessin des polygones
		System::Void DrawPolygons(Graphics^ e);

		// Fonts
		System::Drawing::Font ^ m_fontForSelectedShoal;
		System::Drawing::Font ^ m_fontForShoal;

		// Pens
		Pen ^ m_3DAreaPen;
		Pen ^ m_gridPen;
		Pen ^ m_esuPen;
		
		Pen ^ m_deviationPen;

		Pen ^ m_surfaceLayerPen;
		Pen ^ m_bottomLayerPen;
		Pen ^ m_distanceLayerPen;
		Pen ^ m_polygonPen;

		Pen ^ m_selectionWindowPen;

		Brush ^ m_clearBrush;
		Brush ^ m_blackBrush;

		Brush ^ m_distanceGridBrush;

		ScrollEventHandler ^ historicScrollHandler;
	};
}
