#include "BaseFlatView.h"
#include "DisplayView.h"
#include "DisplayParameter.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "GdiWrapper.h"
#include "MoviesException.h"
#include "SingleTargetDetection/TSHistogramsForm.h"
#include "SingleTargetDetection/TSSpectrumForm.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "IViewSelectionObserver.h"

using namespace MOVIESVIEWCPP;

BaseFlatView::BaseFlatView()
	: m_viewManager(nullptr)
	, m_viewContainer(nullptr)
	, mDockFlatViewDelegate(nullptr)
{
	mCursorCenterXpercent = -1;
	m_bmp = gcnew GdiBmp();
	m_bRealPicture = false;

	InitializeComponent();

	//
	//TODO�: ajoutez ici le code du constructeur
	//
	InitToolTip();
	m_MouseX = -1;
	m_MouseY = -1;
	m_bZoom = false;
	m_bDrawingZone = false;
	m_bFullyStretched = false;
	m_StretchRatio = 1;
	m_bSelection = false;
	m_ViewDesc = "MOVIES 3D : 2D View";

	m_pAVIWriter = nullptr;
	m_userControlNames = gcnew System::Collections::Generic::List<System::String^>;

	m_bUpdatingDepth = false;

	m_defaultFont = SystemFonts::DefaultFont;

	CreateNewView = nullptr;
	
	// Initialisation des pinceaux
	m_cursorPen = gcnew Pen(Color::Blue, 1.0f);
	m_cursorPen->DashStyle = System::Drawing::Drawing2D::DashStyle::DashDotDot;
	m_bottomPen = gcnew Pen(Color::Black, 1.0f);
	m_noiseBrush = gcnew System::Drawing::Drawing2D::HatchBrush(System::Drawing::Drawing2D::HatchStyle::DiagonalCross, Color::Black, Color::Transparent);
	m_noisePen = gcnew Pen(Color::Black, 1.0f);
	m_refNoiseBrush = gcnew System::Drawing::Drawing2D::HatchBrush(System::Drawing::Drawing2D::HatchStyle::DiagonalCross, Color::Black, Color::Transparent);
	m_refNoisePen = gcnew Pen(Color::Black, 1.0f);
}

DisplayView ^ BaseFlatView::ViewManager::get()
{
	return m_viewManager;
}

void BaseFlatView::ViewManager::set(DisplayView^ viewManager)
{
	m_viewManager = viewManager;
}

DisplayViewContainer^ BaseFlatView::ViewContainer::get()
{
	return m_viewContainer;
}

void BaseFlatView::ViewContainer::set(DisplayViewContainer^ viewContainer)
{
	m_viewContainer = viewContainer;
}

DockFlatViewDelegate^ BaseFlatView::DockFlatViewHandler::get()
{
	return mDockFlatViewDelegate;
}

void BaseFlatView::DockFlatViewHandler::set(DockFlatViewDelegate^ handler)
{
	mDockFlatViewDelegate = handler;
}

System::Void BaseFlatView::UpdatePensAndBrushes()
{
	// UPDATE BOTTOM PEN
	auto displayParameter = DisplayParameter::getInstance();
	displayParameter->Lock();

	auto bottomPen = displayParameter->GetBottomPen();
	m_bottomPen->Width = bottomPen.width;
	m_bottomPen->Color = Color::FromArgb(bottomPen.color);

	// UPDATE NOISE COLOR
	auto noiseColor = displayParameter->GetNoiseColor();
	m_noisePen->Color = Color::FromArgb(noiseColor);

	if (m_noiseBrush->ForegroundColor != m_noisePen->Color)
	{
		m_noiseBrush = gcnew System::Drawing::Drawing2D::HatchBrush(
			System::Drawing::Drawing2D::HatchStyle::DiagonalCross
			, m_noisePen->Color, Color::Transparent);
	}

	// UPDATE REF NOISE COLOR
	m_refNoisePen->Color = Color::FromArgb(displayParameter->GetRefNoiseColor());
	if (m_refNoiseBrush->ForegroundColor != m_refNoisePen->Color)
	{
		m_refNoiseBrush = gcnew System::Drawing::Drawing2D::HatchBrush(
			System::Drawing::Drawing2D::HatchStyle::DiagonalCross
			, m_refNoisePen->Color, Color::Transparent);
	}

	displayParameter->Unlock();
}

System::Void BaseFlatView::DrawNow()
{
	m_NeedRedraw = false;

	TransducerContainerView ^  flatView = GetFlatView();
	const int currentIndex = GetCurrentIndex();

	if (flatView->GetImageChanged(currentIndex))
	{
		m_bRealPicture = true;
		FlatPictureBox->Invalidate();
	}
	else
	{
		this->FlatPictureBox->Refresh();
	}

	updateDetectedBottom();
}

System::Void BaseFlatView::RedrawView()
{
	int index = GetCurrentIndex();

	// TODONMD : doit on s�lectionner la vue courante ?
	//Show2DView();

	int transducerCount = FlatViewTransducerList->Items->Count;
	if (index >= 0 && index < transducerCount)
	{
		// OTK - 23/03/2009 - recalcul de la table de transformation ici (le recalcul de l'image se fait dans le setActive
		this->UpdateTransformTable(index);

		// TODONMD : est ce que �a 
		//GetFlatView()->SetActive(index);
		GetFlatView()->ForceUpdate(index);

		this->m_NeedRedraw = true;
	}
}

TransducerView *  BaseFlatView::GetTransducerView() 
{
	return GetFlatView()->GetView(m_currentIndex); 
}

int BaseFlatView::GetCurrentIndex()
{
	return m_currentIndex; 
}

int BaseFlatView::GetSounderId()
{
	auto view = GetFlatView()->GetView(m_currentIndex);
	if (view)
		return view->GetSounderId();
	return -1;
}

unsigned int BaseFlatView::GetTransducerIndex()
{
	auto view = GetFlatView()->GetView(m_currentIndex);
	if (view)
		return view->GetTransducerIndex();
	return -1;
}

System::Void BaseFlatView::SounderChanged()
{
	bool hadTransducerViews = GetFlatView()->GetTransducerList()->Count;

	System::Collections::Generic::List<String^> ^transducerList = GetFlatView()->GetTransducerList();

	FlatViewTransducerList->BeginUpdate();
	System::String ^selName = "";
	if (hadTransducerViews && FlatViewTransducerList->SelectedIndex > 0)
	{
		selName = FlatViewTransducerList->Items[FlatViewTransducerList->SelectedIndex]->ToString();
	}

	FlatViewTransducerList->Items->Clear();

	FlatViewTransducerList->Items->AddRange(transducerList->ToArray());

	// Ajout des controles utilisateurs
	FlatViewTransducerList->Items->AddRange(m_userControlNames->ToArray());

	// Recherche du controle pr�c�dement s�lectionn�
	int selectedIndex = FlatViewTransducerList->Items->IndexOf(selName);
	if (selectedIndex == -1)
	{
		selectedIndex = 0;
	}
	if (selectedIndex < FlatViewTransducerList->Items->Count)
	{
		FlatViewTransducerList->SelectedIndex = selectedIndex;
	}

	FlatViewTransducerList->EndUpdate();
}


System::Void BaseFlatView::PingFanAdded()
{
	// OTK - 23/03/2009 - sur l'ajout des pings, on passe la  taille de la fen�tre
	// pour mise � jour �ventuelle de la transformMap
	
	int maxHeight = 0;
	for each(auto view in ViewManager->SiblingFlatViews(this))
	{
		maxHeight = Math::Max(maxHeight, view->GetFlatPictureBox()->Height);
	}

	int maxWidth = 0;
	for each(auto view in ViewManager->SiblingFlatFrontViews(this))
	{
		maxWidth = Math::Max(maxWidth, view->GetFlatPictureBox()->Width);
	}

	GetFlatView()->PingFanAdded(maxWidth, maxHeight);

	for each(auto internalControl in m_internalControls)
	{
		internalControl->pingFanAdded();
	}
}

System::Void BaseFlatView::EsuClosed()
{
	// Invalidate noise echo lines
	GetFlatView()->EsuClosed();
}

void BaseFlatView::UpdateParameter()
{
	if (GetTransducerView())
	{
		GetTransducerView()->CheckForUpdate();

		for each(auto internalControl in m_internalControls)
		{
			internalControl->parametersUpdated();
		}
	}
}

void BaseFlatView::PingCursorOffsetChanged(int offset)
{
	for each(auto internalControl in m_internalControls)
	{
		internalControl->PingCursorOffsetChanged(offset);
	}
}

System::Void	BaseFlatView::StreamOpened()
{
}

System::Void BaseFlatView::SetCursorPosition(double NewPositionPercent)
{
	mCursorCenterXpercent = NewPositionPercent;
}

/*
void BaseFlatView::ZoomOut() 
{
	// annulation d'un zoom en rmin/rmax
	if (m_VerticalZoomStack.Count >= 1)
	{
		m_VerticalZoomStack.Pop();
	}

	if(m_VerticalZoomStack.Count > 0)
	{
		RMinRMax^ prevZoom = (RMinRMax^)m_VerticalZoomStack.Peek();
		DisplayParameter::getInstance()->SetZoomActivated(true);
		DisplayParameter::getInstance()->SetMinDepth(prevZoom->RMin);
		DisplayParameter::getInstance()->SetMaxDepth(prevZoom->RMax);
	}
	else
	{
		DisplayParameter::getInstance()->SetZoomActivated(false);
	}

	// annulation d'un zoom en horizontal
	OnHorizontalZoomOut();
}
*/

System::Void  BaseFlatView::FlatPictureBox_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	auto pictureBox = (System::Windows::Forms::PictureBox^)sender;
	if (!pictureBox->Focused && Form::ActiveForm == static_cast<Form^>(pictureBox->TopLevelControl))
		pictureBox->Focus();

	if (m_bZoom)
	{
		if (e->Button == System::Windows::Forms::MouseButtons::Right)
		{
			ZoomOut();
		}
		else
		{
			m_bDrawingZone = true;
			m_DrawingRegion = Rectangle(e->X, e->Y, 0, 0);
		}
	}
	else if (m_bSelection)
	{
		if (e->Button == System::Windows::Forms::MouseButtons::Left)
		{
			m_bDrawingZone = true;
			m_DrawingRegion = Rectangle(e->X, e->Y, 0, 0);
		}
	}
	else // si aucun mode sp�cial n'est actif, on agit sur le curseur
	{
		double CursorCenterXpercent = (100.0*e->X) / FlatPictureBox->Width;
		CursorHasChanged(CursorCenterXpercent);
	}
}

//Zoom molette
System::Void  BaseFlatView::FlatPictureBox_MouseWheel(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	int debug = 0;

	if (e->Delta > 0)
	{
		// mise � jour de la zone de s�lection
		Point pt(e->X, e->Y);
		pt.X = pt.X < 0 ? 0 : pt.X;
		pt.X = pt.X >= FlatPictureBox->Width ? FlatPictureBox->Width - 1 : pt.X;
		pt.Y = pt.Y < 0 ? 0 : pt.Y;
		pt.Y = pt.Y >= FlatPictureBox->Height ? FlatPictureBox->Height - 1 : pt.Y;

		m_DrawingRegion.X = (pt.X * 0.2);
		m_DrawingRegion.Y = (pt.Y * 0.2);

		m_DrawingRegion.Width = (FlatPictureBox->Width - 1) * 0.8;
		m_DrawingRegion.Height = (FlatPictureBox->Height - 1) * 0.8;

		if (m_DrawingRegion.Width > 1 && m_DrawingRegion.Height > 1)
		{
			OnZoom();
		}
	}
	else
	{
		ZoomOut();
	}
}

System::Void  BaseFlatView::FlatPictureBox_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if ((m_bZoom || m_bSelection) && m_bDrawingZone && e->Button == System::Windows::Forms::MouseButtons::Left)
	{
		// mise � jour de la zone de s�lection
		Point pt(e->X, e->Y);
		pt.X = pt.X < 0 ? 0 : pt.X;
		pt.X = pt.X >= FlatPictureBox->Width ? FlatPictureBox->Width - 1 : pt.X;
		pt.Y = pt.Y < 0 ? 0 : pt.Y;
		pt.Y = pt.Y >= FlatPictureBox->Height ? FlatPictureBox->Height - 1 : pt.Y;
		m_DrawingRegion.Width = pt.X - m_DrawingRegion.X;
		m_DrawingRegion.Height = pt.Y - m_DrawingRegion.Y;

		// remise dans l'ordre des 2 points du rectangle (topleft + bottomright)
		Point topLeft = Point(std::min(m_DrawingRegion.X, m_DrawingRegion.X + m_DrawingRegion.Width), std::min(m_DrawingRegion.Y, m_DrawingRegion.Y + m_DrawingRegion.Height));
		Point bottomRight = Point(std::max(m_DrawingRegion.X, m_DrawingRegion.X + m_DrawingRegion.Width), std::max(m_DrawingRegion.Y, m_DrawingRegion.Y + m_DrawingRegion.Height));
		m_DrawingRegion.X = topLeft.X;
		m_DrawingRegion.Y = topLeft.Y;
		m_DrawingRegion.Width = bottomRight.X - topLeft.X;
		m_DrawingRegion.Height = bottomRight.Y - topLeft.Y;

		if (m_DrawingRegion.Width > 1 && m_DrawingRegion.Height > 1)
		{
			if (m_bZoom)
			{
				OnZoom();
			}
			else if (m_bSelection)
			{
				OnRegionSelection();
				this->FlatPictureBox->Invalidate();
			}
		}
		// si on fait un clic sans d�placer la souris, on positionne le curseur au lieu de faire un zoom
		else
		{
			if (m_bSelection)
			{
				// s�lection du banc
				Point pt(e->X, e->Y);
				OnShoalSelection(pt);
			}
			else
			{
				double CursorCenterXpercent = (100.0*e->X) / FlatPictureBox->Width;
				CursorHasChanged(CursorCenterXpercent);
			}
		}
	}

	m_bDrawingZone = false;
}

//modifier le transducer s�lectionn�
void BaseFlatView::SetTransducer(String^ transducerName)
{
	//recherche de l'indice du transducteur dans la liste pr�sente
	int transIdx = -1;

	int transducerCount = FlatViewTransducerList->Items->Count;
	for (int i = 0; i < transducerCount; i++)
	{
		if (FlatViewTransducerList->Items[i]->Equals(transducerName))
		{
			transIdx = i;
		}
	}

	if (transIdx >= 0)
	{
		FlatViewTransducerList->SelectedIndex = transIdx;
	}
}

//r�cup�rer le transducer s�lectionn�
String^ BaseFlatView::GetTransducer()
{
	String^ transducerName = gcnew String("");

	int transducerCount = FlatViewTransducerList->Items->Count;

	if (m_currentIndex >= 0 && m_currentIndex < transducerCount)
	{
		transducerName = (System::String^)FlatViewTransducerList->Items[m_currentIndex];
	}

	return transducerName;
}

//mapping d'un point en coord ecran vers coord r�elles
PointF BaseFlatView::MapScreenToRealCoord(PointF screenCoord)
{
	PointF result(screenCoord.X, screenCoord.Y);

	//gestion des coordonn�es verticales
	double yRealMin = DisplayParameter::getInstance()->GetCurrentMinDepth();
	double yRealMax = DisplayParameter::getInstance()->GetCurrentMaxDepth();

	double currentHeightMeters = (yRealMax - yRealMin)*GetVerticalStretchRatio();

	// point superieur
	double heightPercent = (double)(screenCoord.Y) / (double)FlatPictureBox->Height;
	result.Y = yRealMin + heightPercent * currentHeightMeters;

	return result;
}

//mapping d'un point en coord r�elles vers coord �cran
PointF BaseFlatView::MapRealToScreenCoord(PointF realCoord)
{
	PointF result(realCoord.X, realCoord.Y);

	//gestion des coordonn�es verticales
	TransducerView * pRefView = GetTransducerView();

	double yRealMin = pRefView->GetStartPixelDepth();
	double yRealMax = pRefView->GetStopPixelDepth();

	double currentHeightMeters = (yRealMax - yRealMin)*GetVerticalStretchRatio();

	// point superieur
	double heightPercent = (realCoord.Y - yRealMin) / currentHeightMeters;
	result.Y = heightPercent * (double)FlatPictureBox->Height;

	return result;
}

System::Void  BaseFlatView::FlatPictureBox_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	// affichage du curseur
	if (e->Button == System::Windows::Forms::MouseButtons::Left)
	{
		if (m_bZoom || m_bSelection)
		{
			if (m_bDrawingZone)
			{
				// on r�cup�re les coordonn�es de la nouvelle s�lection en bornant pour ne pas d�passer la fenetre
				Point pt(e->X, e->Y);
				pt.X = pt.X < 0 ? 0 : pt.X;
				pt.X = pt.X >= FlatPictureBox->Width ? FlatPictureBox->Width - 1 : pt.X;
				pt.Y = pt.Y < 0 ? 0 : pt.Y;
				pt.Y = pt.Y >= FlatPictureBox->Height ? FlatPictureBox->Height - 1 : pt.Y;
				m_DrawingRegion.Width = pt.X - m_DrawingRegion.X;
				m_DrawingRegion.Height = pt.Y - m_DrawingRegion.Y;

				// pour dessin de la zone de zoom
				this->FlatPictureBox->Refresh();
			}
		}
		// si on n'est pas dans un mode sp�cial, la souris gere le curseur
		else
		{
			// FRE- on regarde donc si la souris a r�ellement boug�
			if (e->X != m_MouseX || e->Y != m_MouseY)
			{
				double CursorCenterXpercent = (100.0*e->X) / FlatPictureBox->Width;
				CursorHasChanged(CursorCenterXpercent);
			}
		}
	}

	// affichage de la tooltip
	// OTK - probl�me sous windows vista : l'affichage de la toolip
	// d�clenche un nouvel evenement mousemouve ! on regarde donc si la souris a r�ellement boug�
	if (e->X != m_MouseX || e->Y != m_MouseY)
	{
		m_EchoToolTip->Active = DisplayParameter::getInstance()->GetDisplayEchoTooltip();

		if (m_EchoToolTip->Active)
		{
			System::String^ echoInfo = this->FormatEchoInformation(e->X, e->Y);
			this->m_EchoToolTip->SetToolTip(this->FlatPictureBox, echoInfo);
		}
	}
	m_MouseX = e->X;
	m_MouseY = e->Y;
}

System::Void BaseFlatView::FlatPictureBox_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e)
{
	// lock transducer view
	TransducerContainerView ^  flatView = GetFlatView();
	TransducerView* view = GetTransducerView();
	if (view)
	{
		view->LockDatas();

		// Update bmp
		m_bmp->Assign(view->GetWidth(), view->GetHeight(), (unsigned int*)view->GetPtr());
	}
	
	// dessin dans l'IHM
	DrawOnGraphics(e->Graphics);

	// dessin dans l'AVI si capture en cours
	if (m_pAVIWriter != nullptr)
	{
		Bitmap^ bit = gcnew Bitmap(this->FlatPictureBox->Width, this->FlatPictureBox->Height);
		Graphics^ g = Graphics::FromImage(bit);
		DrawOnGraphics(g);
		m_pAVIWriter->AddBitmap(bit);
	}

	// unlock transducer view
	if (view)
	{
		view->UnlockDatas();
	}
}

System::Void BaseFlatView::DrawOnGraphics(Graphics^ e)
{
	// Maj des param�tres d'affichage graphiques avant rendu
	UpdatePensAndBrushes();

	// Dessin de l'image 2D
	DrawImage(e);

	// dessin de la grille de profondeur
	if (DisplayParameter::getInstance()->GetDepthGrid())
		DrawDepthGrid(e);

	// dessin de la grille de distance
	DrawDistanceGrid(e);

	// dessin de la grille des ESUs
	DrawESUGrid(e);

	// dessin des contours des couches
	DrawLayersContour(e);

	// Dessin du fond
	DrawBottom(e);

	// Dessin de la port�e de r�f�r�nce
	if (DisplayParameter::getInstance()->GetRefNoiseDisplayEnabled()) {
		DrawRefNoise(e);
	}

	// Dessin de la port�e du niveau de bruit
	if (DisplayParameter::getInstance()->GetNoiseDisplayEnabled()) {
		DrawNoise(e);
	}

	// Dessin de la deviation 
	DrawDeviation(e);

	// Dessin des isocourbes
	DrawContourLines(e);

	// Dessin des cibles simples
	if (DisplayParameter::getInstance()->GetDisplay2DSingleEchoes())
		DrawSingleTargets(e);

	// dessin du curseur de position
	UpdateCursor();
	DrawCursor(e);

	// dessin des id de banc
	DrawShoalId(e);

	// dessin �ventuel de la zone de zoom ou de s�lection en cours de dessin
	DrawSelectionZone(e);

	// dessin �ventuel de la s�lection rectangulaire sur laquelle porte par exemple la r�ponse fr�quentielle
	DrawSelectionWindow(e);
}

// dessin de la zone de s�lection
System::Void BaseFlatView::DrawSelectionZone(Graphics^  e)
{
	// dessin de la zone de s�lection � la souris pendant le clic&drag
	if ((m_bZoom || m_bSelection) && m_bDrawingZone)
	{
		Point topLeft(std::min(m_DrawingRegion.X, m_DrawingRegion.X + m_DrawingRegion.Width), std::min(m_DrawingRegion.Y, m_DrawingRegion.Y + m_DrawingRegion.Height));
		Rectangle^ rect = gcnew Rectangle(topLeft.X, topLeft.Y, abs(m_DrawingRegion.Width), abs(m_DrawingRegion.Height));
		GDIWrapper::DrawXORRectangle(e, gcnew Pen(Color::Black), rect);
	}
}

// dessin de la grille des profondeurs
System::Void BaseFlatView::DrawDepthGrid(Graphics^  e)
{
	Pen^ blackPen = gcnew Pen(Color::Black, 1.0f);

	//TransducerFlatView* ref = this->m_viewContainer->m_sideViewContainer->GetActiveView();
	TransducerFlatView* ref = (TransducerFlatView*)GetTransducerView();
	if (ref)
	{
		// on r�cup�re la profondeur min et max
		double minDepth = DisplayParameter::getInstance()->GetCurrentMinDepth();
		double maxDepth = DisplayParameter::getInstance()->GetCurrentMaxDepth();

		// on r�cup�re le pas d'�chantillonnage du transducteur
		M3DKernel::GetInstance()->Lock();
		Transducer * pTrans = ref->GetTransducer();
		if (pTrans)
		{
			double sampleSpacing = ref->GetTransducer()->getBeamsSamplesSpacing();

			// on r�cup�re l'espacement souhait� entre les lignes de profondeur
			double depthInterLines = DisplayParameter::getInstance()->GetDepthSampling();

			// on laisse une marge entre la ligne et le bas du texte
			int bottomMargin = 3;
			// et entre la ligne et le cot� droit de l'image
			int leftmargin = 3;

			int x1 = 0;
			int x2 = GetDataWidth();
			double height = GetRealDisplayHeight();

			bool terminated = false;
			double depthInterLinesFactor = 1;
			while (!terminated)
			{
				double depth = depthInterLinesFactor*depthInterLines;
				if (depth > minDepth && depth < maxDepth)
				{
					// on trace la ligne correspondante.
					int y1 = (int)(height*(depth - minDepth) / (maxDepth - minDepth));
					int y2 = y1;
					//e->Graphics->DrawLine(blackPen,x1,y1,x2,y2);
					// au lieu d'une ligne pleine, on trace une ligne XOR avec GDI (pas de moyen simple avec GDI+ ...)
					GDIWrapper::DrawXORLine(e, blackPen, x1, y1, x2, y2);

					// Dessin du label de distance
					// on d�termine le rectangle englobant le texte
					String^ str = Convert::ToString(depth) + " m";
					SizeF^ size = e->MeasureString(str, m_defaultFont);

					// si on n'a pas la place, on ne dessine pas le texte
					if (size->Width + leftmargin < x2)
					{
						System::Drawing::Font^ font = m_defaultFont;
						// si on est trop pres de la ligne suivante, on r�tr�ci la taille
						int nbPixelsInterLines = y1 - (int)(height*((depthInterLinesFactor - 1)*depthInterLines - minDepth) / (maxDepth - minDepth));

						if (size->Height + bottomMargin + 1 > nbPixelsInterLines)
						{
							double ratio = (double)(size->Width) / (double)(size->Height);
							size->Height = nbPixelsInterLines - 1 - bottomMargin;
							int oldWidth = size->Width;
							size->Width = size->Height*ratio;
							double shrinkFactor = (double)size->Width / (double)oldWidth;
							font = gcnew System::Drawing::Font(m_defaultFont->FontFamily, std::max<float>(1.0, m_defaultFont->SizeInPoints*shrinkFactor));
							size = e->MeasureString(str, font);
						}

						e->FillRectangle(Brushes::White, System::Drawing::Rectangle(x1 + leftmargin, y2 - (size->Height + bottomMargin), size->Width, size->Height));
						e->DrawString(str, font, SystemBrushes::ControlText, x1 + leftmargin, y2 - (size->Height + bottomMargin));
					}
				}

				if (depth >= maxDepth)
				{
					terminated = true;
				}
				depthInterLinesFactor += 1;
			}
		}
		M3DKernel::GetInstance()->Unlock();
	}
}


double BaseFlatView::GetVerticalStretchRatio()
{
	double result;
	if (m_bFullyStretched)
	{
		result = 1;
	}
	else
	{
		result = ((double)FlatPictureBox->Height) / ((double)m_bmp->Height()) / m_StretchRatio;
	}
	return result;
}

double BaseFlatView::GetHorizontalStretchRatio()
{
	double result;
	if (m_bFullyStretched)
	{
		result = 1;
	}
	else
	{
		result = ((double)FlatPictureBox->Width) / ((double)m_bmp->Width()) / m_StretchRatio;
	}
	return result;
}

double BaseFlatView::GetHorizontalMargin()
{
	double result;
	if (m_bFullyStretched)
	{
		result = 0;
	}
	else
	{
		result = ((double)FlatPictureBox->ClientRectangle.Width - (double)m_bmp->Width()*m_StretchRatio) / 2.0;
	}
	return result;
}

double BaseFlatView::GetVerticalMargin()
{
	double result;
	if (m_bFullyStretched)
	{
		result = 0;
	}
	else
	{
		result = (double)FlatPictureBox->ClientRectangle.Height - (double)m_bmp->Height()*m_StretchRatio;
	}
	return result;
}

double BaseFlatView::GetRealDisplayWidth()
{
	double result;
	if (m_bFullyStretched)
	{
		result = FlatPictureBox->Width;
	}
	else
	{
		result = m_bmp->Width() * m_StretchRatio;
	}
	return result;
}

double BaseFlatView::GetRealDisplayHeight()
{
	double result;
	if (m_bFullyStretched)
	{
		result = FlatPictureBox->Height;
	}
	else
	{
		result = m_bmp->Height() * m_StretchRatio;
	}
	return result;
}

double BaseFlatView::ConvertStretchToFlatPercent(double percent)
{
	double result = (percent - 50)*GetHorizontalStretchRatio() + 50;
	if (result < 0)
	{
		result = 0;
	}
	if (result > 100)
	{
		result = 100;
	}
	return result;
}

double BaseFlatView::ConvertFlatToStretchPercent(double percent)
{
	double result = (percent - 50) / GetHorizontalStretchRatio() + 50;
	if (result < 0)
	{
		result = 0;
	}
	if (result > 100)
	{
		result = 100;
	}
	return result;
}

System::Void BaseFlatView::FlatPictureBox_Resize(System::Object^  sender, System::EventArgs^  e) 
{
	// OTK - 23/03/2009 - recalcul des transformtables en fonction de la nouvelle taille de la fenetre
	this->UpdateTransformTable();
	// forcage du recalcul de l'image
	GetFlatView()->ForceUpdate(GetCurrentIndex());
	this->m_NeedRedraw = true;
	this->FlatPictureBox->Refresh();
	AdjustCursorPosition();
}

// gestion de la g�n�ration des fichiers AVI
System::Void BaseFlatView::buttonAVI_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	// si on lance la g�n�ration de l'AVI ...
	if (buttonAVI->Checked)
	{
		// choix du fichier destination...
		if (m_pAVIWriter == nullptr && saveAVIFileDialog->ShowDialog() == DialogResult::OK)
		{
			// la destination est s�lectionn�e, on commence la g�n�ration
			m_pAVIWriter = gcnew AVIWriter();
			if (m_pAVIWriter->StartAVI(saveAVIFileDialog->FileName) != 0)
			{
				// en cas d'�chec de la cr�ation du fichier, on annule tout
				m_pAVIWriter = nullptr;
				buttonAVI->Checked = false;
			}
		}
		else
		{
			buttonAVI->Checked = false;
		}
	}
	else
	{
		if (m_pAVIWriter != nullptr)
		{
			// arr�t de la g�n�ration
			m_pAVIWriter->StopAVI();
			m_pAVIWriter = nullptr;
		}
	}
}

System::Void BaseFlatView::DepthVScrollBar_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
	if (!m_bUpdatingDepth)
	{
		// calcul du delta pour savoir si on cliqu� sur les fleches ou sur la barre
		int delta = 45 - DepthVScrollBar->Value;
		DisplayParameter *pDisplay = DisplayParameter::getInstance();
		double dMinDepth = pDisplay->GetCurrentMinDepth();
		double dMaxDepth = pDisplay->GetCurrentMaxDepth() - delta;

		// modification de la profondeur
		if (dMaxDepth > dMinDepth)
		{
			m_updateMaxDepthDelegate(dMaxDepth);
		}

		// reinitialisation du scroll vertical au centre
		m_bUpdatingDepth = true;
		DepthVScrollBar->Value = 45;
		m_bUpdatingDepth = false;
	}
}

System::Void BaseFlatView::buttonAddView_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (CreateNewView != nullptr)
	{
		BaseFlatView ^ view = CreateNewView(false);
		view->SounderChanged();
	}
	else
	{
		throw gcnew MOVIESVIEWCPP::MoviesException("CreateNewView Handler not initialized...");
	}
}

System::Void BaseFlatView::buttonSplitView_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (CreateNewView != nullptr)
	{
		BaseFlatView ^ view = CreateNewView(true);
		view->SounderChanged();
	}
	else
	{
		throw gcnew MOVIESVIEWCPP::MoviesException("SplitView Handler not initialized...");
	}
}

IViewSelectionObserver ^ BaseFlatView::GetViewSelectionObserver()
{
	return m_pViewSelectionObserver;
}

System::Void BaseFlatView::SetViewSelectionObserver(IViewSelectionObserver^ pObs)
{
	m_pViewSelectionObserver = pObs;
	m_pViewSelectionObserver->SelectionChanged += gcnew IViewSelectionObserver::SelectionChangedHandler(this, &BaseFlatView::OnSelectionChanged);
}

void BaseFlatView::OnSelectionChanged(System::Object^  sender)
{
	//if( sender != this)
	{
		DrawNow();
	}
}

System::Void BaseFlatView::FlatViewTransducerList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	m_currentIndex = FlatViewTransducerList->SelectedIndex;
	m_currentItem = FlatViewTransducerList->SelectedItem;

	SelectedTransducerChanged();

	mCursorCenterXpercent = -1;

	System::String ^ selected = m_currentItem->ToString();
	int idx = m_userControlNames->IndexOf(selected);
	if (idx == -1)
	{
		if (tabControl->SelectedIndex > m_internalControls.Count)
			tabControl->SelectedIndex = 0;

		TransducerChanged(GetTransducer());
		RedrawView();

		for each(auto internalControl in m_internalControls)
		{
			internalControl->transducerChanged(GetTransducerView()->GetSounderId(), GetTransducerView()->GetTransducerIndex());
		}
	}
	else
	{
		tabControl->SelectedIndex = idx + m_internalControls.Count + 1;
	}
	
	OnTransducerSelectedIndexChanged();
	UpdateHistoricSCrollBar();

	// Maj de l'histograme sur s�lection
	if (IsTSHistogramSelected())
	{
		// Suppression de la TabPage associ�e au contr�le
		for each(TabPage ^tabPage in tabControl->TabPages)
		{
			for each(Control ^control in tabPage->Controls)
			{
				if (control->Text == "TS Histogram")
				{
					((TSHistogramsForm^)control)->UpdateHistogram();
					break;
				}
			}
		}
	}
	else if (IsTSSpectrumSelected())
	{
		// Suppression de la TabPage associ�e au contr�le
		for each(TabPage ^tabPage in tabControl->TabPages)
		{
			for each(Control ^control in tabPage->Controls)
			{
				if (control->Text == "TS Spectrum")
				{
					((TSSpectrumsForm^)control)->UpdateSpectrum();
					break;
				}
			}
		}
	}
}

System::Void BaseFlatView::AddUserControl(System::Windows::Forms::Control ^ userControl)
{
	if (userControl != nullptr && m_userControlNames->Contains(userControl->Text) == false)
	{
		m_userControlNames->Add(userControl->Text);

		userControl->Dock = System::Windows::Forms::DockStyle::Fill;
		TabPage ^ page = gcnew TabPage();
		page->Controls->Add(userControl);
		tabControl->TabPages->Add(page);

		// Ajout dans la combobox et selection du control
		FlatViewTransducerList->Items->Add(userControl->Text);
		FlatViewTransducerList->SelectedIndex = FlatViewTransducerList->Items->Count - 1;
	}
}

System::Void BaseFlatView::RemoveUserControl(System::Windows::Forms::Control ^ userControl)
{
	if (userControl != nullptr && m_userControlNames->Contains(userControl->Text) == true)
	{
		// Suppression de la liste des contr�les utilisateurs g�r�s
		m_userControlNames->Remove(userControl->Text);

		// Suppression de la TabPage associ�e au contr�le
		for each(TabPage ^tabPage in tabControl->TabPages)
		{
			if (tabPage->Controls->Contains(userControl))
			{
				tabControl->TabPages->Remove(tabPage);
				break;
			}
		}

		// Suppression du contr�le dans la combobox des transducteurs / contr�les utilisateurs
		FlatViewTransducerList->Items->Remove(userControl->Text);
		if (FlatViewTransducerList->SelectedIndex == -1)
		{
			FlatViewTransducerList->SelectedIndex = 0;
		}
	}
}

System::Void BaseFlatView::SelectUserControl(System::Windows::Forms::Control ^ userControl)
{
	int idx = FlatViewTransducerList->Items->IndexOf(userControl->Text);
	if (idx != -1)
	{
		FlatViewTransducerList->SelectedIndex = idx;
	}
}

bool BaseFlatView::IsUserControlVisible()
{
	return tabControl->SelectedIndex != 0;
}

System::Void BaseFlatView::FlatPictureBox_MouseEnter(System::Object^  sender, System::EventArgs^  e) 
{
	auto pictureBox = (System::Windows::Forms::PictureBox^)sender;
	if (!pictureBox->Focused && Form::ActiveForm == static_cast<Form^>(pictureBox->TopLevelControl))
		pictureBox->Focus();
}

System::String^ BaseFlatView::FormatEchoInformation(int ScreenPosX, int ScreenPosY)
{
	System::String ^result = "No Data";
	return result;
}

System::Void BaseFlatView::buttonDockView_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	if (buttonDockView->Checked)
	{
		auto dockViewDelegate = gcnew DockViewDelegate(this, &BaseFlatView::DockFlatView);
		mExternalViewWindow = gcnew ExternalViewWindow(this, dockViewDelegate, GetViewDesc());
		mExternalViewWindow->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &MOVIESVIEWCPP::BaseFlatView::OnExternalWindowClosing);

		mDockFlatViewDelegate(false, mExternalViewWindow);
		mExternalViewWindow->Show();
		buttonDockView->ImageIndex = 0;
	}
	else
	{
		DockFlatView();

		// remove handler on closing to prevent call on real close handling
		mExternalViewWindow->Closing -= gcnew System::ComponentModel::CancelEventHandler(this, &MOVIESVIEWCPP::BaseFlatView::OnExternalWindowClosing);

		mExternalViewWindow->Close();
	}
	InitToolTip();
}

System::Void BaseFlatView::Close()
{
	if (Docked)
	{
		CloseInternalFlatViewHandler(this);
	}
	else
	{
		CloseExternalFlatViewHandler(mExternalViewWindow);
	}
}

System::Void BaseFlatView::buttonCloseView_Click(System::Object^  sender, System::EventArgs^  e)
{
	CloseInternalFlatViewHandler(this);
}

void MOVIESVIEWCPP::BaseFlatView::OnExternalWindowClosing(System::Object ^sender, System::ComponentModel::CancelEventArgs ^e)
{	
	CloseExternalFlatViewHandler(mExternalViewWindow);
}

System::Void BaseFlatView::InitToolTip()
{
	this->m_EchoToolTip = (gcnew System::Windows::Forms::ToolTip(this->components));
	this->m_EchoToolTip->AutoPopDelay = 0;
	this->m_EchoToolTip->InitialDelay = 500;
	this->m_EchoToolTip->ReshowDelay = 100;
	this->m_EchoToolTip->ShowAlways = true;
	this->m_EchoToolTip->UseAnimation = false;
	this->m_EchoToolTip->UseFading = false;
}

System::Void BaseFlatView::DockFlatView()
{
	mDockFlatViewDelegate(true, mExternalViewWindow);
	buttonDockView->ImageIndex = 1;
	buttonDockView->Checked = false;
}

bool BaseFlatView::IsTSSpectrumSelected()
{
	bool result = false;
	int idx = FlatViewTransducerList->Items->IndexOf("TS Spectrum");
	if (idx != -1)
	{
		result = (GetCurrentIndex() == idx);
	}
	return result;
}


bool BaseFlatView::IsTSHistogramSelected()
{
	bool result = false;
	int idx = FlatViewTransducerList->Items->IndexOf("TS Histogram");
	if (idx != -1)
	{
		result = (GetCurrentIndex() == idx);
	}
	return result;
}

int BaseFlatView::AddInternalControl(InternalControl^ control)
{
	control->Dock = System::Windows::Forms::DockStyle::Fill;
	TabPage ^ page = gcnew TabPage();
	page->Controls->Add(control);
	page->Padding = System::Windows::Forms::Padding(3);
	tabControl->TabPages->Insert(m_internalControls.Count + 1, page);
	m_internalControls.Add(control);
	return m_internalControls.Count;
}

void BaseFlatView::showInternalControl(int index)
{
	tabControl->SelectedIndex = index;
}

bool BaseFlatView::isInternalControlDisplayed(int index)
{
	return tabControl->SelectedIndex == index;
}

double BaseFlatView::getDetectedBottom()
{
	if (m_currentItem == nullptr)
		return 0.0;

	double bottom = 0.0;
	System::String ^ selected = m_currentItem->ToString();
	int idx = m_userControlNames->IndexOf(selected);
	if (idx == -1)
	{
		auto transView = GetTransducerView();
		auto sounderId = transView->GetSounderId();
		auto transducerIndex = transView->GetTransducerIndex();
		
		auto pKernel = M3DKernel::GetInstance();
		auto & sounderDef = pKernel->getObjectMgr()->GetSounderDefinition();
		auto sounder = sounderDef.GetSounderWithId(sounderId);
		auto transducer = sounder->GetTransducer(transducerIndex);

		TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(sounder->m_SounderId);
		if (pCont)
		{			
			int offset = getCurrentPingOffset();
			int pingIndex = std::max<int>(0, pCont->GetObjectCount() - 1 - offset);
			PingFan *pingFan = (PingFan *)pCont->GetDatedObject(pingIndex);
			if (pingFan)
			{

				const auto nbChannels = transducer->GetChannelId().size();
				for (auto channelIndex = 0; channelIndex < nbChannels; ++channelIndex)
				{
					auto channel = transducer->GetChannelId()[channelIndex];

					// r�cup�ration de la profondeur du fond verticale pour chaque channel
					std::uint32_t echoFondEval = 0;
					std::int32_t bottomRange = 0;
					bool found = false;
					pingFan->getBottom(channel, echoFondEval, bottomRange, found);

					if (found)
					{
						// Position du fond dans le rep�re antenne LB 25/03/2015 on travaille dans le rep�re antenne
						// pour l'EIsupervis� comme pour l'EI couche
						auto vecBottom = sounder->GetPolarToGeoCoord(pingFan, transducerIndex, channelIndex, echoFondEval - transducer->GetSampleOffset());
						bottom = std::max(bottom , vecBottom.z);
					}
				}
			}
		}
	}

	return bottom;
}

System::Void BaseFlatView::updateDetectedBottom()
{
	auto bottom = getDetectedBottom();
	if (bottom > 0)
	{
		labelDettectedBottom->Text = "Bottom Depth : " + bottom.ToString("0.00") + "m";
		labelDettectedBottom->ForeColor = Color::Black;
	}
	else
	{
		labelDettectedBottom->Text = "No Detection";
		labelDettectedBottom->ForeColor = Color::Red;
	}
}

FormWindowState BaseFlatView::getWindowState()
{
	if (mExternalViewWindow) {
		return mExternalViewWindow->WindowState;
	}
	return FormWindowState::Normal;
}

System::Void BaseFlatView::setWindowState(FormWindowState state)
{
	if (mExternalViewWindow) {
		return mExternalViewWindow->WindowState = state;
	}
}
