#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "TransducerContainerFlatView.h"
#include "DisplayViewContainer.h"
#include "ExternalViewWindow.h"
#include "AVIWriter.h"
#include "DisplayParameter.h"

#include "GdiBmp.h"

class PingFan;
interface class IViewSelectionObserver;
ref class DisplayView;

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de BaseFlatView
	/// </summary>
	ref class BaseFlatView;

	// pour rapatriement du control externalis� sur la form principale
	public delegate void DockFlatViewDelegate(bool dock, ExternalViewWindow^ viewWindows);
	public delegate void CloseInternalFlatViewDelegate(BaseFlatView^ view);
	public delegate void CloseExternalFlatViewDelegate(ExternalViewWindow^ viewWindows);

	public ref class InternalControl abstract : public System::Windows::Forms::UserControl
	{
	public:
		InternalControl(UserControl^ subControl)
		{
			subControl->Dock = System::Windows::Forms::DockStyle::Fill;
			Controls->Add(subControl);
		}

		virtual void pingFanAdded() = 0;
		virtual void parametersUpdated() = 0;
		virtual void transducerChanged(std::uint32_t sounderId, unsigned int transducerIndex) = 0;
		virtual void PingCursorOffsetChanged(int offset) = 0;
	};

	public ref class BaseFlatView abstract : public System::Windows::Forms::UserControl
	{

	protected: System::Windows::Forms::PictureBox^  FlatPictureBox;
	private: System::Windows::Forms::TableLayoutPanel^  toolbarTableLayoutPanel;
	protected: System::Windows::Forms::CheckBox^  buttonSpectralView;
	private: System::Windows::Forms::Label^  labelDettectedBottom;
	private: System::Windows::Forms::TableLayoutPanel^  mainLayout;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel3;
	private: System::Windows::Forms::TableLayoutPanel^  mainTableLayoutPanel;

	private: System::Windows::Forms::ComboBox^  FlatViewTransducerList;
	private: System::Windows::Forms::ToolTip^  m_EchoToolTip;
	protected: System::Windows::Forms::ProgressBar^  progressBar;
	protected: System::Windows::Forms::Label^  labelProgress;
	private: System::Windows::Forms::ImageList^  imageListDock;
	protected: System::Windows::Forms::CheckBox^  buttonHistoric;
	protected: System::Windows::Forms::CheckBox^  buttonAVI;
	private: System::Windows::Forms::SaveFileDialog^  saveAVIFileDialog;
	private: System::Windows::Forms::PictureBox^  FlatPictureBoxResults;
	public: System::Windows::Forms::HScrollBar^  HistoricHScrollBar;

	private: System::Windows::Forms::VScrollBar^  DepthVScrollBar;
	private: System::Windows::Forms::TableLayoutPanel^  viewTableLayoutPanel;

	private: System::Windows::Forms::TabControl^  tabControl;
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::CheckBox^  buttonDockView;
	private: System::Windows::Forms::Button^  buttonAddView;
	private: System::Windows::Forms::Button^  buttonSplitView;
	private: System::Windows::Forms::Button^  buttonCloseView;

	public: BaseFlatView();

	public: property DisplayView^  ViewManager
	{
		DisplayView^ get();
		void set(DisplayView^val);
	}

	public: property DisplayViewContainer^ ViewContainer
	{
		DisplayViewContainer^ get();
		void set(DisplayViewContainer^val);
	}

	public: property DockFlatViewDelegate^ DockFlatViewHandler
	{
		DockFlatViewDelegate^ get();
		void set(DockFlatViewDelegate^ val);
	}

	public: property CloseInternalFlatViewDelegate^ CloseInternalFlatViewHandler;
	public: property CloseExternalFlatViewDelegate^ CloseExternalFlatViewHandler;

	public:

		property bool Docked
		{
			bool get() 
			{ 
				return !this->buttonDockView->Checked;	
			}
			void set(bool val) 
			{ 
				this->buttonDockView->Checked = !val; 
			}
		}

		property bool DockEnabled
		{
			bool get()
			{
				return this->buttonDockView->Enabled;
			}
			void set(bool val)
			{
				this->buttonDockView->Enabled = val;
				this->buttonDockView->Visible = val;
			}
		}
		
		property bool SplitEnabled
		{
			bool get()
			{
				return this->buttonSplitView->Enabled;
			}
			void set(bool val)
			{
				this->buttonSplitView->Enabled = val;
				this->buttonSplitView->Visible = val;
			}
		}

		property bool CloseEnabled
		{
			bool get()
			{
				return this->buttonCloseView->Enabled;
			}
			void set(bool val)
			{
				this->buttonCloseView->Enabled = val;
				this->buttonCloseView->Visible = val;
			}
		}
		
		System::Void Close();

	protected:

		System::Drawing::Font ^ m_defaultFont;

		virtual System::Void UpdatePensAndBrushes();

		Pen ^ m_cursorPen;
		Pen ^ m_bottomPen;
		Pen ^ m_noisePen;
		System::Drawing::Drawing2D::HatchBrush ^ m_noiseBrush;
		Pen ^ m_refNoisePen;
		System::Drawing::Drawing2D::HatchBrush ^ m_refNoiseBrush;

		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~BaseFlatView()
		{
			// arr�t �ventuel de la g�n�ration de l'avi
			if (m_pAVIWriter != nullptr)
			{
				m_pAVIWriter->StopAVI();
				m_pAVIWriter = nullptr;
			}
			if (components)
			{
				delete components;
			}
			m_bZoom = true;
		}



#pragma region Windows Form Designer generated code
			 /// <summary>
			 /// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
			 /// le contenu de cette m�thode avec l'�diteur de code.
			 /// </summary>
			 void InitializeComponent(void)
			 {
				 this->components = (gcnew System::ComponentModel::Container());
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(BaseFlatView::typeid));
				 this->FlatViewTransducerList = (gcnew System::Windows::Forms::ComboBox());
				 this->progressBar = (gcnew System::Windows::Forms::ProgressBar());
				 this->labelProgress = (gcnew System::Windows::Forms::Label());
				 this->buttonDockView = (gcnew System::Windows::Forms::CheckBox());
				 this->imageListDock = (gcnew System::Windows::Forms::ImageList(this->components));
				 this->buttonHistoric = (gcnew System::Windows::Forms::CheckBox());
				 this->buttonAVI = (gcnew System::Windows::Forms::CheckBox());
				 this->saveAVIFileDialog = (gcnew System::Windows::Forms::SaveFileDialog());
				 this->buttonAddView = (gcnew System::Windows::Forms::Button());
				 this->HistoricHScrollBar = (gcnew System::Windows::Forms::HScrollBar());
				 this->DepthVScrollBar = (gcnew System::Windows::Forms::VScrollBar());
				 this->viewTableLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->FlatPictureBox = (gcnew System::Windows::Forms::PictureBox());
				 this->tabControl = (gcnew System::Windows::Forms::TabControl());
				 this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
				 this->toolbarTableLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->labelDettectedBottom = (gcnew System::Windows::Forms::Label());
				 this->buttonSpectralView = (gcnew System::Windows::Forms::CheckBox());
				 this->buttonSplitView = (gcnew System::Windows::Forms::Button());
				 this->buttonCloseView = (gcnew System::Windows::Forms::Button());
				 this->mainTableLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
				 this->viewTableLayoutPanel->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->FlatPictureBox))->BeginInit();
				 this->tabControl->SuspendLayout();
				 this->tabPage1->SuspendLayout();
				 this->toolbarTableLayoutPanel->SuspendLayout();
				 this->mainTableLayoutPanel->SuspendLayout();
				 this->SuspendLayout();
				 this->FlatViewTransducerList->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->FlatViewTransducerList->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
				 this->FlatViewTransducerList->FormattingEnabled = true;
				 this->FlatViewTransducerList->Location = System::Drawing::Point(0, 3);
				 this->FlatViewTransducerList->Margin = System::Windows::Forms::Padding(0);
				 this->FlatViewTransducerList->Name = L"FlatViewTransducerList";
				 this->FlatViewTransducerList->Size = System::Drawing::Size(192, 21);
				 this->FlatViewTransducerList->TabIndex = 0;
				 this->FlatViewTransducerList->SelectedIndexChanged += gcnew System::EventHandler(this, &BaseFlatView::FlatViewTransducerList_SelectedIndexChanged);
				 this->progressBar->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
				 this->progressBar->Location = System::Drawing::Point(602, 7);
				 this->progressBar->Margin = System::Windows::Forms::Padding(0);
				 this->progressBar->Name = L"progressBar";
				 this->progressBar->Size = System::Drawing::Size(82, 13);
				 this->progressBar->TabIndex = 0;
				 this->progressBar->Visible = false;
				 this->labelProgress->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
				 this->labelProgress->AutoSize = true;
				 this->labelProgress->Location = System::Drawing::Point(490, 7);
				 this->labelProgress->Margin = System::Windows::Forms::Padding(0);
				 this->labelProgress->Name = L"labelProgress";
				 this->labelProgress->Size = System::Drawing::Size(112, 13);
				 this->labelProgress->TabIndex = 1;
				 this->labelProgress->Text = L"Shoal extraction load :";
				 this->labelProgress->Visible = false;
				 this->buttonDockView->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
				 this->buttonDockView->Appearance = System::Windows::Forms::Appearance::Button;
				 this->buttonDockView->FlatAppearance->BorderSize = 0;
				 this->buttonDockView->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
				 this->buttonDockView->ImageIndex = 1;
				 this->buttonDockView->ImageList = this->imageListDock;
				 this->buttonDockView->Location = System::Drawing::Point(770, 3);
				 this->buttonDockView->Name = L"buttonDockView";
				 this->buttonDockView->Size = System::Drawing::Size(21, 21);
				 this->buttonDockView->TabIndex = 6;
				 this->buttonDockView->UseVisualStyleBackColor = false;
				 this->buttonDockView->CheckedChanged += gcnew System::EventHandler(this, &BaseFlatView::buttonDockView_CheckedChanged);
				 this->imageListDock->ImageStream = (cli::safe_cast<System::Windows::Forms::ImageListStreamer^>(resources->GetObject(L"imageListDock.ImageStream")));
				 this->imageListDock->TransparentColor = System::Drawing::Color::Transparent;
				 this->imageListDock->Images->SetKeyName(0, L"dock.png");
				 this->imageListDock->Images->SetKeyName(1, L"undock.png");
				 this->buttonHistoric->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
				 this->buttonHistoric->Appearance = System::Windows::Forms::Appearance::Button;
				 this->buttonHistoric->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"buttonHistoric.BackgroundImage")));
				 this->buttonHistoric->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Zoom;
				 this->buttonHistoric->FlatAppearance->BorderSize = 0;
				 this->buttonHistoric->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
				 this->buttonHistoric->Location = System::Drawing::Point(687, 3);
				 this->buttonHistoric->Name = L"buttonHistoric";
				 this->buttonHistoric->Size = System::Drawing::Size(21, 21);
				 this->buttonHistoric->TabIndex = 3;
				 this->buttonHistoric->UseVisualStyleBackColor = false;
				 this->buttonHistoric->Visible = false;
				 this->buttonAVI->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
				 this->buttonAVI->Appearance = System::Windows::Forms::Appearance::Button;
				 this->buttonAVI->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"buttonAVI.BackgroundImage")));
				 this->buttonAVI->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Zoom;
				 this->buttonAVI->Enabled = false;
				 this->buttonAVI->FlatAppearance->BorderSize = 0;
				 this->buttonAVI->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
				 this->buttonAVI->Location = System::Drawing::Point(714, 3);
				 this->buttonAVI->Name = L"buttonAVI";
				 this->buttonAVI->Size = System::Drawing::Size(21, 21);
				 this->buttonAVI->TabIndex = 4;
				 this->buttonAVI->UseVisualStyleBackColor = false;
				 this->buttonAVI->Visible = false;
				 this->buttonAVI->CheckedChanged += gcnew System::EventHandler(this, &BaseFlatView::buttonAVI_CheckedChanged);
				 this->saveAVIFileDialog->DefaultExt = L"avi";
				 this->saveAVIFileDialog->Filter = L"AVI File|*.avi";
				 this->saveAVIFileDialog->RestoreDirectory = true;
				 this->buttonAddView->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"buttonAddView.BackgroundImage")));
				 this->buttonAddView->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Zoom;
				 this->buttonAddView->FlatAppearance->BorderSize = 0;
				 this->buttonAddView->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
				 this->buttonAddView->Location = System::Drawing::Point(195, 3);
				 this->buttonAddView->Name = L"buttonAddView";
				 this->buttonAddView->Size = System::Drawing::Size(23, 21);
				 this->buttonAddView->TabIndex = 1;
				 this->buttonAddView->UseVisualStyleBackColor = true;
				 this->buttonAddView->Click += gcnew System::EventHandler(this, &BaseFlatView::buttonAddView_Click);
				 this->HistoricHScrollBar->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->HistoricHScrollBar->Location = System::Drawing::Point(0, 290);
				 this->HistoricHScrollBar->Name = L"HistoricHScrollBar";
				 this->HistoricHScrollBar->Size = System::Drawing::Size(802, 23);
				 this->HistoricHScrollBar->TabIndex = 0;
				 this->DepthVScrollBar->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->DepthVScrollBar->Location = System::Drawing::Point(802, 0);
				 this->DepthVScrollBar->Name = L"DepthVScrollBar";
				 this->DepthVScrollBar->Size = System::Drawing::Size(17, 290);
				 this->DepthVScrollBar->TabIndex = 2;
				 this->DepthVScrollBar->Value = 45;
				 this->DepthVScrollBar->ValueChanged += gcnew System::EventHandler(this, &BaseFlatView::DepthVScrollBar_ValueChanged);
				 this->viewTableLayoutPanel->ColumnCount = 2;
				 this->viewTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 100)));
				 this->viewTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->viewTableLayoutPanel->Controls->Add(this->HistoricHScrollBar, 0, 1);
				 this->viewTableLayoutPanel->Controls->Add(this->DepthVScrollBar, 1, 0);
				 this->viewTableLayoutPanel->Controls->Add(this->FlatPictureBox, 0, 0);
				 this->viewTableLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->viewTableLayoutPanel->Location = System::Drawing::Point(0, 0);
				 this->viewTableLayoutPanel->Margin = System::Windows::Forms::Padding(0);
				 this->viewTableLayoutPanel->Name = L"viewTableLayoutPanel";
				 this->viewTableLayoutPanel->RowCount = 2;
				 this->viewTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
					 100)));
				 this->viewTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
				 this->viewTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
					 20)));
				 this->viewTableLayoutPanel->Size = System::Drawing::Size(819, 313);
				 this->viewTableLayoutPanel->TabIndex = 6;
				 this->FlatPictureBox->BackColor = System::Drawing::SystemColors::Window;
				 this->FlatPictureBox->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
				 this->FlatPictureBox->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->FlatPictureBox->Location = System::Drawing::Point(0, 0);
				 this->FlatPictureBox->Margin = System::Windows::Forms::Padding(0);
				 this->FlatPictureBox->Name = L"FlatPictureBox";
				 this->FlatPictureBox->Size = System::Drawing::Size(802, 290);
				 this->FlatPictureBox->TabIndex = 3;
				 this->FlatPictureBox->TabStop = false;
				 this->FlatPictureBox->Text = L"control1";
				 this->FlatPictureBox->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &BaseFlatView::FlatPictureBox_Paint);
				 this->FlatPictureBox->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &BaseFlatView::FlatPictureBox_MouseDown);
				 this->FlatPictureBox->MouseEnter += gcnew System::EventHandler(this, &BaseFlatView::FlatPictureBox_MouseEnter);
				 this->FlatPictureBox->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &BaseFlatView::FlatPictureBox_MouseMove);
				 this->FlatPictureBox->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &BaseFlatView::FlatPictureBox_MouseUp);
				 this->FlatPictureBox->MouseWheel += gcnew System::Windows::Forms::MouseEventHandler(this, &BaseFlatView::FlatPictureBox_MouseWheel);
				 this->FlatPictureBox->Resize += gcnew System::EventHandler(this, &BaseFlatView::FlatPictureBox_Resize);
				 this->tabControl->Appearance = System::Windows::Forms::TabAppearance::Buttons;
				 this->tabControl->Controls->Add(this->tabPage1);
				 this->tabControl->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tabControl->ItemSize = System::Drawing::Size(0, 1);
				 this->tabControl->Location = System::Drawing::Point(0, 33);
				 this->tabControl->Margin = System::Windows::Forms::Padding(0);
				 this->tabControl->Name = L"tabControl";
				 this->tabControl->SelectedIndex = 0;
				 this->tabControl->Size = System::Drawing::Size(827, 322);
				 this->tabControl->SizeMode = System::Windows::Forms::TabSizeMode::Fixed;
				 this->tabControl->TabIndex = 8;
				 this->tabPage1->Controls->Add(this->viewTableLayoutPanel);
				 this->tabPage1->Location = System::Drawing::Point(4, 5);
				 this->tabPage1->Margin = System::Windows::Forms::Padding(0);
				 this->tabPage1->Name = L"tabPage1";
				 this->tabPage1->Size = System::Drawing::Size(819, 313);
				 this->tabPage1->TabIndex = 0;
				 this->tabPage1->Text = L"tabPage1";
				 this->tabPage1->UseVisualStyleBackColor = true;
				 this->toolbarTableLayoutPanel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
					 | System::Windows::Forms::AnchorStyles::Right));
				 this->toolbarTableLayoutPanel->ColumnCount = 11;
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 100)));
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->toolbarTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
				 this->toolbarTableLayoutPanel->Controls->Add(this->labelDettectedBottom, 3, 0);
				 this->toolbarTableLayoutPanel->Controls->Add(this->labelProgress, 4, 0);
				 this->toolbarTableLayoutPanel->Controls->Add(this->FlatViewTransducerList, 0, 0);
				 this->toolbarTableLayoutPanel->Controls->Add(this->buttonAddView, 1, 0);
				 this->toolbarTableLayoutPanel->Controls->Add(this->progressBar, 5, 0);
				 this->toolbarTableLayoutPanel->Controls->Add(this->buttonHistoric, 6, 0);
				 this->toolbarTableLayoutPanel->Controls->Add(this->buttonAVI, 7, 0);
				 this->toolbarTableLayoutPanel->Controls->Add(this->buttonSpectralView, 8, 0);
				 this->toolbarTableLayoutPanel->Controls->Add(this->buttonDockView, 9, 0);
				 this->toolbarTableLayoutPanel->Controls->Add(this->buttonSplitView, 2, 0);
				 this->toolbarTableLayoutPanel->Controls->Add(this->buttonCloseView, 10, 0);
				 this->toolbarTableLayoutPanel->Location = System::Drawing::Point(3, 3);
				 this->toolbarTableLayoutPanel->Name = L"toolbarTableLayoutPanel";
				 this->toolbarTableLayoutPanel->RowCount = 1;
				 this->toolbarTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
					 100)));
				 this->toolbarTableLayoutPanel->Size = System::Drawing::Size(821, 27);
				 this->toolbarTableLayoutPanel->TabIndex = 11;
				 this->labelDettectedBottom->Anchor = System::Windows::Forms::AnchorStyles::Left;
				 this->labelDettectedBottom->AutoSize = true;
				 this->labelDettectedBottom->Location = System::Drawing::Point(251, 7);
				 this->labelDettectedBottom->Name = L"labelDettectedBottom";
				 this->labelDettectedBottom->Size = System::Drawing::Size(110, 13);
				 this->labelDettectedBottom->TabIndex = 6;
				 this->labelDettectedBottom->Text = L"Bottom Depth : XXXm";
				 this->buttonSpectralView->Appearance = System::Windows::Forms::Appearance::Button;
				 this->buttonSpectralView->AutoSize = true;
				 this->buttonSpectralView->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
				 this->buttonSpectralView->Location = System::Drawing::Point(741, 3);
				 this->buttonSpectralView->Name = L"buttonSpectralView";
				 this->buttonSpectralView->Size = System::Drawing::Size(23, 21);
				 this->buttonSpectralView->TabIndex = 5;
				 this->buttonSpectralView->Text = L"F";
				 this->buttonSpectralView->UseVisualStyleBackColor = true;
				 this->buttonSpectralView->CheckedChanged += gcnew System::EventHandler(this, &BaseFlatView::buttonSpectralView_CheckedChanged);
				 this->buttonSplitView->BackgroundImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"buttonSplitView.BackgroundImage")));
				 this->buttonSplitView->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Zoom;
				 this->buttonSplitView->FlatAppearance->BorderSize = 0;
				 this->buttonSplitView->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
				 this->buttonSplitView->Location = System::Drawing::Point(224, 3);
				 this->buttonSplitView->Name = L"buttonSplitView";
				 this->buttonSplitView->Size = System::Drawing::Size(21, 21);
				 this->buttonSplitView->TabIndex = 2;
				 this->buttonSplitView->UseVisualStyleBackColor = true;
				 this->buttonSplitView->Click += gcnew System::EventHandler(this, &BaseFlatView::buttonSplitView_Click);
				 this->buttonCloseView->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
				 this->buttonCloseView->Location = System::Drawing::Point(797, 3);
				 this->buttonCloseView->Name = L"buttonCloseView";
				 this->buttonCloseView->Size = System::Drawing::Size(21, 21);
				 this->buttonCloseView->TabIndex = 7;
				 this->buttonCloseView->Text = L"X";
				 this->buttonCloseView->UseVisualStyleBackColor = true;
				 this->buttonCloseView->Click += gcnew System::EventHandler(this, &BaseFlatView::buttonCloseView_Click);
				 this->mainTableLayoutPanel->ColumnCount = 1;
				 this->mainTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 100)));
				 this->mainTableLayoutPanel->Controls->Add(this->tabControl, 0, 1);
				 this->mainTableLayoutPanel->Controls->Add(this->toolbarTableLayoutPanel, 0, 0);
				 this->mainTableLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->mainTableLayoutPanel->Location = System::Drawing::Point(0, 0);
				 this->mainTableLayoutPanel->Margin = System::Windows::Forms::Padding(0);
				 this->mainTableLayoutPanel->Name = L"mainTableLayoutPanel";
				 this->mainTableLayoutPanel->RowCount = 2;
				 this->mainTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
				 this->mainTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
					 100)));
				 this->mainTableLayoutPanel->Size = System::Drawing::Size(827, 355);
				 this->mainTableLayoutPanel->TabIndex = 12;
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->Controls->Add(this->mainTableLayoutPanel);
				 this->Margin = System::Windows::Forms::Padding(0);
				 this->Name = L"BaseFlatView";
				 this->Size = System::Drawing::Size(827, 355);
				 this->viewTableLayoutPanel->ResumeLayout(false);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->FlatPictureBox))->EndInit();
				 this->tabControl->ResumeLayout(false);
				 this->tabPage1->ResumeLayout(false);
				 this->toolbarTableLayoutPanel->ResumeLayout(false);
				 this->toolbarTableLayoutPanel->PerformLayout();
				 this->mainTableLayoutPanel->ResumeLayout(false);
				 this->ResumeLayout(false);

			 }
#pragma endregion

	protected:	double mCursorCenterXpercent;
	protected: virtual System::Void CursorHasChanged(double value) abstract;
	private: System::ComponentModel::IContainer^  components;
	private: int m_MouseX, m_MouseY;
			 // OTK - 05/06/2009 - Ajout mode zoom
	protected: bool m_bZoom;
	protected: bool m_bDrawingZone;
	protected: bool m_bRealPicture;
	protected: bool m_bFullyStretched; // �tirement complet
	protected: bool m_bEquallyStretched; // �tirement qui conserve les proportions
	protected: double m_StretchRatio;
	protected: System::Drawing::Rectangle m_DrawingRegion;

			   /*
			   // pile des zooms verticaux 
	protected: static System::Collections::Stack m_VerticalZoomStack;
			   // pile des zooms horizontaux 
	protected: static System::Collections::Stack m_HorizontalZoomStack;
				*/
				// pile des zooms  
	protected: static System::Collections::Stack m_ZoomStack;

			   // mode s�lection (pour R(f) par exemple)
	private: bool m_bSelection;

	protected: IViewSelectionObserver^  m_pViewSelectionObserver;

	private: DockFlatViewDelegate^    mDockFlatViewDelegate;

	protected: System::String^ m_ViewDesc;
	public: System::Void	SetCursorPosition(double NewPositionPercent);
	public: virtual System::Void	DrawNow();
	public: System::Void	SounderChanged();
	public: virtual System::Void PingFanAdded();
	public: virtual System::Void EsuClosed();
	public: virtual void UpdateParameter();
	public: virtual void PingCursorOffsetChanged(int offset);
	public: System::Void	StreamOpened();

	public: System::Windows::Forms::Control ^ GetFlatPictureBox() { return this->FlatPictureBox; };

	protected: GdiBmp ^ m_bmp;

	public:	System::Void	EnterZoomMode() { m_bZoom = true; LeaveSelectionMode(); notifyModeChanged(); };
	public:	System::Void	LeaveZoomMode() { m_bZoom = false;  notifyModeChanged(); };
	public:	System::Void	EnterSelectionMode() { m_bSelection = true; LeaveZoomMode();  notifyModeChanged(); };
	public:	System::Void	LeaveSelectionMode() { m_bSelection = false; notifyModeChanged(); };
	private: void notifyModeChanged()
	{
		if (ModeChanged) 
			ModeChanged();
	}

	protected: virtual System::Void UpdateHistoricSCrollBar() {};

	protected: DisplayView^ m_viewManager;
	protected: DisplayViewContainer ^m_viewContainer;
	protected: virtual TransducerContainerView ^GetFlatView() abstract;

			   // FlatViewTransducerList->SelectedIndex change quand on d�roule la liste d�roulante ce qui peut aboutir � un index incoh�rent
			   // On stocker l'index courant qui est mis � jour uniquement lors de FlatViewTransducerList_SelectedIndexChanged
	private: int m_currentIndex = -1;
	private: System::Object ^ m_currentItem = nullptr;
	public:	TransducerView* GetTransducerView();
	public: int GetCurrentIndex();

			/// Retourne l'ID du sondeur affich�e dans la vue
	public: int GetSounderId();
			
			/// Retourne l'index du transducteur
	public: unsigned int GetTransducerIndex();


		// Gestionnaire de selection
	public: virtual System::Void  SetViewSelectionObserver(IViewSelectionObserver^ pObs);
	public: IViewSelectionObserver^ GetViewSelectionObserver();
	protected: virtual void OnSelectionChanged(System::Object^  sender);

		//modifier le transducer s�lectionn�
	public: virtual void SetTransducer(String^ transducerName);

	//r�cup�rer le transducer s�lectionn�
	public: virtual String^ GetTransducer();

	public: delegate void TransducerChangedHandler(String ^ transducerName);
	public: event TransducerChangedHandler ^ TransducerChanged;

		// selection d'une region
	protected: virtual System::Void  OnRegionSelection() abstract;
			   // selection d'un point
	protected: virtual System::Void  OnShoalSelection(PointF pt) abstract;

			   //mapping d'un point en coord ecran vers coord r�elles
	protected: virtual PointF MapScreenToRealCoord(PointF screenCoord);
			   //mapping d'un point en coord r�elles vers coord �cran
	protected: virtual PointF MapRealToScreenCoord(PointF realCoord);

	public: bool m_NeedRedraw;

			//Effectue des op�rations sp�cifiques aux classes filles de BaseFlatView lors d'un changement de transducer
	protected: virtual System::Void OnTransducerSelectedIndexChanged(){}
	private: System::Void	FlatViewTransducerList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

	public: System::Void RedrawView();
	private: System::Void buttonAVI_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void FlatPictureBox_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e);
	protected: virtual System::Void DrawOnGraphics(Graphics^ e);
	protected: virtual System::Void FlatPictureBox_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
	protected: virtual System::Void FlatPictureBox_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
	protected: virtual System::Void FlatPictureBox_MouseWheel(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
	protected: virtual System::Void FlatPictureBox_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
	protected: virtual System::Void DrawCursor(Graphics^ e) = 0;
	protected: virtual System::Void DrawShoalId(Graphics^  e) = 0;
	protected: virtual System::Void DrawDistanceGrid(Graphics^  e) = 0;
	protected: virtual System::Void DrawDepthGrid(Graphics^  e);
	protected: virtual System::Void DrawESUGrid(Graphics^  e) {};
	protected: virtual System::Void DrawLayersContour(Graphics^  e) {};
	protected: virtual System::Void DrawSelectionZone(Graphics^  e);
	protected: virtual System::Void DrawSelectionWindow(Graphics^  e) {};
	protected: virtual System::Void DrawImage(Graphics^  e) {};
	protected: virtual System::Void DrawBottom(Graphics^  e) {};
	protected: virtual System::Void DrawRefNoise(Graphics^  e) {};
	protected: virtual System::Void DrawNoise(Graphics^  e) {};
	protected: virtual System::Void DrawContourLines(Graphics^  e) {};
	protected: virtual System::Void DrawDeviation(Graphics^  e) {};
	protected: virtual System::Void DrawSingleTargets(Graphics^  e) {}
	public:	virtual double GetStretchRatio() { return m_StretchRatio; };
	public:	virtual double GetVerticalStretchRatio();
	protected: virtual double GetHorizontalStretchRatio();
	public: virtual double GetHorizontalMargin();
	public: virtual double GetVerticalMargin();
	public: virtual System::String^ GetViewDesc() { return m_ViewDesc; };
	protected: virtual double GetRealDisplayWidth();
	protected: virtual double GetRealDisplayHeight();
	protected: virtual double ConvertStretchToFlatPercent(double percent);
	protected: virtual double ConvertFlatToStretchPercent(double percent);
	protected: virtual System::Void FlatPictureBox_Resize(System::Object^  sender, System::EventArgs^  e);
	protected: virtual System::Void HistoricHScrollBar_Scroll(System::Object^ sender, System::Windows::Forms::ScrollEventArgs^ e) {};
	protected: virtual System::Void UpdateTransformTable() {};
	protected: virtual System::Void UpdateTransformTable(int index) {};
	protected: virtual System::Void OnZoom() = 0;
	protected: virtual System::Void ZoomOut() = 0;
	protected: virtual System::Void UpdateCursor() {};
	protected: virtual System::Void AdjustCursorPosition() {};
	protected: virtual int GetDataWidth() { return FlatPictureBox->Width; };
	protected: virtual System::String^ FormatEchoInformation(int ScreenPosX, int ScreenPosY);

	private: System::Void buttonDockView_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonCloseView_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void InitToolTip();
	private: System::Void DockFlatView();
	private: System::Void OnExternalWindowClosing(System::Object ^sender, System::ComponentModel::CancelEventArgs ^e);

	private: ExternalViewWindow^ mExternalViewWindow;

	private: System::Collections::Generic::List<System::String^> ^ m_userControlNames;

	public: System::Void SelectUserControl(System::Windows::Forms::Control ^ userControl);
	public: System::Void AddUserControl(System::Windows::Forms::Control ^ userControl);
	public: System::Void RemoveUserControl(System::Windows::Forms::Control ^ userControl);
	public: bool IsUserControlVisible();
	public: virtual bool IsTSSpectrumSelected();
	public: virtual bool IsTSHistogramSelected();
			
			// OTK - 15/04/2010 - classe pour la cr�ation des vid�os AVIs des vues 2D
	private: AVIWriter^ m_pAVIWriter;

			 // NMD - Ajout d'un delegate pour controler la profondeur max depuis l'interface principale
	public: delegate void UpdateMaxDepthDelegate(int dMaxDepth);
	public: void SetUpdateMaxDepthDelegate(UpdateMaxDepthDelegate ^ _delegate) { m_updateMaxDepthDelegate = _delegate; }

	private: System::Void DepthVScrollBar_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: bool m_bUpdatingDepth;
	private: UpdateMaxDepthDelegate ^ m_updateMaxDepthDelegate;

	private: System::Void buttonAddView_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonSplitView_Click(System::Object^  sender, System::EventArgs^  e);
	public: delegate BaseFlatView ^ CreateNewViewHandler(bool detached);
	public: CreateNewViewHandler ^ CreateNewView;			
			
	public: delegate System::Void SelectedTransducerChangedHandler();
	public: SelectedTransducerChangedHandler ^ SelectedTransducerChanged;

	public: delegate System::Void SelectionModeChangedHandler();
	public: SelectionModeChangedHandler ^ ModeChanged;

	private: System::Void FlatPictureBox_MouseEnter(System::Object^  sender, System::EventArgs^  e);

	public: delegate System::Void OptionHandler(bool optionValue);
	private: System::Collections::Generic::List<OptionHandler^> m_optionHandlerList;
			 
	public: int AddInternalControl(InternalControl^ control); ///<Returns the index of the added internal control
	private: System::Collections::Generic::List<InternalControl^> m_internalControls;
	protected: void showInternalControl(int index);				///<0 is the main Transducer control
	protected: bool isInternalControlDisplayed(int index);		///<0 is the main Transducer control

	protected: virtual System::Void buttonSpectralView_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {}

	protected: virtual int getCurrentPingOffset() = 0;
	protected: double getDetectedBottom();
	public: virtual System::Void updateDetectedBottom();
			
	public: virtual FormWindowState getWindowState();
	public: virtual System::Void setWindowState(FormWindowState state);	
};

	/// <summary>
	/// Couple rmin rmax pour la m�morisation des zooms successifs
	/// </summary>
	public ref class RMinRMax {
	public:
		double RMin;
		double RMax;
	};

	public ref class ZoomSection {
	public:
		RMinRMax ^ horizontalZoom;
		RMinRMax ^ verticalZoom;
	};
}
