
#include "ShoalExtraction/ShoalExtractionOutput.h"
#include "ShoalExtraction/data/ShoalData.h"
#include "BaseVolumicView.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "vtkVolumeSingleEcho.h"
#include "vtkVolumePhaseEcho.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/M3DKernel.h"
#include "CameraSettingsForm.h"
#include "ColorPaletteChannel.h"

#include "vtkTextureObject.h"

#include <windows.h>


using namespace MOVIESVIEWCPP;

BaseVolumicView::BaseVolumicView(DisplayViewContainer ^refView, UpdateToolBarDelegate^ updateToolBarDelegate, REFRESH3D_CALLBACK refreshCB)
	: m_viewContainer(refView)
{
	m_pViewFilter = new ViewFilter();
	m_pPingFanContainer = new PingFan3DContainer();
	m_OptionSounderId = 0;
	// OTK - FAE034 - dans le cas ou on a un sondeur d'ID 0, on ne doit pas prendre en compte
	// la valeur m_OptionSounderId, initialis�e � 0.
	m_ValidOptionSounderId = false;
	mUpdateToolBarDelegate = updateToolBarDelegate;
	
	mContextMenuDelegate = gcnew ContextMenuDelegate(this, &BaseVolumicView::OnContextMenu);

	mWrap = gcnew vtkWrapper(refreshCB, mContextMenuDelegate);
	InitializeComponent();
	mWrap->InitWindow(picture3dView);

	this->splitView3D->Panel2->Controls->Add(this->picture3dView);

	splitView3D->Panel1Collapsed = true;
	this->checkBoxDisplaySingleEchos->Checked = false;
	// OTK - FAE 186 - d�sactivation par d�faut de l'affichage des �chos sous forme
	// de points pour ne pas avoir le double affichage par d�faut (voxels + points).
	this->checkBoxDisplayMultipleEchos->Checked = false;
	this->checkBoxUseGPSPositionning->Checked = true;

	//NMD - FAE122 - Affichage du mode "sample volume" par d�faut
	this->checkBoxDisplayVolumeSampled->Checked = true;

	this->checkBoxBeamVolume->Checked = true;
	this->checkBoxGraduations->Checked = true;
	this->checkBoxLabels->Checked = true;

	// NMD - Mantis1982 - Suppression de la limite du shift
	this->numericUpDown_shiftPingFan->Maximum = Decimal::MaxValue;
}

BaseVolumicView::~BaseVolumicView()
{
	if (components)
	{
		delete components;
	}

	delete mWrap;

	// OTK - FAE003 - fuite m�moire
	delete m_pViewFilter;
	delete m_pPingFanContainer;
}

System::Void BaseVolumicView::Object3dTreeView_AfterCheck(System::Object^  sender, System::Windows::Forms::TreeViewEventArgs^  e)
{
	if (e->Node->Tag)
	{
		bool valueChanged = false;
		if (e->Node->Checked)
		{
			((DataObject3DHull^)e->Node->Tag)->m_pRef->Show(valueChanged);
		}
		else
		{
			((DataObject3DHull^)e->Node->Tag)->m_pRef->Hide(valueChanged);
		}
		if (valueChanged)
		{
			// OTK - 11/05/2009 - on affiche les bancs si le sondeur est coch�
			if (((DataObject3DHull^)e->Node->Tag)->m_pRef->IsSounder())
			{
				Sounder3D *pSound = (Sounder3D *)((DataObject3DHull^)e->Node->Tag)->m_pRef;
				vtkSounder *pVtkSound = mWrap->GetSounderMgr()->GetSounder(pSound->getSounderID());
				pVtkSound->getSeaFloor()->setVisible(e->Node->Checked);
				pVtkSound->getVolumeShoal()->setVisible(e->Node->Checked);
				pVtkSound->getVolumeBeam()->setVisible(e->Node->Checked && checkBoxBeamVolume->Checked);
				pVtkSound->getVolumePhaseEcho()->setVisible(e->Node->Checked && checkBoxDisplayMultipleEchos->Checked);
			}
			this->GetViewFilter()->SetNeedToRegenerateVolume();
		}
	}
}

System::Void BaseVolumicView::Object3dTreeView_AfterSelect(System::Object^  sender, System::Windows::Forms::TreeViewEventArgs^  e)
{
	if (e->Node->Tag)
	{
		if (((DataObject3DHull^)e->Node->Tag)->m_pRef->IsSounder())
		{
			Sounder3D *pSound = (Sounder3D *)((DataObject3DHull^)e->Node->Tag)->m_pRef;
			SetOptionActiveSounder(pSound->getSounderID());
		}
	}
}

System::Void BaseVolumicView::FillObject3dTreeViewContent()
{
	this->Object3dTreeView->Nodes->Clear();
	mWrap->ClearAllSounder();
	this->Object3dTreeView->BeginUpdate();
	// OTK - 20/03/2009 - palette des couleurs par channel
	ColorPaletteChannel colorPaletteChannel;
	colorPaletteChannel.ReInit();
	// Add a root TreeNode for each Sounder object in the ArrayList.
	for (unsigned int i = 0; i < GetViewFilter()->GetSounderCount(); i++)
	{
		Sounder *pSound = GetViewFilter()->GetSounderIdx(i)->getSounder();
		System::Int32 ^sounderId = gcnew System::Int32(pSound->m_SounderId);
		mWrap->AddSounder(pSound->m_SounderId);
		System::Windows::Forms::TreeNode ^Node = gcnew TreeNode("Sounder Id[" + sounderId->ToString() + "]");
		Object3dTreeView->Nodes->Add(Node);
		Node->Checked = GetViewFilter()->GetSounderIdx(i)->isVisible();
		Node->Tag = gcnew DataObject3DHull(GetViewFilter()->GetSounderIdx(i));
		// OTK - 11/05/2009 - on affiche les bancs si le sondeur est coch�
		vtkSounder *pVtkSound = mWrap->GetSounderMgr()->GetSounder(GetViewFilter()->GetSounderIdx(i)->getSounderID());
		pVtkSound->getVolumeBeam()->setVisible(Node->Checked);
		pVtkSound->getVolumeShoal()->setVisible(Node->Checked);
		for (unsigned int idxTrans = 0; idxTrans < GetViewFilter()->GetSounderIdx(i)->GetTransducerCount(); idxTrans++)
		{
			Transducer3D *pTrans = GetViewFilter()->GetSounderIdx(i)->GetTransducerIdx(idxTrans);
			System::String ^NodeName = gcnew System::String(pTrans->GetTransducer()->m_transName);
			System::Windows::Forms::TreeNode ^transNode = Object3dTreeView->Nodes[i]->Nodes->Add(NodeName);
			transNode->Checked = pTrans->isVisible();
			transNode->Tag = gcnew DataObject3DHull(pTrans);
			// now add channel
			for (unsigned int idxChann = 0; idxChann < pTrans->getNumberOfChannel(); idxChann++)
			{
				Channel3D *pChan = pTrans->getChannelWithIdx(idxChann);
				System::String ^NodeName = gcnew System::String(pChan->getChannel()->m_channelName);
				System::Windows::Forms::TreeNode ^chanNode = transNode->Nodes->Add(NodeName);
				chanNode->Checked = pChan->isVisible();
				chanNode->Tag = gcnew DataObject3DHull(pChan);
				// OTK - 20/03/2009 - palette des couleurs par channel
				colorPaletteChannel.AddColorPointValue(pChan->getChannel()->getSoftwareChannelId());
			}
		}
	}

	// NMD - FAE 204 - Rechargement des param�tres d'affichage enregistr�s
	bool displayEchos = DisplayParameter::getInstance()->GetDisplayEchos();
	this->checkBoxDisplayMultipleEchos->Checked = displayEchos;
	this->GetViewFilter()->m_bDisplayEcho = displayEchos;

	bool displayVolumeSampled = DisplayParameter::getInstance()->GetDisplayVolumeSampled();
	this->checkBoxDisplayVolumeSampled->Checked = displayVolumeSampled;
	this->GetViewFilter()->m_bDisplayVolumeSampled = displayVolumeSampled;

	DisplayParameter::getInstance()->SetCurrentColorPaletteChannel(colorPaletteChannel);
	this->Object3dTreeView->EndUpdate();

	checkBox4->Checked = DisplayParameter::getInstance()->GetIsLinkSounderEnabled();
	checkBoxBeamVolume->Checked = DisplayParameter::getInstance()->GetIsBeamVolumeDisplayed();
	checkBoxGraduations->Checked = DisplayParameter::getInstance()->GetIsGradiationsDisplayed();
	checkBoxLabels->Checked = DisplayParameter::getInstance()->GetIsLabelsDisplayed();
	checkBoxSeaFloor->Checked = DisplayParameter::getInstance()->GetIsSeaFloorEnabled();
	numericUpDownSeaFloorScale->Value = System::Decimal(DisplayParameter::getInstance()->GetSeaFloorScale());
	numericUpDown6->Value = (System::Decimal)DisplayParameter::getInstance()->GetSingleEchoDisplaySize();
	checkBoxDisplaySingleEchos->Checked = DisplayParameter::getInstance()->GetIsSingleEchosDisplayed();
	checkBoxShoalVolume->Checked = DisplayParameter::getInstance()->GetIsShoalVolumesDisplayed();
	checkBoxSmoothShoalVolume->Checked = DisplayParameter::getInstance()->GetIsShoalVolumesSmoothed();
	checkBoxDisplayShoalLabel->Checked = DisplayParameter::getInstance()->GetIsShoalLabelsDisplayed();
	trackBarVerticalScale->Value = DisplayParameter::getInstance()->GetVerticalScale();

	short shoaldDisplayType = DisplayParameter::getInstance()->GetShoalDisplayType();
	switch (shoaldDisplayType) {
	case eVolume:
		radioButtonShoalVolume->Checked = true;
		break;
	case ePoints:
		radioButtonShoalPoints->Checked = true;
		break;
	case eBox:
		radioButtonShoalBox->Checked = true;
		break;
	case eDelaunay:
		radioButtonShoalDelaunay->Checked = true;
		break;
	}

	mWrap->LoadCameraSettings();
}

System::Void BaseVolumicView::OnActiveSounderIdChanged()
{
	//check if exist
	bool found = false;
	for (unsigned int i = 0; i < GetViewFilter()->GetSounderCount(); i++)
	{
		Sounder3D *pSound = GetViewFilter()->GetSounderIdx(i);
		if (pSound->getSounderID() == m_OptionSounderId)
		{
			found = true;
			break;
		}
	}

	if (!found)
	{
		this->groupBoxSounder->Enabled = false;
		this->groupBoxSounder->Text = "Sounder ?";
	}
	else
	{
		this->groupBoxSounder->Enabled = true;
		System::Int32 ^sounderId = gcnew System::Int32(m_OptionSounderId);
		this->groupBoxSounder->Text = "Sounder [" + sounderId->ToString() + "]";
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(this->m_OptionSounderId);
		if (pSound)
		{
			this->checkBox1->Checked = DisplayParameter::getInstance()->GetVolumicOpacity();
			numericUpDown5->Value = System::Decimal(DisplayParameter::getInstance()->GetVolumicOpacityFactor());

			this->numericUpDownSeaFloorScale->Value = System::Decimal(pSound->getSeaFloor()->m_floorScale);
			checkBoxSeaFloor->Checked = pSound->getSeaFloor()->isVisible();

			bool beamVisible = pSound->getVolumeBeam()->m_bDisplayVolume;
			checkBoxBeamVolume->Checked = beamVisible;
			checkBoxGraduations->Enabled = beamVisible;
			checkBoxLabels->Enabled = beamVisible;
			checkBoxGraduations->Checked = pSound->getVolumeBeam()->m_bDisplayGraduations;
			checkBoxLabels->Checked = pSound->getVolumeBeam()->m_bDisplayLabels;
			checkBoxShoalVolume->Checked = pSound->getVolumeShoal()->getDisplayShoal();
			checkBoxSmoothShoalVolume->Checked = pSound->getVolumeShoal()->getSmoothShoal();
			checkBoxDisplayShoalLabel->Checked = pSound->getVolumeShoal()->getDisplayLabels();
			TShoalDisplayType type = pSound->getVolumeShoal()->getDisplayType();
			switch (type)
			{
			case eVolume:
				this->radioButtonShoalVolume->Checked = true;
				break;
			case ePoints:
				this->radioButtonShoalPoints->Checked = true;
				break;
			case eBox:
				this->radioButtonShoalBox->Checked = true;
				break;
			}
		}

		this->checkBoxDisplayMultipleEchos->Checked = DisplayParameter::getInstance()->GetDisplayEchos();
		this->checkBoxDisplayVolumeSampled->Checked = DisplayParameter::getInstance()->GetDisplayVolumeSampled();

		this->numericUpDown_PingFanSeen->Value = DisplayParameter::getInstance()->GetPingFanLength();
		this->numericUpDown_shiftPingFan->Value = DisplayParameter::getInstance()->GetPingFanDelayed();
		this->checkBoxUseGPSPositionning->Checked = DisplayParameter::getInstance()->GetUseGPSPositionning();
		this->numericUpDownEchoDisplaySize->Value = System::Decimal(DisplayParameter::getInstance()->GetEchoDisplaySize());
	}
}

System::Void BaseVolumicView::OnContextMenu()
{
	std::int32_t handle = picture3dView->Handle.ToInt32();
	int x = System::Windows::Forms::Cursor::Position.X;
	int y = System::Windows::Forms::Cursor::Position.Y;
	SendMessageA((HWND)handle, WM_CONTEXTMENU, (WPARAM)handle, MAKELPARAM(x, y));
}

System::Void BaseVolumicView::SetOptionActiveSounder(std::uint32_t sounderId)
{
	if (m_OptionSounderId != sounderId || !m_ValidOptionSounderId)
	{
		m_OptionSounderId = sounderId;
		m_ValidOptionSounderId = true;
		OnActiveSounderIdChanged();
	}
}

System::Void BaseVolumicView::checkBox1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetVolumicOpacity(this->checkBox1->Checked);
	if (this->checkBox4->Checked) 
	{
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
			mWrap->SetSort(this->checkBox1->Checked, aIsSound);
		}
	}
	else 
	{
		mWrap->SetSort(this->checkBox1->Checked, m_OptionSounderId);
	}
	mWrap->Render();
}

System::Void BaseVolumicView::numericUpDown_PingFanSeen_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetPingFanLength(System::Decimal::ToInt32(this->numericUpDown_PingFanSeen->Value));
}

System::Void BaseVolumicView::numericUpDown_shiftPingFan_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetPingFanDelayed(System::Decimal::ToInt32(this->numericUpDown_shiftPingFan->Value));
}

System::Void BaseVolumicView::UpdateShiftPingFan(unsigned int shift) 
{
	if (shift >= this->numericUpDown_shiftPingFan->Minimum && shift <= this->numericUpDown_shiftPingFan->Maximum)
	{
		this->numericUpDown_shiftPingFan->Value = shift;
		DisplayParameter::getInstance()->SetPingFanDelayed(shift);//System::Decimal::ToInt32(this->numericUpDown_shiftPingFan->Value));
	}
}

System::Void BaseVolumicView::numericUpDownSeaFloorScale_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
{
	int value = System::Decimal::ToInt32(this->numericUpDownSeaFloorScale->Value);

	DisplayParameter::getInstance()->SetSeaFloorScale(value);

	if (this->checkBox4->Checked) {
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounderIdx(i);
			if (pSound)
				pSound->getSeaFloor()->m_floorScale = value;
		}
	}
	else {
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(m_OptionSounderId);
		if (pSound)
			pSound->getSeaFloor()->m_floorScale = value;
	}
}

System::Void BaseVolumicView::checkBoxSeaFloor_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetIsSeaFloorEnabled(checkBoxSeaFloor->Checked);

	if (this->checkBox4->Checked) {
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounderIdx(i);
			if (pSound)
				pSound->getSeaFloor()->setVisible(checkBoxSeaFloor->Checked);
		}
	}
	else {
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(m_OptionSounderId);
		if (pSound)
			pSound->getSeaFloor()->setVisible(checkBoxSeaFloor->Checked);
	}
	mWrap->Render();
}

System::Void BaseVolumicView::checkBox4_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetIsLinkSounderEnabled(checkBox4->Checked);

	if (this->groupBoxSounder->Enabled)
	{
		checkBox1_CheckedChanged(sender, e);
		numericUpDown_PingFanSeen_ValueChanged(sender, e);
		numericUpDown_shiftPingFan_ValueChanged(sender, e);
		numericUpDownSeaFloorScale_ValueChanged(sender, e);
		checkBoxSeaFloor_CheckedChanged(sender, e);
		numericUpDown5_ValueChanged(sender, e);
	}
	else
	{
		this->checkBox4->Checked = false;
	}
}

System::Void BaseVolumicView::checkBoxDisplayMultipleEchos_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	bool value = this->checkBoxDisplayMultipleEchos->Checked;
	DisplayParameter::getInstance()->SetDisplayEchos(value);
	this->GetViewFilter()->m_bDisplayEcho = value;
	this->GetViewFilter()->SetNeedToRegenerateVolume();
}

System::Void BaseVolumicView::checkBoxUseGPSPositionning_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetUseGPSPositionning(this->checkBoxUseGPSPositionning->Checked);
	for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
	{
		std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
		mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeShoal()->RecomputeActor(mWrap->ren1);
	}
	this->GetViewFilter()->SetNeedToRegenerateVolume();
}

System::Void BaseVolumicView::checkBoxDisplaySingleEchos_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	DisplayParameter::getInstance()->SetIsSingleEchosDisplayed(this->checkBoxDisplaySingleEchos->Checked);

	this->GetViewFilter()->m_bDisplaySingleEcho = this->checkBoxDisplaySingleEchos->Checked;
	this->GetViewFilter()->SetNeedToRegenerateVolume();
}

System::Void BaseVolumicView::numericUpDown5_ValueChanged(System::Object^  sender, System::EventArgs^  e) 
{
	double maValeur = System::Decimal::ToDouble(numericUpDown5->Value);
	DisplayParameter::getInstance()->SetVolumicOpacityFactor(maValeur);
	if (this->checkBox4->Checked) 
	{
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounderIdx(i);
			if (pSound)
				pSound->SetGlobalOpacity(maValeur);
		}
	}
	else
	{
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(m_OptionSounderId);
		if (pSound)
			pSound->SetGlobalOpacity(maValeur);
	}
	mWrap->RecomputePalette();
	mWrap->Render();
}
//// End Sounder specific 

System::Void BaseVolumicView::picture3dView_Resize(System::Object^  sender, System::EventArgs^  e) 
{
	System::Drawing::Rectangle rect = this->picture3dView->DisplayRectangle;
	mWrap->Resize(rect.Height, rect.Width);
	mWrap->Render();
}

ViewFilter* BaseVolumicView::GetViewFilter() 
{ 
	return m_pViewFilter; 
}

System::Void BaseVolumicView::DisplayCameraSettings()
{
	CameraSettings ^aref = gcnew CameraSettings();
	*aref = mWrap->GetCameraSettings();
	CameraSettingsForm ^form = gcnew  CameraSettingsForm(aref);
	form->m_updateDelegate = gcnew UpdateCameraDelegate(this, &BaseVolumicView::UpdateCamera);
	form->Show();
}

System::Void BaseVolumicView::LoadCameraSettings()
{
	mWrap->LoadCameraSettings();
}

System::Void BaseVolumicView::SaveCameraSettings()
{
	mWrap->SaveCameraSettings();
}

System::Void BaseVolumicView::UpdateCamera(CameraSettings ^ref)
{
	mWrap->ApplyCameraSettings(ref, false);
}

System::Void BaseVolumicView::Draw()
{
	TimeCounter draw3DTime;
	draw3DTime.StartCount();
	HacTime minTime, maxTime;
	std::uint64_t minId = 0;
	std::uint64_t maxId = 0;
	for (unsigned int i = 0; i < GetViewFilter()->GetSounderCount(); i++)
	{
		Sounder3D *p = GetViewFilter()->GetSounderIdx(i);
		mWrap->SetToWireFrame(this->checkBoxDisplaySingleEchos->Checked);
		MergedEchoList *pList = mWrap->GetBuildingVolume(p->getSounderID());
		if (pList)
		{
			unsigned int numAdded = m_pPingFanContainer->Concatenate(*pList, *GetViewFilter(), p->getSounderID(), minId, maxId, minTime, maxTime);
			if (numAdded > 0)
			{
				System::String ^label = gcnew System::String("PingFan ID ");
				label = label + minId;
				label = label + " to ";
				label = label + maxId;
				this->labelIdVisible->Text = label;
			}
		}
		mWrap->SetBuildingVolume(p->getSounderID());
	}

	if (mWrap->GetCameraSettings().m_autoReset)
	{
		mWrap->ResetCamera();
	}

	// OTK - 23/03/2009 - on ajoute le texte avant le render sinon on a une frame de retard
	size_t maxFan = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectCount();
	if (maxFan)
	{
		DatedObject *pFan = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(maxFan - 1);
		double minRange = ((PingFan*)pFan)->m_computePingFan.m_minRange;

		// OTK - Ajout temps de d�but de la fen�tre
		char date[255];
		minTime.GetTimeDesc(date, 254);
		System::String ^statText = gcnew System::String(date);
		maxTime.GetShortTimeDesc(date, 254);
		statText += " (" + Convert::ToString(minId) + ") to " + gcnew System::String(date) + " (" + Convert::ToString(maxId) + ")";
		if (((PingFan*)pFan)->m_computePingFan.m_maxRangeFound)
		{
			statText = statText + " ---- Range [ " + System::Double(minRange*0.001).ToString() + " ]";
		}
		else
		{
			statText = statText + " ---- Range [..]";
		}
		mWrap->SetStatText(statText);
	}

	// FRE - 28/09/2009 - MaJ fenetre d'affichage des bancs
	for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
	{
		// OTK - 04/05/2009 - purge des vieux bancs
		mWrap->GetSounderMgr()->GetSounderIdx(i)->getVolumeShoal()->PurgeOldShoals(mWrap);
		mWrap->GetSounderMgr()->GetSounderIdx(i)->getVolumeShoal()->UpdateDisplay();
	}

	mWrap->AviModified();
	mWrap->Render();

	try
	{
		mWrap->AviRendering();
	}
	catch (...)
	{
	}
	draw3DTime.StopCount();
	M3DKernel::GetInstance()->m_timeCounterRecord.AddTimeCounter("Volumic Draw", draw3DTime);
}

System::Void BaseVolumicView::trackBarVerticalScale_Scroll(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetVerticalScale(trackBarVerticalScale->Value);

	double vertScale = ((int)this->trackBarVerticalScale->Value) / 10.0;
	mWrap->SetVolumeScale(1, vertScale, 1);
	mWrap->Render();
}

System::Void BaseVolumicView::numericUpDown6_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
	DisplayParameter::getInstance()->SetSingleEchoDisplaySize(System::Decimal::ToDouble(this->numericUpDown6->Value));
	
	for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
	{
		vtkVolumeSingleEcho* p = mWrap->GetSounderMgr()->GetSounderIdx(i)->getVolumeSingleEcho();
		double size = System::Decimal::ToDouble(this->numericUpDown6->Value);
		p->SetSize(size);
	}
	mWrap->Render();
}

System::Void BaseVolumicView::sounderParameterToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
}

System::Void BaseVolumicView::PingFanAdded()
{
	m_pPingFanContainer->PingFanAdded();
}

System::Void BaseVolumicView::StreamOpened()
{
	// FRE - 28/09/2009 - RaZ fenetre d'affichage des bancs
	for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
	{
		mWrap->GetSounderMgr()->GetSounderIdx(i)->getVolumeShoal()->Reset(mWrap);
	}
	m_pPingFanContainer->RemoveAllPingFan();
}

System::Void BaseVolumicView::SounderChanged()
{
	m_pPingFanContainer->RemoveAllPingFan();
	GetViewFilter()->UpdateSounderDef();

	FillObject3dTreeViewContent();
	double singleEchoDisplaySize = 1;
	double echoDisplaySize = DisplayParameter::getInstance()->GetEchoDisplaySize();
	if (mWrap->GetSounderMgr()->GetNbSounder() > 0)
	{
		vtkVolumeSingleEcho* p = mWrap->GetSounderMgr()->GetSounderIdx(0)->getVolumeSingleEcho();
		singleEchoDisplaySize = p->GetSize();
	}
	this->numericUpDown6->Value = System::Decimal(singleEchoDisplaySize);
	this->numericUpDownEchoDisplaySize->Value = System::Decimal(echoDisplaySize);
	this->checkBox1->Checked = DisplayParameter::getInstance()->GetVolumicOpacity();
	this->numericUpDown5->Value = System::Decimal(DisplayParameter::getInstance()->GetVolumicOpacityFactor());

	if (this->checkBox4->Checked) 
	{
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
			mWrap->SetSort(this->checkBox1->Checked, aIsSound);
		}
	}
	else 
	{
		mWrap->SetSort(this->checkBox1->Checked, m_OptionSounderId);
	}

	mWrap->setBackGroundColor(DisplayParameter::getInstance()->GetVolumicBackgroundRed(),
		DisplayParameter::getInstance()->GetVolumicBackgroundGreen(),
		DisplayParameter::getInstance()->GetVolumicBackgroundBlue());
}

// OTK - 30/04/2009 - ajout des bancs en 3D
System::Void	BaseVolumicView::AddShoal(ShoalExtractionOutput * pOut)
{
	int sounderId = pOut->m_pClosedShoal->GetSounderId();
	mWrap->GetSounderMgr()->GetSounder(sounderId)->getVolumeShoal()->AddShoal(pOut, mWrap);
}

// NMD - 08/09/2011 - FAE 092 - affichage des volumes insonifi�
System::Void BaseVolumicView::checkBoxDisplayVolumeSampled_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	bool value = this->checkBoxDisplayVolumeSampled->Checked;
	DisplayParameter::getInstance()->SetDisplayVolumeSampled(value);
	this->GetViewFilter()->m_bDisplayVolumeSampled = value;
	this->GetViewFilter()->SetNeedToRegenerateVolume();
}

// NMD - 08/09/2011 - FAE 092 - modification de la taille des echos
System::Void BaseVolumicView::numericUpDownEchoDisplaySize_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
	double size = System::Decimal::ToDouble(this->numericUpDownEchoDisplaySize->Value);
	DisplayParameter::getInstance()->SetEchoDisplaySize(size);
	for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
	{
		mWrap->GetSounderMgr()->GetSounderIdx(i)->getVolumePhaseEcho()->SetSize(size);
	}
	mWrap->Render();
}

System::Void BaseVolumicView::backgroundColorToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayBackGroundColorChoice();
}

System::Void BaseVolumicView::DisplayBackGroundColorChoice()
{
	colorDialogBackGround->Color = Color::FromArgb(
		DisplayParameter::getInstance()->GetVolumicBackgroundRed() * 255,
		DisplayParameter::getInstance()->GetVolumicBackgroundGreen() * 255,
		DisplayParameter::getInstance()->GetVolumicBackgroundBlue() * 255);

	if (this->colorDialogBackGround->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		double r = colorDialogBackGround->Color.R / 255.0;
		double g = colorDialogBackGround->Color.G / 255.0;
		double b = colorDialogBackGround->Color.B / 255.0;
		DisplayParameter::getInstance()->SetVolumicBackgroundColor(r, g, b);
		mWrap->setBackGroundColor(r, g, b);
		mWrap->Render();
	}
}

System::Void BaseVolumicView::cameraSettingsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayCameraSettings();
}

System::Void BaseVolumicView::sounderParameterToolStripMenuItem_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	if (sounderParameterToolStripMenuItem->Checked == true)
	{
		splitView3D->Panel1Collapsed = false;
	}
	else
	{
		splitView3D->Panel1Collapsed = true;
	}
	mUpdateToolBarDelegate->Invoke();
}

System::Void BaseVolumicView::checkBoxBeamVolume_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetIsBeamVolumeDisplayed(checkBoxBeamVolume->Checked);

	this->checkBoxGraduations->Enabled = checkBoxBeamVolume->Checked;
	this->checkBoxLabels->Enabled = checkBoxBeamVolume->Checked;

	if (this->checkBox4->Checked) 
	{
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
			mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeBeam()->setVolumeVisible(checkBoxBeamVolume->Checked);
			mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeBeam()->setVisible(checkBoxBeamVolume->Checked);
		}
	}
	else
	{
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(m_OptionSounderId);
		if (pSound)
		{
			pSound->getVolumeBeam()->setVolumeVisible(checkBoxBeamVolume->Checked);
			pSound->getVolumeBeam()->setVisible(checkBoxBeamVolume->Checked);
		}
	}
	mWrap->Render();
}

System::Void BaseVolumicView::checkBoxGraduations_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	DisplayParameter::getInstance()->SetIsGraduationsDisplayed(checkBoxGraduations->Checked);

	if (this->checkBox4->Checked) 
	{
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
			mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeBeam()->setGraduationsVisible(checkBoxGraduations->Checked);
			mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeBeam()->setVisible(checkBoxBeamVolume->Checked);
		}
	}
	else
	{
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(m_OptionSounderId);
		if (pSound)
		{
			pSound->getVolumeBeam()->setGraduationsVisible(checkBoxGraduations->Checked);
			pSound->getVolumeBeam()->setVisible(checkBoxBeamVolume->Checked);
		}
	}
	mWrap->Render();
}

System::Void BaseVolumicView::checkBoxLabels_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetIsLabelsDisplayed(checkBoxLabels->Checked);

	if (this->checkBox4->Checked) {
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
			mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeBeam()->setLabelsVisible(checkBoxLabels->Checked);
			mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeBeam()->setVisible(checkBoxBeamVolume->Checked);
		}
	}
	else
	{
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(m_OptionSounderId);
		if (pSound)
		{
			pSound->getVolumeBeam()->setLabelsVisible(checkBoxLabels->Checked);
			pSound->getVolumeBeam()->setVisible(checkBoxBeamVolume->Checked);
		}
	}
	mWrap->Render();
}

System::Void BaseVolumicView::checkBoxShoalVolume_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetIsShoalVolumesDisplayed(this->checkBoxShoalVolume->Checked);

	if (this->checkBox4->Checked) {
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
			mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeShoal()->setDisplayShoal(checkBoxShoalVolume->Checked, mWrap->ren1);
		}
	}
	else
	{
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(m_OptionSounderId);
		if (pSound)
		{
			pSound->getVolumeShoal()->setDisplayShoal(checkBoxShoalVolume->Checked, mWrap->ren1);
		}
	}
	mWrap->Render();
}

System::Void BaseVolumicView::checkBoxSmoothShoalVolume_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetIsShoalVolumesSmoothed(this->checkBoxSmoothShoalVolume->Checked);

	if (this->checkBox4->Checked) {
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
			mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeShoal()->setSmoothShoal(checkBoxSmoothShoalVolume->Checked, mWrap->ren1);
		}
	}
	else
	{
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(m_OptionSounderId);
		if (pSound)
		{
			pSound->getVolumeShoal()->setSmoothShoal(checkBoxSmoothShoalVolume->Checked, mWrap->ren1);
		}
	}
	mWrap->Render();
}

System::Void BaseVolumicView::checkBoxDisplayShoalLabel_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetIsShoalLabelsDisplayed(this->checkBoxDisplayShoalLabel->Checked);

	if (this->checkBox4->Checked) {
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
			mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeShoal()->setDisplayLabels(checkBoxDisplayShoalLabel->Checked, mWrap->ren1);
		}
	}
	else
	{
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(m_OptionSounderId);
		if (pSound)
		{
			pSound->getVolumeShoal()->setDisplayLabels(checkBoxDisplayShoalLabel->Checked, mWrap->ren1);
		}
	}
	mWrap->Render();
}

System::Void BaseVolumicView::radioButtonShoalVolume_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	UpdateShoalDisplayType();
}

System::Void BaseVolumicView::radioButtonShoalPoints_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	UpdateShoalDisplayType();
}

System::Void BaseVolumicView::radioButtonShoalBox_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	UpdateShoalDisplayType();
}

System::Void BaseVolumicView::radioButtonShoalDelaunay_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	UpdateShoalDisplayType();
}

System::Void BaseVolumicView::UpdateShoalDisplayType()
{
	TShoalDisplayType type;
	if (radioButtonShoalVolume->Checked)
	{
		type = eVolume;
	}
	else if (radioButtonShoalPoints->Checked)
	{
		type = ePoints;
	}
	else if (radioButtonShoalBox->Checked)
	{
		type = eBox;
	}
	else
	{
		type = eDelaunay;
	}

	DisplayParameter::getInstance()->SetShoalDisplayType(type);

	if (this->checkBox4->Checked)
	{
		for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
		{
			std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
			mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeShoal()->setDisplayType(type, mWrap->ren1);
		}
	}
	else
	{
		vtkSounder *pSound = mWrap->GetSounderMgr()->GetSounder(m_OptionSounderId);
		if (pSound)
		{
			pSound->getVolumeShoal()->setDisplayType(type, mWrap->ren1);
		}
	}
	mWrap->Render();
}

System::Void BaseVolumicView::RecomputeShoalDisplay()
{
	for (unsigned int i = 0; i < mWrap->GetSounderMgr()->GetNbSounder(); i++)
	{
		std::uint32_t aIsSound = mWrap->GetSounderMgr()->GetIdSounder(i);
		mWrap->GetSounderMgr()->GetSounder(aIsSound)->getVolumeShoal()->RecomputeActor(mWrap->ren1);
	}
}

bool BaseVolumicView::is3DViewFrozen()
{
	return mWrap->isFrozen();
}

System::Void BaseVolumicView::recomputeView()
{
	m_pPingFanContainer->Build(*m_pViewFilter);
}

bool BaseVolumicView::SounderParameterVisible::get()
{
	return sounderParameterToolStripMenuItem->Checked;
}

void BaseVolumicView::SounderParameterVisible::set(bool value)
{
	sounderParameterToolStripMenuItem->Checked = value;
}

void BaseVolumicView::updateParameters()
{
	mWrap->UpdateParameter();
}

void BaseVolumicView::addPlaneWidget()
{
	mWrap->AddPlaneWidget();
}

void BaseVolumicView::removePlaneWidget()
{
	mWrap->RemovePlaneWidget();
}

bool BaseVolumicView::IsAviRendering::get()  
{
	return mWrap->IsAviRendering();
}

void BaseVolumicView::saveAvi(System::String ^fileName)
{
	mWrap->SaveAvi(fileName);
}

void BaseVolumicView::startAviRendering(System::String ^fileName) 
{
	mWrap->StartAviRendering(fileName);
}

void BaseVolumicView::stopAviRendering()
{
	mWrap->StopAviRendering();
}
