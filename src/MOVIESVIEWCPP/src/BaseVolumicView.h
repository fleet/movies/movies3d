#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


#include "ViewFilter.h"
#include "PingFan3DContainer.h"
#include "DisplayViewContainer.h"
#include "vtkWrapper.h"
#include "vtkVolumeShoal.h"
class PingFan;
class ShoalExtractionOutput;

namespace MOVIESVIEWCPP {

	// Pour mise � jour de la toolbar principale en fonction des actions sur la vue 3D
	public delegate void UpdateToolBarDelegate();

	/// <summary>
	/// Description r�sum�e de BaseVolumicView
	/// </summary>
	public ref class BaseVolumicView : public System::Windows::Forms::UserControl
	{
	public:
		BaseVolumicView(DisplayViewContainer ^refView, UpdateToolBarDelegate^ updateToolBarDelegate, REFRESH3D_CALLBACK refreshCB);
	private:	DisplayViewContainer ^m_viewContainer;
	private: System::Windows::Forms::GroupBox^  groupBoxSounder;
	private: System::Windows::Forms::GroupBox^  groupBox4;
	private: System::Windows::Forms::CheckBox^  checkBoxDisplaySingleEchos;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown6;
	private: System::Windows::Forms::TrackBar^  trackBarVerticalScale;
	private: System::Windows::Forms::Label^  labelIdVisible;
	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::CheckBox^  checkBoxDisplayMultipleEchos;
	private: System::Windows::Forms::CheckBox^  checkBox4;
	private: System::Windows::Forms::Panel^  panel2;
	private: System::Windows::Forms::CheckBox^  checkBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown5;
	private: System::Windows::Forms::CheckBox^  checkBoxSeaFloor;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownSeaFloorScale;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown_PingFanSeen;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown_shiftPingFan;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip3d;
	private: System::Windows::Forms::ToolStripMenuItem^  cameraSettingsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  sounderParameterToolStripMenuItem;
	private: System::Windows::Forms::SplitContainer^  splitContainer1;
	private: System::Windows::Forms::ToolStripMenuItem^  backgroundColorToolStripMenuItem;
	private: System::Windows::Forms::ColorDialog^  colorDialogBackGround;
	private: System::Windows::Forms::GroupBox^  groupBox5;
	private: System::Windows::Forms::CheckBox^  checkBoxLabels;
	private: System::Windows::Forms::CheckBox^  checkBoxUseGPSPositionning;
	private: System::Windows::Forms::CheckBox^  checkBoxGraduations;
	private: System::Windows::Forms::CheckBox^  checkBoxBeamVolume;
	private: System::Windows::Forms::CheckBox^  checkBoxShoalVolume;
	private: System::Windows::Forms::CheckBox^  checkBoxSmoothShoalVolume;
	private: System::Windows::Forms::RadioButton^  radioButtonShoalBox;
	private: System::Windows::Forms::RadioButton^  radioButtonShoalPoints;
	private: System::Windows::Forms::RadioButton^  radioButtonShoalVolume;
	private: System::Windows::Forms::RadioButton^  radioButtonShoalDelaunay;
	private: System::Windows::Forms::CheckBox^  checkBoxDisplayShoalLabel;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownEchoDisplaySize;
	private: System::Windows::Forms::CheckBox^  checkBoxDisplayVolumeSampled;


	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~BaseVolumicView();

	private: System::Windows::Forms::SplitContainer^  splitView3D;
	private: System::Windows::Forms::TreeView^  Object3dTreeView;
	private: System::Windows::Forms::PictureBox^  picture3dView;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>


#pragma region Windows Form Designer generated code
	/// <summary>
	/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
	/// le contenu de cette m�thode avec l'�diteur de code.
	/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->splitView3D = (gcnew System::Windows::Forms::SplitContainer());
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->Object3dTreeView = (gcnew System::Windows::Forms::TreeView());
			this->groupBoxSounder = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxDisplayShoalLabel = (gcnew System::Windows::Forms::CheckBox());
			this->radioButtonShoalDelaunay = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonShoalBox = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonShoalPoints = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonShoalVolume = (gcnew System::Windows::Forms::RadioButton());
			this->checkBoxSmoothShoalVolume = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxShoalVolume = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxLabels = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxGraduations = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxBeamVolume = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxDisplaySingleEchos = (gcnew System::Windows::Forms::CheckBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->numericUpDown6 = (gcnew System::Windows::Forms::NumericUpDown());
			this->trackBarVerticalScale = (gcnew System::Windows::Forms::TrackBar());
			this->labelIdVisible = (gcnew System::Windows::Forms::Label());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxDisplayVolumeSampled = (gcnew System::Windows::Forms::CheckBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownEchoDisplaySize = (gcnew System::Windows::Forms::NumericUpDown());
			this->checkBoxDisplayMultipleEchos = (gcnew System::Windows::Forms::CheckBox());
			this->checkBox4 = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxUseGPSPositionning = (gcnew System::Windows::Forms::CheckBox());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->numericUpDown5 = (gcnew System::Windows::Forms::NumericUpDown());
			this->checkBoxSeaFloor = (gcnew System::Windows::Forms::CheckBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->numericUpDownSeaFloorScale = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown_PingFanSeen = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown_shiftPingFan = (gcnew System::Windows::Forms::NumericUpDown());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->picture3dView = (gcnew System::Windows::Forms::PictureBox());
			this->contextMenuStrip3d = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->cameraSettingsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->sounderParameterToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->backgroundColorToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->colorDialogBackGround = (gcnew System::Windows::Forms::ColorDialog());
			this->splitView3D->Panel1->SuspendLayout();
			this->splitView3D->SuspendLayout();
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->groupBoxSounder->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarVerticalScale))->BeginInit();
			this->groupBox3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownEchoDisplaySize))->BeginInit();
			this->panel2->SuspendLayout();
			this->groupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown5))->BeginInit();
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownSeaFloorScale))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown_PingFanSeen))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown_shiftPingFan))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture3dView))->BeginInit();
			this->contextMenuStrip3d->SuspendLayout();
			this->SuspendLayout();
			// 
			// splitView3D
			// 
			this->splitView3D->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitView3D->Location = System::Drawing::Point(0, 0);
			this->splitView3D->Margin = System::Windows::Forms::Padding(0);
			this->splitView3D->Name = L"splitView3D";
			// 
			// splitView3D.Panel1
			// 
			this->splitView3D->Panel1->Controls->Add(this->splitContainer1);
			this->splitView3D->Panel1MinSize = 0;
			this->splitView3D->Size = System::Drawing::Size(989, 427);
			this->splitView3D->SplitterDistance = 505;
			this->splitView3D->TabIndex = 2;
			// 
			// splitContainer1
			// 
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->Location = System::Drawing::Point(0, 0);
			this->splitContainer1->Name = L"splitContainer1";
			this->splitContainer1->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->Controls->Add(this->Object3dTreeView);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->groupBoxSounder);
			this->splitContainer1->Size = System::Drawing::Size(505, 427);
			this->splitContainer1->SplitterDistance = 146;
			this->splitContainer1->TabIndex = 24;
			// 
			// Object3dTreeView
			// 
			this->Object3dTreeView->CheckBoxes = true;
			this->Object3dTreeView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->Object3dTreeView->Location = System::Drawing::Point(0, 0);
			this->Object3dTreeView->Margin = System::Windows::Forms::Padding(0);
			this->Object3dTreeView->Name = L"Object3dTreeView";
			this->Object3dTreeView->Size = System::Drawing::Size(505, 146);
			this->Object3dTreeView->TabIndex = 0;
			this->Object3dTreeView->AfterCheck += gcnew System::Windows::Forms::TreeViewEventHandler(this, &BaseVolumicView::Object3dTreeView_AfterCheck);
			this->Object3dTreeView->AfterSelect += gcnew System::Windows::Forms::TreeViewEventHandler(this, &BaseVolumicView::Object3dTreeView_AfterSelect);
			// 
			// groupBoxSounder
			// 
			this->groupBoxSounder->Controls->Add(this->checkBoxDisplayShoalLabel);
			this->groupBoxSounder->Controls->Add(this->radioButtonShoalDelaunay);
			this->groupBoxSounder->Controls->Add(this->radioButtonShoalBox);
			this->groupBoxSounder->Controls->Add(this->radioButtonShoalPoints);
			this->groupBoxSounder->Controls->Add(this->radioButtonShoalVolume);
			this->groupBoxSounder->Controls->Add(this->checkBoxSmoothShoalVolume);
			this->groupBoxSounder->Controls->Add(this->checkBoxShoalVolume);
			this->groupBoxSounder->Controls->Add(this->groupBox5);
			this->groupBoxSounder->Controls->Add(this->groupBox4);
			this->groupBoxSounder->Controls->Add(this->trackBarVerticalScale);
			this->groupBoxSounder->Controls->Add(this->labelIdVisible);
			this->groupBoxSounder->Controls->Add(this->groupBox3);
			this->groupBoxSounder->Controls->Add(this->panel2);
			this->groupBoxSounder->Controls->Add(this->checkBoxSeaFloor);
			this->groupBoxSounder->Controls->Add(this->label1);
			this->groupBoxSounder->Controls->Add(this->groupBox1);
			this->groupBoxSounder->Controls->Add(this->numericUpDown_PingFanSeen);
			this->groupBoxSounder->Controls->Add(this->numericUpDown_shiftPingFan);
			this->groupBoxSounder->Controls->Add(this->label4);
			this->groupBoxSounder->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxSounder->Enabled = false;
			this->groupBoxSounder->Location = System::Drawing::Point(0, 0);
			this->groupBoxSounder->Margin = System::Windows::Forms::Padding(0);
			this->groupBoxSounder->Name = L"groupBoxSounder";
			this->groupBoxSounder->Padding = System::Windows::Forms::Padding(0);
			this->groupBoxSounder->Size = System::Drawing::Size(505, 277);
			this->groupBoxSounder->TabIndex = 16;
			this->groupBoxSounder->TabStop = false;
			this->groupBoxSounder->Text = L"Unknown sounder";
			// 
			// checkBoxDisplayShoalLabel
			// 
			this->checkBoxDisplayShoalLabel->AutoSize = true;
			this->checkBoxDisplayShoalLabel->Location = System::Drawing::Point(267, 173);
			this->checkBoxDisplayShoalLabel->Name = L"checkBoxDisplayShoalLabel";
			this->checkBoxDisplayShoalLabel->Size = System::Drawing::Size(124, 17);
			this->checkBoxDisplayShoalLabel->TabIndex = 31;
			this->checkBoxDisplayShoalLabel->Text = L"Display Shoal Labels";
			this->checkBoxDisplayShoalLabel->UseVisualStyleBackColor = true;
			this->checkBoxDisplayShoalLabel->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxDisplayShoalLabel_CheckedChanged);
			// 
			// radioButtonShoalDelaunay
			// 
			this->radioButtonShoalDelaunay->AutoSize = true;
			this->radioButtonShoalDelaunay->Location = System::Drawing::Point(335, 209);
			this->radioButtonShoalDelaunay->Name = L"radioButtonShoalDelaunay";
			this->radioButtonShoalDelaunay->Size = System::Drawing::Size(70, 17);
			this->radioButtonShoalDelaunay->TabIndex = 30;
			this->radioButtonShoalDelaunay->TabStop = true;
			this->radioButtonShoalDelaunay->Text = L"Delaunay";
			this->radioButtonShoalDelaunay->UseVisualStyleBackColor = true;
			this->radioButtonShoalDelaunay->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::radioButtonShoalDelaunay_CheckedChanged);
			// 
			// radioButtonShoalBox
			// 
			this->radioButtonShoalBox->AutoSize = true;
			this->radioButtonShoalBox->Location = System::Drawing::Point(267, 209);
			this->radioButtonShoalBox->Name = L"radioButtonShoalBox";
			this->radioButtonShoalBox->Size = System::Drawing::Size(43, 17);
			this->radioButtonShoalBox->TabIndex = 29;
			this->radioButtonShoalBox->TabStop = true;
			this->radioButtonShoalBox->Text = L"Box";
			this->radioButtonShoalBox->UseVisualStyleBackColor = true;
			this->radioButtonShoalBox->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::radioButtonShoalBox_CheckedChanged);
			// 
			// radioButtonShoalPoints
			// 
			this->radioButtonShoalPoints->AutoSize = true;
			this->radioButtonShoalPoints->Location = System::Drawing::Point(335, 193);
			this->radioButtonShoalPoints->Name = L"radioButtonShoalPoints";
			this->radioButtonShoalPoints->Size = System::Drawing::Size(54, 17);
			this->radioButtonShoalPoints->TabIndex = 28;
			this->radioButtonShoalPoints->TabStop = true;
			this->radioButtonShoalPoints->Text = L"Points";
			this->radioButtonShoalPoints->UseVisualStyleBackColor = true;
			this->radioButtonShoalPoints->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::radioButtonShoalPoints_CheckedChanged);
			// 
			// radioButtonShoalVolume
			// 
			this->radioButtonShoalVolume->AutoSize = true;
			this->radioButtonShoalVolume->Location = System::Drawing::Point(267, 193);
			this->radioButtonShoalVolume->Name = L"radioButtonShoalVolume";
			this->radioButtonShoalVolume->Size = System::Drawing::Size(65, 17);
			this->radioButtonShoalVolume->TabIndex = 27;
			this->radioButtonShoalVolume->TabStop = true;
			this->radioButtonShoalVolume->Text = L"Volumes";
			this->radioButtonShoalVolume->UseVisualStyleBackColor = true;
			this->radioButtonShoalVolume->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::radioButtonShoalVolume_CheckedChanged);
			// 
			// checkBoxSmoothShoalVolume
			// 
			this->checkBoxSmoothShoalVolume->AutoSize = true;
			this->checkBoxSmoothShoalVolume->Location = System::Drawing::Point(267, 154);
			this->checkBoxSmoothShoalVolume->Name = L"checkBoxSmoothShoalVolume";
			this->checkBoxSmoothShoalVolume->Size = System::Drawing::Size(135, 17);
			this->checkBoxSmoothShoalVolume->TabIndex = 26;
			this->checkBoxSmoothShoalVolume->Text = L"Smooth Shoal Volumes";
			this->checkBoxSmoothShoalVolume->UseVisualStyleBackColor = true;
			this->checkBoxSmoothShoalVolume->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxSmoothShoalVolume_CheckedChanged);
			// 
			// checkBoxShoalVolume
			// 
			this->checkBoxShoalVolume->AutoSize = true;
			this->checkBoxShoalVolume->Location = System::Drawing::Point(267, 135);
			this->checkBoxShoalVolume->Name = L"checkBoxShoalVolume";
			this->checkBoxShoalVolume->Size = System::Drawing::Size(133, 17);
			this->checkBoxShoalVolume->TabIndex = 25;
			this->checkBoxShoalVolume->Text = L"Display Shoal Volumes";
			this->checkBoxShoalVolume->UseVisualStyleBackColor = true;
			this->checkBoxShoalVolume->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxShoalVolume_CheckedChanged);
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->checkBoxLabels);
			this->groupBox5->Controls->Add(this->checkBoxGraduations);
			this->groupBox5->Controls->Add(this->checkBoxBeamVolume);
			this->groupBox5->Location = System::Drawing::Point(10, 224);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(364, 42);
			this->groupBox5->TabIndex = 24;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"Beam Volume Parameter";
			// 
			// checkBoxLabels
			// 
			this->checkBoxLabels->AutoSize = true;
			this->checkBoxLabels->Location = System::Drawing::Point(269, 18);
			this->checkBoxLabels->Name = L"checkBoxLabels";
			this->checkBoxLabels->Size = System::Drawing::Size(94, 17);
			this->checkBoxLabels->TabIndex = 2;
			this->checkBoxLabels->Text = L"Display Labels";
			this->checkBoxLabels->UseVisualStyleBackColor = true;
			this->checkBoxLabels->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxLabels_CheckedChanged);
			// 
			// checkBoxGraduations
			// 
			this->checkBoxGraduations->AutoSize = true;
			this->checkBoxGraduations->Location = System::Drawing::Point(143, 18);
			this->checkBoxGraduations->Name = L"checkBoxGraduations";
			this->checkBoxGraduations->Size = System::Drawing::Size(120, 17);
			this->checkBoxGraduations->TabIndex = 1;
			this->checkBoxGraduations->Text = L"Display Graduations";
			this->checkBoxGraduations->UseVisualStyleBackColor = true;
			this->checkBoxGraduations->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxGraduations_CheckedChanged);
			// 
			// checkBoxBeamVolume
			// 
			this->checkBoxBeamVolume->AutoSize = true;
			this->checkBoxBeamVolume->Location = System::Drawing::Point(9, 18);
			this->checkBoxBeamVolume->Name = L"checkBoxBeamVolume";
			this->checkBoxBeamVolume->Size = System::Drawing::Size(128, 17);
			this->checkBoxBeamVolume->TabIndex = 0;
			this->checkBoxBeamVolume->Text = L"Display Beam Volume";
			this->checkBoxBeamVolume->UseVisualStyleBackColor = true;
			this->checkBoxBeamVolume->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxBeamVolume_CheckedChanged);
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->checkBoxDisplaySingleEchos);
			this->groupBox4->Controls->Add(this->label8);
			this->groupBox4->Controls->Add(this->numericUpDown6);
			this->groupBox4->Location = System::Drawing::Point(289, 19);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(207, 81);
			this->groupBox4->TabIndex = 21;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"Global Parameter";
			// 
			// checkBoxDisplaySingleEchos
			// 
			this->checkBoxDisplaySingleEchos->AutoSize = true;
			this->checkBoxDisplaySingleEchos->Location = System::Drawing::Point(10, 60);
			this->checkBoxDisplaySingleEchos->Name = L"checkBoxDisplaySingleEchos";
			this->checkBoxDisplaySingleEchos->Size = System::Drawing::Size(125, 17);
			this->checkBoxDisplaySingleEchos->TabIndex = 20;
			this->checkBoxDisplaySingleEchos->Text = L"Display Single Echos";
			this->checkBoxDisplaySingleEchos->UseVisualStyleBackColor = true;
			this->checkBoxDisplaySingleEchos->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxDisplaySingleEchos_CheckedChanged);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(74, 26);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(124, 13);
			this->label8->TabIndex = 1;
			this->label8->Text = L"Single Echo Display Size";
			// 
			// numericUpDown6
			// 
			this->numericUpDown6->DecimalPlaces = 3;
			this->numericUpDown6->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 196608 });
			this->numericUpDown6->Location = System::Drawing::Point(10, 24);
			this->numericUpDown6->Name = L"numericUpDown6";
			this->numericUpDown6->Size = System::Drawing::Size(58, 20);
			this->numericUpDown6->TabIndex = 0;
			this->numericUpDown6->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 25, 0, 0, 196608 });
			this->numericUpDown6->ValueChanged += gcnew System::EventHandler(this, &BaseVolumicView::numericUpDown6_ValueChanged);
			// 
			// trackBarVerticalScale
			// 
			this->trackBarVerticalScale->LargeChange = 10;
			this->trackBarVerticalScale->Location = System::Drawing::Point(379, 233);
			this->trackBarVerticalScale->Maximum = 100;
			this->trackBarVerticalScale->Minimum = 1;
			this->trackBarVerticalScale->Name = L"trackBarVerticalScale";
			this->trackBarVerticalScale->Size = System::Drawing::Size(95, 45);
			this->trackBarVerticalScale->TabIndex = 23;
			this->trackBarVerticalScale->TickFrequency = 10;
			this->trackBarVerticalScale->Value = 10;
			this->trackBarVerticalScale->Scroll += gcnew System::EventHandler(this, &BaseVolumicView::trackBarVerticalScale_Scroll);
			// 
			// labelIdVisible
			// 
			this->labelIdVisible->AutoSize = true;
			this->labelIdVisible->Location = System::Drawing::Point(264, 116);
			this->labelIdVisible->Name = L"labelIdVisible";
			this->labelIdVisible->Size = System::Drawing::Size(31, 13);
			this->labelIdVisible->TabIndex = 21;
			this->labelIdVisible->Text = L"____";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->checkBoxDisplayVolumeSampled);
			this->groupBox3->Controls->Add(this->label2);
			this->groupBox3->Controls->Add(this->numericUpDownEchoDisplaySize);
			this->groupBox3->Controls->Add(this->checkBoxDisplayMultipleEchos);
			this->groupBox3->Controls->Add(this->checkBox4);
			this->groupBox3->Controls->Add(this->checkBoxUseGPSPositionning);
			this->groupBox3->Location = System::Drawing::Point(6, 19);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(171, 120);
			this->groupBox3->TabIndex = 20;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Global Parameter";
			// 
			// checkBoxDisplayVolumeSampled
			// 
			this->checkBoxDisplayVolumeSampled->AutoSize = true;
			this->checkBoxDisplayVolumeSampled->Location = System::Drawing::Point(5, 99);
			this->checkBoxDisplayVolumeSampled->Name = L"checkBoxDisplayVolumeSampled";
			this->checkBoxDisplayVolumeSampled->Size = System::Drawing::Size(142, 17);
			this->checkBoxDisplayVolumeSampled->TabIndex = 24;
			this->checkBoxDisplayVolumeSampled->Text = L"Display Volume Sampled";
			this->checkBoxDisplayVolumeSampled->UseVisualStyleBackColor = true;
			this->checkBoxDisplayVolumeSampled->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxDisplayVolumeSampled_CheckedChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(5, 61);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(92, 13);
			this->label2->TabIndex = 23;
			this->label2->Text = L"Echo Display Size";
			// 
			// numericUpDownEchoDisplaySize
			// 
			this->numericUpDownEchoDisplaySize->DecimalPlaces = 3;
			this->numericUpDownEchoDisplaySize->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 196608 });
			this->numericUpDownEchoDisplaySize->Location = System::Drawing::Point(104, 57);
			this->numericUpDownEchoDisplaySize->Name = L"numericUpDownEchoDisplaySize";
			this->numericUpDownEchoDisplaySize->Size = System::Drawing::Size(59, 20);
			this->numericUpDownEchoDisplaySize->TabIndex = 22;
			this->numericUpDownEchoDisplaySize->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 25, 0, 0, 196608 });
			this->numericUpDownEchoDisplaySize->ValueChanged += gcnew System::EventHandler(this, &BaseVolumicView::numericUpDownEchoDisplaySize_ValueChanged);
			// 
			// checkBoxDisplayMultipleEchos
			// 
			this->checkBoxDisplayMultipleEchos->AutoSize = true;
			this->checkBoxDisplayMultipleEchos->Location = System::Drawing::Point(5, 40);
			this->checkBoxDisplayMultipleEchos->Name = L"checkBoxDisplayMultipleEchos";
			this->checkBoxDisplayMultipleEchos->Size = System::Drawing::Size(93, 17);
			this->checkBoxDisplayMultipleEchos->TabIndex = 20;
			this->checkBoxDisplayMultipleEchos->Text = L"Display Echos";
			this->checkBoxDisplayMultipleEchos->UseVisualStyleBackColor = true;
			this->checkBoxDisplayMultipleEchos->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxDisplayMultipleEchos_CheckedChanged);
			// 
			// checkBox4
			// 
			this->checkBox4->AutoSize = true;
			this->checkBox4->Checked = true;
			this->checkBox4->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox4->Location = System::Drawing::Point(5, 19);
			this->checkBox4->Name = L"checkBox4";
			this->checkBox4->Size = System::Drawing::Size(89, 17);
			this->checkBox4->TabIndex = 19;
			this->checkBox4->Text = L"Link Sounder";
			this->checkBox4->UseVisualStyleBackColor = true;
			this->checkBox4->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBox4_CheckedChanged);
			// 
			// checkBoxUseGPSPositionning
			// 
			this->checkBoxUseGPSPositionning->AutoSize = true;
			this->checkBoxUseGPSPositionning->Location = System::Drawing::Point(5, 78);
			this->checkBoxUseGPSPositionning->Name = L"checkBoxUseGPSPositionning";
			this->checkBoxUseGPSPositionning->Size = System::Drawing::Size(130, 17);
			this->checkBoxUseGPSPositionning->TabIndex = 21;
			this->checkBoxUseGPSPositionning->Text = L"Use GPS Positionning";
			this->checkBoxUseGPSPositionning->UseVisualStyleBackColor = true;
			this->checkBoxUseGPSPositionning->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxUseGPSPositionning_CheckedChanged);
			// 
			// panel2
			// 
			this->panel2->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel2->Controls->Add(this->checkBox1);
			this->panel2->Controls->Add(this->groupBox2);
			this->panel2->Location = System::Drawing::Point(124, 142);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(133, 78);
			this->panel2->TabIndex = 7;
			// 
			// checkBox1
			// 
			this->checkBox1->AutoSize = true;
			this->checkBox1->Checked = true;
			this->checkBox1->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBox1->Location = System::Drawing::Point(3, 10);
			this->checkBox1->Name = L"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(62, 17);
			this->checkBox1->TabIndex = 5;
			this->checkBox1->Text = L"Opacity";
			this->checkBox1->UseVisualStyleBackColor = true;
			this->checkBox1->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBox1_CheckedChanged);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->numericUpDown5);
			this->groupBox2->Location = System::Drawing::Point(3, 25);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(119, 44);
			this->groupBox2->TabIndex = 15;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Transparency Factor";
			// 
			// numericUpDown5
			// 
			this->numericUpDown5->DecimalPlaces = 2;
			this->numericUpDown5->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 25, 0, 0, 131072 });
			this->numericUpDown5->Location = System::Drawing::Point(11, 17);
			this->numericUpDown5->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->numericUpDown5->Name = L"numericUpDown5";
			this->numericUpDown5->Size = System::Drawing::Size(94, 20);
			this->numericUpDown5->TabIndex = 14;
			this->numericUpDown5->ValueChanged += gcnew System::EventHandler(this, &BaseVolumicView::numericUpDown5_ValueChanged);
			// 
			// checkBoxSeaFloor
			// 
			this->checkBoxSeaFloor->AutoSize = true;
			this->checkBoxSeaFloor->Location = System::Drawing::Point(185, 19);
			this->checkBoxSeaFloor->Name = L"checkBoxSeaFloor";
			this->checkBoxSeaFloor->Size = System::Drawing::Size(74, 17);
			this->checkBoxSeaFloor->TabIndex = 6;
			this->checkBoxSeaFloor->Text = L"Sea Floor ";
			this->checkBoxSeaFloor->UseVisualStyleBackColor = true;
			this->checkBoxSeaFloor->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::checkBoxSeaFloor_CheckedChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(20, 207);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(28, 13);
			this->label1->TabIndex = 10;
			this->label1->Text = L"Shift";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->numericUpDownSeaFloorScale);
			this->groupBox1->Location = System::Drawing::Point(184, 49);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(95, 48);
			this->groupBox1->TabIndex = 13;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Sea floor scale";
			// 
			// numericUpDownSeaFloorScale
			// 
			this->numericUpDownSeaFloorScale->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 0 });
			this->numericUpDownSeaFloorScale->Location = System::Drawing::Point(5, 19);
			this->numericUpDownSeaFloorScale->Name = L"numericUpDownSeaFloorScale";
			this->numericUpDownSeaFloorScale->Size = System::Drawing::Size(81, 20);
			this->numericUpDownSeaFloorScale->TabIndex = 12;
			this->numericUpDownSeaFloorScale->ValueChanged += gcnew System::EventHandler(this, &BaseVolumicView::numericUpDownSeaFloorScale_ValueChanged);
			// 
			// numericUpDown_PingFanSeen
			// 
			this->numericUpDown_PingFanSeen->Location = System::Drawing::Point(9, 145);
			this->numericUpDown_PingFanSeen->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 50000, 0, 0, 0 });
			this->numericUpDown_PingFanSeen->Name = L"numericUpDown_PingFanSeen";
			this->numericUpDown_PingFanSeen->Size = System::Drawing::Size(101, 20);
			this->numericUpDown_PingFanSeen->TabIndex = 8;
			this->numericUpDown_PingFanSeen->ValueChanged += gcnew System::EventHandler(this, &BaseVolumicView::numericUpDown_PingFanSeen_ValueChanged);
			// 
			// numericUpDown_shiftPingFan
			// 
			this->numericUpDown_shiftPingFan->Location = System::Drawing::Point(8, 184);
			this->numericUpDown_shiftPingFan->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000, 0, 0, 0 });
			this->numericUpDown_shiftPingFan->Name = L"numericUpDown_shiftPingFan";
			this->numericUpDown_shiftPingFan->Size = System::Drawing::Size(102, 20);
			this->numericUpDown_shiftPingFan->TabIndex = 9;
			this->numericUpDown_shiftPingFan->ValueChanged += gcnew System::EventHandler(this, &BaseVolumicView::numericUpDown_shiftPingFan_ValueChanged);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(20, 168);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(98, 13);
			this->label4->TabIndex = 11;
			this->label4->Text = L"Number of PingFan";
			// 
			// picture3dView
			// 
			this->picture3dView->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Stretch;
			this->picture3dView->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->picture3dView->ContextMenuStrip = this->contextMenuStrip3d;
			this->picture3dView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->picture3dView->Location = System::Drawing::Point(0, 0);
			this->picture3dView->Margin = System::Windows::Forms::Padding(0);
			this->picture3dView->Name = L"picture3dView";
			this->picture3dView->Size = System::Drawing::Size(456, 360);
			this->picture3dView->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->picture3dView->TabIndex = 0;
			this->picture3dView->TabStop = false;
			this->picture3dView->Resize += gcnew System::EventHandler(this, &BaseVolumicView::picture3dView_Resize);
			// 
			// contextMenuStrip3d
			// 
			this->contextMenuStrip3d->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->cameraSettingsToolStripMenuItem,
					this->sounderParameterToolStripMenuItem, this->backgroundColorToolStripMenuItem
			});
			this->contextMenuStrip3d->Name = L"contextMenuStrip3d";
			this->contextMenuStrip3d->Size = System::Drawing::Size(176, 70);
			// 
			// cameraSettingsToolStripMenuItem
			// 
			this->cameraSettingsToolStripMenuItem->Name = L"cameraSettingsToolStripMenuItem";
			this->cameraSettingsToolStripMenuItem->Size = System::Drawing::Size(175, 22);
			this->cameraSettingsToolStripMenuItem->Text = L"Camera Settings";
			this->cameraSettingsToolStripMenuItem->Click += gcnew System::EventHandler(this, &BaseVolumicView::cameraSettingsToolStripMenuItem_Click);
			// 
			// sounderParameterToolStripMenuItem
			// 
			this->sounderParameterToolStripMenuItem->CheckOnClick = true;
			this->sounderParameterToolStripMenuItem->Name = L"sounderParameterToolStripMenuItem";
			this->sounderParameterToolStripMenuItem->Size = System::Drawing::Size(175, 22);
			this->sounderParameterToolStripMenuItem->Text = L"Sounder Parameter";
			this->sounderParameterToolStripMenuItem->CheckedChanged += gcnew System::EventHandler(this, &BaseVolumicView::sounderParameterToolStripMenuItem_CheckedChanged);
			this->sounderParameterToolStripMenuItem->Click += gcnew System::EventHandler(this, &BaseVolumicView::sounderParameterToolStripMenuItem_Click);
			// 
			// backgroundColorToolStripMenuItem
			// 
			this->backgroundColorToolStripMenuItem->Name = L"backgroundColorToolStripMenuItem";
			this->backgroundColorToolStripMenuItem->Size = System::Drawing::Size(175, 22);
			this->backgroundColorToolStripMenuItem->Text = L"Background Color";
			this->backgroundColorToolStripMenuItem->Click += gcnew System::EventHandler(this, &BaseVolumicView::backgroundColorToolStripMenuItem_Click);
			// 
			// colorDialogBackGround
			// 
			this->colorDialogBackGround->Color = System::Drawing::Color::White;
			// 
			// BaseVolumicView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->splitView3D);
			this->Name = L"BaseVolumicView";
			this->Size = System::Drawing::Size(989, 427);
			this->splitView3D->Panel1->ResumeLayout(false);
			this->splitView3D->ResumeLayout(false);
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel2->ResumeLayout(false);
			this->splitContainer1->ResumeLayout(false);
			this->groupBoxSounder->ResumeLayout(false);
			this->groupBoxSounder->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarVerticalScale))->EndInit();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownEchoDisplaySize))->EndInit();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown5))->EndInit();
			this->groupBox1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownSeaFloorScale))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown_PingFanSeen))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown_shiftPingFan))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture3dView))->EndInit();
			this->contextMenuStrip3d->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion

	private: vtkWrapper  ^mWrap; 
	private: PingFan3DContainer *m_pPingFanContainer;
	private: ViewFilter *m_pViewFilter;
	private: std::uint32_t m_OptionSounderId;
	private: bool m_ValidOptionSounderId;
	private: UpdateToolBarDelegate^ mUpdateToolBarDelegate;
	private: ContextMenuDelegate^ mContextMenuDelegate;

	private: System::Void picture3dView_Resize(System::Object^  sender, System::EventArgs^  e);
	public:	 ViewFilter* GetViewFilter();

	public: System::Void DisplayCameraSettings();
	public: System::Void LoadCameraSettings();
	public: System::Void SaveCameraSettings();

	public: System::Void UpdateCamera(CameraSettings ^ref);
	public: System::Void SounderChanged();
	public: System::Void Draw();
	public: System::Void PingFanAdded();
	public: System::Void StreamOpened();
	public: System::Void UpdateShiftPingFan(unsigned int shift);
	
	// OTK - 30/04/2009 - ajout des bancs en 3D
	public: System::Void AddShoal(ShoalExtractionOutput * pOut);
	
	private: System::Void FillObject3dTreeViewContent();
	private: System::Void OnActiveSounderIdChanged();
	private: System::Void OnContextMenu();
	private: System::Void SetOptionActiveSounder(std::uint32_t sounderId);
	private: System::Void Object3dTreeView_AfterSelect(System::Object^  sender, System::Windows::Forms::TreeViewEventArgs^  e);
	private: System::Void Object3dTreeView_AfterCheck(System::Object^  sender, System::Windows::Forms::TreeViewEventArgs^  e);
	private: System::Void checkBox1_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void numericUpDown_PingFanSeen_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void numericUpDown_shiftPingFan_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void numericUpDownSeaFloorScale_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxSeaFloor_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBox4_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxDisplayMultipleEchos_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void numericUpDown5_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void sounderParameterToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void numericUpDown6_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void trackBarVerticalScale_Scroll(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxDisplaySingleEchos_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxUseGPSPositionning_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void backgroundColorToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	public:  System::Void DisplayBackGroundColorChoice();
	private: System::Void cameraSettingsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void sounderParameterToolStripMenuItem_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxBeamVolume_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxGraduations_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxLabels_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxShoalVolume_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxSmoothShoalVolume_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxDisplayShoalLabel_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radioButtonShoalVolume_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radioButtonShoalPoints_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radioButtonShoalBox_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radioButtonShoalDelaunay_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void UpdateShoalDisplayType();

	// OTK - FAE055 - on doit reconstruire les repr�sentation volumiques des bancs en cas de modification des coordonn�es de projection
	public: System::Void RecomputeShoalDisplay();
	private: System::Void checkBoxDisplayVolumeSampled_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void numericUpDownEchoDisplaySize_ValueChanged(System::Object^  sender, System::EventArgs^  e);
		
	public: bool is3DViewFrozen();
	public: System::Void recomputeView();
	public: property bool SounderParameterVisible { bool get(); void set(bool value); }
	public: void updateParameters();

	public: void addPlaneWidget();
	public: void removePlaneWidget();

	public: property bool IsAviRendering { bool get();  }
	public: void saveAvi(System::String ^fileName);
	public: void startAviRendering(System::String ^fileName);
	public: void stopAviRendering();

	};
}
