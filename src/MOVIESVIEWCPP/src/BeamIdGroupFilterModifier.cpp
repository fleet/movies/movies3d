#include "BeamIdGroupFilterModifier.h"

using namespace MOVIESVIEWCPP;

System::Void BeamIdGroupFilterModifier::BeamIdGroupFilterModifier_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
	GuiToConfig();
}

System::Void BeamIdGroupFilterModifier::dataGridViewBeamIdGroup_DataError(System::Object^  sender, System::Windows::Forms::DataGridViewDataErrorEventArgs^  e)
{
	if (e->Exception != nullptr)
	{
		MessageBox::Show("Please enter a valid number.");
	}
}

System::Void BeamIdGroupFilterModifier::InitializeAll()
{
	ConfigToGui();

	this->comboBoxTransducer->BeginUpdate();
	this->comboBoxTransducer->Items->Clear();

	MapTransducerToBeamRange mapBeamIdGroup = m_pBeamIdGroupFilter->GetMapBeamIdRangeVector();

	for (MapTransducerToBeamRange::iterator iterMap = mapBeamIdGroup.begin(); iterMap != mapBeamIdGroup.end(); iterMap++)
	{
		System::String ^NodeName = gcnew System::String(iterMap->first.c_str());
		this->comboBoxTransducer->Items->Add(NodeName);
	}

	if (this->comboBoxTransducer->Items->Count > 0)
		this->comboBoxTransducer->SelectedIndex = 0;

	this->comboBoxTransducer->EndUpdate();
}

System::Void BeamIdGroupFilterModifier::ConfigToGui()
{
	DataTable^ dataTable = (DataTable^)dataGridViewBeamIdGroup->DataSource;
	dataTable->Clear();

	char * name = (char *)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(this->comboBoxTransducer->Text).ToPointer();
	std::string selectedTransducer = name;
	System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(name));

	BeamIdRangeVector * pBeamIdRangeVector = m_pBeamIdGroupFilter->GetBeamIdRangeVector(selectedTransducer);
	if (pBeamIdRangeVector)
	{
		size_t nbGroups = pBeamIdRangeVector->size();
		for (size_t i = 0; i < nbGroups; i++)
		{
			DataRow^ row = dataTable->NewRow();
			unsigned int firstBeam = pBeamIdRangeVector->at(i).first;
			unsigned int lastBeam = pBeamIdRangeVector->at(i).second;
			row[0] = Convert::ToString(firstBeam);
			row[1] = Convert::ToString(lastBeam);
			dataTable->Rows->Add(row);
		}
	}
}

System::Void BeamIdGroupFilterModifier::GuiToConfig()
{
	if (previousIndex >= 0 && previousIndex < this->comboBoxTransducer->Items->Count)
	{
		System::String^ previousTransducer = this->comboBoxTransducer->Items[previousIndex]->ToString();

		char * name = (char *)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(previousTransducer).ToPointer();
		std::string selectedTransducer = name;
		System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(name));

		BeamIdRangeVector * pBeamIdRangeVector = m_pBeamIdGroupFilter->GetBeamIdRangeVector(selectedTransducer);
		if (pBeamIdRangeVector)
		{
			DataTable^ dataTable = (DataTable^)dataGridViewBeamIdGroup->DataSource;
			pBeamIdRangeVector->clear();
			int nbGroups = dataTable->Rows->Count;
			for (int i = 0; i < nbGroups; i++)
			{
				DataRow^ row = dataTable->Rows[i];
				unsigned int firstBeam = Convert::ToUInt32(row[0]);
				unsigned int lastBeam = Convert::ToUInt32(row[1]);
				pBeamIdRangeVector->push_back(std::make_pair(firstBeam, lastBeam));
			}
		}
	}
}

System::Void BeamIdGroupFilterModifier::comboBoxTransducer_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	// on sauvegarde les param�tres du transducteur duquel on part
	GuiToConfig();
	ConfigToGui();
	previousIndex = this->comboBoxTransducer->SelectedIndex;
}

System::Void BeamIdGroupFilterModifier::dataGridViewBeamIdGroup_CellValidating(System::Object^  sender, System::Windows::Forms::DataGridViewCellValidatingEventArgs^  e)
{
	// verification des valeurs rentr�es
	DataTable^ dataTable = (DataTable^)dataGridViewBeamIdGroup->DataSource;

	if (e->RowIndex < dataTable->Rows->Count)
	{
		DataRow^ row = dataTable->Rows[e->RowIndex];
		unsigned int newValue = Convert::ToUInt32(e->FormattedValue);
		if (e->ColumnIndex == 0)
		{
			unsigned int compareValue = Convert::ToUInt32(row[1]);
			if (newValue > compareValue)
			{
				e->Cancel = true;
			}
		}
		else if (e->ColumnIndex == 1)
		{
			unsigned int compareValue = Convert::ToUInt32(row[0]);
			if (newValue < compareValue)
			{
				e->Cancel = true;
			}
		}
	}
}