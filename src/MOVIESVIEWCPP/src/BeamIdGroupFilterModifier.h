#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "Filtering/BeamIdGroupFilter.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de BeamIdGroupFilterModifier
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class BeamIdGroupFilterModifier : public System::Windows::Forms::Form
	{
	public:
		BeamIdGroupFilterModifier(BeamIdGroupFilter * pBeamIdGroupFilter)
		{
			InitializeComponent();

			previousIndex = -1;

			m_pBeamIdGroupFilter = pBeamIdGroupFilter;

			// Creation des colonnes
			DataTable^ dataTable = gcnew DataTable();

			DataColumn^ column = gcnew DataColumn();
			column->DataType = System::Type::GetType("System.UInt32");
			column->ColumnName = "First Beam Id";
			dataTable->Columns->Add(column);

			// Create second column.
			column = gcnew DataColumn();
			column->DataType = System::Type::GetType("System.UInt32");
			column->ColumnName = "Last Beam Id";
			dataTable->Columns->Add(column);

			dataGridViewBeamIdGroup->DataSource = dataTable;

			InitializeAll();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~BeamIdGroupFilterModifier()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataGridView^  dataGridViewBeamIdGroup;
	private: System::Windows::Forms::ComboBox^  comboBoxTransducer;

	protected:
		BeamIdGroupFilter * m_pBeamIdGroupFilter;
		int previousIndex;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->dataGridViewBeamIdGroup = (gcnew System::Windows::Forms::DataGridView());
			this->comboBoxTransducer = (gcnew System::Windows::Forms::ComboBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridViewBeamIdGroup))->BeginInit();
			this->SuspendLayout();
			// 
			// dataGridViewBeamIdGroup
			// 
			this->dataGridViewBeamIdGroup->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridViewBeamIdGroup->Location = System::Drawing::Point(12, 39);
			this->dataGridViewBeamIdGroup->Name = L"dataGridViewBeamIdGroup";
			this->dataGridViewBeamIdGroup->Size = System::Drawing::Size(260, 211);
			this->dataGridViewBeamIdGroup->TabIndex = 0;
			this->dataGridViewBeamIdGroup->CellValidating += gcnew System::Windows::Forms::DataGridViewCellValidatingEventHandler(this, &BeamIdGroupFilterModifier::dataGridViewBeamIdGroup_CellValidating);
			this->dataGridViewBeamIdGroup->DataError += gcnew System::Windows::Forms::DataGridViewDataErrorEventHandler(this, &BeamIdGroupFilterModifier::dataGridViewBeamIdGroup_DataError);
			// 
			// comboBoxTransducer
			// 
			this->comboBoxTransducer->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxTransducer->FormattingEnabled = true;
			this->comboBoxTransducer->Location = System::Drawing::Point(12, 12);
			this->comboBoxTransducer->Name = L"comboBoxTransducer";
			this->comboBoxTransducer->Size = System::Drawing::Size(260, 21);
			this->comboBoxTransducer->TabIndex = 1;
			this->comboBoxTransducer->SelectedIndexChanged += gcnew System::EventHandler(this, &BeamIdGroupFilterModifier::comboBoxTransducer_SelectedIndexChanged);
			// 
			// BeamIdGroupFilterModifier
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 262);
			this->Controls->Add(this->comboBoxTransducer);
			this->Controls->Add(this->dataGridViewBeamIdGroup);
			this->Name = L"BeamIdGroupFilterModifier";
			this->Text = L"BeamIdGroupFilterModifier";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &BeamIdGroupFilterModifier::BeamIdGroupFilterModifier_FormClosing);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridViewBeamIdGroup))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private:
		System::Void BeamIdGroupFilterModifier_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);

		System::Void dataGridViewBeamIdGroup_DataError(System::Object^  sender, System::Windows::Forms::DataGridViewDataErrorEventArgs^  e);

		System::Void InitializeAll();

		// Affichage des param�tres en m�moire
		System::Void ConfigToGui();

		//Sauvegarde des param�tres dans la memoire
		System::Void GuiToConfig();

		System::Void comboBoxTransducer_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

		System::Void dataGridViewBeamIdGroup_CellValidating(System::Object^  sender, System::Windows::Forms::DataGridViewCellValidatingEventArgs^  e);

	};
}