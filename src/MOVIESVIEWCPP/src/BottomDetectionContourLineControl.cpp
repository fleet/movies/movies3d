#include "BottomDetectionContourLineControl.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "BottomDetection/BottomDetectionContourLine.h"
#include "BottomDetection/ContourLineDef.h"

#include "DisplayParameter.h"

using namespace MOVIESVIEWCPP;


System::Void BottomDetectionContourLineControl::ApplyDetailedView()
{
	// param�tres concernant les sondeurs � traiter
	std::vector<std::uint32_t> & soundersToProcess = m_pContourAlgorithm->GetSounderIDs();
	std::vector<std::uint32_t> soundersToProcessCpy = soundersToProcess;
	soundersToProcess.clear();
	for (int iNode = 0; iNode < m_SoundersTreeView->Nodes->Count; iNode++)
	{
		std::uint32_t sounderID = Convert::ToUInt32((System::Int32^)m_SoundersTreeView->Nodes[iNode]->Tag);
		if (m_SoundersTreeView->Nodes[iNode]->Checked)
		{
			soundersToProcess.push_back(sounderID);
		}
		std::vector<std::uint32_t>::iterator iter = std::find(soundersToProcessCpy.begin(), soundersToProcessCpy.end(), sounderID);
		if (iter != soundersToProcessCpy.end())
		{
			soundersToProcessCpy.erase(iter);
		}
	}
	// on rajoute les sondeurs qui n'ont pas pu �tre modifi�s (pas dans la liste des sondeurs du fichier HAC actuel)
	for (size_t iSounder = 0; iSounder < soundersToProcessCpy.size(); iSounder++)
	{
		soundersToProcess.push_back(soundersToProcessCpy[iSounder]);
	}

	// param�tres concernant les lignes de niveau
	this->dataGridViewLevels->EndEdit();
	std::vector<ContourLineDef *> & contourLines = m_pContourAlgorithm->GetContourLinesDef();

	DataTable ^dt = (DataTable^)this->dataGridViewLevels->DataSource;
	for (int i = 0; i < dt->Rows->Count; i++)
	{
		int rowLevel = Convert::ToInt32(dt->Rows[i]["Level"]);

		if (Convert::ToBoolean(dt->Rows[i]["MovRef."]))
		{
			m_pContourAlgorithm->SetReferenceLevel(rowLevel);
		}

		for (size_t i = 0; i < contourLines.size(); i++)
		{
			ContourLineDef * pContourLine = contourLines[i];
			if (pContourLine->GetLevel() == rowLevel)
			{
				pContourLine->SetCompute(Convert::ToBoolean(dt->Rows[i]["Compute"]));
				pContourLine->SetDisplay(Convert::ToBoolean(dt->Rows[i]["Display"]));
				break;
			}
		}
	}
	m_pContourAlgorithm->SetDefaultDepth(Decimal::ToDouble(this->numericUpDownDefaultBottom->Value));
	m_pContourAlgorithm->SetDisplayMaximumEchoLine(this->checkBoxDisplayMaximumEchoLine->Checked);
	m_pContourAlgorithm->SetMixedBottomDetermination(this->checkBoxMixedDetermination->Checked);
}

void BottomDetectionContourLineControl::FillSoundersTreeView()
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	CurrentSounderDefinition & sounderMgr = pKernel->getObjectMgr()->GetPingFanContainer().m_sounderDefinition;

	const std::vector<std::uint32_t> & soundersToProcess = m_pContourAlgorithm->GetSounderIDs();

	m_SoundersTreeView->BeginUpdate();
	m_SoundersTreeView->Nodes->Clear();
	for (unsigned int i = 0; i < sounderMgr.GetNbSounder(); i++)
	{
		Sounder *pSounder = sounderMgr.GetSounder(i);
		System::Int32 ^sounderId = gcnew System::Int32(pSounder->m_SounderId);
		System::Windows::Forms::TreeNode ^ sounderNode = m_SoundersTreeView->Nodes->Add("Sounder Id[" + sounderId->ToString() + "]");
		sounderNode->Tag = sounderId;

		// on coche les sondeurs � traiter
		if (std::find(soundersToProcess.begin(), soundersToProcess.end(), pSounder->m_SounderId) != soundersToProcess.end())
		{
			sounderNode->Checked = true;
		}
	}
	m_SoundersTreeView->EndUpdate();
}

void BottomDetectionContourLineControl::FillLevelsGridView()
{
	DataTable ^dt = gcnew DataTable();
	dt->Columns->Add("Level");
	dt->Columns[0]->ReadOnly = true;
	dt->Columns->Add("MovRef.", System::Type::GetType("System.Boolean"));
	dt->Columns->Add("Compute", System::Type::GetType("System.Boolean"));
	dt->Columns->Add("Display", System::Type::GetType("System.Boolean"));

	DataRow ^dr;
	const std::vector<ContourLineDef *> & contourLines = m_pContourAlgorithm->GetContourLinesDef();
	for (size_t i = 0; i < contourLines.size(); i++)
	{
		ContourLineDef * pContourLine = contourLines[i];
		dr = dt->NewRow();
		dr["Level"] = pContourLine->GetLevel();
		dr["MovRef."] = pContourLine->GetLevel() == m_pContourAlgorithm->GetReferenceLevel();
		dr["Compute"] = pContourLine->GetCompute();
		dr["Display"] = pContourLine->GetDisplay();
		dt->Rows->Add(dr);
	}
	this->dataGridViewLevels->DataSource = dt;

	this->numericUpDownDefaultBottom->Value = (Decimal)m_pContourAlgorithm->GetDefaultDepth();
	this->checkBoxDisplayMaximumEchoLine->Checked = m_pContourAlgorithm->GetDisplayMaximumEchoLine();
	this->checkBoxMixedDetermination->Checked = m_pContourAlgorithm->GetMixedBottomDetermination();
}

void BottomDetectionContourLineControl::dataGridViewLevels_CellContentClick(System::Object^ sender, DataGridViewCellEventArgs^ e)
{
	if (e->RowIndex >= 0)
	{
		if (e->ColumnIndex == dataGridViewLevels->Columns["MovRef."]->Index)
		{
			dataGridViewLevels->EndEdit();  //Stop editing of cell.

			// si la case se coche, on d�coche le niveau de r�f�rence pour toutes les autres lignes
			if ((bool)dataGridViewLevels->Rows[e->RowIndex]->Cells["MovRef."]->Value)
			{
				for (int i = 0; i < dataGridViewLevels->RowCount; i++)
				{
					if (i != e->RowIndex)
					{
						dataGridViewLevels->Rows[i]->Cells["MovRef."]->Value = false;
					}
				}
			}
			else
				// si la case se d�coche, on la recoche de force
			{
				dataGridViewLevels->Rows[e->RowIndex]->Cells["MovRef."]->Value = true;
			}
		}
		else if (e->ColumnIndex == dataGridViewLevels->Columns["Compute"]->Index)
		{
			dataGridViewLevels->EndEdit();  //Stop editing of cell.

			// Si la case se d�coche alors qu'il s'agit du niveau de r�f�rence, on force le recochage
			if ((bool)dataGridViewLevels->Rows[e->RowIndex]->Cells["MovRef."]->Value)
			{
				dataGridViewLevels->Rows[e->RowIndex]->Cells["Compute"]->Value = true;
			}
		}
	}
}
