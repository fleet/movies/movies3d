#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

class BottomDetectionContourLine;

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de BottomDetectionContourLineControl
	/// </summary>
	public ref class BottomDetectionContourLineControl : public System::Windows::Forms::UserControl
	{
	public:
		BottomDetectionContourLineControl(BottomDetectionContourLine * pContourAlgorithm)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
			m_pContourAlgorithm = pContourAlgorithm;
			FillSoundersTreeView();
			FillLevelsGridView();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~BottomDetectionContourLineControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TreeView^  m_SoundersTreeView;
	protected:

	protected:
	private: System::Windows::Forms::Label^  labelProcessedSounder;
	private: System::Windows::Forms::Label^  labelLevels;
	private: System::Windows::Forms::DataGridView^  dataGridViewLevels;
	private: System::Windows::Forms::CheckBox^  checkBoxMixedDetermination;
	private: System::Windows::Forms::CheckBox^  checkBoxDisplayMaximumEchoLine;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownDefaultBottom;
	private: System::Windows::Forms::Label^  labelDefaultBottom;







	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->m_SoundersTreeView = (gcnew System::Windows::Forms::TreeView());
			this->labelProcessedSounder = (gcnew System::Windows::Forms::Label());
			this->labelLevels = (gcnew System::Windows::Forms::Label());
			this->dataGridViewLevels = (gcnew System::Windows::Forms::DataGridView());
			this->checkBoxMixedDetermination = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxDisplayMaximumEchoLine = (gcnew System::Windows::Forms::CheckBox());
			this->numericUpDownDefaultBottom = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelDefaultBottom = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridViewLevels))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDefaultBottom))->BeginInit();
			this->SuspendLayout();
			// 
			// m_SoundersTreeView
			// 
			this->m_SoundersTreeView->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->m_SoundersTreeView->CheckBoxes = true;
			this->m_SoundersTreeView->Location = System::Drawing::Point(6, 16);
			this->m_SoundersTreeView->Name = L"m_SoundersTreeView";
			this->m_SoundersTreeView->Size = System::Drawing::Size(222, 115);
			this->m_SoundersTreeView->TabIndex = 17;
			// 
			// labelProcessedSounder
			// 
			this->labelProcessedSounder->AutoSize = true;
			this->labelProcessedSounder->Location = System::Drawing::Point(3, 0);
			this->labelProcessedSounder->Name = L"labelProcessedSounder";
			this->labelProcessedSounder->Size = System::Drawing::Size(110, 13);
			this->labelProcessedSounder->TabIndex = 16;
			this->labelProcessedSounder->Text = L"Sounders to process :";
			// 
			// labelLevels
			// 
			this->labelLevels->AutoSize = true;
			this->labelLevels->Location = System::Drawing::Point(3, 134);
			this->labelLevels->Name = L"labelLevels";
			this->labelLevels->Size = System::Drawing::Size(44, 13);
			this->labelLevels->TabIndex = 18;
			this->labelLevels->Text = L"Levels :";
			// 
			// dataGridViewLevels
			// 
			this->dataGridViewLevels->AllowUserToAddRows = false;
			this->dataGridViewLevels->AllowUserToDeleteRows = false;
			this->dataGridViewLevels->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->dataGridViewLevels->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->dataGridViewLevels->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::Window;
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			dataGridViewCellStyle1->ForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->dataGridViewLevels->DefaultCellStyle = dataGridViewCellStyle1;
			this->dataGridViewLevels->Location = System::Drawing::Point(6, 150);
			this->dataGridViewLevels->MultiSelect = false;
			this->dataGridViewLevels->Name = L"dataGridViewLevels";
			this->dataGridViewLevels->Size = System::Drawing::Size(221, 128);
			this->dataGridViewLevels->TabIndex = 19;
			this->dataGridViewLevels->CellContentDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &BottomDetectionContourLineControl::dataGridViewLevels_CellContentClick);
			this->dataGridViewLevels->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &BottomDetectionContourLineControl::dataGridViewLevels_CellContentClick);
			// 
			// checkBoxMixedDetermination
			// 
			this->checkBoxMixedDetermination->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->checkBoxMixedDetermination->AutoSize = true;
			this->checkBoxMixedDetermination->Location = System::Drawing::Point(6, 333);
			this->checkBoxMixedDetermination->Name = L"checkBoxMixedDetermination";
			this->checkBoxMixedDetermination->Size = System::Drawing::Size(176, 17);
			this->checkBoxMixedDetermination->TabIndex = 20;
			this->checkBoxMixedDetermination->Text = L"Use mixed bottom determination";
			this->checkBoxMixedDetermination->UseVisualStyleBackColor = true;
			// 
			// checkBoxDisplayMaximumEchoLine
			// 
			this->checkBoxDisplayMaximumEchoLine->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->checkBoxDisplayMaximumEchoLine->AutoSize = true;
			this->checkBoxDisplayMaximumEchoLine->Location = System::Drawing::Point(6, 310);
			this->checkBoxDisplayMaximumEchoLine->Name = L"checkBoxDisplayMaximumEchoLine";
			this->checkBoxDisplayMaximumEchoLine->Size = System::Drawing::Size(187, 17);
			this->checkBoxDisplayMaximumEchoLine->TabIndex = 21;
			this->checkBoxDisplayMaximumEchoLine->Text = L"Display maximum bottom echo line";
			this->checkBoxDisplayMaximumEchoLine->UseVisualStyleBackColor = true;
			// 
			// numericUpDownDefaultBottom
			// 
			this->numericUpDownDefaultBottom->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->numericUpDownDefaultBottom->Location = System::Drawing::Point(80, 284);
			this->numericUpDownDefaultBottom->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 100000, 0, 0, 0 });
			this->numericUpDownDefaultBottom->Name = L"numericUpDownDefaultBottom";
			this->numericUpDownDefaultBottom->Size = System::Drawing::Size(147, 20);
			this->numericUpDownDefaultBottom->TabIndex = 22;
			this->numericUpDownDefaultBottom->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, 0 });
			// 
			// labelDefaultBottom
			// 
			this->labelDefaultBottom->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->labelDefaultBottom->AutoSize = true;
			this->labelDefaultBottom->Location = System::Drawing::Point(3, 286);
			this->labelDefaultBottom->Name = L"labelDefaultBottom";
			this->labelDefaultBottom->Size = System::Drawing::Size(71, 13);
			this->labelDefaultBottom->TabIndex = 23;
			this->labelDefaultBottom->Text = L"Default depth";
			// 
			// BottomDetectionContourLineControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->labelDefaultBottom);
			this->Controls->Add(this->numericUpDownDefaultBottom);
			this->Controls->Add(this->checkBoxDisplayMaximumEchoLine);
			this->Controls->Add(this->checkBoxMixedDetermination);
			this->Controls->Add(this->dataGridViewLevels);
			this->Controls->Add(this->labelLevels);
			this->Controls->Add(this->m_SoundersTreeView);
			this->Controls->Add(this->labelProcessedSounder);
			this->Name = L"BottomDetectionContourLineControl";
			this->Size = System::Drawing::Size(231, 358);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridViewLevels))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDefaultBottom))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	public: void ApplyDetailedView();

	private: void FillSoundersTreeView();
	private: void FillLevelsGridView();

	private: void dataGridViewLevels_CellContentClick(System::Object^ sender, DataGridViewCellEventArgs^ e);


	private: BottomDetectionContourLine * m_pContourAlgorithm;


	};
}
