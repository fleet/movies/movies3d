#include "BottomDetectionGenericControl.h"

#include "BottomDetection/BottomDetectionParameter.h"

using namespace MOVIESVIEWCPP;

void BottomDetectionGenericControl::SetUpdateGUIDelegate(
	UpdateGUIDelegate ^ updateGUIDlg)
{
	m_UpdateGUIDlg = updateGUIDlg;
}

void BottomDetectionGenericControl::SetBottomDetectionAlgorithmValue(
	BottomDetectionAlgorithmValue * pAlgorithm)
{
	m_pAlgorithm = pAlgorithm;
	textBoxValue->Text = Convert::ToString(m_pAlgorithm->GetValue());
}

System::Void BottomDetectionGenericControl::textBoxValue_Leave(System::Object^  sender, System::EventArgs^  e)
{
	ApplyDetailedView();
	m_UpdateGUIDlg->Invoke();
}

System::Void BottomDetectionGenericControl::textBoxValue_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
	if (e->KeyChar == 13)
	{
		ApplyDetailedView();
		m_UpdateGUIDlg->Invoke();
	}
}

void BottomDetectionGenericControl::ApplyDetailedView()
{
	try
	{
		m_pAlgorithm->SetValue(Convert::ToDouble(textBoxValue->Text));
	}
	catch (System::Exception^)
	{
		MessageBox::Show("Incorrect Value");
	}
}