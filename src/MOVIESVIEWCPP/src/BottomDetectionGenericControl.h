#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

class BottomDetectionAlgorithmValue;


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de BottomDetectionGenericControl
	/// </summary>
	public ref class BottomDetectionGenericControl : public System::Windows::Forms::UserControl
	{
	public:
		BottomDetectionGenericControl(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~BottomDetectionGenericControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  labelValue;
	protected:
	private: System::Windows::Forms::TextBox^  textBoxValue;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->labelValue = (gcnew System::Windows::Forms::Label());
			this->textBoxValue = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// labelValue
			// 
			this->labelValue->AutoSize = true;
			this->labelValue->Location = System::Drawing::Point(5, 68);
			this->labelValue->Name = L"labelValue";
			this->labelValue->Size = System::Drawing::Size(34, 13);
			this->labelValue->TabIndex = 4;
			this->labelValue->Text = L"Value";
			// 
			// textBoxValue
			// 
			this->textBoxValue->Location = System::Drawing::Point(46, 65);
			this->textBoxValue->Name = L"textBoxValue";
			this->textBoxValue->Size = System::Drawing::Size(100, 20);
			this->textBoxValue->TabIndex = 3;
			this->textBoxValue->Leave += gcnew System::EventHandler(this, &BottomDetectionGenericControl::textBoxValue_Leave);
			this->textBoxValue->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &BottomDetectionGenericControl::textBoxValue_KeyPress);
			// 
			// BottomDetectionGenericControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->labelValue);
			this->Controls->Add(this->textBoxValue);
			this->Name = L"BottomDetectionGenericControl";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Void textBoxValue_Leave(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxValue_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e);

	public: void ApplyDetailedView();

	public: delegate void UpdateGUIDelegate();
	public: void SetUpdateGUIDelegate(UpdateGUIDelegate ^ updateGUIDlg);

	public: void SetBottomDetectionAlgorithmValue(BottomDetectionAlgorithmValue * pAlgorithm);


	private: BottomDetectionAlgorithmValue * m_pAlgorithm;
	private: UpdateGUIDelegate ^ m_UpdateGUIDlg;
	};
}
