#include "BottomDetectionParamControl.h"

#include "ModuleManager/ModuleManager.h"
#include "BottomDetection/BottomDetectionModule.h"
#include "BottomDetection/BottomDetectionContourLine.h"

#include <vector>

using namespace MOVIESVIEWCPP;

void BottomDetectionParamControl::UpdateConfiguration()
{
	BottomDetectionModule* bottomModule = CModuleManager::getInstance()->GetBottomDetectionModule();
	BottomDetectionParameter& parameters = bottomModule->GetBottomDetectionParameter();

	parameters.SetChangeEditedPingBottoms(m_EnableEditedPingsChangeCheckBox->Checked);
}

void BottomDetectionParamControl::UpdateGUI()
{
	this->checkedListBox->ItemCheck -= m_CheckEventHandler;

	// r�cup�ration de la d�finition des param�tres
	BottomDetectionModule* bottomModule = CModuleManager::getInstance()->GetBottomDetectionModule();
	BottomDetectionParameter& parameters = bottomModule->GetBottomDetectionParameter();
	const std::vector<BottomDetectionAlgorithm*>& algos = parameters.GetAlgorithmDefinition();

	// remplissage de la liste avec les diff�rents algos de d�tection du fond pr�sents
	m_SelectedIndex = checkedListBox->SelectedIndex;
	checkedListBox->Items->Clear();
	checkedListBox->BeginUpdate();

	for (size_t i = 0; i < algos.size(); i++)
	{
		System::String ^NodeName = gcnew System::String(algos[i]->GetAlgorithmDesc().c_str());
		checkedListBox->Items->Add(NodeName, parameters.GetActivatedAlgo() == algos[i]);
	}

	checkedListBox->EndUpdate();

	checkedListBox->SelectedIndex = m_SelectedIndex;

	this->checkedListBox->ItemCheck += m_CheckEventHandler;

	m_EnableEditedPingsChangeCheckBox->Checked = parameters.GetChangeEditedPingBottoms();
}

System::Void BottomDetectionParamControl::checkedListBox_ItemCheck(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e)
{
	// on n'autorise pas le d�cochage des algos
	if (e->NewValue == CheckState::Checked)
	{
		// on active l'algo coch�
		BottomDetectionModule* bottomModule = CModuleManager::getInstance()->GetBottomDetectionModule();
		BottomDetectionParameter& parameters = bottomModule->GetBottomDetectionParameter();
		parameters.SetActivatedAlgo(parameters.GetAlgorithmDefinition()[e->Index]->GetAlgorithmType());
	}
	else
	{
		e->NewValue = CheckState::Checked;
	}

	// mise � jour de la liste
	UpdateGUI();
}

System::Void BottomDetectionParamControl::checkedListBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	// mise � jour des param�tres de l'algorithme en fonction de son type
	BottomDetectionModule* bottomModule = CModuleManager::getInstance()->GetBottomDetectionModule();
	BottomDetectionParameter& parameters = bottomModule->GetBottomDetectionParameter();
	const std::vector<BottomDetectionAlgorithm*>& algos = parameters.GetAlgorithmDefinition();

	int selectedIndex = checkedListBox->SelectedIndex;
	if (selectedIndex >= 0 && selectedIndex < (int)algos.size())
	{
		BottomDetectionAlgorithm * pSelectedAlgo = algos[selectedIndex];

		BottomDetectionAlgorithmValue* algoValue = dynamic_cast<BottomDetectionAlgorithmValue*>(pSelectedAlgo);
		if (algoValue)
		{
			if (!this->panelParameters->Controls->Contains(m_GenericParamsControl)) {
				this->panelParameters->Controls->Clear();
				this->panelParameters->Controls->Add(m_GenericParamsControl);
				m_GenericParamsControl->Dock = System::Windows::Forms::DockStyle::Fill;
			}
			m_GenericParamsControl->SetBottomDetectionAlgorithmValue(algoValue);
		}
		else
		{
			BottomDetectionContourLine * pBottomContourLine = dynamic_cast<BottomDetectionContourLine*>(pSelectedAlgo);
			if (pBottomContourLine)
			{
				// cas particulier de l'algo ContourLine : utilisation d'un contr�le sp�cifique.
				if (!m_ContourLineParamsControl)
				{
					m_ContourLineParamsControl = gcnew BottomDetectionContourLineControl(pBottomContourLine);
				}
				if (!this->panelParameters->Controls->Contains(m_ContourLineParamsControl)) {
					this->panelParameters->Controls->Clear();
					this->panelParameters->Controls->Add(m_ContourLineParamsControl);
					m_ContourLineParamsControl->Dock = System::Windows::Forms::DockStyle::Fill;
				}
			}
			else
			{
				// cas des algos sans param�tres du tout
				this->panelParameters->Controls->Clear();
			}
		}
	}
}

System::Void BottomDetectionParamControl::ApplyDetailedView()
{
	// mise � jour des param�tres de l'algorithme en fonction de son type
	BottomDetectionModule* bottomModule = CModuleManager::getInstance()->GetBottomDetectionModule();
	BottomDetectionParameter& parameters = bottomModule->GetBottomDetectionParameter();

	if (checkedListBox->SelectedIndex >= 0 && checkedListBox->SelectedIndex < (int)parameters.GetAlgorithmDefinition().size())
	{
		BottomDetectionAlgorithm * pSelectedAlgo = parameters.GetAlgorithmDefinition()[checkedListBox->SelectedIndex];
		BottomDetectionAlgorithmValue* algoValue = dynamic_cast<BottomDetectionAlgorithmValue*>(pSelectedAlgo);
		if (algoValue)
		{
			m_GenericParamsControl->ApplyDetailedView();
		}
		else
		{
			BottomDetectionContourLine * pBottomContourLine = dynamic_cast<BottomDetectionContourLine*>(pSelectedAlgo);
			if (pBottomContourLine)
			{
				// cas particulier de l'algo ContourLine : utilisation d'un contr�le sp�cifique.
				m_ContourLineParamsControl->ApplyDetailedView();
			}
			else
			{
				// cas des algos sans param�tres du tout
				// no nothing
			}
		}
	}
}


