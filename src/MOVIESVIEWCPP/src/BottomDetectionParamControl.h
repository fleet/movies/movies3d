#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"
#include "BottomDetectionGenericControl.h"
#include "BottomDetectionContourLineControl.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de BottomDetectionParamControl
	/// </summary>
	public ref class BottomDetectionParamControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		BottomDetectionParamControl(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
			m_CheckEventHandler = gcnew System::Windows::Forms::ItemCheckEventHandler(this, &BottomDetectionParamControl::checkedListBox_ItemCheck);;
			this->checkedListBox->ItemCheck += m_CheckEventHandler;
			m_SelectedIndex = 0;
			m_GenericParamsControl = gcnew BottomDetectionGenericControl();
			m_GenericParamsControl->SetUpdateGUIDelegate(gcnew BottomDetectionGenericControl::UpdateGUIDelegate(this, &BottomDetectionParamControl::UpdateGUI));
		}

		property bool BottomCorrectionEnabled
		{
			bool get()
			{
				return m_ManualBottomCorrectionCheckBox->Checked;
			}
			void set(bool value)
			{
				m_ManualBottomCorrectionCheckBox->Checked = value;
			}
		}

		property bool BottomCorrectionAvailable
		{
			void set(bool value)
			{
				m_ManualBottomCorrectionCheckBox->Enabled = value;
			}
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~BottomDetectionParamControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckedListBox^  checkedListBox;
	private: System::Windows::Forms::Panel^  panelParameters;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::CheckBox^  m_ManualBottomCorrectionCheckBox;
	private: System::Windows::Forms::CheckBox^  m_EnableEditedPingsChangeCheckBox;



	protected:

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->checkedListBox = (gcnew System::Windows::Forms::CheckedListBox());
			this->panelParameters = (gcnew System::Windows::Forms::Panel());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->m_ManualBottomCorrectionCheckBox = (gcnew System::Windows::Forms::CheckBox());
			this->m_EnableEditedPingsChangeCheckBox = (gcnew System::Windows::Forms::CheckBox());
			this->tableLayoutPanel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// checkedListBox
			// 
			this->checkedListBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->checkedListBox->FormattingEnabled = true;
			this->checkedListBox->Location = System::Drawing::Point(3, 49);
			this->checkedListBox->Name = L"checkedListBox";
			this->checkedListBox->Size = System::Drawing::Size(153, 199);
			this->checkedListBox->TabIndex = 2;
			this->checkedListBox->SelectedIndexChanged += gcnew System::EventHandler(this, &BottomDetectionParamControl::checkedListBox_SelectedIndexChanged);
			// 
			// panelParameters
			// 
			this->panelParameters->Dock = System::Windows::Forms::DockStyle::Fill;
			this->panelParameters->Location = System::Drawing::Point(162, 49);
			this->panelParameters->Name = L"panelParameters";
			this->panelParameters->Size = System::Drawing::Size(234, 212);
			this->panelParameters->TabIndex = 3;
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 2;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				40)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				60)));
			this->tableLayoutPanel1->Controls->Add(this->panelParameters, 1, 2);
			this->tableLayoutPanel1->Controls->Add(this->checkedListBox, 0, 2);
			this->tableLayoutPanel1->Controls->Add(this->m_ManualBottomCorrectionCheckBox, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->m_EnableEditedPingsChangeCheckBox, 0, 1);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 3;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(399, 264);
			this->tableLayoutPanel1->TabIndex = 2;
			// 
			// m_ManualBottomCorrectionCheckBox
			// 
			this->m_ManualBottomCorrectionCheckBox->AutoSize = true;
			this->tableLayoutPanel1->SetColumnSpan(this->m_ManualBottomCorrectionCheckBox, 2);
			this->m_ManualBottomCorrectionCheckBox->Location = System::Drawing::Point(3, 3);
			this->m_ManualBottomCorrectionCheckBox->Name = L"m_ManualBottomCorrectionCheckBox";
			this->m_ManualBottomCorrectionCheckBox->Size = System::Drawing::Size(181, 17);
			this->m_ManualBottomCorrectionCheckBox->TabIndex = 0;
			this->m_ManualBottomCorrectionCheckBox->Text = L"Enable manual bottom correction";
			this->m_ManualBottomCorrectionCheckBox->UseVisualStyleBackColor = true;
			// 
			// m_EnableEditedPingsChangeCheckBox
			// 
			this->m_EnableEditedPingsChangeCheckBox->AutoSize = true;
			this->tableLayoutPanel1->SetColumnSpan(this->m_EnableEditedPingsChangeCheckBox, 2);
			this->m_EnableEditedPingsChangeCheckBox->Location = System::Drawing::Point(3, 26);
			this->m_EnableEditedPingsChangeCheckBox->Name = L"m_EnableEditedPingsChangeCheckBox";
			this->m_EnableEditedPingsChangeCheckBox->Size = System::Drawing::Size(353, 17);
			this->m_EnableEditedPingsChangeCheckBox->TabIndex = 4;
			this->m_EnableEditedPingsChangeCheckBox->Text = L"Overwrite manually edited bottom values with bottom detection results";
			this->m_EnableEditedPingsChangeCheckBox->UseVisualStyleBackColor = true;
			// 
			// BottomDetectionParamControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->tableLayoutPanel1);
			this->Name = L"BottomDetectionParamControl";
			this->Size = System::Drawing::Size(399, 264);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void checkedListBox_ItemCheck(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e);
	private: System::Void checkedListBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	public: System::Void ApplyDetailedView();

#pragma region Impl�mentation de IParamControl
	public: virtual void UpdateConfiguration();
	public: virtual void UpdateGUI();
#pragma endregion



	private: System::Windows::Forms::ItemCheckEventHandler^ m_CheckEventHandler;
	private: int m_SelectedIndex;

	private: BottomDetectionGenericControl^ m_GenericParamsControl;
	private: BottomDetectionContourLineControl^ m_ContourLineParamsControl;

	};
}
