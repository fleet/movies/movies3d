// -*- MC++ -*-
// ****************************************************************************
// Class: BottomDetectionParamForm
//
// Description: Fen�tre de configuration du module de detection du fond
//
// Projet: MOVIES3D
// Auteur: O.Tonck
// Date  : mars 2010
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "ParameterForm.h"
#include "BottomDetectionParamControl.h"

namespace MOVIESVIEWCPP {
	public ref class BottomDetectionParamForm : public ParameterForm
	{
	public:
		BottomDetectionParamForm(void) :
			ParameterForm()
		{
			// on veut pouvoir redimensionner cette fen�tre
			this->AutoSize = false;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->ClientSize = System::Drawing::Size(480, 440);
			// on ne met pas les boutons OK et annuler dans cette form
			// il faudrait impl�menter une config temporaire si on veut r�aliser 
			// le annuler du fait du multiusage de la textbox pour plusieurs modules
			panel1->Hide();
			SetParamControl(gcnew BottomDetectionParamControl(), L"Bottom Detection Parameters");
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &BottomDetectionParamForm::BottomDetectionParamForm_FormClosing);


		}

		property bool BottomCorrectionEnabled
		{
			bool get()
			{
				return ((BottomDetectionParamControl^)m_ParamControl)->BottomCorrectionEnabled;
			}
			void set(bool value)
			{
				((BottomDetectionParamControl^)m_ParamControl)->BottomCorrectionEnabled = value;
			}
		}

		property bool BottomCorrectionAvailable
		{
			void set(bool value)
			{
				((BottomDetectionParamControl^)m_ParamControl)->BottomCorrectionAvailable = value;
			}
		}

	private: System::Void BottomDetectionParamForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
		m_ParamControl->UpdateConfiguration();
		((BottomDetectionParamControl^)m_ParamControl)->ApplyDetailedView();
	}
	};
};
