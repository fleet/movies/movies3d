#include "BroadcastAndRecordParameterForm.h"

#include <math.h>

using namespace MOVIESVIEWCPP;

// mise � jour des param�tres du module � partir des param�tres IHM
void BroadcastAndRecordParameterForm::IHMToConfig()
{
	m_pParameterBroadcastAndRecord->m_enableFileOutput = checkBoxFileOutput->Checked;
	m_pParameterBroadcastAndRecord->m_enableNetworkOutput = checkBoxNetworkOutput->Checked;
	char* str = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBoxPrefix->Text).ToPointer();
	std::string ret(str);
	System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(str));
	m_pParameterBroadcastAndRecord->m_filePrefix = ret;
	str = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBoxDirectory->Text).ToPointer();
	std::string ret2(str);
	System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(str));
	m_pParameterBroadcastAndRecord->m_filePath = ret2;
	if (radioButtonSize->Checked)
	{
		m_pParameterBroadcastAndRecord->m_cutType = BaseKernel::ParameterBroadcastAndRecord::SIZE_CUT;
	}
	else if (radioButtonTime->Checked)
	{
		m_pParameterBroadcastAndRecord->m_cutType = BaseKernel::ParameterBroadcastAndRecord::TIME_CUT;
	}
	else
	{
		assert(false); // type de d�coupage inconnu
	}
	m_pParameterBroadcastAndRecord->m_fileMaxSize = Convert::ToUInt32(numericUpDownSize->Value);
	// pr�fixage de la date
	m_pParameterBroadcastAndRecord->m_prefixDate = checkBoxDatePrefix->Checked;
	// Dur�e
	unsigned int fileMaxTime;
	fileMaxTime = Convert::ToInt32(textBoxHour->Text) * 3600 + Convert::ToInt32(textBoxMinute->Text) * 60;
	m_pParameterBroadcastAndRecord->m_fileMaxTime = fileMaxTime;
	if (radioButtonXML->Checked)
	{
		m_pParameterBroadcastAndRecord->m_tramaType = BaseKernel::ParameterBroadcastAndRecord::XML_TRAMA;
	}
	else if (radioButtonCSV->Checked)
	{
		m_pParameterBroadcastAndRecord->m_tramaType = BaseKernel::ParameterBroadcastAndRecord::CSV_TRAMA;
	}
	else if (radioButtonNetCDF->Checked) 
	{
		m_pParameterBroadcastAndRecord->m_tramaType = BaseKernel::ParameterBroadcastAndRecord::NETCDF_TRAMA;
	}
	else
	{
		assert(false); // type de trame inconnu
	}
}

// mise � jour des param�tres IHM en fonction des param�tres du module
void BroadcastAndRecordParameterForm::ConfigToIHM()
{
	checkBoxFileOutput->Checked = m_pParameterBroadcastAndRecord->m_enableFileOutput;
	checkBoxNetworkOutput->Checked = m_pParameterBroadcastAndRecord->m_enableNetworkOutput;
	textBoxPrefix->Text = gcnew String(m_pParameterBroadcastAndRecord->m_filePrefix.c_str());
	textBoxDirectory->Text = gcnew String(m_pParameterBroadcastAndRecord->m_filePath.c_str());
	radioButtonSize->Checked = m_pParameterBroadcastAndRecord->m_cutType == BaseKernel::ParameterBroadcastAndRecord::SIZE_CUT;
	radioButtonTime->Checked = m_pParameterBroadcastAndRecord->m_cutType == BaseKernel::ParameterBroadcastAndRecord::TIME_CUT;
	numericUpDownSize->Value = m_pParameterBroadcastAndRecord->m_fileMaxSize;
	// pr�fixage de la date
	checkBoxDatePrefix->Checked = m_pParameterBroadcastAndRecord->m_prefixDate;
	// Dur�e
	int hours = (int)floor(m_pParameterBroadcastAndRecord->m_fileMaxTime / 3600.0);
	int minutes = (int)floor((m_pParameterBroadcastAndRecord->m_fileMaxTime - (hours * 3600)) / 60.0);
	textBoxHour->Text = Convert::ToString(hours);
	textBoxMinute->Text = Convert::ToString(minutes);
	radioButtonXML->Checked = m_pParameterBroadcastAndRecord->m_tramaType == BaseKernel::ParameterBroadcastAndRecord::XML_TRAMA;
	radioButtonCSV->Checked = m_pParameterBroadcastAndRecord->m_tramaType == BaseKernel::ParameterBroadcastAndRecord::CSV_TRAMA;
	radioButtonNetCDF->Checked = m_pParameterBroadcastAndRecord->m_tramaType == BaseKernel::ParameterBroadcastAndRecord::NETCDF_TRAMA;

	// on grise les �l�ments inutiles
	UpdateEnabledControls();
}


// grise / d�grise les controles de IHM pour le r�seau inutilis�s
void BroadcastAndRecordParameterForm::UpdateEnabledControls()
{
	// sortie r�seau
	buttonNetwork->Enabled = checkBoxNetworkOutput->Checked;
	// sortie fichier
	textBoxDirectory->Enabled = checkBoxFileOutput->Checked;
	textBoxHour->Enabled = checkBoxFileOutput->Checked  && checkBoxDatePrefix->Checked;
	textBoxMinute->Enabled = checkBoxFileOutput->Checked  && checkBoxDatePrefix->Checked;
	textBoxPrefix->Enabled = checkBoxFileOutput->Checked;
	buttonBrowse->Enabled = checkBoxFileOutput->Checked;
	radioButtonSize->Enabled = checkBoxFileOutput->Checked && checkBoxDatePrefix->Checked;
	radioButtonTime->Enabled = checkBoxFileOutput->Checked && checkBoxDatePrefix->Checked;
	numericUpDownSize->Enabled = checkBoxFileOutput->Checked && checkBoxDatePrefix->Checked;
	checkBoxDatePrefix->Enabled = checkBoxFileOutput->Checked;
	// sortie r�seau ou fichier
	radioButtonCSV->Enabled = checkBoxFileOutput->Checked || checkBoxNetworkOutput->Checked;
	radioButtonXML->Enabled = checkBoxFileOutput->Checked || checkBoxNetworkOutput->Checked;
	radioButtonNetCDF->Enabled = checkBoxFileOutput->Checked || checkBoxNetworkOutput->Checked;
	buttonCustomize->Enabled = !radioButtonNetCDF->Checked && (checkBoxFileOutput->Checked || checkBoxNetworkOutput->Checked);
}
