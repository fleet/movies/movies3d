#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


// D�pendences
#include "TramaDescriptorForm.h"

#include "M3DKernel/parameter/ParameterBroadcastAndRecord.h"
#include "MovNetworkForm.h"

#include <cassert>


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de BroadcastAndRecordParameterForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class BroadcastAndRecordParameterForm : public System::Windows::Forms::Form
	{
	public:
		BroadcastAndRecordParameterForm(BaseKernel::ParameterBroadcastAndRecord * cfg)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
			m_pParameterBroadcastAndRecord = cfg;

			ConfigToIHM();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~BroadcastAndRecordParameterForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckBox^  checkBoxNetworkOutput;
	protected:
	private: System::Windows::Forms::CheckBox^  checkBoxFileOutput;
	private: System::Windows::Forms::Button^  buttonOK;
	private: System::Windows::Forms::Button^  buttonCancel;
	private: System::Windows::Forms::GroupBox^  groupBoxNetwork;
	private: System::Windows::Forms::Button^  buttonNetwork;
	private: System::Windows::Forms::GroupBox^  groupBoxFile;
	private: System::Windows::Forms::RadioButton^  radioButtonTime;
	private: System::Windows::Forms::RadioButton^  radioButtonSize;
	private: System::Windows::Forms::TextBox^  textBoxDirectory;



	private: System::Windows::Forms::Label^  labelDirectory;
	private: System::Windows::Forms::TextBox^  textBoxPrefix;


	private: System::Windows::Forms::Label^  labelPrefix;
	private: System::Windows::Forms::Label^  labelMo;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownSize;

	private: System::Windows::Forms::Button^  buttonBrowse;
	private: System::Windows::Forms::Label^  labelMinute;
	private: System::Windows::Forms::Label^  labelHour;
	private: System::Windows::Forms::TextBox^  textBoxMinute;
	private: System::Windows::Forms::TextBox^  textBoxHour;
	private: System::Windows::Forms::GroupBox^  groupBoxCustomizeTramas;
	private: System::Windows::Forms::Button^  buttonCustomize;

	private: System::Windows::Forms::RadioButton^  radioButtonCSV;
	private: System::Windows::Forms::RadioButton^  radioButtonXML;
	private: System::Windows::Forms::RadioButton^  radioButtonNetCDF;

	private: System::Windows::Forms::FolderBrowserDialog^  folderBrowserDialog;
	private: System::Windows::Forms::CheckBox^  checkBoxDatePrefix;




	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

		// mise � jour des param�tres du module � partir des param�tres IHM
		void IHMToConfig();
		// mise � jour des param�tres IHM en fonction des param�tres du module
		void ConfigToIHM();

		// grise / d�grise les controles de IHM inutilis�s
		void UpdateEnabledControls();

		//Donn�es
		//configuration associ�e
		BaseKernel::ParameterBroadcastAndRecord * m_pParameterBroadcastAndRecord;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->checkBoxNetworkOutput = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxFileOutput = (gcnew System::Windows::Forms::CheckBox());
			this->buttonOK = (gcnew System::Windows::Forms::Button());
			this->buttonCancel = (gcnew System::Windows::Forms::Button());
			this->groupBoxNetwork = (gcnew System::Windows::Forms::GroupBox());
			this->buttonNetwork = (gcnew System::Windows::Forms::Button());
			this->groupBoxFile = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxDatePrefix = (gcnew System::Windows::Forms::CheckBox());
			this->labelMinute = (gcnew System::Windows::Forms::Label());
			this->labelHour = (gcnew System::Windows::Forms::Label());
			this->textBoxMinute = (gcnew System::Windows::Forms::TextBox());
			this->textBoxHour = (gcnew System::Windows::Forms::TextBox());
			this->buttonBrowse = (gcnew System::Windows::Forms::Button());
			this->labelMo = (gcnew System::Windows::Forms::Label());
			this->numericUpDownSize = (gcnew System::Windows::Forms::NumericUpDown());
			this->radioButtonTime = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonSize = (gcnew System::Windows::Forms::RadioButton());
			this->textBoxDirectory = (gcnew System::Windows::Forms::TextBox());
			this->labelDirectory = (gcnew System::Windows::Forms::Label());
			this->textBoxPrefix = (gcnew System::Windows::Forms::TextBox());
			this->labelPrefix = (gcnew System::Windows::Forms::Label());
			this->groupBoxCustomizeTramas = (gcnew System::Windows::Forms::GroupBox());
			this->radioButtonNetCDF = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonCSV = (gcnew System::Windows::Forms::RadioButton());
			this->buttonCustomize = (gcnew System::Windows::Forms::Button());
			this->radioButtonXML = (gcnew System::Windows::Forms::RadioButton());
			this->folderBrowserDialog = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->groupBoxNetwork->SuspendLayout();
			this->groupBoxFile->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownSize))->BeginInit();
			this->groupBoxCustomizeTramas->SuspendLayout();
			this->SuspendLayout();
			// 
			// checkBoxNetworkOutput
			// 
			this->checkBoxNetworkOutput->AutoSize = true;
			this->checkBoxNetworkOutput->Location = System::Drawing::Point(9, 25);
			this->checkBoxNetworkOutput->Name = L"checkBoxNetworkOutput";
			this->checkBoxNetworkOutput->Size = System::Drawing::Size(133, 17);
			this->checkBoxNetworkOutput->TabIndex = 0;
			this->checkBoxNetworkOutput->Text = L"Enable network output";
			this->checkBoxNetworkOutput->UseVisualStyleBackColor = true;
			this->checkBoxNetworkOutput->CheckedChanged += gcnew System::EventHandler(this, &BroadcastAndRecordParameterForm::checkBoxNetworkOutput_CheckedChanged);
			// 
			// checkBoxFileOutput
			// 
			this->checkBoxFileOutput->AutoSize = true;
			this->checkBoxFileOutput->Location = System::Drawing::Point(8, 19);
			this->checkBoxFileOutput->Name = L"checkBoxFileOutput";
			this->checkBoxFileOutput->Size = System::Drawing::Size(108, 17);
			this->checkBoxFileOutput->TabIndex = 1;
			this->checkBoxFileOutput->Text = L"Enable file output";
			this->checkBoxFileOutput->UseVisualStyleBackColor = true;
			this->checkBoxFileOutput->CheckedChanged += gcnew System::EventHandler(this, &BroadcastAndRecordParameterForm::checkBoxFileOutput_CheckedChanged);
			// 
			// buttonOK
			// 
			this->buttonOK->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->buttonOK->Location = System::Drawing::Point(115, 311);
			this->buttonOK->Name = L"buttonOK";
			this->buttonOK->Size = System::Drawing::Size(105, 30);
			this->buttonOK->TabIndex = 2;
			this->buttonOK->Text = L"OK";
			this->buttonOK->UseVisualStyleBackColor = true;
			this->buttonOK->Click += gcnew System::EventHandler(this, &BroadcastAndRecordParameterForm::buttonOK_Click);
			// 
			// buttonCancel
			// 
			this->buttonCancel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->buttonCancel->Location = System::Drawing::Point(362, 311);
			this->buttonCancel->Name = L"buttonCancel";
			this->buttonCancel->Size = System::Drawing::Size(105, 30);
			this->buttonCancel->TabIndex = 3;
			this->buttonCancel->Text = L"Cancel";
			this->buttonCancel->UseVisualStyleBackColor = true;
			this->buttonCancel->Click += gcnew System::EventHandler(this, &BroadcastAndRecordParameterForm::buttonCancel_Click);
			// 
			// groupBoxNetwork
			// 
			this->groupBoxNetwork->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxNetwork->Controls->Add(this->buttonNetwork);
			this->groupBoxNetwork->Controls->Add(this->checkBoxNetworkOutput);
			this->groupBoxNetwork->Location = System::Drawing::Point(10, 8);
			this->groupBoxNetwork->Name = L"groupBoxNetwork";
			this->groupBoxNetwork->Size = System::Drawing::Size(559, 55);
			this->groupBoxNetwork->TabIndex = 4;
			this->groupBoxNetwork->TabStop = false;
			this->groupBoxNetwork->Text = L"Network output";
			// 
			// buttonNetwork
			// 
			this->buttonNetwork->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->buttonNetwork->Location = System::Drawing::Point(278, 14);
			this->buttonNetwork->Name = L"buttonNetwork";
			this->buttonNetwork->Size = System::Drawing::Size(275, 35);
			this->buttonNetwork->TabIndex = 1;
			this->buttonNetwork->Text = L"Network Parameters...";
			this->buttonNetwork->UseVisualStyleBackColor = true;
			this->buttonNetwork->Click += gcnew System::EventHandler(this, &BroadcastAndRecordParameterForm::buttonNetwork_Click);
			// 
			// groupBoxFile
			// 
			this->groupBoxFile->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxFile->Controls->Add(this->checkBoxDatePrefix);
			this->groupBoxFile->Controls->Add(this->labelMinute);
			this->groupBoxFile->Controls->Add(this->labelHour);
			this->groupBoxFile->Controls->Add(this->textBoxMinute);
			this->groupBoxFile->Controls->Add(this->textBoxHour);
			this->groupBoxFile->Controls->Add(this->buttonBrowse);
			this->groupBoxFile->Controls->Add(this->labelMo);
			this->groupBoxFile->Controls->Add(this->numericUpDownSize);
			this->groupBoxFile->Controls->Add(this->radioButtonTime);
			this->groupBoxFile->Controls->Add(this->radioButtonSize);
			this->groupBoxFile->Controls->Add(this->textBoxDirectory);
			this->groupBoxFile->Controls->Add(this->labelDirectory);
			this->groupBoxFile->Controls->Add(this->textBoxPrefix);
			this->groupBoxFile->Controls->Add(this->labelPrefix);
			this->groupBoxFile->Controls->Add(this->checkBoxFileOutput);
			this->groupBoxFile->Location = System::Drawing::Point(11, 73);
			this->groupBoxFile->Name = L"groupBoxFile";
			this->groupBoxFile->Size = System::Drawing::Size(558, 105);
			this->groupBoxFile->TabIndex = 5;
			this->groupBoxFile->TabStop = false;
			this->groupBoxFile->Text = L"File output";
			// 
			// checkBoxDatePrefix
			// 
			this->checkBoxDatePrefix->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->checkBoxDatePrefix->AutoSize = true;
			this->checkBoxDatePrefix->Location = System::Drawing::Point(351, 19);
			this->checkBoxDatePrefix->Name = L"checkBoxDatePrefix";
			this->checkBoxDatePrefix->Size = System::Drawing::Size(174, 17);
			this->checkBoxDatePrefix->TabIndex = 15;
			this->checkBoxDatePrefix->Text = L"Prefix files with current datetime";
			this->checkBoxDatePrefix->UseVisualStyleBackColor = true;
			this->checkBoxDatePrefix->CheckedChanged += gcnew System::EventHandler(this, &BroadcastAndRecordParameterForm::checkBoxDatePrefix_CheckedChanged);
			// 
			// labelMinute
			// 
			this->labelMinute->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->labelMinute->AutoSize = true;
			this->labelMinute->Location = System::Drawing::Point(527, 72);
			this->labelMinute->Name = L"labelMinute";
			this->labelMinute->Size = System::Drawing::Size(15, 13);
			this->labelMinute->TabIndex = 14;
			this->labelMinute->Text = L"m";
			// 
			// labelHour
			// 
			this->labelHour->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->labelHour->AutoSize = true;
			this->labelHour->Location = System::Drawing::Point(472, 72);
			this->labelHour->Name = L"labelHour";
			this->labelHour->Size = System::Drawing::Size(13, 13);
			this->labelHour->TabIndex = 13;
			this->labelHour->Text = L"h";
			// 
			// textBoxMinute
			// 
			this->textBoxMinute->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->textBoxMinute->Location = System::Drawing::Point(487, 68);
			this->textBoxMinute->Name = L"textBoxMinute";
			this->textBoxMinute->Size = System::Drawing::Size(36, 20);
			this->textBoxMinute->TabIndex = 12;
			// 
			// textBoxHour
			// 
			this->textBoxHour->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->textBoxHour->Location = System::Drawing::Point(437, 68);
			this->textBoxHour->Name = L"textBoxHour";
			this->textBoxHour->Size = System::Drawing::Size(34, 20);
			this->textBoxHour->TabIndex = 11;
			// 
			// buttonBrowse
			// 
			this->buttonBrowse->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->buttonBrowse->Location = System::Drawing::Point(314, 69);
			this->buttonBrowse->Name = L"buttonBrowse";
			this->buttonBrowse->Size = System::Drawing::Size(24, 19);
			this->buttonBrowse->TabIndex = 10;
			this->buttonBrowse->Text = L"...";
			this->buttonBrowse->UseVisualStyleBackColor = true;
			this->buttonBrowse->Click += gcnew System::EventHandler(this, &BroadcastAndRecordParameterForm::buttonBrowse_Click);
			// 
			// labelMo
			// 
			this->labelMo->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->labelMo->AutoSize = true;
			this->labelMo->Location = System::Drawing::Point(493, 44);
			this->labelMo->Name = L"labelMo";
			this->labelMo->Size = System::Drawing::Size(22, 13);
			this->labelMo->TabIndex = 9;
			this->labelMo->Text = L"Mo";
			// 
			// numericUpDownSize
			// 
			this->numericUpDownSize->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->numericUpDownSize->Location = System::Drawing::Point(437, 41);
			this->numericUpDownSize->Name = L"numericUpDownSize";
			this->numericUpDownSize->Size = System::Drawing::Size(50, 20);
			this->numericUpDownSize->TabIndex = 8;
			// 
			// radioButtonTime
			// 
			this->radioButtonTime->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->radioButtonTime->AutoSize = true;
			this->radioButtonTime->Location = System::Drawing::Point(351, 71);
			this->radioButtonTime->Name = L"radioButtonTime";
			this->radioButtonTime->Size = System::Drawing::Size(81, 17);
			this->radioButtonTime->TabIndex = 7;
			this->radioButtonTime->TabStop = true;
			this->radioButtonTime->Text = L"Split by time";
			this->radioButtonTime->UseVisualStyleBackColor = true;
			// 
			// radioButtonSize
			// 
			this->radioButtonSize->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->radioButtonSize->AutoSize = true;
			this->radioButtonSize->Location = System::Drawing::Point(351, 41);
			this->radioButtonSize->Name = L"radioButtonSize";
			this->radioButtonSize->Size = System::Drawing::Size(80, 17);
			this->radioButtonSize->TabIndex = 6;
			this->radioButtonSize->TabStop = true;
			this->radioButtonSize->Text = L"Split by size";
			this->radioButtonSize->UseVisualStyleBackColor = true;
			// 
			// textBoxDirectory
			// 
			this->textBoxDirectory->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBoxDirectory->Location = System::Drawing::Point(63, 69);
			this->textBoxDirectory->Name = L"textBoxDirectory";
			this->textBoxDirectory->Size = System::Drawing::Size(245, 20);
			this->textBoxDirectory->TabIndex = 5;
			// 
			// labelDirectory
			// 
			this->labelDirectory->AutoSize = true;
			this->labelDirectory->Location = System::Drawing::Point(10, 72);
			this->labelDirectory->Name = L"labelDirectory";
			this->labelDirectory->Size = System::Drawing::Size(49, 13);
			this->labelDirectory->TabIndex = 4;
			this->labelDirectory->Text = L"Directory";
			// 
			// textBoxPrefix
			// 
			this->textBoxPrefix->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBoxPrefix->Location = System::Drawing::Point(63, 43);
			this->textBoxPrefix->Name = L"textBoxPrefix";
			this->textBoxPrefix->Size = System::Drawing::Size(245, 20);
			this->textBoxPrefix->TabIndex = 3;
			// 
			// labelPrefix
			// 
			this->labelPrefix->AutoSize = true;
			this->labelPrefix->Location = System::Drawing::Point(10, 46);
			this->labelPrefix->Name = L"labelPrefix";
			this->labelPrefix->Size = System::Drawing::Size(33, 13);
			this->labelPrefix->TabIndex = 2;
			this->labelPrefix->Text = L"Prefix";
			// 
			// groupBoxCustomizeTramas
			// 
			this->groupBoxCustomizeTramas->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxCustomizeTramas->Controls->Add(this->radioButtonNetCDF);
			this->groupBoxCustomizeTramas->Controls->Add(this->radioButtonCSV);
			this->groupBoxCustomizeTramas->Controls->Add(this->buttonCustomize);
			this->groupBoxCustomizeTramas->Controls->Add(this->radioButtonXML);
			this->groupBoxCustomizeTramas->Location = System::Drawing::Point(15, 184);
			this->groupBoxCustomizeTramas->Name = L"groupBoxCustomizeTramas";
			this->groupBoxCustomizeTramas->Size = System::Drawing::Size(554, 110);
			this->groupBoxCustomizeTramas->TabIndex = 6;
			this->groupBoxCustomizeTramas->TabStop = false;
			this->groupBoxCustomizeTramas->Text = L"Customize tramas";
			// 
			// radioButtonNetCDF
			// 
			this->radioButtonNetCDF->AutoSize = true;
			this->radioButtonNetCDF->Location = System::Drawing::Point(9, 80);
			this->radioButtonNetCDF->Name = L"radioButtonNetCDF";
			this->radioButtonNetCDF->Size = System::Drawing::Size(98, 17);
			this->radioButtonNetCDF->TabIndex = 17;
			this->radioButtonNetCDF->TabStop = true;
			this->radioButtonNetCDF->Text = L"NetCDF Format";
			this->radioButtonNetCDF->UseVisualStyleBackColor = true;
			this->radioButtonNetCDF->CheckedChanged += gcnew System::EventHandler(this, &BroadcastAndRecordParameterForm::radioButtonNetCDF_CheckedChanged);
			// 
			// radioButtonCSV
			// 
			this->radioButtonCSV->AutoSize = true;
			this->radioButtonCSV->Location = System::Drawing::Point(9, 49);
			this->radioButtonCSV->Name = L"radioButtonCSV";
			this->radioButtonCSV->Size = System::Drawing::Size(78, 17);
			this->radioButtonCSV->TabIndex = 16;
			this->radioButtonCSV->TabStop = true;
			this->radioButtonCSV->Text = L"CSV format";
			this->radioButtonCSV->UseVisualStyleBackColor = true;
			// 
			// buttonCustomize
			// 
			this->buttonCustomize->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->buttonCustomize->Location = System::Drawing::Point(127, 21);
			this->buttonCustomize->Name = L"buttonCustomize";
			this->buttonCustomize->Size = System::Drawing::Size(411, 45);
			this->buttonCustomize->TabIndex = 0;
			this->buttonCustomize->Text = L"Select tramas to be archived / broadcasted...";
			this->buttonCustomize->UseVisualStyleBackColor = true;
			this->buttonCustomize->Click += gcnew System::EventHandler(this, &BroadcastAndRecordParameterForm::buttonCustomize_Click);
			// 
			// radioButtonXML
			// 
			this->radioButtonXML->AutoSize = true;
			this->radioButtonXML->Location = System::Drawing::Point(9, 19);
			this->radioButtonXML->Name = L"radioButtonXML";
			this->radioButtonXML->Size = System::Drawing::Size(79, 17);
			this->radioButtonXML->TabIndex = 15;
			this->radioButtonXML->TabStop = true;
			this->radioButtonXML->Text = L"XML format";
			this->radioButtonXML->UseVisualStyleBackColor = true;
			// 
			// BroadcastAndRecordParameterForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(577, 344);
			this->Controls->Add(this->groupBoxCustomizeTramas);
			this->Controls->Add(this->groupBoxFile);
			this->Controls->Add(this->groupBoxNetwork);
			this->Controls->Add(this->buttonCancel);
			this->Controls->Add(this->buttonOK);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->MinimumSize = System::Drawing::Size(449, 349);
			this->Name = L"BroadcastAndRecordParameterForm";
			this->Text = L"BroadcastAndRecordParameterForm";
			this->groupBoxNetwork->ResumeLayout(false);
			this->groupBoxNetwork->PerformLayout();
			this->groupBoxFile->ResumeLayout(false);
			this->groupBoxFile->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownSize))->EndInit();
			this->groupBoxCustomizeTramas->ResumeLayout(false);
			this->groupBoxCustomizeTramas->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

		// Appel de l'IHM de parametrage des trames choisies pour l'archivage / broadcast
	private: System::Void buttonCustomize_Click(System::Object^  sender, System::EventArgs^  e) {
		TramaDescriptorForm ^ref = gcnew TramaDescriptorForm(&m_pParameterBroadcastAndRecord->m_tramaDescriptor);
		ref->ShowDialog();
	}
			 // Bouton OK
	private: System::Void buttonOK_Click(System::Object^  sender, System::EventArgs^  e) {
		try
		{
			IHMToConfig();
			Close();
		}
		catch (Exception^)
		{
			MessageBox::Show("Invalid configuration.");
		}
	}
			 // Bouton Cancel
	private: System::Void buttonCancel_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}

			 // activation / d�sactivation du broadcast r�seau
	private: System::Void checkBoxNetworkOutput_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		UpdateEnabledControls();
	}
			 // activation / d�sactivation archivage fichiers
	private: System::Void checkBoxFileOutput_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		UpdateEnabledControls();
	}

			 // selection du r�pertoire des sorties
	private: System::Void buttonBrowse_Click(System::Object^  sender, System::EventArgs^  e) {
		folderBrowserDialog->SelectedPath = textBoxDirectory->Text;
		if (folderBrowserDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			textBoxDirectory->Text = folderBrowserDialog->SelectedPath;
		}
	}

			 // ouverture configuration reseau
	private: System::Void buttonNetwork_Click(System::Object^  sender, System::EventArgs^  e) {
		MovNetworkForm ^ref = gcnew MovNetworkForm();
		ref->ShowDialog();
	}
			 // on ne permet pas le d�coupage des fichiers si on ne pr�fixe pas le nom des fichiers de la date
	private: System::Void checkBoxDatePrefix_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		UpdateEnabledControls();
	}

			 // Si le format NetCDF est demand�, on d�sactive la s�lection des trames
	private: System::Void radioButtonNetCDF_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		UpdateEnabledControls();
	}

};
}
