
#define NOMINMAX

#include "CalibrationControl.h"
#include "M3DKernel/M3DKernel.h"
#include "ModuleManager/ModuleManager.h"
#include "Calibration/CalibrationModule.h"
#include "Calibration/CalibrationParameter.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"

#include <set>

#include <msclr/marshal_cppstd.h>

using namespace MOVIESVIEWCPP;

using namespace System::Windows::Forms::DataVisualization::Charting;

namespace
{
	constexpr const char * LoggerName = "MoviesView.SpectralAnalysis.CalibrationControl";
}

CalibrationControl::CalibrationControl()
{
	InitializeComponent();
	m_chartInteractor = gcnew ChartInteractor();

	auto charts = gcnew array<System::Windows::Forms::DataVisualization::Charting::Chart^>{chartGain, chartSaCorr, chartBeamAlong, chartBeamAthwart, chartAngleAlong, chartAngleAthwart};
	for each(auto chart in charts)
	{
		m_chartSeriesMapFM.Add(chart, gcnew System::Collections::Generic::Dictionary<System::String^, System::Windows::Forms::DataVisualization::Charting::Series^>());
		m_chartSeriesMapCW.Add(chart, gcnew System::Collections::Generic::Dictionary<System::String^, System::Windows::Forms::DataVisualization::Charting::Series^>());
		m_chartInteractor->Install(chart);
	}
}

void CalibrationControl::UpdateConfiguration()
{
	auto filename = msclr::interop::marshal_as<std::string>(txtCalibrationFile->Text);
	CModuleManager::getInstance()->GetCalibrationModule()->GetCalibrationParameter().setCalibrationFile(filename);
}

System::Void CalibrationControl::enabledCheckbox_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	CModuleManager::getInstance()->GetCalibrationModule()->setEnable(enabledCheckbox->Checked);
}

void CalibrationControl::UpdateGUI()
{
	enabledCheckbox->Checked = CModuleManager::getInstance()->GetCalibrationModule()->getEnable();

	auto& params = CModuleManager::getInstance()->GetCalibrationModule()->GetCalibrationParameter();
	msclr::interop::marshal_context context;
	txtCalibrationFile->Text = gcnew String(params.getCalibrationFile().c_str());

	std::map<std::string, std::vector<CalibrationData>> valuesFM;
	std::map<std::string, std::vector<CalibrationData>> valuesCW;
	CalibrationParameter::parseCalibrationFile(params.getCalibrationFile(), valuesFM, valuesCW);
	updateCalibrationData(valuesFM, valuesCW);
}

System::Void CalibrationControl::btnBrowse_Click(System::Object^  sender, System::EventArgs^  e)
{
	auto result = openFileDialog->ShowDialog();
	if (result == DialogResult::OK)
	{
		txtCalibrationFile->Text = openFileDialog->FileName;

		auto filename = msclr::interop::marshal_as<std::string>(txtCalibrationFile->Text);
		std::map<std::string, std::vector<CalibrationData>> valuesFM;
		std::map<std::string, std::vector<CalibrationData>> valuesCW;
		CalibrationParameter::parseCalibrationFile(filename, valuesFM, valuesCW);
		updateCalibrationData(valuesFM, valuesCW);
	}
}

void CalibrationControl::UpdateChannelInfoGridTable(const std::map<std::string, std::vector<CalibrationData>>& values)
{
	channelInfoGridView->Columns->Clear();
	channelInfoGridView->Rows->Clear();

	channelInfoGridView->Columns->Add("Transducer", "Transducer");
	channelInfoGridView->Columns->Add("Sv Correction", "Computed Sv Correction");
	channelInfoGridView->Columns[0]->AutoSizeMode = DataGridViewAutoSizeColumnMode::Fill;
	channelInfoGridView->Columns[0]->SortMode = DataGridViewColumnSortMode::NotSortable;
	channelInfoGridView->Columns[1]->AutoSizeMode = DataGridViewAutoSizeColumnMode::Fill;
	channelInfoGridView->Columns[1]->SortMode = DataGridViewColumnSortMode::NotSortable;

	M3DKernel * pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	
	CurrentSounderDefinition & sounderDef = pKernel->getObjectMgr()->GetSounderDefinition();
	for (unsigned int iSound = 0; iSound < sounderDef.GetNbSounder(); iSound++)
	{
		Sounder * pSound = sounderDef.GetSounder(iSound);
		if (pSound)
		{
			if (pSound->m_isMultiBeam) continue;

			for (unsigned int iTrans = 0; iTrans < pSound->GetTransducerCount(); iTrans++)
			{
				Transducer * pTrans = pSound->GetTransducer(iTrans);
				for (size_t iChan = 0; iChan < pTrans->GetChannelId().size(); iChan++)
				{
					SoftChannel * pSoftChan = pTrans->getSoftChannel(pTrans->GetChannelId()[iChan]);

					bool found = false;
					double svCorr;
					auto it = values.find(pSoftChan->m_transNameShort);
					if (it != values.end()) 
					{
						std::vector<CalibrationData> calibrationData = it->second;
						svCorr = CModuleManager::getInstance()->GetCalibrationModule()->getSvCorr(pSoftChan, calibrationData, found);
					}
					
					int rowId = channelInfoGridView->Rows->Add();
					channelInfoGridView->Rows[rowId]->Cells["Transducer"]->Value = gcnew String(pTrans->m_transName);
					if (found)
					{
						channelInfoGridView->Rows[rowId]->Cells["Sv Correction"]->Value = gcnew Double(svCorr);
					}
					else
					{
						channelInfoGridView->Rows[rowId]->Cells["Sv Correction"]->Value = gcnew String("-");
						M3D_LOG_WARN(LoggerName, Log::format("Channel not found: %s", pSoftChan->m_transNameShort));
					}
				}
			}
		}
	}
	pKernel->Unlock();

	for (auto it = values.cbegin(); it != values.cend(); ++it)
	{
		std::string transducer = it->first;
		std::vector<CalibrationData> data = it->second;

		//int rowId = channelInfoGridView->Rows->Add();
		//channelInfoGridView->Rows[rowId]->Cells["Transducer"]->Value = gcnew String(transducer.c_str());
	}
}
namespace {

	Series ^ makeSerie(System::Drawing::Color color, int lineWidth)
	{
		Series ^serie = gcnew Series();
		serie->ChartType = SeriesChartType::Line;
		serie->BorderWidth = lineWidth;
		serie->BorderColor = color;
		serie->IsVisibleInLegend = true;
		return serie;
	}

	Series ^ makeFMSeries()
	{
		return makeSerie(Color::Blue, 2);
	}

	Series ^ makeCWSeries()
	{
		return makeSerie(Color::Red, 2);
	}

	void roundAxis(System::Windows::Forms::DataVisualization::Charting::Axis ^axis, double minValue, double maxValue, int numSteps)
	{
		double stepSize = 8;
		double intervalSize = maxValue - minValue;
		if (numSteps > 0)
		{
			const double v = intervalSize / numSteps;
			if (v != 0.0)
			{
				const double lx = log10(fabs(v));
				const double p = floor(lx);

				const double fraction = pow(10, lx - p);

				int n = 10;
				while ((n > 1) && (fraction <= n / 2))
					n /= 2;

				stepSize = n * pow(10, p);
				if (v < 0)
					stepSize = -stepSize;
			}
		}

		axis->Minimum = minValue;
		axis->Maximum = maxValue;
		axis->Interval = stepSize;
		axis->IntervalOffset = -fmod(minValue, stepSize);
	}
}


void CalibrationControl::updateCalibrationData(
	const std::map<std::string, std::vector<CalibrationData>>& valuesFM
	, const std::map<std::string, std::vector<CalibrationData>>& valuesCW)
{
	UpdateChannelInfoGridTable(valuesCW);

	cbTransducers->Items->Clear();

	for each (auto pair in m_chartSeriesMapFM)
	{
		pair.Key->Series->Clear();
		pair.Value->Clear();
	}

	for each (auto pair in m_chartSeriesMapCW)
	{
		pair.Key->Series->Clear();
		pair.Value->Clear();
	}

	// Make a list of available transducers
	std::set<std::string> transducers;
	for (auto it = valuesFM.cbegin(); it != valuesFM.cend(); ++it)
	{
		transducers.insert(it->first);
	}

	for (auto it = valuesCW.cbegin(); it != valuesCW.cend(); ++it)
	{
		transducers.insert(it->first);
	}

	for (auto it = transducers.cbegin(); it != transducers.cend(); ++it)
	{
		const std::string & transducer = *it;
		String ^ strTranducer = gcnew String(transducer.c_str());
		
		bool hasFM = false;
		std::vector<CalibrationData> fvaluesFM;
		if (valuesFM.find(transducer) != valuesFM.end())
		{
			fvaluesFM = valuesFM.at(transducer);
			hasFM = (fvaluesFM.size() > 0);
		}

		bool hasCW = false;
		std::vector<CalibrationData> fvaluesCW;
		if (valuesCW.find(transducer) != valuesCW.end())
		{
			fvaluesCW = valuesCW.at(transducer);
			hasCW = (fvaluesCW.size() > 0);
		}

		if (hasCW || hasFM)
		{
			cbTransducers->Items->Add(strTranducer);
		}

		double fmin = std::numeric_limits<double>::max();
		double fmax = -fmin;

		for (auto value : fvaluesFM)
		{
			fmin = std::min(fmin, value.Freq);
			fmax = std::max(fmax, value.Freq);
		}

		for (auto value : fvaluesCW)
		{
			fmin = std::min(fmin, value.Freq);
			fmax = std::max(fmax, value.Freq);
		}

		if (fmin == fmax)
		{
			fmin *= 0.9;
			fmax *= 1.1;
		}

		if(hasFM)
		{
			auto s_gainFM = makeFMSeries();
			auto s_saCorrFM = makeFMSeries();
			auto s_beamAlongFM = makeFMSeries();
			auto s_beamAthwartFM = makeFMSeries();
			auto s_angleAlongFM = makeFMSeries();
			auto s_angleAthwartFM = makeFMSeries();

			for (auto value : fvaluesFM)
			{
				s_gainFM->Points->AddXY(value.Freq * 1e-3, value.Gain);
				s_saCorrFM->Points->AddXY(value.Freq * 1e-3, value.SaCorr);
				s_beamAlongFM->Points->AddXY(value.Freq * 1e-3, value.BeamAlong);
				s_beamAthwartFM->Points->AddXY(value.Freq * 1e-3, value.BeamAthwart);
				s_angleAlongFM->Points->AddXY(value.Freq * 1e-3, value.OffsetAlong);
				s_angleAthwartFM->Points->AddXY(value.Freq * 1e-3, value.OffsetAthwart);
			}

			m_chartSeriesMapFM[chartGain][strTranducer]  = s_gainFM;
			m_chartSeriesMapFM[chartSaCorr][strTranducer] = s_saCorrFM;
			m_chartSeriesMapFM[chartBeamAthwart][strTranducer] = s_beamAlongFM;
			m_chartSeriesMapFM[chartBeamAlong][strTranducer] = s_beamAthwartFM;
			m_chartSeriesMapFM[chartAngleAthwart][strTranducer] = s_angleAlongFM;
			m_chartSeriesMapFM[chartAngleAlong][strTranducer] = s_angleAthwartFM;
		}
				
		if (hasCW)
		{
			auto s_gainCW = makeCWSeries();
			auto s_saCorrCW = makeCWSeries();
			auto s_beamAlongCW = makeCWSeries();
			auto s_beamAthwartCW = makeCWSeries();
			auto s_angleAlongCW = makeCWSeries();
			auto s_angleAthwartCW = makeCWSeries();
			
			// extend CW
			if (fvaluesCW.size() > 0)
			{
				auto firstValue = fvaluesCW.front();
				s_gainCW->Points->AddXY(fmin * 1e-3, firstValue.Gain);
				s_saCorrCW->Points->AddXY(fmin * 1e-3, firstValue.SaCorr);
				s_beamAlongCW->Points->AddXY(fmin * 1e-3, firstValue.BeamAlong);
				s_beamAthwartCW->Points->AddXY(fmin * 1e-3, firstValue.BeamAthwart);
				s_angleAlongCW->Points->AddXY(fmin * 1e-3, firstValue.OffsetAlong);
				s_angleAthwartCW->Points->AddXY(fmin * 1e-3, firstValue.OffsetAthwart);

				for (auto value : fvaluesCW)
				{
					s_gainCW->Points->AddXY(value.Freq * 1e-3, value.Gain);
					s_saCorrCW->Points->AddXY(value.Freq * 1e-3, value.SaCorr);
					s_beamAlongCW->Points->AddXY(value.Freq * 1e-3, value.BeamAlong);
					s_beamAthwartCW->Points->AddXY(value.Freq * 1e-3, value.BeamAthwart);
					s_angleAlongCW->Points->AddXY(value.Freq * 1e-3, value.OffsetAlong);
					s_angleAthwartCW->Points->AddXY(value.Freq * 1e-3, value.OffsetAthwart);
				}

				auto lastValue = fvaluesCW.back();
				s_gainCW->Points->AddXY(fmax * 1e-3, lastValue.Gain);
				s_saCorrCW->Points->AddXY(fmax * 1e-3, lastValue.SaCorr);
				s_beamAlongCW->Points->AddXY(fmax * 1e-3, lastValue.BeamAlong);
				s_beamAthwartCW->Points->AddXY(fmax * 1e-3, lastValue.BeamAthwart);
				s_angleAlongCW->Points->AddXY(fmax * 1e-3, lastValue.OffsetAlong);
				s_angleAthwartCW->Points->AddXY(fmax * 1e-3, lastValue.OffsetAthwart);
			}

			m_chartSeriesMapCW[chartGain][strTranducer]  = s_gainCW;
			m_chartSeriesMapCW[chartSaCorr][strTranducer] = s_saCorrCW;
			m_chartSeriesMapCW[chartBeamAlong][strTranducer] = s_beamAlongCW;
			m_chartSeriesMapCW[chartBeamAthwart][strTranducer] = s_beamAthwartCW;
			m_chartSeriesMapCW[chartAngleAlong][strTranducer] = s_angleAlongCW;
			m_chartSeriesMapCW[chartAngleAthwart][strTranducer] = s_angleAthwartCW;
		}
	}

	if (cbTransducers->Items->Count > 0) {
		cbTransducers->SelectedIndex = 0;
	}
}

System::Void CalibrationControl::cbTransducers_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	for each (auto pair in m_chartSeriesMapFM)
	{
		auto chart_i = pair.Key;
		auto seriesFM_i = pair.Value;
		auto seriesCW_i = m_chartSeriesMapCW[chart_i];

		chart_i->Series->Clear();

		double minYAxisValue = std::numeric_limits<double>::max();
		double maxYAxisValue = -std::numeric_limits<double>::max();

		double minAxisValue = std::numeric_limits<double>::max();
		double maxAxisValue = -std::numeric_limits<double>::max();

		Series^serieCW = nullptr;
		seriesCW_i->TryGetValue(cbTransducers->Text, serieCW);
		if (serieCW != nullptr)
		{
			chart_i->Series->Add(serieCW);
			auto points = serieCW->Points;
			if (points->Count > 0)
			{
				minAxisValue = serieCW->Points[0]->XValue;
				maxAxisValue = serieCW->Points[serieCW->Points->Count - 1]->XValue;

				minYAxisValue = points->FindMinByValue()->YValues[0];
				maxYAxisValue = points->FindMaxByValue()->YValues[0];
			}
		}
		
		Series^serieFM = nullptr;
		seriesFM_i->TryGetValue(cbTransducers->Text, serieFM);
		if (serieFM != nullptr)
		{
			chart_i->Series->Add(serieFM);

			// Calcul d'un interval offset coh�rent pour avoir un axes de fr�quence avec des valeurs "rondes"
			if (serieFM->Points->Count > 0)
			{
				minAxisValue = std::min(minAxisValue, serieFM->Points[0]->XValue);
				maxAxisValue = std::max(maxAxisValue, serieFM->Points[serieFM->Points->Count - 1]->XValue);

				auto points = serieFM->Points;
				const double minVal = points->FindMinByValue()->YValues[0];
				const double maxVal = points->FindMaxByValue()->YValues[0];
				minYAxisValue = std::min<double>(minYAxisValue, minVal);
				maxYAxisValue = std::max<double>(maxYAxisValue, maxVal);
			}
		}

		
		roundAxis(chart_i->ChartAreas[0]->AxisX, minAxisValue, maxAxisValue, 8);
		if (minYAxisValue == maxYAxisValue)
		{
			minYAxisValue = std::min(0., minYAxisValue - 0.5);
			maxYAxisValue = maxYAxisValue + 0.5;
		}
		roundAxis(chart_i->ChartAreas[0]->AxisY, minYAxisValue, maxYAxisValue, 8);
	}
}
