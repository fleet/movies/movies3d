#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"
#include "Calibration/CalibrationData.h"
#include "Chart/ChartInteractor.h"

#include <map>
#include <vector>

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Summary for CalibrationControl
	/// </summary>
	public ref class CalibrationControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		CalibrationControl();

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~CalibrationControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  btnBrowse;
	private: System::Windows::Forms::TextBox^  txtCalibrationFile;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog;
	private: System::Windows::Forms::ComboBox^  cbTransducers;

	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chartGain;
	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chartSaCorr;
	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chartBeamAlong;
	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chartBeamAthwart;
	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chartAngleAlong;





	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chartAngleAthwart;

	private: System::Windows::Forms::CheckBox^  enabledCheckbox;
	private: System::Windows::Forms::DataGridView^  channelInfoGridView;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea2 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea3 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea4 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea5 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea6 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			this->btnBrowse = (gcnew System::Windows::Forms::Button());
			this->txtCalibrationFile = (gcnew System::Windows::Forms::TextBox());
			this->openFileDialog = (gcnew System::Windows::Forms::OpenFileDialog());
			this->cbTransducers = (gcnew System::Windows::Forms::ComboBox());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->chartGain = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->chartSaCorr = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->chartBeamAlong = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->chartBeamAthwart = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->chartAngleAlong = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->chartAngleAthwart = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->channelInfoGridView = (gcnew System::Windows::Forms::DataGridView());
			this->enabledCheckbox = (gcnew System::Windows::Forms::CheckBox());
			this->tableLayoutPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartGain))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartSaCorr))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartBeamAlong))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartBeamAthwart))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartAngleAlong))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartAngleAthwart))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->channelInfoGridView))->BeginInit();
			this->SuspendLayout();
			// 
			// btnBrowse
			// 
			this->btnBrowse->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->btnBrowse->Location = System::Drawing::Point(661, 39);
			this->btnBrowse->Name = L"btnBrowse";
			this->btnBrowse->Size = System::Drawing::Size(75, 23);
			this->btnBrowse->TabIndex = 0;
			this->btnBrowse->Text = L"Browse";
			this->btnBrowse->UseVisualStyleBackColor = true;
			this->btnBrowse->Click += gcnew System::EventHandler(this, &CalibrationControl::btnBrowse_Click);
			// 
			// txtCalibrationFile
			// 
			this->txtCalibrationFile->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->txtCalibrationFile->Location = System::Drawing::Point(8, 41);
			this->txtCalibrationFile->Name = L"txtCalibrationFile";
			this->txtCalibrationFile->ReadOnly = true;
			this->txtCalibrationFile->Size = System::Drawing::Size(645, 20);
			this->txtCalibrationFile->TabIndex = 1;
			// 
			// openFileDialog
			// 
			this->openFileDialog->FileName = L"openFileDialog";
			this->openFileDialog->Filter = L"Xml Files|*.xml";
			this->openFileDialog->Title = L"Select a calibration file";
			// 
			// cbTransducers
			// 
			this->cbTransducers->Dock = System::Windows::Forms::DockStyle::Fill;
			this->cbTransducers->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->cbTransducers->FormattingEnabled = true;
			this->cbTransducers->Location = System::Drawing::Point(3, 102);
			this->cbTransducers->Name = L"cbTransducers";
			this->cbTransducers->Size = System::Drawing::Size(241, 21);
			this->cbTransducers->TabIndex = 3;
			this->cbTransducers->SelectedIndexChanged += gcnew System::EventHandler(this, &CalibrationControl::cbTransducers_SelectedIndexChanged);
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->tableLayoutPanel1->ColumnCount = 3;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				33.33333F)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				33.33333F)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				33.33333F)));
			this->tableLayoutPanel1->Controls->Add(this->chartGain, 0, 2);
			this->tableLayoutPanel1->Controls->Add(this->chartSaCorr, 1, 2);
			this->tableLayoutPanel1->Controls->Add(this->chartBeamAlong, 0, 3);
			this->tableLayoutPanel1->Controls->Add(this->chartBeamAthwart, 1, 3);
			this->tableLayoutPanel1->Controls->Add(this->chartAngleAlong, 2, 2);
			this->tableLayoutPanel1->Controls->Add(this->chartAngleAthwart, 2, 3);
			this->tableLayoutPanel1->Controls->Add(this->cbTransducers, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->channelInfoGridView, 0, 0);
			this->tableLayoutPanel1->Location = System::Drawing::Point(3, 67);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 4;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 30)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 30)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 35)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 35)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(743, 361);
			this->tableLayoutPanel1->TabIndex = 4;
			// 
			// chartGain
			// 
			chartArea1->AxisX->ArrowStyle = System::Windows::Forms::DataVisualization::Charting::AxisArrowStyle::Triangle;
			chartArea1->AxisX->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea1->AxisX->Title = L"Frequency (kHz)";
			chartArea1->AxisY->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea1->AxisY->Title = L"Gain";
			chartArea1->Name = L"ChartArea1";
			this->chartGain->ChartAreas->Add(chartArea1);
			this->chartGain->Dock = System::Windows::Forms::DockStyle::Fill;
			this->chartGain->Location = System::Drawing::Point(3, 132);
			this->chartGain->Name = L"chartGain";
			this->chartGain->Size = System::Drawing::Size(241, 109);
			this->chartGain->TabIndex = 1;
			this->chartGain->Text = L"chartGain";
			// 
			// chartSaCorr
			// 
			chartArea2->AxisX->ArrowStyle = System::Windows::Forms::DataVisualization::Charting::AxisArrowStyle::Triangle;
			chartArea2->AxisX->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea2->AxisX->Title = L"Frequency (kHz)";
			chartArea2->AxisY->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea2->AxisY->Title = L"SaCorrection";
			chartArea2->Name = L"JesuisPourtantBienLa";
			this->chartSaCorr->ChartAreas->Add(chartArea2);
			this->chartSaCorr->Dock = System::Windows::Forms::DockStyle::Fill;
			this->chartSaCorr->Location = System::Drawing::Point(250, 132);
			this->chartSaCorr->Name = L"chartSaCorr";
			this->chartSaCorr->Size = System::Drawing::Size(241, 109);
			this->chartSaCorr->TabIndex = 3;
			this->chartSaCorr->Text = L"chartSaCorr";
			// 
			// chartBeamAlong
			// 
			chartArea3->AxisX->ArrowStyle = System::Windows::Forms::DataVisualization::Charting::AxisArrowStyle::Triangle;
			chartArea3->AxisX->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea3->AxisX->Title = L"Frequency (kHz)";
			chartArea3->AxisY->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea3->AxisY->Title = L"BeamWidthAlongShip";
			chartArea3->Name = L"ChartArea3";
			this->chartBeamAlong->ChartAreas->Add(chartArea3);
			this->chartBeamAlong->Dock = System::Windows::Forms::DockStyle::Fill;
			this->chartBeamAlong->Location = System::Drawing::Point(3, 247);
			this->chartBeamAlong->Name = L"chartBeamAlong";
			this->chartBeamAlong->Size = System::Drawing::Size(241, 111);
			this->chartBeamAlong->TabIndex = 4;
			this->chartBeamAlong->Text = L"chartBeamAlong";
			// 
			// chartBeamAthwart
			// 
			chartArea4->AxisX->ArrowStyle = System::Windows::Forms::DataVisualization::Charting::AxisArrowStyle::Triangle;
			chartArea4->AxisX->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea4->AxisX->Title = L"Frequency (kHz)";
			chartArea4->AxisY->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea4->AxisY->Title = L"BeamWidthAthwartship";
			chartArea4->Name = L"ChartArea4";
			this->chartBeamAthwart->ChartAreas->Add(chartArea4);
			this->chartBeamAthwart->Dock = System::Windows::Forms::DockStyle::Fill;
			this->chartBeamAthwart->Location = System::Drawing::Point(250, 247);
			this->chartBeamAthwart->Name = L"chartBeamAthwart";
			this->chartBeamAthwart->Size = System::Drawing::Size(241, 111);
			this->chartBeamAthwart->TabIndex = 5;
			this->chartBeamAthwart->Text = L"chartBeamAthwart";
			// 
			// chartAngleAlong
			// 
			chartArea5->AxisX->ArrowStyle = System::Windows::Forms::DataVisualization::Charting::AxisArrowStyle::Triangle;
			chartArea5->AxisX->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea5->AxisX->Title = L"Frequency (kHz)";
			chartArea5->AxisY->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea5->AxisY->Title = L"AngleOffsetAlongship";
			chartArea5->Name = L"ChartArea5";
			this->chartAngleAlong->ChartAreas->Add(chartArea5);
			this->chartAngleAlong->Dock = System::Windows::Forms::DockStyle::Fill;
			this->chartAngleAlong->Location = System::Drawing::Point(497, 132);
			this->chartAngleAlong->Name = L"chartAngleAlong";
			this->chartAngleAlong->Size = System::Drawing::Size(243, 109);
			this->chartAngleAlong->TabIndex = 6;
			this->chartAngleAlong->Text = L"chartAngleAlong";
			// 
			// chartAngleAthwart
			// 
			chartArea6->AxisX->ArrowStyle = System::Windows::Forms::DataVisualization::Charting::AxisArrowStyle::Triangle;
			chartArea6->AxisX->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea6->AxisX->Title = L"Frequency (kHz)";
			chartArea6->AxisY->MajorGrid->LineColor = System::Drawing::Color::DarkGray;
			chartArea6->AxisY->Title = L"AngleOffsetAthwartship";
			chartArea6->Name = L"ChartArea6";
			this->chartAngleAthwart->ChartAreas->Add(chartArea6);
			this->chartAngleAthwart->Dock = System::Windows::Forms::DockStyle::Fill;
			this->chartAngleAthwart->Location = System::Drawing::Point(497, 247);
			this->chartAngleAthwart->Name = L"chartAngleAthwart";
			this->chartAngleAthwart->Size = System::Drawing::Size(243, 111);
			this->chartAngleAthwart->TabIndex = 7;
			this->chartAngleAthwart->Text = L"chartAngleAthwart";
			// 
			// channelInfoGridView
			// 
			this->channelInfoGridView->AllowUserToAddRows = false;
			this->channelInfoGridView->AllowUserToDeleteRows = false;
			this->channelInfoGridView->AllowUserToResizeColumns = false;
			this->channelInfoGridView->AllowUserToResizeRows = false;
			this->channelInfoGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->tableLayoutPanel1->SetColumnSpan(this->channelInfoGridView, 3);
			this->channelInfoGridView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->channelInfoGridView->Location = System::Drawing::Point(3, 3);
			this->channelInfoGridView->Name = L"channelInfoGridView";
			this->channelInfoGridView->ReadOnly = true;
			this->channelInfoGridView->RowHeadersVisible = false;
			this->channelInfoGridView->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->channelInfoGridView->Size = System::Drawing::Size(737, 93);
			this->channelInfoGridView->TabIndex = 6;
			// 
			// enabledCheckbox
			// 
			this->enabledCheckbox->AutoSize = true;
			this->enabledCheckbox->Checked = true;
			this->enabledCheckbox->CheckState = System::Windows::Forms::CheckState::Checked;
			this->enabledCheckbox->Location = System::Drawing::Point(8, 13);
			this->enabledCheckbox->Name = L"enabledCheckbox";
			this->enabledCheckbox->Size = System::Drawing::Size(151, 17);
			this->enabledCheckbox->TabIndex = 5;
			this->enabledCheckbox->Text = L"Enabled Sv/TS Correction";
			this->enabledCheckbox->UseVisualStyleBackColor = true;
			this->enabledCheckbox->CheckedChanged += gcnew System::EventHandler(this, &CalibrationControl::enabledCheckbox_CheckedChanged);
			// 
			// CalibrationControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->enabledCheckbox);
			this->Controls->Add(this->tableLayoutPanel1);
			this->Controls->Add(this->txtCalibrationFile);
			this->Controls->Add(this->btnBrowse);
			this->Name = L"CalibrationControl";
			this->Size = System::Drawing::Size(749, 431);
			this->tableLayoutPanel1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartGain))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartSaCorr))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartBeamAlong))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartBeamAthwart))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartAngleAlong))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chartAngleAthwart))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->channelInfoGridView))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

#pragma region Implémentation de IParamControl
	public: virtual void UpdateConfiguration();
	public: virtual void UpdateGUI();
#pragma endregion

	private: System::Void btnBrowse_Click(System::Object^  sender, System::EventArgs^  e); 
	private: System::Void cbTransducers_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

	private: System::Collections::Generic::Dictionary<System::Windows::Forms::DataVisualization::Charting::Chart^, System::Collections::Generic::Dictionary<System::String^, System::Windows::Forms::DataVisualization::Charting::Series^>^> m_chartSeriesMapFM;
	private: System::Collections::Generic::Dictionary<System::Windows::Forms::DataVisualization::Charting::Chart^, System::Collections::Generic::Dictionary<System::String^, System::Windows::Forms::DataVisualization::Charting::Series^>^> m_chartSeriesMapCW;
	private: void updateCalibrationData(const std::map<std::string, std::vector<CalibrationData>>& valuesFM, const std::map<std::string, std::vector<CalibrationData>>& valuesCW);
	
	private: System::Void enabledCheckbox_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

	private: void UpdateChannelInfoGridTable(const std::map<std::string, std::vector<CalibrationData>>& values);

	private: ChartInteractor ^ m_chartInteractor;
};
}
