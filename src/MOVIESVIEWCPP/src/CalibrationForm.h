#pragma once

#include "ParameterForm.h"
#include "CalibrationControl.h"

namespace MOVIESVIEWCPP
{
	public ref class CalibrationForm : public ParameterForm
	{
	public:
		CalibrationForm() : ParameterForm()
		{
			SetParamControl(gcnew CalibrationControl(), L"Calibration");
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Sizable;

			this->Load += gcnew System::EventHandler(this, &MOVIESVIEWCPP::CalibrationForm::OnLoad);
		}

		void OnLoad(System::Object ^sender, System::EventArgs ^e);
	};
};


void MOVIESVIEWCPP::CalibrationForm::OnLoad(System::Object ^sender, System::EventArgs ^e)
{
	this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowOnly;
	this->AutoSize = false;

	this->Width = 950;
	this->Height = 700;
}
