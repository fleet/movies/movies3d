#include "CameraSettings.h"

CameraSettings::CameraSettings(void)
{
	m_azimuth = 0;
	m_elevation = 0;

	m_zoomFactor = 1.0;
	m_autoReset = true;

	m_enableClip = false;
	m_AskForReset = false;

	m_useAutoResetVtk = true;
}

CameraSettings::~CameraSettings(void)
{
}
