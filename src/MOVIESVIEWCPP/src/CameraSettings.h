#pragma once
#pragma managed

public ref class CameraVolumePoint
{
public:
	CameraVolumePoint(void)
	{
		x = y = z = 0;
	}
	CameraVolumePoint(CameraVolumePoint% ref)
	{
		x = ref.x;
		y = ref.y;
		z = ref.z;
	}
	void operator=(CameraVolumePoint % ref)
	{
		x = ref.x;
		y = ref.y;
		z = ref.z;
	}
	double x;
	double y;
	double z;
};

public ref class CameraSettings
{
public:
	CameraSettings(void);
	virtual ~CameraSettings(void);
public:
	// bounding volume extends
	CameraVolumePoint m_boundVolA;
	CameraVolumePoint m_boundVolB;

	CameraVolumePoint m_wholeBoundVolA;
	CameraVolumePoint m_wholeBoundVolB;
	
	double m_azimuth;
	double m_elevation;

	double m_zoomFactor;
	double m_defaultParallelScale;

	bool m_AskForReset;
	bool m_useAutoResetVtk;
	
	bool m_enableClip;
	bool m_autoReset;

	void operator=(CameraSettings % ref)
	{
		m_defaultParallelScale = ref.m_defaultParallelScale;
		m_zoomFactor = ref.m_zoomFactor;
		m_elevation = ref.m_elevation;
		m_azimuth = ref.m_azimuth;
		m_boundVolB = ref.m_boundVolB;
		m_boundVolA = ref.m_boundVolA;
		m_wholeBoundVolA = ref.m_wholeBoundVolA;
		m_wholeBoundVolB = ref.m_wholeBoundVolB;

		m_autoReset = ref.m_autoReset;
		m_AskForReset = ref.m_AskForReset;
		m_useAutoResetVtk = ref.m_useAutoResetVtk;
	}

	CameraSettings(CameraSettings% ref)
	{
		m_defaultParallelScale = ref.m_defaultParallelScale;
		m_zoomFactor = ref.m_zoomFactor;
		m_elevation = ref.m_elevation;
		m_azimuth = ref.m_azimuth;
		m_boundVolB = ref.m_boundVolB;
		m_boundVolA = ref.m_boundVolA;
		m_wholeBoundVolA = ref.m_wholeBoundVolA;
		m_wholeBoundVolB = ref.m_wholeBoundVolB;

		m_autoReset = ref.m_autoReset;
		m_AskForReset = ref.m_AskForReset;
		m_useAutoResetVtk = ref.m_useAutoResetVtk;
	}

};
