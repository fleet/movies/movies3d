
#include <memory.h>
#include "BaseMathLib/RotationMatrix.h"
#include "M3DKernel/DefConstants.h"
#include "CameraSettingsForm.h"

namespace MOVIESVIEWCPP {
	
	CameraSettingsForm::CameraSettingsForm(CameraSettings ^camSet)
		: m_CamSet(camSet)
	{
		InitializeComponent();

		CameraSettings a = *camSet;
		this->checkBoxResetWhileReading->Checked = a.m_autoReset;
		double t = a.m_zoomFactor * 100 / 5.0;
		this->trackBarZoom->Value = System::Int32(t);
		this->checkBoxResetWithVtk->Checked = camSet->m_useAutoResetVtk;
	}

	CameraSettingsForm::~CameraSettingsForm()
	{
		if (components)
		{
			delete components;
		}
	}

	System::Void CameraSettingsForm::UpdateDelegate()
	{
		if (this->m_updateDelegate)
			this->m_updateDelegate(this->m_CamSet);
	}

	System::Void CameraSettingsForm::checkBoxResetWhileReading_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
	{
		this->m_CamSet->m_autoReset = this->checkBoxResetWhileReading->Checked;
		UpdateDelegate();
	}

	System::Void CameraSettingsForm::trackBarZoom_Scroll(System::Object^  sender, System::EventArgs^  e)
	{
		this->m_CamSet->m_zoomFactor = System::Decimal::ToInt32(this->trackBarZoom->Value) * 5 / 100.0;
		UpdateDelegate();
	}

	System::Void CameraSettingsForm::buttonResetPosition_Click(System::Object^  sender, System::EventArgs^  e)
	{
		this->m_CamSet->m_AskForReset = true;
		UpdateDelegate();
		this->trackBarZoom->Value = 20.0;
		this->m_CamSet->m_AskForReset = false;
	}

	System::Void CameraSettingsForm::checkBoxResetWithVtk_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
	{
		m_CamSet->m_useAutoResetVtk = this->checkBoxResetWithVtk->Checked;
		UpdateDelegate();
	}

	System::Void CameraSettingsForm::trackBarElevation_ValueChanged(System::Object^  sender, System::EventArgs^  e)
	{
		if (trackBarElevation->Value != 0)
		{
			this->m_CamSet->m_elevation = 0.5*trackBarElevation->Value;
			UpdateDelegate();
			trackBarElevation->Value = 0;
			this->m_CamSet->m_elevation = 0;
		}
	}

	System::Void CameraSettingsForm::trackBarAzimuth_Scroll(System::Object^  sender, System::EventArgs^  e)
	{
		if (trackBarAzimuth->Value != 0)
		{
			this->m_CamSet->m_azimuth += 0.5*trackBarAzimuth->Value / 360;
			UpdateDelegate();
			trackBarAzimuth->Value = 0;
		}
	}
}