#pragma once

#include "CameraSettings.h"
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace MOVIESVIEWCPP {

	public delegate System::Void UpdateCameraDelegate(CameraSettings ^);

	/// <summary>
	/// Description r�sum�e de CameraSettingsForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class CameraSettingsForm : public System::Windows::Forms::Form
	{
	public:
		CameraSettings ^m_CamSet;
	private: System::Windows::Forms::Label^  label2;

	private: System::Windows::Forms::CheckBox^  checkBoxResetWhileReading;
	private: System::Windows::Forms::TrackBar^  trackBarZoom;

	private: System::Windows::Forms::Label^  labelAz;
	private: System::Windows::Forms::Button^  buttonResetPosition;
	private: System::Windows::Forms::CheckBox^  checkBoxResetWithVtk;
	private: System::Windows::Forms::TrackBar^  trackBarElevation;
	private: System::Windows::Forms::TrackBar^  trackBarAzimuth;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Panel^  panel1;

	public:
		UpdateCameraDelegate ^m_updateDelegate;

		CameraSettingsForm(CameraSettings ^camSet);

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~CameraSettingsForm();

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->labelAz = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->trackBarAzimuth = (gcnew System::Windows::Forms::TrackBar());
			this->trackBarElevation = (gcnew System::Windows::Forms::TrackBar());
			this->checkBoxResetWithVtk = (gcnew System::Windows::Forms::CheckBox());
			this->buttonResetPosition = (gcnew System::Windows::Forms::Button());
			this->trackBarZoom = (gcnew System::Windows::Forms::TrackBar());
			this->checkBoxResetWhileReading = (gcnew System::Windows::Forms::CheckBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->panel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarAzimuth))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarElevation))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarZoom))->BeginInit();
			this->SuspendLayout();
			// 
			// labelAz
			// 
			this->labelAz->AutoSize = true;
			this->labelAz->Location = System::Drawing::Point(12, 9);
			this->labelAz->Name = L"labelAz";
			this->labelAz->Size = System::Drawing::Size(43, 13);
			this->labelAz->TabIndex = 3;
			this->labelAz->Text = L"azimuth";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 49);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(51, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"Elevation";
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->trackBarAzimuth);
			this->panel1->Controls->Add(this->trackBarElevation);
			this->panel1->Controls->Add(this->checkBoxResetWithVtk);
			this->panel1->Controls->Add(this->buttonResetPosition);
			this->panel1->Controls->Add(this->trackBarZoom);
			this->panel1->Controls->Add(this->checkBoxResetWhileReading);
			this->panel1->Controls->Add(this->label2);
			this->panel1->Controls->Add(this->label1);
			this->panel1->Controls->Add(this->labelAz);
			this->panel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->panel1->Location = System::Drawing::Point(0, 0);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(293, 302);
			this->panel1->TabIndex = 6;
			// 
			// trackBarAzimuth
			// 
			this->trackBarAzimuth->LargeChange = 10;
			this->trackBarAzimuth->Location = System::Drawing::Point(61, 3);
			this->trackBarAzimuth->Maximum = 5;
			this->trackBarAzimuth->Minimum = -5;
			this->trackBarAzimuth->Name = L"trackBarAzimuth";
			this->trackBarAzimuth->Size = System::Drawing::Size(212, 45);
			this->trackBarAzimuth->TabIndex = 10;
			this->trackBarAzimuth->TickStyle = System::Windows::Forms::TickStyle::None;
			this->trackBarAzimuth->Scroll += gcnew System::EventHandler(this, &CameraSettingsForm::trackBarAzimuth_Scroll);
			// 
			// trackBarElevation
			// 
			this->trackBarElevation->Location = System::Drawing::Point(61, 49);
			this->trackBarElevation->Margin = System::Windows::Forms::Padding(0);
			this->trackBarElevation->Maximum = 5;
			this->trackBarElevation->Minimum = -5;
			this->trackBarElevation->Name = L"trackBarElevation";
			this->trackBarElevation->Size = System::Drawing::Size(212, 45);
			this->trackBarElevation->TabIndex = 10;
			this->trackBarElevation->TickStyle = System::Windows::Forms::TickStyle::None;
			this->trackBarElevation->ValueChanged += gcnew System::EventHandler(this, &CameraSettingsForm::trackBarElevation_ValueChanged);
			// 
			// checkBoxResetWithVtk
			// 
			this->checkBoxResetWithVtk->AutoSize = true;
			this->checkBoxResetWithVtk->Location = System::Drawing::Point(25, 218);
			this->checkBoxResetWithVtk->Name = L"checkBoxResetWithVtk";
			this->checkBoxResetWithVtk->Size = System::Drawing::Size(101, 17);
			this->checkBoxResetWithVtk->TabIndex = 17;
			this->checkBoxResetWithVtk->Text = L"Use Auto Reset";
			this->checkBoxResetWithVtk->UseVisualStyleBackColor = true;
			this->checkBoxResetWithVtk->CheckedChanged += gcnew System::EventHandler(this, &CameraSettingsForm::checkBoxResetWithVtk_CheckedChanged);
			// 
			// buttonResetPosition
			// 
			this->buttonResetPosition->Location = System::Drawing::Point(25, 150);
			this->buttonResetPosition->Name = L"buttonResetPosition";
			this->buttonResetPosition->Size = System::Drawing::Size(199, 23);
			this->buttonResetPosition->TabIndex = 16;
			this->buttonResetPosition->Text = L"Reset Camera";
			this->buttonResetPosition->UseVisualStyleBackColor = true;
			this->buttonResetPosition->Click += gcnew System::EventHandler(this, &CameraSettingsForm::buttonResetPosition_Click);
			// 
			// trackBarZoom
			// 
			this->trackBarZoom->Location = System::Drawing::Point(85, 97);
			this->trackBarZoom->Maximum = 100;
			this->trackBarZoom->Minimum = 1;
			this->trackBarZoom->Name = L"trackBarZoom";
			this->trackBarZoom->Size = System::Drawing::Size(166, 45);
			this->trackBarZoom->TabIndex = 9;
			this->trackBarZoom->TickFrequency = 5;
			this->trackBarZoom->Value = 1;
			this->trackBarZoom->Scroll += gcnew System::EventHandler(this, &CameraSettingsForm::trackBarZoom_Scroll);
			// 
			// checkBox1
			// 
			this->checkBoxResetWhileReading->AutoSize = true;
			this->checkBoxResetWhileReading->Location = System::Drawing::Point(25, 188);
			this->checkBoxResetWhileReading->Name = L"checkBox1";
			this->checkBoxResetWhileReading->Size = System::Drawing::Size(199, 17);
			this->checkBoxResetWhileReading->TabIndex = 8;
			this->checkBoxResetWhileReading->Text = L"Reset camera position while reading";
			this->checkBoxResetWhileReading->UseVisualStyleBackColor = true;
			this->checkBoxResetWhileReading->CheckedChanged += gcnew System::EventHandler(this, &CameraSettingsForm::checkBoxResetWhileReading_CheckedChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 102);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(67, 13);
			this->label2->TabIndex = 6;
			this->label2->Text = L"Zoom Factor";
			// 
			// CameraSettingsForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(293, 302);
			this->Controls->Add(this->panel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->Name = L"CameraSettingsForm";
			this->Text = L"CameraSettingsForm";
			this->TopMost = true;
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarAzimuth))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarElevation))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarZoom))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion


	private: System::Void UpdateDelegate();
			 
	private: System::Void checkBoxResetWhileReading_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

	private: System::Void trackBarZoom_Scroll(System::Object^  sender, System::EventArgs^  e);

	private: System::Void buttonResetPosition_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void checkBoxResetWithVtk_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

	private: System::Void trackBarElevation_ValueChanged(System::Object^  sender, System::EventArgs^  e);

	private: System::Void trackBarAzimuth_Scroll(System::Object^  sender, System::EventArgs^  e);

	};
}
