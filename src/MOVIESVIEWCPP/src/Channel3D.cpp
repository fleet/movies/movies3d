#include "Channel3D.h"
#include "Transducer3D.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Transducer.h"

Channel3D::Channel3D(Transducer3D *parent, unsigned int polarI)
	: DataObject3D()
{
	m_parent = parent;
	m_polarId = polarI;
	m_bIsVisible = true;
}

Channel3D::~Channel3D(void)
{
}

SoftChannel *Channel3D::getChannel()
{
	SoftChannel *pChan = NULL;
	Transducer *ptrans = m_parent->GetTransducer();
	if (ptrans)
		pChan = ptrans->getSoftChannelPolarX(m_polarId);

	return pChan;


}