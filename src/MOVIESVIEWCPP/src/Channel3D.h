#pragma once
#include "dataobject3d.h"


class Transducer3D;
class SoftChannel;
class Channel3D :
	public DataObject3D
{
public:
	Channel3D(Transducer3D *parent, unsigned int polarId);
	virtual ~Channel3D(void);
	SoftChannel *getChannel();
private:
	unsigned int m_polarId;
	Transducer3D *m_parent;
};
