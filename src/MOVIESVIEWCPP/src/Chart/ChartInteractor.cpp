#include "ChartInteractor.h"

#include <limits>
#include <cmath>
#include <algorithm>

namespace {
	
	bool isMouseOverChart(System::Windows::Forms::DataVisualization::Charting::Chart ^ chart, System::Drawing::Point location)
	{
		float wRatio = chart->Width * 0.01;
		float hRatio = chart->Height * 0.01;

		int xmin = chart->ChartAreas[0]->InnerPlotPosition->X  * wRatio;
		int xmax = (chart->ChartAreas[0]->InnerPlotPosition->X + chart->ChartAreas[0]->InnerPlotPosition->Width) * wRatio;
		int ymin = chart->ChartAreas[0]->InnerPlotPosition->Y * hRatio;
		int ymax = (chart->ChartAreas[0]->InnerPlotPosition->Y + chart->ChartAreas[0]->InnerPlotPosition->Height) * hRatio;

		return xmin <= location.X && location.X <= xmax
			&& ymin <= location.Y && location.Y <= ymax;
	}

	int pan(System::Windows::Forms::DataVisualization::Charting::Axis ^axis, int location, int previousLocation)
	{
		if (abs(location - previousLocation) > 1)
		{
			auto scroll = axis->ScaleView->Position
				- axis->PixelPositionToValue(location) 
				+ axis->PixelPositionToValue(previousLocation);
			axis->ScaleView->Scroll(scroll);
			previousLocation = location;
		}
		return previousLocation;
	}

	void zoom(System::Windows::Forms::DataVisualization::Charting::Axis ^axis, double zoomFactor, double location)
	{
		auto center = axis->PixelPositionToValue(location);

		auto a = center - axis->ScaleView->ViewMinimum;
		auto b = axis->ScaleView->ViewMaximum - center;
		auto zoomValue = zoomFactor * (a + b);

		auto zoomMin = center - a * zoomFactor;
		auto zoomMax = center + b * zoomFactor;
		
		zoomMin = std::max(zoomMin, axis->Minimum);
		zoomMax = std::min(zoomMax, axis->Maximum);

		axis->ScaleView->Zoom(zoomMin, zoomMax);
	}

	void zoom(System::Windows::Forms::DataVisualization::Charting::Chart ^ chart, double xFactor, double yFactor, System::Drawing::Point location)
	{
		zoom(chart->ChartAreas[0]->AxisX, xFactor, location.X);
		zoom(chart->ChartAreas[0]->AxisY, yFactor, location.X);
	}

	void zoom(System::Windows::Forms::DataVisualization::Charting::Chart ^ chart, double zoomFactor, System::Drawing::Point location)
	{
		if (isMouseOverChart(chart, location))
		{
			zoom(chart, zoomFactor, zoomFactor, location);
		}
	}
}

void ChartInteractor::Install(System::Windows::Forms::DataVisualization::Charting::Chart ^ chart)
{
	auto cursorX = chart->ChartAreas[0]->CursorX;
	cursorX->IsUserSelectionEnabled = true;
	cursorX->Interval = 0;

	auto cursorY = chart->ChartAreas[0]->CursorY;
	cursorY->IsUserSelectionEnabled = true;
	cursorY->Interval = 0;

	auto axisX = chart->ChartAreas[0]->AxisX;
	axisX->Interval = 0;
	axisX->IsMarginVisible = false;
	axisX->ScaleView->Zoomable = true;
	axisX->ScrollBar->Enabled = false;

	auto axisY = chart->ChartAreas[0]->AxisY;
	axisX->Interval = 0;
	axisX->IsMarginVisible = false;
	axisY->ScaleView->Zoomable = true;
	axisY->ScrollBar->Enabled = false;

	chart->GetToolTipText += gcnew System::EventHandler<System::Windows::Forms::DataVisualization::Charting::ToolTipEventArgs ^>(this, &ChartInteractor::OnGetToolTipText);
	
	chart->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &ChartInteractor::OnMouseMove);
	chart->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &ChartInteractor::OnMouseDown);
	chart->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &ChartInteractor::OnMouseUp);
	chart->MouseWheel += gcnew System::Windows::Forms::MouseEventHandler(this, &ChartInteractor::OnMouseWheel);
}

void ChartInteractor::OnGetToolTipText(System::Object ^sender, System::Windows::Forms::DataVisualization::Charting::ToolTipEventArgs ^ e)
{
	if (e->HitTestResult->ChartElementType == System::Windows::Forms::DataVisualization::Charting::ChartElementType::DataPoint)
	{
		int i = e->HitTestResult->PointIndex;
		auto p = e->HitTestResult->Series->Points[i];
		e->Text = System::String::Format("{0:F}\r\n{1:F}", p->XValue, p->YValues[0]);
	}
}

void ChartInteractor::OnMouseMove(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^ e)
{
	if (e->Button != System::Windows::Forms::MouseButtons::Right)
		return;

	auto chart = (System::Windows::Forms::DataVisualization::Charting::Chart ^) sender;
	try
	{
		auto chartArea = chart->ChartAreas[0];
		prevMouseLocation.X = pan(chartArea->AxisX, e->Location.X, prevMouseLocation.X);
		prevMouseLocation.Y = pan(chartArea->AxisY, e->Location.Y, prevMouseLocation.Y);
	}
	catch (...)
	{
	}
}

void ChartInteractor::OnMouseDown(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^ e)
{
	if (e->Button != System::Windows::Forms::MouseButtons::Right)
		return;

	prevMouseLocation = e->Location;
}

void ChartInteractor::OnMouseUp(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^ e)
{
	if (e->Button == System::Windows::Forms::MouseButtons::Middle)
	{
		auto chart = (System::Windows::Forms::DataVisualization::Charting::Chart ^) sender;
		chart->ChartAreas[0]->AxisX->ScaleView->ZoomReset(0);
		chart->ChartAreas[0]->AxisY->ScaleView->ZoomReset(0);
	}
}

void ChartInteractor::OnMouseWheel(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^ e)
{
	try
	{
		auto chart = (System::Windows::Forms::DataVisualization::Charting::Chart ^) sender;
		if (e->Delta < 0) // Scrolled down.
		{
			zoom(chart, 1.25, e->Location);
		}
		else if (e->Delta > 0) // Scrolled up.
		{
			zoom(chart, 0.8, e->Location);
		}
	}
	catch (...)
	{
	}
}