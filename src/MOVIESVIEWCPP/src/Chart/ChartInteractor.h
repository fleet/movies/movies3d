#pragma once

/**
* Composant pour la gestion des zooms sur les graphiques : 
* s'abonne aux évènmenets souris du chart pour effectuer les zooms / dezooms
*/
ref class ChartInteractor : public System::Object
{
	System::Drawing::Point prevMouseLocation;

public:

	/// Installation du composant sur un
	void Install(System::Windows::Forms::DataVisualization::Charting::Chart ^ chart);

protected:

	/// Affiche un tooltip avec les valeurs du point sous la souris
	virtual void OnGetToolTipText(System::Object ^sender, System::Windows::Forms::DataVisualization::Charting::ToolTipEventArgs ^ e);

	/// Gestion des évènements souris
	virtual void OnMouseMove(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^ e);
	virtual void OnMouseDown(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^ e);
	virtual void OnMouseUp(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^ e);
	virtual void OnMouseWheel(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^ e);
};
