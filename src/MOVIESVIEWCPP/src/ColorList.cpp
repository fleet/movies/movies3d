// -*- C++ -*-
// ****************************************************************************
// Class: ColorList
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Juillet 2009
// Soci�t� : IPSIS
// ****************************************************************************
#include "colorlist.h"
#include <stdlib.h>

List<System::Drawing::Color>^ ColorList::GetColorList()
{
	return m_colorList;
}

System::Drawing::Color ColorList::GetColor(std::int64_t colorIdx)
{
	System::Drawing::Color result;

	if (GetColorList()->Count > 0)
	{
		colorIdx = __max(0, colorIdx);
		result = GetColorList()[colorIdx % GetColorList()->Count];
	}
	return result;
}

void ColorList::InitColorList()
{
	m_colorList = gcnew List<System::Drawing::Color>();

	// Get all the values from the KnownColor enumeration.
	System::Array^ colorsArray = System::Enum::GetValues(System::Drawing::KnownColor::typeid);
	for each(System::Drawing::KnownColor knownColor in colorsArray)
	{
		System::String^ colorName = knownColor.ToString();
		System::Drawing::Color color = System::Drawing::Color::FromName(colorName);

		//use only not system and dark enough colors
		if (!color.IsSystemColor && color.GetBrightness() < 0.60)
		{
			m_colorList->Add(color);
		}
	}
}