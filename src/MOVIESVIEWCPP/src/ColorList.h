// -*- C++ -*-
// ****************************************************************************
// Class: ColorList
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Juillet 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include <cstdint>

using namespace System::Collections::Generic;

ref class ColorList
{
public:

	static ColorList^ GetInstance() {

		GetLock()->Lock();
		if (m_pInstance == nullptr)
		{
			m_pInstance = gcnew ColorList();
			m_pInstance->InitColorList();
		}
		GetLock()->Unlock();

		return m_pInstance;
	};

	List<System::Drawing::Color>^ GetColorList();
	System::Drawing::Color GetColor(std::int64_t colorIdx);

protected:

	static CRecursiveMutex* GetLock() { return m_pLock; };

	void InitColorList();
	List<System::Drawing::Color>^ m_colorList;

private:

	static ColorList^ m_pInstance = nullptr;
	static CRecursiveMutex* m_pLock = new CRecursiveMutex();
};
