// AvitisPalette.h: interface for the AvitisPalette class.
//
//////////////////////////////////////////////////////////////////////
#ifndef COLOR_PALETTE
#define COLOR_PALETTE

#include "M3DKernel/DefConstants.h"

class IColorPalette
{
public:
	virtual unsigned int GetColor(DataFmt value) const = 0;
	
	virtual void PreparePalette(DataFmt minThr, DataFmt maxThr) {};
};

/*
class ComputedColorPalette : public IColorPalette
{
	std::vector<unsigned int> m_cache;

public:
	ComputedColorPalette();

	void Compute(IColorPalette * palette, DataFmt minValue, DataFmt maxValue);
	
	inline unsigned int GetColor(DataFmt value) const
	{
		return m_cache[(int)value + 32768];
	}
};
**/


#endif //COLOR_PALETTE
