#include "ColorPaletteChannel.h"

static unsigned int myColorStack[6] = {
	0x66FF0000,
	0x66FFFF00,
	0x6600FF00,
	0x6600FFFF,
	0x660000FF,
	0x66FF00FF
};

ColorPaletteChannel::ColorPaletteChannel(void) 
{
	ReInit();
}

ColorPaletteChannel::~ColorPaletteChannel(void)
{
}

void ColorPaletteChannel::ReInit()
{
	m_ColorMap.clear();
}

bool ColorPaletteChannel::operator==(const ColorPaletteChannel & other) const
{
	if (m_ColorMap.size() != other.m_ColorMap.size())
		return false;
	
	auto it1 = m_ColorMap.cbegin();
	auto it2 = other.m_ColorMap.cbegin();

	while (it1 != m_ColorMap.cend())
	{
		if (it1->first != it2->first || it1->second != it2->second)
			return false;
		++it1;
		++it2;
	}

	return true;
}

bool ColorPaletteChannel::operator!=(const ColorPaletteChannel & other) const
{
	return !(*this == other);
}

void ColorPaletteChannel::AddColorPointValue(DataFmt channelId)
{
	int nbUsedColorNb = m_ColorMap.size() + 1;
	m_ColorMap[channelId] = myColorStack[(nbUsedColorNb - 1) % 6];
}

unsigned int ColorPaletteChannel::GetColor(DataFmt value) const
{
	const auto it = m_ColorMap.find(value);
	return (it != m_ColorMap.cend()) ? it->second : 0xFF000000;
}