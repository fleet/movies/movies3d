#pragma once

#include "ColorPalette.h"

#include <map>

typedef	std::map<DataFmt, unsigned int> MapChannelColor;

class ColorPaletteChannel : public IColorPalette
{
public:
	ColorPaletteChannel(void);
	virtual ~ColorPaletteChannel(void);

	bool operator==(const ColorPaletteChannel & other) const;
	bool operator!=(const ColorPaletteChannel & other) const;
	
	void AddColorPointValue(DataFmt value);
	unsigned int GetColor(DataFmt value) const;
	void ReInit();
	
	MapChannelColor m_ColorMap;
};
