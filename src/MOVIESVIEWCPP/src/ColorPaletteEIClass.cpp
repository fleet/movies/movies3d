#include "ColorPaletteEIClass.h"

ColorPaletteEIClass::ColorPaletteEIClass()
{
	initializeColors();
}

ColorPaletteEIClass::~ColorPaletteEIClass()
{
}

unsigned int ColorPaletteEIClass::getARGBColorClassification(const short & index) const
{
	if ((index < 0) || (index >= m_colorClassification.size()))
	{
		unsigned int color = 0;
		return color;
	}
	else
	{
		return m_colorClassification[index];
	}
}

void ColorPaletteEIClass::initializeColors()
{
	m_colorClassification.resize(14);
	m_colorClassification[0] = 0xFFFF7F00;
	m_colorClassification[1] = 0xFFFFFF00;
	m_colorClassification[2] = 0xFF00FFA9;
	m_colorClassification[3] = 0xFF00D4FF;
	m_colorClassification[4] = 0xFFB2EBFF;
	m_colorClassification[5] = 0xFF2A00FF;
	m_colorClassification[6] = 0xFFAA00FF;
	m_colorClassification[7] = 0xFFFF00D4;
	m_colorClassification[8] = 0xFFFF007F;
	m_colorClassification[9] = 0xFFB8FF8C;

	m_colorClassification[10] = 0xFF339966;
	m_colorClassification[11] = 0xFFFF7C80;
	m_colorClassification[12] = 0xFFCC6600;
	m_colorClassification[13] = 0xFF33CCCC;
}