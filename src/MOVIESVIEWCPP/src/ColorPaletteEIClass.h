#pragma once

#include <vector>

class ColorPaletteEIClass
{
public:
	ColorPaletteEIClass();

	virtual ~ColorPaletteEIClass();

	unsigned int getARGBColorClassification(const short & index) const;

protected:

	void initializeColors();

	std::vector<unsigned int> m_colorClassification;
};
