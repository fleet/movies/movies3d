#pragma once

#include "colorpalette.h"
#include "M3DKernel/parameter/ParameterObject.h"

#include <map>
#include <vector>

class ManualColorPalette : public IColorPalette, public BaseKernel::ParameterObject
{
public:
	ManualColorPalette();

	bool operator==(const ManualColorPalette  &other) const;
	bool operator!=(const ManualColorPalette  &other) const;

	virtual void Reset();

	unsigned int GetColor(DataFmt value) const;
	int GetColorPointCount() const;

	struct ColorPoint
	{
		DataFmt threshold;
		unsigned color;
		inline bool operator==(const ColorPoint & other) const { return threshold == other.threshold && color == other.color; }
		inline bool operator!=(const ColorPoint & other) const { return threshold != other.threshold || color != other.color; }
	};

	void SetColorPoint(int idx, const ColorPoint & pt);
	const ColorPoint & GetColorPoint(int idx) const;

	virtual void PreparePalette(DataFmt minThr, DataFmt maxThr);

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);


protected:
	std::vector<ColorPoint> m_points;
	DataFmt m_minValue;
	DataFmt m_maxValue;
};

class TSColorPalette : public ManualColorPalette
{
public:
	void Reset();
};

class ParulaColorPalette : public IColorPalette
{
	DataFmt m_minValue;
	DataFmt m_maxValue;

public:
	unsigned int GetColor(DataFmt value) const;
	virtual void PreparePalette(DataFmt minThr, DataFmt maxThr);
};

class GradientColorPalette : public IColorPalette
{
protected:
	DataFmt m_minValue;
	DataFmt m_maxValue;
	std::map<float, unsigned int> m_colorMap;

public:
	unsigned int GetColor(DataFmt value) const;
	virtual void PreparePalette(DataFmt minThr, DataFmt maxThr);
};

class RedBlueColorPalette : public GradientColorPalette
{
public:
	RedBlueColorPalette();
};