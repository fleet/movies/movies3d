#include "ColorPaletteForm.h"
#include "ColorPalette.h"
#include "PalettePreview.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

using namespace MOVIESVIEWCPP;

ColorPaletteForm::ColorPaletteForm()
{
	InitializeComponent();

	this->DialogResult = System::Windows::Forms::DialogResult::Cancel;

	m_palette = new ManualColorPalette;
	m_defaultPalette = new ManualColorPalette;
}

ColorPaletteForm::~ColorPaletteForm()
{
	if (components)
	{
		delete components;
	}

	delete m_palette;
	delete m_defaultPalette;
}

ManualColorPalette ColorPaletteForm::GetPalette()
{
	return *m_palette;
}
	
void ColorPaletteForm::SetPalette(const ManualColorPalette palette)
{
	*m_palette = palette;
	*m_defaultPalette = palette;

	UpdateColorsTable();
}

void ColorPaletteForm::SetMinThreshold(DataFmt minThreshold)
{
	this->minSv = minThreshold;
	UpdateColorsTable();
}

void ColorPaletteForm::SetMaxThreshold(DataFmt maxThreshold)
{
	this->maxSv = maxThreshold;
	UpdateColorsTable();
}

System::Void ColorPaletteForm::UpdateColorsTable()
{
	listViewColor->BeginUpdate();
	listViewColor->Items->Clear();

	for (unsigned int i = 0; i < m_palette->GetColorPointCount(); ++i)
	{
		const auto pt = m_palette->GetColorPoint(i);

		auto color = System::Drawing::Color::FromArgb(pt.color);
		double alpha = color.A / 255.0;
		color = System::Drawing::Color::FromArgb(pt.color | 0xFF000000);

		auto listViewItem = gcnew System::Windows::Forms::ListViewItem();
		listViewItem->BackColor = color;
		auto refSubItem = gcnew System::Windows::Forms::ListViewItem::ListViewSubItem();
				
		int currentThreshold = pt.threshold / 100;
		refSubItem->Text = System::Decimal(currentThreshold).ToString();

		refSubItem->BackColor = System::Drawing::Color::White;
		refSubItem->ForeColor = System::Drawing::Color::White;
		listViewItem->SubItems->Add(refSubItem);
		listViewItem->SubItems->Add(System::Decimal(alpha).ToString("F2"));
		listViewItem->Tag = i;
		listViewColor->Items->Add(listViewItem);
	}

	listViewColor->EndUpdate();
}

System::Void ColorPaletteForm::buttonOK_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->DialogResult = System::Windows::Forms::DialogResult::OK;
	this->Close();
}

System::Void ColorPaletteForm::buttonCancel_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->Close();
}

System::Void ColorPaletteForm::listViewColor_DoubleClick(System::Object^  sender, System::EventArgs^  e) 
{
	if (listViewColor->SelectedItems->Count > 0)
	{
		auto item = listViewColor->SelectedItems[0];
		this->colorDialog1->Color = item->BackColor;
		if (this->colorDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			item->BackColor = colorDialog1->Color;
			auto pt = m_palette->GetColorPoint(item->Index);
			pt.color = colorDialog1->Color.ToArgb();
			m_palette->SetColorPoint(item->Index, pt);
		}
	}
}

System::Void ColorPaletteForm::buttonDefault_Click(System::Object^  sender, System::EventArgs^  e)
{
	*m_palette = *m_defaultPalette;
	UpdateColorsTable();
}