#pragma once

#include "ColorPaletteEcho.h"
#include "PalettePreview.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de ColorPaletteForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class ColorPaletteForm : public System::Windows::Forms::Form
	{
	public:
		ColorPaletteForm();

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~ColorPaletteForm();
	 
	private: System::Windows::Forms::ColorDialog^  colorDialog1;
	private: System::Windows::Forms::Button^  buttonOK;
	private: System::Windows::Forms::Button^  buttonCancel;
	private: System::Windows::Forms::ColumnHeader^  Color;
	private: System::Windows::Forms::ColumnHeader^  Value;
	private: System::Windows::Forms::ColumnHeader^  Opacity;
	private: System::Windows::Forms::ListView^  listViewColor;
	private: System::Windows::Forms::Button^  buttonDefault;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel3;
	private: System::Windows::Forms::TableLayoutPanel^  mainTableLayoutPanel;










	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->colorDialog1 = (gcnew System::Windows::Forms::ColorDialog());
			this->buttonOK = (gcnew System::Windows::Forms::Button());
			this->buttonCancel = (gcnew System::Windows::Forms::Button());
			this->Color = (gcnew System::Windows::Forms::ColumnHeader());
			this->Value = (gcnew System::Windows::Forms::ColumnHeader());
			this->Opacity = (gcnew System::Windows::Forms::ColumnHeader());
			this->listViewColor = (gcnew System::Windows::Forms::ListView());
			this->buttonDefault = (gcnew System::Windows::Forms::Button());
			this->tableLayoutPanel3 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->mainTableLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->tableLayoutPanel3->SuspendLayout();
			this->mainTableLayoutPanel->SuspendLayout();
			this->SuspendLayout();
			// 
			// buttonOK
			// 
			this->buttonOK->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->buttonOK->Location = System::Drawing::Point(26, 7);
			this->buttonOK->Name = L"buttonOK";
			this->buttonOK->Size = System::Drawing::Size(73, 26);
			this->buttonOK->TabIndex = 2;
			this->buttonOK->Text = L"OK";
			this->buttonOK->UseVisualStyleBackColor = true;
			this->buttonOK->Click += gcnew System::EventHandler(this, &ColorPaletteForm::buttonOK_Click);
			// 
			// buttonCancel
			// 
			this->buttonCancel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->buttonCancel->Location = System::Drawing::Point(105, 7);
			this->buttonCancel->Name = L"buttonCancel";
			this->buttonCancel->Size = System::Drawing::Size(73, 26);
			this->buttonCancel->TabIndex = 3;
			this->buttonCancel->Text = L"Cancel";
			this->buttonCancel->UseVisualStyleBackColor = true;
			this->buttonCancel->Click += gcnew System::EventHandler(this, &ColorPaletteForm::buttonCancel_Click);
			// 
			// Color
			// 
			this->Color->Text = L"Color";
			// 
			// Value
			// 
			this->Value->Text = L"Value";
			this->Value->Width = 110;
			// 
			// Opacity
			// 
			this->Opacity->Text = L"Opacity";
			// 
			// listViewColor
			// 
			this->listViewColor->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(3) {
				this->Color, this->Value,
					this->Opacity
			});
			this->listViewColor->Dock = System::Windows::Forms::DockStyle::Fill;
			this->listViewColor->GridLines = true;
			this->listViewColor->HeaderStyle = System::Windows::Forms::ColumnHeaderStyle::Nonclickable;
			this->listViewColor->Location = System::Drawing::Point(3, 3);
			this->listViewColor->MultiSelect = false;
			this->listViewColor->Name = L"listViewColor";
			this->listViewColor->ShowGroups = false;
			this->listViewColor->Size = System::Drawing::Size(260, 262);
			this->listViewColor->TabIndex = 9;
			this->listViewColor->UseCompatibleStateImageBehavior = false;
			this->listViewColor->View = System::Windows::Forms::View::Details;
			this->listViewColor->DoubleClick += gcnew System::EventHandler(this, &ColorPaletteForm::listViewColor_DoubleClick);
			// 
			// buttonDefault
			// 
			this->buttonDefault->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->buttonDefault->Location = System::Drawing::Point(184, 7);
			this->buttonDefault->Name = L"buttonDefault";
			this->buttonDefault->Size = System::Drawing::Size(73, 26);
			this->buttonDefault->TabIndex = 4;
			this->buttonDefault->Text = L"Default";
			this->buttonDefault->UseVisualStyleBackColor = true;
			this->buttonDefault->Click += gcnew System::EventHandler(this, &ColorPaletteForm::buttonDefault_Click);
			// 
			// tableLayoutPanel3
			// 
			this->tableLayoutPanel3->ColumnCount = 4;
			this->tableLayoutPanel3->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel3->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel3->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel3->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel3->Controls->Add(this->buttonDefault, 3, 0);
			this->tableLayoutPanel3->Controls->Add(this->buttonCancel, 2, 0);
			this->tableLayoutPanel3->Controls->Add(this->buttonOK, 1, 0);
			this->tableLayoutPanel3->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel3->Location = System::Drawing::Point(3, 271);
			this->tableLayoutPanel3->Name = L"tableLayoutPanel3";
			this->tableLayoutPanel3->RowCount = 1;
			this->tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel3->Size = System::Drawing::Size(260, 36);
			this->tableLayoutPanel3->TabIndex = 13;
			// 
			// mainTableLayoutPanel
			// 
			this->mainTableLayoutPanel->ColumnCount = 1;
			this->mainTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->mainTableLayoutPanel->Controls->Add(this->tableLayoutPanel3, 0, 1);
			this->mainTableLayoutPanel->Controls->Add(this->listViewColor, 0, 0);
			this->mainTableLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->mainTableLayoutPanel->Location = System::Drawing::Point(0, 0);
			this->mainTableLayoutPanel->Name = L"mainTableLayoutPanel";
			this->mainTableLayoutPanel->RowCount = 2;
			this->mainTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->mainTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->mainTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
				20)));
			this->mainTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
				20)));
			this->mainTableLayoutPanel->Size = System::Drawing::Size(266, 310);
			this->mainTableLayoutPanel->TabIndex = 11;
			// 
			// ColorPaletteForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(266, 310);
			this->Controls->Add(this->mainTableLayoutPanel);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->Name = L"ColorPaletteForm";
			this->Text = L"ColorPaletteForm";
			this->tableLayoutPanel3->ResumeLayout(false);
			this->mainTableLayoutPanel->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion

	public: ManualColorPalette GetPalette();
	public: void SetPalette(const ManualColorPalette palette);

	public: void SetMinThreshold(DataFmt minThreshold);
	public: void SetMaxThreshold(DataFmt maxThreshold);

	private: DataFmt minSv;
	private: DataFmt maxSv;

	private: ManualColorPalette * m_palette;
	private: ManualColorPalette * m_defaultPalette;

	private: System::Void UpdateColorsTable();
			 
	private: System::Void buttonOK_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonCancel_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void listViewColor_DoubleClick(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonDefault_Click(System::Object^  sender, System::EventArgs^  e);
};
}
