#include "ColorPaletteFrequency.h"

void ColorPaletteFrequency::ComputePalette(int size)
{
	colors.resize(size);

	const double thirdWidth = (double)size / 3;
	
	for (unsigned int i = 0; i < size; ++i)
	{
		unsigned int color = 0x00000000;

		ColorPoint pt;
		pt.a = 255;

		// Dans la premi�re partie du spectre, on va du bleu au vert
		if (i <= thirdWidth)
		{
			double factor = (double)i / thirdWidth;

			pt.r = 0;
			pt.g = factor * 255;
			pt.b = (1.0 - factor) * 255;
			
			//color.SetRGBA(0, (unsigned short)(factor * 255), (unsigned short)((1.0 - factor) * 255));
		}
		else if (i <= 2.0*thirdWidth)
		{
			// Dans la second partie du spectre, on va du vert au jaune
			double factor = ((double)i - thirdWidth) / thirdWidth;

			pt.r = factor * 255;
			pt.g = 255;
			pt.b = 0;
			
			//color.SetRGBA((unsigned short)(factor * 255), 255, 0);
		}
		else
		{
			// Dans la derni�re partie du spectre, on va du jaune au rouge
			double factor = ((double)i - 2.0*thirdWidth) / thirdWidth;

			pt.r = 255;
			pt.g = (1 - factor) * 255;
			pt.b = 0;

			//color.SetRGBA(255, (unsigned short)((1.0 - factor) * 255), 0);
		}

		colors[i] = pt;
	}
}