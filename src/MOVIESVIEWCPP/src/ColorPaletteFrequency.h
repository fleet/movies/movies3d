#pragma once
#include "colorpalette.h"
#include <vector>

class ColorPaletteFrequency
{
public:

	struct ColorPoint
	{
		unsigned char r;
		unsigned char g;
		unsigned char b;
		unsigned char a;
	};

	virtual void ComputePalette(int size);

	inline const ColorPoint & GetColor(int idx) const { return colors[idx]; }

private:
	std::vector<ColorPoint> colors;
};