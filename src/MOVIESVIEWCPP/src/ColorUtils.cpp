#include "ColorUtils.h"

unsigned int ColorUtils::FromRGB(unsigned char r, unsigned char g, unsigned char b)
{
	return ColorUtils::FromARGB(0xFF, r, g, b);
}

unsigned int ColorUtils::FromRGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
	Color color;
	color.a = a;
	color.r = r;
	color.b = b;
	color.g = g;
	return color.argb;
}

unsigned int ColorUtils::FromARGB(unsigned char a, unsigned char r, unsigned char g, unsigned char b)
{
	Color color;
	color.a = a;
	color.r = r;
	color.b = b;
	color.g = g;
	return color.argb;
}

unsigned int ColorUtils::Blend(unsigned int color1, unsigned int color2, float ratio)
{
	Color c1(color1);
	Color c2(color2);
	const float invRatio = 1.0f - ratio;
	c1.a = (c1.a * ratio) + (c2.a * invRatio);
	c1.r = (c1.r * ratio) + (c2.r * invRatio);
	c1.g = (c1.g * ratio) + (c2.g * invRatio);
	c1.b = (c1.b * ratio) + (c2.b * invRatio);
	return c1.argb;
}