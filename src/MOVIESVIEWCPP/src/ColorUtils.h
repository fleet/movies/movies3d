#pragma once

namespace ColorUtils
{
	union Color
	{
		unsigned int argb;
		struct
		{
			unsigned char b;
			unsigned char g;
			unsigned char r;
			unsigned char a;
		};

		Color() : argb(0xFF000000) {}
		Color(unsigned int _argb) : argb(_argb) {}
	};

	unsigned int FromRGB(unsigned char r, unsigned char g, unsigned char b);
	unsigned int FromRGBA(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	unsigned int FromARGB(unsigned char a, unsigned char r, unsigned char g, unsigned char b);

	unsigned int Blend(unsigned int color1, unsigned int color2, float ratio);
}

