#pragma once
#pragma managed
class DataObject3D
{
public:
	DataObject3D(void);
	virtual ~DataObject3D(void);
public:
	bool isVisible() { return m_bIsVisible; };
	void Show(bool &valueChanged)
	{
		if (!m_bIsVisible)
		{
			m_bIsVisible = true;
			valueChanged = true;
		}
	}
	void Hide(bool &valueChanged)
	{
		if (m_bIsVisible)
		{
			valueChanged = true;
			m_bIsVisible = false;
		}
	}
	virtual bool IsSounder()
	{
		return false;
	}
protected:
	bool m_bIsVisible;



};
ref class DataObject3DHull
{
public:
	DataObject3DHull(DataObject3D *src) { m_pRef = src; }
	DataObject3D *m_pRef;
};
