#include "DetectedBottomView.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"

#include "DisplayParameter.h"

using namespace MOVIESVIEWCPP;

void DetectedBottomView::SounderChanged()
{
	auto previousSelectedIndex = cbTransducers->SelectedIndex;

	//Fill combobox of transducers
	M3DKernel *pKernel = M3DKernel::GetInstance();
	m_sounderIds.Clear();
	m_transducerIds.Clear();
	cbTransducers->Items->Clear();

	for (unsigned int sound = 0; sound < pKernel->getObjectMgr()->GetSounderDefinition().GetNbSounder(); sound++)
	{
		Sounder *pSounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(sound);
		for (unsigned int i = 0; i < pSounder->GetTransducerCount(); i++)
		{
			auto transducer = pSounder->GetTransducer(i);
			auto transducerName = gcnew System::String(transducer->m_transName);
			cbTransducers->Items->Add(transducerName);

			m_sounderIds.Add(sound);
			m_transducerIds.Add(i);
		}
	}

	//Search for the best transducer to select
	if (cbTransducers->Items->Count > 0)
	{
		if (previousSelectedIndex >= 0 && previousSelectedIndex < cbTransducers->Items->Count)
			cbTransducers->SelectedIndex = previousSelectedIndex;
		else
		{
			auto storedTransducerName = DisplayParameter::getInstance()->GetDetectedBottomViewTransducerName();
			if (!storedTransducerName.empty())
			{
				auto foundIndex = cbTransducers->FindStringExact(gcnew System::String(storedTransducerName.c_str()));
				if (foundIndex > 0)
					cbTransducers->SelectedIndex = foundIndex;
				else
					cbTransducers->SelectedIndex = 0;
			}
			else
				cbTransducers->SelectedIndex = 0;
		}
	}
}

void DetectedBottomView::setPingOffset(int offset)
{
	if (offset != m_pingOffset)
	{
		m_pingOffset = offset;
		UpdateDetectedBottom();
	}
	
}

System::Void DetectedBottomView::checkOptions_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	//Resize the dialog box to hide or show the combobox to select the transducer.
	//Could have used the layouts, but it is a small dialog so it is fine like this.
	Size = System::Drawing::Size(Size.Width, checkOptions->Checked ? 100 : 70);
}

System::Void DetectedBottomView::cbTransducers_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	UpdateDetectedBottom();

	auto iSound = m_sounderIds[cbTransducers->SelectedIndex];
	auto iTrans = m_transducerIds[cbTransducers->SelectedIndex];

	M3DKernel *pKernel = M3DKernel::GetInstance();
	auto sounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(iSound);
	auto transducer = sounder->GetTransducer(iTrans);

	DisplayParameter::getInstance()->SetDetectedBottomViewTransducerName(transducer->m_transName);
}

void DetectedBottomView::UpdateDetectedBottom()
{
	auto bottom = getDetectedBottom();
	if (bottom > 0)
	{
		lblBottom->Text = bottom.ToString("0.00") + "m";
		lblBottom->ForeColor = Color::Black;
	}
	else
	{
		lblBottom->Text = "No Detection";
		lblBottom->ForeColor = Color::Red;
	}
}

double DetectedBottomView::getDetectedBottom()
{
	if (cbTransducers->SelectedIndex < 0)
		return 0.0;

	auto sounderIndex = m_sounderIds[cbTransducers->SelectedIndex];
	auto transducerIndex = m_transducerIds[cbTransducers->SelectedIndex];

	M3DKernel *pKernel = M3DKernel::GetInstance();
	auto sounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(sounderIndex);
	auto transducer = sounder->GetTransducer(transducerIndex);

	double bottom = 0.0;
	for (auto channelIndex = 0; channelIndex < transducer->GetChannelId().size(); ++channelIndex)
	{
		bottom = std::max<double>(bottom, getDetectedBottom(sounderIndex, transducerIndex, channelIndex));
	}

	return bottom;
}

double DetectedBottomView::getDetectedBottom(int sounderIndex, int transducerIndex, int channelIndex)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	
	auto sounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(sounderIndex);
	auto transducer = sounder->GetTransducer(transducerIndex);
	auto channel = transducer->GetChannelId()[channelIndex];

	double bottom = 0.0;

	TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(sounder->m_SounderId);
	if (pCont)
	{
		int pingIndex = std::max<int>(0, pCont->GetObjectCount() - 1 - m_pingOffset);
		PingFan *pingFan = (PingFan *)pCont->GetDatedObject(pingIndex);
		if (pingFan)
		{
			// r�cup�ration de la profondeur du fond verticale pour chaque channel
			std::uint32_t echoFondEval = 0;
			std::int32_t bottomRange = 0;
			bool found = false;
			pingFan->getBottom(channel, echoFondEval, bottomRange, found);

			if (found)
			{
				// Position du fond dans le rep�re antenne LB 25/03/2015 on travaille dans le rep�re antenne
				// pour l'EIsupervis� comme pour l'EI couche
				auto vecBottom = sounder->GetPolarToGeoCoord(pingFan, transducerIndex, channelIndex, echoFondEval - transducer->GetSampleOffset());
				bottom = vecBottom.z;			
			}
		}
	}

	return bottom;
}
