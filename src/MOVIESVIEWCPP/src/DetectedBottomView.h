#pragma once

namespace MOVIESVIEWCPP {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for DisplayBottomDialog
	/// </summary>
	public ref class DetectedBottomView : public System::Windows::Forms::Form
	{
	public:
		DetectedBottomView()
		{
			InitializeComponent();
			checkOptions->Checked = false;
			UpdateDetectedBottom();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~DetectedBottomView()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ComboBox^  cbTransducers;
	protected:

	protected:


	private: System::Windows::Forms::Label^  lblBottom;
	private: System::Windows::Forms::CheckBox^  checkOptions;


	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(DetectedBottomView::typeid));
			this->cbTransducers = (gcnew System::Windows::Forms::ComboBox());
			this->lblBottom = (gcnew System::Windows::Forms::Label());
			this->checkOptions = (gcnew System::Windows::Forms::CheckBox());
			this->SuspendLayout();
			// 
			// cbTransducers
			// 
			this->cbTransducers->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->cbTransducers->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->cbTransducers->FormattingEnabled = true;
			this->cbTransducers->Location = System::Drawing::Point(10, 36);
			this->cbTransducers->Name = L"cbTransducers";
			this->cbTransducers->Size = System::Drawing::Size(159, 21);
			this->cbTransducers->TabIndex = 0;
			this->cbTransducers->SelectedIndexChanged += gcnew System::EventHandler(this, &DetectedBottomView::cbTransducers_SelectedIndexChanged);
			// 
			// lblBottom
			// 
			this->lblBottom->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->lblBottom->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lblBottom->Location = System::Drawing::Point(5, 5);
			this->lblBottom->Name = L"lblBottom";
			this->lblBottom->Size = System::Drawing::Size(136, 25);
			this->lblBottom->TabIndex = 2;
			this->lblBottom->Text = L"No Detection";
			this->lblBottom->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// checkOptions
			// 
			this->checkOptions->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->checkOptions->Appearance = System::Windows::Forms::Appearance::Button;
			this->checkOptions->AutoSize = true;
			this->checkOptions->Checked = true;
			this->checkOptions->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkOptions->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"checkOptions.Image")));
			this->checkOptions->Location = System::Drawing::Point(147, 9);
			this->checkOptions->Name = L"checkOptions";
			this->checkOptions->Size = System::Drawing::Size(22, 22);
			this->checkOptions->TabIndex = 4;
			this->checkOptions->UseVisualStyleBackColor = true;
			this->checkOptions->CheckedChanged += gcnew System::EventHandler(this, &DetectedBottomView::checkOptions_CheckedChanged);
			// 
			// DetectedBottomView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(174, 66);
			this->Controls->Add(this->checkOptions);
			this->Controls->Add(this->lblBottom);
			this->Controls->Add(this->cbTransducers);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->Name = L"DetectedBottomView";
			this->Text = L"Detected Bottom";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


	private: System::Collections::Generic::List<int> m_sounderIds;
	private: System::Collections::Generic::List<int> m_transducerIds;
	private: int m_pingOffset = 0;

	public: void SounderChanged();
	public: void UpdateDetectedBottom();
	public: void setPingOffset(int offset);

	private: double getDetectedBottom();
	private: double getDetectedBottom(int sounderIndex, int transducerIndex, int channelIndex);

	private: System::Void cbTransducers_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkOptions_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
};
}
