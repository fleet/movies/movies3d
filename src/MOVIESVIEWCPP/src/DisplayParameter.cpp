#include "DisplayParameter.h"
#include <windows.h>
#include "ColorPaletteEcho.h"
#include "ColorPaletteChannel.h"
#include "ColorPaletteEIClass.h"
#include "M3DKernel/config/MovConfig.h"
#include <iomanip>

using namespace BaseKernel;

namespace {
	template<typename T>
	void SerializeEnum(BaseKernel::MovConfig * movConfig, const std::string & key, T value)
	{
		movConfig->SerializeData<int>(value, eInt, key);
	}

	template<typename T>
	bool DeSerializeEnum(BaseKernel::MovConfig * movConfig, const std::string & key, T & value)
	{
		int intValue;
		bool result = movConfig->DeSerializeData<int>(&intValue, eInt, key);
		if (result)
		{
			value = (T)intValue;
		}
		return result;
	}
}

namespace DisplayParametersKey {
	const char * PaletteType = "PaletteType";
}

void ColorParameters::Serialize(BaseKernel::MovConfig * movConfig, const std::string & key)
{
	std::stringstream ss;
	ss << std::hex << std::setw(8) << std::uppercase << color;
	movConfig->SerializeData(ss.str(), eString, key);
}

bool ColorParameters::DeSerialize(BaseKernel::MovConfig * movConfig, const std::string & key)
{
	bool ok = true;

	std::string str;
	ok = movConfig->DeSerializeData(&str, eString, key);
	
	std::stringstream ss(str);
	ss >> std::hex >> std::setw(8) >> std::uppercase >> color;

	return ok;
}

void PenParameters::Serialize(BaseKernel::MovConfig * movConfig, const std::string & key)
{
	color.Serialize(movConfig, key + "Color");
	movConfig->SerializeData<unsigned int>(width, eUInt, key + "Width");
}

bool PenParameters::DeSerialize(BaseKernel::MovConfig * movConfig, const std::string & key)
{
	bool ok = true;
	ok &= color.DeSerialize(movConfig, key + "Color");
	ok &= movConfig->DeSerializeData<unsigned int>(&width, eUInt, key + "Width");
	return ok;
}

DisplayParameter* DisplayParameter::m_pDisplayParameter = 0;

DisplayParameter::DisplayParameter()
	: ParameterModule("DisplayParameter")
	, m_bDisplayEchos(false)
	, m_bDisplayVolumeSampled(true)
	, m_current2DEchoPalette(nullptr)
{
	Reset();
}

DisplayParameter::~DisplayParameter()
{
}

// sauvegarde des param�tres dans le fichier de configuration associ�
bool DisplayParameter::Serialize(MovConfig * movConfig)
{
	m_displayLock.Lock();
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	movConfig->SerializeData<double>(m_Sampling, eDouble, "Sampling");
	movConfig->SerializeData<short>(m_minThreshold, eShort, "MinThreshold");
	movConfig->SerializeData<short>(m_maxThreshold, eShort, "MaxThreshold");

	movConfig->SerializeData<unsigned int>(m_pingFanDelayed, eUInt, "PingFanDelayed");
	movConfig->SerializeData<unsigned int>(m_PingFanLength, eUInt, "PingFanLength");

	movConfig->SerializeData<bool>(m_bDisplayEchos, eBool, "DisplayEchos");
	movConfig->SerializeData<bool>(m_bDisplayVolumeSampled, eBool, "DisplayVolumeSampled");

	movConfig->SerializeData<double>(m_minDepth, eDouble, "MinDepth");
	movConfig->SerializeData<double>(m_maxDepth, eDouble, "MaxDepth");

	movConfig->SerializeData<bool>(m_bOverwriteScalarWithChannelId, eBool, "OverwriteScalarWithChannelId");
	movConfig->SerializeData<bool>(m_bUseGlyphCube, eBool, "UseGlyphCube");

	// grilles
	movConfig->SerializeData<bool>(m_distanceGrid, eBool, "DistanceGrid");
	movConfig->SerializeData<double>(m_distanceSampling, eDouble, "DistanceSampling");
	movConfig->SerializeData<bool>(m_depthGrid, eBool, "DepthGrid");
	movConfig->SerializeData<double>(m_depthSampling, eDouble, "DepthSampling");

	movConfig->SerializeData<bool>(m_bStrech, eBool, "Strech");

	//affichage 2D des bancs
	movConfig->SerializeData<bool>(m_bDisplay2DShoals, eBool, "Display2DShoals");

	// affichage des single targets
	movConfig->SerializeData<bool>(m_bDisplay2DSingleEchoes, eBool, "Display2DSingleEchoes");
	SerializeEnum(movConfig, "TSDisplayType", m_tsDisplayType);

	movConfig->SerializeData<bool>(m_bUseGPSPositionning, eBool, "UseGPSPositionning");

	// nombre de pixels par ping
	movConfig->SerializeData<unsigned int>(m_pixelsPerPing, eUInt, "PixelsPerPing");

	// MOVIESDDD-200 - Ajout de la deviation
	movConfig->SerializeData<double>(m_deviationCoefficient, eDouble, "DeviationCoefficient");

	// mode de rendu
	SerializeEnum(movConfig, "Render2DMode", m_render2DMode);
	
	// type de palette pour les echos
	movConfig->SerializeData<short>(m_minPaletteThreshold, eShort, "MinPaletteThreshold");
	movConfig->SerializeData<short>(m_maxPaletteThreshold, eShort, "MaxPaletteThreshold");
	SerializeEnum(movConfig, DisplayParametersKey::PaletteType, m_colorPaletteEchoType);

	// Sauvegarde de la palette de couleurs
	m_manualColorPalette.Serialize(movConfig);

	// OTK - FAE 195 - ajout de la sauvegarde de certains param�tres
	movConfig->SerializeData<double>(m_echoDisplaySize, eDouble, "EchoDisplaySize");
	movConfig->SerializeData<double>(m_volumicBackgroundRed, eDouble, "BackgroundRed");
	movConfig->SerializeData<double>(m_volumicBackgroundGreen, eDouble, "BackgroundGreen");
	movConfig->SerializeData<double>(m_volumicBackgroundBlue, eDouble, "BackgroundBlue");
	movConfig->SerializeData<bool>(m_bOpacity, eBool, "Opacity");
	movConfig->SerializeData<double>(m_OpacityFactor, eDouble, "OpacityFactor");
	
	m_esuGridPen.Serialize(movConfig, "EsuGrid");
	m_bottomPen.Serialize(movConfig, "Bottom");

	movConfig->SerializeData<std::string>(m_detectedBottomTransducerName, eString, "DetectedBottomViewTransducer");

	movConfig->SerializeData<bool>(m_noiseLevelLogScale, eBool, "NoiseLevelLogScale");

	movConfig->SerializeData<bool>(m_noiseDisplayEnable, eBool, "noiseDisplayEnable");
	m_noiseColor.Serialize(movConfig, "noiseColor");

	movConfig->SerializeData<bool>(m_refNoiseDisplayEnable, eBool, "refNoiseDisplayEnable");
	m_refNoiseColor.Serialize(movConfig, "refNoiseColor");
	
	m_echogramBackgroundColor.Serialize(movConfig, "echogramBackgroundColor");

	movConfig->SerializeData<bool>(m_isLinkSounderEnabled, eBool, "isLinkSounderEnabled");
	movConfig->SerializeData<bool>(m_isBeamVolumeDisplayed, eBool, "isBeamVolumeDisplayed");
	movConfig->SerializeData<bool>(m_isGraduationsDisplayed, eBool, "isGraduationsDisplayed");
	movConfig->SerializeData<bool>(m_isLabelsDisplayed, eBool, "isLabelsDisplayed");
	movConfig->SerializeData<bool>(m_isSeaFloorEnabled, eBool, "isSeaFloorEnabled");
	movConfig->SerializeData<int>(m_seaFloorScale, eInt, "seaFloorScale");
	movConfig->SerializeData<double>(m_singleEchoDisplaySize, eDouble, "singleEchoDisplaySize");
	movConfig->SerializeData<bool>(m_isSingleEchosDisplayed, eBool, "isSingleEchosDisplayed");
	movConfig->SerializeData<bool>(m_isShoalVolumesDisplayed, eBool, "isShoalVolumesDisplayed");
	movConfig->SerializeData<bool>(m_isShoalVoumesSmoothed, eBool, "isShoalVoumesSmoothed");
	movConfig->SerializeData<bool>(m_isShoalLabelsDisplayed, eBool, "isShoalLabelsDisplayed");
	movConfig->SerializeData<short>(m_shoalDisplayType, eShort, "shoalDisplayType");
	movConfig->SerializeData<int>(m_verticalScale, eInt, "verticalScale");

	movConfig->SerializeData<double>(m_azimuth, eDouble, "azimuth");
	movConfig->SerializeData<double>(m_elevation, eDouble, "elevation");
	movConfig->SerializeData<double>(m_zoomFactor, eDouble, "zoomFactor");
	movConfig->SerializeData<bool>(m_resetCamera, eBool, "resetCamera");
	movConfig->SerializeData<bool>(m_useAutoReset, eBool, "useAutoReset");

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	m_displayLock.Unlock();

	return true;
}

// lecture des param�tres dans le fichier de configuration associ�
bool DisplayParameter::DeSerialize(MovConfig * movConfig)
{
	m_displayLock.Lock();

	// debut de la d�s�rialisation du module
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	if (result)
	{

		result = result && movConfig->DeSerializeData<double>(&m_Sampling, eDouble, "Sampling");
		result = result && movConfig->DeSerializeData<short>(&m_minThreshold, eShort, "MinThreshold");
		result = result && movConfig->DeSerializeData<short>(&m_maxThreshold, eShort, "MaxThreshold");

		result = result && movConfig->DeSerializeData<unsigned int>(&m_pingFanDelayed, eUInt, "PingFanDelayed");
		result = result && movConfig->DeSerializeData<unsigned int>(&m_PingFanLength, eUInt, "PingFanLength");

		movConfig->DeSerializeData<bool>(&m_bDisplayEchos, eBool, "DisplayEchos");
		movConfig->DeSerializeData<bool>(&m_bDisplayVolumeSampled, eBool, "DisplayVolumeSampled");

		result = result && movConfig->DeSerializeData<double>(&m_minDepth, eDouble, "MinDepth");
		result = result && movConfig->DeSerializeData<double>(&m_maxDepth, eDouble, "MaxDepth");
		m_currentMinDepth = m_minDepth;
		m_currentMaxDepth = m_maxDepth;

		result = result && movConfig->DeSerializeData<bool>(&m_bOverwriteScalarWithChannelId, eBool, "OverwriteScalarWithChannelId");
		result = result && movConfig->DeSerializeData<bool>(&m_bUseGlyphCube, eBool, "UseGlyphCube");

		// grilles
		result = result && movConfig->DeSerializeData<bool>(&m_distanceGrid, eBool, "DistanceGrid");
		result = result && movConfig->DeSerializeData<double>(&m_distanceSampling, eDouble, "DistanceSampling");
		result = result && movConfig->DeSerializeData<bool>(&m_depthGrid, eBool, "DepthGrid");
		result = result && movConfig->DeSerializeData<double>(&m_depthSampling, eDouble, "DepthSampling");

		result = result && movConfig->DeSerializeData<bool>(&m_bStrech, eBool, "Strech");

		//affichage 2D des bancs
		result = result && movConfig->DeSerializeData<bool>(&m_bDisplay2DShoals, eBool, "Display2DShoals");

		//affichage des single targets
		result = result && movConfig->DeSerializeData<bool>(&m_bDisplay2DSingleEchoes, eBool, "Display2DSingleEchoes");
		DeSerializeEnum(movConfig, "TSDisplayType", m_tsDisplayType);

		movConfig->DeSerializeData<bool>(&m_bUseGPSPositionning, eBool, "UseGPSPositionning");

		// nombre de pixels par ping
		movConfig->DeSerializeData<unsigned int>(&m_pixelsPerPing, eUInt, "PixelsPerPing");

		// MOVIESDDD-200 - Ajout de la deviation
		movConfig->DeSerializeData<double>(&m_deviationCoefficient, eDouble, "DeviationCoefficient");

		// mode de rendu
		DeSerializeEnum(movConfig, "Render2DMode", m_render2DMode);

		// type de palette affich�e
		result = result && movConfig->DeSerializeData<short>(&m_minPaletteThreshold, eShort, "MinPaletteThreshold");
		result = result && movConfig->DeSerializeData<short>(&m_maxPaletteThreshold, eShort, "MaxPaletteThreshold");
		DeSerializeEnum(movConfig, DisplayParametersKey::PaletteType, m_colorPaletteEchoType);

		// Chargement de la palette de couleurs
		result = result && m_manualColorPalette.DeSerialize(movConfig);
		
		// Calcul de la palette
		RecomputePalette();

		// OTK - FAE 195 - ajout de la sauvegarde de certains param�tres
		movConfig->DeSerializeData<double>(&m_echoDisplaySize, eDouble, "EchoDisplaySize");
		movConfig->DeSerializeData<double>(&m_volumicBackgroundRed, eDouble, "BackgroundRed");
		movConfig->DeSerializeData<double>(&m_volumicBackgroundGreen, eDouble, "BackgroundGreen");
		movConfig->DeSerializeData<double>(&m_volumicBackgroundBlue, eDouble, "BackgroundBlue");
		movConfig->DeSerializeData<bool>(&m_bOpacity, eBool, "Opacity");
		movConfig->DeSerializeData<double>(&m_OpacityFactor, eDouble, "OpacityFactor");
		
		m_esuGridPen.DeSerialize(movConfig, "EsuGrid");
		m_bottomPen.DeSerialize(movConfig, "Bottom");

		result = result && movConfig->DeSerializeData<std::string>(&m_detectedBottomTransducerName, eString, "DetectedBottomViewTransducer");

		result = result && movConfig->DeSerializeData<bool>(&m_noiseLevelLogScale, eBool, "NoiseLevelLogScale");

		movConfig->DeSerializeData<bool>(&m_noiseDisplayEnable, eBool, "noiseDisplayEnable");
		m_noiseColor.DeSerialize(movConfig, "noiseColor");

		movConfig->DeSerializeData<bool>(&m_refNoiseDisplayEnable, eBool, "refNoiseDisplayEnable");
		m_refNoiseColor.DeSerialize(movConfig, "refNoiseColor");

		m_echogramBackgroundColor.DeSerialize(movConfig, "echogramBackgroundColor");

		movConfig->DeSerializeData<bool>(&m_isLinkSounderEnabled, eBool, "isLinkSounderEnabled");
		movConfig->DeSerializeData<bool>(&m_isBeamVolumeDisplayed, eBool, "isBeamVolumeDisplayed");
		movConfig->DeSerializeData<bool>(&m_isGraduationsDisplayed, eBool, "isGraduationsDisplayed");
		movConfig->DeSerializeData<bool>(&m_isLabelsDisplayed, eBool, "isLabelsDisplayed");
		movConfig->DeSerializeData<bool>(&m_isSeaFloorEnabled, eBool, "isSeaFloorEnabled");
		movConfig->DeSerializeData<int>(&m_seaFloorScale, eInt, "seaFloorScale");
		movConfig->DeSerializeData<double>(&m_singleEchoDisplaySize, eDouble, "singleEchoDisplaySize");
		movConfig->DeSerializeData<bool>(&m_isSingleEchosDisplayed, eBool, "isSingleEchosDisplayed");
		movConfig->DeSerializeData<bool>(&m_isShoalVolumesDisplayed, eBool, "isShoalVolumesDisplayed");
		movConfig->DeSerializeData<bool>(&m_isShoalVoumesSmoothed, eBool, "isShoalVoumesSmoothed");
		movConfig->DeSerializeData<bool>(&m_isShoalLabelsDisplayed, eBool, "isShoalLabelsDisplayed");
		movConfig->DeSerializeData<short>(&m_shoalDisplayType, eShort, "shoalDisplayType");
		movConfig->DeSerializeData<int>(&m_verticalScale, eInt, "verticalScale");

		movConfig->DeSerializeData<double>(&m_azimuth, eDouble, "azimuth");
		movConfig->DeSerializeData<double>(&m_elevation, eDouble, "elevation");
		movConfig->DeSerializeData<double>(&m_zoomFactor, eDouble, "zoomFactor");
		movConfig->DeSerializeData<bool>(&m_resetCamera, eBool, "resetCamera");
		movConfig->DeSerializeData<bool>(&m_useAutoReset, eBool, "useAutoReset");

		// on doit recalculer les visu car les parametres ont potentiellement chang�
		m_bIsDirty = true;
	}

	// fin de la d�s�rialisation du module
	movConfig->DeSerializePushBack();

	m_displayLock.Unlock();

	return result;
}

void DisplayParameter::Reset()
{
	m_bIsDirty = true;
	m_bNewRange = false;
	m_Sampling = 1;

	m_maxThreshold = 1000;
	m_minThreshold = -6000;

	m_angleThreshold = 0;

	m_pingFanDelayed = 0;
	m_PingFanLength = 50;

	m_minDepth = 0;
	m_maxDepth = 300;
	m_isZoomActivated = false;
	m_currentMinDepth = m_minDepth;
	m_currentMaxDepth = m_maxDepth;

	SetOverwriteScalarWithChannelId(false);

	m_distanceGrid = true;
	m_depthGrid = true;
	m_distanceSampling = 0.1;
	m_depthSampling = 15;

	m_bUseGlyphCube = true;

	m_maxPaletteThreshold = -2200;
	m_minPaletteThreshold = -6000;
	m_colorPaletteEchoType = eCustom;
	m_manualColorPalette.Reset();
	m_tsColorPalette.Reset();
	this->RecomputePalette();

	// OTK - 10/06/2009 - parametres du zoom (de 0 � 100% par d�faut)
	m_minFrontHorizontalRatio = 0;
	m_maxFrontHorizontalRatio = 1;
	m_minSideHorizontalRatio = 0;
	m_maxSideHorizontalRatio = 1;

	m_bStrech = false;

	m_bDisplayEchos = false;
	m_bDisplayVolumeSampled = true;

	m_bDisplay2DShoals = true;
	m_bDisplay2DSingleEchoes = false;
	m_tsDisplayType = eTSDisplayAll;
	m_bEchoTooltip = false;
	m_bUseGPSPositionning = true;

	m_pixelsPerPing = 1;
	m_render2DMode = eMaxValueDisplay;

	m_echoDisplaySize = 1;
	m_volumicBackgroundRed = 1;
	m_volumicBackgroundGreen = 1;
	m_volumicBackgroundBlue = 1;
	m_bOpacity = true;
	m_OpacityFactor = 1;

	m_esuGridPen.color = 0xFFFF0000;
	m_esuGridPen.width = 4;
	
	m_bottomPen.color = 0xFF000000;
	m_bottomPen.width = 1;

	m_noiseDisplayEnable = true;
	m_noiseColor = 0xFFFF0000;

	m_refNoiseDisplayEnable = true;
	m_refNoiseColor = 0xFF00FF00;

	m_deviationCoefficient = 0.1;

	m_noiseLevelLogScale = true;

	m_echogramBackgroundColor = 0xFFFFFFFF;

	m_isLinkSounderEnabled = true;
	m_isBeamVolumeDisplayed = true;
	m_isGraduationsDisplayed = true;
	m_isLabelsDisplayed = true;
	m_isSeaFloorEnabled = true;
	m_seaFloorScale = 10;
	m_singleEchoDisplaySize = 0.050;
	m_isSingleEchosDisplayed = false;
	m_isShoalVolumesDisplayed = true;
	m_isShoalVoumesSmoothed = true;
	m_isShoalLabelsDisplayed = false;
	m_shoalDisplayType = 0; // Volumes
	m_verticalScale = 10;

	m_azimuth = 0;
	m_elevation = 0;
	m_zoomFactor = 1.0;
	m_resetCamera = true;
	m_useAutoReset = true;
}

void DisplayParameter::SetManualColorPalette(const ManualColorPalette &ref)
{
	m_displayLock.Lock();
	if (m_manualColorPalette != ref)
	{
		m_manualColorPalette = ref;
		m_bIsDirty = true;
		RecomputePalette();
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetCurrentColorPaletteChannel(const ColorPaletteChannel &ref)
{
	m_displayLock.Lock();	
	if (m_pColorPaletteChannel != ref)
	{
		m_pColorPaletteChannel = ref;
		m_bIsDirty = true;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetMaxThreshold(DataFmt a)
{
	m_displayLock.Lock();
	if (m_maxThreshold != a)
	{
		m_bIsDirty = true;
		m_maxThreshold = a;
		RecomputePalette();
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetMinThreshold(DataFmt a)
{
	m_displayLock.Lock();
	if (m_minThreshold != a)
	{
		m_bIsDirty = true;
		m_minThreshold = a;
		RecomputePalette();
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetAngleThreshold(DataFmt a)
{
	m_displayLock.Lock();
	if (m_minThreshold != a)
	{
		m_bIsDirty = true;
		m_angleThreshold = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetSampling(double a)
{
	m_displayLock.Lock();
	if (m_Sampling != a)
	{
		m_bIsDirty = true;
		m_Sampling = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetPingFanDelayed(unsigned int a)
{
	m_displayLock.Lock();
	if (m_pingFanDelayed != a)
		m_bIsDirty = true;
	m_pingFanDelayed = a;
	m_displayLock.Unlock();
}

void DisplayParameter::SetPingFanLength(unsigned int a)
{
	m_displayLock.Lock();
	if (m_PingFanLength != a)
	{
		m_bIsDirty = true;
		m_PingFanLength = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetUseGPSPositionning(bool a)
{
	m_displayLock.Lock();
	if (m_bUseGPSPositionning != a)
	{
		m_bIsDirty = true;
		m_bUseGPSPositionning = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetDistanceGrid(bool a)
{
	m_displayLock.Lock();
	if (m_distanceGrid != a)
	{
		m_bIsDirty = true;
		m_distanceGrid = a;
	}
	m_displayLock.Unlock();
}
void DisplayParameter::SetDepthGrid(bool a)
{
	m_displayLock.Lock();
	if (m_depthGrid != a)
		m_bIsDirty = true;
	m_depthGrid = a;
	m_displayLock.Unlock();
}

void DisplayParameter::SetDistanceSampling(double a)
{
	m_displayLock.Lock();
	if (m_distanceSampling != a)
	{
		m_bIsDirty = true;
		m_distanceSampling = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetDepthSampling(double a)
{
	m_displayLock.Lock();
	if (m_depthSampling != a)
	{
		m_bIsDirty = true;
		m_depthSampling = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetMinDepth(double a)
{
	m_displayLock.Lock();
	if (a < m_maxDepth)
	{
		if (m_minDepth != a)
		{
			m_bIsDirty = true;
			m_bNewRange = true;
		}
		m_minDepth = a;
		if (!m_isZoomActivated)
		{
			m_currentMinDepth = a;
		}
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetMaxDepth(double a)
{
	m_displayLock.Lock();
	if (a > m_minDepth)
	{
		if (m_maxDepth != a)
		{
			m_bIsDirty = true;
			m_bNewRange = true;
		}
		m_maxDepth = a;
		if (!m_isZoomActivated)
		{
			m_currentMaxDepth = a;
		}
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetZoomActivated(bool activated)
{
	m_displayLock.Lock();
	if (m_isZoomActivated != activated)
	{
		m_bIsDirty = true;
		m_bNewRange = true;
		m_isZoomActivated = activated;
		if (!activated)
		{
			m_currentMinDepth = m_minDepth;
			m_currentMaxDepth = m_maxDepth;
		}
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetCurrentMinDepth(double a)
{
	m_displayLock.Lock();	
	if (m_currentMinDepth != a)
	{
		m_bIsDirty = true;
		m_bNewRange = true;
		m_currentMinDepth = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetCurrentMaxDepth(double a)
{
	m_displayLock.Lock();
	if (m_currentMaxDepth != a)
	{
		m_bIsDirty = true;
		m_bNewRange = true;
		m_currentMaxDepth = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetOverwriteScalarWithChannelId(bool a)
{
	m_displayLock.Lock();
	if (m_bOverwriteScalarWithChannelId != a)
	{
		m_bIsDirty = true;
		m_bOverwriteScalarWithChannelId = a;
	}	
	m_displayLock.Unlock();
}

void DisplayParameter::RecomputePalette()
{
	// delete old palette
	switch (m_colorPaletteEchoType)
	{
	default:
	case eCustom:
		m_current2DEchoPalette = &m_manualColorPalette;
		break;
	case eParula:
		m_current2DEchoPalette = &m_parulaColorPalette;
		break;
	case eRedBlue:
		m_current2DEchoPalette = &m_redblueColorPalette;
		break;
	}

	m_current2DEchoPalette->PreparePalette(m_minPaletteThreshold, m_maxPaletteThreshold);
	m_tsColorPalette.PreparePalette(m_minPaletteThreshold, m_maxPaletteThreshold);
}

void DisplayParameter::SetMinFrontHorizontalRatio(double a)
{
	m_displayLock.Lock();
	if (a < m_maxFrontHorizontalRatio)
	{
		if (m_minFrontHorizontalRatio != a)
		{
			m_bIsDirty = true;
			m_bNewRange = true;
		}
		m_minFrontHorizontalRatio = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetMaxFrontHorizontalRatio(double a)
{
	m_displayLock.Lock();
	if (a > m_minFrontHorizontalRatio)
	{
		if (m_maxFrontHorizontalRatio != a)
		{
			m_bIsDirty = true;
			m_bNewRange = true;
		}
		m_maxFrontHorizontalRatio = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetMinMaxFrontHorizontalRatio(double minValue, double maxValue)
{
	m_displayLock.Lock();
	auto realMinValue = max(0.0, min(minValue, maxValue));
	auto realMaxValue = min(1.0, max(minValue, maxValue));
	if (realMinValue != m_minFrontHorizontalRatio || realMaxValue != m_maxFrontHorizontalRatio)
	{
		m_bIsDirty = true;
		m_bNewRange = true;

		m_minFrontHorizontalRatio = realMinValue;
		m_maxFrontHorizontalRatio = realMaxValue;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetMinSideHorizontalRatio(double a)
{
	m_displayLock.Lock();
	if (a < m_maxSideHorizontalRatio)
	{
		if (m_minSideHorizontalRatio != a)
		{
			m_bIsDirty = true;
			m_bNewRange = true;
		}
		m_minSideHorizontalRatio = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetMaxSideHorizontalRatio(double a)
{
	m_displayLock.Lock();
	if (a > m_minSideHorizontalRatio)
	{
		if (m_maxSideHorizontalRatio != a)
		{
			m_bIsDirty = true;
			m_bNewRange = true;
		}
		m_maxSideHorizontalRatio = a;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetMinMaxSideHorizontalRatio(double minValue, double maxValue)
{
	m_displayLock.Lock();
	
	auto realMinValue = max(0.0, min(minValue, maxValue));
	auto realMaxValue = min(1.0, max(minValue, maxValue));	
	if (realMinValue != m_minSideHorizontalRatio || realMaxValue != m_maxSideHorizontalRatio)
	{
		m_bIsDirty = true;
		m_bNewRange = true;

		m_minSideHorizontalRatio = realMinValue;
		m_maxSideHorizontalRatio = realMaxValue;
	}
	m_displayLock.Unlock();
}

DataFmt DisplayParameter::GetMinPaletteThreshold() const
{ 
	m_displayLock.Lock(); 
	DataFmt a = m_minPaletteThreshold; 
	m_displayLock.Unlock(); 
	return a; 
}

DataFmt DisplayParameter::GetMaxPaletteThreshold() const
{ 
	m_displayLock.Lock(); 
	DataFmt a = m_maxPaletteThreshold; 
	m_displayLock.Unlock(); 
	return a; 
}

void DisplayParameter::SetMinPaletteThreshold(DataFmt val)
{
	m_displayLock.Lock();
	if (m_minPaletteThreshold != val)
	{
		m_bIsDirty = true;
		m_minPaletteThreshold = val;
		RecomputePalette();
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetMaxPaletteThreshold(DataFmt val)
{
	m_displayLock.Lock();
	if (m_maxPaletteThreshold != val)
	{
		m_bIsDirty = true;
		m_maxPaletteThreshold = val;
		RecomputePalette();
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetColorPaletteEchoType(ColorPaletteEchoType val)
{
	m_displayLock.Lock();
	if (m_colorPaletteEchoType != val)
	{
		m_bIsDirty = true;
		m_colorPaletteEchoType = val;
		RecomputePalette();
	}
	m_displayLock.Unlock();
}

ColorPaletteEchoType DisplayParameter::GetColorPaletteEchoType() const
{
	m_displayLock.Lock();
	ColorPaletteEchoType val = m_colorPaletteEchoType;
	m_displayLock.Unlock();
	return val;
}

ColorPaletteChannel DisplayParameter::GetColorPaletteChannel()
{
	m_displayLock.Lock();
	ColorPaletteChannel result = m_pColorPaletteChannel;
	m_displayLock.Unlock();
	return result;
}

ManualColorPalette DisplayParameter::GetManualColorPalette()
{
	m_displayLock.Lock();
	ManualColorPalette result = m_manualColorPalette;
	m_displayLock.Unlock();
	return result;
}

const IColorPalette * DisplayParameter::GetCurrent2DEchoPalette() const
{
	m_displayLock.Lock();
	const IColorPalette * palette = m_current2DEchoPalette;
	m_displayLock.Unlock();
	return palette;
}

const IColorPalette * DisplayParameter::GetTSEchoPalette() const
{
	m_displayLock.Lock();
	const IColorPalette * palette = &m_tsColorPalette;
	m_displayLock.Unlock();
	return palette;
}

const IColorPalette * DisplayParameter::GetCurrent3DEchoPalette() const
{
	m_displayLock.Lock();
	const IColorPalette * palette = m_bOverwriteScalarWithChannelId
		? static_cast<const IColorPalette *>(&m_pColorPaletteChannel)
		: m_current2DEchoPalette;
	m_displayLock.Unlock();
	return palette;
}

ColorPaletteEIClass DisplayParameter::GetColorPaletteEIClass() const
{
	m_displayLock.Lock();
	ColorPaletteEIClass result = m_pColorPaletteEIClass;
	m_displayLock.Unlock();
	return result;
}

void DisplayParameter::SetStrech(bool a)
{
	m_displayLock.Lock();
	if (m_bStrech != a)
	{
		m_bIsDirty = true;
	}
	m_bStrech = a;
	m_displayLock.Unlock();
}

void DisplayParameter::SetPixelsPerPing(unsigned int nbPixels)
{
	m_displayLock.Lock();
	if (m_pixelsPerPing != nbPixels)
	{
		m_bIsDirty = true;
	}
	m_pixelsPerPing = nbPixels;
	m_displayLock.Unlock();
}

void DisplayParameter::SetRender2DMode(Render2DMode val)
{
	m_displayLock.Lock();
	if (m_render2DMode != val)
	{
		m_bIsDirty = true;
	}
	m_render2DMode = val;
	m_displayLock.Unlock();
}

void DisplayParameter::SetDeviationCoefficient(double deviationCoefficient)
{
	m_displayLock.Lock();
	if (m_deviationCoefficient != deviationCoefficient)
	{
		m_bIsDirty = true;
	}
	m_deviationCoefficient = deviationCoefficient;
	m_displayLock.Unlock();
}

bool DisplayParameter::GetDisplay2DSingleEchoes() const
{
	m_displayLock.Lock();
	bool result = m_bDisplay2DSingleEchoes;
	m_displayLock.Unlock();
	return result;
}

void DisplayParameter::SetDisplay2DSingleEchoes(bool val)
{
	m_displayLock.Lock();
	if (m_bDisplay2DSingleEchoes != val)
	{
		m_bIsDirty = true;
	}
	m_bDisplay2DSingleEchoes = val;
	m_displayLock.Unlock();
}

void DisplayParameter::SetTSDisplayType(TSDisplayType d)
{
	m_displayLock.Lock();
	if (m_tsDisplayType != d)
	{
		m_bIsDirty = true;
	}
	m_tsDisplayType = d;
	m_displayLock.Unlock();
}

void DisplayParameter::SetEchogramBackgroundColor(unsigned int color)
{ 
	m_displayLock.Lock();
	if (m_echogramBackgroundColor != color)
	{
		m_echogramBackgroundColor = color;
		m_bIsDirty = true;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetVolumicBackgroundColor(double red, double green, double blue) 
{
	m_displayLock.Lock();
	if (m_volumicBackgroundRed != red || m_volumicBackgroundGreen != green || m_volumicBackgroundBlue != blue)
	{
		m_volumicBackgroundRed = red;
		m_volumicBackgroundGreen = green;
		m_volumicBackgroundBlue = blue;
		m_bIsDirty = true;
	}
	m_displayLock.Unlock();
}
PenParameters DisplayParameter::GetEsuGridPen() const
{
	return m_esuGridPen;
}

void DisplayParameter::SetEsuGridPen(PenParameters pen)
{
	m_displayLock.Lock();
	m_esuGridPen = pen;
	m_displayLock.Unlock();
}

PenParameters DisplayParameter::GetBottomPen() const
{
	return m_bottomPen;
}

void DisplayParameter::SetBottomPen(PenParameters pen)
{
	m_displayLock.Lock();
	m_bottomPen = pen;
	m_displayLock.Unlock();
}

unsigned int DisplayParameter::GetNoiseColor() const
{
	return m_noiseColor;
}

void DisplayParameter::SetNoiseColor(unsigned int color)
{
	m_displayLock.Lock();	
	if (m_noiseColor != color)
	{
		m_noiseColor = color;
		m_bIsDirty = true;
	}
	m_displayLock.Unlock();
}

void DisplayParameter::SetRefNoiseColor(unsigned int value) 
{
	m_displayLock.Lock(); 
	if (m_refNoiseColor != value)
	{
		m_refNoiseColor = value;
		m_bIsDirty = true;
	}
	m_displayLock.Unlock();
}