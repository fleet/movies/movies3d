/******************************************************************************/
/*	Project:	MOVIES3D														  */
/*	Author(s):	C.PONCELET													  */
/*	File:		DisplayParameter.h											  */
/******************************************************************************/
#pragma once
#include "M3DKernel/DefConstants.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "M3DKernel/parameter/ParameterModule.h"

#include "ColorPalette.h"
#include "ColorPaletteEcho.h"
#include "ColorPaletteChannel.h"
#include "ColorPaletteEIClass.h"

enum TSDisplayType
{
	eTSDisplayAll = 0,
	eTSDisplayTracked,
	eTSDisplayUntracked
};

enum Render2DMode
{
	eFastDisplay = 0,	/// Affiche la valeur de l'�cho le plus proche pour un pixel (echo au centre du pixel)
	eMaxValueDisplay	/// Affiche la valeur max des �chos correspondants � un pixel sur la vertical de la colonne d'eau
};

/**
* Type de palette pour la repr�sentation des echos
*/
enum ColorPaletteEchoType
{
	eCustom,	/// Palette d�fini par seuils et couleurs manuellement
	eParula,	/// Palette de type Parula
	eRedBlue	/// Palette de type gradient rouge - gris - bleu
};

/**
* Classe pour la serialisation d'une couleur sous la forme hexa argb
*/
struct ColorParameters
{
	unsigned int color;

	ColorParameters() : color(0xFFFFFFFF) {}
	ColorParameters(unsigned int c) : color(c) {}
	ColorParameters(const ColorParameters & other) : color(other.color) {}

	operator unsigned int() const { return color; }
	ColorParameters operator=(unsigned int c) { color = c; return *this; }
	ColorParameters operator=(const ColorParameters & other) { color = other.color; return *this; }

	virtual void Serialize(BaseKernel::MovConfig * movConfig, const std::string & key);
	virtual bool DeSerialize(BaseKernel::MovConfig * movConfig, const std::string & key);
};


/**
* Param�tres li�s � un pinceau : epaisseur et couleur
*/
struct PenParameters
{
	ColorParameters color;
	unsigned int width;

	virtual void Serialize(BaseKernel::MovConfig * MovConfig, const std::string & key);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig, const std::string & key);
};


/**
**  Singleton contenant les parametres de gestion de l'affichage de Movie3d
**	cr�e lors de l'appel � la m�thode getInstance, et renseign�e avec les valeurs du contructeur
*/
class DisplayParameter : public BaseKernel::ParameterModule
{
	CRecursiveMutex m_displayLock;

private:
	// disable copy
	DisplayParameter(const DisplayParameter & other);
	DisplayParameter & operator=(const DisplayParameter & other);


public:
	static DisplayParameter* getInstance()
	{
		if (!m_pDisplayParameter)
		{
			m_pDisplayParameter = new DisplayParameter();
		}
		return m_pDisplayParameter;
	}

	void Release()
	{
		delete this;
	}

	
	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);
	
	inline void Lock() { m_displayLock.Lock(); }
	inline void Unlock() { m_displayLock.Unlock(); }

	void SetMinThreshold(DataFmt a);
	void SetMaxThreshold(DataFmt a);

	void SetMinDepth(double a);
	void SetMaxDepth(double a);

	void SetZoomActivated(bool activated);
	void SetCurrentMinDepth(double a);
	void SetCurrentMaxDepth(double a);
	
	void SetSampling(double a);
	void SetPingFanDelayed(unsigned int);
	void SetPingFanLength(unsigned int);
	void SetUseGPSPositionning(bool);

	void SetDistanceGrid(bool a);
	void SetDepthGrid(bool a);
	void SetDistanceSampling(double a);
	void SetDepthSampling(double a);

	// OTK - FAE002 - on peut choisir de d�former les images pour les faire tenir dans leur cadre
	// ou d'avoir une image non d�form�e plus proche de la r�alit�
	void SetStrech(bool a);
	bool GetStrech() { m_displayLock.Lock(); bool a = m_bStrech; m_displayLock.Unlock(); return a; };

	DataFmt GetMinThreshold() { m_displayLock.Lock(); DataFmt a = m_minThreshold; m_displayLock.Unlock(); return a; }
	DataFmt GetMaxThreshold() { m_displayLock.Lock(); DataFmt a = m_maxThreshold; m_displayLock.Unlock(); return a; }

	DataFmt GetAngleThreshold() { m_displayLock.Lock(); DataFmt a = m_angleThreshold; m_displayLock.Unlock(); return a; }
	void SetAngleThreshold(DataFmt threshold);
	
	double  GetMinDepth() { m_displayLock.Lock(); double a = m_minDepth; m_displayLock.Unlock(); return a; }
	double  GetMaxDepth() { m_displayLock.Lock(); double a = m_maxDepth; m_displayLock.Unlock(); return a; }

	bool    IsZoomActivated() { m_displayLock.Lock(); bool activated = m_isZoomActivated; m_displayLock.Unlock(); return activated;	}
	double  GetCurrentMinDepth() { m_displayLock.Lock(); double a = m_currentMinDepth; m_displayLock.Unlock(); return a; }
	double  GetCurrentMaxDepth() { m_displayLock.Lock(); double a = m_currentMaxDepth; m_displayLock.Unlock(); return a; }

	double GetSampling() { m_displayLock.Lock(); double a = m_Sampling; m_displayLock.Unlock(); return a; }
	bool GetOverwriteScalarWithChannelId() { m_displayLock.Lock(); bool a = m_bOverwriteScalarWithChannelId; m_displayLock.Unlock(); return a; }
	
	unsigned int GetPingFanDelayed() { m_displayLock.Lock(); unsigned int a = m_pingFanDelayed; m_displayLock.Unlock(); return a; }
	unsigned int GetPingFanLength() { m_displayLock.Lock(); unsigned int a = m_PingFanLength; m_displayLock.Unlock(); return a; }
	bool GetUseGPSPositionning() { m_displayLock.Lock(); bool a = m_bUseGPSPositionning; m_displayLock.Unlock(); return a; }
	
	bool dataChanged() { return m_bIsDirty; };
	void resetDataChanged() { m_displayLock.Lock(); m_bIsDirty = false; m_bNewRange = false; m_displayLock.Unlock(); }
	void setDataChanged() { m_displayLock.Lock(); m_bIsDirty = true; m_bNewRange = true; m_displayLock.Unlock(); }

	bool rangeChanged() { return m_bNewRange; };

	void SetOverwriteScalarWithChannelId(bool a);

	DataFmt GetMinPaletteThreshold() const;
	DataFmt GetMaxPaletteThreshold() const;

	void SetMinPaletteThreshold(DataFmt a);
	void SetMaxPaletteThreshold(DataFmt a);

	void SetColorPaletteEchoType(ColorPaletteEchoType val);
	ColorPaletteEchoType GetColorPaletteEchoType() const;

	ColorPaletteChannel GetColorPaletteChannel();
	void SetCurrentColorPaletteChannel(const ColorPaletteChannel &ref);
	
	ManualColorPalette GetManualColorPalette();
	void SetManualColorPalette(const ManualColorPalette &ref);

	const IColorPalette * GetCurrent2DEchoPalette() const;
	const IColorPalette * GetTSEchoPalette() const;
	const IColorPalette * GetCurrent3DEchoPalette() const;
	ColorPaletteEIClass GetColorPaletteEIClass() const;
	
	bool GetDistanceGrid() { m_displayLock.Lock(); bool a = m_distanceGrid; m_displayLock.Unlock(); return a; }
	bool GetDepthGrid() { m_displayLock.Lock(); bool a = m_depthGrid; m_displayLock.Unlock(); return a; }
	double GetDistanceSampling() { m_displayLock.Lock(); double a = m_distanceSampling; m_displayLock.Unlock(); return a; }
	double GetDepthSampling() { m_displayLock.Lock(); double a = m_depthSampling; m_displayLock.Unlock(); return a; }

	void		SetMinFrontHorizontalRatio(double a);
	void		SetMaxFrontHorizontalRatio(double a);
	void		SetMinMaxFrontHorizontalRatio(double min, double max);
	void		SetMinSideHorizontalRatio(double a);
	void		SetMaxSideHorizontalRatio(double a);
	void		SetMinMaxSideHorizontalRatio(double min, double max);
	double  GetMinFrontHorizontalRatio() { m_displayLock.Lock(); double a = m_minFrontHorizontalRatio; m_displayLock.Unlock(); return a; }
	double  GetMaxFrontHorizontalRatio() { m_displayLock.Lock(); double a = m_maxFrontHorizontalRatio; m_displayLock.Unlock(); return a; }
	double  GetMinSideHorizontalRatio() { m_displayLock.Lock(); double a = m_minSideHorizontalRatio; m_displayLock.Unlock(); return a; }
	double  GetMaxSideHorizontalRatio() { m_displayLock.Lock(); double a = m_maxSideHorizontalRatio; m_displayLock.Unlock(); return a; }

	// OTK - 13/01/2010 - ajout du nombre de pixels par ping
	unsigned int  GetPixelsPerPing() const { m_displayLock.Lock(); unsigned int a = m_pixelsPerPing; m_displayLock.Unlock(); return a; }
	void          SetPixelsPerPing(unsigned int nbPixels);

	// NMD - 07/12/2017 - ajout du mode de repr�sentation des pings
	Render2DMode GetRender2DMode() const { m_displayLock.Lock(); Render2DMode val = m_render2DMode; m_displayLock.Unlock(); return val;}
	void SetRender2DMode(Render2DMode val);

	// MOVIESDDD-200 - Coefficient d'affichage de la deviation
	double GetDeviationCoefficient() const { m_displayLock.Lock(); double val = m_deviationCoefficient; m_displayLock.Unlock(); return val; }
	void SetDeviationCoefficient(double val);

	// ACN - 27/10/2015 - ajout parametre display echos
	bool GetDisplayEchos() const { return m_bDisplayEchos; };
	void SetDisplayEchos(bool val) { m_bDisplayEchos = val; };

	// ACN - 27/10/2015 - ajout parametre display volume sampled
	bool GetDisplayVolumeSampled() const { return m_bDisplayVolumeSampled; };
	void SetDisplayVolumeSampled(bool val) { m_bDisplayVolumeSampled = val; };

	bool UseGlyphCube() const { return m_bUseGlyphCube; }
	void SetUseGlyphCube(bool val) { m_bUseGlyphCube = val; }

	// FRE - 24/08/2009 - ajout parametre visualisation bancs 2D
	bool GetDisplay2DShoals() const { return m_bDisplay2DShoals; };
	void SetDisplay2DShoals(bool val) { m_bDisplay2DShoals = val; };

	// FRE - 23/09/2009 - ajout parametre visualisation echo tooltip
	bool GetDisplayEchoTooltip() const { return m_bEchoTooltip; };
	void SetDisplayEchoTooltip(bool val) { m_bEchoTooltip = val; };
	
	unsigned int GetEchogramBackgroundColor() const { return m_echogramBackgroundColor; }
	void SetEchogramBackgroundColor(unsigned int color);

	double GetEchoDisplaySize() const { return m_echoDisplaySize; }
	void SetEchoDisplaySize(double val) { m_echoDisplaySize = val; }
	
	double GetVolumicBackgroundRed() const { return m_volumicBackgroundRed; }
	double GetVolumicBackgroundGreen() const { return m_volumicBackgroundGreen; }
	double GetVolumicBackgroundBlue() const { return m_volumicBackgroundBlue; }
	void SetVolumicBackgroundColor(double red, double green, double blue);

	bool GetVolumicOpacity() const { return m_bOpacity; }
	void SetVolumicOpacity(bool val) { m_bOpacity = val; }

	double GetVolumicOpacityFactor() const { return m_OpacityFactor; }
	void SetVolumicOpacityFactor(double val) { m_OpacityFactor = val; }

	PenParameters GetEsuGridPen() const;
	void SetEsuGridPen(PenParameters pen);

	PenParameters GetBottomPen() const;
	void SetBottomPen(PenParameters pen);

	bool GetNoiseDisplayEnabled() const { return m_noiseDisplayEnable; }
	void SetNoiseDisplayEnabled(bool val) { m_noiseDisplayEnable = val; }

	unsigned int GetNoiseColor() const;
	void SetNoiseColor(unsigned int color);

	bool GetRefNoiseDisplayEnabled() const { return m_refNoiseDisplayEnable; }
	void SetRefNoiseDisplayEnabled(bool val) { m_refNoiseDisplayEnable = val; }
	unsigned int GetRefNoiseColor() const { return m_refNoiseColor; }
	void SetRefNoiseColor(unsigned int value);

	bool GetDisplay2DSingleEchoes() const;
	void SetDisplay2DSingleEchoes(bool val);

	TSDisplayType GetTSDisplayType() const { return m_tsDisplayType; }
	void SetTSDisplayType(TSDisplayType d);

	std::string GetDetectedBottomViewTransducerName() const { return m_detectedBottomTransducerName; }
	void SetDetectedBottomViewTransducerName(const std::string& name) { m_detectedBottomTransducerName = name; }

	bool GetNoiseLevelLogScale() const { return m_noiseLevelLogScale; }
	void SetNoiseLevelLogScale(bool value) { m_noiseLevelLogScale = value; }

	bool GetIsLinkSounderEnabled() const { return m_isLinkSounderEnabled; }
	void SetIsLinkSounderEnabled(bool value) { m_isLinkSounderEnabled = value; }

	bool GetIsBeamVolumeDisplayed() const { return m_isBeamVolumeDisplayed; }
	void SetIsBeamVolumeDisplayed(bool value) { m_isBeamVolumeDisplayed = value; }

	bool GetIsGradiationsDisplayed() const { return m_isGraduationsDisplayed; }
	void SetIsGraduationsDisplayed(bool value) { m_isGraduationsDisplayed = value; }

	bool GetIsLabelsDisplayed() const { return m_isLabelsDisplayed; }
	void SetIsLabelsDisplayed(bool value) { m_isLabelsDisplayed = value; }

	bool GetIsSeaFloorEnabled() const { return m_isSeaFloorEnabled; }
	void SetIsSeaFloorEnabled(bool value) { m_isSeaFloorEnabled = value; }

	int GetSeaFloorScale() const { return m_seaFloorScale; }
	void SetSeaFloorScale(int value) { m_seaFloorScale = value; }

	double GetSingleEchoDisplaySize() const { return m_singleEchoDisplaySize; }
	void SetSingleEchoDisplaySize(double value) { m_singleEchoDisplaySize = value; }

	bool GetIsSingleEchosDisplayed() const { return m_isSingleEchosDisplayed; }
	void SetIsSingleEchosDisplayed(bool value) { m_isSingleEchosDisplayed = value; }

	bool GetIsShoalVolumesDisplayed() const { return m_isShoalVolumesDisplayed; }
	void SetIsShoalVolumesDisplayed(bool value) { m_isShoalVolumesDisplayed = value; }

	bool GetIsShoalVolumesSmoothed() const { return m_isShoalVoumesSmoothed; }
	void SetIsShoalVolumesSmoothed(bool value) { m_isShoalVoumesSmoothed = value; }

	bool GetIsShoalLabelsDisplayed() const { return m_isShoalLabelsDisplayed; }
	void SetIsShoalLabelsDisplayed(bool value) { m_isShoalLabelsDisplayed = value; }

	short GetShoalDisplayType() const { return m_shoalDisplayType; }
	void SetShoalDisplayType(short value) { m_shoalDisplayType = value; }

	int GetVerticalScale() const { return m_verticalScale; }
	void SetVerticalScale(int value) { m_verticalScale = value; }

	double GetAzimuth() const { return m_azimuth; }
	void SetAzimuth(double value) { m_azimuth = value; }

	double GetElevation() const { return m_elevation; }
	void SetElevation(double value) { m_elevation = value; }

	double GetZoomFactor() const { return m_zoomFactor; }
	void SetZoomFactor(double value) { m_zoomFactor = value; }

	bool GetResetCamera() const { return m_resetCamera; }
	void SetResetCamera(bool value) { m_resetCamera = value; }

	bool GetUseAutoReset() const { return m_useAutoReset; }
	void SetUseAutoReset(bool value) { m_useAutoReset = value; }

private:
	// remet � jour les donn�es des constructeur
	void Reset();

	// recalcul les palettes en fonction des param�tres d'affichage
	void RecomputePalette();
	static DisplayParameter* m_pDisplayParameter;

	// constructeurs destructeurs
	DisplayParameter();
	~DisplayParameter();

	DataFmt	m_minThreshold;
	DataFmt	m_maxThreshold;

	DataFmt m_angleThreshold;

	double m_minDepth;
	double m_maxDepth;

	// Zoom en cours ou non (param�tre non s�rialis�)
	bool m_isZoomActivated;

	/// Profondeur minimum courante (param�tre non s�rialis�)
	double m_currentMinDepth;

	/// Profondeur maximum courante (param�tre non s�rialis�)
	double m_currentMaxDepth;

	// OTK - 10/06/2009 - parametres "cach�s" qui correspondent au zoom.
	double m_minFrontHorizontalRatio;
	double m_maxFrontHorizontalRatio;
	double m_minSideHorizontalRatio;
	double m_maxSideHorizontalRatio;

	bool m_bUseGlyphCube;
	double m_Sampling;
	bool m_bOverwriteScalarWithChannelId;
	bool m_bIsDirty;
	bool m_bNewRange;

	/// the number of ping fan to drop
	unsigned int m_pingFanDelayed;

	/// the number of ping fan we want to see
	unsigned int m_PingFanLength;

	// Type de palette utilis�e pour les echos
	ColorPaletteEchoType m_colorPaletteEchoType;
	
	// Palette 2D
	DataFmt	m_minPaletteThreshold;
	DataFmt	m_maxPaletteThreshold;	
	IColorPalette * m_current2DEchoPalette;

	// Palette manuelle
	ManualColorPalette  m_manualColorPalette;	

	// Palette parula
	ParulaColorPalette m_parulaColorPalette;

	// Palette redblue
	RedBlueColorPalette m_redblueColorPalette;

	// Palette TS
	TSColorPalette m_tsColorPalette;

	// OTK - 20/03/2009 - on stocke 2 palettes de couleurs : une pour le mode par channel
	// et une pour le mode par �cho.
	ColorPaletteChannel m_pColorPaletteChannel;
	ColorPaletteEIClass m_pColorPaletteEIClass;
	
	// Couleur du fond de l'�chogramme
	ColorParameters m_echogramBackgroundColor;

	// OTK - 26/03/2009 - ajout parametres des grilles (profondeur et distance)
	bool m_distanceGrid;
	double m_distanceSampling;
	bool m_depthGrid;
	double m_depthSampling;

	bool m_bStrech;

	// ACN - 27/10/2015 - ajout parametre display echo
	bool m_bDisplayEchos;

	// ACN - 27/10/2015 - ajout parametre display volume sampled
	bool m_bDisplayVolumeSampled;

	// FRE - 24/08/2009 - ajout parametre visualisation bancs 2D
	bool m_bDisplay2DShoals;

	// Affichage single targets sur �chogrammes 2D
	bool m_bDisplay2DSingleEchoes;

	// FRE - 23/09/2009 - ajout parametre visualisation echo tooltip
	bool m_bEchoTooltip;

	// utilsiation des coordonn�es GPS pour l'affichage 3D
	bool m_bUseGPSPositionning;

	// nombre de pixels par ping pour la vue de profil
	unsigned int m_pixelsPerPing;
	
	// Mode d'affichage des �chos
	Render2DMode m_render2DMode;

	// FAE 195 - taille des �chos, couleur de fond et opacit�
	double m_echoDisplaySize;
	double m_volumicBackgroundRed;
	double m_volumicBackgroundGreen;
	double m_volumicBackgroundBlue;
	bool   m_bOpacity;
	double m_OpacityFactor;

	// Pinceau pour le dessin de la grille des ESUs
	PenParameters m_esuGridPen;

	// Pinceau pour la ligne de fond
	PenParameters m_bottomPen;

	// Couleur du niveau de bruit
	bool m_noiseDisplayEnable;
	ColorParameters m_noiseColor;

	// Couleur du niveau de bruit de r�f�rence
	bool m_refNoiseDisplayEnable;
	ColorParameters m_refNoiseColor;
	
	// MOVIESDDD-200 - coefficient d'affichage de la deviation
	double m_deviationCoefficient;

	//tsDisplayType
	TSDisplayType m_tsDisplayType;

	std::string m_detectedBottomTransducerName;

	bool m_noiseLevelLogScale;

	bool m_isLinkSounderEnabled;
	bool m_isBeamVolumeDisplayed;
	bool m_isGraduationsDisplayed;
	bool m_isLabelsDisplayed;
	bool m_isSeaFloorEnabled;
	int m_seaFloorScale;
	double m_singleEchoDisplaySize;
	bool m_isSingleEchosDisplayed;
	bool m_isShoalVolumesDisplayed;
	bool m_isShoalVoumesSmoothed;
	bool m_isShoalLabelsDisplayed;
	short m_shoalDisplayType;
	int m_verticalScale;

	// Param�tres cam�ra
	double m_azimuth;
	double m_elevation;
	double m_zoomFactor;
	bool m_resetCamera;
	bool m_useAutoReset;
};
