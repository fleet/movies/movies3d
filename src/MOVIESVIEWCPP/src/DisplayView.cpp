#include "DisplayView.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "utils.h"

#include "MoviesException.h"

using namespace MOVIESVIEWCPP;

namespace
{
	constexpr const char * LoggerName = "MOVIESVIEWCPP.View.DisplayView";
}

DisplayView::DisplayView(UpdateToolBarDelegate^ updateToolBarDelegate,
	IViewSelectionObserver^ selectionMgr,
	REFRESH3D_CALLBACK refreshCB)
{
	m_isPhasesEnabled = false;
	m_selectionMgr = selectionMgr;
	m_displayViewContainer = gcnew DisplayViewContainer;
	
	m_OffsetHistoricReference = 0;

	// Cr�ation de la vue 3D
	m_volumicViewDisplay = gcnew BaseVolumicView(m_displayViewContainer, updateToolBarDelegate, refreshCB);
 
	m_flatFrontViewArea = gcnew System::Windows::Forms::TableLayoutPanel();
	m_flatFrontViewArea->SuspendLayout();
	m_flatFrontViewArea->ColumnCount = 0;
	m_flatFrontViewArea->RowCount = 1;
	m_flatFrontViewArea->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
	m_flatFrontViewArea->ResumeLayout(false);
	m_flatFrontViewArea->PerformLayout();

	m_flatSideViewArea = gcnew System::Windows::Forms::TableLayoutPanel();
	m_flatSideViewArea->SuspendLayout();
	m_flatSideViewArea->ColumnCount = 1;
	m_flatSideViewArea->RowCount = 0;
	m_flatSideViewArea->ColumnStyles->Add(gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100));
	m_flatSideViewArea->ResumeLayout(false);
	m_flatSideViewArea->PerformLayout();

	// Cr�ations des vues 2D
	CreateFlatFrontView(true);
	CreateFlatSideView(true);
	
	m_flatFrontViewArea->Dock = System::Windows::Forms::DockStyle::Fill;
	m_flatSideViewArea->Dock = System::Windows::Forms::DockStyle::Fill;

	m_noiseLevelGraph = gcnew NoiseLevelGraphControl;
	DefaultFlatFrontView->AddUserControl(m_noiseLevelGraph);
}

DisplayView::~DisplayView()
{
	for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay) {
		delete view;
	}
	for each(BaseFlatSideView ^  view in m_flatSideViewsDisplay) {
		delete view;
	}
	delete m_volumicViewDisplay;
	delete m_detectedBottomView;
}

DisplayViewContainer ^ DisplayView::ViewContainer::get()
{
	return m_displayViewContainer;
}

MOVIESVIEWCPP::BaseVolumicView^ DisplayView::VolumicView::get()
{
	return m_volumicViewDisplay;
}

System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatSideView^ > % DisplayView::FlatSideViews::get()
{
	return m_flatSideViewsDisplay;
}

System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatFrontView^ > % DisplayView::FlatFrontViews::get()
{
	return m_flatFrontViewsDisplay;
}


System::Windows::Forms::TableLayoutPanel ^ DisplayView::FlatSideViewArea::get()
{
	return m_flatSideViewArea;
}

System::Windows::Forms::TableLayoutPanel ^ DisplayView::FlatFrontViewArea::get()
{
	return m_flatFrontViewArea;
}


MOVIESVIEWCPP::BaseFlatSideView ^ DisplayView::DefaultFlatSideView::get()
{
	return m_flatSideViewsDisplay[0];
}

MOVIESVIEWCPP::BaseFlatFrontView ^ DisplayView::DefaultFlatFrontView::get()
{
	return m_flatFrontViewsDisplay[0];
}

BaseFlatView ^ DisplayView::CreateFlatFrontView(bool attached)
{
	BaseFlatFrontView ^ view = gcnew BaseFlatFrontView();
	m_flatFrontViewsDisplay.Add(view);

	view->ViewManager = this;
	view->ViewContainer = m_displayViewContainer;
	view->DockFlatViewHandler = gcnew DockFlatViewDelegate(this, &DisplayView::DockFlatFrontView);
	view->CloseInternalFlatViewHandler = gcnew CloseInternalFlatViewDelegate(this, &DisplayView::CloseInternalFlatFrontView);
	view->CloseExternalFlatViewHandler = gcnew CloseExternalFlatViewDelegate(this, &DisplayView::CloseExternalFlatFrontView);
	view->CreateNewView = gcnew BaseFlatView::CreateNewViewHandler(this, &DisplayView::CreateFlatFrontView);
	view->SelectedTransducerChanged = gcnew BaseFlatView::SelectedTransducerChangedHandler(this, &DisplayView::OnFrontViewSelectedTransducerChanged);
	view->SetUpdateMaxDepthDelegate(m_updateMaxDepthDelegate);
	view->SetViewSelectionObserver(m_selectionMgr);
	view->Dock = System::Windows::Forms::DockStyle::Fill;

	if (attached)
	{
		int colIdx = m_flatFrontViewArea->ColumnCount;

		// add view to area
		m_flatFrontViewArea->SuspendLayout();
		m_flatFrontViewArea->ColumnCount += 1;
		m_flatFrontViewArea->ColumnStyles->Add(gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100));
		m_flatFrontViewArea->Controls->Add(view, colIdx, 0);
		m_flatFrontViewArea->ResumeLayout(false);
		m_flatFrontViewArea->PerformLayout();
	}

	view->Docked = attached;

	UpdateFlatFrontViewsButtons();

	return view;
}

System::Void DisplayView::DockFlatFrontView(bool dock, MOVIESVIEWCPP::ExternalViewWindow^ viewWindows)
{
	m_flatFrontViewArea->SuspendLayout();
	if (dock)
	{
		// add front view to table
		int colIdx = m_flatFrontViewArea->ColumnCount;
		m_flatFrontViewArea->ColumnCount += 1;
		m_flatFrontViewArea->ColumnStyles->Add(gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100));
		m_flatFrontViewArea->Controls->Add(viewWindows->DockableControl, colIdx, 0);
	}
	else
	{
		// remove front view form table
		auto cell = m_flatFrontViewArea->GetCellPosition(viewWindows->DockableControl);
		if (cell.Column != -1)
		{
			// shift view
			for (int col = cell.Column + 1; col < m_flatFrontViewArea->ColumnCount; ++col)
			{
				auto c = m_flatFrontViewArea->GetControlFromPosition(col, 0);
				m_flatFrontViewArea->SetColumn(c, col - 1);
			}

			// remove view
			m_flatFrontViewArea->Controls->Remove(viewWindows->DockableControl);
			m_flatFrontViewArea->ColumnStyles->RemoveAt(cell.Column);
			m_flatFrontViewArea->ColumnCount -= 1;
		}
	}
	m_flatFrontViewArea->ResumeLayout(false);
	m_flatFrontViewArea->PerformLayout();

	FlatViewDockStateChangedHandler();

	UpdateFlatFrontViewsButtons();
}

System::Void DisplayView::CloseInternalFlatFrontView(MOVIESVIEWCPP::BaseFlatView^ view)
{
	if (m_flatFrontViewsDisplay.Count > 1)
	{
		auto flatFrontView = dynamic_cast<MOVIESVIEWCPP::BaseFlatFrontView^>(view);
		
		// remove front view form table
		auto cell = m_flatFrontViewArea->GetCellPosition(view);
		if (cell.Column != -1)
		{
			m_flatFrontViewArea->SuspendLayout();
			
			// shift controls
			for (int col = cell.Column + 1; col < m_flatFrontViewArea->ColumnCount; ++col)
			{
				auto c = m_flatFrontViewArea->GetControlFromPosition(col, 0);
				m_flatFrontViewArea->SetColumn(c, col - 1);
			}

			// remove controls
			m_flatFrontViewArea->Controls->Remove(view);
			m_flatFrontViewArea->ColumnStyles->RemoveAt(cell.Column);
			m_flatFrontViewArea->ColumnCount -= 1;

			m_flatFrontViewArea->ResumeLayout(false);
			m_flatFrontViewArea->PerformLayout();
		}

		m_flatFrontViewsDisplay.Remove(flatFrontView);

		FlatViewDockStateChangedHandler();

		UpdateFlatFrontViewsButtons();
	}
}

System::Void DisplayView::CloseExternalFlatFrontView(MOVIESVIEWCPP::ExternalViewWindow^ viewWindows)
{
	if (m_flatFrontViewArea->ColumnCount == 0)
	{
		auto flatView = dynamic_cast<MOVIESVIEWCPP::BaseFlatView^>(viewWindows->DockableControl);
		if (flatView)
		{
			flatView->Docked = false;
		}
	}
	else
	{
		auto flatFrontView = dynamic_cast<MOVIESVIEWCPP::BaseFlatFrontView^>(viewWindows->DockableControl);
		m_flatFrontViewsDisplay.Remove(flatFrontView);
	}

	UpdateFlatFrontViewsButtons();
}

System::Void DisplayView::UpdateFlatFrontViewsButtons()
{
	// check if at least one front view is docked
	bool canDock = true;
	bool canClose = (m_flatFrontViewsDisplay.Count > 1);
	for (int i = 0; i < m_flatFrontViewsDisplay.Count; ++i)
	{
		if (m_flatFrontViewsDisplay[i]->Docked)
		{
			canDock = false;
			break;
		}
	}

	for (int i = 0; i < m_flatFrontViewsDisplay.Count; ++i)
	{
		m_flatFrontViewsDisplay[i]->CloseEnabled = false;
		if (!m_flatFrontViewsDisplay[i]->Docked)
		{
			m_flatFrontViewsDisplay[i]->DockEnabled = canDock;
		}
	}
}

BaseFlatView ^ DisplayView::CreateFlatSideView(bool attached)
{
	BaseFlatSideView ^ view = gcnew BaseFlatSideView();
	m_flatSideViewsDisplay.Add(view);

	view->ViewManager = this;
	view->ViewContainer = m_displayViewContainer;
	view->DockFlatViewHandler = gcnew DockFlatViewDelegate(this, &DisplayView::DockFlatSideView);
	view->CloseInternalFlatViewHandler = gcnew CloseInternalFlatViewDelegate(this, &DisplayView::CloseInternalFlatSideView);
	view->CloseExternalFlatViewHandler = gcnew CloseExternalFlatViewDelegate(this, &DisplayView::CloseExternalFlatSideView);
	view->CreateNewView = gcnew BaseFlatView::CreateNewViewHandler(this, &DisplayView::CreateFlatSideView);
	view->SelectedTransducerChanged = gcnew BaseFlatView::SelectedTransducerChangedHandler(this, &DisplayView::OnSideViewSelectedTransducerChanged);
	view->OffsetHistoricChange = gcnew BaseFlatSideView::OffsetHistoricChangeHandler(this, &DisplayView::OffsetHistoricChange);
	view->DisplayedPingOffsetChanged = gcnew BaseFlatSideView::DisplayedPingOffsetChangeHandler(this, &DisplayView::DisplayedPingOffsetChanged);
	view->SetUpdateMaxDepthDelegate(m_updateMaxDepthDelegate);

	view->SetViewSelectionObserver(m_selectionMgr);

	view->UpdateHistoricSCrollBar();
	view->SetOffsetHistoricReference(m_OffsetHistoricReference);
	view->Dock = System::Windows::Forms::DockStyle::Fill;

	if (attached)
	{
		int rowIdx = m_flatSideViewArea->RowCount;

		// add view to area
		m_flatSideViewArea->SuspendLayout();
		m_flatSideViewArea->RowCount += 1;
		m_flatSideViewArea->RowStyles->Add(gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100));
		m_flatSideViewArea->Controls->Add(view, 0, rowIdx);
		m_flatSideViewArea->ResumeLayout(false);
		m_flatSideViewArea->PerformLayout();
	}

	view->Docked = attached;

	UpdateFlatSideViewsButtons();

	return view;
}

System::Void DisplayView::DockFlatSideView(bool dock, MOVIESVIEWCPP::ExternalViewWindow^ viewWindows)
{
	m_flatSideViewArea->SuspendLayout();
	if (dock)
	{
		// add side view to table
		int rowIdx = m_flatSideViewArea->RowCount;
		m_flatSideViewArea->RowCount += 1;
		m_flatSideViewArea->RowStyles->Add(gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100));
		m_flatSideViewArea->Controls->Add(viewWindows->DockableControl, 0, rowIdx);
	}
	else
	{
		// remove side view form table
		auto cell = m_flatSideViewArea->GetCellPosition(viewWindows->DockableControl);
		if (cell.Row != -1)
		{
			// shift other controls
			for (int row = cell.Row + 1; row < m_flatSideViewArea->RowCount; ++row)
			{
				auto c = m_flatSideViewArea->GetControlFromPosition(0, row);
				m_flatSideViewArea->SetRow(c, row - 1);
			}

			// remove control
			m_flatSideViewArea->Controls->Remove(viewWindows->DockableControl);
			m_flatSideViewArea->RowStyles->RemoveAt(cell.Row);
			m_flatSideViewArea->RowCount -= 1;
		}
	}
	m_flatSideViewArea->ResumeLayout(false);
	m_flatSideViewArea->PerformLayout();

	FlatViewDockStateChangedHandler();

	UpdateFlatSideViewsButtons();
}

System::Void DisplayView::CloseInternalFlatSideView(MOVIESVIEWCPP::BaseFlatView^ view)
{
	if (m_flatSideViewsDisplay.Count > 1)
	{
		// remove side view form table
		m_flatSideViewArea->SuspendLayout();
		auto cell = m_flatSideViewArea->GetCellPosition(view);
		if (cell.Row != -1)
		{
			// shift other controls
			for (int row = cell.Row + 1; row < m_flatSideViewArea->RowCount; ++row)
			{
				auto c = m_flatSideViewArea->GetControlFromPosition(0, row);
				m_flatSideViewArea->SetRow(c, row - 1);
			}
			
			// remove control
			m_flatSideViewArea->Controls->Remove(view);
			m_flatSideViewArea->RowStyles->RemoveAt(cell.Row);
			m_flatSideViewArea->RowCount -= 1;
		}
		m_flatSideViewArea->ResumeLayout(false);
		m_flatSideViewArea->PerformLayout();

		auto faltSideView = dynamic_cast<MOVIESVIEWCPP::BaseFlatSideView^>(view);
		m_flatSideViewsDisplay.Remove(faltSideView);

		FlatViewDockStateChangedHandler();

		UpdateFlatSideViewsButtons();
	}
}

System::Void DisplayView::CloseExternalFlatSideView(MOVIESVIEWCPP::ExternalViewWindow^ viewWindows)
{
	if (m_flatSideViewArea->RowCount == 0)
	{
		auto flatView = dynamic_cast<MOVIESVIEWCPP::BaseFlatView^>(viewWindows->DockableControl);
		if (flatView)
		{
			flatView->Docked = true;
		}
	}
	else
	{
		auto faltSideView = dynamic_cast<MOVIESVIEWCPP::BaseFlatSideView^>(viewWindows->DockableControl);
		m_flatSideViewsDisplay.Remove(faltSideView);
	}

	UpdateFlatSideViewsButtons();
}

System::Void DisplayView::UpdateFlatSideViewsButtons()
{
	bool canClose = m_flatSideViewsDisplay.Count > 1;
	for (int i = 0; i < m_flatSideViewsDisplay.Count; ++i)
	{
		m_flatSideViewsDisplay[i]->SplitEnabled = m_flatSideViewsDisplay[i]->Docked;
		if (m_flatSideViewsDisplay[i]->Docked)
		{
			m_flatSideViewsDisplay[i]->CloseEnabled = canClose;
		}
		else
		{
			m_flatSideViewsDisplay[i]->CloseEnabled = false;
		}
	}
}

System::Void DisplayView::OnFrontViewSelectedTransducerChanged()
{
	const int nbView = m_displayViewContainer->FrontViewContainer->ViewCount();
	for (int iView = 0; iView < nbView; ++iView)
	{
		bool isActive = false;
		TransducerFlatFrontView* transducerView = m_displayViewContainer->FrontViewContainer->GetView(iView);
		for each (auto frontView in m_flatFrontViewsDisplay)
		{
			if (frontView->GetTransducerView() == transducerView)
			{
				isActive = true;
				break;
			}
		}
		transducerView->SetActive(isActive);
	}
}

System::Void DisplayView::OnSideViewSelectedTransducerChanged()
{
	const int nbView = m_displayViewContainer->SideViewContainer->ViewCount();
	for (int iView = 0; iView < nbView; ++iView)
	{
		bool isActive = false;
		TransducerFlatView* transducerView = m_displayViewContainer->SideViewContainer->GetView(iView);
		for each (auto sideView in m_flatSideViewsDisplay)
		{
			if (sideView->GetTransducerView() == transducerView)
			{
				isActive = true;
				break;
			}
		}
		transducerView->SetActive(isActive);
	}
}

void DisplayView::UpdateParameter()
{
	m_volumicViewDisplay->updateParameters();

	for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay) 
	{
		view->UpdateParameter();
	}

	for each(BaseFlatSideView ^  view in m_flatSideViewsDisplay) 
	{
		view->UpdateParameter();
	}
}

bool DisplayView::UpdatePrivateParameter()
{
	bool ret = false;
	if (m_volumicViewDisplay->GetViewFilter()->GetNeedToRegenerateVolume())
	{
		ret = true;
	}

	if (ret == false) {
		for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay) {
			if (view->m_NeedRedraw) {
				ret = true;
				break;
			}
		}
	}

	if (ret == false) {
		for each(BaseFlatSideView ^  view in m_flatSideViewsDisplay) {
			if (view->m_NeedRedraw) {
				ret = true;
				break;
			}
		}
	}

	// OTK - 19/05/2009 - si on a chang� le range d'affichage, on doit recalculer les tables
	// de transformation 2D pour ajuster la r�solution.
	if (DisplayParameter::getInstance()->rangeChanged())
	{
		// Recalcul des tables de transformation
		System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatView^ >	views;
		for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay) {
			views.Add(view);
		}

		for each(BaseFlatSideView ^  view in m_flatSideViewsDisplay)  {
			views.Add(view);
		}

		bool streched = DisplayParameter::getInstance()->GetStrech();
		while (views.Count > 0)
		{
			auto view = views[0];

			auto frontViews = SiblingFlatFrontViews(view);
			auto sideViews = SiblingFlatSideViews(view);

			int maxWidth = 0;
			int maxHeight = 0;
			for each(BaseFlatFrontView ^  view in frontViews)
			{
				maxWidth = Math::Max(maxWidth, view->GetFlatPictureBox()->Width);
				maxHeight = Math::Max(maxHeight, view->GetFlatPictureBox()->Height);
				views.Remove(view);
			}
			
			for each(BaseFlatSideView ^  view in sideViews)
			{
				maxHeight = Math::Max(maxHeight, view->GetFlatPictureBox()->Height);
				views.Remove(view);
			}

			for (int iView = 0; iView < m_displayViewContainer->FrontViewContainer->ViewCount(); ++iView)
			{
				TransducerFlatFrontView* refFront = m_displayViewContainer->FrontViewContainer->GetView(iView);
				if ((refFront->IsActive()) && (refFront->GetSounderId() == view->GetSounderId()) && (refFront->GetTransducerIndex() == view->GetTransducerIndex()))
				{
					refFront->UpdateTransformMap(maxWidth, maxHeight, streched);
				}
			}

			for (int iView = 0; iView < m_displayViewContainer->SideViewContainer->ViewCount(); ++iView)
			{
				TransducerFlatView* refSide = m_displayViewContainer->SideViewContainer->GetView(iView);
				if ((refSide->IsActive()) && (refSide->GetSounderId() == view->GetSounderId()) && (refSide->GetTransducerIndex() == view->GetTransducerIndex()))
				{
					refSide->UpdateTransformMap(maxWidth, maxHeight, streched);
				}
			}
		}

		
		ret = true;
	}
	return ret;
}

System::Void DisplayView::PingFanAdded()
{
	for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay) {
		view->PingFanAdded();
	}
	for each(BaseFlatSideView ^  view in m_flatSideViewsDisplay) {
		view->PingFanAdded();
	}
	m_volumicViewDisplay->PingFanAdded();
	m_noiseLevelGraph->refreshForCurrentESU();

	if (m_detectedBottomView)
		m_detectedBottomView->UpdateDetectedBottom();
}

System::Void DisplayView::EsuClosed()
{
	for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay) {
		view->EsuClosed();
	}
	for each(BaseFlatSideView ^  view in m_flatSideViewsDisplay) {
		view->EsuClosed();
	}
}

void DisplayView::SetPhasesEnabled(bool enabled)
{
	m_isPhasesEnabled = enabled;
}

bool DisplayView::IsPhasesEnabled()
{
	return m_isPhasesEnabled;
}

System::Void DisplayView::SounderChanged()
{
	m_displayViewContainer->FrontViewContainer->SounderChanged(m_isPhasesEnabled);
	m_displayViewContainer->SideViewContainer->SounderChanged(m_isPhasesEnabled);

	for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay)
	{
		view->SounderChanged();
	}

	for each(BaseFlatSideView ^  view in m_flatSideViewsDisplay)
	{
		view->SounderChanged();
	}

	m_volumicViewDisplay->SounderChanged();

	if (m_detectedBottomView)
	{
		m_detectedBottomView->SounderChanged();
	}
}

System::Void DisplayView::StreamClosed()
{
}

System::Void DisplayView::StreamOpened()
{
	m_volumicViewDisplay->StreamOpened();//m_pPingFanContainer->RemoveAllPingFan();
	for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay) {
		view->StreamOpened();
	}
	for each(BaseFlatSideView ^  view in m_flatSideViewsDisplay) {
		view->StreamOpened();
	}
}

void DisplayView::DrawFrontViews()
{
	for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay) {
		view->DrawNow();
	}
}

void DisplayView::DrawSideViews()
{
	for each(BaseFlatSideView ^  view in m_flatSideViewsDisplay) {
		view->DrawNow();
	}
}

void DisplayView::Draw()
{
	DrawFrontViews();
	DrawSideViews();
	this->m_volumicViewDisplay->Draw();
}

void DisplayView::RecomputeView()
{
	m_volumicViewDisplay->recomputeView();
}

void DisplayView::SounderHasChangedEvent()
{
}

void DisplayView::SetUpdateMaxDepthDelegate(BaseFlatView::UpdateMaxDepthDelegate ^ updateMaxDepthDelegate)
{
	m_updateMaxDepthDelegate = updateMaxDepthDelegate;
	for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay) {
		view->SetUpdateMaxDepthDelegate(m_updateMaxDepthDelegate);
	}
	for each(BaseFlatSideView ^  view in m_flatSideViewsDisplay) {
		view->SetUpdateMaxDepthDelegate(m_updateMaxDepthDelegate);
	}
}

void DisplayView::RedrawFlatSideViews()
{
	for each(BaseFlatSideView ^ view in m_flatSideViewsDisplay)
	{
		view->RedrawView();
	}
}

// FAE 175 - Synchronisation des ascenseurs horizontaux
void DisplayView::OffsetHistoricChange(int offsetHistoric)
{
	m_OffsetHistoricReference = offsetHistoric;
	for each(BaseFlatSideView ^ view in m_flatSideViewsDisplay)
	{
		view->SetOffsetHistoricReference(offsetHistoric);
	}
}

void DisplayView::DisplayedPingOffsetChanged(int sounderId, int offset)
{
	if (m_detectedBottomView) 
	{
		m_detectedBottomView->setPingOffset(offset);
	}
	
	if (m_noiseLevelGraph && DefaultFlatSideView->GetTransducerView())
	{
		m_noiseLevelGraph->setCursorOffset(DefaultFlatSideView->GetTransducerView()->GetSounderId(), offset);
	}

	// on deplacer le curseur pour toutes front les vues du m�me sondeur
	for (int iView = 0; iView < m_displayViewContainer->FrontViewContainer->ViewCount(); ++iView)
	{
		TransducerFlatFrontView * frontView = m_displayViewContainer->FrontViewContainer->GetView(iView);
		if (frontView->GetSounderId() == sounderId)
		{
			frontView->SetCursorOffset(offset);
		}
	}

	for each(BaseFlatFrontView ^  view in m_flatFrontViewsDisplay)
	{
		if (view->GetSounderId() == sounderId)
		{
			view->PingCursorOffsetChanged(offset);
		}
	}

	for each(BaseFlatSideView ^view in m_flatSideViewsDisplay)
	{
		if (view->GetSounderId() == sounderId)
		{
			view->m_OffsetCursor = offset;
		}
	}
}

void DisplayView::OnSaveEIView(String ^ transducer, std::uint32_t esuIdx, String ^outPath)
{
	int viewIdx = m_displayViewContainer->SideViewContainer->GetTransducerList()->IndexOf(transducer);
	if (viewIdx != -1)
	{
		TransducerFlatView * refView = m_displayViewContainer->SideViewContainer->GetView(viewIdx);
		try
		{
			BaseFlatSideView::SaveEIView(refView, transducer, esuIdx, outPath);
		}
		catch (MOVIESVIEWCPP::MoviesException ^e)
		{
			System::String ^ msg = "Movies Exception occured : " + e->Message;
			std::string stdMsg = netStr2CppStr(msg);
			M3D_LOG_ERROR(LoggerName, stdMsg.c_str());
		}
		catch (...)
		{
			M3D_LOG_ERROR(LoggerName, "An unknown error occured while saving the graphical result of the EI supervised");
		}
	}
	else
	{
		System::String ^ msg = "An error occured while saving the graphical result of the EI supervised : tranduscer '"
			+ transducer + "' not found";
		std::string stdMsg = netStr2CppStr(msg);
		M3D_LOG_ERROR(LoggerName, stdMsg.c_str());
	}
}

void DisplayView::showDetectedBottomView()
{
	if (!m_detectedBottomView || m_detectedBottomView->IsDisposed)
	{
		delete m_detectedBottomView;
		m_detectedBottomView = gcnew DetectedBottomView();
		m_detectedBottomView->SounderChanged();
		m_detectedBottomView->Show(m_volumicViewDisplay->TopLevelControl);
	}
}