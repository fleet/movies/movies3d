#pragma once
#include "DisplayViewContainer.h"

#include "BaseFlatFrontView.h"
#include "BaseFlatSideView.h"
#include "BaseVolumicView.h"
#include "IViewSelectionObserver.h"
#include "DetectedBottomView.h"
#include "NoiseLevelGraphControl.h"
#include "ExternalViewWindow.h"


class PingFan;

using namespace System::Windows::Forms;

public ref class DisplayView
{
public:
	DisplayView(
		MOVIESVIEWCPP::UpdateToolBarDelegate^ updateToolBarDelegate,
		IViewSelectionObserver^ selectionMgr,
		REFRESH3D_CALLBACK refreshCB);

	~DisplayView();

	property DisplayViewContainer ^ ViewContainer
	{
		DisplayViewContainer ^ get();
	}
	
	property MOVIESVIEWCPP::BaseVolumicView^ VolumicView
	{
		MOVIESVIEWCPP::BaseVolumicView ^ get();
	}
	
	property System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatSideView^ > % FlatSideViews
	{
		System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatSideView^ > % get();
	}

	property System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatFrontView^ > % FlatFrontViews
	{
		System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatFrontView^ > % get();
	}

	property MOVIESVIEWCPP::BaseFlatFrontView ^DockedFlatFrontView
	{
		MOVIESVIEWCPP::BaseFlatFrontView ^ get()
		{
			for each (auto view in m_flatFrontViewsDisplay)
			{
				if (view->Docked)
				{
					return view;
				}
			}
			return nullptr;
		}
	}
	
	System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatView^ > ^ SiblingFlatViews(MOVIESVIEWCPP::BaseFlatView^ view)
	{
		auto views = gcnew System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatView^ >();
		
		int sounderId = view->GetSounderId();
		unsigned int transducerIndex = view->GetTransducerIndex();
		
		for each (auto flatView in m_flatFrontViewsDisplay)
		{
			if (flatView->GetSounderId() == sounderId && flatView->GetTransducerIndex() == transducerIndex)
			{
				views->Add(flatView);
			}
		}

		for each (auto sideView in m_flatSideViewsDisplay)
		{
			if (sideView->GetSounderId() == sounderId && sideView->GetTransducerIndex() == transducerIndex)
			{
				views->Add(sideView);
			}
		}

		return views;
	}
	
	System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatFrontView^ > ^ SiblingFlatFrontViews(int sounderId, unsigned int transducerIndex)
	{
		auto views = gcnew System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatFrontView^ >();
		for each (auto view in m_flatFrontViewsDisplay)
		{
			if (view->GetSounderId() == sounderId && view->GetTransducerIndex() == transducerIndex)
			{
				views->Add(view);
			}
		}
		return views; 
	}

	System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatFrontView^ > ^ SiblingFlatFrontViews(MOVIESVIEWCPP::BaseFlatView^ view)
	{
		return SiblingFlatFrontViews(view->GetSounderId(), view->GetTransducerIndex());
	}

	System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatSideView^ > ^ SiblingFlatSideViews(int sounderId, unsigned int transducerIndex)
	{
		auto views = gcnew System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatSideView^ >();
		for each (auto view in m_flatSideViewsDisplay)
		{
			if (view->GetSounderId() == sounderId && view->GetTransducerIndex() == transducerIndex)
			{
				views->Add(view);
			}
		}
		return views; 
	}
	
	System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatSideView^ > ^ SiblingFlatSideViews(MOVIESVIEWCPP::BaseFlatView^ view)
	{
		return SiblingFlatSideViews(view->GetSounderId(), view->GetTransducerIndex());
	}

	property System::Windows::Forms::TableLayoutPanel ^ FlatSideViewArea
	{
		System::Windows::Forms::TableLayoutPanel ^ get();
	}

	property System::Windows::Forms::TableLayoutPanel ^ FlatFrontViewArea
	{
		System::Windows::Forms::TableLayoutPanel ^ get();
	}

	property MOVIESVIEWCPP::BaseFlatSideView ^ DefaultFlatSideView
	{
		MOVIESVIEWCPP::BaseFlatSideView ^ get();
	}

	property MOVIESVIEWCPP::BaseFlatFrontView ^ DefaultFlatFrontView
	{
		MOVIESVIEWCPP::BaseFlatFrontView ^ get();
	}

	MOVIESVIEWCPP::BaseFlatView ^ CreateFlatSideView(bool attached);
	MOVIESVIEWCPP::BaseFlatView ^ CreateFlatFrontView(bool attached);

	System::Void OnFrontViewSelectedTransducerChanged();
	System::Void OnSideViewSelectedTransducerChanged();
	
	void SetPhasesEnabled(bool enabled);
	bool IsPhasesEnabled();

	// Event called by dataAlgorithm note : called by the reading thread 
	/// you could not update Gui thing in these function
	System::Void PingFanAdded();
	System::Void EsuClosed();
	System::Void SounderChanged();
	System::Void SingleTSAdded() {};
	System::Void StreamClosed();
	System::Void StreamOpened();

	/// called to recompute and draw volume, called by the GUI thread 
	System::Void SounderHasChangedEvent();
	void UpdateParameter();
	bool UpdatePrivateParameter();
	void RecomputeView();

	void Draw();

	void DrawSideViews();
	void DrawFrontViews();

	void RedrawFlatSideViews();

	// Gestion des d�l�gu�s
	void SetUpdateMaxDepthDelegate(MOVIESVIEWCPP::BaseFlatView::UpdateMaxDepthDelegate ^ updateMaxDepthDelegate);

	// FAE 175 - Synchronisation des ascenseurs horizontaux
	void OffsetHistoricChange(int offsetHistoric);

	void DisplayedPingOffsetChanged(int sounderId, int offset);

	// Sauvegarde graphique de l'EI supervis?e dans un fichier
	void OnSaveEIView(String^transducer, std::uint32_t esuIdx, String ^outPath);

	void showDetectedBottomView();
	
	// Notification � la form principale qu'une vue 2D a �t� dock�e / d�dock�e
	public: delegate void FlatViewDockStateChangedDelegate();
	public: FlatViewDockStateChangedDelegate ^ FlatViewDockStateChangedHandler;


private:
	System::Void DockFlatFrontView(bool dock, MOVIESVIEWCPP::ExternalViewWindow^ viewWindows);
	System::Void CloseInternalFlatFrontView(MOVIESVIEWCPP::BaseFlatView^ view);
	System::Void CloseExternalFlatFrontView(MOVIESVIEWCPP::ExternalViewWindow^ viewWindows);
	System::Void UpdateFlatFrontViewsButtons();
	
	System::Void DockFlatSideView(bool dock, MOVIESVIEWCPP::ExternalViewWindow^ viewWindows);
	System::Void CloseInternalFlatSideView(MOVIESVIEWCPP::BaseFlatView^ view);
	System::Void CloseExternalFlatSideView(MOVIESVIEWCPP::ExternalViewWindow^ viewWindows);
	System::Void UpdateFlatSideViewsButtons();

	MOVIESVIEWCPP::BaseFlatView::UpdateMaxDepthDelegate ^ m_updateMaxDepthDelegate;

	IViewSelectionObserver ^ m_selectionMgr;

	DisplayViewContainer ^m_displayViewContainer;

	System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatSideView^ >	m_flatSideViewsDisplay;
	System::Collections::Generic::List< MOVIESVIEWCPP::BaseFlatFrontView^ > m_flatFrontViewsDisplay;
	MOVIESVIEWCPP::BaseVolumicView^											m_volumicViewDisplay;

	System::Windows::Forms::TableLayoutPanel^  m_flatSideViewArea;
	System::Windows::Forms::TableLayoutPanel^  m_flatFrontViewArea;

	MOVIESVIEWCPP::DetectedBottomView^ m_detectedBottomView = nullptr;
	MOVIESVIEWCPP::NoiseLevelGraphControl^ m_noiseLevelGraph = nullptr;

	int m_OffsetHistoricReference;

	bool m_isPhasesEnabled;
};
