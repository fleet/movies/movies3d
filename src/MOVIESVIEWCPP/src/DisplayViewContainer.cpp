#include "DisplayViewContainer.h"

DisplayViewContainer::DisplayViewContainer(void)
{
	m_frontViewContainer = gcnew TransducerContainerFlatFrontView();
	m_sideViewContainer = gcnew TransducerContainerFlatSideView();
}

TransducerContainerFlatFrontView ^ DisplayViewContainer::FrontViewContainer::get()
{
	return m_frontViewContainer;
}

TransducerContainerFlatSideView ^ DisplayViewContainer::SideViewContainer::get()
{
	return m_sideViewContainer;
}
