#pragma once
#include "TransducerContainerFlatView.h"
#include "TransducerFlatView.h"
#include "TransducerFlatFrontView.h"

public ref class DisplayViewContainer
{
public:
	DisplayViewContainer();

	property TransducerContainerFlatFrontView ^ FrontViewContainer
	{
		TransducerContainerFlatFrontView ^ get();
	}

	property TransducerContainerFlatSideView ^ SideViewContainer
	{
		TransducerContainerFlatSideView ^ get();
	}

private:

	TransducerContainerFlatFrontView ^m_frontViewContainer;

	TransducerContainerFlatSideView ^m_sideViewContainer;
};
