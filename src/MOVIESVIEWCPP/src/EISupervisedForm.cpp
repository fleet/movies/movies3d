#include "EISupervisedForm.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "ModuleManager/ModuleManager.h"
#include "EISupervised/EISupervisedModule.h"

// d�marrage / arr�t des traitements
#include "TreatmentStartModeSelector.h"
#include "TreatmentStopModeSelector.h"

#include "Compensation/CompensationModule.h"
#include "EchoIntegration/EchoIntegrationModule.h"


using namespace MOVIESVIEWCPP;

void EISupervisedForm::ConfigToIHM()
{
	// r�cup�ration du module
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	EISupervisedModule * pModule = pModuleMgr->GetEISupervisedModule();

	EISupervisedParameter eiSupervisedParameter = pModule->GetEISupervisedParameter();
	BaseKernel::ParameterBroadcastAndRecord broadcastAndRecordParameter = eiSupervisedParameter.GetParameterBroadcastAndRecord();

	m_listViewClasses->Items->Clear();

	// param�tres d'archivage
	textBoxDirectory->Text = gcnew System::String(broadcastAndRecordParameter.m_filePath.c_str());
	textBoxPrefix->Text = gcnew System::String(broadcastAndRecordParameter.m_filePrefix.c_str());
	m_checkBoxDatePrefix->Checked = broadcastAndRecordParameter.m_prefixDate;
	m_checkBoxAppendResults->Checked = eiSupervisedParameter.isAppendResults();

	// lecture des classifications
	for (const auto& classification : eiSupervisedParameter.getClassifications())
	{
		auto strName = gcnew String(classification.m_Name.c_str());
		auto item = gcnew ListViewItem(strName);

		auto strDescription = gcnew String(classification.m_Description.c_str());
		item->SubItems->Add(strDescription);

		m_listViewClasses->Items->Add(item);
	}

	// lecture des transducteurs
	for each(System::Windows::Forms::TreeNode ^ sounderNode in m_TransducersTreeView->getNodes())
	{
		for each(System::Windows::Forms::TreeNode ^ transNode in sounderNode->Nodes)
		{
			char * transducerName = (char *)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(transNode->Text).ToPointer();
			const auto& layerConf = eiSupervisedParameter.getTransducerLayerConf(transducerName);
			if (layerConf != nullptr)
			{
				transNode->Checked = true;
				transNode->Nodes[0]->Checked = layerConf->surface;
				transNode->Nodes[1]->Checked = layerConf->distance;
				transNode->Nodes[2]->Checked = layerConf->bottom;
			}
			else
			{
				transNode->Checked = false;
				transNode->Nodes[0]->Checked = false;
				transNode->Nodes[1]->Checked = false;
				transNode->Nodes[2]->Checked = false;
			}
			System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(transducerName));
		}
	}

	// Param�tres d'affichage
	m_checkBoxVolumeNormalization->Checked = eiSupervisedParameter.isUseVolumicWeightedAvg();

	UpdateModuleEnable(pModule->getEnable());
}

void EISupervisedForm::IHMToConfig()
{
	// r�cup�ration du module
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	EISupervisedModule * pModule = pModuleMgr->GetEISupervisedModule();

	EISupervisedParameter & eiSupervisedParameter = pModule->GetEISupervisedParameter();
	BaseKernel::ParameterBroadcastAndRecord & broadcastAndRecordParameter = eiSupervisedParameter.GetParameterBroadcastAndRecord();

	// param�tres d'archivage
	char * filePath = (char *)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBoxDirectory->Text).ToPointer();
	broadcastAndRecordParameter.m_filePath = filePath;
	System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(filePath));

	char * filePrefix = (char *)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBoxPrefix->Text).ToPointer();
	broadcastAndRecordParameter.m_filePrefix = filePrefix;
	System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(filePrefix));

	broadcastAndRecordParameter.m_prefixDate = m_checkBoxDatePrefix->Checked;
	eiSupervisedParameter.setAppendResults(m_checkBoxAppendResults->Checked);

	// effacement de la config pr�c�dente
	eiSupervisedParameter.clearClassifications();

	// recopie des param�tres
	for (int i = 0; i < m_listViewClasses->Items->Count; i++)
	{
		ListViewItem ^item = m_listViewClasses->Items[i];

		ClassificationDef classDef;
		classDef.m_Id = i;

		char * name = (char *)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(item->SubItems[0]->Text).ToPointer();
		classDef.m_Name = name;
		System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(name));

		char* description = (char *)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(item->SubItems[1]->Text).ToPointer();
		classDef.m_Description = description;
		System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(description));

		eiSupervisedParameter.addClassification(classDef);
	}

	// sauvegarde des transducteurs
	eiSupervisedParameter.clearTransducerLayerConf();

	for each(System::Windows::Forms::TreeNode ^ sounderNode in m_TransducersTreeView->getNodes())
	{
		for each(System::Windows::Forms::TreeNode ^ transNode in sounderNode->Nodes)
		{
			if (transNode->Checked == true)
			{
				char* transducerName = (char *)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(transNode->Text).ToPointer();

				TransducerLayerConf transducerLayerConf;
				transducerLayerConf.surface = transNode->Nodes[0]->Checked;
				transducerLayerConf.distance = transNode->Nodes[1]->Checked;
				transducerLayerConf.bottom = transNode->Nodes[2]->Checked;

				eiSupervisedParameter.addTransducerLayerConf(transducerName, transducerLayerConf);

				System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(transducerName));
			}
		}
	}

	// Param�tres d'affichage
	eiSupervisedParameter.setUseVolumicWeightedAvg(m_checkBoxVolumeNormalization->Checked);
}

void EISupervisedForm::UpdateModuleEnable(bool enable)
{
	m_listViewClasses->Enabled = !enable;
	m_buttonAddClass->Enabled = !enable;
	m_buttonRemoveClass->Enabled = !enable;

	m_buttonStart->Enabled = !enable;
	m_buttonStop->Enabled = enable;

	m_TransducersTreeView->Enabled = !enable;
}

System::Void EISupervisedForm::m_buttonRemoveClass_Click(System::Object^  sender, System::EventArgs^  e)
{
	// suppression des classes s�lectionn�es
	for (int i = m_listViewClasses->SelectedItems->Count - 1; i >= 0; i--)
	{
		m_listViewClasses->SelectedItems[i]->Remove();
	}
}

System::Void EISupervisedForm::m_buttonAddClass_Click(System::Object^  sender, System::EventArgs^  e)
{
	ListViewItem ^item = gcnew ListViewItem(s_CLASS_STR);
	item->SubItems->Add("");
	m_listViewClasses->Items->Add(item);

	// on selectionne la classe ajout�e
	m_listViewClasses->SelectedItems->Clear();
	item->Selected = true;
}

System::Void EISupervisedForm::m_buttonOK_Click(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		IHMToConfig();
		Close();
	}
	catch (Exception^)
	{
		MessageBox::Show("Invalid configuration.");
	}
}

System::Void EISupervisedForm::m_buttonCancel_Click(System::Object^  sender, System::EventArgs^  e)
{
	Close();
}

System::Void EISupervisedForm::m_buttonStart_Click(System::Object^  sender, System::EventArgs^  e)
{
	// Application de la configuration
	bool bOk = true;
	M3DKernel::GetInstance()->Lock();
	try
	{
		IHMToConfig();

		// r�cup�ration du module d'echoIntegration supervis�e
		CModuleManager * pModuleMgr = CModuleManager::getInstance();
		EISupervisedModule * pModule = pModuleMgr->GetEISupervisedModule();

		// V�rification qu'au moins un transducteur est s�lectionn�e 
		EISupervisedParameter & eiSupervisedParameter = pModule->GetEISupervisedParameter();
		if (eiSupervisedParameter.getTransducers().size() == 0)
		{
			bOk = false;
			MessageBox::Show("Select at least one transducer.");
		}
		else
		{
			// FAE 198 - test de l'existence du r�pertoire de sortie des fichiers si la sortie fichier est activ�e
			bool bFileOutputOk = true;
			if (eiSupervisedParameter.GetParameterBroadcastAndRecord().m_enableFileOutput)
			{
				// une cha�ne vide n'est pas un chemin valide (� modifier si on souhaite autoriser une �criture dans le dossier courant ?)
				if (eiSupervisedParameter.GetParameterBroadcastAndRecord().m_filePath.empty())
				{
					bFileOutputOk = false;
				}
				else
				{
					// test de l'existence du r�pertoire
					if (!System::IO::Directory::Exists(gcnew String(eiSupervisedParameter.GetParameterBroadcastAndRecord().m_filePath.c_str())))
					{
						bFileOutputOk = false;
					}
				}
			}

			if (!bFileOutputOk)
			{
				bOk = false;
				MessageBox::Show("The specified output path for result files does not exist.");
			}
			else
			{
				// affichage de la boite de dialogue permettant de choisir entre d�marrage imm�diat et diff�r�
				TreatmentStartModeSelector ^ref = gcnew TreatmentStartModeSelector(pModule, m_ReaderCtrlManaged);
				ref->ShowDialog();

				UpdateModuleEnable(pModule->getEnable());
			}
		}
	}
	catch (Exception^)
	{
		bOk = false;
		MessageBox::Show("Invalid configuration.");
	}

	M3DKernel::GetInstance()->Unlock();

	if (bOk)
	{
		Close();
	}
}

System::Void EISupervisedForm::m_buttonStop_Click(System::Object^  sender, System::EventArgs^  e)
{
	// r�cup�ration du module d'echoIntegration supervis�e
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	EISupervisedModule * pModule = pModuleMgr->GetEISupervisedModule();

	// affichage du dialog permettant de choisir entre arret imm�diat et diff�r�
	TreatmentStopModeSelector ^ref = gcnew TreatmentStopModeSelector(pModule);
	M3DKernel::GetInstance()->Lock();
	ref->ShowDialog();

	UpdateModuleEnable(pModule->getEnable());
	M3DKernel::GetInstance()->Unlock();

	// OTK - FAE 2056 - suppression de la fen�tre des r�sultats si elle existe
	EISupervisedStopped(this, System::EventArgs::Empty);
}

System::Void EISupervisedForm::m_listViewClasses_MouseDoubleClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	// on r�cup�re l'Item cliqu� avec l'index la colonne
	m_SelectedColumnIndex = GetSubItemAt(e->X, e->Y, m_SelectedItem);

	if (m_SelectedColumnIndex >= 0)
	{
		// En fonction de l'index de la colonne, on r�cupere le rectangle dans lequel faire apparaitre
		// le controle qui sert � l'�dition
		System::Drawing::Rectangle ClickedItem = m_SelectedItem->Bounds;
		for (int i = 0; i < m_SelectedColumnIndex; i++)
		{
			ClickedItem.X += this->m_listViewClasses->Columns[i]->Width;
		}

		ClickedItem.Width = this->m_listViewClasses->Columns[m_SelectedColumnIndex]->Width;

		// on prend en compte le positionnement relatif de la listview dans la fen�tre
		ClickedItem.Y += this->m_listViewClasses->Top;
		ClickedItem.X += this->m_listViewClasses->Left;

		m_textBox->Bounds = ClickedItem;
		m_textBox->Text = m_SelectedItem->SubItems[m_SelectedColumnIndex]->Text;
		m_textBox->Visible = true;
		m_textBox->BringToFront();
		m_textBox->Focus();
	}
}

System::Void EISupervisedForm::m_textBox_Leave(System::Object^  sender, System::EventArgs^  e)
{
	m_SelectedItem->SubItems[m_SelectedColumnIndex]->Text = m_textBox->Text;
	m_textBox->Visible = false;
}

System::Void EISupervisedForm::m_textBox_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e)
{
	if (e->KeyCode == Keys::Enter)
	{
		m_listViewClasses->Focus();
	}
	else if (e->KeyCode == System::Windows::Forms::Keys::Escape)
	{
		m_textBox->Text = m_SelectedItem->SubItems[m_SelectedColumnIndex]->Text;
		m_listViewClasses->Focus();
	}
}

System::Void EISupervisedForm::buttonBrowse_Click(System::Object^  sender, System::EventArgs^  e)
{
	FolderBrowserDialog ^folderBrowserDialog = gcnew FolderBrowserDialog();
	folderBrowserDialog->SelectedPath = textBoxDirectory->Text;
	if (folderBrowserDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		textBoxDirectory->Text = folderBrowserDialog->SelectedPath;
	}
}

int EISupervisedForm::GetSubItemAt(int x, int y, ListViewItem^ %item)
{
	item = this->m_listViewClasses->GetItemAt(x, y);

	if (item)
	{
		System::Drawing::Rectangle lviBounds;
		int	subItemX;

		lviBounds = item->GetBounds(ItemBoundsPortion::Entire);
		subItemX = lviBounds.Left;
		for (int i = 0; i < this->m_listViewClasses->Columns->Count; i++)
		{
			ColumnHeader ^h = this->m_listViewClasses->Columns[i];
			if (x < subItemX + h->Width)
			{
				return h->Index;
			}
			subItemX += h->Width;
		}
	}

	return -1;
}	