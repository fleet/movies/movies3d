#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "ReaderCtrlManaged.h"
#include "SounderTransducerTreeView.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de EISupervisedForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class EISupervisedForm : public System::Windows::Forms::Form
	{
	public:
		EISupervisedForm(void)
		{
			InitializeComponent();			

			ConfigToIHM();
		}

		event EventHandler ^ EISupervisedStopped;

		void SetReadCtrl(ReaderCtrlManaged^ %readerCtrl)
		{
			m_ReaderCtrlManaged = readerCtrl;
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~EISupervisedForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::Button^  m_buttonStart;
	private: System::Windows::Forms::Button^  m_buttonStop;
	private: System::Windows::Forms::Button^  m_buttonCancel;
	private: System::Windows::Forms::Button^  m_buttonOK;
	private: System::Windows::Forms::GroupBox^  m_groupBoxClass;
	private: System::Windows::Forms::Button^  m_buttonRemoveClass;
	private: System::Windows::Forms::Button^  m_buttonAddClass;
	private: System::Windows::Forms::ListView^  m_listViewClasses;
	private: System::Windows::Forms::TextBox^  m_textBox;
	private: System::Windows::Forms::ColumnHeader^  columnHeaderClass;
	private: System::Windows::Forms::ColumnHeader^  columnHeaderDescription;


	private: System::Windows::Forms::Label^  label1;














	SounderTransducerTreeView^  m_TransducersTreeView;
	private: System::Windows::Forms::CheckBox^  m_checkBoxAppendResults;
	private: System::Windows::Forms::Label^  labelPrefix;
	private: System::Windows::Forms::TextBox^  textBoxPrefix;
	private: System::Windows::Forms::Label^  labelDirectory;
	private: System::Windows::Forms::TextBox^  textBoxDirectory;
	private: System::Windows::Forms::Button^  buttonBrowse;
	private: System::Windows::Forms::CheckBox^  m_checkBoxDatePrefix;
	private: System::Windows::Forms::GroupBox^  groupBoxArchive;
	private: System::Windows::Forms::GroupBox^  m_groupBoxDisplay;
	private: System::Windows::Forms::CheckBox^  m_checkBoxVolumeNormalization;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;









	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->m_buttonStart = (gcnew System::Windows::Forms::Button());
			this->m_buttonStop = (gcnew System::Windows::Forms::Button());
			this->m_buttonCancel = (gcnew System::Windows::Forms::Button());
			this->m_buttonOK = (gcnew System::Windows::Forms::Button());
			this->m_groupBoxClass = (gcnew System::Windows::Forms::GroupBox());
			this->m_listViewClasses = (gcnew System::Windows::Forms::ListView());
			this->columnHeaderClass = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnHeaderDescription = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_buttonRemoveClass = (gcnew System::Windows::Forms::Button());
			this->m_buttonAddClass = (gcnew System::Windows::Forms::Button());
			this->m_textBox = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->m_TransducersTreeView = (gcnew SounderTransducerTreeView(true));
			this->m_checkBoxAppendResults = (gcnew System::Windows::Forms::CheckBox());
			this->labelPrefix = (gcnew System::Windows::Forms::Label());
			this->textBoxPrefix = (gcnew System::Windows::Forms::TextBox());
			this->labelDirectory = (gcnew System::Windows::Forms::Label());
			this->textBoxDirectory = (gcnew System::Windows::Forms::TextBox());
			this->buttonBrowse = (gcnew System::Windows::Forms::Button());
			this->m_checkBoxDatePrefix = (gcnew System::Windows::Forms::CheckBox());
			this->groupBoxArchive = (gcnew System::Windows::Forms::GroupBox());
			this->m_groupBoxDisplay = (gcnew System::Windows::Forms::GroupBox());
			this->m_checkBoxVolumeNormalization = (gcnew System::Windows::Forms::CheckBox());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->m_groupBoxClass->SuspendLayout();
			this->groupBoxArchive->SuspendLayout();
			this->m_groupBoxDisplay->SuspendLayout();
			this->tableLayoutPanel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// m_buttonStart
			// 
			this->m_buttonStart->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->m_buttonStart->BackColor = System::Drawing::Color::PaleGreen;
			this->m_buttonStart->Location = System::Drawing::Point(12, 618);
			this->m_buttonStart->Name = L"m_buttonStart";
			this->m_buttonStart->Size = System::Drawing::Size(75, 23);
			this->m_buttonStart->TabIndex = 1;
			this->m_buttonStart->Text = L"Start";
			this->m_buttonStart->UseVisualStyleBackColor = false;
			this->m_buttonStart->Click += gcnew System::EventHandler(this, &EISupervisedForm::m_buttonStart_Click);
			// 
			// m_buttonStop
			// 
			this->m_buttonStop->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->m_buttonStop->BackColor = System::Drawing::Color::Tomato;
			this->m_buttonStop->Location = System::Drawing::Point(93, 618);
			this->m_buttonStop->Name = L"m_buttonStop";
			this->m_buttonStop->Size = System::Drawing::Size(75, 23);
			this->m_buttonStop->TabIndex = 2;
			this->m_buttonStop->Text = L"Stop";
			this->m_buttonStop->UseVisualStyleBackColor = false;
			this->m_buttonStop->Click += gcnew System::EventHandler(this, &EISupervisedForm::m_buttonStop_Click);
			// 
			// m_buttonCancel
			// 
			this->m_buttonCancel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->m_buttonCancel->Location = System::Drawing::Point(515, 618);
			this->m_buttonCancel->Name = L"m_buttonCancel";
			this->m_buttonCancel->Size = System::Drawing::Size(75, 23);
			this->m_buttonCancel->TabIndex = 3;
			this->m_buttonCancel->Text = L"Cancel";
			this->m_buttonCancel->UseVisualStyleBackColor = true;
			this->m_buttonCancel->Click += gcnew System::EventHandler(this, &EISupervisedForm::m_buttonCancel_Click);
			// 
			// m_buttonOK
			// 
			this->m_buttonOK->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->m_buttonOK->Location = System::Drawing::Point(434, 618);
			this->m_buttonOK->Name = L"m_buttonOK";
			this->m_buttonOK->Size = System::Drawing::Size(75, 23);
			this->m_buttonOK->TabIndex = 4;
			this->m_buttonOK->Text = L"OK";
			this->m_buttonOK->UseVisualStyleBackColor = true;
			this->m_buttonOK->Click += gcnew System::EventHandler(this, &EISupervisedForm::m_buttonOK_Click);
			// 
			// m_groupBoxClass
			// 
			this->m_groupBoxClass->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->m_groupBoxClass->Controls->Add(this->m_listViewClasses);
			this->m_groupBoxClass->Controls->Add(this->m_buttonRemoveClass);
			this->m_groupBoxClass->Controls->Add(this->m_buttonAddClass);
			this->m_groupBoxClass->Controls->Add(this->m_textBox);
			this->m_groupBoxClass->Location = System::Drawing::Point(3, 246);
			this->m_groupBoxClass->Name = L"m_groupBoxClass";
			this->m_groupBoxClass->Size = System::Drawing::Size(572, 165);
			this->m_groupBoxClass->TabIndex = 5;
			this->m_groupBoxClass->TabStop = false;
			this->m_groupBoxClass->Text = L"Classes";
			// 
			// m_listViewClasses
			// 
			this->m_listViewClasses->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->m_listViewClasses->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(2) {
				this->columnHeaderClass,
					this->columnHeaderDescription
			});
			this->m_listViewClasses->FullRowSelect = true;
			this->m_listViewClasses->HeaderStyle = System::Windows::Forms::ColumnHeaderStyle::Nonclickable;
			this->m_listViewClasses->HideSelection = false;
			this->m_listViewClasses->Location = System::Drawing::Point(7, 20);
			this->m_listViewClasses->Name = L"m_listViewClasses";
			this->m_listViewClasses->Size = System::Drawing::Size(559, 110);
			this->m_listViewClasses->TabIndex = 3;
			this->m_listViewClasses->UseCompatibleStateImageBehavior = false;
			this->m_listViewClasses->View = System::Windows::Forms::View::Details;
			this->m_listViewClasses->MouseDoubleClick += gcnew System::Windows::Forms::MouseEventHandler(this, &EISupervisedForm::m_listViewClasses_MouseDoubleClick);
			// 
			// columnHeaderClass
			// 
			this->columnHeaderClass->Text = L"Class";
			// 
			// columnHeaderDescription
			// 
			this->columnHeaderDescription->Text = L"Description";
			this->columnHeaderDescription->Width = 280;
			// 
			// m_buttonRemoveClass
			// 
			this->m_buttonRemoveClass->Anchor = System::Windows::Forms::AnchorStyles::Bottom;
			this->m_buttonRemoveClass->Location = System::Drawing::Point(292, 136);
			this->m_buttonRemoveClass->Name = L"m_buttonRemoveClass";
			this->m_buttonRemoveClass->Size = System::Drawing::Size(97, 23);
			this->m_buttonRemoveClass->TabIndex = 2;
			this->m_buttonRemoveClass->Text = L"Remove Class";
			this->m_buttonRemoveClass->UseVisualStyleBackColor = true;
			this->m_buttonRemoveClass->Click += gcnew System::EventHandler(this, &EISupervisedForm::m_buttonRemoveClass_Click);
			// 
			// m_buttonAddClass
			// 
			this->m_buttonAddClass->Anchor = System::Windows::Forms::AnchorStyles::Bottom;
			this->m_buttonAddClass->Location = System::Drawing::Point(189, 136);
			this->m_buttonAddClass->Name = L"m_buttonAddClass";
			this->m_buttonAddClass->Size = System::Drawing::Size(97, 23);
			this->m_buttonAddClass->TabIndex = 1;
			this->m_buttonAddClass->Text = L"Add Class";
			this->m_buttonAddClass->UseVisualStyleBackColor = true;
			this->m_buttonAddClass->Click += gcnew System::EventHandler(this, &EISupervisedForm::m_buttonAddClass_Click);
			// 
			// m_textBox
			// 
			this->m_textBox->Location = System::Drawing::Point(101, 187);
			this->m_textBox->Name = L"m_textBox";
			this->m_textBox->Size = System::Drawing::Size(97, 20);
			this->m_textBox->TabIndex = 14;
			this->m_textBox->Visible = false;
			this->m_textBox->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &EISupervisedForm::m_textBox_KeyDown);
			this->m_textBox->Leave += gcnew System::EventHandler(this, &EISupervisedForm::m_textBox_Leave);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(3, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(110, 13);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Reference transducer";
			// 
			// m_TransducersTreeView
			// 
			this->m_TransducersTreeView->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->m_TransducersTreeView->Location = System::Drawing::Point(3, 16);
			this->m_TransducersTreeView->Name = L"m_TransducersTreeView";
			this->m_TransducersTreeView->Size = System::Drawing::Size(572, 224);
			this->m_TransducersTreeView->TabIndex = 15;
			// 
			// m_checkBoxAppendResults
			// 
			this->m_checkBoxAppendResults->AutoSize = true;
			this->m_checkBoxAppendResults->Location = System::Drawing::Point(9, 19);
			this->m_checkBoxAppendResults->Name = L"m_checkBoxAppendResults";
			this->m_checkBoxAppendResults->Size = System::Drawing::Size(101, 17);
			this->m_checkBoxAppendResults->TabIndex = 1;
			this->m_checkBoxAppendResults->Text = L"Append Results";
			this->m_checkBoxAppendResults->UseVisualStyleBackColor = true;
			// 
			// labelPrefix
			// 
			this->labelPrefix->AutoSize = true;
			this->labelPrefix->Location = System::Drawing::Point(6, 74);
			this->labelPrefix->Name = L"labelPrefix";
			this->labelPrefix->Size = System::Drawing::Size(33, 13);
			this->labelPrefix->TabIndex = 11;
			this->labelPrefix->Text = L"Prefix";
			// 
			// textBoxPrefix
			// 
			this->textBoxPrefix->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBoxPrefix->Location = System::Drawing::Point(81, 67);
			this->textBoxPrefix->Name = L"textBoxPrefix";
			this->textBoxPrefix->Size = System::Drawing::Size(455, 20);
			this->textBoxPrefix->TabIndex = 12;
			// 
			// labelDirectory
			// 
			this->labelDirectory->AutoSize = true;
			this->labelDirectory->Location = System::Drawing::Point(6, 99);
			this->labelDirectory->Name = L"labelDirectory";
			this->labelDirectory->Size = System::Drawing::Size(49, 13);
			this->labelDirectory->TabIndex = 13;
			this->labelDirectory->Text = L"Directory";
			// 
			// textBoxDirectory
			// 
			this->textBoxDirectory->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBoxDirectory->Location = System::Drawing::Point(81, 93);
			this->textBoxDirectory->Name = L"textBoxDirectory";
			this->textBoxDirectory->Size = System::Drawing::Size(455, 20);
			this->textBoxDirectory->TabIndex = 14;
			// 
			// buttonBrowse
			// 
			this->buttonBrowse->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->buttonBrowse->Location = System::Drawing::Point(542, 93);
			this->buttonBrowse->Name = L"buttonBrowse";
			this->buttonBrowse->Size = System::Drawing::Size(24, 19);
			this->buttonBrowse->TabIndex = 15;
			this->buttonBrowse->Text = L"...";
			this->buttonBrowse->UseVisualStyleBackColor = true;
			this->buttonBrowse->Click += gcnew System::EventHandler(this, &EISupervisedForm::buttonBrowse_Click);
			// 
			// m_checkBoxDatePrefix
			// 
			this->m_checkBoxDatePrefix->AutoSize = true;
			this->m_checkBoxDatePrefix->Location = System::Drawing::Point(9, 42);
			this->m_checkBoxDatePrefix->Name = L"m_checkBoxDatePrefix";
			this->m_checkBoxDatePrefix->Size = System::Drawing::Size(174, 17);
			this->m_checkBoxDatePrefix->TabIndex = 16;
			this->m_checkBoxDatePrefix->Text = L"Prefix files with current datetime";
			this->m_checkBoxDatePrefix->UseVisualStyleBackColor = true;
			// 
			// groupBoxArchive
			// 
			this->groupBoxArchive->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxArchive->Controls->Add(this->m_checkBoxDatePrefix);
			this->groupBoxArchive->Controls->Add(this->buttonBrowse);
			this->groupBoxArchive->Controls->Add(this->textBoxDirectory);
			this->groupBoxArchive->Controls->Add(this->labelDirectory);
			this->groupBoxArchive->Controls->Add(this->textBoxPrefix);
			this->groupBoxArchive->Controls->Add(this->labelPrefix);
			this->groupBoxArchive->Controls->Add(this->m_checkBoxAppendResults);
			this->groupBoxArchive->Location = System::Drawing::Point(3, 470);
			this->groupBoxArchive->Name = L"groupBoxArchive";
			this->groupBoxArchive->Size = System::Drawing::Size(572, 127);
			this->groupBoxArchive->TabIndex = 9;
			this->groupBoxArchive->TabStop = false;
			this->groupBoxArchive->Text = L"Archive";
			// 
			// m_groupBoxDisplay
			// 
			this->m_groupBoxDisplay->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->m_groupBoxDisplay->Controls->Add(this->m_checkBoxVolumeNormalization);
			this->m_groupBoxDisplay->Location = System::Drawing::Point(3, 417);
			this->m_groupBoxDisplay->Name = L"m_groupBoxDisplay";
			this->m_groupBoxDisplay->Size = System::Drawing::Size(572, 47);
			this->m_groupBoxDisplay->TabIndex = 16;
			this->m_groupBoxDisplay->TabStop = false;
			this->m_groupBoxDisplay->Text = L"Display";
			// 
			// m_checkBoxVolumeNormalization
			// 
			this->m_checkBoxVolumeNormalization->AutoSize = true;
			this->m_checkBoxVolumeNormalization->Location = System::Drawing::Point(6, 19);
			this->m_checkBoxVolumeNormalization->Name = L"m_checkBoxVolumeNormalization";
			this->m_checkBoxVolumeNormalization->Size = System::Drawing::Size(355, 17);
			this->m_checkBoxVolumeNormalization->TabIndex = 0;
			this->m_checkBoxVolumeNormalization->Text = L"Use volume-weigthed mean to compute displayed multibeam mean Sa";
			this->m_checkBoxVolumeNormalization->UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->tableLayoutPanel1->ColumnCount = 1;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->label1, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->groupBoxArchive, 0, 4);
			this->tableLayoutPanel1->Controls->Add(this->m_groupBoxDisplay, 0, 3);
			this->tableLayoutPanel1->Controls->Add(this->m_TransducersTreeView, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->m_groupBoxClass, 0, 2);
			this->tableLayoutPanel1->Location = System::Drawing::Point(12, 12);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 5;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(578, 600);
			this->tableLayoutPanel1->TabIndex = 17;
			// 
			// EISupervisedForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(602, 653);
			this->Controls->Add(this->tableLayoutPanel1);
			this->Controls->Add(this->m_buttonOK);
			this->Controls->Add(this->m_buttonCancel);
			this->Controls->Add(this->m_buttonStop);
			this->Controls->Add(this->m_buttonStart);
			this->MinimumSize = System::Drawing::Size(300, 300);
			this->Name = L"EISupervisedForm";
			this->Text = L"Supervised echo-integration";
			this->m_groupBoxClass->ResumeLayout(false);
			this->m_groupBoxClass->PerformLayout();
			this->groupBoxArchive->ResumeLayout(false);
			this->groupBoxArchive->PerformLayout();
			this->m_groupBoxDisplay->ResumeLayout(false);
			this->m_groupBoxDisplay->PerformLayout();
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

#pragma region Events

	private:
		System::Void m_buttonAddClass_Click(System::Object^  sender, System::EventArgs^  e);

		System::Void m_buttonRemoveClass_Click(System::Object^  sender, System::EventArgs^  e);

		System::Void m_buttonOK_Click(System::Object^  sender, System::EventArgs^  e);

		System::Void m_buttonCancel_Click(System::Object^  sender, System::EventArgs^  e);

		System::Void m_buttonStart_Click(System::Object^  sender, System::EventArgs^  e);

		System::Void m_buttonStop_Click(System::Object^  sender, System::EventArgs^  e);

		System::Void m_listViewClasses_MouseDoubleClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);

		System::Void m_textBox_Leave(System::Object^  sender, System::EventArgs^  e);

		System::Void m_textBox_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);

		System::Void buttonBrowse_Click(System::Object^  sender, System::EventArgs^  e);

#pragma endregion

	private:

		static System::String^ s_CLASS_STR = "Class";

		void ConfigToIHM();

		void IHMToConfig();

		void UpdateModuleEnable(bool enable);

		int GetSubItemAt(int x, int y, ListViewItem^ %item);

		int m_SelectedColumnIndex;

		ReaderCtrlManaged^ m_ReaderCtrlManaged;
		
		ListViewItem ^m_SelectedItem;
};
}
