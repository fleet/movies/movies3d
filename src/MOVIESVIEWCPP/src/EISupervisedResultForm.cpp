#include "ColorPaletteEIClass.h"
#include "displayparameter.h"
#include "EISupervisedResultForm.h"
#include "MoviesException.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/parameter/ParameterBroadcastAndRecord.h"

#include "ModuleManager/ModuleManager.h"
#include "OutputManager/MovFileOutput.h"

#include "EISupervised/EISupervisedModule.h"
#include "EISupervised/EISupervisedPolygonFileOutput.h"

#include "utils.h"
#include <time.h>

using namespace MOVIESVIEWCPP;

void EISupervisedResultForm::Initialize()
{
	// r�cup�ration du module
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	EISupervisedModule * pModule = pModuleMgr->GetEISupervisedModule();

	EISupervisedParameter parameter = pModule->GetEISupervisedParameter();

	transducerComboBox->BeginUpdate();
	for(const auto& transducerLayerConf : parameter.getTransducers())
	{
		auto transName = transducerLayerConf.first;
		transducerComboBox->Items->Add(cppStr2NetStr(transName.c_str()));
	}
	transducerComboBox->EndUpdate();


	ColorPaletteEIClass colorPaletteEIClass = DisplayParameter::getInstance()->GetColorPaletteEIClass();

	// Initialisation de la liste des icones
	ImageList ^ colorIconList = gcnew ImageList();
	m_listViewResults->SmallImageList = colorIconList;

	int i = 0;
	for (const auto& classification : parameter.getClassifications())
	{
		// Initialisation de l'icone de couleur
		int w = 16;
		int h = 16;
		Bitmap ^ bmp = gcnew Bitmap(w, h);
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				if (x == 0 || y == 0
					|| x == (w - 1) || y == (h - 1))
				{
					bmp->SetPixel(x, y, Color::Black);
				}
				else
				{
					bmp->SetPixel(x, y, Color::FromArgb(colorPaletteEIClass.getARGBColorClassification(i)));
				}
			}
		}
		colorIconList->Images->Add(bmp);

		auto strName = gcnew String(classification.m_Name.c_str());

		ColumnHeader ^ colHeader = gcnew ColumnHeader(i);
		colHeader->Text = strName;
		colHeader->AutoResize(ColumnHeaderAutoResizeStyle::HeaderSize);
		m_listViewResults->Columns->Insert(i + 1, colHeader);

		i++;
	}

	m_initialized = true;
}

void EISupervisedResultForm::UpdateResults()
{
	m_listViewResults->Items->Clear();

	//  verifie que les colonnes ont �t� initialis�es
	if (m_initialized == false)
	{
		Initialize();
	}

	// r�cuperation des classes
	EISupervisedModule * pModule = CModuleManager::getInstance()->GetEISupervisedModule();
	EISupervisedParameter parameter = pModule->GetEISupervisedParameter();


	std::string transName = netStr2CppStr(TransducerName);
	const auto& results = pModule->GetResults(transName, parameter.getClassifications(), parameter.isUseVolumicWeightedAvg());
	for (MapResultByEsu::const_iterator iter = results.begin(); iter != results.end(); iter++)
	{
		EISupervisedResult * pResult = iter->second;

		System::Int32 esuId = iter->first;

		System::String ^displayedEsuId = "?";
		M3DKernel * pKernel = M3DKernel::GetInstance();
		pKernel->Lock();
		ESUParameter * esu = pKernel->getMovESUManager()->GetESUContainer().GetESUById(esuId);
		if (esu) {
			displayedEsuId = Convert::ToString((std::uint64_t)esu->GetDisplayESUId());
		}
		pKernel->Unlock();

		ListViewItem ^item = gcnew ListViewItem(displayedEsuId);
		item->Tag = esuId;

		// colonnes des classifications
		for (const auto& classification : parameter.getClassifications())
		{
			auto subItem = gcnew ListViewItem::ListViewSubItem();
			subItem->Text = String::Format("{0:0.00}", pResult->GetClassificationSa(classification.m_Id));
			item->SubItems->Add(subItem);
		}

		// colone energie totalle
		ListViewItem::ListViewSubItem ^ subItemTotal = gcnew ListViewItem::ListViewSubItem();
		subItemTotal->Text = System::String::Format("{0:0.00}", pResult->GetTotalSa());
		item->SubItems->Add(subItemTotal);

		// colone energie restante
		ListViewItem::ListViewSubItem ^ subItemRemaining = gcnew ListViewItem::ListViewSubItem();
		subItemRemaining->Text = System::String::Format("{0:0.00}", pResult->GetRemainingSa());
		item->SubItems->Add(subItemRemaining);

		if (pResult->IsAlreadySaved())
		{
			item->BackColor = Color::LightGray;
		}
		m_listViewResults->Items->Add(item);

		//iEsu++;
	}
}

void EISupervisedResultForm::OnEsuEnded()
{
	EISupervisedModule * pModule = CModuleManager::getInstance()->GetEISupervisedModule();
	if (pModule->getEnable())
	{
		UpdateResults();
	}
}

std::string ToStr(HacTime p_datetime)
{
	char date[255];


	// NMD -13/04/2012  - FAE 119
	// Modification du format de sortie des dates dans le CSV
	//p_datetime.GetTimeDesc(date,254);
	time_t time = p_datetime.m_TimeCpu;
	tm t;
	gmtime_s(&t, &time);
	sprintf_s(date, 254, "%4d%02d%02d_%02d%02d%02d",
		t.tm_year + 1900, t.tm_mon + 1, t.tm_mday,
		t.tm_hour, t.tm_min, t.tm_sec, p_datetime.m_TimeFraction);

	std::string result(date);
	return result;
}

System::Void EISupervisedResultForm::buttonSaveResults_Click(System::Object^  sender, System::EventArgs^  e)
{
	EISupervisedModule * pModule = CModuleManager::getInstance()->GetEISupervisedModule();
	if (pModule->getEnable())
	{
		// pr�paration des r�sultats
		EsuIdVector esuIdVector;
		for each(ListViewItem ^item in m_listViewResults->Items)
		{
			if (item->Checked)
			{
				esuIdVector.push_back(Convert::ToUInt32(item->Tag));
				item->BackColor = Color::LightGray;
			}
		}

		if (esuIdVector.size() > 0)
		{
			std::string transName = netStr2CppStr(TransducerName);
			bool bCreatedMovFileOutput;
			std::vector<EISupervisedPolygonFileOutput*> polygonFilesOutputs;
			EISupervisedMovFileOutput * pFileOutput = pModule->GetDestinationFile(transName, esuIdVector, bCreatedMovFileOutput, polygonFilesOutputs);
			if (pFileOutput)
			{
				// construction de la liste des noms des fichiers images � �crire
				std::map<std::uint32_t, std::string> imageFileNames;
				std::set<std::string> imageFileNamesSet;
				std::string outputFile = pFileOutput->getFilePath();
				std::set<std::string> existingFiles;

				// OTK - FAE198 - v�rification de l'existence du fichier de r�sultat CSV MAN si on n'est pas en mode append
				// remarque : dans ce cas, le message est quelque peu approximatif car le fichier CSV ne serait pas r�ellement
				// �cras�, mais le contenu du nouveau fichier serait concat�n� � la fin de ce fichier (ce qui fait
				// qu'on obtient plusieurs lignes d'ent�tes des colonnes CSV, par exemple).
				if (bCreatedMovFileOutput && !pModule->GetEISupervisedParameter().isAppendResults())
				{
					if (System::IO::File::Exists(gcnew System::String(outputFile.c_str())))
					{
						existingFiles.insert(outputFile);
					}
				}

				for each(std::uint32_t esuId in esuIdVector)
				{
					std::string esuBmpFile = outputFile;

					if (esuBmpFile.find_last_of("\\") != std::string::npos)
					{					
						esuBmpFile = esuBmpFile.insert(esuBmpFile.find_last_of("\\"), "\\screenshots");
					}

					if (esuBmpFile.find_last_of('.') != std::string::npos)
					{
						esuBmpFile.erase(outputFile.find_last_of('.'));

						// on a besoin de l'heure de d�but et de fin de l'esu

						M3DKernel * pKernel = M3DKernel::GetInstance();
						pKernel->Lock();
						ESUParameter * esu = pKernel->getMovESUManager()->GetESUContainer().GetESUById(esuId);
						HacTime startTime, endTime;
						if (esu) {
							startTime = esu->GetESUTime();
							endTime = esu->GetESUEndTime();
						}
						pKernel->Unlock();

						std::ostringstream ostr;
						ostr << "_" << transName
							<< "_" << esu->GetDisplayESUId()
							<< "_" << ToStr(startTime)
							<< "_" << ToStr(endTime);

						esuBmpFile.append(ostr.str());
						// L'extension sera ajout�e au moment de la sauvegarde
						// esuBmpFile.append(".bmp");
					}

					imageFileNames[esuId] = esuBmpFile;
					imageFileNamesSet.insert(esuBmpFile);
				}

				// OTK - FAE198 - v�rification de la non existence des fichiers
				std::set<std::string>::iterator alreadyExistingFiles;
				for (alreadyExistingFiles = imageFileNamesSet.begin(); alreadyExistingFiles != imageFileNamesSet.end(); ++alreadyExistingFiles)
				{
					if (System::IO::File::Exists(gcnew System::String((*alreadyExistingFiles).c_str())))
					{
						existingFiles.insert(*alreadyExistingFiles);
					}
				}

				// OTK - FAE259 - v�rification de la non existence des fichiers polygones
				for (size_t iPolyFile = 0; iPolyFile < polygonFilesOutputs.size(); iPolyFile++)
				{
					if (System::IO::File::Exists(gcnew System::String(polygonFilesOutputs[iPolyFile]->getFilePath().c_str())))
					{
						existingFiles.insert(*alreadyExistingFiles);
					}
				}

				bool bWriteResults = true;
				if (!existingFiles.empty())
				{
					System::String^ messageInfo = "The following result file(s) already exist(s). Overwrite ?" + Environment::NewLine;
					int fileIdx = 0;
					for (alreadyExistingFiles = existingFiles.begin(); alreadyExistingFiles != existingFiles.end(); ++alreadyExistingFiles)
					{
						if (fileIdx != 0)
						{
							messageInfo += "," + Environment::NewLine;
						}
						messageInfo += gcnew System::String((*alreadyExistingFiles).c_str());
						fileIdx++;
						// on n'affiche que trois noms de fichier maximum pour ne pas surcharger la bo�te de dialogue
						if (fileIdx == 3 && existingFiles.size() > 3)
						{
							messageInfo += ",...";
							break;
						}
					}
					bWriteResults = MessageBox::Show(messageInfo, "Warning", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning, MessageBoxDefaultButton::Button2) == DialogResult::OK;
				}

				// �criture des fichiers r�sultat
				if (bWriteResults)
				{
					pModule->SaveResults(transName, esuIdVector, pFileOutput, bCreatedMovFileOutput, polygonFilesOutputs);
					for each(std::uint32_t esuId in esuIdVector)
					{
						SaveEIView(TransducerName, esuId, gcnew System::String(imageFileNames[esuId].c_str()));
					}
				}
				else
				{
					if (bCreatedMovFileOutput)
					{
						delete pFileOutput;
					}
				}

				for (size_t iPolyFile = 0; iPolyFile < polygonFilesOutputs.size(); iPolyFile++)
				{
					delete polygonFilesOutputs[iPolyFile];
				}
			}
		}
	}
}


System::Void EISupervisedResultForm::buttonSelectAll_Click(System::Object^  sender, System::EventArgs^  e)
{
	for each(ListViewItem ^item in m_listViewResults->Items)
	{
		item->Checked = true;
	}
}

System::Void EISupervisedResultForm::buttonReset_Click(System::Object^  sender, System::EventArgs^  e)
{
	EISupervisedModule * pModule = CModuleManager::getInstance()->GetEISupervisedModule();
	if (pModule->getEnable())
	{
		EsuIdVector esuIdVector;
		for each(ListViewItem ^item in m_listViewResults->Items)
		{
			if (item->Checked)
			{
				esuIdVector.push_back(Convert::ToUInt32(item->Tag));
			}
		}

		std::string transName = netStr2CppStr(TransducerName);
		pModule->Reset(transName, esuIdVector);
		UpdateResults();
	}
}

System::Void EISupervisedResultForm::OnTransducerChanged(String ^ transducerName)
{
	TransducerName = transducerName;
}

System::Void EISupervisedResultForm::transducerComboBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	UpdateResults();
	TransducerChanged(TransducerName);
}

System::Void EISupervisedResultForm::m_listViewResults_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	// OTK - FAE 2057 - on ne permet pas la s�lection car la surbrillance engendr�e emp�che de voir la couleur 
	// de la ligne.
	this->m_listViewResults->SelectedIndices->Clear();
}
