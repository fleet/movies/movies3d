#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de EISupervisedResultForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class EISupervisedResultForm : public System::Windows::Forms::UserControl
	{

	public:
		EISupervisedResultForm(void)
		{
			InitializeComponent();
			this->Text = "Supervised echo-integration results";
			m_initialized = false;
		}

		void OnEsuEnded();

		// Mise � jour des r�sultats
		void UpdateResults();

		delegate void SaveEIViewHandler(String ^transducer, std::uint32_t esuId, String ^out);
		event SaveEIViewHandler ^ SaveEIView;

		delegate void TransducerChangedHandler(String ^ transducerName);
		event TransducerChangedHandler ^ TransducerChanged;


		// Changement du transducteur dans la vue de c�t�
		System::Void OnTransducerChanged(String ^ transducerName);

		property String^ TransducerName
		{
			String^ get()
			{
				return transducerComboBox->Text;
			}
			void set(String^ value)
			{
				int idx = transducerComboBox->Items->IndexOf(value);
				if (idx != -1)
				{
					transducerComboBox->SelectedIndex = idx;
				}
			}
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~EISupervisedResultForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::Button^  buttonSaveResults;
	private: System::Windows::Forms::Button^  buttonReset;
	private: System::Windows::Forms::Button^  buttonSelectAll;
	private: System::Windows::Forms::ColumnHeader^  m_columnHeaderESU;
	private: System::Windows::Forms::ColumnHeader^  m_columnHeaderTotal;
	private: System::Windows::Forms::ColumnHeader^  m_columnHeaderRemaining;
	private: System::Windows::Forms::ListView^  m_listViewResults;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ComboBox^  transducerComboBox;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->buttonSaveResults = (gcnew System::Windows::Forms::Button());
			this->buttonReset = (gcnew System::Windows::Forms::Button());
			this->buttonSelectAll = (gcnew System::Windows::Forms::Button());
			this->m_columnHeaderESU = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_columnHeaderTotal = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_columnHeaderRemaining = (gcnew System::Windows::Forms::ColumnHeader());
			this->m_listViewResults = (gcnew System::Windows::Forms::ListView());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->transducerComboBox = (gcnew System::Windows::Forms::ComboBox());
			this->SuspendLayout();
			// 
			// buttonSaveResults
			// 
			this->buttonSaveResults->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->buttonSaveResults->Location = System::Drawing::Point(238, 193);
			this->buttonSaveResults->Name = L"buttonSaveResults";
			this->buttonSaveResults->Size = System::Drawing::Size(75, 23);
			this->buttonSaveResults->TabIndex = 1;
			this->buttonSaveResults->Text = L"Save";
			this->buttonSaveResults->UseVisualStyleBackColor = true;
			this->buttonSaveResults->Click += gcnew System::EventHandler(this, &EISupervisedResultForm::buttonSaveResults_Click);
			// 
			// buttonReset
			// 
			this->buttonReset->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->buttonReset->Location = System::Drawing::Point(157, 193);
			this->buttonReset->Name = L"buttonReset";
			this->buttonReset->Size = System::Drawing::Size(75, 23);
			this->buttonReset->TabIndex = 2;
			this->buttonReset->Text = L"Reset";
			this->buttonReset->UseVisualStyleBackColor = true;
			this->buttonReset->Click += gcnew System::EventHandler(this, &EISupervisedResultForm::buttonReset_Click);
			// 
			// buttonSelectAll
			// 
			this->buttonSelectAll->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->buttonSelectAll->Location = System::Drawing::Point(76, 193);
			this->buttonSelectAll->Name = L"buttonSelectAll";
			this->buttonSelectAll->Size = System::Drawing::Size(75, 23);
			this->buttonSelectAll->TabIndex = 3;
			this->buttonSelectAll->Text = L"Select All";
			this->buttonSelectAll->UseVisualStyleBackColor = true;
			this->buttonSelectAll->Click += gcnew System::EventHandler(this, &EISupervisedResultForm::buttonSelectAll_Click);
			// 
			// m_columnHeaderESU
			// 
			this->m_columnHeaderESU->Text = L"ESU";
			this->m_columnHeaderESU->Width = 70;
			// 
			// m_columnHeaderTotal
			// 
			this->m_columnHeaderTotal->Text = L"Total";
			this->m_columnHeaderTotal->Width = 80;
			// 
			// m_columnHeaderRemaining
			// 
			this->m_columnHeaderRemaining->Text = L"Remaining";
			this->m_columnHeaderRemaining->Width = 80;
			// 
			// m_listViewResults
			// 
			this->m_listViewResults->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->m_listViewResults->CheckBoxes = true;
			this->m_listViewResults->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(3) {
				this->m_columnHeaderESU,
					this->m_columnHeaderTotal, this->m_columnHeaderRemaining
			});
			this->m_listViewResults->Location = System::Drawing::Point(3, 30);
			this->m_listViewResults->MultiSelect = false;
			this->m_listViewResults->Name = L"m_listViewResults";
			this->m_listViewResults->Size = System::Drawing::Size(310, 157);
			this->m_listViewResults->TabIndex = 0;
			this->m_listViewResults->UseCompatibleStateImageBehavior = false;
			this->m_listViewResults->View = System::Windows::Forms::View::Details;
			this->m_listViewResults->SelectedIndexChanged += gcnew System::EventHandler(this, &EISupervisedResultForm::m_listViewResults_SelectedIndexChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(3, 6);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(61, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"Transducer";
			// 
			// transducerComboBox
			// 
			this->transducerComboBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->transducerComboBox->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->transducerComboBox->FormattingEnabled = true;
			this->transducerComboBox->Location = System::Drawing::Point(70, 3);
			this->transducerComboBox->Name = L"transducerComboBox";
			this->transducerComboBox->Size = System::Drawing::Size(243, 21);
			this->transducerComboBox->TabIndex = 5;
			this->transducerComboBox->SelectedIndexChanged += gcnew System::EventHandler(this, &EISupervisedResultForm::transducerComboBox_SelectedIndexChanged);
			// 
			// EISupervisedResultForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->transducerComboBox);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->buttonSelectAll);
			this->Controls->Add(this->buttonReset);
			this->Controls->Add(this->buttonSaveResults);
			this->Controls->Add(this->m_listViewResults);
			this->Name = L"EISupervisedResultForm";
			this->Size = System::Drawing::Size(316, 219);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	protected:
		// Initialisation des colonnes des classifications et des transducers activ�s
		void Initialize();

		bool m_initialized;

		String ^ m_Title;

	private:
		System::Void buttonSaveResults_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void buttonSelectAll_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void buttonReset_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void transducerComboBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void m_listViewResults_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	};
}
