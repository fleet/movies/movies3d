#include "EISupervisedSelection.h"

#include "ShoalExtraction/data/ShoalData.h"
#include "ShoalExtraction/data/ShoalID.h"

#include "BaseFlatSideView.h"

using namespace MOVIESVIEWCPP;


MOVIESVIEWCPP::EISelectionManager::EISelectionManager()
{
	m_currentEsuId = -1;
	m_selectedObjects = gcnew System::Collections::Generic::List<ISelection^>;
}

void MOVIESVIEWCPP::EISelectionManager::clearSelection()
{
	m_selectedObjects->Clear();
	m_currentEsuId = -1;
}

bool MOVIESVIEWCPP::EISelectionManager::addToSelection(ISelection ^selection, const eSelectionMode & mode)
{
	bool added = false;
	switch (mode)
	{
	case eSelectionMode::ReplaceSelection:
		m_selectedObjects->Clear();
		m_selectedObjects->Add(selection);
		m_currentEsuId = selection->EsuId;
		added = true;
		break;
	case eSelectionMode::AddToCurrentSelection:
		if (!m_selectedObjects->Contains(selection))
		{
			if (m_selectedObjects->Count == 0
				|| selection->EsuId == m_currentEsuId)
			{
				// si l'objet est d�j� s�lectionn� on le d�selectionne
				int idx = m_selectedObjects->FindIndex(gcnew System::Predicate<ISelection ^>(selection, &ISelection::Match));
				if (idx == -1)
				{
					m_selectedObjects->Add(selection);
					m_currentEsuId = selection->EsuId;
					added = true;
				}
				else
				{
					m_selectedObjects->RemoveAt(idx);
					added = true;
				}
			}
		}
		break;
	case eSelectionMode::ExpandCurrentSelection:

		if (!m_selectedObjects->Contains(selection))
		{
			if (m_selectedObjects->Count == 0)
			{
				m_selectedObjects->Add(selection);
				m_currentEsuId = selection->EsuId;
				added = true;
			}
			else if (selection->EsuId == m_currentEsuId)
			{
				LayerSelection ^ currentLayerSelection = dynamic_cast<LayerSelection^>(selection);
				if (currentLayerSelection != nullptr)
				{
					int firstLayerIdx = currentLayerSelection->LayerIndex;
					int lastLayerIdx = currentLayerSelection->LayerIndex;
					for each(ISelection ^ s in m_selectedObjects)
					{
						LayerSelection ^ layerSelection = dynamic_cast<LayerSelection^>(s);
						if (layerSelection != nullptr)
						{
							if (layerSelection->LayerType == currentLayerSelection->LayerType)
							{
								int layerIdx = layerSelection->LayerIndex;
								if (firstLayerIdx == -1 || layerIdx < firstLayerIdx)
								{
									firstLayerIdx = layerIdx;
								}

								if (lastLayerIdx == -1 || layerIdx > lastLayerIdx)
								{
									lastLayerIdx = layerIdx;
								}
							}
						}
					}

					m_selectedObjects->Clear();
					for (unsigned int i = firstLayerIdx; i <= lastLayerIdx; ++i)
					{
						m_selectedObjects->Add(gcnew LayerSelection(m_currentEsuId, currentLayerSelection->LayerType, i));
					}
				}
			}
		}
		break;
	}
	return added;
}

bool MOVIESVIEWCPP::EISelectionManager::isSelected(const int & esuId, const Layer::Type & layerType, const unsigned int & layerIndex)
{
	LayerSelection ^ searchedSelection = gcnew LayerSelection(layerIndex, layerType, esuId);
	return m_selectedObjects->FindIndex(gcnew System::Predicate<ISelection ^>(searchedSelection, &ISelection::Match)) != -1;
}

bool MOVIESVIEWCPP::EISelectionManager::isSelected(const int & esuId, BaseMathLib::Poly2D * polygon)
{
	PolygonSelection ^ searchedSelection = gcnew PolygonSelection(polygon, esuId);
	return m_selectedObjects->FindIndex(gcnew System::Predicate<ISelection ^>(searchedSelection, &ISelection::Match)) != -1;
}

System::Collections::Generic::List<ISelection^> ^ MOVIESVIEWCPP::EISelectionManager::currentSelection()
{
	return m_selectedObjects;
}

bool PolygonSelection::Match(ISelection ^ other)
{
	bool bMatch = false;
	PolygonSelection ^ polygonSelection = dynamic_cast<PolygonSelection ^>(other);
	if (polygonSelection != nullptr)
	{
		bMatch = (EsuId == polygonSelection->EsuId && Polygon == polygonSelection->Polygon);
	}
	return bMatch;
}

bool LayerSelection::Match(ISelection ^ other)
{
	bool bMatch = false;
	LayerSelection ^ layerSelection = dynamic_cast<LayerSelection ^>(other);
	if (layerSelection != nullptr)
	{
		bMatch = (EsuId == layerSelection->EsuId && LayerType == layerSelection->LayerType && LayerIndex == layerSelection->LayerIndex);
	}
	return bMatch;
}


System::String ^ PolygonSelection::GetDescription()
{
	return "Polygon";
}

System::String ^ LayerSelection::GetDescription()
{
	switch (m_layerType)
	{
	case Layer::Type::SurfaceLayer:
		return "Surface layer";
		break;

	case Layer::Type::BottomLayer:
		return "Bottom layer";
		break;

	case Layer::Type::DistanceLayer:
		return "Distance layer";
		break;
	}
	return "Layer";
}