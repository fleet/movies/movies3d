#pragma once

#include "BaseMathLib/geometry/Poly2D.h"
#include "EchoIntegration/LayerDef.h"

namespace shoalextraction
{
	class ShoalData;
}
class ClassificationDef;

namespace MOVIESVIEWCPP {

	ref class BaseFlatSideView;

	// Classe abstraite représentant la selection d'un objet de l'echo integration supervisée
	public ref class ISelection abstract
	{
	public:

		ISelection(int esuId)
		{
			m_esuId = esuId;
		}

		property int EsuId
		{
			int get()
			{
				return m_esuId;
			}
		}

		property System::String ^ Description
		{
			System::String ^ get()
			{
				return GetDescription();
			}
		}

		virtual bool Match(ISelection ^ other) abstract;

	protected:

		int m_esuId;

		virtual System::String ^ GetDescription() abstract;
	};

	// Selection d'un polygon
	public ref class PolygonSelection : ISelection
	{
	public:

		PolygonSelection(BaseMathLib::Poly2D * polygon, int esuId)
			:ISelection(esuId)
		{
			m_polygon = polygon;
		}

		property BaseMathLib::Poly2D * Polygon
		{
			BaseMathLib::Poly2D * get()
			{
				return m_polygon;
			}
		}

		virtual bool Match(ISelection ^ other) override;

	protected:

		virtual System::String ^ GetDescription() override;

		BaseMathLib::Poly2D * m_polygon;
	};

	// Selection d'une couche
	public ref class LayerSelection : ISelection
	{
	public:
		LayerSelection(int esuId, Layer::Type layerType, int layerIndex)
			:ISelection(esuId)
		{
			m_layerType = layerType;
			m_layerIndex = layerIndex;
		}

		property Layer::Type LayerType
		{
			Layer::Type get()
			{
				return m_layerType;
			}
		}

		property int LayerIndex
		{
			int get()
			{
				return m_layerIndex;
			}
		}

		virtual bool Match(ISelection ^ other) override;

	protected:
		virtual System::String ^ GetDescription() override;

		Layer::Type m_layerType;

		int m_layerIndex;
	};

	public ref class EISelectionManager
	{
	public:
		enum class eSelectionMode
		{
			ReplaceSelection,
			AddToCurrentSelection,
			ExpandCurrentSelection
		};

		EISelectionManager();

		property int CurrentEsuId
		{
			int get()
			{
				return m_currentEsuId;
			}
		}

		void clearSelection();

		bool addToSelection(ISelection ^selection, const eSelectionMode & mode);

		bool isSelected(const int & esuId, const Layer::Type & layerType, const unsigned int & layerIndex);
		bool isSelected(const int & esuId, BaseMathLib::Poly2D * polygon);

		System::Collections::Generic::List<ISelection^> ^ currentSelection();

	private:

		int m_currentEsuId;

		System::Collections::Generic::List<ISelection^> ^ m_selectedObjects;
	};


	// Item pour le menu de selection des objets de l'echo integration supervisée
	public ref class SelectionToolStripItem : System::Windows::Forms::ToolStripMenuItem
	{
	public:
		SelectionToolStripItem(ISelection ^ selection, EISelectionManager::eSelectionMode m_selectionMode)
			:System::Windows::Forms::ToolStripMenuItem()
		{
			m_selection = selection;
			this->Text = selection->Description;
		}

		property ISelection ^Selection
		{
			ISelection ^ get()
			{
				return m_selection;
			}
		}

		property EISelectionManager::eSelectionMode SelectionMode
		{
			EISelectionManager::eSelectionMode get()
			{
				return m_selectionMode;
			}
		}

	private:
		ISelection ^m_selection;
		EISelectionManager::eSelectionMode m_selectionMode;
	};

}