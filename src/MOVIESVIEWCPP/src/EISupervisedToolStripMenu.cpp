#include "EISupervisedToolStripMenu.h"

using namespace MOVIESVIEWCPP;

EISupervisedToolStripMenu::EISupervisedToolStripMenu()
	:ContextMenuStrip()
{
}

void EISupervisedToolStripMenu::OnItemClicked(ToolStripItemClickedEventArgs ^ e)
{
	if (e->ClickedItem == m_DeletePolygonButton)
	{
		// envoi evenement delete polygon
		DeletePolygonClicked();
	}
}


void EISupervisedToolStripMenu::OnSubItemClicked(Object ^sender, ToolStripItemClickedEventArgs ^ e)
{
	EISupervisedOptionToolStripItem ^item = dynamic_cast<EISupervisedOptionToolStripItem ^>(e->ClickedItem);
	if (item != nullptr)
	{
		switch (item->Option)
		{
		case EISupervisedOptionToolStripItem::eOption::ADD:
			AddEnergyClicked(item->ClassIdx);
			break;
		case EISupervisedOptionToolStripItem::eOption::SUBSTRACT:
			SubstractEnergyClicked(item->ClassIdx);
			break;
		case EISupervisedOptionToolStripItem::eOption::RECLASSIFY:
			ReclassifyEnergyClicked(item->ClassIdx);
			break;
		}
	}
}

void EISupervisedToolStripMenu::InitMenu(double & remainingEnergy,
	const std::vector<std::string> & classificationNames,
	const std::vector<double> & classificationEnergies)
{
	this->SuspendLayout();
	this->Items->Clear();

	// Ajout de la description de la selection courante
	if (m_selectionDescriptionLabel == nullptr)
	{
		m_selectionDescriptionLabel = gcnew ToolStripMenuItem();
		m_selectionDescriptionLabel->Enabled = false;
	}
	this->Items->Add(m_selectionDescriptionLabel);

	// Sous-menu de reclassification
	ToolStripMenuItem ^ reclassifyMenuItem = gcnew ToolStripMenuItem("Reclassify as");
	reclassifyMenuItem->DropDownItemClicked += gcnew ToolStripItemClickedEventHandler(this, &EISupervisedToolStripMenu::OnSubItemClicked);

	// Ajout des classes au menu princial et au sous-menu de reclassification
	int nbClasses = classificationNames.size();
	for (int iClass = 0; iClass < nbClasses; iClass++)
	{
		String ^strName = gcnew System::String(classificationNames[iClass].c_str());
		EISupervisedClassToolStripItem ^subMenu = gcnew EISupervisedClassToolStripItem(iClass, strName, remainingEnergy, classificationEnergies[iClass]);
		subMenu->SubItemClicked += gcnew ToolStripItemClickedEventHandler(this, &EISupervisedToolStripMenu::OnSubItemClicked);

		this->Items->Add(subMenu);

		EISupervisedOptionToolStripItem ^reclassifyItem = gcnew EISupervisedOptionToolStripItem(iClass, EISupervisedOptionToolStripItem::eOption::RECLASSIFY, "0.0");
		reclassifyItem->Text = strName;
		reclassifyMenuItem->DropDown->Items->Add(reclassifyItem);
	}

	this->Items->Add(reclassifyMenuItem);

	// Init delete polygon button
	if (m_DeletePolygonButton == nullptr)
	{
		m_DeletePolygonButton = gcnew System::Windows::Forms::ToolStripMenuItem();
		m_DeletePolygonButton->Text = "Delete polygon";
	}
	if (m_separator == nullptr)
	{
		m_separator = gcnew System::Windows::Forms::ToolStripSeparator();
	}

	this->Items->Add(m_separator);
	this->Items->Add(m_DeletePolygonButton);

	this->ResumeLayout();
}


EISupervisedClassToolStripItem::EISupervisedClassToolStripItem(int classIdx, String ^classification, const double & addEnergy, const double & subEnergy)
	:ToolStripMenuItem(classification)
{
	this->DropDown->Items->Add(gcnew EISupervisedOptionToolStripItem(
		classIdx,
		EISupervisedOptionToolStripItem::eOption::ADD,
		String::Format("{0:0.00}",
			addEnergy)));

	this->DropDown->Items->Add(gcnew EISupervisedOptionToolStripItem(
		classIdx,
		EISupervisedOptionToolStripItem::eOption::SUBSTRACT,
		String::Format("{0:0.00}",
			subEnergy)));
}

void EISupervisedClassToolStripItem::OnDropDownItemClicked(ToolStripItemClickedEventArgs ^ e)
{
	SubItemClicked(this, e);
}

EISupervisedOptionToolStripItem::EISupervisedOptionToolStripItem(int classIdx, eOption option, String ^ energy)
	:ToolStripMenuItem()
{
	m_classIdx = classIdx;
	m_option = option;

	if (option == eOption::ADD)
	{
		this->Text = "Add";
	}
	else if (option == eOption::SUBSTRACT)
	{
		this->Text = "Substract";
	}

	this->Text += " (" + energy + ")";
}
