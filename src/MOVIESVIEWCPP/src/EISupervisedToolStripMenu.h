#pragma once

#include <vector>
#include <string>

using namespace System;
using namespace System::Windows::Forms;

namespace MOVIESVIEWCPP
{
	/*
	* Classe représentant le menu affiché pour sélectionner la classe pour un élément lors de l'echo-intégration supervisée
	*/
	public ref class EISupervisedToolStripMenu : ContextMenuStrip
	{
	public:
		EISupervisedToolStripMenu();

		void InitMenu(double & remainingEnergy, const std::vector<std::string> & classificationNames, const std::vector<double> & classificationEnergies);

		property bool DeletePolygonVisible
		{
			bool get()
			{
				return m_DeletePolygonButton->Visible;
			}
			void set(bool value)
			{
				m_DeletePolygonButton->Visible = value;
				m_separator->Visible = value;
			}
		}

		property String^ SelectionDescription
		{
			String ^ get()
			{
				return m_selectionDescriptionLabel->Text;
			}

			void set(String ^ value)
			{
				m_selectionDescriptionLabel->Text = value;
			}
		}

		delegate void DeletePolygonHandler();
		delegate void OperationEnergyHandler(int classIdx);

		event DeletePolygonHandler ^ DeletePolygonClicked;
		event OperationEnergyHandler ^ AddEnergyClicked;
		event OperationEnergyHandler ^ SubstractEnergyClicked;
		event OperationEnergyHandler ^ ReclassifyEnergyClicked;

	protected:
		virtual void OnItemClicked(ToolStripItemClickedEventArgs ^ e) override;
		void OnSubItemClicked(Object ^ sender, ToolStripItemClickedEventArgs ^ e);

	private:
		ToolStripMenuItem ^ m_selectionDescriptionLabel;
		ToolStripSeparator ^ m_separator;
		ToolStripMenuItem ^ m_DeletePolygonButton;
	};

	public ref class EISupervisedClassToolStripItem : ToolStripMenuItem
	{
	public:
		EISupervisedClassToolStripItem(int classIdx, String ^ classification, const double & addEnergy, const double & subEnergy);

		event ToolStripItemClickedEventHandler ^ SubItemClicked;

	protected:
		virtual void OnDropDownItemClicked(ToolStripItemClickedEventArgs ^ e) override;
	};

	public ref class EISupervisedOptionToolStripItem : ToolStripMenuItem
	{
	public:

		enum class eOption
		{
			ADD,
			SUBSTRACT,
			RECLASSIFY
		};

		EISupervisedOptionToolStripItem(int classIdx, eOption option, String ^ energy);

		property eOption Option
		{
			eOption get()
			{
				return m_option;
			}
		}

		property int ClassIdx
		{
			int get()
			{
				return m_classIdx;
			}
		}

	protected:
		eOption m_option;

		int m_classIdx;
	};
}