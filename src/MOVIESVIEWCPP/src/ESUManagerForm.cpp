#include "ESUManagerForm.h"

// D�pendances
#include "M3DKernel/M3DKernel.h"
#include <cassert>


using namespace MOVIESVIEWCPP;

// ***************************************************************************
/**
 * mise � jour du ParameterModule associ� � l'IHM en fonction des
 * reglages IHM
 *
 * @date   16/05/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void ESUManagerForm::IHMToConfig()
{
	// r�cup�ration du module de gestion des ESU
	MovESUMgr * pMovESUMgr = M3DKernel::GetInstance()->getMovESUManager();

	bool paramsChanged = false;

	ESUParameter::TESUCutType cutType = pMovESUMgr->GetRefParameter().GetESUCutType();
	std::int64_t pingNumber = pMovESUMgr->GetRefParameter().GetESUPingNumber();
	double distance = pMovESUMgr->GetRefParameter().GetESUDistance();
	HacTime time = pMovESUMgr->GetRefParameter().GetESUTime();

	// recopie des param�tres
	// Type de d�coupage
	if (radioButtonDistance->Checked)
	{
		pMovESUMgr->GetRefParameter().SetESUCutType(ESUParameter::ESU_CUT_BY_DISTANCE);
	}
	else if (radioButtonPing->Checked)
	{
		pMovESUMgr->GetRefParameter().SetESUCutType(ESUParameter::ESU_CUT_BY_PING_NB);
	}
	else
	{
		pMovESUMgr->GetRefParameter().SetESUCutType(ESUParameter::ESU_CUT_BY_TIME);
	}

	// valeurs des param�tres
	// Distance
	pMovESUMgr->GetRefParameter().SetESUDistance(Convert::ToDouble(numericUpDownDistance->Value));
	// Nombre de pings
	pMovESUMgr->GetRefParameter().SetESUPingNumber(Convert::ToUInt64(numericUpDownPing->Value));
	// Dur�e
	HacTime esuTime;
	esuTime.SetTimeAsSecond(Convert::ToInt32(textBoxHour->Text) * 3600 + Convert::ToInt32(textBoxMinute->Text) * 60);
	pMovESUMgr->GetRefParameter().SetESUTime(esuTime);

	// si les param�tres ont chang�, on force le d�marrage d'un nouvel ESU
	if (cutType != pMovESUMgr->GetRefParameter().GetESUCutType())
	{
		paramsChanged = true;
	}
	switch (cutType)
	{
	case ESUParameter::ESU_CUT_BY_DISTANCE:
		if (distance != pMovESUMgr->GetRefParameter().GetESUDistance())
		{
			paramsChanged = true;
		}
		break;
	case ESUParameter::ESU_CUT_BY_PING_NB:
		if (pingNumber != pMovESUMgr->GetRefParameter().GetESUPingNumber())
		{
			paramsChanged = true;
		}
		break;
	case ESUParameter::ESU_CUT_BY_TIME:
		if (!(time == pMovESUMgr->GetRefParameter().GetESUTime()))
		{
			paramsChanged = true;
		}
		break;
	default:
		assert(false);// type de d�coupage inconnu
	}

	pMovESUMgr->GetRefParameter().setMaxTemporalGap((int)System::Convert::ToInt32(numericUpDownMaxTemporalGap->Value) * 10000);

	if (paramsChanged)
	{
		pMovESUMgr->ForceNewESU();
	}
}

// ***************************************************************************
/**
 * mise � jour du de l'IHM � partir des param�tres du module associ�
 * reglages IHM
 *
 * @date   16/05/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void ESUManagerForm::ConfigToIHM()
{
	// r�cup�ration du module de gestion des ESU
	MovESUMgr * pMovESUMgr = M3DKernel::GetInstance()->getMovESUManager();

	// recopie des param�tres
	// Type de d�coupage
	switch (pMovESUMgr->GetRefParameter().GetESUCutType())
	{
	case ESUParameter::ESU_CUT_BY_DISTANCE:
		radioButtonDistance->Checked = true;
		break;
	case ESUParameter::ESU_CUT_BY_PING_NB:
		radioButtonPing->Checked = true;
		break;
	case ESUParameter::ESU_CUT_BY_TIME:
		radioButtonTime->Checked = true;
		break;
	default:
		assert(false); // type de d�coupage inconnu
		break;
	}


	// valeurs des param�tres
	// Distance
	numericUpDownDistance->Value = Convert::ToDecimal(pMovESUMgr->GetRefParameter().GetESUDistance());
	// Nombre de pings
	numericUpDownPing->Value = Convert::ToDecimal(pMovESUMgr->GetRefParameter().GetESUPingNumber());
	// Dur�e
	int hours = (int)floor(pMovESUMgr->GetRefParameter().GetESUTime().m_TimeCpu / 3600.0);
	int minutes = (int)floor((pMovESUMgr->GetRefParameter().GetESUTime().m_TimeCpu - (hours * 3600)) / 60.0);
	textBoxHour->Text = Convert::ToString(hours);
	textBoxMinute->Text = Convert::ToString(minutes);

	numericUpDownMaxTemporalGap->Value = System::Convert::ToDecimal(pMovESUMgr->GetRefParameter().getMaxTemporalGap().m_timeElapse / 10000);
}

