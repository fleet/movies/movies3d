#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

//d�pendances
#include "ModuleManager/ModuleManager.h"

#include "ReaderCtrlManaged.h"
#include "M3DKernel/M3DKernel.h"



namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de ESUManagerForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	ref class ESUManagerForm : public System::Windows::Forms::Form
	{
	public:
		ESUManagerForm(ReaderCtrlManaged^ %readerCtrl)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//

			m_ReaderCtrlManaged = readerCtrl;

			// mise � jour des composants avec les valeurs du module
			ConfigToIHM();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~ESUManagerForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::RadioButton^  radioButtonDistance;
	private: System::Windows::Forms::RadioButton^  radioButtonPing;
	private: System::Windows::Forms::RadioButton^  radioButtonTime;
	protected:



	private: System::Windows::Forms::Button^  buttonOK;
	private: System::Windows::Forms::Button^  buttonCancel;
	private: System::Windows::Forms::TextBox^  textBoxHour;
	private: System::Windows::Forms::TextBox^  textBoxMinute;
	private: System::Windows::Forms::Label^  labelHour;
	private: System::Windows::Forms::Label^  labelMinute;






	private: System::Windows::Forms::GroupBox^  groupBoxESUCutBy;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownPing;


	private: System::Windows::Forms::NumericUpDown^  numericUpDownDistance;

	protected:

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;
	private: System::Windows::Forms::GroupBox^  groupBoxMaxTemporalGap;

	private: System::Windows::Forms::Label^  labelMaxTemporalGap;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxTemporalGap;

			 // R�f�rence du readerControl pour les Lock du Monitor
			 ReaderCtrlManaged^ m_ReaderCtrlManaged;


			 // mise � jour de l'ESU Manager � partir des param�tres IHM
			 void IHMToConfig();
			 // mise � jour des param�tres IHM en fonction de l'ESU Manager
			 void ConfigToIHM();

#pragma region Windows Form Designer generated code
			 /// <summary>
			 /// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
			 /// le contenu de cette m�thode avec l'�diteur de code.
			 /// </summary>
			 void InitializeComponent(void)
			 {
				 this->radioButtonDistance = (gcnew System::Windows::Forms::RadioButton());
				 this->radioButtonPing = (gcnew System::Windows::Forms::RadioButton());
				 this->radioButtonTime = (gcnew System::Windows::Forms::RadioButton());
				 this->buttonOK = (gcnew System::Windows::Forms::Button());
				 this->buttonCancel = (gcnew System::Windows::Forms::Button());
				 this->textBoxHour = (gcnew System::Windows::Forms::TextBox());
				 this->textBoxMinute = (gcnew System::Windows::Forms::TextBox());
				 this->labelHour = (gcnew System::Windows::Forms::Label());
				 this->labelMinute = (gcnew System::Windows::Forms::Label());
				 this->groupBoxESUCutBy = (gcnew System::Windows::Forms::GroupBox());
				 this->numericUpDownPing = (gcnew System::Windows::Forms::NumericUpDown());
				 this->numericUpDownDistance = (gcnew System::Windows::Forms::NumericUpDown());
				 this->groupBoxMaxTemporalGap = (gcnew System::Windows::Forms::GroupBox());
				 this->labelMaxTemporalGap = (gcnew System::Windows::Forms::Label());
				 this->numericUpDownMaxTemporalGap = (gcnew System::Windows::Forms::NumericUpDown());
				 this->groupBoxESUCutBy->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownPing))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDistance))->BeginInit();
				 this->groupBoxMaxTemporalGap->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxTemporalGap))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // radioButtonDistance
				 // 
				 this->radioButtonDistance->AutoSize = true;
				 this->radioButtonDistance->Location = System::Drawing::Point(17, 17);
				 this->radioButtonDistance->Name = L"radioButtonDistance";
				 this->radioButtonDistance->Size = System::Drawing::Size(67, 17);
				 this->radioButtonDistance->TabIndex = 0;
				 this->radioButtonDistance->TabStop = true;
				 this->radioButtonDistance->Text = L"Distance";
				 this->radioButtonDistance->UseVisualStyleBackColor = true;
				 // 
				 // radioButtonPing
				 // 
				 this->radioButtonPing->AutoSize = true;
				 this->radioButtonPing->Location = System::Drawing::Point(17, 42);
				 this->radioButtonPing->Name = L"radioButtonPing";
				 this->radioButtonPing->Size = System::Drawing::Size(46, 17);
				 this->radioButtonPing->TabIndex = 1;
				 this->radioButtonPing->TabStop = true;
				 this->radioButtonPing->Text = L"Ping";
				 this->radioButtonPing->UseVisualStyleBackColor = true;
				 // 
				 // radioButtonTime
				 // 
				 this->radioButtonTime->AutoSize = true;
				 this->radioButtonTime->Location = System::Drawing::Point(17, 67);
				 this->radioButtonTime->Name = L"radioButtonTime";
				 this->radioButtonTime->Size = System::Drawing::Size(48, 17);
				 this->radioButtonTime->TabIndex = 2;
				 this->radioButtonTime->TabStop = true;
				 this->radioButtonTime->Text = L"Time";
				 this->radioButtonTime->UseVisualStyleBackColor = true;
				 // 
				 // buttonOK
				 // 
				 this->buttonOK->Location = System::Drawing::Point(43, 164);
				 this->buttonOK->Name = L"buttonOK";
				 this->buttonOK->Size = System::Drawing::Size(75, 23);
				 this->buttonOK->TabIndex = 3;
				 this->buttonOK->Text = L"Ok";
				 this->buttonOK->UseVisualStyleBackColor = true;
				 this->buttonOK->Click += gcnew System::EventHandler(this, &ESUManagerForm::buttonOK_Click);
				 // 
				 // buttonCancel
				 // 
				 this->buttonCancel->Location = System::Drawing::Point(124, 164);
				 this->buttonCancel->Name = L"buttonCancel";
				 this->buttonCancel->Size = System::Drawing::Size(75, 23);
				 this->buttonCancel->TabIndex = 4;
				 this->buttonCancel->Text = L"Cancel";
				 this->buttonCancel->UseVisualStyleBackColor = true;
				 this->buttonCancel->Click += gcnew System::EventHandler(this, &ESUManagerForm::buttonCancel_Click);
				 // 
				 // textBoxHour
				 // 
				 this->textBoxHour->Location = System::Drawing::Point(98, 66);
				 this->textBoxHour->Name = L"textBoxHour";
				 this->textBoxHour->Size = System::Drawing::Size(36, 20);
				 this->textBoxHour->TabIndex = 5;
				 // 
				 // textBoxMinute
				 // 
				 this->textBoxMinute->Location = System::Drawing::Point(156, 66);
				 this->textBoxMinute->Name = L"textBoxMinute";
				 this->textBoxMinute->Size = System::Drawing::Size(36, 20);
				 this->textBoxMinute->TabIndex = 6;
				 // 
				 // labelHour
				 // 
				 this->labelHour->AutoSize = true;
				 this->labelHour->Location = System::Drawing::Point(137, 69);
				 this->labelHour->Name = L"labelHour";
				 this->labelHour->Size = System::Drawing::Size(13, 13);
				 this->labelHour->TabIndex = 7;
				 this->labelHour->Text = L"h";
				 // 
				 // labelMinute
				 // 
				 this->labelMinute->AutoSize = true;
				 this->labelMinute->Location = System::Drawing::Point(198, 69);
				 this->labelMinute->Name = L"labelMinute";
				 this->labelMinute->Size = System::Drawing::Size(15, 13);
				 this->labelMinute->TabIndex = 8;
				 this->labelMinute->Text = L"m";
				 // 
				 // groupBoxESUCutBy
				 // 
				 this->groupBoxESUCutBy->Controls->Add(this->numericUpDownPing);
				 this->groupBoxESUCutBy->Controls->Add(this->numericUpDownDistance);
				 this->groupBoxESUCutBy->Controls->Add(this->labelMinute);
				 this->groupBoxESUCutBy->Controls->Add(this->labelHour);
				 this->groupBoxESUCutBy->Controls->Add(this->textBoxMinute);
				 this->groupBoxESUCutBy->Controls->Add(this->textBoxHour);
				 this->groupBoxESUCutBy->Controls->Add(this->radioButtonTime);
				 this->groupBoxESUCutBy->Controls->Add(this->radioButtonPing);
				 this->groupBoxESUCutBy->Controls->Add(this->radioButtonDistance);
				 this->groupBoxESUCutBy->Location = System::Drawing::Point(9, 11);
				 this->groupBoxESUCutBy->Name = L"groupBoxESUCutBy";
				 this->groupBoxESUCutBy->Size = System::Drawing::Size(221, 92);
				 this->groupBoxESUCutBy->TabIndex = 9;
				 this->groupBoxESUCutBy->TabStop = false;
				 this->groupBoxESUCutBy->Text = L"Cut ESU by :";
				 // 
				 // numericUpDownPing
				 // 
				 this->numericUpDownPing->Location = System::Drawing::Point(100, 41);
				 this->numericUpDownPing->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000, 0, 0, 0 });
				 this->numericUpDownPing->Name = L"numericUpDownPing";
				 this->numericUpDownPing->Size = System::Drawing::Size(56, 20);
				 this->numericUpDownPing->TabIndex = 10;
				 // 
				 // numericUpDownDistance
				 // 
				 this->numericUpDownDistance->DecimalPlaces = 3;
				 this->numericUpDownDistance->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 196608 });
				 this->numericUpDownDistance->Location = System::Drawing::Point(100, 16);
				 this->numericUpDownDistance->Name = L"numericUpDownDistance";
				 this->numericUpDownDistance->Size = System::Drawing::Size(56, 20);
				 this->numericUpDownDistance->TabIndex = 9;
				 // 
				 // groupBoxMaxTemporalGap
				 // 
				 this->groupBoxMaxTemporalGap->Controls->Add(this->labelMaxTemporalGap);
				 this->groupBoxMaxTemporalGap->Controls->Add(this->numericUpDownMaxTemporalGap);
				 this->groupBoxMaxTemporalGap->Location = System::Drawing::Point(9, 109);
				 this->groupBoxMaxTemporalGap->Name = L"groupBoxMaxTemporalGap";
				 this->groupBoxMaxTemporalGap->Size = System::Drawing::Size(217, 49);
				 this->groupBoxMaxTemporalGap->TabIndex = 16;
				 this->groupBoxMaxTemporalGap->TabStop = false;
				 this->groupBoxMaxTemporalGap->Text = L"Max Temporal Gap";
				 // 
				 // labelMaxTemporalGap
				 // 
				 this->labelMaxTemporalGap->AutoSize = true;
				 this->labelMaxTemporalGap->Location = System::Drawing::Point(99, 21);
				 this->labelMaxTemporalGap->Name = L"labelMaxTemporalGap";
				 this->labelMaxTemporalGap->Size = System::Drawing::Size(42, 13);
				 this->labelMaxTemporalGap->TabIndex = 1;
				 this->labelMaxTemporalGap->Text = L"second";
				 // 
				 // numericUpDownMaxTemporalGap
				 // 
				 this->numericUpDownMaxTemporalGap->Location = System::Drawing::Point(11, 19);
				 this->numericUpDownMaxTemporalGap->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, 0 });
				 this->numericUpDownMaxTemporalGap->Name = L"numericUpDownMaxTemporalGap";
				 this->numericUpDownMaxTemporalGap->Size = System::Drawing::Size(82, 20);
				 this->numericUpDownMaxTemporalGap->TabIndex = 0;
				 this->numericUpDownMaxTemporalGap->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 100, 0, 0, 0 });
				 // 
				 // ESUManagerForm
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(238, 193);
				 this->Controls->Add(this->groupBoxMaxTemporalGap);
				 this->Controls->Add(this->groupBoxESUCutBy);
				 this->Controls->Add(this->buttonCancel);
				 this->Controls->Add(this->buttonOK);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
				 this->Name = L"ESUManagerForm";
				 this->Text = L"ESUManagerForm";
				 this->groupBoxESUCutBy->ResumeLayout(false);
				 this->groupBoxESUCutBy->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownPing))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDistance))->EndInit();
				 this->groupBoxMaxTemporalGap->ResumeLayout(false);
				 this->groupBoxMaxTemporalGap->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxTemporalGap))->EndInit();
				 this->ResumeLayout(false);

			 }
#pragma endregion

			 // IPSIS - OTK - Bouton OK
	private: System::Void buttonOK_Click(System::Object^  sender, System::EventArgs^  e) {

		// v�rification de l'activit� des modules des traitement.
		CModuleManager * pModuleManager = CModuleManager::getInstance();
		std::string modulesEnabled;
		if (pModuleManager->IsTreatmentEnabled(modulesEnabled))
		{
			System::String^ message =
				gcnew System::String("Unable to change ESU configuration due to the following treatment(s) running:\n");
			System::String^ modules =
				gcnew System::String(modulesEnabled.c_str());
			message = String::Concat(message, modules);
			MessageBox::Show(message);
		}
		else
		{
			// mise � jour de la config
			M3DKernel::GetInstance()->Lock();
			try
			{
				IHMToConfig();
				Close();
			}
			catch (Exception^)
			{
				MessageBox::Show("Invalid configuration.");
			}
			M3DKernel::GetInstance()->Unlock();
		}
	}

			 // IPSIS - OTK - Bouton Cancel
	private: System::Void buttonCancel_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
	};
}
