#include "Echo3DList.h"
#include "vtkPingView.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "PingFan3D.h"
#include "vtk.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/NavAttributes.h"
#include "vtkGlyph4me.h"
#include "Sounder3D.h"
#include "M3DKernel/datascheme/KernelParameter.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/TransformSet.h"
#include "TSAnalysis/TSAnalysisModule.h"
#include "ModuleManager/ModuleManager.h"


using namespace BaseMathLib;

Echo3DList::Echo3DList(void)
{
	m_pingView = NULL;
}

Echo3DList::~Echo3DList(void)
{
	if (m_pingView)
		delete m_pingView;
	m_pingView = NULL;
}


void Echo3DList::CheckUpdate(ViewFilter &refViewFilter, PingFan3D *pingFan3d)
{
	if (m_pingView)
	{
		PingFan *pFan = pingFan3d->getPingFan();
		Sounder *pSounder = pFan->getSounderRef();
		Sounder3D *pSounder3d = refViewFilter.getSounder(pSounder->m_SounderId);
		if (refViewFilter.GetNeedToRegenerateVolume() || !pSounder3d->isVisible())
		{
			delete m_pingView;
			m_pingView = NULL;
		}
	}
}

bool Echo3DList::Build(ViewFilter &refViewFilter, PingFan3D *pingFan3d, PingAxisInformation& pingAxisInfo)
{
	if (getBuildFlag())
		return false;

	Sounder *pSounder = pingFan3d->getPingFan()->getSounderRef();
	PingFan *pFan = pingFan3d->getPingFan();

	Sounder3D *pSounder3d = refViewFilter.getSounder(pSounder->m_SounderId);
	DisplayParameter *pDisplay = DisplayParameter::getInstance();

	if (!m_pingView && pSounder3d->isVisible())
	{
		bool useGPSPositionning = DisplayParameter::getInstance()->GetUseGPSPositionning();
		HacTime currentPingFanTime = pingFan3d->getPingFan()->m_ObjectTime;
		m_bottomPoints.clear();
		m_singleEcho.clear();
		m_phaseEcho.clear();
		assert(pSounder);
		Vector3D firstPoint;
		Vector3D lastPoint;

		double pingMaxRange = ((PingFan*)pFan)->m_computePingFan.m_maxRange*0.001;


		vtkPoints *pPoint = vtkPoints::New();
		vtkFloatArray *pPointScale = vtkFloatArray::New();
		pPointScale->SetNumberOfComponents(3);

		vtkFloatArray *pPointNormal = vtkFloatArray::New();
		pPointNormal->SetNumberOfComponents(3);

		/// strenght
		vtkFloatArray *pStrenght = vtkFloatArray::New();

		for (unsigned int numTransducer = 0; numTransducer < pSounder->GetTransducerCount(); numTransducer++)
		{
			if (!pSounder3d->GetTransducerIdx(numTransducer)->isVisible())
				continue;
			Transducer *pTransducer = pSounder->GetTransducer(numTransducer);
			MemoryStruct *pPolarMem = pFan->GetMemorySetRef()->GetMemoryStruct(numTransducer);
			Vector2I sizeDataPolar = pPolarMem->GetDataFmt()->getSize();
			double sampleSpacing = pTransducer->getBeamsSamplesSpacing();

			unsigned int nbSample = 1;
			if (pDisplay->GetSampling() > sampleSpacing && sampleSpacing > 0 && pDisplay->GetSampling() > 0)
			{
				nbSample = pDisplay->GetSampling() / sampleSpacing;
				if (nbSample == 0)
				{
					nbSample = 1;
				}
			}
			bool AngleDownFound = false;
			double AngleDown;
			double AngleUp;
			FanLineCouple frontFanLine;
			FanLineCouple backFanLine;

			for (int beam = 0; beam < sizeDataPolar.x && beam >= 0; beam++)
			{

				SoftChannel *pSoftChannel = pTransducer->getSoftChannelPolarX(beam);
				if (!pSounder3d->GetTransducerIdx(numTransducer)->getChannelWithIdx(beam)->isVisible())
					continue;
				
				// OTK - 01/07/2009 - en fonction des param�tres de zoom on peut ou non afficher chaque channel
				// on r�cup�re l'angle de d�pointage min et max correspondant aux param�tres d'affichage
				TransformMap *pTransform = pSounder->GetTransformSetRef()->GetTransformMap(numTransducer);
				Vector2I transformSize = pTransform->getCartesianSize2();
				double minX = pTransform->cartesianToRealX((int)(transformSize.x*pDisplay->GetMinFrontHorizontalRatio() + 0.5));
				double maxX = pTransform->cartesianToRealX((int)(transformSize.x*pDisplay->GetMaxFrontHorizontalRatio() + 0.5));
				double offset = pTransducer->m_transDepthMeter + pFan->getHeaveChan(pSoftChannel->m_softChannelId);
				double minY = pDisplay->GetMinDepth() - offset;
				double maxY = pDisplay->GetMaxDepth() - offset;
				double angleMin = std::min(atan2(minX, minY), atan2(minX, maxY));
				double angleMax = std::max(atan2(maxX, minY), atan2(maxX, maxY));

				double beamAngleAlong = pSoftChannel->m_beam3dBWidthAlongRad / 2;
				double beamAngleAthwart = pSoftChannel->m_beam3dBWidthAthwartRad / 2;

				if ((pSoftChannel->m_mainBeamAthwartSteeringAngleRad + beamAngleAlong < angleMin) || (pSoftChannel->m_mainBeamAthwartSteeringAngleRad - beamAngleAlong > angleMax))
					continue;

				if (!AngleDownFound)
				{
					AngleDown = AngleUp = pSoftChannel->m_mainBeamAthwartSteeringAngleRad;
					AngleDownFound = true;
				}
				AngleDown = std::min(AngleDown, pSoftChannel->m_mainBeamAthwartSteeringAngleRad);
				AngleUp = std::max(AngleUp, pSoftChannel->m_mainBeamAthwartSteeringAngleRad);

				/////////////////////////////////////////
				// NMD FAE 121

				Channel3D *pChannel3D = pSounder3d->GetTransducerIdx(numTransducer)->getChannelWithIdx(beam);

				double AthwartSeuil = PI / (pSoftChannel->m_beamAthwartAngleSensitivity) * 0.9375;
				double AlongSeuil = PI / (pSoftChannel->m_beamAlongAngleSensitivity) * 0.9375;

				////////////////////////////////////////////
				
				DataFmt maxValue = UNKNOWN_DB;
				for (int depthPolar = 0; depthPolar < sizeDataPolar.y; depthPolar++)
				{
					if (depthPolar == 0)
					{
						Vector3D vectReal;
						if (useGPSPositionning)
						{
							vectReal = pSounder->GetPolarToCartesianCoord(pFan, numTransducer, beam, depthPolar);
						}
						else
						{
							vectReal = pSounder->GetPolarToWorldCoord(pFan, numTransducer, beam, depthPolar);
						}

						firstPoint = (vectReal);
						double DepthPol = 0;


						if (AngleDown == pTransducer->getSoftChannelPolarX(beam)->m_mainBeamAthwartSteeringAngleRad)
						{
							DepthPol = std::max<double>(0.0, (pDisplay->GetMinDepth() - pTransducer->m_transDepthMeter) / cos(AngleDown));
							if (pingMaxRange > 0)
							{
								DepthPol = std::min(DepthPol, pingMaxRange);
							}
							Vector3D vectRealBeamFront;
							Vector3D echoPos(DepthPol*tan(beamAngleAlong), DepthPol*tan(-beamAngleAthwart), DepthPol);
							if (useGPSPositionning)
							{
								vectRealBeamFront = pSounder->GetSoftChannelCoordinateToCartesianCoord(pFan, numTransducer, beam, echoPos);
							}
							else
							{
								vectRealBeamFront = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, beam, echoPos);
							}
							frontFanLine.m_LowerAngleBeam.m_PointUp = vectRealBeamFront;
							echoPos = Vector3D(DepthPol*tan(-beamAngleAlong), DepthPol*tan(-beamAngleAthwart), DepthPol);
							Vector3D vectRealBeamBack;
							if (useGPSPositionning)
							{
								vectRealBeamBack = pSounder->GetSoftChannelCoordinateToCartesianCoord(pFan, numTransducer, beam, echoPos);
							}
							else
							{
								vectRealBeamBack = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, beam, echoPos);
							}
							backFanLine.m_LowerAngleBeam.m_PointUp = vectRealBeamBack;
						}
						if (AngleUp == pTransducer->getSoftChannelPolarX(beam)->m_mainBeamAthwartSteeringAngleRad)
						{
							DepthPol = std::max<double>(0.0, (pDisplay->GetMinDepth() - pTransducer->m_transDepthMeter) / cos(AngleUp));
							if (pingMaxRange > 0)
							{
								DepthPol = std::min(DepthPol, pingMaxRange);
							}
							Vector3D vectRealBeamFront;
							Vector3D echoPos(DepthPol*tan(beamAngleAlong), DepthPol*tan(beamAngleAthwart), DepthPol);
							if (useGPSPositionning)
							{
								vectRealBeamFront = pSounder->GetSoftChannelCoordinateToCartesianCoord(pFan, numTransducer, beam, echoPos);
							}
							else
							{
								vectRealBeamFront = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, beam, echoPos);
							}
							frontFanLine.m_UpperAngleBeam.m_PointUp = vectRealBeamFront;
							echoPos = Vector3D(DepthPol*tan(-beamAngleAlong), DepthPol*tan(beamAngleAthwart), DepthPol);
							Vector3D vectRealBeamBack;
							if (useGPSPositionning)
							{
								vectRealBeamBack = pSounder->GetSoftChannelCoordinateToCartesianCoord(pFan, numTransducer, beam, echoPos);
							}
							else
							{
								vectRealBeamBack = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, beam, echoPos);
							}
							backFanLine.m_UpperAngleBeam.m_PointUp = vectRealBeamBack;
						}
					}
					if (depthPolar == sizeDataPolar.y - 1)
					{
						Vector3D vectReal;
						if (useGPSPositionning)
						{
							vectReal = pSounder->GetPolarToCartesianCoord(pFan, numTransducer, beam, depthPolar);
						}
						else
						{
							vectReal = pSounder->GetPolarToWorldCoord(pFan, numTransducer, beam, depthPolar);
						}
						lastPoint = (vectReal);
						double DepthPol = depthPolar*sampleSpacing;
						if (AngleDown == pTransducer->getSoftChannelPolarX(beam)->m_mainBeamAthwartSteeringAngleRad)
						{
							DepthPol = std::max<double>(0.0, (pDisplay->GetMaxDepth() - pTransducer->m_transDepthMeter) / cos(AngleDown));
							if (pingMaxRange > 0)
							{
								DepthPol = std::min(DepthPol, pingMaxRange);
							}
							Vector3D echoPos(DepthPol*tan(beamAngleAlong), DepthPol*tan(-beamAngleAthwart), DepthPol);
							Vector3D vectRealBeamFront;
							if (useGPSPositionning)
							{
								vectRealBeamFront = pSounder->GetSoftChannelCoordinateToCartesianCoord(pFan, numTransducer, beam, echoPos);
							}
							else
							{
								vectRealBeamFront = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, beam, echoPos);
							}
							frontFanLine.m_LowerAngleBeam.m_PointDown = vectRealBeamFront;
							echoPos = Vector3D(DepthPol*tan(-beamAngleAlong), DepthPol*tan(-beamAngleAthwart), DepthPol);
							Vector3D vectRealBeamBack;
							if (useGPSPositionning)
							{
								vectRealBeamBack = pSounder->GetSoftChannelCoordinateToCartesianCoord(pFan, numTransducer, beam, echoPos);
							}
							else
							{
								vectRealBeamBack = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, beam, echoPos);
							}
							backFanLine.m_LowerAngleBeam.m_PointDown = vectRealBeamBack;
						}
						if (AngleUp == pTransducer->getSoftChannelPolarX(beam)->m_mainBeamAthwartSteeringAngleRad)
						{
							DepthPol = std::max<double>(0.0, (pDisplay->GetMaxDepth() - pTransducer->m_transDepthMeter) / cos(AngleUp));
							if (pingMaxRange > 0)
							{
								DepthPol = std::min(DepthPol, pingMaxRange);
							}
							Vector3D echoPos(DepthPol*tan(beamAngleAlong), DepthPol*tan(beamAngleAthwart), DepthPol);
							Vector3D vectRealBeamFront;
							if (useGPSPositionning)
							{
								vectRealBeamFront = pSounder->GetSoftChannelCoordinateToCartesianCoord(pFan, numTransducer, beam, echoPos);
							}
							else
							{
								vectRealBeamFront = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, beam, echoPos);
							}
							frontFanLine.m_UpperAngleBeam.m_PointDown = vectRealBeamFront;
							Vector3D vectRealBeamBack;
							echoPos = Vector3D(DepthPol*tan(-beamAngleAlong), DepthPol*tan(beamAngleAthwart), DepthPol);
							if (useGPSPositionning)
							{
								vectRealBeamBack = pSounder->GetSoftChannelCoordinateToCartesianCoord(pFan, numTransducer, beam, echoPos);
							}
							else
							{
								vectRealBeamBack = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, beam, echoPos);
							}
							backFanLine.m_UpperAngleBeam.m_PointDown = vectRealBeamBack;
						}
					}

					// Don't display passive beams
					if (pFan->getBeam(pSoftChannel->getSoftwareChannelId())->second->m_transMode == BeamDataObject::PassiveTransducerMode)
						continue;

					Vector2I pos(beam, depthPolar);
					DataFmt *pVvalue = pPolarMem->GetDataFmt()->GetPointerToVoxel(pos);
					char *pFilter = pPolarMem->GetFilterFlag()->GetPointerToVoxel(pos);

					DataFmt value = *pVvalue;
					if (*pFilter)
						value = UNKNOWN_DB;

					maxValue = std::max(value, maxValue);
					if (!(depthPolar%nbSample == 0))
					{
						continue;
					}
					else 
					{
						value = maxValue;
						maxValue = UNKNOWN_DB;
					}

					// NMD - FAE121 - phase rejection
					//verification du seuil
					bool bReject = false;
					// OTK - FAE 186 - construction des �chos sous forme de points
					// pour affichage �ventuel dans ce mode m�me si pas de phase (on prend 0� dans ce cas)
					if (depthPolar < (sizeDataPolar.y))
					{
						if (!(*pFilter))
						{
							short sv = *(pVvalue);
							if (sv > pDisplay->GetMinThreshold() && sv < pDisplay->GetMaxThreshold())
							{
								double along = 0;
								double athwart = 0;
								if (pPolarMem->GetPhase())
								{
									Phase valueAngle = *((Phase*)pPolarMem->GetPhase()->GetPointerToVoxel(Vector2I(beam, depthPolar)));
									along = valueAngle.GetAlong();
									athwart = valueAngle.GetAthwart();
								}

								//verification du seuil
								if (abs(athwart) < AthwartSeuil && abs(along) < AlongSeuil)
								{
									if (pChannel3D->isVisible())
									{
										double temp = depthPolar * pTransducer->getBeamsSamplesSpacing() * cos(along) * cos(athwart);
										BaseMathLib::Vector3D coordSoftChannel(temp*tan(along), temp*tan(athwart), temp);

										BaseMathLib::Vector3D realPos;
										if (useGPSPositionning)
										{
											realPos = pSounder->GetSoftChannelCoordinateToCartesianCoord(pFan, numTransducer, beam, coordSoftChannel);
										}
										else
										{
											realPos = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, beam, coordSoftChannel);
										}

										if (realPos.z < pDisplay->GetMaxDepth() && realPos.z > pDisplay->GetMinDepth())
										{
											SingleEcho phaseEcho;
											phaseEcho.m_position = realPos;
											phaseEcho.m_value = sv;
											phaseEcho.m_chanId = pChannel3D->getChannel()->m_softChannelId;
											m_phaseEcho.push_back(phaseEcho);
										}
									}
								}
								else {
									bReject = true;
								}
							}
						}
					}

					// rejection du volume sample si on a rejet� la phase
					if (bReject)
						continue;

					/// value rejaction
					if (value < pDisplay->GetMinThreshold())
						continue;

					if (value > pDisplay->GetMaxThreshold())
						continue;


					Vector3D vectReal;
					if (useGPSPositionning)
					{
						vectReal = pSounder->GetPolarToCartesianCoord(pFan, numTransducer, beam, depthPolar);
					}
					else
					{
						vectReal = pSounder->GetPolarToWorldCoord(pFan, numTransducer, beam, depthPolar);
					}


					/// depht rejection
					if (vectReal.z > pDisplay->GetMaxDepth())
						continue;
					if (vectReal.z < pDisplay->GetMinDepth())
						continue;

					if (pDisplay->GetOverwriteScalarWithChannelId())
					{
						value = (float)(pTransducer->getSoftChannelPolarX(beam)->m_softChannelId);
					}

					
					double Yaw = pFan->GetNavAttributesRef(pSoftChannel->getSoftwareChannelId())->m_headingRad;

					//Vector3D scaleFactor(fabs(vectReal.y*tan(beamAngleAthwart)),fabs(sampleSpacing)*nbSample,fabs(vectReal.y*tan(beamAngleAlong)));
					Vector3D scaleFactor(2 * fabs((depthPolar*sampleSpacing)*tan(beamAngleAlong)), 2 * fabs((sampleSpacing*depthPolar)*tan(beamAngleAthwart)), nbSample*fabs(sampleSpacing));

					// prise en compte compression en z

					double Roll = 0;
					double Pitch = 0;
					pSounder->GetBeamOrientation(pingFan3d->getPingFan(), Roll, Pitch, beam);

					Vector3D pseudoReal = Vector3D::ToViewOpengl(vectReal);
					//	pseudoReal.y=pseudoReal.y-numTransducer;
					vtkIdType myId = pPoint->InsertNextPoint(pseudoReal.ptr());
					pStrenght->InsertNextValue(value);
					Vector3D appliedScale = Vector3D::ToViewOpengl(scaleFactor);
					pPointScale->InsertNextTuple3(appliedScale.x, appliedScale.y, appliedScale.z);

					Vector3D YawPitchRoll(-Yaw, Pitch, -Roll);
					pPointNormal->InsertNextTuple3(YawPitchRoll.x, YawPitchRoll.y, YawPitchRoll.z);

				}

				Box myBox(firstPoint, lastPoint);

				m_boundingBox.Extends(myBox);

				/// evaluate ground
				BottomPoint groundPoint;

				// Don't display passive beams
				if (pFan->getBeam(pSoftChannel->getSoftwareChannelId())->second->m_transMode != BeamDataObject::PassiveTransducerMode)
				{
					std::uint32_t echoFondEval;
					std::int32_t a;
					bool found;

					SoftChannel *pChan = pSounder->getSoftChannelPolarX(beam);
					pFan->getBottom(pChan->m_softChannelId, echoFondEval, a, found);
					// OTK - 18/01/2010 - pour avoir une grille r�guli�re des points du fond, on ajoute
					// n�cessairement un point m�me si le fond n'est pas reconnu.
					if (found)
					{
						groundPoint.m_found = true;
						groundPoint.m_numEcho = echoFondEval;
					}
				}

				this->m_bottomPoints.push_back(groundPoint);

				// end Ground stuff
			}
			if (numTransducer == 0)
			{
				pingAxisInfo.m_FrontLines = frontFanLine;
				pingAxisInfo.m_BackLines = backFanLine;
			}


		}

		// construct the grid
		m_pingView = new vtkPingView(pingFan3d->m_fanId);

		vtkUnstructuredGrid* pGridPoint = vtkUnstructuredGrid::New();
		pGridPoint->SetPoints(pPoint);
		pGridPoint->GetPointData()->SetScalars(pStrenght);
		pGridPoint->GetPointData()->SetVectors(pPointScale);

		pGridPoint->GetPointData()->SetNormals(pPointNormal);

		m_pingView->getGlyph()->SetInputData(pGridPoint);

		pPoint->Delete();
		pStrenght->Delete();
		pPointScale->Delete();

		pPointNormal->Delete();

		pGridPoint->Delete();



		/**
		Create Simple Echo stuff
		*/

		TSDisplayType tsDisplayType = pDisplay->GetTSDisplayType();
		M3DKernel *pKernel = M3DKernel::GetInstance();
		TSAnalysisModule * pTSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
		const std::map<std::uint64_t, TSTrack*> & tracks = pTSModule->getTracks();
		PingFanSingleTarget *pSingle = (PingFanSingleTarget *)pKernel->getObjectMgr()->GetPingFanContainer().GetSingleTargetContainer()->GetObjectWithDate(currentPingFanTime);
		if (pSingle)
		{
			for (unsigned int numTransducer = 0; numTransducer < pSounder->GetTransducerCount(); numTransducer++)
			{
				if (!pSounder3d->GetTransducerIdx(numTransducer)->isVisible())
					continue;
				Transducer *pTransducer = pSounder->GetTransducer(numTransducer);
				for (unsigned int beam = 0; beam < pTransducer->m_numberOfSoftChannel && beam >= 0; beam++)
				{
					Channel3D *pChan = pSounder3d->GetTransducerIdx(numTransducer)->getChannelWithIdx(beam);
					if (!pChan->isVisible())
						continue;

					for (unsigned int i = 0; i < pSingle->GetNumberOfTarget(); i++)
					{
						SingleTargetDataObject *pSingleTargetData = pSingle->GetTarget(i);
						SplitBeamPair ref;
						bool found = pKernel->getObjectMgr()->GetChannelParentSplitBeam(pSingleTargetData->m_parentSTId, ref);
						if (found && ref.m_channelId == pChan->getChannel()->m_softChannelId)
						{
							for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataCount(); numTarget++)
							{
								SingleEcho mySingleEcho;

								Vector3D mSoftCoord = pSingleTargetData->GetTargetPositionSoftChannelCoord(numTarget);

								if (useGPSPositionning)
								{
									mySingleEcho.m_position = pSounder->GetSoftChannelCoordinateToCartesianCoord(pFan, numTransducer, beam, mSoftCoord);
								}
								else
								{
									mySingleEcho.m_position = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, numTransducer, beam, mSoftCoord);
								}
								const SingleTargetData * singleTarget = &(pSingleTargetData->GetSingleTargetData(numTarget));
								double singleEchoValue;
								if (numTarget < pSingleTargetData->GetSingleTargetDataCWCount())
								{
									const SingleTargetDataCW * cwTS = static_cast<const SingleTargetDataCW*>(singleTarget);
									singleEchoValue = cwTS->m_compensatedTS;
								}
								else
								{
									const SingleTargetDataFM * fmTS = static_cast<const SingleTargetDataFM*>(singleTarget);
									singleEchoValue = fmTS->m_medianTS;
								}
								mySingleEcho.m_value = singleEchoValue * 100;
								mySingleEcho.m_chanId = pChan->getChannel()->m_softChannelId;
								/// depht rejection
								if (mySingleEcho.m_position.z > pDisplay->GetMaxDepth())
									continue;
								if (mySingleEcho.m_position.z < pDisplay->GetMinDepth())
									continue;

								/// on vire �ventuellement les �chos non track�s ou track�s
								if (tsDisplayType != eTSDisplayAll)
								{
									bool bIsTracked = false;
									if (singleTarget->m_trackLabel > 0)
									{
										std::map<std::uint64_t, TSTrack*>::const_iterator iterTrack = tracks.find(singleTarget->m_trackLabel);
										if (iterTrack != tracks.end())
										{
											bIsTracked = pTSModule->IsValidTrack(iterTrack->second);
										}
									}
									if (tsDisplayType == eTSDisplayTracked)
									{
										if (!bIsTracked)
										{
											continue;
										}
									}
									else
									{
										if (bIsTracked)
										{
											continue;
										}
									}
								}

								m_singleEcho.push_back(mySingleEcho);
							}
						}
					}
				}
			}
		}
	}
	return true;

}

bool Echo3DList::getBuildFlag()
{
	return this->m_pingView != NULL;
}