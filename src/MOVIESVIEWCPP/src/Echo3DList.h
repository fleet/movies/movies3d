#pragma once

class PingFan3D;
class PingAxisInformation;
class vtkPingView;

#include "BaseMathLib/Box.h"
#include "BaseMathLib/Vector3.h"
#include <vector>
#include "viewFilter.h"

class SingleEcho
{
public:
	SingleEcho() {}
	virtual ~SingleEcho() {}

	BaseMathLib::Vector3D	m_position;
	float					m_value;
	unsigned short			m_chanId;
};

class BottomPoint
{
public:
	BottomPoint() : m_numEcho(0), m_found(false) {}
	virtual ~BottomPoint() {}

	unsigned int m_numEcho;
	bool m_found;
};
class Echo3DList
{
public:
	Echo3DList();
	virtual ~Echo3DList();

	/** build an 3d echo list given the pingfan concerned and the filtering parameters */
	bool Build(ViewFilter &refViewFilter, PingFan3D *pingFan, PingAxisInformation& pingAxisInfo);

	void CheckUpdate(ViewFilter &refViewFilter, PingFan3D *pingFan3d);


	/** return true if data are already build, false otherwise*/
	bool getBuildFlag();

	/**return the pingView*/
	vtkPingView * getPingView() { return m_pingView; }
	BaseMathLib::Box m_boundingBox;

	std::vector<SingleEcho>				m_singleEcho;
	std::vector<BottomPoint>			m_bottomPoints;

	// NMD - 08/09/2011 - FAE 092 - echos representÚs par la phase
	std::vector<SingleEcho>       m_phaseEcho;

private:
	vtkPingView *m_pingView;
};
