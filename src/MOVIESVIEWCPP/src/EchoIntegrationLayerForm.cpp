// -*- C++ -*-
// ****************************************************************************
// Class: EchoIntegrationLayerForm
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Avril 2008
// Soci�t� : IPSIS
// ****************************************************************************
#include "EchoIntegrationLayerForm.h"

// d�pendances
#include "EchoIntegration/LayerDef.h"

#include <assert.h>

using namespace MOVIESVIEWCPP;


// ***************************************************************************
/**
 * Applique une valeur de textBox � une trackBar cible de precision decimation
 *
 * @date   24/04/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::TextBoxToTrackBar(TextBox^ sender, TrackBar^ target, Decimal decimation)
{

	assert(decimation != 0);

	try {
		// conversion du text de la textBox en double
		Decimal threshold = Convert::ToDecimal(sender->Text);

		// bornage du threshold aux limites de la trackbar
		threshold = threshold > target->Minimum ? threshold : target->Minimum;
		threshold = threshold < target->Maximum ? threshold : target->Maximum;

		// affectation de la nouvelle valeur � la trackBar
		target->Value = (int)(threshold / decimation);
	}
	catch (...)
	{
		// On ne prend pas en compte la nouvelle valeur si elle est incorrecte
	}
}

// ***************************************************************************
/**
 * Applique une valeur de trackBar de pr�cision decimation � une textBox cible
 *
 * @date   24/04/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::TrackBarToTextBox(TrackBar^ sender, TextBox^ target, Decimal decimation)
{
	target->Text = (sender->Value*decimation).ToString();
}


// ***************************************************************************
/**
 * R�ajuste les hauteurs de couches de surface en fonction du nouvel
 * offset de surface
 *
 * @date   24/04/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::UpdateSurfaceOffset()
{
	// on recup�re la diff�rence avec l'ancien offset qui correpond � la minDepth
	// de la premi�re couche
	Decimal offsetDelta = 0;
	Decimal initialValue = 0;
	if (layersListViewControl->Items->Count > 0)
	{
		// si on a une couche de surface (en supposant que la liste est tri�e)
		if (!String::Compare(layersListViewControl->Items[0]->Text, s_SURFACE_STR))
		{
			initialValue = Convert::ToDecimal(layersListViewControl->Items[0]->SubItems[1]->Text);
			offsetDelta = Convert::ToDecimal(surfLayersOffsetTextBox->Text) - initialValue;

			for (int i = 0; i < layersListViewControl->Items->Count; i++)
			{
				// on ne prend en compte que les couches de surface
				if (!String::Compare(layersListViewControl->Items[i]->Text, s_SURFACE_STR))
				{
					layersListViewControl->Items[i]->SubItems[1]->Text = Convert::ToString(
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text)
						+ offsetDelta
					);
					layersListViewControl->Items[i]->SubItems[2]->Text = Convert::ToString(
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text)
						+ offsetDelta
					);
				}
			}
		}
	}
}

// ***************************************************************************
/**
 * R�ajuste les hauteurs de couches de surface en fonction de la nouvelle
 * largeur des couches de surface
 *
 * @date   24/04/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::UpdateSurfaceWidth()
{
	Decimal newWidth = Convert::ToDecimal(surfLayersWidthTextBox->Text);
	// on part de la premi�re couche � la derni�re et on met a jour les profondeurs
	for (int i = 0; i < layersListViewControl->Items->Count; i++)
	{
		// on ne prend en compte que les couches de surface
		if (!String::Compare(layersListViewControl->Items[i]->Text, s_SURFACE_STR))
		{
			layersListViewControl->Items[i]->SubItems[2]->Text = Convert::ToString(
				Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text)
				+ newWidth
			);
		}
		if (i + 1 < layersListViewControl->Items->Count
			&& !String::Compare(layersListViewControl->Items[i + 1]->Text, s_SURFACE_STR)
			&& !String::Compare(layersListViewControl->Items[i]->Text, s_SURFACE_STR))
		{
			layersListViewControl->Items[i + 1]->SubItems[1]->Text =
				layersListViewControl->Items[i]->SubItems[2]->Text;
		}
	}
	// mise a jour de l'affichage des largeurs de couche
	UpdateLayerWidth();
}

// ***************************************************************************
/**
 * R�ajuste les hauteurs de couches en distance en fonction du nouvel
 * offset
 *
 * @date   25/03/2010 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::UpdateDistanceOffset()
{
	// on recup�re la diff�rence avec l'ancien offset qui correpond � la minDepth
	// de la premi�re couche
	Decimal offsetDelta = 0;
	Decimal initialValue = 0;
	bool found = false;
	for (int j = 0; j < layersListViewControl->Items->Count && !found; j++)
	{
		// si on a une couche en distance (en supposant que la liste est tri�e)
		if (!String::Compare(layersListViewControl->Items[j]->Text, s_DISTANCE_STR))
		{
			found = true;

			initialValue = Convert::ToDecimal(layersListViewControl->Items[j]->SubItems[1]->Text);
			offsetDelta = Convert::ToDecimal(distLayersOffsetTextBox->Text) - initialValue;

			for (int i = 0; i < layersListViewControl->Items->Count; i++)
			{
				// on ne prend en compte que les couches de surface
				if (!String::Compare(layersListViewControl->Items[i]->Text, s_DISTANCE_STR))
				{
					layersListViewControl->Items[i]->SubItems[1]->Text = Convert::ToString(
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text)
						+ offsetDelta
					);
					layersListViewControl->Items[i]->SubItems[2]->Text = Convert::ToString(
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text)
						+ offsetDelta
					);
				}
			}
		}
	}
}

// ***************************************************************************
/**
 * R�ajuste les hauteurs de couches en distance en fonction de la nouvelle
 * largeur des couches
 *
 * @date   25/03/2010 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::UpdateDistanceWidth()
{
	Decimal newWidth = Convert::ToDecimal(distLayersWidthTextBox->Text);
	// on part de la premi�re couche � la derni�re et on met a jour les profondeurs
	for (int i = 0; i < layersListViewControl->Items->Count; i++)
	{
		// on ne prend en compte que les couches de surface
		if (!String::Compare(layersListViewControl->Items[i]->Text, s_DISTANCE_STR))
		{
			layersListViewControl->Items[i]->SubItems[2]->Text = Convert::ToString(
				Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text)
				+ newWidth
			);
		}
		if (i + 1 < layersListViewControl->Items->Count
			&& !String::Compare(layersListViewControl->Items[i + 1]->Text, s_DISTANCE_STR)
			&& !String::Compare(layersListViewControl->Items[i]->Text, s_DISTANCE_STR))
		{
			layersListViewControl->Items[i + 1]->SubItems[1]->Text =
				layersListViewControl->Items[i]->SubItems[2]->Text;
		}
	}
	// mise a jour de l'affichage des largeurs de couche
	UpdateLayerWidth();
}

// ***************************************************************************
/**
 * mise a jour de l'affichage des largeurs de couche
 *
 * @date   24/04/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::UpdateLayerWidth()
{
	for (int i = 0; i < layersListViewControl->Items->Count; i++)
	{
		layersListViewControl->Items[i]->SubItems[3]->Text = Convert::ToString(
			Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text)
			-
			Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text)
		);
	}
}


// ***************************************************************************
/**
 * R�ajuste les hauteurs de couches de fond en fonction du nouvel
 * offset de fond
 *
 * @date   28/04/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::UpdateBottomOffset()
{
	// on recup�re la diff�rence avec l'ancien offset qui correpond � la minDepth
	// de la premi�re couche
	Decimal offsetDelta = 0;
	Decimal initialValue = Decimal::MaxValue;
	bool found = false;
	if (layersListViewControl->Items->Count > 0)
	{
		// on va chercher la premi�re couche de fond
		for (int i = 0; i < layersListViewControl->Items->Count; i++)
		{
			if (!String::Compare(layersListViewControl->Items[i]->Text, s_BOTTOM_STR))
			{
				if (initialValue > Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text))
				{
					initialValue = Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text);
					found = true;
				}
			}
		}

		if (found)
		{
			offsetDelta = Convert::ToDecimal(bottLayersOffsetTextBox->Text) - initialValue;

			for (int i = 0; i < layersListViewControl->Items->Count; i++)
			{
				// on ne prend en compte que les couches de surface
				if (!String::Compare(layersListViewControl->Items[i]->Text, s_BOTTOM_STR))
				{
					layersListViewControl->Items[i]->SubItems[1]->Text = Convert::ToString(
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text)
						+ offsetDelta
					);
					layersListViewControl->Items[i]->SubItems[2]->Text = Convert::ToString(
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text)
						+ offsetDelta
					);
				}
			}
		}
	}
}

// ***************************************************************************
/**
 * R�ajuste les hauteurs de couches de fond en fonction de la nouvelle
 * largeur des couches de fond
 *
 * @date   28/04/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::UpdateBottomWidth()
{
	Decimal newWidth = Convert::ToDecimal(bottLayersWidthTextBox->Text);
	// on part de la premi�re couche � la derni�re et on met a jour les profondeurs
	for (int i = 0; i < layersListViewControl->Items->Count; i++)
	{
		// on ne prend en compte que les couches de fond
		if (!String::Compare(layersListViewControl->Items[i]->Text, s_BOTTOM_STR))
		{
			layersListViewControl->Items[i]->SubItems[2]->Text = Convert::ToString(
				Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text)
				+ newWidth
			);
		}
		if (i + 1 < layersListViewControl->Items->Count
			&& !String::Compare(layersListViewControl->Items[i + 1]->Text, s_BOTTOM_STR)
			&& !String::Compare(layersListViewControl->Items[i]->Text, s_BOTTOM_STR))
		{
			layersListViewControl->Items[i + 1]->SubItems[1]->Text =
				layersListViewControl->Items[i]->SubItems[2]->Text;
		}
	}
	// mise a jour de l'affichage des largeurs de couche
	UpdateLayerWidth();
}


// ***************************************************************************
/**
 * mise � jour du ParameterModule associ� � l'IHM en fonction des
 * reglages IHM
 *
 * @date   16/05/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::IHMToConfig()
{
	// r�cup�ration du module d'echoIntegration
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	EchoIntegrationModule * pModule = pModuleMgr->GetEchoIntegrationModule();

	// recopie des param�tres
	// Sondeurs et Transducteurs
	pModule->GetEchoIntegrationParameter().setEnableTransducerFilter(!sounderTransducerTreeView->isAllchecked());
	pModule->GetEchoIntegrationParameter().clearActiveTransducerNames();	
	if (pModule->GetEchoIntegrationParameter().isEnableTransducerFilter())
	{
		pModule->GetEchoIntegrationParameter().addActiveTransducerNames(sounderTransducerTreeView->getCheckedTransducers());
	}
	
	// Seuils
	pModule->GetEchoIntegrationParameter().setLowThreshold(Convert::ToDouble(minThresholdTextBox->Text));
	pModule->GetEchoIntegrationParameter().setHighThreshold(Convert::ToDouble(maxThresholdTextBox->Text));

	// Offset de fond
	pModule->GetEchoIntegrationParameter().setOffsetBottom(Convert::ToDouble(bottLayersOffsetTextBox->Text));

	// Suppression des d�finitions de conches existantes 
	pModule->GetEchoIntegrationParameter().clearLayersDef();

	// On positionne le flag du mode couches quelconques en fonction des valeurs des couches
	auto customLayers = false;
	auto depth = 0.0;

	// définition des couches de surface
	auto firstBottomLayer = true;
	auto firstDistanceLayer = true;
	for (int i = 0; i < layersListViewControl->Items->Count; i++)
	{
		// Récupération des données de la ligne
		const auto layerStr = layersListViewControl->Items[i]->Text;
		const auto min = Convert::ToDouble(layersListViewControl->Items[i]->SubItems[1]->Text);
		const auto max = Convert::ToDouble(layersListViewControl->Items[i]->SubItems[2]->Text);
		
		// On détermine le type de couche
		auto layerType = Layer::Type::SurfaceLayer;
		if (String::Equals(layerStr, s_SURFACE_STR))
		{
			layerType = Layer::Type::SurfaceLayer;
			if (i > 0 && min != depth)
			{
				customLayers = true;
			}
		}
		else if (String::Equals(layerStr, s_BOTTOM_STR))
		{
			layerType = Layer::Type::BottomLayer;			
			if (!firstBottomLayer && min != depth)
			{
				customLayers = true;
			}
			firstBottomLayer = false;
		}
		else if (String::Equals(layerStr, s_DISTANCE_STR))
		{
			layerType = Layer::Type::DistanceLayer;
			if (!firstDistanceLayer && min != depth)
			{
				customLayers = true;
			}
			firstDistanceLayer = false;
		}
		else
		{
			continue;
		}
		
		depth = max;;

		// Création de la couche		
		const auto layer = pModule->GetEchoIntegrationParameter().addLayerDef(
			layerType,
			min,
			max
			);

		// Cas spécifique de minimum supérieur au maximum -> couche custom
		if (layer->GetMinDepth() > layer->GetMaxDepth())
		{
			customLayers = true;
		}
	}

	pModule->GetEchoIntegrationParameter().setCustomLayersMode(customLayers);
}

// ***************************************************************************
/**
 * mise � jour du de l'IHM � partir des param�tres du module associ�
 * reglages IHM
 *
 * @date   16/05/2008 - Olivier Tonck(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void EchoIntegrationLayerForm::ConfigToIHM()
{
	// r�cup�ration du module d'echoIntegration
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	EchoIntegrationModule * pModule = pModuleMgr->GetEchoIntegrationModule();

	// recopie des param�tres
	// Sondeurs et Transducteurs	
	if (pModule->GetEchoIntegrationParameter().isEnableTransducerFilter())
	{
		sounderTransducerTreeView->checkTransducers(pModule->GetEchoIntegrationParameter().getActiveTransducerNames());
	}
	else 
	{
		sounderTransducerTreeView->checkAll();
	}
	
	// Seuils
	minThresholdTextBox->Text = Convert::ToString(pModule->GetEchoIntegrationParameter().getLowThreshold());
	maxThresholdTextBox->Text = Convert::ToString(pModule->GetEchoIntegrationParameter().getHighThreshold());

	// Offset de fond
	bottLayersOffsetTextBox->Text = Convert::ToString(pModule->GetEchoIntegrationParameter().getOffsetBottom());
	
	// remplissage de la listView avec les couches
	// creationdes titres des colonnes
	ColumnHeader ^columnheader = gcnew ColumnHeader();
	columnheader->Text = "Layer type";
	this->layersListViewControl->Columns->Add(columnheader);
	columnheader = gcnew ColumnHeader();
	columnheader->Text = "Layer start";
	this->layersListViewControl->Columns->Add(columnheader);
	columnheader = gcnew ColumnHeader();
	columnheader->Text = "Layer end";
	this->layersListViewControl->Columns->Add(columnheader);
	columnheader = gcnew ColumnHeader();
	columnheader->Text = "Width";
	this->layersListViewControl->Columns->Add(columnheader);
	
	surfLayersOffsetTextBox->Text = "10";
	surfLayersWidthTextBox->Text = "30";
	bottLayersWidthTextBox->Text = "5";
	distLayersOffsetTextBox->Text = "0";
	distLayersWidthTextBox->Text = "100";

	bool firstSurfaceLayer = true;
	bool firstBottomLayer = true;
	bool firstDistanceLayer = true;
	for (const auto& layer : pModule->GetEchoIntegrationParameter().getLayersDefs())
	{
		ListViewItem ^ listviewitem = {};
		switch (layer->GetLayerType())
		{
		case Layer::Type::SurfaceLayer:
			listviewitem = gcnew ListViewItem(s_SURFACE_STR);
			if (firstSurfaceLayer)
			{
				firstSurfaceLayer = false;

				surfLayersOffsetTextBox->Text = Convert::ToString(layer->GetStart());
				surfLayersWidthTextBox->Text = Convert::ToString(layer->GetWidth());
			}
			break;
		case Layer::Type::BottomLayer:
			listviewitem = gcnew ListViewItem(s_BOTTOM_STR);
			if (firstBottomLayer)
			{
				firstBottomLayer = false;

				bottLayersOffsetTextBox->Text = Convert::ToString(layer->GetStart());
				bottLayersWidthTextBox->Text = Convert::ToString(layer->GetWidth());
			}
			break;
		case Layer::Type::DistanceLayer:
			listviewitem = gcnew ListViewItem(s_DISTANCE_STR);
			if (firstDistanceLayer)
			{
				firstDistanceLayer = false;

				distLayersOffsetTextBox->Text = Convert::ToString(layer->GetStart());
				distLayersWidthTextBox->Text = Convert::ToString(layer->GetWidth());
			}
			break;
		}
		listviewitem->SubItems->Add(Convert::ToString(layer->GetStart()));
		listviewitem->SubItems->Add(Convert::ToString(layer->GetEnd()));
		listviewitem->SubItems->Add(Convert::ToString(layer->GetWidth()));
		this->layersListViewControl->Items->Add(listviewitem);
	}
	
	// on redimensionne les colonnes en fonction de la taille des titres
	ColumnHeader ^ch;
	IEnumerator ^ie = this->layersListViewControl->Columns->GetEnumerator();
	while (ie->MoveNext())
	{
		ch = static_cast<ColumnHeader ^> (ie->Current);
		ch->Width = -2;
	}
	this->layersListViewControl->Columns[this->layersListViewControl->Columns->Count - 1]->AutoResize(ColumnHeaderAutoResizeStyle::HeaderSize);//Width
	this->layersListViewControl->Columns[this->layersListViewControl->Columns->Count - 1]->Width = this->layersListViewControl->Columns[this->layersListViewControl->Columns->Count - 2]->Width;

	UpdateLayerWidth();

	// activation / d�sactivation du module
	buttonStartEI->Enabled = !pModule->getEnable();
	buttonStopEI->Enabled = pModule->getEnable();

	// v�rouillage de l'IHM si le module est actif
	SetReadOnly(pModule->getEnable());
}

void EchoIntegrationLayerForm::SetReadOnly(bool readOnly)
{
	// seuils
	minThresholdTextBox->Enabled = !readOnly;
	minThresholdTrackBar->Enabled = !readOnly;
	maxThresholdTextBox->Enabled = !readOnly;
	maxThresholdTrackBar->Enabled = !readOnly;

	// couches
	surfLayersOffsetTextBox->Enabled = !readOnly;
	surfLayersOffsetTrackBar->Enabled = !readOnly;
	surfLayersWidthTextBox->Enabled = !readOnly;
	surfLayersWidthTrackBar->Enabled = !readOnly;
	distLayersOffsetTextBox->Enabled = !readOnly;
	distLayersOffsetTrackBar->Enabled = !readOnly;
	distLayersWidthTextBox->Enabled = !readOnly;
	distLayersWidthTrackBar->Enabled = !readOnly;
	bottLayersOffsetTextBox->Enabled = !readOnly;
	bottLayersOffsetTrackBar->Enabled = !readOnly;
	bottLayersWidthTextBox->Enabled = !readOnly;
	bottLayersWidthTrackBar->Enabled = !readOnly;
	layersListViewControl->Enabled = !readOnly;
	buttonAddLayer->Enabled = !readOnly;
	buttonRemoveLayer->Enabled = !readOnly;

	// autres
	buttonBroadcastAndRecord->Enabled = !readOnly;
}