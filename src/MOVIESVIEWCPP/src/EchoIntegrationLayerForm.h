#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

// D�pendances
#include "BroadcastAndRecordParameterForm.h"
#include "LayersListViewItemComparer.h"

#include "ModuleManager/ModuleManager.h"
#include "EchoIntegration/EchoIntegrationModule.h"

// d�marrage / arr�t des traitements
#include "TreatmentStartModeSelector.h"
#include "TreatmentStopModeSelector.h"

#include "SounderTransducerTreeView.h"

#include "ReaderCtrlManaged.h"

class EchoIntegrationParameter;
namespace MOVIESVIEWCPP {


	/// <summary>
	/// Description r�sum�e de EchoIntegrationLayerForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	ref class EchoIntegrationLayerForm : public System::Windows::Forms::Form
	{
	public:
		EchoIntegrationLayerForm(ReaderCtrlManaged^ %readerCtrl)
			: m_ReaderCtrlManaged(readerCtrl)
		{
			InitializeComponent();

			// configuration du tri des couches
			this->layersListViewControl->ListViewItemSorter = gcnew LayersListViewItemComparer();

			// mise � jour des composants avec les valeurs du module
			ConfigToIHM();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~EchoIntegrationLayerForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TrackBar^  minThresholdTrackBar;
	private: System::Windows::Forms::Label^  minThresholdLabel;
	private: System::Windows::Forms::TextBox^  minThresholdTextBox;
	private: System::Windows::Forms::GroupBox^  thresholdGroupBox;

	private: System::Windows::Forms::Label^  maxDBUnitLabel;
	private: System::Windows::Forms::TextBox^  maxThresholdTextBox;
	private: System::Windows::Forms::Label^  maxThresholdLabel;
	private: System::Windows::Forms::TrackBar^  maxThresholdTrackBar;


	private: System::Windows::Forms::Button^  buttonCancel;
	private: System::Windows::Forms::Button^  buttonOK;

	private: System::Windows::Forms::GroupBox^  groupBoxBroadcastAndRecord;
	private: System::Windows::Forms::Button^  buttonBroadcastAndRecord;


	private: System::Windows::Forms::ListView^  layersListViewControl;

	private: System::Windows::Forms::NumericUpDown^  numericUpDownLayerDepth;


	private: System::Windows::Forms::Label^  minDBUnitLabel;
	protected:

	private:

		static System::String^ s_SURFACE_STR = "Surface";
		static System::String^ s_BOTTOM_STR = "Bottom";
		static System::String^ s_DISTANCE_STR = "Distance";

		// IPSIS - OTK - item de la listView � modifier
		ListViewItem ^m_SelectedItem;

		// R�f�rence du readerControl pour les Lock du Monitor
		ReaderCtrlManaged^ m_ReaderCtrlManaged;
	private: System::Windows::Forms::TrackBar^  surfLayersOffsetTrackBar;
	private: System::Windows::Forms::Label^  labelSurfaceOffset;
	private: System::Windows::Forms::Label^  labelSurfWidth;
	private: System::Windows::Forms::TrackBar^  surfLayersWidthTrackBar;
	private: System::Windows::Forms::GroupBox^  groupBoxSurface;
	private: System::Windows::Forms::GroupBox^  groupBoxBottom;
	private: System::Windows::Forms::Label^  labelBottomWidth;
	private: System::Windows::Forms::TrackBar^  bottLayersWidthTrackBar;
	private: System::Windows::Forms::Label^  labelBottomOffset;
	private: System::Windows::Forms::TrackBar^  bottLayersOffsetTrackBar;


	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  labelUnit;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  buttonAddLayer;

	private: System::Windows::Forms::Button^  buttonRemoveLayer;
	private: System::Windows::Forms::TextBox^  surfLayersWidthTextBox;
	private: System::Windows::Forms::TextBox^  surfLayersOffsetTextBox;
	private: System::Windows::Forms::TextBox^  bottLayersWidthTextBox;
	private: System::Windows::Forms::TextBox^  bottLayersOffsetTextBox;
	private: System::Windows::Forms::Button^  buttonStartEI;
	private: System::Windows::Forms::Button^  buttonStopEI;
	private: System::Windows::Forms::GroupBox^  groupBoxLayersList;
	private: System::Windows::Forms::GroupBox^  groupBoxDistance;

	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  distLayersWidthTextBox;

	private: System::Windows::Forms::TextBox^  distLayersOffsetTextBox;

	private: System::Windows::Forms::Label^  labelDistanceWidth;
	private: System::Windows::Forms::TrackBar^  distLayersWidthTrackBar;


	private: System::Windows::Forms::Label^  labelDistanceOffset;
	private: System::Windows::Forms::TrackBar^  distLayersOffsetTrackBar;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;

private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel4;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel5;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel7;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel2;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel6;
private: System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanel1;
private: System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanel2;
private: System::Windows::Forms::GroupBox^  groupBoxSounderTransducerFilter;
private: SounderTransducerTreeView^  sounderTransducerTreeView;


private: System::Windows::Forms::ComboBox^  comboBoxLayerType;





	private:


		int m_SelectedColumnIndex;

		// transmet la valeur d'une textBox vers une trackbar de precision decimation
		// (pour g�rer les flotants sur les trackbars qui n'acceptent que les entiers)
		void TextBoxToTrackBar(TextBox^ sender, TrackBar^ target, Decimal decimation);

		// transmet la valeur d'une trackBar de precision decimation vers une textBox
		// (pour g�rer les flotants sur les trackbars qui n'acceptent que les entiers)
		void TrackBarToTextBox(TrackBar^ sender, TextBox^ target, Decimal decimation);

		// R�ajuste les hauteurs de couches de surface en fonction du nouvel
		// offset de surface
		void UpdateSurfaceOffset();

		// R�ajuste les hauteurs de couches de surface en fonction de la nouvelle largeur 
		// des couches de surface
		void UpdateSurfaceWidth();

		// mise a jour de l'affichage des largeurs de couche
		void UpdateLayerWidth();

		// R�ajuste les hauteurs de couches de fond en fonction du nouvel
		// offset de fond
		void UpdateBottomOffset();

		// R�ajuste les hauteurs de couches de fond en fonction de la nouvelle largeur 
		// des couches de fond
		void UpdateBottomWidth();

		// R�ajuste les hauteurs de couches en distance en fonction du nouvel
		// offset
		void UpdateDistanceOffset();

		// R�ajuste les hauteurs de couches en distance en fonction de la nouvelle largeur 
		// des couches
		void UpdateDistanceWidth();

		// mise � jour du module d'echoIntegration � partir des param�tres IHM
		void IHMToConfig();
		// mise � jour des param�tres IHM en fonction du module d'�choIntegration
		void ConfigToIHM();

		// active ou non les composants IHM en fonction de l'activit� du module
		// (on ne peut pas changer la configuration si le module est actif)
		void SetReadOnly(bool readOnly);

		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->minThresholdTrackBar = (gcnew System::Windows::Forms::TrackBar());
			this->minThresholdLabel = (gcnew System::Windows::Forms::Label());
			this->minThresholdTextBox = (gcnew System::Windows::Forms::TextBox());
			this->minDBUnitLabel = (gcnew System::Windows::Forms::Label());
			this->thresholdGroupBox = (gcnew System::Windows::Forms::GroupBox());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->maxDBUnitLabel = (gcnew System::Windows::Forms::Label());
			this->maxThresholdTextBox = (gcnew System::Windows::Forms::TextBox());
			this->maxThresholdLabel = (gcnew System::Windows::Forms::Label());
			this->maxThresholdTrackBar = (gcnew System::Windows::Forms::TrackBar());
			this->buttonCancel = (gcnew System::Windows::Forms::Button());
			this->buttonOK = (gcnew System::Windows::Forms::Button());
			this->groupBoxBroadcastAndRecord = (gcnew System::Windows::Forms::GroupBox());
			this->buttonBroadcastAndRecord = (gcnew System::Windows::Forms::Button());
			this->layersListViewControl = (gcnew System::Windows::Forms::ListView());
			this->numericUpDownLayerDepth = (gcnew System::Windows::Forms::NumericUpDown());
			this->surfLayersOffsetTrackBar = (gcnew System::Windows::Forms::TrackBar());
			this->labelSurfaceOffset = (gcnew System::Windows::Forms::Label());
			this->labelSurfWidth = (gcnew System::Windows::Forms::Label());
			this->surfLayersWidthTrackBar = (gcnew System::Windows::Forms::TrackBar());
			this->groupBoxSurface = (gcnew System::Windows::Forms::GroupBox());
			this->tableLayoutPanel4 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->labelUnit = (gcnew System::Windows::Forms::Label());
			this->surfLayersWidthTextBox = (gcnew System::Windows::Forms::TextBox());
			this->surfLayersOffsetTextBox = (gcnew System::Windows::Forms::TextBox());
			this->groupBoxBottom = (gcnew System::Windows::Forms::GroupBox());
			this->tableLayoutPanel5 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->labelBottomOffset = (gcnew System::Windows::Forms::Label());
			this->bottLayersWidthTextBox = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->labelBottomWidth = (gcnew System::Windows::Forms::Label());
			this->bottLayersOffsetTrackBar = (gcnew System::Windows::Forms::TrackBar());
			this->bottLayersOffsetTextBox = (gcnew System::Windows::Forms::TextBox());
			this->bottLayersWidthTrackBar = (gcnew System::Windows::Forms::TrackBar());
			this->buttonAddLayer = (gcnew System::Windows::Forms::Button());
			this->buttonRemoveLayer = (gcnew System::Windows::Forms::Button());
			this->buttonStartEI = (gcnew System::Windows::Forms::Button());
			this->buttonStopEI = (gcnew System::Windows::Forms::Button());
			this->groupBoxLayersList = (gcnew System::Windows::Forms::GroupBox());
			this->comboBoxLayerType = (gcnew System::Windows::Forms::ComboBox());
			this->tableLayoutPanel7 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->groupBoxDistance = (gcnew System::Windows::Forms::GroupBox());
			this->tableLayoutPanel6 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->labelDistanceOffset = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->labelDistanceWidth = (gcnew System::Windows::Forms::Label());
			this->distLayersWidthTextBox = (gcnew System::Windows::Forms::TextBox());
			this->distLayersOffsetTrackBar = (gcnew System::Windows::Forms::TrackBar());
			this->distLayersOffsetTextBox = (gcnew System::Windows::Forms::TextBox());
			this->distLayersWidthTrackBar = (gcnew System::Windows::Forms::TrackBar());
			this->tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->flowLayoutPanel1 = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->flowLayoutPanel2 = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->groupBoxSounderTransducerFilter = (gcnew System::Windows::Forms::GroupBox());
			this->sounderTransducerTreeView = (gcnew SounderTransducerTreeView());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->minThresholdTrackBar))->BeginInit();
			this->thresholdGroupBox->SuspendLayout();
			this->tableLayoutPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->maxThresholdTrackBar))->BeginInit();
			this->groupBoxBroadcastAndRecord->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownLayerDepth))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->surfLayersOffsetTrackBar))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->surfLayersWidthTrackBar))->BeginInit();
			this->groupBoxSurface->SuspendLayout();
			this->tableLayoutPanel4->SuspendLayout();
			this->groupBoxBottom->SuspendLayout();
			this->tableLayoutPanel5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bottLayersOffsetTrackBar))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bottLayersWidthTrackBar))->BeginInit();
			this->groupBoxLayersList->SuspendLayout();
			this->tableLayoutPanel7->SuspendLayout();
			this->groupBoxDistance->SuspendLayout();
			this->tableLayoutPanel6->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->distLayersOffsetTrackBar))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->distLayersWidthTrackBar))->BeginInit();
			this->tableLayoutPanel2->SuspendLayout();
			this->flowLayoutPanel1->SuspendLayout();
			this->flowLayoutPanel2->SuspendLayout();
			this->groupBoxSounderTransducerFilter->SuspendLayout();
			this->SuspendLayout();
			// 
			// minThresholdTrackBar
			// 
			this->minThresholdTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->minThresholdTrackBar->Location = System::Drawing::Point(53, 3);
			this->minThresholdTrackBar->Name = L"minThresholdTrackBar";
			this->minThresholdTrackBar->Size = System::Drawing::Size(150, 31);
			this->minThresholdTrackBar->TabIndex = 0;
			this->minThresholdTrackBar->TickFrequency = 100;
			this->minThresholdTrackBar->Scroll += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::minThresholdTrackBar_Scroll);
			// 
			// minThresholdLabel
			// 
			this->minThresholdLabel->AutoSize = true;
			this->minThresholdLabel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->minThresholdLabel->Location = System::Drawing::Point(3, 0);
			this->minThresholdLabel->Name = L"minThresholdLabel";
			this->minThresholdLabel->Size = System::Drawing::Size(44, 37);
			this->minThresholdLabel->TabIndex = 1;
			this->minThresholdLabel->Text = L"Min :";
			this->minThresholdLabel->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// minThresholdTextBox
			// 
			this->minThresholdTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->minThresholdTextBox->Location = System::Drawing::Point(209, 3);
			this->minThresholdTextBox->Name = L"minThresholdTextBox";
			this->minThresholdTextBox->Size = System::Drawing::Size(33, 20);
			this->minThresholdTextBox->TabIndex = 2;
			this->minThresholdTextBox->Text = L"-60";
			this->minThresholdTextBox->TextChanged += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::minThresholdTextBox_TextChanged);
			this->minThresholdTextBox->LostFocus += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::minThresholdTextBox_LostFocus);
			// 
			// minDBUnitLabel
			// 
			this->minDBUnitLabel->AutoSize = true;
			this->minDBUnitLabel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->minDBUnitLabel->Location = System::Drawing::Point(248, 0);
			this->minDBUnitLabel->Name = L"minDBUnitLabel";
			this->minDBUnitLabel->Size = System::Drawing::Size(26, 37);
			this->minDBUnitLabel->TabIndex = 3;
			this->minDBUnitLabel->Text = L"dB";
			// 
			// thresholdGroupBox
			// 
			this->thresholdGroupBox->Controls->Add(this->tableLayoutPanel1);
			this->thresholdGroupBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->thresholdGroupBox->Location = System::Drawing::Point(292, 3);
			this->thresholdGroupBox->Name = L"thresholdGroupBox";
			this->thresholdGroupBox->Size = System::Drawing::Size(283, 94);
			this->thresholdGroupBox->TabIndex = 4;
			this->thresholdGroupBox->TabStop = false;
			this->thresholdGroupBox->Text = L"Integration Thresholds";
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 4;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				50)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				80)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				20)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				32)));
			this->tableLayoutPanel1->Controls->Add(this->maxDBUnitLabel, 3, 1);
			this->tableLayoutPanel1->Controls->Add(this->maxThresholdTextBox, 2, 1);
			this->tableLayoutPanel1->Controls->Add(this->maxThresholdLabel, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->maxThresholdTrackBar, 1, 1);
			this->tableLayoutPanel1->Controls->Add(this->minDBUnitLabel, 3, 0);
			this->tableLayoutPanel1->Controls->Add(this->minThresholdTextBox, 2, 0);
			this->tableLayoutPanel1->Controls->Add(this->minThresholdLabel, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->minThresholdTrackBar, 1, 0);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(3, 16);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 2;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(277, 75);
			this->tableLayoutPanel1->TabIndex = 27;
			// 
			// maxDBUnitLabel
			// 
			this->maxDBUnitLabel->AutoSize = true;
			this->maxDBUnitLabel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->maxDBUnitLabel->Location = System::Drawing::Point(248, 37);
			this->maxDBUnitLabel->Name = L"maxDBUnitLabel";
			this->maxDBUnitLabel->Size = System::Drawing::Size(26, 38);
			this->maxDBUnitLabel->TabIndex = 7;
			this->maxDBUnitLabel->Text = L"dB";
			// 
			// maxThresholdTextBox
			// 
			this->maxThresholdTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->maxThresholdTextBox->Location = System::Drawing::Point(209, 40);
			this->maxThresholdTextBox->Name = L"maxThresholdTextBox";
			this->maxThresholdTextBox->Size = System::Drawing::Size(33, 20);
			this->maxThresholdTextBox->TabIndex = 6;
			this->maxThresholdTextBox->Text = L"0";
			this->maxThresholdTextBox->TextChanged += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::maxThresholdTextBox_TextChanged);
			this->maxThresholdTextBox->LostFocus += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::maxThresholdTextBox_LostFocus);
			// 
			// maxThresholdLabel
			// 
			this->maxThresholdLabel->AutoSize = true;
			this->maxThresholdLabel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->maxThresholdLabel->Location = System::Drawing::Point(3, 37);
			this->maxThresholdLabel->Name = L"maxThresholdLabel";
			this->maxThresholdLabel->Size = System::Drawing::Size(44, 38);
			this->maxThresholdLabel->TabIndex = 5;
			this->maxThresholdLabel->Text = L"Max :";
			this->maxThresholdLabel->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// maxThresholdTrackBar
			// 
			this->maxThresholdTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->maxThresholdTrackBar->Location = System::Drawing::Point(53, 40);
			this->maxThresholdTrackBar->Name = L"maxThresholdTrackBar";
			this->maxThresholdTrackBar->Size = System::Drawing::Size(150, 32);
			this->maxThresholdTrackBar->TabIndex = 4;
			this->maxThresholdTrackBar->TickFrequency = 100;
			this->maxThresholdTrackBar->Scroll += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::maxThresholdTrackBar_Scroll);
			// 
			// buttonCancel
			// 
			this->buttonCancel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->buttonCancel->Location = System::Drawing::Point(195, 3);
			this->buttonCancel->Name = L"buttonCancel";
			this->buttonCancel->Size = System::Drawing::Size(75, 23);
			this->buttonCancel->TabIndex = 10;
			this->buttonCancel->Text = L"Cancel";
			this->buttonCancel->UseVisualStyleBackColor = true;
			this->buttonCancel->Click += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::buttonCancel_Click);
			// 
			// buttonOK
			// 
			this->buttonOK->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->buttonOK->Location = System::Drawing::Point(114, 3);
			this->buttonOK->Name = L"buttonOK";
			this->buttonOK->Size = System::Drawing::Size(75, 23);
			this->buttonOK->TabIndex = 9;
			this->buttonOK->Text = L"OK";
			this->buttonOK->UseVisualStyleBackColor = true;
			this->buttonOK->Click += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::ButtonOK_Click);
			// 
			// groupBoxBroadcastAndRecord
			// 
			this->groupBoxBroadcastAndRecord->Controls->Add(this->buttonBroadcastAndRecord);
			this->groupBoxBroadcastAndRecord->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxBroadcastAndRecord->Location = System::Drawing::Point(292, 403);
			this->groupBoxBroadcastAndRecord->Name = L"groupBoxBroadcastAndRecord";
			this->groupBoxBroadcastAndRecord->Padding = System::Windows::Forms::Padding(15);
			this->groupBoxBroadcastAndRecord->Size = System::Drawing::Size(283, 79);
			this->groupBoxBroadcastAndRecord->TabIndex = 11;
			this->groupBoxBroadcastAndRecord->TabStop = false;
			this->groupBoxBroadcastAndRecord->Text = L"Broadcast and record";
			// 
			// buttonBroadcastAndRecord
			// 
			this->buttonBroadcastAndRecord->Dock = System::Windows::Forms::DockStyle::Top;
			this->buttonBroadcastAndRecord->Location = System::Drawing::Point(15, 28);
			this->buttonBroadcastAndRecord->Name = L"buttonBroadcastAndRecord";
			this->buttonBroadcastAndRecord->Size = System::Drawing::Size(253, 37);
			this->buttonBroadcastAndRecord->TabIndex = 0;
			this->buttonBroadcastAndRecord->Text = L"Broadcast and record Parameters...";
			this->buttonBroadcastAndRecord->UseVisualStyleBackColor = true;
			this->buttonBroadcastAndRecord->Click += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::buttonBroadcastAndRecord_Click);
			// 
			// layersListViewControl
			// 
			this->layersListViewControl->Dock = System::Windows::Forms::DockStyle::Fill;
			this->layersListViewControl->FullRowSelect = true;
			this->layersListViewControl->HeaderStyle = System::Windows::Forms::ColumnHeaderStyle::Nonclickable;
			this->layersListViewControl->HideSelection = false;
			this->layersListViewControl->Location = System::Drawing::Point(5, 18);
			this->layersListViewControl->Name = L"layersListViewControl";
			this->layersListViewControl->Size = System::Drawing::Size(273, 221);
			this->layersListViewControl->TabIndex = 13;
			this->layersListViewControl->UseCompatibleStateImageBehavior = false;
			this->layersListViewControl->View = System::Windows::Forms::View::Details;
			this->layersListViewControl->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &EchoIntegrationLayerForm::layersListViewControl_MouseUp);
			// 
			// numericUpDownLayerDepth
			// 
			this->numericUpDownLayerDepth->DecimalPlaces = 1;
			this->numericUpDownLayerDepth->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericUpDownLayerDepth->Location = System::Drawing::Point(159, 224);
			this->numericUpDownLayerDepth->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000000000, 0, 0, 0 });
			this->numericUpDownLayerDepth->Name = L"numericUpDownLayerDepth";
			this->numericUpDownLayerDepth->Size = System::Drawing::Size(50, 20);
			this->numericUpDownLayerDepth->TabIndex = 15;
			this->numericUpDownLayerDepth->Visible = false;
			this->numericUpDownLayerDepth->ValueChanged += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::numericUpDownLayerDepth_ValueChanged);
			this->numericUpDownLayerDepth->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &EchoIntegrationLayerForm::numericUpDownLayerDepth_KeyPress);
			this->numericUpDownLayerDepth->Leave += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::numericUpDownLayerDepth_Leave);
			// 
			// surfLayersOffsetTrackBar
			// 
			this->surfLayersOffsetTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->surfLayersOffsetTrackBar->Location = System::Drawing::Point(53, 3);
			this->surfLayersOffsetTrackBar->Maximum = 5000;
			this->surfLayersOffsetTrackBar->Name = L"surfLayersOffsetTrackBar";
			this->surfLayersOffsetTrackBar->Size = System::Drawing::Size(150, 31);
			this->surfLayersOffsetTrackBar->TabIndex = 16;
			this->surfLayersOffsetTrackBar->TickFrequency = 500;
			this->surfLayersOffsetTrackBar->Scroll += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::surfLayersOffsetTrackBar_Scroll);
			// 
			// labelSurfaceOffset
			// 
			this->labelSurfaceOffset->AutoSize = true;
			this->labelSurfaceOffset->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelSurfaceOffset->Location = System::Drawing::Point(3, 0);
			this->labelSurfaceOffset->Name = L"labelSurfaceOffset";
			this->labelSurfaceOffset->Size = System::Drawing::Size(44, 37);
			this->labelSurfaceOffset->TabIndex = 17;
			this->labelSurfaceOffset->Text = L"Offset :";
			this->labelSurfaceOffset->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// labelSurfWidth
			// 
			this->labelSurfWidth->AutoSize = true;
			this->labelSurfWidth->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelSurfWidth->Location = System::Drawing::Point(3, 37);
			this->labelSurfWidth->Name = L"labelSurfWidth";
			this->labelSurfWidth->Size = System::Drawing::Size(44, 38);
			this->labelSurfWidth->TabIndex = 19;
			this->labelSurfWidth->Text = L"Width :";
			this->labelSurfWidth->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// surfLayersWidthTrackBar
			// 
			this->surfLayersWidthTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->surfLayersWidthTrackBar->Location = System::Drawing::Point(53, 40);
			this->surfLayersWidthTrackBar->Maximum = 5000;
			this->surfLayersWidthTrackBar->Name = L"surfLayersWidthTrackBar";
			this->surfLayersWidthTrackBar->Size = System::Drawing::Size(150, 32);
			this->surfLayersWidthTrackBar->TabIndex = 18;
			this->surfLayersWidthTrackBar->TickFrequency = 500;
			this->surfLayersWidthTrackBar->Scroll += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::surfLayersWidthTrackBar_Scroll);
			// 
			// groupBoxSurface
			// 
			this->groupBoxSurface->Controls->Add(this->tableLayoutPanel4);
			this->groupBoxSurface->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxSurface->Location = System::Drawing::Point(292, 103);
			this->groupBoxSurface->Name = L"groupBoxSurface";
			this->groupBoxSurface->Size = System::Drawing::Size(283, 94);
			this->groupBoxSurface->TabIndex = 20;
			this->groupBoxSurface->TabStop = false;
			this->groupBoxSurface->Text = L"Surface";
			// 
			// tableLayoutPanel4
			// 
			this->tableLayoutPanel4->ColumnCount = 4;
			this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				50)));
			this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				80)));
			this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				20)));
			this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				32)));
			this->tableLayoutPanel4->Controls->Add(this->label1, 3, 1);
			this->tableLayoutPanel4->Controls->Add(this->labelSurfaceOffset, 0, 0);
			this->tableLayoutPanel4->Controls->Add(this->labelUnit, 3, 0);
			this->tableLayoutPanel4->Controls->Add(this->labelSurfWidth, 0, 1);
			this->tableLayoutPanel4->Controls->Add(this->surfLayersWidthTextBox, 2, 1);
			this->tableLayoutPanel4->Controls->Add(this->surfLayersOffsetTrackBar, 1, 0);
			this->tableLayoutPanel4->Controls->Add(this->surfLayersOffsetTextBox, 2, 0);
			this->tableLayoutPanel4->Controls->Add(this->surfLayersWidthTrackBar, 1, 1);
			this->tableLayoutPanel4->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel4->Location = System::Drawing::Point(3, 16);
			this->tableLayoutPanel4->Name = L"tableLayoutPanel4";
			this->tableLayoutPanel4->RowCount = 2;
			this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel4->Size = System::Drawing::Size(277, 75);
			this->tableLayoutPanel4->TabIndex = 28;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label1->Location = System::Drawing::Point(248, 37);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(26, 38);
			this->label1->TabIndex = 23;
			this->label1->Text = L"m";
			// 
			// labelUnit
			// 
			this->labelUnit->AutoSize = true;
			this->labelUnit->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelUnit->Location = System::Drawing::Point(248, 0);
			this->labelUnit->Name = L"labelUnit";
			this->labelUnit->Size = System::Drawing::Size(26, 37);
			this->labelUnit->TabIndex = 22;
			this->labelUnit->Text = L"m";
			// 
			// surfLayersWidthTextBox
			// 
			this->surfLayersWidthTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->surfLayersWidthTextBox->Location = System::Drawing::Point(209, 40);
			this->surfLayersWidthTextBox->Name = L"surfLayersWidthTextBox";
			this->surfLayersWidthTextBox->Size = System::Drawing::Size(33, 20);
			this->surfLayersWidthTextBox->TabIndex = 21;
			this->surfLayersWidthTextBox->TextChanged += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::surfLayersWidthTextBox_TextChanged);
			// 
			// surfLayersOffsetTextBox
			// 
			this->surfLayersOffsetTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->surfLayersOffsetTextBox->Location = System::Drawing::Point(209, 3);
			this->surfLayersOffsetTextBox->Name = L"surfLayersOffsetTextBox";
			this->surfLayersOffsetTextBox->Size = System::Drawing::Size(33, 20);
			this->surfLayersOffsetTextBox->TabIndex = 20;
			this->surfLayersOffsetTextBox->TextChanged += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::surfLayersOffsetTextBox_TextChanged);
			// 
			// groupBoxBottom
			// 
			this->groupBoxBottom->Controls->Add(this->tableLayoutPanel5);
			this->groupBoxBottom->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxBottom->Location = System::Drawing::Point(292, 203);
			this->groupBoxBottom->Name = L"groupBoxBottom";
			this->groupBoxBottom->Size = System::Drawing::Size(283, 94);
			this->groupBoxBottom->TabIndex = 21;
			this->groupBoxBottom->TabStop = false;
			this->groupBoxBottom->Text = L"Bottom";
			// 
			// tableLayoutPanel5
			// 
			this->tableLayoutPanel5->ColumnCount = 4;
			this->tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				50)));
			this->tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				80)));
			this->tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				20)));
			this->tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				32)));
			this->tableLayoutPanel5->Controls->Add(this->label3, 3, 1);
			this->tableLayoutPanel5->Controls->Add(this->labelBottomOffset, 0, 0);
			this->tableLayoutPanel5->Controls->Add(this->bottLayersWidthTextBox, 2, 1);
			this->tableLayoutPanel5->Controls->Add(this->label2, 3, 0);
			this->tableLayoutPanel5->Controls->Add(this->labelBottomWidth, 0, 1);
			this->tableLayoutPanel5->Controls->Add(this->bottLayersOffsetTrackBar, 1, 0);
			this->tableLayoutPanel5->Controls->Add(this->bottLayersOffsetTextBox, 2, 0);
			this->tableLayoutPanel5->Controls->Add(this->bottLayersWidthTrackBar, 1, 1);
			this->tableLayoutPanel5->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel5->Location = System::Drawing::Point(3, 16);
			this->tableLayoutPanel5->Name = L"tableLayoutPanel5";
			this->tableLayoutPanel5->RowCount = 2;
			this->tableLayoutPanel5->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel5->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel5->Size = System::Drawing::Size(277, 75);
			this->tableLayoutPanel5->TabIndex = 29;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label3->Location = System::Drawing::Point(248, 37);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(26, 38);
			this->label3->TabIndex = 25;
			this->label3->Text = L"m";
			// 
			// labelBottomOffset
			// 
			this->labelBottomOffset->AutoSize = true;
			this->labelBottomOffset->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelBottomOffset->Location = System::Drawing::Point(3, 0);
			this->labelBottomOffset->Name = L"labelBottomOffset";
			this->labelBottomOffset->Size = System::Drawing::Size(44, 37);
			this->labelBottomOffset->TabIndex = 17;
			this->labelBottomOffset->Text = L"Offset :";
			this->labelBottomOffset->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// bottLayersWidthTextBox
			// 
			this->bottLayersWidthTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->bottLayersWidthTextBox->Location = System::Drawing::Point(209, 40);
			this->bottLayersWidthTextBox->Name = L"bottLayersWidthTextBox";
			this->bottLayersWidthTextBox->Size = System::Drawing::Size(33, 20);
			this->bottLayersWidthTextBox->TabIndex = 23;
			this->bottLayersWidthTextBox->TextChanged += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::bottLayersWidthTextBox_TextChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label2->Location = System::Drawing::Point(248, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(26, 37);
			this->label2->TabIndex = 24;
			this->label2->Text = L"m";
			// 
			// labelBottomWidth
			// 
			this->labelBottomWidth->AutoSize = true;
			this->labelBottomWidth->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelBottomWidth->Location = System::Drawing::Point(3, 37);
			this->labelBottomWidth->Name = L"labelBottomWidth";
			this->labelBottomWidth->Size = System::Drawing::Size(44, 38);
			this->labelBottomWidth->TabIndex = 19;
			this->labelBottomWidth->Text = L"Width :";
			this->labelBottomWidth->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// bottLayersOffsetTrackBar
			// 
			this->bottLayersOffsetTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->bottLayersOffsetTrackBar->Location = System::Drawing::Point(53, 3);
			this->bottLayersOffsetTrackBar->Maximum = 5000;
			this->bottLayersOffsetTrackBar->Name = L"bottLayersOffsetTrackBar";
			this->bottLayersOffsetTrackBar->Size = System::Drawing::Size(150, 31);
			this->bottLayersOffsetTrackBar->TabIndex = 16;
			this->bottLayersOffsetTrackBar->TickFrequency = 500;
			this->bottLayersOffsetTrackBar->Scroll += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::bottLayersOffsetTrackBar_Scroll);
			// 
			// bottLayersOffsetTextBox
			// 
			this->bottLayersOffsetTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->bottLayersOffsetTextBox->Location = System::Drawing::Point(209, 3);
			this->bottLayersOffsetTextBox->Name = L"bottLayersOffsetTextBox";
			this->bottLayersOffsetTextBox->Size = System::Drawing::Size(33, 20);
			this->bottLayersOffsetTextBox->TabIndex = 22;
			this->bottLayersOffsetTextBox->TextChanged += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::bottLayersOffsetTextBox_TextChanged);
			// 
			// bottLayersWidthTrackBar
			// 
			this->bottLayersWidthTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->bottLayersWidthTrackBar->Location = System::Drawing::Point(53, 40);
			this->bottLayersWidthTrackBar->Maximum = 5000;
			this->bottLayersWidthTrackBar->Name = L"bottLayersWidthTrackBar";
			this->bottLayersWidthTrackBar->Size = System::Drawing::Size(150, 32);
			this->bottLayersWidthTrackBar->TabIndex = 18;
			this->bottLayersWidthTrackBar->TickFrequency = 500;
			this->bottLayersWidthTrackBar->Scroll += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::bottLayersWidthTrackBar_Scroll);
			// 
			// buttonAddLayer
			// 
			this->buttonAddLayer->Anchor = System::Windows::Forms::AnchorStyles::Right;
			this->buttonAddLayer->Location = System::Drawing::Point(35, 5);
			this->buttonAddLayer->Margin = System::Windows::Forms::Padding(3, 3, 20, 3);
			this->buttonAddLayer->Name = L"buttonAddLayer";
			this->buttonAddLayer->Size = System::Drawing::Size(81, 25);
			this->buttonAddLayer->TabIndex = 22;
			this->buttonAddLayer->Text = L"Add layer";
			this->buttonAddLayer->UseVisualStyleBackColor = true;
			this->buttonAddLayer->Click += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::buttonAddLayer_Click);
			// 
			// buttonRemoveLayer
			// 
			this->buttonRemoveLayer->Anchor = System::Windows::Forms::AnchorStyles::Left;
			this->buttonRemoveLayer->Location = System::Drawing::Point(156, 5);
			this->buttonRemoveLayer->Margin = System::Windows::Forms::Padding(20, 3, 3, 3);
			this->buttonRemoveLayer->Name = L"buttonRemoveLayer";
			this->buttonRemoveLayer->Size = System::Drawing::Size(90, 25);
			this->buttonRemoveLayer->TabIndex = 23;
			this->buttonRemoveLayer->Text = L"Remove layer";
			this->buttonRemoveLayer->UseVisualStyleBackColor = true;
			this->buttonRemoveLayer->Click += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::buttonRemoveLayer_Click);
			// 
			// buttonStartEI
			// 
			this->buttonStartEI->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->buttonStartEI->BackColor = System::Drawing::Color::PaleGreen;
			this->buttonStartEI->Location = System::Drawing::Point(13, 3);
			this->buttonStartEI->Name = L"buttonStartEI";
			this->buttonStartEI->Size = System::Drawing::Size(75, 23);
			this->buttonStartEI->TabIndex = 24;
			this->buttonStartEI->Text = L"Start EI";
			this->buttonStartEI->UseVisualStyleBackColor = false;
			this->buttonStartEI->Click += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::buttonStartEI_Click);
			// 
			// buttonStopEI
			// 
			this->buttonStopEI->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->buttonStopEI->BackColor = System::Drawing::Color::Tomato;
			this->buttonStopEI->Location = System::Drawing::Point(94, 3);
			this->buttonStopEI->Name = L"buttonStopEI";
			this->buttonStopEI->Size = System::Drawing::Size(75, 23);
			this->buttonStopEI->TabIndex = 25;
			this->buttonStopEI->Text = L"Stop EI";
			this->buttonStopEI->UseVisualStyleBackColor = false;
			this->buttonStopEI->Click += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::buttonStopEI_Click);
			// 
			// groupBoxLayersList
			// 
			this->groupBoxLayersList->Controls->Add(this->layersListViewControl);
			this->groupBoxLayersList->Controls->Add(this->comboBoxLayerType);
			this->groupBoxLayersList->Controls->Add(this->tableLayoutPanel7);
			this->groupBoxLayersList->Controls->Add(this->numericUpDownLayerDepth);
			this->groupBoxLayersList->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxLayersList->Location = System::Drawing::Point(3, 203);
			this->groupBoxLayersList->Name = L"groupBoxLayersList";
			this->groupBoxLayersList->Padding = System::Windows::Forms::Padding(5);
			this->tableLayoutPanel2->SetRowSpan(this->groupBoxLayersList, 3);
			this->groupBoxLayersList->Size = System::Drawing::Size(283, 279);
			this->groupBoxLayersList->TabIndex = 26;
			this->groupBoxLayersList->TabStop = false;
			this->groupBoxLayersList->Text = L"Layers";
			// 
			// comboBoxLayerType
			// 
			this->comboBoxLayerType->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxLayerType->FormattingEnabled = true;
			this->comboBoxLayerType->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"Surface", L"Bottom", L"Distance" });
			this->comboBoxLayerType->Location = System::Drawing::Point(88, 129);
			this->comboBoxLayerType->Name = L"comboBoxLayerType";
			this->comboBoxLayerType->Size = System::Drawing::Size(121, 21);
			this->comboBoxLayerType->TabIndex = 25;
			this->comboBoxLayerType->Visible = false;
			this->comboBoxLayerType->SelectedIndexChanged += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::comboBoxLayerType_SelectedValueChanged);
			this->comboBoxLayerType->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &EchoIntegrationLayerForm::comboBoxLayerType_KeyPress);
			this->comboBoxLayerType->Leave += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::comboBoxLayerType_Leave);
			// 
			// tableLayoutPanel7
			// 
			this->tableLayoutPanel7->ColumnCount = 2;
			this->tableLayoutPanel7->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->tableLayoutPanel7->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->tableLayoutPanel7->Controls->Add(this->buttonAddLayer, 0, 0);
			this->tableLayoutPanel7->Controls->Add(this->buttonRemoveLayer, 1, 0);
			this->tableLayoutPanel7->Dock = System::Windows::Forms::DockStyle::Bottom;
			this->tableLayoutPanel7->Location = System::Drawing::Point(5, 239);
			this->tableLayoutPanel7->Name = L"tableLayoutPanel7";
			this->tableLayoutPanel7->RowCount = 1;
			this->tableLayoutPanel7->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel7->Size = System::Drawing::Size(273, 35);
			this->tableLayoutPanel7->TabIndex = 24;
			// 
			// groupBoxDistance
			// 
			this->groupBoxDistance->Controls->Add(this->tableLayoutPanel6);
			this->groupBoxDistance->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxDistance->Location = System::Drawing::Point(292, 303);
			this->groupBoxDistance->Name = L"groupBoxDistance";
			this->groupBoxDistance->Size = System::Drawing::Size(283, 94);
			this->groupBoxDistance->TabIndex = 26;
			this->groupBoxDistance->TabStop = false;
			this->groupBoxDistance->Text = L"Distance";
			// 
			// tableLayoutPanel6
			// 
			this->tableLayoutPanel6->ColumnCount = 4;
			this->tableLayoutPanel6->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				50)));
			this->tableLayoutPanel6->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				80)));
			this->tableLayoutPanel6->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				20)));
			this->tableLayoutPanel6->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				32)));
			this->tableLayoutPanel6->Controls->Add(this->label4, 3, 1);
			this->tableLayoutPanel6->Controls->Add(this->labelDistanceOffset, 0, 0);
			this->tableLayoutPanel6->Controls->Add(this->label5, 3, 0);
			this->tableLayoutPanel6->Controls->Add(this->labelDistanceWidth, 0, 1);
			this->tableLayoutPanel6->Controls->Add(this->distLayersWidthTextBox, 2, 1);
			this->tableLayoutPanel6->Controls->Add(this->distLayersOffsetTrackBar, 1, 0);
			this->tableLayoutPanel6->Controls->Add(this->distLayersOffsetTextBox, 2, 0);
			this->tableLayoutPanel6->Controls->Add(this->distLayersWidthTrackBar, 1, 1);
			this->tableLayoutPanel6->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel6->Location = System::Drawing::Point(3, 16);
			this->tableLayoutPanel6->Name = L"tableLayoutPanel6";
			this->tableLayoutPanel6->RowCount = 2;
			this->tableLayoutPanel6->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel6->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel6->Size = System::Drawing::Size(277, 75);
			this->tableLayoutPanel6->TabIndex = 30;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label4->Location = System::Drawing::Point(248, 37);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(26, 38);
			this->label4->TabIndex = 25;
			this->label4->Text = L"m";
			// 
			// labelDistanceOffset
			// 
			this->labelDistanceOffset->AutoSize = true;
			this->labelDistanceOffset->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelDistanceOffset->Location = System::Drawing::Point(3, 0);
			this->labelDistanceOffset->Name = L"labelDistanceOffset";
			this->labelDistanceOffset->Size = System::Drawing::Size(44, 37);
			this->labelDistanceOffset->TabIndex = 17;
			this->labelDistanceOffset->Text = L"Offset :";
			this->labelDistanceOffset->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label5->Location = System::Drawing::Point(248, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(26, 37);
			this->label5->TabIndex = 24;
			this->label5->Text = L"m";
			// 
			// labelDistanceWidth
			// 
			this->labelDistanceWidth->AutoSize = true;
			this->labelDistanceWidth->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelDistanceWidth->Location = System::Drawing::Point(3, 37);
			this->labelDistanceWidth->Name = L"labelDistanceWidth";
			this->labelDistanceWidth->Size = System::Drawing::Size(44, 38);
			this->labelDistanceWidth->TabIndex = 19;
			this->labelDistanceWidth->Text = L"Width :";
			this->labelDistanceWidth->TextAlign = System::Drawing::ContentAlignment::TopRight;
			// 
			// distLayersWidthTextBox
			// 
			this->distLayersWidthTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->distLayersWidthTextBox->Location = System::Drawing::Point(209, 40);
			this->distLayersWidthTextBox->Name = L"distLayersWidthTextBox";
			this->distLayersWidthTextBox->Size = System::Drawing::Size(33, 20);
			this->distLayersWidthTextBox->TabIndex = 23;
			this->distLayersWidthTextBox->TextChanged += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::distLayersWidthTextBox_TextChanged);
			// 
			// distLayersOffsetTrackBar
			// 
			this->distLayersOffsetTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->distLayersOffsetTrackBar->Location = System::Drawing::Point(53, 3);
			this->distLayersOffsetTrackBar->Maximum = 5000;
			this->distLayersOffsetTrackBar->Name = L"distLayersOffsetTrackBar";
			this->distLayersOffsetTrackBar->Size = System::Drawing::Size(150, 31);
			this->distLayersOffsetTrackBar->TabIndex = 16;
			this->distLayersOffsetTrackBar->TickFrequency = 500;
			this->distLayersOffsetTrackBar->Scroll += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::distLayersOffsetTrackBar_Scroll);
			// 
			// distLayersOffsetTextBox
			// 
			this->distLayersOffsetTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->distLayersOffsetTextBox->Location = System::Drawing::Point(209, 3);
			this->distLayersOffsetTextBox->Name = L"distLayersOffsetTextBox";
			this->distLayersOffsetTextBox->Size = System::Drawing::Size(33, 20);
			this->distLayersOffsetTextBox->TabIndex = 22;
			this->distLayersOffsetTextBox->TextChanged += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::distLayersOffsetTextBox_TextChanged);
			// 
			// distLayersWidthTrackBar
			// 
			this->distLayersWidthTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->distLayersWidthTrackBar->Location = System::Drawing::Point(53, 40);
			this->distLayersWidthTrackBar->Maximum = 5000;
			this->distLayersWidthTrackBar->Name = L"distLayersWidthTrackBar";
			this->distLayersWidthTrackBar->Size = System::Drawing::Size(150, 32);
			this->distLayersWidthTrackBar->TabIndex = 18;
			this->distLayersWidthTrackBar->TickFrequency = 500;
			this->distLayersWidthTrackBar->Scroll += gcnew System::EventHandler(this, &EchoIntegrationLayerForm::distLayersWidthTrackBar_Scroll);
			// 
			// tableLayoutPanel2
			// 
			this->tableLayoutPanel2->ColumnCount = 2;
			this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->tableLayoutPanel2->Controls->Add(this->groupBoxSurface, 1, 1);
			this->tableLayoutPanel2->Controls->Add(this->groupBoxLayersList, 0, 2);
			this->tableLayoutPanel2->Controls->Add(this->groupBoxDistance, 1, 3);
			this->tableLayoutPanel2->Controls->Add(this->thresholdGroupBox, 1, 0);
			this->tableLayoutPanel2->Controls->Add(this->groupBoxBroadcastAndRecord, 1, 4);
			this->tableLayoutPanel2->Controls->Add(this->groupBoxBottom, 1, 2);
			this->tableLayoutPanel2->Controls->Add(this->flowLayoutPanel1, 1, 5);
			this->tableLayoutPanel2->Controls->Add(this->flowLayoutPanel2, 0, 5);
			this->tableLayoutPanel2->Controls->Add(this->groupBoxSounderTransducerFilter, 0, 0);
			this->tableLayoutPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel2->Location = System::Drawing::Point(3, 3);
			this->tableLayoutPanel2->Name = L"tableLayoutPanel2";
			this->tableLayoutPanel2->RowCount = 6;
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 100)));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 100)));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 100)));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 100)));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 40)));
			this->tableLayoutPanel2->Size = System::Drawing::Size(578, 525);
			this->tableLayoutPanel2->TabIndex = 27;
			// 
			// flowLayoutPanel1
			// 
			this->flowLayoutPanel1->Controls->Add(this->buttonCancel);
			this->flowLayoutPanel1->Controls->Add(this->buttonOK);
			this->flowLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->flowLayoutPanel1->FlowDirection = System::Windows::Forms::FlowDirection::RightToLeft;
			this->flowLayoutPanel1->Location = System::Drawing::Point(292, 488);
			this->flowLayoutPanel1->Name = L"flowLayoutPanel1";
			this->flowLayoutPanel1->Padding = System::Windows::Forms::Padding(0, 0, 10, 0);
			this->flowLayoutPanel1->Size = System::Drawing::Size(283, 34);
			this->flowLayoutPanel1->TabIndex = 0;
			// 
			// flowLayoutPanel2
			// 
			this->flowLayoutPanel2->Controls->Add(this->buttonStartEI);
			this->flowLayoutPanel2->Controls->Add(this->buttonStopEI);
			this->flowLayoutPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->flowLayoutPanel2->Location = System::Drawing::Point(3, 488);
			this->flowLayoutPanel2->Name = L"flowLayoutPanel2";
			this->flowLayoutPanel2->Padding = System::Windows::Forms::Padding(10, 0, 0, 0);
			this->flowLayoutPanel2->Size = System::Drawing::Size(283, 34);
			this->flowLayoutPanel2->TabIndex = 1;
			// 
			// groupBoxSounderTransducerFilter
			// 
			this->groupBoxSounderTransducerFilter->Controls->Add(this->sounderTransducerTreeView);
			this->groupBoxSounderTransducerFilter->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxSounderTransducerFilter->Location = System::Drawing::Point(3, 3);
			this->groupBoxSounderTransducerFilter->Name = L"groupBoxSounderTransducerFilter";
			this->groupBoxSounderTransducerFilter->Padding = System::Windows::Forms::Padding(5);
			this->tableLayoutPanel2->SetRowSpan(this->groupBoxSounderTransducerFilter, 2);
			this->groupBoxSounderTransducerFilter->Size = System::Drawing::Size(283, 194);
			this->groupBoxSounderTransducerFilter->TabIndex = 27;
			this->groupBoxSounderTransducerFilter->TabStop = false;
			this->groupBoxSounderTransducerFilter->Text = L"Sounder / Transducer";
			// 
			// sounderTransducerTreeView
			// 
			this->sounderTransducerTreeView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->sounderTransducerTreeView->Location = System::Drawing::Point(5, 35);
			this->sounderTransducerTreeView->Name = L"sounderTransducerTreeView";
			this->sounderTransducerTreeView->Size = System::Drawing::Size(273, 154);
			this->sounderTransducerTreeView->TabIndex = 0;
			this->sounderTransducerTreeView->setTransducerFilterActive(false);
			// 
			// EchoIntegrationLayerForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(584, 531);
			this->Controls->Add(this->tableLayoutPanel2);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->MaximumSize = System::Drawing::Size(1200, 1400);
			this->MinimumSize = System::Drawing::Size(547, 470);
			this->Name = L"EchoIntegrationLayerForm";
			this->Padding = System::Windows::Forms::Padding(3);
			this->Text = L"EchoIntegrationLayerForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->minThresholdTrackBar))->EndInit();
			this->thresholdGroupBox->ResumeLayout(false);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->maxThresholdTrackBar))->EndInit();
			this->groupBoxBroadcastAndRecord->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownLayerDepth))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->surfLayersOffsetTrackBar))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->surfLayersWidthTrackBar))->EndInit();
			this->groupBoxSurface->ResumeLayout(false);
			this->tableLayoutPanel4->ResumeLayout(false);
			this->tableLayoutPanel4->PerformLayout();
			this->groupBoxBottom->ResumeLayout(false);
			this->tableLayoutPanel5->ResumeLayout(false);
			this->tableLayoutPanel5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bottLayersOffsetTrackBar))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bottLayersWidthTrackBar))->EndInit();
			this->groupBoxLayersList->ResumeLayout(false);
			this->tableLayoutPanel7->ResumeLayout(false);
			this->groupBoxDistance->ResumeLayout(false);
			this->tableLayoutPanel6->ResumeLayout(false);
			this->tableLayoutPanel6->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->distLayersOffsetTrackBar))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->distLayersWidthTrackBar))->EndInit();
			this->tableLayoutPanel2->ResumeLayout(false);
			this->flowLayoutPanel1->ResumeLayout(false);
			this->flowLayoutPanel2->ResumeLayout(false);
			this->groupBoxSounderTransducerFilter->ResumeLayout(false);
			this->groupBoxSounderTransducerFilter->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

#pragma region Action sur les composants IHM des thresholds

		// IPSIS - OTK - Modification du min threshold
	private: System::Void minThresholdTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e)
	{
		try {
			// mise � jour des composants associ�s (trackBar)
			TextBoxToTrackBar(minThresholdTextBox, minThresholdTrackBar, Decimal(0.1));

			// Dans le cas ou le threshold max est inf�rieur au threshold min,
			// on remet le max � la valeur du min
			if (Convert::ToDecimal(minThresholdTextBox->Text) > Convert::ToDecimal(maxThresholdTextBox->Text))
			{
				Decimal maxThreshold = maxThresholdTrackBar->Maximum*Decimal(0.1) < Convert::ToDecimal(minThresholdTextBox->Text) ?
					maxThresholdTrackBar->Maximum*Decimal(0.1) : Convert::ToDecimal(minThresholdTextBox->Text);
				// mise � jour du texte
				maxThresholdTextBox->Text = maxThreshold.ToString();
				// mise � jour de la trackBar
				maxThresholdTrackBar->Value = maxThreshold.ToInt32(maxThreshold) * 10;
			}
		}
		catch (...)
		{
			// comme on met � jour en direct, lorsqu'on tappe un threshold n�gatif par exemple,
			// le '-' n'est psa une valeur valide. on ne fait rien
		}
	}

			 // IPSIS - OTK - validation du min threshold
	private: System::Void minThresholdTextBox_LostFocus(System::Object^  sender, System::EventArgs^  e)
	{
		try {
			// si la valeur n'est pas comprise dans les bornes, on la ram�ne dans la fourchette
			if (Convert::ToDecimal(minThresholdTextBox->Text) < minThresholdTrackBar->Minimum*Decimal(0.1))
			{
				minThresholdTextBox->Text = (minThresholdTrackBar->Minimum*Decimal(0.1)).ToString();
			}
			else if (Convert::ToDecimal(minThresholdTextBox->Text) > minThresholdTrackBar->Maximum*Decimal(0.1))
			{
				minThresholdTextBox->Text = (minThresholdTrackBar->Maximum*Decimal(0.1)).ToString();
			}
		}
		catch (...)
		{
			minThresholdTextBox->Text = "-60";
		}

		TextBoxToTrackBar(minThresholdTextBox, minThresholdTrackBar, Decimal(0.1));
	}

			 // IPSIS - OTK - Modification du min threshold
	private: System::Void minThresholdTrackBar_Scroll(System::Object^  sender, System::EventArgs^  e)
	{
		// mise � jour des composants associ�s (textbox)
		TrackBarToTextBox(minThresholdTrackBar, minThresholdTextBox, Decimal(0.1));

		// Dans le cas ou le threshold max est inf�rieur au threshold min,
		// on remet le max � la valeur du min
		if (minThresholdTrackBar->Value > maxThresholdTrackBar->Value)
		{
			// mise � jour du texte
			maxThresholdTextBox->Text = minThresholdTextBox->Text;
			// mise � jour de la trackBar
			maxThresholdTrackBar->Value = minThresholdTrackBar->Value;
		}
	}

			 // IPSIS - OTK - Modification du max threshold
	private: System::Void maxThresholdTrackBar_Scroll(System::Object^  sender, System::EventArgs^  e)
	{
		// mise � jour des composants associ�s (textbox)
		TrackBarToTextBox(maxThresholdTrackBar, maxThresholdTextBox, Decimal(0.1));

		// Dans le cas ou le threshold max est inf�rieur au threshold min,
		// on remet le max � la valeur du min
		if (minThresholdTrackBar->Value > maxThresholdTrackBar->Value)
		{
			// mise � jour du texte
			minThresholdTextBox->Text = maxThresholdTextBox->Text;
			// mise � jour de la trackBar
			minThresholdTrackBar->Value = maxThresholdTrackBar->Value;
		}
	}

			 // IPSIS - OTK - Modification du max threshold
	private: System::Void maxThresholdTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e)
	{
		try
		{
			// mise � jour des composants associ�s (trackBar)
			TextBoxToTrackBar(maxThresholdTextBox, maxThresholdTrackBar, Decimal(0.1));

			// Dans le cas ou le threshold max est inf�rieur au threshold min,
			// on remet le max � la valeur du min
			if (Convert::ToDecimal(minThresholdTextBox->Text) > Convert::ToDecimal(maxThresholdTextBox->Text))
			{
				// mise � jour de la trackBar
				minThresholdTrackBar->Value = maxThresholdTrackBar->Value;
				Decimal minThreshold = minThresholdTrackBar->Minimum*Decimal(0.1) > Convert::ToDecimal(maxThresholdTextBox->Text) ?
					minThresholdTrackBar->Minimum*Decimal(0.1) : Convert::ToDecimal(maxThresholdTextBox->Text);
				// mise � jour du texte
				minThresholdTextBox->Text = minThreshold.ToString();
				// mise � jour de la trackBar
				minThresholdTrackBar->Value = minThreshold.ToInt32(minThreshold) * 10;
			}
		}
		catch (...)
		{
			// comme on met � jour en direct, lorsqu'on tappe un threshold n�gatif par exemple,
			// le '-' n'est psa une valeur valide. on ne fait rien
		}
	}

			 // IPSIS - OTK - validation du max threshold
	private: System::Void maxThresholdTextBox_LostFocus(System::Object^  sender, System::EventArgs^  e)
	{
		try {
			// si la valeur n'est pas comprise dans les bornes, on la ram�ne dans la fourchette
			if (Convert::ToDecimal(maxThresholdTextBox->Text) < maxThresholdTrackBar->Minimum*Decimal(0.1))
			{
				maxThresholdTextBox->Text = (maxThresholdTrackBar->Minimum*Decimal(0.1)).ToString();
			}
			else if (Convert::ToDecimal(maxThresholdTextBox->Text) > maxThresholdTrackBar->Maximum*Decimal(0.1))
			{
				maxThresholdTextBox->Text = (maxThresholdTrackBar->Maximum*Decimal(0.1)).ToString();
			}
		}
		catch (...)
		{
			maxThresholdTextBox->Text = "0";
		}

		TextBoxToTrackBar(maxThresholdTextBox, maxThresholdTrackBar, Decimal(0.1));
	}

#pragma endregion

			 // IPSIS - OTK - renvoie l'item � la position x,y
	private: int GetSubItemAt(int x, int y, ListViewItem^ %item)
	{
		item = this->layersListViewControl->GetItemAt(x, y);

		if (item)
		{
			System::Drawing::Rectangle lviBounds;
			int	subItemX;

			lviBounds = item->GetBounds(ItemBoundsPortion::Entire);
			subItemX = lviBounds.Left;
			for (int i = 0; i < this->layersListViewControl->Columns->Count; i++)
			{
				ColumnHeader ^h = this->layersListViewControl->Columns[i];
				if (x < subItemX + h->Width)
				{
					return h->Index;
				}
				subItemX += h->Width;
			}
		}

		return -1;
	}


#pragma region Action sur les composants IHM des couches de surface


			 // IPSIS - OTK - Modification de l'offset des couches de surface
	private: System::Void surfLayersOffsetTrackBar_Scroll(System::Object^  sender, System::EventArgs^  e)
	{
		// mise � jour des composants associ�s (textbox)
		TrackBarToTextBox(surfLayersOffsetTrackBar, surfLayersOffsetTextBox, Decimal(0.1));
	}

			 // IPSIS - OTK - Modification de l'offset des couches de surface
	private: System::Void surfLayersOffsetTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e)
	{
		try
		{
			// recalcul des couches
			UpdateSurfaceOffset();

			// mise � jour des composants associ�s (trackBar)
			TextBoxToTrackBar(surfLayersOffsetTextBox, surfLayersOffsetTrackBar, Decimal(0.1));
		}
		catch (...)
		{
			// comme on met � jour en direct, lorsqu'on tappe un threshold n�gatif par exemple,
			// le '-' n'est pas une valeur valide. on ne fait rien. La validation est faite sur la perte de focus 
			// du controle

		}
	}

			 // IPSIS - OTK - Modification de la largeur des couches de surface
	private: System::Void surfLayersWidthTrackBar_Scroll(System::Object^  sender, System::EventArgs^  e)
	{
		// mise � jour des composants associ�s (textbox)
		TrackBarToTextBox(surfLayersWidthTrackBar, surfLayersWidthTextBox, Decimal(0.1));
	}

			 // IPSIS - OTK - Modification de la largeur des couches de surface
	private: System::Void surfLayersWidthTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e)
	{
		try
		{
			// recalcul des couches
			UpdateSurfaceWidth();

			// mise � jour des composants associ�s (trackBar)
			TextBoxToTrackBar(surfLayersWidthTextBox, surfLayersWidthTrackBar, Decimal(0.1));
		}
		catch (...)
		{
			// comme on met � jour en direct, lorsqu'on tappe un threshold n�gatif par exemple,
			// le '-' n'est psa une valeur valide. on ne fait rien
		}
	}
#pragma endregion



#pragma region Action sur les composants IHM des couches de fond


			 // OTK - IPSIS - modification de l'offset via la trackbar
	private: System::Void bottLayersOffsetTrackBar_Scroll(System::Object^  sender, System::EventArgs^  e)
	{
		// mise � jour des composants associ�s (textbox)
		TrackBarToTextBox(bottLayersOffsetTrackBar, bottLayersOffsetTextBox, Decimal(0.1));
	}

	private: System::Void bottLayersOffsetTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e)
	{
		try
		{
			// recalcul des couches
			UpdateBottomOffset();

			// mise � jour des composants associ�s (trackBar)
			TextBoxToTrackBar(bottLayersOffsetTextBox, bottLayersOffsetTrackBar, Decimal(0.1));
		}
		catch (...)
		{
			// comme on met � jour en direct, lorsqu'on tappe un threshold n�gatif par exemple,
			// le '-' n'est psa une valeur valide. on ne fait rien
		}
	}

			 // OTK - IPSIS - mise a jour de la largeur des couches de fond via la trackbar
	private: System::Void bottLayersWidthTrackBar_Scroll(System::Object^  sender, System::EventArgs^  e)
	{
		// mise � jour des composants associ�s (textbox)
		TrackBarToTextBox(bottLayersWidthTrackBar, bottLayersWidthTextBox, Decimal(0.1));
	}

			 // OTK - IPSIS - mise a jour de la largeur des couches de fond via la textBox
	private: System::Void bottLayersWidthTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e)
	{
		try
		{
			// recalcul des couches
			UpdateBottomWidth();

			// mise � jour des composants associ�s (trackBar)
			TextBoxToTrackBar(bottLayersWidthTextBox, bottLayersWidthTrackBar, Decimal(0.1));
		}
		catch (...)
		{
			// comme on met � jour en direct, lorsqu'on tappe un threshold n�gatif par exemple,
			// le '-' n'est psa une valeur valide. on ne fait rien
		}
	}
#pragma endregion



#pragma region Action sur les composants IHM des couches en distance

			 // IPSIS - OTK - Modification de l'offset des couches en distance
	private: System::Void distLayersOffsetTrackBar_Scroll(System::Object^  sender, System::EventArgs^  e)
	{
		// mise � jour des composants associ�s (textbox)
		TrackBarToTextBox(distLayersOffsetTrackBar, distLayersOffsetTextBox, Decimal(0.1));
	}

			 // IPSIS - OTK - Modification de l'offset des couches en distance
	private: System::Void distLayersOffsetTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e)
	{
		try
		{
			// recalcul des couches
			UpdateDistanceOffset();

			// mise � jour des composants associ�s (trackBar)
			TextBoxToTrackBar(distLayersOffsetTextBox, distLayersOffsetTrackBar, Decimal(0.1));
		}
		catch (...)
		{
			// comme on met � jour en direct, lorsqu'on tappe un threshold n�gatif par exemple,
			// le '-' n'est pas une valeur valide. on ne fait rien. La validation est faite sur la perte de focus 
			// du controle

		}
	}

			 // IPSIS - OTK - Modification de la largeur des couches en distance
	private: System::Void distLayersWidthTrackBar_Scroll(System::Object^  sender, System::EventArgs^  e)
	{
		// mise � jour des composants associ�s (textbox)
		TrackBarToTextBox(distLayersWidthTrackBar, distLayersWidthTextBox, Decimal(0.1));
	}

			 // IPSIS - OTK - Modification de la largeur des couches en distance
	private: System::Void distLayersWidthTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e)
	{
		try
		{
			// recalcul des couches
			UpdateDistanceWidth();

			// mise � jour des composants associ�s (trackBar)
			TextBoxToTrackBar(distLayersWidthTextBox, distLayersWidthTrackBar, Decimal(0.1));
		}
		catch (...)
		{
			// comme on met � jour en direct, lorsqu'on tappe un threshold n�gatif par exemple,
			// le '-' n'est psa une valeur valide. on ne fait rien
		}
	}
#pragma endregion

			 // IPSIS - OTK - Bouton annuler
	private: System::Void buttonCancel_Click(System::Object^  sender, System::EventArgs^  e)
	{
		Close();
	}

			 // IPSIS - OTK - Bouton Ok
	private: System::Void ButtonOK_Click(System::Object^  sender, System::EventArgs^  e)
	{
		try
		{
			IHMToConfig();
			Close();
		}
		catch (Exception^)
		{
			MessageBox::Show("Invalid configuration.");
		}
	}

			 // IPSIS - OTK - Bouton Param�tres broadcast et r�seau
	private: System::Void buttonBroadcastAndRecord_Click(System::Object^  sender, System::EventArgs^  e) {
		// r�cup�ration du module d'echoIntegration
		CModuleManager * pModuleMgr = CModuleManager::getInstance();
		EchoIntegrationModule * pModule = pModuleMgr->GetEchoIntegrationModule();

		BroadcastAndRecordParameterForm ^ref = gcnew BroadcastAndRecordParameterForm(&pModule->GetEchoIntegrationParameter().GetParameterBroadcastAndRecord());
		ref->ShowDialog();
	}

			 // IPSIS - OTK - changement de valeur fond / surface / distance
	private: System::Void comboBoxLayerType_SelectedValueChanged(System::Object^  sender, System::EventArgs^  e) {
		System::String^ tempStr = m_SelectedItem->Text;
		int selectedIndex = m_SelectedItem->Index;
		m_SelectedItem->Text = this->comboBoxLayerType->Text;

		this->comboBoxLayerType->Visible = false;

		// si on change le type d'une couche, il faut modifier ses coordonn�es pour �tre compatibles avec les couches de l'autre type
		if (System::String::Compare(tempStr, m_SelectedItem->Text))
		{
			// On doit combler le vide cr�� par le d�placement de la couche
			// on d�cale toutes les couches "au dessus" de la largeur de la couche modifi�e.
			Decimal freedWidth = Convert::ToDecimal(m_SelectedItem->SubItems[2]->Text) -
				Convert::ToDecimal(m_SelectedItem->SubItems[1]->Text);
			for (int i = selectedIndex; i < layersListViewControl->Items->Count; i++)
			{
				if (!System::String::Compare(tempStr, layersListViewControl->Items[i]->Text))
				{
					layersListViewControl->Items[i]->SubItems[1]->Text = Convert::ToString(
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text) - freedWidth
					);
					layersListViewControl->Items[i]->SubItems[2]->Text = Convert::ToString(
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text) - freedWidth
					);
				}
			}

			// r�initialisation des coordonn�es de la couche 
			m_SelectedItem->SubItems[1]->Text = "0";
			m_SelectedItem->SubItems[2]->Text = "0";
			// r�cup�ration de la derni�re couche du nouveau type de couche
			Decimal newValue, newWidth;
			if (!String::Compare(m_SelectedItem->Text, s_SURFACE_STR))
			{
				newValue = surfLayersOffsetTrackBar->Value * Decimal(0.1);
				newWidth = surfLayersWidthTrackBar->Value * Decimal(0.1);
			}
			else if (!String::Compare(m_SelectedItem->Text, s_BOTTOM_STR))
			{
				newValue = bottLayersOffsetTrackBar->Value * Decimal(0.1);
				newWidth = bottLayersWidthTrackBar->Value * Decimal(0.1);
			}
			else if (!String::Compare(m_SelectedItem->Text, s_DISTANCE_STR))
			{
				newValue = distLayersOffsetTrackBar->Value * Decimal(0.1);
				newWidth = distLayersWidthTrackBar->Value * Decimal(0.1);
			}
			for (int i = 0; i < layersListViewControl->Items->Count; i++)
			{
				if (!System::String::Compare(layersListViewControl->Items[i]->Text, m_SelectedItem->Text) && Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text) > newValue)
				{
					newValue = Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text);
				}
			}
			m_SelectedItem->SubItems[1]->Text = Convert::ToString(newValue);
			m_SelectedItem->SubItems[2]->Text = Convert::ToString(newValue + newWidth);
		}

		// tri de la liste
		layersListViewControl->Sort();

		UpdateLayerWidth();
	}

			 // IPSIS - OTK - changement de valeur fond / surface
	private: System::Void comboBoxLayerType_Leave(System::Object^  sender, System::EventArgs^  e) {
		m_SelectedItem->Text = this->comboBoxLayerType->Text;

		this->comboBoxLayerType->Visible = false;

		// tri de la liste
		layersListViewControl->Sort();

		UpdateLayerWidth();
	}

			 // IPSIS - OTK - changement de valeur fond / surface / distance
	private: System::Void comboBoxLayerType_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
		switch (e->KeyChar)
		{
		case (char)(int)Keys::Escape:
		{
			// Reset de la valeur du text originale
			this->comboBoxLayerType->Text = m_SelectedItem->Text;
			this->comboBoxLayerType->Visible = false;
			break;
		}
		case (char)(int)Keys::Enter:
		{
			this->comboBoxLayerType->Visible = false;
			break;
		}
		}
	}

			 // IPSIS - OTK - changement de valeur de profondeur
	private: System::Void numericUpDownLayerDepth_Leave(System::Object^  sender, System::EventArgs^  e) {
		m_SelectedItem->SubItems[m_SelectedColumnIndex]->Text = Convert::ToString(this->numericUpDownLayerDepth->Value);

		// Hide the ComboBox control.
		this->numericUpDownLayerDepth->Visible = false;

		UpdateLayerWidth();
	}

			 // IPSIS - OTK - changement de valeur de profondeur
	private: System::Void numericUpDownLayerDepth_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
		switch (e->KeyChar)
		{
		case (char)(int)Keys::Escape:
		{
			// Reset de la valeur du text originale
			this->numericUpDownLayerDepth->Value = Convert::ToDecimal(m_SelectedItem->SubItems[m_SelectedColumnIndex]->Text);
			this->numericUpDownLayerDepth->Visible = false;
			break;
		}
		case (char)(int)Keys::Enter:
		{
			this->numericUpDownLayerDepth->Visible = false;
			break;
		}
		}
	}

			 // IPSIS - OTK - popup du control d'�dition sur clic d'un �l�ment de la listView
	private: System::Void layersListViewControl_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {

		// on r�cup�re l'Item cliqu� avec l'index la colonne
		m_SelectedColumnIndex = GetSubItemAt(e->X, e->Y, m_SelectedItem);

		if (m_SelectedColumnIndex >= 0)
		{
			// En fonction de l'index de la colonne, on r�cupere le rectangle dans lequel faire apparaitre
			// le controle qui sert � l'�dition
			System::Drawing::Rectangle ClickedItem = m_SelectedItem->Bounds;
			for (int i = 0; i < m_SelectedColumnIndex; i++)
			{
				ClickedItem.X += this->layersListViewControl->Columns[i]->Width;
			}

			ClickedItem.Width = this->layersListViewControl->Columns[m_SelectedColumnIndex]->Width;

			// on prend en compte le positionnement relatif de la listview dans la fen�tre
			ClickedItem.Y += this->layersListViewControl->Top;
			ClickedItem.X += this->layersListViewControl->Left;

			// on redimensionne le bon composant d'�dition et on l'affiche
			switch (m_SelectedColumnIndex)
			{
			case 0:
				// cas de l'�dition du type de couche
				this->comboBoxLayerType->Bounds = ClickedItem;
				this->comboBoxLayerType->Text = m_SelectedItem->Text;
				this->comboBoxLayerType->Visible = true;
				this->comboBoxLayerType->BringToFront();
				this->comboBoxLayerType->Focus();
				break;
			case 1:
			case 2:
				// cas de l'�dition des limites d'une couche
				this->numericUpDownLayerDepth->Bounds = ClickedItem;
				this->numericUpDownLayerDepth->Value = Convert::ToDecimal(m_SelectedItem->SubItems[m_SelectedColumnIndex]->Text);
				this->numericUpDownLayerDepth->Visible = true;
				this->numericUpDownLayerDepth->BringToFront();
				this->numericUpDownLayerDepth->Focus();
				break;
			}
		}
	}

			 // IPSIS - OTK - modification de la valeur d'une profondeur de couche
	private: System::Void numericUpDownLayerDepth_ValueChanged(System::Object^  sender, System::EventArgs^  e) {

		// on commence par mettre � jour la liste 
		m_SelectedItem->SubItems[m_SelectedColumnIndex]->Text = Convert::ToString(this->numericUpDownLayerDepth->Value);

		// cas ou on met � jour un minDepth
		if (m_SelectedColumnIndex == 1)
		{
			// PREMIERE PASSE DANS LE SENS DES COUCHES CROISSANT
			for (int i = m_SelectedItem->Index; i < layersListViewControl->Items->Count; i++)
			{
				// seulement pour les couches du meme type
				if ((i + 1 < layersListViewControl->Items->Count) && layersListViewControl->Items[i + 1]->Text == m_SelectedItem->Text)
				{
					if (Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text) <
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text))
					{
						layersListViewControl->Items[i]->SubItems[2]->Text =
							layersListViewControl->Items[i]->SubItems[1]->Text;
						layersListViewControl->Items[i + 1]->SubItems[1]->Text =
							layersListViewControl->Items[i]->SubItems[2]->Text;
					}
				}
				else
				{
					if (Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text) <
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text))
					{
						layersListViewControl->Items[i]->SubItems[2]->Text =
							layersListViewControl->Items[i]->SubItems[1]->Text;
					}
				}
			}
			// DEUXIEME PASSE DANS LE SENS DES COUCHES DECROISSANT
			for (int i = m_SelectedItem->Index; i > 0; i--)
			{
				// seulement pour les couches du meme type
				if (layersListViewControl->Items[i - 1]->Text == m_SelectedItem->Text)
				{
					layersListViewControl->Items[i - 1]->SubItems[2]->Text =
						layersListViewControl->Items[i]->SubItems[1]->Text;

					if (Convert::ToDecimal(layersListViewControl->Items[i - 1]->SubItems[2]->Text) <
						Convert::ToDecimal(layersListViewControl->Items[i - 1]->SubItems[1]->Text))
					{
						layersListViewControl->Items[i - 1]->SubItems[1]->Text =
							layersListViewControl->Items[i - 1]->SubItems[2]->Text;
					}
				}
			}
		}

		// cas ou on met � jour un maxDepth
		if (m_SelectedColumnIndex == 2)
		{
			// PREMIERE PASSE DANS LE SENS DES COUCHES CROISSANT
			for (int i = m_SelectedItem->Index; i < layersListViewControl->Items->Count; i++)
			{
				// seulement pour les couches du meme type
				if (i + 1 < layersListViewControl->Items->Count && layersListViewControl->Items[i + 1]->Text == m_SelectedItem->Text)
				{
					layersListViewControl->Items[i + 1]->SubItems[1]->Text =
						layersListViewControl->Items[i]->SubItems[2]->Text;
					if (Convert::ToDecimal(layersListViewControl->Items[i + 1]->SubItems[2]->Text) <
						Convert::ToDecimal(layersListViewControl->Items[i + 1]->SubItems[1]->Text))
					{
						layersListViewControl->Items[i + 1]->SubItems[2]->Text =
							layersListViewControl->Items[i + 1]->SubItems[1]->Text;
					}
				}
			}
			// DEUXIEME PASSE DANS LE SENS DES COUCHES DECROISSANT
			for (int i = m_SelectedItem->Index; i >= 0; i--)
			{
				// seulement pour les couches du meme type
				if (i - 1 >= 0 && layersListViewControl->Items[i - 1]->Text == m_SelectedItem->Text)
				{
					if (Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text) <
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text))
					{
						layersListViewControl->Items[i]->SubItems[1]->Text =
							layersListViewControl->Items[i]->SubItems[2]->Text;
						layersListViewControl->Items[i - 1]->SubItems[2]->Text =
							layersListViewControl->Items[i]->SubItems[1]->Text;
					}
				}
				else
				{
					if (Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text) <
						Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text))
					{
						layersListViewControl->Items[i]->SubItems[1]->Text =
							layersListViewControl->Items[i]->SubItems[2]->Text;
					}
				}
			}
		}

		UpdateLayerWidth();

		// mise � jour des offset
		// r�cup�ration de l'offset de fond sur la premi�re couche de fond
		Decimal offset = Decimal::MaxValue;
		bool found = false;
		for (int i = 0; i < layersListViewControl->Items->Count; i++)
		{
			if (!String::Compare(layersListViewControl->Items[i]->Text, s_BOTTOM_STR))
			{
				if (offset > Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text))
				{
					offset = Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text);
					found = true;
				}
			}
		}
		if (found)
		{
			bottLayersOffsetTextBox->Text = Convert::ToString(offset);
		}

		// pareil pour les couches de surface
		offset = Decimal::MaxValue;
		found = false;
		for (int i = 0; i < layersListViewControl->Items->Count; i++)
		{
			if (!String::Compare(layersListViewControl->Items[i]->Text, s_SURFACE_STR))
			{
				if (offset > Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text))
				{
					offset = Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text);
					found = true;
				}
			}
		}
		if (found)
		{
			surfLayersOffsetTextBox->Text = Convert::ToString(offset);
		}

		// pareil pour les couches en distance
		offset = Decimal::MaxValue;
		found = false;
		for (int i = 0; i < layersListViewControl->Items->Count; i++)
		{
			if (!String::Compare(layersListViewControl->Items[i]->Text, s_DISTANCE_STR))
			{
				if (offset > Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text))
				{
					offset = Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[1]->Text);
					found = true;
				}
			}
		}
		if (found)
		{
			distLayersOffsetTextBox->Text = Convert::ToString(offset);
		}

	}

			 // IPSIS - OTK - suppression de la couche s�lectionn�e
	private: System::Void buttonRemoveLayer_Click(System::Object^  sender, System::EventArgs^  e) {
		// suppression des couches s�lectionn�es
		for (int i = layersListViewControl->SelectedItems->Count - 1; i >= 0; i--)
		{
			layersListViewControl->SelectedItems[i]->Remove();
		}
		// on doit recoller les couches
		for (int i = 0; i < layersListViewControl->Items->Count - 1; i++)
		{
			if (!String::Compare(layersListViewControl->Items[i]->Text, layersListViewControl->Items[i + 1]->Text))
			{
				Decimal maxDepth = Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text);
				Decimal minDepth = Convert::ToDecimal(layersListViewControl->Items[i + 1]->SubItems[1]->Text);
				Decimal gap = minDepth - maxDepth;
				layersListViewControl->Items[i + 1]->SubItems[1]->Text = Convert::ToString(minDepth - gap);
				layersListViewControl->Items[i + 1]->SubItems[2]->Text = Convert::ToString(
					Convert::ToDecimal(layersListViewControl->Items[i + 1]->SubItems[2]->Text) - gap);
			}
		}
	}

			 // IPSIS - OTK - ajout d'une couche dans la liste
	private: System::Void buttonAddLayer_Click(System::Object^  sender, System::EventArgs^  e) {

		ListViewItem ^listviewitem = gcnew ListViewItem(s_SURFACE_STR);
		// r�cup�ration des valeurs coh�rentes pour la nouvelle couche
		Decimal minDepth = surfLayersOffsetTrackBar->Value*Decimal(0.1);
		Decimal width = surfLayersWidthTrackBar->Value*Decimal(0.1);
		for (int i = 0; i < layersListViewControl->Items->Count; i++)
		{
			if (!String::Compare(layersListViewControl->Items[i]->Text, s_SURFACE_STR) && minDepth < Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text))
			{
				minDepth = Convert::ToDecimal(layersListViewControl->Items[i]->SubItems[2]->Text);
			}
		}
		Decimal maxDepth = minDepth + width;
		listviewitem->SubItems->Add(Convert::ToString(minDepth));
		listviewitem->SubItems->Add(Convert::ToString(maxDepth));
		listviewitem->SubItems->Add(Convert::ToString(width));
		layersListViewControl->Items->Add(listviewitem);

		// on selectionne la couche ajout�e
		layersListViewControl->SelectedItems->Clear();
		listviewitem->Selected = true;
	}

			 // D�marrage de l'EI
	private: System::Void buttonStartEI_Click(System::Object^  sender, System::EventArgs^  e) {
		// Application de la configuration
		bool bOk = true;
		M3DKernel::GetInstance()->Lock();
		try
		{
			IHMToConfig();

			// r�cup�ration du module d'echoIntegration
			CModuleManager * pModuleMgr = CModuleManager::getInstance();
			EchoIntegrationModule * pModule = pModuleMgr->GetEchoIntegrationModule();

			// affichage du dialog permettant de choisir entre d�marrage imm�diat et diff�r�
			TreatmentStartModeSelector ^ref = gcnew TreatmentStartModeSelector(pModule, m_ReaderCtrlManaged);
			ref->ShowDialog();

			// MAJ IHM
			buttonStartEI->Enabled = !pModule->getEnable();
			buttonStopEI->Enabled = pModule->getEnable();

			SetReadOnly(pModule->getEnable());
		}
		catch (Exception^)
		{
			bOk = false;
			MessageBox::Show("Invalid configuration.");
		}

		M3DKernel::GetInstance()->Unlock();

		// FAE 185 - Fermeture automatique de la fen�tre au d�marrage de l'EI
		if (bOk)
		{
			Close();
		}
	}

			 // Arr�t de l'EI
	private: System::Void buttonStopEI_Click(System::Object^  sender, System::EventArgs^  e) {
		// r�cup�ration du module d'echoIntegration
		CModuleManager * pModuleMgr = CModuleManager::getInstance();
		EchoIntegrationModule * pModule = pModuleMgr->GetEchoIntegrationModule();

		// affichage du dialog permettant de choisir entre d�marrage imm�diat et diff�r�
		TreatmentStopModeSelector ^ref = gcnew TreatmentStopModeSelector(pModule);
		M3DKernel::GetInstance()->Lock();
		ref->ShowDialog();

		// MAJ IHM
		buttonStartEI->Enabled = !pModule->getEnable();
		buttonStopEI->Enabled = pModule->getEnable();

		SetReadOnly(pModule->getEnable());
		M3DKernel::GetInstance()->Unlock();
	}
};
}