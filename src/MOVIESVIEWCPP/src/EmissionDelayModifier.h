#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "Filtering/InterferenceFilter.h"
#include "Filtering/EmissionDelay.h"

namespace MOVIESVIEWCPP {
	/// <summary>
	/// Description r�sum�e de EmissionDelayModifier
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class EmissionDelayModifier : public System::Windows::Forms::Form
	{
	public:
		EmissionDelayModifier(InterferenceFilter* pFilter)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
			DataTable^ t = gcnew DataTable();

			DataColumn^ column = gcnew DataColumn();
			column->DataType = System::Type::GetType("System.UInt32");
			column->ColumnName = "Group Size";
			t->Columns->Add(column);

			// Create second column.
			column = gcnew DataColumn();
			column->DataType = System::Type::GetType("System.Int32");
			column->ColumnName = "Tx Delay";
			t->Columns->Add(column);

			dataGridView->DataSource = t;
			dataGridView->DataError += gcnew System::Windows::Forms::DataGridViewDataErrorEventHandler(this, &EmissionDelayModifier::dataGridView_DataError);

			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &EmissionDelayModifier::EmissionDelayModifier_FormClosing);

			m_pFilter = pFilter;
			InitializeAll();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~EmissionDelayModifier()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;
	private: System::Windows::Forms::DataGridView^  dataGridView;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;






			 InterferenceFilter * m_pFilter;
			 int previousindex;

#pragma region Windows Form Designer generated code
			 /// <summary>
			 /// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
			 /// le contenu de cette m�thode avec l'�diteur de code.
			 /// </summary>
			 void InitializeComponent(void)
			 {
				 this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
				 this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
				 this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
				 this->tableLayoutPanel1->SuspendLayout();
				 this->SuspendLayout();
				 // 
				 // comboBox1
				 // 
				 this->comboBox1->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
				 this->comboBox1->FormattingEnabled = true;
				 this->comboBox1->Location = System::Drawing::Point(3, 3);
				 this->comboBox1->Name = L"comboBox1";
				 this->comboBox1->Size = System::Drawing::Size(406, 21);
				 this->comboBox1->TabIndex = 1;
				 this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &EmissionDelayModifier::comboBox1_SelectedIndexChanged);
				 // 
				 // dataGridView
				 // 
				 this->dataGridView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
				 this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
				 this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->dataGridView->Location = System::Drawing::Point(3, 30);
				 this->dataGridView->MultiSelect = false;
				 this->dataGridView->Name = L"dataGridView";
				 this->dataGridView->ShowEditingIcon = false;
				 this->dataGridView->Size = System::Drawing::Size(406, 370);
				 this->dataGridView->TabIndex = 2;
				 // 
				 // tableLayoutPanel1
				 // 
				 this->tableLayoutPanel1->ColumnCount = 1;
				 this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
					 100)));
				 this->tableLayoutPanel1->Controls->Add(this->dataGridView, 0, 1);
				 this->tableLayoutPanel1->Controls->Add(this->comboBox1, 0, 0);
				 this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
				 this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
				 this->tableLayoutPanel1->RowCount = 2;
				 this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
				 this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
				 this->tableLayoutPanel1->Size = System::Drawing::Size(412, 403);
				 this->tableLayoutPanel1->TabIndex = 3;
				 // 
				 // EmissionDelayModifier
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(412, 403);
				 this->Controls->Add(this->tableLayoutPanel1);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
				 this->Name = L"EmissionDelayModifier";
				 this->Text = L"EmissionDelayModifier";
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
				 this->tableLayoutPanel1->ResumeLayout(false);
				 this->ResumeLayout(false);

			 }
#pragma endregion


	private: System::Void InitializeAll()
	{
		Config2GUI();

		this->comboBox1->BeginUpdate();
		this->comboBox1->Items->Clear();

		for (unsigned int i = 0; i < m_pFilter->GetEmissionDelaySet().GetRecordCount(); i++)
		{
			EmissionDelay* pDelay = m_pFilter->GetEmissionDelaySet().GetRecord(i);
			if (pDelay)
			{
				System::String ^NodeName = gcnew System::String(pDelay->m_TransducerName.c_str());
				this->comboBox1->Items->Add(NodeName);
			}
		}

		if (this->comboBox1->Items->Count > 0)
			this->comboBox1->SelectedIndex = 0;

		this->comboBox1->EndUpdate();
	}

	private: System::Void Config2GUI()
	{
		// on remplit la tableView
		int selectedIndex = this->comboBox1->SelectedIndex;
		if (selectedIndex == -1)
		{
			selectedIndex = 0;
		}
		EmissionDelay* pDelay = m_pFilter->GetEmissionDelaySet().GetRecord(selectedIndex);
		DataTable^ t = (DataTable^)dataGridView->DataSource;
		t->Clear();
		if (pDelay)
		{
			size_t nbGroups = pDelay->m_Delays.size();
			for (size_t i = 0; i < nbGroups; i++)
			{
				DataRow^ row = t->NewRow();
				unsigned int nbChan = pDelay->m_Delays[i].first;
				int delay = pDelay->m_Delays[i].second;
				row["Group Size"] = Convert::ToString(nbChan);
				row["Tx Delay"] = Convert::ToString(delay);
				t->Rows->Add(row);
			}
		}
	}

	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
	{
		// on sauvegarde les param�tres du transducteur duquel on part
		ApplyParameters();
		Config2GUI();
		previousindex = this->comboBox1->SelectedIndex;
	}
	private: System::Void dataGridView_DataError(System::Object^  sender, System::Windows::Forms::DataGridViewDataErrorEventArgs^ e)
	{
		if (e->Exception != nullptr)
		{
			MessageBox::Show("Please enter a valid number.");
		}
	}
	private: System::Void EmissionDelayModifier_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
	{
		ApplyParameters();
	}
	private: System::Void ApplyParameters()
	{
		EmissionDelay* pDelay = m_pFilter->GetEmissionDelaySet().GetRecord(this->previousindex);
		if (pDelay)
		{
			DataTable^ t = (DataTable^)dataGridView->DataSource;
			pDelay->m_Delays.clear();
			int nbGroups = t->Rows->Count;
			for (int i = 0; i < nbGroups; i++)
			{
				DataRow^ row = t->Rows[i];
				unsigned int nbChan = Convert::ToUInt32(row["Group Size"]);
				int delay = Convert::ToInt32(row["Tx Delay"]);
				pDelay->m_Delays.push_back(std::make_pair(nbChan, delay));
			}
		}
	}
	};
}
