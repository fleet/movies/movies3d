#include "EnvironmentParameterControl.h"

#include "M3DKernel/M3DKernel.h"

using namespace MOVIESVIEWCPP;

void EnvironmentParameterControl::UpdateConfiguration()
{
	KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();

	param.setTemperature(Decimal::ToDouble(udTemperature->Value));
	param.setSalinity(Decimal::ToDouble(udSalinity->Value));
	param.setSpeedOfSound(Decimal::ToDouble(udSoundVelocity->Value));
	param.useCustomEnvironmentValues(checkUseEnvironment->Checked);
}

void EnvironmentParameterControl::UpdateGUI()
{
	KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();

	udTemperature->Value = Decimal(param.getTemperature());
	udSalinity->Value = Decimal(param.getSalinity());
	udSoundVelocity->Value = Decimal(param.getSpeedOfSound());
	checkUseEnvironment->Checked = param.isUsingCustomEnvironmentValues();
}