#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Summary for EnvironmentParameterControl
	/// </summary>
	public ref class EnvironmentParameterControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		EnvironmentParameterControl(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~EnvironmentParameterControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  gbEnvironment;
	protected:

	protected:
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;

	private: System::Windows::Forms::NumericUpDown^  udTemperature;
	private: System::Windows::Forms::NumericUpDown^  udSoundVelocity;



	private: System::Windows::Forms::CheckBox^  checkUseEnvironment;
	private: System::Windows::Forms::NumericUpDown^  udSalinity;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->gbEnvironment = (gcnew System::Windows::Forms::GroupBox());
			this->checkUseEnvironment = (gcnew System::Windows::Forms::CheckBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->udSalinity = (gcnew System::Windows::Forms::NumericUpDown());
			this->udTemperature = (gcnew System::Windows::Forms::NumericUpDown());
			this->udSoundVelocity = (gcnew System::Windows::Forms::NumericUpDown());
			this->gbEnvironment->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udSalinity))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udTemperature))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udSoundVelocity))->BeginInit();
			this->SuspendLayout();
			// 
			// gbEnvironment
			// 
			this->gbEnvironment->Controls->Add(this->label3);
			this->gbEnvironment->Controls->Add(this->label2);
			this->gbEnvironment->Controls->Add(this->label1);
			this->gbEnvironment->Controls->Add(this->udSalinity);
			this->gbEnvironment->Controls->Add(this->udTemperature);
			this->gbEnvironment->Controls->Add(this->udSoundVelocity);
			this->gbEnvironment->Enabled = false;
			this->gbEnvironment->Location = System::Drawing::Point(8, 3);
			this->gbEnvironment->Name = L"gbEnvironment";
			this->gbEnvironment->Size = System::Drawing::Size(224, 108);
			this->gbEnvironment->TabIndex = 0;
			this->gbEnvironment->TabStop = false;
			this->gbEnvironment->Text = L"Environment";
			// 
			// checkUseEnvironment
			// 
			this->checkUseEnvironment->AutoSize = true;
			this->checkUseEnvironment->Location = System::Drawing::Point(4, 3);
			this->checkUseEnvironment->Name = L"checkUseEnvironment";
			this->checkUseEnvironment->Size = System::Drawing::Size(123, 17);
			this->checkUseEnvironment->TabIndex = 6;
			this->checkUseEnvironment->Text = L"Custom Environment";
			this->checkUseEnvironment->UseVisualStyleBackColor = true;
			this->checkUseEnvironment->CheckedChanged += gcnew System::EventHandler(this, &EnvironmentParameterControl::checkUseEnvironment_CheckedChanged);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 78);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(105, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"Sound Velocity (m/s)";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 52);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(70, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"Salinity (�/00)";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 26);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(87, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Temperature (�C)";
			// 
			// udSalinity
			// 
			this->udSalinity->DecimalPlaces = 1;
			this->udSalinity->Location = System::Drawing::Point(143, 50);
			this->udSalinity->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 999, 0, 0, 0 });
			this->udSalinity->Name = L"udSalinity";
			this->udSalinity->Size = System::Drawing::Size(72, 20);
			this->udSalinity->TabIndex = 2;
			this->udSalinity->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 35, 0, 0, 0 });
			// 
			// udTemperature
			// 
			this->udTemperature->DecimalPlaces = 1;
			this->udTemperature->Location = System::Drawing::Point(143, 24);
			this->udTemperature->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 50, 0, 0, 0 });
			this->udTemperature->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 50, 0, 0, System::Int32::MinValue });
			this->udTemperature->Name = L"udTemperature";
			this->udTemperature->Size = System::Drawing::Size(72, 20);
			this->udTemperature->TabIndex = 1;
			this->udTemperature->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 12, 0, 0, 0 });
			// 
			// udSoundVelocity
			// 
			this->udSoundVelocity->DecimalPlaces = 3;
			this->udSoundVelocity->Location = System::Drawing::Point(143, 76);
			this->udSoundVelocity->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
			this->udSoundVelocity->Name = L"udSoundVelocity";
			this->udSoundVelocity->Size = System::Drawing::Size(72, 20);
			this->udSoundVelocity->TabIndex = 0;
			this->udSoundVelocity->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1500, 0, 0, 0 });
			// 
			// EnvironmentParameterControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->checkUseEnvironment);
			this->Controls->Add(this->gbEnvironment);
			this->Name = L"EnvironmentParameterControl";
			this->Size = System::Drawing::Size(240, 116);
			this->gbEnvironment->ResumeLayout(false);
			this->gbEnvironment->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udSalinity))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udTemperature))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udSoundVelocity))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

#pragma region Impl�mentation de IParamControl
	public: virtual void UpdateConfiguration();
	public: virtual void UpdateGUI();
#pragma endregion

	private: System::Void checkUseEnvironment_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		gbEnvironment->Enabled = checkUseEnvironment->Checked;
	}
};
}
