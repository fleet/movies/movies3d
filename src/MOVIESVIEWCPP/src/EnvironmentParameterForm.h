#pragma once

#include "ParameterForm.h"
#include "EnvironmentParameterControl.h"

namespace MOVIESVIEWCPP {
	public ref class EnvironmentParameterForm : public ParameterForm
	{
	public:
		EnvironmentParameterForm(void) : ParameterForm()
		{
			SetParamControl(gcnew EnvironmentParameterControl(), L"Environment Parameters");
		}
	};
};
