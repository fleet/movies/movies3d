#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace MOVIESVIEWCPP {

	// pour rapatriement du control externalis� sur la form principale
	public delegate void DockViewDelegate();


	/// <summary>
	/// Description r�sum�e de ExternalViewWindow
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class ExternalViewWindow : public System::Windows::Forms::Form
	{
	public:
		// control pouvant passer d'une fen�tre dock�e � une fen�tre libre
		Control^ DockableControl;

	public:

		ExternalViewWindow(Control^ control, DockViewDelegate^ dockViewDelegate, String^ caption)
		{
			InitializeComponent();

			// ajout du control pass� en param�tre � la fen�tre
			DockableControl = control;
			this->Text = caption;
			
			if (DockableControl->Parent != nullptr)
			{
				this->Location = DockableControl->Parent->PointToScreen(DockableControl->Location);
			}

			this->Size = DockableControl->Size;
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~ExternalViewWindow()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		DockViewDelegate^ mDockViewDelegate;
		
		System::Void ExternalViewWindow_OnLoad(System::Object^  sender, System::EventArgs^  e)
		{
			this->Controls->Add(DockableControl);
		}


	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ExternalViewWindow::typeid));
			this->SuspendLayout();
			// 
			// ExternalViewWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(292, 266);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"ExternalViewWindow";
			this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
			this->Text = L"ExternalViewWindow";
			this->Load += gcnew System::EventHandler(this, &ExternalViewWindow::ExternalViewWindow_OnLoad);
			this->ResumeLayout(false);

		}
#pragma endregion

	};
}
