#include "FileRunSelector.h"
namespace MOVIESVIEWCPP
{
	void FileRunSelector::Populate(MovFileRun &aMovFileRun)
	{
		this->checkedListBox1->Items->Clear();
		this->checkedListBox1->BeginUpdate();
		for (unsigned int i = 0; i < aMovFileRun.GetFileCount(); i++)
		{
			System::String ^NodeName = gcnew System::String(aMovFileRun.GetFileName(i).c_str());

			ListViewItem ^item = gcnew ListViewItem(NodeName);
			item->Checked = true;

			checkedListBox1->Items->Add(item);
		}
		// Modification de la taille des colonnes
		checkedListBox1->Columns[0]->AutoResize(ColumnHeaderAutoResizeStyle::ColumnContent);
		this->checkedListBox1->EndUpdate();
	}


	void FileRunSelector::UpdateFileRun(MovFileRun &aMovFileRun)
	{
		for (int i = 0; i < checkedListBox1->Items->Count; i++)
		{
			ListViewItem ^ item = safe_cast<ListViewItem^>(checkedListBox1->Items[i]);
			if (item->Checked == false)
			{
				System::IntPtr ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(item->Text);
				const char* Name = static_cast<const char*>(ip.ToPointer());
				aMovFileRun.RemoveFile(Name);
				System::Runtime::InteropServices::Marshal::FreeHGlobal(ip);
			}
		}
	}

	System::Void FileRunSelector::buttonOK_Click(System::Object^  sender, System::EventArgs^  e)
	{
		UpdateFileRun(m_MovFileRun);
		DialogResult = ::DialogResult::OK;
	}

	System::Void FileRunSelector::buttonCancel_Click(System::Object^  sender, System::EventArgs^  e)
	{
		// Do nothing
		DialogResult = ::DialogResult::Cancel;
	}

	System::Void FileRunSelector::buttonCheckSelected_Click(System::Object^  sender, System::EventArgs^  e)
	{
		bool bFirst = true;
		bool checkedState = false;

		this->checkedListBox1->BeginUpdate();

		for (int i = 0; i < checkedListBox1->Items->Count; i++)
		{
			ListViewItem ^ item = safe_cast<ListViewItem^>(checkedListBox1->Items[i]);
			if (item->Selected)
			{
				if (bFirst)
				{
					bFirst = false;
					checkedState = !item->Checked;
				}
				item->Checked = checkedState;
			}
		}

		this->checkedListBox1->EndUpdate();
	}

	System::Void FileRunSelector::buttonCheckAll_Click(System::Object^  sender, System::EventArgs^  e)
	{
		bool checkedState = false;

		this->checkedListBox1->BeginUpdate();

		for (int i = 0; i < checkedListBox1->Items->Count; i++)
		{
			ListViewItem ^ item = safe_cast<ListViewItem^>(checkedListBox1->Items[i]);
			if (i == 0)
			{
				checkedState = !item->Checked;
			}
			item->Checked = checkedState;
		}
		this->checkedListBox1->EndUpdate();
	}
}