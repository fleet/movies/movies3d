#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "Reader/MovFileRun.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de FileRunSelector
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class FileRunSelector : public System::Windows::Forms::Form
	{
	public:
		MovFileRun &m_MovFileRun;
		FileRunSelector(MovFileRun &fileRun) :m_MovFileRun(fileRun)
		{
			InitializeComponent();

			Populate(fileRun);
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~FileRunSelector()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:



	private: System::Windows::Forms::Button^  buttonCheckSelected;


	private: System::Windows::Forms::Button^  buttonOK;
	private: System::Windows::Forms::Button^  buttonCancel;
	private: System::Windows::Forms::Button^  buttonSelectAll;

	private: System::Windows::Forms::ListView^  checkedListBox1;
	private: System::Windows::Forms::ColumnHeader^  columnHeader;













	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void Populate(MovFileRun &aMovFileRun);
		void UpdateFileRun(MovFileRun &aMovFileRun);
		void InitializeComponent(void)
		{
			this->buttonCheckSelected = (gcnew System::Windows::Forms::Button());
			this->buttonOK = (gcnew System::Windows::Forms::Button());
			this->buttonCancel = (gcnew System::Windows::Forms::Button());
			this->buttonSelectAll = (gcnew System::Windows::Forms::Button());
			this->checkedListBox1 = (gcnew System::Windows::Forms::ListView());
			this->columnHeader = (gcnew System::Windows::Forms::ColumnHeader());
			this->SuspendLayout();
			// 
			// buttonCheckSelected
			// 
			this->buttonCheckSelected->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->buttonCheckSelected->Location = System::Drawing::Point(12, 282);
			this->buttonCheckSelected->Name = L"buttonCheckSelected";
			this->buttonCheckSelected->Size = System::Drawing::Size(140, 23);
			this->buttonCheckSelected->TabIndex = 2;
			this->buttonCheckSelected->Text = L"Check/Uncheck selected";
			this->buttonCheckSelected->UseVisualStyleBackColor = true;
			this->buttonCheckSelected->Click += gcnew System::EventHandler(this, &FileRunSelector::buttonCheckSelected_Click);
			// 
			// buttonOK
			// 
			this->buttonOK->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->buttonOK->Location = System::Drawing::Point(312, 282);
			this->buttonOK->Name = L"buttonOK";
			this->buttonOK->Size = System::Drawing::Size(80, 23);
			this->buttonOK->TabIndex = 4;
			this->buttonOK->Text = L"OK";
			this->buttonOK->UseVisualStyleBackColor = true;
			this->buttonOK->Click += gcnew System::EventHandler(this, &FileRunSelector::buttonOK_Click);
			// 
			// buttonCancel
			// 
			this->buttonCancel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->buttonCancel->Location = System::Drawing::Point(398, 282);
			this->buttonCancel->Name = L"buttonCancel";
			this->buttonCancel->Size = System::Drawing::Size(80, 23);
			this->buttonCancel->TabIndex = 5;
			this->buttonCancel->Text = L"Cancel";
			this->buttonCancel->UseVisualStyleBackColor = true;
			this->buttonCancel->Click += gcnew System::EventHandler(this, &FileRunSelector::buttonCancel_Click);
			// 
			// buttonSelectAll
			// 
			this->buttonSelectAll->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->buttonSelectAll->Location = System::Drawing::Point(158, 282);
			this->buttonSelectAll->Name = L"buttonSelectAll";
			this->buttonSelectAll->Size = System::Drawing::Size(140, 23);
			this->buttonSelectAll->TabIndex = 3;
			this->buttonSelectAll->Text = L"Check/Uncheck All";
			this->buttonSelectAll->UseVisualStyleBackColor = true;
			this->buttonSelectAll->Click += gcnew System::EventHandler(this, &FileRunSelector::buttonCheckAll_Click);
			// 
			// checkedListBox1
			// 
			this->checkedListBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->checkedListBox1->CheckBoxes = true;
			this->checkedListBox1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(1) { this->columnHeader });
			this->checkedListBox1->FullRowSelect = true;
			this->checkedListBox1->HeaderStyle = System::Windows::Forms::ColumnHeaderStyle::None;
			this->checkedListBox1->HideSelection = false;
			this->checkedListBox1->Location = System::Drawing::Point(12, 12);
			this->checkedListBox1->Name = L"checkedListBox1";
			this->checkedListBox1->Size = System::Drawing::Size(478, 264);
			this->checkedListBox1->TabIndex = 1;
			this->checkedListBox1->UseCompatibleStateImageBehavior = false;
			this->checkedListBox1->View = System::Windows::Forms::View::Details;
			// 
			// columnHeader
			// 
			this->columnHeader->Text = L"Fichiers";
			// 
			// FileRunSelector
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(502, 317);
			this->Controls->Add(this->checkedListBox1);
			this->Controls->Add(this->buttonCancel);
			this->Controls->Add(this->buttonCheckSelected);
			this->Controls->Add(this->buttonSelectAll);
			this->Controls->Add(this->buttonOK);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->Name = L"FileRunSelector";
			this->Text = L"FileRunSelector";
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void buttonOK_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonCancel_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonCheckAll_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonCheckSelected_Click(System::Object^  sender, System::EventArgs^  e);
	};
}
