#include "FilterModuleForm.h"
#include "Filtering/FilterMgr.h"
#include "M3DKernel/algorithm/base/EchoAlgorithmRoot.h"
#include "ViewFilter.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "ModuleManager/ModuleManager.h"
#include "Filtering/DataFilter.h"
#include "Filtering/InterferenceFilter.h"
#include "EmissionDelayModifier.h"
#include "Filtering/BeamIdGroupFilter.h"
#include "BeamIdGroupFilterModifier.h"

namespace
{
	constexpr const char * LoggerName = "MoviesView.FilterModule.FilterModuleForm";
}

namespace MOVIESVIEWCPP {
	System::Void FilterModuleForm::Populate()
	{
		this->checkedListBox1->BeginUpdate();
		int selectedindex = this->checkedListBox1->SelectedIndex;

		this->checkedListBox1->Items->Clear();


		CModuleManager *pMod = CModuleManager::getInstance();
		FilterMgr *pfilterAlg = (FilterMgr *)pMod->GetFilterModule();

		for (unsigned int i = 0; i < pfilterAlg->getNbFilter(); i++)
		{
			bool isChecked = pfilterAlg->getFilter(i)->getEnable();
			System::String ^NodeName = gcnew System::String(pfilterAlg->getFilter(i)->getName());;
			checkedListBox1->Items->Add(NodeName, isChecked);
		}

		this->checkedListBox1->SelectedIndex = selectedindex;
		this->checkedListBox1->EndUpdate();
	}
	System::Void FilterModuleForm::SetState(unsigned int Index, bool state)
	{

		CModuleManager *pMod = CModuleManager::getInstance();
		FilterMgr *pfilterAlg = (FilterMgr *)pMod->GetFilterModule();
		pfilterAlg->getFilter(Index)->setEnable(state);

	}
	System::Void FilterModuleForm::buttonTxShifts_Click(System::Object^  sender, System::EventArgs^  e)
	{
		// recuperation du filtre courant
		CModuleManager *pMod = CModuleManager::getInstance();
		FilterMgr *pfilterAlg = (FilterMgr *)pMod->GetFilterModule();
		unsigned int selectedindex = this->checkedListBox1->SelectedIndex;
		if (selectedindex < pfilterAlg->getNbFilter())
		{
			DataFilter *p = pfilterAlg->getFilter(selectedindex);
			if (p)
			{
				InterferenceFilter* pInterferenceFilter = dynamic_cast<InterferenceFilter*> (p);
				if (pInterferenceFilter)
				{
					EmissionDelayModifier^dlg = gcnew EmissionDelayModifier(pInterferenceFilter);
					dlg->ShowDialog();
				}
				else
				{
					BeamIdGroupFilter* pBeamIdGroupFilter = dynamic_cast<BeamIdGroupFilter*> (p);
					if (pBeamIdGroupFilter)
					{
						BeamIdGroupFilterModifier^dlg = gcnew BeamIdGroupFilterModifier(pBeamIdGroupFilter);
						dlg->ShowDialog();
					}
				}
			}
		}
	}
	System::Void FilterModuleForm::UpdateDetailedView()
	{
		unsigned int selectedindex = this->checkedListBox1->SelectedIndex;

		CModuleManager *pMod = CModuleManager::getInstance();
		FilterMgr *pfilterAlg = (FilterMgr *)pMod->GetFilterModule();
		if (selectedindex < pfilterAlg->getNbFilter())
		{

			DataFilter *p = pfilterAlg->getFilter(selectedindex);
			if (p)
			{
				InterferenceFilter* pFilter = dynamic_cast<InterferenceFilter*> (p);
				if (pFilter)
				{
					this->panel1->Enabled = true;
					this->panel2->Enabled = true;
					this->panel3->Enabled = true;
					this->panel4->Enabled = true;
					this->panel5->Enabled = true;
					this->panel6->Enabled = true;
					this->buttonTxShifts->Text = L"Tx shifts";

					this->textBoxTolerance->Text = (gcnew System::Int32((int)pFilter->getTolerance()))->ToString();
					this->textBoxThreshold->Text = (gcnew System::Double(pFilter->getThreshold()))->ToString();
					this->textBoxProp->Text = (gcnew System::Int32(pFilter->getProportion()))->ToString();
					this->textBoxHole->Text = (gcnew System::Int32(pFilter->getHoleSize()))->ToString();
					this->textBoxWindow->Text = (gcnew System::Int32(pFilter->getWindowSize()))->ToString();

					this->labelTolerance->Text = gcnew System::String(pFilter->getToleranceName());
					this->labelTolerance->Text += L" (samples)";
					this->labelThreshold->Text = gcnew System::String(pFilter->getThresholdName());
					this->labelThreshold->Text += L" (dB)";
				}
				else
				{
					// NMD - FAE091 - Ajout filtre de groupe de faisceau par leur ID
					BeamIdGroupFilter* pBeamIdGroupFilter = dynamic_cast<BeamIdGroupFilter*> (p);
					if (pBeamIdGroupFilter)
					{
						this->panel1->Enabled = false;
						this->panel2->Enabled = false;
						this->panel3->Enabled = false;
						this->panel4->Enabled = false;
						this->panel5->Enabled = false;
						this->panel6->Enabled = true;
						this->buttonTxShifts->Text = L"Beams Id";
					}
					else
					{
						this->panel3->Enabled = false;
						this->panel4->Enabled = false;
						this->panel5->Enabled = false;
						this->panel6->Enabled = false;

						DataFilterWithTolerance* pFilter = dynamic_cast<DataFilterWithTolerance*> (p);
						if (pFilter)
						{

							this->panel1->Enabled = true;
							this->panel2->Enabled = false;

							this->textBoxTolerance->Text = (gcnew System::Double(pFilter->getTolerance()))->ToString();
							this->textBoxThreshold->Text = "";

							this->labelTolerance->Text = gcnew System::String(pFilter->getToleranceName());
						}
						else
						{

							DataFilterToleranceThreshold* pFilter = dynamic_cast<DataFilterToleranceThreshold*> (p);
							if (pFilter)
							{
								this->panel1->Enabled = true;
								this->panel2->Enabled = true;
								this->textBoxTolerance->Text = (gcnew System::Double(pFilter->getTolerance()))->ToString();
								this->textBoxThreshold->Text = (gcnew System::Double(pFilter->getThreshold()))->ToString();

								this->labelTolerance->Text = gcnew System::String(pFilter->getToleranceName());
								this->labelThreshold->Text = gcnew System::String(pFilter->getThresholdName());
							}
							else {
								DataFilterValue *pFilter = dynamic_cast<DataFilterValue*> (p);
								if (pFilter)
								{
									this->panel1->Enabled = true;
									this->panel2->Enabled = false;
									this->textBoxTolerance->Text = (gcnew System::Double(pFilter->getValue()))->ToString();
									this->labelTolerance->Text = gcnew System::String(pFilter->getValueName());
								}
							}
						}
					}
				}
			}
		}
	}
	System::Void FilterModuleForm::ApplyDetailedView()
	{
		unsigned int selectedindex = this->checkedListBox1->SelectedIndex;

		CModuleManager *pMod = CModuleManager::getInstance();
		FilterMgr *pfilterAlg = (FilterMgr *)pMod->GetFilterModule();
		if (selectedindex < pfilterAlg->getNbFilter())
		{

			DataFilter *p = pfilterAlg->getFilter(selectedindex);
			if (p)
			{
				DataFilterWithTolerance* pFilterWithTolerance = dynamic_cast<DataFilterWithTolerance*> (p);
				if (pFilterWithTolerance)
				{
					if (this->panel1->Enabled)
					{
						System::Single^ref2 = System::Convert::ToSingle(textBoxTolerance->Text);
						pFilterWithTolerance->setTolerance(((float)ref2));
					}
				}
				else
				{

					DataFilterToleranceThreshold* pFilterToleranceThreshold = dynamic_cast<DataFilterToleranceThreshold*> (p);
					if (pFilterToleranceThreshold)
					{
						if (this->panel2->Enabled)
						{
							System::Double^ref2 = System::Convert::ToDouble(textBoxThreshold->Text);
							pFilterToleranceThreshold->setThreshold(((double)ref2));
						}
						InterferenceFilter* pInterferenceFilter = dynamic_cast<InterferenceFilter*> (p);
						if (pInterferenceFilter)
						{
							if (this->panel1->Enabled)
							{
								System::Int32^ref2 = System::Convert::ToInt32(textBoxTolerance->Text);
								pInterferenceFilter->setTolerance((float)(int)ref2);
							}
							if (this->panel3->Enabled)
							{
								System::Int32^ref2 = System::Convert::ToInt32(textBoxProp->Text);
								pInterferenceFilter->setProportion(((int)ref2));
							}
							if (this->panel4->Enabled)
							{
								System::Int32^ref2 = System::Convert::ToInt32(textBoxHole->Text);
								pInterferenceFilter->setHoleSize(((int)ref2));
							}
							if (this->panel5->Enabled)
							{
								System::Int32^ref2 = System::Convert::ToInt32(textBoxWindow->Text);
								pInterferenceFilter->setWindowSize(((int)ref2));
							}
						}
						else
						{
							if (this->panel1->Enabled)
							{
								System::Single^ref2 = System::Convert::ToSingle(textBoxTolerance->Text);
								pFilterToleranceThreshold->setTolerance(((float)ref2));
							}
						}
					}
					else {
						DataFilterValue *pFilterValue = dynamic_cast<DataFilterValue*> (p);
						if (pFilterValue)
						{
							try {
								double value = System::Convert::ToDouble(this->textBoxTolerance->Text);
								pFilterValue->setValue(value);
							}
							catch (System::Exception ^e)
							{
								this->textBoxTolerance->Text = (gcnew System::Int32((int)pFilterValue->getValue()))->ToString();
								System::IntPtr ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(e->Message);
								M3D_LOG_WARN(LoggerName, static_cast<const char*>(ip.ToPointer()));
								System::Runtime::InteropServices::Marshal::FreeHGlobal(ip);
							}
						}
					}
				}
			}
		}
	}
};