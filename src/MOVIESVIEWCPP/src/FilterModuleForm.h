#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de FilterModuleForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class FilterModuleForm : public System::Windows::Forms::Form
	{
	public:

	public:
		FilterModuleForm()
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//

			Populate();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~FilterModuleForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckedListBox^  checkedListBox1;
	private: System::Windows::Forms::SplitContainer^  splitContainer1;
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::Label^  labelThreshold;
	private: System::Windows::Forms::TextBox^  textBoxThreshold;



	private: System::Windows::Forms::Label^  labelTolerance;
	private: System::Windows::Forms::TextBox^  textBoxTolerance;


	private: System::Windows::Forms::Panel^  panel2;

	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::Panel^  panel5;
	private: System::Windows::Forms::Label^  labelWindow;
	private: System::Windows::Forms::TextBox^  textBoxWindow;


	private: System::Windows::Forms::Panel^  panel4;
	private: System::Windows::Forms::Label^  labelHole;
	private: System::Windows::Forms::TextBox^  textBoxHole;


	private: System::Windows::Forms::Panel^  panel3;
	private: System::Windows::Forms::Label^  labelProp;
	private: System::Windows::Forms::TextBox^  textBoxProp;
	private: System::Windows::Forms::Panel^  panel6;
	private: System::Windows::Forms::Button^  buttonTxShifts;


	protected:

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->checkedListBox1 = (gcnew System::Windows::Forms::CheckedListBox());
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->panel6 = (gcnew System::Windows::Forms::Panel());
			this->buttonTxShifts = (gcnew System::Windows::Forms::Button());
			this->panel5 = (gcnew System::Windows::Forms::Panel());
			this->labelWindow = (gcnew System::Windows::Forms::Label());
			this->textBoxWindow = (gcnew System::Windows::Forms::TextBox());
			this->panel4 = (gcnew System::Windows::Forms::Panel());
			this->labelHole = (gcnew System::Windows::Forms::Label());
			this->textBoxHole = (gcnew System::Windows::Forms::TextBox());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->labelProp = (gcnew System::Windows::Forms::Label());
			this->textBoxProp = (gcnew System::Windows::Forms::TextBox());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->textBoxTolerance = (gcnew System::Windows::Forms::TextBox());
			this->labelTolerance = (gcnew System::Windows::Forms::Label());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->labelThreshold = (gcnew System::Windows::Forms::Label());
			this->textBoxThreshold = (gcnew System::Windows::Forms::TextBox());
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->tableLayoutPanel1->SuspendLayout();
			this->panel6->SuspendLayout();
			this->panel5->SuspendLayout();
			this->panel4->SuspendLayout();
			this->panel3->SuspendLayout();
			this->panel1->SuspendLayout();
			this->panel2->SuspendLayout();
			this->SuspendLayout();
			// 
			// checkedListBox1
			// 
			this->checkedListBox1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->checkedListBox1->FormattingEnabled = true;
			this->checkedListBox1->Location = System::Drawing::Point(0, 0);
			this->checkedListBox1->Name = L"checkedListBox1";
			this->checkedListBox1->Size = System::Drawing::Size(437, 259);
			this->checkedListBox1->TabIndex = 0;
			this->checkedListBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &FilterModuleForm::checkedListBox1_SelectedIndexChanged);
			this->checkedListBox1->ItemCheck += gcnew System::Windows::Forms::ItemCheckEventHandler(this, &FilterModuleForm::checkedListBox1_ItemCheck);
			// 
			// splitContainer1
			// 
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->Location = System::Drawing::Point(0, 0);
			this->splitContainer1->Name = L"splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->Controls->Add(this->checkedListBox1);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->tableLayoutPanel1);
			this->splitContainer1->Size = System::Drawing::Size(701, 272);
			this->splitContainer1->SplitterDistance = 437;
			this->splitContainer1->TabIndex = 1;
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 1;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->panel6, 0, 5);
			this->tableLayoutPanel1->Controls->Add(this->panel5, 0, 4);
			this->tableLayoutPanel1->Controls->Add(this->panel4, 0, 3);
			this->tableLayoutPanel1->Controls->Add(this->panel3, 0, 2);
			this->tableLayoutPanel1->Controls->Add(this->panel1, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->panel2, 0, 1);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 6;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 16.66667F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 16.66667F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 16.66667F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 16.66667F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 16.66667F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 16.66667F)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(260, 272);
			this->tableLayoutPanel1->TabIndex = 7;
			// 
			// panel6
			// 
			this->panel6->Controls->Add(this->buttonTxShifts);
			this->panel6->Dock = System::Windows::Forms::DockStyle::Top;
			this->panel6->Enabled = false;
			this->panel6->Location = System::Drawing::Point(3, 228);
			this->panel6->Name = L"panel6";
			this->panel6->Size = System::Drawing::Size(254, 38);
			this->panel6->TabIndex = 9;
			// 
			// buttonTxShifts
			// 
			this->buttonTxShifts->Location = System::Drawing::Point(62, 8);
			this->buttonTxShifts->Name = L"buttonTxShifts";
			this->buttonTxShifts->Size = System::Drawing::Size(75, 23);
			this->buttonTxShifts->TabIndex = 0;
			this->buttonTxShifts->Text = L"Tx shifts";
			this->buttonTxShifts->UseVisualStyleBackColor = true;
			this->buttonTxShifts->Click += gcnew System::EventHandler(this, &FilterModuleForm::buttonTxShifts_Click);
			// 
			// panel5
			// 
			this->panel5->Controls->Add(this->labelWindow);
			this->panel5->Controls->Add(this->textBoxWindow);
			this->panel5->Dock = System::Windows::Forms::DockStyle::Top;
			this->panel5->Enabled = false;
			this->panel5->Location = System::Drawing::Point(3, 183);
			this->panel5->Name = L"panel5";
			this->panel5->Size = System::Drawing::Size(254, 38);
			this->panel5->TabIndex = 8;
			// 
			// labelWindow
			// 
			this->labelWindow->AutoSize = true;
			this->labelWindow->Location = System::Drawing::Point(6, 13);
			this->labelWindow->Name = L"labelWindow";
			this->labelWindow->Size = System::Drawing::Size(93, 13);
			this->labelWindow->TabIndex = 3;
			this->labelWindow->Text = L"Window (samples)";
			// 
			// textBoxWindow
			// 
			this->textBoxWindow->AcceptsReturn = true;
			this->textBoxWindow->Location = System::Drawing::Point(119, 9);
			this->textBoxWindow->Name = L"textBoxWindow";
			this->textBoxWindow->Size = System::Drawing::Size(82, 20);
			this->textBoxWindow->TabIndex = 2;
			this->textBoxWindow->Leave += gcnew System::EventHandler(this, &FilterModuleForm::textBox_Leave);
			this->textBoxWindow->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &FilterModuleForm::textBox_KeyPress);
			// 
			// panel4
			// 
			this->panel4->Controls->Add(this->labelHole);
			this->panel4->Controls->Add(this->textBoxHole);
			this->panel4->Dock = System::Windows::Forms::DockStyle::Top;
			this->panel4->Enabled = false;
			this->panel4->Location = System::Drawing::Point(3, 138);
			this->panel4->Name = L"panel4";
			this->panel4->Size = System::Drawing::Size(254, 38);
			this->panel4->TabIndex = 7;
			// 
			// labelHole
			// 
			this->labelHole->AutoSize = true;
			this->labelHole->Location = System::Drawing::Point(6, 13);
			this->labelHole->Name = L"labelHole";
			this->labelHole->Size = System::Drawing::Size(76, 13);
			this->labelHole->TabIndex = 3;
			this->labelHole->Text = L"Hole (samples)";
			// 
			// textBoxHole
			// 
			this->textBoxHole->AcceptsReturn = true;
			this->textBoxHole->Location = System::Drawing::Point(119, 9);
			this->textBoxHole->Name = L"textBoxHole";
			this->textBoxHole->Size = System::Drawing::Size(82, 20);
			this->textBoxHole->TabIndex = 2;
			this->textBoxHole->Leave += gcnew System::EventHandler(this, &FilterModuleForm::textBox_Leave);
			this->textBoxHole->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &FilterModuleForm::textBox_KeyPress);
			// 
			// panel3
			// 
			this->panel3->Controls->Add(this->labelProp);
			this->panel3->Controls->Add(this->textBoxProp);
			this->panel3->Dock = System::Windows::Forms::DockStyle::Top;
			this->panel3->Enabled = false;
			this->panel3->Location = System::Drawing::Point(3, 93);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(254, 38);
			this->panel3->TabIndex = 6;
			// 
			// labelProp
			// 
			this->labelProp->AutoSize = true;
			this->labelProp->Location = System::Drawing::Point(6, 13);
			this->labelProp->Name = L"labelProp";
			this->labelProp->Size = System::Drawing::Size(72, 13);
			this->labelProp->TabIndex = 3;
			this->labelProp->Text = L"Proportion (%)";
			// 
			// textBoxProp
			// 
			this->textBoxProp->AcceptsReturn = true;
			this->textBoxProp->Location = System::Drawing::Point(119, 9);
			this->textBoxProp->Name = L"textBoxProp";
			this->textBoxProp->Size = System::Drawing::Size(82, 20);
			this->textBoxProp->TabIndex = 2;
			this->textBoxProp->Leave += gcnew System::EventHandler(this, &FilterModuleForm::textBox_Leave);
			this->textBoxProp->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &FilterModuleForm::textBox_KeyPress);
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->textBoxTolerance);
			this->panel1->Controls->Add(this->labelTolerance);
			this->panel1->Dock = System::Windows::Forms::DockStyle::Top;
			this->panel1->Enabled = false;
			this->panel1->Location = System::Drawing::Point(3, 3);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(254, 38);
			this->panel1->TabIndex = 4;
			// 
			// textBoxTolerance
			// 
			this->textBoxTolerance->AcceptsReturn = true;
			this->textBoxTolerance->Location = System::Drawing::Point(119, 9);
			this->textBoxTolerance->Name = L"textBoxTolerance";
			this->textBoxTolerance->Size = System::Drawing::Size(82, 20);
			this->textBoxTolerance->TabIndex = 0;
			this->textBoxTolerance->Leave += gcnew System::EventHandler(this, &FilterModuleForm::textBox_Leave);
			this->textBoxTolerance->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &FilterModuleForm::textBox_KeyPress);
			// 
			// labelTolerance
			// 
			this->labelTolerance->AutoSize = true;
			this->labelTolerance->Location = System::Drawing::Point(6, 12);
			this->labelTolerance->Name = L"labelTolerance";
			this->labelTolerance->Size = System::Drawing::Size(55, 13);
			this->labelTolerance->TabIndex = 1;
			this->labelTolerance->Text = L"Tolerance";
			// 
			// panel2
			// 
			this->panel2->Controls->Add(this->labelThreshold);
			this->panel2->Controls->Add(this->textBoxThreshold);
			this->panel2->Dock = System::Windows::Forms::DockStyle::Top;
			this->panel2->Enabled = false;
			this->panel2->Location = System::Drawing::Point(3, 48);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(254, 38);
			this->panel2->TabIndex = 5;
			// 
			// labelThreshold
			// 
			this->labelThreshold->AutoSize = true;
			this->labelThreshold->Location = System::Drawing::Point(6, 13);
			this->labelThreshold->Name = L"labelThreshold";
			this->labelThreshold->Size = System::Drawing::Size(54, 13);
			this->labelThreshold->TabIndex = 3;
			this->labelThreshold->Text = L"Threshold";
			// 
			// textBoxThreshold
			// 
			this->textBoxThreshold->AcceptsReturn = true;
			this->textBoxThreshold->Location = System::Drawing::Point(119, 9);
			this->textBoxThreshold->Name = L"textBoxThreshold";
			this->textBoxThreshold->Size = System::Drawing::Size(82, 20);
			this->textBoxThreshold->TabIndex = 2;
			this->textBoxThreshold->Leave += gcnew System::EventHandler(this, &FilterModuleForm::textBox_Leave);
			this->textBoxThreshold->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &FilterModuleForm::textBox_KeyPress);
			// 
			// FilterModuleForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(701, 272);
			this->Controls->Add(this->splitContainer1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->Name = L"FilterModuleForm";
			this->Text = L"FilterModuleForm";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &FilterModuleForm::FilterModuleForm_FormClosing);
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel2->ResumeLayout(false);
			this->splitContainer1->ResumeLayout(false);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->panel6->ResumeLayout(false);
			this->panel5->ResumeLayout(false);
			this->panel5->PerformLayout();
			this->panel4->ResumeLayout(false);
			this->panel4->PerformLayout();
			this->panel3->ResumeLayout(false);
			this->panel3->PerformLayout();
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void checkedListBox1_ItemCheck(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e) {
		unsigned int index = e->Index;
		bool bChecked = e->NewValue == System::Windows::Forms::CheckState::Checked;
		SetState(index, bChecked);
	}

	private: System::Void Populate();
	private: System::Void SetState(unsigned int Index, bool state);
	private: System::Void checkedListBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		UpdateDetailedView();
	}
	private: System::Void UpdateDetailedView();
	private: System::Void ApplyDetailedView();
	private: System::Void textBox_Leave(System::Object^  sender, System::EventArgs^  e) {
		ApplyDetailedView();
		Populate();
	}
	private: System::Void textBox_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
		if (e->KeyChar == 13)
		{
			ApplyDetailedView();
			Populate();
		}
	}
	private: System::Void FilterModuleForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
		ApplyDetailedView();
	}

	private: System::Void buttonTxShifts_Click(System::Object^  sender, System::EventArgs^  e);
	};
}
