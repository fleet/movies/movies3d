﻿#include "Form1.h"

#include "MessageBoxUtils.h"

#include "EISupervisedForm.h"
#include "AboutForm.h"
#include "MultifrequencyEchogramsForm.h"
#include "SpectralAnalysisForm.h"
#include "CalibrationForm.h"
#include "NoiseLevelForm.h"

#include "ShoalExtractionConsumer.h"
#include "ShoalExtractionForm.h"

#include "PalettePreview.h"

#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/utils/log/LoggerFactory.h"
#include "M3DKernel/utils/log/LogAppender.h"

using namespace MOVIESVIEWCPP;

namespace {
		
	template<typename T>
	System::Void loadViews(System::Collections::Generic::List<T> % controls, GuiParameter& params, DisplayView ^ displayView, const ViewType type) 
	{
		std::list<GuiViewParameter> paramsType = params.viewsByType(type);
		std::list<GuiViewParameter>::iterator it = paramsType.begin(), end = paramsType.end();
		System::Collections::Generic::List<T> removedViews;

		for each(auto control in controls)
		{
			if (it != end) 
			{
				// Edit view
				control->Docked = it->isDocked();
				if (!control->Docked) 
				{
					control->TopLevelControl->Left = it->xPos();
					control->TopLevelControl->Top = it->yPos();
					control->TopLevelControl->Width = it->width();
					control->TopLevelControl->Height = it->height();
					control->setWindowState(it->windowState());
				}
				control->SetTransducer(cppStr2NetStr(it->transducer().c_str()));
				++it;
			}
			else
			{
				// Remove view
				removedViews.Add(control);
			}
		}

		for each(auto control in removedViews)
		{
			control->Close();
		}

		while (it != end) 
		{
			//Add view
			BaseFlatView^ newView;
			if (type == ViewType::Front) 
			{
				newView = displayView->CreateFlatFrontView(it->isDocked());
			}
			else 
			{
				newView = displayView->CreateFlatSideView(it->isDocked());
			}

			newView->SounderChanged();
			newView->DockEnabled = true;
			if (!newView->Docked) 
			{
				newView->TopLevelControl->Left = it->xPos();
				newView->TopLevelControl->Top = it->yPos();
				newView->TopLevelControl->Width = it->width();
				newView->TopLevelControl->Height = it->height();
				newView->setWindowState(it->windowState());
			}
			newView->SetTransducer(cppStr2NetStr(it->transducer().c_str()));
			++it;
		}
	}
}

Form1::Form1()
{
	m_bIsDrawing = false;
	m_frameRateLimiter.Start();

	// First thing to do intialize logger settings (before M3DKernel creation)
	auto appDir = System::IO::Path::GetDirectoryName(System::Reflection::Assembly::GetExecutingAssembly()->GetName()->CodeBase);
	appDir = appDir->Substring(6); // Remove 'file:\' prefix
	auto log4cplusPropertiesPath = System::IO::Path::Combine(appDir, "log4cplus.properties");
	Log::LoggerFactory::GetInstance()->initialize((char*)(void*)Marshal::StringToHGlobalAnsi(log4cplusPropertiesPath));


	m_bgwMonitor = gcnew System::Object();
	m_pModMgr = CModuleManager::getInstance();
	m_SaveAsAvi = false;
	InitializeComponent();

	// initialize image
	this->toolStripButtonReadChunk->Image = this->ImageListButton->Images[0];
	this->toolStripButtonReadOne->Image = this->ImageListButton->Images[2];
	this->toolStripButtonPauseChunk->Image = this->ImageListButton->Images[4];
	this->toolStripButtonGoTo->Image = this->ImageListButton->Images[6];
	this->toolStripButtonFreeze3DView->Image = this->imageListView3dB->Images[0];

	// add palette preview in tool bar
	m_palettePreview = gcnew PalettePreview();
	m_anglePalettePreview = gcnew AnglePalettePreview();
	m_palettePreview->Size = System::Drawing::Size(129, 26);
	m_anglePalettePreview->Size = System::Drawing::Size(129, 26);
	this->flowLayoutPanel1->Controls->Remove(m_maxThresholdValueLabel);
	this->flowLayoutPanel1->Controls->Add(m_palettePreview);
	this->flowLayoutPanel1->Controls->Add(m_maxThresholdValueLabel);
	this->flowLayoutPanel1->Controls->Add(m_anglePalettePreview);
	
	auto displayParameter = DisplayParameter::getInstance();
	m_palettePreview->Palette = displayParameter->GetCurrent2DEchoPalette();

	m_ShoalConsumer = gcnew ShoalExtractionConsumer();
	mSelectionMgr = gcnew SelectionManager(m_ShoalConsumer);

	mUpdateToolBarDelegate = gcnew UpdateToolBarDelegate(this, &MOVIESVIEWCPP::Form1::UpdateToolBar);
	mASyncShoalAdded = gcnew ASyncShoalAdded(this, &MOVIESVIEWCPP::Form1::RefreshVolumicView);
	IntPtr refreshCB = Marshal::GetFunctionPointerForDelegate(mASyncShoalAdded);
	m_displayView = gcnew DisplayView(mUpdateToolBarDelegate, mSelectionMgr, (REFRESH3D_CALLBACK)(void*)refreshCB);
	m_displayView->FlatViewDockStateChangedHandler = gcnew DisplayView::FlatViewDockStateChangedDelegate(this, &Form1::OnFlatViewDockStateChanged);
	mSelectionMgr->SetDisplayView(m_displayView);

	mASyncShoalExtractionDelay = gcnew ASyncShoalExtractionDelay(m_displayView->DefaultFlatSideView, &BaseFlatSideView::UpdateShoalExtractionDelay);
	mShoalExtractionDelayVisible = gcnew ShoalExtractionDelayVisible(m_displayView->DefaultFlatSideView, &BaseFlatSideView::UpdateShoalExtractionDelayVisible);


	// Creation de la fenêtre de log une fois pour toutes
	m_logForm = gcnew LogForm();
	Log::LogAppender* appender = Log::LogAppender::GetInstance();
	appender->registerAppender(m_logForm->appender);

	// create a timer to make toolstrip errors button blink
	m_timerLogIndicator = gcnew System::Windows::Forms::Timer();
	m_timerLogIndicator->Enabled = true;
	m_timerLogIndicator->Interval = 200;
	m_timerLogIndicator->Tick += gcnew System::EventHandler(this, &Form1::timerLogIndicator_Tick);

	DelegateReadStateChanged ^l_delegate = gcnew DelegateReadStateChanged(this, &MOVIESVIEWCPP::Form1::UpdatePlayButtonState);
	DelegateReadStateChanging ^l_delegateChanging = gcnew DelegateReadStateChanging(this, &MOVIESVIEWCPP::Form1::ReadStateChanging);
	DelegateReadEventChanged ^l_delegateReadEvent = gcnew DelegateReadEventChanged(this, &MOVIESVIEWCPP::Form1::ReadEventProcess);
	mReadCtrl = ReaderCtrlManaged::Instance();
	mReadCtrl->setDelegateReadStateChangedCallback(l_delegate);
	mReadCtrl->setDelegateReadStateChangingCallback(l_delegateChanging);
	mReadCtrl->setDelegateReadEventChangedCallback(l_delegateReadEvent);

	KernelParameter param = M3DKernel::GetInstance()->GetRefKernelParameter();
	displayTimeLogToolStripMenuItem->Checked = param.getLogPingFanInterTime();

	TransducerView::SetShoalList(m_ShoalConsumer->GetConsumedList());

	UpdateShoalButtonState();

	this->SuspendLayout();
	this->splitView2D->Panel1->Controls->Add(this->m_displayView->FlatFrontViewArea);
	this->splitView2D->Panel2->Controls->Add(this->m_displayView->FlatSideViewArea);

	this->splitViewGlobal->Panel1->Controls->Add(this->m_displayView->VolumicView);
	this->m_displayView->VolumicView->Dock = System::Windows::Forms::DockStyle::Fill;
	this->sounderParameterToolStripMenuItem->Checked = this->m_displayView->VolumicView->SounderParameterVisible;

	this->ResumeLayout();

	CurrentMode = MODE_NONE;

	m_viewParameterForm = gcnew ViewParameterForm();
	m_shoalExtractionForm = nullptr;
	m_viewSounderParameterForm = nullptr;

	// NMD - Ajout d'un delegate pour revenir vers le mode selection EI supervisée
	this->m_displayView->DefaultFlatSideView->OnPolygonSelectionFinished += gcnew BaseFlatSideView::PolygonSelectionFinishedHandler(this, &Form1::onPolygonSelectionFinished);

	//initialise min threshold scrollbar
	m_thresholdScrollBar->Minimum = m_viewParameterForm->GetMinThresholdMinValue();
	m_thresholdScrollBar->Maximum = m_viewParameterForm->GetMinThresholdMaxValue() + m_thresholdScrollBar->LargeChange - 1;

	// NMD - Ajout d'un delegate pour controler la profondeur max depuis l'interface principale
	BaseFlatView::UpdateMaxDepthDelegate ^ updateMaxDepthDelegate = gcnew BaseFlatView::UpdateMaxDepthDelegate(m_viewParameterForm, &ViewParameterForm::UpdateMaxDepth);
	m_displayView->SetUpdateMaxDepthDelegate(updateMaxDepthDelegate);

	mCorrectionManager = nullptr;

	// OTK - FAE214 - ajout de la visu des histogramme TS
	m_TSHistogramsForm = gcnew TSHistogramsForm;
	m_displayView->DefaultFlatFrontView->AddUserControl(m_TSHistogramsForm);

	m_TSSpectrumsForm = gcnew TSSpectrumsForm;
	m_displayView->DefaultFlatFrontView->AddUserControl(m_TSSpectrumsForm);

	this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Form1::onFormClosing);
}

Form1::~Form1()
{
	if (components)
	{
		delete components;
	}

	delete m_displayView;
}

System::Void Form1::UpdateShoalButtonState()
{
	CModuleManager *pMod = CModuleManager::getInstance();
	ShoalExtractionModule *pShoalModule = pMod->GetShoalExtractionModule();
	ShoalExtractionParameter &refParam = pShoalModule->GetShoalExtractionParameter();
	this->toolStripButtonShoalExtraction->Checked = pShoalModule->getEnable();

}

System::Void Form1::loadToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (this->openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		//IPSIS - FRE - lecture de la configuration dans le répertoire
		char drive[_MAX_DRIVE];
		char dir[_MAX_DIR];
		char fname[_MAX_FNAME];
		char ext[_MAX_EXT];

		// NMD - FAE 133
		if (openFileDialog1->FileNames->Length == 0)
			return;

		// NMD - FAE 133 - on prend le 1er fichier pour extraire la config
		System::IntPtr ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(openFileDialog1->FileNames[0]);
		const char* strPath = static_cast<const char*>(ip.ToPointer());
		_splitpath_s(strPath, drive, dir, fname, ext);

		m_pModMgr->ChangeConfigurationCurrentDirectory(std::string(drive) + dir, "");
		// OTK - FAE014 - si le chargement échoue et qu'aucune conf n'est chargée, on calcule le range automatiquement
		bool computeRange = !m_pModMgr->LoadConfiguration({DisplayParameter::getInstance(), MultifrequencyEchogramsParameter::getInstance()});
		computeRange = computeRange && !saveConfigurationToolStripMenuItem->Enabled;
		m_viewParameterForm->InitData();

		// on peut à présent faire un save
		saveConfigurationToolStripMenuItem->Enabled = true;

		//pause read
		this->mReadCtrl->Pause();

		SynchronizedtreatmentCancel();
		mSelectionMgr->ResetSelection();

		//Reset shoalextraction module (locking, waiting all is aborted)
		m_pModMgr->GetShoalExtractionModule()->ForcePurge();

		//reset shoal consumer
		m_ShoalConsumer->CleanUp();

		MovFileRun mFileRun;

		// NMD - FAE 133 - construction de la liste des fichiers
		std::vector<std::string> files;
		for each(System::String ^ selectedFile in openFileDialog1->FileNames)
		{
			System::IntPtr ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(selectedFile);
			files.push_back(static_cast<const char*>(ip.ToPointer()));
		}
		mFileRun.SetFileList(files);

		System::Runtime::InteropServices::Marshal::FreeHGlobal(ip);

		// OTK - FAE021 - au chargement, on utilise la taille de la vue de front
		// pour avoir le bon ordre de grandeur du pas d'échantillonnage en mode auto
		M3DKernel::GetInstance()->GetRefKernelParameter().setInitWindowWidth(m_displayView->DefaultFlatFrontView->GetFlatPictureBox()->Width);
		M3DKernel::GetInstance()->GetRefKernelParameter().setInitWindowHeight(m_displayView->DefaultFlatFrontView->GetFlatPictureBox()->Height);

		this->mReadCtrl->OpenFileRun(mFileRun);
		this->Text = openFileDialog1->FileName;
		if (computeRange)
		{
			ResetDepthView();
		}
	}
}

System::Void Form1::loadDir(const std::string & fileType)
{
	//IPSIS - FRE - récupération du dernier répertoire chargé
	std::string initialDirectory = Movies3DEnv::GetInstance()->GetLastLoadDir();
	this->folderBrowserDialog1->SelectedPath = gcnew System::String(initialDirectory.c_str());

	if (this->folderBrowserDialog1->ShowDialog() != System::Windows::Forms::DialogResult::OK)
		return;

	this->Text = folderBrowserDialog1->SelectedPath;

	//IPSIS - FRE - stockage dans l'environenment du dernier répertoire chargé
	std::string loadDir = netStr2CppStr(this->folderBrowserDialog1->SelectedPath);
	Movies3DEnv::GetInstance()->SetLastLoadDir(loadDir);

	MovFileRun mFileRun;
	mFileRun.SetSourceName(loadDir, fileType);

	FileRunSelector ^refFileRunSelector = gcnew FileRunSelector(mFileRun);
	if (refFileRunSelector->ShowDialog() != ::DialogResult::OK)
		return;

	//IPSIS - FRE - lecture de la configuration dans le répertoire
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	_splitpath_s(loadDir.c_str(), drive, dir, fname, ext);

	m_pModMgr->ChangeConfigurationCurrentDirectory(std::string(drive) + dir, fname);
	// OTK - FAE014 - si le chargement échoue et qu'aucune conf n'est chargée, on calcule le range automatiquement
	bool computeRange = !m_pModMgr->LoadConfiguration({ DisplayParameter::getInstance(), MultifrequencyEchogramsParameter::getInstance() });
	computeRange = computeRange && !saveConfigurationToolStripMenuItem->Enabled;
	m_viewParameterForm->InitData();

	// on peut à présent faire un save
	saveConfigurationToolStripMenuItem->Enabled = true;

	SynchronizedtreatmentCancel();

	//pause read
	this->mReadCtrl->Pause();

	//Reset shoalextraction module (locking, waiting all is aborted)
	m_pModMgr->GetShoalExtractionModule()->ForcePurge();

	//reset shoal consumer
	m_ShoalConsumer->CleanUp();

	// OTK - FAE021 - au chargement, on utilise la taille de la vue de front
	// pour avoir le bon ordre de grandeur du pas d'échantillonnage en mode auto
	M3DKernel::GetInstance()->GetRefKernelParameter().setInitWindowWidth(m_displayView->DefaultFlatFrontView->GetFlatPictureBox()->Width);
	M3DKernel::GetInstance()->GetRefKernelParameter().setInitWindowHeight(m_displayView->DefaultFlatFrontView->GetFlatPictureBox()->Height);
	this->mReadCtrl->OpenFileRun(mFileRun);

	if (computeRange)
	{
		ResetDepthView();
	}
}

System::Void Form1::loadHacDirToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	loadDir(".hac");	
}

System::Void Form1::loadSonarNetCDFDirToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	loadDir(".nc");
}

System::Void  Form1::acquireToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{

	// OTK - 03/06/2009 - désactivation du goto en mode acquisition
	this->toolStripButtonGoTo->Enabled = false;
	this->mReadCtrl->OpenNetworkStream();
	// this->mReadCtrl->ProcessViewData(this->m_displayView->mWrap,false);

	mReadCtrl->ReadCont();
}

System::Void Form1::saveConfigurationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	// Save 3D camera settings
	m_displayView->VolumicView->SaveCameraSettings();

	m_pModMgr->SaveConfiguration({ DisplayParameter::getInstance(), MultifrequencyEchogramsParameter::getInstance() });
}

System::Void Form1::saveConfigurationAsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	// récupération de la config courante

	//IPSIS - FRE - récupération du répertoire de la dernière configuration
	std::string initialDirectory = Movies3DEnv::GetInstance()->GetLastConfig();

	folderBrowserDialogConfig->SelectedPath = gcnew System::String(initialDirectory.c_str());
	// Choix du répertoire 
	if (folderBrowserDialogConfig->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		// Save 3D camera settings
		m_displayView->VolumicView->SaveCameraSettings();

		// on récupére le chemin et le prefixe
		int prefixBeginIndex = folderBrowserDialogConfig->SelectedPath->LastIndexOf("\\");
		System::String^ path = folderBrowserDialogConfig->SelectedPath->Substring(0, prefixBeginIndex);
		System::String^ prefix = folderBrowserDialogConfig->SelectedPath->Substring(prefixBeginIndex + 1);

		// conversion en std::string
		std::string strPath = netStr2CppStr(path);
		std::string strPrefix = netStr2CppStr(prefix);

		//IPSIS - FRE - stockage dans l'environenment de la dernier config utilisée
		Movies3DEnv::GetInstance()->SetLastConfig(strPath + "\\" + strPrefix);

		m_pModMgr->ChangeConfigurationCurrentDirectory(strPath, strPrefix);
		m_pModMgr->SaveConfiguration({ DisplayParameter::getInstance(), MultifrequencyEchogramsParameter::getInstance() });

		// on peut à présent faire un save
		saveConfigurationToolStripMenuItem->Enabled = true;

	}
}

System::Void Form1::loadConfigurationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	// On ne peut pas charger une configuration si un module est en cours de traitement
	std::string modulesEnabled;
	if (m_pModMgr->IsTreatmentEnabled(modulesEnabled))
	{
		System::String^ message =
			gcnew System::String("Unable to load configuration due to the following treatment(s) running:\n");
		System::String^ modules =
			gcnew System::String(modulesEnabled.c_str());
		message = String::Concat(message, modules);
		MessageBox::Show(message);
	}
	else
	{
		// récupération de la config courante
		//IPSIS - FRE - récupération du répertoire de la dernière configuration
		std::string initialDirectory = Movies3DEnv::GetInstance()->GetLastConfig();

		folderBrowserDialogConfig->SelectedPath = gcnew System::String(initialDirectory.c_str());
		// Choix du répertoire 
		if (folderBrowserDialogConfig->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			// on récupére le chemin et le prefixe
			int prefixBeginIndex = folderBrowserDialogConfig->SelectedPath->LastIndexOf("\\");
			System::String^ path = folderBrowserDialogConfig->SelectedPath->Substring(0, prefixBeginIndex);
			System::String^ prefix = folderBrowserDialogConfig->SelectedPath->Substring(prefixBeginIndex + 1);

			// conversion en std::string
			std::string strPath = netStr2CppStr(path);
			std::string strPrefix = netStr2CppStr(prefix);

			//IPSIS - FRE - stockage dans l'environenment de la dernier config utilisée
			Movies3DEnv::GetInstance()->SetLastConfig(strPath + "\\" + strPrefix);

			m_pModMgr->ChangeConfigurationCurrentDirectory(strPath, strPrefix);
			m_pModMgr->LoadConfiguration({ DisplayParameter::getInstance(), MultifrequencyEchogramsParameter::getInstance() });
			m_viewParameterForm->InitData();

			// Reload 3D camera settings
			m_displayView->VolumicView->LoadCameraSettings();

			// on peut à présent faire un save
			saveConfigurationToolStripMenuItem->Enabled = true;
		}
	}

}

/////////////////////////////////


System::Void Form1::ResetDepthView()
{
	// OTK - 27/03/2009 - ajout d'un lock ici, sinon on a un plantage si on charge
	// un nouveau fichier sans pauser la lecture
	M3DKernel::GetInstance()->Lock();
	double maxDepth = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetMaxRangeUsed();
	M3DKernel::GetInstance()->Unlock();
	maxDepth += 10;
	DisplayParameter::getInstance()->SetMinDepth(0);
	DisplayParameter::getInstance()->SetMaxDepth(maxDepth);
}

System::Void Form1::ShowEISupervisedForm()
{
	EISupervisedForm ^Dlg = gcnew EISupervisedForm();
	Dlg->SetReadCtrl(mReadCtrl);
	Dlg->EISupervisedStopped += gcnew EventHandler(this, &Form1::OnEISupervisedStopped);
	Dlg->ShowDialog();
	// OTK - FAE166 - refresh de l'IHM d'affichage des résultas d'Ei supervisés (nécessaire si
	// on modifie les paramètres d'affichage (utilisation du volume pour pondérer le sA multifaisceaux)
	if (m_EISupervisedResultForm != nullptr)
	{
		m_EISupervisedResultForm->UpdateResults();
	}
}

// OTK - FAE 2056 - fermeture de la fenêtre des résultats d'EI supervisée lorsqu'on arrête
// l'EI supervisée.
System::Void Form1::OnEISupervisedStopped(System::Object^ sender, System::EventArgs ^ e)
{
	// rmq. on ne ferme pas toute de suite la fenêtre si l'Ei supervisée n'est désactivée qu'en fin d'ESU
	bool isEISupervisedEnabled;
	M3DKernel::GetInstance()->Lock();
	EISupervisedModule * pModule = m_pModMgr->GetEISupervisedModule();
	isEISupervisedEnabled = (pModule != NULL) && pModule->getEnable();
	if (!isEISupervisedEnabled)
	{
		m_displayView->DefaultFlatFrontView->RemoveUserControl(m_EISupervisedResultForm);
		m_EISupervisedResultForm = nullptr;
	}
	M3DKernel::GetInstance()->Unlock();
}

// NMD - FAE 106  - Ajout d'une scrollbar pour controler le seuil bas de visu
System::Void Form1::m_thresholdScrollBar_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
	m_viewParameterForm->SetMinThresholdValue(m_thresholdScrollBar->Value);
}

System::Void Form1::toolStripMenuItemHelp_Click(System::Object^  sender, System::EventArgs^  e)
{
}

System::Void Form1::showShoalExtractionForm()
{
	if (m_shoalExtractionForm == nullptr) {
		m_shoalExtractionForm = gcnew ShoalExtractionForm(mShoalExtractionDelayVisible, mASyncShoalExtractionDelay);
	}
	m_shoalExtractionForm->ShowDialog();
	UpdateShoalButtonState();
}

System::Void Form1::ReadEventProcess(const ChunckEventList &newValue)
{
	bool VolumeHasChanged = false;

	auto pKernel = M3DKernel::GetInstance();
	pKernel->Lock();

	if (newValue.m_streamClosed)
	{
		if (this->m_displayView->VolumicView->IsAviRendering)
		{
			this->m_displayView->VolumicView->stopAviRendering();
		}
		this->m_displayView->StreamClosed();
	}

	if (newValue.m_streamStreamOpened)
	{
		if (m_SaveAsAvi)
		{
			ReaderCtrl *pReaderCtrl = ReaderCtrl::getInstance();
			if (pReaderCtrl->GetActiveService())
			{
				System::String ^Name = gcnew System::String(pReaderCtrl->GetActiveService()->getStreamDesc());
				this->m_displayView->VolumicView->startAviRendering(Name + ".avi");
			}
		}
		this->m_displayView->StreamOpened();
	}

	if (newValue.m_sounderChanged)
	{
		auto displayParameter = DisplayParameter::getInstance();
		auto currentAngleThreshold = displayParameter->GetAngleThreshold();
		
		double maxAngleThreshold = DEG_TO_RAD(currentAngleThreshold * 0.01);
		auto & sounderDef = pKernel->getObjectMgr()->GetSounderDefinition();
		for (unsigned int sounderIdx = 0; sounderIdx < sounderDef.GetNbSounder(); ++sounderIdx)
		{
			auto pSounder = sounderDef.GetSounder(sounderIdx);
			for (unsigned int transIdx = 0; transIdx < pSounder->GetTransducerCount(); ++transIdx)
			{
				auto pTrans = pSounder->GetTransducer(transIdx);
				const auto & channelsId = pTrans->GetChannelId();
				for (auto channelId : channelsId)
				{
					auto pSoftChan = pTrans->getSoftChannel(channelId);
					maxAngleThreshold = std::max(maxAngleThreshold, pSoftChan->m_beam3dBWidthAlongRad);
					maxAngleThreshold = std::max(maxAngleThreshold, pSoftChan->m_beam3dBWidthAthwartRad);
				}
			}
		}
		
		DataFmt angleThreshold = RAD_TO_DEG(maxAngleThreshold * 100);
		if (angleThreshold != currentAngleThreshold)
		{
			M3D_LOG_INFO("MoviesView.Form1", Log::format("Angle threshold changed from %d to %d", currentAngleThreshold, angleThreshold));
			displayParameter->SetAngleThreshold(angleThreshold);
			m_anglePalettePreview->AngleThreshold = angleThreshold;
		}

		this->m_displayView->SounderChanged();
		multifrenquenciesEchogramsToolStripMenuItem->Enabled = MultifrequencyEchogramsParameter::getInstance()->hasCompatibleSounders();

		M3D_LOG_WARN("MoviesView.Form1", "Sounder Definition Changed");
		VolumeHasChanged = true;
	}

	if (newValue.m_fanAdded)
	{
		// dans le cas d'un nouveau ping, il se peut qu'on doivent supprimer les bancs qui sont sortis de la
		// nouvelle fenêtre des pings
		m_ShoalConsumer->PurgeOldShoals();
		m_ShoalConsumer->Consume(this->m_displayView);
		this->m_displayView->PingFanAdded();
		this->m_TSHistogramsForm->InvalidateHistogram();
		this->m_TSSpectrumsForm->InvalidateSpectrum();
		VolumeHasChanged = true;
	}

	if (newValue.m_singleTargetAdded)
	{
		this->m_displayView->SingleTSAdded();
		VolumeHasChanged = true;
	}

	if (newValue.m_esuEnded)
	{
		this->m_displayView->EsuClosed();

		EISupervisedModule * pModule = m_pModMgr->GetEISupervisedModule();
		if (pModule->getEnable())
		{
			// creation de la fenetre des résultats de l'echo integration supervisée
			if (m_EISupervisedResultForm == nullptr)
			{
				m_EISupervisedResultForm = gcnew EISupervisedResultForm();
				m_EISupervisedResultForm->SaveEIView += gcnew EISupervisedResultForm::SaveEIViewHandler(m_displayView, &DisplayView::OnSaveEIView);

				m_displayView->DefaultFlatFrontView->AddUserControl(m_EISupervisedResultForm);

				for each(BaseFlatSideView ^ view in m_displayView->FlatSideViews)
				{
					// NMD - correction bug mantis pour synchroniser les resultats de l'EI supervisée affichée avec la vue courante
					view->TransducerChanged += gcnew BaseFlatView::TransducerChangedHandler(m_EISupervisedResultForm, &EISupervisedResultForm::OnTransducerChanged);
					view->OnEISupervisedResultChanged += gcnew BaseFlatSideView::EISupervisedResultChanged(m_EISupervisedResultForm, &EISupervisedResultForm::UpdateResults);
				}

				// mise a jour de la fenetre d'echo integration supervisée
				m_EISupervisedResultForm->OnEsuEnded();

				// on force l'affichage du transducteur courant pour la 1ère fois
				m_EISupervisedResultForm->TransducerName = m_displayView->DefaultFlatSideView->GetTransducer();
			}
			else
			{
				// mise a jour de la fenetre d'echo integration supervisée
				m_EISupervisedResultForm->OnEsuEnded();
			}
		}
		// OTK - FAE 2056 - suppression de la fenêtre des résultats d'EI supervisée lorsqu'on arrête l'EI supervisée
		else
		{
			if (m_EISupervisedResultForm != nullptr)
			{
				m_displayView->DefaultFlatFrontView->RemoveUserControl(m_EISupervisedResultForm);
				m_EISupervisedResultForm = nullptr;
			}
		}
	}

	if (newValue.m_tupleHeaderUpdate)
	{
		// MAJ de la vue des tuples header
		if (this->m_viewSounderParameterForm != nullptr)
		{
			this->m_viewSounderParameterForm->FillTreeView();
		}
	}

	if (VolumeHasChanged)
	{
		CallDisplayDraw();
	}

	pKernel->Unlock();
}

System::Void Form1::CallDisplayDraw()
{
	// Limitation à 50 Hz  : 20 ms
	//if( m_frameRateLimiter.ElapsedMilliseconds > 40 && m_bIsDrawing == false) // 25Hz
		if (m_frameRateLimiter.ElapsedMilliseconds > 20 && m_bIsDrawing == false)
	{
		m_bIsDrawing = true;

		M3DKernel::GetInstance()->Lock();
		bool shouldDraw = false;
		TimeCounter drawTime;
		drawTime.StartCount();

		// 
		bool isPhaseEnabledKernel = (M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePhase() == false);
		bool isPhaseEnabledDisplay = this->m_displayView->IsPhasesEnabled();
		if (isPhaseEnabledKernel != isPhaseEnabledDisplay)
		{
			this->m_displayView->SetPhasesEnabled(isPhaseEnabledKernel);
			this->m_displayView->SounderChanged();

			// Force call of ping fan added to update volumic view
			this->m_displayView->PingFanAdded();
		}

		if (DisplayParameter::getInstance()->dataChanged())
		{
			auto displayParameter = DisplayParameter::getInstance();
			this->m_palettePreview->MinDataThreshold = displayParameter->GetMinThreshold();
			this->m_palettePreview->MaxDataThreshold = displayParameter->GetMaxThreshold();
			this->m_palettePreview->Palette = displayParameter->GetCurrent2DEchoPalette();
			
			auto minThresholdValue = displayParameter->GetMinThreshold() * 0.01;
			this->m_thresholdScrollBar->Value = minThresholdValue;
			this->m_minThresholdValueLabel->Text = String::Format("{0:0} dB", minThresholdValue);
			this->m_maxThresholdValueLabel->Text = String::Format("{0:0} dB", displayParameter->GetMaxThreshold() * 0.01);

			this->m_displayView->UpdateParameter();
		}

		if (this->m_TSHistogramsForm)
		{
			if (this->m_TSHistogramsForm->NeedUpdate && this->m_displayView->DefaultFlatFrontView->IsTSHistogramSelected())
			{
				this->m_TSHistogramsForm->UpdateHistogram();
			}
		}

		if (this->m_TSSpectrumsForm)
		{
			if (this->m_TSSpectrumsForm->NeedUpdate && this->m_displayView->DefaultFlatFrontView->IsTSSpectrumSelected())
			{
				this->m_TSSpectrumsForm->UpdateSpectrum();
			}
		}
		
		this->m_displayView->RecomputeView();
		
		this->UpdateModuleStat();

		m_displayView->Draw();

		DisplayParameter::getInstance()->resetDataChanged();

		drawTime.StopCount();
		M3DKernel::GetInstance()->m_timeCounterRecord.AddTimeCounter("GUI Draw", drawTime);
		M3DKernel::GetInstance()->Unlock();

		m_frameRateLimiter.Reset();
		m_frameRateLimiter.Start();
		m_bIsDrawing = false;
	}
}

System::Void Form1::EnterBottomCorrectionMode()
{
	mCorrectionManager = new CorrectionManager();
}

System::Void Form1::LeaveBottomCorrectionMode()
{
	if (mCorrectionManager != nullptr && mCorrectionManager->HasModifications())
	{
		if (MessageBox::Show("Some pings are modified : do you want to save modifications ?",
			"Movies3D", MessageBoxButtons::YesNo) == System::Windows::Forms::DialogResult::Yes)
		{
			mCorrectionManager->Save(mReadCtrl->GetReadService());
		}
		else
		{
			mCorrectionManager->Undo();

			// On doit reconstruire les vues 2D longitudinales
			m_displayView->RedrawFlatSideViews();
		}
	}
	delete(mCorrectionManager);
	mCorrectionManager = nullptr;
}

System::Void Form1::onFormClosing(System::Object ^sender, System::Windows::Forms::FormClosingEventArgs^ e)
{
	// interception de la fermeture de la fenêtre pour sauvegarde au pmréalable des éventuelles modifications
	// des pings
	LeaveBottomCorrectionMode();
}

System::Void Form1::aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	AboutForm ^ aboutForm = gcnew AboutForm();
	aboutForm->Show(this);
}

System::Void Form1::Form1_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
{
	if (e->KeyChar == 'z')
	{
		//toolStripButtonZoom->Checked = !toolStripButtonZoom->Checked;
		toolStripButtonZoom->PerformClick();
		/*
		if(toolStripButtonZoom->Checked)
		{
		CurrentMode = MODE_ZOOM;
		}
		else
		{
		CurrentMode = MODE_NONE;
		}*/
		e->Handled = true;
	}
	else if (e->KeyChar == 's')
	{
		//toolStripButtonEISupervisedSelection->Checked = !toolStripButtonEISupervisedSelection->Checked;
		toolStripButtonEISupervisedSelection->PerformClick();
		/*
		if(toolStripButtonEISupervisedSelection->Checked)
		{
		CurrentMode = MODE_EI_SUPERVISED_SELECTION;
		}
		else
		{
		CurrentMode = MODE_NONE;
		}*/
		e->Handled = true;
	}
	else if (e->KeyChar == 'c')
	{
		toolStripButtonShowEchotypes->PerformClick();
		e->Handled = true;
	}
}

System::Void Form1::viewSounderParameterToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (m_viewSounderParameterForm == nullptr) {
		m_viewSounderParameterForm = gcnew ViewSounderParameterForm();
		m_viewSounderParameterForm->Icon = this->Icon;
		m_viewSounderParameterForm->Closing += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::viewSounderParameterFormClosing);
	}
	m_viewSounderParameterForm->Show();
}

System::Void Form1::viewSounderParameterFormClosing(System::Object^ sender, CancelEventArgs^ e)
{
	e->Cancel = true;
	m_viewSounderParameterForm->Hide();
}

System::Void Form1::getControlsToArrange(Generic::List<Control^>^ controls)
{
	for each(BaseFlatFrontView^ frontControl in m_displayView->FlatFrontViews)
	{
		if (!frontControl->Docked)
			controls->Add(frontControl->TopLevelControl);
	}

	for each(BaseFlatSideView^ sideControl in m_displayView->FlatSideViews)
	{
		if (!sideControl->Docked)
			controls->Add(sideControl->TopLevelControl);
	}
}

System::Void Form1::horizontalToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	Generic::List<Control^> controls;
	getControlsToArrange(%controls);
	if (controls.Count == 0)
		return;

	auto screenRect = Screen::GetWorkingArea(this);

	int totalheight = screenRect.Height;
	int heightPerControl = totalheight / controls.Count;

	int i = 0;
	for each(auto control in controls)
	{
		control->Location = Point(0, i * heightPerControl);
		control->Size = System::Drawing::Size(screenRect.Width, heightPerControl);
		control->Select();

		++i;
	}
}

System::Void Form1::verticalToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	Generic::List<Control^> controls;
	getControlsToArrange(%controls);
	if (controls.Count == 0)
		return;

	auto screenRect = Screen::GetWorkingArea(this);

	int totalWidth = screenRect.Width;
	int widthPerControl = totalWidth / controls.Count;

	int i = 0;
	for each(auto control in controls)
	{
		control->Location = Point(i * widthPerControl, 0);
		control->Size = System::Drawing::Size(widthPerControl, screenRect.Height);
		control->Select();

		++i;
	}
}

System::Void Form1::tiledToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	Generic::List<Control^> controls;
	getControlsToArrange(%controls);
	if (controls.Count == 0)
		return;

	//Find the number of control to display horizontally and vertically
	std::vector<int> nb = {1,1,2,2,3,3,3,3,3,4,4,4,4,4,4};	//predefined values to avoid complicated calculation. Higher values don't matter because it will rarely happen.
	int nbH = 5;
	if (controls.Count <= nb.size())
		nbH = nb[controls.Count - 1];
	auto nbV = ceil(controls.Count / (double)nbH);

	auto screenRect = Screen::GetWorkingArea(this);

	int widthPerControl = screenRect.Width / nbH;
	int heightPerControl = screenRect.Height / nbV;

	//Move and resize each controls, columns by columns
	int nbHeight = 0;
	int nbWidth = 0;
	for each(auto control in controls)
	{
		control->Location = Point(nbWidth * widthPerControl, nbHeight * heightPerControl);
		control->Size = System::Drawing::Size(widthPerControl, heightPerControl);
		control->Select();

		++nbHeight;
		if (nbHeight >= nbV)
		{
			nbHeight = 0;
			++nbWidth;
		}
	}
}

System::Void Form1::viewDetectedBottomToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	m_displayView->showDetectedBottomView();
}

System::Void Form1::multifrenquenciesEchogramsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	MultifrequencyEchogramsForm^ dialog = gcnew MultifrequencyEchogramsForm();
	if (dialog->ShowDialog() == ::DialogResult::OK)
	{
		//Reload the sounders in the display view to update the 2D views
		m_displayView->SounderChanged();
	}

	delete dialog;
}

System::Void Form1::spectralAnalysisToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	SpectralAnalysisForm^ dialog = gcnew SpectralAnalysisForm();
	dialog->ShowDialog();
	delete dialog;
}

System::Void Form1::calibrationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	CalibrationForm^ dialog = gcnew CalibrationForm();
	dialog->ShowDialog();
	delete dialog;
}

System::Void Form1::noiseLevelToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	NoiseLevelForm^ dialog = gcnew NoiseLevelForm();
	dialog->ShowDialog();
	delete dialog;
}

System::Void Form1::Dock3DView()
{
	buttonDock3DView->Checked = false;
}

System::Void Form1::buttonDock3DView_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	this->SuspendLayout();
	if (buttonDock3DView->Checked)
	{
		mDock3DViewDelegate = gcnew DockViewDelegate(this, &Form1::Dock3DView);
		mExternalForm3DView = gcnew ExternalViewWindow(m_displayView->VolumicView, mDock3DViewDelegate, "MOVIES3D : 3D View");
		mExternalForm3DView->Show();
		buttonDock3DView->ImageIndex = 0;
		this->splitViewGlobal->Panel1Collapsed = true;
	}
	else
	{
		buttonDock3DView->ImageIndex = 1;
		this->splitViewGlobal->Panel1->Controls->Add(mExternalForm3DView->DockableControl);
		this->splitViewGlobal->Panel1Collapsed = false;
		mExternalForm3DView->Close();
	}
	this->ResumeLayout(true);
}

System::Void Form1::OnFlatViewDockStateChanged()
{
	int nbFrontViews = m_displayView->FlatFrontViewArea->ColumnCount;
	int nbSideViews = m_displayView->FlatSideViewArea->RowCount;

	this->splitView2D->Panel1Collapsed = (nbFrontViews == 0);
	this->splitView2D->Panel2Collapsed = (nbSideViews == 0);
	this->splitViewGlobal->Panel2Collapsed = (nbFrontViews == 0 && nbSideViews == 0);
}

System::Void Form1::loadViewsLayout(GuiParameter guiParam)
{
	// Load front views
	loadViews(m_displayView->FlatFrontViews, guiParam, m_displayView, ViewType::Front);

	// Load Side views
	loadViews(m_displayView->FlatSideViews, guiParam, m_displayView, ViewType::Slide);

	// 3D view
	GuiViewParameter view3D = guiParam.viewsByType(ViewType::Volumic).front();
	buttonDock3DView->Checked = view3D.isDocked();
	if (buttonDock3DView->Checked)
	{
		mExternalForm3DView->Left = view3D.xPos();
		mExternalForm3DView->Top = view3D.yPos();
		mExternalForm3DView->Width = view3D.width();
		mExternalForm3DView->Height = view3D.height();
		mExternalForm3DView->WindowState = view3D.windowState();
	}

	this->Left = guiParam.mainWindowPosX();
	this->Top = guiParam.mainWindowPosY();
	this->Width = guiParam.mainWindowWidth();
	this->Height = guiParam.mainWindowHeight();
	this->WindowState = guiParam.windowState();
	this->splitViewGlobal->SplitterDistance = guiParam.viewGlobalSplitDistance();
	this->splitView2D->SplitterDistance = guiParam.view2DSplitDistance();
}

GuiParameter Form1::saveViewsLayout()
{
	std::list<GuiViewParameter> views;

	// 3D view
	GuiViewParameter view3D(ViewType::Volumic);
	view3D.setIsDocked(buttonDock3DView->Checked);
	if (buttonDock3DView->Checked)
	{
		view3D.setSize(mExternalForm3DView->Size.Width, mExternalForm3DView->Size.Height);
		view3D.setPosition(mExternalForm3DView->Location.X, mExternalForm3DView->Location.Y);
		view3D.setWindowState(mExternalForm3DView->WindowState);
	}
	else
	{
		view3D.setSize(m_displayView->VolumicView->Size.Width, m_displayView->VolumicView->Size.Height);
		view3D.setPosition(m_displayView->VolumicView->Location.X, m_displayView->VolumicView->Location.Y);
		view3D.setWindowState(System::Windows::Forms::FormWindowState::Normal);
	}
	views.push_back(view3D);

	// Save front views
	for each(BaseFlatFrontView^ frontControl in m_displayView->FlatFrontViews)
	{
		GuiViewParameter view(ViewType::Front);
		view.setIsDocked(frontControl->Docked);
		view.setSize(frontControl->TopLevelControl->Size.Width, frontControl->TopLevelControl->Size.Height);
		view.setPosition(frontControl->TopLevelControl->Location.X, frontControl->TopLevelControl->Location.Y);
		view.setTransducer(netStr2CppStr(frontControl->GetTransducer()));
		view.setWindowState(frontControl->getWindowState());
		views.push_back(view);
	}

	// Save side views
	for each(BaseFlatSideView^ sideControl in m_displayView->FlatSideViews)
	{
		GuiViewParameter view(ViewType::Slide);
		view.setIsDocked(sideControl->Docked);
		view.setSize(sideControl->TopLevelControl->Size.Width, sideControl->TopLevelControl->Size.Height);
		view.setPosition(sideControl->TopLevelControl->Location.X, sideControl->TopLevelControl->Location.Y);
		view.setTransducer(netStr2CppStr(sideControl->GetTransducer()));
		view.setWindowState(sideControl->getWindowState());
		views.push_back(view);
	}

	GuiParameter params;
	params.setViews(views);
	params.setMainWindowPos(this->Location.X, this->Location.Y);
	params.setMainWindowSize(this->Size.Width, this->Size.Height);
	params.setWindowState(this->WindowState);
	params.setViewGlobalSplitDistance(this->splitViewGlobal->SplitterDistance);
	params.setView2DSplitDistance(this->splitView2D->SplitterDistance);
	return params;
}

System::Void Form1::loadViewsLayoutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	auto appDataDir = Environment::GetFolderPath(Environment::SpecialFolder::LocalApplicationData);
	String ^ filePath = System::IO::Path::Combine(appDataDir, L"M3D", "GuiParameter.xml");

	BaseKernel::MovConfigFile movConfigFile;
	movConfigFile.SetFilePath(netStr2CppStr(filePath));

	GuiParameter params;
	bool isOk = params.DeSerialize(&movConfigFile);
	if (!isOk)
	{
		MessageBox::Show("Load failed !", "Load views layout", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Warning);
		return;
	}

	loadViewsLayout(params);
}

System::Void Form1::saveViewsLayoutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	// CREATE %APPDATA%/M3D
	auto appDataDir = Environment::GetFolderPath(Environment::SpecialFolder::LocalApplicationData); 
	String ^ newDir = System::IO::Path::Combine(appDataDir, L"M3D");
	System::IO::Directory::CreateDirectory(newDir);

	String ^ filePath = System::IO::Path::Combine(appDataDir, L"M3D", "GuiParameter.xml");
	BaseKernel::MovConfigFile movConfigFile;
	movConfigFile.SetFilePath(netStr2CppStr(filePath));

	auto params = saveViewsLayout();
	bool isOk = params.Serialize(&movConfigFile);
	if (!isOk)
	{
		MessageBox::Show("Save failed !", "Save views layout", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Warning);
	}
}

System::Void Form1::importViewsLayoutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	auto openFileDialog = gcnew System::Windows::Forms::OpenFileDialog;
	openFileDialog->Filter = L"XML File|*.xml";
	openFileDialog->Multiselect = false;
	
	if (openFileDialog->ShowDialog() != System::Windows::Forms::DialogResult::OK)
		return;
	
	if (openFileDialog->FileNames->Length != 1)
		return;

	BaseKernel::MovConfigFile movConfigFile;
	movConfigFile.SetFilePath(netStr2CppStr(openFileDialog->FileNames[0]));

	GuiParameter params;
	bool isOk = params.DeSerialize(&movConfigFile);
	if (!isOk)
	{
		MessageBox::Show("Load failed !", "Load views layout", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Warning);
		return;
	}

	loadViewsLayout(params);
}

System::Void Form1::exportViewsLayoutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	auto saveFileDialog = gcnew System::Windows::Forms::SaveFileDialog;
	saveFileDialog->Filter = L"XML File|*.xml";

	if (saveFileDialog->ShowDialog() != System::Windows::Forms::DialogResult::OK)
		return;

	if (saveFileDialog->FileNames->Length != 1)
		return;

	BaseKernel::MovConfigFile movConfigFile;
	movConfigFile.SetFilePath(netStr2CppStr(saveFileDialog->FileNames[0]));

	auto params = saveViewsLayout();
	bool isOk = params.Serialize(&movConfigFile);
	if (!isOk)
	{
		MessageBox::Show("Save failed !", "Save views layout", System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Warning);
	}
}

System::Void Form1::UpdatePlayButtonState(ReadState newValue)
{
	this->toolStripButtonReadChunk->Image = this->ImageListButton->Images[(newValue == eReadCont) ? 0 : 1];
	this->toolStripButtonReadOne->Image = this->ImageListButton->Images[(newValue == eReadOne) ? 2 : 3];
	this->toolStripButtonPauseChunk->Image = this->ImageListButton->Images[(newValue == ePause) ? 4 : 5];
}

System::Void Form1::ReadStateChanging(ReadState oldValue, ReadState newValue) 
{
	if ((CurrentMode == MODE_BOTTOM_CORRECTION || CurrentMode == MODE_POLYGON) && newValue != ePause)
	{
		CurrentMode = MODE_NONE;
	}
	toolStripButtonEditBottom->Enabled = newValue == ePause;
}

System::Void Form1::Form1_KeyDown(System::Object^  sender, KeyEventArgs^  e) 
{
	// OTK - FAE040 - pause sur bare d'espacement si en mode rejeu
	if (e->KeyCode == Keys::Space)
	{
		if (mReadCtrl->IsFileService())
		{
			// si on est en pause, on met la lecture
			if (mReadCtrl->IsPause())
			{
				if (m_LastReadState == eReadCont)
				{
					mReadCtrl->ReadCont();
				}
				else if (m_LastReadState == eReadOne)
				{
					mReadCtrl->ReadOne();
				}
			}
			// si on est en lecture, on met la pause
			else
			{
				mReadCtrl->Pause();
			}
		}
		e->Handled = true;
	}
}

System::Void Form1::toolStripButtonPauseChunk_Click(System::Object^  sender, System::EventArgs^  e)
{
	mReadCtrl->Pause();
}

System::Void Form1::toolStripButtonReadOne_Click(System::Object^  sender, System::EventArgs^  e)
{
	m_LastReadState = eReadOne;
	mReadCtrl->ReadOne();
}

System::Void Form1::toolStripButtonReadChunk_Click(System::Object^  sender, System::EventArgs^  e)
{
	m_LastReadState = eReadCont;
	mReadCtrl->ReadCont();
}

System::Void Form1::toolStripButtonGoTo_Click(System::Object^  sender, System::EventArgs^  e)
{
	// on vérifie si le service est de type fichier
	if (!mReadCtrl->IsFileService())
	{
		MessageBoxUtils::Warning("No file loaded, GoTo function not possible");
		return; 
	}

	// on met la lecture sur pause
	mReadCtrl->Pause();

	HacTime minTime, maxTime;
	std::uint32_t minPing, maxPing;
	mReadCtrl->GetLimits(minTime, minPing, maxTime, maxPing);

	// R�cup�ration de la position courante
	HacTime * currentTime = NULL;
	std::uint64_t currentPingNo = 0;
	size_t maxFan = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectCount();
	if (maxFan)
	{
		PingFan * pFan = (PingFan*)M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(maxFan - 1);
		currentTime = &pFan->m_ObjectTime;
		currentPingNo = pFan->GetFilePingId();
	}

	// Affichage de la form de choix de la destination
	GoToForm ^goToForm = gcnew GoToForm();
	goToForm->SetLimits(&minTime, minPing, &maxTime, maxPing, currentTime, currentPingNo);
	if (goToForm->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		// On positionne la lecture � l'endroit d�sign�
		GoToTarget target = goToForm->GetTarget();

		// avertissement en cas de goto par num�ro de ping sur un fichier multisondeur
		if (target.byPing && M3DKernel::GetInstance()->getObjectMgr()->GetSounderDefinition().GetNbSounder() > 1)
		{
			M3D_LOG_WARN("MoviesView.Form1", "GoTo by ping with multiple sounders can lead to undefined behavior (ping numbers between sounders are unrelated)");
			MessageBoxUtils::Warning("GoTo by ping in a file with multiple sounders can lead to undefined behavior (ping numbers between sounders are unrelated)");
		}

		bool status;
		Cursor = Cursors::WaitCursor;
		status = mReadCtrl->GoTo(target);
		Cursor = Cursors::Default;
		if (!status)
		{
			// en cas de probl�me on en informe l'utilisateur
			M3D_LOG_ERROR("MoviesView.Form1", "GoTo failed.");
			MessageBoxUtils::Error("GoTo failed.");
		}
		else
		{
			// on lit le premier chunk de destination. FAE 189 - Utilisation d'une m�thode synchrone qui g�re le changement
			// de la taille des chunks � lire
			Cursor = Cursors::WaitCursor;
			mReadCtrl->ReadOnlyOneSync();
			Cursor = Cursors::Default;
		}
	}
}

System::Void Form1::loggingWindowToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
{
	loggingWindowToolStripMenuItem->Checked = !loggingWindowToolStripMenuItem->Checked;
	if (loggingWindowToolStripMenuItem->Checked)
	{
		m_logForm->Show();
	}
	else
	{
		m_logForm->Hide();
	}
}

System::Void Form1::toolStripMenuItemErrors_Click(System::Object^  sender, System::EventArgs^  e)
{
	loggingWindowToolStripMenuItem->Checked = true;
	m_logForm->Show();
}

System::Void Form1::timerLogIndicator_Tick(Object^  sender, EventArgs^  e)
{
	auto currentAlertLevel = m_logForm->MaxAlertLevel;
	switch (currentAlertLevel)
	{
	case Log::WARN_LOG_LEVEL: 
		this->toolStripMenuItemErrors->Image = System::Drawing::SystemIcons::Warning->ToBitmap();
		this->toolStripMenuItemErrors->Text = "Warnings detected !";
		this->toolStripMenuItemErrors->Visible = true;
		break;

	case Log::ERROR_LOG_LEVEL:
		this->toolStripMenuItemErrors->Image = System::Drawing::SystemIcons::Error->ToBitmap();
		this->toolStripMenuItemErrors->Text = "Errors detected !";
		this->toolStripMenuItemErrors->Visible = true;
		break;

	case Log::FATAL_LOG_LEVEL:
		this->toolStripMenuItemErrors->Image = System::Drawing::SystemIcons::Error->ToBitmap();
		this->toolStripMenuItemErrors->Text = "Fatal errors detected !";
		this->toolStripMenuItemErrors->Visible = true;
		break;

	default:
		this->toolStripMenuItemErrors->Visible = false;
		break;
	}
}