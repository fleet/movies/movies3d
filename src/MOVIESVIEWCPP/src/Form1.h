#pragma once

#include "DisplayView.h"
#include "ReaderParamForm.h"
#include "ViewParameterForm.h"
#include "ShoalExtractionForm.h"
#include "ReaderCtrlManaged.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "FilterModuleForm.h"
#include "CameraSettingsForm.h"
#include "TransducerModifier.h"
// IPSIS - OTK - ajout IHM de param�trage du module d'�cho-int�gration
#include "EchoIntegrationLayerForm.h"
#include "M3DKernel/datascheme/MovESUMgr.h"
// IPSIS - OTK - ajout IHM parametrage ESUs
#include "ESUManagerForm.h"
#include "GoToForm.h"
#include "RFSettingsform.h"
#include "RFDisplayView.h"
#include "SelectionManager.h"
#include "ExternalViewWindow.h"
#include "ViewSounderParameter/ViewSounderParameterForm.h"

#include "LogForm.h"
#include "FileRunSelector.h"
#include "MLogMessage.h"
#include "ShoalExtractionConsumer.h"
#include "ShoalDescForm.h"
#include "ModuleManager/ModuleManager.h"
#include "ShoalExtraction/ShoalExtractionModule.h"
#include "ShoalExtraction/ShoalExtractionParameter.h"
#include "DisplayParameter.h"
#include "StatForm.h"
#include "M3DKernel/datascheme/PingFanContainer.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "KernelParamForm.h"
#include "M3DKernel/datascheme/KernelParameter.h"
#include "LogForm.h"
#include "M3DKernel/M3DKernel.h"
#include "Reader/MovReadService.h"
#include "ASyncTreatment.h"
#include "PositionParamForm.h"
#include "SpeedParamForm.h"
#include "BottomDetectionParamForm.h"
#include "ProjectionParamForm.h"
#include "EISupervisedResultForm.h"
#include "EnvironmentParameterForm.h"
#include "EISupervised/EISupervisedModule.h"
#include "Reader/ManualCorrection/CorrectionManager.h"
#include "SingleTargetDetection/SingleTargetDetectionParamForm.h"
#include "SingleTargetDetection/TSHistogramsForm.h"
#include "SingleTargetDetection/TSSpectrumForm.h"
#include "MultifrequencyEchogramsParameter.h"
#include "GuiParameter.h"

//environment
#include "M3DKernel/utils/env/Movies3DEnv.h"

namespace MOVIESVIEWCPP {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Threading;
	public delegate void FuncMessDelegate(const char *, DateTime datetime, int level);

	enum Mode
	{
		MODE_NONE,
		MODE_ZOOM,
		MODE_SELECTION,
		MODE_POLYGON,
		MODE_EI_SUPERVISED_SELECTION,
		MODE_BOTTOM_CORRECTION
	};


	/// <summary>
	/// Description r�sum�e de Form1
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{


	private: System::Windows::Forms::ImageList^  imageListView3dB;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown3;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown4;
	private: System::Windows::Forms::Panel^  panel3;
	private: System::Windows::Forms::CheckBox^  checkBox2;
	private: System::Windows::Forms::Panel^  panel4;
	private: System::Windows::Forms::TrackBar^  trackBar2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Panel^  panel5;
	private: System::Windows::Forms::CheckBox^  checkBox3;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown1;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown2;
	private: System::Windows::Forms::Panel^  panel6;
	private: System::Windows::Forms::TrackBar^  trackBar3;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::ToolStripMenuItem^  backgroundColorToolStripMenuItem;

	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::Windows::Forms::ToolStripMenuItem^  modifyToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  sounderToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  transducerToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  softChannelToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  readerParameterToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  loadHacDirToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  loadSonarNetCDFDirToolStripMenuItem;
	private: System::Windows::Forms::FolderBrowserDialog^  folderBrowserDialog1;
	private: System::Windows::Forms::ToolStripMenuItem^  displayTimeLogToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  setFixedSpeedToolStripMenuItem;

	private: System::Windows::Forms::ToolStripMenuItem^  shoalExtractionToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  shoalListToolStripMenuItem;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonFilter;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonShoalExtraction;
	private: System::Windows::Forms::ToolStripMenuItem^  echoIntegrationParameterToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveConfigurationToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  loadConfigurationToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  eSUManagerToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveConfigurationAsToolStripMenuItem;
	private: System::Windows::Forms::FolderBrowserDialog^  folderBrowserDialogConfig;
	private: System::Windows::Forms::ToolStripMenuItem^  moduleStatToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  saveAsRotatingAviToolStripMenuItem;

	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
	private: System::Windows::Forms::SplitContainer^  splitView2D;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator2;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator5;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator4;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator3;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem2;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator6;
	private: System::Windows::Forms::ToolStripMenuItem^  sounderParameterToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  KernelParameterButton;
			 
	private: System::Windows::Forms::ToolStripMenuItem^  saveAsVideoToolStripMenuItem;
	private: System::Windows::Forms::ToolStrip^  toolStripTools;

	private: System::Windows::Forms::ToolStripButton^  toolStripButtonZoom;

	private: System::Windows::Forms::ToolStripButton^  toolStripButtonCut;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonRF;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator7;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonColor;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonCamera;

	private: System::Windows::Forms::ToolStripButton^  toolStripButtonSounder;
	private: System::Windows::Forms::ToolStripMenuItem^  frequentialResponseToolStripMenuItem;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonSelection;
	private: System::Windows::Forms::ToolStripProgressBar^  toolStripProgressBar;
	private: System::Windows::Forms::ToolStripLabel^  toolStripLabelProgression;

	private: System::Windows::Forms::ToolStripMenuItem^  loggingWindowToolStripMenuItem;
	private: System::Windows::Forms::ImageList^  imageListDock;

	private: System::Windows::Forms::CheckBox^  buttonDock3DView;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorker;

	private: SelectionManager^ mSelectionMgr;

			 // OTK - 15/10/2009 - callback pour la progression des traitements asynchrones
	private: delegate void ASyncTreatmentProgressChanged(const char* stateDesc, int percentage);
	private: ASyncTreatmentProgressChanged^ mASyncTreatmentProgressChanged;
			 // OTK - 19/10/2009 - callback pour l'annulation des traitements asynchrones
	private: delegate bool ASyncTreatmentCancellationPending(void);
	private: ASyncTreatmentCancellationPending^ mASyncTreatmentCancellationPending;
			 // moniteur pour attente de la fin de l'annulation d'un traitement asynchrone
	private: System::Object ^m_bgwMonitor;

			 // OTK - 20/10/2009 - callback pour le signalement de l'ajout d'un banc sur la vue 3D
	private: delegate void ASyncShoalAdded(void);
	private: ASyncShoalAdded^ mASyncShoalAdded;

			 // OTK - 21/10/2009 - callback pour le signalement du retard de l'extraction sur la lecture
	private: ASyncShoalExtractionDelay^ mASyncShoalExtractionDelay;
	private: ShoalExtractionDelayVisible^ mShoalExtractionDelayVisible;

			 // form associ�e � la vue 3D non dock�e
	private: ExternalViewWindow^ mExternalForm3DView;
			 // delegate pour rappatrier le control BaseVolumicView dans la form principale
			 // a la fermeture de la fenetre externe
	private: DockViewDelegate^ mDock3DViewDelegate;

	private: System::Windows::Forms::ToolStripMenuItem^  bottomDetectionToolStripMenuItem;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonBottomDetection;
	private: System::Windows::Forms::ToolStripMenuItem^  projectionParameterToolStripMenuItem;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonIdentify;
	private: System::Windows::Forms::ToolStripMenuItem^  supervisedEchoIntegrationToolStripMenuItem;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonPolygonSelection;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonEISupervised;


	private: System::Windows::Forms::ToolStripMenuItem^  setFixedPositionToolStripMenuItem;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonEISupervisedSelection;
	private: System::Windows::Forms::HScrollBar^  m_thresholdScrollBar;

			 // Composant pour l'affichage de la r�ponse fr�quentielle
	private: RFDisplayView ^ m_rfDisplayView;
			 // Composant pour l'affichage de la r�ponse angulaire
	private: RFDisplayView ^ m_raDisplayView;
	private: System::Windows::Forms::Label^  m_minThresholdValueLabel;

	private: System::Windows::Forms::ToolStripButton^  toolStripButtonEditBottom;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonShowEchotypes;
	private: System::Windows::Forms::ToolStripMenuItem^  viewSounderParameterToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  singleTargetDetectionToolStripMenuItem;

			 // Composant pour l'affichage des r�ssultats de l'echo int�gration supervis�e
	private: EISupervisedResultForm^ m_EISupervisedResultForm;
	private: System::Windows::Forms::ToolStripMenuItem^  arrangeWindowsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  horizontalToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  verticalToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  tiledToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator8;
	private: System::Windows::Forms::ToolStripMenuItem^  environmentParameterToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  viewDetectedBottomToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  multifrenquenciesEchogramsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  spectralAnalysisToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  calibrationToolStripMenuItem;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonRA;
	private: System::Windows::Forms::ToolStripMenuItem^  noiseLevelToolStripMenuItem;

			 // Composant pour l'affichage des histogrammes TS
	private: TSHistogramsForm^ m_TSHistogramsForm;
	private: TSSpectrumsForm^ m_TSSpectrumsForm;
			 
	private: PalettePreview^ m_palettePreview;
	private: AnglePalettePreview^ m_anglePalettePreview;
	private: System::Windows::Forms::ToolStripMenuItem^  saveViewsLayoutToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  loadViewsLayoutToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  importViewsLayoutToolStripMenuItem;

	private: System::Windows::Forms::ToolStripMenuItem^  exportViewLayoutToolStripMenuItem;
private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator9;
private: System::Windows::Forms::ToolStripButton^  toolStripButtonReadChunk;
private: System::Windows::Forms::ToolStripButton^  toolStripButtonPauseChunk;
private: System::Windows::Forms::ToolStripButton^  toolStripButtonReadOne;
private: System::Windows::Forms::ToolStripButton^  toolStripButtonGoTo;
private: System::Windows::Forms::ToolStripButton^  toolStripButtonFreeze3DView;
private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator10;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItemErrors;


	private: System::Windows::Forms::Label^  m_maxThresholdValueLabel;

			 
	public:
		Form1();

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~Form1();

	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  editToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItemHelp;

	private: System::Windows::Forms::ToolStripMenuItem^  loadToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  optionsToolStripMenuItem;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;


	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanel1;
	private: System::Windows::Forms::SplitContainer^  splitViewGlobal;
	private: System::Windows::Forms::Timer^  TimerGenericEvent;
	private: System::Windows::Forms::ImageList^  ImageListButton;



	private: System::Windows::Forms::ToolStripMenuItem^  dViewToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  resetCameraToolStripMenuItem1;

	private: System::Windows::Forms::ToolStripMenuItem^  acquireToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  FilterToolStripMenuItem;
	private: System::ComponentModel::IContainer^  components;
	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>


#pragma region Windows Form Designer generated code
	/// <summary>
	/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
	/// le contenu de cette m�thode avec l'�diteur de code.
	/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->ImageListButton = (gcnew System::Windows::Forms::ImageList(this->components));
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveConfigurationToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveConfigurationAsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->loadConfigurationToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->loadToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->acquireToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->loadHacDirToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->loadSonarNetCDFDirToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->saveAsRotatingAviToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveAsVideoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->editToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->readerParameterToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->KernelParameterButton = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->projectionParameterToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->environmentParameterToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->eSUManagerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator5 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->shoalExtractionToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->FilterToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->bottomDetectionToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->echoIntegrationParameterToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->frequentialResponseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->supervisedEchoIntegrationToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->singleTargetDetectionToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->spectralAnalysisToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->calibrationToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->multifrenquenciesEchogramsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->noiseLevelToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator4 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->modifyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->sounderToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->transducerToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->softChannelToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator3 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->displayTimeLogToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->setFixedSpeedToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->setFixedPositionToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->optionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->dViewToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->resetCameraToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->sounderParameterToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->backgroundColorToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator6 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->viewSounderParameterToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->shoalListToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->moduleStatToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->loggingWindowToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->viewDetectedBottomToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator8 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->arrangeWindowsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->horizontalToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->verticalToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->tiledToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveViewsLayoutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->loadViewsLayoutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->importViewsLayoutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exportViewLayoutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripMenuItemHelp = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripMenuItemErrors = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->splitViewGlobal = (gcnew System::Windows::Forms::SplitContainer());
			this->splitView2D = (gcnew System::Windows::Forms::SplitContainer());
			this->flowLayoutPanel1 = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->toolStripTools = (gcnew System::Windows::Forms::ToolStrip());
			this->toolStripButtonReadChunk = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonPauseChunk = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonReadOne = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonGoTo = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonFreeze3DView = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripSeparator10 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->toolStripButtonSounder = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonCamera = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonColor = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripSeparator7 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->toolStripButtonZoom = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonCut = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonSelection = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonIdentify = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonPolygonSelection = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonEISupervisedSelection = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonEditBottom = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripSeparator9 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->toolStripButtonShowEchotypes = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonFilter = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonBottomDetection = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonShoalExtraction = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonRF = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonRA = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripProgressBar = (gcnew System::Windows::Forms::ToolStripProgressBar());
			this->toolStripLabelProgression = (gcnew System::Windows::Forms::ToolStripLabel());
			this->toolStripButtonEISupervised = (gcnew System::Windows::Forms::ToolStripButton());
			this->m_thresholdScrollBar = (gcnew System::Windows::Forms::HScrollBar());
			this->m_minThresholdValueLabel = (gcnew System::Windows::Forms::Label());
			this->m_maxThresholdValueLabel = (gcnew System::Windows::Forms::Label());
			this->imageListView3dB = (gcnew System::Windows::Forms::ImageList(this->components));
			this->buttonDock3DView = (gcnew System::Windows::Forms::CheckBox());
			this->imageListDock = (gcnew System::Windows::Forms::ImageList(this->components));
			this->TimerGenericEvent = (gcnew System::Windows::Forms::Timer(this->components));
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->numericUpDown3 = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown4 = (gcnew System::Windows::Forms::NumericUpDown());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->checkBox2 = (gcnew System::Windows::Forms::CheckBox());
			this->panel4 = (gcnew System::Windows::Forms::Panel());
			this->trackBar2 = (gcnew System::Windows::Forms::TrackBar());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->panel5 = (gcnew System::Windows::Forms::Panel());
			this->checkBox3 = (gcnew System::Windows::Forms::CheckBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->panel6 = (gcnew System::Windows::Forms::Panel());
			this->trackBar3 = (gcnew System::Windows::Forms::TrackBar());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->folderBrowserDialog1 = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->folderBrowserDialogConfig = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->backgroundWorker = (gcnew System::ComponentModel::BackgroundWorker());
			this->menuStrip1->SuspendLayout();
			this->tableLayoutPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitViewGlobal))->BeginInit();
			this->splitViewGlobal->Panel2->SuspendLayout();
			this->splitViewGlobal->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitView2D))->BeginInit();
			this->splitView2D->SuspendLayout();
			this->flowLayoutPanel1->SuspendLayout();
			this->toolStripTools->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown4))->BeginInit();
			this->panel3->SuspendLayout();
			this->panel4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar2))->BeginInit();
			this->panel5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->BeginInit();
			this->panel6->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar3))->BeginInit();
			this->SuspendLayout();
			// 
			// ImageListButton
			// 
			this->ImageListButton->ImageStream = (cli::safe_cast<System::Windows::Forms::ImageListStreamer^>(resources->GetObject(L"ImageListButton.ImageStream")));
			this->ImageListButton->TransparentColor = System::Drawing::Color::Transparent;
			this->ImageListButton->Images->SetKeyName(0, L"");
			this->ImageListButton->Images->SetKeyName(1, L"");
			this->ImageListButton->Images->SetKeyName(2, L"playcont.png");
			this->ImageListButton->Images->SetKeyName(3, L"playcontR.png");
			this->ImageListButton->Images->SetKeyName(4, L"");
			this->ImageListButton->Images->SetKeyName(5, L"");
			this->ImageListButton->Images->SetKeyName(6, L"goto.png");
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {
				this->fileToolStripMenuItem,
					this->editToolStripMenuItem, this->optionsToolStripMenuItem, this->toolStripMenuItemHelp, this->toolStripMenuItemErrors
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1132, 24);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(11) {
				this->saveConfigurationToolStripMenuItem,
					this->saveConfigurationAsToolStripMenuItem, this->loadConfigurationToolStripMenuItem, this->toolStripSeparator2, this->loadToolStripMenuItem,
					this->acquireToolStripMenuItem, this->loadHacDirToolStripMenuItem, this->loadSonarNetCDFDirToolStripMenuItem, this->toolStripSeparator1,
					this->saveAsRotatingAviToolStripMenuItem, this->saveAsVideoToolStripMenuItem
			});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// saveConfigurationToolStripMenuItem
			// 
			this->saveConfigurationToolStripMenuItem->Enabled = false;
			this->saveConfigurationToolStripMenuItem->Name = L"saveConfigurationToolStripMenuItem";
			this->saveConfigurationToolStripMenuItem->Size = System::Drawing::Size(225, 22);
			this->saveConfigurationToolStripMenuItem->Text = L"Save Configuration";
			this->saveConfigurationToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveConfigurationToolStripMenuItem_Click);
			// 
			// saveConfigurationAsToolStripMenuItem
			// 
			this->saveConfigurationAsToolStripMenuItem->Name = L"saveConfigurationAsToolStripMenuItem";
			this->saveConfigurationAsToolStripMenuItem->Size = System::Drawing::Size(225, 22);
			this->saveConfigurationAsToolStripMenuItem->Text = L"Save Configuration As...";
			this->saveConfigurationAsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveConfigurationAsToolStripMenuItem_Click);
			// 
			// loadConfigurationToolStripMenuItem
			// 
			this->loadConfigurationToolStripMenuItem->Name = L"loadConfigurationToolStripMenuItem";
			this->loadConfigurationToolStripMenuItem->Size = System::Drawing::Size(225, 22);
			this->loadConfigurationToolStripMenuItem->Text = L"Load Configuration...";
			this->loadConfigurationToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::loadConfigurationToolStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this->toolStripSeparator2->Name = L"toolStripSeparator2";
			this->toolStripSeparator2->Size = System::Drawing::Size(222, 6);
			// 
			// loadToolStripMenuItem
			// 
			this->loadToolStripMenuItem->Name = L"loadToolStripMenuItem";
			this->loadToolStripMenuItem->Size = System::Drawing::Size(225, 22);
			this->loadToolStripMenuItem->Text = L"Load";
			this->loadToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::loadToolStripMenuItem_Click);
			// 
			// acquireToolStripMenuItem
			// 
			this->acquireToolStripMenuItem->Name = L"acquireToolStripMenuItem";
			this->acquireToolStripMenuItem->Size = System::Drawing::Size(225, 22);
			this->acquireToolStripMenuItem->Text = L"Acquire";
			this->acquireToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::acquireToolStripMenuItem_Click);
			// 
			// loadHacDirToolStripMenuItem
			// 
			this->loadHacDirToolStripMenuItem->Name = L"loadHacDirToolStripMenuItem";
			this->loadHacDirToolStripMenuItem->Size = System::Drawing::Size(225, 22);
			this->loadHacDirToolStripMenuItem->Text = L"Load HAC Directory";
			this->loadHacDirToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::loadHacDirToolStripMenuItem_Click);
			// 
			// loadSonarNetCDFDirToolStripMenuItem
			// 
			this->loadSonarNetCDFDirToolStripMenuItem->Name = L"loadSonarNetCDFDirToolStripMenuItem";
			this->loadSonarNetCDFDirToolStripMenuItem->Size = System::Drawing::Size(225, 22);
			this->loadSonarNetCDFDirToolStripMenuItem->Text = L"Load SonarNetCDF Directory";
			this->loadSonarNetCDFDirToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::loadSonarNetCDFDirToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this->toolStripSeparator1->Name = L"toolStripSeparator1";
			this->toolStripSeparator1->Size = System::Drawing::Size(222, 6);
			// 
			// saveAsRotatingAviToolStripMenuItem
			// 
			this->saveAsRotatingAviToolStripMenuItem->Name = L"saveAsRotatingAviToolStripMenuItem";
			this->saveAsRotatingAviToolStripMenuItem->Size = System::Drawing::Size(225, 22);
			this->saveAsRotatingAviToolStripMenuItem->Text = L"Save as Rotating Video";
			this->saveAsRotatingAviToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveAsRotatingAviToolStripMenuItem_Click);
			// 
			// saveAsVideoToolStripMenuItem
			// 
			this->saveAsVideoToolStripMenuItem->Name = L"saveAsVideoToolStripMenuItem";
			this->saveAsVideoToolStripMenuItem->Size = System::Drawing::Size(225, 22);
			this->saveAsVideoToolStripMenuItem->Text = L"Save as Video";
			this->saveAsVideoToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveAsVideoToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this->editToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(23) {
				this->readerParameterToolStripMenuItem,
					this->KernelParameterButton, this->projectionParameterToolStripMenuItem, this->environmentParameterToolStripMenuItem, this->eSUManagerToolStripMenuItem,
					this->toolStripSeparator5, this->shoalExtractionToolStripMenuItem, this->FilterToolStripMenuItem, this->bottomDetectionToolStripMenuItem,
					this->echoIntegrationParameterToolStripMenuItem, this->frequentialResponseToolStripMenuItem, this->supervisedEchoIntegrationToolStripMenuItem,
					this->singleTargetDetectionToolStripMenuItem, this->spectralAnalysisToolStripMenuItem, this->calibrationToolStripMenuItem, this->multifrenquenciesEchogramsToolStripMenuItem,
					this->noiseLevelToolStripMenuItem, this->toolStripSeparator4, this->modifyToolStripMenuItem, this->toolStripSeparator3, this->displayTimeLogToolStripMenuItem,
					this->setFixedSpeedToolStripMenuItem, this->setFixedPositionToolStripMenuItem
			});
			this->editToolStripMenuItem->Name = L"editToolStripMenuItem";
			this->editToolStripMenuItem->Size = System::Drawing::Size(39, 20);
			this->editToolStripMenuItem->Text = L"Edit";
			this->editToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::editToolStripMenuItem_Click);
			// 
			// readerParameterToolStripMenuItem
			// 
			this->readerParameterToolStripMenuItem->Name = L"readerParameterToolStripMenuItem";
			this->readerParameterToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->readerParameterToolStripMenuItem->Text = L"Reader Parameter";
			this->readerParameterToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::readerToolStripMenuItem_Click);
			// 
			// KernelParameterButton
			// 
			this->KernelParameterButton->Name = L"KernelParameterButton";
			this->KernelParameterButton->Size = System::Drawing::Size(224, 22);
			this->KernelParameterButton->Text = L"Kernel Parameter";
			this->KernelParameterButton->Click += gcnew System::EventHandler(this, &Form1::KernelParameter_Click);
			// 
			// projectionParameterToolStripMenuItem
			// 
			this->projectionParameterToolStripMenuItem->Name = L"projectionParameterToolStripMenuItem";
			this->projectionParameterToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->projectionParameterToolStripMenuItem->Text = L"Projection Parameter";
			this->projectionParameterToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::projectionParameterToolStripMenuItem_Click);
			// 
			// environmentParameterToolStripMenuItem
			// 
			this->environmentParameterToolStripMenuItem->Name = L"environmentParameterToolStripMenuItem";
			this->environmentParameterToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->environmentParameterToolStripMenuItem->Text = L"Environment Parameter";
			this->environmentParameterToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::environmentParameterToolStripMenuItem_Click);
			// 
			// eSUManagerToolStripMenuItem
			// 
			this->eSUManagerToolStripMenuItem->Name = L"eSUManagerToolStripMenuItem";
			this->eSUManagerToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->eSUManagerToolStripMenuItem->Text = L"ESU Manager";
			this->eSUManagerToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::eSUManagerToolStripMenuItem_Click);
			// 
			// toolStripSeparator5
			// 
			this->toolStripSeparator5->Name = L"toolStripSeparator5";
			this->toolStripSeparator5->Size = System::Drawing::Size(221, 6);
			// 
			// shoalExtractionToolStripMenuItem
			// 
			this->shoalExtractionToolStripMenuItem->Name = L"shoalExtractionToolStripMenuItem";
			this->shoalExtractionToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->shoalExtractionToolStripMenuItem->Text = L"Shoal Extraction";
			this->shoalExtractionToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::shoalExtractionToolStripMenuItem_Click);
			// 
			// FilterToolStripMenuItem
			// 
			this->FilterToolStripMenuItem->Name = L"FilterToolStripMenuItem";
			this->FilterToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->FilterToolStripMenuItem->Text = L"FilterModule";
			this->FilterToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::FilterToolStripMenuItem_Click);
			// 
			// bottomDetectionToolStripMenuItem
			// 
			this->bottomDetectionToolStripMenuItem->Name = L"bottomDetectionToolStripMenuItem";
			this->bottomDetectionToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->bottomDetectionToolStripMenuItem->Text = L"Bottom Detection";
			this->bottomDetectionToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonBottomDetection_Click);
			// 
			// echoIntegrationParameterToolStripMenuItem
			// 
			this->echoIntegrationParameterToolStripMenuItem->Name = L"echoIntegrationParameterToolStripMenuItem";
			this->echoIntegrationParameterToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->echoIntegrationParameterToolStripMenuItem->Text = L"Echo-Integration";
			this->echoIntegrationParameterToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::echoIntegrationParameterToolStripMenuItem_Click);
			// 
			// frequentialResponseToolStripMenuItem
			// 
			this->frequentialResponseToolStripMenuItem->Name = L"frequentialResponseToolStripMenuItem";
			this->frequentialResponseToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->frequentialResponseToolStripMenuItem->Text = L"Frequential Response";
			this->frequentialResponseToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::frequentialResponseToolStripMenuItem_Click);
			// 
			// supervisedEchoIntegrationToolStripMenuItem
			// 
			this->supervisedEchoIntegrationToolStripMenuItem->Name = L"supervisedEchoIntegrationToolStripMenuItem";
			this->supervisedEchoIntegrationToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->supervisedEchoIntegrationToolStripMenuItem->Text = L"Supervised Echo-Integration";
			this->supervisedEchoIntegrationToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::supervisedEchoIntegrationToolStripMenuItem_Click);
			// 
			// singleTargetDetectionToolStripMenuItem
			// 
			this->singleTargetDetectionToolStripMenuItem->Name = L"singleTargetDetectionToolStripMenuItem";
			this->singleTargetDetectionToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->singleTargetDetectionToolStripMenuItem->Text = L"Single Target Detection";
			this->singleTargetDetectionToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::singleTargetDetectionToolStripMenuItem_Click);
			// 
			// spectralAnalysisToolStripMenuItem
			// 
			this->spectralAnalysisToolStripMenuItem->Name = L"spectralAnalysisToolStripMenuItem";
			this->spectralAnalysisToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->spectralAnalysisToolStripMenuItem->Text = L"Spectral Analysis";
			this->spectralAnalysisToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::spectralAnalysisToolStripMenuItem_Click);
			// 
			// calibrationToolStripMenuItem
			// 
			this->calibrationToolStripMenuItem->Name = L"calibrationToolStripMenuItem";
			this->calibrationToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->calibrationToolStripMenuItem->Text = L"Calibration";
			this->calibrationToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::calibrationToolStripMenuItem_Click);
			// 
			// multifrenquenciesEchogramsToolStripMenuItem
			// 
			this->multifrenquenciesEchogramsToolStripMenuItem->Enabled = false;
			this->multifrenquenciesEchogramsToolStripMenuItem->Name = L"multifrenquenciesEchogramsToolStripMenuItem";
			this->multifrenquenciesEchogramsToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->multifrenquenciesEchogramsToolStripMenuItem->Text = L"Multifrenquency Echograms";
			this->multifrenquenciesEchogramsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::multifrenquenciesEchogramsToolStripMenuItem_Click);
			// 
			// noiseLevelToolStripMenuItem
			// 
			this->noiseLevelToolStripMenuItem->Name = L"noiseLevelToolStripMenuItem";
			this->noiseLevelToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->noiseLevelToolStripMenuItem->Text = L"Noise Level";
			this->noiseLevelToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::noiseLevelToolStripMenuItem_Click);
			// 
			// toolStripSeparator4
			// 
			this->toolStripSeparator4->Name = L"toolStripSeparator4";
			this->toolStripSeparator4->Size = System::Drawing::Size(221, 6);
			// 
			// modifyToolStripMenuItem
			// 
			this->modifyToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->sounderToolStripMenuItem,
					this->transducerToolStripMenuItem, this->softChannelToolStripMenuItem
			});
			this->modifyToolStripMenuItem->Name = L"modifyToolStripMenuItem";
			this->modifyToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->modifyToolStripMenuItem->Text = L"Modify";
			// 
			// sounderToolStripMenuItem
			// 
			this->sounderToolStripMenuItem->Enabled = false;
			this->sounderToolStripMenuItem->Name = L"sounderToolStripMenuItem";
			this->sounderToolStripMenuItem->Size = System::Drawing::Size(139, 22);
			this->sounderToolStripMenuItem->Text = L"Sounder";
			// 
			// transducerToolStripMenuItem
			// 
			this->transducerToolStripMenuItem->Name = L"transducerToolStripMenuItem";
			this->transducerToolStripMenuItem->Size = System::Drawing::Size(139, 22);
			this->transducerToolStripMenuItem->Text = L"Transducer";
			this->transducerToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::transducerToolStripMenuItem_Click);
			// 
			// softChannelToolStripMenuItem
			// 
			this->softChannelToolStripMenuItem->Enabled = false;
			this->softChannelToolStripMenuItem->Name = L"softChannelToolStripMenuItem";
			this->softChannelToolStripMenuItem->Size = System::Drawing::Size(139, 22);
			this->softChannelToolStripMenuItem->Text = L"SoftChannel";
			// 
			// toolStripSeparator3
			// 
			this->toolStripSeparator3->Name = L"toolStripSeparator3";
			this->toolStripSeparator3->Size = System::Drawing::Size(221, 6);
			// 
			// displayTimeLogToolStripMenuItem
			// 
			this->displayTimeLogToolStripMenuItem->Checked = true;
			this->displayTimeLogToolStripMenuItem->CheckState = System::Windows::Forms::CheckState::Checked;
			this->displayTimeLogToolStripMenuItem->Name = L"displayTimeLogToolStripMenuItem";
			this->displayTimeLogToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->displayTimeLogToolStripMenuItem->Text = L"DisplayTime Log";
			this->displayTimeLogToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::displayTimeLogToolStripMenuItem_Click);
			// 
			// setFixedSpeedToolStripMenuItem
			// 
			this->setFixedSpeedToolStripMenuItem->CheckOnClick = true;
			this->setFixedSpeedToolStripMenuItem->Name = L"setFixedSpeedToolStripMenuItem";
			this->setFixedSpeedToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->setFixedSpeedToolStripMenuItem->Text = L"Set Fixed Speed";
			this->setFixedSpeedToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::setFixedSpeedToolStripMenuItem_Click);
			// 
			// setFixedPositionToolStripMenuItem
			// 
			this->setFixedPositionToolStripMenuItem->Name = L"setFixedPositionToolStripMenuItem";
			this->setFixedPositionToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->setFixedPositionToolStripMenuItem->Text = L"Set Fixed Position";
			this->setFixedPositionToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::setFixedPositionToolStripMenuItem_Click);
			// 
			// optionsToolStripMenuItem
			// 
			this->optionsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(15) {
				this->toolStripMenuItem2,
					this->dViewToolStripMenuItem, this->toolStripSeparator6, this->viewSounderParameterToolStripMenuItem, this->toolStripMenuItem1,
					this->shoalListToolStripMenuItem, this->moduleStatToolStripMenuItem, this->loggingWindowToolStripMenuItem, this->viewDetectedBottomToolStripMenuItem,
					this->toolStripSeparator8, this->arrangeWindowsToolStripMenuItem, this->saveViewsLayoutToolStripMenuItem, this->loadViewsLayoutToolStripMenuItem,
					this->importViewsLayoutToolStripMenuItem, this->exportViewLayoutToolStripMenuItem
			});
			this->optionsToolStripMenuItem->Name = L"optionsToolStripMenuItem";
			this->optionsToolStripMenuItem->Size = System::Drawing::Size(70, 20);
			this->optionsToolStripMenuItem->Text = L"Affichage";
			this->optionsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::optionsToolStripMenuItem_Click);
			// 
			// toolStripMenuItem2
			// 
			this->toolStripMenuItem2->Name = L"toolStripMenuItem2";
			this->toolStripMenuItem2->Size = System::Drawing::Size(203, 22);
			this->toolStripMenuItem2->Text = L"View Parameter";
			this->toolStripMenuItem2->Click += gcnew System::EventHandler(this, &Form1::toolStripMenuItem2_Click);
			// 
			// dViewToolStripMenuItem
			// 
			this->dViewToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->resetCameraToolStripMenuItem1,
					this->sounderParameterToolStripMenuItem, this->backgroundColorToolStripMenuItem
			});
			this->dViewToolStripMenuItem->Name = L"dViewToolStripMenuItem";
			this->dViewToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->dViewToolStripMenuItem->Text = L"3dView";
			this->dViewToolStripMenuItem->DropDownOpened += gcnew System::EventHandler(this, &Form1::dViewToolStripMenuItem_DropDownOpened);
			// 
			// resetCameraToolStripMenuItem1
			// 
			this->resetCameraToolStripMenuItem1->Name = L"resetCameraToolStripMenuItem1";
			this->resetCameraToolStripMenuItem1->Size = System::Drawing::Size(175, 22);
			this->resetCameraToolStripMenuItem1->Text = L"Camera Settings";
			this->resetCameraToolStripMenuItem1->Click += gcnew System::EventHandler(this, &Form1::resetCameraToolStripMenuItem1_Click);
			// 
			// sounderParameterToolStripMenuItem
			// 
			this->sounderParameterToolStripMenuItem->CheckOnClick = true;
			this->sounderParameterToolStripMenuItem->Name = L"sounderParameterToolStripMenuItem";
			this->sounderParameterToolStripMenuItem->Size = System::Drawing::Size(175, 22);
			this->sounderParameterToolStripMenuItem->Text = L"Sounder Parameter";
			this->sounderParameterToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::sounderParameterToolStripMenuItem_Click);
			// 
			// backgroundColorToolStripMenuItem
			// 
			this->backgroundColorToolStripMenuItem->Name = L"backgroundColorToolStripMenuItem";
			this->backgroundColorToolStripMenuItem->Size = System::Drawing::Size(175, 22);
			this->backgroundColorToolStripMenuItem->Text = L"Background color";
			this->backgroundColorToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::backgroundColorToolStripMenuItem_Click);
			// 
			// toolStripSeparator6
			// 
			this->toolStripSeparator6->Name = L"toolStripSeparator6";
			this->toolStripSeparator6->Size = System::Drawing::Size(200, 6);
			// 
			// viewSounderParameterToolStripMenuItem
			// 
			this->viewSounderParameterToolStripMenuItem->Name = L"viewSounderParameterToolStripMenuItem";
			this->viewSounderParameterToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->viewSounderParameterToolStripMenuItem->Text = L"View Sounder Parameter";
			this->viewSounderParameterToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::viewSounderParameterToolStripMenuItem_Click);
			// 
			// toolStripMenuItem1
			// 
			this->toolStripMenuItem1->Name = L"toolStripMenuItem1";
			this->toolStripMenuItem1->Size = System::Drawing::Size(200, 6);
			// 
			// shoalListToolStripMenuItem
			// 
			this->shoalListToolStripMenuItem->Name = L"shoalListToolStripMenuItem";
			this->shoalListToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->shoalListToolStripMenuItem->Text = L"Shoal List";
			this->shoalListToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::shoalListToolStripMenuItem_Click);
			// 
			// moduleStatToolStripMenuItem
			// 
			this->moduleStatToolStripMenuItem->Name = L"moduleStatToolStripMenuItem";
			this->moduleStatToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->moduleStatToolStripMenuItem->Text = L"Module Stat";
			this->moduleStatToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::moduleStatToolStripMenuItem_Click);
			// 
			// loggingWindowToolStripMenuItem
			// 
			this->loggingWindowToolStripMenuItem->Name = L"loggingWindowToolStripMenuItem";
			this->loggingWindowToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->loggingWindowToolStripMenuItem->Text = L"Logging window";
			this->loggingWindowToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::loggingWindowToolStripMenuItem_Click);
			// 
			// viewDetectedBottomToolStripMenuItem
			// 
			this->viewDetectedBottomToolStripMenuItem->Name = L"viewDetectedBottomToolStripMenuItem";
			this->viewDetectedBottomToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->viewDetectedBottomToolStripMenuItem->Text = L"View Detected Bottom";
			this->viewDetectedBottomToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::viewDetectedBottomToolStripMenuItem_Click);
			// 
			// toolStripSeparator8
			// 
			this->toolStripSeparator8->Name = L"toolStripSeparator8";
			this->toolStripSeparator8->Size = System::Drawing::Size(200, 6);
			// 
			// arrangeWindowsToolStripMenuItem
			// 
			this->arrangeWindowsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->horizontalToolStripMenuItem,
					this->verticalToolStripMenuItem, this->tiledToolStripMenuItem
			});
			this->arrangeWindowsToolStripMenuItem->Name = L"arrangeWindowsToolStripMenuItem";
			this->arrangeWindowsToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->arrangeWindowsToolStripMenuItem->Text = L"Arrange Windows";
			// 
			// horizontalToolStripMenuItem
			// 
			this->horizontalToolStripMenuItem->Name = L"horizontalToolStripMenuItem";
			this->horizontalToolStripMenuItem->Size = System::Drawing::Size(129, 22);
			this->horizontalToolStripMenuItem->Text = L"Horizontal";
			this->horizontalToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::horizontalToolStripMenuItem_Click);
			// 
			// verticalToolStripMenuItem
			// 
			this->verticalToolStripMenuItem->Name = L"verticalToolStripMenuItem";
			this->verticalToolStripMenuItem->Size = System::Drawing::Size(129, 22);
			this->verticalToolStripMenuItem->Text = L"Vertical";
			this->verticalToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::verticalToolStripMenuItem_Click);
			// 
			// tiledToolStripMenuItem
			// 
			this->tiledToolStripMenuItem->Name = L"tiledToolStripMenuItem";
			this->tiledToolStripMenuItem->Size = System::Drawing::Size(129, 22);
			this->tiledToolStripMenuItem->Text = L"Tiled";
			this->tiledToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::tiledToolStripMenuItem_Click);
			// 
			// saveViewsLayoutToolStripMenuItem
			// 
			this->saveViewsLayoutToolStripMenuItem->Name = L"saveViewsLayoutToolStripMenuItem";
			this->saveViewsLayoutToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->saveViewsLayoutToolStripMenuItem->Text = L"Save Views Layout";
			this->saveViewsLayoutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::saveViewsLayoutToolStripMenuItem_Click);
			// 
			// loadViewsLayoutToolStripMenuItem
			// 
			this->loadViewsLayoutToolStripMenuItem->Name = L"loadViewsLayoutToolStripMenuItem";
			this->loadViewsLayoutToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->loadViewsLayoutToolStripMenuItem->Text = L"Load Views Layout";
			this->loadViewsLayoutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::loadViewsLayoutToolStripMenuItem_Click);
			// 
			// importViewsLayoutToolStripMenuItem
			// 
			this->importViewsLayoutToolStripMenuItem->Name = L"importViewsLayoutToolStripMenuItem";
			this->importViewsLayoutToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->importViewsLayoutToolStripMenuItem->Text = L"Import Views Layout...";
			this->importViewsLayoutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::importViewsLayoutToolStripMenuItem_Click);
			// 
			// exportViewLayoutToolStripMenuItem
			// 
			this->exportViewLayoutToolStripMenuItem->Name = L"exportViewLayoutToolStripMenuItem";
			this->exportViewLayoutToolStripMenuItem->Size = System::Drawing::Size(203, 22);
			this->exportViewLayoutToolStripMenuItem->Text = L"Export View Layout...";
			this->exportViewLayoutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::exportViewsLayoutToolStripMenuItem_Click);
			// 
			// toolStripMenuItemHelp
			// 
			this->toolStripMenuItemHelp->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->aboutToolStripMenuItem });
			this->toolStripMenuItemHelp->Name = L"toolStripMenuItemHelp";
			this->toolStripMenuItemHelp->Size = System::Drawing::Size(24, 20);
			this->toolStripMenuItemHelp->Text = L"\?";
			this->toolStripMenuItemHelp->Click += gcnew System::EventHandler(this, &Form1::toolStripMenuItemHelp_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(116, 22);
			this->aboutToolStripMenuItem->Text = L"&About...";
			this->aboutToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::aboutToolStripMenuItem_Click);
			// 
			// toolStripMenuItemErrors
			// 
			this->toolStripMenuItemErrors->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
			this->toolStripMenuItemErrors->Font = (gcnew System::Drawing::Font(L"Segoe UI", 9, System::Drawing::FontStyle::Bold));
			this->toolStripMenuItemErrors->Name = L"toolStripMenuItemErrors";
			this->toolStripMenuItemErrors->Size = System::Drawing::Size(52, 20);
			this->toolStripMenuItemErrors->Text = L"Errors";
			this->toolStripMenuItemErrors->Click += gcnew System::EventHandler(this, &Form1::toolStripMenuItemErrors_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->Filter = L"All supported formats|*.nc;*.hac|Hac file|*.hac|Sonar-NetCDF file|*.nc";
			this->openFileDialog1->Multiselect = true;
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->tableLayoutPanel1->ColumnCount = 1;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->splitViewGlobal, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->flowLayoutPanel1, 0, 0);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 24);
			this->tableLayoutPanel1->Margin = System::Windows::Forms::Padding(0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 2;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 30)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 29)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(1132, 744);
			this->tableLayoutPanel1->TabIndex = 0;
			// 
			// splitViewGlobal
			// 
			this->splitViewGlobal->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitViewGlobal->Location = System::Drawing::Point(0, 30);
			this->splitViewGlobal->Margin = System::Windows::Forms::Padding(0);
			this->splitViewGlobal->Name = L"splitViewGlobal";
			this->splitViewGlobal->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// splitViewGlobal.Panel2
			// 
			this->splitViewGlobal->Panel2->Controls->Add(this->splitView2D);
			this->splitViewGlobal->Size = System::Drawing::Size(1132, 714);
			this->splitViewGlobal->SplitterDistance = 379;
			this->splitViewGlobal->TabIndex = 0;
			this->splitViewGlobal->DoubleClick += gcnew System::EventHandler(this, &Form1::splitViewGlobal_DoubleClick);
			// 
			// splitView2D
			// 
			this->splitView2D->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitView2D->Location = System::Drawing::Point(0, 0);
			this->splitView2D->Margin = System::Windows::Forms::Padding(0);
			this->splitView2D->Name = L"splitView2D";
			this->splitView2D->Size = System::Drawing::Size(1132, 331);
			this->splitView2D->SplitterDistance = 361;
			this->splitView2D->TabIndex = 1;
			this->splitView2D->DoubleClick += gcnew System::EventHandler(this, &Form1::splitView2D_DoubleClick);
			// 
			// flowLayoutPanel1
			// 
			this->flowLayoutPanel1->Controls->Add(this->toolStripTools);
			this->flowLayoutPanel1->Controls->Add(this->m_thresholdScrollBar);
			this->flowLayoutPanel1->Controls->Add(this->m_minThresholdValueLabel);
			this->flowLayoutPanel1->Controls->Add(this->m_maxThresholdValueLabel);
			this->flowLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->flowLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->flowLayoutPanel1->Margin = System::Windows::Forms::Padding(0);
			this->flowLayoutPanel1->Name = L"flowLayoutPanel1";
			this->flowLayoutPanel1->Size = System::Drawing::Size(1132, 30);
			this->flowLayoutPanel1->TabIndex = 3;
			// 
			// toolStripTools
			// 
			this->toolStripTools->Dock = System::Windows::Forms::DockStyle::Fill;
			this->toolStripTools->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(27) {
				this->toolStripButtonReadChunk,
					this->toolStripButtonPauseChunk, this->toolStripButtonReadOne, this->toolStripButtonGoTo, this->toolStripButtonFreeze3DView,
					this->toolStripSeparator10, this->toolStripButtonSounder, this->toolStripButtonCamera, this->toolStripButtonColor, this->toolStripSeparator7,
					this->toolStripButtonZoom, this->toolStripButtonCut, this->toolStripButtonSelection, this->toolStripButtonIdentify, this->toolStripButtonPolygonSelection,
					this->toolStripButtonEISupervisedSelection, this->toolStripButtonEditBottom, this->toolStripSeparator9, this->toolStripButtonShowEchotypes,
					this->toolStripButtonFilter, this->toolStripButtonBottomDetection, this->toolStripButtonShoalExtraction, this->toolStripButtonRF,
					this->toolStripButtonRA, this->toolStripProgressBar, this->toolStripLabelProgression, this->toolStripButtonEISupervised
			});
			this->toolStripTools->Location = System::Drawing::Point(0, 0);
			this->toolStripTools->Name = L"toolStripTools";
			this->toolStripTools->Size = System::Drawing::Size(659, 30);
			this->toolStripTools->TabIndex = 6;
			this->toolStripTools->Text = L"toolStripTools";
			// 
			// toolStripButtonReadChunk
			// 
			this->toolStripButtonReadChunk->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonReadChunk->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonReadChunk.Image")));
			this->toolStripButtonReadChunk->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonReadChunk->Name = L"toolStripButtonReadChunk";
			this->toolStripButtonReadChunk->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonReadChunk->Text = L"Read";
			this->toolStripButtonReadChunk->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonReadChunk_Click);
			// 
			// toolStripButtonPauseChunk
			// 
			this->toolStripButtonPauseChunk->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonPauseChunk->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonPauseChunk.Image")));
			this->toolStripButtonPauseChunk->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonPauseChunk->Name = L"toolStripButtonPauseChunk";
			this->toolStripButtonPauseChunk->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonPauseChunk->Text = L"Pause";
			this->toolStripButtonPauseChunk->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonPauseChunk_Click);
			// 
			// toolStripButtonReadOne
			// 
			this->toolStripButtonReadOne->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonReadOne->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonReadOne.Image")));
			this->toolStripButtonReadOne->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonReadOne->Name = L"toolStripButtonReadOne";
			this->toolStripButtonReadOne->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonReadOne->Text = L"Read one";
			this->toolStripButtonReadOne->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonReadOne_Click);
			// 
			// toolStripButtonGoTo
			// 
			this->toolStripButtonGoTo->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonGoTo->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonGoTo.Image")));
			this->toolStripButtonGoTo->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonGoTo->Name = L"toolStripButtonGoTo";
			this->toolStripButtonGoTo->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonGoTo->Text = L"Go to";
			this->toolStripButtonGoTo->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonGoTo_Click);
			// 
			// toolStripButtonFreeze3DView
			// 
			this->toolStripButtonFreeze3DView->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonFreeze3DView->Enabled = false;
			this->toolStripButtonFreeze3DView->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonFreeze3DView.Image")));
			this->toolStripButtonFreeze3DView->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonFreeze3DView->Name = L"toolStripButtonFreeze3DView";
			this->toolStripButtonFreeze3DView->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonFreeze3DView->Text = L"Freeze 3D View";
			this->toolStripButtonFreeze3DView->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonFreeze3DView_Click);
			// 
			// toolStripSeparator10
			// 
			this->toolStripSeparator10->Name = L"toolStripSeparator10";
			this->toolStripSeparator10->Size = System::Drawing::Size(6, 30);
			// 
			// toolStripButtonSounder
			// 
			this->toolStripButtonSounder->CheckOnClick = true;
			this->toolStripButtonSounder->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->toolStripButtonSounder->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonSounder.Image")));
			this->toolStripButtonSounder->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonSounder->Name = L"toolStripButtonSounder";
			this->toolStripButtonSounder->Size = System::Drawing::Size(55, 27);
			this->toolStripButtonSounder->Text = L"Sounder";
			this->toolStripButtonSounder->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonSounder_Click);
			// 
			// toolStripButtonCamera
			// 
			this->toolStripButtonCamera->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->toolStripButtonCamera->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonCamera->Name = L"toolStripButtonCamera";
			this->toolStripButtonCamera->Size = System::Drawing::Size(52, 27);
			this->toolStripButtonCamera->Text = L"Camera";
			this->toolStripButtonCamera->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonCamera_Click);
			// 
			// toolStripButtonColor
			// 
			this->toolStripButtonColor->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->toolStripButtonColor->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonColor.Image")));
			this->toolStripButtonColor->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonColor->Name = L"toolStripButtonColor";
			this->toolStripButtonColor->Size = System::Drawing::Size(75, 27);
			this->toolStripButtonColor->Text = L"Background";
			this->toolStripButtonColor->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonColor_Click);
			// 
			// toolStripSeparator7
			// 
			this->toolStripSeparator7->Name = L"toolStripSeparator7";
			this->toolStripSeparator7->Size = System::Drawing::Size(6, 30);
			// 
			// toolStripButtonZoom
			// 
			this->toolStripButtonZoom->CheckOnClick = true;
			this->toolStripButtonZoom->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonZoom->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonZoom.Image")));
			this->toolStripButtonZoom->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonZoom->Name = L"toolStripButtonZoom";
			this->toolStripButtonZoom->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonZoom->Text = L"2D Zoom (z)";
			this->toolStripButtonZoom->ToolTipText = L"2D Zoom (z)";
			this->toolStripButtonZoom->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonZoom_Click);
			// 
			// toolStripButtonCut
			// 
			this->toolStripButtonCut->CheckOnClick = true;
			this->toolStripButtonCut->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonCut->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonCut.Image")));
			this->toolStripButtonCut->ImageTransparentColor = System::Drawing::Color::Transparent;
			this->toolStripButtonCut->Name = L"toolStripButtonCut";
			this->toolStripButtonCut->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonCut->Text = L"Cut";
			this->toolStripButtonCut->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonCut_Click);
			// 
			// toolStripButtonSelection
			// 
			this->toolStripButtonSelection->CheckOnClick = true;
			this->toolStripButtonSelection->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonSelection->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonSelection.Image")));
			this->toolStripButtonSelection->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonSelection->Name = L"toolStripButtonSelection";
			this->toolStripButtonSelection->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonSelection->Text = L"Selection";
			this->toolStripButtonSelection->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonSelection_Click);
			// 
			// toolStripButtonIdentify
			// 
			this->toolStripButtonIdentify->CheckOnClick = true;
			this->toolStripButtonIdentify->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonIdentify->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonIdentify.Image")));
			this->toolStripButtonIdentify->ImageTransparentColor = System::Drawing::Color::Transparent;
			this->toolStripButtonIdentify->Name = L"toolStripButtonIdentify";
			this->toolStripButtonIdentify->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonIdentify->Text = L"Identify";
			this->toolStripButtonIdentify->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonIdentify_Click);
			// 
			// toolStripButtonPolygonSelection
			// 
			this->toolStripButtonPolygonSelection->CheckOnClick = true;
			this->toolStripButtonPolygonSelection->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonPolygonSelection->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonPolygonSelection.Image")));
			this->toolStripButtonPolygonSelection->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonPolygonSelection->Name = L"toolStripButtonPolygonSelection";
			this->toolStripButtonPolygonSelection->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonPolygonSelection->Text = L"Polygon Selection";
			this->toolStripButtonPolygonSelection->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonPolygonSelection_Click);
			// 
			// toolStripButtonEISupervisedSelection
			// 
			this->toolStripButtonEISupervisedSelection->CheckOnClick = true;
			this->toolStripButtonEISupervisedSelection->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonEISupervisedSelection->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonEISupervisedSelection.Image")));
			this->toolStripButtonEISupervisedSelection->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonEISupervisedSelection->Name = L"toolStripButtonEISupervisedSelection";
			this->toolStripButtonEISupervisedSelection->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonEISupervisedSelection->Text = L"Supervised Echo-Integration Selection (s)";
			this->toolStripButtonEISupervisedSelection->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonEISupervisedSelection_Click);
			// 
			// toolStripButtonEditBottom
			// 
			this->toolStripButtonEditBottom->CheckOnClick = true;
			this->toolStripButtonEditBottom->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonEditBottom->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonEditBottom.Image")));
			this->toolStripButtonEditBottom->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonEditBottom->Name = L"toolStripButtonEditBottom";
			this->toolStripButtonEditBottom->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonEditBottom->Text = L"Manual Bottom Correction";
			this->toolStripButtonEditBottom->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonEditBottom_Click);
			// 
			// toolStripSeparator9
			// 
			this->toolStripSeparator9->Name = L"toolStripSeparator9";
			this->toolStripSeparator9->Size = System::Drawing::Size(6, 30);
			// 
			// toolStripButtonShowEchotypes
			// 
			this->toolStripButtonShowEchotypes->CheckOnClick = true;
			this->toolStripButtonShowEchotypes->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonShowEchotypes->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonShowEchotypes.Image")));
			this->toolStripButtonShowEchotypes->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonShowEchotypes->Name = L"toolStripButtonShowEchotypes";
			this->toolStripButtonShowEchotypes->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonShowEchotypes->Text = L"Display Supervised Echo-Integration Colored Zones (c)";
			this->toolStripButtonShowEchotypes->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonShowEchotypes_Click);
			// 
			// toolStripButtonFilter
			// 
			this->toolStripButtonFilter->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonFilter->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonFilter.Image")));
			this->toolStripButtonFilter->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonFilter->Name = L"toolStripButtonFilter";
			this->toolStripButtonFilter->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonFilter->Text = L"toolStripButtonFilter";
			this->toolStripButtonFilter->ToolTipText = L"Enable Ground Filter";
			this->toolStripButtonFilter->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonFilter_Click);
			// 
			// toolStripButtonBottomDetection
			// 
			this->toolStripButtonBottomDetection->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonBottomDetection->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonBottomDetection.Image")));
			this->toolStripButtonBottomDetection->ImageTransparentColor = System::Drawing::Color::White;
			this->toolStripButtonBottomDetection->Name = L"toolStripButtonBottomDetection";
			this->toolStripButtonBottomDetection->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonBottomDetection->Text = L"Bottom Detection";
			this->toolStripButtonBottomDetection->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonBottomDetection_Click);
			// 
			// toolStripButtonShoalExtraction
			// 
			this->toolStripButtonShoalExtraction->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonShoalExtraction->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonShoalExtraction.Image")));
			this->toolStripButtonShoalExtraction->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonShoalExtraction->Name = L"toolStripButtonShoalExtraction";
			this->toolStripButtonShoalExtraction->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonShoalExtraction->Text = L"Enable ShoalExtraction";
			this->toolStripButtonShoalExtraction->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonShoalExtraction_Click);
			// 
			// toolStripButtonRF
			// 
			this->toolStripButtonRF->CheckOnClick = true;
			this->toolStripButtonRF->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->toolStripButtonRF->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonRF.Image")));
			this->toolStripButtonRF->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonRF->Name = L"toolStripButtonRF";
			this->toolStripButtonRF->Size = System::Drawing::Size(27, 27);
			this->toolStripButtonRF->Text = L"r(f)";
			this->toolStripButtonRF->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonRF_Click);
			// 
			// toolStripButtonRA
			// 
			this->toolStripButtonRA->CheckOnClick = true;
			this->toolStripButtonRA->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->toolStripButtonRA->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonRA.Image")));
			this->toolStripButtonRA->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonRA->Name = L"toolStripButtonRA";
			this->toolStripButtonRA->Size = System::Drawing::Size(29, 27);
			this->toolStripButtonRA->Text = L"r(a)";
			this->toolStripButtonRA->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonRA_Click);
			// 
			// toolStripProgressBar
			// 
			this->toolStripProgressBar->AutoSize = false;
			this->toolStripProgressBar->Margin = System::Windows::Forms::Padding(6, 2, 1, 1);
			this->toolStripProgressBar->Name = L"toolStripProgressBar";
			this->toolStripProgressBar->Size = System::Drawing::Size(100, 16);
			this->toolStripProgressBar->Style = System::Windows::Forms::ProgressBarStyle::Continuous;
			this->toolStripProgressBar->Visible = false;
			// 
			// toolStripLabelProgression
			// 
			this->toolStripLabelProgression->Name = L"toolStripLabelProgression";
			this->toolStripLabelProgression->Size = System::Drawing::Size(86, 27);
			this->toolStripLabelProgression->Text = L"toolStripLabel1";
			this->toolStripLabelProgression->Visible = false;
			// 
			// toolStripButtonEISupervised
			// 
			this->toolStripButtonEISupervised->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonEISupervised->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonEISupervised.Image")));
			this->toolStripButtonEISupervised->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonEISupervised->Name = L"toolStripButtonEISupervised";
			this->toolStripButtonEISupervised->Size = System::Drawing::Size(23, 27);
			this->toolStripButtonEISupervised->Text = L"Enable Supervised Echo-Integration";
			this->toolStripButtonEISupervised->Click += gcnew System::EventHandler(this, &Form1::toolStripButtonEISupervised_Click);
			// 
			// m_thresholdScrollBar
			// 
			this->m_thresholdScrollBar->Location = System::Drawing::Point(659, 0);
			this->m_thresholdScrollBar->Name = L"m_thresholdScrollBar";
			this->m_thresholdScrollBar->Size = System::Drawing::Size(129, 26);
			this->m_thresholdScrollBar->TabIndex = 7;
			this->m_thresholdScrollBar->Value = 45;
			this->m_thresholdScrollBar->ValueChanged += gcnew System::EventHandler(this, &Form1::m_thresholdScrollBar_ValueChanged);
			// 
			// m_minThresholdValueLabel
			// 
			this->m_minThresholdValueLabel->Location = System::Drawing::Point(791, 0);
			this->m_minThresholdValueLabel->Name = L"m_minThresholdValueLabel";
			this->m_minThresholdValueLabel->Size = System::Drawing::Size(51, 30);
			this->m_minThresholdValueLabel->TabIndex = 8;
			this->m_minThresholdValueLabel->Text = L" dB";
			this->m_minThresholdValueLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// m_maxThresholdValueLabel
			// 
			this->m_maxThresholdValueLabel->Location = System::Drawing::Point(848, 0);
			this->m_maxThresholdValueLabel->Name = L"m_maxThresholdValueLabel";
			this->m_maxThresholdValueLabel->Size = System::Drawing::Size(51, 30);
			this->m_maxThresholdValueLabel->TabIndex = 8;
			this->m_maxThresholdValueLabel->Text = L" dB";
			this->m_maxThresholdValueLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// imageListView3dB
			// 
			this->imageListView3dB->ImageStream = (cli::safe_cast<System::Windows::Forms::ImageListStreamer^>(resources->GetObject(L"imageListView3dB.ImageStream")));
			this->imageListView3dB->TransparentColor = System::Drawing::Color::Transparent;
			this->imageListView3dB->Images->SetKeyName(0, L"Noviews3d.png");
			this->imageListView3dB->Images->SetKeyName(1, L"views3d.png");
			// 
			// buttonDock3DView
			// 
			this->buttonDock3DView->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->buttonDock3DView->Appearance = System::Windows::Forms::Appearance::Button;
			this->buttonDock3DView->FlatAppearance->BorderSize = 0;
			this->buttonDock3DView->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
			this->buttonDock3DView->ImageIndex = 1;
			this->buttonDock3DView->ImageList = this->imageListDock;
			this->buttonDock3DView->Location = System::Drawing::Point(1108, 31);
			this->buttonDock3DView->Name = L"buttonDock3DView";
			this->buttonDock3DView->Size = System::Drawing::Size(21, 21);
			this->buttonDock3DView->TabIndex = 0;
			this->buttonDock3DView->UseVisualStyleBackColor = false;
			this->buttonDock3DView->CheckedChanged += gcnew System::EventHandler(this, &Form1::buttonDock3DView_CheckedChanged);
			// 
			// imageListDock
			// 
			this->imageListDock->ImageStream = (cli::safe_cast<System::Windows::Forms::ImageListStreamer^>(resources->GetObject(L"imageListDock.ImageStream")));
			this->imageListDock->TransparentColor = System::Drawing::Color::Transparent;
			this->imageListDock->Images->SetKeyName(0, L"dock.png");
			this->imageListDock->Images->SetKeyName(1, L"undock.png");
			// 
			// TimerGenericEvent
			// 
			this->TimerGenericEvent->Enabled = true;
			this->TimerGenericEvent->Interval = 1;
			this->TimerGenericEvent->Tick += gcnew System::EventHandler(this, &Form1::Timer_Tick);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(604, 7);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(28, 13);
			this->label2->TabIndex = 10;
			this->label2->Text = L"Shift";
			// 
			// numericUpDown3
			// 
			this->numericUpDown3->Location = System::Drawing::Point(607, 29);
			this->numericUpDown3->Name = L"numericUpDown3";
			this->numericUpDown3->Size = System::Drawing::Size(120, 20);
			this->numericUpDown3->TabIndex = 9;
			// 
			// numericUpDown4
			// 
			this->numericUpDown4->Location = System::Drawing::Point(453, 18);
			this->numericUpDown4->Name = L"numericUpDown4";
			this->numericUpDown4->Size = System::Drawing::Size(120, 20);
			this->numericUpDown4->TabIndex = 8;
			// 
			// panel3
			// 
			this->panel3->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel3->Controls->Add(this->checkBox2);
			this->panel3->Location = System::Drawing::Point(306, 3);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(95, 47);
			this->panel3->TabIndex = 7;
			// 
			// checkBox2
			// 
			this->checkBox2->AutoSize = true;
			this->checkBox2->Location = System::Drawing::Point(22, 14);
			this->checkBox2->Name = L"checkBox2";
			this->checkBox2->Size = System::Drawing::Size(68, 17);
			this->checkBox2->TabIndex = 5;
			this->checkBox2->Text = L"Opacity2";
			this->checkBox2->UseVisualStyleBackColor = true;
			// 
			// panel4
			// 
			this->panel4->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel4->Controls->Add(this->trackBar2);
			this->panel4->Controls->Add(this->label3);
			this->panel4->Location = System::Drawing::Point(8, 3);
			this->panel4->Name = L"panel4";
			this->panel4->Size = System::Drawing::Size(292, 47);
			this->panel4->TabIndex = 6;
			// 
			// trackBar2
			// 
			this->trackBar2->LargeChange = 1000;
			this->trackBar2->Location = System::Drawing::Point(3, 0);
			this->trackBar2->Maximum = 12000;
			this->trackBar2->Minimum = 1000;
			this->trackBar2->Name = L"trackBar2";
			this->trackBar2->Size = System::Drawing::Size(160, 45);
			this->trackBar2->SmallChange = 500;
			this->trackBar2->TabIndex = 4;
			this->trackBar2->TickFrequency = 1000;
			this->trackBar2->Value = 7000;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(187, 14);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(54, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"Threshold";
			// 
			// panel5
			// 
			this->panel5->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel5->Controls->Add(this->checkBox3);
			this->panel5->Location = System::Drawing::Point(8, 41);
			this->panel5->Name = L"panel5";
			this->panel5->Size = System::Drawing::Size(69, 32);
			this->panel5->TabIndex = 7;
			// 
			// checkBox3
			// 
			this->checkBox3->AutoSize = true;
			this->checkBox3->Location = System::Drawing::Point(3, 10);
			this->checkBox3->Name = L"checkBox3";
			this->checkBox3->Size = System::Drawing::Size(68, 17);
			this->checkBox3->TabIndex = 5;
			this->checkBox3->Text = L"Opacity3";
			this->checkBox3->UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(453, 7);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(98, 13);
			this->label5->TabIndex = 11;
			this->label5->Text = L"Number of PingFan";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(604, 7);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(28, 13);
			this->label6->TabIndex = 10;
			this->label6->Text = L"Shift";
			// 
			// numericUpDown1
			// 
			this->numericUpDown1->Location = System::Drawing::Point(607, 29);
			this->numericUpDown1->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000, 0, 0, 0 });
			this->numericUpDown1->Name = L"numericUpDown1";
			this->numericUpDown1->Size = System::Drawing::Size(120, 20);
			this->numericUpDown1->TabIndex = 9;
			// 
			// numericUpDown2
			// 
			this->numericUpDown2->Location = System::Drawing::Point(456, 29);
			this->numericUpDown2->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000, 0, 0, 0 });
			this->numericUpDown2->Name = L"numericUpDown2";
			this->numericUpDown2->Size = System::Drawing::Size(120, 20);
			this->numericUpDown2->TabIndex = 8;
			// 
			// panel6
			// 
			this->panel6->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel6->Controls->Add(this->trackBar3);
			this->panel6->Controls->Add(this->label7);
			this->panel6->Location = System::Drawing::Point(8, 3);
			this->panel6->Name = L"panel6";
			this->panel6->Size = System::Drawing::Size(220, 32);
			this->panel6->TabIndex = 6;
			// 
			// trackBar3
			// 
			this->trackBar3->LargeChange = 1000;
			this->trackBar3->Location = System::Drawing::Point(3, 0);
			this->trackBar3->Maximum = 12000;
			this->trackBar3->Minimum = 1000;
			this->trackBar3->Name = L"trackBar3";
			this->trackBar3->Size = System::Drawing::Size(160, 45);
			this->trackBar3->SmallChange = 500;
			this->trackBar3->TabIndex = 4;
			this->trackBar3->TickFrequency = 1000;
			this->trackBar3->Value = 7000;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(160, 14);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(54, 13);
			this->label7->TabIndex = 5;
			this->label7->Text = L"Threshold";
			// 
			// saveFileDialog1
			// 
			this->saveFileDialog1->DefaultExt = L"avi";
			this->saveFileDialog1->Filter = L"\"Avi file\"|*.avi";
			this->saveFileDialog1->RestoreDirectory = true;
			// 
			// backgroundWorker
			// 
			this->backgroundWorker->WorkerSupportsCancellation = true;
			this->backgroundWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Form1::backgroundWorker_DoWork);
			this->backgroundWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &Form1::backgroundWorker_RunWorkerCompleted);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1132, 768);
			this->Controls->Add(this->buttonDock3DView);
			this->Controls->Add(this->tableLayoutPanel1);
			this->Controls->Add(this->menuStrip1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"MOVIES3D";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Form1::Form1_FormClosing);
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Form1::Form1_KeyPress);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->tableLayoutPanel1->ResumeLayout(false);
			this->splitViewGlobal->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitViewGlobal))->EndInit();
			this->splitViewGlobal->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitView2D))->EndInit();
			this->splitView2D->ResumeLayout(false);
			this->flowLayoutPanel1->ResumeLayout(false);
			this->flowLayoutPanel1->PerformLayout();
			this->toolStripTools->ResumeLayout(false);
			this->toolStripTools->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown4))->EndInit();
			this->panel3->ResumeLayout(false);
			this->panel3->PerformLayout();
			this->panel4->ResumeLayout(false);
			this->panel4->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar2))->EndInit();
			this->panel5->ResumeLayout(false);
			this->panel5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->EndInit();
			this->panel6->ResumeLayout(false);
			this->panel6->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar3))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	public:
		ReaderCtrlManaged	^GetReadCtrl() { return mReadCtrl; };

	private: ReaderCtrlManaged	^mReadCtrl;
	private: CModuleManager *m_pModMgr;
	public: static FuncMessDelegate ^mFuncMessDelegate;
	public: static UpdateToolBarDelegate ^mUpdateToolBarDelegate;
	private: bool	m_SaveAsAvi;
	private: ShoalExtractionConsumer ^m_ShoalConsumer;
	private: DisplayView ^m_displayView;
	private: StatForm ^m_moduleStat;
	private: LogForm ^m_logForm;
	private: System::Windows::Forms::Timer^  m_timerLogIndicator;
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
		// permet d'intercepter les touches du clavier
		KeyPreview = true;
		m_LastReadState = eReadCont;
		this->KeyDown += gcnew KeyEventHandler(this, &Form1::Form1_KeyDown);
	}
	private: ReadState m_LastReadState;

	private: System::Void loadToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void loadDir(const std::string & fileType);
	private: System::Void loadHacDirToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void loadSonarNetCDFDirToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void acquireToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);


	private: System::Void readerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		ReaderParamForm ^readerDlg = gcnew ReaderParamForm();
		readerDlg->ShowDialog();
	}

	private: System::Void KernelParameter_Click(System::Object^  sender, System::EventArgs^  e) {
		KernelParamForm ^Dlg = gcnew KernelParamForm();
		Dlg->ShowDialog();
	}
			 
	private: System::Void UpdateViewButton()
	{
		if (m_displayView->VolumicView->is3DViewFrozen())
		{
			this->toolStripButtonFreeze3DView->Image = this->imageListView3dB->Images[1];
		}
		else
		{
			this->toolStripButtonFreeze3DView->Image = this->imageListView3dB->Images[0];
		}
	}

	private: System::Void toolStripButtonFreeze3DView_Click(System::Object^  sender, System::EventArgs^  e) {
		/// TODONMD ???
		/* m_displayView->m_volumicViewDisplay->mWrap->Freeze(!m_displayView->m_volumicViewDisplay->mWrap->isFrozen());
		if(m_displayView->m_volumicViewDisplay->mWrap->isFrozen())
		this->TimerDraw->Stop();
		else
		this->TimerDraw->Start();

		UpdateViewButton();*/
	}

	private: System::Void UpdatePlayButtonState(ReadState newValue);
	private: System::Void ReadStateChanging(ReadState oldValue, ReadState newValue);
	private: System::Void Form1_KeyDown(System::Object^  sender, KeyEventArgs^  e);

	private: System::Void toolStripButtonReadChunk_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void toolStripButtonPauseChunk_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void toolStripButtonReadOne_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void toolStripButtonGoTo_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void resetCameraToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
		m_displayView->VolumicView->DisplayCameraSettings();
	}

	private: System::Void parameterToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		DisplayViewParameter();
	}

	private: ViewParameterForm ^ m_viewParameterForm;
	private: System::Void DisplayViewParameter()
	{
		m_viewParameterForm->Show();
	}
	private: System::Void FilterToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		FilterModuleForm ^form = gcnew FilterModuleForm();
		form->ShowDialog();
	}


	private: System::Void Timer_Tick(System::Object^  sender, System::EventArgs^  e) {
		bool paused = this->mReadCtrl->UpdateEvent();

		bool NeedReCompute = m_displayView->UpdatePrivateParameter();

		// OTK - les bancs sont r�cup�r�s de mani�re asynchrone (car le module d'extraction
		// les extrait de mani�re asynchrone) lorsque la lecture n'est pas active.
		M3DKernel::GetInstance()->Lock();
		if (paused && m_ShoalConsumer->Consume(this->m_displayView))
		{
			NeedReCompute = true;
		}
		M3DKernel::GetInstance()->Unlock();

		if (NeedReCompute)
		{
			CallDisplayDraw();
		}
	}
	private: delegate System::Void CallDisplayDrawDelegate();
	private: System::Void CallDisplayDraw();
	private: System::Void ReadEventProcess(const ChunckEventList &newValue);
	private: System::Diagnostics::Stopwatch m_frameRateLimiter;
	private: bool m_bIsDrawing;

	private: System::Void Form1_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
		mReadCtrl->Exit();

		SynchronizedtreatmentCancel();
		mSelectionMgr->ResetSelection();
	}



	private: System::Void backgroundColorToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		m_displayView->VolumicView->DisplayBackGroundColorChoice();
	}

	private: System::Void RecomputeVolume() {
		this->m_displayView->RecomputeView();
	}

	private: System::Void transducerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

		TransducerModifier ^ref = gcnew TransducerModifier(
			*(m_pModMgr->GetCompensationModule()),
			gcnew UpdateTransducerChange(this, &MOVIESVIEWCPP::Form1::RecomputeVolume));
		ref->Show();
	}

	private: System::Void displayTimeLogToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {


		KernelParameter param = M3DKernel::GetInstance()->GetRefKernelParameter();
		displayTimeLogToolStripMenuItem->Checked = !displayTimeLogToolStripMenuItem->Checked;

		if (displayTimeLogToolStripMenuItem->Checked == true)
			param.setLogPingFanInterTime(true);
		else
			param.setLogPingFanInterTime(false);
		M3DKernel::GetInstance()->UpdateKernelParameter(param);
	}

	private: System::Void setFixedSpeedToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		// OTK - FAE052 - parametrage possible d'une vitesse fixe
		SpeedParamForm ^dlg = gcnew SpeedParamForm();
		dlg->ShowDialog();
		setFixedSpeedToolStripMenuItem->Checked =
			M3DKernel::GetInstance()->GetRefKernelParameter().getSpeedUsed() == SPUSER;
	}

			 // IPSIS - OTK - ajout IHM de param�trage du module d'�cho-int�gration
	private: System::Void echoIntegrationParameterToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		EchoIntegrationLayerForm ^ref = gcnew EchoIntegrationLayerForm(mReadCtrl);
		ref->ShowDialog();
	}
			 // IPSIS - OTK - Ajout de la gestion des fichiers de configuration (save)
	private: System::Void saveConfigurationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);

			 // IPSIS - OTK - Ajout de la gestion des fichiers de configuration (load...)
	private: System::Void loadConfigurationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);

			 // IPSIS - OTK - ajout IHM de param�trage du manager d'ESU (MovESUMgr)
	private: System::Void eSUManagerToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		ESUManagerForm ^ref = gcnew ESUManagerForm(mReadCtrl);
		ref->ShowDialog();
	}
			 
			 // IPSIS - OTK - Ajout de la gestion des fichiers de configuration (save as...)
	private: System::Void saveConfigurationAsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);

	private:
		ShoalExtractionForm ^ m_shoalExtractionForm;
		ViewSounderParameterForm ^ m_viewSounderParameterForm;
		System::Void showShoalExtractionForm();

	private: System::Void shoalExtractionToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		showShoalExtractionForm();
	}
	private: System::Void shoalListToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		ShoalDescForm ^pForm = gcnew ShoalDescForm(m_ShoalConsumer);
		pForm->Show();
	}
	private: System::Void UpdateShoalButtonState();
	private: System::Void toolStripButtonShoalExtraction_Click(System::Object^  sender, System::EventArgs^  e) {
		showShoalExtractionForm();
	}

	private: System::Void toolStripButtonFilter_Click(System::Object^  sender, System::EventArgs^  e) {
		FilterModuleForm ^form = gcnew FilterModuleForm();
		form->ShowDialog();
	}



	private: System::Void UpdateModuleStat()
	{
		if (this->m_moduleStat && this->Visible)
			m_moduleStat->UpdateStat();
	}
	private: System::Void moduleStatToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!this->m_moduleStat)
			m_moduleStat = gcnew StatForm();
		moduleStatToolStripMenuItem->Checked = !moduleStatToolStripMenuItem->Checked;
		if (moduleStatToolStripMenuItem->Checked)
		{
			m_moduleStat->Show();
		}
		else
		{
			m_moduleStat->Hide();
		}
		UpdateModuleStat();
	}
	private: System::Void saveAsRotatingAviToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (this->saveFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			m_displayView->VolumicView->saveAvi(this->saveFileDialog1->FileName);
		}
	}

	private: System::Void loggingWindowToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void toolStripMenuItemErrors_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void timerLogIndicator_Tick(Object^  sender, EventArgs^  e);

	private: System::Void toolStripMenuItem2_Click(System::Object^  sender, System::EventArgs^  e) {
		DisplayViewParameter();
	}

	private: System::Void sounderParameterToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		this->m_displayView->VolumicView->SounderParameterVisible = this->sounderParameterToolStripMenuItem->Checked;
		this->toolStripButtonSounder->Checked = this->m_displayView->VolumicView->SounderParameterVisible;
	}
	private: System::Void dViewToolStripMenuItem_DropDownOpened(System::Object^  sender, System::EventArgs^  e) {
		this->sounderParameterToolStripMenuItem->Checked = this->m_displayView->VolumicView->SounderParameterVisible;
	}


	private: System::Void ResetDepthView();
	
	private: System::Void saveAsVideoToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

		saveAsVideoToolStripMenuItem->Checked = !saveAsVideoToolStripMenuItem->Checked;
		if (saveAsVideoToolStripMenuItem->Checked == true)
			m_SaveAsAvi = true;
		else
			m_SaveAsAvi = false;
	}
	private: System::Void editToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		// OTK - on doit mettre � jour les checkboxes du menu, car si on charge une configuration,
		// leur �tat a pu changer autrement que par un click dessus
		KernelParameter params = M3DKernel::GetInstance()->GetRefKernelParameter();
		displayTimeLogToolStripMenuItem->Checked = params.getLogPingFanInterTime();
		setFixedSpeedToolStripMenuItem->Checked = (params.getSpeedUsed() == SPUSER);
		setFixedPositionToolStripMenuItem->Checked = params.getPositionUsed() == POSITION_USER;

	}
	private: System::Void optionsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		// mise � jour des checkbox en cas de fermeture manuelle des fen�tres log et stat
		bool logChecked = false;
		if (m_logForm)
		{
			logChecked = m_logForm->Visible;
		}
		loggingWindowToolStripMenuItem->Checked = logChecked;
		bool statChecked = false;
		if (m_moduleStat)
		{
			statChecked = m_moduleStat->Visible;
		}
		moduleStatToolStripMenuItem->Checked = statChecked;
	}


	private: System::Void toolStripButtonZoom_Click(System::Object^  sender, System::EventArgs^  e) {
		if (toolStripButtonZoom->Checked)
		{
			CurrentMode = MODE_ZOOM;
		}
		else
		{
			CurrentMode = MODE_NONE;
		}
	}

	private: System::Void toolStripButtonCut_Click(System::Object^  sender, System::EventArgs^  e) {
		if (toolStripButtonCut->Checked)
		{
			m_displayView->VolumicView->addPlaneWidget();
		}
		else
		{
			m_displayView->VolumicView->removePlaneWidget();
		}
	}
			 // Positionnement automatique des splitters sur double-clic
	private: System::Void splitViewGlobal_DoubleClick(System::Object^  sender, System::EventArgs^  e) {
		int nbPixels = (int)round(m_displayView->DefaultFlatSideView->GetVerticalMargin());
		splitViewGlobal->SplitterDistance += nbPixels;
	}
			 // Positionnement automatique des splitters sur double-clic
	private: System::Void splitView2D_DoubleClick(System::Object^  sender, System::EventArgs^  e)
	{
		int nbPixels = (int)round(-2 * m_displayView->DefaultFlatFrontView->GetHorizontalMargin());
		if (nbPixels == 0)
		{
			// si pas de marge horizontale, on essaye de combler le trou sous la vue de face
			nbPixels = (int)round(m_displayView->DefaultFlatFrontView->GetVerticalMargin());
			splitView2D->SplitterDistance *= m_displayView->DefaultFlatFrontView->GetVerticalStretchRatio();
		}
		else
		{
			splitView2D->SplitterDistance += nbPixels;
		}
	}
	private: System::Void toolStripButtonRF_Click(System::Object^  sender, System::EventArgs^  e)
	{
		if (toolStripButtonRF->Checked)
		{
			// si on a une s�lection ....
			if (mSelectionMgr->GetSelection() != nullptr)
			{
				// d�sactivation du mode s�lection
				//toolStripButtonSelection->Checked = false;
				//m_displayView->m_flatSideViewDisplay->LeaveSelectionMode();
				//m_displayView->m_flatFrontViewDisplay->LeaveSelectionMode();

				// lancement du calcul asynchrone de type eRF
				ASyncTreatment^ ast = gcnew ASyncTreatment(eRF);
				backgroundWorker->RunWorkerAsync(ast);
			}
			else
			{
				MessageBox::Show("No selection.");
				toolStripButtonRF->Checked = false;
			}
		}
		else
		{
			// annulation du traitement asynchrone de type eRF
			SynchronizedtreatmentCancel();
			toolStripButtonRF->Enabled = false;
		}
	}

	private: System::Void toolStripButtonRA_Click(System::Object^  sender, System::EventArgs^  e)
	{
		if (toolStripButtonRA->Checked)
		{
			// si on a une s�lection ....
			if (mSelectionMgr->GetSelection() != nullptr)
			{
				// lancement du calcul asynchrone de type eRA
				ASyncTreatment^ ast = gcnew ASyncTreatment(eRA);
				backgroundWorker->RunWorkerAsync(ast);
			}
			else
			{
				MessageBox::Show("No selection.");
				toolStripButtonRA->Checked = false;
			}
		}
		else
		{
			// annulation du traitement asynchrone de type eRA
			SynchronizedtreatmentCancel();
			toolStripButtonRA->Enabled = false;
		}
	}

	private: System::Void toolStripButtonSelection_Click(System::Object^  sender, System::EventArgs^  e) {
		if (toolStripButtonSelection->Checked)
		{
			// on passe la vue 2D sur le transducteur de r�f�rence

			/*CompensationModule* pModule = m_pModMgr->GetCompensationModule();
			if(pModule!=NULL)
			{
			  String ^ transname = cppStr2NetStr(pModule->GetCompensationParameter().m_rfParameter.GetRefTransducer().c_str());
			  if(m_displayView != nullptr)
			  {
				m_displayView->m_flatSideViewDisplay->SetTransducer(transname);
				m_displayView->m_flatFrontViewDisplay->SetTransducer(transname);
			  }
			}
			*/

			CurrentMode = MODE_SELECTION;
		}
		else
		{
			CurrentMode = MODE_NONE;
		}
	}

	private: System::Void toolStripButtonCamera_Click(System::Object^  sender, System::EventArgs^  e) {
		m_displayView->VolumicView->DisplayCameraSettings();
	}
	private: System::Void toolStripButtonSounder_Click(System::Object^  sender, System::EventArgs^  e) {
		this->m_displayView->VolumicView->SounderParameterVisible = !this->m_displayView->VolumicView->SounderParameterVisible;
	}
	private: System::Void toolStripButtonColor_Click(System::Object^  sender, System::EventArgs^  e) {
		m_displayView->VolumicView->DisplayBackGroundColorChoice();
	}
	private: void UpdateToolBar()
	{
		this->toolStripButtonSounder->Checked = this->m_displayView->VolumicView->SounderParameterVisible;
	}
	private: System::Void frequentialResponseToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e)
	{
		const std::string refTransName = m_pModMgr->GetCompensationModule()->GetCompensationParameter().m_rfParameter.GetRefTransducer();
		RFSettingsForm ^rfSettingsForm = gcnew RFSettingsForm(backgroundWorker);
		rfSettingsForm->Init();
		rfSettingsForm->ShowDialog();
		if (rfSettingsForm->OverlapContextChanged())
		{
			mSelectionMgr->SetHasNewData(true);
		}
		const std::string newRefTransName = m_pModMgr->GetCompensationModule()->GetCompensationParameter().m_rfParameter.GetRefTransducer();
		// si le transducteur de r�f�rence � chang�, on doit RAZ la s�lection des bancs
		if (newRefTransName.compare(refTransName))
		{
			mSelectionMgr->ResetSelectShoals();

			// rafraichissement de la vue de profil pour le recalcul eventuel du NASC de la s�lection
			M3DKernel::GetInstance()->Lock();
			m_displayView->DrawSideViews();

			M3DKernel::GetInstance()->Unlock();
		}
	}

	private: delegate void ProgressChangedDelegate(ASyncTreatmentProgresStatus^ status);
	private: System::Void Form1::UpdateProgressInfo(ASyncTreatmentProgresStatus^ status)
	{
		// on affiche la barre et le label de progression
		toolStripProgressBar->Visible = true;
		toolStripLabelProgression->Visible = true;
		toolStripLabelProgression->Text = status->StateDesc;
		toolStripProgressBar->Value = Math::Max(status->Percentage, toolStripProgressBar->Value);
	}

	public: System::Void Form1::ProgressChanged(const char* stateDesc, int percentage)
	{
		ASyncTreatmentProgresStatus^ status = gcnew ASyncTreatmentProgresStatus(cppStr2NetStr(stateDesc), percentage);
		this->BeginInvoke(gcnew ProgressChangedDelegate(this, &Form1::UpdateProgressInfo), status);
	}

			// callback permettant de savoir si une demande d'annulation du traitement a �t� demand�e
	public: bool Form1::CancelTreatmentPending()
	{
		return backgroundWorker->CancellationPending;
	}
	private:
		System::Void launchTreatment(TreatmentType treatmentType) {

			// si on est pas en lecture, on utilise autant de threads que de coeurs disponibles
			bool nbReservedCores = GetReadCtrl()->IsPause() ? 0 : 1;
			CompensationModule* pModule = m_pModMgr->GetCompensationModule();
			pModule->GetCompensationParameter().m_overlapParameter.SetNbReservedCores(nbReservedCores);

			// r�cup�ration des param�tres d'extraction des bancs
			ShoalExtractionModule * pShoalExtractionModule = m_pModMgr->GetShoalExtractionModule();
			if (pShoalExtractionModule != NULL)
			{
				pModule->GetCompensationParameter().m_rfParameter.SetAcrossIntegrationDistance(
					pShoalExtractionModule->GetShoalExtractionParameter().GetAcrossIntegrationDistance());
				pModule->GetCompensationParameter().m_rfParameter.SetAlongIntegrationDistance(
					pShoalExtractionModule->GetShoalExtractionParameter().GetAlongIntegrationDistance());
				pModule->GetCompensationParameter().m_rfParameter.SetVerticalIntegrationDistance(
					pShoalExtractionModule->GetShoalExtractionParameter().GetVerticalIntegrationDistance());
			}

			// inscription du module en tant que PingFanConsumer. La destruction des pings par le thread de lecture 
			// est soumise � l'accord de ce pingfanconsumer.
			pModule->SetLastComputedPing(mSelectionMgr->GetSelection()->min->ping);
			M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().AddPingFanConsumer(pModule);

			// r�cup�ration de la d�finition de la zone � traiter depuis le SelectionManager
			pModule->GetCompensationParameter().m_overlapParameter.m_depthMin = mSelectionMgr->GetSelection()->min->depth;
			pModule->GetCompensationParameter().m_overlapParameter.m_depthMax = mSelectionMgr->GetSelection()->max->depth;
			pModule->GetCompensationParameter().m_overlapParameter.m_pingFilteredStart = mSelectionMgr->GetSelection()->min->ping;
			pModule->GetCompensationParameter().m_overlapParameter.m_pingFilteredStop = mSelectionMgr->GetSelection()->max->ping;
			mSelectionMgr->ConfigureShoalList(pModule);

			// si le dernier ping de la fen�tre n'existe plus, pas la peine de lancer le traitement...
			// la s�lection est vide dans ce cas.
			PingFan * pFan = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(mSelectionMgr->GetSelection()->max->ping);
			if (pFan != NULL)
			{
				// cr�ation du callback pour les informations de progression
				mASyncTreatmentProgressChanged = gcnew ASyncTreatmentProgressChanged(this, &Form1::ProgressChanged);
				// Marshalling vers un callback C++ natif
				IntPtr progressCB = Marshal::GetFunctionPointerForDelegate(mASyncTreatmentProgressChanged);

				// cr�ation du callback pour permettre l'annulation du traitement
				mASyncTreatmentCancellationPending = gcnew ASyncTreatmentCancellationPending(this, &Form1::CancelTreatmentPending);
				// Marshalling vers un callback C++ natif
				IntPtr cancelCB = Marshal::GetFunctionPointerForDelegate(mASyncTreatmentCancellationPending);


				// lancement du calcul
				bool needOverlapCalculation = mSelectionMgr->HasNewData()
					&& pModule->GetCompensationParameter().m_overlapParameter.IsEnable();

				pModule->GetCompensationParameter().m_rfParameter.SetIsMultiBeam(treatmentType == TreatmentType::eRA);

				pModule->RunFrequencyResponse(needOverlapCalculation,
					(PROGRESS_CALLBACK)(void*)progressCB,
					(CANCEL_CALLBACK)(void*)cancelCB);
			}

			// RAZ de la liste des bancs
			pModule->GetCompensationParameter().m_rfParameter.GetShoals().clear();


			// calcul fini : on n'est plus consommateur des pingfans, qui peuvent maintenant �tre d�truits par la lecture
			M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().RemovePingFanConsumer(pModule);
		}
			// Utilisation du backgroundworker pour les traitement asynchrones lourds (r�ponse fr�quentielle, etc...)
	private: System::Void backgroundWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
		ASyncTreatment^ treatment = (ASyncTreatment^)e->Argument;
		switch (treatment->mType)
		{
		case eRA:
		case eRF:
			launchTreatment(treatment->mType);
		break;
		default:
		{
			assert(0); // type de traitement asynchrone inconnu
		}
		break;
		}

		// on passe en sortie le type de calcul pour pouvoir faire le post-traitement adequat.
		e->Result = e->Argument;

		// on informe qu'il y a eu annulation
		e->Cancel = backgroundWorker->CancellationPending;

		// signalement de la fin du traitement asynchrone
		Monitor::Enter(m_bgwMonitor);
		try
		{
			Monitor::PulseAll(m_bgwMonitor);
		}
		finally
		{
			Monitor::Exit(m_bgwMonitor);
		}
	}

	private: System::Void DisplayResults(bool isMultiBeam)
	{
		// pour ne pas recalculer l'overlap si on relance le calcul sur la m�me zone et avec les m�mes param�tres
		mSelectionMgr->SetHasNewData(false);

		//retrieve compensation module
		CompensationModule * pModule = m_pModMgr->GetCompensationModule();

		if (pModule->GetComputer(isMultiBeam)->HasResults())
		{
			if (isMultiBeam) {
				//Display RA                     
				if (m_raDisplayView == nullptr) {
					m_raDisplayView = gcnew RFDisplayView();
					m_raDisplayView->Text = "Angular Response";
					m_displayView->DefaultFlatFrontView->AddUserControl(m_raDisplayView);
				}
				m_raDisplayView->SetRFResult(pModule->GetComputer(isMultiBeam));
				m_displayView->DefaultFlatFrontView->SelectUserControl(m_raDisplayView);
			}
			else {
				//Display RF                     
				if (m_rfDisplayView == nullptr) {
					m_rfDisplayView = gcnew RFDisplayView();
					m_rfDisplayView->Text = "Frequency Response";
					m_displayView->DefaultFlatFrontView->AddUserControl(m_rfDisplayView);
				}
				m_rfDisplayView->SetRFResult(pModule->GetComputer(isMultiBeam));
				m_displayView->DefaultFlatFrontView->SelectUserControl(m_rfDisplayView);
			}

			// OTK - 04/11/2009 - finalement pas tr�s pratique, on ne fait plus de zoom auto
			/*
			// Dans le cas du rejeu, mise en pause de la lecture et zoom sur la zone d'int�r�t
			MovFileRun fileRun;
			if(mReadCtrl->HasMovFileRun(fileRun))
			{
			// pause
			mReadCtrl->Pause();

			// zoom
			m_displayView->m_flatSideViewDisplay->OnZoom(
			pModule->GetCompensationParameter().m_overlapParameter.m_pingFilteredStart,
			pModule->GetCompensationParameter().m_overlapParameter.m_pingFilteredStop,
			pModule->GetCompensationParameter().m_overlapParameter.m_depthMin,
			pModule->GetCompensationParameter().m_overlapParameter.m_depthMax
			);
			}*/
		}
		else
		{
			MessageBox::Show("No Results.");
		}
	}
			 // terminaison du backgroundworker
	private: System::Void backgroundWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e) {

		this->toolStripProgressBar->Value = 0;
		this->toolStripProgressBar->Visible = false;
		this->toolStripLabelProgression->Text = "";
		this->toolStripLabelProgression->Visible = false;

		if (!e->Cancelled)
		{
			ASyncTreatment^ treatment = (ASyncTreatment^)e->Result;
			switch (treatment->mType)
			{
			case eRA:
				DisplayResults(true);
				break;
			case eRF:
				DisplayResults(false);
				break;
			break;
			default:
				assert(0); // type de traitement asynchrone inconnu
				break;
			}
		}

		// on permet le lancement d'un nouveau traitement asynchrone
		if (((ASyncTreatment^)e->Result)->mType == TreatmentType::eRA) {
			toolStripButtonRA->Checked = false;
			toolStripButtonRA->Enabled = true;
		}
		else {
			toolStripButtonRF->Checked = false;
			toolStripButtonRF->Enabled = true;
		}
	}

	private: System::Void SynchronizedtreatmentCancel()
	{
		Monitor::Enter(m_bgwMonitor);
		try
		{
			if (backgroundWorker->IsBusy)
			{
				backgroundWorker->CancelAsync();
				Monitor::Wait(m_bgwMonitor);
			}
		}
		finally
		{
			Monitor::Exit(m_bgwMonitor);
		}
	}

	private: System::Void Form1::RefreshVolumicView()
	{
		// rafraichissement de la vue 3D car nouveau volume de banc
		this->BeginInvoke(gcnew CallDisplayDrawDelegate(this, &Form1::CallDisplayDraw));
	}

	private: System::Void buttonDock3DView_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

	private: System::Void Dock3DView();

	private: delegate void RedrawFlatSideViewDelegate();
	private: RedrawFlatSideViewDelegate^ mRedrawFlatSideViewDelegate;

	private: System::Void RedrawFlatSideView()
	{
		m_displayView->RedrawFlatSideViews();
	}

	private: System::Void OnFlatViewDockStateChanged();

	private: System::Void setFixedPositionToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		PositionParamForm ^Dlg = gcnew PositionParamForm();
		Dlg->ShowDialog();
		setFixedPositionToolStripMenuItem->Checked =
			M3DKernel::GetInstance()->GetRefKernelParameter().getPositionUsed() == POSITION_USER;
	}
	private: System::Void toolStripButtonBottomDetection_Click(System::Object^  sender, System::EventArgs^  e) {
		BottomDetectionParamForm ^Dlg = gcnew BottomDetectionParamForm();
		Mode oldMode = CurrentMode;
		Dlg->BottomCorrectionEnabled = oldMode == MODE_BOTTOM_CORRECTION;

		// On ne permet d'activer le mode que si la lecture est sur pause et qu'on n'est pas
		// en mode acquisition (rejeu de fichiers uniquement)
		Dlg->BottomCorrectionAvailable = mReadCtrl->IsPause() && mReadCtrl->IsFileService();

		// Affichage de la fen�tre ...
		Dlg->ShowDialog();
		if (Dlg->BottomCorrectionEnabled)
		{
			CurrentMode = MODE_BOTTOM_CORRECTION;
		}
		else if (oldMode == MODE_BOTTOM_CORRECTION)
		{
			CurrentMode = MODE_NONE;
		}
	}
	private: System::Void singleTargetDetectionToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		SingleTargetDetectionParamForm ^Dlg = gcnew SingleTargetDetectionParamForm();
		Dlg->ShowDialog();
		CallDisplayDraw();
	}
	private: System::Void projectionParameterToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		ProjectionParamForm ^Dlg = gcnew ProjectionParamForm();
		Dlg->ShowDialog();
		// 
		// rafraichissement de la visu 3D avec les nouveaux parametres
		m_displayView->VolumicView->GetViewFilter()->SetNeedToRegenerateVolume();
		m_displayView->VolumicView->RecomputeShoalDisplay();
		CallDisplayDraw();
	}

	private: System::Void environmentParameterToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		EnvironmentParameterForm^ Dlg = gcnew EnvironmentParameterForm();
		Dlg->ShowDialog();
	}

			 // OTK - FAE059 - modification du mode identify
	private: System::Void toolStripButtonIdentify_Click(System::Object^  sender, System::EventArgs^  e) {
		DisplayParameter::getInstance()->SetDisplayEchoTooltip(toolStripButtonIdentify->Checked);
	}

	private: System::Void ShowEISupervisedForm();

	private: System::Void OnEISupervisedStopped(System::Object^  sender, System::EventArgs^  e);

	private: System::Void supervisedEchoIntegrationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		ShowEISupervisedForm();
	}
	private: System::Void toolStripButtonEISupervised_Click(System::Object^  sender, System::EventArgs^  e) {
		ShowEISupervisedForm();
	}

	private: System::Void toolStripButtonPolygonSelection_Click(System::Object^  sender, System::EventArgs^  e)
	{
		if (toolStripButtonPolygonSelection->Checked)
		{
			CurrentMode = MODE_POLYGON;
		}
		else
		{
			CurrentMode = MODE_NONE;
		}
	}

	private: System::Void toolStripButtonEISupervisedSelection_Click(System::Object^  sender, System::EventArgs^  e)
	{
		if (toolStripButtonEISupervisedSelection->Checked)
		{
			CurrentMode = MODE_EI_SUPERVISED_SELECTION;
			if (!toolStripButtonShowEchotypes->Checked)
			{
				toolStripButtonShowEchotypes->PerformClick();
			}
		}
		else
		{
			CurrentMode = MODE_NONE;
		}
	}

	private: System::Void toolStripButtonEditBottom_Click(System::Object^  sender, System::EventArgs^  e)
	{
		if (toolStripButtonEditBottom->Checked)
		{
			CurrentMode = MODE_BOTTOM_CORRECTION;
		}
		else
		{
			CurrentMode = MODE_NONE;
		}
	}

	private: System::Void toolStripButtonShowEchotypes_Click(System::Object^  sender, System::EventArgs^  e)
	{
		if ((!toolStripButtonShowEchotypes->Checked) && toolStripButtonEISupervisedSelection->Checked)
		{
			toolStripButtonEISupervisedSelection->PerformClick();
		}
		for each(BaseFlatSideView ^ view in m_displayView->FlatSideViews)
		{
			view->SetEchotypesVisibility(toolStripButtonShowEchotypes->Checked);
			view->Invalidate(true);
		}
	}

			 // NMD - Handler pour rebasculer en mode selection EI supervis�e � la fin du dessin d'un polygone
	private: System::Void onPolygonSelectionFinished()
	{
		CurrentMode = MODE_EI_SUPERVISED_SELECTION;
	}


	public: property Mode CurrentMode
	{
		Mode get()
		{
			return m_currentMode;
		}
		void set(Mode mode)
		{
			if (m_currentMode != mode)
			{
				m_currentMode = mode;
				switch (m_currentMode)
				{
				case MODE_NONE:
					toolStripButtonZoom->Checked = false;
					toolStripButtonSelection->Checked = false;
					toolStripButtonPolygonSelection->Checked = false;
					toolStripButtonEISupervisedSelection->Checked = false;
					toolStripButtonEditBottom->Checked = false;
					LeaveBottomCorrectionMode();
					break;

				case MODE_ZOOM:
					toolStripButtonZoom->Checked = true;
					toolStripButtonSelection->Checked = false;
					toolStripButtonPolygonSelection->Checked = false;
					toolStripButtonEISupervisedSelection->Checked = false;
					toolStripButtonEditBottom->Checked = false;
					LeaveBottomCorrectionMode();
					break;

				case MODE_SELECTION:
					toolStripButtonZoom->Checked = false;
					toolStripButtonSelection->Checked = true;
					toolStripButtonPolygonSelection->Checked = false;
					toolStripButtonEISupervisedSelection->Checked = false;
					toolStripButtonEditBottom->Checked = false;
					LeaveBottomCorrectionMode();
					break;

				case MODE_POLYGON:
					toolStripButtonZoom->Checked = false;
					toolStripButtonSelection->Checked = false;
					toolStripButtonPolygonSelection->Checked = true;
					toolStripButtonEISupervisedSelection->Checked = false;
					toolStripButtonEditBottom->Checked = false;
					LeaveBottomCorrectionMode();
					EnterBottomCorrectionMode(); // rmq. : on re-pr�pare le CorrectionManager comme pour une correction du fond
					break;

				case MODE_EI_SUPERVISED_SELECTION:
					toolStripButtonZoom->Checked = false;
					toolStripButtonSelection->Checked = false;
					toolStripButtonPolygonSelection->Checked = false;
					toolStripButtonEISupervisedSelection->Checked = true;
					toolStripButtonEditBottom->Checked = false;
					LeaveBottomCorrectionMode();
					break;

				case MODE_BOTTOM_CORRECTION:
					toolStripButtonZoom->Checked = false;
					toolStripButtonSelection->Checked = false;
					toolStripButtonPolygonSelection->Checked = false;
					toolStripButtonEISupervisedSelection->Checked = false;
					toolStripButtonEditBottom->Checked = true;
					LeaveBottomCorrectionMode(); // rmq. au cas o� on vienne du mode polygone
					EnterBottomCorrectionMode();
					break;

				default:
					break;
				}

				for each(BaseFlatSideView ^ view in m_displayView->FlatSideViews)
				{
					switch (m_currentMode)
					{
					case MODE_NONE:
						view->LeaveZoomMode();
						view->LeaveSelectionMode();
						view->LeavePolygonSelectionMode();
						view->LeaveEISupervisedSelectionMode();
						view->LeaveBottomCorrectionMode();
						break;

					case MODE_ZOOM:
						view->EnterZoomMode();
						view->LeaveSelectionMode();
						view->LeavePolygonSelectionMode();
						view->LeaveEISupervisedSelectionMode();
						view->LeaveBottomCorrectionMode();
						break;

					case MODE_SELECTION:
						view->LeaveZoomMode();
						view->EnterSelectionMode();
						view->LeavePolygonSelectionMode();
						view->LeaveEISupervisedSelectionMode();
						view->LeaveBottomCorrectionMode();
						break;

					case MODE_POLYGON:
						view->LeaveZoomMode();
						view->LeaveSelectionMode();
						view->EnterPolygonSelectionMode(mCorrectionManager);
						view->LeaveEISupervisedSelectionMode();
						view->LeaveBottomCorrectionMode();
						break;

					case MODE_EI_SUPERVISED_SELECTION:
						view->LeaveZoomMode();
						view->LeaveSelectionMode();
						view->LeavePolygonSelectionMode();
						view->EnterEISupervisedSelectionMode();
						view->LeaveBottomCorrectionMode();
						break;

					case MODE_BOTTOM_CORRECTION:
						view->LeaveZoomMode();
						view->LeaveSelectionMode();
						view->LeavePolygonSelectionMode();
						view->LeaveEISupervisedSelectionMode();
						view->EnterBottomCorrectionMode(mCorrectionManager);
						break;

					default:
						break;
					}
				}

				for each(BaseFlatFrontView ^ view in m_displayView->FlatFrontViews)
				{
					switch (m_currentMode)
					{
					case MODE_NONE:
					case MODE_POLYGON:
					case MODE_EI_SUPERVISED_SELECTION:
					case MODE_BOTTOM_CORRECTION:
						view->LeaveZoomMode();
						view->LeaveSelectionMode();
						break;

					case MODE_ZOOM:
						view->EnterZoomMode();
						view->LeaveSelectionMode();
						break;

					case MODE_SELECTION:
						view->LeaveZoomMode();
						view->EnterSelectionMode();
						break;

					default:
						break;
					}
				}
			}
		}
	}

	private: System::Void EnterBottomCorrectionMode();
	private: System::Void LeaveBottomCorrectionMode();
	private: CorrectionManager * mCorrectionManager;
	private: System::Void onFormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);


	private: Mode m_currentMode;
	private: System::Void m_thresholdScrollBar_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void toolStripMenuItemHelp_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void Form1_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e);
	private: System::Void viewSounderParameterToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void viewSounderParameterFormClosing(System::Object^ sender, CancelEventArgs^ e);

	private: System::Void horizontalToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void verticalToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void tiledToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void getControlsToArrange(Generic::List<Control^>^ controls);
	private: System::Void viewDetectedBottomToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void multifrenquenciesEchogramsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void spectralAnalysisToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void calibrationToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void noiseLevelToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void loadViewsLayoutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void saveViewsLayoutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void importViewsLayoutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void exportViewsLayoutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void loadViewsLayout(GuiParameter guiParam);
	private: GuiParameter saveViewsLayout();
};

}

