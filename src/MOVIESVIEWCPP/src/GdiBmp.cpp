#include "GdiBmp.h"

#include <string.h>
#include "GdiWrapper.h"

using namespace MOVIESVIEWCPP;
using namespace System::Runtime::InteropServices;

enum class TernaryRasterOperations : unsigned int
{
	SRCCOPY = 0x00CC0020,
	SRCPAINT = 0x00EE0086,
	SRCAND = 0x008800C6,
	SRCINVERT = 0x00660046,
	SRCERASE = 0x00440328,
	NOTSRCCOPY = 0x00330008,
	NOTSRCERASE = 0x001100A6,
	MERGECOPY = 0x00C000CA,
	MERGEPAINT = 0x00BB0226,
	PATCOPY = 0x00F00021,
	PATPAINT = 0x00FB0A09,
	PATINVERT = 0x005A0049,
	DSTINVERT = 0x00550009,
	BLACKNESS = 0x00000042,
	WHITENESS = 0x00FF0062,
	CAPTUREBLT = 0x40000000 //only if WinVer >= 5.0.0 (see wingdi.h)
};

public class BITMAPINFOHEADER
{
public:
	int            biSize;
	int            biWidth;
	int            biHeight;
	short      biPlanes;
	short      biBitCount;
	int            biCompression;
	int            biSizeImage;
	int            biXPelsPerMeter;
	int            biYPelsPerMeter;
	int            biClrUsed;
	int            biClrImportant;
	// Valeur trouv�e dans divers exemples, pas directement dans la documentation
	const static int BI_RGB = 0;
};

public ref class Gdi32Wrapper
{
public:
	[DllImport("gdi32.dll")]
	static int StretchDIBits(
		System::IntPtr hdc,
		int XDest,
		int YDest,
		int nDestWidth,
		int nDestHeight,
		int XSrc,
		int YSrc,
		int nSrcWidth,
		int nSrcHeight,
		const void *lpBits,
		const void *lpBitsInfo,
		unsigned int iUsage,
		int dwRop
	);
};

GdiBmp::GdiBmp()
{
	Assign(100, 100, nullptr);
}

GdiBmp::GdiBmp(unsigned int width, unsigned int height)
{
	Assign(width, height, nullptr);
}

GdiBmp::~GdiBmp()
{
}

void GdiBmp::Assign(unsigned int width, unsigned int height, unsigned int * ptr)
{
	m_width = width;
	m_height = height;
	m_ptr = ptr;
}

unsigned int GdiBmp::Width()
{
	return m_width;
}

unsigned int GdiBmp::Height()
{
	return m_height;
}

bool GdiBmp::IsValid()
{
	return m_height > 0 && m_width > 0;
}

void GdiBmp::Render(System::Drawing::Graphics ^ graphics, System::Drawing::Rectangle srcRect, System::Drawing::Rectangle destRect)
{
	if (destRect.Width < 0 || destRect.Height < 0)
		return;

	if (m_ptr == nullptr)
		return;

	if (m_width < 0 || m_height < 0)
		return;

	if (srcRect.Width < 0 || destRect.Height < 0)
		return;

	System::Drawing::Bitmap ^ bmp = gcnew System::Drawing::Bitmap(m_width, m_height, m_width * 4
		, System::Drawing::Imaging::PixelFormat::Format32bppArgb
		, System::IntPtr(m_ptr));
	graphics->DrawImage(bmp, destRect, srcRect, System::Drawing::GraphicsUnit::Pixel);


	// TODO voir si image non valide, si on trace quelque chose d'autre...
	//graphics->FillRectangle(, destRect);
}

void GdiBmp::Render(System::Drawing::Graphics ^ graphics, System::Drawing::Rectangle destRect)
{
	if (destRect.Width < 0 || destRect.Height < 0)
		return;

	if (m_ptr == nullptr)
		return;

	if (m_width < 0 || m_height < 0)
		return;

	System::Drawing::Bitmap ^ bmp = gcnew System::Drawing::Bitmap(m_width, m_height, m_width * 4
		, System::Drawing::Imaging::PixelFormat::Format32bppArgb
		, System::IntPtr(m_ptr));
	graphics->DrawImage(bmp, destRect);


	// TODO voir si image non valide, si on trace quelque chose d'autre...
	//graphics->FillRectangle(, destRect);
}