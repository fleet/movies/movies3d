#pragma once

namespace MOVIESVIEWCPP {

	/**
	* Classe encapsulant un BITMAP GDI permettant d'acc�lerer le rendu des echos 2D
	* L'image est au format ARGB : 1 pixel est cod� sur 32bits
	*/
	public ref class GdiBmp
	{
	public:
		/// Constucteur par d�faut : initialise un bitmap de 100x100
		GdiBmp();

		/**
		* Constucteur
		* @param width largeur de l'image
		* @param hauteur de l'image
		*/
		GdiBmp(unsigned int width, unsigned int height);

		/// Destructeur
		~GdiBmp();

		/// Realloue des nouvelles donn�es l'image
		void Assign(unsigned int width, unsigned int height, unsigned int * ptr);
		
		/**
		* Effectue le rendu d'une section de l'image
		* @param graphics Objet graphique
		* @param srcRect Rectangle de l'image a rendre
		* @param destRect Rectangle de destination pour le rendu
		*/
		void Render(System::Drawing::Graphics ^ graphics, System::Drawing::Rectangle srcRect, System::Drawing::Rectangle destRect);

		/**
		* Effectue le rendu de l'image enti�re
		* @param graphics Objet graphique
		* @param destRect Rectangle de destination
		*/
		void Render(System::Drawing::Graphics ^ graphics, System::Drawing::Rectangle destRect);

		/// Retourne la largeur de l'image
		unsigned int Width();

		/// Retourne la hauteur de l'image
		unsigned int Height();

		/// Retourne si l'image est valide
		bool IsValid();

	private:
		unsigned int* m_ptr;
		unsigned int m_width;
		unsigned int m_height;
	};

}
