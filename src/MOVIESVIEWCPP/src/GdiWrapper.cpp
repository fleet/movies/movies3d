#include "GdiWrapper.h"

//#include "WinGdi.h"

//using namespace System::Drawing;
using namespace System::Runtime::InteropServices;

using namespace MOVIESVIEWCPP;

namespace
{
	public ref class Gdi32
	{
	public:

		static const int R2_NOTXORPEN = 10;

		static const int WM_SETREDRAW = 0x000B;

		[DllImport("gdi32.dll")]
		static bool Rectangle(System::IntPtr hDC, int left, int top, int right, int bottom);

		[DllImport("gdi32.dll")]
		static bool Polygon(System::IntPtr hDC, const POINT * points, int nCount);

		[DllImport("gdi32.dll")]
		static int SetPolyFillMode(System::IntPtr hDC, int iPolyMode);

		[DllImport("gdi32.dll")]
		static int SetROP2(System::IntPtr hDC, int fnDrawMode);

		[DllImport("gdi32.dll")]
		static bool MoveToEx(System::IntPtr hDC, int x, int y, System::Drawing::Point^ p);

		[DllImport("gdi32.dll")]
		static bool LineTo(System::IntPtr hdc, int x, int y);

		[DllImport("gdi32.dll")]
		static System::IntPtr CreatePen(int fnPenStyle, int nWidth, int crColor);

		[DllImport("gdi32.dll")]
		static System::IntPtr CreateSolidBrush(int crColor);

		[DllImport("gdi32.dll")]
		static System::IntPtr CreateHatchBrush(int fnStyle, int crColor);
		// fnStyle : style de hachure 
		// 0 : horizontal
		// 1 : vertical
		// 2 : -45�
		// 3 : 45�
		// 4 : quadrillage
		// 5 : quadrillage � 45�
		// 6 : XOR
		// 7 : 

		[DllImport("gdi32.dll")]
		static System::IntPtr SelectObject(System::IntPtr hDC, System::IntPtr hObj);

		[DllImport("gdi32.dll")]
		static System::IntPtr GetCurrentObject(System::IntPtr hDC, unsigned int uObjectType);

		[DllImport("gdi32.dll")]
		static bool DeleteObject(System::IntPtr hObj);

		[DllImport("gdi32.dll")]
		static System::IntPtr CreateCompatibleDC(System::IntPtr hdc);

		[DllImport("gdi32.dll")]
		static bool DeleteDC(System::IntPtr hdc);

		[DllImport("user32.dll")]
		static System::IntPtr SendMessage(System::IntPtr hWnd, int msg, int wParam, System::IntPtr lParam);
	};
}

void GDIWrapper::DrawXORRectangle(System::Drawing::Graphics^ graphics, System::Drawing::Pen^ pen, System::Drawing::Rectangle^ rectangle)
{
	System::IntPtr hDC = graphics->GetHdc();
	System::IntPtr hPen = Gdi32::CreatePen(0, (int)pen->Width, ArgbToRGB(pen->Color.ToArgb()));
	Gdi32::SelectObject(hDC, hPen);
	Gdi32::SetROP2(hDC, Gdi32::R2_NOTXORPEN);
	Gdi32::Rectangle(hDC, rectangle->Left, rectangle->Top, rectangle->Right, rectangle->Bottom);
	Gdi32::DeleteObject(hPen);
	graphics->ReleaseHdc(hDC);
}

void GDIWrapper::DrawXORLine(System::Drawing::Graphics^ graphics, System::Drawing::Pen^ pen, int x1, int y1, int x2, int y2)
{
	System::IntPtr hDC = graphics->GetHdc();
	System::IntPtr hPen = Gdi32::CreatePen(0, (int)pen->Width, ArgbToRGB(pen->Color.ToArgb()));
	Gdi32::SelectObject(hDC, hPen);
	Gdi32::SetROP2(hDC, Gdi32::R2_NOTXORPEN);
	Gdi32::MoveToEx(hDC, x1, y1, nullptr);
	Gdi32::LineTo(hDC, x2, y2);
	Gdi32::DeleteObject(hPen);
	graphics->ReleaseHdc(hDC);
}

void GDIWrapper::DrawXORPolygon(System::Drawing::Graphics^ graphics, System::Drawing::Pen^ pen, const POINT * points, int nCount, bool hatched)
{
	System::IntPtr hDC = graphics->GetHdc();
	System::IntPtr hPen = Gdi32::CreatePen(0, (int)pen->Width, ArgbToRGB(pen->Color.ToArgb()));
	Gdi32::SelectObject(hDC, hPen);
	System::IntPtr hBrush;
	System::IntPtr savedBrush;
	if (hatched)
	{
		// sauvegarde de la brush courante
		savedBrush = Gdi32::GetCurrentObject(hDC, 2);
		hBrush = Gdi32::CreateHatchBrush(3, ArgbToRGB(pen->Color.ToArgb()));
		Gdi32::SelectObject(hDC, hBrush);
		Gdi32::SetPolyFillMode(hDC, 1);
	}
	Gdi32::SetROP2(hDC, Gdi32::R2_NOTXORPEN);
	Gdi32::Polygon(hDC, points, nCount);
	Gdi32::DeleteObject(hPen);
	if (hatched)
	{
		// restauration de la brush
		Gdi32::SelectObject(hDC, savedBrush);
		Gdi32::DeleteObject(hBrush);
	}
	graphics->ReleaseHdc(hDC);
}

void GDIWrapper::BeginUpdate(System::IntPtr hWnd)
{
	Gdi32::SendMessage(hWnd, Gdi32::WM_SETREDRAW, 0, System::IntPtr::Zero);
}

void GDIWrapper::EndUpdate(System::IntPtr hWnd)
{
	Gdi32::SendMessage(hWnd, Gdi32::WM_SETREDRAW, 1, System::IntPtr::Zero);
}

// Convert the Argb from .NET to a gdi32 RGB
int GDIWrapper::ArgbToRGB(int rgb)
{
	return ((rgb >> 16 & 0x0000FF) | (rgb & 0x00FF00) | (rgb << 16 & 0xFF0000));
}