#pragma once


namespace MOVIESVIEWCPP {
	
	typedef struct tagPOINT {
		int x;
		int y;
	} POINT, *PPOINT;

	/// <summary>
	/// Provides utilities directly accessing the gdi32.dll 
	/// </summary>
	public ref class GDIWrapper
	{
	public:

		static void DrawXORRectangle(System::Drawing::Graphics^ graphics, System::Drawing::Pen^ pen, System::Drawing::Rectangle^ rectangle);

		static void DrawXORLine(System::Drawing::Graphics^ graphics, System::Drawing::Pen^ pen, int x1, int y1, int x2, int y2);

		static void DrawXORPolygon(System::Drawing::Graphics^ graphics, System::Drawing::Pen^ pen, const POINT * points, int nCount, bool hatched);

		static void BeginUpdate(System::IntPtr hWnd);

		static void EndUpdate(System::IntPtr hWnd);
	private:

		// Convert the Argb from .NET to a gdi32 RGB
		static int ArgbToRGB(int rgb);
	};
}