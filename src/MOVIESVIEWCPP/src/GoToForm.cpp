#include "GoToForm.h"

using namespace MOVIESVIEWCPP;

// met � jour les composants en fonction des donn�es
void GoToForm::UpdateComponents()
{
	// mise � jour de l'indication des limites possibles
	char date[255];
	m_MinAvailableTime->GetTimeDesc(date, 254);
	System::String ^statText = gcnew System::String(date);
	m_MinTimeLimit->Text = statText;
	m_MaxAvailableTime->GetTimeDesc(date, 254);
	statText = gcnew System::String(date);
	m_MaxTimeLimit->Text = statText;
	m_MinPingLimit->Text = Convert::ToString(m_MinAvailablePing);
	m_MaxPingLimit->Text = Convert::ToString(m_MaxAvailablePing);

	// mise � jour des composants pour le choix de la destination
	this->m_DayDateTimePicker->Value = Convert::ToDateTime(m_DestinationDate);
	this->m_HourDateTimePicker->Value = Convert::ToDateTime(m_DestinationDate);

	// mise � jour des composants dynamiques
	this->m_MinTimeLimit->Visible = this->radioButtonTime->Checked;
	this->m_MaxTimeLimit->Visible = this->radioButtonTime->Checked;
	this->m_DayDateTimePicker->Enabled = this->radioButtonTime->Checked;
	this->m_HourDateTimePicker->Enabled = this->radioButtonTime->Checked;
	this->m_MinPingLimit->Visible = this->radioButtonPingNb->Checked;
	this->m_MaxPingLimit->Visible = this->radioButtonPingNb->Checked;
	this->m_PingNumberNumericUpDown->Enabled = this->radioButtonPingNb->Checked;
}

// d�fini les limites min et max disponibles
void GoToForm::SetLimits(HacTime *minTime, std::uint32_t minPing,
	HacTime *maxTime, std::uint32_t maxPing,
	HacTime *currentTime, std::uint64_t currentPing)
{
	m_MinAvailableTime = minTime;
	m_MaxAvailableTime = maxTime;
	m_MinAvailablePing = minPing;
	m_MaxAvailablePing = maxPing;

	// si le temps courant est d�fini on s'en sert pour regler
	// la destination
	DateTime^ date1970 = gcnew DateTime(1970, 01, 01, 0, 0, 0, 0);
	std::int64_t sec = 10000000L;
	if (currentTime)
	{
		DateTime^ MoviesDate = gcnew DateTime(
			currentTime->m_TimeFraction * 1000L + currentTime->m_TimeCpu*sec);

		m_DestinationDate = gcnew DateTime(MoviesDate->Ticks + date1970->Ticks);
	}
	else
	{
		DateTime^ MoviesDate = gcnew DateTime(
			m_MinAvailableTime->m_TimeFraction * 1000L + m_MinAvailableTime->m_TimeCpu*sec);

		m_DestinationDate = gcnew DateTime(MoviesDate->Ticks + date1970->Ticks);
	}

	// on ne permet pas d'outrepasser les limites au niveau des composants
	DateTime^ MoviesDate = gcnew DateTime(
		m_MinAvailableTime->m_TimeFraction * 1000L + m_MinAvailableTime->m_TimeCpu*sec);
	this->m_DayDateTimePicker->MinDate = DateTime(MoviesDate->Ticks + date1970->Ticks);
	this->m_HourDateTimePicker->MinDate = DateTime(MoviesDate->Ticks + date1970->Ticks);
	MoviesDate = gcnew DateTime(
		m_MaxAvailableTime->m_TimeFraction * 1000L + m_MaxAvailableTime->m_TimeCpu*sec);
	this->m_DayDateTimePicker->MaxDate = DateTime(MoviesDate->Ticks + date1970->Ticks);
	this->m_HourDateTimePicker->MaxDate = DateTime(MoviesDate->Ticks + date1970->Ticks);

	this->m_PingNumberNumericUpDown->Value = currentPing;
	this->m_PingNumberNumericUpDown->Minimum = System::Decimal((std::uint64_t)minPing);
	this->m_PingNumberNumericUpDown->Maximum = System::Decimal((std::uint64_t)maxPing);

	// mise � jour du panneau IHM en fonction des parametres
	UpdateComponents();
}

// renvoi les param�tres choisis pour le GoTo
GoToTarget GoToForm::GetTarget()
{
	GoToTarget result;
	result.byPing = this->radioButtonPingNb->Checked;
	result.targetPingNumber = Convert::ToUInt32(this->m_PingNumberNumericUpDown->Value);

	// pour la date c'est un peu plus compliqu� :
	DateTime^ TargetDate = gcnew DateTime(m_DayDateTimePicker->Value.Year, m_DayDateTimePicker->Value.Month,
		m_DayDateTimePicker->Value.Day, m_HourDateTimePicker->Value.Hour, m_HourDateTimePicker->Value.Minute,
		m_HourDateTimePicker->Value.Second, m_HourDateTimePicker->Value.Millisecond);
	// on doit ajouter les microsecondes
	std::int64_t microsec = m_HourDateTimePicker->Value.Ticks % (std::int64_t)10000L;
	TargetDate = TargetDate->AddTicks(microsec);
	DateTime^ date1970 = gcnew DateTime(1970, 01, 01, 0, 0, 0, 0);
	TargetDate = gcnew DateTime(TargetDate->Ticks - date1970->Ticks);
	std::int64_t sec = 10000000L;
	result.targetTime.m_TimeCpu = (std::uint32_t)(TargetDate->Ticks / sec);
	std::int64_t secFraction = ((TargetDate->Ticks - result.targetTime.m_TimeCpu*sec)) / 1000L;
	result.targetTime.m_TimeFraction = (unsigned short)secFraction;

	return result;
}

