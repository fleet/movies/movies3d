#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "M3DKernel/datascheme/DateTime.h"
#include "reader/readerctrl.h"


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de GoToForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class GoToForm : public System::Windows::Forms::Form
	{
	private:

		// champs

		// bornes possibles pour le goto
		HacTime *m_MinAvailableTime, *m_MaxAvailableTime;
		unsigned int m_MinAvailablePing, m_MaxAvailablePing;

		// destination choisie
		DateTime^ m_DestinationDate;


	private: System::Windows::Forms::GroupBox^  m_LimitsGroupBox;
	private: System::Windows::Forms::Label^  m_MaxTimeLimitLabel;
	private: System::Windows::Forms::Label^  m_MaxPingLimit;
	private: System::Windows::Forms::Label^  m_MinPingLimit;
	private: System::Windows::Forms::Label^  m_MaxTimeLimit;

	private: System::Windows::Forms::Label^  m_MinTimeLimit;
	private: System::Windows::Forms::GroupBox^  m_DestinationGroupBox;



	private: System::Windows::Forms::Button^  m_OkButton;
	private: System::Windows::Forms::Button^  m_CancelButton;

	private: System::Windows::Forms::DateTimePicker^  m_DayDateTimePicker;
	private: System::Windows::Forms::DateTimePicker^  m_HourDateTimePicker;
	private: System::Windows::Forms::NumericUpDown^  m_PingNumberNumericUpDown;

	private: System::Windows::Forms::RadioButton^  radioButtonPingNb;
	private: System::Windows::Forms::RadioButton^  radioButtonTime;





	private: System::Windows::Forms::Label^  m_MinTimeLimitLabel;

	public:
		GoToForm(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
		}

		// met � jour les composants en fonction des donn�es
		void UpdateComponents();

		// d�fini les limites min et max disponibles
		void SetLimits(HacTime *minTime, std::uint32_t minPing,
			HacTime *maxTime, std::uint32_t maxPing,
			HacTime *currentTime, std::uint64_t currentPing);

		GoToTarget GetTarget();

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~GoToForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->m_LimitsGroupBox = (gcnew System::Windows::Forms::GroupBox());
			this->m_MaxPingLimit = (gcnew System::Windows::Forms::Label());
			this->m_MinPingLimit = (gcnew System::Windows::Forms::Label());
			this->m_MaxTimeLimit = (gcnew System::Windows::Forms::Label());
			this->m_MinTimeLimit = (gcnew System::Windows::Forms::Label());
			this->m_MaxTimeLimitLabel = (gcnew System::Windows::Forms::Label());
			this->m_MinTimeLimitLabel = (gcnew System::Windows::Forms::Label());
			this->m_DestinationGroupBox = (gcnew System::Windows::Forms::GroupBox());
			this->m_PingNumberNumericUpDown = (gcnew System::Windows::Forms::NumericUpDown());
			this->radioButtonPingNb = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonTime = (gcnew System::Windows::Forms::RadioButton());
			this->m_DayDateTimePicker = (gcnew System::Windows::Forms::DateTimePicker());
			this->m_HourDateTimePicker = (gcnew System::Windows::Forms::DateTimePicker());
			this->m_OkButton = (gcnew System::Windows::Forms::Button());
			this->m_CancelButton = (gcnew System::Windows::Forms::Button());
			this->m_LimitsGroupBox->SuspendLayout();
			this->m_DestinationGroupBox->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->m_PingNumberNumericUpDown))->BeginInit();
			this->SuspendLayout();
			// 
			// m_LimitsGroupBox
			// 
			this->m_LimitsGroupBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->m_LimitsGroupBox->Controls->Add(this->m_MaxPingLimit);
			this->m_LimitsGroupBox->Controls->Add(this->m_MinPingLimit);
			this->m_LimitsGroupBox->Controls->Add(this->m_MaxTimeLimit);
			this->m_LimitsGroupBox->Controls->Add(this->m_MinTimeLimit);
			this->m_LimitsGroupBox->Controls->Add(this->m_MaxTimeLimitLabel);
			this->m_LimitsGroupBox->Controls->Add(this->m_MinTimeLimitLabel);
			this->m_LimitsGroupBox->Location = System::Drawing::Point(12, 12);
			this->m_LimitsGroupBox->Name = L"m_LimitsGroupBox";
			this->m_LimitsGroupBox->Size = System::Drawing::Size(272, 69);
			this->m_LimitsGroupBox->TabIndex = 0;
			this->m_LimitsGroupBox->TabStop = false;
			this->m_LimitsGroupBox->Text = L"Available destination range";
			// 
			// m_MaxPingLimit
			// 
			this->m_MaxPingLimit->AutoSize = true;
			this->m_MaxPingLimit->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->m_MaxPingLimit->Location = System::Drawing::Point(69, 42);
			this->m_MaxPingLimit->Name = L"m_MaxPingLimit";
			this->m_MaxPingLimit->Size = System::Drawing::Size(33, 15);
			this->m_MaxPingLimit->TabIndex = 5;
			this->m_MaxPingLimit->Text = L"9999";
			// 
			// m_MinPingLimit
			// 
			this->m_MinPingLimit->AutoSize = true;
			this->m_MinPingLimit->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->m_MinPingLimit->Location = System::Drawing::Point(69, 20);
			this->m_MinPingLimit->Name = L"m_MinPingLimit";
			this->m_MinPingLimit->Size = System::Drawing::Size(33, 15);
			this->m_MinPingLimit->TabIndex = 4;
			this->m_MinPingLimit->Text = L"9999";
			// 
			// m_MaxTimeLimit
			// 
			this->m_MaxTimeLimit->AutoSize = true;
			this->m_MaxTimeLimit->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->m_MaxTimeLimit->Location = System::Drawing::Point(69, 42);
			this->m_MaxTimeLimit->Name = L"m_MaxTimeLimit";
			this->m_MaxTimeLimit->Size = System::Drawing::Size(139, 15);
			this->m_MaxTimeLimit->TabIndex = 3;
			this->m_MaxTimeLimit->Text = L"10/10/1000 16:16:16.6666";
			// 
			// m_MinTimeLimit
			// 
			this->m_MinTimeLimit->AutoSize = true;
			this->m_MinTimeLimit->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->m_MinTimeLimit->Location = System::Drawing::Point(69, 20);
			this->m_MinTimeLimit->Name = L"m_MinTimeLimit";
			this->m_MinTimeLimit->Size = System::Drawing::Size(139, 15);
			this->m_MinTimeLimit->TabIndex = 2;
			this->m_MinTimeLimit->Text = L"10/10/1000 16:16:16.6666";
			// 
			// m_MaxTimeLimitLabel
			// 
			this->m_MaxTimeLimitLabel->AutoSize = true;
			this->m_MaxTimeLimitLabel->Location = System::Drawing::Point(11, 42);
			this->m_MaxTimeLimitLabel->Name = L"m_MaxTimeLimitLabel";
			this->m_MaxTimeLimitLabel->Size = System::Drawing::Size(52, 13);
			this->m_MaxTimeLimitLabel->TabIndex = 1;
			this->m_MaxTimeLimitLabel->Text = L"End limit :";
			// 
			// m_MinTimeLimitLabel
			// 
			this->m_MinTimeLimitLabel->AutoSize = true;
			this->m_MinTimeLimitLabel->Location = System::Drawing::Point(11, 20);
			this->m_MinTimeLimitLabel->Name = L"m_MinTimeLimitLabel";
			this->m_MinTimeLimitLabel->Size = System::Drawing::Size(55, 13);
			this->m_MinTimeLimitLabel->TabIndex = 0;
			this->m_MinTimeLimitLabel->Text = L"Start limit :";
			// 
			// m_DestinationGroupBox
			// 
			this->m_DestinationGroupBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->m_DestinationGroupBox->Controls->Add(this->m_PingNumberNumericUpDown);
			this->m_DestinationGroupBox->Controls->Add(this->radioButtonPingNb);
			this->m_DestinationGroupBox->Controls->Add(this->radioButtonTime);
			this->m_DestinationGroupBox->Controls->Add(this->m_DayDateTimePicker);
			this->m_DestinationGroupBox->Controls->Add(this->m_HourDateTimePicker);
			this->m_DestinationGroupBox->Location = System::Drawing::Point(12, 87);
			this->m_DestinationGroupBox->Name = L"m_DestinationGroupBox";
			this->m_DestinationGroupBox->Size = System::Drawing::Size(272, 82);
			this->m_DestinationGroupBox->TabIndex = 1;
			this->m_DestinationGroupBox->TabStop = false;
			this->m_DestinationGroupBox->Text = L"Destination";
			// 
			// m_PingNumberNumericUpDown
			// 
			this->m_PingNumberNumericUpDown->Location = System::Drawing::Point(87, 49);
			this->m_PingNumberNumericUpDown->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1410065407, 2, 0, 0 });
			this->m_PingNumberNumericUpDown->Name = L"m_PingNumberNumericUpDown";
			this->m_PingNumberNumericUpDown->Size = System::Drawing::Size(89, 20);
			this->m_PingNumberNumericUpDown->TabIndex = 7;
			// 
			// radioButtonPingNb
			// 
			this->radioButtonPingNb->AutoSize = true;
			this->radioButtonPingNb->Location = System::Drawing::Point(9, 49);
			this->radioButtonPingNb->Name = L"radioButtonPingNb";
			this->radioButtonPingNb->Size = System::Drawing::Size(72, 17);
			this->radioButtonPingNb->TabIndex = 6;
			this->radioButtonPingNb->Text = L"Ping Nb. :";
			this->radioButtonPingNb->UseVisualStyleBackColor = true;
			this->radioButtonPingNb->CheckedChanged += gcnew System::EventHandler(this, &GoToForm::radioButtonPingNb_CheckedChanged);
			// 
			// radioButtonTime
			// 
			this->radioButtonTime->AutoSize = true;
			this->radioButtonTime->Checked = true;
			this->radioButtonTime->Location = System::Drawing::Point(9, 23);
			this->radioButtonTime->Name = L"radioButtonTime";
			this->radioButtonTime->Size = System::Drawing::Size(54, 17);
			this->radioButtonTime->TabIndex = 5;
			this->radioButtonTime->TabStop = true;
			this->radioButtonTime->Text = L"Time :";
			this->radioButtonTime->UseVisualStyleBackColor = true;
			this->radioButtonTime->CheckedChanged += gcnew System::EventHandler(this, &GoToForm::radioButtonTime_CheckedChanged);
			// 
			// m_DayDateTimePicker
			// 
			this->m_DayDateTimePicker->Format = System::Windows::Forms::DateTimePickerFormat::Short;
			this->m_DayDateTimePicker->Location = System::Drawing::Point(87, 23);
			this->m_DayDateTimePicker->Name = L"m_DayDateTimePicker";
			this->m_DayDateTimePicker->ShowUpDown = true;
			this->m_DayDateTimePicker->Size = System::Drawing::Size(89, 20);
			this->m_DayDateTimePicker->TabIndex = 4;
			this->m_DayDateTimePicker->ValueChanged += gcnew System::EventHandler(this, &GoToForm::m_DayDateTimePicker_ValueChanged);
			// 
			// m_HourDateTimePicker
			// 
			this->m_HourDateTimePicker->Format = System::Windows::Forms::DateTimePickerFormat::Time;
			this->m_HourDateTimePicker->Location = System::Drawing::Point(182, 23);
			this->m_HourDateTimePicker->Name = L"m_HourDateTimePicker";
			this->m_HourDateTimePicker->ShowUpDown = true;
			this->m_HourDateTimePicker->Size = System::Drawing::Size(73, 20);
			this->m_HourDateTimePicker->TabIndex = 3;
			this->m_HourDateTimePicker->ValueChanged += gcnew System::EventHandler(this, &GoToForm::m_HourDateTimePicker_ValueChanged);
			// 
			// m_OkButton
			// 
			this->m_OkButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->m_OkButton->Location = System::Drawing::Point(78, 177);
			this->m_OkButton->Name = L"m_OkButton";
			this->m_OkButton->Size = System::Drawing::Size(75, 23);
			this->m_OkButton->TabIndex = 2;
			this->m_OkButton->Text = L"Ok";
			this->m_OkButton->UseVisualStyleBackColor = true;
			this->m_OkButton->Click += gcnew System::EventHandler(this, &GoToForm::m_OkButton_Click);
			// 
			// m_CancelButton
			// 
			this->m_CancelButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->m_CancelButton->Location = System::Drawing::Point(159, 177);
			this->m_CancelButton->Name = L"m_CancelButton";
			this->m_CancelButton->Size = System::Drawing::Size(75, 23);
			this->m_CancelButton->TabIndex = 3;
			this->m_CancelButton->Text = L"Cancel";
			this->m_CancelButton->UseVisualStyleBackColor = true;
			this->m_CancelButton->Click += gcnew System::EventHandler(this, &GoToForm::m_CancelButton_Click);
			// 
			// GoToForm
			// 
			this->AcceptButton = this->m_OkButton;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(296, 208);
			this->Controls->Add(this->m_CancelButton);
			this->Controls->Add(this->m_OkButton);
			this->Controls->Add(this->m_DestinationGroupBox);
			this->Controls->Add(this->m_LimitsGroupBox);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->MinimumSize = System::Drawing::Size(312, 242);
			this->Name = L"GoToForm";
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
			this->Text = L"Go To";
			this->m_LimitsGroupBox->ResumeLayout(false);
			this->m_LimitsGroupBox->PerformLayout();
			this->m_DestinationGroupBox->ResumeLayout(false);
			this->m_DestinationGroupBox->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->m_PingNumberNumericUpDown))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void m_CancelButton_Click(System::Object^  sender, System::EventArgs^  e) {
		this->DialogResult = System::Windows::Forms::DialogResult::Cancel;
	}
	private: System::Void m_OkButton_Click(System::Object^  sender, System::EventArgs^  e) {
		this->DialogResult = System::Windows::Forms::DialogResult::OK;
	}
	private: System::Void m_HourDateTimePicker_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		// OTK - Oblig� de refaire le test de validit� � la main car celui par d�faut
		// ne prend pas en compte les milisecondes ce qui pose probl�me quand on
		// arrive en but�e
		if (m_HourDateTimePicker->Value > m_HourDateTimePicker->MaxDate)
		{
			m_HourDateTimePicker->Value = m_HourDateTimePicker->MaxDate;
		}
		if (m_HourDateTimePicker->Value < m_HourDateTimePicker->MinDate)
		{
			m_HourDateTimePicker->Value = m_HourDateTimePicker->MinDate;
		}
		m_DayDateTimePicker->Value = m_HourDateTimePicker->Value;
	}
	private: System::Void m_DayDateTimePicker_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		// OTK - Oblig� de refaire le test de validit� � la main car celui par d�faut
		// ne prend pas en compte les milisecondes ce qui pose probl�me quand on
		// arrive en but�e
		if (m_DayDateTimePicker->Value > m_DayDateTimePicker->MaxDate)
		{
			m_DayDateTimePicker->Value = m_DayDateTimePicker->MaxDate;
		}
		if (m_DayDateTimePicker->Value < m_DayDateTimePicker->MinDate)
		{
			m_DayDateTimePicker->Value = m_DayDateTimePicker->MinDate;
		}
		m_HourDateTimePicker->Value = m_DayDateTimePicker->Value;
	}
	private: System::Void radioButtonTime_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		UpdateComponents();
	}
	private: System::Void radioButtonPingNb_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		UpdateComponents();
	}
	};
}
