﻿#include "GuiParameter.h"

#include <algorithm>

using namespace BaseKernel;

namespace {
	// Clefs utilisées dans le XML
	namespace serialization {
		const auto NB_VIEWS = "NbViews";
		const auto MAIN_WINDOW_X = "MainWindowX";
		const auto MAIN_WINDOW_Y = "MainWindowY";
		const auto MAIN_WINDOW_WIDTH = "MainWindowWidth";
		const auto MAIN_WINDOW_HEIGHT = "MainWindowHeight";
		const auto MAIN_WINDOW_STATE = "MainWindowState";
		const auto VIEW_GLOBAL_SPLIT_DISTANCE = "ViewGlobalSplitDistance";
		const auto VIEW_2D_SPLIT_DISTANCE = "View2DSplitDistance";
	}
}

GuiParameter::GuiParameter()
	: BaseKernel::ParameterModule("GuiParameter")
	, m_windowState(System::Windows::Forms::FormWindowState::Normal)
{
}

GuiParameter::~GuiParameter(void)
{
}

void GuiParameter::setViews(const std::list<GuiViewParameter>& views)
{
	m_views = views;
}

std::list<GuiViewParameter> GuiParameter::views() const
{
	return m_views;
}

std::list<GuiViewParameter> GuiParameter::viewsByType(const ViewType type) const
{
	std::list<GuiViewParameter> result;
	for (const GuiViewParameter& param : m_views){
		if (param.type() == type) {
			result.push_back(param);
		}
	}
	return result;
}

void GuiParameter::setMainWindowPos(const int x, const int y)
{
	m_xPos = x;
	m_yPos = y;
}

int GuiParameter::mainWindowPosX() const
{
	return m_xPos;
}

int GuiParameter::mainWindowPosY() const
{
	return m_yPos;
}

void GuiParameter::setMainWindowSize(const int width, const int height)
{
	m_width = width;
	m_height = height;
}

int GuiParameter::mainWindowWidth() const
{
	return m_width;
}

int GuiParameter::mainWindowHeight() const
{
	return m_height;
}

void GuiParameter::setViewGlobalSplitDistance(int d)
{
	m_viewGlobalSplitDistance = d;
}

int GuiParameter::viewGlobalSplitDistance() const
{
	return m_viewGlobalSplitDistance;
}

void GuiParameter::setView2DSplitDistance(int d)
{
	m_view2DSplitDistance = d;
}

int GuiParameter::view2DSplitDistance() const
{
	return m_view2DSplitDistance;
}

System::Windows::Forms::FormWindowState GuiParameter::windowState() const
{
	return m_windowState;
}

void GuiParameter::setWindowState(const System::Windows::Forms::FormWindowState state)
{
	m_windowState = state;
}

bool GuiParameter::Serialize(BaseKernel::MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	// Paramètres de la fenêtre pincipale
	movConfig->SerializeData<int>(m_xPos, eInt, serialization::MAIN_WINDOW_X);
	movConfig->SerializeData<int>(m_yPos, eInt, serialization::MAIN_WINDOW_Y);
	movConfig->SerializeData<int>(m_width, eInt, serialization::MAIN_WINDOW_WIDTH);
	movConfig->SerializeData<int>(m_height, eInt, serialization::MAIN_WINDOW_HEIGHT);
	movConfig->SerializeData<int>(m_viewGlobalSplitDistance, eInt, serialization::VIEW_GLOBAL_SPLIT_DISTANCE);
	movConfig->SerializeData<int>(m_view2DSplitDistance, eInt, serialization::VIEW_2D_SPLIT_DISTANCE);
	movConfig->SerializeData<int>(static_cast<unsigned int>(m_windowState), eUInt, serialization::MAIN_WINDOW_STATE);

	// Paramètres des vues
	unsigned int nbViews = m_views.size();
	movConfig->SerializeData<unsigned int>(nbViews, eUInt, serialization::NB_VIEWS);

	for (GuiViewParameter& view : m_views) {
		view.Serialize(movConfig);
	}

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();
	return true;
}

bool GuiParameter::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);

	// debut de la d�s�rialisation du module
	if (result)
	{
		// Paramètres des vues
		result = result && movConfig->DeSerializeData<int>(&m_xPos, eInt, serialization::MAIN_WINDOW_X);
		result = result && movConfig->DeSerializeData<int>(&m_yPos, eInt, serialization::MAIN_WINDOW_Y);
		result = result && movConfig->DeSerializeData<int>(&m_width, eInt, serialization::MAIN_WINDOW_WIDTH);
		result = result && movConfig->DeSerializeData<int>(&m_height, eInt, serialization::MAIN_WINDOW_HEIGHT);
		result = result && movConfig->DeSerializeData<int>(&m_viewGlobalSplitDistance, eInt, serialization::VIEW_GLOBAL_SPLIT_DISTANCE);
		result = result && movConfig->DeSerializeData<int>(&m_view2DSplitDistance, eInt, serialization::VIEW_2D_SPLIT_DISTANCE);

		unsigned int tmpType;
		result = result && movConfig->DeSerializeData<unsigned int>(&tmpType, eUInt, serialization::MAIN_WINDOW_STATE);
		m_windowState = static_cast<System::Windows::Forms::FormWindowState>(tmpType);

		unsigned int nbViews = 0;
		result = result && movConfig->DeSerializeData<unsigned int>(&nbViews, eUInt, serialization::NB_VIEWS);

		for (unsigned int i = 0; i < nbViews; ++i)
		{
			GuiViewParameter view;
			result = result && view.DeSerialize(movConfig);
			m_views.push_back(view);
		}
	}
	
		// fin de la d�s�rialisation du module
	movConfig->DeSerializePushBack();

	return result;
}
