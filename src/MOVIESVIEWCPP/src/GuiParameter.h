/******************************************************************************/
/*	Project:	MOVIES3D													  */
/*	File:		GuiParameter.h											      */
/******************************************************************************/
#pragma once

#include "M3DKernel/parameter/ParameterModule.h"
#include "M3DKernel/config/MovConfig.h"
#include "GuiViewParameter.h"

#include <list>

/**
** Gestion de l'affichage de Movie3d
*/
class GuiParameter : public BaseKernel::ParameterModule
{
public:
	GuiParameter();
	virtual ~GuiParameter();

	void setViews(const std::list<GuiViewParameter>& views);
	std::list<GuiViewParameter> views() const;
	std::list<GuiViewParameter> viewsByType(const ViewType type) const;

	void setMainWindowPos(const int x, const int y);
	int mainWindowPosX() const;
	int mainWindowPosY() const;

	void setMainWindowSize(const int width, const int height);
	int mainWindowWidth() const;
	int mainWindowHeight() const;

	void setViewGlobalSplitDistance(int d);
	int viewGlobalSplitDistance() const;

	void setView2DSplitDistance(int d);
	int view2DSplitDistance() const;

	System::Windows::Forms::FormWindowState windowState() const;
	void setWindowState(const System::Windows::Forms::FormWindowState state);

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

private:
	// Liste des vues utilis�es par l'IHM
	std::list<GuiViewParameter> m_views;

	int m_xPos;
	int m_yPos;
	int m_width;
	int m_height;
	int m_viewGlobalSplitDistance;
	int m_view2DSplitDistance;
	System::Windows::Forms::FormWindowState m_windowState;
};
