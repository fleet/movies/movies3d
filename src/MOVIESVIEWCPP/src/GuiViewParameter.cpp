﻿#include "GuiParameter.h"

using namespace BaseKernel;

namespace {
	// Clefs utilisées dans le XML
	namespace serialization {
		const auto VIEW = "View";
		const auto TYPE = "Type";
		const auto IS_DOCKED = "IsDocked";
		const auto TRANSDUCER = "Transducer";
		const auto POS_X = "PosX";
		const auto POS_Y = "PosY";
		const auto WIDTH = "Width";
		const auto HEIGHT = "Height";
		const auto WINDOW_STATE = "WindowState";
	}
}

GuiViewParameter::GuiViewParameter(const ViewType type)
	: BaseKernel::ParameterModule("GuiViewParameter")
{
	m_type = type;
}

GuiViewParameter::~GuiViewParameter(void)
{
}

ViewType GuiViewParameter::type() const
{
	return m_type;
}

bool GuiViewParameter::isDocked() const
{
	return m_isDocked;
}

void GuiViewParameter::setIsDocked(const bool isDocked)
{
	m_isDocked = isDocked;
}

int GuiViewParameter::xPos() const
{
	return m_xPos;
}

int GuiViewParameter::yPos() const
{
	return m_yPos;
}

void GuiViewParameter::setPosition(const int x, const int y)
{
	m_xPos = x;
	m_yPos = y;
}

int GuiViewParameter::width() const
{
	return m_width;
}

int GuiViewParameter::height() const
{
	return m_height;
}

void GuiViewParameter::setSize(const int width, const int height)
{
	m_width = width;
	m_height = height;
}

std::string GuiViewParameter::transducer() const
{
	return m_transducer;
}

void GuiViewParameter::setTransducer(std::string transducerName)
{
	m_transducer = transducerName;
}

System::Windows::Forms::FormWindowState GuiViewParameter::windowState() const
{
	return m_windowState;
}

void GuiViewParameter::setWindowState(const System::Windows::Forms::FormWindowState state)
{
	m_windowState = state;
}

bool GuiViewParameter::Serialize(BaseKernel::MovConfig * movConfig)
{
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, serialization::VIEW);
	movConfig->SerializeData<unsigned int>(static_cast<unsigned int>(m_type), eUInt, serialization::TYPE);
	movConfig->SerializeData<std::string>(m_transducer, eString, serialization::TRANSDUCER);
	movConfig->SerializeData<bool>(m_isDocked, eBool, serialization::IS_DOCKED);
	movConfig->SerializeData<int>(m_xPos, eInt, serialization::POS_X);
	movConfig->SerializeData<int>(m_yPos, eInt, serialization::POS_Y);
	movConfig->SerializeData<int>(m_width, eInt, serialization::WIDTH);
	movConfig->SerializeData<int>(m_height, eInt, serialization::HEIGHT);
	movConfig->SerializeData<int>(static_cast<unsigned int>(m_windowState), eUInt, serialization::WINDOW_STATE);

	movConfig->SerializePushBack();
	return true;
}

bool GuiViewParameter::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = true;

	// debut de la d�s�rialisation du module
	result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, serialization::VIEW);

	// Début de la désérialisation des paramètres du ParameterObject
	unsigned int tmpType;
	result = result && movConfig->DeSerializeData<unsigned int>(&tmpType, eUInt, serialization::TYPE);
	m_type = static_cast<ViewType>(tmpType);
	result = result && movConfig->DeSerializeData<std::string>(&m_transducer, eString, serialization::TRANSDUCER);
	result = result && movConfig->DeSerializeData<bool>(&m_isDocked, eBool, serialization::IS_DOCKED);
	result = result && movConfig->DeSerializeData<int>(&m_xPos, eInt, serialization::POS_X);
	result = result && movConfig->DeSerializeData<int>(&m_yPos, eInt, serialization::POS_Y);
	result = result && movConfig->DeSerializeData<int>(&m_width, eInt, serialization::WIDTH);
	result = result && movConfig->DeSerializeData<int>(&m_height, eInt, serialization::HEIGHT);

	result = result && movConfig->DeSerializeData<unsigned int>(&tmpType, eUInt, serialization::WINDOW_STATE);
	m_windowState = static_cast<System::Windows::Forms::FormWindowState>(tmpType);

	movConfig->DeSerializePushBack();

	return result;
}
