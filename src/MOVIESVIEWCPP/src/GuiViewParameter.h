/******************************************************************************/
/*	Project:	MOVIES3D													  */
/*	File:		GuiParameter.h											      */
/******************************************************************************/
#pragma once

#include "M3DKernel/parameter/ParameterModule.h"
#include "M3DKernel/config/MovConfig.h"

/// Repr�sente le type de vue utilis� dans Movies3d
enum class ViewType : int {
	Main = 0,
	Front = 1,
	Slide = 2,
	Volumic = 3
};

/**
** Gestion de l'affichage de Movie3d
*/
class GuiViewParameter : public BaseKernel::ParameterModule
{
public:
	GuiViewParameter(const ViewType type = ViewType::Main);
	virtual ~GuiViewParameter();

	ViewType type() const;

	bool isDocked() const;
	void setIsDocked(const bool isDocked);
	
	int xPos() const;
	int yPos() const;
	void setPosition(const int x, const int y);

	int width() const;
	int height() const;
	void setSize(const int width, const int height);

	std::string transducer() const;
	void setTransducer(std::string transducerName);

	System::Windows::Forms::FormWindowState windowState() const;
	void setWindowState(const System::Windows::Forms::FormWindowState state);

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

private:
	ViewType m_type; // Type de la vue
	bool m_isDocked; // Permet de savoir si la vue est dock�e ou non
	int m_xPos; // Position en x de la vue
	int m_yPos; // Position en y de la vue
	int m_width; // Largeur de la fen�tre
	int m_height; // Hauteur de la fen�tre
	std::string m_transducer; // Transducteur Selectionn�
	System::Windows::Forms::FormWindowState m_windowState; // Etat de la fen�tre
};
