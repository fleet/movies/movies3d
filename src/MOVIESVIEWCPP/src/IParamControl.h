// -*- MC++ -*-
// ****************************************************************************
// Class: IParamControl
//
// Description: Interface pour les controles de configuration des modules
//
// Projet: MOVIES3D
// Auteur: O.Tonck
// Date  : Janvier 2010
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

namespace MOVIESVIEWCPP {
	public interface class IParamControl
	{
	public:

		// mise � jour de la configuration depuis le controle
		virtual void UpdateConfiguration();
		// mise � jour du controle depuis la configuration
		virtual void UpdateGUI();
	};
};
