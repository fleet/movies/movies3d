// -*- MC++ -*-
// ****************************************************************************
// Class: IViewSelectionObserver
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Juillet 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

#include <string>

public ref class ViewSelectionPolarPoint
{
public:
	ViewSelectionPolarPoint() {};
	ViewSelectionPolarPoint(int _ping, int _beam, int _echoIdx) :
		ping(_ping), beam(_beam), echoIdx(_echoIdx) {}



	int ping;
	int beam;
	int echoIdx;
};

public ref class ViewSelectionPoint
{
public:
	ViewSelectionPoint() {};
	ViewSelectionPoint(int _ping, double _depth) :
		ping(_ping), depth(_depth) {};

	int ping;
	double depth;
};

public ref class ViewSelectionRange
{
public:
	ViewSelectionRange() {};

	ViewSelectionPoint^ min;
	ViewSelectionPoint^ max;
};

class shoalextraction::ShoalData;

public interface class IViewSelectionObserver
{
public:

	//interface callback
	virtual void OnSelectionRange(ViewSelectionRange^ range, bool fromShoal) = 0;
	virtual void OnSelectionPoint(ViewSelectionPolarPoint^ point, System::String^ transducerName) = 0;
	virtual void OnSelectionError() = 0;

	//virtual shoalextraction::ShoalData * GetShoalAtPoint(ViewSelectionPolarPoint^point, const std::string & refTransducerName) = 0;
	virtual void SelectShoal(shoalextraction::ShoalData* pShoal, bool select) = 0;

	virtual ViewSelectionRange^ GetSelection() = 0;
	virtual bool IsShoalSelection() = 0;
	virtual System::String^ GetTransducerRefName() = 0;
	virtual DataFmt GetDataThreshold() = 0;

	delegate void SelectionChangedHandler(System::Object^ sender);
	event IViewSelectionObserver::SelectionChangedHandler ^ SelectionChanged;
};
