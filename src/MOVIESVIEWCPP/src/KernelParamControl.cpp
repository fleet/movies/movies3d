#include "KernelParamControl.h"

#include "ModuleManager/ModuleManager.h"
#include "M3DKernel/M3DKernel.h"
#include "DisplayParameter.h"
#include "EchoIntegration/EchoIntegrationModule.h"

using namespace MOVIESVIEWCPP;

void KernelParamControl::UpdateConfiguration()
{
	KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();

	param.setAutoDepthEnable(checkAutoDepth->Checked);
	param.setAutoLengthEnable(checkBoxAutoLenght->Checked);

	System::Single ^refSingle = System::Convert::ToSingle(textRange->Text);
	param.setMaxRange((float)refSingle);

	System::Int32 ^ref2 = System::Convert::ToInt32(textNbFrame->Text);
	param.setNbPingFanMax((int)ref2);

	System::Double ^ref = System::Convert::ToDouble(textBoxXspacing->Text);
	param.setScreenPixelSizeX(abs((double)ref));

	ref = System::Convert::ToDouble(textBoxYspacing->Text);
	param.setScreenPixelSizeY(abs((double)ref));

	param.setCustomSampling(m_CustomSamplingCheckBox->Checked);

	param.setIgnorePingsWithNoNavigation(checkBoxIgnorePingsNoNav->Checked);

	param.setIgnorePingsWithNoPosition(checkBoxIgnorePingsNoPos->Checked);

	// OTK - FAE082
	param.setIgnoreAsynchronousChannels(checkBoxIgnoreAsynchronousChannels->Checked);

	param.setWeightedEchoIntegration(checkBoxWeightEchoIntegration->Checked);

	param.setInterpolatePingPosition(checkBoxInterpolatePingPosition->Checked);

	param.setInterpolateAttitudes(checkBoxInterpolateAttitudes->Checked);

	// NMD - FAE 095
	param.setIgnorePhase(this->checkBoxIgnorePhase->Checked);

	param.setIgnoreIncompletePings(this->checkBoxIgnoreIncompletesPings->Checked);

	M3DKernel::GetInstance()->UpdateKernelParameter(param);

	// OTK - 04/06/2009 - les paramètres du noyau peuvent influer sur l'affichage
	// (sampling des tables de transformation)
	DisplayParameter::getInstance()->setDataChanged();
}

void KernelParamControl::UpdateGUI()
{
	KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();
	checkAutoDepth->Checked = param.getAutoDepthEnable();
	checkBoxAutoLenght->Checked = param.getAutoLengthEnable();
	textNbFrame->Text = (gcnew System::Int32(param.getNbPingFanMax()))->ToString();
	textRange->Text = (gcnew System::Double(param.getMaxRange()))->ToString();
	textBoxXspacing->Text = (gcnew System::Double(param.getScreenPixelSizeX()))->ToString();
	textBoxYspacing->Text = (gcnew System::Double(param.getScreenPixelSizeY()))->ToString();
	m_CustomSamplingCheckBox->Checked = param.getCustomSampling();
	checkBoxIgnorePingsNoNav->Checked = param.getIgnorePingsWithNoNavigation();
	checkBoxIgnorePingsNoPos->Checked = param.getIgnorePingsWithNoPosition();
	checkBoxIgnoreAsynchronousChannels->Checked = param.getIgnoreAsynchronousChannels(); // OTK - FAE082
	checkBoxWeightEchoIntegration->Checked = param.getWeightedEchoIntegration();
	checkBoxInterpolatePingPosition->Checked = param.getInterpolatePingPosition();
	checkBoxInterpolateAttitudes->Checked = param.getInterpolateAttitudes();

	// NMD - FAE 095
	checkBoxIgnorePhase->Checked = param.getIgnorePhase();
	checkBoxIgnoreIncompletesPings->Checked = param.getIgnoreIncompletePings();

	UpdateComponents();
}

System::Void KernelParamControl::m_CustomSamplingCheckBox_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	UpdateComponents();
}

System::Void KernelParamControl::UpdateComponents()
{
	textBoxYspacing->Enabled = m_CustomSamplingCheckBox->Checked;
	textBoxXspacing->Enabled = m_CustomSamplingCheckBox->Checked;
}

System::Void KernelParamControl::checkBoxWeightEchoIntegration_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	CModuleManager * pModuleManager = CModuleManager::getInstance();
	if (pModuleManager->GetEchoIntegrationModule()->getEnable()
		&& checkBoxWeightEchoIntegration->Checked != M3DKernel::GetInstance()->GetRefKernelParameter().getWeightedEchoIntegration())
	{
		MessageBox::Show("This parameter cannot be changed while the EchoIntegration Module is enabled");
		checkBoxWeightEchoIntegration->Checked = M3DKernel::GetInstance()->GetRefKernelParameter().getWeightedEchoIntegration();
	}
}
