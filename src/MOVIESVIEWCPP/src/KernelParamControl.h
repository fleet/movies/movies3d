#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de KernelParamControl
	/// </summary>
	public ref class KernelParamControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		KernelParamControl(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~KernelParamControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBoxEI;
	protected:
	private: System::Windows::Forms::CheckBox^  checkBoxWeightEchoIntegration;
	private: System::Windows::Forms::GroupBox^  groupBoxData;
	private: System::Windows::Forms::CheckBox^  checkBoxIgnorePingsNoNav;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::CheckBox^  m_CustomSamplingCheckBox;
	private: System::Windows::Forms::TextBox^  textBoxXspacing;
	private: System::Windows::Forms::TextBox^  textBoxYspacing;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::TextBox^  textNbFrame;
	private: System::Windows::Forms::TextBox^  textRange;
	private: System::Windows::Forms::CheckBox^  checkBoxAutoLenght;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::CheckBox^  checkAutoDepth;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::CheckBox^  checkBoxInterpolatePingPosition;
	private: System::Windows::Forms::CheckBox^  checkBoxIgnoreAsynchronousChannels;
	private: System::Windows::Forms::CheckBox^  checkBoxIgnorePhase;
	private: System::Windows::Forms::CheckBox^  checkBoxIgnorePingsNoPos;
	private: System::Windows::Forms::CheckBox^  checkBoxInterpolateAttitudes;
	private: System::Windows::Forms::CheckBox^  checkBoxIgnoreIncompletesPings;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel3;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel2;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel4;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBoxEI = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxWeightEchoIntegration = (gcnew System::Windows::Forms::CheckBox());
			this->groupBoxData = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxInterpolateAttitudes = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxIgnorePingsNoPos = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxIgnorePhase = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxIgnoreAsynchronousChannels = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxInterpolatePingPosition = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxIgnorePingsNoNav = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->m_CustomSamplingCheckBox = (gcnew System::Windows::Forms::CheckBox());
			this->textBoxXspacing = (gcnew System::Windows::Forms::TextBox());
			this->textBoxYspacing = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->textNbFrame = (gcnew System::Windows::Forms::TextBox());
			this->textRange = (gcnew System::Windows::Forms::TextBox());
			this->checkBoxAutoLenght = (gcnew System::Windows::Forms::CheckBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->checkAutoDepth = (gcnew System::Windows::Forms::CheckBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->checkBoxIgnoreIncompletesPings = (gcnew System::Windows::Forms::CheckBox());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->tableLayoutPanel3 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->tableLayoutPanel4 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->groupBoxEI->SuspendLayout();
			this->groupBoxData->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->tableLayoutPanel1->SuspendLayout();
			this->tableLayoutPanel2->SuspendLayout();
			this->tableLayoutPanel3->SuspendLayout();
			this->tableLayoutPanel4->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBoxEI
			// 
			this->groupBoxEI->Controls->Add(this->checkBoxWeightEchoIntegration);
			this->groupBoxEI->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxEI->Location = System::Drawing::Point(4, 344);
			this->groupBoxEI->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBoxEI->Name = L"groupBoxEI";
			this->groupBoxEI->Padding = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBoxEI->Size = System::Drawing::Size(336, 53);
			this->groupBoxEI->TabIndex = 20;
			this->groupBoxEI->TabStop = false;
			this->groupBoxEI->Text = L"Echo-Integration";
			// 
			// checkBoxWeightEchoIntegration
			// 
			this->checkBoxWeightEchoIntegration->AutoSize = true;
			this->checkBoxWeightEchoIntegration->Dock = System::Windows::Forms::DockStyle::Fill;
			this->checkBoxWeightEchoIntegration->Location = System::Drawing::Point(4, 19);
			this->checkBoxWeightEchoIntegration->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->checkBoxWeightEchoIntegration->Name = L"checkBoxWeightEchoIntegration";
			this->checkBoxWeightEchoIntegration->Size = System::Drawing::Size(328, 30);
			this->checkBoxWeightEchoIntegration->TabIndex = 0;
			this->checkBoxWeightEchoIntegration->Text = L"Weight Echo-Integration by distance travelled";
			this->checkBoxWeightEchoIntegration->UseVisualStyleBackColor = true;
			this->checkBoxWeightEchoIntegration->CheckedChanged += gcnew System::EventHandler(this, &KernelParamControl::checkBoxWeightEchoIntegration_CheckedChanged);
			// 
			// groupBoxData
			// 
			this->groupBoxData->Controls->Add(this->tableLayoutPanel1);
			this->groupBoxData->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxData->Location = System::Drawing::Point(4, 108);
			this->groupBoxData->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBoxData->Name = L"groupBoxData";
			this->groupBoxData->Padding = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBoxData->Size = System::Drawing::Size(336, 228);
			this->groupBoxData->TabIndex = 19;
			this->groupBoxData->TabStop = false;
			this->groupBoxData->Text = L"Data";
			// 
			// checkBoxInterpolateAttitudes
			// 
			this->checkBoxInterpolateAttitudes->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->checkBoxInterpolateAttitudes->AutoSize = true;
			this->checkBoxInterpolateAttitudes->Location = System::Drawing::Point(4, 120);
			this->checkBoxInterpolateAttitudes->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->checkBoxInterpolateAttitudes->Name = L"checkBoxInterpolateAttitudes";
			this->checkBoxInterpolateAttitudes->Size = System::Drawing::Size(320, 21);
			this->checkBoxInterpolateAttitudes->TabIndex = 5;
			this->checkBoxInterpolateAttitudes->Text = L"Interpolate attitudes";
			this->checkBoxInterpolateAttitudes->UseVisualStyleBackColor = true;
			// 
			// checkBoxIgnorePingsNoPos
			// 
			this->checkBoxIgnorePingsNoPos->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->checkBoxIgnorePingsNoPos->AutoSize = true;
			this->checkBoxIgnorePingsNoPos->Location = System::Drawing::Point(4, 33);
			this->checkBoxIgnorePingsNoPos->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->checkBoxIgnorePingsNoPos->Name = L"checkBoxIgnorePingsNoPos";
			this->checkBoxIgnorePingsNoPos->Size = System::Drawing::Size(320, 21);
			this->checkBoxIgnorePingsNoPos->TabIndex = 4;
			this->checkBoxIgnorePingsNoPos->Text = L"Ignore pings with no position data";
			this->checkBoxIgnorePingsNoPos->UseVisualStyleBackColor = true;
			// 
			// checkBoxIgnorePhase
			// 
			this->checkBoxIgnorePhase->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->checkBoxIgnorePhase->AutoSize = true;
			this->checkBoxIgnorePhase->Location = System::Drawing::Point(4, 149);
			this->checkBoxIgnorePhase->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->checkBoxIgnorePhase->Name = L"checkBoxIgnorePhase";
			this->checkBoxIgnorePhase->Size = System::Drawing::Size(320, 21);
			this->checkBoxIgnorePhase->TabIndex = 3;
			this->checkBoxIgnorePhase->Text = L"Ignore phase";
			this->checkBoxIgnorePhase->UseVisualStyleBackColor = true;
			// 
			// checkBoxIgnoreAsynchronousChannels
			// 
			this->checkBoxIgnoreAsynchronousChannels->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->checkBoxIgnoreAsynchronousChannels->AutoSize = true;
			this->checkBoxIgnoreAsynchronousChannels->Location = System::Drawing::Point(4, 62);
			this->checkBoxIgnoreAsynchronousChannels->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->checkBoxIgnoreAsynchronousChannels->Name = L"checkBoxIgnoreAsynchronousChannels";
			this->checkBoxIgnoreAsynchronousChannels->Size = System::Drawing::Size(320, 21);
			this->checkBoxIgnoreAsynchronousChannels->TabIndex = 2;
			this->checkBoxIgnoreAsynchronousChannels->Text = L"Ignore pings with asynchronous channels";
			this->checkBoxIgnoreAsynchronousChannels->UseVisualStyleBackColor = true;
			// 
			// checkBoxInterpolatePingPosition
			// 
			this->checkBoxInterpolatePingPosition->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->checkBoxInterpolatePingPosition->AutoSize = true;
			this->checkBoxInterpolatePingPosition->Location = System::Drawing::Point(4, 91);
			this->checkBoxInterpolatePingPosition->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->checkBoxInterpolatePingPosition->Name = L"checkBoxInterpolatePingPosition";
			this->checkBoxInterpolatePingPosition->Size = System::Drawing::Size(320, 21);
			this->checkBoxInterpolatePingPosition->TabIndex = 1;
			this->checkBoxInterpolatePingPosition->Text = L"Interpolate ping positions";
			this->checkBoxInterpolatePingPosition->UseVisualStyleBackColor = true;
			// 
			// checkBoxIgnorePingsNoNav
			// 
			this->checkBoxIgnorePingsNoNav->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->checkBoxIgnorePingsNoNav->AutoSize = true;
			this->checkBoxIgnorePingsNoNav->Location = System::Drawing::Point(4, 4);
			this->checkBoxIgnorePingsNoNav->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->checkBoxIgnorePingsNoNav->Name = L"checkBoxIgnorePingsNoNav";
			this->checkBoxIgnorePingsNoNav->Size = System::Drawing::Size(320, 21);
			this->checkBoxIgnorePingsNoNav->TabIndex = 0;
			this->checkBoxIgnorePingsNoNav->Text = L"Ignore pings with no navigation data";
			this->checkBoxIgnorePingsNoNav->UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->tableLayoutPanel3);
			this->groupBox2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBox2->Location = System::Drawing::Point(4, 405);
			this->groupBox2->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Padding = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBox2->Size = System::Drawing::Size(336, 126);
			this->groupBox2->TabIndex = 18;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Sampling";
			// 
			// m_CustomSamplingCheckBox
			// 
			this->m_CustomSamplingCheckBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->m_CustomSamplingCheckBox->AutoSize = true;
			this->tableLayoutPanel3->SetColumnSpan(this->m_CustomSamplingCheckBox, 2);
			this->m_CustomSamplingCheckBox->Location = System::Drawing::Point(4, 6);
			this->m_CustomSamplingCheckBox->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->m_CustomSamplingCheckBox->Name = L"m_CustomSamplingCheckBox";
			this->m_CustomSamplingCheckBox->Size = System::Drawing::Size(320, 21);
			this->m_CustomSamplingCheckBox->TabIndex = 12;
			this->m_CustomSamplingCheckBox->Text = L"Custom Sampling";
			this->m_CustomSamplingCheckBox->UseVisualStyleBackColor = true;
			this->m_CustomSamplingCheckBox->CheckedChanged += gcnew System::EventHandler(this, &KernelParamControl::m_CustomSamplingCheckBox_CheckedChanged);
			// 
			// textBoxXspacing
			// 
			this->textBoxXspacing->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->textBoxXspacing->Location = System::Drawing::Point(4, 40);
			this->textBoxXspacing->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->textBoxXspacing->Name = L"textBoxXspacing";
			this->textBoxXspacing->Size = System::Drawing::Size(156, 22);
			this->textBoxXspacing->TabIndex = 8;
			// 
			// textBoxYspacing
			// 
			this->textBoxYspacing->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->textBoxYspacing->Location = System::Drawing::Point(4, 74);
			this->textBoxYspacing->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->textBoxYspacing->Name = L"textBoxYspacing";
			this->textBoxYspacing->Size = System::Drawing::Size(156, 22);
			this->textBoxYspacing->TabIndex = 9;
			// 
			// label3
			// 
			this->label3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(168, 77);
			this->label3->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(156, 17);
			this->label3->TabIndex = 11;
			this->label3->Text = L"Y pixel size";
			// 
			// label4
			// 
			this->label4->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(168, 42);
			this->label4->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(156, 17);
			this->label4->TabIndex = 10;
			this->label4->Text = L"X pixel size";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->tableLayoutPanel2);
			this->groupBox1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBox1->Location = System::Drawing::Point(4, 4);
			this->groupBox1->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Padding = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBox1->Size = System::Drawing::Size(336, 96);
			this->groupBox1->TabIndex = 17;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Memory Size";
			// 
			// textNbFrame
			// 
			this->textNbFrame->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->textNbFrame->Location = System::Drawing::Point(4, 7);
			this->textNbFrame->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->textNbFrame->Name = L"textNbFrame";
			this->textNbFrame->Size = System::Drawing::Size(127, 22);
			this->textNbFrame->TabIndex = 1;
			// 
			// textRange
			// 
			this->textRange->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->textRange->Location = System::Drawing::Point(4, 43);
			this->textRange->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->textRange->Name = L"textRange";
			this->textRange->Size = System::Drawing::Size(127, 22);
			this->textRange->TabIndex = 2;
			// 
			// checkBoxAutoLenght
			// 
			this->checkBoxAutoLenght->Anchor = System::Windows::Forms::AnchorStyles::Left;
			this->checkBoxAutoLenght->AutoSize = true;
			this->checkBoxAutoLenght->Location = System::Drawing::Point(217, 7);
			this->checkBoxAutoLenght->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->checkBoxAutoLenght->Name = L"checkBoxAutoLenght";
			this->checkBoxAutoLenght->Size = System::Drawing::Size(107, 21);
			this->checkBoxAutoLenght->TabIndex = 3;
			this->checkBoxAutoLenght->Text = L"Auto Lenght";
			this->checkBoxAutoLenght->UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this->label2->Anchor = System::Windows::Forms::AnchorStyles::Left;
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(139, 46);
			this->label2->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(50, 17);
			this->label2->TabIndex = 7;
			this->label2->Text = L"Range";
			// 
			// checkAutoDepth
			// 
			this->checkAutoDepth->Anchor = System::Windows::Forms::AnchorStyles::Left;
			this->checkAutoDepth->AutoSize = true;
			this->checkAutoDepth->Location = System::Drawing::Point(217, 44);
			this->checkAutoDepth->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->checkAutoDepth->Name = L"checkAutoDepth";
			this->checkAutoDepth->Size = System::Drawing::Size(101, 21);
			this->checkAutoDepth->TabIndex = 4;
			this->checkAutoDepth->Text = L"Auto Depth";
			this->checkAutoDepth->UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this->label1->Anchor = System::Windows::Forms::AnchorStyles::Left;
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(139, 9);
			this->label1->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(70, 17);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Nb Frame";
			// 
			// checkBoxIgnoreIncompletesPings
			// 
			this->checkBoxIgnoreIncompletesPings->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->checkBoxIgnoreIncompletesPings->AutoSize = true;
			this->checkBoxIgnoreIncompletesPings->Location = System::Drawing::Point(3, 179);
			this->checkBoxIgnoreIncompletesPings->Name = L"checkBoxIgnoreIncompletesPings";
			this->checkBoxIgnoreIncompletesPings->Size = System::Drawing::Size(322, 21);
			this->checkBoxIgnoreIncompletesPings->TabIndex = 6;
			this->checkBoxIgnoreIncompletesPings->Text = L"Ignore incompletes pings";
			this->checkBoxIgnoreIncompletesPings->UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 1;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				20)));
			this->tableLayoutPanel1->Controls->Add(this->checkBoxIgnoreIncompletesPings, 0, 6);
			this->tableLayoutPanel1->Controls->Add(this->checkBoxIgnorePingsNoNav, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->checkBoxIgnorePhase, 0, 5);
			this->tableLayoutPanel1->Controls->Add(this->checkBoxInterpolateAttitudes, 0, 4);
			this->tableLayoutPanel1->Controls->Add(this->checkBoxIgnorePingsNoPos, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->checkBoxIgnoreAsynchronousChannels, 0, 2);
			this->tableLayoutPanel1->Controls->Add(this->checkBoxInterpolatePingPosition, 0, 3);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(4, 19);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 7;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 14.28571F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 14.28571F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 14.28571F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 14.28571F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 14.28571F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 14.28571F)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 14.28571F)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(328, 205);
			this->tableLayoutPanel1->TabIndex = 21;
			// 
			// tableLayoutPanel2
			// 
			this->tableLayoutPanel2->ColumnCount = 3;
			this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel2->Controls->Add(this->checkAutoDepth, 2, 1);
			this->tableLayoutPanel2->Controls->Add(this->checkBoxAutoLenght, 2, 0);
			this->tableLayoutPanel2->Controls->Add(this->textRange, 0, 1);
			this->tableLayoutPanel2->Controls->Add(this->label2, 1, 1);
			this->tableLayoutPanel2->Controls->Add(this->textNbFrame, 0, 0);
			this->tableLayoutPanel2->Controls->Add(this->label1, 1, 0);
			this->tableLayoutPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel2->Location = System::Drawing::Point(4, 19);
			this->tableLayoutPanel2->Name = L"tableLayoutPanel2";
			this->tableLayoutPanel2->RowCount = 2;
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel2->Size = System::Drawing::Size(328, 73);
			this->tableLayoutPanel2->TabIndex = 21;
			// 
			// tableLayoutPanel3
			// 
			this->tableLayoutPanel3->ColumnCount = 2;
			this->tableLayoutPanel3->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->tableLayoutPanel3->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->tableLayoutPanel3->Controls->Add(this->label3, 1, 2);
			this->tableLayoutPanel3->Controls->Add(this->textBoxYspacing, 0, 2);
			this->tableLayoutPanel3->Controls->Add(this->label4, 1, 1);
			this->tableLayoutPanel3->Controls->Add(this->textBoxXspacing, 0, 1);
			this->tableLayoutPanel3->Controls->Add(this->m_CustomSamplingCheckBox, 0, 0);
			this->tableLayoutPanel3->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel3->Location = System::Drawing::Point(4, 19);
			this->tableLayoutPanel3->Name = L"tableLayoutPanel3";
			this->tableLayoutPanel3->RowCount = 3;
			this->tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 33.33333F)));
			this->tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 33.33333F)));
			this->tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 33.33333F)));
			this->tableLayoutPanel3->Size = System::Drawing::Size(328, 103);
			this->tableLayoutPanel3->TabIndex = 21;
			// 
			// tableLayoutPanel4
			// 
			this->tableLayoutPanel4->ColumnCount = 1;
			this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel4->Controls->Add(this->groupBox1, 0, 0);
			this->tableLayoutPanel4->Controls->Add(this->groupBox2, 0, 3);
			this->tableLayoutPanel4->Controls->Add(this->groupBoxEI, 0, 2);
			this->tableLayoutPanel4->Controls->Add(this->groupBoxData, 0, 1);
			this->tableLayoutPanel4->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel4->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel4->Name = L"tableLayoutPanel4";
			this->tableLayoutPanel4->RowCount = 5;
			this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel4->Size = System::Drawing::Size(344, 613);
			this->tableLayoutPanel4->TabIndex = 21;
			// 
			// KernelParamControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->tableLayoutPanel4);
			this->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->Name = L"KernelParamControl";
			this->Size = System::Drawing::Size(344, 613);
			this->groupBoxEI->ResumeLayout(false);
			this->groupBoxEI->PerformLayout();
			this->groupBoxData->ResumeLayout(false);
			this->groupBox2->ResumeLayout(false);
			this->groupBox1->ResumeLayout(false);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			this->tableLayoutPanel2->ResumeLayout(false);
			this->tableLayoutPanel2->PerformLayout();
			this->tableLayoutPanel3->ResumeLayout(false);
			this->tableLayoutPanel3->PerformLayout();
			this->tableLayoutPanel4->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion

#pragma region Impl�mentation de IParamControl
	public: virtual void UpdateConfiguration();
	public: virtual void UpdateGUI();
#pragma endregion


	private: System::Void m_CustomSamplingCheckBox_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void UpdateComponents();
	private: System::Void checkBoxWeightEchoIntegration_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
};
}
