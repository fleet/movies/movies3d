// -*- MC++ -*-
// ****************************************************************************
// Class: KernelParamForm
//
// Description: Fen�tre de configuration du noyau
//
// Projet: MOVIES3D
// Auteur: O.Tonck
// Date  : Janvier 2010
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "ParameterForm.h"
#include "KernelParamControl.h"

namespace MOVIESVIEWCPP {
	public ref class KernelParamForm : public ParameterForm
	{
	public:
		KernelParamForm(void) :
			ParameterForm()
		{
			SetParamControl(gcnew KernelParamControl(), L"Kernel Parameters");
		}
	};
};
