using namespace System;
using namespace System::Windows::Forms;
using namespace System::Collections;

// IPSIS - OTK - classe permettant le tri des couches dans la listView en fonction
// du type de couche, de la profondeur de la couche, etc...
public ref class LayersListViewItemComparer : public System::Collections::IComparer
{
private:

public:
	LayersListViewItemComparer()
	{
	}

	virtual int Compare(Object^ x, Object^ y)
	{
		// si le type de la couche est fond, on renvoie sup�rieur
		int result = -String::Compare((dynamic_cast<ListViewItem^>(x))->SubItems[0]->Text,
			(dynamic_cast<ListViewItem^>(y))->SubItems[0]->Text);

		if (result != 0)
		{
			return result;
		}

		// si le type de couche est le m�me, on regarde le d�but de la couche
		Decimal layer1 = Convert::ToDecimal((dynamic_cast<ListViewItem^>(x))->SubItems[1]->Text);
		Decimal layer2 = Convert::ToDecimal((dynamic_cast<ListViewItem^>(y))->SubItems[1]->Text);
		result = Convert::ToInt32(layer1 - layer2);

		if (result != 0)
		{
			return result;
		}

		// si le type de couche est le m�me et que la couche a le meme minDepth ou minHeight, 
		// on compare le maxDepth ou maxHeight
		layer1 = Convert::ToDecimal((dynamic_cast<ListViewItem^>(x))->SubItems[2]->Text);
		layer2 = Convert::ToDecimal((dynamic_cast<ListViewItem^>(y))->SubItems[2]->Text);
		result = Convert::ToInt32(layer1 - layer2);

		return result;

	}
};
