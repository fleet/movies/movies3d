#include "LogForm.h"
#include "MLogMessage.h"

#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"

#include <queue>
#include <set>

using namespace MOVIESVIEWCPP;

class LogFormAppender : public Log::ILogAppender
{
	CRecursiveMutex mutex;
	std::queue<Log::LoggerItem> items;
	int capacity;

	std::set<Log::LogLevel> logAlerts;

public:
	LogFormAppender()
		: capacity(500)
	{
	}

	int maxSize() const
	{
		return capacity;
	}

	void setMaxSize(int maxSize)
	{
		mutex.Lock();

		capacity = maxSize;
		while (items.size() > capacity)
		{
			items.pop();
		}

		mutex.Unlock();
	}

	// Reset le syst�me d'alerte
	void resetCheckLevelMessage(const int level = Log::NO_LOG_LEVEL)
	{
		mutex.Lock();
		if (level == Log::NO_LOG_LEVEL)
		{
			logAlerts.clear();
		}
		else
		{
			logAlerts.erase(level);
		}
		mutex.Unlock();
	}

	// R�cup�re l'alerte de log ayant le niveau le plus haut
	int maxAlertLevel() const
	{
		int level = Log::NO_LOG_LEVEL;
		mutex.Lock();
		if (!logAlerts.empty())
		{
			level = *(logAlerts.rbegin());
		}
		mutex.Unlock();
		return level;
	}

	virtual void append(const Log::LoggerItem & logItem) override
	{
		mutex.Lock();

		items.push(logItem);
		while (items.size() > capacity)
		{
			items.pop();
		}
		
		// Ajout un niveau d'alerte pour ce niveau de log
		logAlerts.emplace(logItem.level);

		mutex.Unlock();
	}
	
	void FillUpListView(System::Windows::Forms::BindingSource ^bindingSource)
	{ 
		DateTime epoch = DateTime(1970, 1, 1, 0, 0, 0, 0, System::DateTimeKind::Utc);

		mutex.Lock();

		const int maxSize = capacity;

		// Shutdown the painting of the ListBox as items are added.
		if (items.size() > 0)
		{
			bindingSource->SuspendBinding();

			// Delete items
			while (bindingSource->Count > 0 && ((bindingSource->Count + items.size()) > capacity))
			{
				bindingSource->RemoveAt(0);
			}

			// Add items
			while(items.size() > 0)
			{
				auto logItem = items.front();
				auto message = gcnew System::String(logItem.message.c_str() );
				auto datetime = epoch.AddMilliseconds(logItem.timestamp);

				auto msg = gcnew MLogMessage(message, datetime, logItem.level);
				bindingSource->Add(msg);

				items.pop();
			}

			bindingSource->ResumeBinding();
		}

		mutex.Unlock();
	}
};

LogForm::LogForm()
{
	appender = new LogFormAppender();

	InitializeComponent();

	LevelColumn->DataPropertyName = "Level";
	DateColumn->DataPropertyName = "Datetime";
	MessageColumn->DataPropertyName = "Message";

	dataGridView->AutoGenerateColumns = false;
	dataGridView->DataSource = bindingSource;

	m_Freeze = false;
	toolStripButtonFreeze->Checked = m_Freeze;
	
	int maxLine = static_cast<LogFormAppender*>(appender)->maxSize();
	toolStripTextBox1->Text = System::Int32(maxLine).ToString();
}

System::Void LogForm::Update()
{
	if (m_Freeze)
		return;
	
	bool atEnd = (dataGridView->RowCount == 0) 
		|| (dataGridView->FirstDisplayedScrollingRowIndex + dataGridView->DisplayedRowCount(true)) >= dataGridView->RowCount;

	inhibateScroll = true;
	static_cast<LogFormAppender*>(appender)->FillUpListView(bindingSource);
	inhibateScroll = false;

	if (atEnd)
	{
		if (dataGridView->RowCount > 0)
		{
			dataGridView->FirstDisplayedScrollingRowIndex = dataGridView->RowCount - 1;
		}
	}
}

System::Void LogForm::dataGridView_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e)
{
	if (inhibateScroll)
	{
		e->NewValue = e->OldValue;
	}
}

int LogForm::MaxAlertLevel::get()
{
	return static_cast<LogFormAppender*>(appender)->maxAlertLevel();
}

System::Void LogForm::toolStripButtonAckErrors_Click(System::Object^  sender, System::EventArgs^  e)
{
	static_cast<LogFormAppender*>(appender)->resetCheckLevelMessage();
}

System::Void LogForm::timerRefreshLog_Tick(System::Object^  sender, System::EventArgs^  e)
{
	if (this->Visible)
		Update();
}

System::Void LogForm::toolStripButtonFreeze_Click(System::Object^  sender, System::EventArgs^  e)
{
	m_Freeze = toolStripButtonFreeze->Checked;
}

System::Void LogForm::toolStripButtonClear_Click(System::Object^  sender, System::EventArgs^  e)
{
	bindingSource->SuspendBinding();
	bindingSource->Clear();
	bindingSource->ResumeBinding();
}

System::Void LogForm::toolStripTextBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) 
{
	int maxLine = 0;
	if (System::Int32::TryParse(toolStripTextBox1->Text, maxLine))
	{
		static_cast<LogFormAppender*>(appender)->setMaxSize(maxLine);
	}
	else
	{
		maxLine = static_cast<LogFormAppender*>(appender)->maxSize();
		toolStripTextBox1->Text = System::Int32(maxLine).ToString();
	}
}

System::Void LogForm::LogForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) 
{
	e->Cancel = true;
	this->Hide();
}

System::Void LogForm::dataGridView_CellFormatting(System::Object^  sender, System::Windows::Forms::DataGridViewCellFormattingEventArgs^  e) 
{
	auto obj = bindingSource[e->RowIndex];
	if (obj == nullptr)
		return;

	auto logMessage = (MLogMessage^) obj;
	
	if (e->ColumnIndex == LevelColumn->Index)
	{
		switch (logMessage->Level)
		{
		case Log::TRACE_LOG_LEVEL:
			e->Value = "Trace";
			break;

		case Log::DEBUG_LOG_LEVEL:
			e->Value = "Debug";
			break;

		case Log::WARN_LOG_LEVEL:
			e->Value = "Warning";
			break;

		case Log::ERROR_LOG_LEVEL:
			e->Value = "Error";
			break;

		case Log::FATAL_LOG_LEVEL:
			e->Value = "Fatal";
			break;

		case Log::HAC_LOG_LEVEL:
			e->Value = "HacEvent";
			break;

		case Log::INFO_LOG_LEVEL:
		default:
			e->Value = "Info";
			break;
		}
	}
	
	switch (logMessage->Level)
	{
	case Log::WARN_LOG_LEVEL:
		e->CellStyle->ForeColor = System::Drawing::Color::Orange;
		break;

	case Log::ERROR_LOG_LEVEL:
	case Log::FATAL_LOG_LEVEL:
		e->CellStyle->ForeColor = System::Drawing::Color::Red;
		break;

	case Log::HAC_LOG_LEVEL:
		e->CellStyle->ForeColor = System::Drawing::Color::Blue;
		break;
	}
}