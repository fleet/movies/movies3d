#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "M3DKernel/utils/log/LogAppender.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de LogForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class LogForm : public System::Windows::Forms::Form
	{
	public:
		LogForm();

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~LogForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanelLog;
	private: System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanel1;
	private: System::Windows::Forms::Timer^  timerRefreshLog;
	private: System::Windows::Forms::ToolStrip^  toolStrip1;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonFreeze;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonClear;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
	private: System::Windows::Forms::ToolStripTextBox^  toolStripTextBox1;
	private: System::Windows::Forms::ToolStripLabel^  toolStripLabel1;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator2;
	private: System::Windows::Forms::DataGridView^  dataGridView;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  LevelColumn;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  DateColumn;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  MessageColumn;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonAckErrors;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(LogForm::typeid));
			this->tableLayoutPanelLog = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->flowLayoutPanel1 = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
			this->toolStripButtonAckErrors = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonFreeze = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonClear = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->toolStripTextBox1 = (gcnew System::Windows::Forms::ToolStripTextBox());
			this->toolStripLabel1 = (gcnew System::Windows::Forms::ToolStripLabel());
			this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->dataGridView = (gcnew System::Windows::Forms::DataGridView());
			this->LevelColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->DateColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->MessageColumn = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->timerRefreshLog = (gcnew System::Windows::Forms::Timer(this->components));
			this->tableLayoutPanelLog->SuspendLayout();
			this->flowLayoutPanel1->SuspendLayout();
			this->toolStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->BeginInit();
			this->SuspendLayout();
			// 
			// tableLayoutPanelLog
			// 
			this->tableLayoutPanelLog->ColumnCount = 1;
			this->tableLayoutPanelLog->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanelLog->Controls->Add(this->flowLayoutPanel1, 0, 0);
			this->tableLayoutPanelLog->Controls->Add(this->dataGridView, 22, 1);
			this->tableLayoutPanelLog->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanelLog->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanelLog->Name = L"tableLayoutPanelLog";
			this->tableLayoutPanelLog->RowCount = 2;
			this->tableLayoutPanelLog->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanelLog->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanelLog->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
				20)));
			this->tableLayoutPanelLog->Size = System::Drawing::Size(707, 305);
			this->tableLayoutPanelLog->TabIndex = 2;
			// 
			// flowLayoutPanel1
			// 
			this->flowLayoutPanel1->Controls->Add(this->toolStrip1);
			this->flowLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->flowLayoutPanel1->Location = System::Drawing::Point(3, 3);
			this->flowLayoutPanel1->Name = L"flowLayoutPanel1";
			this->flowLayoutPanel1->Size = System::Drawing::Size(701, 26);
			this->flowLayoutPanel1->TabIndex = 2;
			// 
			// toolStrip1
			// 
			this->toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {
				this->toolStripButtonAckErrors,
					this->toolStripButtonFreeze, this->toolStripButtonClear, this->toolStripSeparator1, this->toolStripTextBox1, this->toolStripLabel1,
					this->toolStripSeparator2
			});
			this->toolStrip1->Location = System::Drawing::Point(0, 0);
			this->toolStrip1->Name = L"toolStrip1";
			this->toolStrip1->Size = System::Drawing::Size(376, 25);
			this->toolStrip1->TabIndex = 0;
			this->toolStrip1->Text = L"toolStrip1";
			// 
			// toolStripButtonAckErrors
			// 
			this->toolStripButtonAckErrors->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->toolStripButtonAckErrors->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonAckErrors.Image")));
			this->toolStripButtonAckErrors->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonAckErrors->Name = L"toolStripButtonAckErrors";
			this->toolStripButtonAckErrors->Size = System::Drawing::Size(116, 22);
			this->toolStripButtonAckErrors->Text = L"Acknowledge errors";
			this->toolStripButtonAckErrors->Click += gcnew System::EventHandler(this, &LogForm::toolStripButtonAckErrors_Click);
			// 
			// toolStripButtonFreeze
			// 
			this->toolStripButtonFreeze->CheckOnClick = true;
			this->toolStripButtonFreeze->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->toolStripButtonFreeze->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonFreeze.Image")));
			this->toolStripButtonFreeze->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonFreeze->Name = L"toolStripButtonFreeze";
			this->toolStripButtonFreeze->Size = System::Drawing::Size(44, 22);
			this->toolStripButtonFreeze->Text = L"Freeze";
			this->toolStripButtonFreeze->Click += gcnew System::EventHandler(this, &LogForm::toolStripButtonFreeze_Click);
			// 
			// toolStripButtonClear
			// 
			this->toolStripButtonClear->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->toolStripButtonClear->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonClear.Image")));
			this->toolStripButtonClear->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonClear->Name = L"toolStripButtonClear";
			this->toolStripButtonClear->Size = System::Drawing::Size(38, 22);
			this->toolStripButtonClear->Text = L"Clear";
			this->toolStripButtonClear->Click += gcnew System::EventHandler(this, &LogForm::toolStripButtonClear_Click);
			// 
			// toolStripSeparator1
			// 
			this->toolStripSeparator1->Name = L"toolStripSeparator1";
			this->toolStripSeparator1->Size = System::Drawing::Size(6, 25);
			// 
			// toolStripTextBox1
			// 
			this->toolStripTextBox1->Name = L"toolStripTextBox1";
			this->toolStripTextBox1->Size = System::Drawing::Size(100, 25);
			this->toolStripTextBox1->TextChanged += gcnew System::EventHandler(this, &LogForm::toolStripTextBox1_TextChanged);
			// 
			// toolStripLabel1
			// 
			this->toolStripLabel1->Name = L"toolStripLabel1";
			this->toolStripLabel1->Size = System::Drawing::Size(52, 22);
			this->toolStripLabel1->Text = L"MaxLine";
			// 
			// toolStripSeparator2
			// 
			this->toolStripSeparator2->Name = L"toolStripSeparator2";
			this->toolStripSeparator2->Size = System::Drawing::Size(6, 25);
			// 
			// dataGridView
			// 
			this->dataGridView->AllowUserToAddRows = false;
			this->dataGridView->AllowUserToDeleteRows = false;
			this->dataGridView->AllowUserToOrderColumns = true;
			this->dataGridView->AllowUserToResizeRows = false;
			this->dataGridView->CellBorderStyle = System::Windows::Forms::DataGridViewCellBorderStyle::None;
			this->dataGridView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(3) {
				this->LevelColumn,
					this->DateColumn, this->MessageColumn
			});
			this->dataGridView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->dataGridView->Location = System::Drawing::Point(3, 35);
			this->dataGridView->Name = L"dataGridView";
			this->dataGridView->ReadOnly = true;
			this->dataGridView->RowHeadersVisible = false;
			this->dataGridView->Size = System::Drawing::Size(701, 267);
			this->dataGridView->TabIndex = 3;
			this->dataGridView->CellFormatting += gcnew System::Windows::Forms::DataGridViewCellFormattingEventHandler(this, &LogForm::dataGridView_CellFormatting);
			this->dataGridView->Scroll += gcnew System::Windows::Forms::ScrollEventHandler(this, &LogForm::dataGridView_Scroll);
			// 
			// LevelColumn
			// 
			this->LevelColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->LevelColumn->HeaderText = L"Level";
			this->LevelColumn->Name = L"LevelColumn";
			this->LevelColumn->ReadOnly = true;
			this->LevelColumn->Width = 80;
			// 
			// DateColumn
			// 
			this->DateColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->DateColumn->HeaderText = L"Date";
			this->DateColumn->Name = L"DateColumn";
			this->DateColumn->ReadOnly = true;
			this->DateColumn->Width = 120;
			// 
			// MessageColumn
			// 
			this->MessageColumn->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->MessageColumn->HeaderText = L"Message";
			this->MessageColumn->Name = L"MessageColumn";
			this->MessageColumn->ReadOnly = true;
			// 
			// timerRefreshLog
			// 
			this->timerRefreshLog->Enabled = true;
			this->timerRefreshLog->Interval = 1000;
			this->timerRefreshLog->Tick += gcnew System::EventHandler(this, &LogForm::timerRefreshLog_Tick);
			// 
			// LogForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(707, 305);
			this->Controls->Add(this->tableLayoutPanelLog);
			this->Name = L"LogForm";
			this->Text = L"Logs";
			this->TopMost = true;
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &LogForm::LogForm_FormClosing);
			this->tableLayoutPanelLog->ResumeLayout(false);
			this->flowLayoutPanel1->ResumeLayout(false);
			this->flowLayoutPanel1->PerformLayout();
			this->toolStrip1->ResumeLayout(false);
			this->toolStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion


	public: Log::ILogAppender * appender;

	private: System::Boolean m_Freeze;

	public: property int MaxAlertLevel
	{
		int get();
	}

	private: System::Windows::Forms::BindingSource ^ bindingSource = gcnew System::Windows::Forms::BindingSource();

	private: System::Void Update();
	private: System::Void timerRefreshLog_Tick(System::Object^  sender, System::EventArgs^  e);
	private: System::Void toolStripButtonFreeze_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void toolStripButtonClear_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void toolStripTextBox1_TextChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void LogForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
	private: System::Void dataGridView_CellFormatting(System::Object^  sender, System::Windows::Forms::DataGridViewCellFormattingEventArgs^  e);
	private: System::Void toolStripButtonAckErrors_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void dataGridView_Scroll(System::Object^  sender, System::Windows::Forms::ScrollEventArgs^  e);
	private: bool inhibateScroll = false;;
};
}
