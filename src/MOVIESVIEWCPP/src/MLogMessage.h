#pragma once
using namespace System::Windows::Forms;


public ref class MLogMessage
{
public:

	MLogMessage(System::String ^msg, System::DateTime datetime, int level)
	{
		m_datetime = datetime;
		m_Msg = msg;
		m_LogLevel = level;
	}

	property int Level
	{
		int get() { return m_LogLevel; }
	}

	property System::DateTime DateTime
	{
		System::DateTime get() { return m_datetime; }
	}

	property System::String ^ Message
	{
		System::String ^get() { return m_Msg; }
	}

	System::DateTime m_datetime;
	int m_LogLevel;
	System::String ^m_Msg;
};
