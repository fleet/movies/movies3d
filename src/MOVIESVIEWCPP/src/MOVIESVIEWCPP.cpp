// MOVIESVIEWCPP.cpp�: fichier projet principal.

#include "Form1.h"

using namespace MOVIESVIEWCPP;

[STAThreadAttribute]
int APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	// Activation des effets visuels de Windows�XP avant la cr�ation de tout contr�le
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	// Cr�er la fen�tre principale et l'ex�cuter
	Application::Run(gcnew Form1());
	return 0;
}