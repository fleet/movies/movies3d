#include "MergedEchoList.h"
#include "Echo3DList.h"
#include "vtk.h"
#include "vtkAppendPolyData.h"
#include "PingFan3d.h"
#include "vtkPingView.h"
#include "vtkGlyph4me.h"
#include "vtkGlyphSource2D.h"
#include "vtkSphereSource.h"
#include "vtkRegularPolygonSource.h"
#include "vtkCollection.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "vtkPointSource.h"
#include "vtkTubeFilter.h"
#include "vtkDoubleArray.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SingleTargetData.h"
#include "TSAnalysis/TSTrack.h"
#include "vtkSmartPointer.h"

#include <sstream>

MergedEchoList::MergedEchoList()
{
	m_polyDataOutput = vtkAppendPolyData::New();
	m_nbEntry = 0;
	m_pSingleEchoGlyph = NULL;
	m_pTracksTubeFilter = NULL;
	m_pTrackPoints = NULL;
	m_pTrackLines = NULL;
	m_pTracksPolyData = NULL;
	m_pTracksSize = NULL;
	m_pTracksStrength = NULL;
	m_pTracksLabelsPolyData = NULL;
	m_pPoint = NULL;
	m_pgroundPolyData = NULL;
	m_pCells = NULL;
	m_pSingleEchoPoint = NULL;
	m_pSingleEchoStrenght = NULL;
	m_polyDataPhaseOutput = vtkCollection::New();
}

MergedEchoList::~MergedEchoList(void)
{
	m_polyDataOutput->Delete();

	// OTK - FAE003 - Fuite m�moire
	if (m_pPoint)
		m_pPoint->Delete();
	if (m_pCells)
		m_pCells->Delete();
	if (m_pSingleEchoPoint)
		m_pSingleEchoPoint->Delete();
	if (m_pSingleEchoStrenght)
		m_pSingleEchoStrenght->Delete();
	if (m_pTrackLines)
		m_pTrackLines->Delete();
	if (m_pTrackPoints)
		m_pTrackPoints->Delete();
	if (m_pTracksPolyData)
		m_pTracksPolyData->Delete();
	if (m_pTracksSize)
		m_pTracksSize->Delete();
	if (m_pTracksStrength)
		m_pTracksStrength->Delete();
	if (m_pTracksLabelsPolyData)
		m_pTracksLabelsPolyData->Delete();
	if (m_pTracksTubeFilter)
		m_pTracksTubeFilter->Delete();
	// rmq : on ne delete pas les m_pgroundPolyData et m_pSingleEchoGlyph car ils sont r�f�renc�s ailleurs
}

void MergedEchoList::StartMerge()
{
	// OTK - FAE003 - Fuite m�moire - si on a des objets, on les desaloue avant d'en cr�er de nouveaux
	if (m_pPoint)
		m_pPoint->Delete();
	if (m_pCells)
		m_pCells->Delete();
	if (m_pgroundPolyData)
		m_pgroundPolyData->Delete();

	m_pPoint = vtkPoints::New();
	m_pgroundPolyData = vtkPolyData::New();
	m_pCells = vtkCellArray::New();
	m_pLastFan = NULL;
	if (m_pSingleEchoGlyph)
		m_pSingleEchoGlyph->Delete();
	m_pSingleEchoGlyph = vtkGlyph3D::New();
	if (m_pTracksTubeFilter)
		m_pTracksTubeFilter->Delete();
	m_pTracksTubeFilter = vtkTubeFilter::New();
	m_pTracksTubeFilter->SetCapping(true);
	m_pTracksTubeFilter->SetNumberOfSides(30);
	m_pTracksTubeFilter->SetVaryRadiusToVaryRadiusByAbsoluteScalar();


	vtkSphereSource *sphere = vtkSphereSource::New();
	sphere->SetThetaResolution(10);
	sphere->SetPhiResolution(10);

	m_pSingleEchoGlyph->SetSourceConnection(sphere->GetOutputPort());
	sphere->Delete();

	m_pSingleEchoGlyph->ClampingOff();
	m_pSingleEchoGlyph->OrientOff();
	m_pSingleEchoGlyph->SetColorModeToColorByScalar();

	// OTK - FAE003 - Fuite m�moire - si on a des objets, on les desaloue avant d'en cr�er de nouveaux
	if (m_pSingleEchoPoint)
		m_pSingleEchoPoint->Delete();
	if (m_pSingleEchoStrenght)
		m_pSingleEchoStrenght->Delete();
	m_pSingleEchoPoint = vtkPoints::New();
	m_pSingleEchoStrenght = vtkFloatArray::New();

	if (m_pTrackLines)
		m_pTrackLines->Delete();
	if (m_pTrackPoints)
		m_pTrackPoints->Delete();
	if (m_pTracksPolyData)
		m_pTracksPolyData->Delete();
	if (m_pTracksSize)
		m_pTracksSize->Delete();
	if (m_pTracksStrength)
		m_pTracksStrength->Delete();
	m_pTrackPoints = vtkPoints::New();
	m_pTrackLines = vtkCellArray::New();
	m_pTracksPolyData = vtkPolyData::New();
	m_pTracksSize = vtkDoubleArray::New();
	m_pTracksSize->SetName("tuberad");
	m_pTracksStrength = vtkFloatArray::New();
	m_pTracksStrength->SetName("str");

	m_pTracksLabelsPolyData = vtkPolyData::New();
	m_tracksLabels.clear();

	this->m_transducerBeamOpening.clear();

}
void MergedEchoList::StopMerge()
{
	if (m_pLastFan != NULL)
	{
		m_pCells->Squeeze();
		this->m_pgroundPolyData->SetPoints(m_pPoint);
		this->m_pgroundPolyData->SetStrips(m_pCells);

		m_pPoint->Delete();
		m_pCells->Delete();
		m_pPoint = NULL;
		m_pCells = NULL;
	}
	if (m_pSingleEchoPoint->GetNumberOfPoints())
	{
		vtkUnstructuredGrid* pGridPoint = vtkUnstructuredGrid::New();
		pGridPoint->SetPoints(m_pSingleEchoPoint);
		pGridPoint->GetPointData()->SetScalars(m_pSingleEchoStrenght);
		m_pSingleEchoPoint->Delete();
		m_pSingleEchoStrenght->Delete();
		m_pSingleEchoPoint = NULL;
		m_pSingleEchoStrenght = NULL;
		m_pSingleEchoGlyph->SetScaleFactor(1);
		m_pSingleEchoGlyph->SetScaleModeToDataScalingOff();


		m_pSingleEchoGlyph->SetInputData(pGridPoint);
		pGridPoint->Delete();
	}
	else
	{
		m_pSingleEchoGlyph->Delete();
		m_pSingleEchoGlyph = NULL;
	}

	m_pTracksPolyData->SetPoints(m_pTrackPoints);
	m_pTracksPolyData->SetLines(m_pTrackLines);
	m_pTracksPolyData->BuildLinks();
	m_pTracksPolyData->GetPointData()->AddArray(m_pTracksSize);
	m_pTracksPolyData->GetPointData()->SetActiveScalars("tuberad");
	m_pTracksPolyData->GetPointData()->AddArray(m_pTracksStrength);
	m_pTracksTubeFilter->SetInputData(m_pTracksPolyData);

	// Positionnement des labels des tracks
	m_pTracksPolyData->GetLines()->InitTraversal();
	vtkIdType npts;
	vtkIdType* pts;
	vtkSmartPointer<vtkPoints> labelPts = vtkSmartPointer<vtkPoints>::New();
	while (m_pTracksPolyData->GetLines()->GetNextCell(npts, pts))
	{
		double ptsCoords[3];
		m_pTracksPolyData->GetPoint(pts[npts - 1], ptsCoords);
		// On affiche le label un peu au dessus
		ptsCoords[1] += 0.1;
		labelPts->InsertNextPoint(ptsCoords);
	}
	m_pTracksLabelsPolyData->SetPoints(labelPts);
}

void MergedEchoList::AddPingFan(PingFan3D *pfan, bool display)
{
	if (display && pfan->m_echoList.getPingView())
	{
		m_polyDataOutput->AddInputConnection(pfan->m_echoList.getPingView()->getGlyph()->GetOutputPort());
		m_nbEntry++;

		if (m_boundingBox.isEmpty())
		{
			m_lastFanBoundingBox = pfan->m_echoList.m_boundingBox;
			m_boundingBox = pfan->m_echoList.m_boundingBox;
		}
		else
		{
			m_boundingBox.Extends(pfan->m_echoList.m_boundingBox);
		}
	}
	m_transducerBeamOpening.push_back(pfan->m_transducerBeamOpening);
}

void MergedEchoList::AddSingleEcho(PingFan3D *pfan)
{
	unsigned int echoNumber = pfan->m_echoList.m_singleEcho.size();
	if (echoNumber == 0)
		return;
	for (unsigned int echo = 0; echo < echoNumber; echo++)
	{
		BaseMathLib::Vector3D vectReal = BaseMathLib::Vector3D::ToViewOpengl(pfan->m_echoList.m_singleEcho[echo].m_position);
		vtkIdType myId = m_pSingleEchoPoint->InsertNextPoint(vectReal.ptr());
		if (!DisplayParameter::getInstance()->GetOverwriteScalarWithChannelId())
			m_pSingleEchoStrenght->InsertNextValue((float)pfan->m_echoList.m_singleEcho[echo].m_value);
		else
			m_pSingleEchoStrenght->InsertNextValue((float)pfan->m_echoList.m_singleEcho[echo].m_chanId * 100);
	}
}

// OTK - FAE214 - affichage des tracks
void MergedEchoList::AddTrack(TSTrack * pTrack, std::uint64_t firstPingID, std::uint64_t lastPingID)
{
	const std::vector<BaseMathLib::Vector3D> & positions = pTrack->GetPositions();
	const std::vector<BaseMathLib::Vector3D> & gpsPositions = pTrack->GetGPSPositions();
	const std::vector<std::uint64_t> & pingPositions = pTrack->GetPingPositions();

	bool bUseGPSPositionning = DisplayParameter::getInstance()->GetUseGPSPositionning();
	double dbMaxDepth = DisplayParameter::getInstance()->GetMaxDepth();
	double dbMinDepth = DisplayParameter::getInstance()->GetMinDepth();

	std::vector<BaseMathLib::Vector3D> tmpPositions;
	bool displayedDepth = false;
	for (size_t pingPos = 0; pingPos < pingPositions.size(); pingPos++)
	{
		std::uint64_t pingId = pingPositions[pingPos];
		if (pingId <= firstPingID && pingId >= lastPingID)
		{
			const BaseMathLib::Vector3D & point = bUseGPSPositionning ? gpsPositions[pingPos] : positions[pingPos];
			if (bUseGPSPositionning)
			{
				tmpPositions.push_back(BaseMathLib::Vector3D::ToViewOpengl(point));
			}
			else
			{
				tmpPositions.push_back(BaseMathLib::Vector3D::ToViewOpengl(point));
			}

			// On n'affiche la track que si a moins un de ces points est dans la zone de profondeur affich�e
			if (point.z >= dbMinDepth && point.z <= dbMaxDepth)
			{
				displayedDepth = true;
			}
		}
	}

	if (displayedDepth && tmpPositions.size() > 1)
	{
		const std::vector<unsigned short> & channels = pTrack->GetChannelIDs();
		const std::vector<SingleTargetData*> & singleTargets = pTrack->GetSingleTargets();
		Transducer * pTrans = pTrack->GetTransducer();
		double trackRadius = pTrans->getBeamsSamplesSpacing()*pTrans->m_pulseDuration / pTrans->m_timeSampleInterval / 2.0;
		std::vector<vtkIdType> pointIDs;
		for (size_t i = 0; i < tmpPositions.size(); i++)
		{
			SingleTargetDataCW* target = static_cast<SingleTargetDataCW*>(singleTargets[i]);
			if (target)
			{
				const BaseMathLib::Vector3D & pt = tmpPositions[i];
				pointIDs.push_back(m_pTrackPoints->InsertNextPoint(pt.x, pt.y, pt.z));
				m_pTracksSize->InsertNextValue(trackRadius);
				if (!DisplayParameter::getInstance()->GetOverwriteScalarWithChannelId())
					m_pTracksStrength->InsertNextValue((float)target->m_compensatedTS * 100);
				else
					m_pTracksStrength->InsertNextValue((float)channels[i] * 100);
			}
		}
		m_pTrackLines->InsertNextCell(pointIDs.size(), &pointIDs[0]);

		std::ostringstream oss;
		oss << pTrack->GetLabel();
		m_tracksLabels.push_back(oss.str());
	}
}

// NMD - 08/09/2011 - FAE 092 
void MergedEchoList::AddPhaseEcho(PingFan3D *pfan)
{
	if (pfan->m_echoList.getPingView())
	{
		// NMD - FAE 109 - Amelioration perfs 3D
		// On tente de recuperer le glyphe s'il a d�j� �t� construit, sinon on le construit
		vtkGlyph3D* phaseGlyph = pfan->m_echoList.getPingView()->getPhaseGlyph();
		if (phaseGlyph == NULL)
		{
			// Creation du glyphe
			phaseGlyph = vtkGlyph3D::New();

			// NMD - Remplacement du rendu des echos par des points pour gagner en performance
			// ACN - Remplacement du rendu des echos par des sphere suite au passage � VTK7.1,
			// qui rend d�sormais vtkPointSource par des cubes (contre des spheres auparavant)
			vtkSphereSource  * phaseSource = vtkSphereSource::New();
			phaseGlyph->SetSourceConnection(phaseSource->GetOutputPort());
			phaseSource->Delete();

			phaseGlyph->ClampingOff();
			phaseGlyph->OrientOff();
			phaseGlyph->SetColorModeToColorByScalar();

			pfan->m_echoList.getPingView()->setPhaseGlyph(phaseGlyph);

			// Ajout des donn�es au glyphe
			vtkPoints * phaseEchoPoint = vtkPoints::New();
			vtkFloatArray * phaseEchoStrength = vtkFloatArray::New();

			unsigned int echoNumber = pfan->m_echoList.m_phaseEcho.size();
			for (unsigned int echo = 0; echo < echoNumber; echo++)
			{
				BaseMathLib::Vector3D vectReal = BaseMathLib::Vector3D::ToViewOpengl(pfan->m_echoList.m_phaseEcho[echo].m_position);
				vtkIdType myId = phaseEchoPoint->InsertNextPoint(vectReal.ptr());
				if (!DisplayParameter::getInstance()->GetOverwriteScalarWithChannelId())
					phaseEchoStrength->InsertNextValue((float)pfan->m_echoList.m_phaseEcho[echo].m_value);
				else
					phaseEchoStrength->InsertNextValue((float)pfan->m_echoList.m_phaseEcho[echo].m_chanId * 100);
			}

			vtkUnstructuredGrid* pGridPoint = vtkUnstructuredGrid::New();
			pGridPoint->SetPoints(phaseEchoPoint);
			pGridPoint->GetPointData()->SetScalars(phaseEchoStrength);
			phaseEchoPoint->Delete();
			phaseEchoStrength->Delete();

			phaseGlyph->SetScaleModeToDataScalingOff();

			phaseGlyph->SetInputData(pGridPoint);
			pGridPoint->Delete();
		}

		m_polyDataPhaseOutput->AddItem(phaseGlyph);
	}
}

void MergedEchoList::AddGroundFan(PingFan3D *pFan)
{
	if (pFan->getPingFan()->getSounderRef()->m_isMultiBeam)
	{
		int beamNumber = (int)pFan->m_echoList.m_bottomPoints.size();
		// OTK - 08/09/2009 - l'algo de dessin du fond ne fonctionne pas si on n'a qu'un faisceau affich�
		// (dans ce cas, on a une ligne : le mappage par la texture plante dans le render VTK)
		//if(beamNumber==0)
		if (beamNumber > 1)
		{
			// pour le premier ping, rien � faire
			if (m_pLastFan != NULL && ((int)m_pLastFan->m_echoList.m_bottomPoints.size()) == beamNumber)
			{
				// si pas de fond d�tect� du tout sur l'un des deux pings, on ne trace pas le fond
				// si le fond n'est pas d�tect� sur les c�t�s, on ne le trace pas sur les c�t�s (cas SM20)
				// on trace donc entre chaque ping la bande entre le beam min et le beam max des deux pings

				int lastMinBeam, lastMaxBeam, minBeam, maxBeam;

				// Recherche des bords du fond sur le ping courant
				// ************************************************

				bool found = false;
				for (int beam = 0; beam < beamNumber && !found; beam++)
				{
					if (pFan->m_echoList.m_bottomPoints[beam].m_found)
					{
						minBeam = beam;
						found = true;
					}
				}
				if (!found)
				{
					m_pLastFan = pFan;
					return;
				}
				found = false;
				for (int beam = beamNumber - 1; beam >= 0 && !found; beam--)
				{
					if (pFan->m_echoList.m_bottomPoints[beam].m_found)
					{
						maxBeam = beam;
						found = true;
					}
				}

				// Recherche des bords du fond sur le ping pr�c�dent
				// **************************************************

				found = false;
				for (int beam = 0; beam < beamNumber && !found; beam++)
				{
					if (m_pLastFan->m_echoList.m_bottomPoints[beam].m_found)
					{
						lastMinBeam = beam;
						found = true;
					}
				}
				if (!found)
				{
					m_pLastFan = pFan;
					return;
				}
				found = false;
				for (int beam = beamNumber - 1; beam >= 0 && !found; beam--)
				{
					if (m_pLastFan->m_echoList.m_bottomPoints[beam].m_found)
					{
						lastMaxBeam = beam;
						found = true;
					}
				}

				// s'il n'y a qu'un seul faisceau, on ne peut pas tracer de surface...
				if (maxBeam == minBeam || lastMaxBeam == lastMinBeam)
				{
					m_pLastFan = pFan;
					return;
				}

				// on a ici nos fouchettes de faisceaux sur lesquelles repr�senter le fond.
				// on ajoute une grande cell correspondant au fond

				bool useGPSPositionning = DisplayParameter::getInstance()->GetUseGPSPositionning();
				Sounder * pSounder = pFan->getPingFan()->getSounderRef();
				unsigned int echoFondEval;

				// boucle sur les points en cr�ant les IDs
				// on commence par les fonds du ping courant
				std::vector<vtkIdType> currentIDs;
				for (int beam = minBeam; beam <= maxBeam; beam++)
				{
					if (pFan->m_echoList.m_bottomPoints[beam].m_found)
					{
						// fond d�tect� : on ajoute le point
						echoFondEval = pFan->m_echoList.m_bottomPoints[beam].m_numEcho;
						BaseMathLib::Vector3D vectReal;
						if (useGPSPositionning)
						{
							vectReal = BaseMathLib::Vector3D::ToViewOpengl(pSounder->GetPolarToCartesianCoord(pFan->getPingFan(), 0, beam, echoFondEval));
						}
						else
						{
							vectReal = BaseMathLib::Vector3D::ToViewOpengl(pSounder->GetPolarToWorldCoord(pFan->getPingFan(), 0, beam, echoFondEval));
						}
						currentIDs.push_back(m_pPoint->InsertNextPoint(vectReal.x, vectReal.y, vectReal.z));
					}
				}
				// on ajoute les fonds du ping pr�c�dent
				pSounder = m_pLastFan->getPingFan()->getSounderRef();
				std::vector<vtkIdType> previousIDs;
				for (int beam = lastMinBeam; beam <= lastMaxBeam; beam++)
				{
					if (m_pLastFan->m_echoList.m_bottomPoints[beam].m_found)
					{
						// fond d�tect� : on ajoute le point
						echoFondEval = m_pLastFan->m_echoList.m_bottomPoints[beam].m_numEcho;
						BaseMathLib::Vector3D vectReal;
						if (useGPSPositionning)
						{
							vectReal = BaseMathLib::Vector3D::ToViewOpengl(pSounder->GetPolarToCartesianCoord(m_pLastFan->getPingFan(), 0, beam, echoFondEval));
						}
						else
						{
							vectReal = BaseMathLib::Vector3D::ToViewOpengl(pSounder->GetPolarToWorldCoord(m_pLastFan->getPingFan(), 0, beam, echoFondEval));
						}
						previousIDs.push_back(m_pPoint->InsertNextPoint(vectReal.x, vectReal.y, vectReal.z));
					}
				}

				// creation des cellules
				size_t currenSize = currentIDs.size();
				size_t previousSize = previousIDs.size();
				int minSize = std::min(currenSize, previousSize);
				for (int i = 0; i < minSize - 1; i++)
				{
					m_pCells->InsertNextCell(4);
					m_pCells->InsertCellPoint(currentIDs[i]);
					m_pCells->InsertCellPoint(currentIDs[i + 1]);
					m_pCells->InsertCellPoint(previousIDs[i]);
					m_pCells->InsertCellPoint(previousIDs[i + 1]);
				}

				// on termine par les points restant sur le fond le plus std::int32_t
				// on ajoute des triangle avec pour sommet commun le dernier point
				// de la ligne la plus courte
				if (currenSize > previousSize)
				{
					for (int i = minSize - 1; i < currenSize - 1; i++)
					{
						m_pCells->InsertNextCell(3);
						m_pCells->InsertCellPoint(previousIDs[previousSize - 1]);
						m_pCells->InsertCellPoint(currentIDs[i]);
						m_pCells->InsertCellPoint(currentIDs[i + 1]);
					}
				}
				else
				{
					for (int i = minSize - 1; i < previousSize - 1; i++)
					{
						m_pCells->InsertNextCell(3);
						m_pCells->InsertCellPoint(currentIDs[currenSize - 1]);
						m_pCells->InsertCellPoint(previousIDs[i]);
						m_pCells->InsertCellPoint(previousIDs[i + 1]);
					}
				}

			}
		}
	}

	m_pLastFan = pFan;
	return;
}
