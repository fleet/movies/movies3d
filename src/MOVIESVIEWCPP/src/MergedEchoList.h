#pragma once
#include "BaseMathLib/Box.h"

class vtkAppendPolyData;
class vtkPolyData;
class vtkPoints;
class vtkCellArray;

class PingFan3D;
class vtkGlyph3D;
class vtkFloatArray;
class vtkCollection;
class vtkTubeFilter;
class vtkDoubleArray;
#include "Echo3DList.h"
#include "PingAxisInformation.h"

class TSTrack;


class MergedEchoList
{
public:
	MergedEchoList();
	virtual ~MergedEchoList(void);


	void StartMerge();
	void StopMerge();

	void AddPingFan(PingFan3D *pfan, bool display);
	void AddGroundFan(PingFan3D *pfan);
	void AddSingleEcho(PingFan3D *pfan);
	void AddTrack(TSTrack * pTrack, std::uint64_t firstPingID, std::uint64_t lastPingID);

	// NMD - FAE 092
	void AddPhaseEcho(PingFan3D *pfan);

	unsigned int getNbEntry() { return m_nbEntry; }
	vtkAppendPolyData* GetEchoPolyData() { return m_polyDataOutput; }
	vtkPolyData* GetFloorPolyData() { return m_pgroundPolyData; }
	vtkGlyph3D* GetSingleEchoGlyph() { return m_pSingleEchoGlyph; }
	vtkTubeFilter* GetSingleEchoTracks() { return m_pTracksTubeFilter; }
	vtkPolyData* GetSingleEchoTracksLabelPositions() { return m_pTracksLabelsPolyData; }
	const std::vector<std::string> & GetSingleEchoTracksLabels() { return m_tracksLabels; }
	vtkCollection* GetPhaseEchoData() { return m_polyDataPhaseOutput; }

	PingFan3D* GetLastPingFan() { return m_pLastFan; }


	BaseMathLib::Box m_lastFanBoundingBox;
	BaseMathLib::Box m_boundingBox;

	std::vector<PingAxisInformation> m_transducerBeamOpening;


private:
	vtkAppendPolyData*	m_polyDataOutput;
	unsigned int m_nbEntry;

	vtkGlyph3D			*m_pSingleEchoGlyph;
	// OTK - FAE214 - affichage des tracks des �chos simples
	vtkTubeFilter *m_pTracksTubeFilter;
	vtkPoints			*m_pTrackPoints;
	vtkCellArray  *m_pTrackLines;
	vtkPolyData   *m_pTracksPolyData;
	vtkDoubleArray *m_pTracksSize;
	vtkFloatArray *m_pTracksStrength;
	vtkPolyData * m_pTracksLabelsPolyData;
	std::vector<std::string> m_tracksLabels;

	vtkPolyData			*m_pgroundPolyData;
	vtkPoints			*m_pPoint;
	vtkCellArray		*m_pCells;
	// OTK - FAE046 - affichage correct du fond en cas de faisceaux non d�tect�s
	PingFan3D		*m_pLastFan;

	vtkPoints			*m_pSingleEchoPoint;
	vtkFloatArray		*m_pSingleEchoStrenght;

	// NMD - FAE 109 - Collection de glyphe representant l'ensemble des echos de phase
	vtkCollection * m_polyDataPhaseOutput;
};
