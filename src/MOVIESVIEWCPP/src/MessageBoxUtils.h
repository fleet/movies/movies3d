#pragma once


namespace MessageBoxUtils
{
	/// Affichage d'un message
	System::Windows::Forms::DialogResult Show(System::String ^ title, System::String ^msg, System::Windows::Forms::MessageBoxIcon icon)
	{
		return System::Windows::Forms::MessageBox::Show(msg, title, System::Windows::Forms::MessageBoxButtons::OK, icon);
	}

	/// Affichage d'un message type warning avec titre paramétrable
	System::Windows::Forms::DialogResult Warning(System::String ^ title, System::String ^msg)
	{
		return Show(title, msg, System::Windows::Forms::MessageBoxIcon::Warning);
	}

	/// Affichage d'un message type warning
	System::Windows::Forms::DialogResult Warning(System::String ^msg)
	{
		return Warning("Warning", msg);
	}

	/// Affichage d'un message type erreur avec titre paramétrable
	System::Windows::Forms::DialogResult Error(System::String ^ title, System::String ^msg)
	{
		return Show(title, msg, System::Windows::Forms::MessageBoxIcon::Error);
	}

	/// Affichage d'un message type erreur
	System::Windows::Forms::DialogResult Error(System::String ^msg)
	{
		return Error("Error", msg);
	}
}