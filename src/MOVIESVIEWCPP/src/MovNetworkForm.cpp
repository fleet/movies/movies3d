#include "MovNetworkForm.h"

//D�pendances
#include "ModuleManager/ModuleManager.h"
#include "OutputManager/OutputManagerModule.h"
#include "MovNetwork/MovNetwork.h"


using namespace MOVIESVIEWCPP;

// mise � jour des param�tres du module � partir des param�tres IHM
void MovNetworkForm::IHMToConfig()
{
	auto & config = CModuleManager::getInstance()->GetOutputManagerModule()->GetMvNetParameter();
	char* str = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBoxIPAddress->Text).ToPointer();
	std::string ret(str);
	System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(str));

	// validation de la string IP
	if (!ValidateIPString(textBoxIPAddress->Text))
	{
		throw gcnew Exception();
	}

	config.m_netHost = ret;
	config.m_cfgDelay = Convert::ToUInt16(textBoxConfEmissionPeriod->Text);
	config.m_netCfgPort = Convert::ToUInt16(textBoxConfPort->Text);
	config.m_netDescPort = Convert::ToUInt16(textBoxDescPort->Text);
	config.m_netDataPort = Convert::ToUInt16(textBoxResultsPort->Text);

	// application de la nouvelle configuration r�seau
	CModuleManager::getInstance()->GetMovNetwork()->updateConfiguration(config);
}

// mise � jour des param�tres IHM en fonction des param�tres du module
void MovNetworkForm::ConfigToIHM()
{
	const auto& config = CModuleManager::getInstance()->GetOutputManagerModule()->GetMvNetParameter();
	textBoxConfEmissionPeriod->Text = Convert::ToString(config.m_cfgDelay);
	textBoxConfPort->Text = Convert::ToString(config.m_netCfgPort);
	textBoxDescPort->Text = Convert::ToString(config.m_netDescPort);
	textBoxResultsPort->Text = Convert::ToString(config.m_netDataPort);
	textBoxIPAddress->Text = gcnew String(config.m_netHost.c_str());
}

// valide la chaine IP
bool MovNetworkForm::ValidateIPString(System::String^ IP)
{
	bool result = true;

	int i1, i2, i3, i4;

	int pos = IP->IndexOf(".");
	i1 = Convert::ToInt32(IP->Substring(0, pos));
	IP = IP->Substring(pos + 1);
	pos = IP->IndexOf(".");
	i2 = Convert::ToInt32(IP->Substring(0, pos));
	IP = IP->Substring(pos + 1);
	pos = IP->IndexOf(".");
	i3 = Convert::ToInt32(IP->Substring(0, pos));
	IP = IP->Substring(pos + 1);
	i4 = Convert::ToInt32(IP);

	if (i1 < 0 || i1 > 255 || i2 < 0 || i2 > 255 ||
		i3 < 0 || i3 > 255 || i4 < 0 || i4 > 255)
	{
		result = false;
	}

	return result;
}