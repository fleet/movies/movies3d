#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include <string>


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de MovNetworkForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class MovNetworkForm : public System::Windows::Forms::Form
	{
	public:
		MovNetworkForm(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//

			ConfigToIHM();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~MovNetworkForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  textBoxIPAddress;
	protected:
	private: System::Windows::Forms::Label^  labelIPAddress;
	private: System::Windows::Forms::Label^  labelConfPort;
	private: System::Windows::Forms::TextBox^  textBoxConfPort;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBoxResultsPort;
	private: System::Windows::Forms::Label^  labelDescPort;
	private: System::Windows::Forms::TextBox^  textBoxDescPort;
	private: System::Windows::Forms::Button^  buttonCancel;
	private: System::Windows::Forms::Button^  buttonOK;
	private: System::Windows::Forms::Label^  labelConfEmissionPeriod;
	private: System::Windows::Forms::TextBox^  textBoxConfEmissionPeriod;

	protected:

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

		// mise � jour des param�tres du module � partir des param�tres IHM
		void IHMToConfig();
		// mise � jour des param�tres IHM en fonction des param�tres du module
		void ConfigToIHM();
		// valide la chaine IP
		bool ValidateIPString(System::String^ IP);

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBoxIPAddress = (gcnew System::Windows::Forms::TextBox());
			this->labelIPAddress = (gcnew System::Windows::Forms::Label());
			this->labelConfPort = (gcnew System::Windows::Forms::Label());
			this->textBoxConfPort = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBoxResultsPort = (gcnew System::Windows::Forms::TextBox());
			this->labelDescPort = (gcnew System::Windows::Forms::Label());
			this->textBoxDescPort = (gcnew System::Windows::Forms::TextBox());
			this->buttonCancel = (gcnew System::Windows::Forms::Button());
			this->buttonOK = (gcnew System::Windows::Forms::Button());
			this->labelConfEmissionPeriod = (gcnew System::Windows::Forms::Label());
			this->textBoxConfEmissionPeriod = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// textBoxIPAddress
			// 
			this->textBoxIPAddress->Location = System::Drawing::Point(82, 12);
			this->textBoxIPAddress->Name = L"textBoxIPAddress";
			this->textBoxIPAddress->Size = System::Drawing::Size(144, 20);
			this->textBoxIPAddress->TabIndex = 0;
			// 
			// labelIPAddress
			// 
			this->labelIPAddress->AutoSize = true;
			this->labelIPAddress->Location = System::Drawing::Point(12, 15);
			this->labelIPAddress->Name = L"labelIPAddress";
			this->labelIPAddress->Size = System::Drawing::Size(64, 13);
			this->labelIPAddress->TabIndex = 1;
			this->labelIPAddress->Text = L"IP Address :";
			// 
			// labelConfPort
			// 
			this->labelConfPort->AutoSize = true;
			this->labelConfPort->Location = System::Drawing::Point(14, 93);
			this->labelConfPort->Name = L"labelConfPort";
			this->labelConfPort->Size = System::Drawing::Size(96, 13);
			this->labelConfPort->TabIndex = 3;
			this->labelConfPort->Text = L"Configuration port :";
			// 
			// textBoxConfPort
			// 
			this->textBoxConfPort->Location = System::Drawing::Point(162, 90);
			this->textBoxConfPort->Name = L"textBoxConfPort";
			this->textBoxConfPort->Size = System::Drawing::Size(66, 20);
			this->textBoxConfPort->TabIndex = 2;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 67);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(69, 13);
			this->label1->TabIndex = 5;
			this->label1->Text = L"Results port :";
			// 
			// textBoxResultsPort
			// 
			this->textBoxResultsPort->Location = System::Drawing::Point(160, 64);
			this->textBoxResultsPort->Name = L"textBoxResultsPort";
			this->textBoxResultsPort->Size = System::Drawing::Size(66, 20);
			this->textBoxResultsPort->TabIndex = 4;
			// 
			// labelDescPort
			// 
			this->labelDescPort->AutoSize = true;
			this->labelDescPort->Location = System::Drawing::Point(12, 41);
			this->labelDescPort->Name = L"labelDescPort";
			this->labelDescPort->Size = System::Drawing::Size(87, 13);
			this->labelDescPort->TabIndex = 7;
			this->labelDescPort->Text = L"Description port :";
			// 
			// textBoxDescPort
			// 
			this->textBoxDescPort->Location = System::Drawing::Point(160, 38);
			this->textBoxDescPort->Name = L"textBoxDescPort";
			this->textBoxDescPort->Size = System::Drawing::Size(66, 20);
			this->textBoxDescPort->TabIndex = 6;
			// 
			// buttonCancel
			// 
			this->buttonCancel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->buttonCancel->Location = System::Drawing::Point(123, 147);
			this->buttonCancel->Name = L"buttonCancel";
			this->buttonCancel->Size = System::Drawing::Size(106, 30);
			this->buttonCancel->TabIndex = 9;
			this->buttonCancel->Text = L"Cancel";
			this->buttonCancel->UseVisualStyleBackColor = true;
			this->buttonCancel->Click += gcnew System::EventHandler(this, &MovNetworkForm::buttonCancel_Click);
			// 
			// buttonOK
			// 
			this->buttonOK->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->buttonOK->Location = System::Drawing::Point(12, 147);
			this->buttonOK->Name = L"buttonOK";
			this->buttonOK->Size = System::Drawing::Size(106, 30);
			this->buttonOK->TabIndex = 8;
			this->buttonOK->Text = L"OK";
			this->buttonOK->UseVisualStyleBackColor = true;
			this->buttonOK->Click += gcnew System::EventHandler(this, &MovNetworkForm::buttonOK_Click);
			// 
			// labelConfEmissionPeriod
			// 
			this->labelConfEmissionPeriod->AutoSize = true;
			this->labelConfEmissionPeriod->Location = System::Drawing::Point(14, 119);
			this->labelConfEmissionPeriod->Name = L"labelConfEmissionPeriod";
			this->labelConfEmissionPeriod->Size = System::Drawing::Size(164, 13);
			this->labelConfEmissionPeriod->TabIndex = 11;
			this->labelConfEmissionPeriod->Text = L"Configuration emission period (s) :";
			// 
			// textBoxConfEmissionPeriod
			// 
			this->textBoxConfEmissionPeriod->Location = System::Drawing::Point(184, 116);
			this->textBoxConfEmissionPeriod->Name = L"textBoxConfEmissionPeriod";
			this->textBoxConfEmissionPeriod->Size = System::Drawing::Size(44, 20);
			this->textBoxConfEmissionPeriod->TabIndex = 10;
			// 
			// MovNetworkForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(239, 189);
			this->Controls->Add(this->labelConfEmissionPeriod);
			this->Controls->Add(this->textBoxConfEmissionPeriod);
			this->Controls->Add(this->buttonCancel);
			this->Controls->Add(this->buttonOK);
			this->Controls->Add(this->labelDescPort);
			this->Controls->Add(this->textBoxDescPort);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBoxResultsPort);
			this->Controls->Add(this->labelConfPort);
			this->Controls->Add(this->textBoxConfPort);
			this->Controls->Add(this->labelIPAddress);
			this->Controls->Add(this->textBoxIPAddress);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->Name = L"MovNetworkForm";
			this->Text = L"MovNetworkForm";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		// Bouton cancel
	private: System::Void buttonCancel_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
			 // Bouton OK
	private: System::Void buttonOK_Click(System::Object^  sender, System::EventArgs^  e) {
		try
		{
			IHMToConfig();
			Close();
		}
		catch (Exception^)
		{
			MessageBox::Show("Invalid configuration.");
		}
	}
	};
}
