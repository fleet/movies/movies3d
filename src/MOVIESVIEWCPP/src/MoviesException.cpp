#include "MoviesException.h"

using namespace MOVIESVIEWCPP;

MoviesException::MoviesException()
	: System::ApplicationException("Movies3D unknown exception")
{
}

MoviesException::MoviesException(System::String ^ message)
	: System::ApplicationException(message)
{
}