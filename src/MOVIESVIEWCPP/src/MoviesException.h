#pragma once

namespace MOVIESVIEWCPP
{
	ref class MoviesException : System::ApplicationException
	{
	public:
		MoviesException();
		MoviesException(System::String ^ message);
	};
}