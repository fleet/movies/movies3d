#pragma once

#include "ParameterForm.h"
#include "MultifrequencyEchogramsControl.h"

namespace MOVIESVIEWCPP 
{
	public ref class MultifrequencyEchogramsForm : public ParameterForm
	{
	public:
		MultifrequencyEchogramsForm() :	ParameterForm()
		{
			SetParamControl(gcnew MultifrequencyEchogramsControl(), L"Multifrequency Echograms");
			FormBorderStyle = ::FormBorderStyle::Sizable;
			AutoSize = false;
		}
	};
};
