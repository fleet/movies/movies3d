#include "MultifrequencyEchogram.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"

MultifrequencyEchogram::MultifrequencyEchogram()
{
}

MultifrequencyEchogram::MultifrequencyEchogram(const MultifrequencyEchogram& other)
	: m_sounderId(other.m_sounderId), m_transducerRed(other.m_transducerRed), m_minMaxRed(other.m_minMaxRed),
	m_transducerGreen(other.m_transducerGreen), m_minMaxGreen(other.m_minMaxGreen), m_transducerBlue(other.m_transducerBlue), m_minMaxBlue(other.m_minMaxBlue)
{
}

MultifrequencyEchogram::~MultifrequencyEchogram()
{
}

std::string MultifrequencyEchogram::getName() const
{
	std::string name;

	name += m_transducerRed;
	name += ", ";

	name += m_transducerGreen;
	name += ", ";

	name += m_transducerBlue;

	return name;
}

bool MultifrequencyEchogram::getIndexes(unsigned int& sounderIndex, unsigned int& transducerRedIndex, unsigned int& transducerGreenIndex, unsigned int& transducerBlueIndex) const
{
	sounderIndex = -1;
	transducerRedIndex = -1;
	transducerGreenIndex = -1;
	transducerBlueIndex = -1;

	M3DKernel *pKernel = M3DKernel::GetInstance();
	for (unsigned int sound = 0; sound < pKernel->getObjectMgr()->GetSounderDefinition().GetNbSounder(); sound++)
	{
		int indexRed = -1, indexGreen = -1, indexBlue = -1;
		Sounder *sounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(sound);
		if (sounder && sounder->m_SounderId == getSounderId())
		{
			for (unsigned int i = 0; i < sounder->GetTransducerCount(); ++i)
			{
				auto name = std::string(sounder->GetTransducer(i)->m_transName);
				if (m_transducerRed == name)
					indexRed = i;
				if (m_transducerGreen == name)
					indexGreen = i;
				if (m_transducerBlue == name)
					indexBlue = i;
			}

			if (indexRed >= 0 && indexGreen >= 0 && indexBlue >= 0)
			{
				sounderIndex = sound;
				transducerRedIndex = indexRed;
				transducerGreenIndex = indexGreen;
				transducerBlueIndex = indexBlue;
				return true;
			}
		}
	}

	return false;
}

void MultifrequencyEchogram::setIndexes(unsigned int sounderIndex, unsigned int transducerRedIndex, unsigned int transducerGreenIndex, unsigned int transducerBlueIndex)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	Sounder *sounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(sounderIndex);

	if (sounder)
	{
		setSounderId(sounder->m_SounderId);
		auto trRed = sounder->GetTransducer(transducerRedIndex);
		if (trRed)
			setTransducerRed(trRed->m_transName);

		auto trGreen = sounder->GetTransducer(transducerGreenIndex);
		if (trGreen)
			setTransducerGreen(trGreen->m_transName);

		auto trBlue = sounder->GetTransducer(transducerBlueIndex);
		if (trBlue)
			setTransducerBlue(trBlue->m_transName);
	}
}
