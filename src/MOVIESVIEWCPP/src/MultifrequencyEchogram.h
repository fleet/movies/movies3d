#pragma once

#include <utility>
#include <vector>

class MultifrequencyEchogram
{
public:
	MultifrequencyEchogram();
	MultifrequencyEchogram(const MultifrequencyEchogram& other);

	~MultifrequencyEchogram();

	bool isValid() const { return !m_transducerRed.empty() && !m_transducerGreen.empty() && !m_transducerBlue.empty(); }

	std::uint32_t getSounderId() const { return m_sounderId; }
	void setSounderId(std::uint32_t id) { m_sounderId = id; }

	std::string getTransducerRed() const { return m_transducerRed; }
	void setTransducerRed(const std::string& index) { m_transducerRed = index; }

	int getMinValueRed() const { return m_minMaxRed.first; }
	int getMaxValueRed() const { return m_minMaxRed.second; }
	void setMinMaxValueRed(int min, int max) { m_minMaxRed = std::make_pair(min, max); }

	std::string getTransducerGreen() const { return m_transducerGreen; }
	void setTransducerGreen(const std::string& index) { m_transducerGreen = index; }

	int getMinValueGreen() const { return m_minMaxGreen.first; }
	int getMaxValueGreen() const { return m_minMaxGreen.second; }
	void setMinMaxValueGreen(int min, int max) { m_minMaxGreen = std::make_pair(min, max); }

	std::string getTransducerBlue() const { return m_transducerBlue; }
	void setTransducerBlue(const std::string& index) { m_transducerBlue = index; }

	int getMinValueBlue() const { return m_minMaxBlue.first; }
	int getMaxValueBlue() const { return m_minMaxBlue.second; }
	void setMinMaxValueBlue(int min, int max) { m_minMaxBlue = std::make_pair(min, max); }

	std::string getName() const;

	/**
	* \brief Returns the indexes of the sounder and the transducers in the current configuration.
	* \details Returns false if the sounder or the transducers are not found in the current configuration.
	*/
	bool getIndexes(unsigned int& sounderIndex, unsigned int& transducerRedIndex, unsigned int& transducerGreenIndex, unsigned int& transducerBlueIndex) const;

	///Updates the echogram with the specified sounder and transducer indexes
	void setIndexes(unsigned int sounderIndex, unsigned int transducerRedIndex, unsigned int transducerGreenIndex, unsigned int transducerBlueIndex);

private:
	std::uint32_t m_sounderId = 0;
	
	std::string m_transducerRed;
	std::pair<int, int> m_minMaxRed;

	std::string m_transducerGreen;
	std::pair<int, int> m_minMaxGreen;

	std::string m_transducerBlue;
	std::pair<int, int> m_minMaxBlue;
};
