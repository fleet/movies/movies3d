#include "MultifrequencyEchogramsControl.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "DisplayParameter.h"
#include "MultifrequencyEchogramsParameter.h"

using namespace MOVIESVIEWCPP;

MultifrequencyEchogramsControl::MultifrequencyEchogramsControl()
{
	InitializeComponent();
	m_echograms = new std::vector<MultifrequencyEchogram>();

	populateSounders();
	setEchogramEditionEnabled(false);
}

MultifrequencyEchogramsControl::~MultifrequencyEchogramsControl()
{
	delete m_echograms;

	if (components)
	{
		delete components;
	}
}

void MultifrequencyEchogramsControl::UpdateConfiguration()
{
	MultifrequencyEchogramsParameter::getInstance()->setEchograms(*m_echograms);
}

void MultifrequencyEchogramsControl::UpdateGUI()
{
	setEchograms(MultifrequencyEchogramsParameter::getInstance()->getEchograms());
}

void MultifrequencyEchogramsControl::populateSounders()
{
	cbSounder->Items->Clear();
	m_sounderIndexList.Clear();

	M3DKernel *pKernel = M3DKernel::GetInstance();
	for (unsigned int sound = 0; sound < pKernel->getObjectMgr()->GetSounderDefinition().GetNbSounder(); sound++)
	{
		Sounder *sounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(sound);

		if (sounder->GetTransducerCount() >= 3)
		{
			cbSounder->Items->Add(gcnew String("Sounder Id[") + sounder->m_SounderId + "]");
			m_sounderIndexList.Add(sound);
		}
	}

	if (cbSounder->Items->Count == 0)
	{
		btnAdd->Enabled = false;
		btnRemove->Enabled = false;
	}
}

void MultifrequencyEchogramsControl::populateTransducers(Sounder* sounder)
{
	cbTrans1->Items->Clear();
	cbTrans2->Items->Clear();
	cbTrans3->Items->Clear();

	for (unsigned int i = 0; i < sounder->GetTransducerCount(); ++i)
	{
		auto transducer = sounder->GetTransducer(i);
		auto transducerName = gcnew System::String(transducer->m_transName);

		cbTrans1->Items->Add(transducerName);
		cbTrans2->Items->Add(transducerName);
		cbTrans3->Items->Add(transducerName);
	}
}

void MultifrequencyEchogramsControl::setEchograms(const std::vector<MultifrequencyEchogram>& echograms)
{
	listBox->Items->Clear();
	for (auto echogram : echograms)
	{
		unsigned int sounderIndex, index1, index2, index3;
		if (echogram.getIndexes(sounderIndex, index1, index2, index3))
		{
			listBox->Items->Add(gcnew String(echogram.getName().c_str()));
			m_echograms->push_back(echogram);
		}
	}
}

System::Void MultifrequencyEchogramsControl::cbSounder_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	Sounder *sounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(cbSounder->SelectedIndex);
	if (sounder)
	{
		populateTransducers(sounder);
	}
}


void MultifrequencyEchogramsControl::addEchogram()
{
	MultifrequencyEchogram echogram;
	m_echograms->push_back(echogram);
}

void MultifrequencyEchogramsControl::display(const MultifrequencyEchogram& echogram)
{
	unsigned int sounderIndex, redIndex, greenIndex, blueIndex;
	if (!echogram.getIndexes(sounderIndex, redIndex, greenIndex, blueIndex))
		return; //echogram not found

	changeInprogress = true;
	setEchogramEditionEnabled(true);

	cbSounder->SelectedIndex = m_sounderIndexList.IndexOf(sounderIndex);
	M3DKernel *pKernel = M3DKernel::GetInstance();

	if (cbSounder->SelectedIndex < 0 && pKernel->getObjectMgr()->GetSounderDefinition().GetNbSounder() > 0)
		cbSounder->SelectedIndex = 0;
	else if (pKernel->getObjectMgr()->GetSounderDefinition().GetNbSounder() == 0)
		return;

	
	Sounder *sounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(cbSounder->SelectedIndex);
	populateTransducers(sounder);

	cbTrans1->SelectedIndex = redIndex;
	cbTrans2->SelectedIndex = greenIndex;
	cbTrans3->SelectedIndex = blueIndex;

	minDb1->Value = echogram.getMinValueRed() / 100;
	maxDb1->Value = echogram.getMaxValueRed() / 100;
	minDb2->Value = echogram.getMinValueGreen() / 100;
	maxDb2->Value = echogram.getMaxValueGreen() / 100;
	minDb3->Value = echogram.getMinValueBlue() / 100;
	maxDb3->Value = echogram.getMaxValueBlue() / 100;

	changeInprogress = false;
}

void MultifrequencyEchogramsControl::update(MultifrequencyEchogram& echogram)
{
	echogram.setIndexes(m_sounderIndexList[cbSounder->SelectedIndex], cbTrans1->SelectedIndex, cbTrans2->SelectedIndex, cbTrans3->SelectedIndex);

	echogram.setMinMaxValueRed(Decimal::ToInt32(minDb1->Value) * 100, Decimal::ToInt32(maxDb1->Value) * 100);
	echogram.setMinMaxValueGreen(Decimal::ToInt32(minDb2->Value) * 100, Decimal::ToInt32(maxDb2->Value) * 100);
	echogram.setMinMaxValueBlue(Decimal::ToInt32(minDb3->Value) * 100, Decimal::ToInt32(maxDb3->Value) * 100);
}

void MultifrequencyEchogramsControl::updateCurrentEchogram()
{
	if (!changeInprogress)
	{
		int index = listBox->SelectedIndex;
		auto& current = (*m_echograms)[index];
		update(current);

		listBox->Items[index] = gcnew String(current.getName().c_str());
		listBox->SelectedIndex = index;
	}
}

void MultifrequencyEchogramsControl::setEchogramEditionEnabled(bool value)
{
	panel->Enabled = value;
	minColor1->Visible = value;
	minColor2->Visible = value;
	minColor3->Visible = value;
	maxColor1->Visible = value;
	maxColor2->Visible = value;
	maxColor3->Visible = value;
}

System::Void MultifrequencyEchogramsControl::listBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	if (listBox->SelectedIndex >= 0)
		display(m_echograms->at(listBox->SelectedIndex));
}

System::Void  MultifrequencyEchogramsControl::btnAdd_Click(System::Object^  sender, System::EventArgs^  e)
{
	auto displayParams = DisplayParameter::getInstance();
	auto defaultMin = displayParams->GetMinThreshold();
	auto defaultMax = displayParams->GetMaxThreshold();

	MultifrequencyEchogram echogram;
	echogram.setIndexes(m_sounderIndexList[0], 0, 1, 2);
	echogram.setMinMaxValueRed(defaultMin, defaultMax);
	echogram.setMinMaxValueGreen(defaultMin, defaultMax);
	echogram.setMinMaxValueBlue(defaultMin, defaultMax);

	m_echograms->push_back(echogram);
	listBox->Items->Add(gcnew String(echogram.getName().c_str()));

	listBox->SelectedIndex = listBox->Items->Count - 1;
}

System::Void  MultifrequencyEchogramsControl::btnRemove_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (listBox->SelectedIndex >= 0)
	{
		m_echograms->erase(m_echograms->begin() + listBox->SelectedIndex);
		listBox->Items->RemoveAt(listBox->SelectedIndex);
	}
}