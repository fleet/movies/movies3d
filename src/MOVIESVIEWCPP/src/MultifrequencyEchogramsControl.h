#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"
#include "MultifrequencyEchogram.h"

class Sounder;

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Summary for MultifrequencyEchogramsControl
	/// </summary>
	public ref class MultifrequencyEchogramsControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		MultifrequencyEchogramsControl();

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MultifrequencyEchogramsControl();

	protected:

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::NumericUpDown^  maxDb1;

	private: System::Windows::Forms::NumericUpDown^  minDb1;

	private: System::Windows::Forms::ComboBox^  cbTrans1;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ComboBox^  cbSounder;


	private: System::Windows::Forms::GroupBox^  groupBox2;

	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Button^  btnRemove;















	private: System::Windows::Forms::Button^  btnAdd;

	private: System::Windows::Forms::Panel^  panel;

	private: System::Windows::Forms::GroupBox^  groupBox3;
	private: System::Windows::Forms::NumericUpDown^  maxDb3;

	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::NumericUpDown^  minDb3;

	private: System::Windows::Forms::ComboBox^  cbTrans3;

	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::NumericUpDown^  maxDb2;

	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::NumericUpDown^  minDb2;

	private: System::Windows::Forms::ComboBox^  cbTrans2;
	private: System::Windows::Forms::Panel^  minColor1;
	private: System::Windows::Forms::Panel^  maxColor1;
	private: System::Windows::Forms::Panel^  maxColor3;




	private: System::Windows::Forms::Panel^  minColor3;
	private: System::Windows::Forms::Panel^  maxColor2;


	private: System::Windows::Forms::Panel^  minColor2;
	private: System::Windows::Forms::ListBox^  listBox;




	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->minColor1 = (gcnew System::Windows::Forms::Panel());
			this->maxColor1 = (gcnew System::Windows::Forms::Panel());
			this->maxDb1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->minDb1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->cbTrans1 = (gcnew System::Windows::Forms::ComboBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->cbSounder = (gcnew System::Windows::Forms::ComboBox());
			this->btnRemove = (gcnew System::Windows::Forms::Button());
			this->btnAdd = (gcnew System::Windows::Forms::Button());
			this->panel = (gcnew System::Windows::Forms::Panel());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->maxColor3 = (gcnew System::Windows::Forms::Panel());
			this->minColor3 = (gcnew System::Windows::Forms::Panel());
			this->maxDb3 = (gcnew System::Windows::Forms::NumericUpDown());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->minDb3 = (gcnew System::Windows::Forms::NumericUpDown());
			this->cbTrans3 = (gcnew System::Windows::Forms::ComboBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->maxColor2 = (gcnew System::Windows::Forms::Panel());
			this->minColor2 = (gcnew System::Windows::Forms::Panel());
			this->maxDb2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->minDb2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->cbTrans2 = (gcnew System::Windows::Forms::ComboBox());
			this->listBox = (gcnew System::Windows::Forms::ListBox());
			this->groupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->maxDb1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->minDb1))->BeginInit();
			this->panel->SuspendLayout();
			this->groupBox3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->maxDb3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->minDb3))->BeginInit();
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->maxDb2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->minDb2))->BeginInit();
			this->SuspendLayout();
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->minColor1);
			this->groupBox2->Controls->Add(this->maxColor1);
			this->groupBox2->Controls->Add(this->maxDb1);
			this->groupBox2->Controls->Add(this->label4);
			this->groupBox2->Controls->Add(this->label3);
			this->groupBox2->Controls->Add(this->label2);
			this->groupBox2->Controls->Add(this->minDb1);
			this->groupBox2->Controls->Add(this->cbTrans1);
			this->groupBox2->Location = System::Drawing::Point(13, 35);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(321, 75);
			this->groupBox2->TabIndex = 4;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Red";
			// 
			// minColor1
			// 
			this->minColor1->BackColor = System::Drawing::Color::Black;
			this->minColor1->Location = System::Drawing::Point(131, 45);
			this->minColor1->Name = L"minColor1";
			this->minColor1->Size = System::Drawing::Size(20, 20);
			this->minColor1->TabIndex = 7;
			// 
			// maxColor1
			// 
			this->maxColor1->BackColor = System::Drawing::Color::Red;
			this->maxColor1->Location = System::Drawing::Point(290, 45);
			this->maxColor1->Name = L"maxColor1";
			this->maxColor1->Size = System::Drawing::Size(20, 20);
			this->maxColor1->TabIndex = 6;
			// 
			// maxDb1
			// 
			this->maxDb1->Location = System::Drawing::Point(230, 45);
			this->maxDb1->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, 0 });
			this->maxDb1->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, System::Int32::MinValue });
			this->maxDb1->Name = L"maxDb1";
			this->maxDb1->Size = System::Drawing::Size(54, 20);
			this->maxDb1->TabIndex = 2;
			this->maxDb1->ValueChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::maxDb1_ValueChanged);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(180, 47);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(44, 13);
			this->label4->TabIndex = 5;
			this->label4->Text = L"Max Db";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 21);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(61, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"Transducer";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(9, 47);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(41, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Min Db";
			// 
			// minDb1
			// 
			this->minDb1->Location = System::Drawing::Point(74, 45);
			this->minDb1->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, 0 });
			this->minDb1->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, System::Int32::MinValue });
			this->minDb1->Name = L"minDb1";
			this->minDb1->Size = System::Drawing::Size(51, 20);
			this->minDb1->TabIndex = 1;
			this->minDb1->ValueChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::minDb1_ValueChanged);
			// 
			// cbTrans1
			// 
			this->cbTrans1->FormattingEnabled = true;
			this->cbTrans1->Location = System::Drawing::Point(73, 18);
			this->cbTrans1->Name = L"cbTrans1";
			this->cbTrans1->Size = System::Drawing::Size(121, 21);
			this->cbTrans1->TabIndex = 0;
			this->cbTrans1->SelectedIndexChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::cbTrans1_SelectedIndexChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(19, 11);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(47, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Sounder";
			// 
			// cbSounder
			// 
			this->cbSounder->FormattingEnabled = true;
			this->cbSounder->Location = System::Drawing::Point(83, 11);
			this->cbSounder->Name = L"cbSounder";
			this->cbSounder->Size = System::Drawing::Size(240, 21);
			this->cbSounder->TabIndex = 0;
			this->cbSounder->SelectedIndexChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::cbSounder_SelectedIndexChanged);
			// 
			// btnRemove
			// 
			this->btnRemove->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->btnRemove->Location = System::Drawing::Point(218, 264);
			this->btnRemove->Name = L"btnRemove";
			this->btnRemove->Size = System::Drawing::Size(26, 23);
			this->btnRemove->TabIndex = 4;
			this->btnRemove->Text = L"-";
			this->btnRemove->UseVisualStyleBackColor = true;
			this->btnRemove->Click += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::btnRemove_Click);
			// 
			// btnAdd
			// 
			this->btnAdd->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->btnAdd->Location = System::Drawing::Point(186, 264);
			this->btnAdd->Name = L"btnAdd";
			this->btnAdd->Size = System::Drawing::Size(26, 23);
			this->btnAdd->TabIndex = 5;
			this->btnAdd->Text = L"+";
			this->btnAdd->UseVisualStyleBackColor = true;
			this->btnAdd->Click += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::btnAdd_Click);
			// 
			// panel
			// 
			this->panel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->panel->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->panel->Controls->Add(this->groupBox3);
			this->panel->Controls->Add(this->groupBox1);
			this->panel->Controls->Add(this->groupBox2);
			this->panel->Controls->Add(this->cbSounder);
			this->panel->Controls->Add(this->label1);
			this->panel->Enabled = false;
			this->panel->Location = System::Drawing::Point(251, 3);
			this->panel->Name = L"panel";
			this->panel->Size = System::Drawing::Size(346, 284);
			this->panel->TabIndex = 6;
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->maxColor3);
			this->groupBox3->Controls->Add(this->minColor3);
			this->groupBox3->Controls->Add(this->maxDb3);
			this->groupBox3->Controls->Add(this->label8);
			this->groupBox3->Controls->Add(this->label9);
			this->groupBox3->Controls->Add(this->label10);
			this->groupBox3->Controls->Add(this->minDb3);
			this->groupBox3->Controls->Add(this->cbTrans3);
			this->groupBox3->Location = System::Drawing::Point(13, 197);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(321, 75);
			this->groupBox3->TabIndex = 6;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"Blue";
			// 
			// maxColor3
			// 
			this->maxColor3->BackColor = System::Drawing::Color::Blue;
			this->maxColor3->Location = System::Drawing::Point(290, 45);
			this->maxColor3->Name = L"maxColor3";
			this->maxColor3->Size = System::Drawing::Size(20, 20);
			this->maxColor3->TabIndex = 7;
			// 
			// minColor3
			// 
			this->minColor3->BackColor = System::Drawing::Color::Black;
			this->minColor3->Location = System::Drawing::Point(131, 45);
			this->minColor3->Name = L"minColor3";
			this->minColor3->Size = System::Drawing::Size(20, 20);
			this->minColor3->TabIndex = 8;
			// 
			// maxDb3
			// 
			this->maxDb3->Location = System::Drawing::Point(230, 45);
			this->maxDb3->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, 0 });
			this->maxDb3->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, System::Int32::MinValue });
			this->maxDb3->Name = L"maxDb3";
			this->maxDb3->Size = System::Drawing::Size(54, 20);
			this->maxDb3->TabIndex = 2;
			this->maxDb3->ValueChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::maxDb3_ValueChanged);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(180, 47);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(44, 13);
			this->label8->TabIndex = 5;
			this->label8->Text = L"Max Db";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(9, 21);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(61, 13);
			this->label9->TabIndex = 4;
			this->label9->Text = L"Transducer";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(9, 47);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(41, 13);
			this->label10->TabIndex = 3;
			this->label10->Text = L"Min Db";
			// 
			// minDb3
			// 
			this->minDb3->Location = System::Drawing::Point(73, 45);
			this->minDb3->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, 0 });
			this->minDb3->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, System::Int32::MinValue });
			this->minDb3->Name = L"minDb3";
			this->minDb3->Size = System::Drawing::Size(51, 20);
			this->minDb3->TabIndex = 1;
			this->minDb3->ValueChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::minDb3_ValueChanged);
			// 
			// cbTrans3
			// 
			this->cbTrans3->FormattingEnabled = true;
			this->cbTrans3->Location = System::Drawing::Point(73, 18);
			this->cbTrans3->Name = L"cbTrans3";
			this->cbTrans3->Size = System::Drawing::Size(121, 21);
			this->cbTrans3->TabIndex = 0;
			this->cbTrans3->SelectedIndexChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::cbTrans3_SelectedIndexChanged);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->maxColor2);
			this->groupBox1->Controls->Add(this->minColor2);
			this->groupBox1->Controls->Add(this->maxDb2);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label6);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->minDb2);
			this->groupBox1->Controls->Add(this->cbTrans2);
			this->groupBox1->Location = System::Drawing::Point(13, 116);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(321, 75);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Green";
			// 
			// maxColor2
			// 
			this->maxColor2->BackColor = System::Drawing::Color::Green;
			this->maxColor2->Location = System::Drawing::Point(290, 45);
			this->maxColor2->Name = L"maxColor2";
			this->maxColor2->Size = System::Drawing::Size(20, 20);
			this->maxColor2->TabIndex = 9;
			// 
			// minColor2
			// 
			this->minColor2->BackColor = System::Drawing::Color::Black;
			this->minColor2->Location = System::Drawing::Point(131, 45);
			this->minColor2->Name = L"minColor2";
			this->minColor2->Size = System::Drawing::Size(20, 20);
			this->minColor2->TabIndex = 8;
			// 
			// maxDb2
			// 
			this->maxDb2->Location = System::Drawing::Point(230, 45);
			this->maxDb2->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, 0 });
			this->maxDb2->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, System::Int32::MinValue });
			this->maxDb2->Name = L"maxDb2";
			this->maxDb2->Size = System::Drawing::Size(54, 20);
			this->maxDb2->TabIndex = 2;
			this->maxDb2->ValueChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::maxDb2_ValueChanged);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(180, 47);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(44, 13);
			this->label5->TabIndex = 5;
			this->label5->Text = L"Max Db";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(9, 21);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(61, 13);
			this->label6->TabIndex = 4;
			this->label6->Text = L"Transducer";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(9, 47);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(41, 13);
			this->label7->TabIndex = 3;
			this->label7->Text = L"Min Db";
			// 
			// minDb2
			// 
			this->minDb2->Location = System::Drawing::Point(73, 45);
			this->minDb2->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, 0 });
			this->minDb2->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, System::Int32::MinValue });
			this->minDb2->Name = L"minDb2";
			this->minDb2->Size = System::Drawing::Size(51, 20);
			this->minDb2->TabIndex = 1;
			this->minDb2->ValueChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::minDb2_ValueChanged);
			// 
			// cbTrans2
			// 
			this->cbTrans2->FormattingEnabled = true;
			this->cbTrans2->Location = System::Drawing::Point(73, 18);
			this->cbTrans2->Name = L"cbTrans2";
			this->cbTrans2->Size = System::Drawing::Size(121, 21);
			this->cbTrans2->TabIndex = 0;
			this->cbTrans2->SelectedIndexChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::cbTrans2_SelectedIndexChanged);
			// 
			// listBox
			// 
			this->listBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->listBox->FormattingEnabled = true;
			this->listBox->Location = System::Drawing::Point(3, 3);
			this->listBox->Name = L"listBox";
			this->listBox->Size = System::Drawing::Size(241, 251);
			this->listBox->TabIndex = 7;
			this->listBox->SelectedIndexChanged += gcnew System::EventHandler(this, &MultifrequencyEchogramsControl::listBox_SelectedIndexChanged);
			// 
			// MultifrequencyEchogramsControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->listBox);
			this->Controls->Add(this->panel);
			this->Controls->Add(this->btnAdd);
			this->Controls->Add(this->btnRemove);
			this->Name = L"MultifrequencyEchogramsControl";
			this->Size = System::Drawing::Size(602, 293);
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->maxDb1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->minDb1))->EndInit();
			this->panel->ResumeLayout(false);
			this->panel->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->maxDb3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->minDb3))->EndInit();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->maxDb2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->minDb2))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

#pragma region Implémentation de IParamControl
		public: virtual void UpdateConfiguration();
		public: virtual void UpdateGUI();
#pragma endregion
		private:
			System::Collections::Generic::List<int> m_sounderIndexList;
			std::vector<MultifrequencyEchogram>* m_echograms = nullptr;
			bool changeInprogress = false;

			void populateSounders();
			void populateTransducers(Sounder* sounder);

			void addEchogram();
			void display(const MultifrequencyEchogram& echogram);
			void updateCurrentEchogram();
			void update(MultifrequencyEchogram& echogram);
			void setEchogramEditionEnabled(bool value);

			void setEchograms(const std::vector<MultifrequencyEchogram>& echograms);

			System::Void cbSounder_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
			System::Void listBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
			System::Void btnAdd_Click(System::Object^  sender, System::EventArgs^  e);
			System::Void btnRemove_Click(System::Object^  sender, System::EventArgs^  e);

			System::Void cbTrans1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) { updateCurrentEchogram(); }
			System::Void minDb1_ValueChanged(System::Object^  sender, System::EventArgs^  e) { updateCurrentEchogram(); }
			System::Void maxDb1_ValueChanged(System::Object^  sender, System::EventArgs^  e) { updateCurrentEchogram(); }
			System::Void minDb2_ValueChanged(System::Object^  sender, System::EventArgs^  e) { updateCurrentEchogram(); }
			System::Void maxDb2_ValueChanged(System::Object^  sender, System::EventArgs^  e) { updateCurrentEchogram(); }
			System::Void cbTrans2_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) { updateCurrentEchogram(); }
			System::Void cbTrans3_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) { updateCurrentEchogram(); }
			System::Void minDb3_ValueChanged(System::Object^  sender, System::EventArgs^  e) { updateCurrentEchogram(); }
			System::Void maxDb3_ValueChanged(System::Object^  sender, System::EventArgs^  e) { updateCurrentEchogram(); }
};
}
