#include "MultifrequencyEchogramsParameter.h"
#include "M3DKernel/config/MovConfig.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"

#include <sstream>

using namespace BaseKernel;

MultifrequencyEchogramsParameter* MultifrequencyEchogramsParameter::m_instance = 0;


MultifrequencyEchogramsParameter::MultifrequencyEchogramsParameter() : ParameterModule("MultifrequencyEchogramsParameter")
{
}

MultifrequencyEchogramsParameter* MultifrequencyEchogramsParameter::getInstance()
{
	if (!m_instance)
		m_instance = new MultifrequencyEchogramsParameter;
	return m_instance;
}

bool MultifrequencyEchogramsParameter::Serialize(BaseKernel::MovConfig * movConfig)
{
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	movConfig->SerializeData<unsigned int>(m_echograms.size(), eUInt, "EchogramCount");

	int i = 0;
	for (auto e : m_echograms)
	{	
		std::stringstream ss;
		ss << "Echogram_" << i;
		movConfig->SerializeData((ParameterObject*)this, eParameterObject, ss.str().c_str());

		movConfig->SerializeData<std::uint32_t>(e.getSounderId(), eUInt, "SounderId");

		movConfig->SerializeData<std::string>(e.getTransducerRed(), eString, "RedTransducer");
		movConfig->SerializeData<std::string>(e.getTransducerGreen(), eString, "GreenTransducer");
		movConfig->SerializeData<std::string>(e.getTransducerBlue(), eString, "BlueTransducer");

		movConfig->SerializeData(e.getMinValueRed(), eInt, "RedMinValue");
		movConfig->SerializeData(e.getMaxValueRed(), eInt, "RedMaxValue");
		movConfig->SerializeData(e.getMinValueGreen(), eInt, "GreenMinValue");
		movConfig->SerializeData(e.getMaxValueGreen(), eInt, "GreenMaxValue");
		movConfig->SerializeData(e.getMinValueBlue(), eInt, "BlueMinValue");
		movConfig->SerializeData(e.getMaxValueBlue(), eInt, "BlueMaxValue");

		++i;

		movConfig->SerializePushBack();
	}

	movConfig->SerializePushBack();

	return false;
}

bool MultifrequencyEchogramsParameter::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	m_echograms.clear();

	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	if (result)
	{
		unsigned int count = 0;
		if (movConfig->DeSerializeData<unsigned int>(&count, eUInt, "EchogramCount"))
		{
			for (int i = 0; i < count; ++i)
			{
				std::stringstream ss;
				ss << "Echogram_" << i;
				if (movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, ss.str().c_str()))
				{
					std::uint32_t sounderId;
					movConfig->DeSerializeData<std::uint32_t>(&sounderId, eUInt, "SounderId");

					std::string trName1, trName2, trName3;
					movConfig->DeSerializeData<std::string>(&trName1, eString, "RedTransducer");
					movConfig->DeSerializeData<std::string>(&trName2, eString, "GreenTransducer");
					movConfig->DeSerializeData<std::string>(&trName3, eString, "BlueTransducer");

					int minRed, maxRed, minGreen, maxGreen, minBlue, maxBlue;
					movConfig->DeSerializeData(&minRed, eInt, "RedMinValue");
					movConfig->DeSerializeData(&maxRed, eInt, "RedMaxValue");
					movConfig->DeSerializeData(&minGreen, eInt, "GreenMinValue");
					movConfig->DeSerializeData(&maxGreen, eInt, "GreenMaxValue");
					movConfig->DeSerializeData(&minBlue, eInt, "BlueMinValue");
					movConfig->DeSerializeData(&maxBlue, eInt, "BlueMaxValue");


					MultifrequencyEchogram echogram;
					echogram.setSounderId(sounderId);
					echogram.setTransducerRed(trName1);
					echogram.setTransducerGreen(trName2);
					echogram.setTransducerBlue(trName3);
					echogram.setMinMaxValueRed(minRed, maxRed);
					echogram.setMinMaxValueGreen(minGreen, maxGreen);
					echogram.setMinMaxValueBlue(minBlue, maxBlue);

					m_echograms.push_back(echogram);

					movConfig->DeSerializePushBack();
				}
			}
		}
	}

	movConfig->DeSerializePushBack();

	return true;
}

std::vector<MultifrequencyEchogram> MultifrequencyEchogramsParameter::getEchograms() const
{
	return m_echograms;
}

void MultifrequencyEchogramsParameter::setEchograms(const std::vector<MultifrequencyEchogram>& echograms)
{
	m_echograms = echograms;
}

bool MultifrequencyEchogramsParameter::hasCompatibleSounders() const
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	for (unsigned int sound = 0; sound < pKernel->getObjectMgr()->GetSounderDefinition().GetNbSounder(); sound++)
	{
		Sounder *sounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(sound);

		if (sounder->GetTransducerCount() >= 3)
		{
			return true;
		}
	}

	return false;
}
