#pragma once

#include "M3DKernel/parameter/ParameterModule.h"
#include "MultifrequencyEchogram.h"

class MultifrequencyEchogramsParameter : public BaseKernel::ParameterModule
{
public:
	static MultifrequencyEchogramsParameter* getInstance();

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig) override;
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig) override;

	std::vector<MultifrequencyEchogram> getEchograms() const;
	void setEchograms(const std::vector<MultifrequencyEchogram>& echograms);

	bool hasCompatibleSounders() const;

private:
	MultifrequencyEchogramsParameter();
	~MultifrequencyEchogramsParameter() {}

	std::vector<MultifrequencyEchogram> m_echograms;
	static MultifrequencyEchogramsParameter* m_instance;
};

