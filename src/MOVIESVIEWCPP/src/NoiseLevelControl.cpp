#include "NoiseLevelControl.h"
#include "ModuleManager/ModuleManager.h"
#include "NoiseLevel/NoiseLevelModule.h"

#include "utils.h"

using namespace MOVIESVIEWCPP;

void NoiseLevelControl::UpdateConfiguration()
{
	auto& noiseParameters = CModuleManager::getInstance()->GetNoiseLevelModule()->GetNoiseLevelParameters();
	noiseParameters.setEnabled(checkEnableNoiseLevel->Checked);
	noiseParameters.setRange(Decimal::ToInt32(udMinRange->Value), Decimal::ToInt32(udMaxRange->Value));
	noiseParameters.setSvThreshold(Decimal::ToDouble(udThreshold->Value));
	noiseParameters.setReferenceValuesFilePath(netStr2CppStr(textBoxReferenceFilePath->Text));
}

void NoiseLevelControl::UpdateGUI()
{
	const auto& noiseParameters = CModuleManager::getInstance()->GetNoiseLevelModule()->GetNoiseLevelParameters();
	checkEnableNoiseLevel->Checked = noiseParameters.isEnabled();
	udMinRange->Value = noiseParameters.getMinRange();
	udMaxRange->Value = noiseParameters.getMaxRange();
	udThreshold->Value = Decimal(noiseParameters.getSvThreshold());
	textBoxReferenceFilePath->Text = cppStr2NetStr(noiseParameters.getReferenceValuesFilePath().c_str());
}

void NoiseLevelControl::buttonSelectReferenceFilePath_Click(System::Object^  sender, System::EventArgs^  e)
{
	System::Windows::Forms::OpenFileDialog ^ openFileDialog = gcnew System::Windows::Forms::OpenFileDialog();
	openFileDialog->Filter = L"Xml Files|*.xml";
	openFileDialog->Title = L"Select a reference file";
	openFileDialog->FileName = textBoxReferenceFilePath->Text;
	
	auto result = openFileDialog->ShowDialog();
	if (result == DialogResult::OK)
	{
		textBoxReferenceFilePath->Text = openFileDialog->FileName;
	}
}