#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"
namespace MOVIESVIEWCPP {

	/// <summary>
	/// Summary for NoiseLevelControl
	/// </summary>
	public ref class NoiseLevelControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		NoiseLevelControl(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~NoiseLevelControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckBox^  checkEnableNoiseLevel;
	protected:
	private: System::Windows::Forms::Label^  minimumRangeLabel;
	private: System::Windows::Forms::Label^  maximumRangeLabel;

	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::NumericUpDown^  udMinRange;
	private: System::Windows::Forms::NumericUpDown^  udMaxRange;

	private: System::Windows::Forms::Label^  noiseLevelLabel;
	private: System::Windows::Forms::Label^  svThresholLabel;
	private: System::Windows::Forms::NumericUpDown^  udThreshold;
	private: System::Windows::Forms::Button^  buttonSelectReferenceFilePath;
	private: System::Windows::Forms::TextBox^  textBoxReferenceFilePath;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->checkEnableNoiseLevel = (gcnew System::Windows::Forms::CheckBox());
			this->minimumRangeLabel = (gcnew System::Windows::Forms::Label());
			this->maximumRangeLabel = (gcnew System::Windows::Forms::Label());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->udMinRange = (gcnew System::Windows::Forms::NumericUpDown());
			this->udMaxRange = (gcnew System::Windows::Forms::NumericUpDown());
			this->noiseLevelLabel = (gcnew System::Windows::Forms::Label());
			this->svThresholLabel = (gcnew System::Windows::Forms::Label());
			this->udThreshold = (gcnew System::Windows::Forms::NumericUpDown());
			this->buttonSelectReferenceFilePath = (gcnew System::Windows::Forms::Button());
			this->textBoxReferenceFilePath = (gcnew System::Windows::Forms::TextBox());
			this->tableLayoutPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udMinRange))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udMaxRange))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udThreshold))->BeginInit();
			this->SuspendLayout();
			// 
			// checkEnableNoiseLevel
			// 
			this->checkEnableNoiseLevel->AutoSize = true;
			this->checkEnableNoiseLevel->Location = System::Drawing::Point(6, 5);
			this->checkEnableNoiseLevel->Name = L"checkEnableNoiseLevel";
			this->checkEnableNoiseLevel->Size = System::Drawing::Size(118, 17);
			this->checkEnableNoiseLevel->TabIndex = 14;
			this->checkEnableNoiseLevel->Text = L"Enable Noise Level";
			this->checkEnableNoiseLevel->UseVisualStyleBackColor = true;
			// 
			// minimumRangeLabel
			// 
			this->minimumRangeLabel->AutoSize = true;
			this->minimumRangeLabel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->minimumRangeLabel->Location = System::Drawing::Point(3, 0);
			this->minimumRangeLabel->Name = L"minimumRangeLabel";
			this->minimumRangeLabel->Size = System::Drawing::Size(94, 33);
			this->minimumRangeLabel->TabIndex = 2;
			this->minimumRangeLabel->Text = L"Minimum Range";
			// 
			// maximumRangeLabel
			// 
			this->maximumRangeLabel->AutoSize = true;
			this->maximumRangeLabel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->maximumRangeLabel->Location = System::Drawing::Point(3, 33);
			this->maximumRangeLabel->Name = L"maximumRangeLabel";
			this->maximumRangeLabel->Size = System::Drawing::Size(94, 33);
			this->maximumRangeLabel->TabIndex = 1;
			this->maximumRangeLabel->Text = L"Maximum Range";
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->tableLayoutPanel1->ColumnCount = 3;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				100)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel1->Controls->Add(this->minimumRangeLabel, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->maximumRangeLabel, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->udMinRange, 1, 0);
			this->tableLayoutPanel1->Controls->Add(this->udMaxRange, 1, 1);
			this->tableLayoutPanel1->Controls->Add(this->noiseLevelLabel, 0, 3);
			this->tableLayoutPanel1->Controls->Add(this->svThresholLabel, 0, 2);
			this->tableLayoutPanel1->Controls->Add(this->udThreshold, 1, 2);
			this->tableLayoutPanel1->Controls->Add(this->buttonSelectReferenceFilePath, 2, 3);
			this->tableLayoutPanel1->Controls->Add(this->textBoxReferenceFilePath, 1, 3);
			this->tableLayoutPanel1->Location = System::Drawing::Point(6, 26);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 4;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 33)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 33)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 33)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(391, 223);
			this->tableLayoutPanel1->TabIndex = 11;
			// 
			// udMinRange
			// 
			this->udMinRange->Location = System::Drawing::Point(103, 3);
			this->udMinRange->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 999, 0, 0, 0 });
			this->udMinRange->Name = L"udMinRange";
			this->udMinRange->Size = System::Drawing::Size(120, 20);
			this->udMinRange->TabIndex = 4;
			// 
			// udMaxRange
			// 
			this->udMaxRange->Location = System::Drawing::Point(103, 36);
			this->udMaxRange->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 999, 0, 0, 0 });
			this->udMaxRange->Name = L"udMaxRange";
			this->udMaxRange->Size = System::Drawing::Size(120, 20);
			this->udMaxRange->TabIndex = 5;
			// 
			// noiseLevelLabel
			// 
			this->noiseLevelLabel->AutoSize = true;
			this->noiseLevelLabel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->noiseLevelLabel->Location = System::Drawing::Point(3, 99);
			this->noiseLevelLabel->Name = L"noiseLevelLabel";
			this->noiseLevelLabel->Size = System::Drawing::Size(94, 124);
			this->noiseLevelLabel->TabIndex = 3;
			this->noiseLevelLabel->Text = L"Reference noise level";
			// 
			// svThresholLabel
			// 
			this->svThresholLabel->AutoSize = true;
			this->svThresholLabel->Location = System::Drawing::Point(3, 66);
			this->svThresholLabel->Name = L"svThresholLabel";
			this->svThresholLabel->Size = System::Drawing::Size(70, 13);
			this->svThresholLabel->TabIndex = 7;
			this->svThresholLabel->Text = L"Sv Threshold";
			// 
			// udThreshold
			// 
			this->udThreshold->Location = System::Drawing::Point(103, 69);
			this->udThreshold->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 999, 0, 0, 0 });
			this->udThreshold->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 999, 0, 0, System::Int32::MinValue });
			this->udThreshold->Name = L"udThreshold";
			this->udThreshold->Size = System::Drawing::Size(120, 20);
			this->udThreshold->TabIndex = 8;
			// 
			// buttonSelectReferenceFilePath
			// 
			this->buttonSelectReferenceFilePath->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->buttonSelectReferenceFilePath->Location = System::Drawing::Point(363, 102);
			this->buttonSelectReferenceFilePath->Name = L"buttonSelectReferenceFilePath";
			this->buttonSelectReferenceFilePath->Size = System::Drawing::Size(25, 20);
			this->buttonSelectReferenceFilePath->TabIndex = 9;
			this->buttonSelectReferenceFilePath->Text = L"...";
			this->buttonSelectReferenceFilePath->UseVisualStyleBackColor = true;
			this->buttonSelectReferenceFilePath->Click += gcnew System::EventHandler(this, &NoiseLevelControl::buttonSelectReferenceFilePath_Click);
			// 
			// textBoxReferenceFilePath
			// 
			this->textBoxReferenceFilePath->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBoxReferenceFilePath->Location = System::Drawing::Point(103, 102);
			this->textBoxReferenceFilePath->Name = L"textBoxReferenceFilePath";
			this->textBoxReferenceFilePath->Size = System::Drawing::Size(254, 20);
			this->textBoxReferenceFilePath->TabIndex = 10;
			// 
			// NoiseLevelControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->checkEnableNoiseLevel);
			this->Controls->Add(this->tableLayoutPanel1);
			this->Name = L"NoiseLevelControl";
			this->Size = System::Drawing::Size(400, 252);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udMinRange))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udMaxRange))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udThreshold))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

#pragma region Implémentation de IParamControl
		public: virtual void UpdateConfiguration();
		public: virtual void UpdateGUI();
#pragma endregion

	private: System::Void buttonSelectReferenceFilePath_Click(System::Object^  sender, System::EventArgs^  e);
};
}
