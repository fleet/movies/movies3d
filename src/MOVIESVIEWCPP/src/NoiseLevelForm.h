#pragma once

#include "ParameterForm.h"
#include "NoiseLevelControl.h"

namespace MOVIESVIEWCPP 
{
	public ref class NoiseLevelForm : public ParameterForm
	{
	public:
		NoiseLevelForm() :	ParameterForm()
		{
			SetParamControl(gcnew NoiseLevelControl(), L"Noise level");
			FormBorderStyle = ::FormBorderStyle::Sizable;
		}
	};
};
