#include "NoiseLevelGraphControl.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "DisplayParameter.h"

#include "ModuleManager/ModuleManager.h"
#include "NoiseLevel/NoiseLevelModule.h"
#include "NoiseLevel/NoiseLevelParameters.h"

#include "utils.h"

using namespace MOVIESVIEWCPP;
using namespace System::Windows::Forms::DataVisualization::Charting;

NoiseLevelGraphControl::NoiseLevelGraphControl()
{
	InitializeComponent();

	Text = "Noise Level (Passive Pings)";

	chartInteractor = gcnew ChartInteractor();
	chartInteractor->Install(chart);
}

void NoiseLevelGraphControl::refreshForCurrentESU()
{
	M3DKernel * pKernel = M3DKernel::GetInstance();
	pKernel->Lock();

	ESUParameter* esuToDisplay = nullptr;
	if (m_cursorOffset > 0)
	{
		TimeObjectContainer *pingFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(m_cursorSounderId);
		int pingIndex = std::max<int>(0, pingFanContainer->GetObjectCount() - 1 - m_cursorOffset);
		auto pingFan = (PingFan *)pingFanContainer->GetDatedObject(pingIndex);
		if (pingFan)
			esuToDisplay = pKernel->getMovESUManager()->GetESUContainer().GetESUWithPing(pingFan->GetPingId());
	}

	if (!esuToDisplay)
	{
		esuToDisplay = pKernel->getMovESUManager()->GetWorkingESU();
	}

	pKernel->Unlock();

	if (esuToDisplay)
		refreshDataForESU(esuToDisplay);

	checkBoxLogScale->Checked = DisplayParameter::getInstance()->GetNoiseLevelLogScale();
	setLogScale(checkBoxLogScale->Checked);
}

void NoiseLevelGraphControl::setCursorOffset(int sounderId, int offset)
{
	m_cursorSounderId = sounderId;
	m_cursorOffset = offset;
	refreshForCurrentESU();
}

void NoiseLevelGraphControl::refreshDataForESU(ESUParameter* esu)
{
	M3DKernel * pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	
	m_currentESU = esu;
	ESUParameter * esuToDisplay = m_currentESU;
	if (esu == pKernel->getMovESUManager()->GetWorkingESU())
	{
		esuToDisplay = pKernel->getMovESUManager()->GetMostRecentClosedESU();
		lblPings->Text = String::Format("Current ESU: from ping {1}"
			, esu->GetESUId(), esu->GetESUPingNumber());
	}
	else
	{
		lblPings->Text = String::Format("ESU {0}: from ping {1} to ping {2}"
			, esu->GetESUId(), esu->GetESUPingNumber(), esu->GetESULastPingNumber());
	}
	chart->Series->Clear();

	if (esuToDisplay)
	{
		auto noiseLevelModule = CModuleManager::getInstance()->GetNoiseLevelModule();
		const auto result = noiseLevelModule->getResults(esuToDisplay);

		for (auto itSounder : result)
		{
			// noise serie
			const auto & noisePoints = itSounder.second.noiseLevels;
			if (noisePoints.size() > 0)
			{
				auto serie = gcnew Series("Sounder Id[" + itSounder.first + "]");
				serie->ChartType = SeriesChartType::Point;
				serie->BorderWidth = 3;
				for (auto itPoint : noisePoints)
				{
					serie->Points->AddXY(itPoint.first * 1e-3, itPoint.second);
				}
				chart->Series->Add(serie);
			}

			// ref noise serie
			const auto & refNoisePoints = itSounder.second.refNoiseLevels;
			if (refNoisePoints.size() > 0)
			{
				auto serie = gcnew Series("MovRef Sounder Id[" + itSounder.first + "]");
				serie->ChartType = SeriesChartType::Line;
				for (auto itPoint : refNoisePoints)
				{
					serie->Points->AddXY(itPoint.first * 1e-3, itPoint.second);
				}
				chart->Series->Add(serie);
			}
		}
	}

	pKernel->Unlock();

	// Ajout des modeles de Knudesen
	if (m_knudsenSeries.Count == 0)
		addKnudsenSeries();
	
	for each(auto s in m_knudsenSeries)
	{
		chart->Series->Add(s);
	}
	
	chart->ChartAreas[0]->RecalculateAxesScale();
}

void NoiseLevelGraphControl::addKnudsenSeries()
{
	m_knudsenSeries.Clear();

	std::vector<double> ss = { 0, 0.5, 1, 2 ,3 ,4 ,5, 6 }; //State of the sea: 0 = doesn't move, 6 = force 6
	std::vector<double> B0 = { 44.5, 50, 55, 61.5, 64.5, 66.5, 68.5, 70 };
	assert(ss.size() == B0.size());

	for (int iss = 0; iss < ss.size(); ++iss)
	{
		Series^ ssSeries = gcnew Series("SS" + ss[iss]);
		ssSeries->ChartType = SeriesChartType::Line;
		m_knudsenSeries.Add(ssSeries);

		for (auto f = 10000; f < 200000; f += 10000)
		{
			auto br_kn = B0[iss] - 17 * log10(f / 1000.0);
			ssSeries->Points->AddXY(f / 1000.0, br_kn);
		}
	}

	Series^ thSeries = gcnew Series("Thermal Noise");
	thSeries->ChartType = SeriesChartType::Line;
	m_knudsenSeries.Add(thSeries);

	for (auto f = 100000; f < 400000; f += 10000)
	{
		auto br_th = 25 + 20 * log10(f / 100000.0);
		thSeries->Points->AddXY(f / 1000.0, br_th);
	}
}

void NoiseLevelGraphControl::setLogScale(bool value)
{
	chart->ChartAreas[0]->AxisX->IsLogarithmic = value && m_currentESU;
	chart->ChartAreas[0]->AxisX->Minimum = 10;
	chart->ChartAreas[0]->AxisX->Maximum = 400;

	chart->ChartAreas[0]->AxisX->MinorGrid->Enabled = value && m_currentESU;
}

void NoiseLevelGraphControl::checkBoxLogScale_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	setLogScale(checkBoxLogScale->Checked);
	DisplayParameter::getInstance()->SetNoiseLevelLogScale(checkBoxLogScale->Checked);
}