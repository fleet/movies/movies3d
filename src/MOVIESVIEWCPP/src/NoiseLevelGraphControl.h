#pragma once

#include "Chart/ChartInteractor.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

class PingFan;
class ESUParameter;

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Summary for NoiseLevelGraphControl
	/// </summary>
	public ref class NoiseLevelGraphControl : public System::Windows::Forms::UserControl
	{
	public:
		NoiseLevelGraphControl(void);

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~NoiseLevelGraphControl()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chart;
	private: System::Windows::Forms::Label^  lblPings;
	private: System::Windows::Forms::CheckBox^  checkBoxLogScale;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Legend^  legend1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
			this->chart = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->lblPings = (gcnew System::Windows::Forms::Label());
			this->checkBoxLogScale = (gcnew System::Windows::Forms::CheckBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart))->BeginInit();
			this->SuspendLayout();
			// 
			// chart
			// 
			this->chart->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			chartArea1->AxisX->IsStartedFromZero = false;
			chartArea1->AxisX->MajorGrid->LineColor = System::Drawing::Color::WhiteSmoke;
			chartArea1->AxisX->MajorTickMark->Interval = 0;
			chartArea1->AxisX->MajorTickMark->IntervalType = System::Windows::Forms::DataVisualization::Charting::DateTimeIntervalType::Number;
			chartArea1->AxisX->MinorGrid->Enabled = true;
			chartArea1->AxisX->MinorGrid->Interval = 1;
			chartArea1->AxisX->MinorGrid->LineColor = System::Drawing::Color::WhiteSmoke;
			chartArea1->AxisX->Title = L"Frequency (kHz)";
			chartArea1->AxisY->MajorGrid->LineColor = System::Drawing::Color::WhiteSmoke;
			chartArea1->AxisY->MinorGrid->LineColor = System::Drawing::Color::LightGray;
			chartArea1->AxisY->Title = L"Noise (dB)";
			chartArea1->Name = L"ChartArea1";
			this->chart->ChartAreas->Add(chartArea1);
			legend1->Name = L"Legend1";
			this->chart->Legends->Add(legend1);
			this->chart->Location = System::Drawing::Point(0, 21);
			this->chart->Name = L"chart";
			this->chart->Size = System::Drawing::Size(417, 204);
			this->chart->TabIndex = 0;
			this->chart->Text = L"chart";
			// 
			// lblPings
			// 
			this->lblPings->AutoSize = true;
			this->lblPings->Location = System::Drawing::Point(2, 3);
			this->lblPings->Name = L"lblPings";
			this->lblPings->Size = System::Drawing::Size(32, 13);
			this->lblPings->TabIndex = 2;
			this->lblPings->Text = L"pings";
			// 
			// checkBoxLogScale
			// 
			this->checkBoxLogScale->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->checkBoxLogScale->AutoSize = true;
			this->checkBoxLogScale->Checked = true;
			this->checkBoxLogScale->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBoxLogScale->Location = System::Drawing::Point(312, 2);
			this->checkBoxLogScale->Name = L"checkBoxLogScale";
			this->checkBoxLogScale->Size = System::Drawing::Size(102, 17);
			this->checkBoxLogScale->TabIndex = 3;
			this->checkBoxLogScale->Text = L"Logarithm Scale";
			this->checkBoxLogScale->UseVisualStyleBackColor = true;
			this->checkBoxLogScale->CheckedChanged += gcnew System::EventHandler(this, &NoiseLevelGraphControl::checkBoxLogScale_CheckedChanged);
			// 
			// NoiseLevelGraphControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->checkBoxLogScale);
			this->Controls->Add(this->lblPings);
			this->Controls->Add(this->chart);
			this->Name = L"NoiseLevelGraphControl";
			this->Size = System::Drawing::Size(417, 225);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	public:
		void refreshForCurrentESU();
		void setCursorOffset(int sounderId, int offset);
		void refreshDataForESU(ESUParameter* esu);

	private:
		int m_cursorSounderId = 0;
		int m_cursorOffset = 0;
		ESUParameter* m_currentESU = nullptr;
		System::Collections::Generic::List<System::Windows::Forms::DataVisualization::Charting::Series^> m_knudsenSeries;

		ChartInteractor ^chartInteractor;

		void addKnudsenSeries();
		void setLogScale(bool value);
		
		private: void checkBoxLogScale_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	};
}
