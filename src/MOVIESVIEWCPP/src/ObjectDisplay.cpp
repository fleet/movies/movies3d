#include "ObjectDisplay.h"

CObjectDisplay::CObjectDisplay(System::Windows::Forms::ListView ^refListView) : listViewDesc(refListView)
{
	m_defaultColor = System::Drawing::Color::Black;
}

System::Void CObjectDisplay::AddInt64(std::int64_t data, System::String^ dataName)
{
	ListViewItem ^LogItem;
	LogItem = gcnew ListViewItem(dataName);
	LogItem->ForeColor = m_defaultColor;
	LogItem->SubItems->Add(System::Int64(data).ToString());
	this->listViewDesc->Items->Insert(this->listViewDesc->Items->Count, LogItem);
}

System::Void CObjectDisplay::AddString(char *data, System::String ^dataName)
{
	ListViewItem ^LogItem;
	LogItem = gcnew ListViewItem(dataName);
	LogItem->ForeColor = m_defaultColor;
	LogItem->SubItems->Add(System::String(data).ToString());
	this->listViewDesc->Items->Insert(this->listViewDesc->Items->Count, LogItem);
}

System::Void CObjectDisplay::AddInt32(int data, System::String ^dataName)
{
	ListViewItem ^LogItem;
	LogItem = gcnew ListViewItem(dataName);
	LogItem->ForeColor = m_defaultColor;
	LogItem->SubItems->Add(System::Int32(data).ToString());
	this->listViewDesc->Items->Insert(this->listViewDesc->Items->Count, LogItem);
}

System::Void CObjectDisplay::AddDouble(double data, System::String ^dataName)
{
	ListViewItem ^LogItem;
	LogItem = gcnew ListViewItem(dataName);
	LogItem->ForeColor = m_defaultColor;
	LogItem->SubItems->Add(System::Double(data).ToString());
	this->listViewDesc->Items->Insert(this->listViewDesc->Items->Count, LogItem);
}

System::Void CObjectDisplay::AddBool(bool data, System::String ^dataName)
{
	ListViewItem ^LogItem;
	LogItem = gcnew ListViewItem(dataName);
	LogItem->ForeColor = m_defaultColor;
	LogItem->SubItems->Add(System::Boolean(data).ToString());
	this->listViewDesc->Items->Insert(this->listViewDesc->Items->Count, LogItem);
}

System::Void CObjectDisplay::AddRangeString(cli::array<System::String ^> ^data, System::String ^dataName)
{
	ListViewItem ^LogItem;
	LogItem = gcnew ListViewItem(dataName);
	LogItem->ForeColor = m_defaultColor;
	LogItem->SubItems->AddRange(data);
	this->listViewDesc->Items->Insert(this->listViewDesc->Items->Count, LogItem);
}
