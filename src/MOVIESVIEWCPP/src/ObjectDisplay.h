#pragma once
using namespace System;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include <cstdint>

public ref class CObjectDisplay
{
public:
	CObjectDisplay(System::Windows::Forms::ListView ^refListView);
public:
	System::Windows::Forms::ListView^  listViewDesc;
	System::Drawing::Color m_defaultColor;
public: System::Void AddInt64(std::int64_t data, System::String^ dataName);
public: System::Void AddString(char *data, System::String ^dataName);
public: System::Void AddInt32(int data, System::String ^dataName);
public: System::Void AddDouble(double data, System::String ^dataName);
public: System::Void AddBool(bool data, System::String ^dataName);
public: System::Void AddRangeString(cli::array<System::String ^> ^data, System::String ^dataName);

};
