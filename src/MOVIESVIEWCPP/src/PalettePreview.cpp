#include "PalettePreview.h"

#include "ColorPaletteEcho.h"

PalettePreview::PalettePreview()
{
	minDataThreshold = -6000;
	maxDataThreshold = 1000;
	palette = nullptr;
	bmp = nullptr;

	tooltip = gcnew System::Windows::Forms::ToolTip();
	tooltipFormat = "{0:0.00} dB";
}

int PalettePreview::MinDataThreshold::get()
{
	return minDataThreshold;
}

void PalettePreview::MinDataThreshold::set(int val)
{
	minDataThreshold = val;
	Invalidate();
}

int PalettePreview::MaxDataThreshold::get()
{
	return maxDataThreshold;
}

void PalettePreview::MaxDataThreshold::set(int val)
{
	maxDataThreshold = val;
	Invalidate();
}

const IColorPalette * PalettePreview::Palette::get()
{
	return palette;
}

void PalettePreview::Palette::set(const IColorPalette * val)
{
	palette = val;
	Invalidate();
}

System::String ^ PalettePreview::TooltipFormat::get()
{
	return tooltipFormat;
}

void PalettePreview::TooltipFormat::set(System::String ^ fmt)
{
	tooltipFormat = fmt;
}

void PalettePreview::updateBmp(int w, int h)
{
	if (bmp != nullptr && bmp->Width == w && bmp->Height == h)
		return;

	bmp = gcnew System::Drawing::Bitmap(w, h);
	Invalidate();
}

void PalettePreview::rebuildBmp()
{
	if (bmp == nullptr)
		return;

	if (palette == nullptr)
		return;

	const int wrect = bmp->Width;
	const int hrect = bmp->Height;

	const double inv_width = 1.0 / wrect;
	for (int y = 0; y < hrect; ++y)
	{
		for (int x = 0; x < wrect; ++x)
		{
			const double v = x * inv_width;
			const DataFmt val = v * (maxDataThreshold - minDataThreshold) + minDataThreshold;
			auto color = System::Drawing::Color::FromArgb(palette->GetColor(val) | 0xFF000000);
			bmp->SetPixel(x, y, color);
		}
	}
}

void PalettePreview::OnSizeChanged(System::EventArgs^ e)
{
	System::Windows::Forms::Control::OnSizeChanged(e);
	updateBmp(this->DisplayRectangle.Width, this->DisplayRectangle.Height);
}

void PalettePreview::OnPaint(System::Windows::Forms::PaintEventArgs^ e)
{
	System::Windows::Forms::Control::OnPaint(e);		
	e->Graphics->InterpolationMode = System::Drawing::Drawing2D::InterpolationMode::NearestNeighbor;
	rebuildBmp();
	e->Graphics->DrawImage(bmp, this->DisplayRectangle);
}

void PalettePreview::OnMouseMove(System::Windows::Forms::MouseEventArgs ^e)
{
	System::Windows::Forms::Control::OnMouseMove(e);
	if (bmp == nullptr)
		return;

	if (e->X != tooltipLastPos)
	{
		tooltipLastPos = e->X;

		// Compute Sv value
		const auto svValue = ((((double)e->X)/ bmp->Width) * (maxDataThreshold - minDataThreshold) + minDataThreshold) * 0.01;
		
		// Format text
		auto str = System::String::Format(tooltipFormat, svValue);
		tooltip->SetToolTip(this, str);
	}
}

void PalettePreview::OnMouseLeave(System::EventArgs^ e)
{
	System::Windows::Forms::Control::OnMouseLeave(e);
	tooltip->Hide(this);
}

AnglePalettePreview::AnglePalettePreview()
{
	anglePalette = new RedBlueColorPalette();
	Palette = anglePalette;
	AngleThreshold = 18000;
	TooltipFormat = "{0:0.00}�";
}

AnglePalettePreview::~AnglePalettePreview()
{
	delete anglePalette;
}

int AnglePalettePreview::AngleThreshold::get()
{
	return angleThreshold;
}

void AnglePalettePreview::AngleThreshold::set(int val)
{
	angleThreshold = val;
	anglePalette->PreparePalette(-val, val);
	MinDataThreshold = -val;
	MaxDataThreshold = val;
}