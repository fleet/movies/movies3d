#pragma once

class IColorPalette;

ref class PalettePreview : public System::Windows::Forms::Control
{
public:
	PalettePreview();

	property int MinDataThreshold { int get(); void set(int b); }
	property int MaxDataThreshold { int get(); void set(int b); }

	property const IColorPalette * Palette { const IColorPalette * get(); void set(const IColorPalette * pal); }

	property System::String ^ TooltipFormat { System::String ^ get(); void set(System::String ^ fmt); }

protected: 
	virtual void OnSizeChanged(System::EventArgs^ e) override;	
	virtual void OnPaint(System::Windows::Forms::PaintEventArgs^ e) override;	
	virtual void OnMouseMove(System::Windows::Forms::MouseEventArgs ^e) override;	
	virtual void OnMouseLeave(System::EventArgs^ e) override;
	
private:
	int minDataThreshold;
	int maxDataThreshold;

	const IColorPalette * palette;

	System::Drawing::Bitmap ^ bmp;
	void updateBmp(int w, int h);
	void rebuildBmp();

	System::Windows::Forms::ToolTip ^tooltip;
	int tooltipLastPos;
	System::String ^ tooltipFormat;
};

ref class AnglePalettePreview : public PalettePreview
{
public:
	AnglePalettePreview();
	~AnglePalettePreview();
	property int AngleThreshold { int get(); void set(int b); }

private:
	IColorPalette * anglePalette;
	int angleThreshold;
};
