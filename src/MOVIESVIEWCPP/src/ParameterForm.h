#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de ParameterForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class ParameterForm : public System::Windows::Forms::Form
	{
	public:
		ParameterForm(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
		}

	protected:
		System::Void SetParamControl(IParamControl^ control, String^ caption)
		{
			this->Text = caption;
			m_ParamControl = control;
			this->tableLayoutPanel1->Controls->Add((Control^)this->m_ParamControl, 0, 0);
			((Control^)this->m_ParamControl)->Dock = System::Windows::Forms::DockStyle::Fill;
			m_ParamControl->UpdateGUI();
		}


	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~ParameterForm()
		{
			if (components)
			{
				delete components;
			}
		}



	protected:



		// r�f�rence vers le controle de configuration
	protected: IParamControl^ m_ParamControl;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	protected:
	protected: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::Button^  buttonCancel;
	private: System::Windows::Forms::Button^  ButtonOK;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->buttonCancel = (gcnew System::Windows::Forms::Button());
			this->ButtonOK = (gcnew System::Windows::Forms::Button());
			this->tableLayoutPanel1->SuspendLayout();
			this->panel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->AutoSize = true;
			this->tableLayoutPanel1->ColumnCount = 1;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->panel1, 0, 1);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 2;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->Size = System::Drawing::Size(189, 76);
			this->tableLayoutPanel1->TabIndex = 0;
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->buttonCancel);
			this->panel1->Controls->Add(this->ButtonOK);
			this->panel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->panel1->Location = System::Drawing::Point(3, 44);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(183, 29);
			this->panel1->TabIndex = 1;
			// 
			// buttonCancel
			// 
			this->buttonCancel->Anchor = System::Windows::Forms::AnchorStyles::Bottom;
			this->buttonCancel->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->buttonCancel->Location = System::Drawing::Point(105, 3);
			this->buttonCancel->Name = L"buttonCancel";
			this->buttonCancel->Size = System::Drawing::Size(75, 23);
			this->buttonCancel->TabIndex = 10;
			this->buttonCancel->Text = L"Cancel";
			this->buttonCancel->UseVisualStyleBackColor = true;
			this->buttonCancel->Click += gcnew System::EventHandler(this, &ParameterForm::buttonCancel_Click);
			// 
			// ButtonOK
			// 
			this->ButtonOK->Anchor = System::Windows::Forms::AnchorStyles::Bottom;
			this->ButtonOK->DialogResult = System::Windows::Forms::DialogResult::OK;
			this->ButtonOK->Location = System::Drawing::Point(3, 3);
			this->ButtonOK->Name = L"ButtonOK";
			this->ButtonOK->Size = System::Drawing::Size(75, 23);
			this->ButtonOK->TabIndex = 9;
			this->ButtonOK->Text = L"OK";
			this->ButtonOK->UseVisualStyleBackColor = true;
			this->ButtonOK->Click += gcnew System::EventHandler(this, &ParameterForm::ButtonOK_Click);
			// 
			// ParameterForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSize = true;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->ClientSize = System::Drawing::Size(189, 76);
			this->Controls->Add(this->tableLayoutPanel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->Name = L"ParameterForm";
			this->Text = L"ParameterForm";
			this->tableLayoutPanel1->ResumeLayout(false);
			this->panel1->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		// Application des modifications du controle sur la configuration
	private: System::Void ButtonOK_Click(System::Object^  sender, System::EventArgs^  e) {
		m_ParamControl->UpdateConfiguration();
		Hide();
	}
			 // Fermetude sans application des modifications
	private: System::Void buttonCancel_Click(System::Object^  sender, System::EventArgs^  e) {
		Hide();
	}
	};
}
