#pragma once
#include "BaseMathLib/Vector3.h"

class FanLine
{
public:
	BaseMathLib::Vector3D  m_PointUp;
	BaseMathLib::Vector3D  m_PointDown;
};
class FanLineCouple
{
public:
	FanLine m_LowerAngleBeam;
	FanLine m_UpperAngleBeam;
};
// OTK - 06/04/2009 - contient l'ensemble des infos relatives aux axes et labels
// pour le ping
class PingAxisInformation
{
public:
	// lignes d�limitant l'avant du faisceau
	FanLineCouple m_FrontLines;
	// lignes d�limitant l'arri�re du faisceau
	FanLineCouple m_BackLines;
	// indique si le ping repr�sente une �tiquette de distance
	bool m_IsMilesStone;
	// distance parcourue par le ping
	double m_CumulatedLength;
	// profondeur max du ping
	double m_MaxDepth;
	bool	 m_MaxDepthFound;
	std::uint64_t m_PingID;


	PingAxisInformation();
	virtual ~PingAxisInformation();
};