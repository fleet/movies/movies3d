#include "PingFan3D.h"
#include "Sounder3D.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/PingFan.h"


PingFan3D::PingFan3D(PingFan *pFan, std::uint64_t fanId) :
	m_fanId(fanId)
{
	m_pFan = pFan;
	MovRef(pFan);

}

PingFan3D::~PingFan3D(void)
{
	MovUnRefDelete(m_pFan);
}

void  PingFan3D::CheckUpdate(ViewFilter &refViewFilter)
{
	this->m_echoList.CheckUpdate(refViewFilter, this);
}
bool PingFan3D::Build(ViewFilter &refViewFilter)
{
	return this->m_echoList.Build(refViewFilter, this, m_transducerBeamOpening);
}


