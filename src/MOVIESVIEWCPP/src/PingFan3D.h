#pragma once

#include "Echo3DList.h"
#include "PingAxisInformation.h"
class PingFan;

class Sounder3D;
class PingFan3D
{
public:
	PingFan3D(PingFan *pFan, std::uint64_t fanId);
	virtual ~PingFan3D(void);

	/** build an 3d echo list given if the pingfan is visible and given the filtering parameters */
	bool Build(ViewFilter &refViewFilter);
	void  CheckUpdate(ViewFilter &refViewFilter);


	// fan id used
	std::uint64_t m_fanId;

	inline PingFan *getPingFan() const { return m_pFan; }
	Echo3DList m_echoList;
	PingAxisInformation m_transducerBeamOpening;
private:
	PingFan *m_pFan;
};
