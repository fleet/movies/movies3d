#include "PingFan3DContainer.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "ModuleManager/ModuleManager.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "TSAnalysis/TSAnalysisModule.h"
#include "TSAnalysis/TSTrack.h"
#include <algorithm>


struct PingFan3DComparator
{
	bool operator() (const PingFan3D * pFan1, const PingFan3D * pFan2) const
	{
		return pFan1->m_fanId > pFan2->m_fanId;
	}

	bool operator() (const std::uint64_t & fanId, const PingFan3D * pFan) const
	{
		return fanId > pFan->m_fanId;
	}

	bool operator() (const PingFan3D * pFan, const std::uint64_t & fanId) const
	{
		return pFan->m_fanId > fanId;
	}
};

PingFan3DContainer::PingFan3DContainer(void)
{
}

PingFan3DContainer::~PingFan3DContainer(void)
{
	removeAllPing();
}

void PingFan3DContainer::RemoveAllPingFan()
{
	removeAllPing();
}

void PingFan3DContainer::PingFanAdded()
{
	M3DKernel *pKernel = M3DKernel::GetInstance();


	/// on va retirer les ping qui ne nous interessent pas
	PingFan *pFan = (PingFan *)pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(0);
	// LB 02/07/2020 patch pour �viter plantage lorsque l'on charge la configuration avant les donn�es
	if (!pFan)
		return;

	std::uint64_t thisPingId = pFan->GetPingId();
	unsigned int removed = this->removeBatchMin(thisPingId);

	// remove front
	size_t numberOfFan = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectCount();
	pFan = (PingFan *)pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(numberOfFan - 1);
	if (pFan)
	{
		thisPingId = pFan->GetPingId();
		removed = this->removeBatchMax(thisPingId);
	}

	/// ON EVALUE zMIN ET zMax en fonction de ce que l'on a ajoute precedemment
	std::uint64_t thisMaxPingId = pFan->GetPingId();

	// on ne va inserer que ce qui ne l'a pas ete precedemment
	std::uint64_t iPingMrgMinId = getMinPingIdx();
	std::uint64_t iPingMrgMaxId = getMaxPingIdx();
	for (unsigned int z = 0; z < numberOfFan; z++)
	{
		PingFan *localFan = (PingFan *)pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(z);
		std::uint64_t pingNumber = localFan->GetPingId();
		if (pingNumber < iPingMrgMinId || pingNumber <= iPingMrgMaxId)
		{
			// deja affich� on ne fait rien
		}
		else
		{
			// add
			PingFan3D *p3Fan = new PingFan3D(localFan, pingNumber);
			addPing(p3Fan);
		}
	}
}


std::uint64_t PingFan3DContainer::getMinPingIdx()
{
	if (m_vectPing.empty())
		return 0;
	else
		return m_vectPing.back()->m_fanId;
}
std::uint64_t PingFan3DContainer::getMaxPingIdx()
{
	if (m_vectPing.empty())
		return 0;
	else
		return m_vectPing.front()->m_fanId;
}

unsigned int PingFan3DContainer::removeBatchMin(const std::uint64_t & minIdx)
{
	unsigned int ret = 0;
	std::uint64_t minPingId = getMinPingIdx();
	while (minPingId < minIdx && m_vectPing.size())
	{
		removePing(minPingId);
		minPingId = getMinPingIdx();
		ret++;
	}
	return ret;
}

unsigned int PingFan3DContainer::removeBatchMax(const std::uint64_t & maxIdx)
{
	unsigned int ret = 0;
	std::uint64_t maxPingId = getMaxPingIdx();
	while (maxPingId > maxIdx && m_vectPing.size())
	{
		removePing(maxPingId);
		maxPingId = getMaxPingIdx();
		ret++;
	}
	return ret;
}

void PingFan3DContainer::DecPingsForSounder(const std::uint32_t & sounderID)
{
	std::map<std::uint32_t, unsigned int>::iterator it = m_nbPingsForSounder.find(sounderID);
	if (it != m_nbPingsForSounder.end())
	{
		if (it->second > 0)
		{
			it->second--;
		}
	}
}

void PingFan3DContainer::IncPingsForSounder(const std::uint32_t & sounderID)
{
	std::map<std::uint32_t, unsigned int>::iterator it = m_nbPingsForSounder.find(sounderID);
	if (it != m_nbPingsForSounder.end())
	{
		it->second++;
	}
	else
	{
		m_nbPingsForSounder.insert(std::make_pair(sounderID, 1));
	}
}

PingFan3D* PingFan3DContainer::getPing(const std::uint64_t & fanId)
{
	PingFan3D* ret = NULL;
	if (m_vectPing.empty())
	{
		return ret;
	}

	std::vector<PingFan3D*>::iterator it = std::lower_bound(m_vectPing.begin(), m_vectPing.end(), fanId, PingFan3DComparator());
	if (it != m_vectPing.end())
	{
		if ((*it)->m_fanId == fanId)
		{
			ret = *it;
		}
	}

	return ret;
}

void PingFan3DContainer::removePing(const std::uint64_t & fanId)
{
	if (m_vectPing.empty())
	{
		return;
	}

	std::vector<PingFan3D*>::iterator it = std::lower_bound(m_vectPing.begin(), m_vectPing.end(), fanId, PingFan3DComparator());
	if (it != m_vectPing.end())
	{
		if ((*it)->m_fanId == fanId)
		{
			delete(*it);
			m_vectPing.erase(it);
		}
	}
}

void PingFan3DContainer::removeAllPing()
{
	for (std::vector<PingFan3D*>::iterator it = m_vectPing.begin(), itEnd = m_vectPing.end(); it != itEnd; ++it)
	{
		delete (*it);
	}
	m_vectPing.clear();
	m_nbPingsForSounder.clear();
}

void PingFan3DContainer::addPing(PingFan3D * pFan3D)
{
	if (!pFan3D)
		return;

	if (m_vectPing.empty())
	{
		m_vectPing.push_back(pFan3D);
	}
	else
	{
		if (pFan3D->m_fanId > getMaxPingIdx())
		{
			m_vectPing.insert(m_vectPing.begin(), pFan3D);
		}
		else if (pFan3D->m_fanId < getMinPingIdx())
		{
			m_vectPing.push_back(pFan3D);
		}
		else
		{
			std::vector<PingFan3D *>::iterator it = std::upper_bound(m_vectPing.begin(), m_vectPing.end(), pFan3D, PingFan3DComparator());
			m_vectPing.insert(it, pFan3D);
		}
	}

	// incrementation du nombre de pings du sondeur
	IncPingsForSounder(pFan3D->getPingFan()->getSounderRef()->m_SounderId);
}

const double knotRatio = 1.0 / 1.852;

bool PingFan3DContainer::Build(ViewFilter &refViewFilter)
{
	bool ret = false;

	const bool distanceGrid = DisplayParameter::getInstance()->GetDistanceGrid();

	// initialisation des donn�es pour le calcul de l'�chelle de distance
	// on travaille sur tous els sondeurs en m�me temps.

	struct sSounderInfo
	{
		bool firstDistLine;
		int nDistLine;
	};

	std::map<unsigned int, sSounderInfo> sounderInfos;
	if (distanceGrid)
	{
		const unsigned int nbSounder = refViewFilter.GetSounderCount();
		std::uint32_t sounderId;
		for (unsigned int i = 0; i < nbSounder; ++i)
		{
			sounderId = refViewFilter.GetSounderIdx(i)->getSounderID();

			sSounderInfo sounderInfo;
			sounderInfo.firstDistLine = true;
			sounderInfo.nDistLine = 0;
			sounderInfos[sounderId] = sounderInfo;
		}
	}


	unsigned int numAdded = 0;
	unsigned int numSeen = 0;

	const int distanceSampling = (int)(DisplayParameter::getInstance()->GetDistanceSampling()*1000.);
	const unsigned int pingFanDelayed = DisplayParameter::getInstance()->GetPingFanDelayed();
	const unsigned int pingFanLength = DisplayParameter::getInstance()->GetPingFanLength();

	PingFan * pFan;
	PingFan3D* pFan3D;

	const unsigned int nbPings = m_vectPing.size();
	for (int i = 0; i != nbPings; ++i)
	{
		pFan3D = m_vectPing[i];

		pFan3D->CheckUpdate(refViewFilter);
		++numSeen;
		if (numSeen > pingFanDelayed && numAdded < pingFanLength)
		{
			// construction des echos 3D du ping
			ret |= pFan3D->Build(refViewFilter);

			pFan = pFan3D->getPingFan();
			pFan3D->m_transducerBeamOpening.m_PingID = pFan->GetPingId();

			const double currentDist = pFan->m_relativePingFan.m_cumulatedDistance * knotRatio;
			pFan3D->m_transducerBeamOpening.m_CumulatedLength = currentDist;

			// construction de l'�chelle 3D en distance si activ�
			if (distanceGrid)
			{
				bool found = false;
				double bottom;
				pFan->m_computePingFan.getBottomMaxRangeMeter(bottom, found);
				pFan3D->m_transducerBeamOpening.m_MaxDepth = bottom;
				pFan3D->m_transducerBeamOpening.m_MaxDepthFound = found;

				const int pFanDistance = (int)floor(currentDist);

				sSounderInfo & sounderInfo = sounderInfos[pFan->m_pSounder->m_SounderId];
				if (sounderInfo.firstDistLine)
				{
					sounderInfo.nDistLine = (int)(floor((double)pFanDistance / (double)distanceSampling)*distanceSampling);
					sounderInfo.firstDistLine = false;
				}
				pFan3D->m_transducerBeamOpening.m_IsMilesStone = false;
				if (pFanDistance < sounderInfo.nDistLine)
				{
					pFan3D->m_transducerBeamOpening.m_IsMilesStone = true;
					sounderInfo.firstDistLine = true;
				}
			}
			else
			{
				pFan3D->m_transducerBeamOpening.m_IsMilesStone = false;
			}
			++numAdded;
		}
	}

	refViewFilter.OnRegeneratedVolume();

	return ret;
}


unsigned int PingFan3DContainer::Concatenate(MergedEchoList &refEchoList, ViewFilter &refViewFilter, std::uint32_t sounderId, std::uint64_t &minIdAdded, std::uint64_t &maxIdAdded,
	HacTime &minTime, HacTime &maxTime)
{
	unsigned int numAdded = 0;
	unsigned int numSeen = 0;
	std::uint64_t firstAdded = 0;
	std::uint64_t lastAdded = 0;

	refEchoList.StartMerge();

	// OTK - 02/07/2009 - on n'affiche pas les pings hors de la fourchette du zoom longitudinal
	unsigned int nbPingForSounder = 0;
	std::map<std::uint32_t, unsigned int>::iterator it = m_nbPingsForSounder.find(sounderId);
	if (it != m_nbPingsForSounder.end())
	{
		nbPingForSounder = it->second;
	}

	const int nbPingFans2D = M3DKernel::GetInstance()->GetRefKernelParameter().getNbPingFanMax();
	const int displayedNb = std::min<int>(nbPingFans2D, nbPingForSounder);
	const unsigned int pingFanStopIndex = displayedNb - (int)floor(0.5 + nbPingFans2D*DisplayParameter::getInstance()->GetMinSideHorizontalRatio());
	nbPingForSounder = 0;

	const unsigned int pingFanDelayed = DisplayParameter::getInstance()->GetPingFanDelayed();
	const unsigned int pingFanLength = DisplayParameter::getInstance()->GetPingFanLength();

	const bool bDisplayVolumeSampled = refViewFilter.m_bDisplayVolumeSampled;
	const bool bDisplaySingleEcho = refViewFilter.m_bDisplaySingleEcho;
	const bool bDisplayEcho = refViewFilter.m_bDisplayEcho;

	TSAnalysisModule * pTSModule = CModuleManager::getInstance()->GetTSAnalysisModule();


	std::vector<PingFan3D*> fansToConcatenate;

	PingFan3D * pFan3D;
	PingFan * pFan;

	const unsigned int nbPings = m_vectPing.size();
	fansToConcatenate.reserve(nbPings);

	for (; numSeen != nbPings; ++numSeen)
	{
		pFan3D = m_vectPing[numSeen];
		pFan = pFan3D->getPingFan();

		const bool isGoodSounder = pFan->getSounderRef()->m_SounderId == sounderId;

		if (numSeen >= pingFanDelayed && nbPingForSounder < pingFanStopIndex
			&& numAdded < pingFanLength)
		{
			if (numAdded == 0)
			{
				firstAdded = pFan->m_computePingFan.m_pingId;
				maxTime = pFan->m_ObjectTime;
			}
			if (isGoodSounder)
			{
				fansToConcatenate.push_back(pFan3D);
				lastAdded = pFan->m_computePingFan.m_pingId;
				minTime = pFan->m_ObjectTime;
			}
			++numAdded;
		}

		if (isGoodSounder)
		{
			++nbPingForSounder;
		}

		if (numAdded > pingFanLength) {
			break;
		}
	}

	const unsigned int nbFans = fansToConcatenate.size();
	for (unsigned int i = 0; i != nbFans; ++i)
	{
		pFan3D = fansToConcatenate[i];
		refEchoList.AddPingFan(pFan3D, bDisplayVolumeSampled);
		refEchoList.AddGroundFan(pFan3D);
	}

	if (bDisplaySingleEcho)
	{
		TSDisplayType tsDisplay = DisplayParameter::getInstance()->GetTSDisplayType();
		for (unsigned int i = 0; i != nbFans; ++i)
		{
			refEchoList.AddSingleEcho(fansToConcatenate[i]);
		}
		// OTK - FAE214 - Ajout des tracks
		if (tsDisplay != eTSDisplayUntracked)
		{
			Sounder3D * pSounder3D = refViewFilter.getSounder(sounderId);
			// On n'affiche pas les tracks si le sondeur n'est pas visible
			if (pSounder3D->isVisible())
			{
				const std::map<std::uint64_t, TSTrack*> & tracks = CModuleManager::getInstance()->GetTSAnalysisModule()->getTracks();
				std::map<std::uint64_t, TSTrack*>::const_iterator iterTrack;
				for (iterTrack = tracks.begin(); iterTrack != tracks.end(); ++iterTrack)
				{
					if (iterTrack->second->GetSounderID() == sounderId)
					{
						// Filtrage des tracks retenues
						if (pTSModule->IsValidTrack(iterTrack->second))
						{
							// Il faut �galement que le transducteur associ� soit coch� !
							Transducer3D * pTrans3D = pSounder3D->GetTransducer(iterTrack->second->GetTransducer()->m_transName);
							if (pTrans3D->isVisible())
							{
								// Il faut �galement que la track passe au moins une fois par un channel coch� !
								const std::vector<unsigned short> & trackChannels = iterTrack->second->GetChannelIDs();
								for (unsigned int iChan = 0; iChan < pTrans3D->getNumberOfChannel(); iChan++)
								{
									if (pTrans3D->getChannelWithIdx(iChan)->isVisible()
										&& std::find(trackChannels.begin(), trackChannels.end(), pTrans3D->getChannelWithIdx(iChan)->getChannel()->m_softChannelId) != trackChannels.end())
									{
										refEchoList.AddTrack(iterTrack->second, firstAdded, lastAdded);
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if (bDisplayEcho)
	{
		for (unsigned int i = 0; i != nbFans; ++i)
		{
			refEchoList.AddPhaseEcho(fansToConcatenate[i]);
		}
	}

	refEchoList.StopMerge();

	minIdAdded = lastAdded;
	maxIdAdded = firstAdded;

	return numAdded;
}
