#pragma once

#include <list>
#include <vector>
#include <map>

#include "PingFan3D.h"
#include "MergedEchoList.h"

class HacTime;

class PingFan3DContainer
{
public:
	PingFan3DContainer(void);
	virtual ~PingFan3DContainer(void);

	/** build an 3d echo list given if the sounder is visible and given the filtering parameters */
	bool Build(ViewFilter &refViewFilter);

	/** Concatenate our sub echo list to the given merged list
	*/
	unsigned int Concatenate(MergedEchoList &refEchoList, ViewFilter &refViewFilter, std::uint32_t sounderId, std::uint64_t &minIdAdded, std::uint64_t &maxIdAdded,
		HacTime &minTime, HacTime &maxTime);

	void PingFanAdded();
	void RemoveAllPingFan();

protected:
private:
	// these function manage the pingfan data (add/remove and so on)
	PingFan3D*		getPing(const std::uint64_t & fanIdx);
	void			removeAllPing();
	unsigned int	removeBatchMax(const std::uint64_t & maxIdx);
	unsigned int	removeBatchMin(const std::uint64_t & minIdx);
	std::uint64_t	getMaxPingIdx();
	std::uint64_t	getMinPingIdx();
	void			removePing(const std::uint64_t & fanIdx);
	void			addPing(PingFan3D *);

	inline void DecPingsForSounder(const std::uint32_t & sounderID);
	inline void IncPingsForSounder(const std::uint32_t & sounderID);


	std::vector<PingFan3D *> m_vectPing;
	std::map<std::uint32_t, unsigned int> m_nbPingsForSounder;
};
