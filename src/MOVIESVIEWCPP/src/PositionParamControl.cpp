#include "PositionParamControl.h"

#include "M3DKernel/M3DKernel.h"

using namespace MOVIESVIEWCPP;

void PositionParamControl::UpdateConfiguration()
{
	KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();

	PositionType posType;
	if (checkBoxFixedPosition->Checked)
	{
		posType = POSITION_USER;
	}
	else
	{
		posType = POSITION_TUPLE;
	}
	param.setPositionUsed(posType);
	BaseMathLib::Vector3D fixedPos(
		Convert::ToDouble(textBoxLat->Text),
		Convert::ToDouble(textBoxLong->Text),
		Convert::ToDouble(textBoxDepth->Text));

	param.setUserPositionValue(fixedPos);

	M3DKernel::GetInstance()->UpdateKernelParameter(param);
}

void PositionParamControl::UpdateGUI()
{
	KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();

	checkBoxFixedPosition->Checked = param.getPositionUsed() == POSITION_USER;
	textBoxLat->Text = Convert::ToString(param.getUserPositionValue().x);
	textBoxLong->Text = Convert::ToString(param.getUserPositionValue().y);
	textBoxDepth->Text = Convert::ToString(param.getUserPositionValue().z);

	UpdateComponents();
}

System::Void PositionParamControl::UpdateComponents()
{
	textBoxLat->Enabled = checkBoxFixedPosition->Checked;
	textBoxLong->Enabled = checkBoxFixedPosition->Checked;
	textBoxDepth->Enabled = checkBoxFixedPosition->Checked;
}

System::Void PositionParamControl::checkBoxFixedPosition_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	UpdateComponents();
}

System::Void PositionParamControl::textBoxLat_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		Convert::ToDouble(textBoxLat->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect latitude value !");
		textBoxLat->Text = Convert::ToString(M3DKernel::GetInstance()->GetRefKernelParameter().getUserPositionValue().x);
	}
}

System::Void PositionParamControl::textBoxLong_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		Convert::ToDouble(textBoxLong->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect longitude value !");
		textBoxLong->Text = Convert::ToString(M3DKernel::GetInstance()->GetRefKernelParameter().getUserPositionValue().y);
	}
}

System::Void PositionParamControl::textBoxDepth_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		Convert::ToDouble(textBoxDepth->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect depth value !");
		textBoxDepth->Text = Convert::ToString(M3DKernel::GetInstance()->GetRefKernelParameter().getUserPositionValue().z);
	}
}


