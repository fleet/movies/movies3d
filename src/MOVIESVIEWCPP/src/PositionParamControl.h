#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de PositionParamControl
	/// </summary>
	public ref class PositionParamControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		PositionParamControl(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~PositionParamControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  textBoxLat;
	private: System::Windows::Forms::TextBox^  textBoxLong;
	protected:

	protected:

	protected:



	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::CheckBox^  checkBoxFixedPosition;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBoxDepth;


	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBoxLat = (gcnew System::Windows::Forms::TextBox());
			this->textBoxLong = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBoxDepth = (gcnew System::Windows::Forms::TextBox());
			this->checkBoxFixedPosition = (gcnew System::Windows::Forms::CheckBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// textBoxLat
			// 
			this->textBoxLat->Location = System::Drawing::Point(85, 24);
			this->textBoxLat->Name = L"textBoxLat";
			this->textBoxLat->Size = System::Drawing::Size(128, 20);
			this->textBoxLat->TabIndex = 2;
			this->textBoxLat->LostFocus += gcnew System::EventHandler(this, &PositionParamControl::textBoxLat_LostFocus);
			// 
			// textBoxLong
			// 
			this->textBoxLong->Location = System::Drawing::Point(85, 50);
			this->textBoxLong->Name = L"textBoxLong";
			this->textBoxLong->Size = System::Drawing::Size(128, 20);
			this->textBoxLong->TabIndex = 3;
			this->textBoxLong->LostFocus += gcnew System::EventHandler(this, &PositionParamControl::textBoxLong_LostFocus);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->textBoxDepth);
			this->groupBox1->Controls->Add(this->checkBoxFixedPosition);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->textBoxLat);
			this->groupBox1->Controls->Add(this->textBoxLong);
			this->groupBox1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBox1->Location = System::Drawing::Point(0, 0);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(219, 109);
			this->groupBox1->TabIndex = 4;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Enable Fixed Position";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 79);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(59, 13);
			this->label3->TabIndex = 8;
			this->label3->Text = L"Depth (m) :";
			// 
			// textBoxDepth
			// 
			this->textBoxDepth->Location = System::Drawing::Point(85, 76);
			this->textBoxDepth->Name = L"textBoxDepth";
			this->textBoxDepth->Size = System::Drawing::Size(128, 20);
			this->textBoxDepth->TabIndex = 4;
			this->textBoxDepth->LostFocus += gcnew System::EventHandler(this, &PositionParamControl::textBoxDepth_LostFocus);
			// 
			// checkBoxFixedPosition
			// 
			this->checkBoxFixedPosition->AutoSize = true;
			this->checkBoxFixedPosition->Location = System::Drawing::Point(117, 1);
			this->checkBoxFixedPosition->Name = L"checkBoxFixedPosition";
			this->checkBoxFixedPosition->Size = System::Drawing::Size(15, 14);
			this->checkBoxFixedPosition->TabIndex = 6;
			this->checkBoxFixedPosition->UseVisualStyleBackColor = true;
			this->checkBoxFixedPosition->CheckedChanged += gcnew System::EventHandler(this, &PositionParamControl::checkBoxFixedPosition_CheckedChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 53);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(73, 13);
			this->label2->TabIndex = 5;
			this->label2->Text = L"Longitude (�) :";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 27);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(64, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"Latitude (�) :";
			// 
			// PositionParamControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->groupBox1);
			this->Name = L"PositionParamControl";
			this->Size = System::Drawing::Size(219, 109);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion


#pragma region Impl�mentation de IParamControl
	public: virtual void UpdateConfiguration();
	public: virtual void UpdateGUI();
#pragma endregion

	public: System::Void UpdateComponents();

	private: System::Void checkBoxFixedPosition_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxLat_LostFocus(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxLong_LostFocus(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxDepth_LostFocus(System::Object^  sender, System::EventArgs^  e);

	};
}
