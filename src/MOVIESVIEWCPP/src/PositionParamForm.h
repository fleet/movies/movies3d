// -*- MC++ -*-
// ****************************************************************************
// Class: PositionParamForm
//
// Description: Fen�tre de configuration de la position
//
// Projet: MOVIES3D
// Auteur: O.Tonck
// Date  : F�vrier 2010
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "ParameterForm.h"
#include "PositionParamControl.h"

namespace MOVIESVIEWCPP {
	public ref class PositionParamForm : public ParameterForm
	{
	public:
		PositionParamForm(void) :
			ParameterForm()
		{
			SetParamControl(gcnew PositionParamControl(), L"Position Parameters");
		}
	};
};