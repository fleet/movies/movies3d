#include "ProjectionParamControl.h"

#include "M3DKernel/M3DKernel.h"

using namespace MOVIESVIEWCPP;
using namespace BaseKernel;

void ProjectionParamControl::UpdateConfiguration()
{
	KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();

	param.getProjections().clear();
	for (size_t i = 0; i < m_pProjections->size(); i++)
	{
		param.getProjections().push_back(m_pProjections->at(i));
	}
	param.setSelectedProjection(m_SelectedProjection);

	M3DKernel::GetInstance()->UpdateKernelParameter(param);
}

void ProjectionParamControl::UpdateGUI()
{
	KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();

	// on cree notre copie de la config de projection pour travailler dessus
	// dans l'IHM
	m_pProjections->clear();
	for (size_t i = 0; i < param.getProjections().size(); i++)
	{
		m_pProjections->push_back(param.getProjections()[i]);
	}
	m_SelectedProjection = param.getSelectedProjection();

	// ajout de tous les noms de projection � la combobox
	comboBoxProjection->Items->Clear();
	for (size_t i = 0; i < m_pProjections->size(); i++)
	{
		comboBoxProjection->Items->Add(gcnew String(m_pProjections->at(i).m_ProjectionName.c_str()));
	}

	// choix de la projection active
	comboBoxProjection->SelectedIndex = m_SelectedProjection;
}

System::Void ProjectionParamControl::comboBoxProjection_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	m_SelectedProjection = comboBoxProjection->SelectedIndex;

	UpdateComponents();
}

System::Void ProjectionParamControl::buttonDelete_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (m_pProjections->size() > 1)
	{
		comboBoxProjection->Items->RemoveAt(comboBoxProjection->SelectedIndex);
		m_pProjections->erase(m_pProjections->begin() + m_SelectedProjection);
		comboBoxProjection->SelectedIndex = 0;
	}
	else
	{
		MessageBox::Show("the only remaining projection can't be deleted");
	}
}

System::Void ProjectionParamControl::buttonNew_Click(System::Object^  sender, System::EventArgs^  e)
{
	ParameterProjection proj;
	proj.m_ProjectionName = "New Projection";
	m_pProjections->push_back(proj);
	comboBoxProjection->Items->Add("New Projection");
	m_SelectedProjection = m_pProjections->size() - 1;
	comboBoxProjection->SelectedIndex = m_SelectedProjection;
}

System::Void ProjectionParamControl::textBoxProjectionName_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	char* str = (char*)Runtime::InteropServices::Marshal::StringToHGlobalAnsi(textBoxProjectionName->Text).ToPointer();
	std::string ret(str);
	Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(str));
	m_pProjections->at(m_SelectedProjection).m_ProjectionName = ret;
	comboBoxProjection->Items[m_SelectedProjection] = textBoxProjectionName->Text;
}

System::Void ProjectionParamControl::textBoxEccentricity_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		m_pProjections->at(m_SelectedProjection).m_Eccentricity = Convert::ToDouble(textBoxEccentricity->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect string format.");
		textBoxEccentricity->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_Eccentricity);
	}
}

System::Void ProjectionParamControl::textBoxSemiMajorAxis_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		m_pProjections->at(m_SelectedProjection).m_SemiMajorAxis = Convert::ToDouble(textBoxSemiMajorAxis->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect string format.");
		textBoxSemiMajorAxis->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_SemiMajorAxis);
	}
}

System::Void ProjectionParamControl::textBoxFirstParal_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		m_pProjections->at(m_SelectedProjection).m_FirstParal = Convert::ToDouble(textBoxFirstParal->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect string format.");
		textBoxFirstParal->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_FirstParal);
	}
}

System::Void ProjectionParamControl::textBoxSecondParal_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		m_pProjections->at(m_SelectedProjection).m_SecondParal = Convert::ToDouble(textBoxSecondParal->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect string format.");
		textBoxSecondParal->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_SecondParal);
	}
}

System::Void ProjectionParamControl::textBoxRefLong_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		m_pProjections->at(m_SelectedProjection).m_LongMeridOrigin = Convert::ToDouble(textBoxRefLong->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect string format.");
		textBoxRefLong->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_LongMeridOrigin);
	}
}

System::Void ProjectionParamControl::textBoxX0_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		m_pProjections->at(m_SelectedProjection).m_X0 = Convert::ToDouble(textBoxX0->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect string format.");
		textBoxX0->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_X0);
	}
}

System::Void ProjectionParamControl::textBoxY0_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		m_pProjections->at(m_SelectedProjection).m_Y0 = Convert::ToDouble(textBoxY0->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect string format.");
		textBoxY0->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_Y0);
	}
}

System::Void ProjectionParamControl::textBoxScaleFactor_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		m_pProjections->at(m_SelectedProjection).m_ScaleFactor = Convert::ToDouble(textBoxScaleFactor->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect string format.");
		textBoxScaleFactor->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_ScaleFactor);
	}
}

System::Void ProjectionParamControl::UpdateComponents()
{
	// mise � jour des param�tres
	textBoxProjectionName->Text = gcnew String(m_pProjections->at(m_SelectedProjection).m_ProjectionName.c_str());
	textBoxEccentricity->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_Eccentricity);
	textBoxSemiMajorAxis->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_SemiMajorAxis);
	textBoxFirstParal->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_FirstParal);
	textBoxSecondParal->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_SecondParal);
	textBoxRefLong->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_LongMeridOrigin);
	textBoxX0->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_X0);
	textBoxY0->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_Y0);
	textBoxScaleFactor->Text = Convert::ToString(m_pProjections->at(m_SelectedProjection).m_ScaleFactor);
}
