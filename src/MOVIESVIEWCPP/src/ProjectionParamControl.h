#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"

#include "M3DKernel/parameter/ParameterProjection.h"

#include <vector>

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de ProjectionParamControl
	/// </summary>
	public ref class ProjectionParamControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		ProjectionParamControl(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
			m_pProjections = new std::vector<BaseKernel::ParameterProjection>();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~ProjectionParamControl()
		{
			if (components)
			{
				delete components;
			}

			delete m_pProjections;
		}
	private: System::Windows::Forms::GroupBox^  groupBoxProjection;
	private: System::Windows::Forms::Button^  buttonNew;
	private: System::Windows::Forms::Button^  buttonDelete;
	private: System::Windows::Forms::ComboBox^  comboBoxProjection;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::Label^  labelScaleFactor;
	private: System::Windows::Forms::TextBox^  textBoxScaleFactor;
	private: System::Windows::Forms::Label^  labelY0;
	private: System::Windows::Forms::TextBox^  textBoxY0;
	private: System::Windows::Forms::Label^  labelX0;
	private: System::Windows::Forms::TextBox^  textBoxX0;
	private: System::Windows::Forms::Label^  labelRefLong;
	private: System::Windows::Forms::TextBox^  textBoxRefLong;
	private: System::Windows::Forms::Label^  labelSecondParal;
	private: System::Windows::Forms::TextBox^  textBoxSecondParal;
	private: System::Windows::Forms::Label^  labelFirstParal;
	private: System::Windows::Forms::TextBox^  textBoxFirstParal;
	private: System::Windows::Forms::Label^  labelSemiMajorAxis;
	private: System::Windows::Forms::TextBox^  textBoxSemiMajorAxis;
	private: System::Windows::Forms::Label^  labelEccentricity;
	private: System::Windows::Forms::TextBox^  textBoxEccentricity;
	private: System::Windows::Forms::Label^  labelProjectionName;
	private: System::Windows::Forms::TextBox^  textBoxProjectionName;






























	protected:

	protected:

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBoxProjection = (gcnew System::Windows::Forms::GroupBox());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->labelScaleFactor = (gcnew System::Windows::Forms::Label());
			this->textBoxScaleFactor = (gcnew System::Windows::Forms::TextBox());
			this->labelY0 = (gcnew System::Windows::Forms::Label());
			this->textBoxY0 = (gcnew System::Windows::Forms::TextBox());
			this->labelX0 = (gcnew System::Windows::Forms::Label());
			this->textBoxX0 = (gcnew System::Windows::Forms::TextBox());
			this->labelRefLong = (gcnew System::Windows::Forms::Label());
			this->textBoxRefLong = (gcnew System::Windows::Forms::TextBox());
			this->labelSecondParal = (gcnew System::Windows::Forms::Label());
			this->textBoxSecondParal = (gcnew System::Windows::Forms::TextBox());
			this->labelFirstParal = (gcnew System::Windows::Forms::Label());
			this->textBoxFirstParal = (gcnew System::Windows::Forms::TextBox());
			this->labelSemiMajorAxis = (gcnew System::Windows::Forms::Label());
			this->textBoxSemiMajorAxis = (gcnew System::Windows::Forms::TextBox());
			this->labelEccentricity = (gcnew System::Windows::Forms::Label());
			this->textBoxEccentricity = (gcnew System::Windows::Forms::TextBox());
			this->labelProjectionName = (gcnew System::Windows::Forms::Label());
			this->textBoxProjectionName = (gcnew System::Windows::Forms::TextBox());
			this->buttonNew = (gcnew System::Windows::Forms::Button());
			this->buttonDelete = (gcnew System::Windows::Forms::Button());
			this->comboBoxProjection = (gcnew System::Windows::Forms::ComboBox());
			this->groupBoxProjection->SuspendLayout();
			this->tableLayoutPanel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBoxProjection
			// 
			this->groupBoxProjection->Controls->Add(this->tableLayoutPanel1);
			this->groupBoxProjection->Controls->Add(this->buttonNew);
			this->groupBoxProjection->Controls->Add(this->buttonDelete);
			this->groupBoxProjection->Controls->Add(this->comboBoxProjection);
			this->groupBoxProjection->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxProjection->Location = System::Drawing::Point(0, 0);
			this->groupBoxProjection->Name = L"groupBoxProjection";
			this->groupBoxProjection->Size = System::Drawing::Size(282, 312);
			this->groupBoxProjection->TabIndex = 0;
			this->groupBoxProjection->TabStop = false;
			this->groupBoxProjection->Text = L"Projection";
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 2;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				42.96296F)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				57.03704F)));
			this->tableLayoutPanel1->Controls->Add(this->labelScaleFactor, 0, 8);
			this->tableLayoutPanel1->Controls->Add(this->textBoxScaleFactor, 1, 8);
			this->tableLayoutPanel1->Controls->Add(this->labelY0, 0, 7);
			this->tableLayoutPanel1->Controls->Add(this->textBoxY0, 1, 7);
			this->tableLayoutPanel1->Controls->Add(this->labelX0, 0, 6);
			this->tableLayoutPanel1->Controls->Add(this->textBoxX0, 1, 6);
			this->tableLayoutPanel1->Controls->Add(this->labelRefLong, 0, 5);
			this->tableLayoutPanel1->Controls->Add(this->textBoxRefLong, 1, 5);
			this->tableLayoutPanel1->Controls->Add(this->labelSecondParal, 0, 4);
			this->tableLayoutPanel1->Controls->Add(this->textBoxSecondParal, 1, 4);
			this->tableLayoutPanel1->Controls->Add(this->labelFirstParal, 0, 3);
			this->tableLayoutPanel1->Controls->Add(this->textBoxFirstParal, 1, 3);
			this->tableLayoutPanel1->Controls->Add(this->labelSemiMajorAxis, 0, 2);
			this->tableLayoutPanel1->Controls->Add(this->textBoxSemiMajorAxis, 1, 2);
			this->tableLayoutPanel1->Controls->Add(this->labelEccentricity, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->textBoxEccentricity, 1, 1);
			this->tableLayoutPanel1->Controls->Add(this->labelProjectionName, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->textBoxProjectionName, 1, 0);
			this->tableLayoutPanel1->Location = System::Drawing::Point(6, 59);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 9;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->Size = System::Drawing::Size(270, 236);
			this->tableLayoutPanel1->TabIndex = 3;
			// 
			// labelScaleFactor
			// 
			this->labelScaleFactor->AutoSize = true;
			this->labelScaleFactor->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelScaleFactor->Location = System::Drawing::Point(3, 208);
			this->labelScaleFactor->Name = L"labelScaleFactor";
			this->labelScaleFactor->Size = System::Drawing::Size(109, 28);
			this->labelScaleFactor->TabIndex = 14;
			this->labelScaleFactor->Text = L"Scale Factor";
			this->labelScaleFactor->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// textBoxScaleFactor
			// 
			this->textBoxScaleFactor->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBoxScaleFactor->Location = System::Drawing::Point(118, 211);
			this->textBoxScaleFactor->Name = L"textBoxScaleFactor";
			this->textBoxScaleFactor->Size = System::Drawing::Size(149, 20);
			this->textBoxScaleFactor->TabIndex = 15;
			this->textBoxScaleFactor->LostFocus += gcnew System::EventHandler(this, &ProjectionParamControl::textBoxScaleFactor_LostFocus);
			// 
			// labelY0
			// 
			this->labelY0->AutoSize = true;
			this->labelY0->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelY0->Location = System::Drawing::Point(3, 182);
			this->labelY0->Name = L"labelY0";
			this->labelY0->Size = System::Drawing::Size(109, 26);
			this->labelY0->TabIndex = 12;
			this->labelY0->Text = L"Y0";
			this->labelY0->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// textBoxY0
			// 
			this->textBoxY0->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBoxY0->Location = System::Drawing::Point(118, 185);
			this->textBoxY0->Name = L"textBoxY0";
			this->textBoxY0->Size = System::Drawing::Size(149, 20);
			this->textBoxY0->TabIndex = 13;
			this->textBoxY0->LostFocus += gcnew System::EventHandler(this, &ProjectionParamControl::textBoxY0_LostFocus);
			// 
			// labelX0
			// 
			this->labelX0->AutoSize = true;
			this->labelX0->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelX0->Location = System::Drawing::Point(3, 156);
			this->labelX0->Name = L"labelX0";
			this->labelX0->Size = System::Drawing::Size(109, 26);
			this->labelX0->TabIndex = 10;
			this->labelX0->Text = L"X0";
			this->labelX0->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// textBoxX0
			// 
			this->textBoxX0->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBoxX0->Location = System::Drawing::Point(118, 159);
			this->textBoxX0->Name = L"textBoxX0";
			this->textBoxX0->Size = System::Drawing::Size(149, 20);
			this->textBoxX0->TabIndex = 11;
			this->textBoxX0->LostFocus += gcnew System::EventHandler(this, &ProjectionParamControl::textBoxX0_LostFocus);
			// 
			// labelRefLong
			// 
			this->labelRefLong->AutoSize = true;
			this->labelRefLong->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelRefLong->Location = System::Drawing::Point(3, 130);
			this->labelRefLong->Name = L"labelRefLong";
			this->labelRefLong->Size = System::Drawing::Size(109, 26);
			this->labelRefLong->TabIndex = 8;
			this->labelRefLong->Text = L"Reference Longitude";
			this->labelRefLong->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// textBoxRefLong
			// 
			this->textBoxRefLong->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBoxRefLong->Location = System::Drawing::Point(118, 133);
			this->textBoxRefLong->Name = L"textBoxRefLong";
			this->textBoxRefLong->Size = System::Drawing::Size(149, 20);
			this->textBoxRefLong->TabIndex = 9;
			this->textBoxRefLong->LostFocus += gcnew System::EventHandler(this, &ProjectionParamControl::textBoxRefLong_LostFocus);
			// 
			// labelSecondParal
			// 
			this->labelSecondParal->AutoSize = true;
			this->labelSecondParal->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelSecondParal->Location = System::Drawing::Point(3, 104);
			this->labelSecondParal->Name = L"labelSecondParal";
			this->labelSecondParal->Size = System::Drawing::Size(109, 26);
			this->labelSecondParal->TabIndex = 6;
			this->labelSecondParal->Text = L"Second Parallel";
			this->labelSecondParal->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// textBoxSecondParal
			// 
			this->textBoxSecondParal->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBoxSecondParal->Location = System::Drawing::Point(118, 107);
			this->textBoxSecondParal->Name = L"textBoxSecondParal";
			this->textBoxSecondParal->Size = System::Drawing::Size(149, 20);
			this->textBoxSecondParal->TabIndex = 7;
			this->textBoxSecondParal->LostFocus += gcnew System::EventHandler(this, &ProjectionParamControl::textBoxSecondParal_LostFocus);
			// 
			// labelFirstParal
			// 
			this->labelFirstParal->AutoSize = true;
			this->labelFirstParal->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelFirstParal->Location = System::Drawing::Point(3, 78);
			this->labelFirstParal->Name = L"labelFirstParal";
			this->labelFirstParal->Size = System::Drawing::Size(109, 26);
			this->labelFirstParal->TabIndex = 4;
			this->labelFirstParal->Text = L"First Parallel";
			this->labelFirstParal->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// textBoxFirstParal
			// 
			this->textBoxFirstParal->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBoxFirstParal->Location = System::Drawing::Point(118, 81);
			this->textBoxFirstParal->Name = L"textBoxFirstParal";
			this->textBoxFirstParal->Size = System::Drawing::Size(149, 20);
			this->textBoxFirstParal->TabIndex = 5;
			this->textBoxFirstParal->LostFocus += gcnew System::EventHandler(this, &ProjectionParamControl::textBoxFirstParal_LostFocus);
			// 
			// labelSemiMajorAxis
			// 
			this->labelSemiMajorAxis->AutoSize = true;
			this->labelSemiMajorAxis->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelSemiMajorAxis->Location = System::Drawing::Point(3, 52);
			this->labelSemiMajorAxis->Name = L"labelSemiMajorAxis";
			this->labelSemiMajorAxis->Size = System::Drawing::Size(109, 26);
			this->labelSemiMajorAxis->TabIndex = 2;
			this->labelSemiMajorAxis->Text = L"Semi-Major Axis";
			this->labelSemiMajorAxis->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// textBoxSemiMajorAxis
			// 
			this->textBoxSemiMajorAxis->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBoxSemiMajorAxis->Location = System::Drawing::Point(118, 55);
			this->textBoxSemiMajorAxis->Name = L"textBoxSemiMajorAxis";
			this->textBoxSemiMajorAxis->Size = System::Drawing::Size(149, 20);
			this->textBoxSemiMajorAxis->TabIndex = 3;
			this->textBoxSemiMajorAxis->LostFocus += gcnew System::EventHandler(this, &ProjectionParamControl::textBoxSemiMajorAxis_LostFocus);
			// 
			// labelEccentricity
			// 
			this->labelEccentricity->AutoSize = true;
			this->labelEccentricity->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelEccentricity->Location = System::Drawing::Point(3, 26);
			this->labelEccentricity->Name = L"labelEccentricity";
			this->labelEccentricity->Size = System::Drawing::Size(109, 26);
			this->labelEccentricity->TabIndex = 0;
			this->labelEccentricity->Text = L"Eccentricity";
			this->labelEccentricity->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// textBoxEccentricity
			// 
			this->textBoxEccentricity->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBoxEccentricity->Location = System::Drawing::Point(118, 29);
			this->textBoxEccentricity->Name = L"textBoxEccentricity";
			this->textBoxEccentricity->Size = System::Drawing::Size(149, 20);
			this->textBoxEccentricity->TabIndex = 1;
			this->textBoxEccentricity->LostFocus += gcnew System::EventHandler(this, &ProjectionParamControl::textBoxEccentricity_LostFocus);
			// 
			// labelProjectionName
			// 
			this->labelProjectionName->AutoSize = true;
			this->labelProjectionName->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelProjectionName->Location = System::Drawing::Point(3, 0);
			this->labelProjectionName->Name = L"labelProjectionName";
			this->labelProjectionName->Size = System::Drawing::Size(109, 26);
			this->labelProjectionName->TabIndex = 16;
			this->labelProjectionName->Text = L"Projection Name";
			this->labelProjectionName->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// textBoxProjectionName
			// 
			this->textBoxProjectionName->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBoxProjectionName->Location = System::Drawing::Point(118, 3);
			this->textBoxProjectionName->Name = L"textBoxProjectionName";
			this->textBoxProjectionName->Size = System::Drawing::Size(149, 20);
			this->textBoxProjectionName->TabIndex = 17;
			this->textBoxProjectionName->LostFocus += gcnew System::EventHandler(this, &ProjectionParamControl::textBoxProjectionName_LostFocus);
			// 
			// buttonNew
			// 
			this->buttonNew->Location = System::Drawing::Point(222, 17);
			this->buttonNew->Name = L"buttonNew";
			this->buttonNew->Size = System::Drawing::Size(54, 23);
			this->buttonNew->TabIndex = 2;
			this->buttonNew->Text = L"New";
			this->buttonNew->UseVisualStyleBackColor = true;
			this->buttonNew->Click += gcnew System::EventHandler(this, &ProjectionParamControl::buttonNew_Click);
			// 
			// buttonDelete
			// 
			this->buttonDelete->Location = System::Drawing::Point(162, 17);
			this->buttonDelete->Name = L"buttonDelete";
			this->buttonDelete->Size = System::Drawing::Size(54, 23);
			this->buttonDelete->TabIndex = 1;
			this->buttonDelete->Text = L"Delete";
			this->buttonDelete->UseVisualStyleBackColor = true;
			this->buttonDelete->Click += gcnew System::EventHandler(this, &ProjectionParamControl::buttonDelete_Click);
			// 
			// comboBoxProjection
			// 
			this->comboBoxProjection->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxProjection->FormattingEnabled = true;
			this->comboBoxProjection->Location = System::Drawing::Point(6, 19);
			this->comboBoxProjection->Name = L"comboBoxProjection";
			this->comboBoxProjection->Size = System::Drawing::Size(150, 21);
			this->comboBoxProjection->TabIndex = 0;
			this->comboBoxProjection->SelectedIndexChanged += gcnew System::EventHandler(this, &ProjectionParamControl::comboBoxProjection_SelectedIndexChanged);
			// 
			// ProjectionParamControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->groupBoxProjection);
			this->Name = L"ProjectionParamControl";
			this->Size = System::Drawing::Size(282, 312);
			this->groupBoxProjection->ResumeLayout(false);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

#pragma region Impl�mentation de IParamControl
	public: virtual void UpdateConfiguration();
	public: virtual void UpdateGUI();
#pragma endregion

	private: System::Void UpdateComponents();

	private: System::Void comboBoxProjection_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonDelete_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonNew_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void textBoxProjectionName_LostFocus(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxEccentricity_LostFocus(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxSemiMajorAxis_LostFocus(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxFirstParal_LostFocus(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxSecondParal_LostFocus(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxRefLong_LostFocus(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxX0_LostFocus(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxY0_LostFocus(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxScaleFactor_LostFocus(System::Object^  sender, System::EventArgs^  e);

	private: std::vector<BaseKernel::ParameterProjection> *m_pProjections;
	private: int m_SelectedProjection;
	};
}
