// -*- MC++ -*-
// ****************************************************************************
// Class: ProjectionParamForm
//
// Description: Fen�tre de configuration du syst�me de projection
//
// Projet: MOVIES3D
// Auteur: O.Tonck
// Date  : Juin 2010
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "ParameterForm.h"
#include "ProjectionParamControl.h"

namespace MOVIESVIEWCPP {
	public ref class ProjectionParamForm : public ParameterForm
	{
	public:
		ProjectionParamForm(void) :
			ParameterForm()
		{
			SetParamControl(gcnew ProjectionParamControl(), L"Projection Parameters");
		}
	};
};