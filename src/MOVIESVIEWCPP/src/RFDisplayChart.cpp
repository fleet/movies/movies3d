
#include "RFDisplayChart.h"

//rfcomputer
#include <Compensation/rf/RFComputer.h>

#include <iostream>

using namespace MOVIESVIEWCPP;

namespace {
	
	System::Windows::Forms::DataVisualization::Charting::Series^ MakeSerieForRFResults(RFComputer * rfComputer, RFResults* pResults, long shoalId, bool filtered, bool isMultiBeam, bool absolute, bool db)
	{
		System::Windows::Forms::DataVisualization::Charting::Series^ serie = gcnew System::Windows::Forms::DataVisualization::Charting::Series();

		//legend label
		if (shoalId <= 0)
		{
			serie->Name = "Window ";
		}
		else
		{
			serie->Name = "Shoal " + shoalId.ToString();
		}
		serie->Name += (filtered) ? " filtered" : " unfiltered";

		serie->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Line;

		const auto & map = (filtered) ? pResults->filtered : pResults->unfiltered;

		auto iterResult = map.begin();

		//reorder results by frequency
		std::map<float, RFResult*> orderedMap;

		if (isMultiBeam)
		{
			while (iterResult != map.end())
			{
				for (RFResult* pResult : *iterResult->second)
				{
					orderedMap[pResult->GetAngle()] = pResult;
				}
				iterResult++;
			}
		}
		else
		{
			while (iterResult != map.end())
			{
				for (auto pResult : *(iterResult->second))
				{
					orderedMap[pResult->GetFrequency()] = pResult;
				}
				iterResult++;
			}
		}

		double refValue = 1.0;
		if (absolute)
		{
			refValue = rfComputer->GetRefEnergy(shoalId, filtered);
			if (refValue == 0.0)
			{
				refValue = 1.0;
			}
		}

		for(auto it = orderedMap.cbegin(); it != orderedMap.cend(); ++it)
		{
			RFResult* pResult = it->second;

			auto yvalue = pResult->GetRFRatio() * refValue;
			if (db)
			{
				yvalue = 10.0 * log10(yvalue);
			}

			if (isMultiBeam)
			{
				serie->Points->AddXY(pResult->GetAngle(), yvalue);
			}
			else 
			{
				//frequency is put on abscissa in KHz
				serie->Points->AddXY(pResult->GetFrequency() / 1000.0, yvalue);
			}
		}

		serie->Color = filtered ? System::Drawing::Color::Blue : System::Drawing::Color::Green;

		return serie;
	}
}

RFDisplayChart::RFDisplayChart()
{
	this->BeginInit();

	auto chartArea = gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea();
	chartArea->Name = L"ChartArea";

	chartArea->AxisX->Title = L"Frequency (kHz)";
	chartArea->AxisX->IsStartedFromZero = false;
	chartArea->AxisX->LabelStyle->Format = "D";
	chartArea->AxisX->MajorGrid->LineColor = System::Drawing::Color::WhiteSmoke;

	chartArea->AxisY->Title = L"R(F)";
	chartArea->AxisY->MajorGrid->LineColor = System::Drawing::Color::WhiteSmoke;
	chartArea->AxisY->LabelStyle->Format = "g5";
	chartArea->AxisY->IsStartedFromZero = false;

	chartArea->AxisY2->Title = L"TS";
	chartArea->AxisY2->MajorGrid->LineColor = System::Drawing::Color::WhiteSmoke;
	chartArea->AxisY2->LabelStyle->Format = "g5";
	chartArea->AxisY2->IsStartedFromZero = false;

	this->ChartAreas->Add(chartArea);
	this->Dock = System::Windows::Forms::DockStyle::Fill;

	auto chartLegend = gcnew System::Windows::Forms::DataVisualization::Charting::Legend();
	chartLegend->Name = L"Legend";
	chartLegend->DockedToChartArea = chartArea->Name;
	chartLegend->LegendStyle = System::Windows::Forms::DataVisualization::Charting::LegendStyle::Column;
	this->Legends->Add(chartLegend);
	
	// Install interactor
	this->chartInteractor = gcnew ChartInteractor();
	chartInteractor->Install(this);
	
	this->EndInit();
}

void RFDisplayChart::SetRFResult(RFComputer * rfComputer, bool absolute, bool db)
{
	this->ChartAreas->SuspendUpdates();
	this->Titles->Clear();

	auto chart = this->ChartAreas[0];
	auto xaxis = chart->AxisX;
	auto yaxis = chart->AxisY;

	this->Series->SuspendUpdates();
	this->Series->Clear();

	double xMin = double(System::Decimal::MaxValue);
	double xMax = 0;

	double yMinPrimary = double(System::Decimal::MaxValue);
	double yMaxPrimary = double(System::Decimal::MinValue);

	double yMinSecondary = double(System::Decimal::MaxValue);
	double yMaxSecondary = double(System::Decimal::MinValue);

	std::set<int> xlabels;

	bool hasRF = false;
	bool hasTS = false;

	if (rfComputer)
	{
		if (rfComputer->IsMultiBeam()) 
		{
			this->Titles->Add("Angular Response");
			xaxis->Title = "Angle (Deg)";
			yaxis->Title=  "r(a)";
		}
		else
		{
			this->Titles->Add("Frequency Response");
			xaxis->Title = "Frequency (KHz)";
			yaxis->Title = "r(f)";
		}

		//for each input, add curves
		const auto & results = rfComputer->GetResults();
		for (auto iterRes = results.cbegin(); iterRes != results.cend(); ++iterRes)
		{
			if (rfComputer->GetParameters().GetComputeFiltered())
			{
				hasRF = true;
				auto serie = MakeSerieForRFResults(rfComputer, iterRes->second, iterRes->first, true, rfComputer->IsMultiBeam(), absolute, db);
				this->Series->Add(serie);
			}
		
			if (rfComputer->GetParameters().GetComputeUnfiltered())
			{
				hasRF = true;
				auto serie = MakeSerieForRFResults(rfComputer, iterRes->second, iterRes->first, false, rfComputer->IsMultiBeam(), absolute, db);
				this->Series->Add(serie);
			}

			const auto nbTSPoints = iterRes->second->ts.freq.size();
			if (nbTSPoints > 0)
			{
				hasTS = true;

				auto compensatedTSSerie = gcnew System::Windows::Forms::DataVisualization::Charting::Series();
				auto uncompensatedTSSerie = gcnew System::Windows::Forms::DataVisualization::Charting::Series();

				compensatedTSSerie->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;
				uncompensatedTSSerie->YAxisType = System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary;

				compensatedTSSerie->Name = "Compensated TS";
				uncompensatedTSSerie->Name = "Uncompensated TS";

				compensatedTSSerie->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Line;
				uncompensatedTSSerie->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Line;

				const auto xFactor = rfComputer->IsMultiBeam() ? 1.0 : 1e-3;
				for (int i = 0; i < nbTSPoints; ++i)
				{
					const auto freq = iterRes->second->ts.freq[i] * xFactor;
					auto compensatedTS = iterRes->second->ts.compensatedTS[i];
					auto unCompensatedTS = iterRes->second->ts.unCompensatedTS[i];
					if (db)
					{
						compensatedTS = 10 * log10(compensatedTS);
						unCompensatedTS = 10 * log10(unCompensatedTS);
					}
					compensatedTSSerie->Points->AddXY(freq, compensatedTS);
					uncompensatedTSSerie->Points->AddXY(freq, unCompensatedTS);
				}
				this->Series->Add(compensatedTSSerie);
			}
		}
	}
	
	// Compute min / max amplitude
	for each(auto serie in this->Series)
	{
		double & yMin = (serie->YAxisType == System::Windows::Forms::DataVisualization::Charting::AxisType::Primary) ? yMinPrimary : yMinSecondary;
		double & yMax = (serie->YAxisType == System::Windows::Forms::DataVisualization::Charting::AxisType::Primary) ? yMaxPrimary : yMaxSecondary;

		xMin = std::min(xMin, serie->Points[0]->XValue);
		xMax = std::max(xMax, serie->Points[serie->Points->Count - 1]->XValue);

		for (int i = 0; i < serie->Points->Count; ++i)
		{
			auto yvalue = serie->Points[i]->YValues[0];
			yMin = std::min(yvalue, yMin);
			yMax = std::max(yvalue, yMax);
		}
	}

	// Vérification si les bornes ne sont pas égales, sinon crash au niveau de l'affichage
	if (xMin == xMax)
	{
		xMin -= 0.5;
		xMax += 0.5;
	}

	if (yMinPrimary == yMaxPrimary)
	{
		yMinPrimary -= 0.5;
		yMaxPrimary += 0.5;
	}

	if (yMinSecondary == yMaxSecondary)
	{
		yMinSecondary -= 0.5;
		yMaxSecondary += 0.5;
	}

	this->Series->ResumeUpdates();

	this->ChartAreas[0]->AxisX->CustomLabels->Clear();
	for (auto iterLabels = xlabels.cbegin(); iterLabels != xlabels.cend(); ++iterLabels)
	{
		double stepValue = *iterLabels;
		int step = *iterLabels;

		auto cl = this->ChartAreas[0]->AxisX->CustomLabels->Add(stepValue - 100, stepValue + 100, step.ToString());
		cl->GridTicks  = System::Windows::Forms::DataVisualization::Charting::GridTickTypes::All;
	}	

	chart->AxisX->Minimum = xMin;
	chart->AxisX->Maximum = xMax;
	chart->AxisX->RoundAxisValues();

	if (hasRF)
	{	

		chart->AxisY->Minimum = yMinPrimary;
		chart->AxisY->Maximum = yMaxPrimary;
		chart->AxisY->RoundAxisValues();
	}
	else
	{
		chart->AxisY->Minimum = 0;
		chart->AxisY->Maximum = 1;
		chart->AxisY->RoundAxisValues();
	}

	if (hasTS)
	{
		chart->AxisY2->Minimum = yMinSecondary;
		chart->AxisY2->Maximum = yMaxSecondary;
		chart->AxisY2->RoundAxisValues();
	}
	else
	{
		chart->AxisY2->Minimum = 0;
		chart->AxisY2->Maximum = 1;
		chart->AxisY2->RoundAxisValues();
	}

	this->ChartAreas->ResumeUpdates();
	this->ChartAreas->Invalidate();
}

void RFDisplayChart::ShowSv(bool visible)
{
	for each(auto serie in this->Series)
	{
		if (serie->YAxisType == System::Windows::Forms::DataVisualization::Charting::AxisType::Primary)
		{
			serie->Enabled = visible;
		}
	}
}

bool RFDisplayChart::HasSv()
{
	for each(auto serie in this->Series)
	{
		if (serie->YAxisType == System::Windows::Forms::DataVisualization::Charting::AxisType::Primary)
		{
			return true;
		}
	}

	return false;
}

void RFDisplayChart::ShowTS(bool visible)
{
	for each(auto serie in this->Series)
	{
		if (serie->YAxisType == System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary)
		{
			serie->Enabled = visible;
		}
	}
}

bool RFDisplayChart::HasTS()
{
	for each(auto serie in this->Series)
	{
		if (serie->YAxisType == System::Windows::Forms::DataVisualization::Charting::AxisType::Secondary)
		{
			return true;
		}
	}

	return false;
}