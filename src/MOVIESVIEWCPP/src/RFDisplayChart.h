#pragma once

#include "Chart/ChartInteractor.h"

class RFComputer;

namespace MOVIESVIEWCPP 
{
	/// Composant d'affichage des r�sultats de la r�ponse fr�quentielle / angulaire
	ref class RFDisplayChart : public System::Windows::Forms::DataVisualization::Charting::Chart
	{
		ChartInteractor ^chartInteractor;

	public:
		RFDisplayChart();

		/// Affiche les r�sultats produits par le RFComputer
		void SetRFResult(RFComputer * rfComputer, bool absolute, bool db);

		///	Affiche ou masque toutes les series Sv
		void ShowSv(bool visible);

		/// Retourne vrai si au moins une serie Sv est disponible
		bool HasSv();

		///	Affiche ou masque toutes les series TS	
		void ShowTS(bool visible);

		/// Retourne vrai si au moins une serie TS est disponible
		bool HasTS();
	};
};
