#include "RFDisplayView.h"

using namespace MOVIESVIEWCPP;

RFDisplayView::RFDisplayView()
{
	m_rfComputer = nullptr;

	InitializeComponent();

	m_pRFDisplay = gcnew RFDisplayChart();
	m_pRFDisplay->Dock = DockStyle::Fill;
	this->tableLayoutPanel1->Controls->Add(m_pRFDisplay, 0, 1);
	m_pRFDisplay->Name = L"pictureBoxRFDisplay";

	checkBoxSv->Checked = true;
	checkBoxTS->Checked = true;
}

void RFDisplayView::SetRFResult(RFComputer * rfComputer)
{
	m_rfComputer = rfComputer;
	UpdateRFDisplay();

	//
	if (m_pRFDisplay->HasSv())
	{
		checkBoxSv->Enabled = true;
		checkBoxSv->Checked = true;
	}
	else
	{
		checkBoxSv->Enabled = false;
	}
	
	//
	if (m_pRFDisplay->HasTS())
	{
		checkBoxTS->Enabled = true;
		checkBoxTS->Checked = true;
	}
	else
	{
		checkBoxTS->Enabled = false;
	}
}

void RFDisplayView::UpdateRFDisplay()
{
	if (m_rfComputer == nullptr)
		return;
	
	m_pRFDisplay->SetRFResult(m_rfComputer, this->checkBoxRFAbsolute->Checked, this->checkBoxRFdB->Checked);
}

System::Void RFDisplayView::checkBoxRFAbsolute_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	UpdateRFDisplay();
}

System::Void RFDisplayView::checkBoxRFdB_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	UpdateRFDisplay();
}

System::Void RFDisplayView::checkBoxSv_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	m_pRFDisplay->ShowSv(checkBoxSv->Checked);
}

System::Void RFDisplayView::checkBoxTS_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	m_pRFDisplay->ShowTS(checkBoxTS->Checked);
}