#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

// D�clarations anticip�s
class RFComputer;

#include "RFDisplayChart.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de RFDisplayView
	/// </summary>
	public ref class RFDisplayView : public System::Windows::Forms::UserControl
	{
	public:
		RFDisplayView();

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~RFDisplayView()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanel2;
	private: System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanelRFDisplay;
	private: System::Windows::Forms::CheckBox^  checkBoxRFAbsolute;
	private: System::Windows::Forms::CheckBox^  checkBoxRFdB;
	private: System::Windows::Forms::CheckBox^  checkBoxSv;
	private: System::Windows::Forms::CheckBox^  checkBoxTS;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->flowLayoutPanel2 = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->checkBoxRFAbsolute = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxRFdB = (gcnew System::Windows::Forms::CheckBox());
			this->flowLayoutPanelRFDisplay = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->checkBoxSv = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxTS = (gcnew System::Windows::Forms::CheckBox());
			this->tableLayoutPanel1->SuspendLayout();
			this->flowLayoutPanel2->SuspendLayout();
			this->SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 1;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->flowLayoutPanel2, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->flowLayoutPanelRFDisplay, 0, 1);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 2;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(564, 407);
			this->tableLayoutPanel1->TabIndex = 4;
			// 
			// flowLayoutPanel2
			// 
			this->flowLayoutPanel2->Controls->Add(this->checkBoxRFAbsolute);
			this->flowLayoutPanel2->Controls->Add(this->checkBoxRFdB);
			this->flowLayoutPanel2->Controls->Add(this->checkBoxSv);
			this->flowLayoutPanel2->Controls->Add(this->checkBoxTS);
			this->flowLayoutPanel2->Location = System::Drawing::Point(3, 3);
			this->flowLayoutPanel2->Name = L"flowLayoutPanel2";
			this->flowLayoutPanel2->Size = System::Drawing::Size(336, 25);
			this->flowLayoutPanel2->TabIndex = 4;
			// 
			// checkBoxRFAbsolute
			// 
			this->checkBoxRFAbsolute->AutoSize = true;
			this->checkBoxRFAbsolute->Location = System::Drawing::Point(3, 3);
			this->checkBoxRFAbsolute->Name = L"checkBoxRFAbsolute";
			this->checkBoxRFAbsolute->Size = System::Drawing::Size(67, 17);
			this->checkBoxRFAbsolute->TabIndex = 3;
			this->checkBoxRFAbsolute->Text = L"Absolute";
			this->checkBoxRFAbsolute->UseVisualStyleBackColor = true;
			this->checkBoxRFAbsolute->CheckedChanged += gcnew System::EventHandler(this, &RFDisplayView::checkBoxRFAbsolute_CheckedChanged);
			// 
			// checkBoxRFdB
			// 
			this->checkBoxRFdB->AutoSize = true;
			this->checkBoxRFdB->Location = System::Drawing::Point(76, 3);
			this->checkBoxRFdB->Name = L"checkBoxRFdB";
			this->checkBoxRFdB->Size = System::Drawing::Size(39, 17);
			this->checkBoxRFdB->TabIndex = 2;
			this->checkBoxRFdB->Text = L"dB";
			this->checkBoxRFdB->UseVisualStyleBackColor = true;
			this->checkBoxRFdB->CheckedChanged += gcnew System::EventHandler(this, &RFDisplayView::checkBoxRFdB_CheckedChanged);
			// 
			// flowLayoutPanelRFDisplay
			// 
			this->flowLayoutPanelRFDisplay->Dock = System::Windows::Forms::DockStyle::Fill;
			this->flowLayoutPanelRFDisplay->Location = System::Drawing::Point(3, 34);
			this->flowLayoutPanelRFDisplay->Name = L"flowLayoutPanelRFDisplay";
			this->flowLayoutPanelRFDisplay->Size = System::Drawing::Size(558, 370);
			this->flowLayoutPanelRFDisplay->TabIndex = 5;
			// 
			// checkBoxSv
			// 
			this->checkBoxSv->AutoSize = true;
			this->checkBoxSv->Location = System::Drawing::Point(121, 3);
			this->checkBoxSv->Name = L"checkBoxSv";
			this->checkBoxSv->Size = System::Drawing::Size(39, 17);
			this->checkBoxSv->TabIndex = 4;
			this->checkBoxSv->Text = L"Sv";
			this->checkBoxSv->UseVisualStyleBackColor = true;
			this->checkBoxSv->CheckedChanged += gcnew System::EventHandler(this, &RFDisplayView::checkBoxSv_CheckedChanged);
			// 
			// checkBoxTS
			// 
			this->checkBoxTS->AutoSize = true;
			this->checkBoxTS->Location = System::Drawing::Point(166, 3);
			this->checkBoxTS->Name = L"checkBoxTS";
			this->checkBoxTS->Size = System::Drawing::Size(40, 17);
			this->checkBoxTS->TabIndex = 5;
			this->checkBoxTS->Text = L"TS";
			this->checkBoxTS->UseVisualStyleBackColor = true;
			this->checkBoxTS->CheckedChanged += gcnew System::EventHandler(this, &RFDisplayView::checkBoxTS_CheckedChanged);
			// 
			// RFDisplayView
			// 
			this->AccessibleName = L"Frequency Response";
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->tableLayoutPanel1);
			this->Name = L"RFDisplayView";
			this->Size = System::Drawing::Size(564, 407);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->flowLayoutPanel2->ResumeLayout(false);
			this->flowLayoutPanel2->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

	public:
		void SetRFResult(RFComputer * rfComputer);
		
	private:
		System::Void checkBoxRFAbsolute_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	
		System::Void checkBoxRFdB_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

		System::Void checkBoxSv_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

		System::Void checkBoxTS_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
		
		void UpdateRFDisplay();

		RFDisplayChart ^ m_pRFDisplay;

		RFComputer * m_rfComputer;
	};
}
