
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "ModuleManager/ModuleManager.h"
#include "Compensation\CompensationModule.h"

#include "utils.h"

#include "RFSettingsForm.h"


using namespace MOVIESVIEWCPP;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;


// ***************************************************************************
/**
 * list all transducers
 *
 * @date   21/07/2009 - Fran�ois Racap�(IPSIS) - Cr�ation
 */
 // ***************************************************************************
std::set<std::string> RFSettingsForm::ListAllTransducers()
{
	// NMD - FAE 102 - Modification de la r�cuperation des transducteurs
	std::set<std::string>  result;
	M3DKernel *pKernel = M3DKernel::GetInstance();
	CurrentSounderDefinition & sounderMgr = pKernel->getObjectMgr()->GetPingFanContainer().m_sounderDefinition;

	for (unsigned int i = 0; i < sounderMgr.GetNbSounder(); i++)
	{
		Sounder *pSounder = sounderMgr.GetSounder(i);
		if (!pSounder->m_isMultiBeam)
		{
			//search the transducers
			for (unsigned int j = 0; j < pSounder->GetTransducerCount(); j++)
			{
				Transducer *pTrans = pSounder->GetTransducer(j);
				if (pTrans != NULL)
				{
					result.insert(pTrans->m_transName);
				}
			}
		}
	}
	return result;
}

// ***************************************************************************
/**
 * mise � jour de l'IHM � partir des donn�es de param�tres
 * reglages IHM
 *
 * @date   21/07/2009 - Fran�ois Racap�(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void RFSettingsForm::DataToIHM()
{
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	CompensationModule * pModule = pModuleMgr->GetCompensationModule();

	RFParameter      *rfparams = &(pModule->GetCompensationParameter().m_rfParameter);
	OverLapParameter *overlapparams = &(pModule->GetCompensationParameter().m_overlapParameter);

	//overlap parameters
	m_enableOverlapCB->Checked = overlapparams->IsEnable();
	m_referenceThresholdTF->Text = Convert::ToString(overlapparams->GetReferenceThreshold() / 100.0);
	m_dataThresholdTF->Text = Convert::ToString(overlapparams->GetDataThreshold() / 100.0);
	m_zGridSpaceTF->Text = Convert::ToString(overlapparams->GetGridSpaceZ());

	String^ format = gcnew String("F2");
	m_dXYAngleFactorTF = RAD_TO_DEG(overlapparams->GetGridSpaceXYAngleFactor());
	m_xyAngleFactorTF->Text = Double(m_dXYAngleFactorTF).ToString(format);

	m_refWindowTF->Text = Convert::ToString(overlapparams->GetReferenceWindowsPingCount());

	//rf parameters
	m_overlapThresholdTF->Text = Convert::ToString(rfparams->GetOverlapThreshold());
	m_echoesMinNbTF->Text = Convert::ToString(rfparams->GetMinEchoesNb());
	m_precisionLogTF->Text = Convert::ToString(rfparams->GetPrecisionLog());
	m_precisionDepthTF->Text = Convert::ToString(rfparams->GetPrecisionDepth());
	m_useSvCB->Checked = rfparams->GetUseSV();
	m_useUnfilteredCB->Checked = rfparams->GetComputeUnfiltered();

	//initialize the list of transducers
	if (m_refTransducerCB->Items->Count == 0)
	{
		//list all transducers
		std::set<std::string> transList = ListAllTransducers();

		// Shutdown the painting of the ListBox as items are added.
		m_refTransducerCB->BeginUpdate();

		//add every transducer in the list
		int refTransIdx = -1;
		std::set<std::string>::iterator iterTrans = transList.begin();
		for (int i = 0; iterTrans != transList.end(); iterTrans++, i++)
		{
			std::string transName = *iterTrans;
			if (transName == rfparams->GetRefTransducer())
			{
				refTransIdx = i;
			}
			m_refTransducerCB->Items->Add(cppStr2NetStr(transName.c_str()));
		}

		m_refTransducerCB->EndUpdate();

		//select the ref transducer
		if (refTransIdx >= 0)
		{
			m_refTransducerCB->SelectedIndex = refTransIdx;
		}
	}
}


// ***************************************************************************
/**
 * mise � jour des donn�es � partir de l'IHM
 * reglages IHM
 *
 * @date   21/07/2009 - Fran�ois Racap�(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void RFSettingsForm::IHMToData()
{
	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	CompensationModule * pModule = pModuleMgr->GetCompensationModule();

	RFParameter      *rfparams = &(pModule->GetCompensationParameter().m_rfParameter);
	OverLapParameter *overlapparams = &(pModule->GetCompensationParameter().m_overlapParameter);

	//overlap parameters
	bool overlapEnable = m_enableOverlapCB->Checked;
	DataFmt referenceThreshold = (DataFmt)(Convert::ToDouble(m_referenceThresholdTF->Text)*100.0);
	DataFmt dataThreshold = (DataFmt)(Convert::ToDouble(m_dataThresholdTF->Text)*100.0);
	double gridSpaceZ = Convert::ToDouble(m_zGridSpaceTF->Text);

	double dXYAngleFactorTF = Convert::ToDouble(m_xyAngleFactorTF->Text);
	double gridSpaceXYAngleFactor = DEG_TO_RAD(dXYAngleFactorTF);
	int referenceWindowsPingCount = Convert::ToUInt32(m_refWindowTF->Text);

	m_overlapContextChanged = (overlapEnable != overlapparams->IsEnable()) ||
		(referenceThreshold != overlapparams->GetReferenceThreshold()) ||
		(dataThreshold != overlapparams->GetDataThreshold()) ||
		(gridSpaceZ != overlapparams->GetGridSpaceZ()) ||
		abs(dXYAngleFactorTF - m_dXYAngleFactorTF) > 0.01 ||
		(referenceWindowsPingCount != overlapparams->GetReferenceWindowsPingCount());

	overlapparams->SetEnable(overlapEnable);
	overlapparams->SetReferenceThreshold(referenceThreshold);
	overlapparams->SetDataThreshold(dataThreshold);
	overlapparams->SetGridSpaceZ(gridSpaceZ);
	overlapparams->SetGridSpaceXYAngleFactor(gridSpaceXYAngleFactor);
	overlapparams->SetReferenceWindowsPingCount(referenceWindowsPingCount);

	//rf parameters
	rfparams->SetOverlapThreshold((DataFmt)(Convert::ToDouble(m_overlapThresholdTF->Text)));
	rfparams->SetMinEchoesNb(Convert::ToInt32(m_echoesMinNbTF->Text));
	rfparams->SetPrecisionLog(Convert::ToSingle((m_precisionLogTF->Text)));
	rfparams->SetPrecisionDepth(Convert::ToSingle(m_precisionDepthTF->Text));
	rfparams->SetUseSV(m_useSvCB->Checked);
	rfparams->SetComputeUnfiltered(m_useUnfilteredCB->Checked);

	if (m_refTransducerCB->SelectedIndex >= 0)
	{
		rfparams->SetRefTransducer(netStr2CppStr(m_refTransducerCB->Text));
	}
}
