#pragma once

#include <string>
#include <set>

#include "compensation/overlapparameter.h"
#include "compensation/rf/RFparameter.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Summary for RFSettingsForm
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class RFSettingsForm : public System::Windows::Forms::Form
	{
	public:
		RFSettingsForm(BackgroundWorker^ Bgw)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

			m_overlapContextChanged = false;

			// pas de modification des param�tres si un traitement est en cours
			if (Bgw->IsBusy)
			{
				m_okB->Enabled = false;
			}
		}

		void Init()
		{
			try
			{
				DataToIHM();
			}
			catch (Exception^)
			{
				MessageBox::Show("Invalid configuration.");
			}
		}

		//the overlap context is changed
		bool OverlapContextChanged() { return m_overlapContextChanged; };

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~RFSettingsForm()
		{
			if (components)
			{
				delete components;
			}
		}

		// mise � jour des donn�es partir des param�tres IHM
		void IHMToData();
		// mise � jour des param�tres IHM en fonction des donn�es
		void DataToIHM();

		// list all transducers
		std::set<std::string> ListAllTransducers();

		//members
		//the overlap context is changed
		bool m_overlapContextChanged;

		//m_dXYAngleFactorTF
		double m_dXYAngleFactorTF;

	private: System::Windows::Forms::TextBox^  textBox9;
	private: System::Windows::Forms::TextBox^  textBox8;
	private: System::Windows::Forms::TextBox^  textBox7;
	private: System::Windows::Forms::TextBox^  textBox6;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox10;
	private: System::Windows::Forms::TextBox^  m_referenceThresholdTF;
	private: System::Windows::Forms::TextBox^  m_zGridSpaceTF;
	private: System::Windows::Forms::TextBox^  m_xyAngleFactorTF;
	private: System::Windows::Forms::TextBox^  m_refWindowTF;
	private: System::Windows::Forms::TextBox^  m_precisionLogTF;
	private: System::Windows::Forms::TextBox^  m_precisionDepthTF;
	private: System::Windows::Forms::TextBox^  m_overlapThresholdTF;
	private: System::Windows::Forms::TextBox^  m_echoesMinNbTF;
	private: System::Windows::Forms::TextBox^  m_dataThresholdTF;

	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label22;
	private: System::Windows::Forms::Label^  label20;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label18;
	private: System::Windows::Forms::Label^  label17;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::CheckBox^  m_useSvCB;
	private: System::Windows::Forms::Button^  m_okB;
	private: System::Windows::Forms::Button^  m_cancelB;
	private: System::Windows::Forms::ComboBox^  m_refTransducerCB;
	private: System::Windows::Forms::CheckBox^  m_useUnfilteredCB;
	private: System::Windows::Forms::CheckBox^  m_enableOverlapCB;


	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->m_referenceThresholdTF = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->m_enableOverlapCB = (gcnew System::Windows::Forms::CheckBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->m_refWindowTF = (gcnew System::Windows::Forms::TextBox());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->m_zGridSpaceTF = (gcnew System::Windows::Forms::TextBox());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->m_xyAngleFactorTF = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->m_dataThresholdTF = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->m_useUnfilteredCB = (gcnew System::Windows::Forms::CheckBox());
			this->m_refTransducerCB = (gcnew System::Windows::Forms::ComboBox());
			this->m_useSvCB = (gcnew System::Windows::Forms::CheckBox());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->m_echoesMinNbTF = (gcnew System::Windows::Forms::TextBox());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->m_precisionDepthTF = (gcnew System::Windows::Forms::TextBox());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->m_precisionLogTF = (gcnew System::Windows::Forms::TextBox());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->m_overlapThresholdTF = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->m_okB = (gcnew System::Windows::Forms::Button());
			this->m_cancelB = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(11, 26);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(103, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Reference threshold";
			// 
			// m_referenceThresholdTF
			// 
			this->m_referenceThresholdTF->Location = System::Drawing::Point(137, 19);
			this->m_referenceThresholdTF->Name = L"m_referenceThresholdTF";
			this->m_referenceThresholdTF->Size = System::Drawing::Size(122, 20);
			this->m_referenceThresholdTF->TabIndex = 2;
			this->m_referenceThresholdTF->Leave += gcnew System::EventHandler(this, &RFSettingsForm::m_referenceThresholdTF_Leave);
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->m_enableOverlapCB);
			this->groupBox1->Controls->Add(this->label11);
			this->groupBox1->Controls->Add(this->m_refWindowTF);
			this->groupBox1->Controls->Add(this->label12);
			this->groupBox1->Controls->Add(this->label9);
			this->groupBox1->Controls->Add(this->m_zGridSpaceTF);
			this->groupBox1->Controls->Add(this->label10);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->m_xyAngleFactorTF);
			this->groupBox1->Controls->Add(this->label8);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->m_referenceThresholdTF);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Location = System::Drawing::Point(7, 32);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(306, 124);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Overlap";
			// 
			// m_enableOverlapCB
			// 
			this->m_enableOverlapCB->AutoSize = true;
			this->m_enableOverlapCB->CheckAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->m_enableOverlapCB->Checked = true;
			this->m_enableOverlapCB->CheckState = System::Windows::Forms::CheckState::Checked;
			this->m_enableOverlapCB->Location = System::Drawing::Point(53, 0);
			this->m_enableOverlapCB->Name = L"m_enableOverlapCB";
			this->m_enableOverlapCB->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->m_enableOverlapCB->Size = System::Drawing::Size(15, 14);
			this->m_enableOverlapCB->TabIndex = 22;
			this->m_enableOverlapCB->UseVisualStyleBackColor = true;
			this->m_enableOverlapCB->CheckedChanged += gcnew System::EventHandler(this, &RFSettingsForm::m_enableOverlapCB_CheckedChanged);
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(265, 101);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(32, 13);
			this->label11->TabIndex = 15;
			this->label11->Text = L"pings";
			// 
			// m_refWindowTF
			// 
			this->m_refWindowTF->Location = System::Drawing::Point(136, 97);
			this->m_refWindowTF->Name = L"m_refWindowTF";
			this->m_refWindowTF->Size = System::Drawing::Size(122, 20);
			this->m_refWindowTF->TabIndex = 14;
			this->m_refWindowTF->Leave += gcnew System::EventHandler(this, &RFSettingsForm::m_refWindowTF_Leave);
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(10, 104);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(69, 13);
			this->label12->TabIndex = 13;
			this->label12->Text = L"MovRef. Window";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(265, 75);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(15, 13);
			this->label9->TabIndex = 12;
			this->label9->Text = L"m";
			// 
			// m_zGridSpaceTF
			// 
			this->m_zGridSpaceTF->Location = System::Drawing::Point(136, 71);
			this->m_zGridSpaceTF->Name = L"m_zGridSpaceTF";
			this->m_zGridSpaceTF->Size = System::Drawing::Size(122, 20);
			this->m_zGridSpaceTF->TabIndex = 11;
			this->m_zGridSpaceTF->Leave += gcnew System::EventHandler(this, &RFSettingsForm::m_zGridSpaceTF_Leave);
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(10, 78);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(70, 13);
			this->label10->TabIndex = 10;
			this->label10->Text = L"Z Grid Space";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(265, 49);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(11, 13);
			this->label7->TabIndex = 9;
			this->label7->Text = L"�";
			// 
			// m_xyAngleFactorTF
			// 
			this->m_xyAngleFactorTF->Location = System::Drawing::Point(136, 45);
			this->m_xyAngleFactorTF->Name = L"m_xyAngleFactorTF";
			this->m_xyAngleFactorTF->Size = System::Drawing::Size(122, 20);
			this->m_xyAngleFactorTF->TabIndex = 8;
			this->m_xyAngleFactorTF->Leave += gcnew System::EventHandler(this, &RFSettingsForm::m_xyAngleFactorTF_Leave);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(10, 52);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(118, 13);
			this->label8->TabIndex = 7;
			this->label8->Text = L"XY Angle Factor Space";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(266, 23);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(20, 13);
			this->label3->TabIndex = 3;
			this->label3->Text = L"dB";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(272, 10);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(20, 13);
			this->label5->TabIndex = 6;
			this->label5->Text = L"dB";
			// 
			// m_dataThresholdTF
			// 
			this->m_dataThresholdTF->Location = System::Drawing::Point(143, 6);
			this->m_dataThresholdTF->Name = L"m_dataThresholdTF";
			this->m_dataThresholdTF->Size = System::Drawing::Size(122, 20);
			this->m_dataThresholdTF->TabIndex = 5;
			this->m_dataThresholdTF->Leave += gcnew System::EventHandler(this, &RFSettingsForm::m_dataThresholdTF_Leave);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(4, 9);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(76, 13);
			this->label6->TabIndex = 4;
			this->label6->Text = L"Data threshold";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->m_useUnfilteredCB);
			this->groupBox2->Controls->Add(this->m_refTransducerCB);
			this->groupBox2->Controls->Add(this->m_useSvCB);
			this->groupBox2->Controls->Add(this->label22);
			this->groupBox2->Controls->Add(this->m_echoesMinNbTF);
			this->groupBox2->Controls->Add(this->label20);
			this->groupBox2->Controls->Add(this->label17);
			this->groupBox2->Controls->Add(this->m_precisionDepthTF);
			this->groupBox2->Controls->Add(this->label18);
			this->groupBox2->Controls->Add(this->label15);
			this->groupBox2->Controls->Add(this->m_precisionLogTF);
			this->groupBox2->Controls->Add(this->label16);
			this->groupBox2->Controls->Add(this->label1);
			this->groupBox2->Controls->Add(this->m_overlapThresholdTF);
			this->groupBox2->Controls->Add(this->label4);
			this->groupBox2->Location = System::Drawing::Point(7, 162);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(306, 200);
			this->groupBox2->TabIndex = 6;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"r(f)";
			// 
			// m_useUnfilteredCB
			// 
			this->m_useUnfilteredCB->AutoSize = true;
			this->m_useUnfilteredCB->CheckAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->m_useUnfilteredCB->Location = System::Drawing::Point(10, 66);
			this->m_useUnfilteredCB->Name = L"m_useUnfilteredCB";
			this->m_useUnfilteredCB->Size = System::Drawing::Size(142, 17);
			this->m_useUnfilteredCB->TabIndex = 23;
			this->m_useUnfilteredCB->Text = L"Use unfiltered                 ";
			this->m_useUnfilteredCB->UseVisualStyleBackColor = true;
			// 
			// m_refTransducerCB
			// 
			this->m_refTransducerCB->FormattingEnabled = true;
			this->m_refTransducerCB->Location = System::Drawing::Point(137, 168);
			this->m_refTransducerCB->Name = L"m_refTransducerCB";
			this->m_refTransducerCB->Size = System::Drawing::Size(121, 21);
			this->m_refTransducerCB->TabIndex = 22;
			// 
			// m_useSvCB
			// 
			this->m_useSvCB->AutoSize = true;
			this->m_useSvCB->CheckAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->m_useSvCB->Location = System::Drawing::Point(10, 47);
			this->m_useSvCB->Name = L"m_useSvCB";
			this->m_useSvCB->Size = System::Drawing::Size(142, 17);
			this->m_useSvCB->TabIndex = 21;
			this->m_useSvCB->Text = L"Use Sv                           ";
			this->m_useSvCB->UseVisualStyleBackColor = true;
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(11, 173);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(110, 13);
			this->label22->TabIndex = 19;
			this->label22->Text = L"Reference transducer";
			// 
			// m_echoesMinNbTF
			// 
			this->m_echoesMinNbTF->Location = System::Drawing::Point(137, 141);
			this->m_echoesMinNbTF->Name = L"m_echoesMinNbTF";
			this->m_echoesMinNbTF->Size = System::Drawing::Size(122, 20);
			this->m_echoesMinNbTF->TabIndex = 17;
			this->m_echoesMinNbTF->Leave += gcnew System::EventHandler(this, &RFSettingsForm::m_echoesMinNbTF_Leave);
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(11, 142);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(77, 13);
			this->label20->TabIndex = 16;
			this->label20->Text = L"Echoes min nb";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(266, 119);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(15, 13);
			this->label17->TabIndex = 15;
			this->label17->Text = L"m";
			// 
			// m_precisionDepthTF
			// 
			this->m_precisionDepthTF->Location = System::Drawing::Point(137, 115);
			this->m_precisionDepthTF->Name = L"m_precisionDepthTF";
			this->m_precisionDepthTF->Size = System::Drawing::Size(122, 20);
			this->m_precisionDepthTF->TabIndex = 14;
			this->m_precisionDepthTF->Leave += gcnew System::EventHandler(this, &RFSettingsForm::m_precisionDepthTF_Leave);
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(11, 117);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(80, 13);
			this->label18->TabIndex = 13;
			this->label18->Text = L"Precision depth";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(266, 93);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(21, 13);
			this->label15->TabIndex = 12;
			this->label15->Text = L"nm";
			// 
			// m_precisionLogTF
			// 
			this->m_precisionLogTF->Location = System::Drawing::Point(137, 89);
			this->m_precisionLogTF->Name = L"m_precisionLogTF";
			this->m_precisionLogTF->Size = System::Drawing::Size(122, 20);
			this->m_precisionLogTF->TabIndex = 11;
			this->m_precisionLogTF->Leave += gcnew System::EventHandler(this, &RFSettingsForm::m_precisionLogTF_Leave);
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(11, 92);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(67, 13);
			this->label16->TabIndex = 10;
			this->label16->Text = L"Precision log";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(266, 23);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(15, 13);
			this->label1->TabIndex = 6;
			this->label1->Text = L"%";
			// 
			// m_overlapThresholdTF
			// 
			this->m_overlapThresholdTF->Location = System::Drawing::Point(137, 19);
			this->m_overlapThresholdTF->Name = L"m_overlapThresholdTF";
			this->m_overlapThresholdTF->Size = System::Drawing::Size(122, 20);
			this->m_overlapThresholdTF->TabIndex = 5;
			this->m_overlapThresholdTF->Leave += gcnew System::EventHandler(this, &RFSettingsForm::m_overlapThresholdTF_Leave);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(11, 26);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(90, 13);
			this->label4->TabIndex = 4;
			this->label4->Text = L"Overlap threshold";
			// 
			// m_okB
			// 
			this->m_okB->Location = System::Drawing::Point(7, 368);
			this->m_okB->Name = L"m_okB";
			this->m_okB->Size = System::Drawing::Size(152, 23);
			this->m_okB->TabIndex = 7;
			this->m_okB->Text = L"OK";
			this->m_okB->UseVisualStyleBackColor = true;
			this->m_okB->Click += gcnew System::EventHandler(this, &RFSettingsForm::m_okB_Click);
			// 
			// m_cancelB
			// 
			this->m_cancelB->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->m_cancelB->Location = System::Drawing::Point(161, 368);
			this->m_cancelB->Name = L"m_cancelB";
			this->m_cancelB->Size = System::Drawing::Size(152, 23);
			this->m_cancelB->TabIndex = 8;
			this->m_cancelB->Text = L"Cancel";
			this->m_cancelB->UseVisualStyleBackColor = true;
			// 
			// RFSettingsForm
			// 
			this->AcceptButton = this->m_okB;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->CancelButton = this->m_cancelB;
			this->ClientSize = System::Drawing::Size(320, 397);
			this->Controls->Add(this->m_cancelB);
			this->Controls->Add(this->m_okB);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->m_dataThresholdTF);
			this->Controls->Add(this->label5);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->Name = L"RFSettingsForm";
			this->Text = L"r(f) Settings";
			this->TopMost = true;
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void m_okB_Click(System::Object^  sender, System::EventArgs^  e)
	{
		try
		{
			IHMToData();
			Hide();
		}
		catch (Exception^)
		{
			MessageBox::Show("Invalid data.");
		}
	}

	private: System::Void m_referenceThresholdTF_Leave(System::Object^  sender, System::EventArgs^  e) {
		String^ defaultValue = "-65";
		try
		{
			if (Convert::ToDecimal(m_referenceThresholdTF->Text) < -100 ||
				Convert::ToDecimal(m_referenceThresholdTF->Text) > 0)
			{
				m_referenceThresholdTF->Text = defaultValue;
			}
		}
		catch (...)
		{
			// Valeur incoh�rente, on met une valeur par defaut
			m_referenceThresholdTF->Text = defaultValue;
		}
	}
	private: System::Void m_dataThresholdTF_Leave(System::Object^  sender, System::EventArgs^  e) {
		String^ defaultValue = "-75";
		try
		{
			if (Convert::ToDecimal(m_dataThresholdTF->Text) < -100 ||
				Convert::ToDecimal(m_dataThresholdTF->Text) > 0)
			{
				m_dataThresholdTF->Text = defaultValue;
			}
		}
		catch (...)
		{
			// Valeur incoh�rente, on met une valeur par defaut
			m_dataThresholdTF->Text = defaultValue;
		}
	}
	private: System::Void m_zGridSpaceTF_Leave(System::Object^  sender, System::EventArgs^  e) {
		String^ defaultValue = "0.01";
		try
		{
			if (Convert::ToDecimal(m_zGridSpaceTF->Text) < 0)
			{
				m_zGridSpaceTF->Text = defaultValue;
			}
		}
		catch (...)
		{
			// Valeur incoh�rente, on met une valeur par defaut
			m_zGridSpaceTF->Text = defaultValue;
		}
	}
	private: System::Void m_xyAngleFactorTF_Leave(System::Object^  sender, System::EventArgs^  e) {
		String^ defaultValue = "1";
		try
		{
			if (Convert::ToDecimal(m_xyAngleFactorTF->Text) < 0 ||
				Convert::ToDecimal(m_xyAngleFactorTF->Text) > 45)
			{
				m_xyAngleFactorTF->Text = defaultValue;
			}
		}
		catch (...)
		{
			// Valeur incoh�rente, on met une valeur par defaut
			m_xyAngleFactorTF->Text = defaultValue;
		}
	}
	private: System::Void m_refWindowTF_Leave(System::Object^  sender, System::EventArgs^  e) {
		String^ defaultValue = "5";
		try
		{
			if (Convert::ToDecimal(m_refWindowTF->Text) < 0)
			{
				m_refWindowTF->Text = defaultValue;
			}
		}
		catch (...)
		{
			// Valeur incoh�rente, on met une valeur par defaut
			m_refWindowTF->Text = defaultValue;
		}
	}
	private: System::Void m_precisionLogTF_Leave(System::Object^  sender, System::EventArgs^  e) {
		String^ defaultValue = "0.005";
		try
		{
			if (Convert::ToDecimal(m_precisionLogTF->Text) < 0)
			{
				m_precisionLogTF->Text = defaultValue;
			}
		}
		catch (...)
		{
			// Valeur incoh�rente, on met une valeur par defaut
			m_precisionLogTF->Text = defaultValue;
		}
	}
	private: System::Void m_precisionDepthTF_Leave(System::Object^  sender, System::EventArgs^  e) {
		String^ defaultValue = "5";
		try
		{
			if (Convert::ToDecimal(m_precisionDepthTF->Text) < 0)
			{
				m_precisionDepthTF->Text = defaultValue;
			}
		}
		catch (...)
		{
			// Valeur incoh�rente, on met une valeur par defaut
			m_precisionDepthTF->Text = defaultValue;
		}
	}
	private: System::Void m_overlapThresholdTF_Leave(System::Object^  sender, System::EventArgs^  e) {
		String^ defaultValue = "60";
		try
		{
			if (Convert::ToDecimal(m_overlapThresholdTF->Text) > 100 ||
				Convert::ToDecimal(m_overlapThresholdTF->Text) < 0)
			{
				m_overlapThresholdTF->Text = defaultValue;
			}
		}
		catch (...)
		{
			// Valeur incoh�rente, on met une valeur par defaut
			m_overlapThresholdTF->Text = defaultValue;
		}
	}
	private: System::Void m_echoesMinNbTF_Leave(System::Object^  sender, System::EventArgs^  e) {
		String^ defaultValue = "10";
		try
		{
			if (Convert::ToDecimal(m_echoesMinNbTF->Text) < 0)
			{
				m_echoesMinNbTF->Text = defaultValue;
			}
		}
		catch (...)
		{
			// Valeur incoh�rente, on met une valeur par defaut
			m_echoesMinNbTF->Text = defaultValue;
		}
	}
	private: System::Void m_enableOverlapCB_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

		bool enabled = m_enableOverlapCB->Checked;

		m_referenceThresholdTF->Enabled = enabled;
		m_zGridSpaceTF->Enabled = enabled;
		m_xyAngleFactorTF->Enabled = enabled;
		m_refWindowTF->Enabled = enabled;
	}
	};
}
