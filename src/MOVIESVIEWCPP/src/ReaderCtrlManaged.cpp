#include "ReaderCtrlManaged.h"

#include "M3DKernel/algorithm/base/AlgorithmOutput.h"
#include "M3DKernel/algorithm/base/EchoAlgorithmRoot.h"

#include "Reader/ReaderParameter.h"
#include "Reader/MovReadService.h"
#include "ShoalExtraction/ShoalExtractionModule.h"
#include "M3DKernel/M3DKernel.h"

using namespace System;
using namespace System::Threading;

ReaderCtrlManaged::ReaderCtrlManaged()
{
	m_ReadState = ePause;

	m_pReaderCtrl = ReaderCtrl::getInstance();

	m_timeOut = 100;

	M3DKernel *pInstance = M3DKernel::GetInstance();
	KernelParameter param = pInstance->GetRefKernelParameter();
	param.setIgnorePhase(true);
	pInstance->UpdateKernelParameter(param);
}

ReaderCtrlManaged::~ReaderCtrlManaged(void)
{
	ReaderCtrl::FreeMemory();
}

ReaderCtrlManaged ^ ReaderCtrlManaged::Instance() 
{
	if (instance == nullptr) {
		instance = gcnew ReaderCtrlManaged();
	}
	return instance;
}

void ReaderCtrlManaged::setDelegateReadStateChangedCallback(DelegateReadStateChanged ^aDelegateReadState) {
	m_DelegateReadStateChanged = aDelegateReadState;
	m_DelegateReadStateChanged->Invoke(m_ReadState);
}

void ReaderCtrlManaged::setDelegateReadEventChangedCallback(DelegateReadEventChanged ^aDelegateReadEvent) {
	m_DelegateReadEventChanged = aDelegateReadEvent;
}

void ReaderCtrlManaged::setDelegateReadStateChangingCallback(DelegateReadStateChanging ^aDeletageReadStateChanging) {
	m_DelegateReadStateChanging = aDeletageReadStateChanging;
}


void ReaderCtrlManaged::Pause()
{
	if (m_ReadState != ePause)
	{
		OnStateChanging(m_ReadState, ePause);
		ChunckEventList	eventList;
		m_pReaderCtrl->AbortReadChunck(eventList);
		m_DelegateReadEventChanged->Invoke(eventList);
		m_ReadState = ePause;
		OnStateChanged();
	}
}

void ReaderCtrlManaged::ReadCont()
{
	if (m_ReadState != eReadCont)
	{
		OnStateChanging(m_ReadState, eReadCont);
		m_ReadState = eReadCont;
		m_pReaderCtrl->StartReadChunck();
		OnStateChanged();
	}
}

void ReaderCtrlManaged::ReadOne()
{
	if (m_ReadState != eReadOne)
	{
		OnStateChanging(m_ReadState, eReadOne);
		m_pReaderCtrl->StartReadChunck();
		m_ReadState = eReadOne;
		OnStateChanged();
	}
}

void ReaderCtrlManaged::ReadOnlyOneSync()
{
	if (m_ReadState != eReadOne)
	{
		OnStateChanging(m_ReadState, eReadOne);
		m_ReadState = eReadOne;
		OnStateChanged();

		ReaderParameter Readparam = m_pReaderCtrl->GetReaderParameter();
		unsigned int currentChunkSize = Readparam.m_ChunckDef.m_NumberOfFanToRead;
		Readparam.m_ChunckDef.m_NumberOfFanToRead = 1;
		m_pReaderCtrl->UpdateReaderParameter(Readparam);
		m_pReaderCtrl->SyncReadChunk();
		Pause();

		Readparam.m_ChunckDef.m_NumberOfFanToRead = currentChunkSize;
		m_pReaderCtrl->UpdateReaderParameter(Readparam);
	}
}

// OTK - 02/03/2009 - implémentation du GoTo
bool ReaderCtrlManaged::GoTo(GoToTarget target)
{
	return m_pReaderCtrl->GoTo(target);
}

bool ReaderCtrlManaged::UpdateEvent()
{
	bool paused = true;
	M3DKernel *pKernel = M3DKernel::GetInstance();
	if (m_ReadState != ePause)
	{
		paused = false;
		ChunckEventList refEvent;
		bool reading = m_pReaderCtrl->CheckChunckWasRead(refEvent);
		if (reading)
		{
			m_DelegateReadEventChanged->Invoke(refEvent);
			if (m_pReaderCtrl->GetActiveService())
			{
				pKernel->Lock();
				bool ServiceStatus = m_pReaderCtrl->GetActiveService()->IsClosed();
				pKernel->Unlock();

				if (ServiceStatus)
				{
					m_ReadState = ePause;
					OnStateChanged();
				}
				else
				{
					if (m_ReadState == eReadOne)
					{
						m_ReadState = ePause;
						OnStateChanged();
					}
					else
					{
						m_pReaderCtrl->StartReadChunck();
					}
				}
			}
			else
			{
				m_ReadState = ePause;
				OnStateChanged();
			}

		}
	}

	return paused;
}

bool ReaderCtrlManaged::IsFileService()
{
	return m_pReaderCtrl->IsFileService();
}

MovFileRun ReaderCtrlManaged::GetFileRun()
{
	return m_pReaderCtrl->GetFileRun();
}

// détermination des limites du filerun en temps et en numero de ping
bool ReaderCtrlManaged::GetLimits(HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing)
{
	return m_pReaderCtrl->GetLimits(minTime, minPing, maxTime, maxPing);
}

MovReadService * ReaderCtrlManaged::GetReadService()
{
	return m_pReaderCtrl->GetActiveService();
}

void ReaderCtrlManaged::OpenFileRun(MovFileRun &fileRun)
{
	m_pReaderCtrl->OpenFileStream(fileRun);
	ReadOne();
}

void ReaderCtrlManaged::OpenNetworkStream()
{
	//Be careful, the adress here is only used to display messages in the log. 
	m_pReaderCtrl->OpenNetworkStream(255, 255, 255, 255);
	ReadCont();
}

void ReaderCtrlManaged::OnStateChanged()
{
	if (m_DelegateReadStateChanged)
		m_DelegateReadStateChanged->Invoke(m_ReadState);
}

void ReaderCtrlManaged::OnStateChanging(ReadState oldState, ReadState newState)
{
	if (m_DelegateReadStateChanging)
		m_DelegateReadStateChanging->Invoke(oldState, newState);
}


