#pragma once
#include "vtkWrapper.h"

#include "MergedEchoList.h"
#include "Reader/MovFileRun.h"
#include "Reader/ChunckEventList.h"
#include "Reader/readerctrl.h"

class NavigationDataHook;
/***
 Cette classe gere les commandes de lecture

*/

enum ReadState
{
	ePause,
	eReadCont,
	eReadOne,
};

public delegate void DelegateReadStateChanging(ReadState oldValue, ReadState newValue);
public delegate void DelegateReadStateChanged(ReadState newValue);
public delegate void DelegateReadEventChanged(const ChunckEventList &newValue);

public ref class ReaderCtrlManaged
{
public:

	// the lock system between read threads and gui thread, use it carefully

private :
	ReaderCtrlManaged();
	virtual ~ReaderCtrlManaged();
	static ReaderCtrlManaged ^ instance = nullptr;

public:
	static ReaderCtrlManaged ^ Instance();
	
	void setDelegateReadStateChangedCallback(DelegateReadStateChanged ^aDelegateReadState);
	void setDelegateReadEventChangedCallback(DelegateReadEventChanged ^aDelegateReadEvent);
	void setDelegateReadStateChangingCallback(DelegateReadStateChanging ^aDeletageReadStateChanging);

	void Pause();
	void ReadCont();
	void ReadOne();
	void ReadOnlyOneSync();
	bool GoTo(GoToTarget target); // OTK - 02/03/2009 - implémentation du GoTo

	bool IsPause() { return m_ReadState == ePause; }
	bool IsReadCont() { return m_ReadState == eReadCont; }
	bool IsReadOne() { return m_ReadState == eReadOne; }

	void OpenFileRun(MovFileRun &fileRun);
	void OpenNetworkStream();
	void Exit() {};

	bool UpdateEvent();

	bool IsFileService();
	
	MovFileRun GetFileRun();

	// Détermination des limites du filerun en temps et en numero de ping
	bool GetLimits(HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing);

	// acces sur le service de lecture pour modifications des fichiers lus
	// (édition manuelle du fond, effacement d'échos...)
	MovReadService * GetReadService();

private:
	void OnStateChanged();
	void OnStateChanging(ReadState oldState, ReadState newState);
	ReadState m_ReadState;

	DelegateReadStateChanging ^m_DelegateReadStateChanging;
	DelegateReadStateChanged ^m_DelegateReadStateChanged;
	DelegateReadEventChanged ^m_DelegateReadEventChanged;

	int		m_timeOut;
	System::Threading::Thread ^m_ReadThread;
	ReaderCtrl *m_pReaderCtrl;
};
