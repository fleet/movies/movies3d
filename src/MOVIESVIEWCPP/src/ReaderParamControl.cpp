#include "ReaderParamControl.h"

#include "Reader/ReaderCtrl.h"

using namespace MOVIESVIEWCPP;

void ReaderParamControl::UpdateConfiguration()
{
	ReaderCtrl *pRead = ReaderCtrl::getInstance();
	ReaderParameter Readparam = pRead->GetReaderParameter();

	System::Int32 ^ref3 = System::Convert::ToInt32(textFrameToRead->Text);
	Readparam.m_ChunckDef.m_NumberOfFanToRead = (int)ref3;
	ref3 = System::Convert::ToInt32(textBoxRefSounder->Text);
	Readparam.m_ChunckDef.m_SounderId = (int)ref3;
	Readparam.setAcquisitionPort(Decimal::ToInt32(udAcqusitionPort->Value));
	Readparam.setCallbackPort(Decimal::ToInt32(udCallbackPort->Value));

	pRead->UpdateReaderParameter(Readparam, false);
}

void ReaderParamControl::UpdateGUI()
{
	ReaderCtrl *pRead = ReaderCtrl::getInstance();
	this->textFrameToRead->Text = (gcnew System::Int32(pRead->GetReaderParameter().m_ChunckDef.m_NumberOfFanToRead))->ToString();
	this->textBoxRefSounder->Text = (gcnew System::Int32(pRead->GetReaderParameter().m_ChunckDef.m_SounderId))->ToString();

	udAcqusitionPort->Value = pRead->GetReaderParameter().getAcquisitionPort();
	udCallbackPort->Value = pRead->GetReaderParameter().getCallbackPort();
}

