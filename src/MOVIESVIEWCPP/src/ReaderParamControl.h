#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de ReaderParamControl
	/// </summary>
	public ref class ReaderParamControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		ReaderParamControl(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~ReaderParamControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected:
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textFrameToRead;
	private: System::Windows::Forms::Label^  labelRefSounder;
	private: System::Windows::Forms::TextBox^  textBoxRefSounder;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::NumericUpDown^  udAcqusitionPort;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::NumericUpDown^  udCallbackPort;



	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->labelRefSounder = (gcnew System::Windows::Forms::Label());
			this->textBoxRefSounder = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textFrameToRead = (gcnew System::Windows::Forms::TextBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->udCallbackPort = (gcnew System::Windows::Forms::NumericUpDown());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->udAcqusitionPort = (gcnew System::Windows::Forms::NumericUpDown());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udCallbackPort))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udAcqusitionPort))->BeginInit();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->labelRefSounder);
			this->groupBox1->Controls->Add(this->textBoxRefSounder);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->textFrameToRead);
			this->groupBox1->Dock = System::Windows::Forms::DockStyle::Top;
			this->groupBox1->Location = System::Drawing::Point(0, 0);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(237, 87);
			this->groupBox1->TabIndex = 11;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Chunk Definition";
			// 
			// labelRefSounder
			// 
			this->labelRefSounder->AutoSize = true;
			this->labelRefSounder->Location = System::Drawing::Point(115, 54);
			this->labelRefSounder->Name = L"labelRefSounder";
			this->labelRefSounder->Size = System::Drawing::Size(114, 13);
			this->labelRefSounder->TabIndex = 11;
			this->labelRefSounder->Text = L"Reference Sounder ID";
			// 
			// textBoxRefSounder
			// 
			this->textBoxRefSounder->Location = System::Drawing::Point(9, 51);
			this->textBoxRefSounder->Name = L"textBoxRefSounder";
			this->textBoxRefSounder->Size = System::Drawing::Size(100, 20);
			this->textBoxRefSounder->TabIndex = 10;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(115, 24);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(89, 13);
			this->label3->TabIndex = 9;
			this->label3->Text = L"Nb Frame to read";
			// 
			// textFrameToRead
			// 
			this->textFrameToRead->Location = System::Drawing::Point(9, 21);
			this->textFrameToRead->Name = L"textFrameToRead";
			this->textFrameToRead->Size = System::Drawing::Size(100, 20);
			this->textFrameToRead->TabIndex = 5;
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->label2);
			this->groupBox2->Controls->Add(this->udCallbackPort);
			this->groupBox2->Controls->Add(this->label1);
			this->groupBox2->Controls->Add(this->udAcqusitionPort);
			this->groupBox2->Dock = System::Windows::Forms::DockStyle::Top;
			this->groupBox2->Location = System::Drawing::Point(0, 87);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(237, 79);
			this->groupBox2->TabIndex = 12;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Acquisition";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(115, 47);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(70, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Callback Port";
			// 
			// udCallbackPort
			// 
			this->udCallbackPort->Location = System::Drawing::Point(9, 45);
			this->udCallbackPort->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 65535, 0, 0, 0 });
			this->udCallbackPort->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1024, 0, 0, 0 });
			this->udCallbackPort->Name = L"udCallbackPort";
			this->udCallbackPort->Size = System::Drawing::Size(100, 20);
			this->udCallbackPort->TabIndex = 2;
			this->udCallbackPort->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 42001, 0, 0, 0 });
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(115, 23);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(69, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Network Port";
			// 
			// udAcqusitionPort
			// 
			this->udAcqusitionPort->Location = System::Drawing::Point(9, 19);
			this->udAcqusitionPort->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 65535, 0, 0, 0 });
			this->udAcqusitionPort->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1024, 0, 0, 0 });
			this->udAcqusitionPort->Name = L"udAcqusitionPort";
			this->udAcqusitionPort->Size = System::Drawing::Size(100, 20);
			this->udAcqusitionPort->TabIndex = 0;
			this->udAcqusitionPort->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 42000, 0, 0, 0 });
			// 
			// ReaderParamControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Name = L"ReaderParamControl";
			this->Size = System::Drawing::Size(237, 185);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udCallbackPort))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udAcqusitionPort))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

#pragma region Impl�mentation de IParamControl
	public: virtual void UpdateConfiguration();
	public: virtual void UpdateGUI();
#pragma endregion
private: System::Void udAcqusitionPort_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
}
};
}
