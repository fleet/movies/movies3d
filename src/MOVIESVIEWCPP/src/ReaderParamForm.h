// -*- MC++ -*-
// ****************************************************************************
// Class: ReaderParamForm
//
// Description: Fen�tre de configuration du reader
//
// Projet: MOVIES3D
// Auteur: O.Tonck
// Date  : Janvier 2010
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "ParameterForm.h"
#include "ReaderParamControl.h"

namespace MOVIESVIEWCPP {
	public ref class ReaderParamForm : public ParameterForm
	{
	public:
		ReaderParamForm(void) :
			ParameterForm()
		{
			SetParamControl(gcnew ReaderParamControl(), L"Reader Parameters");
		}
	};
};
