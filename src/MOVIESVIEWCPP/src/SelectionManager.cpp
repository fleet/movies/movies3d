
#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/pingdata.h"
#include "ShoalExtraction/ShoalExtractionOutput.h"

#include "ModuleManager/ModuleManager.h"

#include "Compensation/CompensationModule.h"


#include "SelectionManager.h"
#include "ASyncTreatment.h"

#include <algorithm>

using namespace shoalextraction;




//*****************************************************************************
// Name : OnSelectionRange
// Description : notification of a region selection
// Parameters : void
// Return : void
//*****************************************************************************
void SelectionManager::OnSelectionRange(ViewSelectionRange^ range, bool fromShoal)
{
	m_WindowSelection = range;

	// si l'evenement provient de la selection d'une fen�tre rectangulaire,
	// on annule la selection des bancs
	if (!fromShoal)
	{
		// d�s�lection de tous les bancs
		ResetSelectShoals();
	}

	// mise � jour du flag de nouvelles donn�es dans la s�lection
	m_HasNewData = true;

	SelectionChanged(this);
}

void SelectionManager::OnSelectionPoint(ViewSelectionPolarPoint^ point, System::String^ transducerName)
{
	//if the tview transducer is the reference transducer
	if (m_DisplayView != nullptr)
	{
		//    String^ transducerName = m_DisplayView->m_flatSideViewDisplay->GetTransducer();

		CModuleManager * pModuleMgr = CModuleManager::getInstance();
		CompensationModule * pModule = pModuleMgr->GetCompensationModule();

		String^ refTransducerName = cppStr2NetStr(pModule->GetCompensationParameter().m_rfParameter.GetRefTransducer().c_str());

		if (transducerName->Equals(refTransducerName))
		{
			//search the school matching the point coordinate
			ShoalData* pShoal = SelectShoalFromPoint(point);

			if (pShoal != NULL)
			{
				String ^shoalKey = Convert::ToString((std::uint64_t)pShoal->GetExternId());

				int nbPings = pShoal->GetPings().size();
				ViewSelectionPoint^ minPoint = gcnew ViewSelectionPoint(pShoal->GetPings()[0]->GetPingId(), pShoal->GetBbox().GetZMin());
				ViewSelectionPoint^ maxPoint = gcnew ViewSelectionPoint(pShoal->GetPings()[nbPings - 1]->GetPingId(), pShoal->GetBbox().GetZMax());

				//if the shoal is not already added
				bool found = false;
				int itemsCount = m_ShoalSelection->Count;
				int iItem = 0;
				for (iItem = 0; iItem < itemsCount && !found; iItem++)
				{
					found = m_ShoalSelection[iItem]->Equals(shoalKey);
					if (found)
					{
						RemoveShoal(iItem);
						// si c'etait le seul banc, on n'a plus de s�lection
						if (itemsCount == 1)
						{
							m_WindowSelection = nullptr;
						}
					}
				}

				if (!found)
				{
					// il s'agit d'un nouveau banc
					m_HasNewData = true;

					m_ShoalSelection->Add(shoalKey);

					ViewSelectionRange^ range = gcnew ViewSelectionRange();
					range->min = minPoint;
					range->max = maxPoint;

					if (itemsCount > 0)
					{
						IncreaseSelectionRange(range);
					}
					else
					{
						OnSelectionRange(range, true);
					}
				}
			}
		}
		/*
		else
		{
		  MessageBox::Show("The selected transducer is not the reference transducer.");
		}
		*/
	}
}

void SelectionManager::OnSelectionError()
{
	// pas de banc sur le point cliqu�...
}

void SelectionManager::IncreaseSelectionRange(ViewSelectionRange^ range)
{
	const double windowSelection_mindepth = m_WindowSelection->min->depth;
	const double windowSelection_minping = m_WindowSelection->min->ping;

	const double windowSelection_maxdepth = m_WindowSelection->max->depth;
	const double windowSelection_maxping = m_WindowSelection->max->ping;
	
	const double range_mindepth = range->min->depth;
	const double range_minping = range->min->ping;

	const double range_maxdepth = range->max->depth;
	const double range_maxping = range->max->ping;

	m_HasNewData |= (windowSelection_mindepth < range_mindepth)
		|| (windowSelection_minping < range_minping)
		|| (windowSelection_maxdepth > range_maxdepth)
		|| (windowSelection_maxping > range_maxping);
	
	range->min->depth = std::min(windowSelection_mindepth, range_mindepth);
	range->min->ping = std::min(windowSelection_minping, range_minping);

	range->max->depth = std::max(windowSelection_maxdepth, range_maxdepth);
	range->max->ping = std::max(windowSelection_maxping, range_maxping);

	OnSelectionRange(range, true);
}

void SelectionManager::ResetSelection()
{
	// reset de la selection des bancs
	ResetSelectShoals();

	// reset de la selection de zone 
	m_WindowSelection = nullptr;

	m_HasNewData = true;
}

//*****************************************************************************
// Name : SelectShoalFromPoint
// Description : select a shoal from a polar point
// Parameters : void
// Return : void
//*****************************************************************************
ShoalData* SelectionManager::SelectShoalFromPoint(ViewSelectionPolarPoint^ point)
{
	ShoalData* result = NULL;

	CModuleManager * pModuleMgr = CModuleManager::getInstance();
	CompensationModule * pModule = pModuleMgr->GetCompensationModule();


	ShoalExtractionOutput* pOutput = m_refShoalConsumer->GetConsumedList()->GetOutputAtPoint(
		pModule->GetCompensationParameter().m_rfParameter.GetRefTransducer(),
		point->ping, point->beam, point->echoIdx);

	if (pOutput != NULL)
	{
		result = pOutput->m_pClosedShoal;
		SelectShoal(result, true);
	}

	return result;
}

// ***************************************************************************
/**
 * select the shoal
 *
 * @date   21/07/2009 - Fran�ois Racap�(IPSIS) - Cr�ation
 * @return bool
 */
 // ***************************************************************************
void SelectionManager::SelectShoal(shoalextraction::ShoalData* pShoal, bool select)
{
	pShoal->SetSelected(select);

	//
	  //refresh display

	/*
	  m_DisplayView->m_flatSideViewDisplay->m_viewContainer->m_sideViewContainer->GetActiveView()->
		  RefreshShoal(pShoal);
	  m_DisplayView->m_flatSideViewDisplay->DrawNow();
	*/

	/*  TransducerContainerFlatSideView ^ sideViewContainer = m_DisplayView->m_flatSideViewDisplay->m_viewContainer->m_sideViewContainer;

	  for(int iView=0; iView < sideViewContainer->ViewCount(); ++iView){
		TransducerFlatView * view =  sideViewContainer->GetView(iView);
		if(view->IsActive()){
		  view->RefreshShoal();
		}
	  }
	  */

	  // TODONMD : mettre une �v�nement plut�t

	for each(MOVIESVIEWCPP::BaseFlatSideView ^ view in m_DisplayView->FlatSideViews)
	{
		TransducerFlatView * transducerView = (TransducerFlatView *)view->GetTransducerView();
		transducerView->RefreshShoal(pShoal);
	}
	SelectionChanged(this);
	/*
	for each( MOVIESVIEWCPP::BaseFlatSideView ^ view in m_DisplayView->m_flatSideViewsDisplay)
	{
	  TransducerFlatView * transducerView = (TransducerFlatView *) view->GetTransducerView();
	  transducerView->RefreshShoal(pShoal);
	  view->DrawNow();
	}
	*/


}

// ***************************************************************************
/**
 * get a shoal from its extern id
 *
 * @date   21/07/2009 - Fran�ois Racap�(IPSIS) - Cr�ation
 * @return shoalextraction::ShoalData*
 */
 // ***************************************************************************
shoalextraction::ShoalData* SelectionManager::GetShoalFromId(std::uint64_t id)
{
	shoalextraction::ShoalData* result = NULL;

	//search shoal id in shoal database reference
	for (unsigned int iShoalRef = 0; iShoalRef < m_refShoalConsumer->GetShoalCount() && result == NULL; iShoalRef++)
	{
		result = m_refShoalConsumer->GetShoalIdx(iShoalRef)->m_pClosedShoal;
		if (id != result->GetExternId())
		{
			result = NULL;
		}
	}

	return result;
}

//*****************************************************************************
// Name : ResetSelectShoals
// Description : reset selected shoals
// Parameters : void
// Return : void
//*****************************************************************************
void SelectionManager::ResetSelectShoals()
{
	while (m_ShoalSelection->Count > 0)
	{
		RemoveShoal(0);
	}
}

//*****************************************************************************
// Name : RemoveShoal
// Description : remove the shoal from the selected shoal list view
// Parameters : void
// Return : void
//*****************************************************************************
void SelectionManager::RemoveShoal(int index)
{
	//get shoal id
	std::uint64_t shoalId = Convert::ToInt64(m_ShoalSelection[index]);

	//search shoal id in shoal database reference
	shoalextraction::ShoalData* pShoal = GetShoalFromId(shoalId);
	if (pShoal != NULL)
	{
		//refresh shoal display
		SelectShoal(pShoal, false);
	}

	m_ShoalSelection->RemoveAt(index);
}

// indique au module de RF les bancs sur lesquels il doit travailler
void SelectionManager::ConfigureShoalList(CompensationModule * pCompModule)
{
	for (int iShoal = 0; iShoal < m_ShoalSelection->Count; iShoal++)
	{
		//get shoal id
		std::uint64_t shoalId = Convert::ToInt64(m_ShoalSelection[iShoal]);

		//search shoal id in shoal database reference
		ShoalData* pRefShoal = GetShoalFromId(shoalId);

		if (pRefShoal != NULL)
		{
			int nbPings = pRefShoal->GetPings().size();
			int minPing = pRefShoal->GetPings()[0]->GetPingId();
			int maxPing = pRefShoal->GetPings()[nbPings - 1]->GetPingId();


			pCompModule->GetCompensationParameter().m_rfParameter.GetShoals().push_back(pRefShoal);
		}
	}
}

String^ SelectionManager::GetTransducerRefName()
{
	CompensationModule* pCompModule = CModuleManager::getInstance()->GetCompensationModule();
	return cppStr2NetStr(pCompModule->GetCompensationParameter().m_rfParameter.GetRefTransducer().c_str());
}

DataFmt SelectionManager::GetDataThreshold()
{
	CompensationModule* pCompModule = CModuleManager::getInstance()->GetCompensationModule();
	return pCompModule->GetCompensationParameter().m_overlapParameter.GetDataThreshold();
}

/*
ShoalData * SelectionManager::GetShoalAtPoint(ViewSelectionPolarPoint^point, const std::string & refTransducerName)
{
  ShoalData * pShoal = NULL;

  //if the tview transducer is the reference transducer
  if(m_DisplayView != nullptr)
  {
	String^ transducerName = m_DisplayView->m_flatSideViewDisplay->GetTransducer();
	String ^ _refTransducerName = cppStr2NetStr(refTransducerName.c_str());

	if(transducerName->Equals(_refTransducerName))
	{
	  //search the school matching the point coordinate
	  CModuleManager * pModuleMgr = CModuleManager::getInstance();
	  CompensationModule * pModule = pModuleMgr->GetCompensationModule();

	  ShoalExtractionOutput* pOutput = m_refShoalConsumer->GetConsumedList()->GetOutputAtPoint(
		refTransducerName, point->ping, point->beam, point->echoIdx);

	  if(pOutput != NULL)
	  {
		pShoal = pOutput->m_pClosedShoal;
	  }
	}
  }

  return pShoal;
}
*/