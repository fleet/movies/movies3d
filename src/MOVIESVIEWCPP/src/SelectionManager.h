// -*- MC++ -*-
// ****************************************************************************
// Class: SelectionManager
//
// Description: Classe contenant la d�finition de la s�lection active. Une
// S�lection est un ou plusieurs bancs, ou bien une r�gion rectangulaire
// d�finie par un pingmin, pingmax, depthmin et depthmax
//
// Projet: MOVIES3D
// Auteur: O.TONCK
// Date  : Octobre 2009
// Soci�t� : IPSIS
// ****************************************************************************
#include "IViewSelectionObserver.h"

#include "DisplayView.h"
#include "ShoalExtractionConsumer.h"



public ref class SelectionManager : public IViewSelectionObserver
{
public:
	SelectionManager(ShoalExtractionConsumer^ shoalConsumer)
	{
		m_refShoalConsumer = shoalConsumer;
		m_ShoalSelection = gcnew ArrayList();

		ResetSelection();
	}

public:
	virtual void OnSelectionRange(ViewSelectionRange^ range, bool fromShoal);
	virtual void OnSelectionPoint(ViewSelectionPolarPoint^ point, System::String^ transducerName);
	virtual void OnSelectionError();
	virtual void IncreaseSelectionRange(ViewSelectionRange^ range);

	//virtual shoalextraction::ShoalData * GetShoalAtPoint(ViewSelectionPolarPoint^point, const std::string & refTransducerName);

	virtual void ResetSelection();

	virtual ViewSelectionRange^ GetSelection() { return m_WindowSelection; };

	virtual bool HasNewData() { return m_HasNewData; };
	virtual void SetHasNewData(bool newData) { m_HasNewData = newData; };

	virtual void SetDisplayView(DisplayView^ displayView) { m_DisplayView = displayView; };

	virtual void ConfigureShoalList(CompensationModule * pCompModule);

	virtual void ResetSelectShoals();

	virtual bool IsShoalSelection() { return (m_ShoalSelection->Count > 0); };
	virtual String^ GetTransducerRefName();
	virtual DataFmt GetDataThreshold();

	virtual void SelectShoal(shoalextraction::ShoalData* pShoal, bool select);

	virtual event IViewSelectionObserver::SelectionChangedHandler ^ SelectionChanged;

private:

	shoalextraction::ShoalData* SelectShoalFromPoint(ViewSelectionPolarPoint^ point);

	shoalextraction::ShoalData* GetShoalFromId(std::uint64_t id);
	void RemoveShoal(int index);


	// permet de savoir si la s�lection s'est agrandie (permet d'�viter de faire plusieurs fois des calculs lourds sur 
	// la s�lection, par exemple le calcul d'Overlap.
	bool m_HasNewData;

	// fen�tre de s�lection rectangulaire
	ViewSelectionRange^ m_WindowSelection;
	// liste des bancs s�lectionn�s
	Collections::ArrayList^ m_ShoalSelection;

	DisplayView^ m_DisplayView;
	ShoalExtractionConsumer^ m_refShoalConsumer;
};
