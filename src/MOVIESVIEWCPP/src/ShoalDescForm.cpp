
#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/shoalstat.h"
#include "ShoalExtraction/data/shoalid.h"
#include "ShoalExtraction/data/pingdata.h"

#include "ShoalDescForm.h"

using namespace MOVIESVIEWCPP;

System::Void ShoalDescForm::DisplayItemDesc(ShoalExtractionOutput *pIn)
{
	listViewDesc->BeginUpdate();
	CObjectDisplay ^refDisplay = gcnew CObjectDisplay(listViewDesc);
	this->listViewDesc->Items->Clear();
	refDisplay->m_defaultColor = System::Drawing::Color::Red;

	refDisplay->AddInt32(pIn->m_pClosedShoal->GetExternId(), "shoalId");
	refDisplay->m_defaultColor = System::Drawing::Color::Black;
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetVolume(), "totalVolume");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetMeanSv(), "SV");
	refDisplay->AddInt32(pIn->m_pClosedShoal->GetShoalStat()->GetNbEchos(), "nbOfEchos");
	refDisplay->AddInt32(pIn->m_pClosedShoal->GetSounderId(), "sounderId");
	refDisplay->AddInt32(pIn->m_pClosedShoal->GetTransId(), "transducerId");
	refDisplay->AddString((char*)pIn->m_pClosedShoal->GetTransName().c_str(), "transducerName");
	refDisplay->AddBool(pIn->m_pClosedShoal->GetShoalId()->GetClosed() == 0 ? false : true, "Closed");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetMinDepth(), "minDepth");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetMaxDepth(), "maxDepth");
	refDisplay->AddInt64(pIn->m_pClosedShoal->GetShoalStat()->GetBeginTime().m_TimeCpu, "startTimeS");
	refDisplay->AddInt32(pIn->m_pClosedShoal->GetShoalStat()->GetBeginTime().m_TimeFraction * 10, "startTimeMs");
	refDisplay->AddInt64(pIn->m_pClosedShoal->GetShoalStat()->GetEndTime().m_TimeCpu, "endTimeS");
	refDisplay->AddInt32(pIn->m_pClosedShoal->GetShoalStat()->GetEndTime().m_TimeFraction * 10, "endTimeMs");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetMinBottomDistance(), "minDistToBottom");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetMaxBottomDistance(), "maxDistToBottom");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetLength(), "maxLengthAlong");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetWidth(), "maxLengthAcross");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetHeigth(), "maxHeight");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetPings()[0]->GetTransducers()[0]->GetBeamsSamplesSpacing(), "sampleSpacing");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetGeographicCenter().x, "geographicCenterLatitude");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetGeographicCenter().y, "geographicCenterLongitude");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetGeographicCenter().z, "geographicCenterDepth");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetGravityCenter().x, "energyCenterLatitude");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetGravityCenter().y, "energyCenterLongitude");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetGravityCenter().z, "energyCenterDepth");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetFirstPoint().x, "firstPointLatitude");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetFirstPoint().y, "firstPointLongitude");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetFirstPoint().z, "firstPointDepth");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetLastPoint().x, "lastPointLatitude");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetLastPoint().y, "lastPointLongitude");
	refDisplay->AddDouble(pIn->m_pClosedShoal->GetShoalStat()->GetLastPoint().z, "lastPointDepth");

	listViewDesc->EndUpdate();



}