#pragma once


using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


#include "ShoalExtractionConsumer.h"
#include "ShoalExtraction/ShoalExtractionOutput.h"
#include "ObjectDisplay.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de ShoalDescForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class ShoalDescForm : public System::Windows::Forms::Form
	{
	public:
		ShoalDescForm(ShoalExtractionConsumer ^ref) : m_refShoalConsumer(ref)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
			m_currentIndex = 0;
			UpdateShoal();
		}
	private:
		ShoalExtractionConsumer ^m_refShoalConsumer;
	private: System::Windows::Forms::ListView^  listViewDesc;

	private: System::Windows::Forms::ColumnHeader^  columnHeader1;
	private: System::Windows::Forms::ColumnHeader^  columnHeader2;
	private: System::Windows::Forms::Label^  labelShoalCount;

	private: unsigned int m_currentIndex;
	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~ShoalDescForm()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanel1;
	private: System::Windows::Forms::Button^  buttonPrevious;
	private: System::Windows::Forms::Button^  buttonNext;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->flowLayoutPanel1 = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->buttonPrevious = (gcnew System::Windows::Forms::Button());
			this->buttonNext = (gcnew System::Windows::Forms::Button());
			this->labelShoalCount = (gcnew System::Windows::Forms::Label());
			this->listViewDesc = (gcnew System::Windows::Forms::ListView());
			this->columnHeader1 = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnHeader2 = (gcnew System::Windows::Forms::ColumnHeader());
			this->tableLayoutPanel1->SuspendLayout();
			this->flowLayoutPanel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 1;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->flowLayoutPanel1, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->listViewDesc, 0, 1);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 2;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 40)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(393, 620);
			this->tableLayoutPanel1->TabIndex = 1;
			// 
			// flowLayoutPanel1
			// 
			this->flowLayoutPanel1->Controls->Add(this->buttonPrevious);
			this->flowLayoutPanel1->Controls->Add(this->buttonNext);
			this->flowLayoutPanel1->Controls->Add(this->labelShoalCount);
			this->flowLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->flowLayoutPanel1->Location = System::Drawing::Point(3, 3);
			this->flowLayoutPanel1->Name = L"flowLayoutPanel1";
			this->flowLayoutPanel1->Size = System::Drawing::Size(387, 34);
			this->flowLayoutPanel1->TabIndex = 1;
			// 
			// buttonPrevious
			// 
			this->buttonPrevious->Location = System::Drawing::Point(3, 3);
			this->buttonPrevious->Name = L"buttonPrevious";
			this->buttonPrevious->Size = System::Drawing::Size(75, 23);
			this->buttonPrevious->TabIndex = 0;
			this->buttonPrevious->Text = L"Previous";
			this->buttonPrevious->UseVisualStyleBackColor = true;
			this->buttonPrevious->Click += gcnew System::EventHandler(this, &ShoalDescForm::buttonPrevious_Click);
			// 
			// buttonNext
			// 
			this->buttonNext->Location = System::Drawing::Point(84, 3);
			this->buttonNext->Name = L"buttonNext";
			this->buttonNext->Size = System::Drawing::Size(75, 23);
			this->buttonNext->TabIndex = 1;
			this->buttonNext->Text = L"Next";
			this->buttonNext->UseVisualStyleBackColor = true;
			this->buttonNext->Click += gcnew System::EventHandler(this, &ShoalDescForm::buttonNext_Click);
			// 
			// labelShoalCount
			// 
			this->labelShoalCount->AutoSize = true;
			this->labelShoalCount->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelShoalCount->Location = System::Drawing::Point(165, 0);
			this->labelShoalCount->Name = L"labelShoalCount";
			this->labelShoalCount->Padding = System::Windows::Forms::Padding(10, 5, 5, 5);
			this->labelShoalCount->Size = System::Drawing::Size(95, 29);
			this->labelShoalCount->TabIndex = 2;
			this->labelShoalCount->Text = L"Shoal Count : 0";
			// 
			// listViewDesc
			// 
			this->listViewDesc->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(2) {
				this->columnHeader1,
					this->columnHeader2
			});
			this->listViewDesc->Dock = System::Windows::Forms::DockStyle::Fill;
			this->listViewDesc->GridLines = true;
			this->listViewDesc->HeaderStyle = System::Windows::Forms::ColumnHeaderStyle::Nonclickable;
			this->listViewDesc->Location = System::Drawing::Point(3, 43);
			this->listViewDesc->Name = L"listViewDesc";
			this->listViewDesc->Size = System::Drawing::Size(387, 574);
			this->listViewDesc->TabIndex = 2;
			this->listViewDesc->UseCompatibleStateImageBehavior = false;
			this->listViewDesc->View = System::Windows::Forms::View::Details;
			// 
			// columnHeader1
			// 
			this->columnHeader1->Text = L"data";
			this->columnHeader1->Width = 150;
			// 
			// columnHeader2
			// 
			this->columnHeader2->Text = L"value";
			this->columnHeader2->Width = 222;
			// 
			// ShoalDescForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(393, 620);
			this->Controls->Add(this->tableLayoutPanel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->Name = L"ShoalDescForm";
			this->Text = L"Shoal Description";
			this->TopMost = true;
			this->tableLayoutPanel1->ResumeLayout(false);
			this->flowLayoutPanel1->ResumeLayout(false);
			this->flowLayoutPanel1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void buttonNext_Click(System::Object^  sender, System::EventArgs^  e) {
		m_currentIndex++;
		UpdateShoal();
	}
	private: System::Void buttonPrevious_Click(System::Object^  sender, System::EventArgs^  e) {
		if (m_currentIndex > 0)
			m_currentIndex--;
		UpdateShoal();
	}
	private: System::Void UpdateShoal()
	{
		UpdateShoalCount();
		if (m_currentIndex > m_refShoalConsumer->GetShoalCount() - 1)
		{
			m_currentIndex = 0;
		}

		if (m_currentIndex < m_refShoalConsumer->GetShoalCount())
		{
			DisplayItemDesc(m_refShoalConsumer->GetShoalIdx(m_currentIndex));

		}
		else
		{
			listViewDesc->BeginUpdate();
			this->listViewDesc->Items->Clear();
			listViewDesc->EndUpdate();
		}

	}
	private: System::Void DisplayItemDesc(ShoalExtractionOutput *pIn);

	private: System::Void UpdateShoalCount()
	{
		labelShoalCount->Text = "Shoal Count :" + System::Int32(m_refShoalConsumer->GetShoalCount()).ToString();

	}
	};
}
