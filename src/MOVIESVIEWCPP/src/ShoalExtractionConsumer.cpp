
#include "ShoalExtractionConsumer.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "ModuleManager/ModuleManager.h"
#include "ShoalExtraction/ShoalExtractionModule.h"


#include "ShoalExtraction/ShoalExtractionOutput.h"

#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/shoalstat.h"
#include "ShoalExtraction/data/pingdata.h"
#include "ShoalExtraction/data/pingshoalstat.h"



ShoalExtractionConsumer::ShoalExtractionConsumer(void)
{
	CModuleManager *pMod = CModuleManager::getInstance();
	ShoalExtractionModule *pShoalModule = pMod->GetShoalExtractionModule();
	m_reservedConsumerId = pShoalModule->GetOutputContainer().ReserveConsumerId();
	m_pconsumedList = new ShoalExtractionOutputList();
}

ShoalExtractionConsumer::~ShoalExtractionConsumer(void)
{
	CModuleManager *pMod = CModuleManager::getInstance();
	ShoalExtractionModule *pShoalModule = pMod->GetShoalExtractionModule();
	pShoalModule->GetOutputContainer().RemoveConsumer(m_reservedConsumerId);
	CleanUp();
	delete m_pconsumedList;
}

bool ShoalExtractionConsumer::Consume(DisplayView^ displayView)
{
	bool result = false;
	CModuleManager *pMod = CModuleManager::getInstance();
	ShoalExtractionModule *pShoalModule = pMod->GetShoalExtractionModule();
	pShoalModule->GetOutputContainer().Lock();
	for (unsigned int i = 0; i < pShoalModule->GetOutputContainer().GetOutPutCount(m_reservedConsumerId); i++)
	{
		ShoalExtractionOutput* pOut = (ShoalExtractionOutput*)pShoalModule->GetOutputContainer().GetOutPut(m_reservedConsumerId, i);
		MovRef(pOut);
		m_pconsumedList->push_back(pOut);
		displayView->VolumicView->AddShoal(pOut);

		// affichage des bancs sur les vues 2D
		// TODONMD : mettre une �v�nement plut�t, a voir pour mutualiser avec SelectionManager::SelectShoal
		for each(MOVIESVIEWCPP::BaseFlatSideView ^ view in displayView->FlatSideViews)
		{
			TransducerFlatView * transducerView = (TransducerFlatView *)view->GetTransducerView();
			transducerView->RefreshShoal(pOut->m_pClosedShoal);
		}
		result = true;
	}
	pShoalModule->GetOutputContainer().Flush(m_reservedConsumerId);
	pShoalModule->GetOutputContainer().Unlock();

	return result;
}

System::Void ShoalExtractionConsumer::CleanUp()
{
	for (unsigned int i = 0; i < m_pconsumedList->size(); i++)
	{
		MovUnRefDelete(m_pconsumedList->at(i));
	}
	m_pconsumedList->clear();
}

System::Void ShoalExtractionConsumer::PurgeOldShoals()
{
	M3DKernel * pKern = M3DKernel::GetInstance();
	PingFan *pFan = (PingFan *)pKern->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(0);
	std::uint64_t oldestPingId = pFan->GetPingId();

	// on retire tous les bancs qui ne sont plus completement inclus dans la zone m�moire des donn�es brutes
	std::vector<ShoalExtractionOutput*>::iterator vectItr = m_pconsumedList->begin();
	while (vectItr != m_pconsumedList->end())
	{
		std::uint64_t firstPingId = (*((*vectItr)->m_pClosedShoal->GetPings().begin()))->GetPingId();
		if (firstPingId < oldestPingId)
		{
			// suppression du banc
			MovUnRefDelete((*vectItr));
			vectItr = m_pconsumedList->erase(vectItr);
		}
		else
		{
			vectItr++;
		}
	}
}

unsigned int ShoalExtractionConsumer::GetShoalCount() { return  m_pconsumedList->size(); }

ShoalExtractionOutput* ShoalExtractionConsumer::GetShoalIdx(unsigned int index)
{
	return m_pconsumedList->at(index);
}

//IPSIS - FRE - 20/08/2009 - access for unmanaged code
const ShoalExtractionOutputList * ShoalExtractionConsumer::GetConsumedList()
{
	return m_pconsumedList;
}
