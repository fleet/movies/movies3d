#pragma once
#include "M3DKernel/module/ModuleOutputContainer.h"
#include "DisplayView.h"
#include <vector>
class ShoalExtractionOutputList;

public ref class ShoalExtractionConsumer
{
public:
	ShoalExtractionConsumer(void);
	~ShoalExtractionConsumer(void);

	bool         Consume(DisplayView^ displayView);
	System::Void CleanUp();
	System::Void PurgeOldShoals();

	unsigned int GetShoalCount();
	ShoalExtractionOutput* GetShoalIdx(unsigned int index);

	//IPSIS - FRE - 20/08/2009 - access for unmanaged code
	const ShoalExtractionOutputList * GetConsumedList();
private:

	tConsumerId m_reservedConsumerId;
	ShoalExtractionOutputList *m_pconsumedList;
};
