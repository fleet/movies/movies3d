#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "M3DKernel/M3DKernel.h"
#include "ModuleManager/ModuleManager.h"
#include "ShoalExtraction/ShoalExtractionModule.h"
#include "ShoalExtraction/ShoalExtractionParameter.h"
#include "BroadcastAndRecordParameterForm.h"
#include "SounderTransducerTreeView.h"
#include "BaseFlatSideView.h"

#include "MovNetwork/MovNetwork.h"


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de ShoalExtractionForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public delegate void ASyncShoalExtractionDelay(std::uint32_t readerCount, std::uint32_t extractionCount,
		std::uint32_t maxDelay);

	public delegate void ShoalExtractionDelayVisible(bool bVisible);

	public ref class ShoalExtractionForm : public System::Windows::Forms::Form
	{
	public:
		ShoalExtractionForm(ShoalExtractionDelayVisible ^ delayInfoVisibleDelegate, ASyncShoalExtractionDelay^ updateDelayDelegate)
		{
			m_SuspendEvent = false;
			InitializeComponent();
			
			this->numericThreshold->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { -UNKNOWN_DB / 100, 0, 0, System::Int32::MinValue });
			this->numericUpDownMaxThreshold->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { -UNKNOWN_DB / 100, 0, 0, System::Int32::MinValue });
			InitValues();

			mDelayInfoVisible = delayInfoVisibleDelegate;
			mASyncShoalExtractionDelay = updateDelayDelegate;
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~ShoalExtractionForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckBox^  checkBoxEnableModule;
	private: System::Windows::Forms::NumericUpDown^  numericThreshold;
	private: System::Windows::Forms::Label^  labelThreshold;
	private: System::Windows::Forms::GroupBox^  groupParameter;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::NumericUpDown^  numericAlongID;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::TextBox^  textBoxResultPath;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::NumericUpDown^  numericAcrossID;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMinHeight;
	private: System::Windows::Forms::Label^  labelMinHeight;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMinWidth;
	private: System::Windows::Forms::Label^  labelMinWidth;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMinLength;
	private: System::Windows::Forms::Label^  labelMinLength;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::NumericUpDown^  numericHeightID;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::CheckBox^  checkBoxSaveEchos;
	private: System::Windows::Forms::Button^  buttonBroadcastAndRecord;
	private: System::Windows::Forms::CheckBox^  checkBoxSaveContourPositions;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxThreshold;
	private: System::Windows::Forms::Label^  labelThresholdMax;
	private: System::Windows::Forms::GroupBox^  groupBoxSounderTransducerFilter;
	private: SounderTransducerTreeView^  sounderTransducerTreeView;
	private: System::Windows::Forms::CheckBox^  sounderTransducerCheckBox;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->checkBoxEnableModule = (gcnew System::Windows::Forms::CheckBox());
			this->numericThreshold = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelThreshold = (gcnew System::Windows::Forms::Label());
			this->groupParameter = (gcnew System::Windows::Forms::GroupBox());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMaxThreshold = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelThresholdMax = (gcnew System::Windows::Forms::Label());
			this->checkBoxSaveContourPositions = (gcnew System::Windows::Forms::CheckBox());
			this->buttonBroadcastAndRecord = (gcnew System::Windows::Forms::Button());
			this->checkBoxSaveEchos = (gcnew System::Windows::Forms::CheckBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBoxResultPath = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->numericHeightID = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDownMinHeight = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelMinHeight = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMinWidth = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelMinWidth = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMinLength = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelMinLength = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->numericAcrossID = (gcnew System::Windows::Forms::NumericUpDown());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->numericAlongID = (gcnew System::Windows::Forms::NumericUpDown());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->groupBoxSounderTransducerFilter = (gcnew System::Windows::Forms::GroupBox());
			this->sounderTransducerTreeView = (gcnew SounderTransducerTreeView());
			this->sounderTransducerCheckBox = (gcnew System::Windows::Forms::CheckBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericThreshold))->BeginInit();
			this->groupParameter->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxThreshold))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericHeightID))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinHeight))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinWidth))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinLength))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericAcrossID))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericAlongID))->BeginInit();
			this->tableLayoutPanel1->SuspendLayout();
			this->groupBoxSounderTransducerFilter->SuspendLayout();
			this->SuspendLayout();
			// 
			// checkBoxEnableModule
			// 
			this->checkBoxEnableModule->AutoSize = true;
			this->tableLayoutPanel1->SetColumnSpan(this->checkBoxEnableModule, 2);
			this->checkBoxEnableModule->Dock = System::Windows::Forms::DockStyle::Fill;
			this->checkBoxEnableModule->Location = System::Drawing::Point(3, 3);
			this->checkBoxEnableModule->Name = L"checkBoxEnableModule";
			this->checkBoxEnableModule->Size = System::Drawing::Size(490, 24);
			this->checkBoxEnableModule->TabIndex = 0;
			this->checkBoxEnableModule->Text = L"Enable Shoal Extraction";
			this->checkBoxEnableModule->UseVisualStyleBackColor = true;
			this->checkBoxEnableModule->CheckedChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::checkBoxEnableModule_CheckedChanged);
			// 
			// numericThreshold
			// 
			this->numericThreshold->DecimalPlaces = 1;
			this->numericThreshold->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			this->numericThreshold->Location = System::Drawing::Point(184, 21);
			this->numericThreshold->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 400, 0, 0, 0 });
			this->numericThreshold->Name = L"numericThreshold";
			this->numericThreshold->Size = System::Drawing::Size(73, 20);
			this->numericThreshold->TabIndex = 1;
			this->numericThreshold->ValueChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::numericThreshold_ValueChanged);
			// 
			// labelThreshold
			// 
			this->labelThreshold->AutoSize = true;
			this->labelThreshold->Location = System::Drawing::Point(6, 23);
			this->labelThreshold->Name = L"labelThreshold";
			this->labelThreshold->Size = System::Drawing::Size(98, 13);
			this->labelThreshold->TabIndex = 3;
			this->labelThreshold->Text = L"Minimum Threshold";
			// 
			// groupParameter
			// 
			this->groupParameter->Controls->Add(this->label10);
			this->groupParameter->Controls->Add(this->numericUpDownMaxThreshold);
			this->groupParameter->Controls->Add(this->labelThresholdMax);
			this->groupParameter->Controls->Add(this->checkBoxSaveContourPositions);
			this->groupParameter->Controls->Add(this->buttonBroadcastAndRecord);
			this->groupParameter->Controls->Add(this->checkBoxSaveEchos);
			this->groupParameter->Controls->Add(this->button1);
			this->groupParameter->Controls->Add(this->textBoxResultPath);
			this->groupParameter->Controls->Add(this->label9);
			this->groupParameter->Controls->Add(this->label8);
			this->groupParameter->Controls->Add(this->label7);
			this->groupParameter->Controls->Add(this->label6);
			this->groupParameter->Controls->Add(this->label5);
			this->groupParameter->Controls->Add(this->label4);
			this->groupParameter->Controls->Add(this->label3);
			this->groupParameter->Controls->Add(this->numericHeightID);
			this->groupParameter->Controls->Add(this->numericUpDownMinHeight);
			this->groupParameter->Controls->Add(this->labelMinHeight);
			this->groupParameter->Controls->Add(this->numericUpDownMinWidth);
			this->groupParameter->Controls->Add(this->labelMinWidth);
			this->groupParameter->Controls->Add(this->numericUpDownMinLength);
			this->groupParameter->Controls->Add(this->labelMinLength);
			this->groupParameter->Controls->Add(this->label2);
			this->groupParameter->Controls->Add(this->numericAcrossID);
			this->groupParameter->Controls->Add(this->label1);
			this->groupParameter->Controls->Add(this->numericAlongID);
			this->groupParameter->Controls->Add(this->numericThreshold);
			this->groupParameter->Controls->Add(this->labelThreshold);
			this->groupParameter->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupParameter->Location = System::Drawing::Point(199, 33);
			this->groupParameter->Name = L"groupParameter";
			this->groupParameter->Size = System::Drawing::Size(294, 296);
			this->groupParameter->TabIndex = 5;
			this->groupParameter->TabStop = false;
			this->groupParameter->Text = L"Parameter";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(263, 49);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(20, 13);
			this->label10->TabIndex = 28;
			this->label10->Text = L"dB";
			// 
			// numericUpDownMaxThreshold
			// 
			this->numericUpDownMaxThreshold->DecimalPlaces = 1;
			this->numericUpDownMaxThreshold->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			this->numericUpDownMaxThreshold->Location = System::Drawing::Point(184, 47);
			this->numericUpDownMaxThreshold->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 400, 0, 0, 0 });
			this->numericUpDownMaxThreshold->Name = L"numericUpDownMaxThreshold";
			this->numericUpDownMaxThreshold->Size = System::Drawing::Size(73, 20);
			this->numericUpDownMaxThreshold->TabIndex = 26;
			this->numericUpDownMaxThreshold->ValueChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::numericUpDownMaxThreshold_ValueChanged);
			// 
			// labelThresholdMax
			// 
			this->labelThresholdMax->AutoSize = true;
			this->labelThresholdMax->Location = System::Drawing::Point(6, 49);
			this->labelThresholdMax->Name = L"labelThresholdMax";
			this->labelThresholdMax->Size = System::Drawing::Size(101, 13);
			this->labelThresholdMax->TabIndex = 27;
			this->labelThresholdMax->Text = L"Maximum Threshold";
			// 
			// checkBoxSaveContourPositions
			// 
			this->checkBoxSaveContourPositions->AutoSize = true;
			this->checkBoxSaveContourPositions->Location = System::Drawing::Point(9, 247);
			this->checkBoxSaveContourPositions->Name = L"checkBoxSaveContourPositions";
			this->checkBoxSaveContourPositions->Size = System::Drawing::Size(91, 17);
			this->checkBoxSaveContourPositions->TabIndex = 25;
			this->checkBoxSaveContourPositions->Text = L"Save Contour";
			this->checkBoxSaveContourPositions->UseVisualStyleBackColor = true;
			this->checkBoxSaveContourPositions->CheckedChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::checkBoxSaveContourPositions_CheckedChanged);
			// 
			// buttonBroadcastAndRecord
			// 
			this->buttonBroadcastAndRecord->Location = System::Drawing::Point(158, 228);
			this->buttonBroadcastAndRecord->Name = L"buttonBroadcastAndRecord";
			this->buttonBroadcastAndRecord->Size = System::Drawing::Size(120, 35);
			this->buttonBroadcastAndRecord->TabIndex = 24;
			this->buttonBroadcastAndRecord->Text = L"Broadcast and record Parameters...";
			this->buttonBroadcastAndRecord->UseVisualStyleBackColor = true;
			this->buttonBroadcastAndRecord->Click += gcnew System::EventHandler(this, &ShoalExtractionForm::buttonBroadcastAndRecord_Click);
			// 
			// checkBoxSaveEchos
			// 
			this->checkBoxSaveEchos->AutoSize = true;
			this->checkBoxSaveEchos->Location = System::Drawing::Point(9, 226);
			this->checkBoxSaveEchos->Name = L"checkBoxSaveEchos";
			this->checkBoxSaveEchos->Size = System::Drawing::Size(134, 17);
			this->checkBoxSaveEchos->TabIndex = 23;
			this->checkBoxSaveEchos->Text = L"Save Echo Information";
			this->checkBoxSaveEchos->UseVisualStyleBackColor = true;
			this->checkBoxSaveEchos->CheckedChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::checkBoxSaveEchos_CheckedChanged);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(229, 269);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(49, 20);
			this->button1->TabIndex = 11;
			this->button1->Text = L"...";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &ShoalExtractionForm::button1_Click);
			// 
			// textBoxResultPath
			// 
			this->textBoxResultPath->Location = System::Drawing::Point(8, 269);
			this->textBoxResultPath->Name = L"textBoxResultPath";
			this->textBoxResultPath->ReadOnly = true;
			this->textBoxResultPath->Size = System::Drawing::Size(215, 20);
			this->textBoxResultPath->TabIndex = 10;
			this->textBoxResultPath->Text = L"C:\\temp\\";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(263, 179);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(15, 13);
			this->label9->TabIndex = 21;
			this->label9->Text = L"m";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(263, 153);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(15, 13);
			this->label8->TabIndex = 20;
			this->label8->Text = L"m";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(263, 101);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(15, 13);
			this->label7->TabIndex = 19;
			this->label7->Text = L"m";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(263, 127);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(15, 13);
			this->label6->TabIndex = 18;
			this->label6->Text = L"m";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(263, 75);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(15, 13);
			this->label5->TabIndex = 17;
			this->label5->Text = L"m";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(263, 23);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(20, 13);
			this->label4->TabIndex = 16;
			this->label4->Text = L"dB";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(5, 205);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(173, 13);
			this->label3->TabIndex = 15;
			this->label3->Text = L"Vertical Integration Distance Factor";
			// 
			// numericHeightID
			// 
			this->numericHeightID->DecimalPlaces = 1;
			this->numericHeightID->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericHeightID->Location = System::Drawing::Point(184, 203);
			this->numericHeightID->Name = L"numericHeightID";
			this->numericHeightID->Size = System::Drawing::Size(73, 20);
			this->numericHeightID->TabIndex = 7;
			this->numericHeightID->ValueChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::numericHeightID_ValueChanged);
			// 
			// numericUpDownMinHeight
			// 
			this->numericUpDownMinHeight->DecimalPlaces = 1;
			this->numericUpDownMinHeight->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericUpDownMinHeight->Location = System::Drawing::Point(184, 125);
			this->numericUpDownMinHeight->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 500, 0, 0, 0 });
			this->numericUpDownMinHeight->Name = L"numericUpDownMinHeight";
			this->numericUpDownMinHeight->Size = System::Drawing::Size(73, 20);
			this->numericUpDownMinHeight->TabIndex = 4;
			this->numericUpDownMinHeight->ValueChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::numericUpDownMinHeight_ValueChanged);
			// 
			// labelMinHeight
			// 
			this->labelMinHeight->AutoSize = true;
			this->labelMinHeight->Location = System::Drawing::Point(6, 127);
			this->labelMinHeight->Name = L"labelMinHeight";
			this->labelMinHeight->Size = System::Drawing::Size(82, 13);
			this->labelMinHeight->TabIndex = 13;
			this->labelMinHeight->Text = L"Minimum Height";
			// 
			// numericUpDownMinWidth
			// 
			this->numericUpDownMinWidth->DecimalPlaces = 1;
			this->numericUpDownMinWidth->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericUpDownMinWidth->Location = System::Drawing::Point(184, 99);
			this->numericUpDownMinWidth->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 500, 0, 0, 0 });
			this->numericUpDownMinWidth->Name = L"numericUpDownMinWidth";
			this->numericUpDownMinWidth->Size = System::Drawing::Size(73, 20);
			this->numericUpDownMinWidth->TabIndex = 3;
			this->numericUpDownMinWidth->ValueChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::numericUpDownMinWidth_ValueChanged);
			// 
			// labelMinWidth
			// 
			this->labelMinWidth->AutoSize = true;
			this->labelMinWidth->Location = System::Drawing::Point(6, 101);
			this->labelMinWidth->Name = L"labelMinWidth";
			this->labelMinWidth->Size = System::Drawing::Size(79, 13);
			this->labelMinWidth->TabIndex = 11;
			this->labelMinWidth->Text = L"Minimum Width";
			// 
			// numericUpDownMinLength
			// 
			this->numericUpDownMinLength->DecimalPlaces = 1;
			this->numericUpDownMinLength->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericUpDownMinLength->Location = System::Drawing::Point(184, 73);
			this->numericUpDownMinLength->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 500, 0, 0, 0 });
			this->numericUpDownMinLength->Name = L"numericUpDownMinLength";
			this->numericUpDownMinLength->Size = System::Drawing::Size(73, 20);
			this->numericUpDownMinLength->TabIndex = 2;
			this->numericUpDownMinLength->ValueChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::numericUpDownMinLength_ValueChanged);
			// 
			// labelMinLength
			// 
			this->labelMinLength->AutoSize = true;
			this->labelMinLength->Location = System::Drawing::Point(6, 75);
			this->labelMinLength->Name = L"labelMinLength";
			this->labelMinLength->Size = System::Drawing::Size(84, 13);
			this->labelMinLength->TabIndex = 9;
			this->labelMinLength->Text = L"Minimum Length";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 179);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(137, 13);
			this->label2->TabIndex = 8;
			this->label2->Text = L"Across Integration Distance";
			// 
			// numericAcrossID
			// 
			this->numericAcrossID->DecimalPlaces = 1;
			this->numericAcrossID->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericAcrossID->Location = System::Drawing::Point(184, 177);
			this->numericAcrossID->Name = L"numericAcrossID";
			this->numericAcrossID->Size = System::Drawing::Size(73, 20);
			this->numericAcrossID->TabIndex = 6;
			this->numericAcrossID->ValueChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::numericAcrossID_ValueChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 153);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(132, 13);
			this->label1->TabIndex = 6;
			this->label1->Text = L"Along Integration Distance";
			// 
			// numericAlongID
			// 
			this->numericAlongID->DecimalPlaces = 1;
			this->numericAlongID->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericAlongID->Location = System::Drawing::Point(184, 151);
			this->numericAlongID->Name = L"numericAlongID";
			this->numericAlongID->Size = System::Drawing::Size(73, 20);
			this->numericAlongID->TabIndex = 5;
			this->numericAlongID->ValueChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::numericAlongID_ValueChanged);
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 2;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				300)));
			this->tableLayoutPanel1->Controls->Add(this->checkBoxEnableModule, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->groupBoxSounderTransducerFilter, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->groupParameter, 1, 1);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 2;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 30)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(496, 332);
			this->tableLayoutPanel1->TabIndex = 6;
			// 
			// groupBoxSounderTranducerFilter
			// 
			this->groupBoxSounderTransducerFilter->Controls->Add(this->sounderTransducerTreeView);
			this->groupBoxSounderTransducerFilter->Controls->Add(this->sounderTransducerCheckBox);
			this->groupBoxSounderTransducerFilter->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxSounderTransducerFilter->Location = System::Drawing::Point(3, 33);
			this->groupBoxSounderTransducerFilter->Name = L"groupBoxSounderTransducerFilter";
			this->groupBoxSounderTransducerFilter->Padding = System::Windows::Forms::Padding(5);
			this->groupBoxSounderTransducerFilter->Size = System::Drawing::Size(190, 296);
			this->groupBoxSounderTransducerFilter->TabIndex = 6;
			this->groupBoxSounderTransducerFilter->TabStop = false;
			this->groupBoxSounderTransducerFilter->Text = L"Sounder / Transducer Filter";
			// 
			// sounderTransducerTreeView
			// 
			this->sounderTransducerTreeView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->sounderTransducerTreeView->Location = System::Drawing::Point(5, 35);
			this->sounderTransducerTreeView->Name = L"sounderTransducerTreeView";
			this->sounderTransducerTreeView->Size = System::Drawing::Size(180, 256);
			this->sounderTransducerTreeView->TabIndex = 1;
			this->sounderTransducerTreeView->setAfterCheckAction(gcnew NodeEventHandler(this, &ShoalExtractionForm::afterSounderTransducerTreeViewChecked));
			this->sounderTransducerTreeView->setTransducerFilterActive(false);
			// 
			// sounderTransducerCheckBox
			// 
			this->sounderTransducerCheckBox->AutoSize = true;
			this->sounderTransducerCheckBox->Dock = System::Windows::Forms::DockStyle::Top;
			this->sounderTransducerCheckBox->Location = System::Drawing::Point(5, 18);
			this->sounderTransducerCheckBox->Name = L"sounderTransducerCheckBox";
			this->sounderTransducerCheckBox->Size = System::Drawing::Size(180, 17);
			this->sounderTransducerCheckBox->TabIndex = 0;
			this->sounderTransducerCheckBox->Text = L"Active";
			this->sounderTransducerCheckBox->UseVisualStyleBackColor = true;
			this->sounderTransducerCheckBox->CheckedChanged += gcnew System::EventHandler(this, &ShoalExtractionForm::sounderTransducerCheckBox_CheckedChanged);
			// 
			// ShoalExtractionForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(496, 332);
			this->Controls->Add(this->tableLayoutPanel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->Name = L"ShoalExtractionForm";
			this->Text = L"Shoal Extraction Parameter";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericThreshold))->EndInit();
			this->groupParameter->ResumeLayout(false);
			this->groupParameter->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxThreshold))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericHeightID))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinHeight))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinWidth))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinLength))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericAcrossID))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericAlongID))->EndInit();
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			this->groupBoxSounderTransducerFilter->ResumeLayout(false);
			this->groupBoxSounderTransducerFilter->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void checkBoxEnableModule_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
		UpdateEnableControls();
	}

	private:bool m_SuspendEvent;

	private: ShoalExtractionDelayVisible^ mDelayInfoVisible;
	private: ASyncShoalExtractionDelay^ mASyncShoalExtractionDelay;

			 // private: List<BaseFlatSideView^> mFlatSideViews;

	private: System::Void InitValues()
	{
		m_SuspendEvent = true;
		const auto& moduleManager = CModuleManager::getInstance();
		const auto& shoalExtractionModule = moduleManager->GetShoalExtractionModule();
		const auto& shoalExtractionParameter = shoalExtractionModule->GetShoalExtractionParameter();

		// get our values
		this->checkBoxEnableModule->Checked = shoalExtractionModule->getEnable();
		this->sounderTransducerCheckBox->Checked = shoalExtractionParameter.isEnableTransducerFilter();
		this->sounderTransducerTreeView->Enabled = this->sounderTransducerCheckBox->Checked;
		this->sounderTransducerTreeView->checkTransducers(shoalExtractionParameter.getActiveTransducerNames());
		this->numericUpDownMinLength->Value = System::Decimal(shoalExtractionParameter.GetMinLength());
		this->numericUpDownMinWidth->Value = System::Decimal(shoalExtractionParameter.GetMinWidth());
		this->numericUpDownMinHeight->Value = System::Decimal(shoalExtractionParameter.GetMinHeight());
		this->numericThreshold->Value = System::Decimal(shoalExtractionParameter.GetThreshold());
		this->numericUpDownMaxThreshold->Value = System::Decimal(shoalExtractionParameter.GetMaxThreshold());
		this->numericAlongID->Value = System::Decimal(shoalExtractionParameter.GetAlongIntegrationDistance());
		this->numericAcrossID->Value = System::Decimal(shoalExtractionParameter.GetAcrossIntegrationDistance());
		this->numericHeightID->Value = System::Decimal(shoalExtractionParameter.GetVerticalIntegrationDistance());
		this->checkBoxSaveEchos->Checked = shoalExtractionParameter.GetSaveEchos();
		this->checkBoxSaveContourPositions->Checked = shoalExtractionParameter.GetSaveContour();
		this->textBoxResultPath->Text = gcnew System::String(shoalExtractionParameter.GetRecordPath().c_str());

		UpdateEnableControls();

		/// end

		m_SuspendEvent = false;
	}

			 // Event permettant d'afficher/masquer les infos sur la charge du modile d'extraction de banc
	private: System::Void SetOrUpdateValues()
	{
		if (!m_SuspendEvent)
		{
			M3DKernel::GetInstance()->Lock();
			const auto& moduleManager = CModuleManager::getInstance();
			const auto& shoalExtractionModule = moduleManager->GetShoalExtractionModule();
			auto& shoalExtractionParameter = shoalExtractionModule->GetShoalExtractionParameter();
			//set our values
			bool previousState = shoalExtractionModule->getEnable();
			if (this->checkBoxEnableModule->Checked != previousState)
			{
				// OTK - 21/10/2009 - si on active le module, on enregistre aupr�s du module le callback
				// d'information du retard
				if (!previousState)
				{
					mDelayInfoVisible(true);
					IntPtr delayCB = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(mASyncShoalExtractionDelay);
					shoalExtractionModule->SetDelayInfoCallBack((DELAYINFO_CALLBACK)(void*)delayCB);
				}
				
				shoalExtractionModule->setEnable(this->checkBoxEnableModule->Checked);
				if (shoalExtractionParameter.GetParameterBroadcastAndRecord().m_enableNetworkOutput)
				{
					MovNetwork::getInstance()->setEIShoalEnabled(this->checkBoxEnableModule->Checked);
				}
				if (previousState)
				{
					shoalExtractionModule->SetDelayInfoCallBack(NULL);
					mDelayInfoVisible(false);
				}
			}

			// Filtre 
			const auto previousFilterState = shoalExtractionParameter.isEnableTransducerFilter();
			const auto previousTransducerNamesFilter = shoalExtractionParameter.getActiveTransducerNames();
			shoalExtractionParameter.setEnableTransducerFilter(this->sounderTransducerCheckBox->Checked);
			shoalExtractionParameter.clearActiveTransducerNames();
			shoalExtractionParameter.addActiveTransducerNames(this->sounderTransducerTreeView->getCheckedTransducers());

			// Si le filtre a été modifié, on force un redémarrage du module d'extraction de banc
			if (previousFilterState != shoalExtractionParameter.isEnableTransducerFilter() ||
				previousTransducerNamesFilter != shoalExtractionParameter.getActiveTransducerNames()
				)
			{
				shoalExtractionModule->Restart();
			}

			// Paramètres
			shoalExtractionParameter.SetThreshold((float)System::Decimal::ToDouble(this->numericThreshold->Value));
			shoalExtractionParameter.SetMaxThreshold((float)System::Decimal::ToDouble(this->numericUpDownMaxThreshold->Value));
			shoalExtractionParameter.SetAcrossIntegrationDistance((float)System::Decimal::ToDouble(this->numericAcrossID->Value));
			shoalExtractionParameter.SetAlongIntegrationDistance((float)System::Decimal::ToDouble(this->numericAlongID->Value));
			shoalExtractionParameter.SetVerticalIntegrationDistance((float)System::Decimal::ToDouble(this->numericHeightID->Value));
			shoalExtractionParameter.SetMinLength((float)System::Decimal::ToDouble(this->numericUpDownMinLength->Value));
			shoalExtractionParameter.SetMinWidth((float)System::Decimal::ToDouble(this->numericUpDownMinWidth->Value));
			shoalExtractionParameter.SetMinHeight((float)System::Decimal::ToDouble(this->numericUpDownMinHeight->Value));
			shoalExtractionParameter.SetSaveEchos(this->checkBoxSaveEchos->Checked);
			shoalExtractionParameter.SetSaveContour(this->checkBoxSaveContourPositions->Checked);

			String^ file = this->textBoxResultPath->Text;

			System::IntPtr ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(file);
			const char* strPath = static_cast<const char*>(ip.ToPointer());

			shoalExtractionParameter.SetRecordPath(strPath);
			M3DKernel::GetInstance()->Unlock();
			// end
		}
	}

	private: System::Void numericThreshold_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
	}
	private: System::Void numericUpDownMaxThreshold_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
	}
	private: System::Void numericUpDownMinLength_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
	}
	private: System::Void numericUpDownMinWidth_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
	}
	private: System::Void numericUpDownMinHeight_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
	}
	private: System::Void numericAcrossID_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
	}
	private: System::Void numericAlongID_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
	}
	private: System::Void numericHeightID_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
	}
	private: System::Void checkBoxSaveEchos_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
	}
	private: System::Void checkBoxSaveContourPositions_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		SetOrUpdateValues();
	}

	void afterSounderTransducerTreeViewChecked(TreeNode^ node)
	{
		SetOrUpdateValues();
	}

	void UpdateEnableControls()
	{
		this->groupBoxSounderTransducerFilter->Enabled = this->checkBoxEnableModule->Checked;
		this->groupParameter->Enabled = this->checkBoxEnableModule->Checked;		
	}

	private: System::Void buttonBroadcastAndRecord_Click(System::Object^  sender, System::EventArgs^  e) {
		// r�cup�ration du module d'extraction
		CModuleManager * pModuleMgr = CModuleManager::getInstance();
		ShoalExtractionModule * pModule = pModuleMgr->GetShoalExtractionModule();

		BroadcastAndRecordParameterForm ^ref = gcnew BroadcastAndRecordParameterForm(&pModule->GetShoalExtractionParameter().GetParameterBroadcastAndRecord());
		ref->ShowDialog();
	}

	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		String^ folderName;

		FolderBrowserDialog^ folderBrowserDialog1;
		folderBrowserDialog1 = gcnew System::Windows::Forms::FolderBrowserDialog;
		folderBrowserDialog1->Description = "Select the directory that you want to use.";
		folderBrowserDialog1->ShowNewFolderButton = false;
		folderBrowserDialog1->RootFolder = Environment::SpecialFolder::MyComputer;

		System::Windows::Forms::DialogResult result = folderBrowserDialog1->ShowDialog();
		if (result == ::DialogResult::OK)
		{
			folderName = folderBrowserDialog1->SelectedPath;
		}

		System::IntPtr ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(folderName);
		const char* strPath = static_cast<const char*>(ip.ToPointer());

		CModuleManager *pMod = CModuleManager::getInstance();
		ShoalExtractionModule *pShoalModule = pMod->GetShoalExtractionModule();
		ShoalExtractionParameter &refParam = pShoalModule->GetShoalExtractionParameter();
		refParam.SetRecordPath(std::string(strPath) + "\\");
		this->textBoxResultPath->Text = folderName;
	}
	private: System::Void sounderTransducerCheckBox_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
	{
		sounderTransducerTreeView->Enabled = sounderTransducerCheckBox->Checked;
		SetOrUpdateValues();
	}
};
}
