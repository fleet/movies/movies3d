#include "AmplitudeAndPhaseDetectionControl.h"

#include "TSAnalysis/TSAnalysisModule.h"

using namespace MOVIESVIEWCPP;

System::Void AmplitudeAndPhaseDetectionControl::ConfigToGUI()
{
	TSAnalysisParameter & params = m_pTSModule->GetTSAnalysisParameter();

	numericUpDownTSThreshold->Value = Convert::ToDecimal(params.GetTSThreshold());
	numericUpDownMaxGainComp->Value = Convert::ToDecimal(params.GetMaxGainComp());
	numericUpDownPhaseDev->Value = Convert::ToDecimal(params.GetPhaseDev());
	numericUpDownMinEchoLength->Value = Convert::ToDecimal(params.GetMinEchoLength());
	numericUpDownMaxEchoLength->Value = Convert::ToDecimal(params.GetMaxEchoLength());
	numericUpDownMinEchoSpace->Value = Convert::ToDecimal(params.GetMinEchospace());
	numericUpDownMinEchoDepth->Value = Convert::ToDecimal(params.GetMinEchoDepth());
	numericUpDownMaxEchoDepth->Value = Convert::ToDecimal(params.GetMaxEchoDepth());
	numericUpDownDistPrevPost->Value = Convert::ToDecimal(params.GetSpectrumDistPrevPost());
}

System::Void AmplitudeAndPhaseDetectionControl::ApplyDetailedView()
{
	TSAnalysisParameter & params = m_pTSModule->GetTSAnalysisParameter();

	params.SetTSThreshold(Convert::ToSingle(numericUpDownTSThreshold->Value));

	params.SetMaxGainComp(Convert::ToSingle(numericUpDownMaxGainComp->Value));
	params.SetPhaseDev(Convert::ToSingle(numericUpDownPhaseDev->Value));
	params.SetMinEchoLength(Convert::ToSingle(numericUpDownMinEchoLength->Value));
	params.SetMaxEchoLength(Convert::ToSingle(numericUpDownMaxEchoLength->Value));
	params.SetMinEchospace(Convert::ToSingle(numericUpDownMinEchoSpace->Value));
	params.SetMinEchoDepth(Convert::ToSingle(numericUpDownMinEchoDepth->Value));
	params.SetMaxEchoDepth(Convert::ToSingle(numericUpDownMaxEchoDepth->Value));
	params.SetSpectrumDistPrevPost(Convert::ToSingle(numericUpDownDistPrevPost->Value));
}
