#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

class TSAnalysisModule;


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de AmplitudeAndPhaseDetectionControl
	/// </summary>
	public ref class AmplitudeAndPhaseDetectionControl : public System::Windows::Forms::UserControl
	{
	public:
		AmplitudeAndPhaseDetectionControl(TSAnalysisModule * pTSModule)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
			m_pTSModule = pTSModule;
			ConfigToGUI();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~AmplitudeAndPhaseDetectionControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownTSThreshold;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxGainComp;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownPhaseDev;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMinEchoLength;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxEchoLength;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMinEchoSpace;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxEchoDepth;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMinEchoDepth;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownDistPrevPost;

	protected:

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMaxEchoDepth = (gcnew System::Windows::Forms::NumericUpDown());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMinEchoDepth = (gcnew System::Windows::Forms::NumericUpDown());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownDistPrevPost = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDownMinEchoSpace = (gcnew System::Windows::Forms::NumericUpDown());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMaxEchoLength = (gcnew System::Windows::Forms::NumericUpDown());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMinEchoLength = (gcnew System::Windows::Forms::NumericUpDown());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownPhaseDev = (gcnew System::Windows::Forms::NumericUpDown());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMaxGainComp = (gcnew System::Windows::Forms::NumericUpDown());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownTSThreshold = (gcnew System::Windows::Forms::NumericUpDown());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->tableLayoutPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxEchoDepth))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinEchoDepth))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDistPrevPost))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinEchoSpace))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxEchoLength))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinEchoLength))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownPhaseDev))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxGainComp))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownTSThreshold))->BeginInit();
			this->SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 3;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				90)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel1->Controls->Add(this->label10, 0, 7);
			this->tableLayoutPanel1->Controls->Add(this->label11, 3, 7);
			this->tableLayoutPanel1->Controls->Add(this->numericUpDownMaxEchoDepth, 1, 7);
			this->tableLayoutPanel1->Controls->Add(this->label12, 0, 6);
			this->tableLayoutPanel1->Controls->Add(this->label13, 3, 6);
			this->tableLayoutPanel1->Controls->Add(this->numericUpDownMinEchoDepth, 1, 6);
			this->tableLayoutPanel1->Controls->Add(this->label14, 0, 8);
			this->tableLayoutPanel1->Controls->Add(this->label15, 3, 8);
			this->tableLayoutPanel1->Controls->Add(this->numericUpDownDistPrevPost, 1, 8);
			this->tableLayoutPanel1->Controls->Add(this->numericUpDownMinEchoSpace, 1, 5);
			this->tableLayoutPanel1->Controls->Add(this->label9, 0, 5);
			this->tableLayoutPanel1->Controls->Add(this->numericUpDownMaxEchoLength, 1, 4);
			this->tableLayoutPanel1->Controls->Add(this->label8, 0, 4);
			this->tableLayoutPanel1->Controls->Add(this->numericUpDownMinEchoLength, 1, 3);
			this->tableLayoutPanel1->Controls->Add(this->label7, 0, 3);
			this->tableLayoutPanel1->Controls->Add(this->label6, 2, 2);
			this->tableLayoutPanel1->Controls->Add(this->numericUpDownPhaseDev, 1, 2);
			this->tableLayoutPanel1->Controls->Add(this->label5, 0, 2);
			this->tableLayoutPanel1->Controls->Add(this->label4, 2, 1);
			this->tableLayoutPanel1->Controls->Add(this->numericUpDownMaxGainComp, 1, 1);
			this->tableLayoutPanel1->Controls->Add(this->label1, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->numericUpDownTSThreshold, 1, 0);
			this->tableLayoutPanel1->Controls->Add(this->label2, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->label3, 2, 0);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 9;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(400, 208);
			this->tableLayoutPanel1->TabIndex = 0;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label10->Location = System::Drawing::Point(3, 213);
			this->label10->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(201, 15);
			this->label10->TabIndex = 18;
			this->label10->Text = L"Maximum echo depth";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label11->Location = System::Drawing::Point(300, 213);
			this->label11->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(97, 15);
			this->label11->TabIndex = 19;
			this->label11->Text = L"m";
			// 
			// numericUpDownMaxEchoDepth
			// 
			this->numericUpDownMaxEchoDepth->DecimalPlaces = 1;
			this->numericUpDownMaxEchoDepth->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownMaxEchoDepth->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			this->numericUpDownMaxEchoDepth->Location = System::Drawing::Point(210, 211);
			this->numericUpDownMaxEchoDepth->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000, 0, 0, 0 });
			this->numericUpDownMaxEchoDepth->Name = L"numericUpDownMaxEchoDepth";
			this->numericUpDownMaxEchoDepth->Size = System::Drawing::Size(84, 20);
			this->numericUpDownMaxEchoDepth->TabIndex = 20;
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label12->Location = System::Drawing::Point(3, 161);
			this->label12->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(201, 21);
			this->label12->TabIndex = 14;
			this->label12->Text = L"Minimum echo depth";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label13->Location = System::Drawing::Point(300, 161);
			this->label13->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(97, 21);
			this->label13->TabIndex = 16;
			this->label13->Text = L"m";
			// 
			// numericUpDownMinEchoDepth
			// 
			this->numericUpDownMinEchoDepth->DecimalPlaces = 1;
			this->numericUpDownMinEchoDepth->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownMinEchoDepth->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			this->numericUpDownMinEchoDepth->Location = System::Drawing::Point(210, 159);
			this->numericUpDownMinEchoDepth->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000, 0, 0, 0 });
			this->numericUpDownMinEchoDepth->Name = L"numericUpDownMinEchoDepth";
			this->numericUpDownMinEchoDepth->Size = System::Drawing::Size(84, 20);
			this->numericUpDownMinEchoDepth->TabIndex = 17;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label14->Location = System::Drawing::Point(3, 187);
			this->label14->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(201, 21);
			this->label14->TabIndex = 21;
			this->label14->Text = L"Distance around echo (spectral analysis)";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label15->Location = System::Drawing::Point(300, 187);
			this->label15->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(97, 21);
			this->label15->TabIndex = 23;
			this->label15->Text = L"m";
			// 
			// numericUpDownDistPrevPost
			// 
			this->numericUpDownDistPrevPost->DecimalPlaces = 2;
			this->numericUpDownDistPrevPost->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownDistPrevPost->Increment = System::Decimal(0.05);
			this->numericUpDownDistPrevPost->Location = System::Drawing::Point(210, 185);
			this->numericUpDownDistPrevPost->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000, 0, 0, 0 });
			this->numericUpDownDistPrevPost->Name = L"numericUpDownDistPrevPost";
			this->numericUpDownDistPrevPost->Size = System::Drawing::Size(84, 20);
			this->numericUpDownDistPrevPost->TabIndex = 22;
			// 
			// numericUpDownMinEchoSpace
			// 
			this->numericUpDownMinEchoSpace->DecimalPlaces = 1;
			this->numericUpDownMinEchoSpace->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownMinEchoSpace->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericUpDownMinEchoSpace->Location = System::Drawing::Point(210, 133);
			this->numericUpDownMinEchoSpace->Name = L"numericUpDownMinEchoSpace";
			this->numericUpDownMinEchoSpace->Size = System::Drawing::Size(84, 20);
			this->numericUpDownMinEchoSpace->TabIndex = 14;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label9->Location = System::Drawing::Point(3, 135);
			this->label9->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(201, 21);
			this->label9->TabIndex = 13;
			this->label9->Text = L"Minimum echo space";
			// 
			// numericUpDownMaxEchoLength
			// 
			this->numericUpDownMaxEchoLength->DecimalPlaces = 1;
			this->numericUpDownMaxEchoLength->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownMaxEchoLength->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericUpDownMaxEchoLength->Location = System::Drawing::Point(210, 107);
			this->numericUpDownMaxEchoLength->Name = L"numericUpDownMaxEchoLength";
			this->numericUpDownMaxEchoLength->Size = System::Drawing::Size(84, 20);
			this->numericUpDownMaxEchoLength->TabIndex = 12;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label8->Location = System::Drawing::Point(3, 109);
			this->label8->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(201, 21);
			this->label8->TabIndex = 11;
			this->label8->Text = L"Maximum echo length";
			// 
			// numericUpDownMinEchoLength
			// 
			this->numericUpDownMinEchoLength->DecimalPlaces = 1;
			this->numericUpDownMinEchoLength->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownMinEchoLength->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericUpDownMinEchoLength->Location = System::Drawing::Point(210, 81);
			this->numericUpDownMinEchoLength->Name = L"numericUpDownMinEchoLength";
			this->numericUpDownMinEchoLength->Size = System::Drawing::Size(84, 20);
			this->numericUpDownMinEchoLength->TabIndex = 10;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label7->Location = System::Drawing::Point(3, 83);
			this->label7->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(201, 21);
			this->label7->TabIndex = 9;
			this->label7->Text = L"Minimum echo length";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label6->Location = System::Drawing::Point(300, 57);
			this->label6->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(97, 21);
			this->label6->TabIndex = 8;
			this->label6->Text = L"phase steps";
			// 
			// numericUpDownPhaseDev
			// 
			this->numericUpDownPhaseDev->DecimalPlaces = 1;
			this->numericUpDownPhaseDev->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownPhaseDev->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			this->numericUpDownPhaseDev->Location = System::Drawing::Point(210, 55);
			this->numericUpDownPhaseDev->Name = L"numericUpDownPhaseDev";
			this->numericUpDownPhaseDev->Size = System::Drawing::Size(84, 20);
			this->numericUpDownPhaseDev->TabIndex = 7;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label5->Location = System::Drawing::Point(3, 57);
			this->label5->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(201, 21);
			this->label5->TabIndex = 6;
			this->label5->Text = L"Maximum phase deviation";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label4->Location = System::Drawing::Point(300, 31);
			this->label4->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(97, 21);
			this->label4->TabIndex = 5;
			this->label4->Text = L"dB";
			// 
			// numericUpDownMaxGainComp
			// 
			this->numericUpDownMaxGainComp->DecimalPlaces = 1;
			this->numericUpDownMaxGainComp->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownMaxGainComp->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			this->numericUpDownMaxGainComp->Location = System::Drawing::Point(210, 29);
			this->numericUpDownMaxGainComp->Name = L"numericUpDownMaxGainComp";
			this->numericUpDownMaxGainComp->Size = System::Drawing::Size(84, 20);
			this->numericUpDownMaxGainComp->TabIndex = 3;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label1->Location = System::Drawing::Point(3, 5);
			this->label1->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(201, 21);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Minimum TS threshold";
			// 
			// numericUpDownTSThreshold
			// 
			this->numericUpDownTSThreshold->DecimalPlaces = 1;
			this->numericUpDownTSThreshold->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownTSThreshold->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			this->numericUpDownTSThreshold->Location = System::Drawing::Point(210, 3);
			this->numericUpDownTSThreshold->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 150, 0, 0, System::Int32::MinValue });
			this->numericUpDownTSThreshold->Name = L"numericUpDownTSThreshold";
			this->numericUpDownTSThreshold->Size = System::Drawing::Size(84, 20);
			this->numericUpDownTSThreshold->TabIndex = 1;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label2->Location = System::Drawing::Point(3, 31);
			this->label2->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(201, 21);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Maximum angular one-way compensation";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(300, 5);
			this->label3->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(20, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"dB";
			// 
			// AmplitudeAndPhaseDetectionControl
			// 
			this->Controls->Add(this->tableLayoutPanel1);
			this->Name = L"AmplitudeAndPhaseDetectionControl";
			this->Size = System::Drawing::Size(400, 208);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxEchoDepth))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinEchoDepth))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDistPrevPost))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinEchoSpace))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxEchoLength))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinEchoLength))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownPhaseDev))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxGainComp))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownTSThreshold))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

	public: void ApplyDetailedView();

	private: TSAnalysisModule * m_pTSModule;
	private: void ConfigToGUI();
	};
}
