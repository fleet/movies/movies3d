#pragma once

#include <cstdint>

ref class ESUItem {

public:
	ESUItem(std::uint32_t esuId) { this->esuId = esuId; bCurrentESU = false; }

	virtual System::String ^ ToString() override
	{
		if (bCurrentESU)
		{
			return "Current ESU";
		}
		else
		{
			return System::String::Format("ESU {0}", esuId);
		}
	}

	virtual bool Equals(System::Object ^obj) override
	{
		ESUItem ^otherItem = dynamic_cast<ESUItem^>(obj);
		if (otherItem == nullptr) {
			return false;
		}
		return esuId == otherItem->esuId && bCurrentESU == otherItem->bCurrentESU;
	}

	// ID de l'ESU
	unsigned int esuId;
	bool bCurrentESU;
};