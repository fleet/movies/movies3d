#include "SingleTargetDetectionParamControl.h"

#include "TSAnalysis/TSAnalysisModule.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "ModuleManager/ModuleManager.h"
#include "M3DKernel/M3DKernel.h"

#include <set>

using namespace MOVIESVIEWCPP;

ref class SounderCheckedListboxItem {
public:
	SounderCheckedListboxItem(std::uint32_t id) { sounderId = id; }

	std::uint32_t sounderId;

	virtual System::String ^ ToString() override
	{
		return System::String::Format("Sounder Id[{0}]", sounderId);
	}
};

void SingleTargetDetectionParamControl::UpdateConfiguration()
{
	TSAnalysisModule* pTSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
	TSAnalysisParameter& parameters = pTSModule->GetTSAnalysisParameter();
	parameters.SetTrackingEnabled(checkBoxTracking->Checked);
	parameters.SetMaxSpeed(Convert::ToSingle(numericUpDownMaxSpeed->Value));
	parameters.SetMaxHoles(Convert::ToInt32(numericUpDownMaxHoles->Value));
	parameters.SetMinEchoNumber(Convert::ToInt32(numericUpDownMinEchoNumber->Value));
	parameters.SetMaxHolesPercent(Convert::ToSingle(numericUpDownMaxHolesPercent->Value / 100));

	parameters.SetHistogramMinValue(Convert::ToSingle(numericUpDownHistogramMinValue->Value));
	parameters.SetHistogramMaxValue(Convert::ToSingle(numericUpDownHistogramMaxValue->Value));
	parameters.SetHistogramResolution(Convert::ToSingle(numericUpDownHistogramResolution->Value));

	std::vector<std::uint32_t> checkedSounders;
	bool bUnChecked = false;
	for each(SounderCheckedListboxItem ^ sounder in checkedListBoxHistoSounders->Items)
	{
		if (checkedListBoxHistoSounders->CheckedItems->Contains(sounder))
		{
			std::uint32_t sounderId = sounder->sounderId;
			checkedSounders.push_back(sounderId);
		}
		else
		{
			bUnChecked = true;
		}
	}
	if (bUnChecked)
	{
		parameters.GetHistogramSounders() = checkedSounders;
	}
	else
	{
		parameters.GetHistogramSounders().clear();
	}

}

void SingleTargetDetectionParamControl::UpdateGUI()
{
	// r�cup�ration de la d�finition des param�tres
	TSAnalysisModule* pTSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
	TSAnalysisParameter& parameters = pTSModule->GetTSAnalysisParameter();

	const std::vector<std::uint32_t> & sounderIDs = parameters.GetHistogramSounders();
	std::set<std::uint32_t> remainingSounders(sounderIDs.begin(), sounderIDs.end());

	// Peuplement de la liste des sondeurs
	CurrentSounderDefinition & sounderMgr = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().m_sounderDefinition;

	checkedListBoxHistoSounders->BeginUpdate();
	checkedListBoxHistoSounders->Items->Clear();
	int iItem = 0;
	for (unsigned int i = 0; i < sounderMgr.GetNbSounder(); i++)
	{
		Sounder *pSounder = sounderMgr.GetSounder(i);

		checkedListBoxHistoSounders->Items->Add(gcnew SounderCheckedListboxItem(pSounder->m_SounderId));

		// Pas de sondeur sp�cifique d�fini dans la config : c'est comme si tout �tait s�lectionn� !
		if (std::find(sounderIDs.begin(), sounderIDs.end(), pSounder->m_SounderId) != sounderIDs.end() || sounderIDs.empty())
		{
			checkedListBoxHistoSounders->SetItemChecked(iItem, true);
		}
		remainingSounders.erase(pSounder->m_SounderId);
		iItem++;
	}
	// On affiche les sondeurs pas pr�sents dans le flux mais sp�cifi�s dans la config :
	std::set<std::uint32_t>::const_iterator iter;
	for (iter = remainingSounders.begin(); iter != remainingSounders.end(); ++iter)
	{
		checkedListBoxHistoSounders->Items->Add(gcnew SounderCheckedListboxItem(*iter));
		checkedListBoxHistoSounders->SetItemChecked(iItem++, true);
	}
	checkedListBoxHistoSounders->EndUpdate();

	// Peuplement du reste de l'IHM
	this->checkedListBox->ItemCheck -= m_CheckEventHandler;

	checkedListBox->Items->Clear();
	checkedListBox->BeginUpdate();

	System::String ^NodeName = gcnew System::String("Disabled");
	checkedListBox->Items->Add(gcnew System::String("Disabled"), !parameters.GetDetectionEnabled());
	checkedListBox->Items->Add(gcnew System::String("Amplitude and phase detection"), parameters.GetDetectionEnabled());

	checkedListBox->EndUpdate();

	checkBoxTracking->Checked = parameters.GetTrackingEnabled();
	numericUpDownMaxSpeed->Value = Convert::ToDecimal(parameters.GetMaxSpeed());
	numericUpDownMaxHoles->Value = Convert::ToDecimal(parameters.GetMaxHoles());
	numericUpDownMinEchoNumber->Value = Convert::ToDecimal(parameters.GetMinEchoNumber());
	numericUpDownMaxHolesPercent->Value = Convert::ToDecimal(parameters.GetMaxHolesPercent() * 100);

	numericUpDownHistogramMinValue->Value = Convert::ToDecimal(parameters.GetHistogramMinValue());
	numericUpDownHistogramMaxValue->Value = Convert::ToDecimal(parameters.GetHistogramMaxValue());
	numericUpDownHistogramResolution->Value = Convert::ToDecimal(parameters.GetHistogramResolution());

	this->checkedListBox->ItemCheck += m_CheckEventHandler;
}
System::Void SingleTargetDetectionParamControl::checkedListBox_ItemCheck(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e)
{
	// on n'autorise pas le d�cochage des algos
	if (e->NewValue == CheckState::Checked)
	{
		TSAnalysisModule * TSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
		TSAnalysisParameter& parameters = TSModule->GetTSAnalysisParameter();

		bool bDetectionOldState = parameters.GetDetectionEnabled();

		// on active l'algo coch�
		parameters.SetDetectionEnabled(e->Index == 1);

		// Si l'�tat d'activation de cet algo passe de false � true et que les donn�es de phase sont ignor�es,
		// on les d�signore et on en informe l'utilisateur
		if (parameters.GetDetectionEnabled() && !bDetectionOldState)
		{
			KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();
			if (param.getIgnorePhase())
			{
				param.setIgnorePhase(false);
				M3DKernel::GetInstance()->UpdateKernelParameter(param);
				MessageBox::Show("Phase data is needed for TS detection and has been automatically unignored in kernel configuration.");
			}
		}
	}
	else
	{
		e->NewValue = CheckState::Checked;
	}

	// mise � jour de la liste
	UpdateGUI();
}
System::Void SingleTargetDetectionParamControl::checkedListBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	// mise � jour des param�tres de l'algorithme en fonction de son type
	TSAnalysisModule * TSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
	TSAnalysisParameter& parameters = TSModule->GetTSAnalysisParameter();

	int selectedIndex = checkedListBox->SelectedIndex;
	if (selectedIndex == 1)
	{
		if (!m_AmplitudeAndPhaseDetectionControl)
		{
			m_AmplitudeAndPhaseDetectionControl = gcnew AmplitudeAndPhaseDetectionControl(TSModule);
		}
		if (!this->panelParameters->Controls->Contains(m_AmplitudeAndPhaseDetectionControl)) {
			this->panelParameters->Controls->Clear();
			this->panelParameters->Controls->Add(m_AmplitudeAndPhaseDetectionControl);
			m_AmplitudeAndPhaseDetectionControl->Dock = System::Windows::Forms::DockStyle::Fill;
		}
	}
	else
	{
		// cas des algos sans param�tres du tout
		this->panelParameters->Controls->Clear();
	}
}

System::Void SingleTargetDetectionParamControl::ApplyDetailedView()
{
	if (m_AmplitudeAndPhaseDetectionControl)
	{
		m_AmplitudeAndPhaseDetectionControl->ApplyDetailedView();
	}
}
