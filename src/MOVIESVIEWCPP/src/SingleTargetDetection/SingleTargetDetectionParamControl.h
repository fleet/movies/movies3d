#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"
#include "AmplitudeAndPhaseDetectionControl.h"


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de SingleTargetDetectionParamControl
	/// </summary>
	public ref class SingleTargetDetectionParamControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		SingleTargetDetectionParamControl(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
			m_CheckEventHandler = gcnew System::Windows::Forms::ItemCheckEventHandler(this, &SingleTargetDetectionParamControl::checkedListBox_ItemCheck);
			this->checkedListBox->ItemCheck += m_CheckEventHandler;
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~SingleTargetDetectionParamControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBoxDetection;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::CheckedListBox^  checkedListBox;

	private: System::Windows::Forms::Panel^  panelParameters;
	private: System::Windows::Forms::GroupBox^  groupBoxTracking;
	private: System::Windows::Forms::CheckBox^  checkBoxTracking;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanelTracking;
	private: System::Windows::Forms::Label^  labelMaxSpeed;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxSpeed;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxHoles;














	private: System::Windows::Forms::GroupBox^  groupBoxHistogram;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel2;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownHistogramMinValue;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownHistogramMaxValue;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownHistogramResolution;




	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxHolesPercent;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMinEchoNumber;
	private: System::Windows::Forms::CheckedListBox^  checkedListBoxHistoSounders;





	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBoxDetection = (gcnew System::Windows::Forms::GroupBox());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->checkedListBox = (gcnew System::Windows::Forms::CheckedListBox());
			this->panelParameters = (gcnew System::Windows::Forms::Panel());
			this->groupBoxTracking = (gcnew System::Windows::Forms::GroupBox());
			this->tableLayoutPanelTracking = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMaxHolesPercent = (gcnew System::Windows::Forms::NumericUpDown());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMinEchoNumber = (gcnew System::Windows::Forms::NumericUpDown());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMaxHoles = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelMaxSpeed = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMaxSpeed = (gcnew System::Windows::Forms::NumericUpDown());
			this->checkBoxTracking = (gcnew System::Windows::Forms::CheckBox());
			this->groupBoxHistogram = (gcnew System::Windows::Forms::GroupBox());
			this->checkedListBoxHistoSounders = (gcnew System::Windows::Forms::CheckedListBox());
			this->tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownHistogramResolution = (gcnew System::Windows::Forms::NumericUpDown());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownHistogramMaxValue = (gcnew System::Windows::Forms::NumericUpDown());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownHistogramMinValue = (gcnew System::Windows::Forms::NumericUpDown());
			this->groupBoxDetection->SuspendLayout();
			this->tableLayoutPanel1->SuspendLayout();
			this->groupBoxTracking->SuspendLayout();
			this->tableLayoutPanelTracking->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxHolesPercent))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinEchoNumber))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxHoles))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxSpeed))->BeginInit();
			this->groupBoxHistogram->SuspendLayout();
			this->tableLayoutPanel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownHistogramResolution))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownHistogramMaxValue))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownHistogramMinValue))->BeginInit();
			this->SuspendLayout();
			// 
			// groupBoxDetection
			// 
			this->groupBoxDetection->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxDetection->Controls->Add(this->tableLayoutPanel1);
			this->groupBoxDetection->Location = System::Drawing::Point(3, 3);
			this->groupBoxDetection->Name = L"groupBoxDetection";
			this->groupBoxDetection->Size = System::Drawing::Size(776, 299);
			this->groupBoxDetection->TabIndex = 0;
			this->groupBoxDetection->TabStop = false;
			this->groupBoxDetection->Text = L"Detection algorithm";
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 2;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				200)));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->checkedListBox, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->panelParameters, 1, 0);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(3, 16);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 1;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(770, 280);
			this->tableLayoutPanel1->TabIndex = 0;
			// 
			// checkedListBox
			// 
			this->checkedListBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->checkedListBox->FormattingEnabled = true;
			this->checkedListBox->Location = System::Drawing::Point(3, 3);
			this->checkedListBox->Name = L"checkedListBox";
			this->checkedListBox->Size = System::Drawing::Size(194, 274);
			this->checkedListBox->TabIndex = 0;
			this->checkedListBox->SelectedIndexChanged += gcnew System::EventHandler(this, &SingleTargetDetectionParamControl::checkedListBox_SelectedIndexChanged);
			// 
			// panelParameters
			// 
			this->panelParameters->Dock = System::Windows::Forms::DockStyle::Fill;
			this->panelParameters->Location = System::Drawing::Point(203, 3);
			this->panelParameters->Name = L"panelParameters";
			this->panelParameters->Size = System::Drawing::Size(564, 274);
			this->panelParameters->TabIndex = 1;
			// 
			// groupBoxTracking
			// 
			this->groupBoxTracking->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxTracking->Controls->Add(this->tableLayoutPanelTracking);
			this->groupBoxTracking->Controls->Add(this->checkBoxTracking);
			this->groupBoxTracking->Location = System::Drawing::Point(3, 305);
			this->groupBoxTracking->Name = L"groupBoxTracking";
			this->groupBoxTracking->Size = System::Drawing::Size(776, 133);
			this->groupBoxTracking->TabIndex = 1;
			this->groupBoxTracking->TabStop = false;
			this->groupBoxTracking->Text = L"Tracking algorithm";
			// 
			// tableLayoutPanelTracking
			// 
			this->tableLayoutPanelTracking->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->tableLayoutPanelTracking->ColumnCount = 4;
			this->tableLayoutPanelTracking->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanelTracking->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				90)));
			this->tableLayoutPanelTracking->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanelTracking->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanelTracking->Controls->Add(this->label10, 0, 3);
			this->tableLayoutPanelTracking->Controls->Add(this->label11, 2, 3);
			this->tableLayoutPanelTracking->Controls->Add(this->numericUpDownMaxHolesPercent, 1, 3);
			this->tableLayoutPanelTracking->Controls->Add(this->label8, 0, 2);
			this->tableLayoutPanelTracking->Controls->Add(this->numericUpDownMinEchoNumber, 1, 2);
			this->tableLayoutPanelTracking->Controls->Add(this->label2, 0, 1);
			this->tableLayoutPanelTracking->Controls->Add(this->label3, 2, 1);
			this->tableLayoutPanelTracking->Controls->Add(this->numericUpDownMaxHoles, 1, 1);
			this->tableLayoutPanelTracking->Controls->Add(this->labelMaxSpeed, 0, 0);
			this->tableLayoutPanelTracking->Controls->Add(this->label1, 2, 0);
			this->tableLayoutPanelTracking->Controls->Add(this->numericUpDownMaxSpeed, 1, 0);
			this->tableLayoutPanelTracking->Location = System::Drawing::Point(3, 45);
			this->tableLayoutPanelTracking->Name = L"tableLayoutPanelTracking";
			this->tableLayoutPanelTracking->RowCount = 4;
			this->tableLayoutPanelTracking->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanelTracking->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanelTracking->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanelTracking->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanelTracking->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
				20)));
			this->tableLayoutPanelTracking->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
				20)));
			this->tableLayoutPanelTracking->Size = System::Drawing::Size(770, 82);
			this->tableLayoutPanelTracking->TabIndex = 1;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label10->Location = System::Drawing::Point(3, 83);
			this->label10->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(220, 21);
			this->label10->TabIndex = 15;
			this->label10->Text = L"Maximum missing echoes percent in track";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label11->Location = System::Drawing::Point(319, 83);
			this->label11->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(32, 21);
			this->label11->TabIndex = 16;
			this->label11->Text = L"%";
			// 
			// numericUpDownMaxHolesPercent
			// 
			this->numericUpDownMaxHolesPercent->DecimalPlaces = 1;
			this->numericUpDownMaxHolesPercent->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownMaxHolesPercent->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			this->numericUpDownMaxHolesPercent->Location = System::Drawing::Point(229, 81);
			this->numericUpDownMaxHolesPercent->Name = L"numericUpDownMaxHolesPercent";
			this->numericUpDownMaxHolesPercent->Size = System::Drawing::Size(84, 20);
			this->numericUpDownMaxHolesPercent->TabIndex = 17;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label8->Location = System::Drawing::Point(3, 57);
			this->label8->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(220, 21);
			this->label8->TabIndex = 12;
			this->label8->Text = L"Minimum single target number in track";
			// 
			// numericUpDownMinEchoNumber
			// 
			this->numericUpDownMinEchoNumber->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownMinEchoNumber->Location = System::Drawing::Point(229, 55);
			this->numericUpDownMinEchoNumber->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->numericUpDownMinEchoNumber->Name = L"numericUpDownMinEchoNumber";
			this->numericUpDownMinEchoNumber->Size = System::Drawing::Size(84, 20);
			this->numericUpDownMinEchoNumber->TabIndex = 14;
			this->numericUpDownMinEchoNumber->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label2->Location = System::Drawing::Point(3, 31);
			this->label2->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(220, 21);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Maximum holes between two tracked echoes";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label3->Location = System::Drawing::Point(319, 31);
			this->label3->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(32, 21);
			this->label3->TabIndex = 4;
			this->label3->Text = L"pings";
			// 
			// numericUpDownMaxHoles
			// 
			this->numericUpDownMaxHoles->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownMaxHoles->Location = System::Drawing::Point(229, 29);
			this->numericUpDownMaxHoles->Name = L"numericUpDownMaxHoles";
			this->numericUpDownMaxHoles->Size = System::Drawing::Size(84, 20);
			this->numericUpDownMaxHoles->TabIndex = 5;
			// 
			// labelMaxSpeed
			// 
			this->labelMaxSpeed->AutoSize = true;
			this->labelMaxSpeed->Dock = System::Windows::Forms::DockStyle::Fill;
			this->labelMaxSpeed->Location = System::Drawing::Point(3, 5);
			this->labelMaxSpeed->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->labelMaxSpeed->Name = L"labelMaxSpeed";
			this->labelMaxSpeed->Size = System::Drawing::Size(220, 21);
			this->labelMaxSpeed->TabIndex = 0;
			this->labelMaxSpeed->Text = L"Maximum target speed";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label1->Location = System::Drawing::Point(319, 5);
			this->label1->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(32, 21);
			this->label1->TabIndex = 1;
			this->label1->Text = L"m/s";
			// 
			// numericUpDownMaxSpeed
			// 
			this->numericUpDownMaxSpeed->DecimalPlaces = 1;
			this->numericUpDownMaxSpeed->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownMaxSpeed->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			this->numericUpDownMaxSpeed->Location = System::Drawing::Point(229, 3);
			this->numericUpDownMaxSpeed->Name = L"numericUpDownMaxSpeed";
			this->numericUpDownMaxSpeed->Size = System::Drawing::Size(84, 20);
			this->numericUpDownMaxSpeed->TabIndex = 2;
			// 
			// checkBoxTracking
			// 
			this->checkBoxTracking->AutoSize = true;
			this->checkBoxTracking->Location = System::Drawing::Point(6, 19);
			this->checkBoxTracking->Name = L"checkBoxTracking";
			this->checkBoxTracking->Size = System::Drawing::Size(104, 17);
			this->checkBoxTracking->TabIndex = 0;
			this->checkBoxTracking->Text = L"Enable Tracking";
			this->checkBoxTracking->UseVisualStyleBackColor = true;
			// 
			// groupBoxHistogram
			// 
			this->groupBoxHistogram->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxHistogram->Controls->Add(this->checkedListBoxHistoSounders);
			this->groupBoxHistogram->Controls->Add(this->tableLayoutPanel2);
			this->groupBoxHistogram->Location = System::Drawing::Point(3, 441);
			this->groupBoxHistogram->Name = L"groupBoxHistogram";
			this->groupBoxHistogram->Size = System::Drawing::Size(776, 100);
			this->groupBoxHistogram->TabIndex = 2;
			this->groupBoxHistogram->TabStop = false;
			this->groupBoxHistogram->Text = L"Histogram";
			// 
			// checkedListBoxHistoSounders
			// 
			this->checkedListBoxHistoSounders->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->checkedListBoxHistoSounders->FormattingEnabled = true;
			this->checkedListBoxHistoSounders->Location = System::Drawing::Point(229, 16);
			this->checkedListBoxHistoSounders->Name = L"checkedListBoxHistoSounders";
			this->checkedListBoxHistoSounders->Size = System::Drawing::Size(541, 79);
			this->checkedListBoxHistoSounders->TabIndex = 1;
			// 
			// tableLayoutPanel2
			// 
			this->tableLayoutPanel2->ColumnCount = 3;
			this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				90)));
			this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				20)));
			this->tableLayoutPanel2->Controls->Add(this->label15, 0, 2);
			this->tableLayoutPanel2->Controls->Add(this->label16, 2, 2);
			this->tableLayoutPanel2->Controls->Add(this->numericUpDownHistogramResolution, 1, 2);
			this->tableLayoutPanel2->Controls->Add(this->label13, 0, 1);
			this->tableLayoutPanel2->Controls->Add(this->label14, 2, 1);
			this->tableLayoutPanel2->Controls->Add(this->numericUpDownHistogramMaxValue, 1, 1);
			this->tableLayoutPanel2->Controls->Add(this->label9, 0, 0);
			this->tableLayoutPanel2->Controls->Add(this->label12, 2, 0);
			this->tableLayoutPanel2->Controls->Add(this->numericUpDownHistogramMinValue, 1, 0);
			this->tableLayoutPanel2->Location = System::Drawing::Point(3, 16);
			this->tableLayoutPanel2->Name = L"tableLayoutPanel2";
			this->tableLayoutPanel2->RowCount = 3;
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel2->Size = System::Drawing::Size(223, 78);
			this->tableLayoutPanel2->TabIndex = 0;
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label15->Location = System::Drawing::Point(3, 57);
			this->label15->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(96, 21);
			this->label15->TabIndex = 9;
			this->label15->Text = L"Resolution";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label16->Location = System::Drawing::Point(195, 57);
			this->label16->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(25, 21);
			this->label16->TabIndex = 10;
			this->label16->Text = L"dB";
			// 
			// numericUpDownHistogramResolution
			// 
			this->numericUpDownHistogramResolution->DecimalPlaces = 1;
			this->numericUpDownHistogramResolution->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownHistogramResolution->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			this->numericUpDownHistogramResolution->Location = System::Drawing::Point(105, 55);
			this->numericUpDownHistogramResolution->Name = L"numericUpDownHistogramResolution";
			this->numericUpDownHistogramResolution->Size = System::Drawing::Size(84, 20);
			this->numericUpDownHistogramResolution->TabIndex = 11;
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label13->Location = System::Drawing::Point(3, 31);
			this->label13->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(96, 21);
			this->label13->TabIndex = 6;
			this->label13->Text = L"Maximum dB value";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label14->Location = System::Drawing::Point(195, 31);
			this->label14->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(25, 21);
			this->label14->TabIndex = 7;
			this->label14->Text = L"dB";
			// 
			// numericUpDownHistogramMaxValue
			// 
			this->numericUpDownHistogramMaxValue->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownHistogramMaxValue->Location = System::Drawing::Point(105, 29);
			this->numericUpDownHistogramMaxValue->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 100, 0, 0, System::Int32::MinValue });
			this->numericUpDownHistogramMaxValue->Name = L"numericUpDownHistogramMaxValue";
			this->numericUpDownHistogramMaxValue->Size = System::Drawing::Size(84, 20);
			this->numericUpDownHistogramMaxValue->TabIndex = 8;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label9->Location = System::Drawing::Point(3, 5);
			this->label9->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(96, 21);
			this->label9->TabIndex = 3;
			this->label9->Text = L"Minimum dB value";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label12->Location = System::Drawing::Point(195, 5);
			this->label12->Margin = System::Windows::Forms::Padding(3, 5, 3, 0);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(25, 21);
			this->label12->TabIndex = 4;
			this->label12->Text = L"dB";
			// 
			// numericUpDownHistogramMinValue
			// 
			this->numericUpDownHistogramMinValue->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numericUpDownHistogramMinValue->Location = System::Drawing::Point(105, 3);
			this->numericUpDownHistogramMinValue->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 100, 0, 0, System::Int32::MinValue });
			this->numericUpDownHistogramMinValue->Name = L"numericUpDownHistogramMinValue";
			this->numericUpDownHistogramMinValue->Size = System::Drawing::Size(84, 20);
			this->numericUpDownHistogramMinValue->TabIndex = 5;
			// 
			// SingleTargetDetectionParamControl
			// 
			this->Controls->Add(this->groupBoxHistogram);
			this->Controls->Add(this->groupBoxTracking);
			this->Controls->Add(this->groupBoxDetection);
			this->Name = L"SingleTargetDetectionParamControl";
			this->Size = System::Drawing::Size(782, 567);
			this->groupBoxDetection->ResumeLayout(false);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->groupBoxTracking->ResumeLayout(false);
			this->groupBoxTracking->PerformLayout();
			this->tableLayoutPanelTracking->ResumeLayout(false);
			this->tableLayoutPanelTracking->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxHolesPercent))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinEchoNumber))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxHoles))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxSpeed))->EndInit();
			this->groupBoxHistogram->ResumeLayout(false);
			this->tableLayoutPanel2->ResumeLayout(false);
			this->tableLayoutPanel2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownHistogramResolution))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownHistogramMaxValue))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownHistogramMinValue))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion


	private: System::Void checkedListBox_ItemCheck(System::Object^  sender, System::Windows::Forms::ItemCheckEventArgs^  e);
	private: System::Void checkedListBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
	public: System::Void ApplyDetailedView();

#pragma region Impl�mentation de IParamControl
	public: virtual void UpdateConfiguration();
	public: virtual void UpdateGUI();
#pragma endregion

	private: System::Windows::Forms::ItemCheckEventHandler^ m_CheckEventHandler;
	private: AmplitudeAndPhaseDetectionControl^ m_AmplitudeAndPhaseDetectionControl;
	};
}
