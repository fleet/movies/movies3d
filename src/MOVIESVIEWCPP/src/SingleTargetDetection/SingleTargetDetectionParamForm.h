// -*- MC++ -*-
// ****************************************************************************
// Class: SingleTargetDetectionParamForm
//
// Description: Fen�tre de configuration du module d'analyse TS
//
// Projet: MOVIES3D
// Auteur: O.Tonck
// Date  : F�vrier 2016
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "ParameterForm.h"
#include "SingleTargetDetectionParamControl.h"

namespace MOVIESVIEWCPP {
	public ref class SingleTargetDetectionParamForm : public ParameterForm
	{
	public:
		SingleTargetDetectionParamForm(void) :
			ParameterForm()
		{
			// on veut pouvoir redimensionner cette fen�tre
			this->AutoSize = false;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->ClientSize = System::Drawing::Size(590, 525);
			// on ne met pas les boutons OK et annuler dans cette form
			// il faudrait impl�menter une config temporaire si on veut r�aliser 
			// le annuler du fait du multiusage de la textbox pour plusieurs modules
			panel1->Hide();
			SetParamControl(gcnew SingleTargetDetectionParamControl(), L"Single Target Detection Parameters");
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &SingleTargetDetectionParamForm::SingleTargetDetectionParamForm_FormClosing);


		}

	private: System::Void SingleTargetDetectionParamForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
		m_ParamControl->UpdateConfiguration();
		((SingleTargetDetectionParamControl^)m_ParamControl)->ApplyDetailedView();
	}
	};
};
