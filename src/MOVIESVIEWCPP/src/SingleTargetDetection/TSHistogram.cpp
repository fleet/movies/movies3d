// -*- C++ -*-
// ****************************************************************************
// Class: TSHistogram
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: O.Tonck
// Date  : F�vrier 2016
// Soci�t� : IPSIS
// ****************************************************************************

#include "TSHistogram.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "TSAnalysis/TSAnalysisModule.h"
#include "ModuleManager/ModuleManager.h"
#include "DisplayParameter.h"

#include "ColorPaletteFrequency.h"

#include "vtkSmartPointer.h"
#include "vtkChartXY.h"
#include "vtkFloatArray.h"
#include "vtkTable.h"
#include "vtkPlot.h"
#include "vtkContextView.h"
#include "vtkContextScene.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkPen.h"
#include "vtkBrush.h"
#include "vtkAxis.h"
#include "vtkImageData.h"

#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

using namespace MOVIESVIEWCPP;

TSHistogram::TSHistogram(std::int32_t parenthandle, int width, int height)
{
	m_pPalette = new ColorPaletteFrequency;
	m_pView = vtkContextView::New();
	m_pView->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
	m_pView->GetRenderWindow()->SetParentId((void*)parenthandle);
	m_pView->GetRenderWindow()->SetSize(width, height);
	m_pView->GetInteractor()->Initialize();

	chart = vtkChartXY::New();
	chart->SetShowLegend(true);
	chart->GetAxis(1)->SetTitle("TS (dB)");
	chart->GetAxis(0)->SetTitle("Count");

	m_pView->GetScene()->AddItem(chart);
}

TSHistogram::~TSHistogram(void)
{
	if (chart)
		chart->Delete();
	if (m_pView)
		m_pView->Delete();
	if (m_pPalette)
		delete m_pPalette;
}

void TSHistogram::UpdateHistogram(ESUParameter * pESUParams)
{
	// le clearPlots est bugg� et provoque des crash dans la version 5.6.1 de VTK...
	//chart->ClearPlots();
	while (chart->GetNumberOfPlots() > 0)
	{
		chart->RemovePlot(chart->GetNumberOfPlots() - 1);
	}

	if (pESUParams)
	{
		const HacTime & startTime = pESUParams->GetESUTime();
		const HacTime & endTime = pESUParams->GetESUEndTime();

		TSAnalysisModule * pTSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
		const TSAnalysisParameter & params = pTSModule->GetTSAnalysisParameter();
		double dbMin = params.GetHistogramMinValue();
		double dbMax = params.GetHistogramMaxValue();
		double dbWidth = params.GetHistogramResolution();
		TSDisplayType graphType = DisplayParameter::getInstance()->GetTSDisplayType();
		const std::vector<std::uint32_t> & sounderIDs = params.GetHistogramSounders();

		VTK_CREATE(vtkTable, table);

		M3DKernel *pKernel = M3DKernel::GetInstance();

		// G�n�ration de la liste des plages de dB une fois pour toutes
		std::vector<std::pair<double, double> > listPlages;
		for (double currentMindB = dbMin; currentMindB < dbMax; currentMindB += dbWidth)
		{
			listPlages.push_back(std::make_pair(currentMindB, std::min(currentMindB + dbWidth, dbMax)));
		}

		// R�cup�ration de la d�finition des sondeurs
		const CurrentSounderDefinition & soundersDef = pKernel->getObjectMgr()->GetSounderDefinition();

		bool bNoData = true;
		// map du nombre de single target track�e et non track�e par fr�quence et par dB moyen pour la plage de dB,
		std::map<unsigned short, std::map<std::pair<double, double>, std::pair<int, int> > > mapSingleTargetByFreq;
		std::map<unsigned short, double> mapFrequencies;
		for (unsigned int iSounder = 0; iSounder < soundersDef.GetNbSounder(); iSounder++)
		{
			Sounder * pSounder = soundersDef.GetSounder(iSounder);
			// Si pas de sondeur d�fini, on les affiche tous...
			if (sounderIDs.empty() || std::find(sounderIDs.begin(), sounderIDs.end(), pSounder->m_SounderId) != sounderIDs.end())
			{
				for (unsigned int iTrans = 0; iTrans < pSounder->GetTransducerCount(); iTrans++)
				{
					Transducer * pTrans = pSounder->GetTransducer(iTrans);
					const std::vector<unsigned short> & channelIDs = pTrans->GetChannelId();
					for (size_t iChan = 0; iChan < channelIDs.size(); iChan++)
					{
						SoftChannel * pSoftChan = pTrans->getSoftChannel(channelIDs[iChan]);
						if (pSoftChan)
						{
							mapFrequencies[pSoftChan->getSoftwareChannelId()] = pSoftChan->m_acousticFrequency / 1000;
							std::map<std::pair<double, double>, std::pair<int, int> > & mapTS = mapSingleTargetByFreq[channelIDs[iChan]];
							// Pour chaque plage de dB...
							for (size_t iPlage = 0; iPlage < listPlages.size(); iPlage++)
							{
								mapTS[listPlages[iPlage]] = std::make_pair<int, int>(0, 0);
								bNoData = false;
							}
						}
					}
				}
			}
		}

		if (!bNoData)
		{
			const std::map<std::uint64_t, TSTrack*> & tracks = pTSModule->getTracks();
			SounderIndexedTimeContainer * pContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetSingleTargetContainer();
			for (size_t iSingleTarget = 0; iSingleTarget < pContainer->GetObjectCount(); iSingleTarget++)
			{
				PingFanSingleTarget *pSingle = (PingFanSingleTarget*)pContainer->GetObjectWithIndex(iSingleTarget);

				if (pSingle && pSingle->m_ObjectTime >= startTime && (endTime.IsNull() || pSingle->m_ObjectTime <= endTime))
				{
					for (unsigned int i = 0; i < pSingle->GetNumberOfTarget(); i++)
					{
						SingleTargetDataObject *pSingleTargetData = pSingle->GetTarget(i);
						std::map<unsigned short, std::map<std::pair<double, double>, std::pair<int, int> > >::iterator iterFreq = mapSingleTargetByFreq.find(pSingleTargetData->m_parentSTId);
						if (iterFreq != mapSingleTargetByFreq.end())
						{
							for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataCWCount(); numTarget++)
							{
								const SingleTargetDataCW & tsData = pSingleTargetData->GetSingleTargetDataCW(numTarget);
								std::map<std::pair<double, double>, std::pair<int, int> >::iterator iter;
								for (iter = iterFreq->second.begin(); iter != iterFreq->second.end(); iter++)
								{
									// On regarde si la cible est dans la plage en dB qui nous int�resse
									if (tsData.m_compensatedTS >= iter->first.first && tsData.m_compensatedTS < iter->first.second)
									{
										// On incr�mente le compteur track� ou non track� en fonction
										bool bIsTracked = false;
										if (tsData.m_trackLabel > 0)
										{
											std::map<std::uint64_t, TSTrack*>::const_iterator iterTrack = tracks.find(tsData.m_trackLabel);
											if (iterTrack != tracks.end())
											{
												bIsTracked = pTSModule->IsValidTrack(iterTrack->second);
											}
										}
										if (bIsTracked)
										{
											iter->second.first += 1;
										}
										else
										{
											iter->second.second += 1;
										}
									}
								}
							}
						}
					}
				}
			}

			// Calcul fr�quence par fr�quence du nombre de cible par plage de dB pour normalisation
			std::map<double, std::pair<int, int> > mapNbTS;
			std::map<unsigned short, std::map<std::pair<double, double>, std::pair<int, int> > >::iterator iterFreq;
			for (iterFreq = mapSingleTargetByFreq.begin(); iterFreq != mapSingleTargetByFreq.end(); ++iterFreq)
			{
				double dbFreq = mapFrequencies[iterFreq->first];
				std::map<double, std::pair<int, int> >::iterator iter = mapNbTS.find(dbFreq);
				if (iter == mapNbTS.end())
				{
					mapNbTS[dbFreq] = std::make_pair<int, int>(0, 0);
				}
				std::pair<int, int> & nbTsForFreq = mapNbTS[dbFreq];
				std::map<std::pair<double, double>, std::pair<int, int> >::iterator iterTS;
				for (iterTS = iterFreq->second.begin(); iterTS != iterFreq->second.end(); ++iterTS)
				{
					nbTsForFreq.first += iterTS->second.first;
					nbTsForFreq.second += iterTS->second.second;
				}
			}

			// On trie tout �a par fr�quence
			std::map<double, std::map<std::pair<double, double>, std::pair<int, int> > * > mapSingleTargetByRealFreq;
			for (iterFreq = mapSingleTargetByFreq.begin(); iterFreq != mapSingleTargetByFreq.end(); ++iterFreq)
			{
				mapSingleTargetByRealFreq[mapFrequencies[iterFreq->first]] = &iterFreq->second;
			}

			// On met � jour la palette qui va bien
			m_pPalette->ComputePalette(mapSingleTargetByRealFreq.size());
			
			// tableau correspondant aux plages en dB (axe horizontal)
			VTK_CREATE(vtkFloatArray, arrDBScale);
			arrDBScale->SetName("dB");
			table->AddColumn(arrDBScale);

			// cr�ation d'un tableau par fr�quence (ou deux par fr�quence si on affiche les cibles track�es et non track�es
			std::ostringstream oss;
			std::map<double, std::map<std::pair<double, double>, std::pair<int, int> > * >::const_iterator iterRealFreq;
			for (iterRealFreq = mapSingleTargetByRealFreq.begin(); iterRealFreq != mapSingleTargetByRealFreq.end(); ++iterRealFreq)
			{
				VTK_CREATE(vtkFloatArray, arrFreq);

				oss.str("");
				oss << round(iterRealFreq->first) << " kHz";

				// on indique s'il s'agit des cibles track�es ou non track�es
				if (graphType == eTSDisplayTracked || graphType == eTSDisplayAll)
				{
					oss << " (tracked)";
				}
				else
				{
					oss << " (untracked)";
				}

				arrFreq->SetName(oss.str().c_str());
				table->AddColumn(arrFreq);

				if (graphType == eTSDisplayAll)
				{
					VTK_CREATE(vtkFloatArray, arrFreq2);

					oss.str("");
					oss << round(iterRealFreq->first) << " kHz (untracked)";

					arrFreq2->SetName(oss.str().c_str());
					table->AddColumn(arrFreq2);
				}
			}

			// Peuplement de la table
			table->SetNumberOfRows(listPlages.size());
			for (size_t i = 0; i < listPlages.size(); i++)
			{
				const std::pair<double, double> & dbRange = listPlages[i];
				int iCol = 0;
				// On affecte � la plage en dB la valeur m�diane de la plage
				table->SetValue(i, iCol++, (dbRange.first + dbRange.second) / 2.0);

				// Ensuite, peuplement pour chaque pr�quence
				for (iterRealFreq = mapSingleTargetByRealFreq.begin(); iterRealFreq != mapSingleTargetByRealFreq.end(); ++iterRealFreq)
				{
					float fValue = 0.f;
					const std::pair<int, int> & nbTS = mapNbTS[iterRealFreq->first];
					if (graphType == eTSDisplayTracked || graphType == eTSDisplayAll)
					{
						if (nbTS.first > 0)
						{
							fValue = (float)iterRealFreq->second->operator [](dbRange).first / nbTS.first;
						}
					}
					else
					{
						if (nbTS.second > 0)
						{
							fValue = (float)iterRealFreq->second->operator [](dbRange).second / nbTS.second;
						}
					}
					table->SetValue(i, iCol++, fValue);
					if (graphType == eTSDisplayAll)
					{
						fValue = 0.f;
						if (nbTS.second > 0)
						{
							fValue = (float)iterRealFreq->second->operator [](dbRange).second / nbTS.second;
						}
						table->SetValue(i, iCol++, fValue);
					}
				}
			}

			// D�finition des barres � afficher, des couleurs, etc...
			vtkPlot *line = 0;
			unsigned int iCol = 1;
			unsigned int iFreq = 1;
			for (iterRealFreq = mapSingleTargetByRealFreq.begin(); iterRealFreq != mapSingleTargetByRealFreq.end(); ++iterRealFreq)
			{
				line = chart->AddPlot(vtkChart::BAR);
				line->SetInputData(table, 0, iCol);

				const auto & color = m_pPalette->GetColor(iFreq - 1);

				if (graphType == eTSDisplayAll)
				{
					// si on affiche track�es et non track�es, on affiche les track�es en ligne simple sans remplissage
					line->SetColor(255, 255, 255, 255);
					line->GetPen()->SetColor(color.r, color.g, color.b, color.a);
					iCol = iCol + 2;
				}
				else
				{
					line->SetColor(color.r, color.g, color.b, color.a);
					line->GetPen()->SetLineType(vtkPen::NO_PEN);
					iCol++;
				}
				iFreq++;
			}

			if (graphType == eTSDisplayAll)
			{
				iCol = 2;
				iFreq = 1;
				for (iterRealFreq = mapSingleTargetByRealFreq.begin(); iterRealFreq != mapSingleTargetByRealFreq.end(); ++iterRealFreq)
				{
					line = chart->AddPlot(vtkChart::BAR);
					line->SetInputData(table, 0, iCol);

					const auto & color = m_pPalette->GetColor(iFreq - 1);
					line->SetColor(color.r, color.g, color.b, color.a);
					line->GetPen()->SetLineType(vtkPen::NO_PEN);

					iCol += 2;
					iFreq++;
				}
			}

			// Ajout de points invisibles pour bien voir toutes les barres (les points
			// des plages correspondent aux centre des plages en dB, du coup en fonction
			// de la taille de la fen�tre il peut y avoir des probl�mes dans le zoom auto de VTK)
			VTK_CREATE(vtkTable, invisibleTable);

			VTK_CREATE(vtkFloatArray, arrInvisibleDBScale);
			arrInvisibleDBScale->SetName("invdB");
			invisibleTable->AddColumn(arrInvisibleDBScale);

			VTK_CREATE(vtkFloatArray, arrInvisibleTSScale);
			arrInvisibleTSScale->SetName(" ");
			invisibleTable->AddColumn(arrInvisibleTSScale);

			invisibleTable->SetNumberOfRows(2);

			invisibleTable->SetValue(0, 0, dbMin);
			invisibleTable->SetValue(0, 1, 0);
			invisibleTable->SetValue(1, 0, dbMax);
			invisibleTable->SetValue(1, 1, 0);

			vtkPlot * pInvisiblePlot = chart->AddPlot(vtkChart::LINE);
			pInvisiblePlot->SetInputData(invisibleTable, 0, 1);
			pInvisiblePlot->GetPen()->SetLineType(vtkPen::NO_PEN);
			// Fin ajout points invisibles
		}
	}

	m_pView->GetInteractor()->Render();
}

void TSHistogram::OnResize(int width, int height)
{
	if (m_pView)
	{
		m_pView->GetRenderWindow()->SetSize(width, height);
	}
}