// -*- C++ -*-
// ****************************************************************************
// Class: TSHistogram
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: O.Tonck
// Date  : F�vrier 2016
// Soci�t� : IPSIS
// ****************************************************************************

#include <cstdint>

#pragma once

class ESUParameter;

class vtkContextView;
class vtkChartXY;

class ColorPaletteFrequency;

namespace MOVIESVIEWCPP {

	class TSHistogram
	{
	public:

		TSHistogram(std::int32_t parenthandle, int width, int height);
		virtual ~TSHistogram(void);

		// ****************************************************************************
		// Methods
		// ****************************************************************************

		// Refresh
		void UpdateHistogram(ESUParameter * pESUParams);

		// Resizes the histogram
		void OnResize(int width, int height);


		// ****************************************************************************
		// Accessors
		// ****************************************************************************

	private:

		// ****************************************************************************
		// Member Data
		// ****************************************************************************

		vtkContextView * m_pView;
		vtkChartXY * chart;

		ColorPaletteFrequency * m_pPalette;
	};
};
