#include "TSHistogramChart.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "ModuleManager/ModuleManager.h"

#include "TSAnalysis/TSAnalysisModule.h"
#include "DisplayParameter.h"
#include "ColorPaletteFrequency.h"


TSHistogramChart::TSHistogramChart()
{
	BeginInit();

	auto chartArea = gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea();

	System::Windows::Forms::DataVisualization::Charting::Series^  series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
	chartArea->Name = L"ChartArea";

	chartArea->AxisX->Title = "TS (dB)";
	chartArea->AxisX->TitleFont = gcnew System::Drawing::Font(chartArea->AxisX->TitleFont, System::Drawing::FontStyle::Bold);
	chartArea->AxisX->MajorGrid->LineColor = System::Drawing::Color::LightGray;
	chartArea->AxisX->MinorGrid->LineColor = System::Drawing::Color::WhiteSmoke;
	chartArea->AxisX->IsStartedFromZero = false;

	chartArea->AxisY->Title = "C o u n t";
	chartArea->AxisY->TitleFont = gcnew System::Drawing::Font(chartArea->AxisY->TitleFont, System::Drawing::FontStyle::Bold);
	chartArea->AxisY->MajorGrid->LineColor = System::Drawing::Color::LightGray;
	chartArea->AxisY->MinorGrid->LineColor = System::Drawing::Color::WhiteSmoke;
	chartArea->AxisY->LabelStyle->Format = "F4";
	chartArea->AxisY->IsStartedFromZero = false;
	
	ChartAreas->Add(chartArea);

	auto chartLegend = gcnew System::Windows::Forms::DataVisualization::Charting::Legend();
	chartLegend->Name = L"Legend";
	chartLegend->LegendStyle = System::Windows::Forms::DataVisualization::Charting::LegendStyle::Column;
	Legends->Add(chartLegend);	
	
	// Install interactor
	this->chartInteractor = gcnew ChartInteractor();
	chartInteractor->Install(this);

	EndInit();
}

void TSHistogramChart::UpdateHistogram(ESUParameter * pESUParams)
{
	TSAnalysisModule * pTSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
	const TSAnalysisParameter & params = pTSModule->GetTSAnalysisParameter();
	const double dbMin = params.GetHistogramMinValue();
	const double dbMax = params.GetHistogramMaxValue();

	this->Series->SuspendUpdates();

	System::Collections::Generic::Dictionary<System::Windows::Forms::DataVisualization::Charting::Series ^, bool> deprecatedSeries;
	for each(auto serie in this->Series)
	{
		deprecatedSeries[serie] = false;
	}

	if (pESUParams)
	{
		const HacTime & startTime = pESUParams->GetESUTime();
		const HacTime & endTime = pESUParams->GetESUEndTime();

		double dbWidth = params.GetHistogramResolution();
		TSDisplayType graphType = DisplayParameter::getInstance()->GetTSDisplayType();
		const std::vector<std::uint32_t> & sounderIDs = params.GetHistogramSounders();

		M3DKernel *pKernel = M3DKernel::GetInstance();

		// G�n�ration de la liste des plages de dB une fois pour toutes
		std::vector<std::pair<double, double> > listPlages;
		for (double currentMindB = dbMin; currentMindB < dbMax; currentMindB += dbWidth)
		{
			listPlages.push_back(std::make_pair(currentMindB, std::min(currentMindB + dbWidth, dbMax)));
		}

		// R�cup�ration de la d�finition des sondeurs
		const CurrentSounderDefinition & soundersDef = pKernel->getObjectMgr()->GetSounderDefinition();

		bool bNoData = true;
		// map du nombre de single target track�e et non track�e par fr�quence et par dB moyen pour la plage de dB,
		std::map<unsigned short, std::map<std::pair<double, double>, std::pair<int, int> > > mapSingleTargetByFreq;
		std::map<unsigned short, double> mapFrequencies;
		for (unsigned int iSounder = 0; iSounder < soundersDef.GetNbSounder(); iSounder++)
		{
			Sounder * pSounder = soundersDef.GetSounder(iSounder);
			// Si pas de sondeur d�fini, on les affiche tous...
			if (sounderIDs.empty() || std::find(sounderIDs.begin(), sounderIDs.end(), pSounder->m_SounderId) != sounderIDs.end())
			{
				for (unsigned int iTrans = 0; iTrans < pSounder->GetTransducerCount(); iTrans++)
				{
					Transducer * pTrans = pSounder->GetTransducer(iTrans);
					const std::vector<unsigned short> & channelIDs = pTrans->GetChannelId();
					for (size_t iChan = 0; iChan < channelIDs.size(); iChan++)
					{
						SoftChannel * pSoftChan = pTrans->getSoftChannel(channelIDs[iChan]);
						if (pSoftChan)
						{
							mapFrequencies[pSoftChan->getSoftwareChannelId()] = pSoftChan->m_acousticFrequency / 1000;
							std::map<std::pair<double, double>, std::pair<int, int> > & mapTS = mapSingleTargetByFreq[channelIDs[iChan]];
							// Pour chaque plage de dB...
							for (size_t iPlage = 0; iPlage < listPlages.size(); iPlage++)
							{
								mapTS[listPlages[iPlage]] = std::make_pair<int, int>(0, 0);
								bNoData = false;
							}
						}
					}
				}
			}
		}

		if (!bNoData)
		{
			const std::map<std::uint64_t, TSTrack*> & tracks = pTSModule->getTracks();
			SounderIndexedTimeContainer * pContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetSingleTargetContainer();
			for (size_t iSingleTarget = 0; iSingleTarget < pContainer->GetObjectCount(); iSingleTarget++)
			{
				PingFanSingleTarget *pSingle = (PingFanSingleTarget*)pContainer->GetObjectWithIndex(iSingleTarget);

				if (pSingle && pSingle->m_ObjectTime >= startTime && (endTime.IsNull() || pSingle->m_ObjectTime <= endTime))
				{
					for (unsigned int i = 0; i < pSingle->GetNumberOfTarget(); i++)
					{
						SingleTargetDataObject *pSingleTargetData = pSingle->GetTarget(i);
						std::map<unsigned short, std::map<std::pair<double, double>, std::pair<int, int> > >::iterator iterFreq = mapSingleTargetByFreq.find(pSingleTargetData->m_parentSTId);
						if (iterFreq != mapSingleTargetByFreq.end())
						{
							for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataCWCount(); numTarget++)
							{
								const SingleTargetDataCW & tsData = pSingleTargetData->GetSingleTargetDataCW(numTarget);
								std::map<std::pair<double, double>, std::pair<int, int> >::iterator iter;
								for (iter = iterFreq->second.begin(); iter != iterFreq->second.end(); iter++)
								{
									// On regarde si la cible est dans la plage en dB qui nous int�resse
									if (tsData.m_compensatedTS >= iter->first.first && tsData.m_compensatedTS < iter->first.second)
									{
										// On incr�mente le compteur track� ou non track� en fonction
										bool bIsTracked = false;
										if (tsData.m_trackLabel > 0)
										{
											std::map<std::uint64_t, TSTrack*>::const_iterator iterTrack = tracks.find(tsData.m_trackLabel);
											if (iterTrack != tracks.end())
											{
												bIsTracked = pTSModule->IsValidTrack(iterTrack->second);
											}
										}
										if (bIsTracked)
										{
											iter->second.first += 1;
										}
										else
										{
											iter->second.second += 1;
										}
									}
								}
							}
						}
					}
				}
			}

			// Calcul fr�quence par fr�quence du nombre de cible par plage de dB pour normalisation
			std::map<double, std::pair<int, int> > mapNbTS;
			std::map<unsigned short, std::map<std::pair<double, double>, std::pair<int, int> > >::iterator iterFreq;
			for (iterFreq = mapSingleTargetByFreq.begin(); iterFreq != mapSingleTargetByFreq.end(); ++iterFreq)
			{
				double dbFreq = mapFrequencies[iterFreq->first];
				std::map<double, std::pair<int, int> >::iterator iter = mapNbTS.find(dbFreq);
				if (iter == mapNbTS.end())
				{
					mapNbTS[dbFreq] = std::make_pair<int, int>(0, 0);
				}
				std::pair<int, int> & nbTsForFreq = mapNbTS[dbFreq];
				std::map<std::pair<double, double>, std::pair<int, int> >::iterator iterTS;
				for (iterTS = iterFreq->second.begin(); iterTS != iterFreq->second.end(); ++iterTS)
				{
					nbTsForFreq.first += iterTS->second.first;
					nbTsForFreq.second += iterTS->second.second;
				}
			}

			// On trie tout �a par fr�quence
			std::map<double, std::map<std::pair<double, double>, std::pair<int, int> > * > mapSingleTargetByRealFreq;
			for (iterFreq = mapSingleTargetByFreq.begin(); iterFreq != mapSingleTargetByFreq.end(); ++iterFreq)
			{
				mapSingleTargetByRealFreq[mapFrequencies[iterFreq->first]] = &iterFreq->second;
			}

			ColorPaletteFrequency palette;
			palette.ComputePalette(mapSingleTargetByRealFreq.size());
			
			auto series = this->Series;

			// cr�ation d'un tableau par fr�quence (ou deux par fr�quence si on affiche les cibles track�es et non track�es
			
			std::map<double, std::map<std::pair<double, double>, std::pair<int, int> > * >::const_iterator iterRealFreq;
			
			const int nbCol = 1 + mapSingleTargetByRealFreq.size()  + ((graphType == eTSDisplayAll) ? mapSingleTargetByRealFreq.size() : 0);
			const int nbLines = listPlages.size();

			// tableau correspondant aux plages en dB(axe horizontal)
			std::vector< std::vector<double> > table(listPlages.size(), std::vector<double>(nbCol, 0.0));

			// Peuplement de la table
			for (int i = 0; i < nbLines; ++i)
			{
				const std::pair<double, double> & dbRange = listPlages[i];
				int iCol = 0;
				// On affecte � la plage en dB la valeur m�diane de la plage
				table[i][iCol++] = (dbRange.first + dbRange.second) / 2.0;

				// Ensuite, peuplement pour chaque pr�quence
				for (iterRealFreq = mapSingleTargetByRealFreq.begin(); iterRealFreq != mapSingleTargetByRealFreq.end(); ++iterRealFreq)
				{
					float fValue = 0.f;
					const std::pair<int, int> & nbTS = mapNbTS[iterRealFreq->first];
					if (graphType == eTSDisplayTracked || graphType == eTSDisplayAll)
					{
						if (nbTS.first > 0)
						{
							fValue = (float)iterRealFreq->second->operator [](dbRange).first / nbTS.first;
						}
					}
					else
					{
						if (nbTS.second > 0)
						{
							fValue = (float)iterRealFreq->second->operator [](dbRange).second / nbTS.second;
						}
					}
					table[i][iCol++] = fValue;
					if (graphType == eTSDisplayAll)
					{
						fValue = 0.f;
						if (nbTS.second > 0)
						{
							fValue = (float)iterRealFreq->second->operator [](dbRange).second / nbTS.second;
						}
						table[i][iCol++] = fValue;
					}
				}
			}

			// D�finition des barres � afficher, des couleurs, etc...
			unsigned int iCol = 1;
			unsigned int iFreq = 1;
			for (iterRealFreq = mapSingleTargetByRealFreq.begin(); iterRealFreq != mapSingleTargetByRealFreq.end(); ++iterRealFreq)
			{
				System::String ^ serieName = System::String::Format("{0} kHz ({1})"
					, round(iterRealFreq->first)
					, (graphType == eTSDisplayTracked || graphType == eTSDisplayAll) ? "tracked" : "untracked");
				
				System::Windows::Forms::DataVisualization::Charting::Series ^ line = series->FindByName(serieName);
				if (line)
				{
					deprecatedSeries[line] = true;
					line->Points->Clear();
				}
				else
				{
					line = series->Add(serieName);
					line->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Column;

					const auto & color = palette.GetColor(iFreq - 1);
					if (graphType == eTSDisplayAll)
					{
						// si on affiche track�es et non track�es, on affiche les track�es en ligne simple sans remplissage
						line->Color = System::Drawing::Color::FromArgb(255, 255, 255, 255);
						line->BorderColor = System::Drawing::Color::FromArgb(color.a, color.r, color.g, color.b);
					}
					else
					{
						line->Color = System::Drawing::Color::FromArgb(color.a, color.r, color.g, color.b);
						line->BorderWidth = 0;
					}

				}

				for (int i = 0; i < nbLines; ++i)
				{
					line->Points->AddXY(table[i][0], table[i][iCol]);
				}				

				if (graphType == eTSDisplayAll)
				{
					iCol += 2;
				}
				else
				{
					iCol++;
				}
				iFreq++;
			}

			if (graphType == eTSDisplayAll)
			{
				iCol = 2;
				iFreq = 1;
				for (iterRealFreq = mapSingleTargetByRealFreq.begin(); iterRealFreq != mapSingleTargetByRealFreq.end(); ++iterRealFreq)
				{
					System::String ^ serieName = System::String::Format("{0} kHz (untracked)", round(iterRealFreq->first));

					System::Windows::Forms::DataVisualization::Charting::Series ^ line = series->FindByName(serieName);
					if (line)
					{
						deprecatedSeries[line] = true;
						line->Points->Clear();
					}
					else
					{
						line = series->Add(serieName);
						line->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Column;

						const auto & color = palette.GetColor(iFreq - 1);
						line->Color = System::Drawing::Color::FromArgb(color.a, color.r, color.g, color.b);
						line->BorderWidth = 0;
					}
					
					for (int i = 0; i < nbLines; ++i)
					{
						line->Points->AddXY(table[i][0], table[i][iCol]);
					}


					iCol += 2;
					iFreq++;
				}
			}
		}
	}

	/// Nettoyage des series non utilis�es
	for each(auto entry in deprecatedSeries)
	{
		if (entry.Value == false)
		{
			this->Series->Remove(entry.Key);
		}
	}

	this->Series->ResumeUpdates();

	
	this->ChartAreas[0]->AxisX->Minimum = dbMin;
	this->ChartAreas[0]->AxisX->Maximum = dbMax;
	this->ChartAreas[0]->AxisX->RoundAxisValues();

	this->ChartAreas[0]->AxisY->Minimum = 0;
	this->ChartAreas[0]->AxisY->Maximum = 1;
	this->ChartAreas[0]->AxisY->RoundAxisValues();
}
