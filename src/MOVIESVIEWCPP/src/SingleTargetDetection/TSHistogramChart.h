#pragma once

#include "Chart/ChartInteractor.h"

class ESUParameter;

ref class TSHistogramChart : public System::Windows::Forms::DataVisualization::Charting::Chart
{
	ChartInteractor ^chartInteractor;
public:
	TSHistogramChart();
	
	void UpdateHistogram(ESUParameter * pESUParams);
};

