#include "TSHistogramsForm.h"
#include "ESUItem.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/MovESUMgr.h"

using namespace MOVIESVIEWCPP;


TSHistogramsForm::TSHistogramsForm()
{
	InitializeComponent();
	m_dirty = true;

	Text = "TS Histogram";

	m_pTSChart = gcnew TSHistogramChart();
	m_pTSChart->Dock = System::Windows::Forms::DockStyle::Fill;
	this->panelChart->Controls->Add(m_pTSChart);
}

System::Void TSHistogramsForm::InvalidateHistogram() { m_dirty = true; }

bool TSHistogramsForm::NeedUpdate::get() { return m_dirty; }

System::Void TSHistogramsForm::UpdateHistogram()
{
	m_dirty = false;

	// Récupération de la liste des ESU en mémoire :
	M3DKernel * pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	ESUContainer& esuContainer = pKernel->getMovESUManager()->GetESUContainer();

	// On note l'ESU éventuellement sélectionné précédemment
	ESUItem ^previouslySelectedItem = (ESUItem^)this->comboBoxESU->SelectedItem;
	ESUItem ^newlySelectedItem = nullptr;

	// On constitue la nouvelle liste des ESU disponibles
	this->comboBoxESU->SuspendLayout();
	this->comboBoxESU->Items->Clear();

	size_t esuNb = esuContainer.GetESUNb();
	for (size_t iESU = 0; iESU < esuNb; iESU++)
	{
		ESUParameter * esu = esuContainer.GetESUWithIdx((std::uint32_t)iESU);
		ESUItem ^esuItem = gcnew ESUItem(esu->GetESUId());
		this->comboBoxESU->Items->Add(esuItem);

		if (previouslySelectedItem && !newlySelectedItem && esuItem->Equals(previouslySelectedItem))
		{
			newlySelectedItem = esuItem;
		}
	}
	// Enfin on ajoute l'ESU en cours de traitement
	ESUParameter * pCurrentESU = pKernel->getMovESUManager()->GetWorkingESU();
	if (pCurrentESU)
	{
		ESUItem ^esuItem = gcnew ESUItem(0);
		esuItem->bCurrentESU = true;
		this->comboBoxESU->Items->Add(esuItem);

		if (previouslySelectedItem && !newlySelectedItem && esuItem->Equals(previouslySelectedItem))
		{
			newlySelectedItem = esuItem;
		}
	}

	// On sélectionne l'ancien ESU Sélectionné le cas échéant, ou le derneir item si aucun item n'était sélectionné
	if (newlySelectedItem == nullptr && this->comboBoxESU->Items->Count > 0)
	{
		newlySelectedItem = (ESUItem^)this->comboBoxESU->Items[this->comboBoxESU->Items->Count - 1];
	}
	this->comboBoxESU->SelectedItem = newlySelectedItem;

	pKernel->Unlock();

	this->comboBoxESU->ResumeLayout();
}

System::Void TSHistogramsForm::comboBoxESU_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	M3DKernel * pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	ESUParameter * pESUParams = NULL;
	ESUItem ^selectedItem = (ESUItem^)this->comboBoxESU->SelectedItem;
	System::String ^label = nullptr;
	if (selectedItem)
	{
		pESUParams = pKernel->getMovESUManager()->GetESUContainer().GetESUById(selectedItem->esuId);
		if (!pESUParams)
		{
			pESUParams = pKernel->getMovESUManager()->GetWorkingESU();
			label = String::Format("From ping {0}", pESUParams->GetESUPingNumber());
		}
	}

	if (pESUParams)
	{
		if (!label)
		{
			label = String::Format("From ping {0} to ping {1}", pESUParams->GetESUPingNumber(), pESUParams->GetESULastPingNumber());
		}
		this->labelESU->Text = label;
	}
	else
	{
		this->labelESU->Text = "No Selected ESU.";
	}

	m_pTSChart->UpdateHistogram(pESUParams);
	pKernel->Unlock();
}