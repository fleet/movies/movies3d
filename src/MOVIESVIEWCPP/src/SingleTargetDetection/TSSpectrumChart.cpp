#include "TSSpectrumChart.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"

#include "ModuleManager/ModuleManager.h"

#include "TSAnalysis/TSAnalysisModule.h"
#include "DisplayParameter.h"
#include "ColorPaletteFrequency.h"

namespace {
	struct TSPoint
	{
		double freq;
		double sum;
		int nbPoint;

		TSPoint() : sum(0.0) , nbPoint(0) {}
	};
}

TSSpectrumChart::TSSpectrumChart()
{
	auto chartArea = gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea();
	auto chartLegend = gcnew System::Windows::Forms::DataVisualization::Charting::Legend();
	System::Windows::Forms::DataVisualization::Charting::Series^  series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
	
	BeginInit();
	chartArea->Name = L"ChartArea";

	chartArea->AxisX->Title = "Freq (Hz)";
	chartArea->AxisX->TitleFont = gcnew System::Drawing::Font(chartArea->AxisX->TitleFont, System::Drawing::FontStyle::Bold);
	chartArea->AxisX->MajorGrid->LineColor = System::Drawing::Color::LightGray;
	chartArea->AxisX->IntervalAutoMode = System::Windows::Forms::DataVisualization::Charting::IntervalAutoMode::VariableCount;
	chartArea->AxisX->IsStartedFromZero = false;
	
	chartArea->AxisY->Title = "T S (dB)";
	chartArea->AxisY->TitleFont = gcnew System::Drawing::Font(chartArea->AxisY->TitleFont, System::Drawing::FontStyle::Bold); 
	chartArea->AxisY->Interval = 5;
	chartArea->AxisY->MajorGrid->LineColor = System::Drawing::Color::LightGray;
	
	ChartAreas->Add(chartArea);

	chartLegend->Name = L"Legend";
	chartLegend->LegendStyle = System::Windows::Forms::DataVisualization::Charting::LegendStyle::Column;
	Legends->Add(chartLegend);

	EndInit();

	chartInteractor = gcnew ChartInteractor();
	chartInteractor->Install(this);
}

void TSSpectrumChart::UpdateSpectrum(unsigned int esuId, std::uint64_t trackLabel)
{
	this->Series->SuspendUpdates();
	this->Series->Clear();

	auto pKernel = M3DKernel::GetInstance();
	ESUParameter * pESUParams = pKernel->getMovESUManager()->GetESUContainer().GetESUById(esuId);
	if (!pESUParams)
	{
		pESUParams = pKernel->getMovESUManager()->GetWorkingESU();
	}

	double yMin = double(System::Decimal::MaxValue);
	double yMax = double(System::Decimal::MinValue);

	if (pESUParams)
	{
		const HacTime & startTime = pESUParams->GetESUTime();
		const HacTime & endTime = pESUParams->GetESUEndTime();

		const double minValue = double(System::Decimal::MinValue);
		const double maxValue = double(System::Decimal::MaxValue);
		
		TSAnalysisModule * pTSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
		
		std::map<double, TSPoint> tsPoints;
		
		// Récupération de la définition des sondeurs
		const CurrentSounderDefinition & soundersDef = pKernel->getObjectMgr()->GetSounderDefinition();
		const auto & tracks = pTSModule->getTracks();
		SounderIndexedTimeContainer * pContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetSingleTargetContainer();
		for (size_t iSingleTarget = 0; iSingleTarget < pContainer->GetObjectCount(); iSingleTarget++)
		{
			PingFanSingleTarget *pSingle = (PingFanSingleTarget*)pContainer->GetObjectWithIndex(iSingleTarget);

			if (pSingle && pSingle->m_ObjectTime >= startTime && (endTime.IsNull() || pSingle->m_ObjectTime <= endTime))
			{
				for (unsigned int i = 0; i < pSingle->GetNumberOfTarget(); i++)
				{
					SingleTargetDataObject *pSingleTargetData = pSingle->GetTarget(i);
					for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataFMCount(); numTarget++)
					{
						const SingleTargetDataFM & tsData = pSingleTargetData->GetSingleTargetDataFM(numTarget);
						if (tsData.m_trackLabel == trackLabel)
						{
							auto spectrum = gcnew System::Windows::Forms::DataVisualization::Charting::Series("TS Spectrum[" + this->Series->Count + "] " + tsData.m_trackLabel);
							spectrum->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Line;
							this->Series->Add(spectrum);

							for (int iF = 0; iF < tsData.m_compensatedTS.size(); iF++)
							{
								const auto freq = tsData.m_freq[iF];
								const auto compensatedTS = tsData.m_compensatedTS[iF];

								if ((freq >= minValue && freq <= maxValue) && (compensatedTS >= minValue && compensatedTS <= maxValue))
								{
									spectrum->Points->AddXY(freq, compensatedTS);
									yMin = std::min(compensatedTS, yMin);
									yMax = std::max(compensatedTS, yMax);

									auto & tsPoint = tsPoints[freq];
									tsPoint.freq = freq;
									tsPoint.sum += pow(10.0, compensatedTS * 0.1);
									tsPoint.nbPoint += 1;
								}
							}
						}
					}
				}
			}
		}

		//
		if (tsPoints.size() > 0 && this->Series->Count > 1)
		{
			auto spectrum = gcnew System::Windows::Forms::DataVisualization::Charting::Series("Mean TS Spectrum");
			spectrum->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Line;
			spectrum->BorderWidth = 2;
			this->Series->Add(spectrum);

			for (auto it = tsPoints.cbegin(); it != tsPoints.cend(); ++it)
			{
				const auto & tsPoint = it->second;
				if (tsPoint.nbPoint > 0)
				{
					const auto compensatedTS = 10 * log10(tsPoint.sum / tsPoint.nbPoint);
					spectrum->Points->AddXY(tsPoint.freq, compensatedTS);
					yMin = std::min(compensatedTS, yMin);
					yMax = std::max(compensatedTS, yMax);
				}
			}
		}
	}

	this->ChartAreas[0]->AxisX->RoundAxisValues();
	
	// find lower 
	int p = log10(abs(yMin));
	int n = yMin / pow(10, p);
	yMin = (n - 1) * pow(10, p);

	p = log10(abs(yMax));
	n = yMax / pow(10, p);
	yMax = n * pow(10, p);

	this->ChartAreas[0]->AxisY->Minimum = yMin;
	this->ChartAreas[0]->AxisY->Maximum = yMax;
	///this->ChartAreas[0]->AxisY->RoundAxisValues();
	this->Series->ResumeUpdates();
}