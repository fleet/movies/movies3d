#pragma once

#include <cstdint>
#include "Chart/ChartInteractor.h"

class ESUParameter;

ref class TSSpectrumChart : public System::Windows::Forms::DataVisualization::Charting::Chart
{
	ChartInteractor ^chartInteractor;
public:
	TSSpectrumChart();
	
	void UpdateSpectrum(unsigned int esuId, std::uint64_t trackLabel);
};

