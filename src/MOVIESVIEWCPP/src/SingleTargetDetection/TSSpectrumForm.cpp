#include "TSSpectrumForm.h"
#include "ESUItem.h"

#include "M3DKernel/M3DKernel.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "ModuleManager/ModuleManager.h"
#include "TSAnalysis/TSAnalysisModule.h"

#include <set>

using namespace MOVIESVIEWCPP;

ref class TrackItem {

public:
	TrackItem(std::uint64_t trackLabel) { this->trackLabel = trackLabel; }

	virtual System::String ^ ToString() override
	{
		return System::String::Format("Track {0}", trackLabel);
	}

	virtual bool Equals(System::Object ^obj) override
	{
		TrackItem ^otherItem = dynamic_cast<TrackItem^>(obj);
		if (otherItem == nullptr) {
			return false;
		}
		return trackLabel == otherItem->trackLabel;
	}

	// Track label
	std::uint64_t trackLabel;
};

TSSpectrumsForm::TSSpectrumsForm()
{
	InitializeComponent();
	m_dirty = true;

	Text = "TS Spectrum";

	m_pTSChart = gcnew TSSpectrumChart();
	m_pTSChart->Dock = System::Windows::Forms::DockStyle::Fill;
	this->panelChart->Controls->Add(m_pTSChart);
}

System::Void TSSpectrumsForm::InvalidateSpectrum() { m_dirty = true; }

bool TSSpectrumsForm::NeedUpdate::get() { return m_dirty; }

System::Void TSSpectrumsForm::UpdateSpectrum()
{
	if (m_dirty == false)
		return;

	m_dirty = false;

	// Récupération de la liste des ESU en mémoire :
	M3DKernel * pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	ESUContainer& esuContainer = pKernel->getMovESUManager()->GetESUContainer();

	// On note l'ESU éventuellement sélectionné précédemment
	ESUItem ^previouslySelectedItem = (ESUItem^)this->comboBoxESU->SelectedItem;
	ESUItem ^newlySelectedItem = nullptr;

	// On constitue la nouvelle liste des ESU disponibles
	this->comboBoxESU->SuspendLayout();
	this->comboBoxESU->Items->Clear();

	size_t esuNb = esuContainer.GetESUNb();
	for (size_t iESU = 0; iESU < esuNb; iESU++)
	{
		ESUParameter * esu = esuContainer.GetESUWithIdx((std::uint32_t)iESU);
		ESUItem ^esuItem = gcnew ESUItem(esu->GetESUId());
		this->comboBoxESU->Items->Add(esuItem);

		if (previouslySelectedItem && !newlySelectedItem && esuItem->Equals(previouslySelectedItem))
		{
			newlySelectedItem = esuItem;
		}
	}
	// Enfin on ajoute l'ESU en cours de traitement
	ESUParameter * pCurrentESU = pKernel->getMovESUManager()->GetWorkingESU();
	if (pCurrentESU)
	{
		ESUItem ^esuItem = gcnew ESUItem(0);
		esuItem->bCurrentESU = true;
		this->comboBoxESU->Items->Add(esuItem);

		if (previouslySelectedItem && !newlySelectedItem && esuItem->Equals(previouslySelectedItem))
		{
			newlySelectedItem = esuItem;
		}
	}

	// On sélectionne l'ancien ESU Sélectionné le cas échéant, ou le derneir item si aucun item n'était sélectionné
	if (newlySelectedItem == nullptr && this->comboBoxESU->Items->Count > 0)
	{
		newlySelectedItem = (ESUItem^)this->comboBoxESU->Items[this->comboBoxESU->Items->Count - 1];
	}
	this->comboBoxESU->SelectedItem = newlySelectedItem;

	pKernel->Unlock();

	this->comboBoxESU->ResumeLayout();
}

System::Void TSSpectrumsForm::UpdateTargets(ESUParameter * pESUParams)
{
	// On note l'ESU éventuellement sélectionné précédemment
	auto previouslySelectedItem = this->comboBoxTarget->SelectedItem;
	

	// On constitue la nouvelle liste des targets pour l'ESU
	this->comboBoxTarget->SuspendLayout();
	this->comboBoxTarget->Items->Clear();

	std::set<std::uint64_t> trackLabels;

	if (pESUParams)
	{
		// Récupération de la liste des ESU en mémoire :
		M3DKernel * pKernel = M3DKernel::GetInstance();
		pKernel->Lock();

		const HacTime & startTime = pESUParams->GetESUTime();
		const HacTime & endTime = pESUParams->GetESUEndTime();

		double minValue = double(System::Decimal::MinValue);
		double maxValue = double(System::Decimal::MaxValue);

		TSAnalysisModule * pTSModule = CModuleManager::getInstance()->GetTSAnalysisModule();
		
		// Récupération de la définition des sondeurs
		const CurrentSounderDefinition & soundersDef = pKernel->getObjectMgr()->GetSounderDefinition();
		const std::map<std::uint64_t, TSTrack*> & tracks = pTSModule->getTracks();
		SounderIndexedTimeContainer * pContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetSingleTargetContainer();
		for (size_t iSingleTarget = 0; iSingleTarget < pContainer->GetObjectCount(); iSingleTarget++)
		{
			PingFanSingleTarget *pSingle = (PingFanSingleTarget*)pContainer->GetObjectWithIndex(iSingleTarget);

			if (pSingle && pSingle->m_ObjectTime >= startTime && (endTime.IsNull() || pSingle->m_ObjectTime <= endTime))
			{
				for (unsigned int i = 0; i < pSingle->GetNumberOfTarget(); i++)
				{
					SingleTargetDataObject *pSingleTargetData = pSingle->GetTarget(i);
					for (unsigned int numTarget = 0; numTarget < pSingleTargetData->GetSingleTargetDataFMCount(); numTarget++)
					{
						const SingleTargetDataFM & tsData = pSingleTargetData->GetSingleTargetDataFM(numTarget);
						trackLabels.insert(tsData.m_trackLabel);
					}
				}
			}
		}

		pKernel->Unlock();
	}

	Object ^ newlySelectedItem = nullptr;

	for (auto trackLabel : trackLabels)
	{
		auto trackItem = gcnew TrackItem(trackLabel);
		this->comboBoxTarget->Items->Add(trackItem);
		if (previouslySelectedItem != nullptr && newlySelectedItem == nullptr  && trackItem->Equals(previouslySelectedItem))
		{
			newlySelectedItem = trackItem;
		}
	}
	
	if (newlySelectedItem != nullptr)
	{
		newlySelectedItem = this->comboBoxTarget->Items[this->comboBoxTarget->Items->Count - 1];
	}
	this->comboBoxTarget->SelectedItem = previouslySelectedItem;
	
	this->comboBoxTarget->ResumeLayout();
}

System::Void TSSpectrumsForm::comboBoxESU_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	M3DKernel * pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	ESUParameter * pESUParams = NULL;
	ESUItem ^selectedItem = (ESUItem^)this->comboBoxESU->SelectedItem;
	System::String ^label = nullptr;
	if (selectedItem)
	{
		pESUParams = pKernel->getMovESUManager()->GetESUContainer().GetESUById(selectedItem->esuId);
		if (!pESUParams)
		{
			pESUParams = pKernel->getMovESUManager()->GetWorkingESU();
			label = String::Format("From ping {0}", pESUParams->GetESUPingNumber());
		}
	}

	if (pESUParams)
	{
		if (!label)
		{
			label = String::Format("From ping {0} to ping {1}", pESUParams->GetESUPingNumber(), pESUParams->GetESULastPingNumber());
		}
		this->labelESU->Text = label;
	}
	else
	{
		this->labelESU->Text = "No Selected ESU.";
	}

	pKernel->Unlock();

	// Update target list
	UpdateTargets(pESUParams);
}

System::Void TSSpectrumsForm::comboBoxTarget_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e)
{
	UpdateChart();
}

System::Void TSSpectrumsForm::UpdateChart()
{
	std::uint32_t esuId = 0;
	std::uint64_t trackLabel = 0;

	auto selectedEsu = (ESUItem^)this->comboBoxESU->SelectedItem;
	if (selectedEsu)
	{
		esuId = selectedEsu->esuId;
	}

	auto selectedTarget = (TrackItem^) this->comboBoxTarget->SelectedItem;
	if (selectedTarget)
	{
		trackLabel = selectedTarget->trackLabel;
	}

	m_pTSChart->UpdateSpectrum(esuId, trackLabel);
}