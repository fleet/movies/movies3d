#pragma once

#include "TSSpectrumChart.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de TSSpectrumsForm
	/// </summary>
	public ref class TSSpectrumsForm : public System::Windows::Forms::UserControl
	{
	public:
		TSSpectrumsForm();

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~TSSpectrumsForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	protected:
	private: System::Windows::Forms::Panel^  panelChart;
	private: System::Windows::Forms::FlowLayoutPanel^  flowLayoutPanel1;
	private: System::Windows::Forms::ComboBox^  comboBoxESU;
	private: System::Windows::Forms::Label^  labelESU;
	private: System::Windows::Forms::ComboBox^  comboBoxTarget;
	private: System::Windows::Forms::Label^  label1;




	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->panelChart = (gcnew System::Windows::Forms::Panel());
			this->flowLayoutPanel1 = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->comboBoxESU = (gcnew System::Windows::Forms::ComboBox());
			this->labelESU = (gcnew System::Windows::Forms::Label());
			this->comboBoxTarget = (gcnew System::Windows::Forms::ComboBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->tableLayoutPanel1->SuspendLayout();
			this->flowLayoutPanel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 1;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->panelChart, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->flowLayoutPanel1, 0, 0);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel1->Margin = System::Windows::Forms::Padding(0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 2;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(417, 375);
			this->tableLayoutPanel1->TabIndex = 0;
			// 
			// panelChart
			// 
			this->panelChart->Dock = System::Windows::Forms::DockStyle::Fill;
			this->panelChart->Location = System::Drawing::Point(0, 27);
			this->panelChart->Margin = System::Windows::Forms::Padding(0);
			this->panelChart->Name = L"panelChart";
			this->panelChart->Size = System::Drawing::Size(417, 348);
			this->panelChart->TabIndex = 0;
			// 
			// flowLayoutPanel1
			// 
			this->flowLayoutPanel1->AutoSize = true;
			this->flowLayoutPanel1->Controls->Add(this->comboBoxESU);
			this->flowLayoutPanel1->Controls->Add(this->labelESU);
			this->flowLayoutPanel1->Controls->Add(this->label1);
			this->flowLayoutPanel1->Controls->Add(this->comboBoxTarget);
			this->flowLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->flowLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->flowLayoutPanel1->Margin = System::Windows::Forms::Padding(0);
			this->flowLayoutPanel1->Name = L"flowLayoutPanel1";
			this->flowLayoutPanel1->Size = System::Drawing::Size(417, 27);
			this->flowLayoutPanel1->TabIndex = 1;
			// 
			// comboBoxESU
			// 
			this->comboBoxESU->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxESU->FormattingEnabled = true;
			this->comboBoxESU->Location = System::Drawing::Point(0, 3);
			this->comboBoxESU->Margin = System::Windows::Forms::Padding(0, 3, 3, 3);
			this->comboBoxESU->Name = L"comboBoxESU";
			this->comboBoxESU->Size = System::Drawing::Size(85, 21);
			this->comboBoxESU->TabIndex = 0;
			this->comboBoxESU->SelectedIndexChanged += gcnew System::EventHandler(this, &TSSpectrumsForm::comboBoxESU_SelectedIndexChanged);
			// 
			// labelESU
			// 
			this->labelESU->AutoSize = true;
			this->labelESU->Location = System::Drawing::Point(91, 6);
			this->labelESU->Margin = System::Windows::Forms::Padding(3, 6, 3, 0);
			this->labelESU->Name = L"labelESU";
			this->labelESU->Size = System::Drawing::Size(0, 13);
			this->labelESU->TabIndex = 1;
			// 
			// comboBoxTarget
			// 
			this->comboBoxTarget->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxTarget->FormattingEnabled = true;
			this->comboBoxTarget->Location = System::Drawing::Point(112, 3);
			this->comboBoxTarget->Name = L"comboBoxTarget";
			this->comboBoxTarget->Size = System::Drawing::Size(77, 21);
			this->comboBoxTarget->TabIndex = 2;
			this->comboBoxTarget->SelectedIndexChanged += gcnew System::EventHandler(this, &TSSpectrumsForm::comboBoxTarget_SelectedIndexChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(97, 6);
			this->label1->Margin = System::Windows::Forms::Padding(3, 6, 3, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(9, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"|";
			// 
			// TSSpectrumsForm
			// 
			this->Controls->Add(this->tableLayoutPanel1);
			this->Name = L"TSSpectrumsForm";
			this->Size = System::Drawing::Size(417, 375);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			this->flowLayoutPanel1->ResumeLayout(false);
			this->flowLayoutPanel1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

	public:
		System::Void InvalidateSpectrum();
		property bool NeedUpdate { bool get();  };
		System::Void UpdateSpectrum();

	private:
		TSSpectrumChart ^ m_pTSChart;
		bool m_dirty;

	private:
		System::Void comboBoxESU_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void comboBoxTarget_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

		System::Void UpdateTargets(ESUParameter * pESUParams);

		System::Void UpdateChart();
	};
}
