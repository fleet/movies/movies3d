#include "Sounder3D.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/PingFanContainer.h"
#include "M3DKernel/datascheme/Transducer.h"
Sounder3D::Sounder3D(std::uint32_t sounderId) :
	DataObject3D(),
	m_sounderId(sounderId)
{
	Sounder *pSounder = getSounder();

	for (unsigned int i = 0; i < pSounder->GetTransducerCount(); i++)
	{
		m_tranducer3D.push_back(new Transducer3D(this, i));
	}
	if (pSounder->m_isMultiBeam)
	{
		m_bIsVisible = false;
	}
	else
	{
		m_bIsVisible = true;
	}
}


Sounder3D::~Sounder3D(void)
{

	while (m_tranducer3D.size())
	{
		delete m_tranducer3D[0];
		m_tranducer3D.erase(m_tranducer3D.begin());

	}
}

Sounder *Sounder3D::getSounder()
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	Sounder *pSounder = pKernel->getObjectMgr()->GetLastValidSounder(m_sounderId);
	assert(pSounder);
	return pSounder;
}

Transducer3D* Sounder3D::GetTransducer(const char * transName)
{
	Transducer3D * pTrans = NULL;
	for (unsigned int i = 0; i < m_tranducer3D.size(); i++)
	{
		if (strcmp(m_tranducer3D[i]->GetTransducer()->m_transName, transName) == 0)
		{
			pTrans = m_tranducer3D[i];
			break;
		}
	}
	return pTrans;
}
