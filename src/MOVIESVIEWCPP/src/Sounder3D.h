#pragma once
#include "dataobject3d.h"
#include "Transducer3D.h"
#include "Sounder3D.h"

#include <vector>
class Sounder;

class Sounder3D : public DataObject3D
{
public:
	Sounder3D(std::uint32_t sounderId);
	virtual ~Sounder3D(void);

	std::uint32_t getSounderID() { return m_sounderId; }

	Sounder *getSounder();

	unsigned int GetTransducerCount() { return m_tranducer3D.size(); }
	Transducer3D* GetTransducerIdx(unsigned int idx) { return m_tranducer3D[idx]; }
	Transducer3D* GetTransducer(const char * transName);


	virtual bool IsSounder()
	{
		return true;
	}

private:



private:
	std::uint32_t m_sounderId;
	std::vector<Transducer3D *> m_tranducer3D;





};
