#include "SounderTransducerTreeView.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/PingFanContainer.h"
#include "M3DKernel/datascheme/Transducer.h"

using namespace MOVIESVIEWCPP;

namespace
{
	constexpr const char * SURFACE_LAYER_FILTER = "Surface layers";
    constexpr const char * BOTTOM_LAYER_FILTER = "Bottom layers";
    constexpr const char * DISTANCE_LAYER_FILTER = "Distance layers";
}

void SounderTransducerTreeView::setAfterCheckAction(NodeEventHandler^ eventHandler)
{
    m_afterCheckAction = eventHandler;
}

void SounderTransducerTreeView::setLayerTypeFilterActive(bool active)
{
    if (m_layerTypeFilterActive != active)
    {
        m_layerTypeFilterActive = active;
        UpdateLayerTypeFilterDisplay();        
    }
}

void SounderTransducerTreeView::setTransducerFilterActive(bool active)
{
    m_transducerFilterActive = active;
    if (!active)
    {
        for each (TreeNode^ sounderNode in m_treeView->Nodes)
        {
            updateChildrenCheck(sounderNode);
        }
    }
}

TreeNodeCollection^ SounderTransducerTreeView::getNodes()
{
    return m_treeView->Nodes;
}

std::set<std::string> SounderTransducerTreeView::getCheckedTransducers()
{
    std::set<std::string> checkedTransducers;
    for each (TreeNode^ sounderNode in m_treeView->Nodes)
    {
        for each (TreeNode^ transducerNode in sounderNode->Nodes)
        {
			if (transducerNode->Checked) {
				const auto transducerName = static_cast<const char*>(Runtime::InteropServices::Marshal::StringToHGlobalAnsi(transducerNode->Text).ToPointer());
				checkedTransducers.insert(transducerName);
			}
        }
    }

    return checkedTransducers;
}

void SounderTransducerTreeView::checkTransducers(const std::set<std::string>& checkedTransducers)
{
    for each (TreeNode^ sounderNode in m_treeView->Nodes)
    {   
        for each (TreeNode^ transducerNode in sounderNode->Nodes)
        {
			const auto transducerName = static_cast<const char*>(Runtime::InteropServices::Marshal::StringToHGlobalAnsi(transducerNode->Text).ToPointer());
            transducerNode->Checked = checkedTransducers.count(transducerName);            
        }
    }
}

void SounderTransducerTreeView::checkAllTransducers(TreeNode^ node)
{
    // Recherche du plus haut parent (le noeud sondeur)
    TreeNode^ sounderNode = node;
    while (sounderNode->Parent != nullptr)
    {
        sounderNode = sounderNode->Parent;
    }

    // Cochage/Décochage de tous les enfants (transducteurs + couches)
    sounderNode->Checked = node->Checked;
    updateChildrenCheck(sounderNode);
}

void SounderTransducerTreeView::checkAll() {
    for each (TreeNode^ sounderNode in m_treeView->Nodes) 
    {
        sounderNode->Checked = true;
    }
}

bool SounderTransducerTreeView::isAllchecked() {
    for each (TreeNode^ sounderNode in m_treeView->Nodes) {
        if (!sounderNode->Checked) {
            return false;
        }
        for each (TreeNode^ transducerNode in sounderNode->Nodes) {
            if (!transducerNode->Checked) {
                return false;
            }
        }
    }

    return true;
}

void SounderTransducerTreeView::InitializeSounderTransducersTreeView()
{
    const auto& pKernel = M3DKernel::GetInstance();
    const auto& sounderMgr = pKernel->getObjectMgr()->GetPingFanContainer().m_sounderDefinition;

    m_treeView->BeginUpdate();
    m_treeView->Nodes->Clear();

    // Parcours des sondeurs    
    for (unsigned int i = 0; i < sounderMgr.GetNbSounder(); i++)
    {
        const auto& sounder = sounderMgr.GetSounder(i);

        const auto sounderId = gcnew Int32(sounder->m_SounderId);
        const auto sounderNode = m_treeView->Nodes->Add("Sounder Id[" + sounderId->ToString() + "]");

        // Parcours des transducteurs
        for (const auto& transducer : sounder->GetAllTransducers())
        {
            if (transducer != nullptr)
            {
                const auto transName = gcnew String(transducer->m_transName);
                const auto transNode = sounderNode->Nodes->Add(transName);
                transNode->Checked = false;
                
                UpdateTransducerNodeWithLayerTypeFilter(transNode);
            }
        }
        sounderNode->Expand();
    }
    m_treeView->EndUpdate();
}

void SounderTransducerTreeView::UpdateLayerTypeFilterDisplay()
{
    m_treeView->BeginUpdate();
    for each (TreeNode^ sounderNode in m_treeView->Nodes)
    {   
        for each (TreeNode^ transducerNode in sounderNode->Nodes)
        {
            UpdateTransducerNodeWithLayerTypeFilter(transducerNode);
        }
    }    
    m_treeView->EndUpdate();
}

void SounderTransducerTreeView::UpdateTransducerNodeWithLayerTypeFilter(TreeNode^ node)
{
    node->Nodes->Clear();
    if (m_layerTypeFilterActive)
    {
        node->Nodes->Add(gcnew String(SURFACE_LAYER_FILTER));
        node->Nodes->Add(gcnew String(DISTANCE_LAYER_FILTER));
        node->Nodes->Add(gcnew String(BOTTOM_LAYER_FILTER));                    
    }
}

void SounderTransducerTreeView::treeViewAfterCheck(Object^ sender, TreeViewEventArgs^ e)
{    
    if (!m_isUpdating)
    {
        m_isUpdating = true;
        auto node = e->Node;

        // Appel du call back personnalisé
        if (m_afterCheckAction != nullptr)
        {
            m_afterCheckAction->Invoke(node);
        }

        // Si le filtrage par transducteur n'est pas actif et que le check modifié est celui d'un transducteur, on active tous les transducteurs du sondeur concerné
        if (!m_transducerFilterActive && node->Parent != nullptr)
        {
            checkAllTransducers(node);
        }

        // Mise à jour des parents/enfants
        updateParentCheck(node);
        updateChildrenCheck(node);

        // Fin de la mise à jour
        m_isUpdating = false;  
    }  
}

void SounderTransducerTreeView::updateParentCheck(TreeNode^ node)
{
    // Si le noeud est nul, on sort
    if (node == nullptr)
    {
        return;
    }

    // Si il n'a pas de parent, rien à faire
    const auto parent = node->Parent;
    if (parent == nullptr)
    {
        return;
    }

    // Si le parent a au moin un noeud coché, on coche le parent (puis on met à jour ses parents à lui)
    auto checkParent = false;
    for each (TreeNode^ n in parent->Nodes)
    {
        checkParent = n->Checked;
        if (checkParent)
        {
            break;
        }
    }
        
    if (parent->Checked != checkParent)
    {
        parent->Checked = checkParent;
        updateParentCheck(parent);
    }
}

void SounderTransducerTreeView::updateChildrenCheck(TreeNode^ node)
{
    // Si le noeud est nul, rien à faire
    if (node == nullptr)
    {
        return;
    }
    
    // Cochage/Décochage des noeuds descendants
    for each (TreeNode^ children in node->Nodes)
    {
        if (children->Checked != node->Checked)
        {
            children->Checked = node->Checked;
            updateChildrenCheck(children);
        }
    }
}
