#pragma once
#include <set>

using namespace System;
using namespace System::ComponentModel;
using namespace Collections;
using namespace Windows::Forms;
using namespace Data;
using namespace Drawing;


public delegate void NodeEventHandler(TreeNode^ node);

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Summary for SounderTransducerTreeView
	/// </summary>
	public ref class SounderTransducerTreeView sealed : public UserControl
	{
	public:		
		SounderTransducerTreeView() : SounderTransducerTreeView(false) {}
		SounderTransducerTreeView(const bool layerTypeFilterActive) :
			m_layerTypeFilterActive(layerTypeFilterActive),
			m_transducerFilterActive(true)
		{
			InitializeComponent();
			
			InitializeSounderTransducersTreeView();
		}
		
		/// <summary>
		/// Définir l'action à réaliser lors d'un check sur un noeud
		/// </summary>
		void setAfterCheckAction(NodeEventHandler^ eventHandler);
		
		/// <summary>
		/// Définir la proposition de filtrage par type de couche
		/// </summary>
		void setLayerTypeFilterActive(bool active);
		
		/// <summary>
		/// Définir la proposition de filtrage par transducteur
		/// </summary>
		void setTransducerFilterActive(bool active);
		
		/// <summary>
		/// Récupérer l'ensemble des noeuds
		/// </summary>gcn
		TreeNodeCollection^ getNodes();
		
		/// <summary>
		/// Récupérer les noms de tous les transducteurs cochés
		/// </summary>
		std::set<std::string> getCheckedTransducers();
		
		/// <summary>
		/// Cocher tous les transducteurs fournis
		/// </summary>gcn
		void checkTransducers(const std::set<std::string>& checkedTransducers);

		/// <summary>
		/// Cochage de tous les éléments du sondeur associé
		/// </summary>
		static void checkAllTransducers(TreeNode^ node);
		
		/// <summary>
		/// Cochage de tous les éléments
		/// </summary>
		void checkAll();

		/// <summary>
		/// Vérification si l'ensemble des noeuds est coché
		/// </summary>
		bool isAllchecked();

		/// <summary>
		/// Mise à jour de la coche des noeuds parents
		/// </summary>
		static void updateParentCheck(TreeNode^ node);
		
		/// <summary>
		/// Mise à jour de la coche des noeuds enfants
		/// </summary>
		static void updateChildrenCheck(TreeNode^ node);

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~SounderTransducerTreeView()
		{
			if (m_components)
			{
				delete m_components;
			}

			if (m_afterCheckAction != nullptr)
			{
				delete m_afterCheckAction;
			}
		}
	private:
		/// <summary>
		/// L'arbre controle principal
		/// </summary>
		TreeView^  m_treeView;
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^m_components;
				
		/// <summary>
		/// Activation du filtrage par type de couche
		/// </summary>
		bool m_layerTypeFilterActive;		
				
		/// <summary>
		/// Activation du filtrage par transducteur
		/// </summary>
		bool m_transducerFilterActive;
		
		/// <summary>
		/// Action à effectuer après un check d'une combobox
		/// </summary>
		NodeEventHandler^ m_afterCheckAction;

		/// <summary>
		/// Flag indiquant si une mise à jour des coches est en cours (pour ne pas redeclencher le 'treeViewAfterCheck')
		/// </summary>
		bool m_isUpdating = false;

		/// <summary>
		/// Initialisation des sondeurs et transducteurs dans l'arbre
		/// </summary>
		void InitializeSounderTransducersTreeView();
		
		/// <summary>
		/// Mise à jour de l'affichage des filtres par type de couche
		/// </summary>
		void UpdateLayerTypeFilterDisplay();
		
		/// <summary>
		/// Mise à jour d'un transducteur avec les filtre de type de couche
		/// </summary>
		void UpdateTransducerNodeWithLayerTypeFilter(TreeNode^ node); 

		/// <summary>
		/// Déclencher l'action à réaliser après un changement avoir coché une checkbox
		/// </summary>
		void treeViewAfterCheck(Object^  sender, TreeViewEventArgs^  e);

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->m_treeView = (gcnew System::Windows::Forms::TreeView());
			this->SuspendLayout();
			// 
			// m_treeView
			// 
			this->m_treeView->CheckBoxes = true;
			this->m_treeView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->m_treeView->Location = System::Drawing::Point(0, 0);
			this->m_treeView->Name = L"m_treeView";
			this->m_treeView->Size = System::Drawing::Size(718, 701);
			this->m_treeView->TabIndex = 0;
			this->m_treeView->AfterCheck += gcnew System::Windows::Forms::TreeViewEventHandler(this, &SounderTransducerTreeView::treeViewAfterCheck);
			// 
			// SounderTransducerTreeView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->m_treeView);
			this->Name = L"SounderTransducerTreeView";
			this->Size = System::Drawing::Size(718, 701);
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}
