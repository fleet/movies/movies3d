#include "SpectralAnalysisControl.h"
#include "M3DKernel/M3DKernel.h"

using namespace MOVIESVIEWCPP;

void SpectralAnalysisControl::UpdateConfiguration()
{
	auto& params = M3DKernel::GetInstance()->GetSpectralAnalysisParameters();

	params.setPulseCompressionEnabled(checkPulseCompression->Checked);
	params.setSpectralAnalysisEnabled(checkSpectralAnalysis->Checked);
	//params.setLayerThickness(Decimal::ToDouble(udLayerThickness->Value));
	params.setFrequencyResolution(Decimal::ToInt32(udFrequencyResolution->Value));
}

void SpectralAnalysisControl::UpdateGUI()
{
	auto& params = M3DKernel::GetInstance()->GetSpectralAnalysisParameters();

	checkPulseCompression->Checked = params.isPulseCompressionEnabled();
	checkSpectralAnalysis->Checked = params.isSpectralAnalysisEnabled();
	//udLayerThickness->Value = Decimal(params.getLayerThickness());
	udFrequencyResolution->Value = params.getFrequencyResolution();
}