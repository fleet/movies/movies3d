#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Summary for SpectralAnalysisControl
	/// </summary>
	public ref class SpectralAnalysisControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		SpectralAnalysisControl(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~SpectralAnalysisControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckBox^  checkPulseCompression;
	protected:

	protected:
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::NumericUpDown^  udFrequencyResolution;

	//private: System::Windows::Forms::NumericUpDown^  udLayerThickness;
	private: System::Windows::Forms::CheckBox^  checkSpectralAnalysis;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->checkPulseCompression = (gcnew System::Windows::Forms::CheckBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->checkSpectralAnalysis = (gcnew System::Windows::Forms::CheckBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->udFrequencyResolution = (gcnew System::Windows::Forms::NumericUpDown());
			this->groupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udFrequencyResolution))->BeginInit();
			this->SuspendLayout();
			// 
			// checkPulseCompression
			// 
			this->checkPulseCompression->AutoSize = true;
			this->checkPulseCompression->Location = System::Drawing::Point(13, 3);
			this->checkPulseCompression->Name = L"checkPulseCompression";
			this->checkPulseCompression->Size = System::Drawing::Size(115, 17);
			this->checkPulseCompression->TabIndex = 0;
			this->checkPulseCompression->Text = L"Pulse Compression";
			this->checkPulseCompression->UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->checkSpectralAnalysis);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->udFrequencyResolution);
			this->groupBox1->Location = System::Drawing::Point(3, 26);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(280, 55);
			this->groupBox1->TabIndex = 1;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Spectral Analysis";
			// 
			// checkSpectralAnalysis
			// 
			this->checkSpectralAnalysis->AutoSize = true;
			this->checkSpectralAnalysis->Location = System::Drawing::Point(10, 0);
			this->checkSpectralAnalysis->Name = L"checkSpectralAnalysis";
			this->checkSpectralAnalysis->Size = System::Drawing::Size(106, 17);
			this->checkSpectralAnalysis->TabIndex = 4;
			this->checkSpectralAnalysis->Text = L"Spectral Analysis";
			this->checkSpectralAnalysis->UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(7, 25);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(132, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Frequency Resolution (Hz)";
			// 
			// label1
			// 
			this->label1->Location = System::Drawing::Point(0, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(100, 23);
			this->label1->TabIndex = 5;
			// 
			// udFrequencyResolution
			// 
			this->udFrequencyResolution->Location = System::Drawing::Point(152, 18);
			this->udFrequencyResolution->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 999999, 0, 0, 0 });
			this->udFrequencyResolution->Name = L"udFrequencyResolution";
			this->udFrequencyResolution->Size = System::Drawing::Size(120, 20);
			this->udFrequencyResolution->TabIndex = 1;
			// 
			// SpectralAnalysisControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->checkPulseCompression);
			this->Name = L"SpectralAnalysisControl";
			this->Size = System::Drawing::Size(288, 88);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->udFrequencyResolution))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

#pragma region Implémentation de IParamControl
	public: virtual void UpdateConfiguration();
	public: virtual void UpdateGUI();
#pragma endregion

	};
}
