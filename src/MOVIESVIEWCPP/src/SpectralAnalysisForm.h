#pragma once

#include "ParameterForm.h"
#include "SpectralAnalysisControl.h"

namespace MOVIESVIEWCPP
{
	public ref class SpectralAnalysisForm : public ParameterForm
	{
	public:
		SpectralAnalysisForm() : ParameterForm()
		{
			SetParamControl(gcnew SpectralAnalysisControl(), L"Spectral Analysis");
		}
	};
};
