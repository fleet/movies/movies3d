#include "SpectrogramControl.h"
#include "DisplayParameter.h"
#include "ColorPaletteEcho.h"

#include <algorithm>

using namespace MOVIESVIEWCPP;
using namespace System::Windows::Forms::DataVisualization::Charting;

const double WHEEL_ZOOM_FACTOR = 1.5;

void SpectrogramControl::InitializeSpectrogram()
{
	auto chartArea = chart1->ChartAreas[0];
	auto axisX = chartArea->AxisX;
	auto axisY = chartArea->AxisY;

	axisX->MajorGrid->Enabled = true;
	axisX->MajorGrid->LineDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dot;
	axisY->MajorGrid->Enabled = true;
	axisY->MajorGrid->LineDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dot;

	axisX->IsMarginVisible = false;
	axisY->IsMarginVisible = false;
	
	setAxisX_Display("Frequencies (kHz)", "F2", false);
	setAxisY_Display("Range (m)", "D", true);
	setMinimumZoomedAxisLengths(10, 5);
}

void SpectrogramControl::setAxisXDataRange(double minValue, double range)
{
	m_minDataValueX = minValue;
	m_rangeDataX = range;
}

void SpectrogramControl::setAxisYDataRange(double minValue, double range)
{
	m_minDataValueY = minValue;
	m_rangeDataY = range;
}

void SpectrogramControl::setAxisXDisplayRange(double minAxisValue, double maxAxisValue)
{
	chart1->ChartAreas[0]->AxisX->Minimum = minAxisValue;
	chart1->ChartAreas[0]->AxisX->Maximum = maxAxisValue;

	// Calcul d'un interval offset coh�rent pour avoir un axes de fr�quence avec des valeurs "rondes"
	int p = floor(log10(minAxisValue));
	int n = floor(minAxisValue / pow(10, p));
	double offset = pow(10, p) * n - minAxisValue;
	chart1->ChartAreas[0]->AxisX->IntervalOffset = offset;

	resetZoom();
}

void SpectrogramControl::setAxisYDisplayRange(double minAxisValue, double maxAxisValue)
{
	chart1->ChartAreas[0]->AxisY->Minimum = minAxisValue;
	chart1->ChartAreas[0]->AxisY->Maximum = maxAxisValue;
	chart1->ChartAreas[0]->AxisY->Crossing = maxAxisValue;
	
	// on r�cup�re l'espacement souhait� entre les lignes de profondeur
	double depthInterLines = DisplayParameter::getInstance()->GetDepthSampling();
	chart1->ChartAreas[0]->AxisY->Interval = depthInterLines;

	resetZoom();
}

void SpectrogramControl::setAxisX_Display(String^ title, String^ labelFormat, bool reversed)
{
	//chart1->ChartAreas[0]->AxisX->Title = title;
	chart1->ChartAreas[0]->AxisX->IsReversed = reversed;
	chart1->ChartAreas[0]->AxisX->LabelStyle->Format = labelFormat;
}

void SpectrogramControl::setAxisY_Display(String^ title, String^ labelFormat, bool reversed)
{
	//chart1->ChartAreas[0]->AxisY->Title = title;
	chart1->ChartAreas[0]->AxisY->IsReversed = reversed;
	chart1->ChartAreas[0]->AxisY->LabelStyle->Format = labelFormat;
}

void SpectrogramControl::setData(const std::vector<std::vector<double>>& colorData, double multiplier)
{
	delete m_data;
	m_data = new std::vector<std::vector<double>>(colorData);
	m_multiplier = multiplier;
	m_dataChanged = true;
	chart1->Invalidate();
}

void SpectrogramControl::setDepthOffset(double depthOffset)
{
	m_depthOffset = depthOffset;
	m_dataChanged = true;
	chart1->Invalidate();
}

void SpectrogramControl::clear()
{
	setData(std::vector<std::vector<double>>(), m_multiplier);
}

void SpectrogramControl::chart1_PrePaint(System::Object^  sender, System::Windows::Forms::DataVisualization::Charting::ChartPaintEventArgs^  e)
{
	if (e->ChartElement->GetType() == ChartArea::typeid)
	{
		e->Position->Auto = false;
		e->Position->Y = 0;
		e->Position->Height = 100;
	}	
	const double offset = m_depthOffset;
	
	if(m_data)
	{
		auto area = e->Chart->ChartAreas[0];
		double zoomedMinX = area->AxisX->ScaleView->ViewMinimum;
		double zoomedMaxX = area->AxisX->ScaleView->ViewMaximum;
		double zoomedMinY = area->AxisY->ScaleView->ViewMinimum;
		double zoomedMaxY = area->AxisY->ScaleView->ViewMaximum;

		if (m_dataChanged || !m_image || m_image->Size != chart1->Size || imageAreaMinX != zoomedMinX || imageAreaMaxX != zoomedMaxX || imageAreaMinY != zoomedMinY || imageAreaMaxY != zoomedMaxY)
		{
			m_dataChanged = false;

			imageAreaMinX = zoomedMinX;
			imageAreaMaxX = zoomedMaxX;
			imageAreaMinY = zoomedMinY;
			imageAreaMaxY = zoomedMaxY;

			delete m_image;
			m_image = gcnew Bitmap(chart1->Size.Width, chart1->Size.Height);

			auto graphics = Graphics::FromImage(m_image);
			//graphics->FillRectangle(gcnew SolidBrush(Color::Black), 0, 0, chart1->Size.Width, chart1->Size.Height);

			auto brush = gcnew SolidBrush(Color::White);
			auto palette = DisplayParameter::getInstance()->GetCurrent2DEchoPalette();

			double xPosAxisMin = e->ChartGraphics->GetPositionFromAxis(area->Name, AxisName::X, zoomedMinX);
			double yPosAxisMin = e->ChartGraphics->GetPositionFromAxis(area->Name, AxisName::Y, zoomedMinY);

			double borderValue = 0.1;
			int yIndex = 0;
			for (auto xValues : *m_data)
			{
				int xIndex = 0;
				for (auto v : xValues)
				{
					if (!isnan(v))
					{
						v *= m_multiplier;

						double x = m_minDataValueX + xIndex * m_rangeDataX;
						double y = m_minDataValueY + yIndex * m_rangeDataY  + offset;
						
						if (x >= zoomedMinX && x <= zoomedMaxX && y >= zoomedMinY && y <= zoomedMaxY)
						{
							double xPosMin = e->ChartGraphics->GetPositionFromAxis(area->Name, AxisName::X, x - m_rangeDataX / 2.0);
							double xPosMax = e->ChartGraphics->GetPositionFromAxis(area->Name, AxisName::X, x + m_rangeDataX / 2.0);
							if (xPosMin > xPosMax)
							{
								auto temp = xPosMax;
								xPosMax = xPosMin;
								xPosMin = temp;
							}

							double yPosMin = e->ChartGraphics->GetPositionFromAxis(area->Name, AxisName::Y, y + m_rangeDataY / 2.0);
							double yPosMax = e->ChartGraphics->GetPositionFromAxis(area->Name, AxisName::Y, y - m_rangeDataY / 2.0);
							if (yPosMin > yPosMax)
							{
								auto temp = yPosMax;
								yPosMax = yPosMin;
								yPosMin = temp;
							}

							auto width = (xPosMax - xPosMin) + 2.0 * borderValue;
							auto height = (yPosMax - yPosMin) + 2.0 * borderValue;

							auto rectXPos = std::max(xPosMin - borderValue, xPosAxisMin);
							auto rectYPos = std::max(yPosMin - borderValue, yPosAxisMin);

							RectangleF rect(rectXPos, rectYPos, width, height);
							rect = e->ChartGraphics->GetAbsoluteRectangle(rect);

							if (rect.Width != 0 && rect.Height != 0)
							{
								if (DisplayParameter::getInstance()->GetMinThreshold() <= v && v <= DisplayParameter::getInstance()->GetMaxThreshold())
								{
									brush->Color = Color::FromArgb(palette->GetColor(v) | 0xFF000000);
								}
								else
								{
									brush->Color = Color::White;
								}

								graphics->FillRectangle(brush, rect);
							}
						}
					}

					++xIndex;
				}
				++yIndex;
			}

		}

		if (e->ChartElement->GetType() == ChartArea::typeid)
		{
			e->ChartGraphics->Graphics->DrawImage(m_image, 0, 0);
		}
	}
}

void SpectrogramControl::OnMouseEnter(System::Object ^sender, System::EventArgs ^e)
{
	//Put the focus on the picture box to allow wheel events to reach it
	if (!chart1->Focused && Form::ActiveForm == static_cast<Form^>(TopLevelControl))
		chart1->Focus();
}

void SpectrogramControl::OnMouseWheel(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e)
{
	if (e->Delta > 0)
	{
		double xMin = chart1->ChartAreas[0]->AxisX->ScaleView->ViewMinimum;
		double xMax = chart1->ChartAreas[0]->AxisX->ScaleView->ViewMaximum;
		auto xLength = (xMax - xMin);

		double yMin = chart1->ChartAreas[0]->AxisY->ScaleView->ViewMinimum;
		double yMax = chart1->ChartAreas[0]->AxisY->ScaleView->ViewMaximum;
		auto yLength = (yMax - yMin);

		if (xLength > m_minZoomedLengthX && yLength > m_minZoomedLengthY)
		{
			//New length of each axis after the zoom
			auto newXLength = xLength / WHEEL_ZOOM_FACTOR;
			auto newYLength = yLength / WHEEL_ZOOM_FACTOR;

			//Position of the mouse cursor
			auto vx = chart1->ChartAreas[0]->AxisX->PixelPositionToValue(e->Location.X);
			auto vy = chart1->ChartAreas[0]->AxisY->PixelPositionToValue(e->Location.Y);

			//Position of the mouse cursor in percentage from the start of the axis
			auto vxPercent = (vx - xMin) / xLength;
			auto vyPercent = (vy - yMin) / yLength;

			//New start of each axis to keep the value under the mouse cursor at the same location
			double posXStart = vx - newXLength * vxPercent;
			double posYStart = vy - newYLength * vyPercent;

			//Make the zoom
			chart1->ChartAreas[0]->AxisX->ScaleView->Zoom(posXStart, newXLength, DateTimeIntervalType::Auto, true);
			chart1->ChartAreas[0]->AxisY->ScaleView->Zoom(posYStart, newYLength, DateTimeIntervalType::Auto, true);	
		}
	}
	else
	{
		//unzoom
		chart1->ChartAreas[0]->AxisX->ScaleView->ZoomReset(1);
		chart1->ChartAreas[0]->AxisY->ScaleView->ZoomReset(1);	
	}
}

void SpectrogramControl::OnMouseClick(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e)
{
	if (e->Button == System::Windows::Forms::MouseButtons::Right)
	{
		chart1->ChartAreas[0]->AxisX->ScaleView->ZoomReset();
		chart1->ChartAreas[0]->AxisY->ScaleView->ZoomReset();		
	}
}

void SpectrogramControl::resetZoom()
{
	while (chart1->ChartAreas[0]->AxisX->ScaleView->IsZoomed && chart1->ChartAreas[0]->AxisY->ScaleView->IsZoomed)
	{
		chart1->ChartAreas[0]->AxisX->ScaleView->ZoomReset(9999);
		chart1->ChartAreas[0]->AxisY->ScaleView->ZoomReset(9999);		
	}
}

void SpectrogramControl::OnMouseDown(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e)
{
	if (m_boxZoomMode && e->Button == System::Windows::Forms::MouseButtons::Left)
	{
		//assert(!m_isDrawingZoomBox);

		double xMin = chart1->ChartAreas[0]->AxisX->ScaleView->ViewMinimum;
		double xMax = chart1->ChartAreas[0]->AxisX->ScaleView->ViewMaximum;
		double yMin = chart1->ChartAreas[0]->AxisY->ScaleView->ViewMinimum;
		double yMax = chart1->ChartAreas[0]->AxisY->ScaleView->ViewMaximum;

		m_zoomboxOrigin = Point(e->X, e->Y);
		auto clickVx = chart1->ChartAreas[0]->AxisX->PixelPositionToValue(e->X);
		auto clickVy = chart1->ChartAreas[0]->AxisY->PixelPositionToValue(e->Y);

		//Ensure the click occurs in the chart zone
		if (xMin <= clickVx && clickVx <= xMax && yMin <= clickVy && clickVy <= yMax)
		{
			m_isDrawingZoomBox = true;
			m_zoomBox = Rectangle(e->X, e->Y, 0, 0);
		}
	}
}

void SpectrogramControl::OnMouseMove(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e)
{
	if ((m_boxZoomMode && m_isDrawingZoomBox && e->Button == System::Windows::Forms::MouseButtons::Left))
	{
		auto w = e->X - m_zoomboxOrigin.X;
		auto h = e->Y - m_zoomboxOrigin.Y;
		m_zoomBox.Width = abs(w);
		m_zoomBox.Height = abs(h);
		m_zoomBox.X = std::min(m_zoomboxOrigin.X, m_zoomboxOrigin.X + w);
		m_zoomBox.Y = std::min(m_zoomboxOrigin.Y, m_zoomboxOrigin.Y + h);
		
		chart1->Invalidate();
	}
}

void SpectrogramControl::OnMouseUp(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e)
{
	if (m_boxZoomMode && m_isDrawingZoomBox && m_zoomBox.Width > 0 && m_zoomBox.Height > 0 && e->Button == System::Windows::Forms::MouseButtons::Left)
	{
		m_isDrawingZoomBox = false;
		chart1->Invalidate();

		double xMin = chart1->ChartAreas[0]->AxisX->ScaleView->ViewMinimum;
		double xMax = chart1->ChartAreas[0]->AxisX->ScaleView->ViewMaximum;
		double yMin = chart1->ChartAreas[0]->AxisY->ScaleView->ViewMinimum;
		double yMax = chart1->ChartAreas[0]->AxisY->ScaleView->ViewMaximum;

		double minZoomX = std::max(xMin, chart1->ChartAreas[0]->AxisX->PixelPositionToValue(std::max(m_zoomBox.Left, 0)));
		double maxZoomX = std::min(xMax, chart1->ChartAreas[0]->AxisX->PixelPositionToValue(std::min(m_zoomBox.Right, chart1->Width - 2)));
		double minZoomY = std::max(yMin, chart1->ChartAreas[0]->AxisY->PixelPositionToValue(std::max(m_zoomBox.Top, 0)));
		double maxZoomY = std::min(yMax, chart1->ChartAreas[0]->AxisY->PixelPositionToValue(std::min(m_zoomBox.Bottom, chart1->Height - 2)));

		chart1->ChartAreas[0]->AxisX->ScaleView->Zoom(minZoomX, maxZoomX - minZoomX, DateTimeIntervalType::Auto, true);
		chart1->ChartAreas[0]->AxisY->ScaleView->Zoom(minZoomY, maxZoomY - minZoomY, DateTimeIntervalType::Auto, true);
	}
}

void MOVIESVIEWCPP::SpectrogramControl::OnPaint(System::Object ^sender, System::Windows::Forms::PaintEventArgs ^e)
{
	if (m_boxZoomMode && m_isDrawingZoomBox)
	{
		auto pen = gcnew Pen(Color::Black);
		pen->Width = 1;
		e->Graphics->DrawRectangle(pen, m_zoomBox);
	}
}
