#pragma once

#include <vector>

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Summary for SpectrogramControl
	/// </summary>
	public ref class SpectrogramControl : public System::Windows::Forms::UserControl
	{
	public:
		SpectrogramControl(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

			InitializeSpectrogram();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~SpectrogramControl()
		{
			delete m_data;
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chart1;

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Series^  series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::DataPoint^  dataPoint1 = (gcnew System::Windows::Forms::DataVisualization::Charting::DataPoint(0,
				0));
			this->chart1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->BeginInit();
			this->SuspendLayout();
			// 
			// chart1
			// 
			chartArea1->AxisX->IsStartedFromZero = false;
			chartArea1->AxisX->MajorGrid->Enabled = false;
			chartArea1->AxisX->ScrollBar->ButtonStyle = System::Windows::Forms::DataVisualization::Charting::ScrollBarButtonStyles::SmallScroll;
			chartArea1->AxisX->ScrollBar->IsPositionedInside = false;
			chartArea1->AxisY->IsStartedFromZero = false;
			chartArea1->AxisY->MajorGrid->Enabled = false;
			chartArea1->AxisY->ScrollBar->ButtonStyle = System::Windows::Forms::DataVisualization::Charting::ScrollBarButtonStyles::SmallScroll;
			chartArea1->AxisY->ScrollBar->IsPositionedInside = false;
			chartArea1->Name = L"ChartArea1";
			chartArea1->Position->Auto = false;
			chartArea1->Position->Height = 100;
			chartArea1->Position->Width = 100;
			this->chart1->ChartAreas->Add(chartArea1);
			this->chart1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->chart1->Location = System::Drawing::Point(0, 0);
			this->chart1->Name = L"chart1";
			series1->ChartArea = L"ChartArea1";
			series1->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Point;
			series1->Name = L"Series1";
			dataPoint1->IsEmpty = true;
			series1->Points->Add(dataPoint1);
			this->chart1->Series->Add(series1);
			this->chart1->Size = System::Drawing::Size(805, 389);
			this->chart1->TabIndex = 1;
			this->chart1->Text = L"chart1";
			this->chart1->PrePaint += gcnew System::EventHandler<System::Windows::Forms::DataVisualization::Charting::ChartPaintEventArgs^ >(this, &SpectrogramControl::chart1_PrePaint);
			this->chart1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SpectrogramControl::OnMouseClick);
			this->chart1->MouseEnter += gcnew System::EventHandler(this, &SpectrogramControl::OnMouseEnter);
			this->chart1->MouseWheel += gcnew System::Windows::Forms::MouseEventHandler(this, &SpectrogramControl::OnMouseWheel);
			this->chart1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MOVIESVIEWCPP::SpectrogramControl::OnMouseDown);
			this->chart1->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MOVIESVIEWCPP::SpectrogramControl::OnMouseUp);
			this->chart1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MOVIESVIEWCPP::SpectrogramControl::OnMouseMove);
			this->chart1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MOVIESVIEWCPP::SpectrogramControl::OnPaint);
			// 
			// SpectrogramControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->chart1);
			this->Name = L"SpectrogramControl";
			this->Size = System::Drawing::Size(805, 389);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->chart1))->EndInit();
		
			this->ResumeLayout(false);

		}
#pragma endregion

	public:

		void setAxisXDataRange(double minDataValue, double range);
		void setAxisYDataRange(double minDataValue, double range);
		
		void setAxisXDisplayRange(double minAxisValue, double maxAxisValue);
		void setAxisYDisplayRange(double minAxisValue, double maxAxisValue);

		void setAxisX_Display(String^ title, String^ labelFormat, bool reversed);
		void setAxisY_Display(String^ title, String^ labelFormat, bool reversed);

		///colorData is a table of y * x values, i.e. v(x, y) = colorData[y][x]
		void setData(const std::vector<std::vector<double>>& colorData, double multiplier);
		void setDepthOffset(double depthOffset);

		void clear();

		void setMinimumZoomedAxisLengths(double minXLength, double minYLength) { m_minZoomedLengthX = minXLength; m_minZoomedLengthY = minYLength; }
		void resetZoom();

		///For additional chart customisations if needed
		System::Windows::Forms::DataVisualization::Charting::ChartArea^ getChartArea() { return chart1->ChartAreas[0]; }

		void setBoxZoomModeEnabled(bool value) { m_boxZoomMode = value; };

	private:
		void InitializeSpectrogram();

		Image^ m_image = nullptr;
		bool m_dataChanged = false;
		double imageAreaMinX = 0;
		double imageAreaMaxX = 0;
		double imageAreaMinY = 0;
		double imageAreaMaxY = 0;

		double m_multiplier = 1.0;

		double m_minDataValueX = 0;
		double m_rangeDataX = 5;

		double m_minDataValueY = 0;
		double m_rangeDataY = 5;

		double m_minZoomedLengthX = 1;
		double m_minZoomedLengthY = 1;

		bool m_boxZoomMode = false;
		bool m_isDrawingZoomBox = false;
		System::Drawing::Rectangle m_zoomBox;
		System::Drawing::Point m_zoomboxOrigin;

		std::vector<std::vector<double>>* m_data = nullptr;
		double m_depthOffset = 0.0;

		void chart1_PrePaint(System::Object^  sender, System::Windows::Forms::DataVisualization::Charting::ChartPaintEventArgs^  e);
		void OnMouseEnter(System::Object ^sender, System::EventArgs ^e);
		void OnMouseWheel(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e);
		void OnMouseClick(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e);
		void OnMouseDown(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e);
		void OnMouseUp(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e);					
		void OnMouseMove(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e);
		void OnPaint(System::Object ^sender, System::Windows::Forms::PaintEventArgs ^e);
};
}


