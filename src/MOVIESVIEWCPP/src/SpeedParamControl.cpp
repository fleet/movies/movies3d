#include "SpeedParamControl.h"

#include "M3DKernel/M3DKernel.h"

using namespace MOVIESVIEWCPP;

void SpeedParamControl::UpdateConfiguration()
{
	KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();

	SpeedType speedType;
	if (checkBoxFixedSpeed->Checked)
	{
		speedType = SPUSER;
	}
	else
	{
		speedType = SPTW;
	}
	param.setSpeedUsed(speedType);

	param.setUserSpeedValue(Convert::ToSingle(textBoxSpeed->Text));

	M3DKernel::GetInstance()->UpdateKernelParameter(param);
}

void SpeedParamControl::UpdateGUI()
{
	KernelParameter &param = M3DKernel::GetInstance()->GetRefKernelParameter();

	checkBoxFixedSpeed->Checked = param.getSpeedUsed() == SPUSER;
	textBoxSpeed->Text = Convert::ToString(param.getUserSpeedValue());

	UpdateComponents();
}

System::Void SpeedParamControl::UpdateComponents()
{
	textBoxSpeed->Enabled = checkBoxFixedSpeed->Checked;
}

System::Void SpeedParamControl::checkBoxFixedSpeed_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	UpdateComponents();
}

System::Void SpeedParamControl::textBoxSpeed_LostFocus(System::Object^  sender, System::EventArgs^  e)
{
	try
	{
		Convert::ToSingle(textBoxSpeed->Text);
	}
	catch (...)
	{
		MessageBox::Show("Incorrect speed value !");
		textBoxSpeed->Text = Convert::ToString(M3DKernel::GetInstance()->GetRefKernelParameter().getUserSpeedValue());
	}
}