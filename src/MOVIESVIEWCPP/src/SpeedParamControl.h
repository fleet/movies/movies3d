#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "IParamControl.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de SpeedParamControl
	/// </summary>
	public ref class SpeedParamControl : public System::Windows::Forms::UserControl, public IParamControl
	{
	public:
		SpeedParamControl(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~SpeedParamControl()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::CheckBox^  checkBoxFixedSpeed;
	protected:




	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBoxSpeed;



	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxFixedSpeed = (gcnew System::Windows::Forms::CheckBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBoxSpeed = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->checkBoxFixedSpeed);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->textBoxSpeed);
			this->groupBox1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBox1->Location = System::Drawing::Point(0, 0);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(233, 63);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Enable Fixed Speed";
			// 
			// checkBoxFixedSpeed
			// 
			this->checkBoxFixedSpeed->AutoSize = true;
			this->checkBoxFixedSpeed->Location = System::Drawing::Point(117, 1);
			this->checkBoxFixedSpeed->Name = L"checkBoxFixedSpeed";
			this->checkBoxFixedSpeed->Size = System::Drawing::Size(15, 14);
			this->checkBoxFixedSpeed->TabIndex = 6;
			this->checkBoxFixedSpeed->UseVisualStyleBackColor = true;
			this->checkBoxFixedSpeed->CheckedChanged += gcnew System::EventHandler(this, &SpeedParamControl::checkBoxFixedSpeed_CheckedChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 27);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(71, 13);
			this->label1->TabIndex = 4;
			this->label1->Text = L"Speed (m/s) :";
			// 
			// textBoxSpeed
			// 
			this->textBoxSpeed->Location = System::Drawing::Point(85, 24);
			this->textBoxSpeed->Name = L"textBoxSpeed";
			this->textBoxSpeed->Size = System::Drawing::Size(128, 20);
			this->textBoxSpeed->TabIndex = 2;
			this->textBoxSpeed->LostFocus += gcnew System::EventHandler(this, &SpeedParamControl::textBoxSpeed_LostFocus);
			// 
			// SpeedParamControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->groupBox1);
			this->Name = L"SpeedParamControl";
			this->Size = System::Drawing::Size(233, 63);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

#pragma region Impl�mentation de IParamControl
	public: virtual void UpdateConfiguration();
	public: virtual void UpdateGUI();
#pragma endregion

	public: System::Void UpdateComponents();

	private: System::Void checkBoxFixedSpeed_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void textBoxSpeed_LostFocus(System::Object^  sender, System::EventArgs^  e);
	};
}
