// -*- MC++ -*-
// ****************************************************************************
// Class: SpeedParamForm
//
// Description: Fen�tre de configuration de la vitesse
//
// Projet: MOVIES3D
// Auteur: O.Tonck
// Date  : Mai 2010
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "ParameterForm.h"
#include "SpeedParamControl.h"

namespace MOVIESVIEWCPP {
	public ref class SpeedParamForm : public ParameterForm
	{
	public:
		SpeedParamForm(void) :
			ParameterForm()
		{
			SetParamControl(gcnew SpeedParamControl(), L"Speed Parameters");
		}
	};
};