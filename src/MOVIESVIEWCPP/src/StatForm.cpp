#include "StatForm.h"
using namespace MOVIESVIEWCPP;
#include "ModuleManager/ModuleManager.h"
#include "M3DKernel/module/ProcessModule.h"
#include "ObjectDisplay.h"
#include "M3DKernel/M3DKernel.h"

System::Void StatForm::UpdateStat()
{
	listViewResult->BeginUpdate();
	CObjectDisplay ^refDisplay = gcnew CObjectDisplay(listViewResult);
	this->listViewResult->Items->Clear();
	refDisplay->m_defaultColor = System::Drawing::Color::Red;
	for (size_t i = 0; i < M3DKernel::GetInstance()->m_timeCounterRecord.GetObjectCount(); i++)
	{
		TimeCountStat timeC;
		char l_Name[255];
		if (M3DKernel::GetInstance()->m_timeCounterRecord.GetObjectDescWithIndex(i, l_Name, 254, timeC))
		{
			System::String ^Name = gcnew System::String(l_Name);
			array<System::String^>^myIntArray = { System::Int32(timeC.m_Time).ToString(), System::Int32(timeC.m_Min).ToString(),System::Int32(timeC.m_Max).ToString() };
			refDisplay->AddRangeString(myIntArray, Name);
			//	refDisplay->AddInt32(timeC.m_Time,Name);
		}
	}
	listViewResult->EndUpdate();
}

System::Void StatForm::ClearStat()
{
	M3DKernel::GetInstance()->m_timeCounterRecord.ClearAll();
}