#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de StatForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class StatForm : public System::Windows::Forms::Form
	{
	public:
		StatForm(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~StatForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ListView^  listViewResult;
	protected:

	protected:
	private: System::Windows::Forms::ColumnHeader^  columnName;
	private: System::Windows::Forms::ColumnHeader^  columnTime;
	private: System::Windows::Forms::ColumnHeader^  columnMin;
	private: System::Windows::Forms::ColumnHeader^  columnMax;
	private: System::Windows::Forms::Button^  buttonClear;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;




	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->listViewResult = (gcnew System::Windows::Forms::ListView());
			this->columnName = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnTime = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnMin = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnMax = (gcnew System::Windows::Forms::ColumnHeader());
			this->buttonClear = (gcnew System::Windows::Forms::Button());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->tableLayoutPanel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// listViewResult
			// 
			this->listViewResult->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(4) {
				this->columnName,
					this->columnTime, this->columnMin, this->columnMax
			});
			this->listViewResult->Dock = System::Windows::Forms::DockStyle::Fill;
			this->listViewResult->Location = System::Drawing::Point(3, 3);
			this->listViewResult->Name = L"listViewResult";
			this->listViewResult->Size = System::Drawing::Size(326, 300);
			this->listViewResult->TabIndex = 0;
			this->listViewResult->UseCompatibleStateImageBehavior = false;
			this->listViewResult->View = System::Windows::Forms::View::Details;
			// 
			// columnName
			// 
			this->columnName->Text = L"Module Name";
			this->columnName->Width = 202;
			// 
			// columnTime
			// 
			this->columnTime->Text = L"time";
			this->columnTime->Width = 40;
			// 
			// columnMin
			// 
			this->columnMin->Text = L"Min";
			this->columnMin->Width = 38;
			// 
			// columnMax
			// 
			this->columnMax->Text = L"Max";
			this->columnMax->Width = 42;
			// 
			// buttonClear
			// 
			this->buttonClear->Dock = System::Windows::Forms::DockStyle::Top;
			this->buttonClear->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->buttonClear->Location = System::Drawing::Point(3, 309);
			this->buttonClear->Name = L"buttonClear";
			this->buttonClear->Size = System::Drawing::Size(326, 22);
			this->buttonClear->TabIndex = 1;
			this->buttonClear->Text = L"clear";
			this->buttonClear->UseVisualStyleBackColor = true;
			this->buttonClear->Click += gcnew System::EventHandler(this, &StatForm::buttonClear_Click);
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 1;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->buttonClear, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->listViewResult, 0, 0);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 2;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 28)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 20)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(332, 334);
			this->tableLayoutPanel1->TabIndex = 2;
			// 
			// StatForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(332, 334);
			this->ControlBox = false;
			this->Controls->Add(this->tableLayoutPanel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->Name = L"StatForm";
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->Text = L"Module Statistics";
			this->TopMost = true;
			this->Load += gcnew System::EventHandler(this, &StatForm::StatForm_Load);
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &StatForm::StatForm_FormClosing);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->ResumeLayout(false);

		}
	public: System::Void UpdateStat();
#pragma endregion
	private: System::Void StatForm_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void StatForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
		e->Cancel = true;
		this->Hide();
	}
	private: System::Void buttonClear_Click(System::Object^  sender, System::EventArgs^  e) {
		ClearStat();
	}
	private: System::Void StatForm::ClearStat();

	};
}
