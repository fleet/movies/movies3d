#include "TramaDescriptorForm.h"

// D�pendances
#define WIN32_LEAN_AND_MEAN // Pour ne pas avoir le confit entre Xerces et les ent�tes standard  (a cause de windows.h)
#define NOMINMAX

// STD
#include <string>
#include <windows.h>


using namespace MOVIESVIEWCPP;


// mise � jour des param�tres IHM en fonction du module 
void TramaDescriptorForm::ConfigToIHM()
{
	// construction de l'arbre des trames et cochage des checkbox
	// en fonction de la config
	if (!m_pTramaDescriptor->m_Configured)
	{
		m_pTramaDescriptor->ParseDictionnary();
	}

	UpdateTree();
}

// mise � jour de l'arbre en fonction de la configuration
void TramaDescriptorForm::UpdateTree()
{
	treeViewSelectedTramas->Nodes->Clear();

	for (size_t i = 0; i < m_pTramaDescriptor->m_xmlNodesArray.size(); i++)
	{
		System::String^ nodeName = gcnew System::String(m_pTramaDescriptor->m_xmlNodesArray[i].xmlNodeName.c_str());
		int newLevel = GetLevelAndShortName(nodeName);
		// on ajoute la description si il y en a une
		System::String ^desc = gcnew System::String(m_pTramaDescriptor->m_xmlNodesArray[i].xmlNodeDesc.c_str());
		if (!desc->Equals(""))
		{
			nodeName += " (";
			nodeName += desc;
			nodeName += ")";
		}

		TreeNode ^node;
		// 1er cas : on ajoute un noeud racine
		if (newLevel == 0)
		{
			node = treeViewSelectedTramas->Nodes->Add(nodeName);
		}
		// 2eme cas : on ajoute un fils de niveau 1
		else if (newLevel == 1)
		{
			node = treeViewSelectedTramas->Nodes[treeViewSelectedTramas->Nodes->Count - 1]->Nodes->Add(nodeName);
		}
		// 3eme cas : on ajoute un fils de niveau 2
		else if (newLevel == 2)
		{
			node = treeViewSelectedTramas->Nodes[treeViewSelectedTramas->Nodes->Count - 1]
				->Nodes[treeViewSelectedTramas->Nodes[treeViewSelectedTramas->Nodes->Count - 1]->Nodes->Count - 1]->Nodes->Add(nodeName);
		}
		node->Checked = m_pTramaDescriptor->m_xmlNodesArray[i].needed || m_pTramaDescriptor->m_xmlNodesArray[i].wanted;
		if (m_pTramaDescriptor->m_xmlNodesArray[i].needed)
		{
			node->ForeColor = Color::Blue;
		}

	}
	treeViewSelectedTramas->ExpandAll();
}

// renvoie le niveau du noeud et son nom court
int TramaDescriptorForm::GetLevelAndShortName(System::String^ %longNodeName)
{
	int level = 0;
	int slashPos = longNodeName->IndexOf("\\");
	while (slashPos > -1)
	{
		level++;
		longNodeName = longNodeName->Substring(slashPos + 1);
		slashPos = longNodeName->IndexOf("\\");
	}

	return level;
}

// mise � jour des param�tres du module en fonction des param�tres IHM
void TramaDescriptorForm::IHMToConfig()
{
	// Pour tous les noeuds racine (normalement un seul)
	for (int i = 0; i < treeViewSelectedTramas->GetNodeCount(false); i++)
	{
		System::String^ stringID = "";
		UpdateConfigFromNode(treeViewSelectedTramas->Nodes[i], "");
	}

	// on indique que les trames ont �t� configur�es
	m_pTramaDescriptor->m_Configured = true;
}

// mise � jour de l'�tat d'un noeud en fonction de la checkbox associ�e
void TramaDescriptorForm::UpdateConfigFromNode(System::Windows::Forms::TreeNode^ node, System::String^ stringID)
{
	char* str = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(stringID + node->Text).ToPointer();
	std::string ret(str);
	System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(str));

	// on enl�ve l'�ventuelle description entre parenth�ses
	int index = ret.find(" (", 0);
	if (index != -1)
	{
		ret = ret.substr(0, index);
	}

	m_pTramaDescriptor->SetWanted(ret, node->Checked);

	// mise � jour des fils
	for (int i = 0; i < node->GetNodeCount(false); i++)
	{
		UpdateConfigFromNode(node->Nodes[i], stringID + node->Text + "\\");
	}
}


// met � jour les checkboxes de l'arbre apr�s click sur l'une d'entre elles
void TramaDescriptorForm::UpdateCheckBoxes(System::Windows::Forms::TreeViewEventArgs^  e)
{
	// R�cup�ration du noeud modifi�
	System::Windows::Forms::TreeNode ^ node = e->Node;

	// on recoche si on d�coche un noeud exig�
	if (m_pTramaDescriptor->IsNeeded(GetXmlNodeName(node)))
	{
		if (!node->Checked)
		{
			node->Checked = true;
		}
	}
	else
	{
		// si un item est coch� / d�coch�, on coche / d�coche tous les fils
		SetCheckAll(node, node->Checked);
	}
}


// Coche ou d�coche r�cursivement tous les noeuds fils d'un TreeNode
void TramaDescriptorForm::SetCheckAll(System::Windows::Forms::TreeNode ^ node, bool checked)
{
	for (int i = 0; i < node->GetNodeCount(false); i++)
	{
		if (m_pTramaDescriptor->IsNeeded(GetXmlNodeName(node->Nodes[i])))
		{
			node->Nodes[i]->Checked = true;
		}
		else
		{
			node->Nodes[i]->Checked = checked;
		}
		SetCheckAll(node->Nodes[i], checked);
	}
}

// renvoie le nom complet du noeud XML
std::string TramaDescriptorForm::GetXmlNodeName(System::Windows::Forms::TreeNode ^node)
{
	std::string result = "";

	for (int i = 0; i <= node->Level; i++)
	{
		System::Windows::Forms::TreeNode ^parentNode = node;
		for (int j = 0; j < node->Level - i; j++)
		{
			parentNode = parentNode->Parent;
		}
		char* str = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(parentNode->Text).ToPointer();
		std::string ret(str);
		System::Runtime::InteropServices::Marshal::FreeHGlobal(IntPtr(str));
		if (i != 0)
		{
			result.append("\\");
		}
		result.append(ret);
	}

	// on enl�ve l'�ventuelle description entre parenth�ses
	int index = result.find(" (", 0);
	if (index != -1)
	{
		result = result.substr(0, index);
	}

	return result;
}
