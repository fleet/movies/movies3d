#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

// D�pendances
#include "M3DKernel/parameter/TramaDescriptor.h"


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de TramaDescriptorForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class TramaDescriptorForm : public System::Windows::Forms::Form
	{
	public:
		TramaDescriptorForm(BaseKernel::TramaDescriptor * cfg)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
			m_pTramaDescriptor = cfg;
			ConfigToIHM();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~TramaDescriptorForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TreeView^  treeViewSelectedTramas;
	private: System::Windows::Forms::Button^  buttonCancel;
	private: System::Windows::Forms::Button^  ButtonOK;
	protected:

	protected:

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

		// mise � jour du module d'echoIntegration � partir des param�tres IHM
		void IHMToConfig();

		// ajoute les trames voulues dans la configuration � partir du noeud source
		void UpdateConfigFromNode(System::Windows::Forms::TreeNode ^node, System::String^ stringID);

		// mise � jour des param�tres IHM en fonction du module d'�choIntegration
		void ConfigToIHM();

		// construit le treeView � partir des noeuds XMLs du tramaDescriptor assci�
		void UpdateTree();

		// renvoie le niveau du noeud et son nom court
		int GetLevelAndShortName(System::String^ %longNodeName);

		// renvoie le nom complet du noeud XML
		std::string GetXmlNodeName(System::Windows::Forms::TreeNode ^node);

		// met � jour les checkboxes de l'arbre apr�s click sur l'une d'entre elles
		void UpdateCheckBoxes(System::Windows::Forms::TreeViewEventArgs^  e);

		// permet de cocher ou decocher tous les enfants d'un noeud de l'arbre
		void SetCheckAll(System::Windows::Forms::TreeNode ^node, bool checked);

		// Donn�es
		BaseKernel::TramaDescriptor * m_pTramaDescriptor;


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->treeViewSelectedTramas = (gcnew System::Windows::Forms::TreeView());
			this->buttonCancel = (gcnew System::Windows::Forms::Button());
			this->ButtonOK = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// treeViewSelectedTramas
			// 
			this->treeViewSelectedTramas->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->treeViewSelectedTramas->CheckBoxes = true;
			this->treeViewSelectedTramas->Location = System::Drawing::Point(12, 12);
			this->treeViewSelectedTramas->Name = L"treeViewSelectedTramas";
			this->treeViewSelectedTramas->Size = System::Drawing::Size(364, 356);
			this->treeViewSelectedTramas->TabIndex = 0;
			this->treeViewSelectedTramas->AfterCheck += gcnew System::Windows::Forms::TreeViewEventHandler(this, &TramaDescriptorForm::treeViewSelectedTramas_AfterCheck);
			// 
			// buttonCancel
			// 
			this->buttonCancel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->buttonCancel->Location = System::Drawing::Point(196, 383);
			this->buttonCancel->Name = L"buttonCancel";
			this->buttonCancel->Size = System::Drawing::Size(75, 23);
			this->buttonCancel->TabIndex = 12;
			this->buttonCancel->Text = L"Cancel";
			this->buttonCancel->UseVisualStyleBackColor = true;
			this->buttonCancel->Click += gcnew System::EventHandler(this, &TramaDescriptorForm::buttonCancel_Click);
			// 
			// ButtonOK
			// 
			this->ButtonOK->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->ButtonOK->Location = System::Drawing::Point(108, 383);
			this->ButtonOK->Name = L"ButtonOK";
			this->ButtonOK->Size = System::Drawing::Size(75, 23);
			this->ButtonOK->TabIndex = 11;
			this->ButtonOK->Text = L"OK";
			this->ButtonOK->UseVisualStyleBackColor = true;
			this->ButtonOK->Click += gcnew System::EventHandler(this, &TramaDescriptorForm::ButtonOK_Click);
			// 
			// TramaDescriptorForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(388, 421);
			this->Controls->Add(this->buttonCancel);
			this->Controls->Add(this->ButtonOK);
			this->Controls->Add(this->treeViewSelectedTramas);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->MaximumSize = System::Drawing::Size(396, 1024);
			this->MinimumSize = System::Drawing::Size(396, 34);
			this->Name = L"TramaDescriptorForm";
			this->Text = L"TramaDescriptorForm";
			this->ResumeLayout(false);

		}
#pragma endregion

		// Bouton cancel
	private: System::Void buttonCancel_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}

			 // Bouton OK
	private: System::Void ButtonOK_Click(System::Object^  sender, System::EventArgs^  e) {
		IHMToConfig();
		Close();
	}

			 // checkbox coch�e / d�coch�e
	private: System::Void treeViewSelectedTramas_AfterCheck(System::Object^  sender, System::Windows::Forms::TreeViewEventArgs^  e)
	{
		// mise � jour des checkboxes filles / m�res
		UpdateCheckBoxes(e);
	}
	};
}
