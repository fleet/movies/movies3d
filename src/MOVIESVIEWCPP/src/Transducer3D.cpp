#include "Transducer3D.h"

#include "Sounder3D.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"

Transducer3D::Transducer3D(Sounder3D *parent, unsigned int index)
{
	m_parent = parent;
	m_TransducerIndex = index;
	m_bIsVisible = true;

	Transducer *pTrans = GetTransducer();

	for (unsigned int i = 0; i < pTrans->m_numberOfSoftChannel; i++)
	{
		unsigned int polarId = i;
		m_channel3D.push_back(new Channel3D(this, polarId));
	}
}

Transducer3D::~Transducer3D(void)
{
	while (m_channel3D.size())
	{
		delete m_channel3D[0];
		m_channel3D.erase(m_channel3D.begin());
	}
}

Transducer *Transducer3D::GetTransducer()
{
	M3DKernel *pKern = M3DKernel::GetInstance();
	Sounder *pSounder = pKern->getObjectMgr()->GetLastValidSounder(m_parent->getSounderID());

	Transducer *pTrans = pSounder->GetTransducer(m_TransducerIndex);
	assert(pTrans);

	return pTrans;
}