#pragma once
#include "dataobject3d.h"
#include "Channel3D.h"
#include <vector>

class Sounder3D;
class Transducer;

class Transducer3D :
	public DataObject3D
{
public:
	Transducer3D(Sounder3D *parent, unsigned int index);
	virtual ~Transducer3D(void);

	Transducer *GetTransducer();

	unsigned int getNumberOfChannel() { return m_channel3D.size(); }
	Channel3D* getChannelWithIdx(unsigned int idx) { return m_channel3D[idx]; }

private:
	Sounder3D *m_parent;
	unsigned int m_TransducerIndex;

	std::vector<Channel3D*> m_channel3D;
};
