#include "TransducerContainerFlatView.h"
#include "TransducerFlatFrontView.h"
#include "TransducerFlatView.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "ColorPalette.h"
#include "MultifrequencyEchogramsParameter.h"


TransducerContainerView::TransducerContainerView()
{
	m_pTransducerArrayList = new TransViewCont();
}

TransducerContainerView::~TransducerContainerView(void)
{
	Clear();
	delete m_pTransducerArrayList;
}

void TransducerContainerView::UpdateParameter()
{
	for (int i = 0; i < m_pTransducerArrayList->size(); i++)
	{
		TransducerView* obj = (TransducerView*)(m_pTransducerArrayList->at(i));
		obj->CheckForUpdate();
	}
}


void TransducerContainerView::SounderChanged(bool displayPhases)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	Clear();

	for (unsigned int sound = 0; sound < pKernel->getObjectMgr()->GetSounderDefinition().GetNbSounder(); sound++)
	{
		Sounder *pSounder = pKernel->getObjectMgr()->GetSounderDefinition().GetSounder(sound);
		for (unsigned int i = 0; i < pSounder->GetTransducerCount(); i++)
		{
			m_pTransducerArrayList->push_back(Create(pSounder->m_SounderId, i, TransducerView::Sv));
			if (displayPhases)
			{
				m_pTransducerArrayList->push_back(Create(pSounder->m_SounderId, i, TransducerView::AlongShipAngle));
				m_pTransducerArrayList->push_back(Create(pSounder->m_SounderId, i, TransducerView::AthwartShipAngle));
			}
		}
	}

	//Multifrequency Echograms
	auto multiFrequencyParameters = MultifrequencyEchogramsParameter::getInstance();
	for (auto echogram : multiFrequencyParameters->getEchograms())
	{
		unsigned int sounderIndex, index1, index2, index3;
		if (echogram.getIndexes(sounderIndex, index1, index2, index3))
			m_pTransducerArrayList->push_back(CreateMultiFrequency(echogram));
	}
}

void TransducerContainerView::Clear()
{
	for (int i = 0; i < m_pTransducerArrayList->size(); i++)
	{
		TransducerView* obj = (TransducerView*)(m_pTransducerArrayList->at(i));
		delete obj;
	}

	m_pTransducerArrayList->clear();
}

void TransducerContainerView::PingFanAdded(int width, int height)
{
	for (int i = 0; i < m_pTransducerArrayList->size(); i++)
	{
		TransducerView* obj = (TransducerView*)(m_pTransducerArrayList->at(i));
		obj->PingFanAdded(width, height);
	}
}

void TransducerContainerView::EsuClosed()
{
	for (int i = 0; i < m_pTransducerArrayList->size(); i++)
	{
		TransducerView* obj = (TransducerView*)(m_pTransducerArrayList->at(i));
		obj->EsuClosed();
	}
}

System::Collections::Generic::List<System::String^> ^ TransducerContainerView::GetTransducerList()
{
	System::Collections::Generic::List<System::String^> ^ list = gcnew System::Collections::Generic::List<System::String^>;
	for (int i = 0; i < m_pTransducerArrayList->size(); i++)
	{
		TransducerView* obj = safe_cast<TransducerView*> (m_pTransducerArrayList->at(i));
		System::String ^NodeName = gcnew System::String(obj->getName().c_str());
		list->Add(NodeName);
	}
	return list;
}

void TransducerContainerView::ForceUpdate(unsigned int Index)
{
	if (Index >= 0 && Index < m_pTransducerArrayList->size())
	{
		TransducerView * view = m_pTransducerArrayList->at(Index);
		view->ForceUpdate();
	}
}

bool TransducerContainerView::GetImageChanged(unsigned int Index)
{
	if (Index < m_pTransducerArrayList->size())
	{
		TransducerView* obj = (TransducerView*)(m_pTransducerArrayList->at(Index));
		return obj->GetImageChanged();
	}
	return false;
}

void TransducerContainerView::ResetImageChanged(unsigned int Index)
{
	if (Index < m_pTransducerArrayList->size())
	{
		TransducerView* obj = (TransducerView*)(m_pTransducerArrayList->operator[](Index));
		return obj->ResetImageChanged();
	}
}
