#pragma once
#include "ColorPalette.h"
#include "TransducerFlatView.h"
#include "TransducerFlatFrontView.h"
#include "GdiBmp.h"

#include <vector>
using namespace System::Collections;
class PingFan;

typedef std::vector<TransducerView*> TransViewCont;


public ref class TransducerContainerView abstract
{
public:
	TransducerContainerView();
	virtual ~TransducerContainerView(void);

	/** called in case of parameter change*/
	void						UpdateParameter();
	void						PingFanAdded(int width, int height);
	void						EsuClosed();
	virtual void				SounderChanged(bool displayPhases);
	void						Clear();

	System::Collections::Generic::List<System::String^> ^ GetTransducerList();

	bool GetImageChanged(unsigned int Index);
	void ResetImageChanged(unsigned int Index);
	void ForceUpdate(unsigned int Index);

	virtual TransducerView*		Create(std::uint32_t sounderId, unsigned int idx, TransducerView::DisplayDataType displayDataType) = 0;
	virtual TransducerView*		CreateMultiFrequency(const MultifrequencyEchogram& coloredEchogram) = 0;

	TransducerView* GetView(int index)
	{
		if (index >= 0 && (int)m_pTransducerArrayList->size() > index)
			return m_pTransducerArrayList->at(index);
		else 
			return nullptr;
	}

	int ViewCount() 
	{
		return m_pTransducerArrayList->size();
	}

protected:
	TransViewCont		*m_pTransducerArrayList;

private:
	unsigned int m_ActiveIndex;
};

/// bug or pb with template instanciation in cli 

public ref class TransducerContainerFlatFrontView : TransducerContainerView
{
public:
	TransducerContainerFlatFrontView() : TransducerContainerView() {	};
	virtual TransducerView* Create(std::uint32_t sounderId, unsigned int idx, TransducerView::DisplayDataType displayDataType) override
	{
		return (TransducerView*) new TransducerFlatFrontView(sounderId, idx, displayDataType);
	};

	virtual TransducerView* CreateMultiFrequency(const MultifrequencyEchogram& coloredEchogram) override
	{
		unsigned int sounderIndex, transducerRedIndex, transducerGreenIndex, transducerBlueIndex;
		bool ok = coloredEchogram.getIndexes(sounderIndex, transducerRedIndex, transducerGreenIndex, transducerBlueIndex);
		if (ok)
			return new TransducerFlatFrontView(coloredEchogram.getSounderId(), transducerRedIndex, transducerGreenIndex, transducerBlueIndex, coloredEchogram);
		else
			return nullptr;
	}

	TransducerFlatFrontView* GetView(int index)
	{
		if ((int)m_pTransducerArrayList->size() > index)
			return (TransducerFlatFrontView*)(m_pTransducerArrayList->at(index));
		else 
			return nullptr;
	}
};

public ref class TransducerContainerFlatSideView : TransducerContainerView
{
public:
	TransducerContainerFlatSideView() : TransducerContainerView() {	};
	virtual TransducerView* Create(std::uint32_t sounderId, unsigned int idx, TransducerView::DisplayDataType displayDataType) override
	{
		return (TransducerView*)new TransducerFlatView(sounderId, idx, displayDataType);
	};

	virtual TransducerView* CreateMultiFrequency(const MultifrequencyEchogram& coloredEchogram) override
	{
		unsigned int sounderIndex, transducerRedIndex, transducerGreenIndex, transducerBlueIndex;
		bool ok = coloredEchogram.getIndexes(sounderIndex, transducerRedIndex, transducerGreenIndex, transducerBlueIndex);
		if (ok)
			return new TransducerFlatView(coloredEchogram.getSounderId(), transducerRedIndex, transducerGreenIndex, transducerBlueIndex, coloredEchogram);
		else
			return nullptr;
	}

	void SetActiveSlide(double percentView, unsigned int Index)
	{
		TransducerFlatView *obj = (TransducerFlatView*)(m_pTransducerArrayList->at(Index));
		obj->SetActiveSlide(percentView);
	}

	double GetActiveSlide(unsigned int Index)
	{
		TransducerFlatView *obj = (TransducerFlatView*)(m_pTransducerArrayList->at(Index));
		return obj->GetActiveSlide();
	}

	TransducerFlatView* GetView(int index)
	{
		if ((int)m_pTransducerArrayList->size() > index)
			return (TransducerFlatView*)(m_pTransducerArrayList->at(index));
		else 
			return nullptr;
	}
};