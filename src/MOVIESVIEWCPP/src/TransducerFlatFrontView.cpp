
#include "TransducerFlatFrontView.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/FanMemory.h"
#include "M3DKernel/datascheme/TransformSet.h"
#include "M3DKernel/datascheme/TransformMap.h"
#include "BaseMathLib/Vector2.h"
#include "ColorPaletteEcho.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "DisplayParameter.h"
#include "M3DKernel/utils/multithread/MutexLocker.h"

#include "colorlist.h"
#include "shoalextraction/data/pingdata.h"
#include "shoalextraction/data/shoaldata.h"
#include "shoalextraction/data/shoalstat.h"
#include "shoalextraction/data/pingshoalstat.h"
#include "shoalextraction/shoalextractionoutput.h"

#include "BottomDetection/BottomDetectionModule.h"
#include "BottomDetection/BottomDetectionContourLine.h"
#include "BottomDetection/ContourLineDef.h"
#include "ModuleManager/ModuleManager.h"

using namespace BaseMathLib;
using namespace shoalextraction;
using namespace System::Collections::Generic;

namespace
{
	constexpr const char * LoggerName = "MoviesView.View.2DViews.TransducerFlatFrontView";
}

TransducerFlatFrontView::TransducerFlatFrontView(std::uint32_t sounderId, unsigned int transducerIndex, DisplayDataType displayDataType)
	: TransducerView(sounderId, transducerIndex, displayDataType)
{
	initValues();
}

TransducerFlatFrontView::TransducerFlatFrontView(std::uint32_t sounderId, unsigned int transducerIndex, unsigned int transducerIndex2, unsigned int transducerIndex3
	,const MultifrequencyEchogram& coloredEchogram)
	: TransducerView(sounderId, transducerIndex, transducerIndex2, transducerIndex3, coloredEchogram)
{
	initValues();
}

void TransducerFlatFrontView::initValues()
{
	m_OffsetCursor = 0;
}

TransducerFlatFrontView::~TransducerFlatFrontView(void)
{
}

void TransducerFlatFrontView::PingFanAdded(int pixelwidth, int pixelheight)
{
	if (!m_Active)
		return;
	/// good sounderID and we're active so compute 2D view
	TimeCounter statTime;
	statTime.StartCount();
	this->m_pLock->Lock();
	UpdateTransformMap(pixelwidth, pixelheight, DisplayParameter::getInstance()->GetStrech());

	statTime.StopCount();
	M3DKernel::GetInstance()->m_timeCounterRecord.AddTimeCounter("FrontView UpdateTransformMap", statTime);
	statTime.StartCount();

	ComputeAll();

	statTime.StopCount();
	M3DKernel::GetInstance()->m_timeCounterRecord.AddTimeCounter("FrontView ComputeAll", statTime);

	this->m_pLock->Unlock();
}

void TransducerFlatFrontView::SetCursorOffset(unsigned int offset)
{
	this->m_pLock->Lock();
	if (m_OffsetCursor != offset)
	{
		m_OffsetCursor = offset;
		ComputeAll();
	}
	this->m_pLock->Unlock();
}

double TransducerFlatFrontView::GetFanCenter()
{
	CRecursiveMutexLocker lock(this->m_pLock);

	DisplayParameter *pDisplay = DisplayParameter::getInstance();
	M3DKernel *pKernel = M3DKernel::GetInstance();
	TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(this->GetSounderId());
	
	double fanCenter = 50.0;
	if (pCont)
	{
		int Index = pCont->GetObjectCount() - 1 - m_OffsetCursor;
		PingFan *lastFan = (PingFan *)pCont->GetDatedObject(Index);
		if (lastFan)
		{
			TransformMap *pTransform = lastFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
			auto delta = pTransform->getDelta();
			// On reproduit le calcul de pTransform->getCartesianSize2() sans convertir en entier
			auto memSizeX = delta.x / pTransform->getXSpacing();
			const Vector2D minDepthPos = pTransform->realToCartesian2Double(Vector2D(0, 0));
			fanCenter = (1 - ((memSizeX - minDepthPos.x) / (double)memSizeX))*100;
		}
	}

	return fanCenter;
}

bool TransducerFlatFrontView::UpdateBMP(PingFan *pFan)
{
	bool newBmp = false;
	TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
	if (pTransform)
	{
		Vector2I memSize = pTransform->getCartesianSize2();
		DisplayParameter *pDisplay = DisplayParameter::getInstance();

		double interBeam = pTransform->getYSpacing();
		double minProf = pDisplay->GetCurrentMinDepth();
		double maxProf = pDisplay->GetCurrentMaxDepth();

		// OTK - 10/06/2009 - zoom horizontal
		//unsigned int MyWidth=memSize.x;
		m_StartPixelHRange = (unsigned int)floor(pDisplay->GetMinFrontHorizontalRatio()*memSize.x);
		m_StopPixelHRange = (unsigned int)floor(pDisplay->GetMaxFrontHorizontalRatio()*memSize.x);
		m_TotalRange = memSize.x;
		unsigned int MyWidth = m_StopPixelHRange - m_StartPixelHRange;
		m_HorizontalMeters = MyWidth*pTransform->getXSpacing();
		// OTK - 04/11/2009 - correction cas SM20 : la transformmap ne d�marre pas � 0m
		m_StartPixelRange = (unsigned int)floor((minProf - pTransform->getRealOrigin2().y) / interBeam);
		m_StopPixelRange = (unsigned int)floor((maxProf - pTransform->getRealOrigin2().y) / interBeam);

		// OTK - FAE214 - pour placer pr�cisement les �chos simples sur l'image, on a besoin de la plage de profondeur affich�e
		m_StartPixelDepth = interBeam * m_StartPixelRange + pTransform->getRealOrigin2().y - pTransform->getYSpacing() / 2.0;
		m_StopPixelDepth = interBeam * m_StopPixelRange + pTransform->getRealOrigin2().y - pTransform->getYSpacing() / 2.0;

		unsigned int MyHeight = m_StopPixelRange - m_StartPixelRange;
		newBmp = this->AllocateBmp(MyWidth, MyHeight);
		if (newBmp) 
		{
			ResetImage(pDisplay->GetDisplay2DSingleEchoes() ? System::Drawing::Color::White : System::Drawing::Color::Black);
		}
	}
	return newBmp;
}

void TransducerFlatFrontView::ComputeAll()
{
	m_bottomPoints.clear();
	m_noisePoints.clear();
	m_refNoisePoints.clear();
	m_contourLines.clear();

	DisplayParameter *pDisplay = DisplayParameter::getInstance();
	M3DKernel *pKernel = M3DKernel::GetInstance();
	TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(this->GetSounderId());
	if (pCont)
	{
		int Index = pCont->GetObjectCount() - 1 - m_OffsetCursor;
		if (Index < 0)
		{
			Index = 0;
		}
		PingFan *lastFan = (PingFan *)pCont->GetDatedObject(Index);
		if (lastFan)
		{
			UpdateBMP(lastFan);

			if (!GetPtr())
				return;
			
			pingFanRenderer->update(pDisplay);
			Compute(lastFan);
		}
	}

	if (pDisplay->GetDisplay2DShoals())
	{
		//IPSIS - FRE - 20/08/2009 display shoals
		DisplayShoals();
	}
}

void TransducerFlatFrontView::Compute(PingFan *lastFan)
{
	DisplayParameter *pDisplay = DisplayParameter::getInstance();
	pingFanRenderer->setupPingFan(lastFan, (int)pDisplay->GetRender2DMode());

	MemoryStruct *pMemStruct = lastFan->GetMemorySetRef()->GetMemoryStruct(m_transducerIndex);
	TransformMap *pTransform = lastFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
	double compensateHeave = 0;

	Transducer *pTrans = lastFan->getSounderRef()->GetTransducer(m_transducerIndex);
	SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
	if (pSoftChan)
	{
		compensateHeave = lastFan->getHeaveChan(pSoftChan->m_softChannelId);
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "Channel Not Found");
	}

	const double prof = pTrans->m_transDepthMeter;	// transducer depth en cm

	const int offset = (int)((prof + compensateHeave) / pTransform->getYSpacing()) + pTrans->GetSampleOffset();
	const Vector2I memSize = pTransform->getCartesianSize2();
	const unsigned int Xdim = memSize.x;
	const unsigned int Ydim = memSize.y;
	const int maxTabIndex = Xdim * Ydim;  // NMD - FAE 130 - taille max d'index
	const double minProf = pDisplay->GetCurrentMinDepth();
	const double maxProf = pDisplay->GetCurrentMaxDepth();

	const int sizeX = pMemStruct->GetDataFmt()->getSize().x;
	const int sizeY = pMemStruct->GetDataFmt()->getSize().y;
	const int indexMax = sizeX * sizeY;

	m_ImageChanged = true;

	// fin des precalculs

	ResetImage(pingFanRenderer->backgroundColor);

	const Vector2I minDepthPos = pTransform->realToCartesian2(Vector2D(0, minProf));
	const Vector2I maxDepthPos = pTransform->realToCartesian2(Vector2D(0, maxProf));
	
	int * indexes = pTransform->getPointerToIndex();
	int * channelIndexes = pTransform->getPointerToChannelIndex();

	//  correctedJ (= j - offset) doit �tre born�e entre [0, Ydim[ 
	const int jMin1 = std::max(offset, minDepthPos.y - 1);
	const int jMax1 = std::min<int>(Ydim + offset, maxDepthPos.y + 1);

	// posRealY = pTransform->cartesianToRealY(correctedJ) + prof + compensateHeave;
	// posRealY doit �tre born�e entre ]minProf, maxProf[ 
	const int jMin2 = pTransform->realToCartesianY(minProf - prof - compensateHeave) + offset;
	const int jMax2 = pTransform->realToCartesianY(maxProf - prof - compensateHeave) + offset;

	const int jMin = std::max(jMin1, jMin2);
	const int jMax = std::min(jMax1, jMax2);

	for (int j = jMax-1; j >= jMin; --j)
	{
		const int correctedJ = j - offset;

		int * index_ptr = indexes + correctedJ * Xdim;		
		const int iMax = std::min(Xdim, maxTabIndex - correctedJ * Xdim);
		for (int i = 0; i < iMax; ++i)
		{
			const int index = *index_ptr;
			if (index >= 0 && index < indexMax)
			{
				SetPixel(i, j, pingFanRenderer->getColorForIndex(index));
			}
			++index_ptr;
		}
	}

	EchoPositionComputer echoPosComputer;
	echoPosComputer.transducer = pTrans;
	echoPosComputer.channel = pSoftChan;
	echoPosComputer.indexes = indexes;
	echoPosComputer.channelIndexes = channelIndexes;
	echoPosComputer.jMin = jMin;
	echoPosComputer.jMax = jMax;
	echoPosComputer.Xdim = Xdim;
	echoPosComputer.Ydim = Ydim;
	echoPosComputer.maxTabIndex = maxTabIndex;
	echoPosComputer.sizeX = sizeX;
	echoPosComputer.sizeY = sizeY;
	echoPosComputer.indexMax = indexMax;
	echoPosComputer.offset = offset;

	// DEBUT DU DESSIN DU FOND

	// Calcul des indices min / max pour la recherche du fond
	int bottomMinIdx = -1;
	int bottomMaxIdx = -1;

	const unsigned int nombreCh = pTrans->m_numberOfSoftChannel;
	std::vector<int> bottomRanges(nombreCh, -1);
	for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
	{
		SoftChannel* pChannel = pTrans->getSoftChannelPolarX(softChan);
		// pour le fond
		std::int32_t bottomRange;
		bool found = false;
		std::uint32_t echoRange = 0;
		lastFan->getBottom(pChannel->m_softChannelId, echoRange, bottomRange, found);
		if (found)
		{
			bottomRanges[softChan] = echoRange - pTrans->GetSampleOffset();

			const double angle1 = pChannel->m_mainBeamAthwartSteeringAngleRad - pChannel->m_beam3dBWidthAthwartRad *0.5;
			const double angle2 = pChannel->m_mainBeamAthwartSteeringAngleRad + pChannel->m_beam3dBWidthAthwartRad *0.5;

			const BaseMathLib::Vector2D v1 = BaseMathLib::Vector2D(sin(angle1), cos(angle1)) * bottomRange;
			const BaseMathLib::Vector2D v2 = BaseMathLib::Vector2D(sin(angle2), cos(angle2)) * bottomRange;

			std::uint32_t rangeMin = (std::uint32_t)(std::min(v1.y, v2.y) * 1e-3 / pTrans->getBeamsSamplesSpacing() + 0.5);
			std::uint32_t rangeMax = (std::uint32_t)(std::max(v1.y, v2.y) * 1e-3 / pTrans->getBeamsSamplesSpacing() + 0.5);

			rangeMin = std::min(echoRange, rangeMin) - pTrans->GetSampleOffset();
			rangeMax = std::max(echoRange, rangeMax) - pTrans->GetSampleOffset();

			double bottomMin = rangeMin * Ydim;
			bottomMin /= sizeY;

			double bottomMax = rangeMax * Ydim;
			bottomMax /= sizeY;

			int softChanBottomMinIdx = (int)floor(bottomMin - 1) + offset;
			int softChanBottomMaxIdx = (int)ceil(bottomMax + 1) + offset;

			if (bottomMinIdx == -1) bottomMinIdx = softChanBottomMinIdx;
			else bottomMinIdx = std::min(bottomMinIdx, softChanBottomMinIdx);

			if (bottomMaxIdx == -1) bottomMaxIdx = softChanBottomMaxIdx;
			else bottomMaxIdx = std::max(bottomMaxIdx, softChanBottomMaxIdx);
		}
	}

	echoPosComputer.ComputeEchoPos(bottomRanges, bottomMinIdx, bottomMaxIdx, m_bottomPoints);
	
	// FIN DU DESSIN DU FOND


	// DEBUT DU DESSIN DES LIGNES DE PORTEE DE BRUIT
	std::vector<int> noiseEchoRanges(nombreCh, -1);
	for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
	{
		bool found = false;
		auto channel = pTrans->getSoftChannelPolarX(softChan);
		noiseEchoRanges[softChan] = lastFan->getNoiseEchoNum(channel->m_softChannelId, found);
	}
	echoPosComputer.ComputeEchoPos(noiseEchoRanges, m_noisePoints);

	for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
	{

		bool found = false;
		auto channel = pTrans->getSoftChannelPolarX(softChan);
		noiseEchoRanges[softChan] = lastFan->getRefNoiseEchoNum(channel->m_softChannelId, found);
	}
	echoPosComputer.ComputeEchoPos(noiseEchoRanges, m_refNoisePoints);

	// FIN DU DESSIN DES LIGNES DE PORTEE DE BRUIT


	// DEBUT DESSIN DES LIGNES DE CONTOUR

	BottomDetectionModule* bottomModule = CModuleManager::getInstance()->GetBottomDetectionModule();
	BottomDetectionContourLine * pContourLineModule = bottomModule->GetBottomDetectionParameter().GetContourLineAlgorithm();

	// Courbes de niveau organiser par channels
	const std::map<int, std::vector<BottomDetectionContourLine::sContourLine> > & contourLines
		= pContourLineModule->GetContourLines(this->GetSounderId(), lastFan);
		
	// Reorganisation des courbes de niveau par niveau
	std::map<int, std::map<int, int> > upperIndexes;
	std::map<int, std::map<int, int> > lowerIndexes;

	for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
	{
		auto iterLine = contourLines.find(m_transducerIndex + softChan);
		if (iterLine != contourLines.end())
		{
			const std::vector<BottomDetectionContourLine::sContourLine> & contourLinesForChannel = iterLine->second;
			for (auto it = contourLinesForChannel.cbegin(); it != contourLinesForChannel.cend(); ++it)
			{
				if (pContourLineModule->DisplayLevel(it->iLevel))
				{
					upperIndexes[it->iLevel][softChan] = it->iUpperIndex;
					if (it->iLevel != BottomDetectionContourLine::s_MAX_SV_LEVEL_MAGIC_NUMBER)
					{
						upperIndexes[it->iLevel][softChan] = it->iLowerIndex;
					}
				}
			}
		}		
	}

	// merge in one vector of index per channel
	std::vector< std::vector<int> > contourLinesIndexes;
	contourLinesIndexes.reserve(upperIndexes.size() + lowerIndexes.size());
	for (auto itLevel = upperIndexes.cbegin(); itLevel != upperIndexes.cend(); ++itLevel)
	{
		std::vector<int> levelIndexes(nombreCh, -1);
		const std::map<int, int> & values = itLevel->second;
		for (auto itChan = values.cbegin(); itChan != values.cend(); ++itChan)
		{
			levelIndexes[itChan->first] = itChan->second;
		}
		contourLinesIndexes.push_back(levelIndexes);
	}

	for (auto itLevel = lowerIndexes.cbegin(); itLevel != lowerIndexes.cend(); ++itLevel)
	{
		std::vector<int> levelIndexes(nombreCh, -1);
		const std::map<int, int> & values = itLevel->second;
		for (auto itChan = values.cbegin(); itChan != values.cend(); ++itChan)
		{
			levelIndexes[itChan->first] = itChan->second;
		}
		contourLinesIndexes.push_back(levelIndexes);
	}

	for(auto it = contourLinesIndexes.cbegin(); it != contourLinesIndexes.cend(); ++it)
	{
		const std::vector<int> & contourLineIndexes = *it;

		bottomMinIdx = -1;
		bottomMaxIdx = -1;

		for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
		{
			const int echoRange = contourLineIndexes[softChan];
			bottomRanges[softChan] = echoRange;
			
			if (echoRange != -1)
			{
				//const std::vector<BottomDetectionContourLine::sContourLine> & contourLinesForChannel = iterLine->second;

				SoftChannel* pChannel = pTrans->getSoftChannelPolarX(softChan);

				const double angle1 = pChannel->m_mainBeamAthwartSteeringAngleRad - pChannel->m_beam3dBWidthAthwartRad *0.5;
				const double angle2 = pChannel->m_mainBeamAthwartSteeringAngleRad + pChannel->m_beam3dBWidthAthwartRad *0.5;

				const BaseMathLib::Vector2D v1 = BaseMathLib::Vector2D(sin(angle1), cos(angle1)) * echoRange;
				const BaseMathLib::Vector2D v2 = BaseMathLib::Vector2D(sin(angle2), cos(angle2)) * echoRange;

				std::uint32_t rangeMin = std::min(v1.y, v2.y);
				std::uint32_t rangeMax = std::max(v1.y, v2.y);

				rangeMin = std::min<std::uint32_t>(echoRange, rangeMin); // -pTrans->GetSampleOffset();
				rangeMax = std::max<std::uint32_t>(echoRange, rangeMax); // -pTrans->GetSampleOffset();

				double bottomMin = rangeMin * Ydim;
				bottomMin /= sizeY;

				double bottomMax = rangeMax * Ydim;
				bottomMax /= sizeY;

				int softChanBottomMinIdx = (int)floor(bottomMin - 1) + offset;
				int softChanBottomMaxIdx = (int)ceil(bottomMax + 1) + offset;

				if (bottomMinIdx == -1) bottomMinIdx = softChanBottomMinIdx;
				else bottomMinIdx = std::min(bottomMinIdx, softChanBottomMinIdx);

				if (bottomMaxIdx == -1) bottomMaxIdx = softChanBottomMaxIdx;
				else bottomMaxIdx = std::max(bottomMaxIdx, softChanBottomMaxIdx);
			}
		}

		if (bottomMinIdx != -1 && bottomMaxIdx != -1)
		{
			std::vector< BaseMathLib::Vector2I > contourLinePoints;

			int jStart = std::max(jMin, bottomMinIdx);
			int jEnd = std::min(jMax, bottomMaxIdx);
			std::vector<bool> bottomFound(Xdim, false);

			for (int j = jEnd; j >= jStart; --j)
			{
				const int correctedJ = j - offset;
				int * index_ptr = indexes + correctedJ * Xdim;
				int * channel_ptr = channelIndexes + correctedJ * Xdim;

				const int iMax = std::min(Xdim, maxTabIndex - correctedJ * Xdim);
				for (int i = 0; i < iMax; ++i)
				{
					if (!bottomFound[i])
					{
						const int index = *index_ptr;
						if (index >= 0 && index < indexMax)
						{
							const int channelIndex = *channel_ptr;
							if (channelIndex >= 0 && channelIndex < indexMax)
							{
								const int echoNum = index%sizeY;
								if (echoNum <= bottomRanges[channelIndex])
								{
									// OTK - 30/03/2010 - on ne trace plus le fond directement sur l'image,
									// mais on sauvegarde sa position pour le tracer par dessus, en GDI+
									contourLinePoints.push_back(BaseMathLib::Vector2I(i, j));
									bottomFound[i] = true;
								}
							}
						}
					}
					++index_ptr;
					++channel_ptr;
				}
			}

			m_contourLines.push_back(contourLinePoints);
		}
	}	

	// FIN DESSIN DES LIGNES DE CONTOUR
}

System::String^ TransducerFlatFrontView::FormatEchoData(double posX, double posY)
{
	System::String^ result = "";
	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(this->GetSounderId());
	if (pCont)
	{
		int Index = pCont->GetObjectCount() - 1 - m_OffsetCursor;
		if (Index < 0)
		{
			Index = 0;
		}
		PingFan *pFan = (PingFan *)pCont->GetDatedObject(Index);
		if (pFan)
		{
			int pixelX = (int)floor((m_StopPixelHRange - m_StartPixelHRange)*posX + m_StartPixelHRange);
			int pixelY = (int)floor((m_StopPixelRange - m_StartPixelRange)*posY + m_StartPixelRange);

			result = FormatEchoToolTip(pFan, pixelX, pixelY);
		}
	}
	pKernel->Unlock();

	return result;
}

//*****************************************************************************
// Name : GetPingId
// Description : get the ping id from the current cursor
// Parameters : void
// Return : void
//*****************************************************************************
std::uint64_t TransducerFlatFrontView::GetPingId()
{
	std::uint64_t result = 0;

	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(this->GetSounderId());
	if (pCont)
	{
		int Index = pCont->GetObjectCount() - 1 - m_OffsetCursor;
		if (Index < 0)
		{
			Index = 0;
		}
		PingFan *pFan = (PingFan *)pCont->GetDatedObject(Index);
		if (pFan)
		{
			result = pFan->GetPingId();
		}
	}
	pKernel->Unlock();

	return result;
}

//*****************************************************************************
// Name : DisplayShoals
// Description : display the shoals
// Parameters : void
// Return : void
//*****************************************************************************
void TransducerFlatFrontView::DisplayShoals()
{
	System::Drawing::Color color;

	//get the shoal info for this ping, sounder and transducer
	std::vector<TShoalPingInfo> shoalInfo = GetFilteredShoalsInfo(GetPingId());
	std::vector<TShoalPingInfo>::iterator iterInfo = shoalInfo.begin();

	DisplayParameter *pDisplay = DisplayParameter::getInstance();

	while (iterInfo != shoalInfo.end())
	{
		ShoalData* pShoalData = (*iterInfo).first;
		PingData* pPingData = (*iterInfo).second;

		if (pShoalData != NULL && pPingData != NULL)
		{
			if (pDisplay->GetCurrentMinDepth() < pShoalData->GetShoalStat()->GetMaxDepth() &&
				pDisplay->GetCurrentMaxDepth() > pShoalData->GetShoalStat()->GetMinDepth())
			{
				double depthOffset = 0;
				Sounder* pSounder = pPingData->GetPingFan()->getSounderRef();
				Transducer *pTrans = pSounder->GetTransducer(m_transducerIndex);

				if (pTrans != NULL)
				{
					SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
					if (pSoftChan)
					{
						depthOffset += pPingData->GetPingFan()->getHeaveChan(pSoftChan->m_softChannelId);
					}
					depthOffset += pTrans->m_transDepthMeter;	// transducer depth en cm

					color = ColorList::GetInstance()->GetColor(pShoalData->GetExternId());

					std::vector<HullItem*>::iterator iter = pPingData->GetShoalStat()->GetHullList().begin();
					TransformMap *pTransform = pSounder->GetTransformSetRef()->GetTransformMap(m_transducerIndex);

					while (iter != pPingData->GetShoalStat()->GetHullList().end())
					{
						//draw the 2D hull
						RenderShoalHull(*iter, pTransform, depthOffset, color);
						iter++;
					}
				}
			}
		}//end if pRefPingData != NULL
		iterInfo++;
	}
}


//*****************************************************************************
// Name : RenderShoalHull
// Description : Draw the hull
// Parameters : (in)	HullItem* pHullItem
//				(in)	TransformMap *pTransform
//				(in)	double depthOffset
// Return : void
//*****************************************************************************
void TransducerFlatFrontView::RenderShoalHull(HullItem* pHullItem,
	TransformMap *pTransform,
	double depthOffset,
	System::Drawing::Color color)
{
	Poly2D& poly = pHullItem->surfOrtho;

	int nbPoints = poly.GetNbPoints();
	for (int i = 0; i < nbPoints; i++)
	{
		BaseMathLib::Vector2D pointI(*poly.GetPointX(i), *poly.GetPointY(i) + depthOffset);
		BaseMathLib::Vector2D pointJ(*poly.GetPointX((i + 1) % nbPoints), *poly.GetPointY((i + 1) % nbPoints) + depthOffset);

		BaseMathLib::Vector2I point1 = pTransform->realToCartesian2(pointI);
		BaseMathLib::Vector2I point2 = pTransform->realToCartesian2(pointJ);

		DrawLine(point1, point2, color);
	}
}
