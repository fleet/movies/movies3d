#pragma once
#include "TransducerView.h"

namespace shoalextraction
{
	struct HullItem;
}

class TransformMap;

class TransducerFlatFrontView : public TransducerView
{
public:
	TransducerFlatFrontView(std::uint32_t sounderId, unsigned int transducerIndex, DisplayDataType displayDataType);
	TransducerFlatFrontView(std::uint32_t sounderId, unsigned int transducerIndex, unsigned int transducerIndex2, unsigned int transducerIndex3
		, const MultifrequencyEchogram& coloredEchogram);

	virtual ~TransducerFlatFrontView(void);

	virtual void PingFanAdded(int width, int height) override;

	void SetCursorOffset(unsigned int offset);
	// Renvoie la position du curseur correspondant au centre des donn�es
	double GetFanCenter();
	unsigned int GetCursorOffset() { return m_OffsetCursor; };
	virtual System::String^ FormatEchoData(double posX, double posY);

	double GetHorizontalMeters() { return m_HorizontalMeters; };

	inline const std::vector<BaseMathLib::Vector2I> & GetBottomPoints() const { return m_bottomPoints; }
	inline const std::vector<BaseMathLib::Vector2I> & GetNoisePoints() const { return m_noisePoints; }
	inline const std::vector<BaseMathLib::Vector2I> & GetRefNoisePoints() const { return m_refNoisePoints; }
	inline const std::vector< std::vector<BaseMathLib::Vector2I> > & GetContourLines() { return m_contourLines; }

protected:

	void initValues();

	// Get the ping id from the current cursor
	std::uint64_t GetPingId();

	// display the shoals
	void DisplayShoals();

	// Draw the hull
	void RenderShoalHull(shoalextraction::HullItem* pHullItem,
		TransformMap *pTransform,
		double depthOffset,
		System::Drawing::Color color);

	bool UpdateBMP(PingFan *pFan);
	virtual	void ComputeAll() override;

	void Compute(PingFan *lastFan);
	
	unsigned int m_OffsetCursor;

	// dimension horizontale r�elle de l'image en m�tres
	double m_HorizontalMeters;
		
	std::vector<BaseMathLib::Vector2I> m_bottomPoints;   // Points du fond
	std::vector<BaseMathLib::Vector2I> m_noisePoints;    // Noise Range points
	std::vector<BaseMathLib::Vector2I> m_refNoisePoints; // Ref Noise Range points
	std::vector< std::vector<BaseMathLib::Vector2I> > m_contourLines;
};
