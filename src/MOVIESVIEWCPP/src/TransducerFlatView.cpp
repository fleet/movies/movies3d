﻿
#include "TransducerFlatView.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/FanMemory.h"
#include "M3DKernel/datascheme/TransformSet.h"
#include "M3DKernel/datascheme/TransformMap.h"
#include "BaseMathLib/Vector2.h"
#include "ColorPaletteEcho.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "DisplayParameter.h"

#include <algorithm>


#include "colorlist.h"
#include "shoalextraction/data/pingdata.h"
#include "shoalextraction/data/shoaldata.h"
#include "shoalextraction/data/shoalstat.h"
#include "shoalextraction/data/pingshoalstat.h"
#include "shoalextraction/shoalextractionoutput.h"

#include "BottomDetection/BottomDetectionModule.h"
#include "BottomDetection/BottomDetectionContourLine.h"
#include "ModuleManager/ModuleManager.h"

using namespace BaseMathLib;
using namespace shoalextraction;
using namespace System::Collections::Generic;

namespace
{
	constexpr const char * LoggerName = "MoviesView.View.2DViews.TransducerFlatView";
}

TransducerFlatView::TransducerFlatView(std::uint32_t sounderId, unsigned int transducerIndex, DisplayDataType displayDataType)
	: TransducerView(sounderId, transducerIndex, displayDataType)
{
	initValues();
}

TransducerFlatView::TransducerFlatView(std::uint32_t sounderId, unsigned int transducerIndex, unsigned int transducerIndex2, unsigned int transducerIndex3, const MultifrequencyEchogram& coloredEchogram)
	: TransducerView(sounderId, transducerIndex, transducerIndex2, transducerIndex3, coloredEchogram)
{
	initValues();
}

void TransducerFlatView::initValues()
{
	m_ActiveSlide = 50.0;
	m_RecomputeAll = false;
	m_lastAddedIdx = -1;
	m_lastDisplayedIdx = -1;
	m_lastPoint3d = 0;
	m_firstPoint3d = 0;
	m_firstDrawnPingFanID = 0;
	m_lastDrawnPingFanID = 0;
	m_surfaceValues = nullptr;
	m_bottomValues = nullptr;
	m_deviationValues = nullptr;
	m_refNoiseValues = nullptr;
	m_noiseValues = nullptr;
}

TransducerFlatView::~TransducerFlatView()
{
	delete[] m_bottomValues;
	delete[] m_deviationValues;
	delete[] m_refNoiseValues;
	delete[] m_noiseValues;
}

void TransducerFlatView::PingFanAdded(int pixelwidth, int pixelheight)
{
	if (!m_Active)
		return;

	/// good sounderID and we're active so compute 2D view
	TimeCounter statTime;
	statTime.StartCount();
	this->m_pLock->Lock();
	M3DKernel *pKernel = M3DKernel::GetInstance();
	FanMemory *pFanMemory = pKernel->getObjectMgr()->GetSounderDefinition().GetLinkedFanMemory(m_sounderId);
	unsigned int width = pFanMemory->GetMemoryLength();

	double interBeam, minYOrigin, maxYOrigin;
	ComputeTransformMapsProperties(interBeam, minYOrigin, maxYOrigin);

	bool newBmp = UpdateBMP(interBeam, minYOrigin, maxYOrigin, width);
	if (newBmp)
	{
		ComputeAll();
	}
	else
	{
		// OTK - 26/02/2009 - OPTI - ajout du mode incr�mental
		ComputePartial();

		//IPSIS - FRE - 20/08/2009 display shoals
		if (DisplayParameter::getInstance()->GetDisplay2DShoals())
		{
			DisplayShoals();
		}
	}

	m_ImageChanged = true;

	this->m_pLock->Unlock();
	statTime.StopCount();
	M3DKernel::GetInstance()->m_timeCounterRecord.AddTimeCounter("SideView fanAdded", statTime);
}

void TransducerFlatView::EsuClosed()
{
	// Rebuild noise echo lines
	M3DKernel *pKernel = M3DKernel::GetInstance();
	ESUParameter* esu = pKernel->getMovESUManager()->GetESUContainer().GetMostRecentClosedESU();
	if (esu == NULL)
		return;

	this->m_pLock->Lock();

	if (m_refNoiseValues != NULL && m_noiseValues != NULL)
	{
		SounderIndexedTimeContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();
		FanMemory *pFanMemory = pKernel->getObjectMgr()->GetSounderDefinition().GetLinkedFanMemory(m_sounderId);

		auto pDisplay = DisplayParameter::getInstance();
		bool firstDistLine = true;
		int nDistLine = 0;
		int distanceSampling = (int)(pDisplay->GetDistanceSampling()*1000.);
		int delayed3d = pDisplay->GetPingFanDelayed();
		int lenght3d = pDisplay->GetPingFanLength();

		unsigned int nbPingsToDraw = 0;
		for (size_t w = 0; w < pFanContainer->GetObjectCount(); w++)
		{
			PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);
			if (pFan->getSounderRef()->m_SounderId == m_sounderId)
			{
				nbPingsToDraw++;
			}
		}
		unsigned int width = pFanMemory->GetMemoryLength();
		int fanIdx = std::min<int>(nbPingsToDraw - 1, width - 1);

		for (int w = pFanContainer->GetObjectCount() - 1; w >= 0 && fanIdx >= 0; w--)
		{
			PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);
			Sounder* pSounder = pFan->getSounderRef();
			std::uint64_t  pingId = pFan->GetPingId();
			if (pSounder->m_SounderId == m_sounderId && pingId >= esu->GetESUPingNumber() && pingId <= esu->GetESULastPingNumber())
			{
				MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(m_transducerIndex);
				TransformMap *pTransform = pSounder->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
				Transducer *pTrans = pSounder->GetTransducer(m_transducerIndex);
				SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);

				double compensateHeave = 0;
				if (pSoftChan)
				{
					compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
				}

				double prof = pTrans->m_transDepthMeter;	// transducer depth en cm
				int offset = (int)floor((prof + compensateHeave) / pTransform->getYSpacing()) + pTrans->GetSampleOffset();
				Vector2I memSize = pTransform->getCartesianSize2();
				unsigned int Xdim = memSize.x;
				unsigned int Ydim = memSize.y;
				int maxTabIndex = Xdim * Ydim;
				double minProf = pDisplay->GetCurrentMinDepth();
				double maxProf = pDisplay->GetCurrentMaxDepth();
				m_ImageChanged = true;
				unsigned int i = (unsigned int)floor(Xdim*this->m_ActiveSlide / 100.0);

				// pour ne pas avoir � le faire � chaque pixel...
				int sizeX = pMemStruct->GetDataFmt()->getSize().x;
				int sizeY = pMemStruct->GetDataFmt()->getSize().y;
				const int indexMax = sizeX * sizeY;

				int * indexes = pTransform->getPointerToIndex() + i;
				int * channelIndexes = pTransform->getPointerToChannelIndex() + i;

				int jMin = pTransform->realToCartesianY(minProf - prof - compensateHeave) + offset;
				int jMax = pTransform->realToCartesianY(maxProf - prof - compensateHeave) + offset;

				int jStart = std::min<int>(jMax, Ydim - 1);
				int jEnd = std::max<int>(jMin, offset);

				EchoPositionComputer echoPosComputer;
				echoPosComputer.transducer = pTrans;
				echoPosComputer.channel = pSoftChan;
				echoPosComputer.indexes = indexes;
				echoPosComputer.channelIndexes = channelIndexes;
				echoPosComputer.jMin = jEnd;
				echoPosComputer.jMax = jStart;
				echoPosComputer.Xdim = Xdim;
				echoPosComputer.Ydim = Ydim;
				echoPosComputer.maxTabIndex = maxTabIndex;
				echoPosComputer.sizeX = sizeX;
				echoPosComputer.sizeY = sizeY;
				echoPosComputer.indexMax = indexMax;
				echoPosComputer.offset = offset;

				const unsigned int nombreCh = pTrans->m_numberOfSoftChannel;
				std::vector<int> noiseEchoRanges(nombreCh, -1);

				int noiseEchoPos = -1;
				for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
				{
					bool found = false;
					auto channel = pTrans->getSoftChannelPolarX(softChan);
					noiseEchoRanges[softChan] = pFan->getNoiseEchoNum(channel->m_softChannelId, found);
				}

				echoPosComputer.ComputeSingleEchoPos(noiseEchoRanges, noiseEchoPos);
				if (noiseEchoPos != -1)
				{
					m_noiseValues[fanIdx - m_StartPixelHRange] = noiseEchoPos;
				}

				for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
				{
					bool found = false;
					auto channel = pTrans->getSoftChannelPolarX(softChan);
					noiseEchoRanges[softChan] = pFan->getRefNoiseEchoNum(channel->m_softChannelId, found);
				}
				echoPosComputer.ComputeSingleEchoPos(noiseEchoRanges, noiseEchoPos);
				if (noiseEchoPos != -1)
				{
					m_refNoiseValues[fanIdx - m_StartPixelHRange] = noiseEchoPos;
				}

				fanIdx--;
			}
		}

		m_ImageChanged = true;
	}
	this->m_pLock->Unlock();
}

void TransducerFlatView::Shift(int n)
{
	System::Drawing::Color^ white = GetBackgroundColor();
	const int whiteArgb = white->ToArgb();
	const int w = GetWidth();
	int * buffer = new int[w - n];

	for (unsigned int i = 0; i < GetHeight(); i++)
	{
		int * StartPtr = GetPtr() + i*w;
		memcpy(buffer, StartPtr + n, sizeof(int)*(w - n));
		memcpy(StartPtr, buffer, sizeof(int)*(w - n));
		memset(StartPtr + w - n, whiteArgb, sizeof(int) * n);
	}

	// shift de la ligne des surface
	memcpy(buffer, m_surfaceValues + n, sizeof(int)*(w - n));
	memcpy(m_surfaceValues, buffer, sizeof(int)*(w - n));
	memset(m_surfaceValues + w - n, -1, sizeof(int) * n);

	// shift de la ligne de fond
	memcpy(buffer, m_bottomValues + n, sizeof(int)*(w - n));
	memcpy(m_bottomValues, buffer, sizeof(int)*(w - n));
	memset(m_bottomValues + w - n, -1, sizeof(int) * n);

	// shift de la ligne de bruit de r�f�rence
	memcpy(buffer, m_refNoiseValues + n, sizeof(int)*(w - n));
	memcpy(m_refNoiseValues, buffer, sizeof(int)*(w - n));
	memset(m_refNoiseValues + w - n, -1, sizeof(int) * n);

	// shift de la ligne de bruit
	memcpy(buffer, m_noiseValues + n, sizeof(int)*(w - n));
	memcpy(m_noiseValues, buffer, sizeof(int)*(w - n));
	memset(m_noiseValues + w - n, -1, sizeof(int) * n);

	// shift des lignes de contour
	//std::map<int, std::pair<std::vector<int>, std::vector<int> > >::iterator iter;
	for (auto iter = m_contourLines.begin(); iter != m_contourLines.end(); ++iter)
	{
		// ligne superieure
		std::vector<int> & line = *iter;
		memcpy(buffer, line.data() + n, sizeof(int)*(w - n));
		memcpy(line.data(), buffer, sizeof(int)*(w - n));
		memset(line.data() + w - n, -1, sizeof(int) * n);

		/*
		// rmq. : pas de ligne inf�rieure pour la ligne du SV max
		if (iter->first != BottomDetectionContourLine::s_MAX_SV_LEVEL_MAGIC_NUMBER)
		{
		// ligne inf�rieure
		memcpy(buffer, &(iter->second.second[0]) + n, sizeof(int)*(w - n));
		memcpy(&(iter->second.second[0]), buffer, sizeof(int)*(w - n));
		memset(&(iter->second.second[0]) + w - n, -1, sizeof(int) * n);
		}
		*/
	}

	// shift de la ligne de deviation
	memcpy(buffer, m_deviationValues + n, sizeof(int)*(w - n));
	memcpy(m_deviationValues, buffer, sizeof(int)*(w - n));
	memset(m_deviationValues + w - n, -1, sizeof(int) * n);

	delete[] buffer;
}

void TransducerFlatView::AddFan(PingFan *pFan, int fanIdx)
{
	// OTK - si le ping est trop ancien, on ne l'affiche pas
	if (fanIdx >= 0)
	{
		m_lastAddedIdx = std::max(m_lastAddedIdx, (int)fanIdx);
		DisplayParameter *pDisplay = DisplayParameter::getInstance();
		
		// on ne prend en compte que ce qui est r�ellement affich� (zoom)
		if (fanIdx < m_StopPixelHRange && fanIdx >= m_StartPixelHRange)
		{
			m_lastDisplayedIdx = std::max(m_lastDisplayedIdx, (int)fanIdx);
			if (pFan)
			{
				pingFanRenderer->setupPingFan(pFan, pDisplay->GetRender2DMode());

				Sounder* pSounder = pFan->getSounderRef();
				MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(m_transducerIndex);
				TransformMap *pTransform = pSounder->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
				short *value = ((short*)pMemStruct->GetDataFmt()->GetData());
				double compensateHeave = 0;
				Transducer *pTrans = pSounder->GetTransducer(m_transducerIndex);
				SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
				if (pSoftChan)
				{
					compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
				}
				else
				{
					M3D_LOG_ERROR(LoggerName, "Channel Not Found");
				}

				const double prof = pTrans->m_transDepthMeter;	// transducer depth en cm
				const int offset = (int)floor((prof + compensateHeave) / pTransform->getYSpacing()) + pTrans->GetSampleOffset();
				m_surfaceValues[fanIdx - m_StartPixelHRange] = offset;

				const Vector2I memSize = pTransform->getCartesianSize2();
				const unsigned int Xdim = memSize.x;
				const unsigned int Ydim = memSize.y;
				const int maxTabIndex = Xdim * Ydim;  // NMD - FAE 130 - taille max d'index
				const double minProf = pDisplay->GetCurrentMinDepth();
				const double maxProf = pDisplay->GetCurrentMaxDepth();
				m_ImageChanged = true;
				
				bool afterBottom = false;
				bool drawningBottom = false;

				// pour ne pas avoir � le faire � chaque pixel...
				const int sizeX = pMemStruct->GetDataFmt()->getSize().x;
				const int sizeY = pMemStruct->GetDataFmt()->getSize().y;
				const int indexMax = sizeX * sizeY;

				int * indexes = pTransform->getPointerToIndex();
				int * channelIndexes = pTransform->getPointerToChannelIndex();

				const Vector2I minDepthPos = pTransform->realToCartesian2(Vector2D(0, minProf));
				const Vector2I maxDepthPos = pTransform->realToCartesian2(Vector2D(0, maxProf));

				//  correctedJ (= j - offset) doit être bornée entre [0, Ydim[ 
				const int jMin1 = std::max(offset, minDepthPos.y - 1);
				const int jMax1 = std::min<int>(Ydim + offset, maxDepthPos.y + 1);

				// posRealY = pTransform->cartesianToRealY(correctedJ) + prof + compensateHeave;
				// posRealY doit être bornée entre ]minProf, maxProf[ 
				const int jMin2 = pTransform->realToCartesianY(minProf - prof - compensateHeave) + offset;
				const int jMax2 = pTransform->realToCartesianY(maxProf - prof - compensateHeave) + offset;

				const int jMin = std::max(jMin1, jMin2);
				const int jMax = std::min(jMax1, jMax2);

				const int blackARGB = System::Drawing::Color::Black.ToArgb();

				for (int j = jMin - 1; j >= 0; --j)
				{
					SetPixel(fanIdx, j, blackARGB);
				}

				for (int j = jMax - 1; j >= jMin; --j)
				{
					int pxColor = blackARGB;
					const int correctedJ = j - offset;

					const int iMax = std::min(Xdim, maxTabIndex - correctedJ * Xdim);
					const unsigned int i = std::min<unsigned int>(floor(Xdim * this->m_ActiveSlide / 100.0), iMax);

					const int index = *(indexes + i + correctedJ * Xdim);
					if (index >= 0 && index < indexMax)
					{
						pxColor = pingFanRenderer->getColorForIndex(index);
					}

					SetPixel(fanIdx, j, pxColor);
				}

				for (int j = this->GetHeight() + this->m_StartPixelRange - 1; j >= jMax; --j)
				{
					SetPixel(fanIdx, j, blackARGB);
				}

				EchoPositionComputer echoPosComputer;
				echoPosComputer.transducer = pTrans;
				echoPosComputer.channel = pSoftChan;
				echoPosComputer.indexes = indexes;
				echoPosComputer.channelIndexes = channelIndexes;
				echoPosComputer.jMin = jMin;
				echoPosComputer.jMax = jMax;
				echoPosComputer.Xdim = Xdim;
				echoPosComputer.Ydim = Ydim;
				echoPosComputer.maxTabIndex = maxTabIndex;
				echoPosComputer.sizeX = sizeX;
				echoPosComputer.sizeY = sizeY;
				echoPosComputer.indexMax = indexMax;
				echoPosComputer.offset = offset;

				/// CALCUL DU FOND

				const double deviationRatio = pDisplay->GetDeviationCoefficient();

				// Calcul des indices min / max pour la recherche du fond
				int bottomMinIdx = -1;
				int bottomMaxIdx = -1;

				const unsigned int nombreCh = pTrans->m_numberOfSoftChannel;
				std::vector<int> bottomRanges(nombreCh, -1);
				std::vector<int> deviations(nombreCh, -1);
				for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
				{
					SoftChannel* pChannel = pTrans->getSoftChannelPolarX(softChan);
					std::int32_t bottomRange;
					bool found = false;
					std::uint32_t echoRange = 0;
					pFan->getBottom(pChannel->m_softChannelId, echoRange, bottomRange, found);
					if (found)
					{
						bottomRanges[softChan] = echoRange - pTrans->GetSampleOffset();

						const double angle1 = pChannel->m_mainBeamAthwartSteeringAngleRad - pChannel->m_beam3dBWidthAthwartRad *0.5;
						const double angle2 = pChannel->m_mainBeamAthwartSteeringAngleRad + pChannel->m_beam3dBWidthAthwartRad *0.5;

						const BaseMathLib::Vector2D v1 = BaseMathLib::Vector2D(sin(angle1), cos(angle1)) * bottomRange;
						const BaseMathLib::Vector2D v2 = BaseMathLib::Vector2D(sin(angle2), cos(angle2)) * bottomRange;

						std::uint32_t rangeMin = (std::uint32_t)(std::min(v1.y, v2.y) * 1e-3 / pTrans->getBeamsSamplesSpacing() + 0.5);
						std::uint32_t rangeMax = (std::uint32_t)(std::max(v1.y, v2.y) * 1e-3 / pTrans->getBeamsSamplesSpacing() + 0.5);

						rangeMin = std::min(echoRange, rangeMin) - pTrans->GetSampleOffset();
						rangeMax = std::max(echoRange, rangeMax) - pTrans->GetSampleOffset();

						double bottomMin = rangeMin * Ydim;
						bottomMin /= sizeY;

						double bottomMax = rangeMax * Ydim;
						bottomMax /= sizeY;

						int softChanBottomMinIdx = (int)floor(bottomMin - 1) + offset;
						int softChanBottomMaxIdx = (int)ceil(bottomMax + 1) + offset;

						if (bottomMinIdx == -1) bottomMinIdx = softChanBottomMinIdx;
						else bottomMinIdx = std::min(bottomMinIdx, softChanBottomMinIdx);

						if (bottomMaxIdx == -1) bottomMaxIdx = softChanBottomMaxIdx;
						else bottomMaxIdx = std::max(bottomMaxIdx, softChanBottomMaxIdx);
					}
					else
					{
						bottomRanges[softChan] = -1;
					}

					double deviation;
					pFan->getDeviation(pChannel->m_softChannelId, deviation, found);
					if (found)
					{
						deviations[softChan] = (int)floor(0.5 + deviation * deviationRatio);
					}
					else
					{
						deviations[softChan] = -1;
					}
				}

				if (bottomMinIdx != -1 && bottomMaxIdx != -1)
				{
					int jBottomMax = std::min(jMax, bottomMaxIdx);
					int jBottomMin = std::max(jMin, bottomMinIdx - 1);

					// assigne bottom to max
					m_bottomValues[fanIdx - m_StartPixelHRange] = jBottomMax;

					for (int j = jBottomMax; j > jBottomMin; --j)
					{
						const int correctedJ = j - offset;

						const int iMax = std::min(Xdim, maxTabIndex - correctedJ * Xdim);
						const unsigned int i = std::min<unsigned int>(floor(Xdim * this->m_ActiveSlide / 100.0), iMax);
						const int index = *(indexes + i + correctedJ * Xdim);

						if (index >= 0 && index < indexMax)
						{
							const int channelIndex = *(channelIndexes + i + correctedJ * Xdim);
							if (channelIndex >= 0 && channelIndex < indexMax)
							{
								const int echoNum = index%sizeY;
								if (echoNum <= bottomRanges[channelIndex])
								{
									// OTK - 30/03/2010 - on ne trace plus le fond directement sur l'image,
									// mais on sauvegarde sa position pour le tracer par dessus, en GDI+
									//SetPixel(fanIdx,j,blackARGB);
									m_bottomValues[fanIdx - m_StartPixelHRange] = j;
									break;
								}
							}
						}
					}
				}

				// FIN DU CALCUL DU FOND

				// DEBUT DU DESSIN DES LIGNES DE PORTEE DE BRUIT

				std::vector<int> noiseEchoRanges(nombreCh, -1);

				int noiseEchoPos = -1;
				for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
				{
					bool found = false;
					auto channel = pTrans->getSoftChannelPolarX(softChan);
					noiseEchoRanges[softChan] = pFan->getNoiseEchoNum(channel->m_softChannelId, found);
				}

				echoPosComputer.ComputeSingleEchoPos(noiseEchoRanges, noiseEchoPos);
				if (noiseEchoPos != -1)
				{
					m_noiseValues[fanIdx - m_StartPixelHRange] = noiseEchoPos;
				}

				for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
				{
					bool found = false;
					auto channel = pTrans->getSoftChannelPolarX(softChan);
					noiseEchoRanges[softChan] = pFan->getRefNoiseEchoNum(channel->m_softChannelId, found);
				}
				echoPosComputer.ComputeSingleEchoPos(noiseEchoRanges, noiseEchoPos);
				if (noiseEchoPos != -1)
				{
					m_refNoiseValues[fanIdx - m_StartPixelHRange] = noiseEchoPos;
				}

				// FIN DU DESSIN DES LIGNES DE PORTEE DE BRUIT

				// CALCUL DES LIGNES DE CONTOUR

				BottomDetectionModule* bottomModule = CModuleManager::getInstance()->GetBottomDetectionModule();
				BottomDetectionContourLine * pContourLineModule = bottomModule->GetBottomDetectionParameter().GetContourLineAlgorithm();

				// Courbes de niveau organiser par channels
				const std::map<int, std::vector<BottomDetectionContourLine::sContourLine> > & contourLines
					= pContourLineModule->GetContourLines(this->GetSounderId(), pFan);

				// Reorganisation des courbes de niveau par niveau
				std::map<int, std::map<int, int> > upperIndexes;
				std::map<int, std::map<int, int> > lowerIndexes;

				for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
				{
					auto iterLine = contourLines.find(m_transducerIndex + softChan);
					if (iterLine != contourLines.end())
					{
						const std::vector<BottomDetectionContourLine::sContourLine> & contourLinesForChannel = iterLine->second;
						for (auto it = contourLinesForChannel.cbegin(); it != contourLinesForChannel.cend(); ++it)
						{
							if (pContourLineModule->DisplayLevel(it->iLevel))
							{
								upperIndexes[it->iLevel][softChan] = it->iUpperIndex;
								if (it->iLevel != BottomDetectionContourLine::s_MAX_SV_LEVEL_MAGIC_NUMBER)
								{
									upperIndexes[it->iLevel][softChan] = it->iLowerIndex;
								}
							}
						}
					}
				}

				// merge in one vector of index per channel
				std::vector< std::vector<int> > contourLinesIndexes;
				contourLinesIndexes.reserve(upperIndexes.size() + lowerIndexes.size());
				for (auto itLevel = upperIndexes.cbegin(); itLevel != upperIndexes.cend(); ++itLevel)
				{
					std::vector<int> levelIndexes(nombreCh, -1);
					const std::map<int, int> & values = itLevel->second;
					for (auto itChan = values.cbegin(); itChan != values.cend(); ++itChan)
					{
						levelIndexes[itChan->first] = itChan->second;
					}
					contourLinesIndexes.push_back(levelIndexes);
				}

				for (auto itLevel = lowerIndexes.cbegin(); itLevel != lowerIndexes.cend(); ++itLevel)
				{
					std::vector<int> levelIndexes(nombreCh, -1);
					const std::map<int, int> & values = itLevel->second;
					for (auto itChan = values.cbegin(); itChan != values.cend(); ++itChan)
					{
						levelIndexes[itChan->first] = itChan->second;
					}
					contourLinesIndexes.push_back(levelIndexes);
				}

				int contourLineIdx = 0;
				for (auto it = contourLinesIndexes.cbegin(); it != contourLinesIndexes.cend(); ++it)
				{
					const std::vector<int> & contourLineIndexes = *it;

					bottomMinIdx = -1;
					bottomMaxIdx = -1;

					for (unsigned int softChan = 0; softChan < nombreCh; ++softChan)
					{
						const int echoRange = contourLineIndexes[softChan];
						bottomRanges[softChan] = echoRange;

						if (echoRange != -1)
						{
							//const std::vector<BottomDetectionContourLine::sContourLine> & contourLinesForChannel = iterLine->second;

							SoftChannel* pChannel = pTrans->getSoftChannelPolarX(softChan);

							const double angle1 = pChannel->m_mainBeamAthwartSteeringAngleRad - pChannel->m_beam3dBWidthAthwartRad *0.5;
							const double angle2 = pChannel->m_mainBeamAthwartSteeringAngleRad + pChannel->m_beam3dBWidthAthwartRad *0.5;

							const BaseMathLib::Vector2D v1 = BaseMathLib::Vector2D(sin(angle1), cos(angle1)) * echoRange;
							const BaseMathLib::Vector2D v2 = BaseMathLib::Vector2D(sin(angle2), cos(angle2)) * echoRange;

							std::uint32_t rangeMin = std::min(v1.y, v2.y);
							std::uint32_t rangeMax = std::max(v1.y, v2.y);

							rangeMin = std::min<std::uint32_t>(echoRange, rangeMin); // -pTrans->GetSampleOffset();
							rangeMax = std::max<std::uint32_t>(echoRange, rangeMax); // -pTrans->GetSampleOffset();

							double bottomMin = rangeMin * Ydim;
							bottomMin /= sizeY;

							double bottomMax = rangeMax * Ydim;
							bottomMax /= sizeY;

							int softChanBottomMinIdx = (int)floor(bottomMin - 1) + offset;
							int softChanBottomMaxIdx = (int)ceil(bottomMax + 1) + offset;

							if (bottomMinIdx == -1) bottomMinIdx = softChanBottomMinIdx;
							else bottomMinIdx = std::min(bottomMinIdx, softChanBottomMinIdx);

							if (bottomMaxIdx == -1) bottomMaxIdx = softChanBottomMaxIdx;
							else bottomMaxIdx = std::max(bottomMaxIdx, softChanBottomMaxIdx);
						}
					}

					if (bottomMinIdx != -1 && bottomMaxIdx != -1)
					{
						int jBottomMax = std::min(jMax, bottomMaxIdx);
						int jBottomMin = std::max(jMin, bottomMinIdx - 1);

						m_contourLines[contourLineIdx][fanIdx - m_StartPixelHRange] = jBottomMax;

						for (int j = jBottomMax; j > jBottomMin; --j)
						{
							const int correctedJ = j - offset;

							const int iMax = std::min(Xdim, maxTabIndex - correctedJ * Xdim);
							const unsigned int i = std::min<unsigned int>(floor(Xdim * this->m_ActiveSlide / 100.0), iMax);
							const int index = *(indexes + i + correctedJ * Xdim);

							if (index >= 0 && index < indexMax)
							{
								const int channelIndex = *(channelIndexes + i + correctedJ * Xdim);
								if (channelIndex >= 0 && channelIndex < indexMax)
								{
									const int echoNum = index%sizeY;
									if (echoNum <= bottomRanges[channelIndex])
									{
										// OTK - 30/03/2010 - on ne trace plus le fond directement sur l'image,
										// mais on sauvegarde sa position pour le tracer par dessus, en GDI+
										//SetPixel(fanIdx,j,blackARGB);
										m_contourLines[contourLineIdx][fanIdx - m_StartPixelHRange] = j;
										break;
									}
								}
							}
						}
					}

					contourLineIdx++;
				}

				// FIN CALCUL DES LIGNES DE CONTOUR
			}
		}
	}
}

void TransducerFlatView::SetActiveSlide(double percentView)
{
	this->m_pLock->Lock();
	if (percentView != m_ActiveSlide)
	{
		this->m_ActiveSlide = percentView;
		m_lastPoint3d = 0;
		m_firstPoint3d = 0;

		ComputeAll();

		//IPSIS - FRE - 20/08/2009 display shoals
		if (DisplayParameter::getInstance()->GetDisplay2DShoals())
		{
			DisplayShoals();
		}
	}
	this->m_pLock->Unlock();
}

bool TransducerFlatView::UpdateBMP(double interBeam, double minYOrigin, double maxYOrigin, unsigned int width)
{
	bool newBmp = false;
	DisplayParameter *pDisplay = DisplayParameter::getInstance();

	double minProf = pDisplay->GetCurrentMinDepth();
	double maxProf = pDisplay->GetCurrentMaxDepth();

	// OTK - 04/11/2009 - correction cas SM20 : la transformmap ne d�marre pas � 0m
	m_StartPixelRange = (unsigned int)floor((minProf - minYOrigin) / interBeam);
	m_StopPixelRange = (unsigned int)floor((maxProf - maxYOrigin) / interBeam);
	unsigned int MyHeight = m_StopPixelRange - m_StartPixelRange;

	// OTK - FAE214 - pour placer pr�cisement les �chos simples sur l'image, on a besoin de la plage de profondeur affich�e
	m_StartPixelDepth = interBeam * m_StartPixelRange + minYOrigin - interBeam / 2.0;
	m_StopPixelDepth = interBeam * m_StopPixelRange + minYOrigin - interBeam / 2.0;

	// OTK - 10/06/2009 - zoom horizontal
	m_StartPixelHRange = (unsigned int)floor(pDisplay->GetMinSideHorizontalRatio()*width);
	m_StopPixelHRange = (unsigned int)floor(pDisplay->GetMaxSideHorizontalRatio()*width);
	unsigned int MyWidth = m_StopPixelHRange - m_StartPixelHRange;

	newBmp = this->AllocateBmp(MyWidth, MyHeight);
	if (newBmp)
	{
		ResetImage(GetBackgroundColor());
	}

	return newBmp;
}

bool TransducerFlatView::AllocateBmp(unsigned int W, unsigned int H)
{
	if (W != 0 && W != GetWidth())
	{
		// allocation de la surface
		delete[] m_surfaceValues;
		m_surfaceValues = new int[W];
		memset(m_surfaceValues, -1, W * sizeof(int));

		// allocation du fond
		delete[] m_bottomValues;
		m_bottomValues = new int[W];
		memset(m_bottomValues, -1, W * sizeof(int));

		// allocation de la port�e de r�f�rence
		delete[] m_refNoiseValues;
		m_refNoiseValues = new int[W];
		memset(m_refNoiseValues, -1, W * sizeof(int));

		// allocation de la port�e calcul�e
		delete[] m_noiseValues;
		m_noiseValues = new int[W];
		memset(m_noiseValues, -1, W * sizeof(int));

		delete[] m_deviationValues;
		m_deviationValues = new int[W];
		memset(m_deviationValues, -1, W * sizeof(int));

		// allocation des lignes de contour
		m_contourLines.clear();

		std::vector<int> defaultLine(W, -1);
		m_contourLines.resize(BottomDetectionContourLine::s_AVAILABLE_CONTOUR_LEVELS.size() * 2 + 1, defaultLine);

		/*
		for (size_t iLine = 0; iLine < BottomDetectionContourLine::s_AVAILABLE_CONTOUR_LEVELS.size() + 1; iLine++)
		{
		if (iLine == BottomDetectionContourLine::s_AVAILABLE_CONTOUR_LEVELS.size())
		{
		// cas particulier de la ligne du Sv Max
		m_contourLines[BottomDetectionContourLine::s_MAX_SV_LEVEL_MAGIC_NUMBER].first.resize(W, -1);
		}
		else
		{
		m_contourLines[BottomDetectionContourLine::s_AVAILABLE_CONTOUR_LEVELS[iLine]].first.resize(W, -1);
		m_contourLines[BottomDetectionContourLine::s_AVAILABLE_CONTOUR_LEVELS[iLine]].second.resize(W, -1);
		}
		}
		*/
	}

	// appel � la m�thode de la classe m�re
	return TransducerView::AllocateBmp(W, H);
}

void TransducerFlatView::ComputeAll()
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	m_lastAddedIdx = -1;
	m_lastDisplayedIdx = -1;
	m_DistanceIndexes.clear();

	if (!this->GetPtr())
	{
		bool newBmp = false;
		TimeObjectContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(m_sounderId);
		PingFan *pFan = (PingFan *)pFanContainer->GetDatedObject(pFanContainer->GetObjectCount() - 1);
		if (pFan)
		{
			double interBeam, minYOrigin, maxYOrigin;
			ComputeTransformMapsProperties(interBeam, minYOrigin, maxYOrigin);
			unsigned int width = pFan->GetFanMemoryRef()->GetMemoryLength();
			newBmp = UpdateBMP(interBeam, minYOrigin, maxYOrigin, width);
		}
		if (!newBmp)
		{
			pKernel->Unlock();
			return;
		}
	}
	else
	{
		// on met par defaut du blanc partout (ou noir si affichage �chos simples)
		int colVal = GetBackgroundColor()->ToArgb();
		memset(GetPtr(), colVal, m_bytesSize * sizeof(int));
	}
	m_RecomputeAll = false;

	TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(this->GetSounderId());
	FanMemory *pFanMemory = pKernel->getObjectMgr()->GetSounderDefinition().GetLinkedFanMemory(m_sounderId);

	if (pCont)
	{
		PingFan *lastFan = (PingFan *)pCont->GetDatedObject(pCont->GetObjectCount() - 1);
		if (lastFan)
		{
			auto pDisplay = DisplayParameter::getInstance();

			double interBeam, minYOrigin, maxYOrigin;
			ComputeTransformMapsProperties(interBeam, minYOrigin, maxYOrigin);
			unsigned int width = pFanMemory->GetMemoryLength();
			this->m_pLock->Lock();
			UpdateBMP(interBeam, minYOrigin, maxYOrigin, width);
			//BlanckImage()
			this->m_pLock->Unlock();
			SounderIndexedTimeContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();
			m_lastPoint3d = 0;
			m_firstPoint3d = 0;


			unsigned int nbPingsToDraw = 0;
			for (size_t w = 0; w < pFanContainer->GetObjectCount(); w++)
			{
				PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);

				if (pFan->getSounderRef()->m_SounderId == m_sounderId)
				{
					nbPingsToDraw++;
				}
			}

			int pictureIndex = std::min(nbPingsToDraw - 1, width - 1);
			m_firstDrawnPingFanID = m_lastDrawnPingFanID;

			pingFanRenderer->update(pDisplay);
			ComputeAll_AddFans(pictureIndex);

			// on repasse en % de la taille de l'image
			m_lastPoint3d = (m_lastPoint3d - m_StartPixelHRange) / (m_StopPixelHRange - m_StartPixelHRange);
			m_firstPoint3d = (m_firstPoint3d - m_StartPixelHRange) / (m_StopPixelHRange - m_StartPixelHRange);
		}
	}

	GetShoalAreaInfo().clear();
	//IPSIS - FRE - 20/08/2009 display shoals
	if (DisplayParameter::getInstance()->GetDisplay2DShoals())
	{
		DisplayShoals();
	}

	pKernel->Unlock();
}

void TransducerFlatView::ComputeAll_AddFans(int pictureIndex)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	SounderIndexedTimeContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();

	auto pDisplay = DisplayParameter::getInstance();
	bool firstDistLine = true;
	int nDistLine = 0;
	int distanceSampling = (int)(pDisplay->GetDistanceSampling()*1000.);
	int delayed3d = pDisplay->GetPingFanDelayed();
	int lenght3d = pDisplay->GetPingFanLength();

	for (int w = pFanContainer->GetObjectCount() - 1; w >= 0 && pictureIndex >= 0; w--)
	{
		PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);

		if (pFan->getSounderRef()->m_SounderId == m_sounderId)
		{
			// Calcul de la grille de distance
			double currentDist = pFan->m_relativePingFan.m_cumulatedDistance / 1.852;
			int pFanDistance = (int)floor(currentDist);
			if (firstDistLine)
			{
				nDistLine = (int)(floor((double)pFanDistance / (double)distanceSampling)*distanceSampling);
				firstDistLine = false;
			}
			if (pFanDistance < nDistLine)
			{
				// on doit tracer une ligne de distance sur ce ping
				if (pictureIndex >= m_StartPixelHRange && pictureIndex < m_StopPixelHRange)
				{
					DistanceGridLine line;
					line.PingIndex = pictureIndex - m_StartPixelHRange;
					line.MilesFraction = nDistLine;
					m_DistanceIndexes.push_back(line);
				}

				// on r�initialise ce bool�en pour la prochaine ligne
				firstDistLine = true;
			}

			// calcul des position de la fourchette des pings 3D sur la vue 2D de profil
			if (w < pFanContainer->GetObjectCount() - delayed3d)
			{
				m_lastPoint3d = std::max<double>(pictureIndex, m_lastPoint3d);

			}
			if (w < pFanContainer->GetObjectCount() - delayed3d - lenght3d)
			{
				m_firstPoint3d = std::max<double>(pictureIndex, m_firstPoint3d);
			}

			AddFan(pFan, pictureIndex);

			m_lastDrawnPingFanID = std::max(m_lastDrawnPingFanID, (int)pFan->GetPingId());
			m_firstDrawnPingFanID = std::min(m_firstDrawnPingFanID, (int)pFan->GetPingId());

			pictureIndex--;
		}
	}
}

// OTK - 26/02/2009 - OPTI - ajout du mode incr�mental
void TransducerFlatView::ComputePartial()
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();

	m_DistanceIndexes.clear();
	TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(this->GetSounderId());
	FanMemory *pFanMemory = pKernel->getObjectMgr()->GetSounderDefinition().GetLinkedFanMemory(m_sounderId);

	if (pCont)
	{
		PingFan *lastFan = (PingFan *)pCont->GetDatedObject(pCont->GetObjectCount() - 1);
		if (lastFan)
		{
			unsigned int width = pFanMemory->GetMemoryLength();
			auto pDisplay = DisplayParameter::getInstance();
			SounderIndexedTimeContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();
			m_lastPoint3d = 0;
			m_firstPoint3d = 0;
			int delayed3d = pDisplay->GetPingFanDelayed();
			int lenght3d = pDisplay->GetPingFanLength();

			int newLastDrawPingFanID = m_lastDrawnPingFanID;
			m_firstDrawnPingFanID = m_lastDrawnPingFanID;
			unsigned int nbPingsToDraw = 0;
			for (size_t w = 0; w < pFanContainer->GetObjectCount(); w++)
			{
				PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);

				if (pFan->getSounderRef()->m_SounderId == m_sounderId)
				{
					nbPingsToDraw++;
					newLastDrawPingFanID = std::max<int>(newLastDrawPingFanID, (int)pFan->GetPingId());
					m_firstDrawnPingFanID = std::min<int>(m_firstDrawnPingFanID, (int)pFan->GetPingId());
				}
			}

			bool firstDistLine = true;
			int nDistLine = 0;
			int distanceSampling = (int)(pDisplay->GetDistanceSampling()*1000.);

			int pictureIndex = std::min<int>(nbPingsToDraw - 1, width - 1);
			int nbPingsToAdd = 0;
			for (int w = pFanContainer->GetObjectCount() - 1; w >= 0 && pictureIndex >= 0; w--)
			{
				PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);

				if (pFan->getSounderRef()->m_SounderId == m_sounderId)
				{
					// Calcul de la grille de distance
					double currentDist = pFan->m_relativePingFan.m_cumulatedDistance / 1.852;
					int pFanDistance = (int)floor(currentDist);
					if (firstDistLine)
					{
						nDistLine = (int)(floor((double)pFanDistance / (double)distanceSampling)*distanceSampling);
						firstDistLine = false;
					}
					if (pFanDistance < nDistLine)
					{
						// on doit tracer une ligne de distance sur ce ping
						if (pictureIndex >= m_StartPixelHRange && pictureIndex < m_StopPixelHRange)
						{
							DistanceGridLine line;
							line.PingIndex = pictureIndex - m_StartPixelHRange;
							line.MilesFraction = nDistLine;
							m_DistanceIndexes.push_back(line);
						}

						// on r�initialise ce bool�en pour la prochaine ligne
						firstDistLine = true;
					}


					// calcul des position de la fourchette des pings 3D sur la vue 2D de profil
					if (w < pFanContainer->GetObjectCount() - delayed3d)
					{
						m_lastPoint3d = std::max<double>(pictureIndex, m_lastPoint3d);
					}
					if (w < pFanContainer->GetObjectCount() - delayed3d - lenght3d)
					{
						m_firstPoint3d = std::max<double>(pictureIndex, m_firstPoint3d);
					}

					pictureIndex--;

					if (pFan->GetPingId() > m_lastDrawnPingFanID)
					{
						nbPingsToAdd++;
					}
				}
			}

			int nbPingsToRemove = std::max<int>(0, m_lastAddedIdx + nbPingsToAdd + 1 - nbPingsToDraw);
			if (nbPingsToRemove > 0)
			{
				m_lastDisplayedIdx -= nbPingsToRemove;
				m_lastAddedIdx -= nbPingsToRemove;
			}

			nbPingsToRemove += std::max<int>(0, 1 + m_lastAddedIdx + nbPingsToAdd - width);
			if (nbPingsToRemove > 0)
			{
				Shift(nbPingsToRemove);
				m_lastDisplayedIdx -= nbPingsToRemove;
				m_lastAddedIdx -= nbPingsToRemove;
			}

			// ajout des nouveaux pingFans : entre m_lastDisplayedIdx et m_StopPixelHRange
			pictureIndex = std::min<int>(nbPingsToDraw - 1, width - 1);

			pingFanRenderer->update(pDisplay);
			ComputePartial_AddFans(pictureIndex);

			m_lastDrawnPingFanID = newLastDrawPingFanID;

			// on repasse en % de la taille de l'image
			m_lastPoint3d = (m_lastPoint3d - m_StartPixelHRange) / (m_StopPixelHRange - m_StartPixelHRange);
			m_firstPoint3d = (m_firstPoint3d - m_StartPixelHRange) / (m_StopPixelHRange - m_StartPixelHRange);

		}
	}

	pKernel->Unlock();
}

void TransducerFlatView::ComputePartial_AddFans(int pictureIndex)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	SounderIndexedTimeContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();
	int tempLastDisplayedIdx = m_lastDisplayedIdx;
	for (int w = pFanContainer->GetObjectCount() - 1; w >= 0 && pictureIndex >= 0; w--)
	{
		PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);

		if (pFan->getSounderRef()->m_SounderId == m_sounderId)
		{
			if (pictureIndex < m_StopPixelHRange && pictureIndex > tempLastDisplayedIdx)
			{
				AddFan(pFan, pictureIndex);
			}
			m_lastAddedIdx = std::max(m_lastAddedIdx, pictureIndex);
			pictureIndex--;
		}
	}
}

// r�cup�ration des donn�es de l'echo point� et mise en forme du texte de la tooltip
System::String^ TransducerFlatView::FormatEchoData(unsigned int pingFanIndex, double posY)
{
	System::String^ result = "";
	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();

	SounderIndexedTimeContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();

	if (pFanContainer != NULL)
	{
		int pictureIndex = 0;
		PingFan *pFan = 0;
		for (int w = pFanContainer->GetObjectCount() - 1; w >= 0 && !pFan; w--)
		{
			PingFan *p = (PingFan*)pFanContainer->GetObjectWithIndex(w);

			if (p != NULL)
			{
				if (p->getSounderRef()->m_SounderId == m_sounderId)
				{
					if (pictureIndex == (m_lastAddedIdx - pingFanIndex))
					{
						pFan = p;
					}
					pictureIndex++;
				}
			}
		}

		if (pFan)
		{
			// On r�cup�re le num�ro du pixel en fonction de posX et posY
			TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(m_transducerIndex);

			if (pTransform != NULL)
			{
				Vector2I memSize = pTransform->getCartesianSize2();

				int pixelY = (int)floor((m_StopPixelRange - m_StartPixelRange)*posY + m_StartPixelRange);
				int pixelX = (unsigned int)floor(memSize.x*this->m_ActiveSlide / 100.0);

				result = FormatEchoToolTip(pFan, pixelX, pixelY);
			}
		}
	}

	pKernel->Unlock();

	return result;
}

// M�thode permettant de faire le lien entre les ID des pings et leur position dans l'image
void TransducerFlatView::GetImagePixelsFromPingIDs(const std::vector<std::uint64_t>& pingIDs, std::vector<unsigned int>& result)
{
	size_t pingIDsCount = pingIDs.size();
	assert(pingIDsCount);

	result.clear();

	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	SounderIndexedTimeContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();

	int nbPingsDraw = 0;
	for (size_t w = 0; w < pFanContainer->GetObjectCount(); w++)
	{
		PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);

		if (pFan->getSounderRef()->m_SounderId == m_sounderId)
		{
			nbPingsDraw++;
		}
	}
	int delta = std::max<int>(0, nbPingsDraw - M3DKernel::GetInstance()->GetRefKernelParameter().getNbPingFanMax());

	// on parcourt le conteneur des pings en notant pour chaque pingID le bon pixel
	size_t pingIndex = 0;
	std::uint64_t pingID = pingIDs[pingIndex];
	int pictureIndex = -1;
	if (pFanContainer != NULL)
	{
		size_t pFanContainerSize = pFanContainer->GetObjectCount();
		for (size_t w = 0; w < pFanContainerSize && pingIndex < pingIDsCount; w++)
		{
			PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);
			if (pFan != NULL)
			{
				if (pFan->getSounderRef()->m_SounderId == m_sounderId)
				{
					if (delta > 0)
					{
						delta--;
					}
					else
					{
						pictureIndex++;
					}
				}
				if (pFan->GetPingId() >= pingID)
				{
					if (pictureIndex == -1)
					{
						// si on n'a encore mis aucun ping du sondeur selectionn�, on trace tout de m�me sur le premier pixel
						result.push_back(0);
					}
					else
					{
						// FAE 189 - correction d'un probl�me de positionnement des lignes de d�finition des ESU.
						// Dans ce cas, le ping � afficher est r�ellement affich� : on peut utiliser directement pictureIndex
						if (pFan->getSounderRef()->m_SounderId == m_sounderId)
						{
							result.push_back(pictureIndex);
						}
						else
						{
							// Dans ce cas, le ping � afficher ne fait pas partie de l'image. On va donc positionner la ligne sur le ping suivant,
							// afin que la ligne apparaissent entre les deux pings affich�s qui entourent le ping recherch�
							result.push_back(pictureIndex + 1);
						}
					}
					pingIndex++;
					if (pingIndex < pingIDsCount)
					{
						pingID = pingIDs[pingIndex];
						while (pFan->GetPingId() >= pingID && pingIndex < pingIDsCount)
						{
							pingIndex++;
							if (pingIndex < pingIDsCount)
							{
								pingID = pingIDs[pingIndex];
							}
						}
					}
				}
			}
		}
	}
	pKernel->Unlock();
}


// M�thode permettant de faire le lien entre les ID des pings et leur position dans le composant d'affichage
void TransducerFlatView::GetScreenPixelsFromPingIDs(const std::vector<std::uint64_t>& pingIDs,
	std::vector<unsigned int>& result)
{
	//calcul dans le repere image
	GetImagePixelsFromPingIDs(pingIDs, result);

	//d�calage pour le repere affichage
	std::vector<unsigned int>::iterator iterPixels = result.begin();
	unsigned int pictureIndex = 0;
	std::vector<unsigned int> tmpRes;
	while (iterPixels != result.end())
	{
		pictureIndex = *iterPixels;
		//test de validit�
		if (pictureIndex >= m_StartPixelHRange && pictureIndex <= m_StopPixelHRange)
		{
			//d�calage
			tmpRes.push_back(pictureIndex - m_StartPixelHRange);
		}
		iterPixels++;
	}
	result = tmpRes;
}

//*****************************************************************************
// Name : DisplayShoals
// Description : displays the shoals
// Parameters : 
// Return : void
//*****************************************************************************
void TransducerFlatView::DisplayShoals()
{
	double factor = (double)(m_lastDrawnPingFanID - m_firstDrawnPingFanID) / (double)m_lastAddedIdx;
	int minPing = m_firstDrawnPingFanID + m_StartPixelHRange * factor;
	int maxPing = minPing + (m_StopPixelHRange - m_StartPixelHRange)* factor;

	//get shoals overlapping displayed ping range
	std::vector<ShoalData*> shoalList = GetFilteredShoals(minPing, maxPing);
	std::vector<ShoalData*>::iterator iterShoal = shoalList.begin();
	std::map<std::uint64_t, ShoalAreaInfo>::iterator iterShoalAreaInfo;

	while (iterShoal != shoalList.end())
	{
		ShoalData *pShoal = *iterShoal;
		std::uint64_t shoalId = pShoal->GetExternId();

		//need to display the shoal ?
		bool displayShoal = false;

		//display the shoal if the shoal is not already displayed
		iterShoalAreaInfo = GetShoalAreaInfo().find(shoalId);
		displayShoal = iterShoalAreaInfo == GetShoalAreaInfo().end();

		//or if the selected state of the shoal has changed
		if (!displayShoal)
		{
			displayShoal = iterShoalAreaInfo->second.selected != pShoal->GetSelected();
		}
		//or if the shoal was not entirely displayed
		if (!displayShoal)
		{
			int maxShoalPing = pShoal->GetPings()[pShoal->GetPings().size() - 1]->GetPingId();
			displayShoal = maxShoalPing >= maxPing;
		}

		if (displayShoal)
		{
			DisplayShoal(pShoal);
		}
		iterShoal++;
	}
}

//*****************************************************************************
// Name : DisplayShoal
// Description : displays the shoal
// Parameters : (in)	ShoalData* pShoal
// Return : void
//*****************************************************************************
void TransducerFlatView::DisplayShoal(ShoalData* pShoal)
{
	M3DKernel::GetInstance()->Lock();
	DisplayParameter *pDisplay = DisplayParameter::getInstance();

	// 1 - compute the hull for the shoal
	if (pShoal != NULL)
	{
		std::uint64_t shoalId = pShoal->GetExternId();

		//the hull points for each ping
		std::vector<std::list<CoordY> > pingHullPoints;

		//for each ping
		std::vector<PingData*>::iterator iterPings = pShoal->GetPings().begin();
		while (iterPings != pShoal->GetPings().end())
		{
			PingData* pPingData = *iterPings;

			if (pPingData != NULL)
			{
				//does the shoal intersect the transversal cut plan ?
				bool shoalIntersect = false;
				std::list<CoordY> shoalPingPoints;

				if (pDisplay->GetCurrentMinDepth() < pShoal->GetShoalStat()->GetMaxDepth() &&
					pDisplay->GetCurrentMaxDepth() > pShoal->GetShoalStat()->GetMinDepth())
				{
					//compute the depth offset
					double depthOffset = 0;
					Sounder* pSounder = pPingData->GetPingFan()->getSounderRef();
					Transducer *pTrans = pSounder->GetTransducer(m_transducerIndex);
					if (pTrans != NULL)
					{
						SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
						if (pSoftChan)
						{
							depthOffset += pPingData->GetPingFan()->getHeaveChan(pSoftChan->m_softChannelId);
						}
						depthOffset += pTrans->m_transDepthMeter;	// transducer depth en cm				

						std::vector<HullItem*>::iterator iter = pPingData->GetShoalStat()->GetHullList().begin();
						TransformMap *pTransform = pSounder->GetTransformSetRef()->GetTransformMap(m_transducerIndex);

						while (iter != pPingData->GetShoalStat()->GetHullList().end())
						{
							//compute the 2D hull and update the points for the ping
							shoalIntersect |= ComputeShoalHull(*iter, pTransform, depthOffset, shoalPingPoints);
							iter++;
						}

						//sort the points by value
						shoalPingPoints.sort();
					}
				}

				//if the shoal intersects the transversal cut plan
				if (shoalIntersect)
				{
					//update shoal area info
					ShoalAreaInfo areaInfo = GetShoalAreaInfo()[shoalId];
					areaInfo.pingMin = std::min<int>(pPingData->GetPingId(), areaInfo.pingMin);
					areaInfo.pingMax = std::max<int>(pPingData->GetPingId(), areaInfo.pingMax);
					areaInfo.depthMin = std::min<double>((*shoalPingPoints.begin()).depth, areaInfo.depthMin);
					areaInfo.depthMax = std::max<double>((*shoalPingPoints.rbegin()).depth, areaInfo.depthMax);
					GetShoalAreaInfo()[shoalId] = areaInfo;
				}

				//store the hull points for the ping
				pingHullPoints.push_back(shoalPingPoints);
			}

			iterPings++;
		}

		// 2 - FillShoals with transparency factor, depending on select attribute
		if (GetShoalAreaInfo()[shoalId].selected != pShoal->GetSelected())
		{
			FillShoal(pShoal, pingHullPoints, 100, !pShoal->GetSelected());
			GetShoalAreaInfo()[shoalId].selected = pShoal->GetSelected();
		}

		// 3 - render the hull edges on each ping
		RenderShoalHull(pShoal, pingHullPoints);
	}

	M3DKernel::GetInstance()->Unlock();
}

//*****************************************************************************
// Name : ComputeShoalHull
// Description : compute the points of the shoal hull intersecting the transversal plan
// Parameters : (in)	HullItem* pHullItem
//				(in)	TransformMap *pTransform
//				(in)	double depthOffset
//				(out) 	std::list<CoordY> the list of y coord for intersection with transversal plan
// Return : true if shoalPoints.size() > 0
//*****************************************************************************
bool TransducerFlatView::ComputeShoalHull(HullItem* pHullItem,
	TransformMap *pTransform,
	double depthOffset,
	std::list<CoordY>& shoalPoints)
{
	Poly2D& poly = pHullItem->surfOrtho;
	Vector2I memSize = pTransform->getCartesianSize2();

	//compute the cartesian 2D X coord
	int cartesianX = (unsigned int)floor(memSize.x*m_ActiveSlide / 100.0);

	//compute the real 2D X coord
	double realX = pTransform->cartesianToReal2(Vector2D(cartesianX, memSize.y - 1)).x;

	//determine if possible the min and max y for intersection between hull and X-axis value
	int nbPoints = poly.GetNbPoints();

	// NMD - FAE 116 
	// Bug de calcul des points du banc intersectant le plan transversal :
	// Le nombre de points retourn�s doit �tre pair (pour tracer ensuite des lignes par couple de points)
	// Or certains points sont en double dans la liste de points initiaux 
	// et certains points peuvent aussi �tre positionn�s exactement sur le plan transversal
	// -> on ajoute le booleen bSame pour verifier si le point pr�c�dent ajout� est un point double sur le plan transversal
	bool bSame = false;
	for (int i = 0; i < nbPoints; i++)
	{
		BaseMathLib::Vector2D pointI(*poly.GetPointX(i), *poly.GetPointY(i) + depthOffset);
		BaseMathLib::Vector2D pointJ(*poly.GetPointX((i + 1) % nbPoints), *poly.GetPointY((i + 1) % nbPoints) + depthOffset);

		bool bAdd = false;

		//if segment crosses the x-axis value
		if ((pointI.x < realX && pointJ.x > realX) || (pointJ.x < realX && pointI.x > realX))
		{
			bAdd = true;
			bSame = false;
		}
		else if (!bSame && (pointI.x == realX && pointJ.x == realX))
		{
			bAdd = true;
			bSame = true;
		}
		else
		{
			bSame = false;
		}

		if (bAdd)
		{
			//compute the real depth value for intersection
			double depth = pointI.y;
			if ((pointJ.x - pointI.x) != 0)
			{
				depth = pointI.y + (realX - pointI.x) * (pointJ.y - pointI.y) / (pointJ.x - pointI.x);
			}

			//convert to cartesian
			int y = pTransform->realToCartesian2(Vector2D(realX, depth)).y;

			//add the y coord to the list of point for the shoal
			shoalPoints.push_back(CoordY(y, depth));
		}
	}

	return !shoalPoints.empty();
}

//*****************************************************************************
// Name : GetShoalPictureRange
// Description : get the picture X range for the shoal
// Parameters : (in)	ShoalData* pShoal
//				(in)	int& minXPixel
//              (in)	int& maxXPixel
// Return : void
//*****************************************************************************
void TransducerFlatView::GetShoalPictureRange(ShoalData* pShoal, int& minXPixel, int& maxXPixel)
{
	int nbPing = pShoal->GetPings().size();

	if (nbPing > 0)
	{
		std::vector<std::uint64_t> pingIds;
		std::vector<unsigned int> pictureIndexes;
		int minPing = pShoal->GetPings()[0]->GetPingId();
		int maxPing = pShoal->GetPings()[nbPing - 1]->GetPingId();

		pingIds.push_back(minPing);
		pingIds.push_back(maxPing);
		GetImagePixelsFromPingIDs(pingIds, pictureIndexes);

		if (pictureIndexes.size() > 1)
		{
			minXPixel = pictureIndexes[0];
			maxXPixel = pictureIndexes[1];
		}
	}
}

//*****************************************************************************
// Name : RenderShoalHull
// Description : display the shoal Hull
// Parameters : (in)	ShoalData* pShoal
//				(in)	std::vector<std::list<CoordY> >& pingPoints
// Return : void
//*****************************************************************************
void TransducerFlatView::RenderShoalHull(ShoalData* pShoal, std::vector<std::list<CoordY> >& pingPoints)
{
	System::Drawing::Color color = ColorList::GetInstance()->GetColor(pShoal->GetExternId());

	//render each shoal ping hull in regards to the previous ping hull
	std::vector<std::list<CoordY> >::iterator iterPings = pingPoints.begin();
	std::list<CoordY> previousPingPoints;
	std::list<CoordY> currentPingPoints;

	int minPictureIndex = 0;
	int maxPictureIndex = 0;
	GetShoalPictureRange(pShoal, minPictureIndex, maxPictureIndex);

	if (maxPictureIndex > 0)
	{
		unsigned int pictureIndex = minPictureIndex;
		while (iterPings != pingPoints.end() && pictureIndex >= 0)
		{
			currentPingPoints = *iterPings;

			pictureIndex = std::max<unsigned int>(0, pictureIndex);
			RenderShoalPingHull(currentPingPoints, previousPingPoints, pictureIndex, color);

			previousPingPoints = currentPingPoints;

			pictureIndex++;
			iterPings++;
		}
		currentPingPoints.clear();
		RenderShoalPingHull(previousPingPoints, currentPingPoints, maxPictureIndex, color);
	}
}

//*****************************************************************************
// Name : RenderShoalPingHull
// Description : display the shoal Hull for the given ping
// Parameters : (in)	const std::list<CoordY>& ref_currentPingPoints
//				(in)	const std::list<CoordY>& ref_previousPingPoints
//				(in)	int pictureIndex
//				(in)	System::Drawing::Color color
// Return : void
//*****************************************************************************
void TransducerFlatView::RenderShoalPingHull(const std::list<CoordY>& ref_currentPingPoints,
	const std::list<CoordY>& ref_previousPingPoints,
	int pictureIndex,
	System::Drawing::Color color)
{
	std::list<CoordY> currentPingPoints = ref_currentPingPoints;
	std::list<CoordY> previousPingPoints = ref_previousPingPoints;

	//reduce the nb of couple
	ReduceCouples(pictureIndex, color, previousPingPoints, currentPingPoints);

	//for each couple of points in previous ping
	std::list<CoordY>::iterator iterPointPreviousPing = previousPingPoints.begin();
	std::list<CoordY>::iterator iterPointCurrentPing = currentPingPoints.begin();
	std::list<CoordY>::iterator iterPointPrevious1 = iterPointPreviousPing;
	std::list<CoordY>::iterator iterPointPrevious2 = iterPointCurrentPing;

	while (iterPointPreviousPing != previousPingPoints.end())
	{
		iterPointPrevious1 = iterPointPreviousPing;

		int y1PreviousPing = (*iterPointPreviousPing++).cartesianY;
		int y2PreviousPing = (*iterPointPreviousPing++).cartesianY;
		int y1CurrentPing = y1PreviousPing;
		int y2CurrentPing = y2PreviousPing;

		//if a couple of point in the current list matches the range
		bool matchRange = false;
		while (iterPointCurrentPing != currentPingPoints.end() && !matchRange &&
			y1PreviousPing <= y2CurrentPing)
		{
			iterPointPrevious2 = iterPointCurrentPing;
			y1CurrentPing = (*iterPointCurrentPing++).cartesianY;
			y2CurrentPing = (*iterPointCurrentPing++).cartesianY;

			matchRange = (y1PreviousPing <= y2CurrentPing && y2PreviousPing >= y1CurrentPing);
		}

		if (matchRange)
		{
			//draw the edge always inside the shoal
			int pictureIndexUp = (y1CurrentPing >= y1PreviousPing) ? pictureIndex - 1 : pictureIndex;
			int pictureIndexBottom = (y2CurrentPing >= y2PreviousPing) ? pictureIndex : pictureIndex - 1;

			//draw the segment linking the two couples of points
			DrawLine(pictureIndexUp, y1PreviousPing, pictureIndexUp, y1CurrentPing, color);
			DrawLine(pictureIndexBottom, y2PreviousPing, pictureIndexBottom, y2CurrentPing, color);
			m_ImageChanged = true;
			SetPixel(pictureIndex, y1CurrentPing, color);
			SetPixel(pictureIndex, y2CurrentPing, color);

			//remove drawn points from list
			previousPingPoints.erase(iterPointPrevious1, iterPointPreviousPing);
			currentPingPoints.erase(iterPointPrevious2, iterPointCurrentPing);
		}
	}//while(iterPointPrevious != previousPoints.end())

	 //draw remaining points in previous and current list of couple of points
	DrawShoalEnding(previousPingPoints, pictureIndex - 1, color);
	DrawShoalEnding(currentPingPoints, pictureIndex, color);
}


//*****************************************************************************
// Name : ReduceCouples
// Description : reduces the list of points matching the depth range into a single couple
//		and draw the extra couples of points
// Parameters : (in)	int pictureIndex
//				(in)	System::Drawing::Color color
//				(in)	std::list<CoordY>& refPrevious
//				(in)	std::list<CoordY>& refCurrent
// Return : void
//*****************************************************************************
void TransducerFlatView::ReduceCouples(int pictureIndex,
	System::Drawing::Color color,
	std::list<CoordY>& refPrevious,
	std::list<CoordY>& refCurrent)
{
	std::list<int> res;
	bool process = refPrevious.size() > 2 || refCurrent.size() > 2;
	//while there are couples to reduce
	while (process)
	{
		int size2Before = refCurrent.size();
		if (size2Before > 2)
		{
			//draw the edge always inside the shoal
			ReduceCouples(refPrevious, pictureIndex, pictureIndex - 1, color, refCurrent);
		}
		int size2After = refCurrent.size();

		int size1Before = refPrevious.size();
		if (size1Before > 2)
		{
			//draw the edge always inside the shoal
			ReduceCouples(refCurrent, pictureIndex - 1, pictureIndex, color, refPrevious);
		}
		int size1After = refPrevious.size();

		process = (size2Before != size2After && size2After > 2) ||
			(size1Before != size1After && size1After > 2);
	}
}

//*****************************************************************************
// Name : ReduceCouples
// Description : reduces the list of points matching the depth range into a single couple
//		and draw the extra couples of points
// Parameters : (in)	const std::list<CoordY>& ref
//				(in)	int pictureIndex : index of the picture for the toReduce data
//				(in)	int pictureIndexInside : index of the picture inside the shoal
//				(in)	System::Drawing::Color color
//				(in)	std::list<CoordY>& toReduce	
// Return : void
//*****************************************************************************
void TransducerFlatView::ReduceCouples(const std::list<CoordY>& ref,
	int pictureIndex,
	int pictureIndexInside,
	System::Drawing::Color color,
	std::list<CoordY>& toReduce)
{
	//if a couple of point in the current list matches the range
	bool matchRange = false;

	CoordY yMin(0, 0);
	CoordY yMax(0, 0);

	assert(ref.size() % 2 == 0);
	assert(toReduce.size() % 2 == 0);

	if (toReduce.size() > 2 && ref.size() > 0)
	{
		std::list<CoordY>::const_iterator iterPointRef = ref.begin();
		while (iterPointRef != ref.end())
		{
			int refMin = (*iterPointRef++).cartesianY;
			int refMax = (*iterPointRef++).cartesianY;

			if (toReduce.size() > 2)
			{
				std::list<CoordY> res;
				bool hasMatchingCouples = false;
				std::list<CoordY>::iterator iterPointToReduce = toReduce.begin();
				while (iterPointToReduce != toReduce.end())
				{
					yMin = (*iterPointToReduce++);
					yMax = (*iterPointToReduce++);

					matchRange = (refMin <= yMax.cartesianY && refMax >= yMin.cartesianY);

					//the extra couples are drawn and deleted from toReduce
					if (matchRange && hasMatchingCouples)
					{
						//draw intra couple
						DrawLine(pictureIndexInside, (*res.rbegin()).cartesianY, pictureIndexInside, yMin.cartesianY, color);
						//draw edge pixels
						m_ImageChanged = true;
						SetPixel(pictureIndex, (*res.rbegin()).cartesianY, color);
						SetPixel(pictureIndex, yMin.cartesianY, color);

						//modify max depth
						*res.rbegin() = yMax;
					}
					else
					{
						res.push_back(yMin);
						res.push_back(yMax);
					}
					hasMatchingCouples |= matchRange;
				}
				toReduce = res;
			}
		}
	}
}

//*****************************************************************************
// Name : FillShoal
// Description : fill the shoal
// Parameters : (in)	ShoalData* pShoal,
//				(in)	std::vector<std::list<CoordY> >& pingPoints
//				(in)	int transparency (0 -> 255, 0 : transparent)
// Return : void
//*****************************************************************************
void TransducerFlatView::FillShoal(ShoalData* pShoal,
	std::vector<std::list<CoordY> >& pingPoints,
	int transparency,
	bool erase)
{
	assert(transparency >= 0 && transparency <= 255);

	System::Drawing::Color color = System::Drawing::Color::FromArgb(transparency,
		ColorList::GetInstance()->GetColor(pShoal->GetExternId()));

	int minPictureIndex = 0;
	int maxPictureIndex = 0;
	GetShoalPictureRange(pShoal, minPictureIndex, maxPictureIndex);

	if (maxPictureIndex > 0)
	{
		unsigned int pictureIndex = minPictureIndex;

		//for each ping
		std::vector<std::list<CoordY> >::iterator iterPingsPoints = pingPoints.begin();
		while (iterPingsPoints != pingPoints.end())
		{
			DrawShoalEnding(*iterPingsPoints, pictureIndex, color, erase);

			iterPingsPoints++;
			pictureIndex++;
		}
	}
}

//*****************************************************************************
// Name : DrawShoalEnding
// Description : display the segment ending the shoals
//	a line is drawn between each couple of point for a shoal
// Parameters : (in)	std::list<int>& shoalPoints
//				(in)	int pictureIndex
//				(in)	System::Drawing::Color& color 
// Return : void
//*****************************************************************************
void TransducerFlatView::DrawShoalEnding(std::list<CoordY>& shoalPoints,
	int pictureIndex,
	System::Drawing::Color& color,
	bool erase)
{
	if (shoalPoints.size() > 0)
	{
		//sort the points by value
		shoalPoints.sort();

		//for each couple of points in previous shoal
		std::list<CoordY>::iterator iterPoints = shoalPoints.begin();

		//finish the drawing of the remaining couple of points
		while (iterPoints != shoalPoints.end())
		{
			int y1 = (*iterPoints++).cartesianY;
			int y2 = (*iterPoints++).cartesianY;

			//draw the segment linking the two current points
			DrawLine(pictureIndex, y1, pictureIndex, y2, color, erase);
		}
	}
}

//*****************************************************************************
// Name : RefreshShoal
// Description : refresh the shoal
// Parameters : (in)	shoalextraction::ShoalData* pShoal
// Return : void
//*****************************************************************************
void TransducerFlatView::RefreshShoal(shoalextraction::ShoalData* pShoal)
{
	this->m_pLock->Lock();

	if (pShoal->GetSounderId() == GetSounderId() &&
		pShoal->GetTransId() == m_transducerIndex)
	{
		DisplayShoal(pShoal);
	}

	this->m_pLock->Unlock();
}

// OTK - FAE065 - plusieurs transformMap pour une m�me image
void TransducerFlatView::ComputeTransformMapsProperties(double& interBeam,
	double& minYOrigin,
	double& maxYOrigin)
{
	// on boucle sur l'ensemble des transformMaps correspondantes
	std::vector<Sounder*> sounders = M3DKernel::GetInstance()->getObjectMgr()->GetSounderDefinition().GetSoundersWithId(m_sounderId);
	size_t nbSounders = sounders.size();
	for (size_t sounderIdx = 0; sounderIdx < nbSounders; sounderIdx++)
	{
		TransformMap* pMap = sounders[sounderIdx]->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
		// l'interbeam est par construction identique pour toutes les transformMaps (soit fix�, soit auto, et le calcul auto fait en sorte que 
		// cela soit vrai)
		interBeam = pMap->getYSpacing();
		double y = pMap->getRealOrigin2().y;
		if (sounderIdx == 0)
		{
			minYOrigin = y;
			maxYOrigin = y;
		}
		else
		{
			minYOrigin = std::min<double>(minYOrigin, y);
			maxYOrigin = std::max<double>(maxYOrigin, y);
		}
	}
}