#pragma once

#include "TransducerView.h"

#include <vector>
#include <list>
#include <float.h>

namespace shoalextraction
{
	struct HullItem;
}

public struct DistanceGridLine
{
	int PingIndex;
	int MilesFraction;
};

public struct CoordY
{
	int cartesianY;
	double depth;

	CoordY(int _cartesianY, double _depth) : cartesianY(_cartesianY), depth(_depth) {}
	inline bool operator<(const CoordY& other) const { return cartesianY < other.cartesianY; }
};

public struct ShoalAreaInfo
{
	int pingMin;
	int pingMax;
	double depthMin;
	double depthMax;
	bool selected;

	ShoalAreaInfo()
	{
		pingMin = std::numeric_limits<int>::max();
		pingMax = 0;
		depthMin = std::numeric_limits<double>::max();
		depthMax = 0;
		selected = false;
	}
};

class TransformMap;
class ColorPaletteEcho;

class TransducerFlatView : public TransducerView
{
public:
	TransducerFlatView(std::uint32_t sounderId, unsigned int transducerIndex, DisplayDataType displayDataType);
	TransducerFlatView(std::uint32_t sounderId, unsigned int transducerIndex, unsigned int transducerIndex2, unsigned int transducerIndex3
		, const MultifrequencyEchogram& coloredEchogram); ///<For colored echogram

	virtual ~TransducerFlatView(void);

	virtual void PingFanAdded(int width, int height);
	virtual void EsuClosed();

	void SetActiveSlide(double percentView);
	double GetActiveSlide() { return m_ActiveSlide; }
	virtual bool GetImageChanged() override {
		if (m_pLock != NULL)
		{
			m_pLock->Lock();
			if (m_RecomputeAll)
				ComputeAll();
			m_pLock->Unlock();
		}
		return m_ImageChanged;
	}
	int  GetLastAddedIndex() { return m_lastAddedIdx; }
	int  GetLastDisplayedIndex() { return m_lastDisplayedIdx; }
	void GetImagePixelsFromPingIDs(const std::vector<std::uint64_t>& pingIDs, std::vector<unsigned int>& result);
	void GetScreenPixelsFromPingIDs(const std::vector<std::uint64_t>& pingIDs, std::vector<unsigned int>& result);
	std::vector<DistanceGridLine> GetDistanceIndexes() { return m_DistanceIndexes; }
	double GetLastPoint3DAdded() { return m_lastPoint3d; }
	double GetFirstPoint3DAdded() { return m_firstPoint3d; }
	virtual System::String^ FormatEchoData(unsigned int pingFanIdx, double posY);

	//liste des info d'affichage des bancs(utile pour afficher le num�ro de banc dans BaseFlatView)
	std::map<std::uint64_t, ShoalAreaInfo>& GetShoalAreaInfo() { return m_shoalAreaInfo; };

	// refresh the shoal
	void RefreshShoal(shoalextraction::ShoalData* pShoal);
	virtual	void ComputeAll() override;

	inline int GetSurfaceValue(int index) { return m_surfaceValues[index]; }
	inline int* GetSurfaceValues() { return m_surfaceValues; }

	inline int GetBottomValue(int index) { return m_bottomValues[index]; }
	inline int* GetBottomValues() { return m_bottomValues; }
	inline int* GetDeviationValues() { return m_deviationValues; }

	inline int GetNoiseValue(int index) { return m_noiseValues[index]; }
	inline int* GetNoiseValues() { return m_noiseValues; }
	inline int* GetRefNoiseValues() { return m_refNoiseValues; }

	inline const std::vector< std::vector<int> > & GetContourLines() { return m_contourLines; }

protected:

	// display the shoals
	void DisplayShoals();

	// display the shoal
	void DisplayShoal(shoalextraction::ShoalData* pShoal);

	//fill the shoal
	void FillShoal(shoalextraction::ShoalData* pShoal,
		std::vector<std::list<CoordY> >& pingPoints,
		int transparency,
		bool erase);


	// compute the points of the shoal hull intersecting the transversal plan
	bool ComputeShoalHull(shoalextraction::HullItem* pHullItem,
		TransformMap *pTransform,
		double depthOffset,
		std::list<CoordY>& shoalPoints);

	//Render the shoal Hulls
	void RenderShoalHull(shoalextraction::ShoalData* pShoal, std::vector<std::list<CoordY> >& pingPoints);

	//Render the shoal Hull for the given ping
	void RenderShoalPingHull(const std::list<CoordY>& currentPingPoints,
		const std::list<CoordY>& previousPingPoints,
		int pictureIndex,
		System::Drawing::Color color);

	void ReduceCouples(int pictureIndex,
		System::Drawing::Color color,
		std::list<CoordY>& ref1,
		std::list<CoordY>& ref2);
	void ReduceCouples(const std::list<CoordY>& ref,
		int pictureIndex,
		int pictureIndexInside,
		System::Drawing::Color color,
		std::list<CoordY>& toReduce);

	//display the segment ending the shoals
	//(a line is drawn between each couple of points)
	void DrawShoalEnding(std::list<CoordY>& shoalPoints,
		int pictureIndex,
		System::Drawing::Color& color,
		bool erase = false);

	//get the picture X range for the shoal
	void GetShoalPictureRange(shoalextraction::ShoalData* pShoal, int& minXPixel, int& maxXPixel);

	// OTK - FAE065 - plusieurs transformmaps pour une m�me image
	void ComputeTransformMapsProperties(double& interBeam, double& minYOrigin, double& maxYOrigin);

	bool UpdateBMP(double interBeam, double minYOrigin, double maxYOrigin, unsigned int width);
	virtual bool AllocateBmp(unsigned int W, unsigned int H);
	virtual	void ComputePartial();
	void Shift(int n);

	void AddFan(PingFan *pFan, int fanIdx);

	void ComputeAll_AddFans(int pictureIndex);

	void ComputePartial_AddFans(int pictureIndex);

private:
	void initValues();

	int		m_lastAddedIdx;
	int		m_lastDisplayedIdx; // pour gestion du zoom
	double	m_ActiveSlide;
	bool	m_RecomputeAll;
	double m_lastPoint3d;
	double m_firstPoint3d;
	int m_firstDrawnPingFanID;
	int m_lastDrawnPingFanID;

	// ligne de surface
	int * m_surfaceValues;

	// ligne de fond
	int * m_bottomValues;

	// noise level range
	int * m_refNoiseValues;
	int * m_noiseValues;

	// deviation
	int * m_deviationValues;

	// lignes de contour, index�es par niveau.
	std::vector< std::vector<int> > m_contourLines;

	// indices des pings pour lesquels on franchi une unit� de distance pour la grille
	std::vector<DistanceGridLine> m_DistanceIndexes;

	//liste des info d'affichage des bancs(utile pour afficher le num�ro de banc dans BaseFlatView)
	std::map<std::uint64_t, ShoalAreaInfo> m_shoalAreaInfo;
};
