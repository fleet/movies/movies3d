
#include "TransducerImageFlatView.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/FanMemory.h"
#include "M3DKernel/datascheme/TransformSet.h"
#include "M3DKernel/datascheme/TransformMap.h"
#include "BaseMathLib/Vector2.h"
#include "ColorPaletteEcho.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "DisplayParameter.h"

using namespace BaseMathLib;
using namespace System::Collections::Generic;

namespace
{
	constexpr const char * LoggerName = "MoviesView.View.2DViews.TransducerImageFlatView";
}

TransducerImageFlatView::TransducerImageFlatView(std::uint32_t sounderId, unsigned int transducerIndex, const std::uint64_t & startPingID, const std::uint64_t & stopPingID)
	: TransducerView(sounderId, transducerIndex, Sv)
	, m_startPingID(startPingID)
	, m_stopPingID(stopPingID)
	, m_surfaceValues(nullptr)
	, m_bottomValues(nullptr)
{
	// ComputeImageData();
}

TransducerImageFlatView::~TransducerImageFlatView(void)
{
	delete[] m_surfaceValues;
	delete[] m_bottomValues;
}

bool TransducerImageFlatView::ComputeImageData()
{
	bool isValid = false;

	ComputeImageBoundary();
	if (m_startIdx != -1 && m_stopIdx != -1)
	{
		ComputeAll();
		isValid = true;
	}

	return isValid;
}

System::Drawing::Image ^ TransducerImageFlatView::GetImage()
{
	System::Drawing::Bitmap ^ refRet = gcnew System::Drawing::Bitmap(
		this->GetWidth(), this->GetHeight(), System::Drawing::Imaging::PixelFormat::Format32bppArgb);

	System::Drawing::Rectangle rect = System::Drawing::Rectangle(0, 0, refRet->Width, refRet->Height);

	System::Drawing::Imaging::BitmapData ^bmpData = refRet->LockBits(
		rect, System::Drawing::Imaging::ImageLockMode::ReadWrite, refRet->PixelFormat);

	System::IntPtr ptr = bmpData->Scan0;
	memcpy((void*)ptr, this->GetPtr(), this->GetByteSize() * sizeof(int));
	refRet->UnlockBits(bmpData);

	return refRet;
}

void TransducerImageFlatView::PingFanAdded(int width, int height)
{

}

void TransducerImageFlatView::ComputeTransformMapsProperties(double& interBeam,
	double& minYOrigin,
	double& maxYOrigin)
{
	// on boucle sur l'ensemble des transformMaps correspondantes
	std::vector<Sounder*> sounders = M3DKernel::GetInstance()->getObjectMgr()->GetSounderDefinition().GetSoundersWithId(m_sounderId);
	size_t nbSounders = sounders.size();
	for (size_t sounderIdx = 0; sounderIdx < nbSounders; sounderIdx++)
	{
		TransformMap* pMap = sounders[sounderIdx]->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
		// l'interbeam est par construction identique pour toutes les transformMaps (soit fix�, soit auto, et le calcul auto fait en sorte que 
		// cela soit vrai)
		interBeam = pMap->getYSpacing();
		double y = pMap->getRealOrigin2().y;
		if (sounderIdx == 0)
		{
			minYOrigin = y;
			maxYOrigin = y;
		}
		else
		{
			minYOrigin = std::min(minYOrigin, y);
			maxYOrigin = std::max(maxYOrigin, y);
		}
	}
}

unsigned int TransducerImageFlatView::GetPixelFromPingId(const std::uint64_t & pingID)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	SounderIndexedTimeContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();

	unsigned int pixelIndex = -1;

	if (pFanContainer != NULL)
	{
		m_nbPingsDraw = 0;

		Sounder* pSounder = NULL;
		for (size_t w = 0; w < pFanContainer->GetObjectCount(); w++)
		{
			PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);
			if (pFan->getSounderRef()->m_SounderId == m_sounderId)
			{
				m_nbPingsDraw++;
				pSounder = pFan->getSounderRef();
			}
		}

		TransformMap *pTransform = pSounder->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
		int delta = std::max(0, m_nbPingsDraw - M3DKernel::GetInstance()->GetRefKernelParameter().getNbPingFanMax());

		int pictureIndex = -1;

		// on parcourt le conteneur des pings dans l'ordre croissant au premier ping qui est superieur ou �gale, c'est le bon    
		const size_t pFanContainerSize = pFanContainer->GetObjectCount();
		for (size_t w = 0; w < pFanContainerSize; ++w)
		{
			PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);
			if (pFan != NULL)
			{
				if (pFan->getSounderRef()->m_SounderId == m_sounderId)
				{
					if (delta > 0)
					{
						delta--;
					}
					else
					{
						pictureIndex++;
					}
				}

				if (pFan->GetPingId() >= pingID)
				{
					pixelIndex = pictureIndex;
					break;
				}
			}
		}
	}
	pKernel->Unlock();

	return pixelIndex;
}

// M�thode permettant de faire le lien entre les ID des pings et leur position dans l'image
void TransducerImageFlatView::ComputeImageBoundary()
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	SounderIndexedTimeContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();

	m_nbPingsDraw = 0;

	Sounder* pSounder = NULL;
	for (size_t w = 0; w < pFanContainer->GetObjectCount(); w++)
	{
		PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);
		if (pFan->getSounderRef()->m_SounderId == m_sounderId)
		{
			m_nbPingsDraw++;
			pSounder = pFan->getSounderRef();
		}
	}

	TransformMap *pTransform = pSounder->GetTransformSetRef()->GetTransformMap(m_transducerIndex);

	int delta = std::max(0, m_nbPingsDraw - M3DKernel::GetInstance()->GetRefKernelParameter().getNbPingFanMax());

	bool idx1Found = false;
	m_startIdx = -1;
	m_stopIdx = -1;
	m_maxHeight = pTransform->getCartesianSize2().y;

	// on parcourt le conteneur des pings en notant pour chaque pingID le bon pixel
	size_t pingIndex = 0;
	int pictureIndex = -1;
	if (pFanContainer != NULL)
	{
		size_t pFanContainerSize = pFanContainer->GetObjectCount();
		for (size_t w = 0; w < pFanContainerSize; ++w)
		{
			PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);
			if (pFan != NULL)
			{
				if (pFan->getSounderRef()->m_SounderId == m_sounderId)
				{
					if (delta > 0)
					{
						delta--;
					}
					else
					{
						pictureIndex++;
					}
				}
				if (pFan->GetPingId() >= m_startPingID && pFan->GetPingId() <= m_stopPingID)
				{
					if (idx1Found == false)
					{
						idx1Found = true;
						if (pictureIndex == -1) {
							// si on n'a encore mis aucun ping du sondeur selectionn�, on trace tout de m�me sur le premier pixel
							m_startIdx = 0;
						}
						else {
							m_startIdx = pictureIndex;
						}
					}
					m_stopIdx = pictureIndex;
				}
			}
		}
	}

	pKernel->Unlock();
}

bool TransducerImageFlatView::CreateBmp()
{
	bool newBmp = false;

	// OTK - 04/11/2009 - correction cas SM20 : la transformmap ne d�marre pas � 0m
	m_StartPixelRange = 0; //(unsigned int) floor((minProf-minYOrigin)/interBeam);
	m_StopPixelRange = m_maxHeight; // (unsigned int) floor((maxProf-maxYOrigin)/interBeam);
	unsigned int MyHeight = m_StopPixelRange - m_StartPixelRange;

	m_StartPixelHRange = m_startIdx;
	m_StopPixelHRange = m_stopIdx;
	unsigned int MyWidth = m_StopPixelHRange - m_StartPixelHRange;

	m_surfaceValues = new int[MyWidth];
	memset(m_surfaceValues, -1, MyWidth * sizeof(int));

	m_bottomValues = new int[MyWidth];
	memset(m_bottomValues, -1, MyWidth * sizeof(int));

	newBmp = this->AllocateBmp(MyWidth, MyHeight);
	if (newBmp)
	{
		ResetImage(System::Drawing::Color::White);
	}

	return newBmp;
}

void TransducerImageFlatView::ComputeAll()
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();

	TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(this->GetSounderId());
	FanMemory *pFanMemory = pKernel->getObjectMgr()->GetSounderDefinition().GetLinkedFanMemory(m_sounderId);

	if (pCont)
	{
		PingFan *lastFan = (PingFan *)pCont->GetDatedObject(pCont->GetObjectCount() - 1);
		if (lastFan)
		{
			double interBeam, minYOrigin, maxYOrigin;
			ComputeTransformMapsProperties(interBeam, minYOrigin, maxYOrigin);
			bool bOk = CreateBmp();
			unsigned int width = m_StopPixelHRange - m_StartPixelHRange; //lastFan->GetFanMemoryRef()->GetMemoryLength();

			if (bOk)
			{
				auto pDisplay = DisplayParameter::getInstance();
				auto palette = pDisplay->GetCurrent2DEchoPalette();

				SounderIndexedTimeContainer *pFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();

				int pictureIndex = std::min<int>(m_nbPingsDraw - 1, width - 1);
				for (int w = pFanContainer->GetObjectCount() - 1; w >= 0 && pictureIndex >= 0; w--)
				{
					PingFan *pFan = (PingFan*)pFanContainer->GetObjectWithIndex(w);

					if (pFan->getSounderRef()->m_SounderId == m_sounderId
						&& pFan->GetPingId() >= m_startPingID && pFan->GetPingId() <= m_stopPingID)
					{
						AddFan(pFan, pictureIndex + m_StartPixelHRange, palette);
						pictureIndex--;
					}
				}
			}
		}
	}

	pKernel->Unlock();
}


void TransducerImageFlatView::AddFan(PingFan *pFan, int fanIdx, const IColorPalette * palette)
{
	// OTK - si le ping est trop ancien, on ne l'affiche pas
	if (fanIdx >= 0)
	{
		this->m_pLock->Lock();

		DisplayParameter *pDisplay = DisplayParameter::getInstance();
		// on ne prend en compte que ce qui est r�ellement affich� (zoom)
		if (fanIdx < m_StopPixelHRange && fanIdx >= m_StartPixelHRange)
		{
			if (pFan)
			{
				Sounder* pSounder = pFan->getSounderRef();
				MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(m_transducerIndex);
				TransformMap *pTransform = pSounder->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
				short *value = ((short*)pMemStruct->GetDataFmt()->GetData());
				double m_compensateHeave = 0;
				Transducer *pTrans = pSounder->GetTransducer(m_transducerIndex);
				SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
				if (pSoftChan)
				{
					m_compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
				}
				else
				{
					M3D_LOG_ERROR(LoggerName, "Channel Not Found");
				}

				double prof = pTrans->m_transDepthMeter;	// transducer depth en cm

				int offset = (int)floor((prof + m_compensateHeave) / pTransform->getYSpacing()) + pTrans->GetSampleOffset();
				m_surfaceValues[fanIdx - m_StartPixelHRange] = offset;

				unsigned int Xdim = 0;
				unsigned int Ydim = 0;
				Vector2I memSize = pTransform->getCartesianSize2();
				Xdim = memSize.x;
				Ydim = memSize.y;
				int maxTabIndex = Xdim * Ydim;  // NMD - FAE 130 - taille max d'index
				DataFmt minThr = pDisplay->GetMinThreshold();
				DataFmt maxThr = pDisplay->GetMaxThreshold();
				m_ImageChanged = true;

				unsigned int i = (unsigned int)floor(Xdim * 0.5);

				int blackARGB = System::Drawing::Color::Black.ToArgb();

				bool afterBottom = false;
				bool drawningBottom = false;

				// pour ne pas avoir � le faire � chaque pixel...
				int sizeY = pMemStruct->GetDataFmt()->getSize().y;
				int nombreCh = pTrans->m_numberOfSoftChannel;
				int* bottomRanges = new int[nombreCh];
				for (int softChan = 0; softChan < nombreCh; softChan++)
				{
					SoftChannel* pChannel = pTrans->getSoftChannelPolarX(softChan);
					std::int32_t bottomRange;
					bool found = false;
					std::uint32_t echoRange = 0;
					pFan->getBottom(pChannel->m_softChannelId, echoRange, bottomRange, found);
					if (found)
					{
						bottomRanges[softChan] = echoRange - pTrans->GetSampleOffset();
					}
					else
					{
						bottomRanges[softChan] = -1;
					}
				}

				// fin des precalculs
				bool bottomTraced = false;
				int * indexes = pTransform->getPointerToIndex() + i;
				int * channelIndexes = pTransform->getPointerToChannelIndex() + i;
				for (int j = Ydim - 1; j >= 0; j--)
				{
					int index = -1;
					int correctedJ = j - offset;
					int tabIndex = correctedJ * Xdim;

					// NMD - FAE 130 - verification que l'index calcul� le d�passe pas la taille max d'index
					if (correctedJ > 0 && tabIndex < maxTabIndex)
					{
						index = *(indexes + tabIndex);
					}
					if (index < 0)
					{
						SetPixel(fanIdx, j, blackARGB);
					}
					else
					{
						if (value[index] > minThr && value[index] < maxThr)
						{
							SetPixel(fanIdx, j, palette->GetColor(value[index]));
						}

						// OTK - FAE018 - sonde en noir
						if (!bottomTraced)
						{
							int channelIndex = *(channelIndexes + tabIndex);
							int echoNum = index%sizeY;
							if (echoNum <= bottomRanges[channelIndex])
							{
								// OTK - 30/03/2010 - on ne trace plus le fond directement sur l'image,
								// mais on sauvegarde sa position pour le tracer par dessus, en GDI+
								//SetPixel(fanIdx,j,blackARGB);
								m_bottomValues[fanIdx - m_StartPixelHRange] = j;
								bottomTraced = true;
							}
						}
					}
				}
				int maxJ = this->GetHeight() + this->m_StartPixelRange;
				for (int j = Ydim + 1; j < maxJ; j++)
				{
					SetPixel(fanIdx, j, blackARGB);
				}
				delete[] bottomRanges;
			}
		}
		this->m_pLock->Unlock();
	}
}
