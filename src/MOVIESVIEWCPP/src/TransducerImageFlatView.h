#pragma once

#include "TransducerView.h"
#include "TransducerFlatView.h"

#include <vector>
#include <list>
#include <float.h>



class TransformMap;
class ColorPalette;

/**
* Classe permettant d'afficher la vue de cot� d'un trasnducteur dans une image sans prendre
* en compte les param�tres de la vue courante (notamment la profondeur max courante)
*/
class TransducerImageFlatView : public TransducerView
{
public:

	/**
	* Constructeur
	* \param sounderId ID du sondeur rendu
	* \param transudcerIndex ID du transducteur rendu
	* \param startPingID ID du premier ping rendu
	* \param stopPingID  ID du dernier ping rendu
	*/
	TransducerImageFlatView(std::uint32_t sounderId, unsigned int transducerIndex,
		const std::uint64_t & startPingID, const std::uint64_t & stopPingID);

	/// Destructeur
	virtual ~TransducerImageFlatView(void);

	/// Retourne l'image
	System::Drawing::Image ^ GetImage();
	
	/**
	* Retourne les valeurs de surface (en pixel)
	* \param index l'index du ping
	* \return L'index de début de la surface dans l'image
	*/
	inline int GetSurfaceValue(int index) { return m_surfaceValues[index - m_StartPixelHRange]; }

	/**
	* Retourne les valeurs du fond (en pixel)
	* \param index l'index du ping
	* \return L'index du fond dans l'image
	*/
	inline int GetBottomValue(int index) { return m_bottomValues[index - m_StartPixelHRange]; }

	/**
	* Calcul la position horizontale dans l'image correpondant � un ping donn�
	* \param l'ID du ping
	* \return L'index correspondant dans l'image
	*/
	unsigned int GetPixelFromPingId(const std::uint64_t & pingID);

	/// Calcul de l'image compl�te, retournbe vrai si l'image a pu etre calcul�e , faux sinon
	bool ComputeImageData();


protected:

	/// Surcharge obligatoire
	virtual void PingFanAdded(int width, int height) override;

	/// Rempli l'image avec les donn�es de ping
	virtual	void ComputeAll() override;

	/// Ajoute un ping fan � l'image
	void AddFan(PingFan *pFan, int fanIdx, const IColorPalette *palette);

	/// Alloue l'image
	bool CreateBmp();

	/// Calcul des propri�t�s de la transform amp
	void ComputeTransformMapsProperties(
		double& interBeam,
		double& minYOrigin,
		double& maxYOrigin);

	/// Calcul les dimensions de l'image
	void ComputeImageBoundary();

	/// ID du premier ping rendu
	std::uint64_t m_startPingID;

	/// ID du dernier ping rendu
	std::uint64_t m_stopPingID;

	/// Hauteur de l'image
	unsigned int m_maxHeight;

	unsigned int m_startIdx;
	unsigned int m_stopIdx;
	int m_nbPingsDraw;

	// ligne de surface
	int * m_surfaceValues;

	// ligne de fond
	int * m_bottomValues;
};
