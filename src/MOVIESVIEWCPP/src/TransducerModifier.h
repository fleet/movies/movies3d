#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "ModuleManager\ModuleManager.h"
#include "Compensation\TransducerPositionCompensation.h"
#include "Compensation\TransducerPositionSet.h"
#include "Compensation\CompensationModule.h"

#include "BaseMathLib/Vector3.h"

#include "M3DKernel/DefConstants.h"
namespace MOVIESVIEWCPP {
	public delegate System::Void UpdateTransducerChange();

	/// <summary>
	/// Description r�sum�e de TransducerModifier
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class TransducerModifier : public System::Windows::Forms::Form
	{
		CompensationModule &m_refModMgr;
		UpdateTransducerChange ^mDelegate;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown4;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Panel^  panel2;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown5;
	private: System::Windows::Forms::Panel^  panel3;
	private: System::Windows::Forms::Label^  label5;

	private: System::Windows::Forms::RadioButton^  radioButtonUserValue;
	private: System::Windows::Forms::RadioButton^  radioButtonHACFileValues;
	public:
		TransducerModifier(CompensationModule &p, UpdateTransducerChange ^refDelegate) :m_refModMgr(p)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//

			mDelegate = refDelegate;
			InitializeAll();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~TransducerModifier()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ComboBox^  comboBox1;
	protected:
	private: System::Windows::Forms::NumericUpDown^  numericUpDown1;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown2;
	private: System::Windows::Forms::NumericUpDown^  numericUpDown3;
	private: System::Windows::Forms::Label^  label1;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Panel^  panel1;


	protected:

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->numericUpDown1 = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown2 = (gcnew System::Windows::Forms::NumericUpDown());
			this->numericUpDown3 = (gcnew System::Windows::Forms::NumericUpDown());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->radioButtonHACFileValues = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonUserValue = (gcnew System::Windows::Forms::RadioButton());
			this->numericUpDown4 = (gcnew System::Windows::Forms::NumericUpDown());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->numericUpDown5 = (gcnew System::Windows::Forms::NumericUpDown());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->label5 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown3))->BeginInit();
			this->panel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown4))->BeginInit();
			this->panel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown5))->BeginInit();
			this->panel3->SuspendLayout();
			this->SuspendLayout();
			// 
			// comboBox1
			// 
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(12, 12);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(300, 21);
			this->comboBox1->TabIndex = 1;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &TransducerModifier::comboBox1_SelectedIndexChanged);
			// 
			// numericUpDown1
			// 
			this->numericUpDown1->DecimalPlaces = 10;
			this->numericUpDown1->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 131072 });
			this->numericUpDown1->Location = System::Drawing::Point(11, 39);
			this->numericUpDown1->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 180, 0, 0, 0 });
			this->numericUpDown1->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 180, 0, 0, System::Int32::MinValue });
			this->numericUpDown1->Name = L"numericUpDown1";
			this->numericUpDown1->Size = System::Drawing::Size(120, 20);
			this->numericUpDown1->TabIndex = 2;
			this->numericUpDown1->ValueChanged += gcnew System::EventHandler(this, &TransducerModifier::numericUpDown1_ValueChanged);
			// 
			// numericUpDown2
			// 
			this->numericUpDown2->DecimalPlaces = 10;
			this->numericUpDown2->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 131072 });
			this->numericUpDown2->Location = System::Drawing::Point(11, 67);
			this->numericUpDown2->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 180, 0, 0, 0 });
			this->numericUpDown2->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 180, 0, 0, System::Int32::MinValue });
			this->numericUpDown2->Name = L"numericUpDown2";
			this->numericUpDown2->Size = System::Drawing::Size(120, 20);
			this->numericUpDown2->TabIndex = 3;
			this->numericUpDown2->ValueChanged += gcnew System::EventHandler(this, &TransducerModifier::numericUpDown2_ValueChanged);
			// 
			// numericUpDown3
			// 
			this->numericUpDown3->DecimalPlaces = 10;
			this->numericUpDown3->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 131072 });
			this->numericUpDown3->Location = System::Drawing::Point(11, 91);
			this->numericUpDown3->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 180, 0, 0, 0 });
			this->numericUpDown3->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 180, 0, 0, System::Int32::MinValue });
			this->numericUpDown3->Name = L"numericUpDown3";
			this->numericUpDown3->Size = System::Drawing::Size(120, 20);
			this->numericUpDown3->TabIndex = 4;
			this->numericUpDown3->ValueChanged += gcnew System::EventHandler(this, &TransducerModifier::numericUpDown3_ValueChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(149, 41);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(59, 13);
			this->label1->TabIndex = 5;
			this->label1->Text = L"X/Rotation";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(149, 93);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(133, 13);
			this->label3->TabIndex = 7;
			this->label3->Text = L"Z/Angle Face AthwartShip";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(149, 69);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(124, 13);
			this->label4->TabIndex = 8;
			this->label4->Text = L"Y/Angle Face AlongShip";
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->radioButtonHACFileValues);
			this->panel1->Controls->Add(this->label1);
			this->panel1->Controls->Add(this->radioButtonUserValue);
			this->panel1->Controls->Add(this->label4);
			this->panel1->Controls->Add(this->numericUpDown1);
			this->panel1->Controls->Add(this->label3);
			this->panel1->Controls->Add(this->numericUpDown2);
			this->panel1->Controls->Add(this->numericUpDown3);
			this->panel1->Location = System::Drawing::Point(12, 39);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(330, 121);
			this->panel1->TabIndex = 9;
			// 
			// radioButtonHACFileValues
			// 
			this->radioButtonHACFileValues->AutoSize = true;
			this->radioButtonHACFileValues->Location = System::Drawing::Point(127, 10);
			this->radioButtonHACFileValues->Name = L"radioButtonHACFileValues";
			this->radioButtonHACFileValues->Size = System::Drawing::Size(81, 17);
			this->radioButtonHACFileValues->TabIndex = 16;
			this->radioButtonHACFileValues->TabStop = true;
			this->radioButtonHACFileValues->Text = L"HAC values";
			this->radioButtonHACFileValues->UseVisualStyleBackColor = true;
			this->radioButtonHACFileValues->CheckedChanged += gcnew System::EventHandler(this, &TransducerModifier::radioButtonHACFileValues_CheckedChanged);
			// 
			// radioButtonUserValue
			// 
			this->radioButtonUserValue->AutoSize = true;
			this->radioButtonUserValue->Location = System::Drawing::Point(11, 10);
			this->radioButtonUserValue->Name = L"radioButtonUserValue";
			this->radioButtonUserValue->Size = System::Drawing::Size(94, 17);
			this->radioButtonUserValue->TabIndex = 15;
			this->radioButtonUserValue->TabStop = true;
			this->radioButtonUserValue->Text = L"Custom values";
			this->radioButtonUserValue->UseVisualStyleBackColor = true;
			this->radioButtonUserValue->CheckedChanged += gcnew System::EventHandler(this, &TransducerModifier::radioButtonUserValue_CheckedChanged);
			// 
			// numericUpDown4
			// 
			this->numericUpDown4->DecimalPlaces = 10;
			this->numericUpDown4->Location = System::Drawing::Point(11, 8);
			this->numericUpDown4->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, 0 });
			this->numericUpDown4->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, System::Int32::MinValue });
			this->numericUpDown4->Name = L"numericUpDown4";
			this->numericUpDown4->Size = System::Drawing::Size(120, 20);
			this->numericUpDown4->TabIndex = 10;
			this->numericUpDown4->ValueChanged += gcnew System::EventHandler(this, &TransducerModifier::numericUpDown4_ValueChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(149, 10);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(124, 13);
			this->label2->TabIndex = 11;
			this->label2->Text = L"Internal DelayTranslation";
			// 
			// panel2
			// 
			this->panel2->Controls->Add(this->label2);
			this->panel2->Controls->Add(this->numericUpDown4);
			this->panel2->Location = System::Drawing::Point(12, 166);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(330, 35);
			this->panel2->TabIndex = 12;
			// 
			// numericUpDown5
			// 
			this->numericUpDown5->DecimalPlaces = 10;
			this->numericUpDown5->Location = System::Drawing::Point(11, 8);
			this->numericUpDown5->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, 0 });
			this->numericUpDown5->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, System::Int32::MinValue });
			this->numericUpDown5->Name = L"numericUpDown5";
			this->numericUpDown5->Size = System::Drawing::Size(120, 20);
			this->numericUpDown5->TabIndex = 10;
			this->numericUpDown5->ValueChanged += gcnew System::EventHandler(this, &TransducerModifier::numericUpDown5_ValueChanged);
			// 
			// panel3
			// 
			this->panel3->Controls->Add(this->label5);
			this->panel3->Controls->Add(this->numericUpDown5);
			this->panel3->Location = System::Drawing::Point(12, 207);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(330, 35);
			this->panel3->TabIndex = 13;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(149, 10);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(170, 13);
			this->label5->TabIndex = 11;
			this->label5->Text = L"Sample offset for range calculation";
			// 
			// TransducerModifier
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(354, 253);
			this->Controls->Add(this->panel3);
			this->Controls->Add(this->panel2);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->comboBox1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->Name = L"TransducerModifier";
			this->Text = L"TransducerModifier";
			this->TopMost = true;
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown3))->EndInit();
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown4))->EndInit();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDown5))->EndInit();
			this->panel3->ResumeLayout(false);
			this->panel3->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion


	private: System::Void InitializeAll()
	{
		this->comboBox1->Items->Clear();
		for (unsigned int i = 0; i < m_refModMgr.GetCompensationParameter().m_transducerPositionSet.GetRecordCount(); i++)
		{
			TransducerPositionCompensation* pPos = m_refModMgr.GetCompensationParameter().m_transducerPositionSet.GetRecord(i);
			if (pPos)
			{
				System::String ^NodeName = gcnew System::String(pPos->m_TransducerName.c_str());
				this->comboBox1->Items->Add(NodeName);
			}
		}
		if (this->comboBox1->Items->Count > 0)
			this->comboBox1->SelectedIndex = 0;

		UpdateComponents();
	}

	private: System::Void UpdateComponents()
	{
		int index = comboBox1->SelectedIndex;
		TransducerPositionCompensation* pPos = m_refModMgr.GetCompensationParameter().m_transducerPositionSet.GetRecord(index);
		if (pPos)
		{
			if (/*m_refModMgr.GetCompensationParameter().m_bUseHACFileValues &&*/ pPos->m_bUseHACFileValues)
			{
				numericUpDown1->Value = System::Decimal(RAD_TO_DEG(pPos->m_HACheadingRot));
				numericUpDown2->Value = System::Decimal(RAD_TO_DEG(pPos->m_HACpitchRot));
				numericUpDown3->Value = System::Decimal(RAD_TO_DEG(pPos->m_HACrollRot));
				numericUpDown1->Enabled = false;
				numericUpDown2->Enabled = false;
				numericUpDown3->Enabled = false;
			}
			else
			{
				numericUpDown1->Value = System::Decimal(RAD_TO_DEG(pPos->m_headingRot));
				numericUpDown2->Value = System::Decimal(RAD_TO_DEG(pPos->m_pitchRot));
				numericUpDown3->Value = System::Decimal(RAD_TO_DEG(pPos->m_rollRot));
				numericUpDown1->Enabled = true;
				numericUpDown2->Enabled = true;
				numericUpDown3->Enabled = true;
			}
			numericUpDown4->Value = System::Decimal(pPos->m_internalDelay);
			numericUpDown5->Value = System::Decimal(pPos->m_sampleOffset);
			radioButtonHACFileValues->Checked = pPos->m_bUseHACFileValues;
			radioButtonUserValue->Checked = !pPos->m_bUseHACFileValues;
		}
	}


	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		UpdateComponents();
	}

	private: System::Void numericUpDown1_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		if (this->comboBox1->Items->Count == 0)
			return;

		int index = this->comboBox1->SelectedIndex;
		TransducerPositionCompensation* pPos = m_refModMgr.GetCompensationParameter().m_transducerPositionSet.GetRecord(index);
		if (pPos)
		{
			bool useHacValues = pPos->m_bUseHACFileValues;
			if (!useHacValues)
			{
				double t = DEG_TO_RAD(System::Decimal::ToDouble(numericUpDown1->Value));
				if (t != pPos->m_headingRot)
				{
					pPos->m_headingRot = t;
					M3D_LOG_WARN("MoviesView.TransducerModifier", "Modifying Transducer Angle Settings");
					m_refModMgr.GetCompensationTransducerPosition().ApplyParameter(m_refModMgr.GetCompensationParameter());
					mDelegate->Invoke();
				}
			}
		}
	}
	private: System::Void numericUpDown3_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		if (this->comboBox1->Items->Count == 0)
			return;

		int index = this->comboBox1->SelectedIndex;
		TransducerPositionCompensation* pPos = m_refModMgr.GetCompensationParameter().m_transducerPositionSet.GetRecord(index);
		if (pPos)
		{
			bool useHacValues = pPos->m_bUseHACFileValues;
			if (!useHacValues)
			{
				double t = DEG_TO_RAD(System::Decimal::ToDouble(numericUpDown3->Value));
				if (t != pPos->m_rollRot)
				{
					pPos->m_rollRot = t;
					M3D_LOG_WARN("MoviesView.TransducerModifier", "Modifying Transducer Angle Settings");
					m_refModMgr.GetCompensationTransducerPosition().ApplyParameter(m_refModMgr.GetCompensationParameter());
					mDelegate->Invoke();
				}
			}
		}
	}
	private: System::Void numericUpDown2_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		if (this->comboBox1->Items->Count == 0)
			return;

		int index = this->comboBox1->SelectedIndex;
		TransducerPositionCompensation* pPos = m_refModMgr.GetCompensationParameter().m_transducerPositionSet.GetRecord(index);
		if (pPos)
		{
			bool useHacValues = pPos->m_bUseHACFileValues;
			if (!useHacValues)
			{
				double t = DEG_TO_RAD(System::Decimal::ToDouble(numericUpDown2->Value));
				if (t != pPos->m_pitchRot)
				{
					pPos->m_pitchRot = t;
					M3D_LOG_WARN("MoviesView.TransducerModifier", "Modifying Transducer Angle Settings");
					m_refModMgr.GetCompensationTransducerPosition().ApplyParameter(m_refModMgr.GetCompensationParameter());
					mDelegate->Invoke();
				}
			}
		}
	}
	private: System::Void numericUpDown4_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		if (this->comboBox1->Items->Count == 0)
			return;

		int index = this->comboBox1->SelectedIndex;
		TransducerPositionCompensation* pPos = m_refModMgr.GetCompensationParameter().m_transducerPositionSet.GetRecord(index);
		if (pPos)
		{
			double t = System::Decimal::ToDouble(numericUpDown4->Value);
			if (t != pPos->m_internalDelay)
			{
				pPos->m_internalDelay = t;
				M3D_LOG_WARN("MoviesView.TransducerModifier", "Modifying Transducer Angle Settings");
				m_refModMgr.GetCompensationTransducerPosition().ApplyParameter(m_refModMgr.GetCompensationParameter());
				mDelegate->Invoke();
			}
		}
	}

	private: System::Void numericUpDown5_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		if (this->comboBox1->Items->Count == 0)
			return;

		int index = this->comboBox1->SelectedIndex;
		TransducerPositionCompensation* pPos = m_refModMgr.GetCompensationParameter().m_transducerPositionSet.GetRecord(index);
		if (pPos)
		{
			double t = System::Decimal::ToDouble(numericUpDown5->Value);
			if (t != pPos->m_sampleOffset)
			{
				pPos->m_sampleOffset = t;
				M3D_LOG_WARN("MoviesView.TransducerModifier", "Modifying Transducer Angle Settings");
				m_refModMgr.GetCompensationTransducerPosition().ApplyParameter(m_refModMgr.GetCompensationParameter());
				mDelegate->Invoke();
			}
		}
	}
	private: System::Void radioButtonUserValue_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		if (this->comboBox1->Items->Count == 0)
			return;

		int index = this->comboBox1->SelectedIndex;
		TransducerPositionCompensation* pPos = m_refModMgr.GetCompensationParameter().m_transducerPositionSet.GetRecord(index);
		if (pPos)
		{
			bool t = !radioButtonUserValue->Checked;
			if (t != pPos->m_bUseHACFileValues)
			{
				pPos->m_bUseHACFileValues = t;
				M3D_LOG_WARN("MoviesView.TransducerModifier", "Modifying Transducer Angle Settings");
				m_refModMgr.GetCompensationTransducerPosition().ApplyParameter(m_refModMgr.GetCompensationParameter());
				mDelegate->Invoke();
			}
		}
		UpdateComponents();
	}
	private: System::Void radioButtonHACFileValues_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {
		if (this->comboBox1->Items->Count == 0)
			return;

		int index = this->comboBox1->SelectedIndex;
		TransducerPositionCompensation* pPos = m_refModMgr.GetCompensationParameter().m_transducerPositionSet.GetRecord(index);
		if (pPos)
		{
			bool t = radioButtonHACFileValues->Checked;
			if (t != pPos->m_bUseHACFileValues)
			{
				pPos->m_bUseHACFileValues = t;
				M3D_LOG_WARN("MoviesView.TransducerModifier", "Modifying Transducer Angle Settings");
				m_refModMgr.GetCompensationTransducerPosition().ApplyParameter(m_refModMgr.GetCompensationParameter());
				mDelegate->Invoke();
			}
		}
		UpdateComponents();
	}
	};
}
