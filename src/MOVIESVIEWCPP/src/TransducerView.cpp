#include "TransducerView.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/FanMemory.h"
#include "M3DKernel/datascheme/TransformSet.h"
#include "M3DKernel/datascheme/TransformMap.h"
#include "BaseMathLib/Vector2.h"
#include "ColorPalette.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "DisplayParameter.h"
#include "ColorPaletteEcho.h"
#include "ColorUtils.h"
#include "MultifrequencyEchogram.h"

#include "M3DKernel/utils/carto/CartoTools.h"

#include "ShoalExtraction/ShoalExtractionOutput.h"


#include "shoalextraction/data/shoaldata.h"
#include "shoalextraction/data/pingdata.h"
#include "shoalextraction/data/pingshoalstat.h"
#include "shoalextraction/shoalextractionoutput.h"

using namespace BaseMathLib;
using namespace shoalextraction;

//static
//shoal consumer
const ShoalExtractionOutputList * TransducerView::m_pShoalList = NULL;

TransducerView::TransducerView(std::uint32_t sounderId, unsigned int transducerIndex, DisplayDataType displayDataType)
	: m_displayDataType(displayDataType)
	, m_sounderId(sounderId)
	, m_transducerIndex(transducerIndex)
	, m_transducerIndex2(0)
	, m_transducerIndex3(0)
	, m_Active(false)
	, m_ImageChanged(false)
{
	m_pLock = new CRecursiveMutex();
	m_StartPixelRange = m_StopPixelRange = 0;
	m_StartPixelHRange = m_StopPixelHRange = 0;
	m_StartPixelDepth = m_StopPixelDepth = 0.0;
	m_Width = 0;
	m_Height = 0;
	m_TotalRange = 0;
	m_rgbValues = NULL;

	SinglePingFanRenderer * singlePingFanRenderer;
	switch (m_displayDataType)
	{
	case AlongShipAngle:
		singlePingFanRenderer = new AlongShipAngleRenderer();
		break;

	case AthwartShipAngle:
		singlePingFanRenderer = new AthwartShipAngleRenderer();
		break;

	case Sv:
	default:
		singlePingFanRenderer = new SvRenderer();
		break;
	}

	singlePingFanRenderer->transducerIndex = transducerIndex;
	pingFanRenderer = singlePingFanRenderer;
}

TransducerView::TransducerView(std::uint32_t sounderId, unsigned int transducerIndex, unsigned int transducerIndex2, unsigned int transducerIndex3
	, const MultifrequencyEchogram& coloredEchogram)
	: m_displayDataType(Sv)
	, m_sounderId(sounderId)
	, m_transducerIndex(transducerIndex)
	, m_transducerIndex2(transducerIndex2)
	, m_transducerIndex3(transducerIndex3)
	, m_coloredEchogram(coloredEchogram)
	, m_Active(false)
	, m_ImageChanged(false)
{
	m_pLock = new CRecursiveMutex();
	m_StartPixelRange = m_StopPixelRange = 0;
	m_StartPixelHRange = m_StopPixelHRange = 0;
	m_StartPixelDepth = m_StopPixelDepth = 0.0;
	m_Width = 0;
	m_Height = 0;
	m_TotalRange = 0;
	m_rgbValues = NULL;

	ComposedPingFanRenderer * composedPingFanRenderer = new ComposedPingFanRenderer();
	composedPingFanRenderer->transducerIndexR = m_transducerIndex;
	composedPingFanRenderer->transducerIndexG = m_transducerIndex2;
	composedPingFanRenderer->transducerIndexB = m_transducerIndex3;
	composedPingFanRenderer->initialize(m_coloredEchogram.getMinValueRed(), m_coloredEchogram.getMaxValueRed()
		, m_coloredEchogram.getMinValueGreen(), m_coloredEchogram.getMaxValueGreen()
		, m_coloredEchogram.getMinValueBlue(), m_coloredEchogram.getMaxValueBlue());
	pingFanRenderer = composedPingFanRenderer;
}

TransducerView::~TransducerView(void)
{
	m_pLock->Lock();
	m_pLock->Unlock();
	delete m_pLock;
	m_pLock = NULL;

	if (m_rgbValues)
		delete m_rgbValues;
}

void TransducerView::LockDatas()
{
	m_pLock->Lock();
}

void TransducerView::UnlockDatas()
{
	m_pLock->Unlock();
}

void TransducerView::SetActive(bool a)
{
	if (m_pLock != NULL)
	{
		m_pLock->Lock();
		m_Active = a;
		M3DKernel *pKernel = M3DKernel::GetInstance();
		pKernel->Lock();
		if (m_Active)
		{
			ComputeAll();
		}
		pKernel->Unlock();
		m_pLock->Unlock();
	}
}

void TransducerView::EsuClosed()
{
	// do nothing
}

void TransducerView::ForceUpdate()
{
	if (m_pLock != NULL)
	{
		m_pLock->Lock();
		M3DKernel *pKernel = M3DKernel::GetInstance();
		pKernel->Lock();
		ComputeAll();
		pKernel->Unlock();
		m_pLock->Unlock();
	}
}

void TransducerView::CheckForUpdate()
{
	if (m_pLock != NULL)
	{
		m_pLock->Lock();
		if (m_Active)
		{
			bool needRecompute = false;
			if (DisplayParameter::getInstance()->dataChanged())
				needRecompute = true;
			if (needRecompute)
				ComputeAll();
		}
		m_pLock->Unlock();
	}
}

Transducer *TransducerView::GetTransducer()
{
	Transducer *pTransFound = nullptr;
	
	if (m_pLock != nullptr)
	{
		m_pLock->Lock();
		M3DKernel *pKernel = M3DKernel::GetInstance();
		Sounder *pSounder = pKernel->getObjectMgr()->GetLastValidSounder(m_sounderId);
		if (pSounder)
			pTransFound = pSounder->GetTransducer(m_transducerIndex);
	
		m_pLock->Unlock();
	}
	return pTransFound;
}

std::string TransducerView::getName()
{ 
	if (isColoredEchogram())
	{
		return m_coloredEchogram.getName();
	}
	else
	{
		Transducer *  transducer = GetTransducer();
		if (transducer != nullptr) 
		{
			std::string strName = transducer->m_transName;
			switch (m_displayDataType)
			{
			case AlongShipAngle:
				strName += " Along Ship Angle";
				break;
			case AthwartShipAngle:
				strName += " Athwart Ship Angle";
				break;
			case Sv:
			default:
				//LB 02/07/2020 patch pour que l'EI supervis�e fonctionne
				//strName += " Sv";
				break;
			}
			return strName;
		}
	}

	return std::string();
}

bool TransducerView::AllocateBmp(unsigned int W, unsigned int H)
{
	if (W == m_Width && H == m_Height)
		return false;

	if (W == 0 || H == 0)
		return false;

	m_bytesSize = W * H;
	m_Width = W;
	m_Height = H;
	if (m_rgbValues)
		delete m_rgbValues;
	m_rgbValues = new int[m_bytesSize];

	return true;
}

void TransducerView::EraseAlphaPixel(const unsigned int & widthIdx, const unsigned int & profAsPixel, System::Drawing::Color Color)
{
	int red = (Color.R & 0xFF0000) >> 16;
	int green = (Color.G & 0x00FF00) >> 8;
	int blue = Color.B & 0x0000FF;
	double alpha = 0.0;

	//to erase a transparent pixel, compute with alpha = - alpha / (1.0 - alpha)
	if (Color.A != 255)
	{
		alpha = Color.A / 255.0;
		alpha = alpha / (alpha - 1.0);
	}

	SetAlphaPixel(widthIdx, profAsPixel, alpha, red, green, blue);
}

void TransducerView::SetAlphaPixel(const unsigned int & widthIdx, const unsigned int & profAsPixel, System::Drawing::Color Color)
{
	int red = (Color.R & 0xFF0000) >> 16;
	int green = (Color.G & 0x00FF00) >> 8;
	int blue = Color.B & 0x0000FF;
	double alpha = Color.A / 255.0;

	SetAlphaPixel(widthIdx, profAsPixel, alpha, red, green, blue);
}

void TransducerView::SetAlphaPixel(const unsigned int & widthIdx, const unsigned int & profAsPixel, double alpha, int red, int green, int blue)
{
	m_ImageChanged = true;
	if (profAsPixel >= m_StartPixelRange && profAsPixel < m_StopPixelRange
		&& widthIdx >= m_StartPixelHRange && widthIdx < m_StopPixelHRange)
	{
		// just in case
		if (m_Width *(profAsPixel - m_StartPixelRange) + widthIdx - m_StartPixelHRange < m_bytesSize)
		{
			int currentCol = m_rgbValues[m_Width *(profAsPixel - m_StartPixelRange) + widthIdx - m_StartPixelHRange];
			int currentRed = (currentCol & 0xFF0000) >> 16;
			int currenGreen = (currentCol & 0x00FF00) >> 8;
			int currentBlue = currentCol & 0x0000FF;
			currentRed += (red - currentRed) * alpha;
			currenGreen += (green - currenGreen) * alpha;
			currentBlue += (blue - currentBlue) * alpha;
			//mix alpha, red, green and blue components
			currentCol = (0xFF000000) + (currentRed << 16) + (currenGreen << 8) + currentBlue;
			m_rgbValues[m_Width *(profAsPixel - m_StartPixelRange) + widthIdx - m_StartPixelHRange] = currentCol;
		}
	}
}

void TransducerView::ResetImage(System::Drawing::Color^ color)
{
	ResetImage(color->ToArgb());
}

void TransducerView::ResetImage(const int color)
{
	if (m_rgbValues)
	{
		const unsigned int size = m_Width * m_Height;
		for (unsigned int i = 0; i < size; ++i)
		{
			m_rgbValues[i] = color;
		}
		m_ImageChanged = true;
	}
}

System::Drawing::Color ^ TransducerView::GetBackgroundColor()
{
	return DisplayParameter::getInstance()->GetDisplay2DSingleEchoes() ? System::Drawing::Color::Black : System::Drawing::Color::White;
}

System::String^ TransducerView::FormatEchoToolTip(PingFan *pFan, int x, int y)
{
	System::String^ result = "";

	System::String^ valueStr = "---";
	System::String^ valueUnit = "dB";
	double depth = 0;

	// on r�cup�re les donn�es
	double compensateHeave = 0;
	Sounder* pSounder = pFan->getSounderRef();
	Transducer *pTrans = pSounder->GetTransducer(m_transducerIndex);
	SoftChannel *pSoftChan = pTrans->getSoftChannelPolarX(0);
	if (pSoftChan)
	{
		compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
	}
	double prof = pTrans->m_transDepthMeter;	// transducer depth en cm
	TransformMap *pTransform = pSounder->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
	int offset = (int)((prof + compensateHeave) / pTransform->getYSpacing()) + pTrans->GetSampleOffset();
	int echoDepth = y - offset;
	int index = -1;
	int numBeam = -1;
	int numEcho = -1;
	if (echoDepth >= 0 && echoDepth < pTransform->getCartesianSize2().y)
	{
		index = pTransform->getIndex(x, echoDepth);
	}

	depth = pTransform->cartesianToRealY(echoDepth) + prof + compensateHeave;
	if (index > 0)
	{
		MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(m_transducerIndex);
		if (index < pMemStruct->GetDataFmt()->getSize().x * pMemStruct->GetDataFmt()->getSize().y)
		{
			switch (m_displayDataType)
			{
			case AlongShipAngle:
			case AthwartShipAngle:
			{
				valueUnit = "�";
				MemoryObjectPhase * phase = pMemStruct->GetPhase();
				if (phase)
				{
					Phase * data = phase->GetData();
					if (data)
					{
						// TODO UNKNWON ANGLE
						short angleValue = UNKNWON_ANGLE;
						if (m_displayDataType == AlongShipAngle)
						{
							angleValue = data[index].GetRawAlongValue();
						}
						else if(m_displayDataType == AthwartShipAngle)
						{
							angleValue = data[index].GetRawAthwartValue();
						}

						if (angleValue != UNKNWON_ANGLE)
						{
							valueStr = System::Convert::ToString(angleValue / 100.);
						}
					}					
				}
			}
				break;

			case Sv:
			default:
			{
				valueUnit = "dB";
				short *value = ((short*)pMemStruct->GetDataFmt()->GetData());
				short valueDB = value[index];
				if (valueDB > UNKNOWN_DB)
				{
					valueStr = System::Convert::ToString(valueDB / 100.);
				}
			}
				break;
			}

			

			int numFaisceau = *pTransform->getPointerToChannelIndex(x, echoDepth);
			if (numFaisceau >= 0 && numFaisceau < pTrans->m_numberOfSoftChannel)
			{
				numBeam = numFaisceau;
			}

			// NMD - FE 090 - calcul du num�ro d'echo
			Vector2D polarCoord = pTransform->cartesianToPolar2(Vector2D(x, echoDepth));
			if (polarCoord.y >= 0 && polarCoord.y < pMemStruct->GetDataFmt()->getSize().y)
			{
				numEcho = polarCoord.y;
			}
		}
	}

	// On utilise l'heure du beam plutot que celle du ping
	char date[255];
	BeamDataObject *beamData = pFan->getRefBeamDataObject(pSoftChan->getSoftwareChannelId(), pSoftChan->getSoftwareVirtualChannelId());
	HacTime objectTime{ beamData->m_timeCPU, beamData->m_timeFraction };
	objectTime.GetTimeDesc(date, 254);
	
	// calcul des dimensions du beam a -3dB
	System::String^ athDimention = "---";
	System::String^ alongDimention = "---";
	if (echoDepth > 0 && numBeam > -1)
	{
		SoftChannel * pSoftChan = pTrans->getSoftChannelPolarX(numBeam);
		athDimention = System::String::Format("{0:0.00}", 2.*pTransform->cartesianToRealY(echoDepth)*tan(pSoftChan->m_beam3dBWidthAthwartRad / 2.));
		alongDimention = System::String::Format("{0:0.00}", 2.*pTransform->cartesianToRealY(echoDepth)*tan(pSoftChan->m_beam3dBWidthAlongRad / 2.));
	}

	// NMD - FE 090 - calcul de la position r�elle de l'echo point�
	System::String ^ strLattitude = "---";
	System::String ^ strLongitude = "---";
	if (numEcho != -1)
	{
		BaseMathLib::Vector3D geoCoord = pFan->getSounderRef()->GetPolarToGeoCoord(pFan, m_transducerIndex, numBeam, numEcho);
		strLattitude = gcnew System::String(CCartoTools::LattitudeToString(geoCoord.x).c_str());
		strLongitude = gcnew System::String(CCartoTools::LongitudeToString(geoCoord.y).c_str());;
	}

	result += "Ping ID\t\t: " + pFan->m_computePingFan.m_pingId;
	result += "\nPing ID (File)\t: " + pFan->m_computePingFan.m_filePingId;
	result += "\nLatitude\t\t: " + strLattitude;
	result += "\nLongitude\t: " + strLongitude;
	result += System::String::Format("\nDistance \t: {0:0.000} nm\n", pFan->m_relativePingFan.m_cumulatedDistance / 1852.);

	if (pSoftChan != nullptr) 
	{
		auto softId = pSoftChan->getSoftwareChannelId();
		result += System::String::Format("Speed\t\t: {0:0.0} knt\nHeading\t\t: {1:0.0}�"
			+ "\nPitch\t\t: {2:0.0}�\nRoll\t\t: {3:0.0}�\nHeave\t\t: {4:0.00} m",
			pFan->GetNavAttributesRef(softId)->m_speedMeter*3.6 / 1.852,
			RAD_TO_DEG(pFan->GetNavAttributesRef(softId)->m_headingRad),
			RAD_TO_DEG(pFan->GetNavAttitudeRef(softId)->m_pitchRad),
			RAD_TO_DEG(pFan->GetNavAttitudeRef(softId)->m_rollRad),
			pFan->GetNavAttitudeRef(softId)->m_heaveMeter);
	}
	else 
	{
		result += "Speed\t\t: --- knt\nHeading\t\t: ---�"
			+ "\nPitch\t\t: ---�\nRoll\t\t: ---�\nHeave\t\t: --- m";
	}

	result += System::String::Format("\nSample Spacing\t: {6:0.000} m"
		+ "\n-3dB Ath. dim.\t: {0} m\n-3dB Along. dim.\t: {1} m"
		+ "\nDepth\t\t: {2:0.0} m\nDate\t\t: {3}\nValue\t\t: {4} {5}",
		athDimention,
		alongDimention,
		depth,
		gcnew System::String(date),
		valueStr, valueUnit,
		pTrans->getBeamsSamplesSpacing()
	);

	return result;
}

void TransducerView::UpdateTransformMap(int width, int height, bool stretched)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	pKernel->Lock();
	TimeObjectContainer *pCont = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(GetSounderId());
	if (pCont)
	{
		DisplayParameter *pDisplay = DisplayParameter::getInstance();
		double minProf = pDisplay->GetCurrentMinDepth();
		double maxProf = pDisplay->GetCurrentMaxDepth();
		double minWidth = pDisplay->GetMinFrontHorizontalRatio();
		double maxWidth = pDisplay->GetMaxFrontHorizontalRatio();
		double defaultXSpacing = pKernel->GetRefKernelParameter().getScreenPixelSizeX();
		double defaultYSpacing = pKernel->GetRefKernelParameter().getScreenPixelSizeY();
		bool customSampling = pKernel->GetRefKernelParameter().getCustomSampling();

		// OTK - FAE057 - il faut mettre � jour les transormTables de l'ensemble des pings affich�s et pas seulement
		// du dernier (il y en a potentiellement plusieurs).
		std::vector<Sounder*> sounders = pKernel->getObjectMgr()->GetSounderDefinition().GetSoundersWithId(GetSounderId());
		size_t nbSounders = sounders.size();

		double globalXSpacing = defaultXSpacing;
		double globalYSpacing = defaultYSpacing;
		// en mode auto, premi�re boucle sur les transformmaps pour avoir le samplespacing min
		if (!customSampling)
		{
			double spacing = (maxProf - minProf) / height;
			globalXSpacing = std::min<double>(globalXSpacing, spacing);

			// en mode auto, globalXSpacing == globalYSpacing pour ne pas d�former l'image
			globalYSpacing = globalXSpacing;
		}
		
		// application du pas pour toutes les tables
		for (size_t sounderIdx = 0; sounderIdx < nbSounders; sounderIdx++)
		{
			TransformMap* pTransform = sounders[sounderIdx]->GetTransformSetRef()->GetTransformMap(m_transducerIndex);
			pTransform->setXYSpacing(globalXSpacing, globalYSpacing);
			pTransform->Update();
		}
	}
	pKernel->Unlock();
}

//*****************************************************************************
// Name : GetFilteredShoals
// Description : get the filtered shoals for the given pingId and the current sounder and transducer
// Parameters : (in)	std::uint64_t pingMinId
//              (in)	std::uint64_t pingMaxId
// Return : std::vector<ShoalData*>
//*****************************************************************************
std::vector<ShoalData*> TransducerView::GetFilteredShoals(std::uint64_t pingMinId,
	std::uint64_t pingMaxId)
{
	std::vector<ShoalData*> result;

	if (pingMinId >= 0 && GetShoalList() != NULL)
	{
		//for each shoal
		for (int iShoal = 0; iShoal < GetShoalList()->size(); iShoal++)
		{
			ShoalData* pShoal = GetShoalList()->at(iShoal)->m_pClosedShoal;

			//verify the sounder and transducer are correct
			if (pShoal->GetSounderId() == GetSounderId() &&
				pShoal->GetTransId() == m_transducerIndex)
			{
				int nbPings = pShoal->GetPings().size();
				int firstPingId = pShoal->GetPings()[0]->GetPingId();
				int lastPingId = pShoal->GetPings()[nbPings - 1]->GetPingId();

				//verify the ping range is correct
				if (firstPingId <= pingMaxId && lastPingId >= pingMinId)
				{
					result.push_back(pShoal);
				}//end ping range is correct
			}//end the transducer is correct
		}
	}

	return result;
}


//*****************************************************************************
// Name : GetFilteredShoals
// Description : get the filtered shoals for the given pingId and the current sounder and transducer
// Parameters : (in)	int pingId
// Return : std::vector<ShoalData*>
//*****************************************************************************
std::vector<TShoalPingInfo> TransducerView::GetFilteredShoalsInfo(std::uint64_t pingId)
{
	std::vector<TShoalPingInfo> result;

	if (pingId >= 0 && GetShoalList() != NULL)
	{
		//for each shoal
		for (int iShoal = 0; iShoal < GetShoalList()->size(); iShoal++)
		{
			ShoalData* pShoal = GetShoalList()->at(iShoal)->m_pClosedShoal;

			//verify the sounder and transducer are correct
			if (pShoal->GetSounderId() == GetSounderId() &&
				pShoal->GetTransId() == m_transducerIndex)
			{
				int nbPings = pShoal->GetPings().size();
				int firstPingId = pShoal->GetPings()[0]->GetPingId();
				int lastPingId = pShoal->GetPings()[nbPings - 1]->GetPingId();

				//verify the ping range is correct
				if (firstPingId <= pingId && lastPingId >= pingId)
				{
					//get the ping data
					PingData* pRefPingData = NULL;
					for (int iPing = 0; iPing < nbPings && pRefPingData == NULL; iPing++)
					{
						PingData* pPingData = pShoal->GetPings()[iPing];
						if (pPingData->GetPingId() == pingId)
						{
							pRefPingData = pPingData;
						}
					}
					if (pRefPingData != NULL)
					{
						result.push_back(std::make_pair(pShoal, pRefPingData));
					}//end if pRefPingData != NULL
				}//end ping range is correct
			}//end the transducer is correct
		}
	}

	return result;
}

//*****************************************************************************
// Name : DrawLine
// Description : Draw the line
// Parameters : (in)	BaseMathLib::Vector2I pointI
//				(in)	BaseMathLib::Vector2I pointJ
//				(in)	System::Drawing::Color color
//				(in)	bool erase (for transparent colors only)
// Return : void
//*****************************************************************************
void TransducerView::DrawLine(BaseMathLib::Vector2I& point1,
	BaseMathLib::Vector2I& point2,
	System::Drawing::Color color,
	bool erase)
{
	DrawLine(point1.x, point1.y, point2.x, point2.y, color);
}


//*****************************************************************************
// Name : DrawLine
// Description : Draw the line
// Parameters : (in)	int x1, int y1, int x2, int y2
//				(in)	System::Drawing::Color color
//				(in)	bool erase (for transparent colors only)
// Return : void
//*****************************************************************************
void TransducerView::DrawLine(int x1, int y1, int x2, int y2,
	System::Drawing::Color color,
	bool erase)
{
	int nbStepX = x2 - x1;
	int nbStepY = y2 - y1;
	double x, y;
	int xMax, yMax;

	//declaration du type generique d'affichage de pixel
	void (TransducerView::*DrawPixelFn)
		(const unsigned int & widthIdx, const unsigned int & profAsPixel, System::Drawing::Color Color) =
		&TransducerView::SetPixel;

	//si la couleur poss�de une composante de transparence, appel de l'affichage avec transparence
	if (color.A != 255)
	{
		if (erase)
		{
			DrawPixelFn = &TransducerView::EraseAlphaPixel;
		}
		else
		{
			DrawPixelFn = &TransducerView::SetAlphaPixel;
		}
	}

	if (nbStepX == 0)
	{
		y = std::min<double>(y1, y2);
		yMax = std::max<int>(y1, y2);
		while ((int)y <= yMax)
		{
			(this->*DrawPixelFn)(x1, y, color);
			y++;
		}
	}
	else
	{
		double coef = (double)nbStepY / (double)nbStepX;

		if (coef > 1 || coef < -1)
		{
			y = std::min<double>(y1, y2);
			yMax = std::max<int>(y1, y2);
			x = (nbStepY < 0) ? x2 : x1;

			coef = 1.0 / coef;
			while ((int)y <= yMax)
			{
				(this->*DrawPixelFn)(x, y, color);
				y++;
				x += coef;
			}
		}
		else
		{
			x = std::min<double>(x1, x2);
			xMax = std::max<int>(x1, x2);
			y = (nbStepX < 0) ? y2 : y1;

			while ((int)x <= xMax)
			{
				(this->*DrawPixelFn)(x, y, color);
				x++;
				y += coef;
			}
		}
	}
}

void EchoPositionComputer::EstimatePixelRangeForEcho(int echoNum, int & minIdx, int & maxIdx) const
{
	minIdx = -1;
	maxIdx = -1;

	if (echoNum >= 0)
	{
		double angle1 = channel->m_mainBeamAthwartSteeringAngleRad - channel->m_beam3dBWidthAthwartRad *0.5;
		double angle2 = channel->m_mainBeamAthwartSteeringAngleRad + channel->m_beam3dBWidthAthwartRad *0.5;

		BaseMathLib::Vector2D v1 = BaseMathLib::Vector2D(sin(angle1), cos(angle1)) * echoNum;
		BaseMathLib::Vector2D v2 = BaseMathLib::Vector2D(sin(angle2), cos(angle2)) * echoNum;

		std::uint32_t rangeMin = std::min<std::uint32_t>(v1.y, v2.y);
		std::uint32_t rangeMax = std::max<std::uint32_t>(v1.y, v2.y);

		rangeMin = std::min<std::uint32_t>(echoNum, rangeMin);
		rangeMax = std::max<std::uint32_t>(echoNum, rangeMax);

		double bottomMin = rangeMin * Ydim;
		bottomMin /= sizeY;

		double bottomMax = rangeMax * Ydim;
		bottomMax /= sizeY;

		minIdx = (int)floor(bottomMin - 1) + offset;
		maxIdx = (int)ceil(bottomMax + 1) + offset;
	}
}

void EchoPositionComputer::EstimatePixelRangeForEcho(const std::vector<int>& echoNums, int & minIdx, int & maxIdx) const
{
	minIdx = -1;
	maxIdx = -1;

	for (unsigned int softChan = 0; softChan < transducer->m_numberOfSoftChannel; ++softChan)
	{
		auto channel = transducer->getSoftChannelPolarX(softChan);
		const int echo = echoNums[softChan];

		int channelMinIdx = -1;
		int channelMaxIdx = -1;
		EstimatePixelRangeForEcho(echo, channelMinIdx, channelMaxIdx);

		if (minIdx = -1)
			minIdx = channelMinIdx;
		else
			minIdx = std::min<int>(minIdx, channelMinIdx);

		if (maxIdx = -1)
			maxIdx = channelMaxIdx;
		else
			maxIdx = std::max<int>(maxIdx, channelMaxIdx);
	}
}

void EchoPositionComputer::ComputeEchoPos(const std::vector<int> & echoRanges, std::vector<BaseMathLib::Vector2I> & points) const
{
	int minIdx = -1;
	int maxIdx = -1;
	EstimatePixelRangeForEcho(echoRanges, minIdx, maxIdx);
	ComputeEchoPos(echoRanges, minIdx, maxIdx, points);
}

void EchoPositionComputer::ComputeEchoPos(const std::vector<int> & echoRanges, int minIdx, int maxIdx, std::vector<BaseMathLib::Vector2I> & points) const
{
	if (minIdx != -1 && maxIdx != -1)
	{
		int jStart = std::max<int>(jMin, minIdx);
		int jEnd = std::min<int>(jMax, maxIdx);
		std::vector<bool> echoFound(Xdim, false);

		for (int j = jEnd; j >= jStart; --j)
		{
			const int correctedJ = j - offset;
			int * index_ptr = indexes + correctedJ * Xdim;
			int * channel_ptr = channelIndexes + correctedJ * Xdim;

			const int iMax = std::min<int>(Xdim, maxTabIndex - correctedJ * Xdim);
			for (int i = 0; i < iMax; ++i)
			{
				if (!echoFound[i])
				{
					int index = *index_ptr;
					if (index >= 0 && index < indexMax)
					{
						int channelIndex = *channel_ptr;
						if (channelIndex >= 0 && channelIndex < indexMax)
						{
							int echoNum = index % sizeY;
							if (echoNum <= echoRanges[channelIndex])
							{
								// OTK - 30/03/2010 - on ne trace plus le fond directement sur l'image,
								// mais on sauvegarde sa position pour le tracer par dessus, en GDI+
								points.push_back(BaseMathLib::Vector2I(i, j));
								echoFound[i] = true;
							}
						}
					}
				}
				++index_ptr;
				++channel_ptr;
			}
		}
	}
}

void EchoPositionComputer::ComputeSingleEchoPos(const std::vector<int> & echoRanges, int & echoPos) const
{
	int minIdx = -1;
	int maxIdx = -1;
	EstimatePixelRangeForEcho(echoRanges, minIdx, maxIdx);
	ComputeSingleEchoPos(echoRanges, minIdx, maxIdx, echoPos);
}

void EchoPositionComputer::ComputeSingleEchoPos(const std::vector<int> & echoRanges, int minIdx, int maxIdx, int & echoPos) const
{ 
	echoPos = -1;

	if (minIdx != -1 && maxIdx != -1)
	{
		int jEnd = std::min<int>(jMax, maxIdx);
		int jStart = std::max<int>(jMin, minIdx - 1);
		int * index_ptr = indexes + ((jMax - offset) * Xdim);
		int * channel_ptr = channelIndexes + ((jMax - offset) * Xdim);

		// assign noiseValues to max Range
		echoPos = jEnd;

		for (int j = jEnd; j > jStart; --j)
		{
			int index = *index_ptr;
			if (index >= 0 && index < indexMax)
			{
				int channelIndex = *channel_ptr;
				if (channelIndex >= 0 && channelIndex < indexMax)
				{
					int echoNum = index%sizeY;
					if (echoNum <= echoRanges[channelIndex])
					{
						// OTK - 30/03/2010 - on ne trace plus le fond directement sur l'image,
						// mais on sauvegarde sa position pour le tracer par dessus, en GDI+
						//SetPixel(fanIdx,j,blackARGB);
						echoPos = j;
						break;
					}
				}
			}
			index_ptr -= Xdim;
			channel_ptr -= Xdim;
		}
	}
}


void PaletteCommon::computeMaxValues(short * values, short * maxValues, int sizeX, int sizeY, TransformMap * pTransform)
{
	const int indexMax = sizeX * sizeY;
	memcpy(maxValues, values, sizeof(short) * indexMax);
	
	for (int iX = 0; iX < sizeX; ++iX)
	{
		const Vector2I p1 = pTransform->polarToCartesian2(Vector2I(iX, 0));
		const Vector2I p2 = pTransform->polarToCartesian2(Vector2I(iX, sizeY));

		const double xx = p2.x - p1.x;
		const double yy = p2.y - p1.y;
		const double ratio = (double)sizeY / sqrt(xx * xx + yy * yy);

		for (double iY = 0.0; iY < sizeY; iY += ratio)
		{
			const int y1 = std::min<int>(sizeY, (int)(iY + 0.5));
			const int y2 = std::min<int>(sizeY, (int)(iY + 0.5 + ratio));

			const int idx1 = iX * sizeY + y1;
			const int idx2 = iX * sizeY + y2;
			short maxValue = std::numeric_limits<short>::min();
			for (int idx = idx1; idx < idx2; ++idx)
			{
				maxValue = std::max<short>(maxValue, values[idx]);
			}

			for (int idx = idx1; idx < idx2; ++idx) 
			{
				maxValues[idx] = maxValue;
			}
		}
	}
}

void PaletteCommon::computeMaxAbsValues(short * values, short * maxValues, int sizeX, int sizeY, TransformMap * pTransform)
{
	const int indexMax = sizeX * sizeY;
	memcpy(maxValues, values, sizeof(short) * indexMax);

	for (int iX = 0; iX < sizeX; ++iX)
	{
		const Vector2I p1 = pTransform->polarToCartesian2(Vector2I(iX, 0));
		const Vector2I p2 = pTransform->polarToCartesian2(Vector2I(iX, sizeY));

		const double xx = p2.x - p1.x;
		const double yy = p2.y - p1.y;
		const double ratio = (double)sizeY / sqrt(xx * xx + yy * yy);

		for (double iY = 0.0; iY < sizeY; iY += ratio)
		{
			const int y1 = std::min(sizeY, (int)(iY + 0.5));
			const int y2 = std::min(sizeY, (int)(iY + 0.5 + ratio));

			const int idx1 = iX * sizeY + y1;
			const int idx2 = iX * sizeY + y2;

			short maxValue = 0;
			for (int idx = idx1; idx < idx2; ++idx)
			{		
				const auto current = values[idx];
				if (std::abs(current) > std::abs(maxValue))
				{
					maxValue = current;
				}
			}

			for (int idx = idx1; idx < idx2; ++idx)
			{
				maxValues[idx] = maxValue;
			}
		}
	}
}

PingFanRenderer::PingFanRenderer()
{
	defaultColor = System::Drawing::Color::White.ToArgb();
	backgroundColor = System::Drawing::Color::Black.ToArgb();
}

SinglePingFanRenderer::SinglePingFanRenderer()
{
	colorForIndexFunc = [this](int index) { return cache[values[index] + 32768]; };
}

void SinglePingFanRenderer::initialize(const IColorPalette * palette, short minThreshold, short maxThreshold)
{
	cache.resize(65536, defaultColor);

	int min = minThreshold;
	min += 32768;

	int max = maxThreshold;
	max += 32768;

	for (int i = 0; i < min; ++i)
	{
		cache[i] = defaultColor;
	}

	for (int i = min; i < max; ++i)
	{
		DataFmt value = i - 32768;
		unsigned int color = palette->GetColor(value);
		cache[i] = color | 0xFF000000;
	}

	for (int i = max; i < 65536; ++i)
	{
		cache[i] = defaultColor;
	}
}

void SvRenderer::update(DisplayParameter *pDisplay)
{
	bool hasChanged = true;

	defaultColor = pDisplay->GetEchogramBackgroundColor();
	auto palette = pDisplay->GetDisplay2DSingleEchoes()
		? pDisplay->GetTSEchoPalette()
		: pDisplay->GetCurrent2DEchoPalette();

	if (hasChanged)
	{
		initialize(palette, pDisplay->GetMinThreshold(), pDisplay->GetMaxThreshold());
	}
}

void SvRenderer::setupPingFan(PingFan * pFan, int render2DMode)
{
	MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(transducerIndex);
	MemoryObjectDataFmt * dataFmt = pMemStruct->GetDataFmt();

	switch (render2DMode)
	{
		case eMaxValueDisplay:
		{
			TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transducerIndex);
			Vector2I size = dataFmt->getSize();
			computedValues.resize(size.x * size.y);
			PaletteCommon::computeMaxValues(dataFmt->GetData(), computedValues.data(), size.x, size.y, pTransform);
			values = computedValues.data();
		}
		break;

		case eFastDisplay:
		default:
		{
			values = dataFmt->GetData();
		}
		break;
	}
}

void AngleRenderer::update(DisplayParameter *pDisplay)
{
	bool hasChanged = true;

	defaultColor = pDisplay->GetEchogramBackgroundColor();

	if (hasChanged)
	{
		RedBlueColorPalette palette;
		short range = pDisplay->GetAngleThreshold();

		palette.PreparePalette(-range, range);
		initialize(&palette, -range, range);
	}
}

template<typename T>
void AngleRenderer::setupPingFanT(PingFan * pFan, int render2DMode)
{
	MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(transducerIndex);
	MemoryObjectPhase * phase = pMemStruct->GetPhase();
	if (phase)
	{
		Vector2I size = phase->getSize();

		const int nbValues = size.x * size.y;

		std::vector<short> phaseValues;
		phaseValues.resize(nbValues);
		computedValues.resize(nbValues);
			
		Phase * data = phase->GetData();
		for (int i = 0; i < nbValues; ++i)
		{
			phaseValues[i] = T::get(data[i]);
		}

		if (render2DMode == eMaxValueDisplay)
		{
			TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transducerIndex);
			PaletteCommon::computeMaxAbsValues(phaseValues.data(), computedValues.data(), size.x, size.y, pTransform);
		}
		else
		{
			phaseValues.swap(computedValues);
		}
	}
	else
	{
		MemoryObjectDataFmt * dataFmt = pMemStruct->GetDataFmt();

		Vector2I size = dataFmt->getSize();

		const int nbValues = size.x * size.y;
		computedValues.resize(size.x * size.y, UNKNOWN_DB);
	}

	values = computedValues.data();
} 

struct PhaseAlongShipAngleGetter
{
	static inline short get(const Phase & data) { return data.GetRawAlongValue(); }
};

void AlongShipAngleRenderer::setupPingFan(PingFan * pFan, int render2DMode)
{
	setupPingFanT<PhaseAlongShipAngleGetter>(pFan, render2DMode);
}

struct PhaseAthwartShipAngleGetter
{
	static inline short get(const Phase & data) { return data.GetRawAthwartValue(); }
};

void AthwartShipAngleRenderer::setupPingFan(PingFan * pFan, int render2DMode)
{
	setupPingFanT<PhaseAthwartShipAngleGetter>(pFan, render2DMode);
}

ComposedPingFanRenderer::ComposedPingFanRenderer()
{
	colorForIndexFunc = [this](int index) 
	{
		if (valuesR[index] > m_minThresholdR && valuesR[index]<m_maxThresholdR && valuesG[index]>m_minThresholdG && valuesG[index]<m_maxThresholdG && valuesB[index]>m_minThresholdB && valuesB[index] < m_maxThresholdB)
			return cacheR[valuesR[index] + 32768] | cacheG[valuesG[index] + 32768] | cacheB[valuesB[index] + 32768] | 0xFF000000;
		else
			return backgroundColor;
	};
}

unsigned int ComposedPingFanRenderer::valueToColor(short value, int minValue, int maxValue)
{
	auto a = 255.0 / (double)(maxValue - minValue);
	auto b = -a * minValue;

	auto color = a *value + b;
	color = std::max<unsigned int>(0, std::min<unsigned int>(color, 255));

	return color;
}

void ComposedPingFanRenderer::initCache(std::vector<unsigned int> & cache, int minThreshold, int maxThreshold, int offset)
{
	cache.resize(65536, 0);
		
	for (int i = 0; i<65536; ++i)
	{
		int color = valueToColor(i - 32768, minThreshold, maxThreshold);
		cache[i] = (color << offset);
	}
}

void ComposedPingFanRenderer::setupPingFan(PingFan * pFan, int render2DMode)
{
	MemoryStruct *pMemStructR = pFan->GetMemorySetRef()->GetMemoryStruct(transducerIndexR);
	MemoryObjectDataFmt * dataFmtR = pMemStructR->GetDataFmt();

	MemoryStruct *pMemStructG = pFan->GetMemorySetRef()->GetMemoryStruct(transducerIndexG);
	MemoryObjectDataFmt * dataFmtG = pMemStructG->GetDataFmt();

	MemoryStruct *pMemStructB = pFan->GetMemorySetRef()->GetMemoryStruct(transducerIndexB);
	MemoryObjectDataFmt * dataFmtB = pMemStructB->GetDataFmt();
	
	switch (render2DMode)
	{
		case eMaxValueDisplay:
		{
			TransformMap *pTransform = pFan->getSounderRef()->GetTransformSetRef()->GetTransformMap(transducerIndexR);

			Vector2I sizeR = dataFmtR->getSize();
			computedValuesR.resize(sizeR.x * sizeR.y);
			PaletteCommon::computeMaxValues(dataFmtR->GetData(), computedValuesR.data(), sizeR.x, sizeR.y, pTransform);
			valuesR = computedValuesR.data();

			Vector2I sizeG = dataFmtG->getSize();
			computedValuesG.resize(sizeG.x * sizeG.y);
			PaletteCommon::computeMaxValues(dataFmtG->GetData(), computedValuesG.data(), sizeG.x, sizeG.y, pTransform);
			valuesG = computedValuesG.data();

			Vector2I sizeB = dataFmtB->getSize();
			computedValuesB.resize(sizeB.x * sizeB.y);
			PaletteCommon::computeMaxValues(dataFmtB->GetData(), computedValuesB.data(), sizeB.x, sizeB.y, pTransform);
			valuesB = computedValuesB.data();
		}
		break;

		case eFastDisplay:
		default:
		{
			valuesR = dataFmtR->GetData();
			valuesG = dataFmtG->GetData();
			valuesB = dataFmtB->GetData();
		}
		break;
	}
}

void ComposedPingFanRenderer::update(DisplayParameter *pDisplay)
{
}

void ComposedPingFanRenderer::initialize(int minThresholdR, int maxThresholdR, int minThresholdG, int maxThresholdG, int minThresholdB, int maxThresholdB)
{
	defaultColor = System::Drawing::Color::White.ToArgb();
	backgroundColor = System::Drawing::Color::Black.ToArgb();

	m_minThresholdR = minThresholdR;
	m_maxThresholdR = maxThresholdR;
	m_minThresholdG = minThresholdG;
	m_maxThresholdG = maxThresholdG;
	m_minThresholdB = minThresholdB;
	m_maxThresholdB = maxThresholdB;

	initCache(cacheR, minThresholdR, maxThresholdR, 16);
	initCache(cacheG, minThresholdG, maxThresholdG, 8);
	initCache(cacheB, minThresholdB, maxThresholdB, 0);
}
