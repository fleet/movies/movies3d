#pragma once

#include <vector>
#include <map>
#include <functional>
#include "BaseMathLib/Vector2.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "MultifrequencyEchogram.h"

class Transducer;
class PingFan;
class SoftChannel;
class TransformMap;
class ShoalExtractionOutputList;
class IColorPalette;
class DisplayParameter;

namespace shoalextraction
{
	class ShoalData;
	class PingData;
}

typedef std::pair<shoalextraction::ShoalData*, shoalextraction::PingData*> TShoalPingInfo;


struct PingFanRenderer;

class TransducerView abstract
{
public:
	enum DisplayDataType
	{
		Sv,
		AlongShipAngle,
		AthwartShipAngle
	};

	TransducerView(std::uint32_t sounderId, unsigned int transducerIndex, DisplayDataType displayDataType);
	TransducerView(std::uint32_t sounderId, unsigned int transducerIndex
		, unsigned int transducerIndex2, unsigned int transducerIndex3
		, const MultifrequencyEchogram& coloredEchogram); ///<For colored echogram

	virtual ~TransducerView(void);

	void LockDatas();
	void UnlockDatas();

	Transducer *GetTransducer();
	unsigned int	GetTransducerIndex() const { return m_transducerIndex; };

	bool IsActive() { return m_Active; }
	void SetActive(bool a);
	std::uint32_t GetSounderId() { return m_sounderId; }

	std::string getName();

	//virtual void PingFanAdded(PingFan *pFan, unsigned long sounderID)=0;
	virtual void PingFanAdded(int width, int height) = 0;
	virtual void EsuClosed();

	/** called in case of parameter change*/
	void CheckForUpdate();
	void ForceUpdate();

	virtual bool GetImageChanged() { return m_ImageChanged; }
	virtual void ResetImageChanged() { m_ImageChanged = false; }

	virtual void UpdateTransformMap(int width, int height, bool stretched);
	unsigned int GetWidth() { return m_Width; }
	unsigned int GetHeight() { return m_Height; }
	unsigned int GetByteSize() { return m_bytesSize; }
	unsigned int GetStartPixel() { return m_StartPixelRange; }
	unsigned int GetStopPixel() { return m_StopPixelRange; }
	unsigned int GetStartHPixel() { return m_StartPixelHRange; }
	unsigned int GetStopHPixel() { return m_StopPixelHRange; }
	unsigned int GetTotalRange() { return m_TotalRange; }
	double GetStartPixelDepth() { return m_StartPixelDepth; }
	double GetStopPixelDepth() { return m_StopPixelDepth; }
	int* GetPtr() { return m_rgbValues; }

	bool isColoredEchogram() const { return m_coloredEchogram.isValid(); }

	//shoal consumer
	static const ShoalExtractionOutputList * GetShoalList() { return m_pShoalList; };
	static void SetShoalList(const ShoalExtractionOutputList * pShoalList) { m_pShoalList = pShoalList; };

protected:

	//get the filtered shoals for the given pingId and the current sounder and transducer
	std::vector<shoalextraction::ShoalData*> GetFilteredShoals(std::uint64_t pingMinId, std::uint64_t pingMaxId);

	//get the filtered shoals for the given pingId and the current sounder and transducer
	std::vector<TShoalPingInfo> GetFilteredShoalsInfo(std::uint64_t pingId);

	//Draw the line
	void DrawLine(int x1, int y1, int x2, int y2,
		System::Drawing::Color color,
		bool erase = false);

	//Draw the line
	void DrawLine(BaseMathLib::Vector2I& pointI,
		BaseMathLib::Vector2I& pointJ,
		System::Drawing::Color color,
		bool erase = false);

	// this flags tell if a new image has been computed
	bool			m_ImageChanged;
	virtual void ComputeAll() = 0;

	virtual bool AllocateBmp(unsigned int W, unsigned int H);
	inline void SetPixel(const unsigned int & widthIdx, const unsigned int & profAsPixel, System::Drawing::Color Color)
	{
		if (profAsPixel >= m_StartPixelRange && profAsPixel < m_StopPixelRange
			&& widthIdx >= m_StartPixelHRange && widthIdx < m_StopPixelHRange)
		{
			// just in case
			const size_t pos = m_Width *(profAsPixel - m_StartPixelRange) + widthIdx - m_StartPixelHRange;
			if (pos < m_bytesSize)
			{
				m_rgbValues[pos] = Color.ToArgb();
			}
		}
	}
	inline void SetPixel(const unsigned int & widthIdx, const unsigned int & profAsPixel, const int & argbColor)
	{
		if (profAsPixel >= m_StartPixelRange && profAsPixel < m_StopPixelRange
			&& widthIdx >= m_StartPixelHRange && widthIdx < m_StopPixelHRange)
		{
			// just in case
			const size_t pos = m_Width *(profAsPixel - m_StartPixelRange) + widthIdx - m_StartPixelHRange;
			if (pos < m_bytesSize)
			{
				m_rgbValues[pos] = argbColor;
			}
		}
	}
	void SetAlphaPixel(const unsigned int & widthIdx, const unsigned int & profAsPixel, System::Drawing::Color Color);
	void EraseAlphaPixel(const unsigned int & widthIdx, const unsigned int & profAsPixel, System::Drawing::Color Color);
	void SetAlphaPixel(const unsigned int & widthIdx, const unsigned int & profAsPixel, double alpha, int red, int green, int blue);

	void ResetImage(System::Drawing::Color^ color);
	void ResetImage(const int color);

	System::String^ FormatEchoToolTip(PingFan *pFan, int x, int echoDepth);

	System::Drawing::Color ^GetBackgroundColor();

	PingFanRenderer * pingFanRenderer;

	const DisplayDataType m_displayDataType;
	const unsigned long	m_sounderId;
	const unsigned int	m_transducerIndex;
	const unsigned int	m_transducerIndex2; ///<For colored echogram
	const unsigned int	m_transducerIndex3; ///<For colored echogram
	const MultifrequencyEchogram m_coloredEchogram;


	bool			m_Active;

	unsigned int m_StartPixelRange;
	unsigned int m_StopPixelRange;
	// OTK - 10/06/2009 - on a aussi un range de pixels horizontal avec le zoom horizontal
	unsigned int m_StartPixelHRange;
	unsigned int m_StopPixelHRange;
	unsigned int m_TotalRange; // taille des donn�es de l'image non zoom�e

	double m_StartPixelDepth;
	double m_StopPixelDepth;

	CRecursiveMutex *m_pLock;
	unsigned int m_bytesSize;


	unsigned int m_Width;
	unsigned int m_Height;
	int*	m_rgbValues;
	
	//shoal consumer
	static const ShoalExtractionOutputList * m_pShoalList;
};

// Classe utilitaire pour mutualiser les calcul des positions d'un echo dans l'affichage 2D
struct EchoPositionComputer
{
	Transducer * transducer;
	SoftChannel* channel;
	int * indexes;
	int * channelIndexes;
	int jMin, jMax;
	unsigned int Xdim, Ydim, maxTabIndex;
	int sizeX, sizeY, indexMax;
	int offset;

	// Estime la position Y d'un echo dans l'image 2D 
	void EstimatePixelRangeForEcho(int echoNum, int & minIdx, int & maxIdx) const;

	// Estime la position Y d'un ensemble d'echos dans l'image 2D
	void EstimatePixelRangeForEcho(const std::vector<int>& echoNums, int & minIdx, int & maxIdx) const;

	// Calcul les positions d'un ensemble d'echos dans l'image 2D
	void ComputeEchoPos(const std::vector<int> & echoRanges, std::vector<BaseMathLib::Vector2I> & points) const;

	// Calcul les positions d'un ensemble d'echos dans l'image 2D, la position est calcul�e entre minIdx et maxIdx
	void ComputeEchoPos(const std::vector<int> & echoRanges, int minIdx, int maxIdx, std::vector<BaseMathLib::Vector2I> & points) const;

	void ComputeSingleEchoPos(const std::vector<int> & echoRanges, int & echoPos) const;

	void ComputeSingleEchoPos(const std::vector<int> & echoRanges, int minIdx, int maxIdx, int & echoPos) const;
};

// Classe utilitaire pour le sous-�chantillonage de la colonne d'eau en utilisant la valeur max
struct PaletteCommon
{
	static void computeMaxValues(short * values, short * dest, int sizeX, int sizeY, TransformMap * pTransform);
	static void computeMaxAbsValues(short * values, short * dest, int sizeX, int sizeY, TransformMap * pTransform);
};

// Classe de base pour le calcul de la couleur d'un pixel 
struct PingFanRenderer
{
	unsigned int defaultColor;
	unsigned int backgroundColor;

	PingFanRenderer();

	virtual void update(DisplayParameter *pDisplay) = 0;

	virtual void setupPingFan(PingFan * pFan, int render2DMode) = 0;
	
	std::function<unsigned int(int)> colorForIndexFunc;
	
	inline unsigned int getColorForIndex(int index) const { return colorForIndexFunc(index); }
};

struct SinglePingFanRenderer : public PingFanRenderer
{
	SinglePingFanRenderer();

	unsigned int transducerIndex;
	std::vector<unsigned int> cache;
	short *values;
	std::vector<short> computedValues;

	void initialize(const IColorPalette * palette, short minThreshold, short maxThreshold);
};

// Classe pour le calcul de la couleur d'un pixel (mode nominal)
struct SvRenderer : public SinglePingFanRenderer
{
	void update(DisplayParameter *pDisplay);

	void setupPingFan(PingFan * pFan, int render2DMode);
};

struct AngleRenderer : public SinglePingFanRenderer
{
	void update(DisplayParameter *pDisplay);

	template<typename T>
	void setupPingFanT(PingFan * pFan, int render2DMode);
};

struct AlongShipAngleRenderer : public AngleRenderer
{
	void setupPingFan(PingFan * pFan, int render2DMode);
};

struct AthwartShipAngleRenderer : public AngleRenderer
{
	void setupPingFan(PingFan * pFan, int render2DMode);
};

// Classe pour le calcul de la couleur d'un pixel des �chogrammes compos�s
struct ComposedPingFanRenderer : public PingFanRenderer
{
	ComposedPingFanRenderer();

	unsigned int defaultColor;
	unsigned int backgroundColor;

	unsigned int transducerIndexR;
	unsigned int transducerIndexG;
	unsigned int transducerIndexB;

	int m_minThresholdR;
	int m_maxThresholdR;
	int m_minThresholdG;
	int m_maxThresholdG;
	int m_minThresholdB;
	int m_maxThresholdB;

	short *valuesR;
	short* valuesG;
	short* valuesB;

	std::vector<short> computedValuesR;
	std::vector<short> computedValuesG;
	std::vector<short> computedValuesB;

	std::vector<unsigned int> cacheR;
	std::vector<unsigned int> cacheG;
	std::vector<unsigned int> cacheB;

	static unsigned int valueToColor(short value, int minValue, int maxValue);
	static void initCache(std::vector<unsigned int> & cache, int minThreshold, int maxThreshold, int offset);
	void initialize(int minThresholdR, int maxThresholdR, int minThresholdG, int maxThresholdG, int minThresholdB, int maxThresholdB);

	void update(DisplayParameter *pDisplay);

	void setupPingFan(PingFan * pFan, int render2DMode);
};