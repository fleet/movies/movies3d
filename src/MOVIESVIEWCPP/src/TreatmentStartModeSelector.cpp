﻿#include "TreatmentStartModeSelector.h"

using namespace MOVIESVIEWCPP;

namespace
{
	constexpr const char * LoggerName = "MoviesView.TreatmentStartModeSelector";
}

TreatmentStartModeSelector::TreatmentStartModeSelector(ProcessModule * processModule, ReaderCtrlManaged^ %readerCtrl)
{
	InitializeComponent();

	// onr�cup�re le run courant
	if (readerCtrl->IsFileService())
	{
		// R�cup�ration des informations sur le run (temps min max et ping min max)
		HacTime minTime, maxTime;
		std::uint32_t minPing, maxPing;
		readerCtrl->GetLimits(minTime, minPing, maxTime, maxPing);

		// R�cup�ration de la position courante
		HacTime * currentTime = NULL;
		std::uint64_t currentPingNo = 0;
		size_t maxFan = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectCount();
		if (maxFan)
		{
			PingFan * pFan = (PingFan*)M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(maxFan - 1);
			currentTime = &pFan->m_ObjectTime;
			currentPingNo = pFan->GetFilePingId();
		}
		SetLimits(currentTime, &maxTime);
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "Aucun fichier en lecture...");
	}

	m_pModule = processModule;
}

// met à jour les composants en fonction des données
void TreatmentStartModeSelector::UpdateComponents()
{
	// mise à jour de l'indication des limites possibles
	char date[255];
	m_MinAvailableTime->GetTimeDesc(date, 254);
	System::String ^statText = gcnew System::String(date);
	m_MinTimeLimit->Text = statText;
	m_MaxAvailableTime->GetTimeDesc(date, 254);
	statText = gcnew System::String(date);
	m_MaxTimeLimit->Text = statText;
	//m_MinPingLimit->Text = Convert::ToString(m_MinAvailablePing);
	//m_MaxPingLimit->Text = Convert::ToString(m_MaxAvailablePing);

	// mise à jour des composants pour le choix de la destination
	this->m_DayDateTimePicker->Value = Convert::ToDateTime(m_DestinationDate);
	this->m_HourDateTimePicker->Value = Convert::ToDateTime(m_DestinationDate);

	// mise à jour des composants dynamiques
	this->m_DayDateTimePicker->Enabled = this->radioButtonTime->Checked;
	this->m_HourDateTimePicker->Enabled = this->radioButtonTime->Checked;
}

// défini les limites min et max disponibles
void TreatmentStartModeSelector::SetLimits(HacTime *minTime,
	HacTime *maxTime)
{
	m_MinAvailableTime = minTime;
	m_MaxAvailableTime = maxTime;

	// si le temps courant est défini on s'en sert pour regler
	// la destination
	DateTime^ date1970 = gcnew DateTime(1970, 01, 01, 0, 0, 0, 0);
	std::int64_t sec = 10000000L;

	DateTime^ MoviesDateStart = gcnew DateTime(
		m_MinAvailableTime->m_TimeFraction * 1000L + m_MinAvailableTime->m_TimeCpu*sec);

	m_DestinationDate = gcnew DateTime(MoviesDateStart->Ticks + date1970->Ticks);

	// on ne permet pas d'outrepasser les limites au niveau des composants
	DateTime^ MoviesDate = gcnew DateTime(
		m_MinAvailableTime->m_TimeFraction * 1000L + m_MinAvailableTime->m_TimeCpu*sec);
	this->m_DayDateTimePicker->MinDate = DateTime(MoviesDate->Ticks + date1970->Ticks);
	this->m_HourDateTimePicker->MinDate = DateTime(MoviesDate->Ticks + date1970->Ticks);
	MoviesDate = gcnew DateTime(
		m_MaxAvailableTime->m_TimeFraction * 1000L + m_MaxAvailableTime->m_TimeCpu*sec);
	this->m_DayDateTimePicker->MaxDate = DateTime(MoviesDate->Ticks + date1970->Ticks);
	this->m_HourDateTimePicker->MaxDate = DateTime(MoviesDate->Ticks + date1970->Ticks);

	// mise à jour du panneau IHM en fonction des parametres
	UpdateComponents();
}

// renvoi les paramètres choisis pour la date
HacTime* TreatmentStartModeSelector::GetTarget()
{
	HacTime* targetTime = new HacTime();
	// pour la date c'est un peu plus compliqué :
	DateTime^ TargetDate = gcnew DateTime(m_DayDateTimePicker->Value.Year, m_DayDateTimePicker->Value.Month,
		m_DayDateTimePicker->Value.Day, m_HourDateTimePicker->Value.Hour, m_HourDateTimePicker->Value.Minute,
		m_HourDateTimePicker->Value.Second, m_HourDateTimePicker->Value.Millisecond);
	// on doit ajouter les microsecondes
	std::int64_t microsec = m_HourDateTimePicker->Value.Ticks % (std::int64_t)10000L;
	TargetDate = TargetDate->AddTicks(microsec);
	DateTime^ date1970 = gcnew DateTime(1970, 01, 01, 0, 0, 0, 0);
	TargetDate = gcnew DateTime(TargetDate->Ticks - date1970->Ticks);
	std::int64_t sec = 10000000L;
	targetTime->m_TimeCpu = (std::uint32_t)(TargetDate->Ticks / sec);
	std::int64_t secFraction = ((TargetDate->Ticks - targetTime->m_TimeCpu*sec)) / 1000L;
	targetTime->m_TimeFraction = (unsigned short)secFraction;

	return targetTime;
}

System::Void TreatmentStartModeSelector::buttonCancel_Click(System::Object^  sender, System::EventArgs^  e)
{
	Close();
}

System::Void TreatmentStartModeSelector::buttonOK_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (radioButtonImmediate->Checked) // mode immédiat
	{
		// on commence par activer le module (qui ne fait normalement rien
		// tant qu'il ne recoit pas d'ESUStart
		m_pModule->setEnable(true);
		// on force le démarrage d'un nouvel ESU
		MovESUMgr * pMovESUMgr = M3DKernel::GetInstance()->getMovESUManager();
		pMovESUMgr->ForceNewESU();
	}
	else if (radioButtonNextESU->Checked) // mode différé (prochain ESU)
	{
		// il suffit d'activer le module, qui commencera son traitement sur 
		// le premier ESUStart recu
		m_pModule->setEnable(true);
	}
	else
	{
		m_pModule->setStartTime(GetTarget());
	}

	Close();
}

System::Void TreatmentStartModeSelector::radioButtonTime_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	m_DayDateTimePicker->Enabled = radioButtonTime->Checked;
	m_HourDateTimePicker->Enabled = radioButtonTime->Checked;
}