#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

// d�pendances
#include "M3DKernel/module/ProcessModule.h"
#include "M3DKernel/M3DKernel.h"

#include "ReaderCtrlManaged.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de TreatmentStartModeSelector
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class TreatmentStartModeSelector : public System::Windows::Forms::Form
	{

	private:
		// d�fini les limites min et max disponibles
		void SetLimits(HacTime *minTime,
			HacTime *maxTime);
		// met � jour les composants en fonction des donn�es
		void UpdateComponents();

		// bornes possibles
		HacTime *m_MinAvailableTime, *m_MaxAvailableTime;
		//unsigned int m_MinAvailablePing, m_MaxAvailablePing;

		// destination choisie
		DateTime^ m_DestinationDate;

		HacTime* GetTarget();

	public:
		TreatmentStartModeSelector(ProcessModule * processModule, ReaderCtrlManaged^ %readerCtrl);

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~TreatmentStartModeSelector()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::RadioButton^  radioButtonImmediate;
	protected:
	private: System::Windows::Forms::RadioButton^  radioButtonNextESU;
	private: System::Windows::Forms::Button^  buttonCancel;

	private: System::Windows::Forms::Button^  buttonOK;


	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;
	private: System::Windows::Forms::RadioButton^  radioButtonTime;

	private: System::Windows::Forms::GroupBox^  m_LimitsGroupBox;


	private: System::Windows::Forms::Label^  m_MaxTimeLimit;
	private: System::Windows::Forms::Label^  m_MinTimeLimit;
	private: System::Windows::Forms::Label^  m_MaxTimeLimitLabel;
	private: System::Windows::Forms::Label^  m_MinTimeLimitLabel;
	private: System::Windows::Forms::DateTimePicker^  m_DayDateTimePicker;
	private: System::Windows::Forms::DateTimePicker^  m_HourDateTimePicker;

			 /// <summary>
			 /// Pointeur vers le module de traitement associ�au panneau IHM.
			 /// </summary>
			 ProcessModule * m_pModule;

#pragma region Windows Form Designer generated code
			 /// <summary>
			 /// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
			 /// le contenu de cette m�thode avec l'�diteur de code.
			 /// </summary>
			 void InitializeComponent(void)
			 {
				 this->radioButtonImmediate = (gcnew System::Windows::Forms::RadioButton());
				 this->radioButtonNextESU = (gcnew System::Windows::Forms::RadioButton());
				 this->buttonCancel = (gcnew System::Windows::Forms::Button());
				 this->buttonOK = (gcnew System::Windows::Forms::Button());
				 this->radioButtonTime = (gcnew System::Windows::Forms::RadioButton());
				 this->m_LimitsGroupBox = (gcnew System::Windows::Forms::GroupBox());
				 this->m_MaxTimeLimit = (gcnew System::Windows::Forms::Label());
				 this->m_MinTimeLimit = (gcnew System::Windows::Forms::Label());
				 this->m_MaxTimeLimitLabel = (gcnew System::Windows::Forms::Label());
				 this->m_MinTimeLimitLabel = (gcnew System::Windows::Forms::Label());
				 this->m_DayDateTimePicker = (gcnew System::Windows::Forms::DateTimePicker());
				 this->m_HourDateTimePicker = (gcnew System::Windows::Forms::DateTimePicker());
				 this->m_LimitsGroupBox->SuspendLayout();
				 this->SuspendLayout();
				 // 
				 // radioButtonImmediate
				 // 
				 this->radioButtonImmediate->AutoSize = true;
				 this->radioButtonImmediate->Checked = true;
				 this->radioButtonImmediate->Location = System::Drawing::Point(29, 12);
				 this->radioButtonImmediate->Name = L"radioButtonImmediate";
				 this->radioButtonImmediate->Size = System::Drawing::Size(72, 17);
				 this->radioButtonImmediate->TabIndex = 2;
				 this->radioButtonImmediate->TabStop = true;
				 this->radioButtonImmediate->Text = L"Start Now";
				 this->radioButtonImmediate->UseVisualStyleBackColor = true;
				 // 
				 // radioButtonNextESU
				 // 
				 this->radioButtonNextESU->AutoSize = true;
				 this->radioButtonNextESU->Location = System::Drawing::Point(29, 35);
				 this->radioButtonNextESU->Name = L"radioButtonNextESU";
				 this->radioButtonNextESU->Size = System::Drawing::Size(97, 17);
				 this->radioButtonNextESU->TabIndex = 3;
				 this->radioButtonNextESU->Text = L"Start Next ESU";
				 this->radioButtonNextESU->UseVisualStyleBackColor = true;
				 // 
				 // buttonCancel
				 // 
				 this->buttonCancel->Location = System::Drawing::Point(158, 200);
				 this->buttonCancel->Name = L"buttonCancel";
				 this->buttonCancel->Size = System::Drawing::Size(90, 25);
				 this->buttonCancel->TabIndex = 1;
				 this->buttonCancel->Text = L"Cancel";
				 this->buttonCancel->UseVisualStyleBackColor = true;
				 this->buttonCancel->Click += gcnew System::EventHandler(this, &TreatmentStartModeSelector::buttonCancel_Click);
				 // 
				 // buttonOK
				 // 
				 this->buttonOK->Location = System::Drawing::Point(12, 200);
				 this->buttonOK->Name = L"buttonOK";
				 this->buttonOK->Size = System::Drawing::Size(90, 25);
				 this->buttonOK->TabIndex = 0;
				 this->buttonOK->Text = L"OK";
				 this->buttonOK->UseVisualStyleBackColor = true;
				 this->buttonOK->Click += gcnew System::EventHandler(this, &TreatmentStartModeSelector::buttonOK_Click);
				 // 
				 // radioButtonTime
				 // 
				 this->radioButtonTime->AutoSize = true;
				 this->radioButtonTime->Location = System::Drawing::Point(29, 58);
				 this->radioButtonTime->Name = L"radioButtonTime";
				 this->radioButtonTime->Size = System::Drawing::Size(138, 17);
				 this->radioButtonTime->TabIndex = 4;
				 this->radioButtonTime->Text = L"Start at Date and Time :";
				 this->radioButtonTime->UseVisualStyleBackColor = true;
				 this->radioButtonTime->CheckedChanged += gcnew System::EventHandler(this, &TreatmentStartModeSelector::radioButtonTime_CheckedChanged);
				 // 
				 // m_LimitsGroupBox
				 // 
				 this->m_LimitsGroupBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
					 | System::Windows::Forms::AnchorStyles::Right));
				 this->m_LimitsGroupBox->Controls->Add(this->m_MaxTimeLimit);
				 this->m_LimitsGroupBox->Controls->Add(this->m_MinTimeLimit);
				 this->m_LimitsGroupBox->Controls->Add(this->m_MaxTimeLimitLabel);
				 this->m_LimitsGroupBox->Controls->Add(this->m_MinTimeLimitLabel);
				 this->m_LimitsGroupBox->Location = System::Drawing::Point(29, 116);
				 this->m_LimitsGroupBox->Name = L"m_LimitsGroupBox";
				 this->m_LimitsGroupBox->Size = System::Drawing::Size(219, 69);
				 this->m_LimitsGroupBox->TabIndex = 5;
				 this->m_LimitsGroupBox->TabStop = false;
				 this->m_LimitsGroupBox->Text = L"Available destination range";
				 // 
				 // m_MaxTimeLimit
				 // 
				 this->m_MaxTimeLimit->AutoSize = true;
				 this->m_MaxTimeLimit->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
				 this->m_MaxTimeLimit->Location = System::Drawing::Point(69, 42);
				 this->m_MaxTimeLimit->Name = L"m_MaxTimeLimit";
				 this->m_MaxTimeLimit->Size = System::Drawing::Size(139, 15);
				 this->m_MaxTimeLimit->TabIndex = 3;
				 this->m_MaxTimeLimit->Text = L"10/10/1000 16:16:16.6666";
				 // 
				 // m_MinTimeLimit
				 // 
				 this->m_MinTimeLimit->AutoSize = true;
				 this->m_MinTimeLimit->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
				 this->m_MinTimeLimit->Location = System::Drawing::Point(69, 20);
				 this->m_MinTimeLimit->Name = L"m_MinTimeLimit";
				 this->m_MinTimeLimit->Size = System::Drawing::Size(139, 15);
				 this->m_MinTimeLimit->TabIndex = 2;
				 this->m_MinTimeLimit->Text = L"10/10/1000 16:16:16.6666";
				 // 
				 // m_MaxTimeLimitLabel
				 // 
				 this->m_MaxTimeLimitLabel->AutoSize = true;
				 this->m_MaxTimeLimitLabel->Location = System::Drawing::Point(11, 42);
				 this->m_MaxTimeLimitLabel->Name = L"m_MaxTimeLimitLabel";
				 this->m_MaxTimeLimitLabel->Size = System::Drawing::Size(52, 13);
				 this->m_MaxTimeLimitLabel->TabIndex = 1;
				 this->m_MaxTimeLimitLabel->Text = L"End limit :";
				 // 
				 // m_MinTimeLimitLabel
				 // 
				 this->m_MinTimeLimitLabel->AutoSize = true;
				 this->m_MinTimeLimitLabel->Location = System::Drawing::Point(11, 20);
				 this->m_MinTimeLimitLabel->Name = L"m_MinTimeLimitLabel";
				 this->m_MinTimeLimitLabel->Size = System::Drawing::Size(55, 13);
				 this->m_MinTimeLimitLabel->TabIndex = 0;
				 this->m_MinTimeLimitLabel->Text = L"Start limit :";
				 // 
				 // m_DayDateTimePicker
				 // 
				 this->m_DayDateTimePicker->Enabled = false;
				 this->m_DayDateTimePicker->Format = System::Windows::Forms::DateTimePickerFormat::Short;
				 this->m_DayDateTimePicker->Location = System::Drawing::Point(45, 81);
				 this->m_DayDateTimePicker->Name = L"m_DayDateTimePicker";
				 this->m_DayDateTimePicker->ShowUpDown = true;
				 this->m_DayDateTimePicker->Size = System::Drawing::Size(89, 20);
				 this->m_DayDateTimePicker->TabIndex = 7;
				 // 
				 // m_HourDateTimePicker
				 // 
				 this->m_HourDateTimePicker->Enabled = false;
				 this->m_HourDateTimePicker->Format = System::Windows::Forms::DateTimePickerFormat::Time;
				 this->m_HourDateTimePicker->Location = System::Drawing::Point(140, 81);
				 this->m_HourDateTimePicker->Name = L"m_HourDateTimePicker";
				 this->m_HourDateTimePicker->ShowUpDown = true;
				 this->m_HourDateTimePicker->Size = System::Drawing::Size(73, 20);
				 this->m_HourDateTimePicker->TabIndex = 6;
				 // 
				 // TreatmentStartModeSelector
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(260, 237);
				 this->Controls->Add(this->m_DayDateTimePicker);
				 this->Controls->Add(this->m_HourDateTimePicker);
				 this->Controls->Add(this->m_LimitsGroupBox);
				 this->Controls->Add(this->radioButtonTime);
				 this->Controls->Add(this->buttonOK);
				 this->Controls->Add(this->buttonCancel);
				 this->Controls->Add(this->radioButtonNextESU);
				 this->Controls->Add(this->radioButtonImmediate);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
				 this->Name = L"TreatmentStartModeSelector";
				 this->Text = L"Starting Mode";
				 this->m_LimitsGroupBox->ResumeLayout(false);
				 this->m_LimitsGroupBox->PerformLayout();
				 this->ResumeLayout(false);
				 this->PerformLayout();

			 }
#pragma endregion

			 // annulation du d�marrage du traitement
	private: System::Void buttonCancel_Click(System::Object^  sender, System::EventArgs^  e);

			 // d�marrage du traitement en fonction du mode choisi
	private: System::Void buttonOK_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radioButtonTime_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	};
}
