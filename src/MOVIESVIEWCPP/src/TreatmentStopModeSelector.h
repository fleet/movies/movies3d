#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

// d�pendances
#include "M3DKernel/module/ProcessModule.h"
#include "M3DKernel/M3DKernel.h"


namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de TreatmentStopSelector
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class TreatmentStopModeSelector : public System::Windows::Forms::Form
	{
	public:
		TreatmentStopModeSelector(ProcessModule * processModule)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//

			m_pModule = processModule;
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~TreatmentStopModeSelector()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  buttonOK;
	protected:
	private: System::Windows::Forms::Button^  buttonCancel;
	private: System::Windows::Forms::RadioButton^  radioButtonNextESU;
	private: System::Windows::Forms::RadioButton^  radioButtonImmediate;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

		/// <summary>
		/// Pointeur vers le module de traitement associ�au panneau IHM.
		/// </summary>
		ProcessModule * m_pModule;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->buttonOK = (gcnew System::Windows::Forms::Button());
			this->buttonCancel = (gcnew System::Windows::Forms::Button());
			this->radioButtonNextESU = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonImmediate = (gcnew System::Windows::Forms::RadioButton());
			this->SuspendLayout();
			// 
			// buttonOK
			// 
			this->buttonOK->Location = System::Drawing::Point(12, 75);
			this->buttonOK->Name = L"buttonOK";
			this->buttonOK->Size = System::Drawing::Size(90, 25);
			this->buttonOK->TabIndex = 0;
			this->buttonOK->Text = L"OK";
			this->buttonOK->UseVisualStyleBackColor = true;
			this->buttonOK->Click += gcnew System::EventHandler(this, &TreatmentStopModeSelector::buttonOK_Click);
			// 
			// buttonCancel
			// 
			this->buttonCancel->Location = System::Drawing::Point(123, 75);
			this->buttonCancel->Name = L"buttonCancel";
			this->buttonCancel->Size = System::Drawing::Size(90, 25);
			this->buttonCancel->TabIndex = 1;
			this->buttonCancel->Text = L"Cancel";
			this->buttonCancel->UseVisualStyleBackColor = true;
			this->buttonCancel->Click += gcnew System::EventHandler(this, &TreatmentStopModeSelector::buttonCancel_Click);
			// 
			// radioButtonNextESU
			// 
			this->radioButtonNextESU->AutoSize = true;
			this->radioButtonNextESU->Location = System::Drawing::Point(29, 39);
			this->radioButtonNextESU->Name = L"radioButtonNextESU";
			this->radioButtonNextESU->Size = System::Drawing::Size(97, 17);
			this->radioButtonNextESU->TabIndex = 5;
			this->radioButtonNextESU->Text = L"Stop Next ESU";
			this->radioButtonNextESU->UseVisualStyleBackColor = true;
			// 
			// radioButtonImmediate
			// 
			this->radioButtonImmediate->AutoSize = true;
			this->radioButtonImmediate->Checked = true;
			this->radioButtonImmediate->Location = System::Drawing::Point(29, 12);
			this->radioButtonImmediate->Name = L"radioButtonImmediate";
			this->radioButtonImmediate->Size = System::Drawing::Size(72, 17);
			this->radioButtonImmediate->TabIndex = 4;
			this->radioButtonImmediate->TabStop = true;
			this->radioButtonImmediate->Text = L"Stop Now";
			this->radioButtonImmediate->UseVisualStyleBackColor = true;
			// 
			// TreatmentStopModeSelector
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(225, 112);
			this->Controls->Add(this->buttonOK);
			this->Controls->Add(this->buttonCancel);
			this->Controls->Add(this->radioButtonNextESU);
			this->Controls->Add(this->radioButtonImmediate);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
			this->Name = L"TreatmentStopModeSelector";
			this->Text = L"Stop Mode";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		// annulation de l'arret du traitement
	private: System::Void buttonCancel_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
			 // confirmation de l'arr�t du traitement selon le mode choisi
	private: System::Void buttonOK_Click(System::Object^  sender, System::EventArgs^  e) {
		if (radioButtonImmediate->Checked) // mode imm�diat
		{
			m_pModule->setEnable(false);
		}
		else // mode diff�r� (prochain ESU)
		{
			m_pModule->DisableForNextESU();
		}

		Close();
	}
	};
}
