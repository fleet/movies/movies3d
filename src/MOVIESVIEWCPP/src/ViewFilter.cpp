#include "ViewFilter.h"
#include "PingFan3DContainer.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
ViewFilter::ViewFilter(void)
{
	m_bDisplayEcho = true;
	m_bDisplaySingleEcho = false;

	// NMD - 08/09/2011 - FAE 092 - modification de la visibilt� des volumes insonifi�es
	m_bDisplayVolumeSampled = false;
	//	m_bUseChannelIdAsScalar=false;
}

ViewFilter::~ViewFilter(void)
{
	RemoveAll();
}
Sounder3D* ViewFilter::addSounder(std::uint32_t sounderId)
{
	Sounder3D* p_SounderToAdd = getSounder(sounderId);
	if (p_SounderToAdd)
		return p_SounderToAdd;

	p_SounderToAdd = new Sounder3D(sounderId);
	m_pSounderVector.push_back(p_SounderToAdd);
	return p_SounderToAdd;
}
void ViewFilter::removeSounder(std::uint32_t sounderId)
{
	std::vector<Sounder3D *>::iterator Iter;

	for (Iter = m_pSounderVector.begin(); Iter != m_pSounderVector.end(); Iter++)
	{
		if ((*Iter)->getSounderID() == sounderId)
		{
			delete *Iter;
			m_pSounderVector.erase(Iter);
			break;
		}
	}


}
Sounder3D* ViewFilter::getSounder(std::uint32_t sounderId)
{
	size_t sounderNb = m_pSounderVector.size();
	for (unsigned int i = 0; i < sounderNb; i++)
	{
		if (m_pSounderVector[i]->getSounderID() == sounderId)
			return m_pSounderVector[i];
	}
	return NULL;
}
void	ViewFilter::RemoveAll()
{
	while (m_pSounderVector.size())
		removeSounder(m_pSounderVector[0]->getSounderID());
}
void	ViewFilter::UpdateSounderDef()
{
	// OTK - FAE063 - pas de modification des sondeurs coch�s sur l'�v�nement sounderchanged.
	// on ne supprime pas de suite les anciens objets de la liste, on les met de cot�
	// pour adapter les flags m_bVisible des novueaux objets en fonction.

	// r�cup�ration de l'ancien vecteur
	std::vector<Sounder3D*> backupSounders(m_pSounderVector.size());
	std::copy(m_pSounderVector.begin(), m_pSounderVector.end(), backupSounders.begin());

	// construction du nouveau vecteur
	m_pSounderVector.clear();
	M3DKernel *pKern = M3DKernel::GetInstance();
	for (unsigned int i = 0; i < pKern->getObjectMgr()->GetSounderDefinition().GetNbSounder(); i++)
	{
		addSounder(pKern->getObjectMgr()->GetSounderDefinition().GetSounder(i)->m_SounderId);
	}

	// positionnement des flags m_bVisible dans le novueau vecteur des sondeurs
	ApplyVisibleFlags(backupSounders, m_pSounderVector);

	// nettoyage du vecteur des anciens sondeurs seulement une fois qu'on en a plus besoin
	// (= en fin de cette fonction)
	size_t nbSounders = backupSounders.size();
	for (size_t i = 0; i < nbSounders; i++)
	{
		delete backupSounders[i];
	}
}
void  ViewFilter::ApplyVisibleFlags(const std::vector<Sounder3D*>& in, std::vector<Sounder3D*>& out)
{
	size_t nbOldSounder = in.size();
	size_t nbNewSounder = out.size();
	bool placeholder;
	for (size_t oldSounderIdx = 0; oldSounderIdx < nbOldSounder; oldSounderIdx++)
	{
		Sounder3D* pOldSounder = in[oldSounderIdx];

		// r�cup�ration du sondeur de m�me ID dans le nouveau vecteur
		for (size_t newSounderIdx = 0; newSounderIdx < nbNewSounder; newSounderIdx++)
		{
			Sounder3D* pNewSounder = out[newSounderIdx];

			if (pNewSounder->getSounderID() == pOldSounder->getSounderID())
			{
				// on a trouv� le sondeur de m�me ID, application des param�tres de visibilit�

				// NIVEAU SONDEUR
				if (pOldSounder->isVisible())
				{
					pNewSounder->Show(placeholder);
				}
				else
				{
					pNewSounder->Hide(placeholder);
				}

				// NIVEAU TRANSDUCTEUR
				unsigned int nbOldTransducer = pOldSounder->GetTransducerCount();
				unsigned int nbNewTransducer = pNewSounder->GetTransducerCount();
				unsigned int nbTransducer = std::min(nbOldTransducer, nbNewTransducer);
				for (unsigned int transducerIdx = 0; transducerIdx < nbTransducer; transducerIdx++)
				{
					Transducer3D* pOldTrans = pOldSounder->GetTransducerIdx(transducerIdx);
					Transducer3D* pNewTrans = pNewSounder->GetTransducerIdx(transducerIdx);

					if (pOldTrans->isVisible())
					{
						pNewTrans->Show(placeholder);
					}
					else
					{
						pNewTrans->Hide(placeholder);
					}

					// NIVEAU CHANNEL
					unsigned int nbOldChannel = pOldTrans->getNumberOfChannel();
					unsigned int nbNewChannel = pNewTrans->getNumberOfChannel();
					unsigned int nbChannel = std::min(nbOldChannel, nbNewChannel);
					for (unsigned int channelIdx = 0; channelIdx < nbChannel; channelIdx++)
					{
						Channel3D* pOldChan = pOldTrans->getChannelWithIdx(channelIdx);
						Channel3D* pNewChan = pNewTrans->getChannelWithIdx(channelIdx);

						if (pOldChan->isVisible())
						{
							pNewChan->Show(placeholder);
						}
						else
						{
							pNewChan->Hide(placeholder);
						}
					}

				}

				break;
			}
		}
	}
}

void ViewFilter::SetNeedToRegenerateVolume()
{
	m_bNeedToRegenerateVolume = true;
}



void ViewFilter::OnRegeneratedVolume()
{
	m_bNeedToRegenerateVolume = false;
}