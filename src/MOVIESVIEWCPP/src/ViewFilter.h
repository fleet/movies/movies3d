#pragma once
#include "Sounder3D.h"
#include <vector>
#include "DisplayParameter.h"

class PingFan3DContainer;

class ViewFilter
{
public:
	ViewFilter(void);
	virtual ~ViewFilter(void);


	Sounder3D* addSounder(std::uint32_t sounderId);
	void removeSounder(std::uint32_t sounderId);
	Sounder3D* getSounder(std::uint32_t sounderId);
	void	 RemoveAll();
	void	UpdateSounderDef();
	void ApplyVisibleFlags(const std::vector<Sounder3D*>& in, std::vector<Sounder3D*>& out);

	unsigned int GetSounderCount() { return m_pSounderVector.size(); }
	Sounder3D* GetSounderIdx(const unsigned int & idx) { return m_pSounderVector[idx]; }

	/// force volume recompute
	void SetNeedToRegenerateVolume();

	/// volume was recomputed
	void OnRegeneratedVolume();

	/// 
	/// flag telling if we need to recompute all the volumes (typically on sounder3d is newly hidden or shown 
	bool GetNeedToRegenerateVolume() { return m_bNeedToRegenerateVolume || DisplayParameter::getInstance()->dataChanged(); }




	bool m_bDisplayEcho;
	bool m_bDisplaySingleEcho;

	// NMD - 08/09/2011 - FAE 092 - modification de la visibilit� des volumes insonifi�s
	bool m_bDisplayVolumeSampled;
	//	bool m_bUseChannelIdAsScalar;

private:
	std::vector<Sounder3D*> m_pSounderVector;
	bool m_bNeedToRegenerateVolume;
};
