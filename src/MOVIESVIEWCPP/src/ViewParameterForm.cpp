#include "ViewParameterForm.h"

#include "PalettePreview.h"


template<typename T>
ref class EnumComboBoxItem
{
	T m_value;
	System::String ^ m_text;
public:
	EnumComboBoxItem(T value, System::String ^ text) : m_value(value), m_text(text) {}
	property T value { T get() { return m_value; } }
	System::String ^ ToString() override { return m_text; }
};

System::Void MOVIESVIEWCPP::ViewParameterForm::InitFields()
{
	palettePreview = gcnew PalettePreview();
	palettePreview->Dock = System::Windows::Forms::DockStyle::Fill;
	tableLayoutPanelPalette->Controls->Add(palettePreview, 0, 0);

	const int minRange = -150;
	const int maxRange = 150;

	this->numericUpDownMaxdB->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { maxRange, 0, 0, 0 });
	this->numericUpDownMaxdB->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { maxRange, 0, 0, System::Int32::MinValue });
	this->trackBarMindB->Maximum = maxRange;
	this->trackBarMindB->Minimum = minRange;
	this->numericUpDownMindB->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { maxRange, 0, 0, 0 });
	this->numericUpDownMindB->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { maxRange, 0, 0, System::Int32::MinValue });
	this->trackBarMaxdB->Maximum = maxRange;
	this->trackBarMaxdB->Minimum = minRange;

	this->numericUpDownMaxPalette->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { maxRange, 0, 0, 0 });
	this->numericUpDownMaxPalette->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { maxRange, 0, 0, System::Int32::MinValue });
	this->trackBarMinPalette->Maximum = maxRange;
	this->trackBarMinPalette->Minimum = minRange;
	this->numericUpDownMinPalette->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { maxRange, 0, 0, 0 });
	this->numericUpDownMinPalette->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { maxRange, 0, 0, System::Int32::MinValue });
	this->trackBarMaxPalette->Maximum = maxRange;
	this->trackBarMaxPalette->Minimum = minRange;

	this->comboBoxRender2DMode->Items->Add(gcnew EnumComboBoxItem<Render2DMode>(eFastDisplay, "Fast render"));
	this->comboBoxRender2DMode->Items->Add(gcnew EnumComboBoxItem<Render2DMode>(eMaxValueDisplay, "Max value"));

	this->comboBoxPaletteType->Items->Add(gcnew EnumComboBoxItem<ColorPaletteEchoType>(eCustom, "Custom"));
	this->comboBoxPaletteType->Items->Add(gcnew EnumComboBoxItem<ColorPaletteEchoType>(eParula, "Parula"));
	this->comboBoxPaletteType->Items->Add(gcnew EnumComboBoxItem<ColorPaletteEchoType>(eRedBlue, "RedBlue"));
}

System::Void MOVIESVIEWCPP::ViewParameterForm::InitData()
{
	m_SuspendEvent = true;

	DisplayParameter *pDisplay = DisplayParameter::getInstance();

	numericUpDownMindB->Value = pDisplay->GetMinThreshold() / 100;
	trackBarMindB->Value = System::Decimal::ToInt32(numericUpDownMindB->Value);

	numericUpDownMaxdB->Value = pDisplay->GetMaxThreshold() / 100;
	trackBarMaxdB->Value = System::Decimal::ToInt32(numericUpDownMaxdB->Value);

	numericUpDownSampling->Value = System::Decimal(pDisplay->GetSampling());

	numericUpDownMinDepth->Value = System::Decimal(pDisplay->GetMinDepth());
	trackBarMinDepth->Value = 0;

	numericUpDownMaxDepth->Value = System::Decimal(pDisplay->GetMaxDepth());
	trackBarMaxDepth->Value = 0;

	checkBoxUseChannelPalette->Checked = pDisplay->GetOverwriteScalarWithChannelId();

	checkBoxDepthGrid->Checked = pDisplay->GetDepthGrid();
	checkBoxDistanceGrid->Checked = pDisplay->GetDistanceGrid();
	numericUpDownDepthGrid->Value = System::Convert::ToDecimal(pDisplay->GetDepthSampling());
	numericUpDownDistanceGrid->Value = System::Convert::ToDecimal(pDisplay->GetDistanceSampling());

	checkBoxStrech2DViews->Checked = pDisplay->GetStrech();
	checkBoxDisplay2DShoals->Checked = pDisplay->GetDisplay2DShoals();
	checkBoxDisplay2DSingleEchoes->Checked = pDisplay->GetDisplay2DSingleEchoes();

	numericUpDownPixelsPerPing->Value = System::Decimal(pDisplay->GetPixelsPerPing());
	
	numericUpDownDeviationCoefficient->Value = System::Decimal(pDisplay->GetDeviationCoefficient());

	Render2DMode render2DMode = pDisplay->GetRender2DMode();
	EnumComboBoxItem<Render2DMode> ^ renderItem = nullptr;
	for each(EnumComboBoxItem<Render2DMode> ^item in comboBoxRender2DMode->Items)
	{
		if (item->value == render2DMode)
		{
			renderItem = item;
			break;
		}
	}
	comboBoxRender2DMode->SelectedItem = renderItem;

	ColorPaletteEchoType paletteType = pDisplay->GetColorPaletteEchoType();
	EnumComboBoxItem<ColorPaletteEchoType> ^paletteItem = nullptr;
	for each(EnumComboBoxItem<ColorPaletteEchoType> ^item in comboBoxPaletteType->Items)
	{
		if (item->value == paletteType)
		{
			paletteItem = item;
			break;
		}
	}
	comboBoxPaletteType->SelectedItem = paletteItem;
	buttonColorPalette->Enabled = (paletteItem != nullptr && paletteItem->value == eCustom);

	numericUpDownMinPalette->Value = pDisplay->GetMinPaletteThreshold() / 100;
	trackBarMinPalette->Value = System::Decimal::ToInt32(numericUpDownMinPalette->Value);

	numericUpDownMaxPalette->Value = pDisplay->GetMaxPaletteThreshold() / 100;
	trackBarMaxPalette->Value = System::Decimal::ToInt32(numericUpDownMaxPalette->Value);

	switch (pDisplay->GetTSDisplayType())
	{
	case eTSDisplayAll:
		radioButtonTSBoth->Checked = true;
		break;
	case eTSDisplayTracked:
		radioButtonTSTracked->Checked = true;
		break;
	case eTSDisplayUntracked:
	default:
		radioButtonTSUntracked->Checked = true;
		break;
	}

	PenParameters esuGridPenParameters = pDisplay->GetEsuGridPen();
	numericUpDownEsuGridWidth->Value = System::Decimal(esuGridPenParameters.width);
	buttonEsuGridColor->BackColor = Color::FromArgb(esuGridPenParameters.color);

	PenParameters bottomPenParameters = pDisplay->GetBottomPen();
	numericUpDownBottomWidth->Value = System::Decimal(bottomPenParameters.width);
	buttonBottomColor->BackColor = Color::FromArgb(bottomPenParameters.color);

	buttonColorNoise->BackColor = Color::FromArgb(pDisplay->GetNoiseColor());
	checkBoxDisplayNoise->Checked = pDisplay->GetNoiseDisplayEnabled();

	buttonColorRefNoise->BackColor = Color::FromArgb(pDisplay->GetRefNoiseColor());
	checkBoxDisplayRefNoise->Checked = pDisplay->GetRefNoiseDisplayEnabled();

	displayBackgroundButton->BackColor = Color::FromArgb(pDisplay->GetEchogramBackgroundColor());
		
	// Update palette preview
	palettePreview->MinDataThreshold = pDisplay->GetMinThreshold();
	palettePreview->MaxDataThreshold = pDisplay->GetMaxThreshold();
	palettePreview->Palette = pDisplay->GetCurrent2DEchoPalette();

	m_SuspendEvent = false;
}

System::Void MOVIESVIEWCPP::ViewParameterForm::ApplyParameter()
{
	if (!m_SuspendEvent)
	{
		DisplayParameter *pDisplay = DisplayParameter::getInstance();
		pDisplay->SetMinThreshold(((int)System::Decimal::ToInt32(numericUpDownMindB->Value)) * 100);
		pDisplay->SetMaxThreshold(((int)System::Decimal::ToInt32(numericUpDownMaxdB->Value)) * 100);

		pDisplay->SetSampling((double)System::Decimal::ToDouble(numericUpDownSampling->Value));
		pDisplay->SetMinDepth((double)System::Decimal::ToDouble(numericUpDownMinDepth->Value));
		pDisplay->SetMaxDepth((double)System::Decimal::ToDouble(numericUpDownMaxDepth->Value));

		pDisplay->SetDepthGrid(checkBoxDepthGrid->Checked);
		pDisplay->SetDistanceGrid(checkBoxDistanceGrid->Checked);
		pDisplay->SetDepthSampling(System::Decimal::ToDouble(numericUpDownDepthGrid->Value));
		pDisplay->SetDistanceSampling(System::Decimal::ToDouble(numericUpDownDistanceGrid->Value));

		pDisplay->SetDisplay2DShoals(checkBoxDisplay2DShoals->Checked);
		pDisplay->SetDisplay2DSingleEchoes(checkBoxDisplay2DSingleEchoes->Checked);

		pDisplay->SetPixelsPerPing(System::Decimal::ToUInt32(numericUpDownPixelsPerPing->Value));

		pDisplay->SetDeviationCoefficient(System::Decimal::ToDouble(numericUpDownDeviationCoefficient->Value));

		if (comboBoxRender2DMode->SelectedItem != nullptr)
		{
			EnumComboBoxItem<Render2DMode> ^ currentItem = (EnumComboBoxItem<Render2DMode>^)comboBoxRender2DMode->SelectedItem;
			pDisplay->SetRender2DMode(currentItem->value);
		}

		if (comboBoxPaletteType->SelectedItem != nullptr)
		{
			EnumComboBoxItem<ColorPaletteEchoType> ^ currentItem = (EnumComboBoxItem<ColorPaletteEchoType>^)comboBoxPaletteType->SelectedItem;
			pDisplay->SetColorPaletteEchoType(currentItem->value);
			buttonColorPalette->Enabled = (currentItem != nullptr && currentItem->value == eCustom);
		}
		else
		{
			buttonColorPalette->Enabled = false;
		}

		pDisplay->SetMinPaletteThreshold(((int)System::Decimal::ToInt32(numericUpDownMinPalette->Value)) * 100);
		pDisplay->SetMaxPaletteThreshold(((int)System::Decimal::ToInt32(numericUpDownMaxPalette->Value)) * 100);

		if (radioButtonTSBoth->Checked)
		{
			pDisplay->SetTSDisplayType(eTSDisplayAll);
		}
		else if (radioButtonTSTracked->Checked)
		{
			pDisplay->SetTSDisplayType(eTSDisplayTracked);
		}
		else
		{
			pDisplay->SetTSDisplayType(eTSDisplayUntracked);
		}		
		const double colorRatio = 1.0 / 255;

		PenParameters esuGridPenParameters;
		esuGridPenParameters.color = buttonEsuGridColor->BackColor.ToArgb();
		esuGridPenParameters.width = System::Decimal::ToUInt32(numericUpDownEsuGridWidth->Value);
		pDisplay->SetEsuGridPen(esuGridPenParameters);
		
		PenParameters bottomPenParameters;
		bottomPenParameters.color = buttonBottomColor->BackColor.ToArgb();
		bottomPenParameters.width = System::Decimal::ToUInt32(numericUpDownBottomWidth->Value);
		pDisplay->SetBottomPen(bottomPenParameters);

		pDisplay->SetNoiseColor(buttonColorNoise->BackColor.ToArgb());
		pDisplay->SetNoiseDisplayEnabled(checkBoxDisplayNoise->Checked);

		pDisplay->SetRefNoiseColor(buttonColorRefNoise->BackColor.ToArgb());
		pDisplay->SetRefNoiseDisplayEnabled(checkBoxDisplayRefNoise->Checked);

		pDisplay->SetEchogramBackgroundColor( displayBackgroundButton->BackColor.ToArgb() );

		// Update palette preview
		palettePreview->MinDataThreshold = pDisplay->GetMinThreshold();
		palettePreview->MaxDataThreshold = pDisplay->GetMaxThreshold();
		palettePreview->Palette = pDisplay->GetCurrent2DEchoPalette();
	}
}

System::Void MOVIESVIEWCPP::ViewParameterForm::on_component_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
	ApplyParameter();
}

System::Void MOVIESVIEWCPP::ViewParameterForm::on_colorButton_Click(System::Object^  sender, System::EventArgs^  e)
{
	System::Windows::Forms::Button^ button = dynamic_cast<System::Windows::Forms::Button^>(sender);
	if (button == nullptr)
		return;

	System::Windows::Forms::ColorDialog ^ colorDialog = gcnew System::Windows::Forms::ColorDialog();
	colorDialog->Color = button->BackColor;

	if (colorDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		button->BackColor = colorDialog->Color;
		ApplyParameter();
	}
}

System::Void MOVIESVIEWCPP::ViewParameterForm::trackBarMindB_Scroll(System::Object^  sender, System::EventArgs^  e)
{
	numericUpDownMindB->Value = trackBarMindB->Value;
}

System::Void MOVIESVIEWCPP::ViewParameterForm::trackBarMaxdB_Scroll(System::Object^  sender, System::EventArgs^  e)
{
	numericUpDownMaxdB->Value = trackBarMaxdB->Value;
}

System::Void MOVIESVIEWCPP::ViewParameterForm::trackBarMaxDepth_Scroll(System::Object^  sender, System::EventArgs^  e)
{
	if (numericUpDownMaxDepth->Value + trackBarMaxDepth->Value < numericUpDownMaxDepth->Maximum
		&& numericUpDownMaxDepth->Value + trackBarMaxDepth->Value > numericUpDownMaxDepth->Minimum
		&& numericUpDownMaxDepth->Value + trackBarMaxDepth->Value > numericUpDownMinDepth->Value
		)
		numericUpDownMaxDepth->Value = numericUpDownMaxDepth->Value + trackBarMaxDepth->Value;
	trackBarMaxDepth->Value = 0;
}

System::Void MOVIESVIEWCPP::ViewParameterForm::trackBarMinDepth_Scroll(System::Object^  sender, System::EventArgs^  e) 
{
	if (numericUpDownMinDepth->Value + trackBarMinDepth->Value < numericUpDownMinDepth->Maximum
		&& numericUpDownMinDepth->Value + trackBarMinDepth->Value > numericUpDownMinDepth->Minimum
		&& numericUpDownMinDepth->Value + trackBarMinDepth->Value < numericUpDownMaxDepth->Value
		)
		numericUpDownMinDepth->Value = numericUpDownMinDepth->Value + trackBarMinDepth->Value;
	trackBarMinDepth->Value = 0;
}

System::Void MOVIESVIEWCPP::ViewParameterForm::numericUpDownMindB_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
	ApplyParameter();
}

System::Void MOVIESVIEWCPP::ViewParameterForm::numericUpDownMinPalette_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
	ApplyParameter();
}

System::Void MOVIESVIEWCPP::ViewParameterForm::numericUpDownMaxPalette_ValueChanged(System::Object^  sender, System::EventArgs^  e)
{
	ApplyParameter();
}

System::Void MOVIESVIEWCPP::ViewParameterForm::trackBarMinPalette_Scroll(System::Object^  sender, System::EventArgs^  e)
{
	numericUpDownMinPalette->Value = trackBarMinPalette->Value;
}

System::Void MOVIESVIEWCPP::ViewParameterForm::trackBarMaxPalette_Scroll(System::Object^  sender, System::EventArgs^  e)
{
	numericUpDownMaxPalette->Value = trackBarMaxPalette->Value;
}

System::Void MOVIESVIEWCPP::ViewParameterForm::buttonColorPalette_Click(System::Object^  sender, System::EventArgs^  e) 
{
	auto diplayParameter = DisplayParameter::getInstance();

	ColorPaletteForm ^form = gcnew ColorPaletteForm();
	form->SetPalette(diplayParameter->GetManualColorPalette());
	form->SetMinThreshold(diplayParameter->GetMinThreshold());
	form->SetMaxThreshold(diplayParameter->GetMaxThreshold());

	auto res = form->ShowDialog(this);
	if (res == System::Windows::Forms::DialogResult::OK)
	{
		diplayParameter->SetManualColorPalette(form->GetPalette());
		palettePreview->Palette = diplayParameter->GetCurrent2DEchoPalette();
	}
}

System::Void MOVIESVIEWCPP::ViewParameterForm::checkBoxUseChannelPalette_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	DisplayParameter::getInstance()->SetOverwriteScalarWithChannelId(checkBoxUseChannelPalette->Checked);
}

System::Void MOVIESVIEWCPP::ViewParameterForm::checkBoxStrech2DViews_CheckedChanged(System::Object^  sender, System::EventArgs^  e) 
{
	if (!m_SuspendEvent)
	{
		DisplayParameter *pDisplay = DisplayParameter::getInstance();
		pDisplay->SetStrech(checkBoxStrech2DViews->Checked);
	}
}

int MOVIESVIEWCPP::ViewParameterForm::GetMinThresholdMinValue() { return trackBarMindB->Minimum; }

int MOVIESVIEWCPP::ViewParameterForm::GetMinThresholdMaxValue() { return trackBarMindB->Maximum; }

int MOVIESVIEWCPP::ViewParameterForm::GetMinThresholdValue() { return trackBarMindB->Value; }

int MOVIESVIEWCPP::ViewParameterForm::GetMaxThresholdValue() { return trackBarMaxdB->Value; }

void MOVIESVIEWCPP::ViewParameterForm::SetMinThresholdValue(int newValue)
{
	trackBarMindB->Value = newValue;
	numericUpDownMindB->Value = newValue;
}

System::Void MOVIESVIEWCPP::ViewParameterForm::ViewParameterForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e)
{
	this->Hide();
	e->Cancel = true;
}

void MOVIESVIEWCPP::ViewParameterForm::UpdateMaxDepth(int maxDepth) 
{
	numericUpDownMaxDepth->Value = maxDepth;
	ApplyParameter();
}