#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "DisplayParameter.h"
#include "ColorPaletteForm.h"
#include "ColorPaletteEcho.h"

namespace MOVIESVIEWCPP {

	/// <summary>
	/// Description r�sum�e de ViewParameterForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class ViewParameterForm : public System::Windows::Forms::Form
	{
	public:
		bool m_SuspendEvent;
		ViewParameterForm(void)
		{
			m_SuspendEvent = false;
			InitializeComponent();
			InitFields();
			InitData();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~ViewParameterForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::NumericUpDown^  numericUpDownSampling;
	private: System::Windows::Forms::Label^  labelSampling;
	private: System::Windows::Forms::Label^  labelMindB;
	private: System::Windows::Forms::TrackBar^  trackBarMindB;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMindB;
	private: System::Windows::Forms::GroupBox^  groupBoxThreshold;

	private: System::Windows::Forms::TrackBar^  trackBarMaxdB;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxdB;
	private: System::Windows::Forms::Label^  labelMaxdB;
	private: System::Windows::Forms::GroupBox^  groupBoxDepth;

	private: System::Windows::Forms::Label^  labelMaxDepth;
	private: System::Windows::Forms::TrackBar^  trackBarMinDepth;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMinDepth;
	private: System::Windows::Forms::Label^  labelMinDepth;
	private: System::Windows::Forms::TrackBar^  trackBarMaxDepth;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxDepth;
	private: System::Windows::Forms::GroupBox^  groupBoxDisplay;

	private: System::Windows::Forms::Button^  buttonColorPalette;
	private: System::Windows::Forms::CheckBox^  checkBoxUseChannelPalette;
	private: System::Windows::Forms::GroupBox^  groupBoxGrid;
	private: System::Windows::Forms::Label^  labelDistanceGrid;

	private: System::Windows::Forms::NumericUpDown^  numericUpDownDistanceGrid;
	private: System::Windows::Forms::CheckBox^  checkBoxDistanceGrid;
	private: System::Windows::Forms::CheckBox^  checkBoxDepthGrid;
	private: System::Windows::Forms::Label^  labelDepthGrid;

	private: System::Windows::Forms::NumericUpDown^  numericUpDownDepthGrid;
	private: System::Windows::Forms::CheckBox^  checkBoxStrech2DViews;
	private: System::Windows::Forms::CheckBox^  checkBoxDisplay2DShoals;

	private: System::Windows::Forms::Label^  PixelsPerPingLabel;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownPixelsPerPing;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownDeviationCoefficient;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::CheckBox^  checkBoxDisplay2DSingleEchoes;
	private: System::Windows::Forms::RadioButton^  radioButtonTSBoth;
	private: System::Windows::Forms::RadioButton^  radioButtonTSUntracked;
	private: System::Windows::Forms::RadioButton^  radioButtonTSTracked;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::GroupBox^  groupBoxBottom;
	private: System::Windows::Forms::Label^  labelBottomColor;

	private: System::Windows::Forms::NumericUpDown^  numericUpDownBottomWidth;
	private: System::Windows::Forms::Label^  labelBottomWidth;
	private: System::Windows::Forms::Button^  buttonBottomColor;
	private: System::Windows::Forms::ComboBox^  comboBoxRender2DMode;
	private: System::Windows::Forms::Label^  labelRender2DMode;

	private: System::Windows::Forms::GroupBox^  groupBoxColorNoise;
	private: System::Windows::Forms::Label^  labelColorNoise;
	private: System::Windows::Forms::Button^  buttonColorNoise;
	private: System::Windows::Forms::CheckBox^  checkBoxDisplayNoise;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::CheckBox^  checkBoxDisplayRefNoise;
	private: System::Windows::Forms::Label^  labelColorRefNoise;
	private: System::Windows::Forms::Button^  buttonColorRefNoise;
	private: System::Windows::Forms::Label^  displayBackgroundLabel;
	private: System::Windows::Forms::Button^  displayBackgroundButton;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel2;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel3;
	private: System::Windows::Forms::GroupBox^  groupBoxEsuGrid;
	private: System::Windows::Forms::Button^  buttonEsuGridColor;
	private: System::Windows::Forms::Label^  labelEsuGridColor;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownEsuGridWidth;
	private: System::Windows::Forms::Label^  labelEsuGridWidth;
	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel4;
	private: System::Windows::Forms::ComboBox^  comboBoxPaletteType;
	private: System::Windows::Forms::Label^  labelPaletteType;
	private: System::Windows::Forms::GroupBox^  groupBoxPalette;
	private: System::Windows::Forms::TrackBar^  trackBarMaxPalette;
	private: System::Windows::Forms::NumericUpDown^  numericUpDownMaxPalette;



	private: System::Windows::Forms::Label^  label4;
private: System::Windows::Forms::TrackBar^  trackBarMinPalette;
private: System::Windows::Forms::NumericUpDown^  numericUpDownMinPalette;


	private: System::Windows::Forms::Label^  label3;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanelPalette;

private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel8;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel7;
private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel5;



	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			this->numericUpDownSampling = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelSampling = (gcnew System::Windows::Forms::Label());
			this->labelMindB = (gcnew System::Windows::Forms::Label());
			this->trackBarMindB = (gcnew System::Windows::Forms::TrackBar());
			this->numericUpDownMindB = (gcnew System::Windows::Forms::NumericUpDown());
			this->groupBoxThreshold = (gcnew System::Windows::Forms::GroupBox());
			this->trackBarMaxdB = (gcnew System::Windows::Forms::TrackBar());
			this->numericUpDownMaxdB = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelMaxdB = (gcnew System::Windows::Forms::Label());
			this->groupBoxDepth = (gcnew System::Windows::Forms::GroupBox());
			this->trackBarMaxDepth = (gcnew System::Windows::Forms::TrackBar());
			this->numericUpDownMaxDepth = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelMaxDepth = (gcnew System::Windows::Forms::Label());
			this->trackBarMinDepth = (gcnew System::Windows::Forms::TrackBar());
			this->numericUpDownMinDepth = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelMinDepth = (gcnew System::Windows::Forms::Label());
			this->groupBoxDisplay = (gcnew System::Windows::Forms::GroupBox());
			this->comboBoxRender2DMode = (gcnew System::Windows::Forms::ComboBox());
			this->labelRender2DMode = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->radioButtonTSBoth = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonTSUntracked = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonTSTracked = (gcnew System::Windows::Forms::RadioButton());
			this->checkBoxDisplay2DSingleEchoes = (gcnew System::Windows::Forms::CheckBox());
			this->numericUpDownDeviationCoefficient = (gcnew System::Windows::Forms::NumericUpDown());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownPixelsPerPing = (gcnew System::Windows::Forms::NumericUpDown());
			this->PixelsPerPingLabel = (gcnew System::Windows::Forms::Label());
			this->checkBoxDisplay2DShoals = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxStrech2DViews = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxUseChannelPalette = (gcnew System::Windows::Forms::CheckBox());
			this->displayBackgroundButton = (gcnew System::Windows::Forms::Button());
			this->displayBackgroundLabel = (gcnew System::Windows::Forms::Label());
			this->comboBoxPaletteType = (gcnew System::Windows::Forms::ComboBox());
			this->labelPaletteType = (gcnew System::Windows::Forms::Label());
			this->buttonColorPalette = (gcnew System::Windows::Forms::Button());
			this->groupBoxGrid = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxDepthGrid = (gcnew System::Windows::Forms::CheckBox());
			this->labelDepthGrid = (gcnew System::Windows::Forms::Label());
			this->numericUpDownDepthGrid = (gcnew System::Windows::Forms::NumericUpDown());
			this->checkBoxDistanceGrid = (gcnew System::Windows::Forms::CheckBox());
			this->labelDistanceGrid = (gcnew System::Windows::Forms::Label());
			this->numericUpDownDistanceGrid = (gcnew System::Windows::Forms::NumericUpDown());
			this->groupBoxBottom = (gcnew System::Windows::Forms::GroupBox());
			this->buttonBottomColor = (gcnew System::Windows::Forms::Button());
			this->labelBottomColor = (gcnew System::Windows::Forms::Label());
			this->numericUpDownBottomWidth = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelBottomWidth = (gcnew System::Windows::Forms::Label());
			this->groupBoxColorNoise = (gcnew System::Windows::Forms::GroupBox());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->checkBoxDisplayNoise = (gcnew System::Windows::Forms::CheckBox());
			this->checkBoxDisplayRefNoise = (gcnew System::Windows::Forms::CheckBox());
			this->labelColorNoise = (gcnew System::Windows::Forms::Label());
			this->labelColorRefNoise = (gcnew System::Windows::Forms::Label());
			this->buttonColorNoise = (gcnew System::Windows::Forms::Button());
			this->buttonColorRefNoise = (gcnew System::Windows::Forms::Button());
			this->tableLayoutPanel2 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->tableLayoutPanel3 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->groupBoxEsuGrid = (gcnew System::Windows::Forms::GroupBox());
			this->buttonEsuGridColor = (gcnew System::Windows::Forms::Button());
			this->labelEsuGridColor = (gcnew System::Windows::Forms::Label());
			this->numericUpDownEsuGridWidth = (gcnew System::Windows::Forms::NumericUpDown());
			this->labelEsuGridWidth = (gcnew System::Windows::Forms::Label());
			this->groupBoxPalette = (gcnew System::Windows::Forms::GroupBox());
			this->tableLayoutPanelPalette = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->tableLayoutPanel8 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->tableLayoutPanel7 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->tableLayoutPanel5 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->trackBarMinPalette = (gcnew System::Windows::Forms::TrackBar());
			this->trackBarMaxPalette = (gcnew System::Windows::Forms::TrackBar());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMinPalette = (gcnew System::Windows::Forms::NumericUpDown());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->numericUpDownMaxPalette = (gcnew System::Windows::Forms::NumericUpDown());
			this->tableLayoutPanel4 = (gcnew System::Windows::Forms::TableLayoutPanel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownSampling))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMindB))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMindB))->BeginInit();
			this->groupBoxThreshold->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMaxdB))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxdB))->BeginInit();
			this->groupBoxDepth->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMaxDepth))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxDepth))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMinDepth))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinDepth))->BeginInit();
			this->groupBoxDisplay->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDeviationCoefficient))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownPixelsPerPing))->BeginInit();
			this->groupBoxGrid->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDepthGrid))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDistanceGrid))->BeginInit();
			this->groupBoxBottom->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownBottomWidth))->BeginInit();
			this->groupBoxColorNoise->SuspendLayout();
			this->tableLayoutPanel1->SuspendLayout();
			this->tableLayoutPanel2->SuspendLayout();
			this->tableLayoutPanel3->SuspendLayout();
			this->groupBoxEsuGrid->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownEsuGridWidth))->BeginInit();
			this->groupBoxPalette->SuspendLayout();
			this->tableLayoutPanelPalette->SuspendLayout();
			this->tableLayoutPanel8->SuspendLayout();
			this->tableLayoutPanel7->SuspendLayout();
			this->tableLayoutPanel5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMinPalette))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMaxPalette))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinPalette))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxPalette))->BeginInit();
			this->tableLayoutPanel4->SuspendLayout();
			this->SuspendLayout();
			// 
			// numericUpDownSampling
			// 
			this->numericUpDownSampling->DecimalPlaces = 2;
			this->numericUpDownSampling->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericUpDownSampling->Location = System::Drawing::Point(166, 14);
			this->numericUpDownSampling->Name = L"numericUpDownSampling";
			this->numericUpDownSampling->Size = System::Drawing::Size(120, 20);
			this->numericUpDownSampling->TabIndex = 0;
			this->numericUpDownSampling->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelSampling
			// 
			this->labelSampling->AutoSize = true;
			this->labelSampling->Location = System::Drawing::Point(6, 16);
			this->labelSampling->Name = L"labelSampling";
			this->labelSampling->Size = System::Drawing::Size(70, 13);
			this->labelSampling->TabIndex = 1;
			this->labelSampling->Text = L"3D Sampling ";
			// 
			// labelMindB
			// 
			this->labelMindB->AutoSize = true;
			this->labelMindB->Location = System::Drawing::Point(159, 34);
			this->labelMindB->Name = L"labelMindB";
			this->labelMindB->Size = System::Drawing::Size(40, 13);
			this->labelMindB->TabIndex = 3;
			this->labelMindB->Text = L"Min dB";
			// 
			// trackBarMindB
			// 
			this->trackBarMindB->LargeChange = 10;
			this->trackBarMindB->Location = System::Drawing::Point(6, 19);
			this->trackBarMindB->Name = L"trackBarMindB";
			this->trackBarMindB->Size = System::Drawing::Size(149, 45);
			this->trackBarMindB->TabIndex = 4;
			this->trackBarMindB->TickFrequency = 10;
			this->trackBarMindB->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->trackBarMindB->Scroll += gcnew System::EventHandler(this, &ViewParameterForm::trackBarMindB_Scroll);
			// 
			// numericUpDownMindB
			// 
			this->numericUpDownMindB->Location = System::Drawing::Point(223, 34);
			this->numericUpDownMindB->Name = L"numericUpDownMindB";
			this->numericUpDownMindB->ReadOnly = true;
			this->numericUpDownMindB->Size = System::Drawing::Size(63, 20);
			this->numericUpDownMindB->TabIndex = 5;
			this->numericUpDownMindB->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::numericUpDownMindB_ValueChanged);
			// 
			// groupBoxThreshold
			// 
			this->groupBoxThreshold->Controls->Add(this->trackBarMaxdB);
			this->groupBoxThreshold->Controls->Add(this->numericUpDownMaxdB);
			this->groupBoxThreshold->Controls->Add(this->labelMaxdB);
			this->groupBoxThreshold->Controls->Add(this->trackBarMindB);
			this->groupBoxThreshold->Controls->Add(this->numericUpDownMindB);
			this->groupBoxThreshold->Controls->Add(this->labelMindB);
			this->groupBoxThreshold->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxThreshold->Location = System::Drawing::Point(3, 3);
			this->groupBoxThreshold->Name = L"groupBoxThreshold";
			this->groupBoxThreshold->Size = System::Drawing::Size(311, 123);
			this->groupBoxThreshold->TabIndex = 6;
			this->groupBoxThreshold->TabStop = false;
			this->groupBoxThreshold->Text = L"Threshold";
			// 
			// trackBarMaxdB
			// 
			this->trackBarMaxdB->LargeChange = 10;
			this->trackBarMaxdB->Location = System::Drawing::Point(6, 70);
			this->trackBarMaxdB->Name = L"trackBarMaxdB";
			this->trackBarMaxdB->Size = System::Drawing::Size(149, 45);
			this->trackBarMaxdB->TabIndex = 7;
			this->trackBarMaxdB->TickFrequency = 10;
			this->trackBarMaxdB->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->trackBarMaxdB->Value = 10;
			this->trackBarMaxdB->Scroll += gcnew System::EventHandler(this, &ViewParameterForm::trackBarMaxdB_Scroll);
			// 
			// numericUpDownMaxdB
			// 
			this->numericUpDownMaxdB->Location = System::Drawing::Point(223, 83);
			this->numericUpDownMaxdB->Name = L"numericUpDownMaxdB";
			this->numericUpDownMaxdB->ReadOnly = true;
			this->numericUpDownMaxdB->Size = System::Drawing::Size(63, 20);
			this->numericUpDownMaxdB->TabIndex = 8;
			this->numericUpDownMaxdB->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelMaxdB
			// 
			this->labelMaxdB->AutoSize = true;
			this->labelMaxdB->Location = System::Drawing::Point(161, 85);
			this->labelMaxdB->Name = L"labelMaxdB";
			this->labelMaxdB->Size = System::Drawing::Size(43, 13);
			this->labelMaxdB->TabIndex = 6;
			this->labelMaxdB->Text = L"Max dB";
			// 
			// groupBoxDepth
			// 
			this->groupBoxDepth->Controls->Add(this->trackBarMaxDepth);
			this->groupBoxDepth->Controls->Add(this->numericUpDownMaxDepth);
			this->groupBoxDepth->Controls->Add(this->labelMaxDepth);
			this->groupBoxDepth->Controls->Add(this->trackBarMinDepth);
			this->groupBoxDepth->Controls->Add(this->numericUpDownMinDepth);
			this->groupBoxDepth->Controls->Add(this->labelMinDepth);
			this->groupBoxDepth->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxDepth->Location = System::Drawing::Point(3, 132);
			this->groupBoxDepth->Name = L"groupBoxDepth";
			this->groupBoxDepth->Size = System::Drawing::Size(311, 123);
			this->groupBoxDepth->TabIndex = 9;
			this->groupBoxDepth->TabStop = false;
			this->groupBoxDepth->Text = L"Depth";
			// 
			// trackBarMaxDepth
			// 
			this->trackBarMaxDepth->LargeChange = 10;
			this->trackBarMaxDepth->Location = System::Drawing::Point(6, 70);
			this->trackBarMaxDepth->Maximum = 50;
			this->trackBarMaxDepth->Minimum = -50;
			this->trackBarMaxDepth->Name = L"trackBarMaxDepth";
			this->trackBarMaxDepth->Size = System::Drawing::Size(149, 45);
			this->trackBarMaxDepth->TabIndex = 8;
			this->trackBarMaxDepth->TickFrequency = 10;
			this->trackBarMaxDepth->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->trackBarMaxDepth->Scroll += gcnew System::EventHandler(this, &ViewParameterForm::trackBarMaxDepth_Scroll);
			// 
			// numericUpDownMaxDepth
			// 
			this->numericUpDownMaxDepth->DecimalPlaces = 2;
			this->numericUpDownMaxDepth->Location = System::Drawing::Point(223, 87);
			this->numericUpDownMaxDepth->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000, 0, 0, 0 });
			this->numericUpDownMaxDepth->Name = L"numericUpDownMaxDepth";
			this->numericUpDownMaxDepth->Size = System::Drawing::Size(63, 20);
			this->numericUpDownMaxDepth->TabIndex = 7;
			this->numericUpDownMaxDepth->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelMaxDepth
			// 
			this->labelMaxDepth->AutoSize = true;
			this->labelMaxDepth->Location = System::Drawing::Point(161, 89);
			this->labelMaxDepth->Name = L"labelMaxDepth";
			this->labelMaxDepth->Size = System::Drawing::Size(59, 13);
			this->labelMaxDepth->TabIndex = 6;
			this->labelMaxDepth->Text = L"Max Depth";
			// 
			// trackBarMinDepth
			// 
			this->trackBarMinDepth->LargeChange = 10;
			this->trackBarMinDepth->Location = System::Drawing::Point(6, 19);
			this->trackBarMinDepth->Maximum = 50;
			this->trackBarMinDepth->Minimum = -50;
			this->trackBarMinDepth->Name = L"trackBarMinDepth";
			this->trackBarMinDepth->Size = System::Drawing::Size(149, 45);
			this->trackBarMinDepth->TabIndex = 4;
			this->trackBarMinDepth->TickFrequency = 10;
			this->trackBarMinDepth->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->trackBarMinDepth->Scroll += gcnew System::EventHandler(this, &ViewParameterForm::trackBarMinDepth_Scroll);
			// 
			// numericUpDownMinDepth
			// 
			this->numericUpDownMinDepth->DecimalPlaces = 2;
			this->numericUpDownMinDepth->Location = System::Drawing::Point(223, 34);
			this->numericUpDownMinDepth->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000, 0, 0, 0 });
			this->numericUpDownMinDepth->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10000, 0, 0, System::Int32::MinValue });
			this->numericUpDownMinDepth->Name = L"numericUpDownMinDepth";
			this->numericUpDownMinDepth->Size = System::Drawing::Size(63, 20);
			this->numericUpDownMinDepth->TabIndex = 5;
			this->numericUpDownMinDepth->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelMinDepth
			// 
			this->labelMinDepth->AutoSize = true;
			this->labelMinDepth->Location = System::Drawing::Point(161, 36);
			this->labelMinDepth->Name = L"labelMinDepth";
			this->labelMinDepth->Size = System::Drawing::Size(56, 13);
			this->labelMinDepth->TabIndex = 3;
			this->labelMinDepth->Text = L"Min Depth";
			// 
			// groupBoxDisplay
			// 
			this->groupBoxDisplay->Controls->Add(this->comboBoxRender2DMode);
			this->groupBoxDisplay->Controls->Add(this->labelRender2DMode);
			this->groupBoxDisplay->Controls->Add(this->label2);
			this->groupBoxDisplay->Controls->Add(this->radioButtonTSBoth);
			this->groupBoxDisplay->Controls->Add(this->radioButtonTSUntracked);
			this->groupBoxDisplay->Controls->Add(this->radioButtonTSTracked);
			this->groupBoxDisplay->Controls->Add(this->checkBoxDisplay2DSingleEchoes);
			this->groupBoxDisplay->Controls->Add(this->numericUpDownDeviationCoefficient);
			this->groupBoxDisplay->Controls->Add(this->label1);
			this->groupBoxDisplay->Controls->Add(this->numericUpDownPixelsPerPing);
			this->groupBoxDisplay->Controls->Add(this->PixelsPerPingLabel);
			this->groupBoxDisplay->Controls->Add(this->checkBoxDisplay2DShoals);
			this->groupBoxDisplay->Controls->Add(this->checkBoxStrech2DViews);
			this->groupBoxDisplay->Controls->Add(this->checkBoxUseChannelPalette);
			this->groupBoxDisplay->Controls->Add(this->numericUpDownSampling);
			this->groupBoxDisplay->Controls->Add(this->labelSampling);
			this->groupBoxDisplay->Location = System::Drawing::Point(3, 261);
			this->groupBoxDisplay->Name = L"groupBoxDisplay";
			this->groupBoxDisplay->Size = System::Drawing::Size(311, 241);
			this->groupBoxDisplay->TabIndex = 10;
			this->groupBoxDisplay->TabStop = false;
			this->groupBoxDisplay->Text = L"Display";
			// 
			// comboBoxRender2DMode
			// 
			this->comboBoxRender2DMode->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxRender2DMode->FormattingEnabled = true;
			this->comboBoxRender2DMode->Location = System::Drawing::Point(166, 85);
			this->comboBoxRender2DMode->Name = L"comboBoxRender2DMode";
			this->comboBoxRender2DMode->Size = System::Drawing::Size(120, 21);
			this->comboBoxRender2DMode->TabIndex = 37;
			this->comboBoxRender2DMode->SelectedIndexChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelRender2DMode
			// 
			this->labelRender2DMode->AutoSize = true;
			this->labelRender2DMode->Location = System::Drawing::Point(6, 91);
			this->labelRender2DMode->Name = L"labelRender2DMode";
			this->labelRender2DMode->Size = System::Drawing::Size(88, 13);
			this->labelRender2DMode->TabIndex = 36;
			this->labelRender2DMode->Text = L"2D Render mode";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 216);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(62, 13);
			this->label2->TabIndex = 35;
			this->label2->Text = L"TS display :";
			// 
			// radioButtonTSBoth
			// 
			this->radioButtonTSBoth->AutoSize = true;
			this->radioButtonTSBoth->Location = System::Drawing::Point(232, 214);
			this->radioButtonTSBoth->Name = L"radioButtonTSBoth";
			this->radioButtonTSBoth->Size = System::Drawing::Size(46, 17);
			this->radioButtonTSBoth->TabIndex = 34;
			this->radioButtonTSBoth->TabStop = true;
			this->radioButtonTSBoth->Text = L"both";
			this->radioButtonTSBoth->UseVisualStyleBackColor = true;
			this->radioButtonTSBoth->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// radioButtonTSUntracked
			// 
			this->radioButtonTSUntracked->AutoSize = true;
			this->radioButtonTSUntracked->Location = System::Drawing::Point(145, 214);
			this->radioButtonTSUntracked->Name = L"radioButtonTSUntracked";
			this->radioButtonTSUntracked->Size = System::Drawing::Size(81, 17);
			this->radioButtonTSUntracked->TabIndex = 33;
			this->radioButtonTSUntracked->TabStop = true;
			this->radioButtonTSUntracked->Text = L"Not tracked";
			this->radioButtonTSUntracked->UseVisualStyleBackColor = true;
			this->radioButtonTSUntracked->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// radioButtonTSTracked
			// 
			this->radioButtonTSTracked->AutoSize = true;
			this->radioButtonTSTracked->Location = System::Drawing::Point(74, 214);
			this->radioButtonTSTracked->Name = L"radioButtonTSTracked";
			this->radioButtonTSTracked->Size = System::Drawing::Size(65, 17);
			this->radioButtonTSTracked->TabIndex = 32;
			this->radioButtonTSTracked->TabStop = true;
			this->radioButtonTSTracked->Text = L"Tracked";
			this->radioButtonTSTracked->UseVisualStyleBackColor = true;
			this->radioButtonTSTracked->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// checkBoxDisplay2DSingleEchoes
			// 
			this->checkBoxDisplay2DSingleEchoes->AutoSize = true;
			this->checkBoxDisplay2DSingleEchoes->Location = System::Drawing::Point(9, 189);
			this->checkBoxDisplay2DSingleEchoes->Name = L"checkBoxDisplay2DSingleEchoes";
			this->checkBoxDisplay2DSingleEchoes->Size = System::Drawing::Size(145, 17);
			this->checkBoxDisplay2DSingleEchoes->TabIndex = 31;
			this->checkBoxDisplay2DSingleEchoes->Text = L"Display 2D single echoes";
			this->checkBoxDisplay2DSingleEchoes->UseVisualStyleBackColor = true;
			this->checkBoxDisplay2DSingleEchoes->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// numericUpDownDeviationCoefficient
			// 
			this->numericUpDownDeviationCoefficient->DecimalPlaces = 3;
			this->numericUpDownDeviationCoefficient->Location = System::Drawing::Point(166, 61);
			this->numericUpDownDeviationCoefficient->Name = L"numericUpDownDeviationCoefficient";
			this->numericUpDownDeviationCoefficient->Size = System::Drawing::Size(120, 20);
			this->numericUpDownDeviationCoefficient->TabIndex = 30;
			this->numericUpDownDeviationCoefficient->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 63);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(105, 13);
			this->label1->TabIndex = 29;
			this->label1->Text = L"Deviation Coefficient";
			// 
			// numericUpDownPixelsPerPing
			// 
			this->numericUpDownPixelsPerPing->Location = System::Drawing::Point(166, 37);
			this->numericUpDownPixelsPerPing->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
			this->numericUpDownPixelsPerPing->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->numericUpDownPixelsPerPing->Name = L"numericUpDownPixelsPerPing";
			this->numericUpDownPixelsPerPing->Size = System::Drawing::Size(120, 20);
			this->numericUpDownPixelsPerPing->TabIndex = 28;
			this->numericUpDownPixelsPerPing->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->numericUpDownPixelsPerPing->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// PixelsPerPingLabel
			// 
			this->PixelsPerPingLabel->AutoSize = true;
			this->PixelsPerPingLabel->Location = System::Drawing::Point(6, 39);
			this->PixelsPerPingLabel->Name = L"PixelsPerPingLabel";
			this->PixelsPerPingLabel->Size = System::Drawing::Size(111, 13);
			this->PixelsPerPingLabel->TabIndex = 27;
			this->PixelsPerPingLabel->Text = L"2D Ping Width (pixels)";
			// 
			// checkBoxDisplay2DShoals
			// 
			this->checkBoxDisplay2DShoals->AutoSize = true;
			this->checkBoxDisplay2DShoals->Location = System::Drawing::Point(9, 166);
			this->checkBoxDisplay2DShoals->Name = L"checkBoxDisplay2DShoals";
			this->checkBoxDisplay2DShoals->Size = System::Drawing::Size(110, 17);
			this->checkBoxDisplay2DShoals->TabIndex = 25;
			this->checkBoxDisplay2DShoals->Text = L"Display 2D shoals";
			this->checkBoxDisplay2DShoals->UseVisualStyleBackColor = true;
			this->checkBoxDisplay2DShoals->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// checkBoxStrech2DViews
			// 
			this->checkBoxStrech2DViews->AutoSize = true;
			this->checkBoxStrech2DViews->Location = System::Drawing::Point(9, 143);
			this->checkBoxStrech2DViews->Name = L"checkBoxStrech2DViews";
			this->checkBoxStrech2DViews->Size = System::Drawing::Size(104, 17);
			this->checkBoxStrech2DViews->TabIndex = 24;
			this->checkBoxStrech2DViews->Text = L"Strech 2D views";
			this->checkBoxStrech2DViews->UseVisualStyleBackColor = true;
			this->checkBoxStrech2DViews->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::checkBoxStrech2DViews_CheckedChanged);
			// 
			// checkBoxUseChannelPalette
			// 
			this->checkBoxUseChannelPalette->AutoSize = true;
			this->checkBoxUseChannelPalette->Location = System::Drawing::Point(9, 120);
			this->checkBoxUseChannelPalette->Name = L"checkBoxUseChannelPalette";
			this->checkBoxUseChannelPalette->Size = System::Drawing::Size(123, 17);
			this->checkBoxUseChannelPalette->TabIndex = 23;
			this->checkBoxUseChannelPalette->Text = L"Color with ChannelId";
			this->checkBoxUseChannelPalette->UseVisualStyleBackColor = true;
			this->checkBoxUseChannelPalette->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::checkBoxUseChannelPalette_CheckedChanged);
			// 
			// displayBackgroundButton
			// 
			this->displayBackgroundButton->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->displayBackgroundButton->Location = System::Drawing::Point(274, 5);
			this->displayBackgroundButton->Name = L"displayBackgroundButton";
			this->displayBackgroundButton->Size = System::Drawing::Size(20, 20);
			this->displayBackgroundButton->TabIndex = 39;
			this->displayBackgroundButton->UseVisualStyleBackColor = true;
			this->displayBackgroundButton->Click += gcnew System::EventHandler(this, &ViewParameterForm::on_colorButton_Click);
			// 
			// displayBackgroundLabel
			// 
			this->displayBackgroundLabel->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->displayBackgroundLabel->AutoSize = true;
			this->displayBackgroundLabel->Location = System::Drawing::Point(103, 8);
			this->displayBackgroundLabel->Name = L"displayBackgroundLabel";
			this->displayBackgroundLabel->Size = System::Drawing::Size(65, 13);
			this->displayBackgroundLabel->TabIndex = 38;
			this->displayBackgroundLabel->Text = L"Background";
			// 
			// comboBoxPaletteType
			// 
			this->comboBoxPaletteType->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->comboBoxPaletteType->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxPaletteType->FormattingEnabled = true;
			this->comboBoxPaletteType->Location = System::Drawing::Point(40, 4);
			this->comboBoxPaletteType->Name = L"comboBoxPaletteType";
			this->comboBoxPaletteType->Size = System::Drawing::Size(168, 21);
			this->comboBoxPaletteType->TabIndex = 37;
			this->comboBoxPaletteType->SelectedIndexChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelPaletteType
			// 
			this->labelPaletteType->Anchor = System::Windows::Forms::AnchorStyles::Left;
			this->labelPaletteType->AutoSize = true;
			this->labelPaletteType->Location = System::Drawing::Point(3, 8);
			this->labelPaletteType->Name = L"labelPaletteType";
			this->labelPaletteType->Size = System::Drawing::Size(31, 13);
			this->labelPaletteType->TabIndex = 36;
			this->labelPaletteType->Text = L"Type";
			// 
			// buttonColorPalette
			// 
			this->buttonColorPalette->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->buttonColorPalette->Location = System::Drawing::Point(214, 4);
			this->buttonColorPalette->Name = L"buttonColorPalette";
			this->buttonColorPalette->Size = System::Drawing::Size(80, 21);
			this->buttonColorPalette->TabIndex = 2;
			this->buttonColorPalette->Text = L"ColorPalette";
			this->buttonColorPalette->UseVisualStyleBackColor = true;
			this->buttonColorPalette->Click += gcnew System::EventHandler(this, &ViewParameterForm::buttonColorPalette_Click);
			// 
			// groupBoxGrid
			// 
			this->groupBoxGrid->Controls->Add(this->checkBoxDepthGrid);
			this->groupBoxGrid->Controls->Add(this->labelDepthGrid);
			this->groupBoxGrid->Controls->Add(this->numericUpDownDepthGrid);
			this->groupBoxGrid->Controls->Add(this->checkBoxDistanceGrid);
			this->groupBoxGrid->Controls->Add(this->labelDistanceGrid);
			this->groupBoxGrid->Controls->Add(this->numericUpDownDistanceGrid);
			this->groupBoxGrid->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxGrid->Location = System::Drawing::Point(3, 3);
			this->groupBoxGrid->Name = L"groupBoxGrid";
			this->groupBoxGrid->Size = System::Drawing::Size(309, 70);
			this->groupBoxGrid->TabIndex = 24;
			this->groupBoxGrid->TabStop = false;
			this->groupBoxGrid->Text = L"Grid";
			// 
			// checkBoxDepthGrid
			// 
			this->checkBoxDepthGrid->AutoSize = true;
			this->checkBoxDepthGrid->Location = System::Drawing::Point(9, 42);
			this->checkBoxDepthGrid->Name = L"checkBoxDepthGrid";
			this->checkBoxDepthGrid->Size = System::Drawing::Size(142, 17);
			this->checkBoxDepthGrid->TabIndex = 29;
			this->checkBoxDepthGrid->Text = L"Display depth grid, every";
			this->checkBoxDepthGrid->UseVisualStyleBackColor = true;
			this->checkBoxDepthGrid->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelDepthGrid
			// 
			this->labelDepthGrid->AutoSize = true;
			this->labelDepthGrid->Location = System::Drawing::Point(231, 43);
			this->labelDepthGrid->Name = L"labelDepthGrid";
			this->labelDepthGrid->Size = System::Drawing::Size(41, 13);
			this->labelDepthGrid->TabIndex = 28;
			this->labelDepthGrid->Text = L"meters.";
			// 
			// numericUpDownDepthGrid
			// 
			this->numericUpDownDepthGrid->Location = System::Drawing::Point(167, 41);
			this->numericUpDownDepthGrid->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 500, 0, 0, 0 });
			this->numericUpDownDepthGrid->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
			this->numericUpDownDepthGrid->Name = L"numericUpDownDepthGrid";
			this->numericUpDownDepthGrid->Size = System::Drawing::Size(58, 20);
			this->numericUpDownDepthGrid->TabIndex = 27;
			this->numericUpDownDepthGrid->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->numericUpDownDepthGrid->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
			this->numericUpDownDepthGrid->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// checkBoxDistanceGrid
			// 
			this->checkBoxDistanceGrid->AutoSize = true;
			this->checkBoxDistanceGrid->Location = System::Drawing::Point(9, 19);
			this->checkBoxDistanceGrid->Name = L"checkBoxDistanceGrid";
			this->checkBoxDistanceGrid->Size = System::Drawing::Size(155, 17);
			this->checkBoxDistanceGrid->TabIndex = 26;
			this->checkBoxDistanceGrid->Text = L"Display distance grid, every";
			this->checkBoxDistanceGrid->UseVisualStyleBackColor = true;
			this->checkBoxDistanceGrid->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelDistanceGrid
			// 
			this->labelDistanceGrid->AutoSize = true;
			this->labelDistanceGrid->Location = System::Drawing::Point(231, 20);
			this->labelDistanceGrid->Name = L"labelDistanceGrid";
			this->labelDistanceGrid->Size = System::Drawing::Size(33, 13);
			this->labelDistanceGrid->TabIndex = 25;
			this->labelDistanceGrid->Text = L"miles.";
			// 
			// numericUpDownDistanceGrid
			// 
			this->numericUpDownDistanceGrid->DecimalPlaces = 3;
			this->numericUpDownDistanceGrid->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 131072 });
			this->numericUpDownDistanceGrid->Location = System::Drawing::Point(167, 18);
			this->numericUpDownDistanceGrid->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 131072 });
			this->numericUpDownDistanceGrid->Name = L"numericUpDownDistanceGrid";
			this->numericUpDownDistanceGrid->Size = System::Drawing::Size(58, 20);
			this->numericUpDownDistanceGrid->TabIndex = 0;
			this->numericUpDownDistanceGrid->TextAlign = System::Windows::Forms::HorizontalAlignment::Right;
			this->numericUpDownDistanceGrid->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->numericUpDownDistanceGrid->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// groupBoxBottom
			// 
			this->groupBoxBottom->Controls->Add(this->buttonBottomColor);
			this->groupBoxBottom->Controls->Add(this->labelBottomColor);
			this->groupBoxBottom->Controls->Add(this->numericUpDownBottomWidth);
			this->groupBoxBottom->Controls->Add(this->labelBottomWidth);
			this->groupBoxBottom->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxBottom->Location = System::Drawing::Point(3, 124);
			this->groupBoxBottom->Name = L"groupBoxBottom";
			this->groupBoxBottom->Size = System::Drawing::Size(309, 39);
			this->groupBoxBottom->TabIndex = 25;
			this->groupBoxBottom->TabStop = false;
			this->groupBoxBottom->Text = L"Bottom";
			// 
			// buttonBottomColor
			// 
			this->buttonBottomColor->Location = System::Drawing::Point(203, 14);
			this->buttonBottomColor->Name = L"buttonBottomColor";
			this->buttonBottomColor->Size = System::Drawing::Size(20, 20);
			this->buttonBottomColor->TabIndex = 3;
			this->buttonBottomColor->UseVisualStyleBackColor = true;
			this->buttonBottomColor->Click += gcnew System::EventHandler(this, &ViewParameterForm::on_colorButton_Click);
			// 
			// labelBottomColor
			// 
			this->labelBottomColor->AutoSize = true;
			this->labelBottomColor->Location = System::Drawing::Point(159, 18);
			this->labelBottomColor->Name = L"labelBottomColor";
			this->labelBottomColor->Size = System::Drawing::Size(31, 13);
			this->labelBottomColor->TabIndex = 2;
			this->labelBottomColor->Text = L"Color";
			// 
			// numericUpDownBottomWidth
			// 
			this->numericUpDownBottomWidth->Location = System::Drawing::Point(59, 14);
			this->numericUpDownBottomWidth->Name = L"numericUpDownBottomWidth";
			this->numericUpDownBottomWidth->Size = System::Drawing::Size(60, 20);
			this->numericUpDownBottomWidth->TabIndex = 1;
			this->numericUpDownBottomWidth->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelBottomWidth
			// 
			this->labelBottomWidth->AutoSize = true;
			this->labelBottomWidth->Location = System::Drawing::Point(11, 18);
			this->labelBottomWidth->Name = L"labelBottomWidth";
			this->labelBottomWidth->Size = System::Drawing::Size(35, 13);
			this->labelBottomWidth->TabIndex = 0;
			this->labelBottomWidth->Text = L"Width";
			// 
			// groupBoxColorNoise
			// 
			this->groupBoxColorNoise->Controls->Add(this->tableLayoutPanel1);
			this->groupBoxColorNoise->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxColorNoise->Location = System::Drawing::Point(3, 169);
			this->groupBoxColorNoise->Name = L"groupBoxColorNoise";
			this->groupBoxColorNoise->Size = System::Drawing::Size(309, 74);
			this->groupBoxColorNoise->TabIndex = 26;
			this->groupBoxColorNoise->TabStop = false;
			this->groupBoxColorNoise->Text = L"Noise Level";
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 4;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->checkBoxDisplayNoise, 0, 0);
			this->tableLayoutPanel1->Controls->Add(this->checkBoxDisplayRefNoise, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->labelColorNoise, 1, 0);
			this->tableLayoutPanel1->Controls->Add(this->labelColorRefNoise, 1, 1);
			this->tableLayoutPanel1->Controls->Add(this->buttonColorNoise, 2, 0);
			this->tableLayoutPanel1->Controls->Add(this->buttonColorRefNoise, 2, 1);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(3, 16);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 3;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(303, 55);
			this->tableLayoutPanel1->TabIndex = 10;
			// 
			// checkBoxDisplayNoise
			// 
			this->checkBoxDisplayNoise->Anchor = System::Windows::Forms::AnchorStyles::Left;
			this->checkBoxDisplayNoise->AutoSize = true;
			this->checkBoxDisplayNoise->Location = System::Drawing::Point(3, 4);
			this->checkBoxDisplayNoise->Name = L"checkBoxDisplayNoise";
			this->checkBoxDisplayNoise->Size = System::Drawing::Size(88, 17);
			this->checkBoxDisplayNoise->TabIndex = 6;
			this->checkBoxDisplayNoise->Text = L"Display noise";
			this->checkBoxDisplayNoise->UseVisualStyleBackColor = true;
			this->checkBoxDisplayNoise->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// checkBoxDisplayRefNoise
			// 
			this->checkBoxDisplayRefNoise->Anchor = System::Windows::Forms::AnchorStyles::Left;
			this->checkBoxDisplayRefNoise->AutoSize = true;
			this->checkBoxDisplayRefNoise->Location = System::Drawing::Point(3, 30);
			this->checkBoxDisplayRefNoise->Name = L"checkBoxDisplayRefNoise";
			this->checkBoxDisplayRefNoise->Size = System::Drawing::Size(103, 17);
			this->checkBoxDisplayRefNoise->TabIndex = 7;
			this->checkBoxDisplayRefNoise->Text = L"Display ref noise";
			this->checkBoxDisplayRefNoise->UseVisualStyleBackColor = true;
			this->checkBoxDisplayRefNoise->CheckedChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelColorNoise
			// 
			this->labelColorNoise->Anchor = System::Windows::Forms::AnchorStyles::Right;
			this->labelColorNoise->AutoSize = true;
			this->labelColorNoise->Location = System::Drawing::Point(112, 6);
			this->labelColorNoise->Name = L"labelColorNoise";
			this->labelColorNoise->Size = System::Drawing::Size(31, 13);
			this->labelColorNoise->TabIndex = 4;
			this->labelColorNoise->Text = L"Color";
			// 
			// labelColorRefNoise
			// 
			this->labelColorRefNoise->Anchor = System::Windows::Forms::AnchorStyles::Right;
			this->labelColorRefNoise->AutoSize = true;
			this->labelColorRefNoise->Location = System::Drawing::Point(112, 32);
			this->labelColorRefNoise->Name = L"labelColorRefNoise";
			this->labelColorRefNoise->Size = System::Drawing::Size(31, 13);
			this->labelColorRefNoise->TabIndex = 8;
			this->labelColorRefNoise->Text = L"Color";
			// 
			// buttonColorNoise
			// 
			this->buttonColorNoise->Location = System::Drawing::Point(149, 3);
			this->buttonColorNoise->Name = L"buttonColorNoise";
			this->buttonColorNoise->Size = System::Drawing::Size(20, 20);
			this->buttonColorNoise->TabIndex = 5;
			this->buttonColorNoise->UseVisualStyleBackColor = true;
			this->buttonColorNoise->Click += gcnew System::EventHandler(this, &ViewParameterForm::on_colorButton_Click);
			// 
			// buttonColorRefNoise
			// 
			this->buttonColorRefNoise->Location = System::Drawing::Point(149, 29);
			this->buttonColorRefNoise->Name = L"buttonColorRefNoise";
			this->buttonColorRefNoise->Size = System::Drawing::Size(20, 20);
			this->buttonColorRefNoise->TabIndex = 9;
			this->buttonColorRefNoise->UseVisualStyleBackColor = true;
			this->buttonColorRefNoise->Click += gcnew System::EventHandler(this, &ViewParameterForm::on_colorButton_Click);
			// 
			// tableLayoutPanel2
			// 
			this->tableLayoutPanel2->ColumnCount = 1;
			this->tableLayoutPanel2->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel2->Controls->Add(this->groupBoxThreshold, 0, 0);
			this->tableLayoutPanel2->Controls->Add(this->groupBoxDepth, 0, 1);
			this->tableLayoutPanel2->Controls->Add(this->groupBoxDisplay, 0, 2);
			this->tableLayoutPanel2->Location = System::Drawing::Point(3, 3);
			this->tableLayoutPanel2->Name = L"tableLayoutPanel2";
			this->tableLayoutPanel2->RowCount = 4;
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel2->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel2->Size = System::Drawing::Size(317, 536);
			this->tableLayoutPanel2->TabIndex = 0;
			// 
			// tableLayoutPanel3
			// 
			this->tableLayoutPanel3->ColumnCount = 1;
			this->tableLayoutPanel3->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel3->Controls->Add(this->groupBoxGrid, 0, 0);
			this->tableLayoutPanel3->Controls->Add(this->groupBoxColorNoise, 0, 3);
			this->tableLayoutPanel3->Controls->Add(this->groupBoxBottom, 0, 2);
			this->tableLayoutPanel3->Controls->Add(this->groupBoxEsuGrid, 0, 1);
			this->tableLayoutPanel3->Controls->Add(this->groupBoxPalette, 0, 4);
			this->tableLayoutPanel3->Location = System::Drawing::Point(326, 3);
			this->tableLayoutPanel3->Name = L"tableLayoutPanel3";
			this->tableLayoutPanel3->RowCount = 5;
			this->tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel3->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel3->Size = System::Drawing::Size(315, 519);
			this->tableLayoutPanel3->TabIndex = 27;
			// 
			// groupBoxEsuGrid
			// 
			this->groupBoxEsuGrid->Controls->Add(this->buttonEsuGridColor);
			this->groupBoxEsuGrid->Controls->Add(this->labelEsuGridColor);
			this->groupBoxEsuGrid->Controls->Add(this->numericUpDownEsuGridWidth);
			this->groupBoxEsuGrid->Controls->Add(this->labelEsuGridWidth);
			this->groupBoxEsuGrid->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxEsuGrid->Location = System::Drawing::Point(3, 79);
			this->groupBoxEsuGrid->Name = L"groupBoxEsuGrid";
			this->groupBoxEsuGrid->Size = System::Drawing::Size(309, 39);
			this->groupBoxEsuGrid->TabIndex = 25;
			this->groupBoxEsuGrid->TabStop = false;
			this->groupBoxEsuGrid->Text = L"Esu Grid";
			// 
			// buttonEsuGridColor
			// 
			this->buttonEsuGridColor->Location = System::Drawing::Point(203, 14);
			this->buttonEsuGridColor->Name = L"buttonEsuGridColor";
			this->buttonEsuGridColor->Size = System::Drawing::Size(20, 20);
			this->buttonEsuGridColor->TabIndex = 3;
			this->buttonEsuGridColor->UseVisualStyleBackColor = true;
			this->buttonEsuGridColor->Click += gcnew System::EventHandler(this, &ViewParameterForm::on_colorButton_Click);
			// 
			// labelEsuGridColor
			// 
			this->labelEsuGridColor->AutoSize = true;
			this->labelEsuGridColor->Location = System::Drawing::Point(159, 18);
			this->labelEsuGridColor->Name = L"labelEsuGridColor";
			this->labelEsuGridColor->Size = System::Drawing::Size(31, 13);
			this->labelEsuGridColor->TabIndex = 2;
			this->labelEsuGridColor->Text = L"Color";
			// 
			// numericUpDownEsuGridWidth
			// 
			this->numericUpDownEsuGridWidth->Location = System::Drawing::Point(59, 14);
			this->numericUpDownEsuGridWidth->Name = L"numericUpDownEsuGridWidth";
			this->numericUpDownEsuGridWidth->Size = System::Drawing::Size(60, 20);
			this->numericUpDownEsuGridWidth->TabIndex = 1;
			this->numericUpDownEsuGridWidth->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::on_component_ValueChanged);
			// 
			// labelEsuGridWidth
			// 
			this->labelEsuGridWidth->AutoSize = true;
			this->labelEsuGridWidth->Location = System::Drawing::Point(11, 18);
			this->labelEsuGridWidth->Name = L"labelEsuGridWidth";
			this->labelEsuGridWidth->Size = System::Drawing::Size(35, 13);
			this->labelEsuGridWidth->TabIndex = 0;
			this->labelEsuGridWidth->Text = L"Width";
			// 
			// groupBoxPalette
			// 
			this->groupBoxPalette->Controls->Add(this->tableLayoutPanelPalette);
			this->groupBoxPalette->Dock = System::Windows::Forms::DockStyle::Fill;
			this->groupBoxPalette->Location = System::Drawing::Point(3, 249);
			this->groupBoxPalette->Name = L"groupBoxPalette";
			this->groupBoxPalette->Size = System::Drawing::Size(309, 267);
			this->groupBoxPalette->TabIndex = 27;
			this->groupBoxPalette->TabStop = false;
			this->groupBoxPalette->Text = L"Palette";
			// 
			// tableLayoutPanelPalette
			// 
			this->tableLayoutPanelPalette->ColumnCount = 1;
			this->tableLayoutPanelPalette->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanelPalette->Controls->Add(this->tableLayoutPanel8, 0, 2);
			this->tableLayoutPanelPalette->Controls->Add(this->tableLayoutPanel7, 0, 1);
			this->tableLayoutPanelPalette->Controls->Add(this->tableLayoutPanel5, 0, 3);
			this->tableLayoutPanelPalette->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanelPalette->Location = System::Drawing::Point(3, 16);
			this->tableLayoutPanelPalette->Name = L"tableLayoutPanelPalette";
			this->tableLayoutPanelPalette->RowCount = 4;
			this->tableLayoutPanelPalette->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute,
				40)));
			this->tableLayoutPanelPalette->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanelPalette->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanelPalette->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanelPalette->Size = System::Drawing::Size(303, 248);
			this->tableLayoutPanelPalette->TabIndex = 41;
			// 
			// tableLayoutPanel8
			// 
			this->tableLayoutPanel8->ColumnCount = 2;
			this->tableLayoutPanel8->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel8->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel8->Controls->Add(this->displayBackgroundLabel, 0, 0);
			this->tableLayoutPanel8->Controls->Add(this->displayBackgroundButton, 1, 0);
			this->tableLayoutPanel8->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel8->Location = System::Drawing::Point(3, 79);
			this->tableLayoutPanel8->Name = L"tableLayoutPanel8";
			this->tableLayoutPanel8->RowCount = 1;
			this->tableLayoutPanel8->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel8->Size = System::Drawing::Size(297, 30);
			this->tableLayoutPanel8->TabIndex = 42;
			// 
			// tableLayoutPanel7
			// 
			this->tableLayoutPanel7->ColumnCount = 3;
			this->tableLayoutPanel7->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel7->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel7->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel7->Controls->Add(this->comboBoxPaletteType, 1, 0);
			this->tableLayoutPanel7->Controls->Add(this->labelPaletteType, 0, 0);
			this->tableLayoutPanel7->Controls->Add(this->buttonColorPalette, 2, 0);
			this->tableLayoutPanel7->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel7->Location = System::Drawing::Point(3, 43);
			this->tableLayoutPanel7->Name = L"tableLayoutPanel7";
			this->tableLayoutPanel7->RowCount = 1;
			this->tableLayoutPanel7->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel7->Size = System::Drawing::Size(297, 30);
			this->tableLayoutPanel7->TabIndex = 42;
			// 
			// tableLayoutPanel5
			// 
			this->tableLayoutPanel5->ColumnCount = 3;
			this->tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel5->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel5->Controls->Add(this->trackBarMinPalette, 0, 0);
			this->tableLayoutPanel5->Controls->Add(this->trackBarMaxPalette, 0, 1);
			this->tableLayoutPanel5->Controls->Add(this->label3, 1, 0);
			this->tableLayoutPanel5->Controls->Add(this->numericUpDownMinPalette, 2, 0);
			this->tableLayoutPanel5->Controls->Add(this->label4, 1, 1);
			this->tableLayoutPanel5->Controls->Add(this->numericUpDownMaxPalette, 2, 1);
			this->tableLayoutPanel5->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel5->Location = System::Drawing::Point(3, 115);
			this->tableLayoutPanel5->Name = L"tableLayoutPanel5";
			this->tableLayoutPanel5->RowCount = 2;
			this->tableLayoutPanel5->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel5->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 50)));
			this->tableLayoutPanel5->Size = System::Drawing::Size(297, 130);
			this->tableLayoutPanel5->TabIndex = 40;
			// 
			// trackBarMinPalette
			// 
			this->trackBarMinPalette->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->trackBarMinPalette->LargeChange = 10;
			this->trackBarMinPalette->Location = System::Drawing::Point(3, 10);
			this->trackBarMinPalette->Name = L"trackBarMinPalette";
			this->trackBarMinPalette->Size = System::Drawing::Size(173, 45);
			this->trackBarMinPalette->TabIndex = 4;
			this->trackBarMinPalette->TickFrequency = 10;
			this->trackBarMinPalette->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->trackBarMinPalette->Scroll += gcnew System::EventHandler(this, &ViewParameterForm::trackBarMinPalette_Scroll);
			// 
			// trackBarMaxPalette
			// 
			this->trackBarMaxPalette->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Left | System::Windows::Forms::AnchorStyles::Right));
			this->trackBarMaxPalette->LargeChange = 10;
			this->trackBarMaxPalette->Location = System::Drawing::Point(3, 75);
			this->trackBarMaxPalette->Name = L"trackBarMaxPalette";
			this->trackBarMaxPalette->Size = System::Drawing::Size(173, 45);
			this->trackBarMaxPalette->TabIndex = 7;
			this->trackBarMaxPalette->TickFrequency = 10;
			this->trackBarMaxPalette->TickStyle = System::Windows::Forms::TickStyle::Both;
			this->trackBarMaxPalette->Value = 10;
			this->trackBarMaxPalette->Scroll += gcnew System::EventHandler(this, &ViewParameterForm::trackBarMaxPalette_Scroll);
			// 
			// label3
			// 
			this->label3->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(183, 26);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(40, 13);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Min dB";
			// 
			// numericUpDownMinPalette
			// 
			this->numericUpDownMinPalette->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->numericUpDownMinPalette->Location = System::Drawing::Point(231, 22);
			this->numericUpDownMinPalette->Name = L"numericUpDownMinPalette";
			this->numericUpDownMinPalette->ReadOnly = true;
			this->numericUpDownMinPalette->Size = System::Drawing::Size(63, 20);
			this->numericUpDownMinPalette->TabIndex = 5;
			this->numericUpDownMinPalette->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::numericUpDownMinPalette_ValueChanged);
			// 
			// label4
			// 
			this->label4->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(182, 91);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(43, 13);
			this->label4->TabIndex = 6;
			this->label4->Text = L"Max dB";
			// 
			// numericUpDownMaxPalette
			// 
			this->numericUpDownMaxPalette->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->numericUpDownMaxPalette->Location = System::Drawing::Point(231, 87);
			this->numericUpDownMaxPalette->Name = L"numericUpDownMaxPalette";
			this->numericUpDownMaxPalette->ReadOnly = true;
			this->numericUpDownMaxPalette->Size = System::Drawing::Size(63, 20);
			this->numericUpDownMaxPalette->TabIndex = 8;
			this->numericUpDownMaxPalette->ValueChanged += gcnew System::EventHandler(this, &ViewParameterForm::numericUpDownMaxPalette_ValueChanged);
			// 
			// tableLayoutPanel4
			// 
			this->tableLayoutPanel4->ColumnCount = 3;
			this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
			this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Absolute,
				321)));
			this->tableLayoutPanel4->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel4->Controls->Add(this->tableLayoutPanel2, 0, 0);
			this->tableLayoutPanel4->Controls->Add(this->tableLayoutPanel3, 1, 0);
			this->tableLayoutPanel4->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel4->Location = System::Drawing::Point(0, 0);
			this->tableLayoutPanel4->Name = L"tableLayoutPanel4";
			this->tableLayoutPanel4->RowCount = 2;
			this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->tableLayoutPanel4->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->tableLayoutPanel4->Size = System::Drawing::Size(644, 527);
			this->tableLayoutPanel4->TabIndex = 28;
			// 
			// ViewParameterForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(644, 527);
			this->Controls->Add(this->tableLayoutPanel4);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->Name = L"ViewParameterForm";
			this->ShowIcon = false;
			this->Text = L"View Parameter";
			this->TopMost = true;
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ViewParameterForm::ViewParameterForm_FormClosing);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownSampling))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMindB))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMindB))->EndInit();
			this->groupBoxThreshold->ResumeLayout(false);
			this->groupBoxThreshold->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMaxdB))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxdB))->EndInit();
			this->groupBoxDepth->ResumeLayout(false);
			this->groupBoxDepth->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMaxDepth))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxDepth))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMinDepth))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinDepth))->EndInit();
			this->groupBoxDisplay->ResumeLayout(false);
			this->groupBoxDisplay->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDeviationCoefficient))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownPixelsPerPing))->EndInit();
			this->groupBoxGrid->ResumeLayout(false);
			this->groupBoxGrid->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDepthGrid))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownDistanceGrid))->EndInit();
			this->groupBoxBottom->ResumeLayout(false);
			this->groupBoxBottom->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownBottomWidth))->EndInit();
			this->groupBoxColorNoise->ResumeLayout(false);
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			this->tableLayoutPanel2->ResumeLayout(false);
			this->tableLayoutPanel3->ResumeLayout(false);
			this->groupBoxEsuGrid->ResumeLayout(false);
			this->groupBoxEsuGrid->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownEsuGridWidth))->EndInit();
			this->groupBoxPalette->ResumeLayout(false);
			this->tableLayoutPanelPalette->ResumeLayout(false);
			this->tableLayoutPanel8->ResumeLayout(false);
			this->tableLayoutPanel8->PerformLayout();
			this->tableLayoutPanel7->ResumeLayout(false);
			this->tableLayoutPanel7->PerformLayout();
			this->tableLayoutPanel5->ResumeLayout(false);
			this->tableLayoutPanel5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMinPalette))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBarMaxPalette))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMinPalette))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->numericUpDownMaxPalette))->EndInit();
			this->tableLayoutPanel4->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion


	private: PalettePreview ^palettePreview;

	private: System::Void InitFields();
	private: System::Void ApplyParameter();
	public: System::Void InitData();

			// M�thode g�n�rique pour appliquer la config si un champ num�rique est modifi�
	private: System::Void on_component_ValueChanged(System::Object^  sender, System::EventArgs^  e);

			/// M�thode g�n�rique pour g�rer le clic sur un bouton associ� � une couleur
	private: System::Void on_colorButton_Click(System::Object^  sender, System::EventArgs^  e);
			 			 
	private: System::Void trackBarMindB_Scroll(System::Object^  sender, System::EventArgs^  e);
	private: System::Void trackBarMaxdB_Scroll(System::Object^  sender, System::EventArgs^  e);
	private: System::Void trackBarMinDepth_Scroll(System::Object^  sender, System::EventArgs^  e);
	private: System::Void trackBarMaxDepth_Scroll(System::Object^  sender, System::EventArgs^  e);

	private: System::Void numericUpDownMindB_ValueChanged(System::Object^  sender, System::EventArgs^  e);

	private: System::Void numericUpDownMinPalette_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void numericUpDownMaxPalette_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void trackBarMinPalette_Scroll(System::Object^  sender, System::EventArgs^  e);
	private: System::Void trackBarMaxPalette_Scroll(System::Object^  sender, System::EventArgs^  e);

			 // r�glage de la palette de couleurs des �chos en fonction de leur force
	private: System::Void buttonColorPalette_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void checkBoxUseChannelPalette_CheckedChanged(System::Object^  sender, System::EventArgs^  e);
	
	private: System::Void checkBoxStrech2DViews_CheckedChanged(System::Object^  sender, System::EventArgs^  e);

	// Accesseurs sur la valeur du min threshold pour que la valeur soit coh�rentea avec l'interface principale
	public: int GetMinThresholdMinValue();
	public: int GetMinThresholdMaxValue();
	public: int GetMinThresholdValue();
	public: int GetMaxThresholdValue();
	public: void SetMinThresholdValue(int newValue);

	private: System::Void ViewParameterForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);

	// Fonction pour controler la profondeur max depuis l'interface principale
	public: void UpdateMaxDepth(int maxDepth);

};
}
