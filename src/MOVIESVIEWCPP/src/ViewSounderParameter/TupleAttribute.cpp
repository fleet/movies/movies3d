#include "TupleAttribute.h"

using namespace MOVIESVIEWCPP;

TupleAttribute::TupleAttribute()
{
	m_Name = "";
	m_Values = gcnew array<System::String^>(5);
	m_Values[0] = "";
	m_Values[1] = "";
	m_Values[2] = "";
	m_Values[3] = "";
	m_Values[4] = "";
}

TupleAttribute::TupleAttribute(System::String^ name, System::String^ value)
{
	m_Name = name;
	m_Values = gcnew array<System::String^>(5);
	m_Values[0] = value;
	m_Values[1] = "";
	m_Values[2] = "";
	m_Values[3] = "";
	m_Values[4] = "";
}

System::String^ TupleAttribute::GetName()
{
	return m_Name;
}

array<System::String^> ^ TupleAttribute::GetStringList()
{
	return m_Values;
}

System::String^ TupleAttribute::GetUnit()
{
	return m_Values[1];
}

System::String^ TupleAttribute::GetComment()
{
	return m_Values[2];
}

System::String^ TupleAttribute::GetMinValue()
{
	return m_Values[3];
}

System::String^ TupleAttribute::GetMaxValue()
{
	return m_Values[4];
}

System::Void  TupleAttribute::SetUnit(System::String^ unit)
{
	m_Values[1] = unit;
}

System::Void  TupleAttribute::SetComment(System::String^ comment)
{
	m_Values[2] = comment;
}

System::Void  TupleAttribute::SetMinValue(System::String^ minValue)
{
	m_Values[3] = minValue;
}

System::Void  TupleAttribute::SetMaxValue(System::String^ maxValue)
{
	m_Values[4] = maxValue;
}
