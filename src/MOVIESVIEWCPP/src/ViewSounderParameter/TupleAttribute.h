#pragma once

namespace MOVIESVIEWCPP {

	public ref class TupleAttribute
	{
	public:
		TupleAttribute();
		TupleAttribute(System::String^ name, System::String^ value);

		// Accesseurs
		System::String^           GetName();
		System::String^           GetUnit();
		System::String^           GetComment();
		System::String^           GetMinValue();
		System::String^           GetMaxValue();
		array<System::String^> ^  GetStringList();

		System::Void              SetUnit(System::String^ unit);
		System::Void              SetComment(System::String^ comment);
		System::Void              SetMinValue(System::String^ minValue);
		System::Void              SetMaxValue(System::String^ maxValue);

	private:
		System::String ^          m_Name;
		array<System::String^> ^  m_Values;
	};

}
