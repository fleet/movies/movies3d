#include "TupleDictionary.h"

#include "M3DKernel/utils/log/ILogger.h"

using namespace MOVIESVIEWCPP;

namespace
{
	constexpr const char * LoggerName = "MovNetwork.TupleDictionary";
}

TupleDictionary::TupleDictionary()
{
	m_Attributes = gcnew System::Collections::Generic::Dictionary<unsigned short, System::Collections::Generic::Dictionary<System::String^, TupleAttribute^>^ >();
}

bool TupleDictionary::Load(System::String^ file)
{
	bool bOk = true;
	unsigned short currentTupleCode = 0;
	try
	{
		System::IO::StreamReader ^ sr = gcnew System::IO::StreamReader(file, System::Text::Encoding::GetEncoding(1252));
		while (sr->Peek() >= 0)
		{
			// Lecture de la ligne
			System::String ^line = sr->ReadLine();

			// d�codage de la ligne
			if (line->StartsWith("tuple_code"))
			{
				System::String ^ tupleCodeStr = line->Substring(line->IndexOf("=") + 1);
				tupleCodeStr = tupleCodeStr->Substring(0, tupleCodeStr->IndexOf(";"));
				currentTupleCode = System::Convert::ToUInt16(tupleCodeStr);
			}
			else if (!line->StartsWith("#")
				&& !line->StartsWith("Code"))
			{
				array<System::String^> ^lineParts = line->Split(';');
				if (lineParts->Length >= 10)
				{
					TupleAttribute ^attribute = gcnew TupleAttribute(lineParts[0], nullptr);

					attribute->SetMinValue(lineParts[6]);
					attribute->SetMaxValue(lineParts[7]);
					attribute->SetUnit(lineParts[8]);
					attribute->SetComment(lineParts[9]);

					if (currentTupleCode != 0)
					{
						System::Collections::Generic::Dictionary<System::String^, TupleAttribute^>^ attributesForTuple;
						if (!m_Attributes->TryGetValue(currentTupleCode, attributesForTuple))
						{
							attributesForTuple = gcnew System::Collections::Generic::Dictionary<System::String^, TupleAttribute^>();
							m_Attributes->Add(currentTupleCode, attributesForTuple);
						}
						TupleAttribute ^attr;
						if (!attributesForTuple->TryGetValue(attribute->GetName(), attr))
						{
							attributesForTuple->Add(attribute->GetName(), attribute);
						}
						else
						{
							M3D_LOG_WARN(LoggerName, Log::format("The attribute '%s' for tuple '%d' has already been read... second occurence ignored.", attribute->GetName(), currentTupleCode));
						}
					}
				}
			}
		}
	}
	catch (System::Exception^ e)
	{
		M3D_LOG_ERROR(LoggerName, Log::format("The process failed: %s", e->Message));
		bOk = false;
	}
	return bOk;
}

TupleAttribute ^ TupleDictionary::GetAttribute(unsigned short tuple, System::String^ attributeName)
{
	TupleAttribute ^ result = nullptr;
	System::Collections::Generic::Dictionary<System::String^, TupleAttribute^>^ attributesForTuple;
	if (m_Attributes->TryGetValue(tuple, attributesForTuple))
	{
		if (!attributesForTuple->TryGetValue(attributeName, result))
		{
			result = nullptr;
		}
	}
	return result;
}