#pragma once

#include "TupleAttribute.h"

namespace MOVIESVIEWCPP {

	public ref class TupleDictionary
	{
	public:
		TupleDictionary();

		bool Load(System::String^ file);

		TupleAttribute ^ GetAttribute(unsigned short tuple, System::String^ attributeName);

	private:
		System::Collections::Generic::Dictionary<unsigned short, System::Collections::Generic::Dictionary<System::String^, TupleAttribute^>^ > ^ m_Attributes;
	};

}
