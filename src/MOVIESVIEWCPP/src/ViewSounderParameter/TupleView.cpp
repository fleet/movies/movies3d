#include "TupleView.h"

#include "TupleDictionary.h"

using namespace MOVIESVIEWCPP;

#define	NOT_AVAILABLE_SHORT "-32768"
#define	NOT_AVAILABLE_USHORT "65535"
#define	NOT_AVAILABLE_LONG "-2147483648"
#define	NOT_AVAILABLE_LONG2 "2147483647"
#define	NOT_AVAILABLE_ULONG "4294967295"

TupleView::TupleView()
{
	m_Attributes = gcnew System::Collections::Generic::List<TupleAttribute^>();
}

generic<typename T>
	System::Void TupleView::AddAttribute(unsigned short tupleCode, System::String^ name, TupleDictionary ^dict, T value)
	{
		System::String ^ valueString = System::String::Format("{0}", value);

		// Reprise de la gestion des valeurs non disponibles comme fait dans la vue correspondante de Movies+
		if (name != "tuple_type" &&
			(valueString == NOT_AVAILABLE_SHORT ||
				valueString == NOT_AVAILABLE_USHORT ||
				valueString == NOT_AVAILABLE_LONG ||
				valueString == NOT_AVAILABLE_LONG2 ||
				valueString == NOT_AVAILABLE_ULONG))
		{
			valueString = "Not Available";
		}

		TupleAttribute^ attribute = gcnew TupleAttribute(name, valueString);

		System::Text::RegularExpressions::Regex^ unitRegex = gcnew System::Text::RegularExpressions::Regex("^([-+]?[0-9]*\\.?[0-9]+)\\s*([a-zA-Z/\\.]+)$");

		TupleAttribute ^ dictionaryAttribute = dict->GetAttribute(tupleCode, name);
		if (dictionaryAttribute != nullptr)
		{
			System::Text::RegularExpressions::Match^ m = unitRegex->Match(dictionaryAttribute->GetUnit());
			if (m->Success)
			{
				// Cas d'une unit� avec facteur multiplicatif (ignor� par M3D car on met tout en unit� SI � la lecture)
				attribute->SetUnit(m->Groups[2]->Value);
			}
			else
			{
				// Cas classique
				attribute->SetUnit(dictionaryAttribute->GetUnit());
			}
			attribute->SetComment(dictionaryAttribute->GetComment());
			attribute->SetMinValue(dictionaryAttribute->GetMinValue());
			attribute->SetMaxValue(dictionaryAttribute->GetMaxValue());
		}
		m_Attributes->Add(attribute);
	}

	System::Void TupleView::AddAttribute(unsigned short tupleCode, System::String^ name, TupleDictionary ^dict, const char * value)
	{
		AddAttribute(tupleCode, name, dict, gcnew System::String(value));
	}

	System::Collections::Generic::List<TupleAttribute^> ^ TupleView::GetAttributes()
	{
		return m_Attributes;
	}