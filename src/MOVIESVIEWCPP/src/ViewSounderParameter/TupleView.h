#pragma once

#include "TupleAttribute.h"

namespace MOVIESVIEWCPP {

	ref class TupleDictionary;

	public ref class TupleView
	{
	public:
		TupleView();

		generic<typename T>
			System::Void AddAttribute(unsigned short tupleCode, System::String^ name, TupleDictionary ^dict, T value);

			System::Void AddAttribute(unsigned short tupleCode, System::String^ name, TupleDictionary ^dict, const char* value);

			// Accesseurs
			System::Collections::Generic::List<TupleAttribute^> ^ GetAttributes();

	private:

		System::Collections::Generic::List<TupleAttribute^> ^ m_Attributes;
	};

}
