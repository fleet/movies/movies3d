#include "ViewSounderParameterForm.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "ModuleManager/ModuleManager.h"
#include "Compensation/CompensationModule.h"
#include "Compensation/TransducerPositionCompensation.h"

#include "Reader/ReaderCtrl.h"
#include "Reader/Tup9001.h"
#include "Reader/MovReadService.h"

#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/SignalFilteringObject.h"
#include "TupleView.h"
#include "TupleDictionary.h"

using namespace MOVIESVIEWCPP;


System::Void ViewSounderParameterForm::LoadDictionary()
{
	System::String^ excecutableDir = System::IO::Path::GetDirectoryName(System::Reflection::Assembly::GetExecutingAssembly()->Location);
	System::String^ hacDictFile = excecutableDir + "\\hac.csv";
	m_Dictionary = gcnew TupleDictionary();
	if (!m_Dictionary->Load(hacDictFile))
	{
		MessageBox::Show(System::String::Format("Unable to load dictionary file '{0}' (See Logging window for details).", hacDictFile));
	}
}

System::Void ViewSounderParameterForm::OnSelectionChange(System::Object^ sender, TreeViewEventArgs^ e)
{
	// Affichage des attributs du noeud s�lectionn�
	if (e->Node->Tag)
	{
		TupleView ^tupleView = (TupleView^)e->Node->Tag;
		FillListView(tupleView);
	}
}

System::Void ViewSounderParameterForm::FillTreeView()
{
	treeView->BeginUpdate();

	// Pour conservation du noeud s�lectionn� s'il existe toujours apr�s l'update de l'IHM
	System::String^ oldSelectedPath = nullptr;
	if (treeView->SelectedNode != nullptr)
	{
		oldSelectedPath = treeView->SelectedNode->FullPath;
	}

	TreeNode^ treeNodeTuples = treeView->Nodes[0];
	treeNodeTuples->Nodes->Clear();
	listView->Items->Clear();

	System::Collections::Generic::List<System::Windows::Forms::TreeNode^> ^addedNodes = gcnew System::Collections::Generic::List<System::Windows::Forms::TreeNode^>();

	M3DKernel * pKernel = M3DKernel::GetInstance();
	pKernel->Lock();

	MovReadService * pReadService = ReaderCtrl::getInstance()->GetActiveService();
	if (pReadService)
	{
		// Ajout du noeud Signature
		System::Windows::Forms::TreeNode^ signatureNode = treeNodeTuples->Nodes->Add("Signature");
		signatureNode->Tag = BuildSignatureView(pReadService->getServiceFormatDesc());
		addedNodes->Add(signatureNode);
	}

	// Ajout des noeuds sondeur
	CurrentSounderDefinition & sounderDef = pKernel->getObjectMgr()->GetSounderDefinition();
	for (unsigned int iSound = 0; iSound < sounderDef.GetNbSounder(); iSound++)
	{
		Sounder * pSound = sounderDef.GetSounder(iSound);
		if (pSound)
		{
			// Cr�ation du noeud pour le sondeur
			System::Windows::Forms::TreeNode^ sounderNode = treeNodeTuples->Nodes->Add(System::String::Format("Sounder Id[{0}]", pSound->m_SounderId));
			sounderNode->Tag = BuildSounderView(pSound);
			addedNodes->Add(sounderNode);

			// Creation d'un noeud par channel
			for (unsigned int iTrans = 0; iTrans < pSound->GetTransducerCount(); iTrans++)
			{
				Transducer * pTrans = pSound->GetTransducer(iTrans);
				for (size_t iChan = 0; iChan < pTrans->GetChannelId().size(); iChan++)
				{
					SoftChannel * pSoftChan = pTrans->getSoftChannel(pTrans->GetChannelId()[iChan]);
					System::Windows::Forms::TreeNode^ channelNode = sounderNode->Nodes->Add(System::String::Format("{0:0} kHz", (double)pSoftChan->m_acousticFrequency / 1000.0));
					channelNode->Tag = BuildChannelView(pSound, pTrans, pSoftChan);
					addedNodes->Add(channelNode);

					// Cr�ation du noeud Platform le cas �ch�ant.
					Platform * pPlatform = pTrans->GetPlatform();
					if (pPlatform)
					{
						System::Windows::Forms::TreeNode^ platformNode = channelNode->Nodes->Add("Platform");
						platformNode->Tag = BuildPlatformView(pPlatform, pSoftChan);
						addedNodes->Add(platformNode);
					}

					// Cr�ation du noeud Threshold le cas �ch�ant
					Threshold * pThreshold = pSoftChan->GetThreshold();
					if (pThreshold)
					{
						System::Windows::Forms::TreeNode^ thresholdNode = channelNode->Nodes->Add("Threshold");
						thresholdNode->Tag = BuildThresholdView(pThreshold);
						addedNodes->Add(thresholdNode);
					}

					// Cr�ation du noeud TS le cas �ch�ant
					SplitBeamParameter* splitBeamData = pSoftChan->GetSplitBeamParameter();
					if (splitBeamData)
					{
						System::Windows::Forms::TreeNode^ TSNode = channelNode->Nodes->Add("TS");
						TSNode->Tag = BuildTSView(pSoftChan, splitBeamData);
						addedNodes->Add(TSNode);
					}
				}
			}
		}
	}

	pKernel->Unlock();

	treeView->ExpandAll();

	treeView->EndUpdate();

	// res�lection �ventuelle du noeud pr�c�demment s�lectionn�
	if (oldSelectedPath != nullptr)
	{
		for each(System::Windows::Forms::TreeNode^ node in addedNodes)
		{
			if (node->FullPath == oldSelectedPath)
			{
				treeView->SelectedNode = node;
				break;
			}
		}
	}
}

System::Void ViewSounderParameterForm::FillListView(TupleView ^view)
{
	listView->BeginUpdate();
	listView->Items->Clear();
	for each(TupleAttribute ^ attr in view->GetAttributes())
	{
		ListViewItem ^ item = listView->Items->Add(attr->GetName());
		item->SubItems->AddRange(attr->GetStringList());
	}
	listView->AutoResizeColumns(ColumnHeaderAutoResizeStyle::ColumnContent);
	listView->EndUpdate();
}

TupleView^ ViewSounderParameterForm::BuildSignatureView(const ServiceFormatDesc * pServiceDesc)
{
	TupleView ^signatureView = gcnew TupleView();
	unsigned short tupleType = pServiceDesc->getTupleType();
	auto desc = pServiceDesc->getDesc();
	for (auto it : desc)
	{
		signatureView->AddAttribute(tupleType, gcnew System::String(it.first.c_str()), m_Dictionary, it.second.c_str());
	}
	return signatureView;
}

TupleView^ ViewSounderParameterForm::BuildSounderView(SounderData * pSounder)
{
	TupleView ^sounderView = gcnew TupleView();

	sounderView->AddAttribute(pSounder->m_tupleType, "tuple_type", m_Dictionary, pSounder->m_tupleType);

	SounderSBES * pSBES = dynamic_cast<SounderSBES*>(pSounder);
	if (pSBES)
	{
		sounderView->AddAttribute(pSounder->m_tupleType, "nb_channels", m_Dictionary, pSounder->m_numberOfTransducer);
		sounderView->AddAttribute(pSounder->m_tupleType, "doc_ident", m_Dictionary, pSounder->m_SounderId);
		sounderView->AddAttribute(pSounder->m_tupleType, "sound_speed", m_Dictionary, pSounder->m_soundVelocity.get());
		sounderView->AddAttribute(pSounder->m_tupleType, "ping_mode", m_Dictionary, pSBES->m_pingMode);
		sounderView->AddAttribute(pSounder->m_tupleType, "ping_interval", m_Dictionary, pSounder->m_pingInterval);
	}
	else
	{
		SounderMulti* pMBES = dynamic_cast<SounderMulti*>(pSounder);
		// Car les tuples 901 si multifaisceaux sont instanci�s dans un SounderMulti avec un typleType 901, � ne pas traiter comme un MBES
		if (pMBES && pSounder->m_tupleType != 901)
		{
			Transducer * pTrans = pMBES->GetTransducerCount() == 1 ? pMBES->GetTransducer(0) : NULL;
			if (pTrans)
			{
				// Car � la lecture du tuple 220, on ne conserve pas la valeur du tuple dans pSounder->m_numberOfTransducer
				sounderView->AddAttribute(pSounder->m_tupleType, "nb_channels", m_Dictionary, pTrans->GetChannelId().size());
			}
			else
			{
				sounderView->AddAttribute(pSounder->m_tupleType, "nb_channels", m_Dictionary, pSounder->m_numberOfTransducer);
			}
			sounderView->AddAttribute(pSounder->m_tupleType, "doc_ident", m_Dictionary, pSounder->m_SounderId);
			if (pTrans)
			{
				sounderView->AddAttribute(pSounder->m_tupleType, "transducer_name", m_Dictionary, pTrans->m_transName);
				sounderView->AddAttribute(pSounder->m_tupleType, "trscv_software_version", m_Dictionary, pTrans->m_transSoftVersion);
			}
			sounderView->AddAttribute(pSounder->m_tupleType, "sound_speed", m_Dictionary, pSounder->m_soundVelocity.get());
			sounderView->AddAttribute(pSounder->m_tupleType, "trigger_mode", m_Dictionary, pSounder->m_triggerMode);
			sounderView->AddAttribute(pSounder->m_tupleType, "ping_interval", m_Dictionary, pSounder->m_pingInterval);
			if (pTrans)
			{
				sounderView->AddAttribute(pSounder->m_tupleType, "pulse_form", m_Dictionary, pTrans->m_pulseForm);
				sounderView->AddAttribute(pSounder->m_tupleType, "pulse_duration", m_Dictionary, pTrans->m_pulseDuration);
				sounderView->AddAttribute(pSounder->m_tupleType, "sampling_interval", m_Dictionary, pTrans->m_timeSampleInterval);
				sounderView->AddAttribute(pSounder->m_tupleType, "beam_spacing", m_Dictionary, pTrans->m_frequencyBeamSpacing);
				sounderView->AddAttribute(pSounder->m_tupleType, "beam_shape", m_Dictionary, pTrans->m_frequencySpaceShape);
				sounderView->AddAttribute(pSounder->m_tupleType, "max_power", m_Dictionary, pTrans->m_transPower);
				sounderView->AddAttribute(pSounder->m_tupleType, "transducer_depth", m_Dictionary, pTrans->m_transDepthMeter);
				sounderView->AddAttribute(pSounder->m_tupleType, "platform_identifier", m_Dictionary, pTrans->m_platformId);
				sounderView->AddAttribute(pSounder->m_tupleType, "transducer_shape", m_Dictionary, pTrans->m_transShape);

				// Gestion des angles qui ont pu �tre mis � zero par le module de compensation !
				CompensationModule * pCompensationModule = CModuleManager::getInstance()->GetCompensationModule();
				TransducerPositionCompensation * pTransPos = pCompensationModule ? pCompensationModule->GetCompensationParameter().m_transducerPositionSet.GetTransducerPosition(pTrans->m_transName) : NULL;
				if (pTransPos)
				{
					sounderView->AddAttribute(pSounder->m_tupleType, "alongship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACpitchRot));
					sounderView->AddAttribute(pSounder->m_tupleType, "athwartship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACrollRot));
					sounderView->AddAttribute(pSounder->m_tupleType, "rotation_angle", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACheadingRot));
				}
				else
				{
					sounderView->AddAttribute(pSounder->m_tupleType, "alongship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTrans->m_transFaceAlongAngleOffsetRad));
					sounderView->AddAttribute(pSounder->m_tupleType, "athwartship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTrans->m_transFaceAthwarAngleOffsetRad));
					sounderView->AddAttribute(pSounder->m_tupleType, "rotation_angle", m_Dictionary, RAD_TO_DEG(pTrans->m_transRotationAngleRad));
				}
			}
		}
		else
		{
			SounderEk500 * pEk500 = dynamic_cast<SounderEk500*>(pSounder);
			if (pEk500)
			{
				sounderView->AddAttribute(pSounder->m_tupleType, "nb_channels", m_Dictionary, pSounder->m_numberOfTransducer);
				sounderView->AddAttribute(pSounder->m_tupleType, "doc_ident", m_Dictionary, pSounder->m_SounderId);
				sounderView->AddAttribute(pSounder->m_tupleType, "sound_speed", m_Dictionary, pSounder->m_soundVelocity.get());
				sounderView->AddAttribute(pSounder->m_tupleType, "ping_mode", m_Dictionary, pEk500->pingMode);
				sounderView->AddAttribute(pSounder->m_tupleType, "ping_interval", m_Dictionary, pSounder->m_pingInterval);
				sounderView->AddAttribute(pSounder->m_tupleType, "transmit_power", m_Dictionary, pEk500->transmitPower);
				sounderView->AddAttribute(pSounder->m_tupleType, "noise_margin", m_Dictionary, pEk500->noiseMargin);
				sounderView->AddAttribute(pSounder->m_tupleType, "sample_range", m_Dictionary, pEk500->sampleRange);
				sounderView->AddAttribute(pSounder->m_tupleType, "sup_layer_type", m_Dictionary, pEk500->superLayerType);
				sounderView->AddAttribute(pSounder->m_tupleType, "sup_layer_nb", m_Dictionary, pEk500->superLayerNumber);

				sounderView->AddAttribute(pSounder->m_tupleType, "sup_layer_range", m_Dictionary, pEk500->superLayerRange);
				sounderView->AddAttribute(pSounder->m_tupleType, "sup_layer_start", m_Dictionary, pEk500->superLayerStart);
				sounderView->AddAttribute(pSounder->m_tupleType, "sup_layer_mgin", m_Dictionary, pEk500->superLayerMargin);
				sounderView->AddAttribute(pSounder->m_tupleType, "sup_layer_svthr", m_Dictionary, pEk500->superLayerThreshold);
				sounderView->AddAttribute(pSounder->m_tupleType, "ek500_version", m_Dictionary, pEk500->ek500version);
			}
			else
			{
				// Cas g�n�rique
				sounderView->AddAttribute(pSounder->m_tupleType, "nb_channels", m_Dictionary, pSounder->m_numberOfTransducer);
				sounderView->AddAttribute(pSounder->m_tupleType, "doc_ident", m_Dictionary, pSounder->m_SounderId);
				sounderView->AddAttribute(pSounder->m_tupleType, "sound_speed", m_Dictionary, pSounder->m_soundVelocity.get());
				sounderView->AddAttribute(pSounder->m_tupleType, "ping_interval", m_Dictionary, pSounder->m_pingInterval);
				sounderView->AddAttribute(pSounder->m_tupleType, "trigger_mode", m_Dictionary, pSounder->m_triggerMode);
			}
		}
	}

	sounderView->AddAttribute(pSounder->m_tupleType, "remarks", m_Dictionary, pSounder->GetRemarks());

	return sounderView;
}

TupleView^ ViewSounderParameterForm::BuildChannelView(SounderData * pSounder, Transducer * pTrans, SoftChannelData * pSoftChan)
{
	TupleView ^channelView = gcnew TupleView();

	channelView->AddAttribute(pSoftChan->m_tupleType, "tuple_type", m_Dictionary, pSoftChan->m_tupleType);
	channelView->AddAttribute(pSoftChan->m_tupleType, "channel_ident", m_Dictionary, pSoftChan->m_softChannelId);
	channelView->AddAttribute(pSoftChan->m_tupleType, "sounder_ident", m_Dictionary, pSounder->m_SounderId);

	SounderSBES* pSBES = dynamic_cast<SounderSBES*>(pSounder);
	if (pSBES)
	{
		SounderER60 * pER60 = dynamic_cast<SounderER60*>(pSBES);
		SounderEK80 * pEK80 = dynamic_cast<SounderEK80*>(pSBES);
		channelView->AddAttribute(pSoftChan->m_tupleType, "frequency_channel_name", m_Dictionary, pSoftChan->m_channelName);
		channelView->AddAttribute(pSoftChan->m_tupleType, "trscv_software_version", m_Dictionary, pTrans->m_transSoftVersion);
		channelView->AddAttribute(pSoftChan->m_tupleType, "transducer_name", m_Dictionary, pTrans->m_transName);
		channelView->AddAttribute(pSoftChan->m_tupleType, "sampling_interval", m_Dictionary, pTrans->m_timeSampleInterval);
		channelView->AddAttribute(pSoftChan->m_tupleType, "data_type", m_Dictionary, pSoftChan->m_dataType);
		channelView->AddAttribute(pSoftChan->m_tupleType, "transducer_beam_type", m_Dictionary, pSoftChan->m_beamType);
		if (pER60)
		{
			channelView->AddAttribute(pSoftChan->m_tupleType, "acoustic_freq", m_Dictionary, pSoftChan->m_acousticFrequency);
		}
		else
		{
			channelView->AddAttribute(pSoftChan->m_tupleType, "nominal_freq", m_Dictionary, pSoftChan->m_acousticFrequency);
			channelView->AddAttribute(pSoftChan->m_tupleType, "start_freq", m_Dictionary, pSoftChan->m_startFrequency);
			channelView->AddAttribute(pSoftChan->m_tupleType, "end_freq", m_Dictionary, pSoftChan->m_endFrequency);
			channelView->AddAttribute(pSoftChan->m_tupleType, "pulse_shape", m_Dictionary, pTrans->m_pulseShape);
			channelView->AddAttribute(pSoftChan->m_tupleType, "pulse_slope", m_Dictionary, pTrans->m_pulseSlope);
			channelView->AddAttribute(pSoftChan->m_tupleType, "pulse_duration", m_Dictionary, pTrans->m_pulseDuration);
			channelView->AddAttribute(pSoftChan->m_tupleType, "bandwidth", m_Dictionary, pSoftChan->m_bandWidth);
		}
		channelView->AddAttribute(pSoftChan->m_tupleType, "transducer_depth", m_Dictionary, pTrans->m_transDepthMeter);
		channelView->AddAttribute(pSoftChan->m_tupleType, "start_sample", m_Dictionary, pSoftChan->m_startSample);
		channelView->AddAttribute(pSoftChan->m_tupleType, "platform_identifier", m_Dictionary, pTrans->m_platformId);
		channelView->AddAttribute(pSoftChan->m_tupleType, "transducer_shape", m_Dictionary, pTrans->m_transShape);

		// Gestion des angles qui ont pu �tre mis � zero par le module de compensation !
		CompensationModule * pCompensationModule = CModuleManager::getInstance()->GetCompensationModule();
		TransducerPositionCompensation * pTransPos = pCompensationModule ? pCompensationModule->GetCompensationParameter().m_transducerPositionSet.GetTransducerPosition(pTrans->m_transName) : NULL;
		if (pTransPos)
		{
			channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACpitchRot));
			channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACrollRot));
			channelView->AddAttribute(pSoftChan->m_tupleType, "rotation_angle", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACheadingRot));
		}
		else
		{
			channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTrans->m_transFaceAlongAngleOffsetRad));
			channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTrans->m_transFaceAthwarAngleOffsetRad));
			channelView->AddAttribute(pSoftChan->m_tupleType, "rotation_angle", m_Dictionary, RAD_TO_DEG(pTrans->m_transRotationAngleRad));
		}
		channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_a_o_beam", m_Dictionary, RAD_TO_DEG(pSoftChan->m_mainBeamAlongSteeringAngleRad));
		channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_a_o_beam", m_Dictionary, RAD_TO_DEG(pSoftChan->m_mainBeamAthwartSteeringAngleRad));
		channelView->AddAttribute(pSoftChan->m_tupleType, "sound_absorpt", m_Dictionary, pSoftChan->m_absorptionCoef*0.0001);
		if (pER60)
		{
			channelView->AddAttribute(pSoftChan->m_tupleType, "pulse_duration", m_Dictionary, pTrans->m_pulseDuration);
			channelView->AddAttribute(pSoftChan->m_tupleType, "bandwidth", m_Dictionary, pSoftChan->m_bandWidth);
		}
		channelView->AddAttribute(pSoftChan->m_tupleType, "max_power", m_Dictionary, pSoftChan->m_transmissionPower);
		channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_angl_sens", m_Dictionary, pSoftChan->m_beamAlongAngleSensitivity);
		channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_angl_sens", m_Dictionary, pSoftChan->m_beamAthwartAngleSensitivity);
		channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_3dB_bw", m_Dictionary, RAD_TO_DEG(pSoftChan->m_beam3dBWidthAlongRad));
		channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_3dB_bw", m_Dictionary, RAD_TO_DEG(pSoftChan->m_beam3dBWidthAthwartRad));
		channelView->AddAttribute(pSoftChan->m_tupleType, "2_way_beam_angle", m_Dictionary, pSoftChan->m_beamEquTwoWayAngle);

		channelView->AddAttribute(pSoftChan->m_tupleType, "sv_trsdu_gain", m_Dictionary, pSoftChan->m_beamGain);
		channelView->AddAttribute(pSoftChan->m_tupleType, "sv_trsdu_sA_correction", m_Dictionary, pSoftChan->m_beamSACorrection);
		channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_window_min", m_Dictionary, pSoftChan->m_bottomDetectionMinDepth);
		channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_window_max", m_Dictionary, pSoftChan->m_bottomDetectionMaxDepth);
		channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_min_level", m_Dictionary, pSoftChan->m_bottomDetectionMinLevel);
		if (pEK80)
		{
			channelView->AddAttribute(pSoftChan->m_tupleType, "initial_wbt_sampling_freq", m_Dictionary, pTrans->m_initialWBTSamplingFrequency);
			channelView->AddAttribute(pSoftChan->m_tupleType, "trans_impedance", m_Dictionary, pTrans->m_transducerImpedance);
			channelView->AddAttribute(pSoftChan->m_tupleType, "wbt_impedance", m_Dictionary, pTrans->m_WBTImpedance);
			channelView->AddAttribute(pSoftChan->m_tupleType, "fpga_filter_id", m_Dictionary, pTrans->GetFPGAFilter()->m_FilterID);
			channelView->AddAttribute(pSoftChan->m_tupleType, "fpga_decimation", m_Dictionary, pTrans->GetFPGAFilter()->m_DecimationFactor);
			channelView->AddAttribute(pSoftChan->m_tupleType, "ek80_filter_id", m_Dictionary, pTrans->GetApplicationFilter()->m_FilterID);
			channelView->AddAttribute(pSoftChan->m_tupleType, "ek80_decimation", m_Dictionary, pTrans->GetApplicationFilter()->m_DecimationFactor);
		}
	}
	else
	{
		SounderMulti* pMBES = dynamic_cast<SounderMulti*>(pSounder);
		// Car les tuples 901 si multifaisceaux sont instanci�s dans un SounderMulti avec un typleType 901, � ne pas traiter comme un MBES
		if (pMBES && pSounder->m_tupleType != 901)
		{
			channelView->AddAttribute(pSoftChan->m_tupleType, "frequency_channel_name", m_Dictionary, pSoftChan->m_channelName);
			channelView->AddAttribute(pSoftChan->m_tupleType, "data_type", m_Dictionary, pSoftChan->m_dataType);
			channelView->AddAttribute(pSoftChan->m_tupleType, "beam_type", m_Dictionary, pSoftChan->m_beamType);
			channelView->AddAttribute(pSoftChan->m_tupleType, "acoustic_freq", m_Dictionary, pSoftChan->m_acousticFrequency);
			channelView->AddAttribute(pSoftChan->m_tupleType, "start_sample", m_Dictionary, pSoftChan->m_startSample);
			channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_a_o_beam", m_Dictionary, RAD_TO_DEG(pSoftChan->m_mainBeamAlongSteeringAngleRad));
			channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_a_o_beam", m_Dictionary, RAD_TO_DEG(pSoftChan->m_mainBeamAthwartSteeringAngleRad));
			channelView->AddAttribute(pSoftChan->m_tupleType, "sound_absorpt", m_Dictionary, pSoftChan->m_absorptionCoef*0.0001);
			channelView->AddAttribute(pSoftChan->m_tupleType, "bandwidth", m_Dictionary, pSoftChan->m_bandWidth);
			channelView->AddAttribute(pSoftChan->m_tupleType, "transmit_power", m_Dictionary, pSoftChan->m_transmissionPower);
			channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_angl_sens", m_Dictionary, pSoftChan->m_beamAlongAngleSensitivity);
			channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_angl_sens", m_Dictionary, pSoftChan->m_beamAthwartAngleSensitivity);
			channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_3dB_bw", m_Dictionary, RAD_TO_DEG(pSoftChan->m_beam3dBWidthAlongRad));
			channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_3dB_bw", m_Dictionary, RAD_TO_DEG(pSoftChan->m_beam3dBWidthAthwartRad));
			channelView->AddAttribute(pSoftChan->m_tupleType, "2_way_beam_angle", m_Dictionary, pSoftChan->m_beamEquTwoWayAngle);
			channelView->AddAttribute(pSoftChan->m_tupleType, "trsdu_gain", m_Dictionary, pSoftChan->m_beamGain);
			channelView->AddAttribute(pSoftChan->m_tupleType, "trsdu_sA_correction", m_Dictionary, pSoftChan->m_beamSACorrection);
			channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_window_min", m_Dictionary, pSoftChan->m_bottomDetectionMinDepth);
			channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_window_max", m_Dictionary, pSoftChan->m_bottomDetectionMaxDepth);
			channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_min_level", m_Dictionary, pSoftChan->m_bottomDetectionMinLevel);
			channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_TX_weighting_identifier", m_Dictionary, pSoftChan->m_AlongTXRXWeightId);
			channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_TX_weighting_identifierr", m_Dictionary, pSoftChan->m_AthwartTXRXWeightId);
			channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_RX_weighting_identifier", m_Dictionary, pSoftChan->m_SplitBeamAlongTXRXWeightId);
			channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_RX_weighting_identifier", m_Dictionary, pSoftChan->m_SplitBeamAthwartTXRXWeightId);
		}
		else
		{
			SounderEk500 * pEk500 = dynamic_cast<SounderEk500*>(pSounder);
			if (pEk500)
			{
				channelView->AddAttribute(pSoftChan->m_tupleType, "sample_type", m_Dictionary, pSoftChan->m_dataType);
				channelView->AddAttribute(pSoftChan->m_tupleType, "trscv_channel_nb", m_Dictionary, pSoftChan->m_softChannelId);
				channelView->AddAttribute(pSoftChan->m_tupleType, "acoustic_freq", m_Dictionary, pSoftChan->m_acousticFrequency);
				channelView->AddAttribute(pSoftChan->m_tupleType, "transducer_depth", m_Dictionary, pTrans->m_transDepthMeter);
				channelView->AddAttribute(pSoftChan->m_tupleType, "platform_identifier", m_Dictionary, pTrans->m_platformId);
				channelView->AddAttribute(pSoftChan->m_tupleType, "transducer_shape", m_Dictionary, pTrans->m_transShape);

				// Gestion des angles qui ont pu �tre mis � zero par le module de compensation !
				CompensationModule * pCompensationModule = CModuleManager::getInstance()->GetCompensationModule();
				TransducerPositionCompensation * pTransPos = pCompensationModule ? pCompensationModule->GetCompensationParameter().m_transducerPositionSet.GetTransducerPosition(pTrans->m_transName) : NULL;
				if (pTransPos)
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACpitchRot));
					channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACrollRot));
					channelView->AddAttribute(pSoftChan->m_tupleType, "rotation_angle", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACheadingRot));
				}
				else
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTrans->m_transFaceAlongAngleOffsetRad));
					channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTrans->m_transFaceAthwarAngleOffsetRad));
					channelView->AddAttribute(pSoftChan->m_tupleType, "rotation_angle", m_Dictionary, RAD_TO_DEG(pTrans->m_transRotationAngleRad));
				}
				channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_a_o_beam", m_Dictionary, RAD_TO_DEG(pSoftChan->m_mainBeamAlongSteeringAngleRad));
				channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_a_o_beam", m_Dictionary, RAD_TO_DEG(pSoftChan->m_mainBeamAthwartSteeringAngleRad));
				channelView->AddAttribute(pSoftChan->m_tupleType, "sound_absorpt", m_Dictionary, pSoftChan->m_absorptionCoef*0.0001);
				channelView->AddAttribute(pSoftChan->m_tupleType, "pulse_length_mode", m_Dictionary, pTrans->m_pulseDuration);
				channelView->AddAttribute(pSoftChan->m_tupleType, "bandwidth_mode", m_Dictionary, pSoftChan->m_bandWidth);
				channelView->AddAttribute(pSoftChan->m_tupleType, "max_power", m_Dictionary, pSoftChan->m_transmissionPower);

				channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_angl_sens", m_Dictionary, pSoftChan->m_beamAlongAngleSensitivity);
				channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_angl_sens", m_Dictionary, pSoftChan->m_beamAthwartAngleSensitivity);
				channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_3dB_bw", m_Dictionary, RAD_TO_DEG(pSoftChan->m_beam3dBWidthAlongRad));
				channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_3dB_bw", m_Dictionary, RAD_TO_DEG(pSoftChan->m_beam3dBWidthAthwartRad));
				channelView->AddAttribute(pSoftChan->m_tupleType, "2_way_beam_angle", m_Dictionary, pSoftChan->m_beamEquTwoWayAngle);
				channelView->AddAttribute(pSoftChan->m_tupleType, "sv_trsdu_gain", m_Dictionary, pSoftChan->m_beamGain);
				channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_min_level", m_Dictionary, pSoftChan->m_bottomDetectionMinLevel);
				channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_window_min", m_Dictionary, pSoftChan->m_bottomDetectionMinDepth);
				channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_window_max", m_Dictionary, pSoftChan->m_bottomDetectionMaxDepth);
			}
			else
			{
				// Cas g�n�rique

				HacTupleDef* pTuple = pSoftChan->GetTupleUserData();
				Hac9001 * pHac9001 = pTuple == NULL ? NULL : (Hac9001*)(pTuple);

				if (pHac9001)
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "sampling_rate", m_Dictionary, pHac9001->SamplingRate);
					channelView->AddAttribute(pSoftChan->m_tupleType, "sampling_interval", m_Dictionary, pHac9001->SamplingInterval*0.000001);
				}
				channelView->AddAttribute(pSoftChan->m_tupleType, "acoustic_freq", m_Dictionary, pSoftChan->m_acousticFrequency);
				if (pHac9001)
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "trscv_channel_nb", m_Dictionary, pHac9001->TransChannelNumber);
				}
				channelView->AddAttribute(pSoftChan->m_tupleType, "sample_type", m_Dictionary, pSoftChan->m_dataType);
				if (pHac9001)
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "tvg_multiplier", m_Dictionary, pHac9001->TimeVariedGain);
					channelView->AddAttribute(pSoftChan->m_tupleType, "tvg_blanking_mode", m_Dictionary, pHac9001->TVGBlankMode);
					channelView->AddAttribute(pSoftChan->m_tupleType, "tvg_min_range", m_Dictionary, pHac9001->TVGMinRange*0.1);
					channelView->AddAttribute(pSoftChan->m_tupleType, "tvg_max_range", m_Dictionary, pHac9001->TVGMaxRange*0.1);
					channelView->AddAttribute(pSoftChan->m_tupleType, "blanking_range", m_Dictionary, pHac9001->BlankUpToRange*0.0001);
					channelView->AddAttribute(pSoftChan->m_tupleType, "sample_range", m_Dictionary, pHac9001->SampleRange*0.0001);
				}
				channelView->AddAttribute(pSoftChan->m_tupleType, "transducer_depth", m_Dictionary, pTrans->m_transDepthMeter);
				channelView->AddAttribute(pSoftChan->m_tupleType, "platform_identifier", m_Dictionary, pTrans->m_platformId);
				if (pHac9001)
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_offset", m_Dictionary, pHac9001->TransAlongOffset*0.0001);
					channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_offset", m_Dictionary, pHac9001->TransAthwartOffset*0.0001);
					channelView->AddAttribute(pSoftChan->m_tupleType, "vertical_offset", m_Dictionary, pHac9001->TransVerticalOffset*0.0001);
				}

				// Gestion des angles qui ont pu �tre mis � zero par le module de compensation !
				CompensationModule * pCompensationModule = CModuleManager::getInstance()->GetCompensationModule();
				TransducerPositionCompensation * pTransPos = pCompensationModule ? pCompensationModule->GetCompensationParameter().m_transducerPositionSet.GetTransducerPosition(pTrans->m_transName) : NULL;
				if (pTransPos)
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACpitchRot));
					channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACrollRot));
					channelView->AddAttribute(pSoftChan->m_tupleType, "rotation_angle", m_Dictionary, RAD_TO_DEG(pTransPos->m_HACheadingRot));
				}
				else
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTrans->m_transFaceAlongAngleOffsetRad));
					channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_a_o_trsd", m_Dictionary, RAD_TO_DEG(pTrans->m_transFaceAthwarAngleOffsetRad));
					channelView->AddAttribute(pSoftChan->m_tupleType, "rotation_angle", m_Dictionary, RAD_TO_DEG(pTrans->m_transRotationAngleRad));
				}

				channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_a_o_beam", m_Dictionary, RAD_TO_DEG(pSoftChan->m_mainBeamAlongSteeringAngleRad));
				channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_a_o_beam", m_Dictionary, RAD_TO_DEG(pSoftChan->m_mainBeamAthwartSteeringAngleRad));
				channelView->AddAttribute(pSoftChan->m_tupleType, "sound_absorpt", m_Dictionary, pSoftChan->m_absorptionCoef*0.0001);
				channelView->AddAttribute(pSoftChan->m_tupleType, "pulse_duration", m_Dictionary, pTrans->m_pulseDuration*0.001);
				channelView->AddAttribute(pSoftChan->m_tupleType, "pulse_shape_mode", m_Dictionary, pTrans->m_transShape);
				channelView->AddAttribute(pSoftChan->m_tupleType, "bandwidth", m_Dictionary, pSoftChan->m_bandWidth*0.001);
				if (pHac9001)
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "transducer_shape_mode", m_Dictionary, pHac9001->TransShapeMode);
				}
				channelView->AddAttribute(pSoftChan->m_tupleType, "alongship_3dB_bw", m_Dictionary, RAD_TO_DEG(pSoftChan->m_beam3dBWidthAlongRad));
				channelView->AddAttribute(pSoftChan->m_tupleType, "athwartship_3dB_bw", m_Dictionary, RAD_TO_DEG(pSoftChan->m_beam3dBWidthAthwartRad));
				channelView->AddAttribute(pSoftChan->m_tupleType, "2_way_beam_angle", m_Dictionary, pSoftChan->m_beamEquTwoWayAngle);
				if (pHac9001)
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "cali_sl", m_Dictionary, pHac9001->CalibSourceLevel*0.01);
					channelView->AddAttribute(pSoftChan->m_tupleType, "cali_vr", m_Dictionary, pHac9001->CalibReceivLevel*0.01);
					channelView->AddAttribute(pSoftChan->m_tupleType, "cali_sl_plus_vr", m_Dictionary, pHac9001->SVVR*0.01);
				}
				channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_min_level", m_Dictionary, pSoftChan->m_bottomDetectionMinLevel);
				channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_window_min", m_Dictionary, pSoftChan->m_bottomDetectionMinDepth);
				channelView->AddAttribute(pSoftChan->m_tupleType, "bottom_window_max", m_Dictionary, pSoftChan->m_bottomDetectionMaxDepth);
				if (pHac9001)
				{
					channelView->AddAttribute(pSoftChan->m_tupleType, "remarks", m_Dictionary, pHac9001->remarks);
				}
			}
		}
	}

	return channelView;
}

TupleView^ ViewSounderParameterForm::BuildPlatformView(Platform * pPlatform, SoftChannelData * pSoftChan)
{
	TupleView ^platformView = gcnew TupleView();
	platformView->AddAttribute(41, "tuple_type", m_Dictionary, 41);
	platformView->AddAttribute(41, "time_fraction", m_Dictionary, pPlatform->m_timeFrac);

	System::DateTime ^ dtDateTime = gcnew System::DateTime(1970, 1, 1, 0, 0, 0, System::DateTimeKind::Utc);
	dtDateTime = dtDateTime->AddSeconds(pPlatform->m_timeCpu).ToUniversalTime();

	platformView->AddAttribute(41, "cpu_time", m_Dictionary, dtDateTime->ToString());
	platformView->AddAttribute(41, "sensor_ident", m_Dictionary, pPlatform->m_attitudeSensorId);
	platformView->AddAttribute(41, "channel_ident", m_Dictionary, pSoftChan->m_softChannelId);
	platformView->AddAttribute(41, "platform", m_Dictionary, pPlatform->m_platformType);

	const BaseMathLib::Vector3D & offset = pPlatform->GetOffset();
	platformView->AddAttribute(41, "alongship_offset", m_Dictionary, offset.x);
	platformView->AddAttribute(41, "athwartship_offset", m_Dictionary, offset.y);
	platformView->AddAttribute(41, "vertical_offset", m_Dictionary, offset.z);

	return platformView;
}

TupleView^ ViewSounderParameterForm::BuildThresholdView(Threshold * pThreshold)
{
	TupleView ^thresholdView = gcnew TupleView();
	thresholdView->AddAttribute(10100, "tuple_type", m_Dictionary, 10100);
	thresholdView->AddAttribute(10100, "time_fraction", m_Dictionary, pThreshold->m_ObjectTime.m_TimeFraction);

	System::DateTime ^ dtDateTime = gcnew System::DateTime(1970, 1, 1, 0, 0, 0, System::DateTimeKind::Utc);
	dtDateTime = dtDateTime->AddSeconds(pThreshold->m_ObjectTime.m_TimeCpu).ToUniversalTime();

	thresholdView->AddAttribute(10100, "cpu_time", m_Dictionary, dtDateTime->ToString());
	thresholdView->AddAttribute(10100, "channel_ident", m_Dictionary, pThreshold->m_SoftwareChannelID);
	thresholdView->AddAttribute(10100, "tvg_max", m_Dictionary, pThreshold->m_TVGMaxRange);
	thresholdView->AddAttribute(10100, "tvg_min", m_Dictionary, pThreshold->m_TVGMinRange);
	thresholdView->AddAttribute(10100, "tvt_mode", m_Dictionary, pThreshold->m_TVTEvaluationMode);
	thresholdView->AddAttribute(10100, "tvt_interval", m_Dictionary, pThreshold->m_TVTEvaluationInterval);
	thresholdView->AddAttribute(10100, "tvt_nb_ping", m_Dictionary, pThreshold->m_TVTEvaluationNumberOfPing);
	thresholdView->AddAttribute(10100, "tvt_first_ping", m_Dictionary, pThreshold->m_TVTStartPingNumber);
	thresholdView->AddAttribute(10100, "tvt_offset", m_Dictionary, pThreshold->m_TVTParameter);
	thresholdView->AddAttribute(10100, "tvt_amplification", m_Dictionary, pThreshold->m_TVTAmpParameter);

	thresholdView->AddAttribute(10100, "tuple_attribute", m_Dictionary, pThreshold->m_tupleAttributes);

	return thresholdView;
}

TupleView^ ViewSounderParameterForm::BuildTSView(SoftChannelData * pSoftChan, SplitBeamParameter * pSplitBeamData)
{
	TupleView ^tsView = gcnew TupleView();
	tsView->AddAttribute(4000, "tuple_type", m_Dictionary, 4000);
	tsView->AddAttribute(4000, "time_fraction", m_Dictionary, pSplitBeamData->m_startLogTime.m_TimeFraction);

	System::DateTime ^ dtDateTime = gcnew System::DateTime(1970, 1, 1, 0, 0, 0, System::DateTimeKind::Utc);
	dtDateTime = dtDateTime->AddSeconds(pSplitBeamData->m_startLogTime.m_TimeCpu).ToUniversalTime();

	tsView->AddAttribute(4000, "cpu_time", m_Dictionary, dtDateTime->ToString());
	tsView->AddAttribute(4000, "channel_ident", m_Dictionary, pSoftChan->m_softChannelId);
	tsView->AddAttribute(4000, "subchannel_ident", m_Dictionary, pSplitBeamData->m_splitBeamParameterChannelId);
	tsView->AddAttribute(4000, "min_value", m_Dictionary, 100 * pSplitBeamData->m_minimumValue);
	tsView->AddAttribute(4000, "min_echo_length", m_Dictionary, pSplitBeamData->m_minimumEchoLength);
	tsView->AddAttribute(4000, "max_echo_length", m_Dictionary, pSplitBeamData->m_maximumEchoLenght);
	tsView->AddAttribute(4000, "max_gain_comp", m_Dictionary, pSplitBeamData->m_maximumGainCompensation);
	tsView->AddAttribute(4000, "max_phase_dev", m_Dictionary, pSplitBeamData->m_maximumPhaseCompensation);
	tsView->AddAttribute(4000, "remarks", m_Dictionary, pSplitBeamData->m_Remark);
	tsView->AddAttribute(10100, "tuple_attribute", m_Dictionary, pSplitBeamData->m_attribute);

	return tsView;
}
