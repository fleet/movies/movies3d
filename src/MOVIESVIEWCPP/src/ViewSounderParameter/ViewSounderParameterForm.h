#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

class ServiceFormatDesc;
class SounderData;
class SoftChannelData;
class TransData;
class Transducer;
class Platform;
class Threshold;
class SplitBeamParameter;

namespace MOVIESVIEWCPP {

	ref class TupleView;
	ref class TupleDictionary;

	/// <summary>
	/// Description r�sum�e de ViewSounderParameterForm
	///
	/// AVERTISSEMENT�: si vous modifiez le nom de cette classe, vous devrez modifier la
	///          propri�t� 'Nom du fichier de ressources' de l'outil de compilation de ressource manag�e
	///          pour tous les fichiers .resx dont d�pend cette classe. Dans le cas contraire,
	///          les concepteurs ne pourront pas interagir correctement avec les ressources
	///          localis�es associ�es � ce formulaire.
	/// </summary>
	public ref class ViewSounderParameterForm : public System::Windows::Forms::Form
	{
	public:
		ViewSounderParameterForm(void)
		{
			InitializeComponent();
			//
			//TODO�: ajoutez ici le code du constructeur
			//
			LoadDictionary();
			this->treeView->AfterSelect += gcnew TreeViewEventHandler(this, &ViewSounderParameterForm::OnSelectionChange);
			FillTreeView();
		}

	protected:
		/// <summary>
		/// Nettoyage des ressources utilis�es.
		/// </summary>
		~ViewSounderParameterForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::SplitContainer^  splitContainer1;
	private: System::Windows::Forms::TreeView^  treeView;
	private: System::Windows::Forms::ListView^  listView;
	private: System::Windows::Forms::ColumnHeader^  columnHeaderName;
	private: System::Windows::Forms::ColumnHeader^  columnHeaderValue;
	private: System::Windows::Forms::ColumnHeader^  columnHeaderUnit;
	private: System::Windows::Forms::ColumnHeader^  columnHeaderComment;
	private: System::Windows::Forms::ColumnHeader^  columnHeaderMin;
	private: System::Windows::Forms::ColumnHeader^  columnHeaderMax;

	private:
		/// <summary>
		/// Variable n�cessaire au concepteur.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�thode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette m�thode avec l'�diteur de code.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::TreeNode^  treeNode1 = (gcnew System::Windows::Forms::TreeNode(L"Tuples"));
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->treeView = (gcnew System::Windows::Forms::TreeView());
			this->listView = (gcnew System::Windows::Forms::ListView());
			this->columnHeaderName = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnHeaderValue = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnHeaderUnit = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnHeaderComment = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnHeaderMin = (gcnew System::Windows::Forms::ColumnHeader());
			this->columnHeaderMax = (gcnew System::Windows::Forms::ColumnHeader());
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->SuspendLayout();
			// 
			// splitContainer1
			// 
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->Location = System::Drawing::Point(0, 0);
			this->splitContainer1->Name = L"splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->Controls->Add(this->treeView);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->listView);
			this->splitContainer1->Size = System::Drawing::Size(823, 498);
			this->splitContainer1->SplitterDistance = 175;
			this->splitContainer1->TabIndex = 0;
			// 
			// treeView
			// 
			this->treeView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->treeView->Location = System::Drawing::Point(0, 0);
			this->treeView->Name = L"treeView";
			treeNode1->Name = L"Tuples";
			treeNode1->Text = L"Tuples";
			this->treeView->Nodes->AddRange(gcnew cli::array< System::Windows::Forms::TreeNode^  >(1) { treeNode1 });
			this->treeView->Size = System::Drawing::Size(175, 498);
			this->treeView->TabIndex = 0;
			// 
			// listView
			// 
			this->listView->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(6) {
				this->columnHeaderName,
					this->columnHeaderValue, this->columnHeaderUnit, this->columnHeaderComment, this->columnHeaderMin, this->columnHeaderMax
			});
			this->listView->Dock = System::Windows::Forms::DockStyle::Fill;
			this->listView->FullRowSelect = true;
			this->listView->HeaderStyle = System::Windows::Forms::ColumnHeaderStyle::Nonclickable;
			this->listView->Location = System::Drawing::Point(0, 0);
			this->listView->MultiSelect = false;
			this->listView->Name = L"listView";
			this->listView->Size = System::Drawing::Size(644, 498);
			this->listView->TabIndex = 0;
			this->listView->UseCompatibleStateImageBehavior = false;
			this->listView->View = System::Windows::Forms::View::Details;
			// 
			// columnHeaderName
			// 
			this->columnHeaderName->Text = L"Name";
			this->columnHeaderName->Width = 40;
			// 
			// columnHeaderValue
			// 
			this->columnHeaderValue->Text = L"Value";
			this->columnHeaderValue->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->columnHeaderValue->Width = 39;
			// 
			// columnHeaderUnit
			// 
			this->columnHeaderUnit->Text = L"Unit";
			this->columnHeaderUnit->Width = 31;
			// 
			// columnHeaderComment
			// 
			this->columnHeaderComment->Text = L"Comment";
			this->columnHeaderComment->Width = 56;
			// 
			// columnHeaderMin
			// 
			this->columnHeaderMin->Text = L"Min";
			this->columnHeaderMin->Width = 29;
			// 
			// columnHeaderMax
			// 
			this->columnHeaderMax->Text = L"Max";
			this->columnHeaderMax->Width = 346;
			// 
			// ViewSounderParameterForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(823, 498);
			this->Controls->Add(this->splitContainer1);
			this->Name = L"ViewSounderParameterForm";
			this->Text = L"View Sounder Parameter";
			this->TopMost = true;
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel2->ResumeLayout(false);
			this->splitContainer1->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void OnSelectionChange(System::Object^ sender, TreeViewEventArgs^ e);
	public:  System::Void FillTreeView();
	private: System::Void FillListView(TupleView ^view);

			 // fonction de d�finition de la vue pour chaque type de tuple
	private: TupleView^ BuildSignatureView(const ServiceFormatDesc * pServiceDesc);
	private: TupleView^ BuildSounderView(SounderData * pSounder);
	private: TupleView^ BuildChannelView(SounderData * pSounder, Transducer * pTrans, SoftChannelData * pSoftChan);
	private: TupleView^ BuildPlatformView(Platform * pPlatform, SoftChannelData * pSoftChan);
	private: TupleView^ BuildThresholdView(Threshold * pThreshold);
	private: TupleView^ BuildTSView(SoftChannelData * pSoftChan, SplitBeamParameter * pSplitBeamData);


	private: System::Void LoadDictionary();

	private: TupleDictionary ^ m_Dictionary;

	};
}
