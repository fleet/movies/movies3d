#pragma once

using namespace System::Runtime::InteropServices;

static std::string netStr2CppStr(System::String ^ ns)
{
	char* str = (char*)Marshal::StringToHGlobalAnsi(ns).ToPointer();
	std::string ret(str);
	Marshal::FreeHGlobal(System::IntPtr(str));
	return ret;
}

static System::String^ cppStr2NetStr(const char* s)
{
	return gcnew System::String(s);
}