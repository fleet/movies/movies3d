
#ifndef __vtkGlyph4me_h
#define __vtkGlyph4me_h

#include "vtkGlyph3d.h"
#include "vtkGarbageCollector.h"
class vtkGlyph4me : public vtkGlyph3D
{
public:

	// Description
	// Construct object with scaling on, scaling mode is by scalar value,
	// scale factor = 1.0, the range is (0,1), orient geometry is on, and
	// orientation is by vector. Clamping and indexing are turned off. No
	// initial sources are defined.
	static vtkGlyph4me *New();
	vtkTypeMacro(vtkGlyph4me, vtkGlyph3D);

	vtkSetMacro(OrientWithNormal, bool);
	vtkGetMacro(OrientWithNormal, bool);



protected:
	vtkGlyph4me();
	~vtkGlyph4me();

	virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
	virtual int RequestUpdateExtent(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
	virtual int FillInputPortInformation(int, vtkInformation *);

	bool OrientWithNormal;

private:
	vtkGlyph4me(const vtkGlyph4me&);  // Not implemented.
	void operator=(const vtkGlyph4me&);  // Not implemented.
};


#endif
