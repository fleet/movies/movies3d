
#include "vtkCallbackCommand.h"
#include "vtkCommand.h"
#include "vtkMovCompassWidget.h"
#include "vtkMovCompassRepresentation.h"
#include "vtkEvent.h"
#include "vtkObjectFactory.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkWidgetCallbackMapper.h" 
#include "vtkWidgetEvent.h"
#include "vtkWidgetEventTranslator.h"
#include "vtkTimerLog.h"



vtkStandardNewMacro(vtkMovCompassWidget);

//------------------------------------------------------------
vtkMovCompassWidget::vtkMovCompassWidget()
{
	// Set the initial state
	this->WidgetState = vtkMovCompassWidget::Start;
	this->TimerDuration = 50;

	// Okay, define the events
	this->CallbackMapper->SetCallbackMethod
	(vtkCommand::LeftButtonPressEvent,
		vtkWidgetEvent::Select,
		this, vtkMovCompassWidget::SelectAction);
	this->CallbackMapper->SetCallbackMethod
	(vtkCommand::MouseMoveEvent,
		vtkWidgetEvent::Move,
		this, vtkMovCompassWidget::MoveAction);
	this->CallbackMapper->SetCallbackMethod
	(vtkCommand::LeftButtonReleaseEvent,
		vtkWidgetEvent::EndSelect,
		this, vtkMovCompassWidget::EndSelectAction);
	this->CallbackMapper->SetCallbackMethod
	(vtkCommand::TimerEvent,
		vtkWidgetEvent::TimedOut,
		this, vtkMovCompassWidget::TimerAction);
}

//----------------------------------------------------------------------
void vtkMovCompassWidget::CreateDefaultRepresentation()
{
	if (!this->WidgetRep)
	{
		this->WidgetRep = vtkMovCompassRepresentation::New();
	}
}

//-----------------------------------------------------------------
double vtkMovCompassWidget::GetHeading()
{
	this->CreateDefaultRepresentation();
	vtkMovCompassRepresentation *slider =
		vtkMovCompassRepresentation::SafeDownCast(this->WidgetRep);
	return slider->GetHeading();
}

//-----------------------------------------------------------------
void vtkMovCompassWidget::SetHeading(double value)
{
	this->CreateDefaultRepresentation();
	vtkMovCompassRepresentation *slider =
		vtkMovCompassRepresentation::SafeDownCast(this->WidgetRep);
	slider->SetHeading(value);
}

//-----------------------------------------------------------------
double vtkMovCompassWidget::GetTilt()
{
	this->CreateDefaultRepresentation();
	vtkMovCompassRepresentation *slider =
		vtkMovCompassRepresentation::SafeDownCast(this->WidgetRep);
	return slider->GetTilt();
}

//-----------------------------------------------------------------
void vtkMovCompassWidget::SetTilt(double value)
{
	this->CreateDefaultRepresentation();
	vtkMovCompassRepresentation *slider =
		vtkMovCompassRepresentation::SafeDownCast(this->WidgetRep);
	slider->SetTilt(value);
}

//-----------------------------------------------------------------
double vtkMovCompassWidget::GetDistance()
{
	this->CreateDefaultRepresentation();
	vtkMovCompassRepresentation *slider =
		vtkMovCompassRepresentation::SafeDownCast(this->WidgetRep);
	return slider->GetDistance();
}

//-----------------------------------------------------------------
void vtkMovCompassWidget::SetDistance(double value)
{
	this->CreateDefaultRepresentation();
	vtkMovCompassRepresentation *slider =
		vtkMovCompassRepresentation::SafeDownCast(this->WidgetRep);
	slider->SetDistance(value);
}

//-------------------------------------------------------------
void vtkMovCompassWidget::SelectAction(vtkAbstractWidget *w)
{
	vtkMovCompassWidget *self =
		reinterpret_cast<vtkMovCompassWidget*>(w);

	double eventPos[2];
	eventPos[0] = self->Interactor->GetEventPosition()[0];
	eventPos[1] = self->Interactor->GetEventPosition()[1];

	// Okay, make sure that the pick is in the current renderer
	if (!self->CurrentRenderer ||
		!self->CurrentRenderer->IsInViewport(static_cast<int>(eventPos[0]),
			static_cast<int>(eventPos[1])))
	{
		return;
	}

	// See if the widget has been selected. StartWidgetInteraction records the
	// starting point of the motion.
	self->CreateDefaultRepresentation();
	self->WidgetRep->StartWidgetInteraction(eventPos);
	int interactionState = self->WidgetRep->GetInteractionState();

	if (interactionState == vtkMovCompassRepresentation::TiltDown)
	{
		self->SetTilt(-2.5);
		self->InvokeEvent(vtkCommand::InteractionEvent, NULL);
		self->EventCallbackCommand->SetAbortFlag(1);
		return;
	}
	if (interactionState == vtkMovCompassRepresentation::TiltUp)
	{
		self->SetTilt(+2.5);
		self->InvokeEvent(vtkCommand::InteractionEvent, NULL);
		self->EventCallbackCommand->SetAbortFlag(1);
		return;
	}

	if (interactionState == vtkMovCompassRepresentation::TiltAdjusting)
	{
		self->GrabFocus(self->EventCallbackCommand);
		self->WidgetState = vtkMovCompassWidget::TiltAdjusting;
		// Start off the timer
		self->TimerId =
			self->Interactor->CreateRepeatingTimer(self->TimerDuration);
		self->StartTime = vtkTimerLog::GetUniversalTime();
		// start the interaction
		self->StartInteraction();
		self->InvokeEvent(vtkCommand::StartInteractionEvent, NULL);
		self->EventCallbackCommand->SetAbortFlag(1);
		return;
	}

	if (interactionState == vtkMovCompassRepresentation::DistanceIn)
	{
		self->SetDistance(self->GetDistance() + 0.25);
		self->InvokeEvent(vtkCommand::InteractionEvent, NULL);
		self->EventCallbackCommand->SetAbortFlag(1);
		return;
	}
	if (interactionState == vtkMovCompassRepresentation::DistanceOut)
	{
		self->SetDistance(self->GetDistance() - 0.25);
		self->InvokeEvent(vtkCommand::InteractionEvent, NULL);
		self->EventCallbackCommand->SetAbortFlag(1);
		return;
	}

	if (interactionState == vtkMovCompassRepresentation::DistanceAdjusting)
	{
		self->GrabFocus(self->EventCallbackCommand);
		self->WidgetState = vtkMovCompassWidget::DistanceAdjusting;
		// Start off the timer
		self->TimerId =
			self->Interactor->CreateRepeatingTimer(self->TimerDuration);
		self->StartTime = vtkTimerLog::GetUniversalTime();
		// start the interaction
		self->StartInteraction();
		self->InvokeEvent(vtkCommand::StartInteractionEvent, NULL);
		self->EventCallbackCommand->SetAbortFlag(1);
		return;
	}

	if (interactionState != vtkMovCompassRepresentation::Adjusting)
	{
		return;
	}

	// We are definitely selected
	self->GrabFocus(self->EventCallbackCommand);
	self->EventCallbackCommand->SetAbortFlag(1);
	if (interactionState == vtkMovCompassRepresentation::Adjusting)
	{
		self->WidgetState = vtkMovCompassWidget::Adjusting;
		// start the interaction
		self->StartInteraction();
		self->InvokeEvent(vtkCommand::StartInteractionEvent, NULL);
		self->EventCallbackCommand->SetAbortFlag(1);
		return;
	}
}


//---------------------------------------------------------------
void vtkMovCompassWidget::MoveAction(vtkAbstractWidget *w)
{
	vtkMovCompassWidget *self =
		reinterpret_cast<vtkMovCompassWidget*>(w);

	// do we need to change highlight state?
	self->CreateDefaultRepresentation();
	int interactionState = self->WidgetRep->ComputeInteractionState
	(self->Interactor->GetEventPosition()[0],
		self->Interactor->GetEventPosition()[1]);

	// if we are outside and in the start state then return
	if (interactionState == vtkMovCompassRepresentation::Outside &&
		self->WidgetState == vtkMovCompassWidget::Start)
	{
		return;
	}

	// if we are not outside and in the Hovering state then return
	if (interactionState != vtkMovCompassRepresentation::Outside &&
		self->WidgetState == vtkMovCompassWidget::Hovering)
	{
		return;
	}

	// if we are not outside and in the Start state highlight
	if (interactionState != vtkMovCompassRepresentation::Outside &&
		self->WidgetState == vtkMovCompassWidget::Start)
	{
		self->WidgetState = vtkMovCompassWidget::Hovering;
		return;
	}

	// if we are outside but in the highlight state then stop highlighting
	if (self->WidgetState == vtkMovCompassWidget::Hovering &&
		interactionState == vtkMovCompassRepresentation::Outside)
	{
		self->WidgetState = vtkMovCompassWidget::Start;
		return;
	}

	vtkMovCompassRepresentation *rep =
		vtkMovCompassRepresentation::SafeDownCast(self->WidgetRep);

	// Definitely moving the slider, get the updated position
	double eventPos[2];
	eventPos[0] = self->Interactor->GetEventPosition()[0];
	eventPos[1] = self->Interactor->GetEventPosition()[1];
	if (self->WidgetState == vtkMovCompassWidget::TiltAdjusting)
	{
		rep->TiltWidgetInteraction(eventPos);
	}
	if (self->WidgetState == vtkMovCompassWidget::DistanceAdjusting)
	{
		rep->DistanceWidgetInteraction(eventPos);
	}
	if (self->WidgetState == vtkMovCompassWidget::Adjusting)
	{
		self->WidgetRep->WidgetInteraction(eventPos);
	}
	self->InvokeEvent(vtkCommand::InteractionEvent, NULL);

	// Interact, if desired
	self->EventCallbackCommand->SetAbortFlag(1);
	self->Render();
}


//-----------------------------------------------------------------
void vtkMovCompassWidget::EndSelectAction(vtkAbstractWidget *w)
{
	vtkMovCompassWidget *self =
		reinterpret_cast<vtkMovCompassWidget*>(w);

	if (self->WidgetState != vtkMovCompassWidget::Adjusting &&
		self->WidgetState != vtkMovCompassWidget::TiltAdjusting &&
		self->WidgetState != vtkMovCompassWidget::DistanceAdjusting)
	{
		return;
	}

	if (self->WidgetState == vtkMovCompassWidget::TiltAdjusting)
	{
		// stop the timer
		self->Interactor->DestroyTimer(self->TimerId);
		vtkMovCompassRepresentation *rep =
			vtkMovCompassRepresentation::SafeDownCast(self->WidgetRep);
		rep->EndTilt();
	}

	if (self->WidgetState == vtkMovCompassWidget::DistanceAdjusting)
	{
		// stop the timer
		self->Interactor->DestroyTimer(self->TimerId);
		vtkMovCompassRepresentation *rep =
			vtkMovCompassRepresentation::SafeDownCast(self->WidgetRep);
		rep->EndDistance();
	}

	int interactionState = self->WidgetRep->ComputeInteractionState
	(self->Interactor->GetEventPosition()[0],
		self->Interactor->GetEventPosition()[1]);

	if (interactionState == vtkMovCompassRepresentation::Outside)
	{
		self->WidgetState = vtkMovCompassWidget::Start;
	}
	else
	{
		self->WidgetState = vtkMovCompassWidget::Hovering;
	}

	// The state returns to unselected
	self->ReleaseFocus();

	// Complete interaction
	self->EventCallbackCommand->SetAbortFlag(1);
	self->EndInteraction();
	self->InvokeEvent(vtkCommand::EndInteractionEvent, NULL);
	self->Render();
}

void vtkMovCompassWidget::TimerAction(vtkAbstractWidget *w)
{
	vtkMovCompassWidget *self =
		reinterpret_cast<vtkMovCompassWidget*>(w);
	int timerId = *(reinterpret_cast<int*>(self->CallData));

	// If this is the timer event we are waiting for...
	if (timerId == self->TimerId)
	{
		vtkMovCompassRepresentation *rep =
			vtkMovCompassRepresentation::SafeDownCast(self->WidgetRep);
		if (self->WidgetState == vtkMovCompassWidget::TiltAdjusting)
		{
			double tElapsed = vtkTimerLog::GetUniversalTime() - self->StartTime;
			rep->UpdateTilt(tElapsed);
		}
		if (self->WidgetState == vtkMovCompassWidget::DistanceAdjusting)
		{
			double tElapsed = vtkTimerLog::GetUniversalTime() - self->StartTime;
			rep->UpdateDistance(tElapsed);
		}
		self->StartTime = vtkTimerLog::GetUniversalTime();
		self->InvokeEvent(vtkCommand::InteractionEvent, NULL);
		self->EventCallbackCommand->SetAbortFlag(1); //no one else gets this timer
	}
}

//-----------------------------------------------------------------
void vtkMovCompassWidget::PrintSelf(ostream& os, vtkIndent indent)
{
	//Superclass typedef defined in vtkTypeMacro() found in vtkSetGet.h
	this->Superclass::PrintSelf(os, indent);
}

