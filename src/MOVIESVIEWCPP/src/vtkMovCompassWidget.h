#ifndef __vtkMovCompassWidget_h
#define __vtkMovCompassWidget_h

#include "vtkAbstractWidget.h"

class vtkMovCompassRepresentation;


class vtkMovCompassWidget : public vtkAbstractWidget
{
public:
	// Description:
	// Instantiate the class.
	static vtkMovCompassWidget *New();

	// Description:
	// Standard macros.
	vtkTypeMacro(vtkMovCompassWidget, vtkAbstractWidget);
	void PrintSelf(ostream& os, vtkIndent indent);

	// Description:
	// Specify an instance of vtkWidgetRepresentation used to represent this
	// widget in the scene. Note that the representation is a subclass of vtkProp
	// so it can be added to the renderer independent of the widget.
	void SetRepresentation(vtkMovCompassRepresentation *r)
	{
		this->Superclass::SetWidgetRepresentation
		(reinterpret_cast<vtkWidgetRepresentation*>(r));
	}

	// Description:
	// Create the default widget representation if one is not set. 
	void CreateDefaultRepresentation();

	// Description:
	// Get the value for this widget. 
	double GetHeading();
	void SetHeading(double v);
	double GetTilt();
	void SetTilt(double t);
	double GetDistance();
	void SetDistance(double t);

protected:
	vtkMovCompassWidget();
	~vtkMovCompassWidget() {}

	// These are the events that are handled
	static void SelectAction(vtkAbstractWidget*);
	static void EndSelectAction(vtkAbstractWidget*);
	static void MoveAction(vtkAbstractWidget*);
	static void TimerAction(vtkAbstractWidget*);

	//BTX - manage the state of the widget
	int WidgetState;
	enum _WidgetState
	{
		Start = 0,
		Hovering,
		Adjusting,
		TiltAdjusting,
		DistanceAdjusting
	};
	//ETX

	int TimerId;
	int TimerDuration;
	double StartTime;

private:
	vtkMovCompassWidget(const vtkMovCompassWidget&);  //Not implemented
	void operator=(const vtkMovCompassWidget&);  //Not implemented
};

#endif
