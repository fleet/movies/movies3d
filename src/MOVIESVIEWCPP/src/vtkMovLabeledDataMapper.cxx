/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkLabeledDataMapper.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

	 This software is distributed WITHOUT ANY WARRANTY; without even
	 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
	 PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkMovLabeledDataMapper.h"

#include "vtkExecutive.h"
#include "vtkInformation.h"
#include "vtkActor2D.h"
#include "vtkDataArray.h"
#include "vtkDataSet.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkTextMapper.h"
#include "vtkTextProperty.h"
#include "vtkViewport.h"
#include "vtkRenderer.h"
#include "vtkCamera.h"
#include "vtkPoints.h"
#include "vtkIdTypeArray.h"
#include "vtkAbstractArray.h"

#include "M3DKernel/DefConstants.h"

vtkStandardNewMacro(vtkMovLabeledDataMapper);

void vtkMovLabeledDataMapper::AddLabel(const std::string &text)
{
	vtkTextMapper *textMapper = vtkTextMapper::New();

	textMapper->SetInput(text.c_str());
	textMapper->SetTextProperty(LabelTextProperty);

	m_TextMappers.push_back(textMapper);
}

void vtkMovLabeledDataMapper::SetLabels(const std::vector<std::string> & labels)
{
	ClearLabels();
	for (auto &label : labels) {
		AddLabel(label);
	}
}

void vtkMovLabeledDataMapper::ClearLabels()
{
	m_Labels.clear();
	for (auto &textMapper : m_TextMappers) {
		textMapper->Delete();
	}
	m_TextMappers.clear();
}

//----------------------------------------------------------------------------
// Creates a new label mapper

vtkMovLabeledDataMapper::vtkMovLabeledDataMapper() : m_TextMappers{}
{
	this->Input = NULL;

	this->NumberOfLabels = 0;
	this->LabelTextProperty = vtkTextProperty::New();
	this->LabelTextProperty->SetFontSize(12);
	this->LabelTextProperty->SetBold(0);
	this->LabelTextProperty->SetItalic(0);
	this->LabelTextProperty->SetShadow(1);
	this->LabelTextProperty->SetColor(0, 0, 0);
	this->LabelTextProperty->SetFontFamilyToArial();

	this->LabelWidth = NULL;
	this->LabelHeight = NULL;
	this->Cutoff = NULL;
	this->LabelHeightPadding = 15;
	this->LabelWidthPadding = 10;

	this->m_ScaleY = 1.0;
}

//----------------------------------------------------------------------------
vtkMovLabeledDataMapper::~vtkMovLabeledDataMapper()
{
	if (this->LabelWidth)
	{
		delete[] this->LabelWidth;
		this->LabelWidth = NULL;
	}
	if (this->LabelHeight)
	{
		delete[] this->LabelHeight;
		this->LabelHeight = NULL;
	}
	if (this->Cutoff)
	{
		delete[] this->Cutoff;
		this->Cutoff = NULL;
	}

	ClearLabels();

	this->LabelTextProperty->Delete();
}

//----------------------------------------------------------------------------
void vtkMovLabeledDataMapper::SetInput(vtkDataSet *input)
{
	if (input)
	{
		this->SetInputDataObject(0, input);
	}
	else
	{
		// Setting a NULL input removes the connection.
		this->SetInputDataObject(0, NULL);
	}
}

//----------------------------------------------------------------------------
// Specify the input data or filter.
vtkDataSet *vtkMovLabeledDataMapper::GetInput()
{
	return vtkDataSet::SafeDownCast(
		this->GetExecutive()->GetInputData(0, 0));
}

//----------------------------------------------------------------------------
// Release any graphics resources that are being consumed by this mapper.
void vtkMovLabeledDataMapper::ReleaseGraphicsResources(vtkWindow *win)
{
	for (auto &textMapper : m_TextMappers)
	{
		textMapper->ReleaseGraphicsResources(win);
	}
}

//----------------------------------------------------------------------------
void vtkMovLabeledDataMapper::RenderOverlay(vtkViewport *viewport,
	vtkActor2D *actor)
{
	int i;
	double x[3];
	vtkDataSet *input = this->GetInput();

	// Determine the current scale
	double scale = this->GetCurrentScale(viewport) / this->ReferenceScale;

	if (!input)
	{
		return;
	}
	for (i = 0; i < this->NumberOfLabels; i++)
	{
		input->GetPoint(i, x);
		x[1] = m_ScaleY*x[1];
		actor->GetPositionCoordinate()->SetCoordinateSystemToWorld();
		actor->GetPositionCoordinate()->SetValue(x);
		double* display = actor->GetPositionCoordinate()->GetComputedDoubleDisplayValue(viewport);
		double screenX = display[0];
		double screenY = display[1];

		bool inside =
			viewport->IsInViewport(
				static_cast<int>(screenX + this->LabelWidth[i]),
				static_cast<int>(screenY + this->LabelHeight[i]))
			|| viewport->IsInViewport(
				static_cast<int>(screenX + this->LabelWidth[i]),
				static_cast<int>(screenY - this->LabelHeight[i]))
			|| viewport->IsInViewport(
				static_cast<int>(screenX - this->LabelWidth[i]),
				static_cast<int>(screenY + this->LabelHeight[i]))
			|| viewport->IsInViewport(
				static_cast<int>(screenX - this->LabelWidth[i]),
				static_cast<int>(screenY - this->LabelHeight[i]));
		if (inside && (1.0f / scale) < this->Cutoff[i])
		{
			m_TextMappers[i]->RenderOverlay(viewport, actor);
		}
	}
}

//----------------------------------------------------------------------------
void vtkMovLabeledDataMapper::RenderOpaqueGeometry(vtkViewport *viewport,
	vtkActor2D *actor)
{
	int i, j, numComp = 0, activeComp = 0;
	double x[3];
	vtkDataSet *input = this->GetInput();

	if (!input)
	{
		return;
	}

	vtkPointData *pd = input->GetPointData();

	//input->Update();

	vtkDebugMacro(<< "Rebuilding labels");

	//
	// Perform the label layout preprocessing
	//
	this->NumberOfLabels = input->GetNumberOfPoints();

	// Calculate height and width padding
	float widthPadding = 0, heightPadding = 0;
	if (this->NumberOfLabels > 0)
	{
		widthPadding = m_TextMappers[0]->GetHeight(viewport) *
			this->LabelWidthPadding / 100.0;
		heightPadding = m_TextMappers[0]->GetHeight(viewport) *
			this->LabelHeightPadding / 100.0;
	}

	// Calculate label widths / heights
	if (this->LabelWidth != NULL)
	{
		delete[] this->LabelWidth;
	}
	this->LabelWidth = new float[this->NumberOfLabels];
	for (i = 0; i < this->NumberOfLabels; i++)
	{
		this->LabelWidth[i] = m_TextMappers[i]->GetWidth(viewport) +
			widthPadding;
	}

	if (this->LabelHeight != NULL)
	{
		delete[] this->LabelHeight;
	}
	this->LabelHeight = new float[this->NumberOfLabels];
	for (i = 0; i < this->NumberOfLabels; i++)
	{
		this->LabelHeight[i] = m_TextMappers[i]->GetHeight(viewport) +
			heightPadding;
	}

	// Determine cutoff scales of each point
	if (this->Cutoff != NULL)
	{
		delete[] this->Cutoff;
	}
	this->Cutoff = new float[this->NumberOfLabels];

	vtkCoordinate* coord = vtkCoordinate::New();
	coord->SetViewport(viewport);
	vtkPoints* pts = vtkPoints::New();
	for (i = 0; i < this->NumberOfLabels; i++)
	{
		double* dc;
		double pti[3];
		input->GetPoint(i, pti);
		coord->SetValue(pti);
		dc = coord->GetComputedDoubleDisplayValue(0);
		pts->InsertNextPoint(dc[0], dc[1], 0);
	}
	coord->Delete();

	// Create an index array to store the offsets of the sorted elements.
	vtkIdTypeArray* index = vtkIdTypeArray::New();
	index->SetNumberOfValues(this->NumberOfLabels);
	for (i = 0; i < this->NumberOfLabels; i++)
	{
		index->SetValue(i, i);
	}

	vtkIdType begin = 0;
	vtkIdType end = this->NumberOfLabels;
	vtkIdType step = 1;
	for (i = begin; i != end; i += step)
	{
		vtkIdType indexI = index->GetValue(i);
		float* pti = reinterpret_cast<float*>(pts->GetVoidPointer(3 * indexI));
		this->Cutoff[indexI] = VTK_FLOAT_MAX;
		for (j = begin; j != i; j += step)
		{
			vtkIdType indexJ = index->GetValue(j);
			float* ptj = reinterpret_cast<float*>(pts->GetVoidPointer(3 * indexJ));
			float absX = (pti[0] - ptj[0]) > 0 ? (pti[0] - ptj[0]) : -(pti[0] - ptj[0]);
			float absY = (pti[1] - ptj[1]) > 0 ? (pti[1] - ptj[1]) : -(pti[1] - ptj[1]);
			float xScale = 2 * absX / (this->LabelWidth[indexI] + this->LabelWidth[indexJ]);
			float yScale = 2 * absY / (this->LabelHeight[indexI] + this->LabelHeight[indexJ]);
			float maxScale = xScale < yScale ? yScale : xScale;
			if (maxScale < this->Cutoff[indexJ] && maxScale < this->Cutoff[indexI])
			{
				this->Cutoff[indexI] = maxScale;
			}
		}
	}
	index->Delete();
	pts->Delete();

	// Determine the reference scale
	this->ReferenceScale = this->GetCurrentScale(viewport);

	//
	// Draw labels visible in the current scale
	//

	for (i = 0; i < this->NumberOfLabels; i++)
	{
		input->GetPoint(i, x);
		if (1 < this->Cutoff[i])
		{
			actor->GetPositionCoordinate()->SetCoordinateSystemToWorld();
			actor->GetPositionCoordinate()->SetValue(x);
			m_TextMappers[i]->RenderOpaqueGeometry(viewport, actor);
		}
	}
}

//----------------------------------------------------------------------------
double vtkMovLabeledDataMapper::GetCurrentScale(vtkViewport *viewport)
{
	// The current scale is the size on the screen of 1 unit in the xy plane

	vtkRenderer* ren = vtkRenderer::SafeDownCast(viewport);
	if (!ren)
	{
		vtkErrorMacro("vtkMovLabeledDataMapper only works in a vtkRenderer or subclass");
		return 1.0;
	}
	vtkCamera* camera = ren->GetActiveCamera();
	if (camera->GetParallelProjection())
	{
		// For parallel projection, the scale depends on the parallel scale 
		double scale = (ren->GetSize()[1] / 2.0) / camera->GetParallelScale();
		return scale;
	}
	else
	{
		// For perspective projection, the scale depends on the view angle
		double viewAngle = camera->GetViewAngle();
		double distZ = camera->GetPosition()[2] > 0 ? camera->GetPosition()[2] : -camera->GetPosition()[2];
		double unitAngle = DEG_TO_RAD(atan2(1.0, distZ));
		double scale = ren->GetSize()[1] * unitAngle / viewAngle;
		return scale;
	}
}

//----------------------------------------------------------------------------
int vtkMovLabeledDataMapper::FillInputPortInformation(
	int vtkNotUsed(port), vtkInformation* info)
{
	info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
	return 1;
}

//----------------------------------------------------------------------------
void vtkMovLabeledDataMapper::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);

	if (this->Input)
	{
		os << indent << "Input: (" << this->Input << ")\n";
	}
	else
	{
		os << indent << "Input: (none)\n";
	}

	if (this->LabelTextProperty)
	{
		os << indent << "Label Text Property:\n";
		this->LabelTextProperty->PrintSelf(os, indent.GetNextIndent());
	}
	else
	{
		os << indent << "Label Text Property: (none)\n";
	}
}
