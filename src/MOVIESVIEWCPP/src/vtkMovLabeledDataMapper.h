/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkLabeledDataMapper.h,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

	 This software is distributed WITHOUT ANY WARRANTY; without even
	 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
	 PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkLabeledDataMapper - draw text labels at dataset points
// .SECTION Description
// vtkLabeledDataMapper is a mapper that renders text at dataset
// points. Various items can be labeled including point ids, scalars,
// vectors, normals, texture coordinates, tensors, and field data components.
//
// The format with which the label is drawn is specified using a
// printf style format string. The font attributes of the text can
// be set through the vtkTextProperty associated to this mapper. 
//
// By default, all the components of multi-component data such as
// vectors, normals, texture coordinates, tensors, and multi-component
// scalars are labeled. However, you can specify a single component if
// you prefer. (Note: the label format specifies the format to use for
// a single component. The label is creating by looping over all components
// and using the label format to render each component.)

// .SECTION Caveats
// Use this filter in combination with vtkSelectVisiblePoints if you want
// to label only points that are visible. If you want to label cells rather
// than points, use the filter vtkCellCenters to generate points at the
// center of the cells. Also, you can use the class vtkIdFilter to
// generate ids as scalars or field data, which can then be labeled.

// .SECTION See Also
// vtkMapper2D vtkActor2D vtkTextMapper vtkTextProperty vtkSelectVisiblePoints 
// vtkIdFilter vtkCellCenters

#ifndef __vtkMovLabeledDataMapper_h
#define __vtkMovLabeledDataMapper_h

#include "vtkMapper2D.h"

#include <vector>

class vtkDataSet;
class vtkTextMapper;
class vtkTextProperty;


class vtkMovLabeledDataMapper : public vtkMapper2D
{
public:
	// Description:
	// Instantiate object with %%-#6.3g label format. By default, point ids
	// are labeled.
	static vtkMovLabeledDataMapper *New();

	vtkTypeMacro(vtkMovLabeledDataMapper, vtkMapper2D);
	void PrintSelf(ostream& os, vtkIndent indent);

	// Description:
	// Set the input dataset to the mapper. This mapper handles any type of data.
	virtual void SetInput(vtkDataSet*);
	vtkDataSet *GetInput();

	// Description:
	// Draw the text to the screen at each input point.
	void RenderOpaqueGeometry(vtkViewport* viewport, vtkActor2D* actor);
	void RenderOverlay(vtkViewport* viewport, vtkActor2D* actor);

	// Description:
	// Release any graphics resources that are being consumed by this actor.
	virtual void ReleaseGraphicsResources(vtkWindow *);

	double m_ScaleY;

	void AddLabel(const std::string &label);
	void SetLabels(const std::vector<std::string> & labels);
	void ClearLabels();

protected:
	vtkMovLabeledDataMapper();
	~vtkMovLabeledDataMapper();

	// Description:
  // Calculate the current zoom scale of the viewport.
	double GetCurrentScale(vtkViewport *viewport);

	float* LabelWidth;
	float* LabelHeight;
	float* Cutoff;
	float ReferenceScale;
	float LabelHeightPadding;
	float LabelWidthPadding;

	vtkDataSet *Input;
	vtkTextProperty *LabelTextProperty;

	vtkTimeStamp BuildTime;

private:
	int NumberOfLabels;
	std::vector<vtkTextMapper *> m_TextMappers;

	// Ensemble des chaines de caract�res � mapper sur les points
	std::vector<std::string> m_Labels;

	virtual int FillInputPortInformation(int, vtkInformation*);

private:
	vtkMovLabeledDataMapper(const vtkMovLabeledDataMapper&);  // Not implemented.
	void operator=(const vtkMovLabeledDataMapper&);  // Not implemented.
};

#endif

