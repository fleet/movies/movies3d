
#include "vtkMovSurfaceFilter.h"

#include "vtkCellArray.h"
#include "vtkMath.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkPolyLine.h"
#include "vtkPolygon.h"

vtkStandardNewMacro(vtkMovSurfaceFilter);

vtkMovSurfaceFilter::vtkMovSurfaceFilter()
{
}

vtkMovSurfaceFilter::~vtkMovSurfaceFilter()
{
}

int vtkMovSurfaceFilter::RequestData(
	vtkInformation *vtkNotUsed(request),
	vtkInformationVector **inputVector,
	vtkInformationVector *outputVector)
{
	// get the info objects
	vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
	vtkInformation *outInfo = outputVector->GetInformationObject(0);

	// get the input and ouptut
	vtkPolyData *input = vtkPolyData::SafeDownCast(
		inInfo->Get(vtkDataObject::DATA_OBJECT()));
	vtkPolyData *output = vtkPolyData::SafeDownCast(
		outInfo->Get(vtkDataObject::DATA_OBJECT()));

	vtkPoints *inPts, *newPts = NULL;
	vtkIdType i, numPts, numLines;
	vtkCellArray *inLines, *newPolys;
	vtkIdType *pts = 0;
	vtkIdType *pts2 = 0;
	vtkIdType npts = 0;
	vtkIdType npts2 = 0;
	vtkPointData *inPD = input->GetPointData(), *outPD = output->GetPointData();

	// Check input, pass data if requested
	//
	inPts = input->GetPoints();
	inLines = input->GetLines();
	if (!inPts || !inLines)
	{
		return 1;
	}
	numLines = inLines->GetNumberOfCells();
	numPts = inPts->GetNumberOfPoints();
	if (numPts < 1 || numLines < 2)
	{
		return 1;
	}

	output->SetPoints(inPts);
	output->GetPointData()->PassData(input->GetPointData());
	newPolys = vtkCellArray::New();
	newPolys->Allocate(2 * numPts);
	output->SetPolys(newPolys);
	newPolys->Delete();

	// For each pair of lines (as selected by Offset and OnRatio), create a
	// stripe (a ruled surfac between two lines). 
	//
	inLines->InitTraversal();

	for (i = 0; i < numLines; i++)
	{
		inLines->GetNextCell(npts, pts);
		//abort/progress methods
		this->UpdateProgress((double)i / numLines);
		if (this->GetAbortExecute())
		{
			break; //out of line loop
		}
		this->Triangulate(output, inPts, npts, pts);
	}//for all selected line pairs

	return 1;
}


void  vtkMovSurfaceFilter::Triangulate(vtkPolyData *output, vtkPoints *inPts,
	int npts, vtkIdType *pts)
{
	if (npts >= 3)
	{
		vtkCellArray *newPolys = output->GetPolys();
		if (npts == 3)
		{
			newPolys->InsertNextCell(3, pts);
		}
		else
		{

			vtkIdType i, j;
			vtkIdType triPts[3];
			vtkIdList *ptIds = vtkIdList::New();
			ptIds->Allocate(VTK_CELL_SIZE);

			vtkPolygon * poly = vtkPolygon::New();
			//initialize polygon
			poly->PointIds->SetNumberOfIds(npts);
			poly->Points->SetNumberOfPoints(npts);
			for (i = 0; i < npts; i++)
			{
				poly->PointIds->SetId(i, pts[i]);
				poly->Points->SetPoint(i, inPts->GetPoint(pts[i]));
			}
			poly->Triangulate(ptIds);
			vtkIdType numPts = ptIds->GetNumberOfIds();
			vtkIdType numSimplices = numPts / 3;
			for (i = 0; i < numSimplices; i++)
			{
				for (j = 0; j < 3; j++)
				{
					triPts[j] = poly->PointIds->GetId(ptIds->GetId(3 * i + j));
				}
				newPolys->InsertNextCell(3, triPts);
			}//for each simplex

			poly->Delete();
			ptIds->Delete();
		}
	}
}

void vtkMovSurfaceFilter::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
}

