// -*- C++ -*-
// ****************************************************************************
// Class: vtkMovSurfaceFilter
//
// Description: cr�e les surfaces des voxels travers�s par la coupe 2D/3D
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Septembre 2009
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "vtkPolyDataAlgorithm.h"

class vtkIdList;
class vtkPoints;
class vtkPolyData;


class vtkMovSurfaceFilter : public vtkPolyDataAlgorithm
{
public:
	vtkTypeMacro(vtkMovSurfaceFilter, vtkPolyDataAlgorithm);
	void PrintSelf(ostream& os, vtkIndent indent);

	// Description:
	// Construct object with OnRatio=1, Offset=0. DistanceFactor=3.0,
	// CloseSurface off, and PassLines off.
	static vtkMovSurfaceFilter *New();

protected:
	vtkMovSurfaceFilter();
	~vtkMovSurfaceFilter();

	// Usual data generation method
	int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

private:

	void  Triangulate(vtkPolyData *output, vtkPoints *inPts,
		int npts, vtkIdType *pts);

private:
	vtkMovSurfaceFilter(const vtkMovSurfaceFilter&);  // Not implemented.
	void operator=(const vtkMovSurfaceFilter&);  // Not implemented.
};
