
#include "vtk.h"
#include "vtkPaletteScalarsToColors.h"

#include "ColorPalette.h"
#include "ColorUtils.h"

vtkStandardNewMacro(vtkPaletteScalarsToColors);

// Construct a new vtkColorTransferFunctionAlpha with default values
vtkPaletteScalarsToColors::vtkPaletteScalarsToColors()
	: m_echoColors(65536, 0x00000000)
{
}

vtkPaletteScalarsToColors::~vtkPaletteScalarsToColors()
{
}

void vtkPaletteScalarsToColors::MapScalarsThroughTable2(
	void *input, unsigned char *output,
	int inputDataType, int numberOfValues,
	int inputIncrement,
	int outputFormat)
{
	if (outputFormat == VTK_RGBA)
	{
		float * data = static_cast<float*>(input);	
		for (int i = 0; i < numberOfValues; ++i)
		{
			DataFmt value = data[i];
			ColorUtils::Color color(m_echoColors[(int)value + 32768]);
			*(output++) = color.r;
			*(output++) = color.g;
			*(output++) = color.b;
			*(output++) = color.a;
		}		
	}
}

void vtkPaletteScalarsToColors::setPalette(const IColorPalette * palette, double globalOpacity, bool useAlpha)
{
	if (palette == nullptr)
		return;

	const int min = -32768;
	const int max = 32768;

	for (int i = min; i < max; ++i)
	{
		ColorUtils::Color color = palette->GetColor(i);
		if (useAlpha)
		{
			color.a = color.a * globalOpacity;
		}
		else
		{
			color.a = 255;
		}
		m_echoColors[i + 32768] = color.argb;
	}

	this->Modified();
}