#pragma once

#include "vtkScalarsToColors.h"

class IColorPalette;

class vtkPaletteScalarsToColors : public vtkScalarsToColors
{
	std::vector<unsigned int> m_echoColors;
	const double colorFactor = 1.0 / 255.0;

public:
	static vtkPaletteScalarsToColors *New();

	vtkPaletteScalarsToColors();
	~vtkPaletteScalarsToColors();

	virtual void MapScalarsThroughTable2(
		void *input, unsigned char *output,
		int inputDataType, int numberOfValues,
		int inputIncrement,
		int outputFormat);

	void setPalette(const IColorPalette * palette, double globalOpacity, bool useAlpha);
};
