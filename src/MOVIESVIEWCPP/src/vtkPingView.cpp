
#include "vtk.h"
#include "vtkPingView.h"

//#include <vtkProgrammableGlyphFilter.h>
#include <vtkTransform.h>

#include "vtkGlyph4me.h"
#include <vtkCylinderSource.h>

#include "DisplayParameter.h"

vtkPingView::vtkPingView(std::uint32_t afanIdx)
{
	m_glyph = NULL;
	m_phaseGlyph = NULL;
	this->m_fanIdx = afanIdx;
	m_bUseCube = true;
}

vtkPingView::~vtkPingView()
{
	if (m_glyph)
	{
		m_glyph->Delete();
		m_glyph = NULL;
	}

	if (m_phaseGlyph)
	{
		m_phaseGlyph->Delete();
		m_phaseGlyph = NULL;
	}
}

vtkGlyph4me* vtkPingView::getGlyph()
{
	if (DisplayParameter::getInstance()->UseGlyphCube() != this->m_bUseCube)
	{
		this->m_bUseCube = !this->m_bUseCube;
		if (m_glyph)
		{
			m_glyph->Delete();
			m_glyph = NULL;
		}
	}

	if (!m_glyph)
	{
		/**
		create glyph cube
		*/
		int i;
		m_glyph = vtkGlyph4me::New();
		if (m_bUseCube)
		{
			static float x[8][3] = {
				{-0.5,-0.5,0.5}, {0.5,-0.5,0.5}, {0.5,0.5,0.5}, {-0.5,0.5,0.5},{-0.5,-0.5,-0.5}, {0.5,-0.5,-0.5}, {0.5,0.5,-0.5}, {-0.5,0.5,-0.5} };
			static vtkIdType pts[6][4] = { {2,3,7,6}, {4,5,6,7}, {1,2,6,5},{3,0,4,7},{0,1,2,3},{0,1,5,4}
			};

			// We'll create the building blocks of polydata including data attributes.
			vtkPolyData *cube = vtkPolyData::New();
			vtkPoints *points = vtkPoints::New();
			vtkCellArray *polys = vtkCellArray::New();

			// Load the point, cell, and data attributes.
			for (i = 0; i < 8; i++) points->InsertPoint(i, x[i]);
			for (i = 0; i < 6; i++) polys->InsertNextCell(4, pts[i]);

			// We now assign the pieces to the vtkPolyData.
			cube->SetPoints(points);
			points->Delete();
			cube->SetPolys(polys);
			polys->Delete();
			m_glyph->SetSourceData(cube);
			cube->Delete();
		}
		else
		{
			vtkCylinderSource *cube = vtkCylinderSource::New();
			cube->SetResolution(8);
			cube->SetCenter(0, 0, 0);
			cube->SetHeight(1);
			cube->SetRadius(0.5);
			m_glyph->SetSourceConnection(cube->GetOutputPort());
			cube->Delete();
		}
		// end create cube

		//cube->
		//
		m_glyph->ClampingOff();
		m_glyph->OrientOff();
		m_glyph->SetScaleModeToScaleByVectorComponents();
		m_glyph->SetColorModeToColorByScalar();

		m_glyph->SetOrientWithNormal(true);

		m_glyph->SetIndexModeToOff();
	}
	return m_glyph;
}

vtkGlyph3D* vtkPingView::getPhaseGlyph()
{
	return m_phaseGlyph;
}

void vtkPingView::setPhaseGlyph(vtkGlyph3D* glyph)
{
	if (m_phaseGlyph)
		m_phaseGlyph->Delete();
	m_phaseGlyph = glyph;
}
