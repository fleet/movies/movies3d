
/******************************************************************************/
/*	Project:	MOVIE 3d														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		vtkPingView.h												  */
/******************************************************************************/
#ifndef AVITIS_3D_VIEW_VOLUME_PING
#define AVITIS_3D_VIEW_VOLUME_PING


class vtkPolyData;
class vtkPolyDataMapper;

class vtkOutlineSource;
class vtkSphereSource;
class vtkPoint;

class vtkGlyph4me;
class vtkMovAppendPolyData;

class vtkGlyph3D;

/*
* cette classe gere les pings de la scene
*
*
*
*/

class vtkPingView
{
public:
	vtkPingView(std::uint32_t afanIdx);
	~vtkPingView();

	vtkGlyph4me* getGlyph();
	std::uint32_t getFanId() const { return m_fanIdx; }

	// NMD - FAE 109 - Accesseurs sur le glyphe 3D associ� 
	vtkGlyph3D* getPhaseGlyph();
	void setPhaseGlyph(vtkGlyph3D* glyph);

private:
	vtkGlyph4me* m_glyph;
	std::uint32_t m_fanIdx;

	// NMD - FAE 109 - Sauvegarde du glyphe 3D deja calcul�
	vtkGlyph3D* m_phaseGlyph;

	bool m_bUseCube;
};





#endif
