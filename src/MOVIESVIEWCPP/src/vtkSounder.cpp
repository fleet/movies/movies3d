#include "vtkSounder.h"
#include "vtkAppendPolyData.h"
#include "vtkDepthSortPolyData.h"
#include "vtkVolumeSingleEcho.h"
#include "vtkVolumePhaseEcho.h"
#include "vtkVolumeShoal.h"
#include "vtkPlaneCollection.h"

#include "vtkPaletteScalarsToColors.h"

vtkSounder::vtkSounder(REFRESH3D_CALLBACK refreshCB)
{
	m_volumeActor = NULL;
	m_pSort = NULL;
	m_pMapper = NULL;
	m_volumeActor = vtkActor::New();
	m_pMapper = vtkPolyDataMapper::New();
	m_colorFunction = NULL;
	m_pVolumeSeaFloor = NULL;
	m_pVolumeSingleEcho = NULL;
	m_pVolumePhaseEcho = NULL;
	m_pBuildingVolume = NULL;
	m_pLastAppliedVolume = NULL;
	m_pVolumeShoal = NULL;
	m_bDisplay = true;
	m_bWireFrame = false;


	m_bnRendering = false;
	m_globalOpacity = 1.0;

	m_pMapper->ScalarVisibilityOn();
	/*
	* By default, VTK uses OpenGL display lists which results in another copy of the data being stored in memory. For most large datasets you will be better off saving memory by not using display lists. You can turn off display lists by turning on ImmediateModeRendering. This can be controlled on a mapper by mapper basis using ImmediateModeRendering, or globally for all mappers in a process by using GlobalImmediateModeRendering.
	* thus we set this mode on while we destroy the mapper at each cycle
	*/

	/// do not use this one : 	mapper->ImmediateModeRenderingOn();
	m_pMapper->GlobalImmediateModeRenderingOn();
	m_colorFunction = vtkPaletteScalarsToColors::New();
	m_pMapper->SetLookupTable(m_colorFunction);

	m_pVolumeSeaFloor = new vtkVolumeSeaFloor();
	m_pVolumeSingleEcho = new vtkVolumeSingleEcho();
	m_pVolumePhaseEcho = new vtkVolumePhaseEcho();
	m_pVolumePhaseEcho->SetSize(DisplayParameter::getInstance()->GetEchoDisplaySize());

	m_pVolumeShoal = new vtkVolumeShoal(m_colorFunction, refreshCB);

	m_pVolumeBeam = new vtkVolumeBeam();

	m_pPlaneCut = NULL;
	m_pCutStrips = NULL;
	m_pContourActor = NULL;
	m_pContourMapper = NULL;
}

vtkSounder::~vtkSounder(void)
{
	m_colorFunction->Delete();
	m_pMapper->Delete();
	if (m_pSort)
		m_pSort->Delete();
	m_volumeActor->Delete();

	if (m_pBuildingVolume)
		delete m_pBuildingVolume;
	if (m_pLastAppliedVolume)
		delete m_pLastAppliedVolume;
	if (m_pVolumeSeaFloor)
		delete m_pVolumeSeaFloor;
	if (m_pVolumeSingleEcho)
		delete m_pVolumeSingleEcho;
	if (m_pVolumePhaseEcho)
		delete m_pVolumePhaseEcho;
	if (m_pVolumeBeam)
		delete m_pVolumeBeam;
	if (m_pVolumeShoal)
		delete m_pVolumeShoal;
	if (m_pPlaneCut)
		m_pPlaneCut->Delete();
	if (m_pCutStrips)
		m_pCutStrips->Delete();
	if (m_pContourActor)
		m_pContourActor->Delete();
	if (m_pContourMapper)
		m_pContourMapper->Delete();
}

bool  vtkSounder::isUseSort()
{
	return m_pSort != NULL;
}

void vtkSounder::SetToWireFrame(bool a)
{
	if (this->IsWireFrame() == a)
		return;
	if (a && m_volumeActor)
	{
		this->m_volumeActor->GetProperty()->SetRepresentationToWireframe();
		m_bWireFrame = true;
	}
	else
	{
		this->m_volumeActor->GetProperty()->SetRepresentationToSurface();
		m_bWireFrame = false;
	}

	if (m_pVolumePhaseEcho)
	{
		m_pVolumePhaseEcho->RenderWireframe(a);
	}
}

void  vtkSounder::SetSort(bool a, vtkCamera *pcam)
{
	if (isUseSort() != a)
	{
		if (a)
		{
			m_pSort = vtkDepthSortPolyData::New();
			m_pSort->SetInputData(m_pMapper->GetInput());
			m_pSort->SetDirectionToBackToFront();
			m_pSort->SetDepthSortModeToBoundsCenter();
			m_pSort->SortScalarsOff();
			m_pSort->SetCamera(pcam);
			m_pMapper->SetInputConnection(m_pSort->GetOutputPort());
		}
		else {
			m_pMapper->SetInputConnection(NULL);
			m_pSort->SetInputData(NULL);
			m_pSort->Delete();
			m_pSort = NULL;
		}
	}
}
void vtkSounder::SetGlobalOpacity(double a)
{
	m_globalOpacity = a;
}

void vtkSounder::RemoveFromRenderer(vtkRenderer* ren)
{
	ren->RemoveActor(m_volumeActor);
	this->m_pVolumeBeam->RemoveFromRenderer(ren);
	this->m_pVolumeShoal->RemoveFromRenderer(ren);
	this->m_pVolumePhaseEcho->RemoveFromRenderer(ren);
	this->m_pVolumeSingleEcho->RemoveFromRenderer(ren);
	if (m_pContourActor)
	{
		ren->RemoveActor(m_pContourActor);
	}
}

void vtkSounder::AddToRenderer(vtkRenderer* ren)
{
	ren->AddActor(m_volumeActor);
	this->m_pVolumeBeam->AddToRenderer(ren);
	this->m_pVolumeShoal->AddToRenderer(ren);
	this->m_pVolumePhaseEcho->AddToRenderer(ren);
	this->m_pVolumeSingleEcho->AddToRenderer(ren);
	if (m_pContourActor)
	{
		ren->AddActor(m_pContourActor);
	}
}

void vtkSounder::SetVolumeScale(double x, double y, double z)
{
	m_volumeActor->SetScale(x, y, z);
	m_pVolumeSeaFloor->SetVolumeScale(x, y, z);
	m_pVolumeSingleEcho->SetVolumeScale(x, y, z);
	m_pVolumePhaseEcho->SetVolumeScale(x, y, z);
	m_pVolumeBeam->SetVolumeScale(x, y, z);
	m_pVolumeShoal->SetVolumeScale(x, y, z);
	if (m_pContourActor)
	{
		m_pContourActor->SetScale(x, y, z);
	}
}

MergedEchoList *vtkSounder::GetBuildingVolume()
{
	if (m_pBuildingVolume)
		delete m_pBuildingVolume;
	m_pBuildingVolume = new MergedEchoList();
	return m_pBuildingVolume;
}
void vtkSounder::SetBuildingVolume(CameraSettings ^a_cameraSettings)
{
	if (m_pBuildingVolume && m_pBuildingVolume->GetEchoPolyData())
	{
		a_cameraSettings->m_wholeBoundVolA.x = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_boundingBox.m_minBox)).x;
		a_cameraSettings->m_wholeBoundVolA.y = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_boundingBox.m_minBox)).y;
		a_cameraSettings->m_wholeBoundVolA.z = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_boundingBox.m_minBox)).z;
		a_cameraSettings->m_wholeBoundVolB.x = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_boundingBox.m_maxBox)).x;
		a_cameraSettings->m_wholeBoundVolB.y = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_boundingBox.m_maxBox)).y;
		a_cameraSettings->m_wholeBoundVolB.z = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_boundingBox.m_maxBox)).z;

		a_cameraSettings->m_boundVolA.x = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_lastFanBoundingBox.m_minBox)).x;
		a_cameraSettings->m_boundVolA.y = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_lastFanBoundingBox.m_minBox)).y;
		a_cameraSettings->m_boundVolA.z = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_lastFanBoundingBox.m_minBox)).z;
		a_cameraSettings->m_boundVolB.x = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_lastFanBoundingBox.m_maxBox)).x;
		a_cameraSettings->m_boundVolB.y = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_lastFanBoundingBox.m_maxBox)).y;
		a_cameraSettings->m_boundVolB.z = (BaseMathLib::Vector3D::ToViewOpengl(m_pBuildingVolume->m_lastFanBoundingBox.m_maxBox)).z;
	}
	if (isUseSort())
	{
		if (m_pBuildingVolume->GetEchoPolyData() && m_pBuildingVolume->getNbEntry())
		{
			m_pSort->SetInputConnection(m_pBuildingVolume->GetEchoPolyData()->GetOutputPort());
			m_volumeActor->SetVisibility(true);
		}
		else
		{
			m_volumeActor->SetVisibility(false);
		}
	}
	else
	{
		if (m_pBuildingVolume->GetEchoPolyData() && m_pBuildingVolume->getNbEntry())
		{
			m_pMapper->SetInputConnection(m_pBuildingVolume->GetEchoPolyData()->GetOutputPort());
			m_volumeActor->SetVisibility(true);
		}
		else
		{
			m_pMapper->SetInputConnection(NULL);
			m_volumeActor->SetVisibility(false);
		}
	}
	m_volumeActor->SetMapper(m_pMapper);

	//m_pVolumeBeam->setVisible(m_volumeActor->GetVisibility() == 1);

	if (m_pBuildingVolume)
	{
		this->m_pVolumeSeaFloor->applyFloorPolyData(m_pBuildingVolume->GetFloorPolyData());
		this->m_pVolumeSingleEcho->applyGlyph(m_pBuildingVolume->GetSingleEchoGlyph());
		this->m_pVolumeSingleEcho->applyTracks(m_pBuildingVolume->GetSingleEchoTracks(),
			m_pBuildingVolume->GetSingleEchoTracksLabelPositions(), m_pBuildingVolume->GetSingleEchoTracksLabels());
		this->m_pVolumePhaseEcho->applyPolyData(m_pBuildingVolume->GetPhaseEchoData());
		if (m_pBuildingVolume->m_transducerBeamOpening.size())
		{

			m_pVolumeBeam->Compute(m_pBuildingVolume->m_transducerBeamOpening);
			if (m_pContourActor)
			{
				m_pContourActor->SetVisibility(1);
			}
		}
		else
		{
			if (m_pContourActor)
			{
				// on n'affiche pas la coupe 3D si on n'a pas d'�chos
				m_pContourActor->SetVisibility(0);
			}
		}
	}
	if (this->m_pLastAppliedVolume)
		delete m_pLastAppliedVolume;
	this->m_pLastAppliedVolume = this->m_pBuildingVolume;
	m_pBuildingVolume = NULL;

	// mise � jour de la coupe 3D lorsqu'on change les �chos visualis�s
	if (m_pPlaneCut)
	{
		m_pPlaneCut->SetInputConnection(GetLastBuildingVolume()->GetEchoPolyData()->GetOutputPort());
	}
}

void vtkSounder::SetEchoPalette(const IColorPalette * palette)
{
	m_colorFunction->setPalette(palette, this->GetGlobalOpacity(), this->isUseSort());

	this->m_pVolumeSingleEcho->SetEchoPalette(palette);
	this->m_pVolumePhaseEcho->SetEchoPalette(palette, GetGlobalOpacity(), this->isUseSort());
}


// coupe des �chos du sondeur par le plan de coupe sp�cifi�
void vtkSounder::Apply3DCut(vtkRenderer* ren, vtkPlane* plane)
{
	// plan de clipping
	vtkPlaneCollection * planes = vtkPlaneCollection::New();
	planes->AddItem(plane);
	m_pMapper->SetClippingPlanes(planes);
	planes->Delete();

	// fermeture des �chos "ouverts" par la coupe
	if (m_pPlaneCut == NULL)
	{
		m_pPlaneCut = vtkCutter::New();
		m_pPlaneCut->SetInputConnection(GetLastBuildingVolume()->GetEchoPolyData()->GetOutputPort());
	}

	m_pPlaneCut->SetCutFunction(plane);

	if (m_pCutStrips == NULL)
	{
		m_pCutStrips = vtkStripper::New();
	}

	m_pCutStrips->PassCellDataAsFieldDataOn();
	m_pCutStrips->SetInputConnection(m_pPlaneCut->GetOutputPort());

	auto surfaceFilter = vtkMovSurfaceFilter::New();
	
	surfaceFilter->SetInputConnection(m_pCutStrips->GetOutputPort());

	if (m_pContourActor == NULL)
	{
		m_pContourActor = vtkActor::New();
		m_pContourMapper = vtkPolyDataMapper::New();
		m_pContourMapper->SetInputConnection(surfaceFilter->GetOutputPort());
		m_pContourMapper->SetLookupTable(m_colorFunction);
		m_pContourActor->SetMapper(m_pContourMapper);

		ren->AddActor(m_pContourActor);

		// update de l'�chelle
		double scale[3];
		m_volumeActor->GetScale(scale);
		m_pContourActor->SetScale(scale);
	}

	surfaceFilter->Delete();
}

void vtkSounder::Undo3DCut(vtkRenderer* ren)
{
	m_pMapper->RemoveAllClippingPlanes();
	if (m_pPlaneCut != NULL)
	{
		m_pPlaneCut->Delete();
		m_pPlaneCut = NULL;
	}
	if (m_pCutStrips != NULL)
	{
		m_pCutStrips->Delete();
		m_pCutStrips = NULL;
	}
	if (m_pContourActor != NULL)
	{
		ren->RemoveActor(m_pContourActor);
		m_pContourActor->Delete();
		m_pContourActor = NULL;
	}
	if (m_pContourMapper != NULL)
	{
		m_pContourMapper->Delete();
		m_pContourMapper = NULL;
	}
}
