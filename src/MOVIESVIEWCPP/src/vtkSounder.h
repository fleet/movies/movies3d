#pragma once
#include "vtk.h"
#include "MergedEchoList.h"
#include "CameraSettings.h"
#include "BaseMathLib/Box.h"
#include "vtkVolumeSeaFloor.h"
#include "vtkVolumeBeam.h"
#include "vtkCutter.h"
#include "vtkStripper.h"
#include "vtkMovSurfaceFilter.h"
#include "vtkPlane.h"
#include "vtkVolumeShoal.h"

class IColorPalette;

#pragma managed
class vtkClipPolyData;
class vtkVolumeSeaFloor;
class vtkVolumeSingleEcho;
class vtkVolumePhaseEcho;
class vtkDepthSortPolyData;
class vtkPaletteScalarsToColors;

class vtkSounder
{
public:
	vtkSounder(REFRESH3D_CALLBACK refreshCB);
	virtual ~vtkSounder(void);
	std::uint32_t m_sounderId;

	vtkActor* m_volumeActor;
	vtkPolyDataMapper *m_pMapper;
	vtkDepthSortPolyData *m_pSort;

	bool IsTransparent() { return isUseSort(); };
	bool  isUseSort();
	void  SetSort(bool a, vtkCamera *pcam);
	void SetVolumeScale(double x, double y, double z);

	void AddToRenderer(vtkRenderer* ren);
	void RemoveFromRenderer(vtkRenderer* ren);

	void SetEchoPalette(const IColorPalette * palette);

	void SetBuildingVolume(CameraSettings ^a_cameraSettings);
	MergedEchoList *GetBuildingVolume();
	MergedEchoList *GetLastBuildingVolume() { return m_pLastAppliedVolume; };

	vtkVolumeSeaFloor* getSeaFloor() { return m_pVolumeSeaFloor; }
	vtkVolumeSingleEcho* getVolumeSingleEcho() { return m_pVolumeSingleEcho; }
	vtkVolumePhaseEcho* getVolumePhaseEcho() { return m_pVolumePhaseEcho; }
	vtkVolumeBeam* getVolumeBeam() { return m_pVolumeBeam; }
	vtkVolumeShoal* getVolumeShoal() { return m_pVolumeShoal; }

	double	GetGlobalOpacity() { return m_globalOpacity; }
	void	SetGlobalOpacity(double a);
	
	bool IsWireFrame() { return m_bWireFrame; }
	void SetToWireFrame(bool a);

	// coupe des �chos du sondeur par le plan de coupe sp�cifi�
	void Apply3DCut(vtkRenderer* ren, vtkPlane* plane);
	void Undo3DCut(vtkRenderer* ren);
	
private:
	bool	m_bnRendering;
	double	m_globalOpacity;
	bool	m_bWireFrame;
	bool	m_bDisplay;
	MergedEchoList *m_pBuildingVolume;
	MergedEchoList *m_pLastAppliedVolume;

	vtkVolumeSeaFloor	*m_pVolumeSeaFloor;
	vtkVolumeSingleEcho *m_pVolumeSingleEcho;
	vtkVolumePhaseEcho *m_pVolumePhaseEcho;
	vtkVolumeBeam *m_pVolumeBeam;
	vtkVolumeShoal *m_pVolumeShoal;

	// coupe 2D/3D
	vtkCutter * m_pPlaneCut;
	vtkStripper * m_pCutStrips;
	vtkActor* m_pContourActor; // acteur repr�sentant aux polygones de fermeture des voxels coup�s
	vtkPolyDataMapper* m_pContourMapper;

	/**Palette stuff*/
	vtkPaletteScalarsToColors *m_colorFunction;
};
