#include "vtkSounderMgr.h"
#include "vtkVolumeSingleEcho.h"
#include "vtkVolumePhaseEcho.h"

vtkSounderMgr::vtkSounderMgr(REFRESH3D_CALLBACK refreshCB)
{
	m_RefreshCB = refreshCB;
}

vtkSounderMgr::~vtkSounderMgr(void)
{

}
void vtkSounderMgr::AddSounder(std::uint32_t sounderId, vtkRenderer *renderer)
{
	vtkSounder *pSound = new vtkSounder(m_RefreshCB);
	m_sounderMap.insert(MapvtkSounder::value_type(sounderId, pSound));
	m_sounderId.push_back(sounderId);
}

vtkSounder *vtkSounderMgr::GetSounder(std::uint32_t sounderId)
{
	MapvtkSounder::iterator res = m_sounderMap.find(sounderId);

	if (res != m_sounderMap.end())
	{
		return res->second;
	}
	return NULL;

}
vtkSounder *vtkSounderMgr::GetSounderIdx(unsigned int idx)
{
	if (idx < GetNbSounder())
	{
		return GetSounder(m_sounderId[idx]);
	}
	return NULL;
}

std::uint32_t vtkSounderMgr::GetIdSounder(unsigned int idx)
{
	if (idx < GetNbSounder())
	{
		return m_sounderId[idx];
	}
	return 0;

}

unsigned int vtkSounderMgr::GetNbSounder()
{
	return m_sounderId.size();
}

void vtkSounderMgr::Clear(vtkRenderer *renderer)
{

	MapvtkSounder::iterator res = m_sounderMap.begin();

	while (res != m_sounderMap.end())
	{
		res->second->getSeaFloor()->RemoveFromRenderer(renderer);
		res->second->getVolumeSingleEcho()->RemoveFromRenderer(renderer);
		res->second->getVolumePhaseEcho()->RemoveFromRenderer(renderer);
		res->second->getVolumeShoal()->RemoveFromRenderer(renderer);

		res->second->RemoveFromRenderer(renderer);
		delete (res->second);
		res++;
	}
	m_sounderMap.clear();
	m_sounderId.clear();
}
