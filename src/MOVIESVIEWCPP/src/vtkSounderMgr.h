#pragma once
#include <map>
#include <vector>

#include "vtkSounder.h"
typedef	 std::map<std::uint32_t, vtkSounder*> MapvtkSounder;

class vtkSounderMgr
{
public:
	vtkSounderMgr(REFRESH3D_CALLBACK refreshCB);
	virtual ~vtkSounderMgr(void);
	void AddSounder(std::uint32_t sounderId, vtkRenderer *renderer);
	vtkSounder *GetSounder(std::uint32_t sounderId);
	vtkSounder *GetSounderIdx(unsigned int idx);
	std::uint32_t GetIdSounder(unsigned int idx);
	unsigned int GetNbSounder();

	void Clear(vtkRenderer *renderer);
private:
	MapvtkSounder m_sounderMap;
	std::vector   <std::uint32_t> m_sounderId;
	REFRESH3D_CALLBACK m_RefreshCB;
};
