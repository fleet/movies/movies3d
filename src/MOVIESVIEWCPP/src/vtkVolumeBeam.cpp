#include "vtkVolumeBeam.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkRenderer.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/MovESUMgr.h"

#include "Echo3DList.h"
#include "PingAxisInformation.h"
#include "vtkCellArray.h"

#include "vtkMovLabeledDataMapper.h"
#include "vtkActor2D.h"

#include <sstream>
#include <algorithm>

vtkVolumeBeam::vtkVolumeBeam(void)
{
	m_cylinderMapper = vtkPolyDataMapper::New();
	m_cylinderActor = vtkActor::New();

	m_cylinderActor->SetVisibility(false);
	m_cylinderActor->GetProperty()->SetColor(1.0f, 0.0f, 0.0f);
	m_cylinderActor->GetProperty()->SetAmbient(1.0f);
	m_cylinderActor->GetProperty()->SetDiffuse(0.0f);
	m_cylinderActor->SetMapper(m_cylinderMapper);

	m_bIsVisible = true;
	m_pPolyData = NULL;

	// Labels
	m_pLabelPolyData = NULL;
	m_pLabelActor = vtkActor2D::New();
	m_pLabelActor->SetVisibility(false);
	m_pLabelMapper = vtkMovLabeledDataMapper::New();
	m_pLabelActor->SetMapper(m_pLabelMapper);

	// Graduations
	m_pGraduationPolyData = NULL;
	m_pGraduationActor = vtkActor::New();
	m_pGraduationActor->SetVisibility(false);
	m_pGraduationActor->GetProperty()->SetColor(0.0f, 0.0f, 0.0f);
	m_pGraduationActor->GetProperty()->SetAmbient(1.0f);
	m_pGraduationActor->GetProperty()->SetDiffuse(0.0f);
	m_pGraduationMapper = vtkPolyDataMapper::New();
	m_pGraduationActor->SetMapper(m_pGraduationMapper);

	// possibilit� d'afficher ou non les graduations et labels
	m_bDisplayVolume = true;
	m_bDisplayGraduations = true;
	m_bDisplayLabels = true;
}

vtkVolumeBeam::~vtkVolumeBeam(void)
{
	m_cylinderMapper->Delete();
	m_cylinderActor->Delete();
	if (m_pPolyData)
		m_pPolyData->Delete();

	m_pLabelMapper->Delete();
	m_pLabelActor->Delete();
	if (m_pLabelPolyData)
		m_pLabelPolyData->Delete();

	m_pGraduationMapper->Delete();
	m_pGraduationActor->Delete();
	if (m_pGraduationPolyData)
		m_pGraduationPolyData->Delete();
}


void vtkVolumeBeam::RemoveFromRenderer(vtkRenderer* ren)
{
	ren->RemoveActor(m_cylinderActor);
	ren->RemoveActor(m_pLabelActor);
	ren->RemoveActor(m_pGraduationActor);
}

void vtkVolumeBeam::AddToRenderer(vtkRenderer* ren)
{
	ren->AddActor(m_cylinderActor);
	ren->AddActor(m_pLabelActor);
	ren->AddActor(m_pGraduationActor);
}

bool vtkVolumeBeam::isVisible()
{
	return m_bIsVisible;
}
void vtkVolumeBeam::setVisible(bool a)
{
	m_bIsVisible = a;
	if (m_pPolyData)
	{
		m_cylinderActor->SetVisibility(a && m_bDisplayVolume);
	}
	if (m_pLabelPolyData)
	{
		m_pLabelActor->SetVisibility(a && m_bDisplayLabels && m_bDisplayVolume);
	}
	if (m_pGraduationPolyData)
	{
		m_pGraduationActor->SetVisibility(a && m_bDisplayGraduations && m_bDisplayVolume);
	}
}
void vtkVolumeBeam::setVolumeVisible(bool a)
{
	m_bDisplayVolume = a;
	UpdateVisibility();
}
void vtkVolumeBeam::setLabelsVisible(bool a)
{
	m_bDisplayLabels = a;
	UpdateVisibility();
}
void vtkVolumeBeam::setGraduationsVisible(bool a)
{
	m_bDisplayGraduations = a;
	UpdateVisibility();
}
void vtkVolumeBeam::UpdateVisibility()
{
	// visibilit� du volume insonnifi�
	if (m_pPolyData)
	{
		m_cylinderActor->SetVisibility(m_bDisplayVolume);
	}
	// visibilit� des labels
	if (m_pLabelPolyData && m_bDisplayLabels && m_bDisplayVolume)
	{
		m_pLabelActor->SetVisibility(true);
	}
	else if (m_pLabelPolyData && !m_bDisplayLabels || !m_bDisplayVolume)
	{
		m_pLabelActor->SetVisibility(false);
	}
	// visibilit� des graduations
	if (m_pGraduationPolyData && m_bDisplayGraduations && m_bDisplayVolume)
	{
		m_pGraduationActor->SetVisibility(true);
	}
	else if (m_pGraduationPolyData && !m_bDisplayGraduations || !m_bDisplayVolume)
	{
		m_pGraduationActor->SetVisibility(false);
	}
}
void vtkVolumeBeam::SetVolumeScale(double x, double y, double z)
{
	m_cylinderActor->SetScale(x, y, z);
	m_pGraduationActor->SetScale(x, y, z);
	m_pLabelMapper->m_ScaleY = y;
}

void vtkVolumeBeam::Compute(std::vector<PingAxisInformation> &refLines)
{
	if (!isVisible())
		return;

	// beam volume
	if (!m_pPolyData)
	{
		m_pPolyData = vtkPolyData::New();
		m_cylinderMapper->SetInputData(m_pPolyData);
		setVisible(true);
	}

	// labels
	if (!m_pLabelPolyData)
	{
		m_pLabelPolyData = vtkPolyData::New();
		m_pLabelMapper->SetInputDataObject(m_pLabelPolyData);
		setVisible(true);
	}

	// graduations
	if (!m_pGraduationPolyData)
	{
		m_pGraduationPolyData = vtkPolyData::New();
		m_pGraduationMapper->SetInputData(m_pGraduationPolyData);
		setVisible(true);
	}

	// r�cup�ration de la liste des IDs des pings en frontiere d'ESU
	M3DKernel * pKern = M3DKernel::GetInstance();
	pKern->Lock();
	std::vector<std::uint64_t> pingIds;
	size_t ESUNumber = pKern->getMovESUManager()->GetESUContainer().GetESUNb();
	// pour chaque ESU, on dessine les limites
	for (size_t i = 0; i < ESUNumber; i++)
	{
		ESUParameter * esu = pKern->getMovESUManager()->GetESUContainer().GetESUWithIdx(i);
		if (esu->GetUsed())
		{
			pingIds.push_back(esu->GetESUPingNumber());
			pingIds.push_back(esu->GetESULastPingNumber());
		}
	}

	// on fait de m�me pour l'ESU en cours le cas �ch�ant
	ESUParameter * currentESU = pKern->getMovESUManager()->GetWorkingESU();
	if (currentESU && currentESU->GetUsed())
	{
		pingIds.push_back(currentESU->GetESUPingNumber());
	}
	pKern->Unlock();
	// suppression des doublons
	pingIds.erase(std::unique(pingIds.begin(), pingIds.end()), pingIds.end());
	// suppression des ESU qui n'apparaissent pas dans la zone de visu 3D
	size_t lineCoupleNb = refLines.size();
	while (pingIds.size() && refLines[0].m_PingID < pingIds[pingIds.size() - 1])
	{
		pingIds.erase(pingIds.end() - 1);
	}
	while (pingIds.size() && refLines[lineCoupleNb - 1].m_PingID > pingIds[0])
	{
		pingIds.erase(pingIds.begin());
	}



	vtkPoints* pointsLabel = vtkPoints::New();
	pointsLabel->Allocate(3); // 3 labels : distance et 2 profondeurs
	m_pLabelMapper->ClearLabels(); // on efface les labels pr�c�dents

	// on r�cup�re le fond moyen pour dimensionner les graduations
	double bottom = 0;
	int nbBottoms = 0;
	for (size_t i = 0; i < lineCoupleNb; i++)
	{
		if (refLines[i].m_MaxDepthFound)
		{
			bottom += refLines[i].m_MaxDepth;
			nbBottoms++;
		}
	}
	if (nbBottoms > 0)
	{
		bottom /= nbBottoms;
	}
	else
	{
		bottom = 100;
	}

	int nbPoints = 8 + lineCoupleNb; // 8 points pour les extr�mites, un point par ping pour la courbe
	vtkPoints* points = vtkPoints::New();
	points->Allocate(nbPoints);

	int pointOffset = 0;

	// vecteur repr�sentant le cap
	BaseMathLib::Vector3D cap;
	cap.y = refLines[0].m_FrontLines.m_LowerAngleBeam.m_PointDown.y - refLines[0].m_BackLines.m_LowerAngleBeam.m_PointDown.y;
	cap.z = refLines[0].m_FrontLines.m_LowerAngleBeam.m_PointDown.z - refLines[0].m_BackLines.m_LowerAngleBeam.m_PointDown.z;
	cap.x = refLines[0].m_FrontLines.m_LowerAngleBeam.m_PointDown.x - refLines[0].m_BackLines.m_LowerAngleBeam.m_PointDown.x;
	cap.normalise();


	// Ping le plus r�cent
	BaseMathLib::Vector3D PointA = refLines[0].m_FrontLines.m_LowerAngleBeam.m_PointDown;
	BaseMathLib::Vector3D PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
	points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
	pointOffset++;

	// label de pronfondeur lowerAngle
	PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA + 5 * cap);
	pointsLabel->InsertPoint(0, PointVtk.x, PointVtk.y, PointVtk.z);
	std::ostringstream ss;
	ss << std::setiosflags(ios::fixed) << std::setprecision(2) << PointA.z << "m";
	m_pLabelMapper->AddLabel(ss.str());


	PointA = refLines[0].m_FrontLines.m_LowerAngleBeam.m_PointUp;
	PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
	points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
	pointOffset++;

	// label de distance
	pointsLabel->InsertPoint(1, PointVtk.x, PointVtk.y + 1.5, PointVtk.z);
	ss.str("");
	ss << std::setiosflags(ios::fixed) << std::setprecision(3) << refLines[0].m_CumulatedLength / 1000. << " nm";
	m_pLabelMapper->AddLabel(ss.str());


	PointA = refLines[0].m_FrontLines.m_UpperAngleBeam.m_PointUp;
	PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
	points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
	pointOffset++;

	PointA = refLines[0].m_FrontLines.m_UpperAngleBeam.m_PointDown;
	PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
	points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
	pointOffset++;

	// label de pronfondeur upperAngle
	PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA + 5 * cap);
	pointsLabel->InsertPoint(2, PointVtk.x, PointVtk.y, PointVtk.z);
	ss.str("");
	ss << std::setiosflags(ios::fixed) << std::setprecision(2) << PointA.z << "m";
	m_pLabelMapper->AddLabel(ss.str());

	m_pLabelPolyData->SetPoints(pointsLabel);
	pointsLabel->Delete();

	// Ping le plus ancien
	PointA = refLines[lineCoupleNb - 1].m_BackLines.m_LowerAngleBeam.m_PointDown;
	PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
	points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
	pointOffset++;

	PointA = refLines[lineCoupleNb - 1].m_BackLines.m_LowerAngleBeam.m_PointUp;
	PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
	points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
	pointOffset++;

	PointA = refLines[lineCoupleNb - 1].m_BackLines.m_UpperAngleBeam.m_PointUp;
	PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
	points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
	pointOffset++;

	PointA = refLines[lineCoupleNb - 1].m_BackLines.m_UpperAngleBeam.m_PointDown;
	PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
	points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
	pointOffset++;


	// rappel du volume au niveau des ESU
	vtkPoints* gradPoints = vtkPoints::New();
	size_t nbEsu = 0;
	int graduationNb = 0;
	for (size_t i = 0; i < lineCoupleNb; i++)
	{
		std::uint64_t pingId = refLines[i].m_PingID;
		if (pingIds.size() && pingId <= pingIds[pingIds.size() - 1])
		{
			PointA = (refLines[i].m_FrontLines.m_LowerAngleBeam.m_PointDown + refLines[i].m_BackLines.m_LowerAngleBeam.m_PointDown) / 2.;
			PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
			points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
			pointOffset++;

			PointA = (refLines[i].m_FrontLines.m_LowerAngleBeam.m_PointUp + refLines[i].m_BackLines.m_LowerAngleBeam.m_PointUp) / 2.;
			PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
			points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
			pointOffset++;

			PointA = (refLines[i].m_FrontLines.m_UpperAngleBeam.m_PointUp + refLines[i].m_BackLines.m_UpperAngleBeam.m_PointUp) / 2.;
			PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
			points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
			pointOffset++;

			PointA = (refLines[i].m_FrontLines.m_UpperAngleBeam.m_PointDown + refLines[i].m_BackLines.m_UpperAngleBeam.m_PointDown) / 2.;
			PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
			points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
			pointOffset++;

			nbEsu++;

			graduationNb += ApplyDepthGraduation(gradPoints, refLines[i], eESU);

			pingIds.erase(pingIds.end() - 1);
		}
	}

	// ligne sup�rieure
	std::vector<size_t> milesStonesIndexes;
	int nbMilesStones = 0;
	for (size_t i = 0; i < lineCoupleNb; i++)
	{
		PointA.x = (refLines[i].m_FrontLines.m_LowerAngleBeam.m_PointUp.x + refLines[i].m_FrontLines.m_UpperAngleBeam.m_PointUp.x) / 2.0;
		PointA.y = (refLines[i].m_FrontLines.m_LowerAngleBeam.m_PointUp.y + refLines[i].m_FrontLines.m_UpperAngleBeam.m_PointUp.y) / 2.0;
		PointA.z = (refLines[i].m_FrontLines.m_LowerAngleBeam.m_PointUp.z + refLines[i].m_FrontLines.m_UpperAngleBeam.m_PointUp.z) / 2.0;
		PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
		points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
		pointOffset++;

		// OTK - ajout lignes de graduation en distance
		if (refLines[i].m_IsMilesStone)
		{
			milesStonesIndexes.push_back(i);
			nbMilesStones++;
		}

		// pour le dernier ping, on ajoute le point backline (pour les zooms, c'est assez different du frontline)
		if (i == lineCoupleNb - 1)
		{
			PointA.x = (refLines[i].m_BackLines.m_LowerAngleBeam.m_PointUp.x + refLines[i].m_BackLines.m_UpperAngleBeam.m_PointUp.x) / 2.0;
			PointA.y = (refLines[i].m_BackLines.m_LowerAngleBeam.m_PointUp.y + refLines[i].m_BackLines.m_UpperAngleBeam.m_PointUp.y) / 2.0;
			PointA.z = (refLines[i].m_BackLines.m_LowerAngleBeam.m_PointUp.z + refLines[i].m_BackLines.m_UpperAngleBeam.m_PointUp.z) / 2.0;
			PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);
			points->InsertPoint(pointOffset, PointVtk.x, PointVtk.y, PointVtk.z);
			pointOffset++;
		}
	}

	m_pPolyData->SetPoints(points);
	points->Delete();

	for (int i = 0; i < nbMilesStones; i++)
	{
		size_t index = milesStonesIndexes[i];
		PointA.x = (refLines[index].m_FrontLines.m_LowerAngleBeam.m_PointUp.x + refLines[index].m_FrontLines.m_UpperAngleBeam.m_PointUp.x) / 2.0;
		PointA.y = (refLines[index].m_FrontLines.m_LowerAngleBeam.m_PointUp.y + refLines[index].m_FrontLines.m_UpperAngleBeam.m_PointUp.y) / 2.0;
		PointA.z = (refLines[index].m_FrontLines.m_LowerAngleBeam.m_PointUp.z + refLines[index].m_FrontLines.m_UpperAngleBeam.m_PointUp.z) / 2.0;
		PointVtk = BaseMathLib::Vector3D::ToViewOpengl(PointA);

		gradPoints->InsertNextPoint(PointVtk.x, PointVtk.y + bottom / 20., PointVtk.z);
		gradPoints->InsertNextPoint(PointVtk.x, PointVtk.y, PointVtk.z);
	}

	// graduations en profondeur
	// graduations sur le devant du volume
	graduationNb += ApplyDepthGraduation(gradPoints, refLines[0], eBeamFront);
	// graduations sur l'arri�re du volume
	graduationNb += ApplyDepthGraduation(gradPoints, refLines[lineCoupleNb - 1], eBeamBack);
	m_pGraduationPolyData->SetPoints(gradPoints);
	gradPoints->Delete();

	// Choix des lignes � tracer entre les points d�finis pr�c�demment
	int nbLines = 6 + 3 * nbEsu + (lineCoupleNb);
	vtkCellArray* lines = vtkCellArray::New();
	lines->Allocate(lines->EstimateSize(nbLines, 2));
	vtkIdType t[2] = { 0,1 };
	lines->InsertNextCell(2, t);
	vtkIdType t1[2] = { 1,2 };
	lines->InsertNextCell(2, t1);
	vtkIdType t2[2] = { 2,3 };
	lines->InsertNextCell(2, t2);
	vtkIdType t3[2] = { 4,5 };
	lines->InsertNextCell(2, t3);
	vtkIdType t4[2] = { 5,6 };
	lines->InsertNextCell(2, t4);
	vtkIdType t5[2] = { 6,7 };
	lines->InsertNextCell(2, t5);

	pointOffset = 8; // les 8 premiers points sont pour les contours ext�rieurs

	// ESUs
	for (size_t i = 0; i < nbEsu; i++)
	{
		vtkIdType t7[2] = { pointOffset,pointOffset + 1 };
		pointOffset++;
		lines->InsertNextCell(2, t7);
		vtkIdType t8[2] = { pointOffset,pointOffset + 1 };
		pointOffset++;
		lines->InsertNextCell(2, t8);
		vtkIdType t9[2] = { pointOffset,pointOffset + 1 };
		pointOffset++;
		lines->InsertNextCell(2, t9);

		pointOffset++;
	}

	// ligne sup�rieure
	for (size_t i = 0; i < lineCoupleNb; i++)
	{
		vtkIdType t6[2];
		t6[0] = pointOffset;
		pointOffset++;
		t6[1] = pointOffset;
		lines->InsertNextCell(2, t6);
	}

	lines->Squeeze();
	m_pPolyData->SetLines(lines);
	lines->Delete();


	// graduations
	nbLines = nbMilesStones + graduationNb;
	lines = vtkCellArray::New();
	lines->Allocate(lines->EstimateSize(nbLines, 2));
	for (int i = 0; i < nbLines; i++)
	{
		vtkIdType t6[2];
		t6[0] = i * 2;
		t6[1] = i * 2 + 1;
		lines->InsertNextCell(2, t6);
	}

	lines->Squeeze();
	m_pGraduationPolyData->SetLines(lines);
	lines->Delete();

}


int vtkVolumeBeam::ApplyDepthGraduation(vtkPoints * vtkPoints, PingAxisInformation & pingInfo, VolumeType type)
{
	BaseMathLib::Vector3D topToBottomLower, topToBottomUpper, PointVtk;
	switch (type)
	{
	case eBeamFront:
	{
		topToBottomLower.y = pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointDown.y - pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp.y;
		topToBottomLower.z = pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointDown.z - pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp.z;
		topToBottomLower.x = pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointDown.x - pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp.x;
		topToBottomUpper.y = pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointDown.y - pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp.y;
		topToBottomUpper.z = pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointDown.z - pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp.z;
		topToBottomUpper.x = pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointDown.x - pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp.x;
	}
	break;
	case eBeamBack:
	{
		topToBottomLower.y = pingInfo.m_BackLines.m_LowerAngleBeam.m_PointDown.y - pingInfo.m_BackLines.m_LowerAngleBeam.m_PointUp.y;
		topToBottomLower.z = pingInfo.m_BackLines.m_LowerAngleBeam.m_PointDown.z - pingInfo.m_BackLines.m_LowerAngleBeam.m_PointUp.z;
		topToBottomLower.x = pingInfo.m_BackLines.m_LowerAngleBeam.m_PointDown.x - pingInfo.m_BackLines.m_LowerAngleBeam.m_PointUp.x;
		topToBottomUpper.y = pingInfo.m_BackLines.m_UpperAngleBeam.m_PointDown.y - pingInfo.m_BackLines.m_UpperAngleBeam.m_PointUp.y;
		topToBottomUpper.z = pingInfo.m_BackLines.m_UpperAngleBeam.m_PointDown.z - pingInfo.m_BackLines.m_UpperAngleBeam.m_PointUp.z;
		topToBottomUpper.x = pingInfo.m_BackLines.m_UpperAngleBeam.m_PointDown.x - pingInfo.m_BackLines.m_UpperAngleBeam.m_PointUp.x;
	}
	break;
	default:
	{
		topToBottomLower.y = (pingInfo.m_BackLines.m_LowerAngleBeam.m_PointDown.y + pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointDown.y) / 2.
			- (pingInfo.m_BackLines.m_LowerAngleBeam.m_PointUp.y + pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp.y) / 2.0;
		topToBottomLower.z = (pingInfo.m_BackLines.m_LowerAngleBeam.m_PointDown.z + pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointDown.z) / 2.
			- (pingInfo.m_BackLines.m_LowerAngleBeam.m_PointUp.z + pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp.z) / 2.0;
		topToBottomLower.x = (pingInfo.m_BackLines.m_LowerAngleBeam.m_PointDown.x + pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointDown.x) / 2.
			- (pingInfo.m_BackLines.m_LowerAngleBeam.m_PointUp.x + pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp.x) / 2.0;
		topToBottomUpper.y = (pingInfo.m_BackLines.m_UpperAngleBeam.m_PointDown.y + pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointDown.y) / 2.
			- (pingInfo.m_BackLines.m_UpperAngleBeam.m_PointUp.y + pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp.y) / 2.0;
		topToBottomUpper.z = (pingInfo.m_BackLines.m_UpperAngleBeam.m_PointDown.z + pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointDown.z) / 2.
			- (pingInfo.m_BackLines.m_UpperAngleBeam.m_PointUp.z + pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp.z) / 2.0;
		topToBottomUpper.x = (pingInfo.m_BackLines.m_UpperAngleBeam.m_PointDown.x + pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointDown.x) / 2.
			- (pingInfo.m_BackLines.m_UpperAngleBeam.m_PointUp.x + pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp.x) / 2.0;
	}
	break;
	}

	double depthInterLines = DisplayParameter::getInstance()->GetDepthSampling();
	int graduationNb = 1;
	double depth = graduationNb*depthInterLines;
	if (topToBottomLower.z > 0 && topToBottomUpper.z > 0 && DisplayParameter::getInstance()->GetDepthGrid())
	{
		while (depth < (pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp.z + topToBottomLower.z)
			&& depth < (pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp.z + topToBottomUpper.z))
		{
			// OTK - FAE005 - tronquer les graduations � la zone zoom�e
			if (depth > pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp.z)
			{
				BaseMathLib::Vector3D depthGradLower, depthGradUpper;

				switch (type)
				{
				case eBeamFront:
				{
					depthGradLower = pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp
						+ ((depth - pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp.z) / topToBottomLower.z)*topToBottomLower;
					depthGradUpper = pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp
						+ ((depth - pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp.z) / topToBottomUpper.z)*topToBottomUpper;
				}
				break;
				case eBeamBack:
				{
					depthGradLower = pingInfo.m_BackLines.m_LowerAngleBeam.m_PointUp
						+ ((depth - pingInfo.m_BackLines.m_LowerAngleBeam.m_PointUp.z) / topToBottomLower.z)*topToBottomLower;
					depthGradUpper = pingInfo.m_BackLines.m_UpperAngleBeam.m_PointUp
						+ ((depth - pingInfo.m_BackLines.m_UpperAngleBeam.m_PointUp.z) / topToBottomUpper.z)*topToBottomUpper;
				}
				break;
				default:
				{
					depthGradLower = (pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp + pingInfo.m_BackLines.m_LowerAngleBeam.m_PointUp) / 2.0
						+ ((depth - (pingInfo.m_FrontLines.m_LowerAngleBeam.m_PointUp.z + pingInfo.m_BackLines.m_LowerAngleBeam.m_PointUp.z) / 2.0) / topToBottomLower.z)*topToBottomLower;
					depthGradUpper = (pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp + pingInfo.m_BackLines.m_UpperAngleBeam.m_PointUp) / 2.0
						+ ((depth - (pingInfo.m_FrontLines.m_UpperAngleBeam.m_PointUp.z + pingInfo.m_BackLines.m_UpperAngleBeam.m_PointUp.z) / 2.0) / topToBottomUpper.z)*topToBottomUpper;
				}
				break;
				}

				PointVtk = BaseMathLib::Vector3D::ToViewOpengl(depthGradLower);
				vtkPoints->InsertNextPoint(PointVtk.x, PointVtk.y, PointVtk.z);
				PointVtk = BaseMathLib::Vector3D::ToViewOpengl(depthGradUpper);
				vtkPoints->InsertNextPoint(PointVtk.x, PointVtk.y, PointVtk.z);
				graduationNb++;
			}

			depth += depthInterLines;

		}
	}

	return graduationNb - 1;
}