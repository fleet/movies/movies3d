#pragma once
class vtkRenderer;
class vtkPolyData;
class vtkPolyDataMapper;
class vtkActor;
class vtkMovLabeledDataMapper;
class vtkActor2D;
class vtkPoints;
class PingAxisInformation;

#include <vector>

enum VolumeType
{
	eBeamFront,
	eBeamBack,
	eESU,
};

class vtkVolumeBeam
{
public:
	vtkVolumeBeam(void);
public:
	virtual ~vtkVolumeBeam(void);


	void Compute(std::vector<PingAxisInformation> &refLines);
	void RemoveFromRenderer(vtkRenderer* ren);
	void AddToRenderer(vtkRenderer* ren);
	void SetVolumeScale(double x, double y, double z);

	bool isVisible();
	void setVisible(bool a);
	void setVolumeVisible(bool a);
	void setLabelsVisible(bool a);
	void setGraduationsVisible(bool a);
private:
	void UpdateVisibility();
	int  ApplyDepthGraduation(vtkPoints * vtkPoints, PingAxisInformation & pingInfo, VolumeType type);
public:
	bool m_bDisplayVolume;
	bool m_bDisplayGraduations;
	bool m_bDisplayLabels;
private:
	vtkPolyData* m_pPolyData;
	vtkPolyDataMapper* m_cylinderMapper;
	vtkActor* m_cylinderActor;

	// labels
	vtkPolyData* m_pLabelPolyData;
	vtkMovLabeledDataMapper* m_pLabelMapper;
	vtkActor2D* m_pLabelActor;

	//graduations
	vtkPolyData* m_pGraduationPolyData;
	vtkPolyDataMapper* m_pGraduationMapper;
	vtkActor* m_pGraduationActor;

	bool m_bIsVisible;


};
