#include "vtkVolumePhaseEcho.h"

#include "vtkPolyDataMapper.h"
#include "vtkActor.h"

#include "vtkProperty.h"
#include "vtkRenderer.h"
#include "vtkGlyph3D.h"
#include "vtkPaletteScalarsToColors.h"
#include "vtkAppendPolyData.h"
#include "vtkCollection.h"

#include "M3DKernel/utils/log/ILogger.h"

namespace
{
	constexpr const char * LoggerName = "MoviesView.View.VolumicView.3dViewer.vtkVolumePhaseEcho";
}

vtkVolumePhaseEcho::vtkVolumePhaseEcho()
{
	m_pMapper = vtkPolyDataMapper::New();
	m_pActor = vtkActor::New();
	m_bIsVisible = true;
	m_glyphInit = false;
	m_colorFunction = vtkPaletteScalarsToColors::New();
	m_pMapper->SetLookupTable(m_colorFunction);
	m_pActor->SetMapper(m_pMapper);
	m_pActor->SetVisibility(false);
	m_Size = 0.05;
	m_fScale = 10.0;
	m_bDisplay = true;
	m_pRefGlyphs = NULL;
	m_pAppendPolyData = NULL;
}

vtkVolumePhaseEcho::~vtkVolumePhaseEcho()
{
	m_pMapper->Delete();
	m_pActor->Delete();
	m_colorFunction->Delete();
	// OTK - FAE003 - fuite m�moire
	if (m_pRefGlyphs)
		m_pRefGlyphs->Delete();
	if (m_pAppendPolyData)
		m_pAppendPolyData->Delete();
}

void vtkVolumePhaseEcho::SetVolumeScale(double x, double y, double z)
{
	m_pActor->SetScale(x, y, z);
}

void vtkVolumePhaseEcho::SetSize(double size)
{
	if (size == 0)
	{
		M3D_LOG_WARN(LoggerName, "Null Size given,  skipping");
		return;
	}

	m_Size = size;
	if (m_glyphInit && m_pRefGlyphs)
	{
		m_pRefGlyphs->InitTraversal();
		vtkGlyph3D * pGlyph = (vtkGlyph3D *)m_pRefGlyphs->GetNextItemAsObject();
		while (pGlyph)
		{
			// ACN 05/06/17 : On divise par Pi pour avoir, pour une m�me valeur d'Echo Display Size, un affichage similaire
			// � la solution pr�c�dente (utilisant des vtkPointSource au lieu de vtkSphereSource)
			double approximately_pi = 3.14;
			pGlyph->SetScaleFactor(size / approximately_pi);
			pGlyph = (vtkGlyph3D *)m_pRefGlyphs->GetNextItemAsObject();
		}
	}
}

void vtkVolumePhaseEcho::SetScaleFactor(vtkCollection * p, double size)
{
	int nbItems = p->GetNumberOfItems();
	for (int i = 0; i < nbItems; i++) {
		vtkGlyph3D * pGlyph = (vtkGlyph3D *)p->GetItemAsObject(i);
		pGlyph->SetScaleFactor(size);
	}
}

bool vtkVolumePhaseEcho::isVisible()
{
	return m_bIsVisible;
}

void vtkVolumePhaseEcho::setVisible(bool a)
{
	m_bIsVisible = a;
	if (m_glyphInit)
		m_pActor->SetVisibility(a);
}

void vtkVolumePhaseEcho::RemoveFromRenderer(vtkRenderer* ren)
{
	ren->RemoveActor(m_pActor);
}

void vtkVolumePhaseEcho::AddToRenderer(vtkRenderer* ren)
{
	ren->AddActor(m_pActor);
}

void vtkVolumePhaseEcho::applyPolyData(vtkCollection * p)
{
	if (p)
	{
		if (m_pAppendPolyData)
			m_pAppendPolyData->Delete();

		m_pAppendPolyData = vtkAppendPolyData::New();

		// concatenation des obets dans un seul et m�me objet
		p->InitTraversal();
		vtkGlyph3D * pGlyph = (vtkGlyph3D *)p->GetNextItemAsObject();
		while (pGlyph)
		{
			m_pAppendPolyData->AddInputConnection(pGlyph->GetOutputPort());
			// ACN 05/06/17 : On divise par Pi pour avoir, pour une m�me valeur d'Echo Display Size, un affichage similaire
			// � la solution pr�c�dente (utilisant des vtkPointSource au lieu de vtkSphereSource)
			double approximately_pi = 3.14;
			pGlyph->SetScaleFactor(m_Size / approximately_pi);
			pGlyph = (vtkGlyph3D *)p->GetNextItemAsObject();
		}

		m_pMapper->SetInputConnection(m_pAppendPolyData->GetOutputPort());
		m_pActor->SetVisibility(m_bIsVisible);

		if (m_pRefGlyphs)
			m_pRefGlyphs->Delete();
		m_pRefGlyphs = p;
		m_glyphInit = true;
	}
	else
	{
		m_glyphInit = false;
		m_pActor->SetVisibility(0);
		if (m_pRefGlyphs)
			m_pRefGlyphs->Delete();
		m_pRefGlyphs = NULL;
		if (m_pAppendPolyData)
			m_pAppendPolyData->Delete();
		m_pAppendPolyData = NULL;
	}
}

void vtkVolumePhaseEcho::SetEchoPalette(const IColorPalette * palette, double opacity, bool bUseSort)
{
	m_colorFunction->setPalette(palette, opacity, bUseSort);
}

void vtkVolumePhaseEcho::RenderWireframe(bool bWireframe)
{
	if (bWireframe)
		m_pActor->GetProperty()->SetRepresentationToWireframe();
	else
		m_pActor->GetProperty()->SetRepresentationToSurface();
}