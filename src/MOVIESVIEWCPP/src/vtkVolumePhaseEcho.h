#pragma once

class vtkPolyData;
class vtkPolyDataMapper;
class vtkActor;
class vtkRenderer;
class vtkGlyph3D;
class vtkPaletteScalarsToColors;
class vtkAppendPolyData;
class vtkCollection;

class IColorPalette;

// NMD - FAE 109 - Representation des phases en 3D
class vtkVolumePhaseEcho
{
public:
	vtkVolumePhaseEcho();
	virtual ~vtkVolumePhaseEcho();

	void applyPolyData(vtkCollection* p);

	bool IsTransparent() { return false; };

	void RemoveFromRenderer(vtkRenderer* ren);
	void AddToRenderer(vtkRenderer* ren);
	void SetVolumeScale(double x, double y, double z);
	void SetSize(double size);
	double GetSize() { return m_Size; };
	bool isVisible();
	void setVisible(bool a);
	double m_fScale;

	void SetEchoPalette(const IColorPalette * palette, double opacity, bool bUseSort);

	// NMD - 08/09/2011 - FAE 092 - rendu fil de fer
	void RenderWireframe(bool bWireframe);

private:

	void SetScaleFactor(vtkCollection * p, double factor);

	double m_Size;
	bool	m_bDisplay;
	vtkPaletteScalarsToColors  *m_colorFunction;
	vtkPolyDataMapper	*m_pMapper;
	vtkActor			*m_pActor;
	bool				m_glyphInit;
	bool m_bIsVisible;

	vtkCollection *m_pRefGlyphs;
	vtkAppendPolyData * m_pAppendPolyData;
};

