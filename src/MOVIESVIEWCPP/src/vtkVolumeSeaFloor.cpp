#include "vtkVolumeSeaFloor.h"
#include "vtkJPEGReader.h"

#include "vtkTexture.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkTextureMapToPlane.h"

#include "vtkProperty.h"
#include "vtkRenderer.h"


using namespace System;
using namespace System::Windows::Forms;

vtkVolumeSeaFloor::vtkVolumeSeaFloor()
{
	m_pGroundMapper = vtkPolyDataMapper::New();
	m_floorActor = vtkActor::New();
	m_floorActor->SetVisibility(false);
	m_bIsVisible = true;
	m_pPolyData = NULL;
	m_pTexture = vtkTexture::New();
	loadTexture();
	m_floorScale = 10.0;
}

vtkVolumeSeaFloor::~vtkVolumeSeaFloor(void)
{
	m_pGroundMapper->Delete();
	m_floorActor->Delete();

	if (m_pPolyData)
		m_pPolyData->Delete();

	if (m_pTexture)
		m_pTexture->Delete();
}

void vtkVolumeSeaFloor::RemoveFromRenderer(vtkRenderer* ren)
{

	ren->RemoveActor(m_floorActor);

}
void vtkVolumeSeaFloor::AddToRenderer(vtkRenderer* ren)
{
	ren->AddActor(m_floorActor);
}
void vtkVolumeSeaFloor::loadTexture()
{
	vtkJPEGReader *jpgReader = vtkJPEGReader::New();

	System::IntPtr ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(Application::StartupPath);
	const char* strPath = static_cast<const char*>(ip.ToPointer());
	std::string name = strPath;
	System::Runtime::InteropServices::Marshal::FreeHGlobal(ip);
	name += "\\Ground.JPG";

	jpgReader->SetFileName(name.c_str());



	m_pTexture->SetInputConnection(jpgReader->GetOutputPort());
	m_pTexture->InterpolateOn();
	m_pTexture->RepeatOn();
	jpgReader->Delete();


}
bool vtkVolumeSeaFloor::isVisible()
{
	return m_bIsVisible;
}
void vtkVolumeSeaFloor::setVisible(bool a)
{
	m_bIsVisible = a;
	if (m_pPolyData)
		m_floorActor->SetVisibility(a);
}
/*
vtkPolyData*  vtkVolumeSeaFloor::getFloorPolyData()
{
	if(m_pPolyData)
		m_pPolyData->Delete();
	m_pPolyData=	vtkPolyData::New();
	return m_pPolyData;
}
*/
void vtkVolumeSeaFloor::SetVolumeScale(double x, double y, double z)
{
	m_floorActor->SetScale(x, y, z);
}
void vtkVolumeSeaFloor::applyFloorPolyData(vtkPolyData*p)
{
	// OTK - FAE003 - fuite m�moire
	if (!p)
		return;

	if (m_pPolyData)
	{
		m_pPolyData->Delete();
	}
	m_pPolyData = p;
	vtkTextureMapToPlane *pTextureMap = vtkTextureMapToPlane::New();

	pTextureMap->SetOrigin(0, 1, 0);
	pTextureMap->SetPoint1(m_floorScale, 1, 0);
	pTextureMap->SetPoint2(0, 1, m_floorScale);


	pTextureMap->SetInputData(m_pPolyData);

	m_pGroundMapper->SetInputConnection(pTextureMap->GetOutputPort());


	pTextureMap->Delete();

	m_floorActor->SetMapper(m_pGroundMapper);
	m_floorActor->SetTexture(m_pTexture);
	m_floorActor->GetProperty()->SetOpacity(1);
	m_floorActor->GetProperty()->SetColor(219.0f / 255.0f, 219.0f / 255.0f, 219.0f / 255.0f);
	m_floorActor->GetProperty()->SetAmbient(1.0f);
	m_floorActor->GetProperty()->SetDiffuse(0.0f);
	if (m_bIsVisible)
	{
		m_floorActor->SetVisibility(1);
	}
	else
	{
		m_floorActor->SetVisibility(0);

	}
}
