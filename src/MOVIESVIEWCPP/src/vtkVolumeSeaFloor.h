#pragma once
#include "BaseMathLib/Box.h"
class vtkTexture;
class vtkPolyData;
class vtkPolyDataMapper;
class vtkActor;
class vtkRenderer;
class vtkVolumeSeaFloor
{
public:
	vtkVolumeSeaFloor();
	virtual ~vtkVolumeSeaFloor(void);


	//vtkPolyData*  getFloorPolyData();
	void applyFloorPolyData(vtkPolyData*p);

	bool IsTransparent() { return false; };

	void loadTexture();
	void RemoveFromRenderer(vtkRenderer* ren);
	void AddToRenderer(vtkRenderer* ren);
	void SetVolumeScale(double x, double y, double z);

	bool isVisible();
	void setVisible(bool a);

	double m_floorScale;
private:
	vtkTexture			*m_pTexture;
	vtkPolyData			*m_pPolyData;
	vtkPolyDataMapper	*m_pGroundMapper;
	vtkActor			*m_floorActor;

	bool m_bIsVisible;
};
