
#include "vtkVolumeShoal.h"
#include "ShoalExtraction/ShoalExtractionOutput.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "displayparameter.h"

/// vtk Stuff
#include "vtkActor.h"
#include "vtkActor2D.h"
#include "vtkMovLabeledDataMapper.h"
#include "vtkPoints.h" 
#include "vtkDataSetMapper.h"
#include "vtkCleanPolyData.h"
#include "vtkPolyData.h"
#include "vtkProperty.h"
#include "vtkDelaunay3D.h"
#include "vtkTriangleFilter.h"
#include "vtkPaletteScalarsToColors.h"
#include "vtkSmoothPolyDataFilter.h"
#include "vtkPolyDataNormals.h"
#include "vtkGeometryFilter.h"
#include "vtkUnstructuredGrid.h"
#include "vtkDecimatePro.h"
#include "vtkOutlineSource.h"
#include "vtkWrapper.h"

#include "colorlist.h"
#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/shoalstat.h"
#include "ShoalExtraction/data/pingdata.h"
#include "ShoalExtraction/data/pingshoalstat.h"

#include "M3DKernel/utils/carto/CartoTools.h"

#include <sstream>

using namespace shoalextraction;
using namespace std;

#define _USE_WORKER_THREAD


ShoalDisplay::ShoalDisplay()
	: volumicActor(NULL)
	, labelActor(NULL)
	, labelMapper(NULL)
{
	RegisterActors();
}

ShoalDisplay::ShoalDisplay(const ShoalDisplay & other)
	: volumicActor(other.volumicActor)
	, labelActor(other.labelActor)
	, labelMapper(other.labelMapper)
{
	RegisterActors();
}

ShoalDisplay & ShoalDisplay::operator=(const ShoalDisplay & other)
{
	other.RegisterActors();

	UnRegisterActors();

	volumicActor = other.volumicActor;
	labelActor = other.labelActor;
	labelMapper = other.labelMapper;

	return *this;
}

ShoalDisplay::~ShoalDisplay()
{
	UnRegisterActors();
}

void ShoalDisplay::RegisterActors() const
{
	if (volumicActor) volumicActor->Register(NULL);
	if (labelActor) labelActor->Register(NULL);
	if (labelMapper) labelMapper->Register(NULL);
}

void ShoalDisplay::UnRegisterActors() const
{
	if (volumicActor) volumicActor->UnRegister(NULL);
	if (labelActor) labelActor->UnRegister(NULL);
	if (labelMapper) labelMapper->UnRegister(NULL);
}


// ***************************************************************************
// class used for worker thread
// ***************************************************************************
ref class ComputeShoalWorker
{
private:
	vtkVolumeShoal * m_pVtkVolumeShoal;
	ShoalExtractionOutput * m_pShoal;
	vtkWrapper^ m_pWrapper;
	REFRESH3D_CALLBACK m_RefreshCB;

public:
	ComputeShoalWorker(vtkVolumeShoal * vtkVolume, ShoalExtractionOutput * shoal, vtkWrapper^ pWrapper,
		REFRESH3D_CALLBACK refreshCB)
		: m_pVtkVolumeShoal(vtkVolume), m_pShoal(shoal), m_pWrapper(pWrapper)
	{
		m_RefreshCB = refreshCB;
	}

	// thread delegate
	void Compute()
	{
		//compute volumic display and add resulting actor
		m_pVtkVolumeShoal->AddShoalActor(
			m_pShoal,
			m_pVtkVolumeShoal->ComputeShoalDisplay(m_pShoal),
			m_pWrapper);

		// OTK - on signale � l'IHM qu'un banc a �t� ajout� : un rendu doit �tre fait
		m_RefreshCB();
	}
};

vtkVolumeShoal::vtkVolumeShoal(vtkPaletteScalarsToColors * colorTransfer, REFRESH3D_CALLBACK refreshCB)
{
	m_bIsVisible = false;
	m_bDisplay = true;
	m_bSmooth = true;
	m_bDisplayLabels = false;
	m_DisplayType = eVolume;
	m_pColorTransferFunction = colorTransfer;
	m_RefreshCB = refreshCB;
}

vtkVolumeShoal::~vtkVolumeShoal(void)
{
	m_Lock.Lock();

	size_t nbShoals = m_ShoalList.size();
	for (size_t i = 0; i < nbShoals; i++)
	{
		MovUnRefDelete(m_ShoalList[i]);
	}

	m_Lock.Unlock();
}

void vtkVolumeShoal::Reset(vtkWrapper^ pWrapper)
{
	m_Lock.Lock();

	pWrapper->GetRendererLock()->Lock();
	ClearActors(pWrapper->ren1);
	pWrapper->GetRendererLock()->Unlock();

	size_t nbShoals = m_ShoalList.size();
	for (size_t i = 0; i < nbShoals; i++)
	{
		MovUnRefDelete(m_ShoalList[i]);
	}
	m_ShoalList.clear();

	m_Lock.Unlock();
}

void vtkVolumeShoal::SetVolumeScale(double x, double y, double z)
{
	m_Lock.Lock();

	for (size_t i = 0; i < m_ActorList.size(); i++)
	{
		m_ActorList[i].volumicActor->SetScale(x, y, z);
		if (m_ActorList[i].labelMapper != NULL)
		{
			m_ActorList[i].labelMapper->m_ScaleY = y;
		}
	}

	m_Lock.Unlock();
}

bool vtkVolumeShoal::isVisible()
{
	return m_bIsVisible;
}

void vtkVolumeShoal::setVisible(bool a)
{
	if (m_bIsVisible != a)
	{
		m_Lock.Lock();

		for (size_t i = 0; i < m_ActorList.size(); i++)
		{
			m_ActorList[i].volumicActor->SetVisibility(a);
			m_ActorList[i].labelActor->SetVisibility(a);
		}

		m_Lock.Unlock();
	}
	m_bIsVisible = a;
}

void vtkVolumeShoal::setDisplayShoal(bool display, vtkRenderer* ren)
{
	if (display != m_bDisplay)
	{
		m_bDisplay = display;

		m_Lock.Lock();
		if (!m_bDisplay)
		{
			// on enleve tous les acteurs vtk.
			ClearActors(ren);
		}
		else
		{
			// on construit tous les acteurs vtk
			ComputeActors(ren);
		}
		m_Lock.Unlock();
	}
}

void vtkVolumeShoal::setSmoothShoal(bool smooth, vtkRenderer* ren)
{
	if (smooth != m_bSmooth)
	{
		m_bSmooth = smooth;
		RecomputeActor(ren);
	}
}
void vtkVolumeShoal::setDisplayLabels(bool displayLabels, vtkRenderer* ren)
{
	if (displayLabels != m_bDisplayLabels)
	{
		m_bDisplayLabels = displayLabels;
		UpdateDisplay();
	}
}
void vtkVolumeShoal::RecomputeActor(vtkRenderer* ren)
{
	if (m_bDisplay)
	{
		m_Lock.Lock();
		// on enleve tous les acteurs vtk.
		ClearActors(ren);

		// on construit tous les acteurs vtk
		ComputeActors(ren);
		m_Lock.Unlock();
		UpdateDisplay();
	}
}

void vtkVolumeShoal::setDisplayType(TShoalDisplayType type, vtkRenderer* ren)
{
	if (type != m_DisplayType)
	{
		m_DisplayType = type;
		RecomputeActor(ren);
	}
}


void vtkVolumeShoal::ClearActors(vtkRenderer* ren)
{
	size_t nbActors = m_ActorList.size();
	for (size_t i = 0; i < nbActors; i++)
	{
		ren->RemoveActor(m_ActorList[i].volumicActor);
		ren->RemoveActor(m_ActorList[i].labelActor);
	}
	m_ActorList.clear();
}

void vtkVolumeShoal::ComputeActors(vtkRenderer* ren)
{
	vector<ShoalExtractionOutput*>::iterator vectItr = m_ShoalList.begin();
	while (vectItr != m_ShoalList.end())
	{
		ShoalDisplay shoalDisplay = ComputeShoalDisplay(*vectItr);

		//si une forme 3D est cr��e
		if (shoalDisplay.volumicActor != NULL)
		{
			//ajouter la forme 3D
			m_ActorList.push_back(shoalDisplay);
			ren->AddActor(shoalDisplay.volumicActor);
			ren->AddActor(shoalDisplay.labelActor);
			shoalDisplay.volumicActor->SetVisibility(m_bIsVisible);
			shoalDisplay.labelActor->SetVisibility(m_bIsVisible);
			vectItr++;
		}
		else //sinon
		{
			//retirer le banc
			MovUnRefDelete((*vectItr));
			vectItr = m_ShoalList.erase(vectItr);
		}
	}
}

void vtkVolumeShoal::RemoveFromRenderer(vtkRenderer* ren)
{
	m_Lock.Lock();

	for (size_t i = 0; i < m_ActorList.size(); i++)
	{
		ren->RemoveActor(m_ActorList[i].volumicActor);
		ren->RemoveActor(m_ActorList[i].labelActor);
	}

	m_Lock.Unlock();
}

void vtkVolumeShoal::AddToRenderer(vtkRenderer* ren)
{
	m_Lock.Lock();

	for (size_t i = 0; i < m_ActorList.size(); i++)
	{
		ren->AddActor(m_ActorList[i].volumicActor);
		ren->AddActor(m_ActorList[i].labelActor);
	}

	m_Lock.Unlock();
}

//ajout du banc dans la liste des bancs affichables
void vtkVolumeShoal::AddShoal(ShoalExtractionOutput * shoal, vtkWrapper^ pWrapper)
{
	MovRef(shoal);

	if (m_bDisplay)
	{
		shoal->m_pClosedShoal->LockDestruction();

#ifndef _USE_WORKER_THREAD
		//compute volumic display and add resulting actor
		AddShoalActor(shoal, ComputeShoalDisplay(shoal), pWrapper);
#else
		ComputeShoalWorker^ computeShoalWorker = gcnew ComputeShoalWorker(this, shoal, pWrapper, m_RefreshCB);
		System::Threading::ThreadStart^ threadDelegate = gcnew System::Threading::ThreadStart(computeShoalWorker,
			&ComputeShoalWorker::Compute
		);
		System::Threading::Thread^ computeThread = gcnew System::Threading::Thread(threadDelegate);
		//launch worker thread
		computeThread->Start();
#endif	
	}
	else
	{
		m_Lock.Lock();
		m_ShoalList.push_back(shoal);
		m_Lock.Unlock();
	}
}

//ajout du banc dans la liste des bancs affichables
void vtkVolumeShoal::AddShoalActor(ShoalExtractionOutput * shoal, ShoalDisplay shoalDisplay,
	vtkWrapper^ pWrapper)
{
	if (shoalDisplay.volumicActor != NULL)
	{

		// si on a un volume, on a forc�ment un label associ� ...
		assert(shoalDisplay.labelActor != NULL);

		m_Lock.Lock();
		m_ShoalList.push_back(shoal);
		m_ActorList.push_back(shoalDisplay);

		pWrapper->GetRendererLock()->Lock();
		pWrapper->ren1->AddActor(shoalDisplay.volumicActor);
		shoalDisplay.volumicActor->SetVisibility(false);
		pWrapper->ren1->AddActor(shoalDisplay.labelActor);
		shoalDisplay.labelActor->SetVisibility(false);
		pWrapper->GetRendererLock()->Unlock();

		shoal->m_pClosedShoal->UnlockDestruction();
		m_Lock.Unlock();
	}
	else
	{
		shoal->m_pClosedShoal->UnlockDestruction();
		MovUnRefDelete(shoal);
	}
}

//purge des bancs qui sont sortis de l'historique de l'appli
void vtkVolumeShoal::PurgeOldShoals(vtkWrapper^ pWrapper)
{
	M3DKernel * pKern = M3DKernel::GetInstance();
	pKern->Lock();
	std::uint64_t oldestPingId = -1;
	if (pKern->getObjectMgr()->GetPingFanContainer().GetNbFan() > 0)
	{
		PingFan *pFan = (PingFan *)pKern->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(0);
		oldestPingId = pFan->GetPingId();
	}
	pKern->Unlock();

	if (oldestPingId >= 0)
	{
		m_Lock.Lock();
		pWrapper->GetRendererLock()->Lock();

		// on retire tous les bancs qui ne sont plus completement inclus dans la zone m�moire des donn�es brutes
		vector<ShoalExtractionOutput*>::iterator vectItr = m_ShoalList.begin();
		vector<ShoalDisplay>::iterator vectActorItr = m_ActorList.begin();
		while (vectItr != m_ShoalList.end())
		{
			ShoalData* pShoal = (*vectItr)->m_pClosedShoal;

			std::uint64_t firstPingId = (*(pShoal->GetPings().begin()))->GetPingId();
			if (firstPingId < oldestPingId && !pShoal->IsLockedDestruction())
			{
				// suppression du banc
				MovUnRefDelete((*vectItr));
				vectItr = m_ShoalList.erase(vectItr);

				if (m_bDisplay)
				{
					pWrapper->ren1->RemoveActor(vectActorItr->volumicActor);
					pWrapper->ren1->RemoveActor(vectActorItr->labelActor);
					vectActorItr = m_ActorList.erase(vectActorItr);
				}
			}
			else
			{
				vectItr++;
				if (m_bDisplay)
				{
					vectActorItr++;
				}
			}
		}

		pWrapper->GetRendererLock()->Unlock();
		m_Lock.Unlock();
	}
}

void vtkVolumeShoal::UpdateDisplay()
{
	if (isVisible() && m_bDisplay)
	{
		M3DKernel * pKern = M3DKernel::GetInstance();
		pKern->Lock();

		// on cache tous les bancs qui ne poss�dent pas d'�l�ments dans la zone d'�chos affich�s
		int pingFanSeen = DisplayParameter::getInstance()->GetPingFanLength();
		int shiftPingFan = DisplayParameter::getInstance()->GetPingFanDelayed();

		size_t nbFan = pKern->getObjectMgr()->GetPingFanContainer().GetNbFan();
		if (nbFan > 0)
		{
			PingFan *pFan = (PingFan *)pKern->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(nbFan - 1);
			std::uint64_t newestPingId = pFan->GetPingId();
			pKern->Unlock();

			std::int64_t newestDisplayedPingId = newestPingId - shiftPingFan;
			std::int64_t oldestDisplayedPingId = std::max<std::int64_t>(0, newestDisplayedPingId - pingFanSeen);

			m_Lock.Lock();

			vector<ShoalExtractionOutput*>::iterator vectItr = m_ShoalList.begin();
			vector<ShoalDisplay>::iterator vectActorItr = m_ActorList.begin();
			while (vectItr != m_ShoalList.end() && vectActorItr != m_ActorList.end())
			{
				ShoalData* pShoal = (*vectItr)->m_pClosedShoal;

				std::int64_t nbPings = pShoal->GetPings().size();
				if (nbPings > 0)
				{
					std::uint64_t firstPingId = (*(pShoal->GetPings().begin()))->GetPingId();
					std::uint64_t lastPingId = (pShoal->GetPings()[nbPings - 1])->GetPingId();

					bool displayShoal = m_bDisplay && m_bIsVisible &&
						(lastPingId > oldestDisplayedPingId) &&
						(firstPingId < newestDisplayedPingId);

					bool displayShoalLabel = displayShoal && m_bDisplayLabels;

					vectActorItr->volumicActor->SetVisibility(displayShoal);
					vectActorItr->labelActor->SetVisibility(displayShoalLabel);
				}

				vectItr++;
				vectActorItr++;
			}
			m_Lock.Unlock();
		}
		else
		{
			pKern->Unlock();
		}
	}
}

ShoalDisplay vtkVolumeShoal::ComputeShoalDisplay(ShoalExtractionOutput * shoal)
{
	ShoalDisplay result;

	bool useGPSPositionning = DisplayParameter::getInstance()->GetUseGPSPositionning();

	System::Drawing::Color color = ColorList::GetInstance()->GetColor(
		shoal->m_pClosedShoal->GetExternId());

	// Affichage des volumes
	switch (m_DisplayType)
	{
	case eVolume:
	{
		vtkPolyData* stripsPoly = vtkPolyData::New();
		vtkPoints* stripsPoints = vtkPoints::New();
		vtkCellArray* stripsArray = vtkCellArray::New();

		// pour chaque facette du banc, r�cup�ration et affichage du trianglestrip correspondant
		const vector<vector<double>>& strips = shoal->m_pClosedShoal->GetContourStrips(useGPSPositionning);

		size_t nbStrips = strips.size();
		double x, y;
		for (size_t i = 0; i < nbStrips; i++)
		{
			const vector<double>& strip = strips[i];
			vtkIdType nbPoints = strip.size() / 3; // 3 doubles par point (X,Y,Z)
			vtkIdType* ids = new vtkIdType[nbPoints];


			for (vtkIdType j = 0; j < nbPoints; j++)
			{
				x = strip[j * 3];
				y = strip[j * 3 + 1];
				if (useGPSPositionning)
				{
					// conversion du lat/long en cartesien (projection conique conforme)
					CCartoTools::GetCartesianCoords(x, y, x, y);
				}

				// rappel : toviewopenGL = Vector3D(ref.y,-ref.z,-ref.x);
				ids[j] = stripsPoints->InsertNextPoint(y, -strip[j * 3 + 2], -x);
			}
			stripsArray->InsertNextCell(nbPoints, ids);
			delete[] ids;
		}

		stripsPoly->SetPoints(stripsPoints);
		stripsPoly->SetStrips(stripsArray);

		vtkPolyDataMapper *map = vtkPolyDataMapper::New();

		vtkDecimatePro* deci = NULL;
		vtkSmoothPolyDataFilter* smoother = NULL;
		vtkPolyDataNormals* normals = NULL;
		vtkTriangleFilter* triangle = NULL;
		vtkCleanPolyData *cleaner = NULL;

		if (m_bSmooth)
		{
			triangle = vtkTriangleFilter::New();
			cleaner = vtkCleanPolyData::New();
			deci = vtkDecimatePro::New();
			smoother = vtkSmoothPolyDataFilter::New();
			normals = vtkPolyDataNormals::New();
			triangle->SetInputData(stripsPoly);
			cleaner->AddInputConnection(triangle->GetOutputPort());
			deci->SetTargetReduction(0.9);
			deci->PreserveTopologyOn();
			deci->SetInputConnection(cleaner->GetOutputPort());
			smoother->SetInputConnection(deci->GetOutputPort());
			smoother->SetNumberOfIterations(50);
			smoother->BoundarySmoothingOn();
			smoother->SetFeatureAngle(90);
			smoother->SetEdgeAngle(60);
			smoother->SetRelaxationFactor(.0025);
			normals->SetInputConnection(smoother->GetOutputPort());
			normals->FlipNormalsOn();
			map->SetInputConnection(normals->GetOutputPort());
		}
		else
		{
			map->SetInputData(stripsPoly);
		}

		result.volumicActor = vtkActor::New();
		result.volumicActor->SetMapper(map);

		// couleur du banc : sv
		double rgba[4];
		m_pColorTransferFunction->GetColor(shoal->m_pClosedShoal->GetShoalStat()->GetMeanSv()*100., rgba);
		result.volumicActor->GetProperty()->SetColor(rgba[0], rgba[1], rgba[2]);
		result.volumicActor->GetProperty()->SetOpacity(rgba[3]);


		if (m_bSmooth)
		{
			triangle->Delete();
			cleaner->Delete();
			deci->Delete();
			smoother->Delete();
			normals->Delete();
		}
		map->Delete();
		stripsPoly->Delete();
		stripsArray->Delete();
		stripsPoints->Delete();
	}
	break;
	case ePoints:
	{
		vtkPoints *points = vtkPoints::New();

		// pour chaque facette du banc, r�cup�ration et affichage du trianglestrip correspondant
		const vector<vector<double>>& strips = shoal->m_pClosedShoal->GetContourStrips(useGPSPositionning);

		size_t nbStrips = strips.size();
		double x, y;
		for (size_t i = 0; i < nbStrips; i++)
		{
			const vector<double>& strip = strips[i];
			vtkIdType nbPoints = strip.size() / 3; // 3 doubles par point (X,Y,Z)

			for (vtkIdType j = 0; j < nbPoints; j++)
			{
				x = strip[j * 3];
				y = strip[j * 3 + 1];
				if (useGPSPositionning)
				{
					// conversion du lat/long en cartesien (projection conique conforme)
					CCartoTools::GetCartesianCoords(x, y, x, y);
				}

				// rappel : toviewopenGL = Vector3D(ref.y,-ref.z,-ref.x);
				points->InsertNextPoint(y, -strip[j * 3 + 2], -x);
			}
		}

		vtkUnstructuredGrid* vtk_grid = MakeGrid(points);

		//point visu
		vtkPolyData *pointData = vtkPolyData::New();
		pointData->SetVerts(vtk_grid->GetCells());
		pointData->SetPoints(vtk_grid->GetPoints());

		vtkDataSetMapper *pointMapper = vtkDataSetMapper::New();
		pointMapper->SetInputData(pointData);
		pointMapper->ScalarVisibilityOn();

		//2 actors
		result.volumicActor = vtkActor::New();
		result.volumicActor->SetMapper(pointMapper);
		result.volumicActor->GetProperty()->SetColor(color.R / 255.0, color.G / 255.0, color.B / 255.0);
		result.volumicActor->GetProperty()->SetPointSize(2);

		pointMapper->Delete();
		pointData->Delete();
		points->Delete();
		vtk_grid->Delete();
	}
	break;
	case eBox:
	{
		vtkOutlineSource *src = vtkOutlineSource::New();
		const auto& obbox = shoal->m_pClosedShoal->GetShoalStat()->GetOBBox();
		const auto& obbox_points = obbox.GetPoints();
		const size_t pointNb = obbox_points.size();

		double bbpoints[24];

		if (useGPSPositionning)
		{
			obbox.ComputePointsGPS(bbpoints);
		}
		else
		{
			for (size_t j = 0; j < pointNb; j++)
			{
				BaseMathLib::Vector3D realCoord = BaseMathLib::Vector3D::ToViewOpengl(obbox_points[j]);

				bbpoints[j * 3] = realCoord.x;
				bbpoints[j * 3 + 1] = realCoord.y;
				bbpoints[j * 3 + 2] = realCoord.z;
			}
		}

		src->SetBoxTypeToOriented();
		src->SetCorners(bbpoints);

		vtkDataSetMapper *pointMapper = vtkDataSetMapper::New();
		pointMapper->SetInputConnection(src->GetOutputPort());
		pointMapper->ScalarVisibilityOn();

		//2 actors
		result.volumicActor = vtkActor::New();
		result.volumicActor->SetMapper(pointMapper);
		result.volumicActor->GetProperty()->SetColor(color.R / 255.0, color.G / 255.0, color.B / 255.0);
		result.volumicActor->GetProperty()->SetPointSize(2);

		pointMapper->Delete();
		src->Delete();
	}
	break;
	case eDelaunay:
	{
		// ancienne m�thode de repr�sentation des volumes des bancs (triangulation vtkDelaunay3D)
		// OTK - 07/05/2009 - Si on n'a pas au moins 2 surfaces dans 2 pings diff�rents, on ne trace pas le volume
		// sinon, le rendu VTK provoque une division par zero.
		if (shoal->m_pClosedShoal->GetPings().size() >= 2)
		{
			vtkPoints *points = vtkPoints::New();
			// pour chaque facette du banc, r�cup�ration et affichage du trianglestrip correspondant
			const vector<vector<double>>& strips = shoal->m_pClosedShoal->GetContourStrips(useGPSPositionning);

			size_t nbStrips = strips.size();
			double x, y;
			for (size_t i = 0; i < nbStrips; i++)
			{
				const vector<double>& strip = strips[i];
				vtkIdType nbPoints = strip.size() / 3; // 3 doubles par point (X,Y,Z)

				for (vtkIdType j = 0; j < nbPoints; j++)
				{
					x = strip[j * 3];
					y = strip[j * 3 + 1];
					if (useGPSPositionning)
					{
						// conversion du lat/long en cartesien (projection conique conforme)
						CCartoTools::GetCartesianCoords(x, y, x, y);
					}

					// rappel : toviewopenGL = Vector3D(ref.y,-ref.z,-ref.x);
					points->InsertNextPoint(y, -strip[j * 3 + 2], -x);
				}
			}

			vtkPolyData* profile = vtkPolyData::New();
			profile->SetPoints(points);

			vtkDelaunay3D *del = vtkDelaunay3D::New();
			del->SetInputData(profile);
			del->SetTolerance(0.00);
			del->SetAlpha(0.0);

			//if compute of 3D shoal is on worker thread and sounder is visible, 
			//force compute of algo on current thread
#ifdef _USE_WORKER_THREAD
			if (isVisible() && m_bDisplay)
			{
				del->Update();
			}
#endif

			vtkPolyDataNormals *normals = NULL;
			vtkSmoothPolyDataFilter *smooth = NULL;
			vtkGeometryFilter *geom = NULL;
			if (m_bSmooth)
			{
				geom = vtkGeometryFilter::New();
				geom->SetInputConnection(del->GetOutputPort());

				smooth = vtkSmoothPolyDataFilter::New();
				smooth->SetInputConnection(geom->GetOutputPort());
				smooth->SetNumberOfIterations(50);
				smooth->BoundarySmoothingOn();
				smooth->SetFeatureAngle(90);
				smooth->SetEdgeAngle(60);
				smooth->SetRelaxationFactor(.025);

				normals = vtkPolyDataNormals::New();
				normals->SetInputConnection(smooth->GetOutputPort());
			}

			vtkDataSetMapper *map = vtkDataSetMapper::New();
			if (m_bSmooth)
			{
				map->SetInputConnection(normals->GetOutputPort());
			}
			else
			{
				map->SetInputConnection(del->GetOutputPort());
			}


			result.volumicActor = vtkActor::New();
			result.volumicActor->SetMapper(map);
			// couleur du banc : sv
			double rgba[4];
			m_pColorTransferFunction->GetColor(shoal->m_pClosedShoal->GetShoalStat()->GetMeanSv()*100., rgba);
			result.volumicActor->GetProperty()->SetColor(rgba[0], rgba[1], rgba[2]);
			result.volumicActor->GetProperty()->SetOpacity(rgba[3]);
			if (m_bSmooth)
			{
				result.volumicActor->GetProperty()->SetInterpolationToFlat();
				result.volumicActor->GetProperty()->SetInterpolationToGouraud();
			}

			// nettoyage
			points->Delete();
			profile->Delete();
			del->Delete();
			map->Delete();
			if (m_bSmooth)
			{
				geom->Delete();
				normals->Delete();
				smooth->Delete();
			}
		}
	}
	break;
	}

	// affichage du label du banc
	// *****************************

	// seulement si on affiche un banc
	if (result.volumicActor != NULL)
	{
		result.labelActor = vtkActor2D::New();
		const vector<vector<double>>& strips = shoal->m_pClosedShoal->GetContourStrips(useGPSPositionning);
		if (strips.size() > 0)
		{
			vtkPoints * labelPoints = vtkPoints::New();
			labelPoints->SetNumberOfPoints(1);

			double x = strips[0][0];
			double y = strips[0][1];
			if (useGPSPositionning)
			{
				// conversion du lat/long en cartesien (projection conique conforme)
				CCartoTools::GetCartesianCoords(x, y, x, y);
			}
			labelPoints->SetPoint(0, y, -strips[0][2], -x);

			vtkPolyData * labelPolyData = vtkPolyData::New();
			labelPolyData->SetPoints(labelPoints);

			result.labelMapper = vtkMovLabeledDataMapper::New();
			result.labelActor->SetMapper(result.labelMapper);

			result.labelMapper->SetInput(labelPolyData);
			std::ostringstream oss;
			oss << shoal->m_pClosedShoal->GetExternId();
			result.labelMapper->AddLabel(oss.str());

			labelPolyData->Delete();
			labelPoints->Delete();
		}

	}

	return result;
}

// Seulement pour repr�sentation des points
vtkUnstructuredGrid* vtkVolumeShoal::MakeGrid(vtkPoints* points)
{
	vtkUnstructuredGrid* result = vtkUnstructuredGrid::New();
	if (points != NULL)
	{
		result->SetPoints(points);
		int nbP = result->GetNumberOfPoints();
		for (vtkIdType i = 0; i < nbP; i++)
			result->InsertNextCell(VTK_VERTEX, 1, &i);
	}

	return result;
}