#pragma once

#include <vector>
#include "M3DKernel/utils/multithread/RecursiveMutex.h"

class ShoalExtractionOutput;
class vtkActor;
class vtkPaletteScalarsToColors;
class vtkPoints;
class vtkUnstructuredGrid;
class vtkActor;
class vtkActor2D;
class vtkMovLabeledDataMapper;
class vtkRenderer;
ref class vtkWrapper;

typedef enum ShoalDisplayType
{
	eVolume,
	eBox,
	ePoints,
	eDelaunay
} TShoalDisplayType;

typedef void(__stdcall *REFRESH3D_CALLBACK)(void);

struct ShoalDisplay {
	vtkActor * volumicActor;
	vtkActor2D * labelActor;
	vtkMovLabeledDataMapper * labelMapper;

	ShoalDisplay();
	ShoalDisplay(const ShoalDisplay & other);
	ShoalDisplay & operator=(const ShoalDisplay & other);
	~ShoalDisplay();

private:
	void RegisterActors() const;
	void UnRegisterActors() const;
};

class vtkVolumeShoal
{
public:
	vtkVolumeShoal(vtkPaletteScalarsToColors * colorTransfer, REFRESH3D_CALLBACK refreshCB);
	virtual ~vtkVolumeShoal(void);

	void AddShoal(ShoalExtractionOutput * shoal, vtkWrapper^ pWrapper);
	void AddShoalActor(ShoalExtractionOutput * shoal, ShoalDisplay shoalDisplay,
		vtkWrapper^ pWrapper);
	ShoalDisplay ComputeShoalDisplay(ShoalExtractionOutput * shoal);

	vtkUnstructuredGrid* MakeGrid(vtkPoints* points);
	void PurgeOldShoals(vtkWrapper^ pWrapper);
	void UpdateDisplay();

	void setDisplayShoal(bool display, vtkRenderer* ren);
	bool getDisplayShoal() { return m_bDisplay; };

	void setSmoothShoal(bool smooth, vtkRenderer* ren);
	bool getSmoothShoal() { return m_bSmooth; };

	void setDisplayLabels(bool displaylabels, vtkRenderer* ren);
	bool getDisplayLabels() { return m_bDisplayLabels; };

	void setDisplayType(TShoalDisplayType type, vtkRenderer* ren);
	TShoalDisplayType getDisplayType() { return m_DisplayType; };

	bool IsTransparent() { return false; };

	void RemoveFromRenderer(vtkRenderer* ren);
	void AddToRenderer(vtkRenderer* ren);
	void SetVolumeScale(double x, double y, double z);
	bool isVisible();
	void setVisible(bool a);

	void Reset(vtkWrapper^ pWrapper);
	void RecomputeActor(vtkRenderer* ren);


protected:
	void ClearActors(vtkRenderer* ren);
	void ComputeActors(vtkRenderer* ren);

private:

	CRecursiveMutex m_Lock;

	// param�tres
	bool	m_bDisplay;
	bool	m_bDisplayLabels;
	bool m_bIsVisible;
	bool	m_bSmooth;
	TShoalDisplayType	m_DisplayType;

	// donn�es bancs
	std::vector<ShoalExtractionOutput*> m_ShoalList;
	// acteurs VTK bancs
	std::vector<ShoalDisplay> m_ActorList;

	// palette
	vtkPaletteScalarsToColors * m_pColorTransferFunction;

	// callback de rafraichissement de la vue 3D
	REFRESH3D_CALLBACK m_RefreshCB;
};
