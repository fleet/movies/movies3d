#include "vtkVolumeSingleEcho.h"

#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"

#include "vtkProperty.h"
#include "vtkRenderer.h"
#include "vtkGlyph3D.h"
#include "vtkTubeFilter.h"
#include "vtkActor2D.h"
#include "vtkMovLabeledDataMapper.h"

#include "vtkPaletteScalarsToColors.h"

#include "M3DKernel/utils/log/ILogger.h"

namespace
{
	constexpr const char * LoggerName = "MoviesView.View.VolumicView.3dViewer.vtkVolumeSingleEcho";
}

vtkVolumeSingleEcho::vtkVolumeSingleEcho()
{
	m_pMapper = vtkPolyDataMapper::New();
	m_pActor = vtkActor::New();
	m_bIsVisible = true;
	m_glyphInit = false;
	m_colorFunction = vtkPaletteScalarsToColors::New();
	m_pMapper->SetLookupTable(m_colorFunction);
	m_pActor->SetMapper(m_pMapper);
	m_pActor->SetVisibility(false);
	m_Size = 0.05;
	m_fScale = 10.0;
	m_pRefGlyph = NULL;
	m_bDisplay = true;

	// OTK - FAE214 - affichage des tracks
	m_pTracksMapper = vtkPolyDataMapper::New();
	m_pTracksMapper->ScalarVisibilityOn();
	m_pTracksMapper->SetLookupTable(m_colorFunction);
	m_pTracksMapper->SetScalarModeToUsePointFieldData();
	m_pTracksMapper->SelectColorArray("str");
	m_pTracksActor = vtkActor::New();
	m_pTracksActor->SetMapper(m_pTracksMapper);
	m_pTracksActor->SetVisibility(false);

	m_pTracksLabelsMapper = vtkMovLabeledDataMapper::New();
	m_pTracksLabelsActor = vtkActor2D::New();
	m_pTracksLabelsActor->SetMapper(m_pTracksLabelsMapper);
	m_pTracksLabelsActor->SetVisibility(false);
}

vtkVolumeSingleEcho::~vtkVolumeSingleEcho()
{
	m_pTracksMapper->Delete();
	m_pTracksActor->Delete();
	m_pTracksLabelsMapper->Delete();
	m_pTracksLabelsActor->Delete();
	m_pMapper->Delete();
	m_pActor->Delete();
	m_colorFunction->Delete();
	// OTK - FAE003 - fuite m�moire
	if (m_pRefGlyph)
		m_pRefGlyph->Delete();
}

void vtkVolumeSingleEcho::SetVolumeScale(double x, double y, double z)
{
	m_pActor->SetScale(x, y, z);
	m_pTracksActor->SetScale(x, y, z);
	m_pTracksLabelsMapper->m_ScaleY = y;
}

void vtkVolumeSingleEcho::SetSize(double size)
{
	if (size == 0)
	{
		M3D_LOG_WARN(LoggerName, "Null Size given,  skipping");
		return;
	}

	m_Size = size;
	if (m_glyphInit && m_pRefGlyph)
	{
		m_pRefGlyph->SetScaleFactor(size);
	}
}

bool vtkVolumeSingleEcho::isVisible()
{
	return m_bIsVisible;
}

void vtkVolumeSingleEcho::setVisible(bool a)
{
	m_bIsVisible = a;
	if (m_glyphInit)
	{
		m_pActor->SetVisibility(a);
	}
	m_pTracksActor->SetVisibility(a);
	m_pTracksLabelsActor->SetVisibility(a);
}

void vtkVolumeSingleEcho::RemoveFromRenderer(vtkRenderer* ren)
{
	ren->RemoveActor(m_pActor);
	ren->RemoveActor(m_pTracksActor);
	ren->RemoveActor(m_pTracksLabelsActor);
}

void vtkVolumeSingleEcho::AddToRenderer(vtkRenderer* ren)
{
	ren->AddActor(m_pActor);
	ren->AddActor(m_pTracksActor);
	ren->AddActor(m_pTracksLabelsActor);
}

void vtkVolumeSingleEcho::applyGlyph(vtkGlyph3D *p)
{
	if (p)
	{
		p->SetScaleFactor(m_Size);
		m_pMapper->SetInputConnection(p->GetOutputPort());
		m_pActor->SetVisibility(m_bIsVisible);
		if (m_pRefGlyph)
			m_pRefGlyph->Delete();
		m_pRefGlyph = p;
		m_glyphInit = true;
	}
	else
	{
		m_glyphInit = false;
		m_pActor->SetVisibility(0);
		if (m_pRefGlyph)
			m_pRefGlyph->Delete();
		m_pRefGlyph = NULL;
	}
}

void vtkVolumeSingleEcho::SetEchoPalette(const IColorPalette * palette)
{
	m_colorFunction->setPalette(palette, 1.0, false);
}

void vtkVolumeSingleEcho::RenderWireframe(bool bWireframe)
{
	if (bWireframe)
	{
		m_pTracksActor->GetProperty()->SetRepresentationToWireframe();
		m_pActor->GetProperty()->SetRepresentationToWireframe();
	}
	else
	{
		m_pTracksActor->GetProperty()->SetRepresentationToSurface();
		m_pActor->GetProperty()->SetRepresentationToSurface();
	}
}

void vtkVolumeSingleEcho::applyTracks(vtkTubeFilter* p, vtkPolyData * pLabelsPolyData, const std::vector<std::string> & labels)
{
	if (p)
	{
		m_pTracksMapper->SetInputConnection(p->GetOutputPort());
		m_pTracksActor->SetVisibility(m_bIsVisible);
		m_pTracksLabelsMapper->SetInput(pLabelsPolyData);
		m_pTracksLabelsMapper->SetLabels(labels);
		m_pTracksLabelsActor->SetVisibility(m_bIsVisible);
	}
	else
	{
		m_pTracksActor->SetVisibility(0);
		m_pTracksLabelsActor->SetVisibility(0);
	}
}