#pragma once

class vtkPolyData;
class vtkPolyDataMapper;
class vtkActor;
class vtkRenderer;
class vtkGlyph3D;
class vtkTubeFilter;
class vtkActor2D;
class vtkMovLabeledDataMapper;
class vtkPaletteScalarsToColors;

class IColorPalette;

#include <vector>
#include <string>

class vtkVolumeSingleEcho
{
public:
	vtkVolumeSingleEcho();
	virtual ~vtkVolumeSingleEcho();
	
	void applyGlyph(vtkGlyph3D *p);
	void applyTracks(vtkTubeFilter* p, vtkPolyData * pLabelsPolyData, const std::vector<std::string> & labels);

	bool IsTransparent() { return false; };

	void RemoveFromRenderer(vtkRenderer* ren);
	void AddToRenderer(vtkRenderer* ren);
	void SetVolumeScale(double x, double y, double z);
	void SetSize(double size);
	double GetSize() { return m_Size; };
	bool isVisible();
	void setVisible(bool a);
	double m_fScale;

	void SetEchoPalette(const IColorPalette * palette);

	// NMD - 08/09/2011 - FAE 092 - rendu fil de fer
	void RenderWireframe(bool bWireframe);


private:
	double m_Size;
	bool	m_bDisplay;
	vtkPaletteScalarsToColors *m_colorFunction;
	vtkPolyDataMapper	*m_pMapper;
	vtkActor			*m_pActor;
	bool				m_glyphInit;
	vtkGlyph3D *m_pRefGlyph;
	bool m_bIsVisible;

	// OTK - FAE214 - ajout de la représentation des tracks des échos simples
	vtkPolyDataMapper	*m_pTracksMapper;
	vtkActor			    *m_pTracksActor;

	vtkActor2D        *m_pTracksLabelsActor;
	vtkMovLabeledDataMapper *m_pTracksLabelsMapper;

};
