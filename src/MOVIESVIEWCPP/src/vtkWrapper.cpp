#pragma unmanaged
//#include "M3DKernel/M3DKernel.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyleUser.h"

//#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "vtkWrapper.h"
#include "vtk.h"
#include "vtkGlyph4me.h"
#include "vtkMovPolyData.h"
#include "vtkAppendPolyData.h"
#include "vtkPolyDataAlgorithm.h"
#include "vtkAlgorithmOutput.h"
#include "vtkMatrix4x4.h"
#include "M3DKernel/DefConstants.h"

#include "vtkClipPolyData.h"
#include "vtkBox.h"
#include "vtkCubeSource.h"
#include "vtkPlane.h"
#include "vtkMovSurfaceFilter.h" 
#include <float.h>
#include "vtkWindowToImageFilter.h"
#include "vtkAVIWriter.h"
#include "vtkCallbackCommand.h"
//#include "vtkMPEG2Writer.h"

#include "vtkVolumeSingleEcho.h"
#include "vtkVolumePhaseEcho.h"
#include "ColorPaletteEcho.h"
#include "ColorPaletteChannel.h"

#include "vtkVectorText.h"
#include "vtkFollower.h"
#include "vtkActor2D.h"
#include "vtkTextMapper.h"
#include "vtkTextProperty.h"

#include "vtkSmartPointer.h"
#include "vtkMovCompassRepresentation.h"

#include "vtkOpenGLRenderer.h"

#include "PingFan3D.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/NavAttributes.h"

#include "vtkAutoInit.h"

VTK_MODULE_INIT(vtkRenderingContextOpenGL2);
VTK_MODULE_INIT(vtkRenderingOpenGL2)
VTK_MODULE_INIT(vtkInteractionStyle)
VTK_MODULE_INIT(vtkRenderingFreeType);

#define maxMax(a,b,c) max(max(a,b),c)

using namespace System;
using namespace System::Runtime::InteropServices;

typedef void(__stdcall *VTK_EVENT_CALLBACK)(void);


vtkWrapper::vtkWrapper(REFRESH3D_CALLBACK refreshCB, ContextMenuDelegate^ contextMenuDelegate)
{
	m_pSounderMgr = new vtkSounderMgr(refreshCB);
	m_cameraSettings = gcnew CameraSettings();
	renWin = NULL;
	ren1 = NULL;

	m_bFreeze = false;

	m_bisAviRendering = false;

	m_pPlaneWidget = NULL;
	m_pCompassWidget = NULL;
	m_pPlane = NULL;
	m_vtkUpdatePlaneEventCBC = NULL;
	m_vtkCompassWidgetCBC = NULL;
	m_pPlaneContext = NULL;

	m_ParentContextMenuDelegate = contextMenuDelegate;

	//FRE - 29/09/2009 renderer lock
	m_pLock = new CRecursiveMutex();

#ifndef _DEBUG
	vtkObject::GlobalWarningDisplayOff();
#endif
}

vtkWrapper::~vtkWrapper()
{
	ClearAllSounder();
	RemovePlaneWidget();
	m_pTextActor->Delete();
	m_pCompassWidget->Delete();
	m_vtkUpdatePlaneEventCBC->Delete();
	m_vtkCompassWidgetCBC->Delete();
	m_vtkMouseEventCBC->Delete();

	delete m_cameraSettings;

	delete m_pSounderMgr;
	ren1->Delete();
	renWin->Delete();
	renWin = NULL;
	//FRE - 29/09/2009 renderer lock
	delete m_pLock;
}

bool  vtkWrapper::isUseSort(unsigned long sounderId)
{
	vtkSounder *pSound = this->m_pSounderMgr->GetSounder(sounderId);
	if (pSound)
		return pSound->isUseSort();
	return false;
}

void vtkWrapper::SetToWireFrame(bool bWireFrame)
{
	for (unsigned int gfg = 0; gfg < GetSounderMgr()->GetNbSounder(); gfg++)
	{
		vtkSounder*pSound = GetSounderMgr()->GetSounderIdx(gfg);
		pSound->SetToWireFrame(bWireFrame);
	}
}

void  vtkWrapper::SetSort(bool a, unsigned long sounderId)
{
	vtkSounder *pSound = this->m_pSounderMgr->GetSounder(sounderId);
	if (pSound)
	{
		pSound->SetSort(a, ren1->GetActiveCamera());
		pSound->SetEchoPalette(DisplayParameter::getInstance()->GetCurrent3DEchoPalette());
	}
	RefreshAndSortActorList();
}

void vtkWrapper::setBackGroundColor(double r, double g, double b)
{
	if (ren1)
		ren1->SetBackground(r, g, b);
}

// callback associ� au widget boussole (pour agir sur la camera
// en fonction des actions sur le widget)
void UpdateCompassEvents(vtkObject* object, unsigned long
	Event, void* clientdata, void*
	calldata)
{
	// on se contente d'appeler un d�l�g�, qui lui aura acc�s � tous les membres de vtkWrapper
	((VTK_EVENT_CALLBACK)clientdata)();
}

// m�thode d�l�g�e pour le traitement des actions sur la boussole
void vtkWrapper::UpdateFromCompass()
{
	m_cameraSettings->m_zoomFactor = m_pCompassWidget->GetDistance();
	m_cameraSettings->m_elevation = m_pCompassWidget->GetTilt();
	m_cameraSettings->m_azimuth = m_pCompassWidget->GetHeading();
	ApplyCameraSettings(m_cameraSettings, true);
}

void vtkWrapper::UpdateCompass()
{
	m_pCompassWidget->SetDistance( m_cameraSettings->m_zoomFactor );
	//m_pCompassWidget->SetTilt(m_cameraSettings->m_elevation );
	m_pCompassWidget->SetHeading(m_cameraSettings->m_azimuth);
}

// m�thode d�l�g�e pour le traitement du contextMenu (menu sur clic droit)
void vtkWrapper::OnContextMenu()
{
	m_ParentContextMenuDelegate->Invoke();
	m_vtkMouseEventCBC->SetAbortFlag(1);
}

void ProcessMouseEvents(vtkObject* object, unsigned long
	Event, void* clientdata, void*
	calldata)
{
	switch (Event)
	{
	case vtkCommand::RightButtonPressEvent:
		((VTK_EVENT_CALLBACK)clientdata)();
		break;
	}
}

void vtkWrapper::InitWindow(System::Windows::Forms::PictureBox ^WindParent)
{
	m_WindParent = WindParent;
	long Handle = WindParent->Handle.ToInt32();

	renWin = vtkRenderWindow::New();

	ren1 = vtkOpenGLRenderer::New();
	ren1->SetBackground(DisplayParameter::getInstance()->GetVolumicBackgroundRed(),
		DisplayParameter::getInstance()->GetVolumicBackgroundGreen(),
		DisplayParameter::getInstance()->GetVolumicBackgroundBlue());

	renWin->AddRenderer(ren1);
	renWin->SetSize(100, 300);
	renWin->SetParentId((void*)Handle);

	renWin->PointSmoothingOn();

	vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = vtkSmartPointer<vtkRenderWindowInteractor>::New();
	renderWindowInteractor->SetRenderWindow(renWin);

	vtkSmartPointer<vtkInteractorStyleUser> style = vtkSmartPointer<vtkInteractorStyleUser>::New();
	renderWindowInteractor->SetInteractorStyle(style);

	m_ContextMenuDelegate = gcnew ContextMenuDelegate(this, &vtkWrapper::OnContextMenu);
	IntPtr contextMenuPtr = Marshal::GetFunctionPointerForDelegate(m_ContextMenuDelegate);

	m_vtkMouseEventCBC = vtkCallbackCommand::New();
	m_vtkMouseEventCBC->SetCallback(ProcessMouseEvents);
	m_vtkMouseEventCBC->SetClientData((void*)contextMenuPtr);

	renderWindowInteractor->AddObserver(vtkCommand::RightButtonPressEvent,
		m_vtkMouseEventCBC, 0.5);

	vtkCamera* cam = ren1->GetActiveCamera();
	cam->SetViewUp(0, 1, 0);

	cam->ParallelProjectionOn();
	cam->Azimuth(0);

	cam->Elevation(0);

	m_pTextActor = vtkActor2D::New();
	vtkTextMapper   *text2Mapper = vtkTextMapper::New();


	text2Mapper->GetTextProperty()->SetColor(1, 0, 0);
	m_pTextActor->SetMapper(text2Mapper);
	text2Mapper->Delete();


	m_pTextActor->SetDisplayPosition(10, 10);
	ren1->AddActor2D(m_pTextActor);

	m_vtkUpdatePlaneEventCBC = vtkCallbackCommand::New();

	// Ajout de la boussole
	m_vtkCompassWidgetCBC = vtkCallbackCommand::New();
	m_vtkCompassWidgetCBC->SetCallback(UpdateCompassEvents);

	m_UpdateCompassDelegate = gcnew UpdateCompassDelegate(this, &vtkWrapper::UpdateFromCompass);
	IntPtr compassCallbackPtr = Marshal::GetFunctionPointerForDelegate(m_UpdateCompassDelegate);

	vtkSmartPointer<vtkMovCompassRepresentation> compassRepresentation = vtkSmartPointer<vtkMovCompassRepresentation>::New();
	m_pCompassWidget = vtkMovCompassWidget::New();
	m_pCompassWidget->SetInteractor(renWin->GetInteractor());
	m_pCompassWidget->SetRepresentation(compassRepresentation);
	m_pCompassWidget->AddObserver(vtkCommand::InteractionEvent, m_vtkCompassWidgetCBC);
	m_pCompassWidget->EnabledOn();

	m_vtkCompassWidgetCBC->SetClientData((void*)compassCallbackPtr);

	m_pCompassWidget->EnabledOn();
	// fin ajout boussole

	ResetCamera();
	Render();
}

void vtkWrapper::SetStatText(System::String ^text)
{
	vtkTextMapper   *text2Mapper = vtkTextMapper::New();
	System::IntPtr ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(text);
	const char* strPath = static_cast<const char*>(ip.ToPointer());
	text2Mapper->SetInput(strPath);
	text2Mapper->GetTextProperty()->SetColor(1, 0, 0);
	m_pTextActor->SetMapper(text2Mapper);
	System::Runtime::InteropServices::Marshal::FreeHGlobal(ip);
	text2Mapper->Delete();
}

void vtkWrapper::ClearAllSounder()
{
	m_pSounderMgr->Clear(ren1);
}

void vtkWrapper::AddSounder(unsigned long sounderId)
{
	m_pSounderMgr->AddSounder(sounderId, ren1);
	m_pSounderMgr->GetSounder(sounderId)->SetEchoPalette(DisplayParameter::getInstance()->GetCurrent3DEchoPalette());
	RefreshAndSortActorList();
}

void vtkWrapper::RefreshAndSortActorList()
{
	for (unsigned int i = 0; i < m_pSounderMgr->GetNbSounder(); i++)
	{
		m_pSounderMgr->GetSounderIdx(i)->RemoveFromRenderer(ren1);
		m_pSounderMgr->GetSounderIdx(i)->getSeaFloor()->RemoveFromRenderer(ren1);
		m_pSounderMgr->GetSounderIdx(i)->getVolumeSingleEcho()->RemoveFromRenderer(ren1);
		m_pSounderMgr->GetSounderIdx(i)->getVolumePhaseEcho()->RemoveFromRenderer(ren1);

		if (!m_pSounderMgr->GetSounderIdx(i)->IsTransparent())
		{
			m_pSounderMgr->GetSounderIdx(i)->AddToRenderer(ren1);
		}
		m_pSounderMgr->GetSounderIdx(i)->getSeaFloor()->AddToRenderer(ren1);
		m_pSounderMgr->GetSounderIdx(i)->getVolumeSingleEcho()->AddToRenderer(ren1);
		m_pSounderMgr->GetSounderIdx(i)->getVolumePhaseEcho()->AddToRenderer(ren1);
	}
	for (unsigned int i = 0; i < m_pSounderMgr->GetNbSounder(); i++)
	{
		if (m_pSounderMgr->GetSounderIdx(i)->IsTransparent())
		{
			m_pSounderMgr->GetSounderIdx(i)->AddToRenderer(ren1);
		}
	}
}

void vtkWrapper::SaveCameraSettings()
{
	vtkCamera* cam = ren1->GetActiveCamera();

	// Compute cam elevation
	const double * camPos = cam->GetPosition();
	const double * camFocalPoint = cam->GetFocalPoint();

	const double dx = camPos[0] - camFocalPoint[0];
	const double dy = camPos[1] - camFocalPoint[1];
	const double dz = camPos[2] - camFocalPoint[2];

	const double camElevation = atan2(dy, sqrt(dx * dx + dz * dz));
	
	// Save camera settings
	auto displayParameters = DisplayParameter::getInstance();
	displayParameters->SetAzimuth(this->m_cameraSettings->m_azimuth);
	displayParameters->SetElevation(camElevation);
	displayParameters->SetZoomFactor(this->m_cameraSettings->m_zoomFactor);

	displayParameters->SetResetCamera(m_cameraSettings->m_autoReset);
	displayParameters->SetUseAutoReset(m_cameraSettings->m_useAutoResetVtk);
}

void vtkWrapper::LoadCameraSettings()
{
	vtkCamera* cam = ren1->GetActiveCamera();

	// Compute current cam elevation to apply delta
	const double * camPos = cam->GetPosition();
	const double * camFocalPoint = cam->GetFocalPoint();

	const double dx = camPos[0] - camFocalPoint[0];
	const double dy = camPos[1] - camFocalPoint[1];
	const double dz = camPos[2] - camFocalPoint[2];

	const double camElevation = atan2(dy, sqrt(dx * dx + dz * dz));

	// Load camera settings
	auto displayParameters = DisplayParameter::getInstance();

	this->m_cameraSettings->m_azimuth = displayParameters->GetAzimuth();
	this->m_cameraSettings->m_elevation = (displayParameters->GetElevation() - camElevation) * 180 / PI;
	this->m_cameraSettings->m_zoomFactor = displayParameters->GetZoomFactor();

	this->m_cameraSettings->m_autoReset = DisplayParameter::getInstance()->GetResetCamera();
	this->m_cameraSettings->m_useAutoResetVtk = DisplayParameter::getInstance()->GetUseAutoReset();

	ApplyCameraSettings(m_cameraSettings, false);

	// Reset elevation
	this->m_cameraSettings->m_elevation = 0.0;
}

void vtkWrapper::ApplyCameraSettings(CameraSettings ^aCamSet, bool fromCompass)
{
	*m_cameraSettings = *aCamSet;

	vtkCamera* cam = ren1->GetActiveCamera();	

	if (m_cameraSettings->m_AskForReset == true)
	{
		cam->SetPosition(0, 0, 1);
		cam->SetFocalPoint(0, 0, 0);
		cam->SetViewUp(0, 1, 0);
		ren1->ResetCamera();

		m_cameraSettings->m_azimuth = 0;
		m_cameraSettings->m_elevation = 0;
		m_cameraSettings->m_zoomFactor = 1.0;

		m_cameraSettings->m_AskForReset = false;
	}
	else 
	{
		cam->SetViewUp(0, 1, 0);

		const double * camPos = cam->GetPosition();
		const double * camFocalPoint = cam->GetFocalPoint();
				
		const double dx = camPos[0] - camFocalPoint[0];
		const double dy = camPos[1] - camFocalPoint[1];
		const double dz = camPos[2] - camFocalPoint[2];

		const double camAzimuth = atan2(dz, dx);
		double compassAzimuth = fmod((m_cameraSettings->m_azimuth - 0.25) * PI * 2, 2 * PI) - PI;
		if (compassAzimuth <= -PI) {
			compassAzimuth += 2 * PI;
		}

		if (compassAzimuth > PI) {
			compassAzimuth -= 2 * PI;
		}
		cam->Azimuth((camAzimuth - compassAzimuth) * 180 / PI);		
		cam->Elevation(m_cameraSettings->m_elevation);
		cam->SetParallelScale(m_cameraSettings->m_defaultParallelScale / m_cameraSettings->m_zoomFactor);
	}

	if (!fromCompass)
	{
		UpdateCompass();
	}

	this->Render();
} 

double vtkWrapper::ComputeCameraHeading()
{
	double camHeading = 0;

	// r�cup�ration du dernier pingFan affich� sur la vue 3D pour r�cup�ration du cap
	// et mise � jour de la boussole
	PingFan3D * pFan = NULL;
	unsigned int nbSounder = m_pSounderMgr->GetNbSounder();
	for (unsigned int iSound = 0; iSound < nbSounder && !pFan; iSound++)
	{
		vtkSounder* pSound = m_pSounderMgr->GetSounderIdx(iSound);
		if (pSound->getVolumeBeam()->isVisible() || pSound->getVolumeShoal()->isVisible()
			|| pSound->getVolumeBeam()->isVisible())
		{
			MergedEchoList* echoList = pSound->GetLastBuildingVolume();
			if (echoList)
			{
				pFan = echoList->GetLastPingFan();
			}
		}
	}

	// si on a trouv� un fan affich� ....
	if (pFan)
	{
		// cap r�el du dernier ping
		double heading = pFan->getPingFan()->GetNavAttributesRef(pFan->getPingFan()->GetFirstChannelId())->m_headingRad;

		// vecteur direction de la camera
		BaseMathLib::Vector3D cameraDirection(0, 0, 0);
		double z;
		ren1->GetActiveCamera()->GetDirectionOfProjection(cameraDirection.x, z, cameraDirection.z);
		cameraDirection.normalise();

		// vecteur direction du navire pour le dernier ping
		BaseMathLib::Vector3D shipDirection(0, 0, 0);
		shipDirection.y = pFan->m_transducerBeamOpening.m_FrontLines.m_LowerAngleBeam.m_PointDown.y - pFan->m_transducerBeamOpening.m_BackLines.m_LowerAngleBeam.m_PointDown.y;
		shipDirection.x = pFan->m_transducerBeamOpening.m_FrontLines.m_LowerAngleBeam.m_PointDown.x - pFan->m_transducerBeamOpening.m_BackLines.m_LowerAngleBeam.m_PointDown.x;
		shipDirection.normalise();
		BaseMathLib::Vector3D realShipDirection = BaseMathLib::Vector3D::ToViewOpengl(shipDirection);

		// finalement, le cap de la camera vaut l'angle entre nos deux vecteurs + le cap 
		camHeading = RAD_TO_DEG(Math::Acos(realShipDirection.x*cameraDirection.x + realShipDirection.z*cameraDirection.z));
		double sinus = realShipDirection.x*cameraDirection.z - realShipDirection.z*cameraDirection.x;
		if (sinus < 0)
		{
			camHeading = -camHeading;
		}
		camHeading = camHeading + RAD_TO_DEG(heading);
		camHeading = fmod(camHeading, 360.0) / 360.0;
	}

	return camHeading;
}

void vtkWrapper::PreRender()
{
	//double cameraHeading = ComputeCameraHeading();
	//m_pCompassWidget->SetHeading(cameraHeading);
}

void vtkWrapper::Render()
{
	GetRendererLock()->Lock();

	if (renWin != NULL)
	{
		this->PreRender();
		renWin->Render();
	}

	GetRendererLock()->Unlock();

	if (renWin != NULL)
	{
		AviRendering();
	}
}

void vtkWrapper::Zoom(double a)
{
	ren1->GetActiveCamera()->Zoom(a);
	this->m_cameraSettings->m_zoomFactor = a;
}

void vtkWrapper::ResetCamera()
{
	double bounds[6];
	//	

	if (this->m_cameraSettings->m_useAutoResetVtk)
	{
		// OTK - 26/02/2009 - Optimisation : on d�sactive l'aglorithme de tri des polys
		// pour le reset camera (pas besoin et tr�s couteux).
		// ACN - 29/05/2017 - D�sactivation de l'optimisation suite au passage vs2015 / vtk 7.1
		// Comportements �tranges : pas d'affichage si Opacity Transparency Factor est coch�
		/*std::vector<unsigned int> soundersToSort;
		for(unsigned int i = 0; i < m_pSounderMgr->GetNbSounder(); i++)
		{
			if(m_pSounderMgr->GetSounderIdx(i)->isUseSort())
			{
				soundersToSort.push_back(i);
				m_pSounderMgr->GetSounderIdx(i)->SetSort(false, ren1->GetActiveCamera());
			}
		}*/

		ren1->ResetCamera();

		this->m_cameraSettings->m_defaultParallelScale = ren1->GetActiveCamera()->GetParallelScale();

		ren1->GetActiveCamera()->Zoom(this->m_cameraSettings->m_zoomFactor);

		// r�activation des tris d�sactiv�s
		/*for(unsigned int i = 0; i < soundersToSort.size(); i++)
		{
			m_pSounderMgr->GetSounderIdx(soundersToSort[i])->SetSort(true, ren1->GetActiveCamera());
		}*/
		return;
	}

	bounds[0] = this->m_cameraSettings->m_boundVolA.x;
	bounds[1] = this->m_cameraSettings->m_boundVolB.x;
	bounds[2] = this->m_cameraSettings->m_boundVolA.y;
	bounds[3] = this->m_cameraSettings->m_boundVolB.y;
	bounds[4] = this->m_cameraSettings->m_boundVolA.z;
	bounds[5] = this->m_cameraSettings->m_boundVolB.z;

	ren1->ResetCamera(bounds);
	bounds[0] = this->m_cameraSettings->m_wholeBoundVolA.x;
	bounds[1] = this->m_cameraSettings->m_wholeBoundVolB.x;
	bounds[2] = this->m_cameraSettings->m_wholeBoundVolA.y;
	bounds[3] = this->m_cameraSettings->m_wholeBoundVolB.y;
	bounds[4] = this->m_cameraSettings->m_wholeBoundVolA.z;
	bounds[5] = this->m_cameraSettings->m_wholeBoundVolB.z;

	// OTK - 08/07/2009 - on ne passe pas de bounds pour ce pas couper le planewidget
	if (m_pPlaneWidget)
	{
		ren1->ResetCameraClippingRange();
	}
	else
	{
		ren1->ResetCameraClippingRange(bounds);
	}

	this->m_cameraSettings->m_defaultParallelScale = ren1->GetActiveCamera()->GetParallelScale();
	ren1->GetActiveCamera()->Zoom(this->m_cameraSettings->m_zoomFactor);
}

void vtkWrapper::Resize(int height, int width)
{
	renWin->SetSize(width, height);
}

void vtkWrapper::UpdateParameter()
{
	RecomputePalette();
}

MergedEchoList *vtkWrapper::GetBuildingVolume(unsigned long sounderId)
{
	vtkSounder *pSound = this->m_pSounderMgr->GetSounder(sounderId);
	if (pSound)
		return pSound->GetBuildingVolume();
	return NULL;
}

void vtkWrapper::SetBuildingVolume(unsigned long sounderId)
{
	vtkSounder *pSound = this->m_pSounderMgr->GetSounder(sounderId);
	if (pSound)
		pSound->SetBuildingVolume(m_cameraSettings);
}

void vtkWrapper::RecomputePalette()
{
	auto displayParameter = DisplayParameter::getInstance();
	auto echoPalette = displayParameter->GetCurrent3DEchoPalette();

	for (unsigned int i = 0; i < m_pSounderMgr->GetNbSounder(); i++)
	{
		m_pSounderMgr->GetSounderIdx(i)->SetEchoPalette(echoPalette);
	}
}

void vtkWrapper::SetVolumeScale(double x, double y, double z)
{
	for (unsigned int i = 0; i < m_pSounderMgr->GetNbSounder(); i++)
	{
		vtkSounder *pSound = m_pSounderMgr->GetSounderIdx(i);
		if (pSound)
			pSound->SetVolumeScale(x, y, z);
	}
}

void vtkWrapper::StartAviRendering(System::String ^aFileName)
{
	if (m_bisAviRendering)
		StopAviRendering();
	System::IntPtr ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(aFileName);
	const char* strPath = static_cast<const char*>(ip.ToPointer());

	m_pAviRenderer = vtkAVIWriter::New();
	// FE M3D-247 - Modification de la vitesse de capture du fichier AVI
	m_pAviRenderer->SetRate(100);
	m_pW2i = vtkWindowToImageFilter::New();
	m_pW2i->SetInput(renWin);//the render window
	m_pAviRenderer->SetFileName(strPath);

	m_pAviRenderer->SetInputConnection(m_pW2i->GetOutputPort());

	System::Runtime::InteropServices::Marshal::FreeHGlobal(ip);

	m_pAviRenderer->Start();

	m_bisAviRendering = true;
}

void vtkWrapper::StopAviRendering()
{
	if (m_bisAviRendering)
	{
		m_pAviRenderer->End();
		//clean
		m_pAviRenderer->Delete();
		m_pAviRenderer = NULL;
		m_pW2i->Delete();
		m_pW2i = NULL;
		m_bisAviRendering = false;
	}
}

void vtkWrapper::AviModified()
{
	if (m_bisAviRendering)
	{
		m_pW2i->Modified();
	}
}

void vtkWrapper::AviRendering()
{
	if (m_bisAviRendering)
	{
		//m_pW2i->Modified();
		AviModified();
		m_pAviRenderer->Write();
	}
}

void vtkWrapper::SaveAvi(System::String ^aFileName)
{
	StartAviRendering(aFileName);

	vtkCamera *cam = ren1->GetActiveCamera();
	BaseMathLib::Vector3D center;

	const unsigned num = 180;
	for (int i = 0; i < num; i++)
	{
		cam->Azimuth(2 * 360.0 / num);
		vtkLightCollection* lc = ren1->GetLights();
		lc->InitTraversal();
		vtkLight* light = lc->GetNextItem();
		light->SetPosition(cam->GetPosition());
		light->SetFocalPoint(cam->GetFocalPoint());

		Render();
	}
	StopAviRendering();
}

void UpdatePlaneEvents(vtkObject* object, unsigned long
	Event, void* clientdata, void*
	calldata)
{
	planeContext* pc = reinterpret_cast<planeContext*>(clientdata);
	pc->m_pPlaneWidget->GetPlane(pc->m_pPlane);
	for (size_t i = 0; i < pc->m_Sounders.size(); i++)
	{
		pc->m_Sounders[i]->Apply3DCut(pc->m_pRenderer, pc->m_pPlane);
	}
}

// OTK - 07/07/2009 - ajout du plan de coupe
void vtkWrapper::AddPlaneWidget()
{
	// on commence par regarder si un sondeur est coch� dans la liste : si non, on ne fait rien
	unsigned int sounderNb = m_pSounderMgr->GetNbSounder();
	unsigned int nbVisibleSounder = 0;
	for (unsigned int i = 0; i < sounderNb; i++)
	{
		if (m_pSounderMgr->GetSounderIdx(i)->getVolumeBeam()->isVisible())
		{
			nbVisibleSounder++;
		}
	}

	if (nbVisibleSounder == 0)
		return;

	if (m_pPlaneWidget == NULL)
	{
		m_pPlaneWidget = vtkPlaneWidget::New();
		// on fait un setinput pour que le widget soit centr� sur les donn�es affich�es. on fait ceci
		// � l'aide du premier sondeur visible
		for (unsigned int i = 0; i < sounderNb; i++)
		{
			vtkSounder* pSounder = m_pSounderMgr->GetSounderIdx(i);
			if (pSounder->getVolumeBeam()->isVisible())
			{
				m_pPlaneWidget->SetInputConnection(pSounder->GetLastBuildingVolume()->GetEchoPolyData()->GetOutputPort());
				break;
			}
		}
		m_pPlaneWidget->SetInteractor(renWin->GetInteractor());
		m_pPlaneWidget->AddObserver(vtkCommand::StartInteractionEvent, m_vtkUpdatePlaneEventCBC);
		m_pPlaneWidget->AddObserver(vtkCommand::InteractionEvent, m_vtkUpdatePlaneEventCBC);
		m_pPlaneWidget->SetNormalToZAxis(1);
		m_pPlaneWidget->SetRepresentationToOutline();
		m_pPlaneWidget->SetResolution(50);
		m_pPlaneWidget->SetPlaceFactor(1);
		m_pPlaneWidget->PlaceWidget();
	}

	m_pPlaneWidget->On();
	if (!m_pPlane)
	{
		m_pPlane = vtkPlane::New();
	}
	m_pPlaneWidget->GetPlane(m_pPlane);
	// application du plan de clipping aux �chos
	for (unsigned int i = 0; i < sounderNb; i++)
	{
		vtkSounder* pSounder = m_pSounderMgr->GetSounderIdx(i);
		if (pSounder->getVolumeBeam()->isVisible())
		{
			pSounder->Apply3DCut(ren1, m_pPlane);
		}
	}

	if (m_pPlaneContext == NULL)
	{
		m_pPlaneContext = new planeContext;
	}
	m_pPlaneContext->m_pPlaneWidget = m_pPlaneWidget;
	m_pPlaneContext->m_pRenderer = ren1;
	m_pPlaneContext->m_Sounders.clear();
	for (unsigned int i = 0; i < sounderNb; i++)
	{
		vtkSounder* pSounder = m_pSounderMgr->GetSounderIdx(i);
		m_pPlaneContext->m_Sounders.push_back(pSounder);
	}
	m_pPlaneContext->m_pPlane = m_pPlane;


	m_vtkUpdatePlaneEventCBC->SetCallback(UpdatePlaneEvents);
	m_vtkUpdatePlaneEventCBC->SetClientData(m_pPlaneContext);

	ren1->ResetCameraClippingRange();

	renWin->Render();
}

void vtkWrapper::RemovePlaneWidget()
{
	// d�sactivation de la coupe pour chaque sondeur
	unsigned int sounderNb = m_pSounderMgr->GetNbSounder();
	for (unsigned int i = 0; i < sounderNb; i++)
	{
		m_pSounderMgr->GetSounderIdx(i)->Undo3DCut(ren1);
	}

	// nettoyage des objets
	if (m_pPlane)
	{
		m_pPlane->Delete();
		m_pPlane = NULL;
	}
	if (m_pPlaneWidget)
	{
		m_pPlaneWidget->Off();
		m_pPlaneWidget->Delete();
		m_pPlaneWidget = NULL;
	}

	if (m_pPlaneContext)
	{
		delete m_pPlaneContext;
		m_pPlaneContext = NULL;
	}
}