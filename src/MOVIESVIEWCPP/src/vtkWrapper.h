#pragma once
#include "vtk.h"
#include "MergedEchoList.h"
#include "ColorPalette.h"
#include "vtkPlaneWidget.h"
#include "CameraSettings.h"
#include "BaseMathLib/Box.h"
#include "vtkSounderMgr.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "vtkMovCompassWidget.h"

#pragma managed
class vtkClipPolyData;
class vtkVolumeSeaFloor;

class vtkAVIWriter;
class vtkMPEG2Writer;
class vtkWindowToImageFilter;
class vtkActor2D;

#include <vector>

struct planeContext {
	vtkRenderer * m_pRenderer;
	vtkPlaneWidget * m_pPlaneWidget;
	vtkPlane * m_pPlane;
	std::vector<vtkSounder*> m_Sounders;
};

static void UpdatePlaneEvents(vtkObject* object, unsigned long
	Event, void* clientdata, void*
	calldata);

static void ProcessMouseEvents(vtkObject* object, unsigned long
	Event, void* clientdata, void*
	calldata);

public delegate System::Void UpdateCompassDelegate(void);

public delegate System::Void ContextMenuDelegate(void);

public ref class vtkWrapper
{
public:
	vtkWrapper(REFRESH3D_CALLBACK refreshCB, ContextMenuDelegate^ contextMenuDelegate);
	virtual ~vtkWrapper(void);
	vtkRenderWindow *renWin;
	vtkRenderer *ren1;

	void InitWindow(System::Windows::Forms::PictureBox ^m_WindParent);
	void Render();
	void ResetCamera();
	void Resize(int height, int width);

	MergedEchoList *GetBuildingVolume(unsigned long sounderId);
	void SetBuildingVolume(unsigned long sounderId);
	void SetVolumeScale(double x, double y, double z);

	void RecomputePalette();

	void SetSort(bool a, unsigned long sounderId);
	bool isUseSort(unsigned long sounderId);

	void SetToWireFrame(bool bWireFrame);

	bool isFrozen() { return m_bFreeze; }
	void Freeze(bool a) { m_bFreeze = a; }

	void Zoom(double ZoomFactor);

	//	void SetClipPlanBox(BaseMathLib::Vector3D minPt, BaseMathLib::Vector3D maxPt);
	//	void SetUseClipPlanBox(bool a_bUSe);
	void setBackGroundColor(double r, double g, double b);


	void ApplyCameraSettings(CameraSettings ^aCamSet, bool fromCompass);
	CameraSettings GetCameraSettings() { return *m_cameraSettings; }

	/// Sauvegarde les param�tres de la camera dans les settings
	void SaveCameraSettings();

	/// Charge les param�tres de la camera depuis les settings
	void LoadCameraSettings();

	// OTK - 07/07/2009 - ajout du plan de coupe
	void AddPlaneWidget();
	void RemovePlaneWidget();

	void SaveAvi(System::String ^fileName);

	void StartAviRendering(System::String ^aFileName);
	void StopAviRendering();
	void AviRendering();
	void AviModified();
	bool IsAviRendering() { return m_bisAviRendering; };

	void ClearAllSounder();
	void AddSounder(unsigned long sounderId);


	void SetStatText(System::String ^text);
	vtkSounderMgr* GetSounderMgr() { return m_pSounderMgr; };


	void UpdateParameter();

	//FRE - 29/09/2009 renderer lock
	CRecursiveMutex* GetRendererLock() { return  m_pLock; };

private:

	void RefreshAndSortActorList();

	void UpdateFromCompass();

	void UpdateCompass();

	void OnContextMenu();

	// op�rations � effectuer avant la phase de rendu VTK
	void PreRender();

	// calcul du cap de la camera
	double ComputeCameraHeading();

	vtkSounderMgr *m_pSounderMgr;

	// Widget pour le plan de coupe 3D
	vtkPlaneWidget *m_pPlaneWidget;
	// Widget boussole
	vtkMovCompassWidget *m_pCompassWidget;
	// plan associ� au widget
	vtkPlane * m_pPlane;
	// callback associ� aux actions sur le widget (manipulation du plan de coupe a la souris...)
	vtkCallbackCommand* m_vtkUpdatePlaneEventCBC;
	// contexte � passer au callback ci-dessus
	planeContext *m_pPlaneContext;
	// callback associ� aux actions sur le widget boussole (zoom, rotation, elevation)
	vtkCallbackCommand* m_vtkCompassWidgetCBC;
	UpdateCompassDelegate^ m_UpdateCompassDelegate;

	// callback pour intercepter les clics droits sur la visu VTK pour affichage du ContextMenu 
	vtkCallbackCommand* m_vtkMouseEventCBC;

	// d�l�g� pour le fonctionnement du menu sur clic droit
	ContextMenuDelegate^ m_ContextMenuDelegate;
	ContextMenuDelegate^ m_ParentContextMenuDelegate;

	bool m_bFreeze;
	CameraSettings^ m_cameraSettings;
	// camera settings

	System::Windows::Forms::PictureBox ^m_WindParent;

	bool m_bisAviRendering;
	vtkAVIWriter *m_pAviRenderer;
	vtkWindowToImageFilter *m_pW2i;

	vtkActor2D *m_pTextActor;

	//FRE - 29/09/2009 renderer lock
	CRecursiveMutex* m_pLock;
};
