#pragma once

#include "ModuleManagerExport.h"
#include "M3DKernel/config/MovConfig.h"
#include "Calibration/CalibrationModule.h"

#include <vector>

class ProcessModule;
class FilterMgr;
class CompensationModule;
class BottomDetectionModule;
class EchoIntegrationModule;
class ShoalExtractionModule;
class ReaderCtrl;
class MovNetwork;
class EISupervisedModule;
class NoiseLevelModule;
class TSAnalysisModule;
class OutputManagerModule;

class MODULEMANAGER_API CModuleManager
{
public:
	static CModuleManager* getInstance();
	static void FreeMemory();
	virtual ~CModuleManager();

	unsigned int GetModuleCount();
	ProcessModule* GetModule(unsigned int idx);

	CompensationModule * GetCompensationModule();
	CalibrationModule* GetCalibrationModule();
	BottomDetectionModule * GetBottomDetectionModule();
	FilterMgr *GetFilterModule();
	EchoIntegrationModule	*GetEchoIntegrationModule();
	OutputManagerModule *GetOutputManagerModule();
	MovNetwork * GetMovNetwork();
	TSAnalysisModule * GetTSAnalysisModule();	
	EISupervisedModule * GetEISupervisedModule();
	NoiseLevelModule * GetNoiseLevelModule();
	ShoalExtractionModule	*GetShoalExtractionModule();

	// IPSIS - OTK - indique si au moins un module de traitement est actif
	// et renseigne les noms des modules correspondant.
	bool IsTreatmentEnabled(std::string & moduleNames);

	void SaveConfiguration(const std::vector<BaseKernel::ParameterModule*>& additionalModules);
	bool LoadConfiguration(const std::vector<BaseKernel::ParameterModule*>& additionalModules);
	void ChangeConfigurationCurrentDirectory(std::string path, std::string prefix);
	void GetConfigurationCurrentDirectory(std::string& path, std::string& prefix);

	static CModuleManager* m_pModuleMgr;

private:
	CModuleManager(void);
	void CreateModuleList();
	void EraseModuleList();

	std::vector<ProcessModule*> m_algList;

	int m_CalibrationIdx;
	int m_CompensationIdx;
	int m_FilteringIdx;
	int m_EchoIntegrationIdx;
	int m_OutputManagerModuleIdx;
	int m_ShoalExtractionIdx;
	int m_BottomDetectionModuleIdx;
	int m_EISupervisedIdx;
	int m_NoiseLevelModuleIdx;
	int m_TSAnalysisIdx;

	/// <summary>
	/// Methode utilitaire pour obtenir un module typé
	/// </summary>
	/// <typeparam name="T">Type du module</typeparam>
	/// <param name="idx">Index du module</param>
	/// <returns>Le module ou nullptr si l'index est -1</returns>
	template<typename T>
	T* GetModule(int idx);

	/// <summary>
	/// Méthode utilitire pour la création d'un module
	/// </summary>
	/// <typeparam name="T">Type de module</typeparam>
	/// <param name="idx">Index du module sauvegardé</param>
	/// <returns>Le module</returns>
	template<typename T>
	T* CreateModule(int & idx);

	BaseKernel::MovConfigDirectory * m_pMovConfig;

	MovNetwork * m_pMovNetwork;
};
