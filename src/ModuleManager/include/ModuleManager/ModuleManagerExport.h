// Le bloc ifdef suivant est la fa�on standard de cr�er des macros qui facilitent l'exportation 
// � partir d'une DLL. Tous les fichiers contenus dans cette DLL sont compil�s avec le symbole MODULEMANAGER_EXPORTS
// d�fini sur la ligne de commande. Ce symbole ne doit pas �tre d�fini dans les projets
// qui utilisent cette DLL. De cette mani�re, les autres projets dont les fichiers sources comprennent ce fichier consid�rent les fonctions 
// MODULEMANAGER_API comme �tant import�es � partir d'une DLL, tandis que cette DLL consid�re les symboles
// d�finis avec cette macro comme �tant export�s.
#ifdef WIN32
#ifdef MODULEMANAGER_EXPORTS
#define MODULEMANAGER_API __declspec(dllexport)
#else
#define MODULEMANAGER_API __declspec(dllimport)
#endif
#else
#define MODULEMANAGER_API
#endif
