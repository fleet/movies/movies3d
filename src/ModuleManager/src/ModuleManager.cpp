// ModuleManager.cpp�: d�finit le point d'entr�e pour l'application DLL.
//

#include "ModuleManager/ModuleManager.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/algorithm/base/EchoAlgorithmRoot.h"
#include "M3DKernel/config/MovConfig.h"
#include "M3DKernel/datascheme/MovESUMgr.h"
#include "M3DKernel/utils/carto/CartoTools.h"


#include "Filtering/FilterMgr.h"

#include "Calibration/CalibrationModule.h"

#include "Compensation/CompensationModule.h"
#include "Compensation/CompensationParameter.h"

#include "BottomDetection/BottomDetectionModule.h"
#include "BottomDetection/BottomDetectionParameter.h"

#include "EchoIntegration/EchoIntegrationModule.h"
#include "EISupervised/EISupervisedModule.h"
#include "NoiseLevel/NoiseLevelModule.h"
#include "ShoalExtraction/ShoalExtractionModule.h"
#include "TSAnalysis/TSAnalysisModule.h"
#include "OutputManager/OutputManagerModule.h"
#include "MovNetwork/MovNetwork.h"
#include "Reader/ReaderCtrl.h"


using namespace BaseKernel;

namespace
{
	constexpr const char * LoggerName = "ModuleManager.ModuleManager";
}

#ifdef _MANAGED
#pragma managed(push, off)
#endif

#ifdef WIN32
BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		CModuleManager::FreeMemory();
		break;
	}
	return TRUE;
}
#else
__attribute__((destructor)) void dllUnload()
{
    CModuleManager::FreeMemory();
}
#endif

#ifdef _MANAGED
#pragma managed(pop)
#endif

CModuleManager* CModuleManager::m_pModuleMgr = nullptr;

CModuleManager* CModuleManager::getInstance()
{
	if (!m_pModuleMgr)
	{
		m_pModuleMgr = new CModuleManager();
	}
	return m_pModuleMgr;
}

void CModuleManager::FreeMemory()
{
	if (m_pModuleMgr)
	{
		delete m_pModuleMgr;
		m_pModuleMgr = nullptr;
	}
}


// Il s'agit du constructeur d'une classe qui a �t� export�e.
// consultez ModuleManager.h pour la d�finition de la classe
CModuleManager::CModuleManager()
{
	m_TSAnalysisIdx =
		m_EISupervisedIdx =
		m_CompensationIdx =
		m_CalibrationIdx = 
		m_BottomDetectionModuleIdx =
		m_FilteringIdx =
		m_EchoIntegrationIdx =
		m_OutputManagerModuleIdx =
		m_ShoalExtractionIdx = -1;
	M3DKernel::GetInstance();

	m_pMovConfig = new MovConfigDirectory();
	m_pMovNetwork = MovNetwork::getInstance();

	CreateModuleList();
}

CModuleManager::~CModuleManager()
{
	EraseModuleList();

	delete m_pMovConfig;
	MovNetwork::releaseInstance();
}

template<typename T>
T* CModuleManager::CreateModule(int & idx)
{
	auto m = T::Create();	
	MovRef(m);
	idx = (int)m_algList.size();
	m_algList.push_back(m);
	return m;
}

void CModuleManager::CreateModuleList()
{
	M3DKernel *pKern = M3DKernel::GetInstance();

	// BLOC PRE-TRAITEMENTS
	auto pCalibration = CreateModule<CalibrationModule>(m_CalibrationIdx);	
	pCalibration->AddInput(pKern->getEchoAlgorithmRoot());

	auto pCompensation = CreateModule<CompensationModule>(m_CompensationIdx);
	pCompensation->setCalibrationModule(pCalibration);
	pCompensation->AddInput(pCalibration);

	auto pBottomDetection = CreateModule<BottomDetectionModule>(m_BottomDetectionModuleIdx);
	pBottomDetection->AddInput(pCompensation);

	auto *pFilterMgr = CreateModule<FilterMgr>(m_FilteringIdx);
	pFilterMgr->AddInput(pBottomDetection);
	pBottomDetection->setFilterMgr(pFilterMgr);

	// BLOC DETECTION
	auto pAlgEI = CreateModule<EchoIntegrationModule>(m_EchoIntegrationIdx);
	pAlgEI->setCalibrationModule(pCalibration);
	pAlgEI->AddInput(pFilterMgr);

	auto pAlgExtract = CreateModule<ShoalExtractionModule>(m_ShoalExtractionIdx);
	pAlgExtract->AddInput(pFilterMgr);

	// ajout du module d'echointegration supervisée
	auto pEISupervised = CreateModule<EISupervisedModule>(m_EISupervisedIdx);
	pEISupervised->SetEchoIntegrationModule(pAlgEI);
	pEISupervised->AddInput(pFilterMgr);

	// ajout du module d'analyse TS
	auto pTSAnalysis = CreateModule<TSAnalysisModule>(m_TSAnalysisIdx);
	pTSAnalysis->setCalibrationModule(pCalibration);
	pTSAnalysis->AddInput(pFilterMgr);
	
	// ajout du module d'analyse NoiseLevel
	auto pNoiseLevel = CreateModule<NoiseLevelModule>(m_NoiseLevelModuleIdx);
	pNoiseLevel->AddInput(pFilterMgr);

#pragma message("___________________________________________________________________________")
#pragma message("|Ajouter les autres modules dont les sorties doivent �tre s�rialis�es ici |")
#pragma message("|_________________________________________________________________________|")

	// Ajout du Module OutputManager
	// (gestion des sorties des modules)
	auto pAlgOutput = CreateModule<OutputManagerModule>(m_OutputManagerModuleIdx);
	// on lie le module des sorties avec les modules producteurs de sorties
	pAlgOutput->AddInput(pAlgEI);
	pAlgOutput->AddInput(pAlgExtract);
	pAlgEI->GetOutputMgrConsumer();
	pAlgExtract->GetOutputMgrConsumer();
}

void CModuleManager::EraseModuleList()
{
	for (unsigned int i = 0; i < m_algList.size(); i++)
	{
		MovUnRefDelete(m_algList[i]);
	}
	m_algList.clear();
}

template<typename T>
T* CModuleManager::GetModule(int idx)
{
	return (idx != -1) ? (T*)m_algList[idx] : nullptr;
}

ProcessModule* CModuleManager::GetModule(unsigned int idx)
{
	return m_algList[idx];
}

unsigned int CModuleManager::GetModuleCount()
{
	return (unsigned int)m_algList.size();
}

CompensationModule * CModuleManager::GetCompensationModule()
{
	return GetModule<CompensationModule>(m_CompensationIdx);
}

CalibrationModule * CModuleManager::GetCalibrationModule()
{
	return GetModule<CalibrationModule>(m_CalibrationIdx);
}

BottomDetectionModule * CModuleManager::GetBottomDetectionModule()
{
	return GetModule<BottomDetectionModule>(m_BottomDetectionModuleIdx);
}

FilterMgr *CModuleManager::GetFilterModule()
{
	return GetModule<FilterMgr>(m_FilteringIdx);
}

EchoIntegrationModule *CModuleManager::GetEchoIntegrationModule()
{
	return GetModule<EchoIntegrationModule>(m_EchoIntegrationIdx);
}

EISupervisedModule * CModuleManager::GetEISupervisedModule()
{
	return GetModule<EISupervisedModule>(m_EISupervisedIdx);
}

NoiseLevelModule * CModuleManager::GetNoiseLevelModule()
{
	return GetModule<NoiseLevelModule>(m_NoiseLevelModuleIdx);
}

TSAnalysisModule * CModuleManager::GetTSAnalysisModule()
{
	return GetModule<TSAnalysisModule>(m_TSAnalysisIdx);
}

ShoalExtractionModule* CModuleManager::GetShoalExtractionModule()
{
	return GetModule<ShoalExtractionModule>(m_ShoalExtractionIdx);
}

OutputManagerModule* CModuleManager::GetOutputManagerModule()
{
	return GetModule<OutputManagerModule>(m_OutputManagerModuleIdx);
}

// IPSIS - OTK - indique si au moins un module de traitement est actif
// et construit une string �num�rant les traitements en cours.
bool CModuleManager::IsTreatmentEnabled(std::string & moduleNames)
{
	bool result = false;
	if (GetEchoIntegrationModule() != NULL && GetEchoIntegrationModule()->getEnable())
	{
		moduleNames = moduleNames + "  - " + GetEchoIntegrationModule()->getName() + "\n";
		result = true;
	}

	if (GetShoalExtractionModule() != NULL && GetShoalExtractionModule()->getEnable())
	{
		moduleNames = moduleNames + "  - " + GetShoalExtractionModule()->getName() + "\n";
		result = true;
	}

#pragma message("__________________________________________________________________________________")
#pragma message("|Ajouter les nouveaux modules de traitements ici afin d'interdire la modification  |")
#pragma message("|         des param�tres ESU si un traitement utilisant les ESU est actif          |")
#pragma message("|__________________________________________________________________________________|")

	return result;
}

// IPSIS - OTK - acc�s au module de diffusion r�seau pour mise � jour de la configuration
MovNetwork * CModuleManager::GetMovNetwork()
{
	return m_pMovNetwork;
}

// IPSIS - OTK - Ajout de la gestion des fichiers de configuration
void CModuleManager::SaveConfiguration(const std::vector<BaseKernel::ParameterModule*>& additionalModules)
{
	std::string path, prefix;
	GetConfigurationCurrentDirectory(path, prefix);
	std::string message = "Saving configuration folder (" + path + "\\" + prefix + ")";
	M3D_LOG_INFO(LoggerName, message.c_str());

	// Module noyau
	M3DKernel::GetInstance()->GetRefKernelParameter().Serialize(m_pMovConfig);
	M3DKernel::GetInstance()->GetSpectralAnalysisParameters().Serialize(m_pMovConfig);

	// Module de lecture
	ReaderCtrl::getInstance()->GetReaderParameter().Serialize(m_pMovConfig);

	// Module d'EchoIntegration
	GetEchoIntegrationModule()->GetEchoIntegrationParameter().Serialize(m_pMovConfig);

	// Module d'EchoIntegration supervis�e
	GetEISupervisedModule()->GetEISupervisedParameter().Serialize(m_pMovConfig);

	// Module d'Extraction des bancs
	GetShoalExtractionModule()->GetShoalExtractionParameter().Serialize(m_pMovConfig);

	// Module de compensation
	GetCompensationModule()->GetCompensationParameter().Serialize(m_pMovConfig);

	// Module de calibration
	GetCalibrationModule()->Serialize(m_pMovConfig);

	// Module de d�tection du fond
	GetBottomDetectionModule()->GetBottomDetectionParameter().Serialize(m_pMovConfig);

	// Module de Gestion des ESU
	M3DKernel::GetInstance()->getMovESUManager()->GetRefParameter().Serialize(m_pMovConfig);

	// Module de filtrage
	GetFilterModule()->Serialize(m_pMovConfig);

	// Module de gestion des sorties
	GetOutputManagerModule()->GetMvNetParameter().Serialize(m_pMovConfig);

	// Module TS
	GetTSAnalysisModule()->GetTSAnalysisParameter().Serialize(m_pMovConfig);

	// Module de niveau de bruit
	GetNoiseLevelModule()->GetNoiseLevelParameters().Serialize(m_pMovConfig);

	// Eventuellement, parametres d'affichage (seulement en mode IHM)
	for (auto module : additionalModules)
	{
		module->Serialize(m_pMovConfig);
	}
}

bool CModuleManager::LoadConfiguration(const std::vector<BaseKernel::ParameterModule*>& additionalModules)
{
	bool result = true;

	M3DKernel::GetInstance()->Lock();
	std::string path, prefix;
	GetConfigurationCurrentDirectory(path, prefix);
	std::string message = "Loading configuration folder (" + path + "\\" + prefix + ")";
	M3D_LOG_INFO(LoggerName, message.c_str());

	// Module noyau
	M3DKernel::GetInstance()->GetRefKernelParameter().DeSerialize(m_pMovConfig);
	// mise � jour de l'outil de projection en fonction des param�tres charg�s
	CCartoTools::Initialize(M3DKernel::GetInstance()->GetRefKernelParameter());
	M3DKernel::GetInstance()->GetSpectralAnalysisParameters().DeSerialize(m_pMovConfig);

	// Module de lecture
	ReaderParameter localReaderParams;
	localReaderParams.DeSerialize(m_pMovConfig);
	ReaderCtrl::getInstance()->UpdateReaderParameter(localReaderParams);

	// Module d'EchoIntegration
	GetEchoIntegrationModule()->GetEchoIntegrationParameter().DeSerialize(m_pMovConfig);

	// Module d'echointegration supervis�e
	GetEISupervisedModule()->GetEISupervisedParameter().DeSerialize(m_pMovConfig);

	// Module d'extraction de bancs
	GetShoalExtractionModule()->GetShoalExtractionParameter().DeSerialize(m_pMovConfig);

	// Module de compensation
	GetCompensationModule()->GetCompensationParameter().DeSerialize(m_pMovConfig);

	// Module de calibration
	GetCalibrationModule()->DeSerialize(m_pMovConfig);

	// Module de d�tection du fond
	GetBottomDetectionModule()->GetBottomDetectionParameter().DeSerialize(m_pMovConfig);

	// Module de Gestion des ESU
	M3DKernel::GetInstance()->getMovESUManager()->GetRefParameter().DeSerialize(m_pMovConfig);

	// Module de filtrage
	GetFilterModule()->DeSerialize(m_pMovConfig);

	// Module de gestion des sorties
	GetOutputManagerModule()->GetMvNetParameter().DeSerialize(m_pMovConfig);

	// Module TS
	GetTSAnalysisModule()->GetTSAnalysisParameter().DeSerialize(m_pMovConfig);

	// Module de niveau de bruit
	GetNoiseLevelModule()->GetNoiseLevelParameters().DeSerialize(m_pMovConfig);

	// Eventuellement, parametres d'affichage (seulement en mode IHM)
	for (auto module : additionalModules)
	{
		// remarque : pour l'instant seul le r�sultat du chargement de cette config avec le maxDepth nous importe.
		// (pour calcul automatique du maxrange si pas de configuration charg�e)
		result = result && module->DeSerialize(m_pMovConfig);
	}

	// on appelle updateConfiguration des qu'on change la configuration r�seau
	// pour mettre � jour les param�tres r�seau de MovNetwork
	m_pMovNetwork->updateConfiguration(GetOutputManagerModule()->GetMvNetParameter());
	M3DKernel::GetInstance()->Unlock();

	return result;
}

// IPSIS - OTK - gestion des fichiers de configuration
void CModuleManager::ChangeConfigurationCurrentDirectory(std::string path, std::string prefix)
{
	m_pMovConfig->SetPath(path);
	m_pMovConfig->SetPrefix(prefix);
}

// IPSIS - OTK - gestion des fichiers de configuration
void CModuleManager::GetConfigurationCurrentDirectory(std::string& path, std::string& prefix)
{
	path = m_pMovConfig->GetPath();
	prefix = m_pMovConfig->GetPrefix();
}