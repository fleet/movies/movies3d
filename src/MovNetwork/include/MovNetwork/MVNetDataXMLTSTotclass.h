//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLTSTotclass
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for TS totclass echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLTSTotclass_h
#define MvNetDataXMLTSTotclass_h

#include "MovNetwork/MvNetDataXML.h"
#include "MovNetwork/MvNetDataXMLCellset.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLTSTotclass : public CMvNetDataXML
{

private:


public:

	//type de cible se r�f�rent � la statistique
	typedef enum
	{
		kUndefined = -1,
		kTarget = 0,	//Cible individuelle
		kTrack,	//cible composant une trajectoire
		kTSi
	}E_type;

	// frame eitstotclass
	float					m_class;
	unsigned int			m_nb;
	E_type					m_type;

	//Cellule d'analyse
	CMvNetDataXMLCellset	m_cell;

	CMvNetDataXMLTSTotclass();
	~CMvNetDataXMLTSTotclass();
	std::string createXML();

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);
};


#endif
