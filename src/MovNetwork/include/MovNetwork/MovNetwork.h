// -*- C++ -*-
// ****************************************************************************
// Class: MovNetwork
//
// Description: Classe statique servant d'interface avec le reste de
// l'application. Elle contient en particulier les param�tres de broadcast
// r�seau, et contient les 2 classes r�alisant les fonctions de MvNetwork
// de Movies+ (la 3eme, l'emission de trames XML contenant les r�sultats
// des traitements est prise en charge par le OutputManager
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mai 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "MovNetwork/MovNetworkExport.h"
#include "MovNetwork/MvNetConfig.h"
#include "MovNetwork/MvNetDesc.h"
#include "MovNetwork/MvNetParameter.h"


// ***************************************************************************
// Declarations
// ***************************************************************************
class MOVNETWORK_API MovNetwork
{
public:

	// Accesseur � l'instance statique de la classe.
	static MovNetwork* getInstance();
	// Lib�re l'instance de MovNetwork
	static void releaseInstance();

	// *********************************************************************
  // Constructeurs / Destructeur
  // *********************************************************************
	// Constructeur par d�faut
	MovNetwork(void);

	// Destructeur
	virtual ~MovNetwork(void);

	// *********************************************************************
  // Accesseurs
  // *********************************************************************

	// *********************************************************************
  // traitements
  // *********************************************************************
	// lance les threads d'emission de la config et de gestion des requetes
	// de description. Ceci doit �tre appel� des lors qu'un traitement
	// d'EchoIntegration est lanc�
	virtual void startThreads();
	// termine les threads d'emission de la config et de gestion des requetes
	virtual void stopThreads();
	// maj la nouvelle configuration dans les threads fils.
	// a appeler apres chaque modification de la configuration
	virtual void updateConfiguration(MvNetParameter & config);
	// ces fonctions mettent � jour les flags d'activit� des modules d'Echo-
	// int�gration. il faut les appeler sur le OnEnableStateChange des modules
	// concern�s
	virtual void setEILayerEnabled(bool enabled);
	virtual void setEIShoalEnabled(bool enabled);


private:

	// pointeur vers l'instance statique de la classe
	static MovNetwork* m_pMovNetwork;

	// *********************************************************************
  // Donn�es
  // *********************************************************************

	// Thread de la gestion de l'emission de la config a intervalles r�guliers
	CMvNetConfig	* m_pMvNetConfig;

	// Thread de la gestion des requetes de description XML via TCP
	CMvNetDesc * m_pMvNetDesc;

	// indique si les threads sont actifs ou non.
	bool m_Active;
};
