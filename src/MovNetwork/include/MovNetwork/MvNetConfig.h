//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetConfig
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	class to diffuse MOVIES configuration
//
//===================================================================
#ifndef MvNetConfig_h
#define MvNetConfig_h

#include "MovNetwork/MvNetServUDP.h"
#include "MovNetwork/MvNetConfigXML.h"
#include "MovNetwork/MovNetworkExport.h"
#include "MovNetwork/MvNetThread.h"
//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetConfig : public CMvNetThread
{

protected:
	MvNetParameter m_currConfig;
	void initialize();

public:
	CMvNetConfig();
	CMvNetConfig(MvNetParameter& config);
	void updateConfig(MvNetParameter& newConfig);
	~CMvNetConfig();

	void startThread();
	int stopThread();
	void internalThreadFunc();

	bool m_EILayerEnabled;
	bool m_EIShoalEnabled;
	bool m_TSEnabled;
	bool m_continue;
};

#endif
