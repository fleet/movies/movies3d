//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetConfigXML
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	class to create XML string for MOVIES configuration.
//
//===================================================================
#ifndef MvNetConfigXML_h
#define MvNetConfigXML_h

#include <iostream>
#include <string>
#include "MovNetwork/MvNetParameter.h"
#include "MovNetwork/MovNetworkExport.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetConfigXML
{

private:
	MvNetParameter m_currConfig;
	std::string m_buffer;

public:
	CMvNetConfigXML();
	CMvNetConfigXML(MvNetParameter& config);
	~CMvNetConfigXML();

	void setCurrConfig(MvNetParameter& config);
	int readConfigParameters();
	int createXML();
	std::string getBuffer();


	bool m_EILayerEnabled;
	bool m_EIShoalEnabled;
	bool m_TSEnabled;
};


#endif


