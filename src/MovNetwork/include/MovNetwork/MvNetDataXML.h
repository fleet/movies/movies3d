//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXML
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for all echo-integration  XML message (Layer,Shoal,TS,...)
//
//===================================================================
#ifndef MvNetDataXML_h
#define MvNetDataXML_h

// D�pendances
#include <string>
#include "M3DKernel/parameter/TramaDescriptor.h"
#include "M3DKernel/datascheme/DateTime.h"
#include "MovNetwork/MovNetworkExport.h"

#define s_SEPARATOR ";"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXML
{

private:


public:
	static	std::string M_device;

	CMvNetDataXML();
	~CMvNetDataXML();

    static	std::string createXMLHeader(std::string msgname, int major, int minor);
    static	std::string createXMLFooter();

	virtual std::string createXML();


	// IPSIS - OTK - archivage selectif des trames XML
	// renvoie vrai si la config demande d'archiver la trame stringID
	static bool tramaWanted(BaseKernel::TramaDescriptor * cfg, std::string stringID);

	static std::string ToStrExcel(double datetime);
	static std::string ToStr(HacTime datetime);
	static std::string ToStr(short num);
	static std::string ToStr(std::uint32_t num);
	static std::string ToStr(unsigned short num);
	static std::string ToStr(double num);
	static std::string ToStr(float num);
    static std::string ToStr(std::int32_t num);

	double	m_acquisitionTime;

};


#endif
