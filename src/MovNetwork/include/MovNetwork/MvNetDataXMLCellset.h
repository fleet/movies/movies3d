//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLCellset
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for all echo-integration  XML  heading message (Layer,Shoal,TS)
//  create heading frame
//
//===================================================================
#ifndef MvNetDataXMLCellset_h
#define MvNetDataXMLCellset_h

#include "MovNetwork/MvNetDataXML.h"
//#include "MovNetwork/MvInput/MSTime.h"

// OTK - FAE006 - optimisation - on calcule les descripteurs en fonction du dictionnaire
// une fois pour toutes
struct cellsetDescriptor
{
	bool m_enabled;
	bool m_acquisitionTime;
	bool m_cellindex;
	bool m_celltype;
	bool m_depthstart;
	bool m_depthend;
	bool m_pingstart;
	bool m_pingend;
	bool m_timestart;
	bool m_timeend;
	bool m_diststart;
	bool m_distend;
	bool m_threshldup;
	bool m_threshldlow;

	// NMD -  FAE 118
	bool m_latitude;
	bool m_longitude;
	bool m_volume;
	bool m_surface;
};
//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLCellset : public CMvNetDataXML
{

private:


public:
	unsigned int	m_cellindex;
	unsigned int	m_celltype;
	double			m_depthstart;
	double			m_depthend;
	std::int32_t			m_pingstart;
	std::int32_t			m_pingend;
	HacTime			m_timestart;
	HacTime			m_timeend;
	double			m_diststart;
	double			m_distend;
	double			m_threshldup;
	double			m_threshldlow;

	// NMD -  FAE 118
	double			m_latitude;
	double			m_longitude;
	double			m_volume;
	double			m_surface;

	typedef enum
	{
		kCellUndefined = -1,
		kCellSurface = 0,
		kCellBottom,
		kCellDistance,
		kCellTotal = 4
	}E_cellType;

	CMvNetDataXMLCellset();
	~CMvNetDataXMLCellset();

	static void ComputeDescriptor(BaseKernel::TramaDescriptor * cfg, cellsetDescriptor & desc);
	static void ComputeEISupervisedDescriptor(BaseKernel::TramaDescriptor * cfg, cellsetDescriptor & desc);

	// OTK - FAE169-170
	std::string createXMLSelectedOnly(const cellsetDescriptor & desc);

	// OTK - FAE169-170 archivage fichiers CSV
	std::string createCSV(const cellsetDescriptor & desc);
	std::string createCSV(BaseKernel::TramaDescriptor * cfg);

	const CMvNetDataXMLCellset &	operator=(const CMvNetDataXMLCellset &right);
};


#endif