//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLHeading
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for all echo-integration  XML  heading message (Layer,Shoal,TS)
//  create heading frame
//
//===================================================================
#ifndef MvNetDataXMLHeading_h
#define MvNetDataXMLHeading_h

#include "MovNetwork/MvNetDataXML.h"
//#include "MovNetwork/MvInput/MSTime.h"


//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLHeading : public CMvNetDataXML
{

private:

public:
	HacTime	m_datetime;
	std::string m_surveyname;
	std::string m_comment;

	CMvNetDataXMLHeading();
	~CMvNetDataXMLHeading();

	std::string createXML();

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);

	// OTK - FAE169-170 archivage fichiers XML
	std::string createCSV(BaseKernel::TramaDescriptor * cfg);

	const CMvNetDataXMLHeading &	operator=(const CMvNetDataXMLHeading &right);
};


#endif