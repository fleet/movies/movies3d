//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLLayer
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for layer echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLLayer_h
#define MvNetDataXMLLayer_h


#include "MovNetwork/MvNetDataXML.h"
#include <string>


// OTK - FAE006 - optimisation - on calcule les descripteurs en fonction du dictionnaire
// une fois pour toutes
struct layerDescriptor
{
	bool m_enabled;
	bool m_acquisitionTime;
	bool m_sa;
	bool m_sv;
	bool m_ni;
	bool m_nt;
};
//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLLayer : public CMvNetDataXML
{

private:


public:

	// frame eilayer
	double	m_sa;
	double	m_sv;
	std::int32_t	m_ni;
	std::int32_t	m_nt;

	CMvNetDataXMLLayer();
	~CMvNetDataXMLLayer();

	static void ComputeDescriptor(BaseKernel::TramaDescriptor * cfg, layerDescriptor & desc);
	static void ComputeEISupervisedDescriptor(BaseKernel::TramaDescriptor * cfg, layerDescriptor & desc);

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(const layerDescriptor & desc);

	// OTK - FAE169-170 archivage fichiers CSV
	std::string createCSV(const layerDescriptor & desc);
};


#endif