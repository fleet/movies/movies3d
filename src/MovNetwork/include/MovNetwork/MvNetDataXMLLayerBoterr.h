//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLLayerBoterr
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for bottom error layer echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLLayerBoterr_h
#define MvNetDataXMLLayerBoterr_h

#include "MovNetwork/MvNetDataXML.h"
#include <string>


// OTK - FAE006 - optimisation - on calcule les descripteurs en fonction du dictionnaire
// une fois pour toutes
struct botterrDescriptor
{
	bool m_enabled;

	bool m_sa;
	bool m_ni;
};
//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLLayerBoterr : public CMvNetDataXML
{

private:


public:

	// frame botterr
	double		 m_sa;
	unsigned int m_ni;

	CMvNetDataXMLLayerBoterr();
	~CMvNetDataXMLLayerBoterr();

	static void ComputeDescriptor(BaseKernel::TramaDescriptor * cfg, botterrDescriptor & desc);

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(const botterrDescriptor & desc);

	// OTK - FAE169-170 archivage fichiers XML
	std::string createCSV(const botterrDescriptor & desc);
};


#endif