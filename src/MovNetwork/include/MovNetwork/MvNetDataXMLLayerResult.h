//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLLayerResult
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL MVL
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for layer echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLLayerResult_h
#define MvNetDataXMLLayerResult_h

#include "MovNetwork/MvNetDataXML.h"
#include "MovNetwork/MvNetDataXMLLayer.h"
#include "MovNetwork/MvNetDataXMLCellset.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLLayerResult : public CMvNetDataXML
{

private:



public:

	CMvNetDataXMLLayer		m_netDataXMLLayer;
	CMvNetDataXMLCellset	m_netDataXMLCellset;

	CMvNetDataXMLLayerResult();
	~CMvNetDataXMLLayerResult();

	std::string createXML();
};

// collection of layers
#define CMvNetDataXMLLayerResultList CList<CMvNetDataXMLLayerResult*, CMvNetDataXMLLayerResult*>


#endif