//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLShipnav
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for all echo-integration  XML  heading message (Layer,Shoal,TS)
//  create heading frame
//
//===================================================================
#ifndef MvNetDataXMLShipnav_h
#define MvNetDataXMLShipnav_h

#include "MovNetwork/MvNetDataXML.h"

#include <string>

// OTK - FAE006 - optimisation - on calcule les descripteurs en fonction du dictionnaire
// une fois pour toutes
struct shipnavDescriptor
{
	bool m_enabled;
	bool  m_acquisitionTime;
	bool	m_latitude;
	bool	m_longitude;
	bool	m_groundspeed;
	bool	m_groundcourse;
	bool	m_heading;
	bool	m_depth;
	bool	m_driftcourse;
	bool	m_roll;
	bool	m_pitch;
	bool	m_heave;
	bool	m_driftspeed;
	bool	m_surfspeed;
	bool  m_surfcourse;
	bool	m_altitude;
	bool	m_draught;
};
//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLShipnav : public CMvNetDataXML
{

private:

public:
	double	m_latitude;
	double	m_longitude;
	double	m_groundspeed;
	double	m_groundcourse;
	double	m_heading;
	double	m_depth;
	double	m_driftcourse; //N
	double	m_roll; //N
	double	m_pitch; //N
	double	m_heave; //N
	double	m_driftspeed; //N
	double	m_surfspeed; //N
	double	m_altitude; //N
	double	m_draught; //N

	CMvNetDataXMLShipnav();
	~CMvNetDataXMLShipnav();

	static void ComputeDescriptor(BaseKernel::TramaDescriptor * cfg, shipnavDescriptor & desc);
	static shipnavDescriptor BuildDescriptor(BaseKernel::TramaDescriptor* tramaDescriptor);
	static void ComputeEISupervisedDescriptor(BaseKernel::TramaDescriptor * cfg, shipnavDescriptor & desc);

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(shipnavDescriptor & desc);

	// OTK - FAE169-170 archivage fichiers CSV
	std::string createCSV(const shipnavDescriptor & desc) const;

	std::string createCSV(BaseKernel::TramaDescriptor * cfg);

	const CMvNetDataXMLShipnav &	operator=(const CMvNetDataXMLShipnav &right);
};


#endif