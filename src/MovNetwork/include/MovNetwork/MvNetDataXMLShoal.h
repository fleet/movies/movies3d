//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLShoal
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for shoal echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLShoal_h
#define MvNetDataXMLShoal_h

#include "MovNetwork/MvNetDataXML.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																																		//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLShoal : public CMvNetDataXML
{

public:
	std::uint32_t m_shoalId;
	float		m_minDepth;
	float		m_maxDepth;
	float		m_meanDepth;
	float		m_minDtBot;
	float		m_maxDtBot;
	float		m_maxLength;
	float		m_maxWidth;
	float		m_maxHeight;
	float		m_sigma_ag;
	float		m_MVBS;
	float		m_MVBS_WEIGHTED;
	float		m_volume;
	float		m_nbOfEchoes;
	double	m_startTS;
	double	m_endTS;
	double	m_centerlat;
	double	m_centerlong;
	float		m_centerdepth;
	float		m_maxAcrossAngle;
	float		m_minAcrossAngle;

	CMvNetDataXMLShoal();
	~CMvNetDataXMLShoal();

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);

	// OTK - FAE169-170 archivage fichiers XML
	std::string createCSV(BaseKernel::TramaDescriptor * cfg);

};

#endif