//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLShoalAnset
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for shoal class echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLShoalAnset_h
#define MvNetDataXMLShoalAnset_h


#include "MovNetwork/MvNetDataXML.h"


//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLShoalAnset : public CMvNetDataXML
{

private:


public:
	float			m_minlength;
	float			m_minwidth;
	float			m_minheight;
	float			m_threshold;
	float			m_verticalintegrationdistance;
	float			m_alongintegrationdistance;
	float			m_acrossintegrationdistance;
	bool			m_saveEchoData;

	CMvNetDataXMLShoalAnset();
	~CMvNetDataXMLShoalAnset();

	std::string createXML();

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);

	// OTK - FAE169-170 archivage fichiers CSV
	std::string createCSV(BaseKernel::TramaDescriptor * cfg);
};

#endif