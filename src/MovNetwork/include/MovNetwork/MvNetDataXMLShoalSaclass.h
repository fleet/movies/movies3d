//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLShoalSaClass
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for shoal class echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLShoalSaClass_h
#define MvNetDataXMLShoalSaClass_h

#include "MovNetwork/MvNetDataXML.h"


//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLShoalSaclass : public CMvNetDataXML
{

private:


public:
	std::string m_classname;
	std::string m_classdef;
	double	m_sa;

	CMvNetDataXMLShoalSaclass();
	~CMvNetDataXMLShoalSaclass();

	std::string createXML();

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);

	// OTK - FAE169-170 archivage fichiers XML
	std::string createCSV(BaseKernel::TramaDescriptor * cfg);
};

#define CMvNetDataXMLShoalSaclassList CList <CMvNetDataXMLShoalSaclass*,CMvNetDataXMLShoalSaclass*>

#endif