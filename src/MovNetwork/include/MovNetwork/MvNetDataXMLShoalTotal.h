//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLShoalTotal
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for shoal class echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLShoalTotal_h
#define MvNetDataXMLShoalTotal_h

#include "MovNetwork/MvNetDataXML.h"


//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLShoalTotal : public CMvNetDataXML
{

private:


public:
	float			m_sa;
	float			m_svt;
	unsigned int	m_ni;
	unsigned int	m_nt;
	float			m_sashoals;
	unsigned int	m_notclass;
	float			m_sanoclass;

	CMvNetDataXMLShoalTotal();
	~CMvNetDataXMLShoalTotal();

	std::string createXML();

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);

	// OTK - FAE169-170 archivage fichiers XML
	std::string createCSV(BaseKernel::TramaDescriptor * cfg);
};


#endif