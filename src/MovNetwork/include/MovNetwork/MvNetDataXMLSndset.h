//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLSndset
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for all echo-integration  XML  heading message (Layer,Shoal,TS)
//  create heading frame
//
//===================================================================
#ifndef MvNetDataXMLSndset_h
#define MvNetDataXMLSndset_h

#include "MovNetwork/MvNetDataXML.h"

class SoftChannel;
// OTK - FAE006 - optimisation - on calcule les descripteurs en fonction du dictionnaire
// une fois pour toutes
struct sndsetDescriptor
{
	bool m_enabled;

	bool m_acquisitionTime;
	bool m_soundername;
	bool m_sounderident;

	// donn�es du channel 
	bool m_softChannelId;
	bool m_channelName;
	bool m_dataType;
	bool m_beamType;
	bool m_acousticFrequency;
	bool m_startSample;
	bool m_mainBeamAlongSteeringAngle;
	bool m_mainBeamAthwartSteeringAngle;
	bool m_absorptionCoef;
	bool m_transmissionPower;
	bool m_beamAlongAngleSensitivity;
	bool m_beamAthwartAngleSensitivity;
	bool m_beam3dBWidthAlong;
	bool m_beam3dBWidthAthwart;
	bool m_beamEquTwoWayAngle;
	bool m_beamGain;
	bool m_beamSACorrection;
	bool m_bottomDetectionMinDepth;
	bool m_bottomDetectionMaxDepth;
	bool m_bottomDetectionMinLevel;
	bool m_AlongTXRXWeightId;
	bool m_AthwartTXRXWeightId;
	bool m_SplitBeamAlongTXRXWeightId;
	bool m_SplitBeamAthwartTXRXWeightId;
	bool m_bandWidth;

	// NMD - FAE 2300 - correction de l'ordre des colonnes d�cal�es
	bool m_tvgtype;
	bool m_tvgminrange;
	bool m_tvgmaxrange;
	bool m_pulseduration;

	//NMD - FAE 112 ajout de la celerit� du son
	bool m_soundcelerity;

	bool m_slplusvr;
	bool m_tsgain;
};
//--------------------------------------------------------------------------------------//
//	Class Declaration																																		//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLSndset : public CMvNetDataXML
{

private:


public:

	// donn�es propres au sondeur
	std::string			m_soundername;
	std::int32_t						m_sounderident;


	// donn�es du channel 
	unsigned short    m_softChannelId;
	std::string				m_channelName;
	unsigned short    m_dataType;
	unsigned short    m_beamType;
	double						m_acousticFrequency;
	std::uint32_t			m_startSample;
	double            m_mainBeamAlongSteeringAngle;
	double            m_mainBeamAthwartSteeringAngle;
	double						m_absorptionCoef;
	std::uint32_t			m_transmissionPower;
	std::uint32_t			m_beamAlongAngleSensitivity;
	std::uint32_t			m_beamAthwartAngleSensitivity;
	double            m_beam3dBWidthAlong;
	double            m_beam3dBWidthAthwart;
	double            m_beamEquTwoWayAngle;
	double            m_beamGain;
	double            m_beamSACorrection;
	double            m_bottomDetectionMinDepth;
	double            m_bottomDetectionMaxDepth;
	double            m_bottomDetectionMinLevel;
	unsigned short    m_AlongTXRXWeightId;
	unsigned short    m_AthwartTXRXWeightId;
	unsigned short    m_SplitBeamAlongTXRXWeightId;
	unsigned short    m_SplitBeamAthwartTXRXWeightId;
	std::uint32_t			m_bandWidth;
	float							m_tvgminrange;
	float							m_tvgmaxrange;
	float							m_pulseduration;

	// NMD - FAE 112 - Ajout de la celerit� du son
	float             m_soundcelerity;


	CMvNetDataXMLSndset();
	CMvNetDataXMLSndset(std::string sndName, SoftChannel * pSoft, double pulseDur);
	~CMvNetDataXMLSndset();

	static void ComputeDescriptor(BaseKernel::TramaDescriptor * cfg, sndsetDescriptor & desc);
	static sndsetDescriptor BuildDescriptor(BaseKernel::TramaDescriptor * tramaDescriptor);
	static void ComputeEISupervisedDescriptor(BaseKernel::TramaDescriptor * cfg, sndsetDescriptor & desc);

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(const sndsetDescriptor & desc);


	// OTK - FAE169-170 archivage fichiers XML
	std::string createCSV(BaseKernel::TramaDescriptor * cfg);

	std::string createCSV(const sndsetDescriptor & desc) const;

	// permet de remplir le bon nombre de colonnes vides si un sndset n'appartient pas au banc de poisson
	std::string createEmptyCSV(const sndsetDescriptor & desc);

	const CMvNetDataXMLSndset &	operator=(const CMvNetDataXMLSndset &right);
};


#endif
