//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLTS
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for TS echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLTS_h
#define MvNetDataXMLTS_h

#include "MovNetwork/MvNetDataXML.h"
#include "MovNetwork/MvNetDataXMLTSTarget.h"
#include "MovNetwork/MvNetDataXMLTSLayer.h"
#include "MovNetwork/MvNetDataXMLTSTrack.h"
#include "MovNetwork/MvNetDataXMLCellset.h"


//--------------------------------------------------------------------------------------//
//	Class Declaration									     //
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLTS : public CMvNetDataXML
{

private:


public:
	CMvNetDataXMLTS();
	~CMvNetDataXMLTS();

	std::string createXML();

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);

	//CMvNetDataXMLCellset									m_allLayersCell;	// cellule d'analyse global : all layers

	//CList <CMvNetDataXMLTSTarget*, CMvNetDataXMLTSTarget*>	m_targetList;	//liste des cibles individuelles
	//CList <CMvNetDataXMLTSTrack*, CMvNetDataXMLTSTrack* >	m_trackList;	//liste des cibles traqu�es

	CMvNetDataXMLTSLayer									m_allSurfaceLayers; // for all surface layers	
	CMvNetDataXMLTSLayer									m_allSeabedLayers;  // for all seabed layers

	//CList <CMvNetDataXMLTSLayer*,CMvNetDataXMLTSLayer*>		m_surfaceLayersList; // list of all surfaces layers
	//CList <CMvNetDataXMLTSLayer*,CMvNetDataXMLTSLayer*>		m_seabedLayersList;	 // list of all seabed layers

};


#endif
