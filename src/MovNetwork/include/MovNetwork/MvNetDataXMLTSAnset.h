//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLTSAnset
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for TS echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLTSAnset_h
#define MvNetDataXMLTSAnset_h

#include "MovNetwork/MvNetDataXML.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLTSAnset : public CMvNetDataXML
{

private:


public:
	//TSSET
	int			m_minThresholdValue;
	float		m_minEchoLength;
	float		m_maxEchoLength;
	float		m_maxGainComp;
	float		m_maxPhaseDev;

	// ANSET
	unsigned int	m_nbMinSucEcho;
	unsigned int	m_nbMaxPing;
	float			m_maxDepthDiff;
	float			m_fishSpeed;
	unsigned int	m_nbClasses;
	float			m_step;
	float			m_tsInf;
	float			m_tsSup;

	CMvNetDataXMLTSAnset();
	~CMvNetDataXMLTSAnset();
	std::string createXML();
	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);
	// OTK - FAE169-170 archivage fichiers CSV
	std::string createCSV(BaseKernel::TramaDescriptor * cfg);

};


#endif