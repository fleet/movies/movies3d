//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLTS
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for TS XML message, for all layers , or specific group layers (seabed / surface)
//
//===================================================================
#ifndef MvNetDataXMLTSLayer_h
#define MvNetDataXMLTSLayer_h

#include "MovNetwork/MvNetDataXML.h"
#include "MovNetwork/MVNetDataXMLTSTotclass.h"
#include "MovNetwork/MvNetDataXMLTSStat.h"
#include "MovNetwork/MvNetDataXMLCellset.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLTSLayer : public CMvNetDataXML
{

private:


public:
	CMvNetDataXMLTSLayer();
	~CMvNetDataXMLTSLayer();

	std::string createXML();

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);

	int										type;			// surface or seabed

	CMvNetDataXMLCellset					m_cellLayer;	// cellule d'analyse: all layers/ surface layers /seabed layers )

	//CList <CMvNetDataXMLTSTotclass*,CMvNetDataXMLTSTotclass*>	m_totclassTsiList;		// liste des totclass des Targets d'une track
	//CList <CMvNetDataXMLTSTotclass*,CMvNetDataXMLTSTotclass*>	m_totclassTrackList;	// liste des totclass des Tracks
	//CList <CMvNetDataXMLTSTotclass*,CMvNetDataXMLTSTotclass*>	m_totclassTargetList;	// liste des totclass des Targets unitaires

	CMvNetDataXMLTSStat						m_tsstatTsi;		// tsstat des targets d'une track
	CMvNetDataXMLTSStat						m_tsstatTrack;		// tsstat des tracks
	CMvNetDataXMLTSStat						m_tsstatTarget;		// tsstat des targets unitaires
};


#endif
