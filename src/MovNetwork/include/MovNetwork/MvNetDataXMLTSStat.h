//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLTSStat
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for TS totclass echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLTSStat_h
#define MvNetDataXMLTSStat_h

#include "MovNetwork/MvNetDataXML.h"
#include "MovNetwork/MvNetDataXMLCellset.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLTSStat : public CMvNetDataXML
{

private:


public:

	//type de cible se r�f�rent � la statistique
	typedef enum
	{
		kUndefined = -1,
		kTarget = 0,	//Cible individuelle
		kTSi,	//cible composant une trajectoire
		kTrack				//trajectoire
	}E_type;

	E_type			m_type;

	//Attributs communs entre Targets/TSi et Tracks
	unsigned int	m_nb;
	float			m_tsmean;
	float			m_sigma;

	//Attributs valables uniquement pour les Tracks
	float			m_nbpingmean;
	float			m_prrm;
	float			m_dplrm;

	//Cellule d'analyse
	CMvNetDataXMLCellset	m_cell;

	CMvNetDataXMLTSStat();
	~CMvNetDataXMLTSStat();

	std::string createXML();

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);
};


#endif



