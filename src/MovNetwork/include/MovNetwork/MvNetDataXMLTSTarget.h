//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLTSTarget
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for TS echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLTSTarget_h
#define MvNetDataXMLTSTarget_h

#include "MovNetwork/MvNetDataXMLTSTarget.h"
#include "MovNetwork/MvNetDataXML.h"

#include "M3DKernel/datascheme/DateTime.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLTSTarget : public CMvNetDataXML
{

private:


public:
	// frame eitstarget
	double			m_lognum;

	//type de cible
	typedef enum
	{
		kUndefined = -1,
		kTarget = 0,	//cible individuelle
		kTSi				//cible composant une trajectoire		
	}E_ttype;

	E_ttype			m_targettype;

	unsigned int	m_pingnum;
	HacTime			m_time;
	double			m_lat;
	double			m_long;
	float			m_waterdepth;
	float			m_targetdepth;
	float			m_tscomp;
	float			m_tsuncomp;
	float			m_alphaalgmax;
	float			m_alphaathwmax;

	CMvNetDataXMLTSTarget();
	~CMvNetDataXMLTSTarget();

	std::string createXML();
	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);
	// OTK - FAE169-170 archivage fichiers CSV
	std::string createCSV(BaseKernel::TramaDescriptor * cfg);
};


#endif