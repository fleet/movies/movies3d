//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLTSTrack
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for TS echo-integration XML message
//
//===================================================================
#ifndef MvNetDataXMLTSTrack_h
#define MvNetDataXMLTSTrack_h

#include "MovNetwork/MvNetDataXMLTSTarget.h"
#include "MovNetwork/MvNetDataXML.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDataXMLTSTrack : public CMvNetDataXML
{

private:


public:
	// frame eiTSTrack
	double			m_lognum;
	HacTime			m_time;
	double			m_lat;
	double			m_long;
	unsigned int	m_pingstart;
	unsigned int	m_pingend;
	unsigned int	m_nping;
	float			m_depthstart;
	float			m_depthend;
	float			m_depthmean;
	float			m_meanwatdepth;
	float			m_tsmin;
	float			m_tsmax;
	float			m_tsmean;
	float			m_alphaalgmax;
	float			m_alphaathwmax;
	float			m_resdepth;
	float			m_resdispl;
	float			m_trackangle;

	// TSI list
	//CList <CMvNetDataXMLTSTarget*,CMvNetDataXMLTSTarget*> m_tsilist;

	CMvNetDataXMLTSTrack();
	~CMvNetDataXMLTSTrack();

	std::string createXML();

	// OTK - FAE169-170 archivage fichiers XML
	std::string createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg);

	// OTK - FAE169-170 archivage fichiers XML
	std::string createCSV(BaseKernel::TramaDescriptor * cfg);
};


#endif