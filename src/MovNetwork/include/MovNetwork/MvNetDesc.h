//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDesc
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for MOVIES XML description queries
#ifndef MvNetDesc_h
#define MvNetDesc_h

#include "MovNetwork/MvNetThread.h"
#include "MovNetwork/MvNetParameter.h"
#include "MovNetwork/MvNetDescQuery.h"
#include "MovNetwork/MovNetworkExport.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDesc : public CMvNetThread
{

protected:
	std::string getDirPath();


public:

	static int DESC_QUERY_ERR;
	static int DESC_QUERY_TS;
	static int DESC_QUERY_LAYER;
	static int DESC_QUERY_SHOAL;

	CMvNetDesc();
	CMvNetDesc(MvNetParameter& config);
	~CMvNetDesc();

	int createSocket();
	int listenSocket();
	int receiveRequest();
	int sendResponse(int desc_type);
	int sendStr(std::string description);
	void updateConfig(MvNetParameter& newConfig);

	void startThread();
	int stopThread();
	void internalThreadFunc();

private:
    class Impl;
    Impl * impl;
};


#endif
