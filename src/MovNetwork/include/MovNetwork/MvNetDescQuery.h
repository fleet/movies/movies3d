//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDescQuery
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class of MOVIES to analyse query description

#ifndef MvNetDescQuery_h
#define MvNetDescQuery_h


#define WIN32_LEAN_AND_MEAN // Pour ne pas avoir le confit Xerces et msxml.h (a cause de winsock)
#define NOMINMAX

#include <string>
#include <iostream>

#include "MovNetwork/MvNetParameter.h"
#include "MovNetwork/MovNetworkExport.h"

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetDescQuery
{
private:
	std::string m_XLMQuery;
	MvNetParameter m_currConfig;

public:
	CMvNetDescQuery(MvNetParameter& config);
	~CMvNetDescQuery();
	void setXMLBuffer(char * buffer);
	int parseXMLQuery();
};

#endif
