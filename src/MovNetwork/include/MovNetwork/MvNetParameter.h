#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "MovNetwork/MovNetworkExport.h"
#include "M3DKernel/parameter/ParameterModule.h"

#include <string>


// ***************************************************************************
// Declarations
// ***************************************************************************
class MOVNETWORK_API MvNetParameter :
	public BaseKernel::ParameterModule
{
public:
	// *********************************************************************
  // Constructeurs / Destructeur
  // *********************************************************************
	// Constructeur par d�faut
	MvNetParameter(void);

	// Destructeur
	virtual ~MvNetParameter(void);

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);


	// operateurs
	const MvNetParameter &	operator=(const MvNetParameter &right);

	// *********************************************************************
  // Donn�es
  // *********************************************************************

	// Periode d'emission de la configuration en secondes
	unsigned short		m_cfgDelay;
	// Adresse IP de l'host (par d�faut , IP locale)
	std::string				m_netHost;
	// Port utilis� pour l'emission de la configuration
	unsigned short		m_netCfgPort;
	// Port utilis� pour l'emission des r�sultats d'EI
	unsigned short		m_netDataPort;
	// Port utilis� pour l'emission des trames de description sur requete
	unsigned short		m_netDescPort;

	// Versionning
	unsigned int m_eiLayerMajor;
	unsigned int m_eiLayerMinor;
	unsigned int m_eiShoalMajor;
	unsigned int m_eiShoalMinor;
	unsigned int m_TSMajor;
	unsigned int m_TSMinor;
};
