//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetServUDP
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	class for sending data on network by UDP protocol
//
//===================================================================
#ifndef MvNetServUDP_h
#define MvNetServUDP_h

#pragma comment(lib, "ws2_32.lib")

#include <string>

#include "MovNetwork/MovNetworkExport.h"

#define DEFAULT_PORT 4950

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetServUDP
{

private:
    class Impl;
    Impl * impl;

public:
	CMvNetServUDP();
	CMvNetServUDP(int a_port);
	~CMvNetServUDP();

	int		createSocket(std::string hostAddr);
	int		sendData(std::string data);
	int		closeSocket();

	int		getServerPort();
	void	setServerPort(int a_port);
	std::string getServerAddr();

};


#endif
