//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetThread
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	Base class for thread
//
//===================================================================
#ifndef MvNetThread_h
#define MvNetThread_h

#include "MovNetwork/MovNetworkExport.h"

#include "M3DKernel/utils/socket/SocketCore.h"

#define WM_END_THREAD WM_USER+300

//--------------------------------------------------------------------------------------//
//	Class Declaration																	//
//--------------------------------------------------------------------------------------//
class MOVNETWORK_API CMvNetThread : CSocketCore
{

private:
    class Impl;
    Impl * impl;

protected:
    static void ThreadFunc(CMvNetThread* param);

public:
	CMvNetThread();
	~CMvNetThread();
	void launchThread();
	virtual int stopThread() = 0;
	virtual void internalThreadFunc() = 0;
    bool joinable();
	// attente de la fin de l'ex�cution du thread
	void waitEndThread();
};


#endif
