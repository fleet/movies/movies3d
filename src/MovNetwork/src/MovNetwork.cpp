#include "MovNetwork/MovNetwork.h"


// initialisation de notre pointeur statique
MovNetwork* MovNetwork::m_pMovNetwork = 0;

// accesseur vers l'instance statique de la classe
MovNetwork* MovNetwork::getInstance()
{
	if (!m_pMovNetwork)
	{
		m_pMovNetwork = new MovNetwork();
	}
	return m_pMovNetwork;
}

// Lib�re l'instance de MovNetwork
void MovNetwork::releaseInstance()
{
	if (m_pMovNetwork)
	{
		delete m_pMovNetwork;
		m_pMovNetwork = 0;
	}
}


// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************
// Constructeur par d�faut
MovNetwork::MovNetwork()
{
	m_Active = false;
	m_pMvNetConfig = new CMvNetConfig();
	m_pMvNetDesc = new CMvNetDesc();
}

// Destructeur
MovNetwork::~MovNetwork()
{
	if (m_Active)
	{
		stopThreads();
	}

	delete m_pMvNetConfig;
	delete m_pMvNetDesc;
}

// *********************************************************************
// traitements
// *********************************************************************
// lance les threads d'emission de la config et de gestion des requetes
// de description. Ceci doit �tre appel� des lors qu'un traitement
// d'EchoIntegration est lanc�
void MovNetwork::startThreads()
{
	m_pMvNetConfig->startThread();
	m_pMvNetDesc->startThread();
	m_Active = true;
}

// termine les threads d'emission de la config et de gestion des requetes
// de description.
void MovNetwork::stopThreads()
{
	m_pMvNetConfig->stopThread();
	m_pMvNetDesc->stopThread();
	m_Active = false;
}

// maj la nouvelle configuration dans les threads fils
void MovNetwork::updateConfiguration(MvNetParameter & config)
{
	m_pMvNetConfig->updateConfig(config);
	m_pMvNetDesc->updateConfig(config);
}

void MovNetwork::setEILayerEnabled(bool enabled)
{
	m_pMvNetConfig->m_EILayerEnabled = enabled;

	if ((m_pMvNetConfig->m_EILayerEnabled || m_pMvNetConfig->m_EIShoalEnabled)
		&& (!m_Active))
	{
		startThreads();
	}

	if ((!m_pMvNetConfig->m_EILayerEnabled && !m_pMvNetConfig->m_EIShoalEnabled)
		&& (m_Active))
	{
		stopThreads();
	}
}

void MovNetwork::setEIShoalEnabled(bool enabled)
{
	m_pMvNetConfig->m_EIShoalEnabled = enabled;

	if ((m_pMvNetConfig->m_EILayerEnabled || m_pMvNetConfig->m_EIShoalEnabled)
		&& (!m_Active))
	{
		startThreads();
	}

	if ((!m_pMvNetConfig->m_EILayerEnabled && !m_pMvNetConfig->m_EIShoalEnabled)
		&& (m_Active))
	{
		stopThreads();
	}
}
