//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetConfig
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	class to manage network diffusion
//	 of MOVIES configuration information.
//
//===================================================================

#include "MovNetwork/MvNetConfig.h"

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

/**
 * constructor
 */
CMvNetConfig::CMvNetConfig()
	:m_currConfig()
{
	m_EILayerEnabled = false;
	m_EIShoalEnabled = false;
	m_TSEnabled = false;
}


/**
 * constructor
 */
CMvNetConfig::CMvNetConfig(MvNetParameter& config)
	:m_currConfig()
{
	m_currConfig = config;

	m_EILayerEnabled = false;
	m_EIShoalEnabled = false;
	m_TSEnabled = false;
}

/**
 * initialise
 */
void CMvNetConfig::startThread()
{
	// create and launch thread
	launchThread();
}

/**
 * destructor
 */
CMvNetConfig::~CMvNetConfig()
{
	stopThread();
}


/**
 * redefine CMvNetThread method: action of thread
 */
void CMvNetConfig::internalThreadFunc()
{
#ifdef WIN32
	MSG l_msg;
	UINT l_timer;
	CMvNetConfigXML*	l_pConfXML = NULL;
	CMvNetServUDP*		l_pServUDP = NULL;
	std::string			l_strXML = "";

	// create configuration XML message 
	l_pConfXML = new CMvNetConfigXML(m_currConfig);

	// IPSIS - OTK - pusitionnement des flags d'activit� des modules d'Echo-int�gration
	l_pConfXML->m_EILayerEnabled = m_EILayerEnabled;
	l_pConfXML->m_EIShoalEnabled = m_EIShoalEnabled;
	l_pConfXML->m_TSEnabled = m_TSEnabled;

	l_strXML = l_pConfXML->getBuffer();

	// create UDP server to send configuration XML message
	l_pServUDP = new CMvNetServUDP(m_currConfig.m_netCfgPort);
	l_pServUDP->createSocket(m_currConfig.m_netHost);

	l_timer = SetTimer(NULL, NULL, m_currConfig.m_cfgDelay * 1000, NULL);
	m_continue = true;
	while (m_continue && GetMessage(&l_msg, NULL, 0, 0))
	{
		switch (l_msg.message)
		{
			// TIMER SIGNAL
		case WM_TIMER:
		{
			// if network settings changed, need to recreate socket & XML string
			if (m_currConfig.m_netCfgPort != l_pServUDP->getServerPort() ||
				m_currConfig.m_netHost.compare(l_pServUDP->getServerAddr()))
			{
				l_pServUDP->closeSocket();
				l_pServUDP->setServerPort(m_currConfig.m_netCfgPort);
				l_pServUDP->createSocket(m_currConfig.m_netHost);
				l_pConfXML->setCurrConfig(m_currConfig);
				l_strXML = l_pConfXML->getBuffer();
			}
			l_pServUDP->sendData(l_strXML);
			break;
		}

		// END SIGNAL
		case WM_END_THREAD: m_continue = false;
		default: break;
		}
	}
	KillTimer(NULL, l_timer);
	l_pServUDP->closeSocket();

	delete l_pConfXML;
	delete l_pServUDP;
#endif
}


/**
 * stop thread with message
 */
int CMvNetConfig::stopThread()
{
	m_continue = false;
	waitEndThread();
	return 0;
}


void CMvNetConfig::updateConfig(MvNetParameter& newConfig)
{
	m_currConfig = newConfig;
}
