//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetConfigXML
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	class to create XML string for MOVIES configuration.
//
//===================================================================
#include "MovNetwork/MvNetConfigXML.h"

#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/utils/M3DStdUtils.h"

//#include <afxwin.h>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/MemBufFormatTarget.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>

XERCES_CPP_NAMESPACE_USE

namespace
{
	constexpr const char * LoggerName = "MovNetwork.MvNetConfigXML";
}

/**
 * constructor
 */
CMvNetConfigXML::CMvNetConfigXML(MvNetParameter& config)
{
	m_currConfig = config;

	m_EILayerEnabled = false;
	m_EIShoalEnabled = false;
	m_TSEnabled = false;
}


/**
 * destructor
 */
CMvNetConfigXML::~CMvNetConfigXML()
{

}



/**
 * update config
 */
void CMvNetConfigXML::setCurrConfig(MvNetParameter& config)
{
	m_currConfig = config;
}


/**
 * get XML configuration string
 */
std::string CMvNetConfigXML::getBuffer()
{
	// create XML string that describe current config.
	createXML();

	return m_buffer;
}

/**
 * get parameters for XML configuration
 */
int CMvNetConfigXML::readConfigParameters()
{
	return 0;
}


/**
 * create XML String
 */
int CMvNetConfigXML::createXML()
{
	// Watch for special case help request
	int errorCode = 0;
	DOMDocument* l_doc = NULL;
	XMLFormatTarget *myFormatTarget = NULL;
	DOMLSSerializer* myWriter = NULL;
	DOMImplementation* l_impl = NULL;
	m_buffer = "";
	char tmp[20];


	// Initialize the XML4C2 system.
	try
	{
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& toCatch)
	{
		char *pMsg = XMLString::transcode(toCatch.getMessage());
		M3D_LOG_ERROR(LoggerName, "MvNetConfigXML  Error during Xerces-c Initialization.");
		XMLString::release(&pMsg);
		return -1;
	}


	// basic DOM document
	XMLCh *xmlch1, *xmlch2;
	xmlch1 = XMLString::transcode("Core");
	l_impl = DOMImplementationRegistry::getDOMImplementation(xmlch1);
	XMLString::release(&xmlch1);
	if (l_impl != NULL)
	{
		try
		{
			// last version date
			std::string versiondate = "2007-03-09T09:00Z";

			xmlch1 = XMLString::transcode("configuration");
			l_doc = l_impl->createDocument(0,						// root element namespace URI.
				xmlch1,				// root element name
				0);						// document type object (no DTD).
			XMLString::release(&xmlch1);

			// xml options
			xmlch1 = XMLString::transcode("1.0");
			l_doc->setXmlVersion(xmlch1);
			XMLString::release(&xmlch1);
			//xmlch1 = XStr("UTF-8");
			//l_doc->setEncoding(xmlch1);
			//XMLString::release(&xmlch1);
			l_doc->setXmlStandalone(true);

			// config element
			DOMElement* rootElem = l_doc->getDocumentElement();
			xmlch1 = XMLString::transcode("xmlns");
			xmlch2 = XMLString::transcode("http://www.ifremer.fr/flotte/equipements_sc/logiciels_embaques/movies");
			rootElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);


			// device element
			xmlch1 = XMLString::transcode("device");
			DOMElement*  devElem = l_doc->createElement(xmlch1);
			XMLString::release(&xmlch1);
			xmlch1 = XMLString::transcode("deviceid");
			xmlch2 = XMLString::transcode("MOVIES");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("devicename");
			xmlch2 = XMLString::transcode("Logiciel de traitement de donnees acoustiques");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("sourcetypelist");
			xmlch2 = XMLString::transcode("MOVIES");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("firstusedate");
			xmlch2 = XMLString::transcode(versiondate.c_str());
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("position");
			xmlch2 = XMLString::transcode("");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("x");
			xmlch2 = XMLString::transcode("0.0");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("y");
			xmlch2 = XMLString::transcode("0.0");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("z");
			xmlch2 = XMLString::transcode("0.0");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("latestcalibrationdate");
			xmlch2 = XMLString::transcode("versiondate.c_str()");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("calibrationparameters");
			xmlch2 = XMLString::transcode("");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("installdate");
			xmlch2 = XMLString::transcode("versiondate.c_str()");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("workingparameters");
			xmlch2 = XMLString::transcode("");
			devElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			rootElem->appendChild(devElem);


			//message LAYER element
			if (m_EILayerEnabled)
			{
				xmlch1 = XMLString::transcode("message");
				DOMElement*  msgLayerElem = l_doc->createElement(xmlch1);
				XMLString::release(&xmlch1);
				xmlch1 = XMLString::transcode("name");
				xmlch2 = XMLString::transcode("MOVIES_EILayer");
				msgLayerElem->setAttribute(xmlch1, xmlch2);
				XMLString::release(&xmlch1);
                XMLString::release(&xmlch2);
				_itoa_s(m_currConfig.m_eiLayerMajor, tmp, 20, 10);
				xmlch1 = XMLString::transcode("major");
				xmlch2 = XMLString::transcode(tmp);
				msgLayerElem->setAttribute(xmlch1, xmlch2);
				XMLString::release(&xmlch1);
				XMLString::release(&xmlch2);
				_itoa_s(m_currConfig.m_eiLayerMinor, tmp, 20, 10);
				xmlch1 = XMLString::transcode("minor");
				xmlch2 = XMLString::transcode(tmp);
				msgLayerElem->setAttribute(xmlch1, xmlch2);
				XMLString::release(&xmlch1);
				XMLString::release(&xmlch2);
				devElem->appendChild(msgLayerElem);
			}

			//message SHOAL element	
			if (m_EIShoalEnabled)
			{
				xmlch1 = XMLString::transcode("message");
				DOMElement*  msgShoalElem = l_doc->createElement(xmlch1);
				XMLString::release(&xmlch1);
				xmlch1 = XMLString::transcode("name");
				xmlch2 = XMLString::transcode("MOVIES_EIShoal");
				msgShoalElem->setAttribute(xmlch1, xmlch2);
				XMLString::release(&xmlch1);
				XMLString::release(&xmlch2);
				_itoa_s(m_currConfig.m_eiShoalMajor, tmp, 20, 10);
				xmlch1 = XMLString::transcode("major");
				xmlch2 = XMLString::transcode(tmp);
				msgShoalElem->setAttribute(xmlch1, xmlch2);
				XMLString::release(&xmlch1);
				XMLString::release(&xmlch2);
				_itoa_s(m_currConfig.m_eiShoalMinor, tmp, 20, 10);
				xmlch1 = XMLString::transcode("minor");
				xmlch2 = XMLString::transcode(tmp);
				msgShoalElem->setAttribute(xmlch1, xmlch2);
				XMLString::release(&xmlch1);
				XMLString::release(&xmlch2);
				devElem->appendChild(msgShoalElem);
			}

			//message TS element	
			if (m_TSEnabled)
			{
				DOMElement*  msgTSElem = l_doc->createElement(XMLString::transcode("message"));
				XMLString::release(&xmlch1);
				xmlch1 = XMLString::transcode("name");
				xmlch2 = XMLString::transcode("MOVIES_TSAnalysis");
				msgTSElem->setAttribute(xmlch1, xmlch2);
				XMLString::release(&xmlch1);
				XMLString::release(&xmlch2);
				_itoa_s(m_currConfig.m_eiShoalMajor, tmp, 20, 10);
				xmlch1 = XMLString::transcode("major");
				xmlch2 = XMLString::transcode(tmp);
				msgTSElem->setAttribute(xmlch1, xmlch2);
				XMLString::release(&xmlch1);
				XMLString::release(&xmlch2);
				_itoa_s(m_currConfig.m_eiShoalMinor, tmp, 20, 10);
				xmlch1 = XMLString::transcode("minor");
				xmlch2 = XMLString::transcode(tmp);
				msgTSElem->setAttribute(xmlch1, xmlch2);
				XMLString::release(&xmlch1);
				XMLString::release(&xmlch2);
				devElem->appendChild(msgTSElem);
			}

			//broadcast element		  
			xmlch1 = XMLString::transcode("broadcastaddress");
			DOMElement*  broadcastElem = l_doc->createElement(xmlch1);
			XMLString::release(&xmlch1);
			xmlch1 = XMLString::transcode("host");
			xmlch2 = XMLString::transcode(m_currConfig.m_netHost.c_str());
			broadcastElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			_itoa_s(m_currConfig.m_netDataPort, tmp, 20, 10);
			xmlch1 = XMLString::transcode("port");
			xmlch2 = XMLString::transcode(tmp);
			broadcastElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("protocol");
			xmlch2 = XMLString::transcode("udp");
			broadcastElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			devElem->appendChild(broadcastElem);

			//description element		   
			xmlch1 = XMLString::transcode("descriptionaddress");
			DOMElement*  descriptionElem = l_doc->createElement(xmlch1);
			XMLString::release(&xmlch1);
			xmlch1 = XMLString::transcode("host");
			xmlch2 = XMLString::transcode(m_currConfig.m_netHost.c_str());
			descriptionElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			_itoa_s(m_currConfig.m_netDescPort, tmp, 20, 10);
			xmlch1 = XMLString::transcode("port");
			xmlch2 = XMLString::transcode(tmp);
			descriptionElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			xmlch1 = XMLString::transcode("protocol");
			xmlch2 = XMLString::transcode("tcp");
			descriptionElem->setAttribute(xmlch1, xmlch2);
			XMLString::release(&xmlch1);
			XMLString::release(&xmlch2);
			devElem->appendChild(descriptionElem);

			// get a serializer, an instance of DOMWriter
			myWriter = ((DOMImplementationLS*)l_impl)->createLSSerializer();

			//d�finition de la sortie
			myFormatTarget = new MemBufFormatTarget();
			DOMLSOutput* theOutput = ((DOMImplementationLS*)l_impl)->createLSOutput();
			xmlch1 = XMLString::transcode("UTF-8");
			theOutput->setEncoding(xmlch1);
			XMLString::release(&xmlch1);

			theOutput->setByteStream(myFormatTarget);

			// to write XML into buffer
			if (!myWriter->write(l_doc, theOutput))
			{
				M3D_LOG_ERROR(LoggerName, "MvNetConfigXML  myWriter->write() error.");
				errorCode = 6;
			}
			else
			{
				// get xml tree into string
				m_buffer = (const char*)((MemBufFormatTarget*)myFormatTarget)->getRawBuffer();
			}

			theOutput->release();
			myWriter->release();
			l_doc->release();

			delete myFormatTarget;
		}
		catch (const OutOfMemoryException&)
		{
			M3D_LOG_ERROR(LoggerName, "MvNetConfigXML  OutOfMemoryException.");
			errorCode = 5;
		}
		catch (const DOMException&)
		{
			M3D_LOG_ERROR(LoggerName, "MvNetConfigXML  DOMException.");
			errorCode = 2;
		}
		catch (...)
		{
			M3D_LOG_ERROR(LoggerName, "MvNetConfigXML  An error occurred creating the document.");
			errorCode = 3;
		}
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "MvNetConfigXML  Requested implementation is not supported.");
	}

	XMLPlatformUtils::Terminate();

	return errorCode;
}

