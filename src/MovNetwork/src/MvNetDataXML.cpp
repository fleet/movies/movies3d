//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXML
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for all echo-intergation XML messages
//
//=================================================================

#include "MovNetwork/MvNetDataXML.h"
#include <iomanip>
#include <sstream>
#include <time.h>

#include <M3DKernel/utils/M3DStdUtils.h>


std::string CMvNetDataXML::M_device = "MOVIES";

static std::stringstream fixed_precision6_ss;
static std::stringstream fixed_ss;

/**
 * constructor
 */
CMvNetDataXML::CMvNetDataXML()
{
	m_acquisitionTime = 0.0;
	fixed_precision6_ss << std::fixed;
	fixed_precision6_ss.precision(6);
	fixed_ss << std::fixed;
}


/**
 * destructor
 */
CMvNetDataXML::~CMvNetDataXML()
{

}

/**
 * create XML meta header & start frame message
 */
std::string CMvNetDataXML::createXMLHeader(std::string msgname, int major, int minor)
{
	std::string l_XmlHeader = "";
	l_XmlHeader += "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
	l_XmlHeader += "<message name=\"" + msgname + "\" ";
	l_XmlHeader += "major=\"" + CMvNetDataXML::ToStr(major) + "\" ";
	l_XmlHeader += "minor=\"" + CMvNetDataXML::ToStr(minor) + "\" ";
	l_XmlHeader += "deviceid=\"" + CMvNetDataXML::M_device + "\">\n";
	return l_XmlHeader;
}


/**
 * create end frame message
 */
std::string CMvNetDataXML::createXMLFooter()
{
	std::string l_XmlHeader = "";
	l_XmlHeader += "</message>";
	return l_XmlHeader;
}



/**
 * empty method
 */
std::string CMvNetDataXML::createXML()
{
	return "";
}


/**
 * conversion  MSTime => String
 */
std::string CMvNetDataXML::ToStrExcel(double p_datetime)
{
	// difference from 1970-01-01 00:00:00 to 1899-12-30 00:00:00
	double dif_days = 70 * 365 + 17 + 2;
	double res = (p_datetime / 3600 / 24) + dif_days;

	fixed_precision6_ss.str("");
	fixed_precision6_ss << res;
	return fixed_precision6_ss.str();
}


/**
 * conversion  MSTime => String
 */
std::string CMvNetDataXML::ToStr(HacTime p_datetime)
{
	char date[255];

	// NMD -13/04/2012  - FAE 119
	// Modification du format de sortie des dates dans le CSV
	//p_datetime.GetTimeDesc(date,254);
	time_t time = p_datetime.m_TimeCpu;
	tm t;
#ifdef WIN32
	gmtime_s(&t, &time);
#else
	gmtime_r(&time, &t);
#endif
    sprintf(date, "%4d-%02d-%02d %02d:%02d:%02d.%03d",
		t.tm_year + 1900, t.tm_mon + 1, t.tm_mday,
		t.tm_hour, t.tm_min, t.tm_sec, p_datetime.m_TimeFraction);

	std::string result(date);
	return result;
}


/**
 * conversion  std::int32_t => String
 */
std::string CMvNetDataXML::ToStr(std::int32_t num)
{
	fixed_ss.str("");
	fixed_ss << num;
	return fixed_ss.str();
}

/**
 * conversion  short => String
 */
std::string CMvNetDataXML::ToStr(short num)
{
	fixed_ss.str("");
	fixed_ss << num;
	return fixed_ss.str();
}

/**
 * conversion  unsigned short => String
 */
std::string CMvNetDataXML::ToStr(unsigned short num)
{
	fixed_ss.str("");
	fixed_ss << num;
	return fixed_ss.str();
}

/**
 * conversion  std::uint32_t => String
 */
std::string CMvNetDataXML::ToStr(std::uint32_t num)
{
	fixed_ss.str("");
	fixed_ss << num;
	return fixed_ss.str();
}

/**
 * conversion  double => String
 */
std::string CMvNetDataXML::ToStr(double num)
{
	fixed_precision6_ss.str("");
	fixed_precision6_ss << num;
	return fixed_precision6_ss.str();
}

/**
 * conversion  float => String
 */
std::string CMvNetDataXML::ToStr(float num)
{
	fixed_precision6_ss.str("");
	fixed_precision6_ss << num;
	return fixed_precision6_ss.str();
}



// OTK - FAE169-170 - archivage selectif des trames XML
// renvoie vrai si la config demande d'archiver la trame stringID
bool CMvNetDataXML::tramaWanted(BaseKernel::TramaDescriptor * cfg, std::string stringID)
{
	// par defaut, toutes les trames sont envoy�es (si elles n'ont pas �t� configur�es)
	if (!cfg->m_Configured || cfg->m_xmlNodesArray.size() == 0)
	{
		return true;
	}

	int nbTramas = (int)cfg->m_xmlNodesArray.size();

	for (int i = 0; i < nbTramas; i++)
	{
		//on supprime la racine (pour que la comparaison soit ind�pendante de EILayer, shoal ou TSa
		if (cfg->m_xmlNodesArray[i].wanted
			&& !cfg->m_xmlNodesArray[i].xmlNodeName.compare(cfg->m_xmlNodesArray[i].xmlNodeName.find('\\') + 1, stringID.length(), stringID))
		{
			// renvoi direct pour perfs
			return true;
		}
	}

	return false;
}
