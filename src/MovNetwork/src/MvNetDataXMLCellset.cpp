//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLCellset
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for cell setting XML element
//
//=================================================================


#include "MovNetwork/MvNetDataXMLCellset.h"
//#include <iostream>


CMvNetDataXMLCellset::CMvNetDataXMLCellset()
{
	m_cellindex = -1;
	m_celltype = kCellUndefined;
	m_depthstart = 0.0;
	m_depthend = 0.0;
	m_pingstart = -1;
	m_pingend = -1;
	m_timestart = HacTime(0, 0);
	m_timeend = HacTime(0, 0);
	m_diststart = 0.0;
	m_distend = 0.0;
	m_threshldup = 0.0;
	m_threshldlow = 0.0;

	// NMD - FAE 118
	m_latitude = -100;
	m_longitude = -200;
	m_volume = -1;
	m_surface = -1;
}

CMvNetDataXMLCellset::~CMvNetDataXMLCellset()
{

}

// OTK - FAE006 - optimisation : on ne d�termine les donn�es � produire en fonction de la configuration
// qu'une fois pour tous les r�sultats
void CMvNetDataXMLCellset::ComputeDescriptor(BaseKernel::TramaDescriptor * cfg, cellsetDescriptor & desc)
{
	if (tramaWanted(cfg, "cellset"))
	{
		desc.m_enabled = true;
		desc.m_acquisitionTime = true;
		desc.m_cellindex = tramaWanted(cfg, "cellset\\cellnum") ? true : false;
		desc.m_celltype = tramaWanted(cfg, "cellset\\celltype") ? true : false;
		desc.m_depthstart = tramaWanted(cfg, "cellset\\depthstart") ? true : false;
		desc.m_depthend = tramaWanted(cfg, "cellset\\depthend") ? true : false;
		desc.m_pingstart = tramaWanted(cfg, "cellset\\indexstart") ? true : false;
		desc.m_pingend = tramaWanted(cfg, "cellset\\indexend") ? true : false;
		desc.m_timestart = tramaWanted(cfg, "cellset\\datestart") ? true : false;
		desc.m_timeend = tramaWanted(cfg, "cellset\\dateend") ? true : false;
		desc.m_diststart = tramaWanted(cfg, "cellset\\diststart") ? true : false;
		desc.m_distend = tramaWanted(cfg, "cellset\\distend") ? true : false;
		desc.m_threshldup = tramaWanted(cfg, "cellset\\thresholdup") ? true : false;
		desc.m_threshldlow = tramaWanted(cfg, "cellset\\thresholdlow") ? true : false;

		// NMD - FAE 118
		desc.m_latitude = tramaWanted(cfg, "cellset\\lat") ? true : false;
		desc.m_longitude = tramaWanted(cfg, "cellset\\long") ? true : false;
		desc.m_volume = tramaWanted(cfg, "cellset\\volume") ? true : false;
		desc.m_surface = tramaWanted(cfg, "cellset\\surface") ? true : false;
	}
	else
	{
		desc.m_enabled = false;
	}
}

void CMvNetDataXMLCellset::ComputeEISupervisedDescriptor(BaseKernel::TramaDescriptor * cfg, cellsetDescriptor & desc)
{
	if (tramaWanted(cfg, "cellset"))
	{
		desc.m_enabled = true;
		desc.m_acquisitionTime = false;
		desc.m_cellindex = tramaWanted(cfg, "cellset\\cellnum") ? true : false;
		desc.m_celltype = tramaWanted(cfg, "cellset\\celltype") ? true : false;
		desc.m_depthstart = tramaWanted(cfg, "cellset\\depthstart") ? true : false;
		desc.m_depthend = tramaWanted(cfg, "cellset\\depthend") ? true : false;
		desc.m_pingstart = tramaWanted(cfg, "cellset\\indexstart") ? true : false;
		desc.m_pingend = tramaWanted(cfg, "cellset\\indexend") ? true : false;
		desc.m_timestart = tramaWanted(cfg, "cellset\\datestart") ? true : false;
		desc.m_timeend = tramaWanted(cfg, "cellset\\dateend") ? true : false;
		desc.m_diststart = tramaWanted(cfg, "cellset\\diststart") ? true : false;
		desc.m_distend = tramaWanted(cfg, "cellset\\distend") ? true : false;
		desc.m_threshldup = tramaWanted(cfg, "cellset\\thresholdup") ? true : false;
		desc.m_threshldlow = tramaWanted(cfg, "cellset\\thresholdlow") ? true : false;

		// NMD - FAE 118
		desc.m_latitude = tramaWanted(cfg, "cellset\\lat") ? true : false;
		desc.m_longitude = tramaWanted(cfg, "cellset\\long") ? true : false;
		desc.m_volume = tramaWanted(cfg, "cellset\\volume") ? true : false;
		desc.m_surface = tramaWanted(cfg, "cellset\\surface") ? true : false;
	}
	else
	{
		desc.m_enabled = false;
	}
}

// OTK - FAE169-170 - cretaion du fichier XML en fonction des noeuds s�lectionn�s
std::string CMvNetDataXMLCellset::createXMLSelectedOnly(const cellsetDescriptor & desc)
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		if (desc.m_acquisitionTime)
		{
			l_XmlStr += "<cellset acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		}
		if (desc.m_cellindex)
		{
			l_XmlStr += "<cellnum>" + CMvNetDataXML::ToStr(m_cellindex) + "</cellnum>\n";
		}
		if (desc.m_celltype)
		{
			l_XmlStr += "<celltype>" + CMvNetDataXML::ToStr(m_celltype) + "</celltype>\n";
		}
		if (desc.m_depthstart)
		{
			l_XmlStr += "<depthstart>" + CMvNetDataXML::ToStr(m_depthstart) + "</depthstart>\n";
		}
		if (desc.m_depthend)
		{
			l_XmlStr += "<depthend>" + CMvNetDataXML::ToStr(m_depthend) + "</depthend>\n";
		}
		if (desc.m_pingstart)
		{
			l_XmlStr += "<indexstart>" + CMvNetDataXML::ToStr(m_pingstart) + "</indexstart>\n";
		}
		if (desc.m_pingend)
		{
			l_XmlStr += "<indexend>" + CMvNetDataXML::ToStr(m_pingend) + "</indexend>\n";
		}
		if (desc.m_timestart)
		{
			l_XmlStr += "<datestart>" + CMvNetDataXML::ToStr(m_timestart) + "</datestart>\n";
		}
		if (desc.m_timeend)
		{
			l_XmlStr += "<dateend>" + CMvNetDataXML::ToStr(m_timeend) + "</dateend>\n";
		}
		if (desc.m_diststart)
		{
			l_XmlStr += "<diststart>" + CMvNetDataXML::ToStr(m_diststart) + "</diststart>\n";
		}
		if (desc.m_distend)
		{
			l_XmlStr += "<distend>" + CMvNetDataXML::ToStr(m_distend) + "</distend>\n";
		}
		if (desc.m_threshldup)
		{
			l_XmlStr += "<threshldup>" + CMvNetDataXML::ToStr(m_threshldup) + "</threshldup>\n";
		}
		if (desc.m_threshldlow)
		{
			l_XmlStr += "<threshldlow>" + CMvNetDataXML::ToStr(m_threshldlow) + "</threshldlow>\n";
		}

		// NMD - FAE 118
		if (desc.m_latitude)
		{
			l_XmlStr += "<lat>" + CMvNetDataXML::ToStr(m_latitude) + "</lat>\n";
		}
		if (desc.m_longitude)
		{
			l_XmlStr += "<long>" + CMvNetDataXML::ToStr(m_latitude) + "</long>\n";
		}
		if (desc.m_volume)
		{
			l_XmlStr += "<volume>" + CMvNetDataXML::ToStr(m_latitude) + "</volume>\n";
		}
		if (desc.m_surface)
		{
			l_XmlStr += "<surface>" + CMvNetDataXML::ToStr(m_surface) + "</surface>\n";
		}

		l_XmlStr += "</cellset>\n";
	}

	return l_XmlStr;
}

// OTK - FAE169-170 - cretaion du fichier XML en fonction des noeuds s�lectionn�s
std::string CMvNetDataXMLCellset::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::map<std::string, std::string> values;

	values["cellset\\cellnum"] = CMvNetDataXML::ToStr(m_cellindex);
	values["cellset\\celltype"] = CMvNetDataXML::ToStr(m_celltype);
	values["cellset\\depthstart"] = CMvNetDataXML::ToStr(m_depthstart);
	values["cellset\\depthend"] = CMvNetDataXML::ToStr(m_depthend);
	values["cellset\\indexstart"] = CMvNetDataXML::ToStr(m_pingstart);
	values["cellset\\indexend"] = CMvNetDataXML::ToStr(m_pingend);
	values["cellset\\datestart"] = CMvNetDataXML::ToStr(m_timestart);
	values["cellset\\dateend"] = CMvNetDataXML::ToStr(m_timeend);
	values["cellset\\diststart"] = CMvNetDataXML::ToStr(m_diststart);
	values["cellset\\distend"] = CMvNetDataXML::ToStr(m_distend);
	values["cellset\\thresholdup"] = CMvNetDataXML::ToStr(m_threshldup);
	values["cellset\\thresholdlow"] = CMvNetDataXML::ToStr(m_threshldlow);

	// NMD - FAE 118
	values["cellset\\lat"] = CMvNetDataXML::ToStr(m_latitude);
	values["cellset\\long"] = CMvNetDataXML::ToStr(m_longitude);
	values["cellset\\volume"] = CMvNetDataXML::ToStr(m_volume);
	values["cellset\\surface"] = CMvNetDataXML::ToStr(m_surface);

	std::string l_sResult = cfg->GetValuesByCat("cellset\\", values);
	return l_sResult;
}

// OTK - FAE169-170 - cretaion du fichier XML en fonction des noeuds s�lectionn�s
std::string CMvNetDataXMLCellset::createCSV(const cellsetDescriptor & desc)
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		if (desc.m_acquisitionTime)
		{
			l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		}
		if (desc.m_cellindex)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_cellindex) + s_SEPARATOR;
		}
		if (desc.m_celltype)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_celltype) + s_SEPARATOR;
		}
		if (desc.m_depthstart)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_depthstart) + s_SEPARATOR;
		}
		if (desc.m_depthend)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_depthend) + s_SEPARATOR;
		}
		if (desc.m_pingstart)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_pingstart) + s_SEPARATOR;
		}
		if (desc.m_pingend)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_pingend) + s_SEPARATOR;
		}
		if (desc.m_timestart)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_timestart) + s_SEPARATOR;
		}
		if (desc.m_timeend)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_timeend) + s_SEPARATOR;
		}
		if (desc.m_diststart)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_diststart) + s_SEPARATOR;
		}
		if (desc.m_distend)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_distend) + s_SEPARATOR;
		}
		if (desc.m_threshldup)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_threshldup) + s_SEPARATOR;
		}
		if (desc.m_threshldlow)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_threshldlow) + s_SEPARATOR;
		}

		// NMD - FAE 118
		if (desc.m_latitude)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_latitude) + s_SEPARATOR;
		}
		if (desc.m_longitude)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_longitude) + s_SEPARATOR;
		}
		if (desc.m_volume)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_volume) + s_SEPARATOR;
		}
		if (desc.m_surface)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_surface) + s_SEPARATOR;
		}
	}

	return l_XmlStr;
}

const CMvNetDataXMLCellset &	CMvNetDataXMLCellset::operator=(const CMvNetDataXMLCellset &right)
{
	m_cellindex = right.m_cellindex;
	m_celltype = right.m_celltype;
	m_depthstart = right.m_depthstart;
	m_depthend = right.m_depthend;
	m_pingstart = right.m_pingstart;
	m_pingend = right.m_pingend;
	m_timestart = right.m_timestart;
	m_timeend = right.m_timeend;
	m_diststart = right.m_diststart;
	m_distend = right.m_distend;
	m_threshldup = right.m_threshldup;
	m_threshldlow = right.m_threshldlow;

	// NMD - FAE 118
	m_latitude = right.m_latitude;
	m_longitude = right.m_longitude;
	m_volume = right.m_volume;
	m_surface = right.m_surface;

	return *this;
}
