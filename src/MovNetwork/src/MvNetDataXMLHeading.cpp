//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLHeading
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for heading XML element
//
//=================================================================

#include "MovNetwork/MvNetDataXMLHeading.h"
//#include <iostream>

CMvNetDataXMLHeading::CMvNetDataXMLHeading()
{
	m_datetime = HacTime(0, 0);
	m_surveyname = "";
	m_comment = "";
}

CMvNetDataXMLHeading::~CMvNetDataXMLHeading()
{

}


std::string CMvNetDataXMLHeading::createXML()
{
	std::string l_XmlStr = "";
	l_XmlStr += "<header acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
	l_XmlStr += "<datetime>" + CMvNetDataXML::ToStr(m_datetime) + "</datetime>\n";
	l_XmlStr += "<surveyname>" + m_surveyname + "</surveyname>\n";
	l_XmlStr += "<comment>" + m_comment + "</comment>\n";
	l_XmlStr += "</header>\n";
	return l_XmlStr;
}

// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLHeading::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "heading"))
	{
		l_XmlStr += "<header acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		if (tramaWanted(cfg, "heading\\datetime"))
		{
			l_XmlStr += "<datetime>" + CMvNetDataXML::ToStr(m_datetime) + "</datetime>\n";
		}
		if (tramaWanted(cfg, "heading\\surveyname"))
		{
			l_XmlStr += "<surveyname>" + m_surveyname + "</surveyname>\n";
		}
		if (tramaWanted(cfg, "heading\\comment"))
		{
			l_XmlStr += "<comment>" + m_comment + "</comment>\n";
		}
		l_XmlStr += "</header>\n";
	}
	return l_XmlStr;
}

// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLHeading::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "heading"))
	{
		l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		if (tramaWanted(cfg, "heading\\datetime"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_datetime) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "heading\\surveyname"))
		{
			l_XmlStr += m_surveyname + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "heading\\comment"))
		{
			l_XmlStr += m_comment + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}


const CMvNetDataXMLHeading &	CMvNetDataXMLHeading::operator=(const CMvNetDataXMLHeading &right)
{
	m_datetime = right.m_datetime;
	m_surveyname = right.m_surveyname;
	m_comment = right.m_comment;

	return *this;
}
