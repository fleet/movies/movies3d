//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLLayer
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for layer echo-intergation XML message
//
//=================================================================


#include "MovNetwork/MvNetDataXMLLayer.h"
#include "M3DKernel/DefConstants.h"
//#include <iostream>


CMvNetDataXMLLayer::CMvNetDataXMLLayer()
{
	m_sa = XMLBadValue;
	m_sv = XMLBadValue;
	m_ni = (std::int32_t)XMLBadValue;
	m_nt = (std::int32_t)XMLBadValue;
}

CMvNetDataXMLLayer::~CMvNetDataXMLLayer()
{

}

// OTK - FAE006 - optimisation : on ne d�termine les donn�es � produire en fonction de la configuration
// qu'une fois pour tous les r�sultats
void CMvNetDataXMLLayer::ComputeDescriptor(BaseKernel::TramaDescriptor * cfg, layerDescriptor & desc)
{
	if (tramaWanted(cfg, "eilayer"))
	{
		desc.m_enabled = true;
		desc.m_acquisitionTime = true;
		desc.m_sa = tramaWanted(cfg, "eilayer\\sa") ? true : false;
		desc.m_sv = tramaWanted(cfg, "eilayer\\sv") ? true : false;
		desc.m_ni = tramaWanted(cfg, "eilayer\\ni") ? true : false;
		desc.m_nt = tramaWanted(cfg, "eilayer\\nt") ? true : false;
	}
	else
	{
		desc.m_enabled = false;
	}
}

void CMvNetDataXMLLayer::ComputeEISupervisedDescriptor(BaseKernel::TramaDescriptor * cfg, layerDescriptor & desc)
{
	if (tramaWanted(cfg, "eilayer"))
	{
		desc.m_enabled = true;
		desc.m_acquisitionTime = false;
		desc.m_sa = tramaWanted(cfg, "eilayer\\sa") ? true : false;
		desc.m_sv = tramaWanted(cfg, "eilayer\\sv") ? true : false;
		desc.m_ni = tramaWanted(cfg, "eilayer\\ni") ? true : false;
		desc.m_nt = tramaWanted(cfg, "eilayer\\nt") ? true : false;
	}
	else
	{
		desc.m_enabled = false;
	}
}

// OTK - FAE169-170
std::string CMvNetDataXMLLayer::createXMLSelectedOnly(const layerDescriptor & desc)
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		if (desc.m_acquisitionTime)
		{
			l_XmlStr += "<eilayer acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		}
		if (desc.m_sa)
		{
			l_XmlStr += "<sa>" + CMvNetDataXML::ToStr(m_sa) + "</sa>\n";
		}
		if (desc.m_sv)
		{
			l_XmlStr += "<sv>" + CMvNetDataXML::ToStr(m_sv) + "</sv>\n";
		}
		if (desc.m_ni)
		{
			l_XmlStr += "<ni>" + CMvNetDataXML::ToStr(m_ni) + "</ni>\n";
		}
		if (desc.m_nt)
		{
			l_XmlStr += "<nt>" + CMvNetDataXML::ToStr(m_nt) + "</nt>\n";
		}
		l_XmlStr += "</eilayer>\n";
	}
	return l_XmlStr;
}

// OTK - FAE169-170
std::string CMvNetDataXMLLayer::createCSV(const layerDescriptor & desc)
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		if (desc.m_acquisitionTime)
		{
			l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		}
		if (desc.m_sa)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_sa) + s_SEPARATOR;
		}
		if (desc.m_sv)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_sv) + s_SEPARATOR;
		}
		if (desc.m_ni)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_ni) + s_SEPARATOR;
		}
		if (desc.m_nt)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_nt) + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}
