//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLLayerBoterr
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for layer echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLLayerBoterr.h"
//#include <iostream>

CMvNetDataXMLLayerBoterr::CMvNetDataXMLLayerBoterr()
{
	m_sa = 0.0;
	m_ni = 0;
}

CMvNetDataXMLLayerBoterr::~CMvNetDataXMLLayerBoterr()
{

}

// OTK - FAE006 - optimisation : on ne d�termine les donn�es � produire en fonction de la configuration
// qu'une fois pour tous les r�sultats
void CMvNetDataXMLLayerBoterr::ComputeDescriptor(BaseKernel::TramaDescriptor * cfg, botterrDescriptor & desc)
{
	if (tramaWanted(cfg, "eilayer"))
	{
		desc.m_enabled = true;

		desc.m_sa = tramaWanted(cfg, "boterr\\sa") ? true : false;
		desc.m_ni = tramaWanted(cfg, "boterr\\ni") ? true : false;
	}
	else
	{
		desc.m_enabled = false;
	}
}

// OTK - FAE169-170 - selection des trames a archiver
std::string CMvNetDataXMLLayerBoterr::createXMLSelectedOnly(const botterrDescriptor & desc)
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		l_XmlStr += "<boterr acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		if (desc.m_sa)
		{
			l_XmlStr += "<sa>" + CMvNetDataXML::ToStr(m_sa) + "</sa>\n";
		}
		if (desc.m_ni)
		{
			l_XmlStr += "<ni>" + CMvNetDataXML::ToStr(m_ni) + "</ni>\n";
		}
		l_XmlStr += "</boterr>\n";
	}
	return l_XmlStr;
}

// OTK - FAE169-170 - selection des trames a archiver
std::string CMvNetDataXMLLayerBoterr::createCSV(const botterrDescriptor & desc)
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		if (desc.m_sa)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_sa) + s_SEPARATOR;
		}
		if (desc.m_ni)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_ni) + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}
