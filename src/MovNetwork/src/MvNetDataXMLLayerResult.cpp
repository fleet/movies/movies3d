//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLLayerResult
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL MVL
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for layer echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLLayerResult.h"
//#include <iostream>

CMvNetDataXMLLayerResult::CMvNetDataXMLLayerResult()
{

}

CMvNetDataXMLLayerResult::~CMvNetDataXMLLayerResult()
{

}

std::string CMvNetDataXMLLayerResult::createXML()
{
	return "";
}
