//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLShipnav
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for ship navigation XML element
//
//=================================================================

#include "MovNetwork/MvNetDataXMLShipnav.h"
//#include <iostream>

CMvNetDataXMLShipnav::CMvNetDataXMLShipnav()
{
	m_latitude = -100;
	m_longitude = -200;
	m_groundspeed = -1;
	m_groundcourse = -1;
	m_heading = -1;
	m_depth = -1;
	m_driftcourse = -1;
	m_roll = -100;
	m_pitch = -100;
	m_heave = -200;
	m_driftspeed = -1;
	m_surfspeed = -1;
	m_altitude = -10000000;
	m_draught = -1;

}


CMvNetDataXMLShipnav::~CMvNetDataXMLShipnav()
{

}

// OTK - FAE006 - optimisation : on ne d�termine les donn�es � produire en fonction de la configuration
// qu'une fois pour tous les r�sultats
void CMvNetDataXMLShipnav::ComputeDescriptor(BaseKernel::TramaDescriptor * cfg, shipnavDescriptor & desc)
{
	if (tramaWanted(cfg, "shipnav"))
	{
		desc.m_enabled = true;
		desc.m_acquisitionTime = true;
		desc.m_latitude = tramaWanted(cfg, "shipnav\\lat") ? true : false;
		desc.m_longitude = tramaWanted(cfg, "shipnav\\long") ? true : false;
		desc.m_altitude = tramaWanted(cfg, "shipnav\\alt") ? true : false;
		desc.m_groundspeed = tramaWanted(cfg, "shipnav\\gndspeed") ? true : false;
		desc.m_groundcourse = tramaWanted(cfg, "shipnav\\gndcourse") ? true : false;
		desc.m_surfspeed = tramaWanted(cfg, "shipnav\\surfspeed") ? true : false;
		desc.m_surfcourse = tramaWanted(cfg, "shipnav\\surfcourse") ? true : false;
		desc.m_driftspeed = tramaWanted(cfg, "shipnav\\driftspeed") ? true : false;
		desc.m_driftcourse = tramaWanted(cfg, "shipnav\\driftcourse") ? true : false;
		desc.m_heading = tramaWanted(cfg, "shipnav\\heading") ? true : false;
		desc.m_roll = tramaWanted(cfg, "shipnav\\roll") ? true : false;
		desc.m_pitch = tramaWanted(cfg, "shipnav\\pitch") ? true : false;
		desc.m_heave = tramaWanted(cfg, "shipnav\\heave") ? true : false;
		desc.m_depth = tramaWanted(cfg, "shipnav\\depth") ? true : false;
		desc.m_draught = tramaWanted(cfg, "shipnav\\draught") ? true : false;
	}
	else
	{
		desc.m_enabled = false;
	}
}

shipnavDescriptor CMvNetDataXMLShipnav::BuildDescriptor(BaseKernel::TramaDescriptor* tramaDescriptor)
{
	shipnavDescriptor descriptor{};
	ComputeDescriptor(tramaDescriptor, descriptor);
	return descriptor;
}

void CMvNetDataXMLShipnav::ComputeEISupervisedDescriptor(BaseKernel::TramaDescriptor * cfg, shipnavDescriptor & desc)
{
	if (tramaWanted(cfg, "shipnav"))
	{
		desc.m_enabled = true;
		desc.m_acquisitionTime = false;
		desc.m_latitude = tramaWanted(cfg, "shipnav\\lat") ? true : false;
		desc.m_longitude = tramaWanted(cfg, "shipnav\\long") ? true : false;
		desc.m_altitude = tramaWanted(cfg, "shipnav\\alt") ? true : false;
		desc.m_groundspeed = tramaWanted(cfg, "shipnav\\gndspeed") ? true : false;
		desc.m_groundcourse = tramaWanted(cfg, "shipnav\\gndcourse") ? true : false;
		desc.m_surfspeed = tramaWanted(cfg, "shipnav\\surfspeed") ? true : false;
		desc.m_surfcourse = tramaWanted(cfg, "shipnav\\surfcourse") ? true : false;
		desc.m_driftspeed = tramaWanted(cfg, "shipnav\\driftspeed") ? true : false;
		desc.m_driftcourse = tramaWanted(cfg, "shipnav\\driftcourse") ? true : false;
		desc.m_heading = tramaWanted(cfg, "shipnav\\heading") ? true : false;
		desc.m_roll = tramaWanted(cfg, "shipnav\\roll") ? true : false;
		desc.m_pitch = tramaWanted(cfg, "shipnav\\pitch") ? true : false;
		desc.m_heave = tramaWanted(cfg, "shipnav\\heave") ? true : false;
		desc.m_depth = tramaWanted(cfg, "shipnav\\depth") ? true : false;
		desc.m_draught = tramaWanted(cfg, "shipnav\\draught") ? true : false;
	}
	else
	{
		desc.m_enabled = false;
	}
}


// OTK - FAE169-170
std::string CMvNetDataXMLShipnav::createXMLSelectedOnly(shipnavDescriptor & desc)
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		if (desc.m_acquisitionTime)
		{
			l_XmlStr += "<shipnav acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		}

		//l_XmlStr += "<shipnav acquisitiontime=\""+CMvNetDataXML::ToStrExcel(m_acquisitionTime)+"\">\n";
		if (desc.m_latitude)
		{
			l_XmlStr += "<lat>" + CMvNetDataXML::ToStr(m_latitude) + "</lat>\n";
		}
		if (desc.m_longitude)
		{
			l_XmlStr += "<long>" + CMvNetDataXML::ToStr(m_longitude) + "</long>\n";
		}
		if (desc.m_altitude)
		{
			l_XmlStr += "<alt>" + CMvNetDataXML::ToStr(m_altitude) + "</alt>\n";
		}
		if (desc.m_groundspeed)
		{
			l_XmlStr += "<gndspeed>" + CMvNetDataXML::ToStr(m_groundspeed) + "</gndspeed>\n";
		}
		if (desc.m_groundcourse)
		{
			l_XmlStr += "<gndcourse>" + CMvNetDataXML::ToStr(m_groundcourse) + "</gndcourse>\n";
		}
		if (desc.m_surfspeed)
		{
			l_XmlStr += "<surfspeed>" + CMvNetDataXML::ToStr(m_surfspeed) + "</surfspeed>\n";
		}
		if (desc.m_surfcourse)
		{
			l_XmlStr += "<surfcourse>" + CMvNetDataXML::ToStr(-1) + "</surfcourse>\n";
		}
		if (desc.m_driftspeed)
		{
			l_XmlStr += "<driftspeed>" + CMvNetDataXML::ToStr(m_driftspeed) + "</driftspeed>\n";
		}
		if (desc.m_driftcourse)
		{
			l_XmlStr += "<driftcourse>" + CMvNetDataXML::ToStr(m_driftcourse) + "</driftcourse>\n";
		}
		if (desc.m_heading)
		{
			l_XmlStr += "<heading>" + CMvNetDataXML::ToStr(m_heading) + "</heading>\n";
		}
		if (desc.m_roll)
		{
			l_XmlStr += "<roll>" + CMvNetDataXML::ToStr(m_roll) + "</roll>\n";
		}
		if (desc.m_pitch)
		{
			l_XmlStr += "<pitch>" + CMvNetDataXML::ToStr(m_pitch) + "</pitch>\n";
		}
		if (desc.m_heave)
		{
			l_XmlStr += "<heave>" + CMvNetDataXML::ToStr(m_heave) + "</heave>\n";
		}
		if (desc.m_depth)
		{
			l_XmlStr += "<depth>" + CMvNetDataXML::ToStr(m_depth) + "</depth>\n";
		}
		if (desc.m_draught)
		{
			l_XmlStr += "<draught>" + CMvNetDataXML::ToStr(m_draught) + "</draught>\n";
		}
		l_XmlStr += "</shipnav>\n";
	}
	return l_XmlStr;
}

std::string CMvNetDataXMLShipnav::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::map<std::string, std::string> values;

	values["shipnav\\lat"] = CMvNetDataXML::ToStr(m_latitude);
	values["shipnav\\long"] = CMvNetDataXML::ToStr(m_longitude);
	values["shipnav\\alt"] = CMvNetDataXML::ToStr(m_altitude);
	values["shipnav\\gndspeed"] = CMvNetDataXML::ToStr(m_groundspeed);
	values["shipnav\\gndcourse"] = CMvNetDataXML::ToStr(m_groundcourse);
	values["shipnav\\surfspeed"] = CMvNetDataXML::ToStr(m_surfspeed);
	values["shipnav\\surfcourse"] = CMvNetDataXML::ToStr(0.0);
	values["shipnav\\driftspeed"] = CMvNetDataXML::ToStr(m_driftspeed);
	values["shipnav\\driftcourse"] = CMvNetDataXML::ToStr(m_driftcourse);
	values["shipnav\\heading"] = CMvNetDataXML::ToStr(m_heading);
	values["shipnav\\roll"] = CMvNetDataXML::ToStr(m_roll);
	values["shipnav\\pitch"] = CMvNetDataXML::ToStr(m_pitch);
	values["shipnav\\heave"] = CMvNetDataXML::ToStr(m_heave);
	values["shipnav\\depth"] = CMvNetDataXML::ToStr(m_depth);
	values["shipnav\\draught"] = CMvNetDataXML::ToStr(m_draught);

	std::string l_sResult = cfg->GetValuesByCat("shipnav\\", values);
	return l_sResult;
}

// OTK - FAE169-170
std::string CMvNetDataXMLShipnav::createCSV(const shipnavDescriptor & desc) const
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		if (desc.m_acquisitionTime)
		{
			l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		}
		if (desc.m_latitude)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_latitude) + s_SEPARATOR;
		}
		if (desc.m_longitude)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_longitude) + s_SEPARATOR;
		}
		if (desc.m_altitude)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_altitude) + s_SEPARATOR;
		}
		if (desc.m_groundspeed)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_groundspeed) + s_SEPARATOR;
		}
		if (desc.m_groundcourse)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_groundcourse) + s_SEPARATOR;
		}
		if (desc.m_surfspeed)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_surfspeed) + s_SEPARATOR;
		}
		if (desc.m_surfcourse)
		{
			l_XmlStr += CMvNetDataXML::ToStr(-1) + s_SEPARATOR;
		}
		if (desc.m_driftspeed)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_driftspeed) + s_SEPARATOR;
		}
		if (desc.m_driftcourse)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_driftcourse) + s_SEPARATOR;
		}
		if (desc.m_heading)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_heading) + s_SEPARATOR;
		}
		if (desc.m_roll)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_roll) + s_SEPARATOR;
		}
		if (desc.m_pitch)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_pitch) + s_SEPARATOR;
		}
		if (desc.m_heave)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_heave) + s_SEPARATOR;
		}
		if (desc.m_depth)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_depth) + s_SEPARATOR;
		}
		if (desc.m_draught)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_draught) + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}

const CMvNetDataXMLShipnav &	CMvNetDataXMLShipnav::operator=(const CMvNetDataXMLShipnav &right)
{
	m_latitude = right.m_latitude;
	m_longitude = right.m_longitude;
	m_groundspeed = right.m_groundspeed;
	m_groundcourse = right.m_groundcourse;
	m_heading = right.m_heading;
	m_depth = right.m_depth;
	m_driftcourse = right.m_driftcourse;
	m_roll = right.m_roll;
	m_pitch = right.m_pitch;
	m_heave = right.m_heave;
	m_driftspeed = right.m_driftspeed;
	m_surfspeed = right.m_surfspeed;
	m_altitude = right.m_altitude;
	m_draught = right.m_draught;

	return *this;
}
