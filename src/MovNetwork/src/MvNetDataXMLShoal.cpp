//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLShoal
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for shoal echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLShoal.h"
//#include <iostream>

CMvNetDataXMLShoal::CMvNetDataXMLShoal()
{
	m_shoalId = 0;
	m_minDepth = 0;
	m_maxDepth = 0;
	m_meanDepth = 0;
	m_minDtBot = 0;
	m_maxDtBot = 0;
	m_maxLength = 0;
	m_maxWidth = 0;
	m_maxHeight = 0;
	m_sigma_ag = 0;
	m_MVBS = 0;
	m_MVBS_WEIGHTED = 0;
	m_volume = 0;
	m_nbOfEchoes = 0;
	m_startTS = 0;
	m_endTS = 0;
	m_centerlat = 0;
	m_centerlong = 0;
	m_centerdepth = 0;
	m_maxAcrossAngle = 0;
	m_minAcrossAngle = 0;
}

CMvNetDataXMLShoal::~CMvNetDataXMLShoal()
{
}


std::string CMvNetDataXMLShoal::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "Extraction"))
	{
		l_XmlStr += "<Extraction acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		l_XmlStr += "<Extraction shoalId=\"" + CMvNetDataXML::ToStr(m_shoalId) + "\">\n";
		if (tramaWanted(cfg, "Extraction\\minDepth"))
		{
			l_XmlStr += "<minDepth>" + CMvNetDataXML::ToStr(m_minDepth) + "</minDepth>\n";
		}
		if (tramaWanted(cfg, "Extraction\\maxDepth"))
		{
			l_XmlStr += "<maxDepth>" + CMvNetDataXML::ToStr(m_maxDepth) + "</maxDepth>\n";
		}
		if (tramaWanted(cfg, "Extraction\\meanDepth"))
		{
			l_XmlStr += "<meanDepth>" + CMvNetDataXML::ToStr(m_meanDepth) + "</meanDepth>\n";
		}
		if (tramaWanted(cfg, "Extraction\\minDtBot"))
		{
			l_XmlStr += "<minDtBot>" + CMvNetDataXML::ToStr(m_minDtBot) + "</minDtBot>\n";
		}
		if (tramaWanted(cfg, "Extraction\\maxDtBot"))
		{
			l_XmlStr += "<maxDtBot>" + CMvNetDataXML::ToStr(m_maxDtBot) + "</maxDtBot>\n";
		}
		if (tramaWanted(cfg, "Extraction\\maxLength"))
		{
			l_XmlStr += "<maxLength>" + CMvNetDataXML::ToStr(m_maxLength) + "</maxLength>\n";
		}
		if (tramaWanted(cfg, "Extraction\\maxWidth"))
		{
			l_XmlStr += "<maxWidth>" + CMvNetDataXML::ToStr(m_maxWidth) + "</maxWidth>\n";
		}
		if (tramaWanted(cfg, "Extraction\\maxHeight"))
		{
			l_XmlStr += "<maxHeight>" + CMvNetDataXML::ToStr(m_maxHeight) + "</maxHeight>\n";
		}
		if (tramaWanted(cfg, "Extraction\\sigma_ag"))
		{
			l_XmlStr += "<sigma_ag>" + CMvNetDataXML::ToStr(m_sigma_ag) + "</sigma_ag>\n";
		}
		if (tramaWanted(cfg, "Extraction\\MVBS"))
		{
			l_XmlStr += "<MVBS>" + CMvNetDataXML::ToStr(m_MVBS) + "</MVBS>\n";
		}
		if (tramaWanted(cfg, "Extraction\\MVBS_WEIGHTED"))
		{
			l_XmlStr += "<MVBS_WEIGHTED>" + CMvNetDataXML::ToStr(m_MVBS_WEIGHTED) + "</MVBS_WEIGHTED>\n";
		}
		if (tramaWanted(cfg, "Extraction\\volume"))
		{
			l_XmlStr += "<volume>" + CMvNetDataXML::ToStr(m_volume) + "</volume>\n";
		}
		if (tramaWanted(cfg, "Extraction\\nbOfEchoes"))
		{
			l_XmlStr += "<nbOfEchoes>" + CMvNetDataXML::ToStr(m_nbOfEchoes) + "</nbOfEchoes>\n";
		}
		if (tramaWanted(cfg, "Extraction\\startTS"))
		{
			l_XmlStr += "<startTS>" + CMvNetDataXML::ToStrExcel(m_startTS) + "</startTS>\n";
		}
		if (tramaWanted(cfg, "Extraction\\endTS"))
		{
			l_XmlStr += "<endTS>" + CMvNetDataXML::ToStrExcel(m_endTS) + "</endTS>\n";
		}
		if (tramaWanted(cfg, "Extraction\\centerlat"))
		{
			l_XmlStr += "<centerlat>" + CMvNetDataXML::ToStr(m_centerlat) + "</centerlat>\n";
		}
		if (tramaWanted(cfg, "Extraction\\centerlong"))
		{
			l_XmlStr += "<centerlong>" + CMvNetDataXML::ToStr(m_centerlong) + "</centerlong>\n";
		}
		if (tramaWanted(cfg, "Extraction\\centerdepth"))
		{
			l_XmlStr += "<centerdepth>" + CMvNetDataXML::ToStr(m_centerdepth) + "</centerdepth>\n";
		}
		if (tramaWanted(cfg, "Extraction\\maxAcrossAngle"))
		{
			l_XmlStr += "<maxAcrossAngle>" + CMvNetDataXML::ToStr(m_maxAcrossAngle) + "</maxAcrossAngle>\n";
		}
		if (tramaWanted(cfg, "Extraction\\minAcrossAngle"))
		{
			l_XmlStr += "<minAcrossAngle>" + CMvNetDataXML::ToStr(m_minAcrossAngle) + "</minAcrossAngle>\n";
		}
		l_XmlStr += "</Extraction>\n";
	}
	return l_XmlStr;
}


// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLShoal::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "Extraction"))
	{
		l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		l_XmlStr += CMvNetDataXML::ToStr(m_shoalId) + s_SEPARATOR;
		if (tramaWanted(cfg, "Extraction\\minDepth"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_minDepth) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\maxDepth"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_maxDepth) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\meanDepth"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_meanDepth) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\minDtBot"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_minDtBot) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\maxDtBot"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_maxDtBot) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\maxLength"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_maxLength) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\maxWidth"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_maxWidth) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\maxHeight"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_maxHeight) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\sigma_ag"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_sigma_ag) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\MVBS"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_MVBS) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\MVBS_WEIGHTED"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_MVBS_WEIGHTED) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\volume"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_volume) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\nbOfEchoes"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_nbOfEchoes) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\startTS"))
		{
			l_XmlStr += CMvNetDataXML::ToStrExcel(m_startTS) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\endTS"))
		{
			l_XmlStr += CMvNetDataXML::ToStrExcel(m_endTS) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\centerlat"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_centerlat) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\centerlong"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_centerlong) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\centerdepth"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_centerdepth) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\maxAcrossAngle"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_maxAcrossAngle) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "Extraction\\minAcrossAngle"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_minAcrossAngle) + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}
