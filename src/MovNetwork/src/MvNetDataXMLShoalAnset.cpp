//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLShoalAnset
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for shoal class echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLShoalAnset.h"

#include <sstream>

CMvNetDataXMLShoalAnset::CMvNetDataXMLShoalAnset()
{
	m_verticalintegrationdistance = 0.0;
	m_alongintegrationdistance = 0.0;
	m_acrossintegrationdistance = 0.0;
	m_threshold = 0.0;
	m_minlength = 0.0;
	m_minwidth = 0.0;
	m_minheight = 0.0;
	m_saveEchoData = false;
}

CMvNetDataXMLShoalAnset::~CMvNetDataXMLShoalAnset()
{
}

std::string CMvNetDataXMLShoalAnset::createXML()
{
	std::string l_XmlStr = "";
	l_XmlStr += "<ExtractionParameters acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
	l_XmlStr += "<AlongIntDist>" + CMvNetDataXML::ToStr(m_alongintegrationdistance) + "</AlongIntDist>\n";
	l_XmlStr += "<AcrossIntDist>" + CMvNetDataXML::ToStr(m_acrossintegrationdistance) + "</AcrossIntDist>\n";
	l_XmlStr += "<VertIntDist>" + CMvNetDataXML::ToStr(m_verticalintegrationdistance) + "</VertIntDist>\n";
	l_XmlStr += "<Threshold>" + CMvNetDataXML::ToStr(m_threshold) + "</Threshold>\n";
	l_XmlStr += "<MinLength>" + CMvNetDataXML::ToStr(m_minlength) + "</MinLength>\n";
	l_XmlStr += "<MinWidth>" + CMvNetDataXML::ToStr(m_minwidth) + "</MinWidth>\n";
	l_XmlStr += "<MinHeight>" + CMvNetDataXML::ToStr(m_minheight) + "</MinHeight>\n";
	l_XmlStr += "<SaveEchoData>" + CMvNetDataXML::ToStr(m_saveEchoData) + "</SaveEchoData>\n";
	l_XmlStr += "</ExtractionParameters>\n";
	return l_XmlStr;
}


std::string CMvNetDataXMLShoalAnset::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "ExtractionParameters"))
	{
		l_XmlStr += "<ExtractionParameters acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		if (tramaWanted(cfg, "ExtractionParameters\\AlongIntDist"))
		{
			l_XmlStr += "<AlongIntDist>" + CMvNetDataXML::ToStr(m_alongintegrationdistance) + "</AlongIntDist>\n";
		}
		if (tramaWanted(cfg, "ExtractionParameters\\AcrossIntDist"))
		{
			l_XmlStr += "<AcrossIntDist>" + CMvNetDataXML::ToStr(m_acrossintegrationdistance) + "</AcrossIntDist>\n";
		}
		if (tramaWanted(cfg, "ExtractionParameters\\VertIntDist"))
		{
			l_XmlStr += "<VertIntDist>" + CMvNetDataXML::ToStr(m_verticalintegrationdistance) + "</VertIntDist>\n";
		}
		if (tramaWanted(cfg, "ExtractionParameters\\Threshold"))
		{
			l_XmlStr += "<Threshold>" + CMvNetDataXML::ToStr(m_threshold) + "</Threshold>\n";
		}
		if (tramaWanted(cfg, "ExtractionParameters\\MinLength"))
		{
			l_XmlStr += "<MinLength>" + CMvNetDataXML::ToStr(m_minlength) + "</MinLength>\n";
		}
		if (tramaWanted(cfg, "ExtractionParameters\\MinWidth"))
		{
			l_XmlStr += "<MinWidth>" + CMvNetDataXML::ToStr(m_minwidth) + "</MinWidth>\n";
		}
		if (tramaWanted(cfg, "ExtractionParameters\\MinHeight"))
		{
			l_XmlStr += "<MinHeight>" + CMvNetDataXML::ToStr(m_minheight) + "</MinHeight>\n";
		}
		if (tramaWanted(cfg, "ExtractionParameters\\SaveEchoData"))
		{
			l_XmlStr += "<SaveEchoData>" + CMvNetDataXML::ToStr(m_saveEchoData) + "</SaveEchoData>\n";
		}
		l_XmlStr += "</ExtractionParameters>\n";
	}
	return l_XmlStr;
}


std::string CMvNetDataXMLShoalAnset::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "ExtractionParameters"))
	{
		l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		if (tramaWanted(cfg, "ExtractionParameters\\AlongIntDist"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_alongintegrationdistance) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ExtractionParameters\\AcrossIntDist"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_acrossintegrationdistance) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ExtractionParameters\\VertIntDist"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_verticalintegrationdistance) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ExtractionParameters\\Threshold"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_threshold) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ExtractionParameters\\MinLength"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_minlength) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ExtractionParameters\\MinWidth"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_minwidth) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ExtractionParameters\\MinHeight"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_minheight) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ExtractionParameters\\SaveEchoData"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_saveEchoData) + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}
