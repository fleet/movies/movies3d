//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLShoalSaclass
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for shoal class echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLShoalSaclass.h"
//#include <iostream>
CMvNetDataXMLShoalSaclass::CMvNetDataXMLShoalSaclass()
{
	m_classname = "";
	m_classdef = "";
	m_sa = 0.0;
}

CMvNetDataXMLShoalSaclass::~CMvNetDataXMLShoalSaclass()
{

}


std::string CMvNetDataXMLShoalSaclass::createXML()
{
	std::string l_XmlStr = "";
	l_XmlStr += "<saclass acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
	l_XmlStr += "<classname>" + (m_classname != "" ? m_classname : "None") + "</classname>\n";
	l_XmlStr += "<classdef>" + m_classdef + "</classdef>\n";
	l_XmlStr += "<sa>" + CMvNetDataXML::ToStr(m_sa) + "</sa>\n";
	l_XmlStr += "</saclass>\n";
	return l_XmlStr;
}


std::string CMvNetDataXMLShoalSaclass::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "saclass"))
	{
		l_XmlStr += "<saclass acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		if (tramaWanted(cfg, "saclass\\classname"))
		{
			l_XmlStr += "<classname>" + (m_classname != "" ? m_classname : "None") + "</classname>\n";
		}
		if (tramaWanted(cfg, "saclass\\classdef"))
		{
			l_XmlStr += "<classdef>" + m_classdef + "</classdef>\n";
		}
		if (tramaWanted(cfg, "saclass\\sa"))
		{
			l_XmlStr += "<sa>" + CMvNetDataXML::ToStr(m_sa) + "</sa>\n";
		}
		l_XmlStr += "</saclass>\n";
	}
	return l_XmlStr;
}

std::string CMvNetDataXMLShoalSaclass::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "saclass"))
	{
		l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		if (tramaWanted(cfg, "saclass\\classname"))
		{
			l_XmlStr += (m_classname != "" ? m_classname : "None") + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "saclass\\classdef"))
		{
			l_XmlStr += m_classdef + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "saclass\\sa"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_sa) + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}