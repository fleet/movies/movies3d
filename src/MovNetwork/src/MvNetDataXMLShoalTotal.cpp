//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLShoalTotal
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for shoal class echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLShoalTotal.h"
//#include <iostream>

CMvNetDataXMLShoalTotal::CMvNetDataXMLShoalTotal()
{
	m_sa = 0.0;
	m_svt = 0.0;
	m_ni = 0;
	m_nt = 0;
	m_sashoals = 0.0;
	m_notclass = 0;
	m_sanoclass = 0.0;
}

CMvNetDataXMLShoalTotal::~CMvNetDataXMLShoalTotal()
{

}


std::string CMvNetDataXMLShoalTotal::createXML()
{
	std::string l_XmlStr = "";
	l_XmlStr += "<totshoal acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
	l_XmlStr += "<sa>" + CMvNetDataXML::ToStr(m_sa) + "</sa>\n";
	l_XmlStr += "<svt>" + CMvNetDataXML::ToStr(m_svt) + "</svt>\n";
	l_XmlStr += "<ni>" + CMvNetDataXML::ToStr(m_ni) + "</ni>\n";
	l_XmlStr += "<nt>" + CMvNetDataXML::ToStr(m_nt) + "</nt>\n";
	l_XmlStr += "<sashoals>" + CMvNetDataXML::ToStr(m_sashoals) + "</sashoals>\n";
	l_XmlStr += "<notclass>" + CMvNetDataXML::ToStr(m_notclass) + "</notclass>\n";
	l_XmlStr += "<sanoclass>" + CMvNetDataXML::ToStr(m_sanoclass) + "</sanoclass>\n";
	l_XmlStr += "</totshoal>\n";
	return l_XmlStr;
}

// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLShoalTotal::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "totshoal"))
	{
		l_XmlStr += "<totshoal acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		if (tramaWanted(cfg, "totshoal\\sa"))
		{
			l_XmlStr += "<sa>" + CMvNetDataXML::ToStr(m_sa) + "</sa>\n";
		}
		if (tramaWanted(cfg, "totshoal\\sv"))
		{
			l_XmlStr += "<sv>" + CMvNetDataXML::ToStr(m_svt) + "</sv>\n";
		}
		if (tramaWanted(cfg, "totshoal\\ni"))
		{
			l_XmlStr += "<ni>" + CMvNetDataXML::ToStr(m_ni) + "</ni>\n";
		}
		if (tramaWanted(cfg, "totshoal\\nt"))
		{
			l_XmlStr += "<nt>" + CMvNetDataXML::ToStr(m_nt) + "</nt>\n";
		}
		if (tramaWanted(cfg, "totshoal\\sashoals"))
		{
			l_XmlStr += "<sashoals>" + CMvNetDataXML::ToStr(m_sashoals) + "</sashoals>\n";
		}
		if (tramaWanted(cfg, "totshoal\\notclass"))
		{
			l_XmlStr += "<notclass>" + CMvNetDataXML::ToStr(m_notclass) + "</notclass>\n";
		}
		if (tramaWanted(cfg, "totshoal\\sanotclass"))
		{
			l_XmlStr += "<sanotclass>" + CMvNetDataXML::ToStr(m_sanoclass) + "</sanotclass>\n";
		}
		l_XmlStr += "</totshoal>\n";
	}
	return l_XmlStr;
}

// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLShoalTotal::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "totshoal"))
	{
		l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		if (tramaWanted(cfg, "totshoal\\sa"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_sa) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "totshoal\\sv"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_svt) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "totshoal\\ni"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_ni) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "totshoal\\nt"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_nt) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "totshoal\\sashoals"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_sashoals) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "totshoal\\notclass"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_notclass) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "totshoal\\sanotclass"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_sanoclass) + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}
