//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLSndset
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for sounder setting XML element
//
//=================================================================

#include "MovNetwork/MvNetDataXMLSndset.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/DefConstants.h"

CMvNetDataXMLSndset::CMvNetDataXMLSndset()
{
	m_soundername = "";
	m_sounderident = 0;
	m_softChannelId = 0;
	m_channelName = "";
	m_dataType = 0;
	m_beamType = 0;
	m_acousticFrequency = 0.0;
	m_startSample = 0;
	m_mainBeamAlongSteeringAngle = 0.0;
	m_mainBeamAthwartSteeringAngle = 0.0;
	m_absorptionCoef = 0.0;
	m_transmissionPower = 0;
	m_beamAlongAngleSensitivity = 0;
	m_beamAthwartAngleSensitivity = 0;
	m_beam3dBWidthAlong = 0.0;
	m_beam3dBWidthAthwart = 0.0;
	m_beamEquTwoWayAngle = 0.0;
	m_beamGain = 0.0;
	m_beamSACorrection = 0.0;
	m_bottomDetectionMinDepth = 0.0;
	m_bottomDetectionMaxDepth = 0.0;
	m_bottomDetectionMinLevel = 0.0;
	m_AlongTXRXWeightId = 0;
	m_AthwartTXRXWeightId = 0;
	m_SplitBeamAlongTXRXWeightId = 0;
	m_SplitBeamAthwartTXRXWeightId = 0;
	m_bandWidth = 0;
	m_tvgminrange = 0.0;
	m_tvgmaxrange = 0.0;
	m_pulseduration = 0.0;
	m_soundcelerity = 0.0;
}

CMvNetDataXMLSndset::CMvNetDataXMLSndset(std::string sndName, SoftChannel * pSoft, double pulseDur)
{
	// nom du sondeur
	m_soundername = sndName;

	// parametres du channel
	m_softChannelId = pSoft->m_softChannelId;
	m_channelName = pSoft->m_channelName;
	m_dataType = pSoft->m_dataType;
	m_beamType = pSoft->m_beamType;
	m_acousticFrequency = (double)pSoft->m_acousticFrequency / 1000.0;
	m_startSample = pSoft->m_startSample;
	m_mainBeamAlongSteeringAngle = RAD_TO_DEG(pSoft->m_mainBeamAlongSteeringAngleRad);
	m_mainBeamAthwartSteeringAngle = RAD_TO_DEG(pSoft->m_mainBeamAthwartSteeringAngleRad);
	m_absorptionCoef = pSoft->m_absorptionCoef / 10000.0;
	m_transmissionPower = pSoft->m_transmissionPower;
	m_beamAlongAngleSensitivity = pSoft->m_beamAlongAngleSensitivity;
	m_beamAthwartAngleSensitivity = pSoft->m_beamAthwartAngleSensitivity;
	m_beam3dBWidthAlong = RAD_TO_DEG(pSoft->m_beam3dBWidthAlongRad);
	m_beam3dBWidthAthwart = RAD_TO_DEG(pSoft->m_beam3dBWidthAthwartRad);
	m_beamEquTwoWayAngle = pSoft->m_beamEquTwoWayAngle;
	m_beamGain = pSoft->m_beamGain;
	m_beamSACorrection = pSoft->m_beamSACorrection;
	m_bottomDetectionMinDepth = pSoft->m_bottomDetectionMinDepth;
	m_bottomDetectionMaxDepth = pSoft->m_bottomDetectionMaxDepth;
	m_bottomDetectionMinLevel = pSoft->m_bottomDetectionMinLevel;
	m_AlongTXRXWeightId = pSoft->m_AlongTXRXWeightId;
	m_AthwartTXRXWeightId = pSoft->m_AthwartTXRXWeightId;
	m_SplitBeamAlongTXRXWeightId = pSoft->m_SplitBeamAlongTXRXWeightId;
	m_SplitBeamAthwartTXRXWeightId = pSoft->m_SplitBeamAthwartTXRXWeightId;
	m_bandWidth = pSoft->m_bandWidth;
	// OTK - FAE048 - plantage de l'EI si pas de tupel threshold
	if (pSoft->GetThreshold())
	{
		m_tvgminrange = (float)pSoft->GetThreshold()->m_TVGMinRange;
		m_tvgmaxrange = (float)pSoft->GetThreshold()->m_TVGMaxRange;
	}
	else
	{
		m_tvgminrange = 0;
		m_tvgmaxrange = 0;
	}
	m_pulseduration = (float)pulseDur / 1000.0f;

	m_soundcelerity = 0.0;
}


CMvNetDataXMLSndset::~CMvNetDataXMLSndset()
{
}

// OTK - FAE006 - optimisation : on ne d�termine les donn�es � produire en fonction de la configuration
// qu'une fois pour tous les r�sultats
void CMvNetDataXMLSndset::ComputeDescriptor(BaseKernel::TramaDescriptor * cfg, sndsetDescriptor & desc)
{
	if (tramaWanted(cfg, "sndset"))
	{
		desc.m_enabled = true;
		desc.m_acquisitionTime = true;
		desc.m_soundername = tramaWanted(cfg, "sndset\\sndname") ? true : false;
		desc.m_sounderident = tramaWanted(cfg, "sndset\\sndident") ? true : false;
		desc.m_softChannelId = tramaWanted(cfg, "sndset\\softChannelId") ? true : false;
		desc.m_channelName = tramaWanted(cfg, "sndset\\channelName") ? true : false;
		desc.m_dataType = tramaWanted(cfg, "sndset\\dataType") ? true : false;
		desc.m_beamType = tramaWanted(cfg, "sndset\\beamType") ? true : false;
		desc.m_acousticFrequency = tramaWanted(cfg, "sndset\\acousticFrequency") ? true : false;
		desc.m_startSample = tramaWanted(cfg, "sndset\\startSample") ? true : false;
		desc.m_mainBeamAlongSteeringAngle = tramaWanted(cfg, "sndset\\mainBeamAlongSteeringAngle") ? true : false;
		desc.m_mainBeamAthwartSteeringAngle = tramaWanted(cfg, "sndset\\mainBeamAthwartSteeringAngle") ? true : false;
		desc.m_absorptionCoef = tramaWanted(cfg, "sndset\\absorptionCoef") ? true : false;
		desc.m_transmissionPower = tramaWanted(cfg, "sndset\\transmissionPower") ? true : false;
		desc.m_beamAlongAngleSensitivity = tramaWanted(cfg, "sndset\\beamAlongAngleSensitivity") ? true : false;
		desc.m_beamAthwartAngleSensitivity = tramaWanted(cfg, "sndset\\beamAthwartAngleSensitivity") ? true : false;
		desc.m_beam3dBWidthAlong = tramaWanted(cfg, "sndset\\beam3dBWidthAlong") ? true : false;
		desc.m_beam3dBWidthAthwart = tramaWanted(cfg, "sndset\\beam3dBWidthAthwart") ? true : false;
		desc.m_beamEquTwoWayAngle = tramaWanted(cfg, "sndset\\beamEquTwoWayAngle") ? true : false;
		desc.m_beamGain = tramaWanted(cfg, "sndset\\beamGain") ? true : false;
		desc.m_beamSACorrection = tramaWanted(cfg, "sndset\\beamSACorrection") ? true : false;
		desc.m_bottomDetectionMinDepth = tramaWanted(cfg, "sndset\\bottomDetectionMinDepth") ? true : false;
		desc.m_bottomDetectionMaxDepth = tramaWanted(cfg, "sndset\\bottomDetectionMaxDepth") ? true : false;
		desc.m_bottomDetectionMinLevel = tramaWanted(cfg, "sndset\\bottomDetectionMinLevel") ? true : false;
		desc.m_AlongTXRXWeightId = tramaWanted(cfg, "sndset\\AlongTXRXWeightId") ? true : false;
		desc.m_AthwartTXRXWeightId = tramaWanted(cfg, "sndset\\AthwartTXRXWeightId") ? true : false;
		desc.m_SplitBeamAlongTXRXWeightId = tramaWanted(cfg, "sndset\\SplitBeamAlongTXRXWeightId") ? true : false;
		desc.m_SplitBeamAthwartTXRXWeightId = tramaWanted(cfg, "sndset\\SplitBeamAthwartTXRXWeightId") ? true : false;
		desc.m_bandWidth = tramaWanted(cfg, "sndset\\bandWidth") ? true : false;
		desc.m_tvgtype = tramaWanted(cfg, "sndset\\tvgtype") ? true : false;
		desc.m_tvgminrange = tramaWanted(cfg, "sndset\\tvgminrange") ? true : false;
		desc.m_tvgmaxrange = tramaWanted(cfg, "sndset\\tvgmaxrange") ? true : false;
		desc.m_pulseduration = tramaWanted(cfg, "sndset\\pulseduration") ? true : false;
		desc.m_soundcelerity = tramaWanted(cfg, "sndset\\soundcelerity") ? true : false;
		desc.m_slplusvr = tramaWanted(cfg, "sndset\\slplusvr") ? true : false;
		desc.m_tsgain = tramaWanted(cfg, "sndset\\tsgain") ? true : false;
	}
	else
	{
		desc.m_enabled = false;
	}
}

sndsetDescriptor CMvNetDataXMLSndset::BuildDescriptor(BaseKernel::TramaDescriptor* tramaDescriptor)
{
	sndsetDescriptor descriptor{};
	ComputeDescriptor(tramaDescriptor, descriptor);
	return descriptor;
}

void CMvNetDataXMLSndset::ComputeEISupervisedDescriptor(BaseKernel::TramaDescriptor * cfg, sndsetDescriptor & desc)
{
	if (tramaWanted(cfg, "sndset"))
	{
		desc.m_enabled = true;
		desc.m_acquisitionTime = false;
		desc.m_soundername = tramaWanted(cfg, "sndset\\sndname") ? true : false;
		desc.m_sounderident = tramaWanted(cfg, "sndset\\sndident") ? true : false;
		desc.m_softChannelId = tramaWanted(cfg, "sndset\\channelident") ? true : false;
		desc.m_channelName = tramaWanted(cfg, "sndset\\channelName") ? true : false;
		desc.m_dataType = tramaWanted(cfg, "sndset\\dataType") ? true : false;
		desc.m_beamType = tramaWanted(cfg, "sndset\\beamType") ? true : false;    //MOVIES_EISupervised\sndset\tvgtype;
		desc.m_acousticFrequency = tramaWanted(cfg, "sndset\\freq") ? true : false;
		desc.m_startSample = tramaWanted(cfg, "sndset\\startSample") ? true : false;
		desc.m_mainBeamAlongSteeringAngle = tramaWanted(cfg, "sndset\\algoffbeam") ? true : false;
		desc.m_mainBeamAthwartSteeringAngle = tramaWanted(cfg, "sndset\\athoffbeam") ? true : false;
		desc.m_absorptionCoef = tramaWanted(cfg, "sndset\\sndabsorpt") ? true : false;
		desc.m_transmissionPower = tramaWanted(cfg, "sndset\\maxpower") ? true : false;
		desc.m_beamAlongAngleSensitivity = tramaWanted(cfg, "sndset\\alganglesens") ? true : false;
		desc.m_beamAthwartAngleSensitivity = tramaWanted(cfg, "sndset\\athanglesens") ? true : false;
		desc.m_beam3dBWidthAlong = tramaWanted(cfg, "sndset\\alg3dbbeamw") ? true : false;
		desc.m_beam3dBWidthAthwart = tramaWanted(cfg, "sndset\\ath3dbbeamw") ? true : false;
		desc.m_beamEquTwoWayAngle = tramaWanted(cfg, "sndset\\twowaysbeamang") ? true : false;
		desc.m_beamGain = tramaWanted(cfg, "sndset\\svgain") ? true : false;
		desc.m_beamSACorrection = tramaWanted(cfg, "sndset\\beamSACorrection") ? true : false;
		desc.m_bottomDetectionMinDepth = tramaWanted(cfg, "sndset\\bottomDetectionMinDepth") ? true : false;
		desc.m_bottomDetectionMaxDepth = tramaWanted(cfg, "sndset\\bottomDetectionMaxDepth") ? true : false;
		desc.m_bottomDetectionMinLevel = tramaWanted(cfg, "sndset\\bottomDetectionMinLevel") ? true : false;
		desc.m_AlongTXRXWeightId = tramaWanted(cfg, "sndset\\AlongTXRXWeightId") ? true : false;
		desc.m_AthwartTXRXWeightId = tramaWanted(cfg, "sndset\\AthwartTXRXWeightId") ? true : false;
		desc.m_SplitBeamAlongTXRXWeightId = tramaWanted(cfg, "sndset\\SplitBeamAlongTXRXWeightId") ? true : false;
		desc.m_SplitBeamAthwartTXRXWeightId = tramaWanted(cfg, "sndset\\SplitBeamAthwartTXRXWeightId") ? true : false;
		desc.m_bandWidth = tramaWanted(cfg, "sndset\\bandwidth") ? true : false;
		desc.m_tvgtype = tramaWanted(cfg, "sndset\\tvgtype") ? true : false;
		desc.m_tvgminrange = tramaWanted(cfg, "sndset\\tvgminrang") ? true : false;
		desc.m_tvgmaxrange = tramaWanted(cfg, "sndset\\tvgmaxrang") ? true : false;
		desc.m_pulseduration = tramaWanted(cfg, "sndset\\pulsedur") ? true : false;
		desc.m_soundcelerity = tramaWanted(cfg, "sndset\\soundcelerity") ? true : false;
		desc.m_slplusvr = tramaWanted(cfg, "sndset\\slplusvr") ? true : false;
		desc.m_tsgain = tramaWanted(cfg, "sndset\\tsgain") ? true : false;
	}
	else
	{
		desc.m_enabled = false;
	}
}

// OTK - FAE169-170 - cretaion du fichier XML en fonction des noeuds s�lectionn�s
std::string CMvNetDataXMLSndset::createXMLSelectedOnly(const sndsetDescriptor & desc)
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		if (desc.m_acquisitionTime)
		{
			l_XmlStr += "<sndset acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		}
		if (desc.m_soundername)
		{
			l_XmlStr += "<sndname>" + m_soundername + "</sndname>\n";
		}
		if (desc.m_sounderident)
		{
			l_XmlStr += "<sndident>" + CMvNetDataXML::ToStr(m_sounderident) + "</sndident>\n";
		}
		if (desc.m_softChannelId)
		{
			l_XmlStr += "<softChannelId>" + CMvNetDataXML::ToStr(m_softChannelId) + "</softChannelId>\n";
		}
		if (desc.m_channelName)
		{
			l_XmlStr += "<channelName>" + m_channelName + "</channelName>\n";
		}
		if (desc.m_dataType)
		{
			l_XmlStr += "<dataType>" + CMvNetDataXML::ToStr(m_dataType) + "</dataType>\n";
		}
		if (desc.m_beamType)
		{
			l_XmlStr += "<beamType>" + CMvNetDataXML::ToStr(m_beamType) + "</beamType>\n";
		}
		if (desc.m_acousticFrequency)
		{
			l_XmlStr += "<acousticFrequency>" + CMvNetDataXML::ToStr(m_acousticFrequency) + "</acousticFrequency>\n";
		}
		if (desc.m_startSample)
		{
			l_XmlStr += "<startSample>" + CMvNetDataXML::ToStr(m_startSample) + "</startSample>\n";
		}
		if (desc.m_mainBeamAlongSteeringAngle)
		{
			l_XmlStr += "<mainBeamAlongSteeringAngle>" + CMvNetDataXML::ToStr(m_mainBeamAlongSteeringAngle) + "</mainBeamAlongSteeringAngle>\n";
		}
		if (desc.m_mainBeamAthwartSteeringAngle)
		{
			l_XmlStr += "<mainBeamAthwartSteeringAngle>" + CMvNetDataXML::ToStr(m_mainBeamAthwartSteeringAngle) + "</mainBeamAthwartSteeringAngle>\n";
		}
		if (desc.m_absorptionCoef)
		{
			l_XmlStr += "<absorptionCoef>" + CMvNetDataXML::ToStr(m_absorptionCoef) + "</absorptionCoef>\n";
		}
		if (desc.m_transmissionPower)
		{
			l_XmlStr += "<transmissionPower>" + CMvNetDataXML::ToStr(m_transmissionPower) + "</transmissionPower>\n";
		}
		if (desc.m_beamAlongAngleSensitivity)
		{
			l_XmlStr += "<beamAlongAngleSensitivity>" + CMvNetDataXML::ToStr(m_beamAlongAngleSensitivity) + "</beamAlongAngleSensitivity>\n";
		}
		if (desc.m_beamAthwartAngleSensitivity)
		{
			l_XmlStr += "<beamAthwartAngleSensitivity>" + CMvNetDataXML::ToStr(m_beamAthwartAngleSensitivity) + "</beamAthwartAngleSensitivity>\n";
		}
		if (desc.m_beam3dBWidthAlong)
		{
			l_XmlStr += "<beam3dBWidthAlong>" + CMvNetDataXML::ToStr(m_beam3dBWidthAlong) + "</beam3dBWidthAlong>\n";
		}
		if (desc.m_beam3dBWidthAthwart)
		{
			l_XmlStr += "<beam3dBWidthAthwart>" + CMvNetDataXML::ToStr(m_beam3dBWidthAthwart) + "</beam3dBWidthAthwart>\n";
		}
		if (desc.m_beamEquTwoWayAngle)
		{
			l_XmlStr += "<beamEquTwoWayAngle>" + CMvNetDataXML::ToStr(m_beamEquTwoWayAngle) + "</beamEquTwoWayAngle>\n";
		}
		if (desc.m_beamGain)
		{
			l_XmlStr += "<beamGain>" + CMvNetDataXML::ToStr(m_beamGain) + "</beamGain>\n";
		}
		if (desc.m_beamSACorrection)
		{
			l_XmlStr += "<beamSACorrection>" + CMvNetDataXML::ToStr(m_beamSACorrection) + "</beamSACorrection>\n";
		}
		if (desc.m_bottomDetectionMinDepth)
		{
			l_XmlStr += "<bottomDetectionMinDepth>" + CMvNetDataXML::ToStr(m_bottomDetectionMinDepth) + "</bottomDetectionMinDepth>\n";
		}
		if (desc.m_bottomDetectionMaxDepth)
		{
			l_XmlStr += "<bottomDetectionMaxDepth>" + CMvNetDataXML::ToStr(m_bottomDetectionMaxDepth) + "</bottomDetectionMaxDepth>\n";
		}
		if (desc.m_bottomDetectionMinLevel)
		{
			l_XmlStr += "<bottomDetectionMinLevel>" + CMvNetDataXML::ToStr(m_bottomDetectionMinLevel) + "</bottomDetectionMinLevel>\n";
		}
		if (desc.m_AlongTXRXWeightId)
		{
			l_XmlStr += "<AlongTXRXWeightId>" + CMvNetDataXML::ToStr(m_AlongTXRXWeightId) + "</AlongTXRXWeightId>\n";
		}
		if (desc.m_AthwartTXRXWeightId)
		{
			l_XmlStr += "<AthwartTXRXWeightId>" + CMvNetDataXML::ToStr(m_AthwartTXRXWeightId) + "</AthwartTXRXWeightId>\n";
		}
		if (desc.m_SplitBeamAlongTXRXWeightId)
		{
			l_XmlStr += "<SplitBeamAlongTXRXWeightId>" + CMvNetDataXML::ToStr(m_SplitBeamAlongTXRXWeightId) + "</SplitBeamAlongTXRXWeightId>\n";
		}
		if (desc.m_SplitBeamAthwartTXRXWeightId)
		{
			l_XmlStr += "<SplitBeamAthwartTXRXWeightId>" + CMvNetDataXML::ToStr(m_SplitBeamAthwartTXRXWeightId) + "</SplitBeamAthwartTXRXWeightId>\n";
		}
		if (desc.m_bandWidth)
		{
			l_XmlStr += "<bandWidth>" + CMvNetDataXML::ToStr(m_bandWidth) + "</bandWidth>\n";
		}
		if (desc.m_tvgtype)
		{
			l_XmlStr += "<tvgtype>" + CMvNetDataXML::ToStr(m_beamType) + "</tvgtype>\n";
		}
		if (desc.m_tvgminrange)
		{
			l_XmlStr += "<tvgminrange>" + CMvNetDataXML::ToStr(m_tvgminrange) + "</tvgminrange>\n";
		}
		if (desc.m_tvgmaxrange)
		{
			l_XmlStr += "<tvgmaxrange>" + CMvNetDataXML::ToStr(m_tvgmaxrange) + "</tvgmaxrange>\n";
		}
		if (desc.m_pulseduration)
		{
			l_XmlStr += "<pulseduration>" + CMvNetDataXML::ToStr(m_pulseduration) + "</pulseduration>\n";
		}
		if (desc.m_soundcelerity)
		{
			l_XmlStr += "<soundcelerity>" + CMvNetDataXML::ToStr(m_pulseduration) + "</soundCelerity>\n";
		}
		if (desc.m_slplusvr)
		{
			l_XmlStr = "<slplusvr>" + CMvNetDataXML::ToStr(0.0) + "</slplusvr>\n";
		}
		if (desc.m_tsgain)
		{
			l_XmlStr = "<tsgain>" + CMvNetDataXML::ToStr(0.0) + "</tsgain>\n";
		}
		l_XmlStr += "</sndset>";
	}
	return l_XmlStr;
}

std::string CMvNetDataXMLSndset::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::map<std::string, std::string> values;

	values["sndset\\sndname"] = m_soundername;
	values["sndset\\sndident"] = CMvNetDataXML::ToStr(m_sounderident);
	values["sndset\\channelident"] = CMvNetDataXML::ToStr(m_softChannelId);
	values["sndset\\channelName"] = m_channelName;
	values["sndset\\dataType"] = CMvNetDataXML::ToStr(m_dataType);
	values["sndset\\beamType"] = CMvNetDataXML::ToStr(m_beamType);
	values["sndset\\freq"] = CMvNetDataXML::ToStr(m_acousticFrequency);
	values["sndset\\startSample"] = CMvNetDataXML::ToStr(m_startSample);
	values["sndset\\algoffbeam"] = CMvNetDataXML::ToStr(m_mainBeamAlongSteeringAngle);
	values["sndset\\athoffbeam"] = CMvNetDataXML::ToStr(m_mainBeamAthwartSteeringAngle);
	values["sndset\\sndabsorpt"] = CMvNetDataXML::ToStr(m_absorptionCoef);
	values["sndset\\maxpower"] = CMvNetDataXML::ToStr(m_transmissionPower);
	values["sndset\\alganglesens"] = CMvNetDataXML::ToStr(m_beamAlongAngleSensitivity);
	values["sndset\\athanglesens"] = CMvNetDataXML::ToStr(m_beamAthwartAngleSensitivity);
	values["sndset\\alg3dbbeamw"] = CMvNetDataXML::ToStr(m_beam3dBWidthAlong);
	values["sndset\\ath3dbbeamw"] = CMvNetDataXML::ToStr(m_beam3dBWidthAthwart);
	values["sndset\\twowaysbeamang"] = CMvNetDataXML::ToStr(m_beamEquTwoWayAngle);
	values["sndset\\svgain"] = CMvNetDataXML::ToStr(m_beamGain);
	values["sndset\\beamSACorrection"] = CMvNetDataXML::ToStr(m_beamSACorrection);
	values["sndset\\bottomDetectionMinDepth"] = CMvNetDataXML::ToStr(m_bottomDetectionMinDepth);
	values["sndset\\bottomDetectionMaxDepth"] = CMvNetDataXML::ToStr(m_bottomDetectionMaxDepth);
	values["sndset\\bottomDetectionMinLevel"] = CMvNetDataXML::ToStr(m_bottomDetectionMinLevel);
	values["sndset\\AlongTXRXWeightId"] = CMvNetDataXML::ToStr(m_AlongTXRXWeightId);
	values["sndset\\AthwartTXRXWeightId"] = CMvNetDataXML::ToStr(m_AthwartTXRXWeightId);
	values["sndset\\SplitBeamAlongTXRXWeightId"] = CMvNetDataXML::ToStr(m_SplitBeamAlongTXRXWeightId);
	values["sndset\\SplitBeamAthwartTXRXWeightId"] = CMvNetDataXML::ToStr(m_SplitBeamAthwartTXRXWeightId);
	values["sndset\\bandwidth"] = CMvNetDataXML::ToStr(m_bandWidth);
	values["sndset\\tvgtype"] = CMvNetDataXML::ToStr(m_beamType);
	values["sndset\\tvgminrang"] = CMvNetDataXML::ToStr(m_tvgminrange);
	values["sndset\\tvgmaxrang"] = CMvNetDataXML::ToStr(m_tvgmaxrange);
	values["sndset\\pulsedur"] = CMvNetDataXML::ToStr(m_pulseduration);
	values["sndset\\soundcelerity"] = CMvNetDataXML::ToStr(m_soundcelerity);
	values["sndset\\slplusvr"] = CMvNetDataXML::ToStr(0.0);
	values["sndset\\tsgain"] = CMvNetDataXML::ToStr(0.0);

	std::string l_sResult = cfg->GetValuesByCat("sndset\\", values);
	return l_sResult;
}

// OTK - FAE169-170 - cretaion du fichier XML en fonction des noeuds s�lectionn�s
std::string CMvNetDataXMLSndset::createCSV(const sndsetDescriptor & desc) const
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		if (desc.m_acquisitionTime)
		{
			l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		}
		if (desc.m_soundername)
		{
			l_XmlStr += m_soundername + s_SEPARATOR;
		}
		if (desc.m_sounderident)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_sounderident) + s_SEPARATOR;
		}
		if (desc.m_softChannelId)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_softChannelId) + s_SEPARATOR;
		}
		if (desc.m_channelName)
		{
			l_XmlStr += m_channelName + s_SEPARATOR;
		}
		if (desc.m_dataType)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_dataType) + s_SEPARATOR;
		}
		if (desc.m_beamType)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_beamType) + s_SEPARATOR;
		}
		if (desc.m_acousticFrequency)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_acousticFrequency) + s_SEPARATOR;
		}
		if (desc.m_startSample)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_startSample) + s_SEPARATOR;
		}
		if (desc.m_mainBeamAlongSteeringAngle)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_mainBeamAlongSteeringAngle) + s_SEPARATOR;
		}
		if (desc.m_mainBeamAthwartSteeringAngle)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_mainBeamAthwartSteeringAngle) + s_SEPARATOR;
		}
		if (desc.m_absorptionCoef)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_absorptionCoef) + s_SEPARATOR;
		}
		if (desc.m_transmissionPower)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_transmissionPower) + s_SEPARATOR;
		}
		if (desc.m_beamAlongAngleSensitivity)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_beamAlongAngleSensitivity) + s_SEPARATOR;
		}
		if (desc.m_beamAthwartAngleSensitivity)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_beamAthwartAngleSensitivity) + s_SEPARATOR;
		}
		if (desc.m_beam3dBWidthAlong)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_beam3dBWidthAlong) + s_SEPARATOR;
		}
		if (desc.m_beam3dBWidthAthwart)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_beam3dBWidthAthwart) + s_SEPARATOR;
		}
		if (desc.m_beamEquTwoWayAngle)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_beamEquTwoWayAngle) + s_SEPARATOR;
		}
		if (desc.m_beamGain)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_beamGain) + s_SEPARATOR;
		}
		if (desc.m_beamSACorrection)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_beamSACorrection) + s_SEPARATOR;
		}
		if (desc.m_bottomDetectionMinDepth)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_bottomDetectionMinDepth) + s_SEPARATOR;
		}
		if (desc.m_bottomDetectionMaxDepth)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_bottomDetectionMaxDepth) + s_SEPARATOR;
		}
		if (desc.m_bottomDetectionMinLevel)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_bottomDetectionMinLevel) + s_SEPARATOR;
		}
		if (desc.m_AlongTXRXWeightId)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_AlongTXRXWeightId) + s_SEPARATOR;
		}
		if (desc.m_AthwartTXRXWeightId)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_AthwartTXRXWeightId) + s_SEPARATOR;
		}
		if (desc.m_SplitBeamAlongTXRXWeightId)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_SplitBeamAlongTXRXWeightId) + s_SEPARATOR;
		}
		if (desc.m_SplitBeamAthwartTXRXWeightId)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_SplitBeamAthwartTXRXWeightId) + s_SEPARATOR;
		}
		if (desc.m_bandWidth)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_bandWidth) + s_SEPARATOR;
		}
		if (desc.m_tvgtype)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_beamType) + s_SEPARATOR;
		}
		if (desc.m_tvgminrange)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_tvgminrange) + s_SEPARATOR;
		}
		if (desc.m_tvgmaxrange)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_tvgmaxrange) + s_SEPARATOR;
		}
		if (desc.m_pulseduration)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_pulseduration) + s_SEPARATOR;
		}
		if (desc.m_soundcelerity)
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_soundcelerity) + s_SEPARATOR;
		}
		if (desc.m_slplusvr)
		{
			l_XmlStr += CMvNetDataXML::ToStr(0.0) + s_SEPARATOR;
		}
		if (desc.m_tsgain)
		{
			l_XmlStr += CMvNetDataXML::ToStr(0.0) + s_SEPARATOR;
		}

	}
	return l_XmlStr;
}

// permet de remplir le bon nombre de colonnes vides si un sndset n'appartient pas au banc de poisson
std::string CMvNetDataXMLSndset::createEmptyCSV(const sndsetDescriptor & desc)
{
	std::string l_XmlStr = "";
	if (desc.m_enabled)
	{
		if (desc.m_acquisitionTime)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_soundername)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_sounderident)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_softChannelId)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_channelName)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_dataType)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_beamType)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_acousticFrequency)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_startSample)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_mainBeamAlongSteeringAngle)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_mainBeamAthwartSteeringAngle)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_absorptionCoef)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_transmissionPower)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_beamAlongAngleSensitivity)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_beamAthwartAngleSensitivity)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_beam3dBWidthAlong)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_beam3dBWidthAthwart)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_beamEquTwoWayAngle)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_beamGain)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_beamSACorrection)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_bottomDetectionMinDepth)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_bottomDetectionMaxDepth)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_bottomDetectionMinLevel)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_AlongTXRXWeightId)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_AthwartTXRXWeightId)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_SplitBeamAlongTXRXWeightId)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_SplitBeamAthwartTXRXWeightId)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_bandWidth)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_tvgtype)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_tvgminrange)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_tvgmaxrange)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_pulseduration)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_soundcelerity)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_slplusvr)
		{
			l_XmlStr += s_SEPARATOR;
		}
		if (desc.m_tsgain)
		{
			l_XmlStr += s_SEPARATOR;
		}
	}
	return l_XmlStr;
}

const CMvNetDataXMLSndset &	CMvNetDataXMLSndset::operator=(const CMvNetDataXMLSndset &right)
{
	m_soundername = right.m_soundername;
	m_sounderident = right.m_sounderident;
	m_softChannelId = right.m_softChannelId;
	m_channelName = right.m_channelName;
	m_dataType = right.m_dataType;
	m_beamType = right.m_beamType;
	m_acousticFrequency = right.m_acousticFrequency;
	m_startSample = right.m_startSample;
	m_mainBeamAlongSteeringAngle = right.m_mainBeamAlongSteeringAngle;
	m_mainBeamAthwartSteeringAngle = right.m_mainBeamAthwartSteeringAngle;
	m_absorptionCoef = right.m_absorptionCoef;
	m_transmissionPower = right.m_transmissionPower;
	m_beamAlongAngleSensitivity = right.m_beamAlongAngleSensitivity;
	m_beamAthwartAngleSensitivity = right.m_beamAthwartAngleSensitivity;
	m_beam3dBWidthAlong = right.m_beam3dBWidthAlong;
	m_beam3dBWidthAthwart = right.m_beam3dBWidthAthwart;
	m_beamEquTwoWayAngle = right.m_beamEquTwoWayAngle;
	m_beamGain = right.m_beamGain;
	m_beamSACorrection = right.m_beamSACorrection;
	m_bottomDetectionMinDepth = right.m_bottomDetectionMinDepth;
	m_bottomDetectionMaxDepth = right.m_bottomDetectionMaxDepth;
	m_bottomDetectionMinLevel = right.m_bottomDetectionMinLevel;
	m_AlongTXRXWeightId = right.m_AlongTXRXWeightId;
	m_AthwartTXRXWeightId = right.m_AthwartTXRXWeightId;
	m_SplitBeamAlongTXRXWeightId = right.m_SplitBeamAlongTXRXWeightId;
	m_SplitBeamAthwartTXRXWeightId = right.m_SplitBeamAthwartTXRXWeightId;
	m_bandWidth = right.m_bandWidth;
	m_tvgminrange = right.m_tvgminrange;
	m_tvgmaxrange = right.m_tvgmaxrange;
	m_pulseduration = right.m_pulseduration;
	m_soundcelerity = right.m_soundcelerity;

	return *this;
}
