//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLTS
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for TS echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLTS.h"
//#include <iostream>



CMvNetDataXMLTS::CMvNetDataXMLTS()
{

}

CMvNetDataXMLTS::~CMvNetDataXMLTS()
{

}

std::string CMvNetDataXMLTS::createXML()
{
	return "";
}

std::string CMvNetDataXMLTS::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	return "";
}

