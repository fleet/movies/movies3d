//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLTSAnset
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for TS echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLTSAnset.h"
//#include <iostream>



CMvNetDataXMLTSAnset::CMvNetDataXMLTSAnset()
{
	// TSSET
	int			m_minThresholdValue = 0;
	float		m_minEchoLength = 0.0;
	float		m_maxEchoLength = 0.0;
	float		m_maxGainComp = 0.0;
	float		m_maxPhaseDev = 0.0;

	// ANSET
	unsigned int	m_nbMinSucEcho = 0;
	unsigned int	m_nbMaxPing = 0;
	float			m_maxDepthDiff = 0.0;
	float			m_fishSpeed = 0.0;
	unsigned int	m_nbClasses = 0;
	float			m_step = 0.0;
	float			m_tsInf = 0.0;
	float			m_tsSup = 0.0;
}

CMvNetDataXMLTSAnset::~CMvNetDataXMLTSAnset()
{

}

std::string CMvNetDataXMLTSAnset::createXML()
{
	std::string l_XmlStr = "";
	l_XmlStr += "<ansetts acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
	l_XmlStr += "<minval>" + CMvNetDataXML::ToStr(m_minThresholdValue) + "</minval>\n";
	l_XmlStr += "<minecholength>" + CMvNetDataXML::ToStr(m_minThresholdValue) + "</minecholength>\n";
	l_XmlStr += "<maxgaincomp>" + CMvNetDataXML::ToStr(m_maxGainComp) + "</maxgaincomp>\n";
	l_XmlStr += "<maxphasedev>" + CMvNetDataXML::ToStr(m_maxPhaseDev) + "</maxphasedev>\n";
	l_XmlStr += "<minsucecho>" + CMvNetDataXML::ToStr(m_nbMinSucEcho) + "</minsucecho>\n";
	l_XmlStr += "<nbmaxping>" + CMvNetDataXML::ToStr(m_nbMaxPing) + "</nbmaxping>\n";
	l_XmlStr += "<maxdepthdiff>" + CMvNetDataXML::ToStr(m_maxDepthDiff) + "</maxdepthdiff>\n";
	l_XmlStr += "<fishspeed>" + CMvNetDataXML::ToStr(m_fishSpeed) + "</fishspeed>\n";
	l_XmlStr += "<nbclasses>" + CMvNetDataXML::ToStr(m_nbClasses) + "</nbclasses>\n";
	l_XmlStr += "<step>" + CMvNetDataXML::ToStr(m_step) + "</step>\n";
	l_XmlStr += "<tsinf>" + CMvNetDataXML::ToStr(m_tsInf) + "</tsinf>\n";
	l_XmlStr += "<tssup>" + CMvNetDataXML::ToStr(m_tsSup) + "</tssup>\n";
	l_XmlStr += "</ansetts>\n";
	return l_XmlStr;
}

// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLTSAnset::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "ansetts"))
	{
		l_XmlStr += "<ansetts acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		if (tramaWanted(cfg, "ansetts\\minval"))
		{
			l_XmlStr += "<minval>" + CMvNetDataXML::ToStr(m_minThresholdValue) + "</minval>\n";
		}
		if (tramaWanted(cfg, "ansetts\\minecholength"))
		{
			l_XmlStr += "<minecholength>" + CMvNetDataXML::ToStr(m_minThresholdValue) + "</minecholength>\n";
		}
		if (tramaWanted(cfg, "ansetts\\maxecholength"))
		{
			l_XmlStr += "<maxecholength>" + CMvNetDataXML::ToStr(m_minThresholdValue) + "</maxecholength>\n";
		}
		if (tramaWanted(cfg, "ansetts\\maxgaincomp"))
		{
			l_XmlStr += "<maxgaincomp>" + CMvNetDataXML::ToStr(m_maxGainComp) + "</maxgaincomp>\n";
		}
		if (tramaWanted(cfg, "ansetts\\maxphasedev"))
		{
			l_XmlStr += "<maxphasedev>" + CMvNetDataXML::ToStr(m_maxPhaseDev) + "</maxphasedev>\n";
		}
		if (tramaWanted(cfg, "ansetts\\minsucecho"))
		{
			l_XmlStr += "<minsucecho>" + CMvNetDataXML::ToStr(m_nbMinSucEcho) + "</minsucecho>\n";
		}
		if (tramaWanted(cfg, "ansetts\\nbmaxping"))
		{
			l_XmlStr += "<nbmaxping>" + CMvNetDataXML::ToStr(m_nbMaxPing) + "</nbmaxping>\n";
		}
		if (tramaWanted(cfg, "ansetts\\maxdepthdiff"))
		{
			l_XmlStr += "<maxdepthdiff>" + CMvNetDataXML::ToStr(m_maxDepthDiff) + "</maxdepthdiff>\n";
		}
		if (tramaWanted(cfg, "ansetts\\fishspeed"))
		{
			l_XmlStr += "<fishspeed>" + CMvNetDataXML::ToStr(m_fishSpeed) + "</fishspeed>\n";
		}
		if (tramaWanted(cfg, "ansetts\\nbclasses"))
		{
			l_XmlStr += "<nbclasses>" + CMvNetDataXML::ToStr(m_nbClasses) + "</nbclasses>\n";
		}
		if (tramaWanted(cfg, "ansetts\\step"))
		{
			l_XmlStr += "<step>" + CMvNetDataXML::ToStr(m_step) + "</step>\n";
		}
		if (tramaWanted(cfg, "ansetts\\tsinf"))
		{
			l_XmlStr += "<tsinf>" + CMvNetDataXML::ToStr(m_tsInf) + "</tsinf>\n";
		}
		if (tramaWanted(cfg, "ansetts\\tssup"))
		{
			l_XmlStr += "<tssup>" + CMvNetDataXML::ToStr(m_tsSup) + "</tssup>\n";
		}
		l_XmlStr += "</ansetts>\n";
	}
	return l_XmlStr;
}

// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLTSAnset::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "ansetts"))
	{
		l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		if (tramaWanted(cfg, "ansetts\\minval"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_minThresholdValue) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\minecholength"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_minThresholdValue) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\maxecholength"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_minThresholdValue) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\maxgaincomp"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_maxGainComp) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\maxphasedev"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_maxPhaseDev) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\minsucecho"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_nbMinSucEcho) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\nbmaxping"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_nbMaxPing) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\maxdepthdiff"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_maxDepthDiff) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\fishspeed"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_fishSpeed) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\nbclasses"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_nbClasses) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\step"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_step) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\tsinf"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_tsInf) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "ansetts\\tssup"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_tsSup) + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}
