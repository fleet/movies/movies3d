//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLTS
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for TS echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLTSLayer.h"
//#include <iostream>



CMvNetDataXMLTSLayer::CMvNetDataXMLTSLayer()
{

}

CMvNetDataXMLTSLayer::~CMvNetDataXMLTSLayer()
{

}

std::string CMvNetDataXMLTSLayer::createXML()
{
	return "";

}

std::string CMvNetDataXMLTSLayer::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	return "";
}

