//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetDataXMLTSStat
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	super class for TS tsstat echo-integration XML message
//
//===================================================================

#include "MovNetwork/MvNetDataXMLTSStat.h"
#include "M3DKernel/DefConstants.h"
//#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CMvNetDataXMLTSStat::CMvNetDataXMLTSStat()
{
	m_type = kUndefined;

	//Attributs communs entre Targets/TSi et Tracks
	m_nb = 0;
	m_tsmean = 0.0;
	m_sigma = 0.0;

	//Attributs valables uniquement pour les Tracks
	m_nbpingmean = (float)XMLBadValue;
	m_prrm = (float)XMLBadValue;
	m_dplrm = (float)XMLBadValue;
}

CMvNetDataXMLTSStat::~CMvNetDataXMLTSStat()
{

}

std::string CMvNetDataXMLTSStat::createXML()
{
	std::string l_XmlStr = "";
	l_XmlStr += "<tsstat acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";

	if (m_type == CMvNetDataXMLTSStat::kTrack)
		l_XmlStr += "<!-- track -->\n";
	else if (m_type == CMvNetDataXMLTSStat::kTarget)
		l_XmlStr += "<!-- target -->\n";
	else if (m_type == CMvNetDataXMLTSStat::kTSi)
		l_XmlStr += "<!-- tsi -->\n";

	l_XmlStr += "<type>" + CMvNetDataXML::ToStr(m_type) + "</type>\n";
	l_XmlStr += "<tsnb>" + CMvNetDataXML::ToStr(m_nb) + "</tsnb>\n";
	l_XmlStr += "<tsmean>" + CMvNetDataXML::ToStr(m_tsmean) + "</tsmean>\n";
	l_XmlStr += "<sigma>" + CMvNetDataXML::ToStr(m_sigma) + "</sigma>\n";

	// data only for track
	if (m_type == CMvNetDataXMLTSStat::kTrack)
	{
		l_XmlStr += "<meannbping>" + CMvNetDataXML::ToStr(m_nbpingmean) + "</meannbping>\n";
		l_XmlStr += "<prrm>" + CMvNetDataXML::ToStr(m_prrm) + "</prrm>\n";
		l_XmlStr += "<dplrm>" + CMvNetDataXML::ToStr(m_dplrm) + "</dplrm>\n";
	}
	l_XmlStr += "</tsstat>\n";
	return l_XmlStr;
}

// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLTSStat::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "tsstat"))
	{
		l_XmlStr += "<tsstat acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";

		if (m_type == CMvNetDataXMLTSStat::kTrack)
			l_XmlStr += "<!-- track -->\n";
		else if (m_type == CMvNetDataXMLTSStat::kTarget)
			l_XmlStr += "<!-- target -->\n";
		else if (m_type == CMvNetDataXMLTSStat::kTSi)
			l_XmlStr += "<!-- tsi -->\n";

		if (tramaWanted(cfg, "tsstat\\type"))
		{
			l_XmlStr += "<type>" + CMvNetDataXML::ToStr(m_type) + "</type>\n";
		}
		if (tramaWanted(cfg, "tsstat\\tsnb"))
		{
			l_XmlStr += "<tsnb>" + CMvNetDataXML::ToStr(m_nb) + "</tsnb>\n";
		}
		if (tramaWanted(cfg, "tsstat\\tsmean"))
		{
			l_XmlStr += "<tsmean>" + CMvNetDataXML::ToStr(m_tsmean) + "</tsmean>\n";
		}
		if (tramaWanted(cfg, "tsstat\\sigma"))
		{
			l_XmlStr += "<sigma>" + CMvNetDataXML::ToStr(m_sigma) + "</sigma>\n";
		}
		// data only for track
		if (m_type == CMvNetDataXMLTSStat::kTrack)
		{
			if (tramaWanted(cfg, "tsstat\\meannbping"))
			{
				l_XmlStr += "<meannbping>" + CMvNetDataXML::ToStr(m_nbpingmean) + "</meannbping>\n";
			}
			if (tramaWanted(cfg, "tsstat\\prrm"))
			{
				l_XmlStr += "<prrm>" + CMvNetDataXML::ToStr(m_prrm) + "</prrm>\n";
			}
			if (tramaWanted(cfg, "tsstat\\dplrm"))
			{
				l_XmlStr += "<dplrm>" + CMvNetDataXML::ToStr(m_dplrm) + "</dplrm>\n";
			}
		}
		l_XmlStr += "</tsstat>\n";
	}
	return l_XmlStr;
}



