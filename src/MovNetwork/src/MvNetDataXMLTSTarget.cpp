//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLTSTarget
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for TS Target echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLTSTarget.h"
//#include <iostream>

CMvNetDataXMLTSTarget::CMvNetDataXMLTSTarget()
{
	m_lognum = 0.0;
	m_pingnum = 0;
	m_time = HacTime(0, 0);
	m_lat = 0.0;
	m_long = 0.0;
	m_waterdepth = 0.0;
	m_targetdepth = 0.0;
	m_tscomp = 0.0;
	m_tsuncomp = 0.0;
	m_alphaalgmax = 0.0;
	m_alphaathwmax = 0.0;
}

CMvNetDataXMLTSTarget::~CMvNetDataXMLTSTarget()
{

}

std::string CMvNetDataXMLTSTarget::createXML()
{
	std::string l_XmlStr = "";
	l_XmlStr += "<tstarget acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
	l_XmlStr += "<lognum>" + CMvNetDataXML::ToStr(m_lognum) + "</lognum>\n";
	l_XmlStr += "<targettype>" + CMvNetDataXML::ToStr(m_targettype) + "</targettype>\n";
	l_XmlStr += "<pingnum>" + CMvNetDataXML::ToStr(m_pingnum) + "</pingnum>\n";
	l_XmlStr += "<datetime>" + CMvNetDataXML::ToStr(m_time) + "</datetime>\n";
	l_XmlStr += "<lat>" + CMvNetDataXML::ToStr(m_lat) + "</lat>\n";
	l_XmlStr += "<long>" + CMvNetDataXML::ToStr(m_long) + "</long>\n";
	l_XmlStr += "<waterdepth>" + CMvNetDataXML::ToStr(m_waterdepth) + "</waterdepth>\n";
	l_XmlStr += "<targetdepth>" + CMvNetDataXML::ToStr(m_targetdepth) + "</targetdepth>\n";
	l_XmlStr += "<tscomp>" + CMvNetDataXML::ToStr(m_tscomp) + "</tscomp>\n";
	l_XmlStr += "<tsuncomp>" + CMvNetDataXML::ToStr(m_tsuncomp) + "</tsuncomp>\n";
	l_XmlStr += "<alphaalgmax>" + CMvNetDataXML::ToStr(m_alphaalgmax) + "</alphaalgmax>\n";
	l_XmlStr += "<alphaathwmax>" + CMvNetDataXML::ToStr(m_alphaathwmax) + "</alphaathwmax>\n";
	l_XmlStr += "</tstarget>";
	return l_XmlStr;
}



// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLTSTarget::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "tstarget"))
	{
		l_XmlStr += "<tstarget acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		if (tramaWanted(cfg, "tstarget\\lognum"))
		{
			l_XmlStr += "<lognum>" + CMvNetDataXML::ToStr(m_lognum) + "</lognum>\n";
		}
		if (tramaWanted(cfg, "tstarget\\targettype"))
		{
			l_XmlStr += "<targettype>" + CMvNetDataXML::ToStr(m_targettype) + "</targettype>\n";
		}
		if (tramaWanted(cfg, "tstarget\\pingnum"))
		{
			l_XmlStr += "<pingnum>" + CMvNetDataXML::ToStr(m_pingnum) + "</pingnum>\n";
		}
		if (tramaWanted(cfg, "tstarget\\datetime"))
		{
			l_XmlStr += "<datetime>" + CMvNetDataXML::ToStr(m_time) + "</datetime>\n";
		}
		if (tramaWanted(cfg, "tstarget\\lat"))
		{
			l_XmlStr += "<lat>" + CMvNetDataXML::ToStr(m_lat) + "</lat>\n";
		}
		if (tramaWanted(cfg, "tstarget\\long"))
		{
			l_XmlStr += "<long>" + CMvNetDataXML::ToStr(m_long) + "</long>\n";
		}
		if (tramaWanted(cfg, "tstarget\\waterdepth"))
		{
			l_XmlStr += "<waterdepth>" + CMvNetDataXML::ToStr(m_waterdepth) + "</waterdepth>\n";
		}
		if (tramaWanted(cfg, "tstarget\\targetdepth"))
		{
			l_XmlStr += "<targetdepth>" + CMvNetDataXML::ToStr(m_targetdepth) + "</targetdepth>\n";
		}
		if (tramaWanted(cfg, "tstarget\\tscomp"))
		{
			l_XmlStr += "<tscomp>" + CMvNetDataXML::ToStr(m_tscomp) + "</tscomp>\n";
		}
		if (tramaWanted(cfg, "tstarget\\tsuncomp"))
		{
			l_XmlStr += "<tsuncomp>" + CMvNetDataXML::ToStr(m_tsuncomp) + "</tsuncomp>\n";
		}
		if (tramaWanted(cfg, "tstarget\\alphaalgmax"))
		{
			l_XmlStr += "<alphaalgmax>" + CMvNetDataXML::ToStr(m_alphaalgmax) + "</alphaalgmax>\n";
		}
		if (tramaWanted(cfg, "tstarget\\alphaathmax"))
		{
			l_XmlStr += "<alphaathwmax>" + CMvNetDataXML::ToStr(m_alphaathwmax) + "</alphaathwmax>\n";
		}
		l_XmlStr += "</tstarget>";
	}
	return l_XmlStr;
}

// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLTSTarget::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "tstarget"))
	{
		l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		if (tramaWanted(cfg, "tstarget\\lognum"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_lognum) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\targettype"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_targettype) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\pingnum"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_pingnum) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\datetime"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_time) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\lat"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_lat) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\long"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_long) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\waterdepth"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_waterdepth) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\targetdepth"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_targetdepth) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\tscomp"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_tscomp) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\tsuncomp"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_tsuncomp) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\alphaalgmax"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_alphaalgmax) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstarget\\alphaathmax"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_alphaathwmax) + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}
