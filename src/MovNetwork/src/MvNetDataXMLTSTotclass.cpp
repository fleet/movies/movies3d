//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLTSTotclass
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for TS totclass echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MVNetDataXMLTSTotclass.h"
//#include <iostream>


CMvNetDataXMLTSTotclass::CMvNetDataXMLTSTotclass()
{
	m_class = 0.0;
	m_nb = 0;
	m_type = kUndefined;
}

CMvNetDataXMLTSTotclass::~CMvNetDataXMLTSTotclass()
{

}

std::string CMvNetDataXMLTSTotclass::createXML()
{
	std::string l_XmlStr = "";
	l_XmlStr += "<tstotclass acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
	if (m_type == CMvNetDataXMLTSTotclass::kTrack)
		l_XmlStr += "<!-- track -->\n";
	else	if (m_type == CMvNetDataXMLTSTotclass::kTarget)
		l_XmlStr += "<!-- target -->\n";
	else	if (m_type == CMvNetDataXMLTSTotclass::kTSi)
		l_XmlStr += "<!-- tsi -->\n";

	l_XmlStr += "<type>" + CMvNetDataXML::ToStr(m_type) + "</type>\n";
	l_XmlStr += "<nclass>" + CMvNetDataXML::ToStr(m_class) + "</nclass>\n";
	l_XmlStr += "<tsnb>" + CMvNetDataXML::ToStr(m_nb) + "</tsnb>\n";
	l_XmlStr += "</tstotclass>\n";
	return l_XmlStr;
}

// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLTSTotclass::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "tstotclass"))
	{
		l_XmlStr += "<tstotclass acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		if (m_type == CMvNetDataXMLTSTotclass::kTrack)
			l_XmlStr += "<!-- track -->\n";
		else	if (m_type == CMvNetDataXMLTSTotclass::kTarget)
			l_XmlStr += "<!-- target -->\n";
		else	if (m_type == CMvNetDataXMLTSTotclass::kTSi)
			l_XmlStr += "<!-- tsi -->\n";
		if (tramaWanted(cfg, "tstotclass\\type"))
		{
			l_XmlStr += "<type>" + CMvNetDataXML::ToStr(m_type) + "</type>\n";
		}
		if (tramaWanted(cfg, "tstotclass\\nclass"))
		{
			l_XmlStr += "<nclass>" + CMvNetDataXML::ToStr(m_class) + "</nclass>\n";
		}
		if (tramaWanted(cfg, "tstotclass\\tsnb"))
		{
			l_XmlStr += "<tsnb>" + CMvNetDataXML::ToStr(m_nb) + "</tsnb>\n";
		}
		l_XmlStr += "</tstotclass>\n";
	}
	return l_XmlStr;
}

