//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDataXMLTSTrack
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for TS Track echo-intergation XML message
//
//=================================================================

#include "MovNetwork/MvNetDataXMLTSTrack.h"
//#include <iostream>


CMvNetDataXMLTSTrack::CMvNetDataXMLTSTrack()
{
	m_lognum = 0.0;
	m_time = HacTime(0, 0);
	m_lat = 0.0;
	m_long = 0.0;
	m_pingstart = 0;
	m_pingend = 0;
	m_nping = 0;
	m_depthstart = 0.0;
	m_depthend = 0.0;
	m_depthmean = 0.0;
	m_meanwatdepth = 0.0;
	m_tsmin = 0.0;
	m_tsmax = 0.0;
	m_tsmean = 0.0;
	m_alphaalgmax = 0.0;
	m_alphaathwmax = 0.0;
	m_resdepth = 0.0;
	m_resdispl = 0.0;
	m_trackangle = 0.0;
}

CMvNetDataXMLTSTrack::~CMvNetDataXMLTSTrack()
{

}

std::string CMvNetDataXMLTSTrack::createXML()
{
	std::string l_XmlStr = "";
	l_XmlStr += "<tstrack acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
	l_XmlStr += "<lognum>" + CMvNetDataXML::ToStr(m_lognum) + "</lognum>\n";
	l_XmlStr += "<datetime>" + CMvNetDataXML::ToStr(m_time) + "</datetime>\n";
	l_XmlStr += "<lat>" + CMvNetDataXML::ToStr(m_lat) + "</lat>\n";
	l_XmlStr += "<long>" + CMvNetDataXML::ToStr(m_long) + "</long>\n";
	l_XmlStr += "<pingstart>" + CMvNetDataXML::ToStr(m_pingstart) + "</pingstart>\n";
	l_XmlStr += "<pingend>" + CMvNetDataXML::ToStr(m_pingend) + "</pingend>\n";
	l_XmlStr += "<nping>" + CMvNetDataXML::ToStr(m_nping) + "</nping>\n";
	l_XmlStr += "<depthstart>" + CMvNetDataXML::ToStr(m_depthstart) + "</depthstart>\n";
	l_XmlStr += "<depthend>" + CMvNetDataXML::ToStr(m_depthend) + "</depthend>\n";
	l_XmlStr += "<depthmean>" + CMvNetDataXML::ToStr(m_depthmean) + "</depthmean>\n";
	l_XmlStr += "<meanwatdepth>" + CMvNetDataXML::ToStr(m_meanwatdepth) + "</meanwatdepth>\n";
	l_XmlStr += "<tsmin>" + CMvNetDataXML::ToStr(m_tsmin) + "</tsmin>\n";
	l_XmlStr += "<tsmax>" + CMvNetDataXML::ToStr(m_tsmax) + "</tsmax>\n";
	l_XmlStr += "<tsmean>" + CMvNetDataXML::ToStr(m_tsmean) + "</tsmean>\n";
	l_XmlStr += "<alphaalgmax>" + CMvNetDataXML::ToStr(m_alphaalgmax) + "</alphaalgmax>\n";
	l_XmlStr += "<alphaathwmax>" + CMvNetDataXML::ToStr(m_alphaathwmax) + "</alphaathwmax>\n";
	l_XmlStr += "<resdepth>" + CMvNetDataXML::ToStr(m_resdepth) + "</resdepth>\n";
	l_XmlStr += "<resdispl>" + CMvNetDataXML::ToStr(m_resdispl) + "</resdispl>\n";
	l_XmlStr += "<trackangle>" + CMvNetDataXML::ToStr(m_trackangle) + "</trackangle>\n";
	l_XmlStr += "</tstrack>\n";
	return l_XmlStr;
}

// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLTSTrack::createXMLSelectedOnly(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "tstrack"))
	{
		l_XmlStr += "<tstrack acquisitiontime=\"" + CMvNetDataXML::ToStrExcel(m_acquisitionTime) + "\">\n";
		if (tramaWanted(cfg, "tstrack\\lognum"))
		{
			l_XmlStr += "<lognum>" + CMvNetDataXML::ToStr(m_lognum) + "</lognum>\n";
		}
		if (tramaWanted(cfg, "tstrack\\datetime"))
		{
			l_XmlStr += "<datetime>" + CMvNetDataXML::ToStr(m_time) + "</datetime>\n";
		}
		if (tramaWanted(cfg, "tstrack\\lat"))
		{
			l_XmlStr += "<lat>" + CMvNetDataXML::ToStr(m_lat) + "</lat>\n";
		}
		if (tramaWanted(cfg, "tstrack\\long"))
		{
			l_XmlStr += "<long>" + CMvNetDataXML::ToStr(m_long) + "</long>\n";
		}
		if (tramaWanted(cfg, "tstrack\\pingstart"))
		{
			l_XmlStr += "<pingstart>" + CMvNetDataXML::ToStr(m_pingstart) + "</pingstart>\n";
		}
		if (tramaWanted(cfg, "tstrack\\pingend"))
		{
			l_XmlStr += "<pingend>" + CMvNetDataXML::ToStr(m_pingend) + "</pingend>\n";
		}
		if (tramaWanted(cfg, "tstrack\\nping"))
		{
			l_XmlStr += "<nping>" + CMvNetDataXML::ToStr(m_nping) + "</nping>\n";
		}
		if (tramaWanted(cfg, "tstrack\\depthstart"))
		{
			l_XmlStr += "<depthstart>" + CMvNetDataXML::ToStr(m_depthstart) + "</depthstart>\n";
		}
		if (tramaWanted(cfg, "tstrack\\depthend"))
		{
			l_XmlStr += "<depthend>" + CMvNetDataXML::ToStr(m_depthend) + "</depthend>\n";
		}
		if (tramaWanted(cfg, "tstrack\\depthmean"))
		{
			l_XmlStr += "<depthmean>" + CMvNetDataXML::ToStr(m_depthmean) + "</depthmean>\n";
		}
		if (tramaWanted(cfg, "tstrack\\meanwatdepth"))
		{
			l_XmlStr += "<meanwatdepth>" + CMvNetDataXML::ToStr(m_meanwatdepth) + "</meanwatdepth>\n";
		}
		if (tramaWanted(cfg, "tstrack\\tsmin"))
		{
			l_XmlStr += "<tsmin>" + CMvNetDataXML::ToStr(m_tsmin) + "</tsmin>\n";
		}
		if (tramaWanted(cfg, "tstrack\\tsmax"))
		{
			l_XmlStr += "<tsmax>" + CMvNetDataXML::ToStr(m_tsmax) + "</tsmax>\n";
		}
		if (tramaWanted(cfg, "tstrack\\tsmean"))
		{
			l_XmlStr += "<tsmean>" + CMvNetDataXML::ToStr(m_tsmean) + "</tsmean>\n";
		}
		if (tramaWanted(cfg, "tstrack\\alphaalgmax"))
		{
			l_XmlStr += "<alphaalgmax>" + CMvNetDataXML::ToStr(m_alphaalgmax) + "</alphaalgmax>\n";
		}
		if (tramaWanted(cfg, "tstrack\\alphaathmax"))
		{
			l_XmlStr += "<alphaathmax>" + CMvNetDataXML::ToStr(m_alphaathwmax) + "</alphaathmax>\n";
		}
		if (tramaWanted(cfg, "tstrack\\resdepth"))
		{
			l_XmlStr += "<resdepth>" + CMvNetDataXML::ToStr(m_resdepth) + "</resdepth>\n";
		}
		if (tramaWanted(cfg, "tstrack\\resdispl"))
		{
			l_XmlStr += "<resdispl>" + CMvNetDataXML::ToStr(m_resdispl) + "</resdispl>\n";
		}
		if (tramaWanted(cfg, "tstrack\\trackangle"))
		{
			l_XmlStr += "<trackangle>" + CMvNetDataXML::ToStr(m_trackangle) + "</trackangle>\n";
		}
		l_XmlStr += "</tstrack>\n";
	}
	return l_XmlStr;
}


// OTK - FAE169-170 - on n'archive que les trames selectionn�es
std::string CMvNetDataXMLTSTrack::createCSV(BaseKernel::TramaDescriptor * cfg)
{
	std::string l_XmlStr = "";
	if (tramaWanted(cfg, "tstrack"))
	{
		l_XmlStr += CMvNetDataXML::ToStrExcel(m_acquisitionTime) + s_SEPARATOR;
		if (tramaWanted(cfg, "tstrack\\lognum"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_lognum) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\datetime"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_time) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\lat"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_lat) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\long"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_long) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\pingstart"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_pingstart) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\pingend"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_pingend) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\nping"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_nping) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\depthstart"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_depthstart) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\depthend"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_depthend) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\depthmean"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_depthmean) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\meanwatdepth"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_meanwatdepth) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\tsmin"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_tsmin) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\tsmax"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_tsmax) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\tsmean"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_tsmean) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\alphaalgmax"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_alphaalgmax) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\alphaathmax"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_alphaathwmax) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\resdepth"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_resdepth) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\resdispl"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_resdispl) + s_SEPARATOR;
		}
		if (tramaWanted(cfg, "tstrack\\trackangle"))
		{
			l_XmlStr += CMvNetDataXML::ToStr(m_trackangle) + s_SEPARATOR;
		}
	}
	return l_XmlStr;
}
