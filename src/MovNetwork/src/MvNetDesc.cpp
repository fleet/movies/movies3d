

//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDesc
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for MOVIES XML description queries

#include "MovNetwork/MvNetDesc.h"

#include "M3DKernel/utils/log/ILogger.h"

// constants
int CMvNetDesc::DESC_QUERY_ERR = 1000;
int CMvNetDesc::DESC_QUERY_TS = 1010;
int CMvNetDesc::DESC_QUERY_LAYER = 1020;
int CMvNetDesc::DESC_QUERY_SHOAL = 1030;

#ifdef WIN32

#define WIN32_LEAN_AND_MEAN // Pour ne pas avoir le confit Xerces et msxml.h (a cause de winsock)
#define NOMINMAX
#include <winsock2.h>

namespace
{
	constexpr const char * LoggerName = "MovNetwork.MvNetDesc";
}

class CMvNetDesc::Impl
{
public:
    int				m_server_port;
    int				m_active;
	MvNetParameter	m_currConfig;
    std::string			m_xmlfile_desc_eilayer;
    std::string			m_xmlfile_desc_eishoal;
    std::string			m_xmlfile_desc_eits;
    SOCKET			m_server_sock;
    SOCKET			m_client_sock;
    SOCKADDR_IN		m_server_in;
    SOCKADDR_IN		m_client_in;
	bool			m_continue;
};


/**
 *   constructor
 */
CMvNetDesc::CMvNetDesc()
    : impl(new CMvNetDesc::Impl)
{
    impl->m_active = -1;

    impl->m_server_sock = NULL;

	// path for XML description file
	std::string l_dirPath = getDirPath();
    impl->m_xmlfile_desc_eilayer = l_dirPath + "desc_eilayer.xml";
    impl->m_xmlfile_desc_eishoal = l_dirPath + "desc_eishoal.xml";
    impl->m_xmlfile_desc_eits = l_dirPath + "desc_tsanalysis.xml";

    impl->m_server_port = impl->m_currConfig.m_netDescPort;
}

/**
 * Constructor
 */
CMvNetDesc::CMvNetDesc(MvNetParameter& config)
	: impl(new CMvNetDesc::Impl)
{
    impl->m_server_sock = NULL;

	// path for XML description file
	std::string l_dirPath = getDirPath();
    impl->m_xmlfile_desc_eilayer = l_dirPath + "desc_eilayer.xml";
    impl->m_xmlfile_desc_eishoal = l_dirPath + "desc_eishoal.xml";
    impl->m_xmlfile_desc_eits = l_dirPath + "desc_tsanalysis.xml";

	// network config 
    impl->m_currConfig = config;

	// get TCP port from config
    impl->m_server_port = impl->m_currConfig.m_netDescPort;
}



/**
 * get application path
 */
std::string CMvNetDesc::getDirPath()
{
	char Buffer[1024];
	GetModuleFileNameA(NULL, Buffer, sizeof(Buffer));
	std::string str = Buffer;
	size_t pos = str.rfind('\\');
	std::string path = str.substr(0, pos + 1);
	return path;
}

void CMvNetDesc::startThread()
{
    impl->m_active = 1;

	// create and launch thread
	launchThread();
}

/**
 *   destructor
 */
CMvNetDesc::~CMvNetDesc()
{
}


/**
 * redefine CMvNetThread method: action of thread
 */
void CMvNetDesc::internalThreadFunc()
{
	int res = 0;
	MSG msg;

	// create TCP server socket
	createSocket();

	// always run until kill message comes
	impl->m_continue = true;
	while (impl->m_continue)
	{
		// read message to kill thread
        PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
		switch (msg.message)
		{
        case WM_END_THREAD: impl->m_continue = false; break;	// END SIGNAL
		default: break;
		}

		if (impl->m_continue)
		{
			// update TCP port
            if (impl->m_server_port != impl->m_currConfig.m_netDescPort)
			{
                closesocket(impl->m_client_sock);
                closesocket(impl->m_server_sock);
                impl->m_server_port = impl->m_currConfig.m_netDescPort;
				createSocket();
			}


			//listen bloquant
			listenSocket();

			// analyse XML request and send XML response
			res = receiveRequest();
			if (res != CMvNetDesc::DESC_QUERY_ERR)
			{
				sendResponse(res);
			}

			// close client socket
            closesocket(impl->m_client_sock);
		}
	}
}


/**
 *  create & attach TCP socket on system
 */
int CMvNetDesc::createSocket()
{
	// socket config
    impl->m_server_in.sin_addr.s_addr = INADDR_ANY;
    impl->m_server_in.sin_family = AF_INET;
    impl->m_server_in.sin_port = htons(impl->m_server_port);

	// create socket
    impl->m_server_sock = socket(AF_INET, SOCK_STREAM, 0);

	// enabling reuse of addr
	int sock_opt = 1;
    setsockopt(impl->m_server_sock, SOL_SOCKET, SO_REUSEADDR, (char *)&sock_opt, sizeof(sock_opt));

	// connect socket to system	
    if (bind(impl->m_server_sock, (SOCKADDR *)&impl->m_server_in, sizeof(impl->m_server_in)) == SOCKET_ERROR)
	{
		printf("Error socket, can't bind .\n");
		return -1;
	}

	return 0;
}


/**
 * waiting for client connexion
 */
int CMvNetDesc::listenSocket()
{
	// wait clients	
    listen(impl->m_server_sock, 0);
	return 0;
}


/**
 * analyse XML description query sent from client
 */
int CMvNetDesc::receiveRequest()
{
    int l_client_in_size = sizeof(impl->m_client_in);
	// open session TCP
    if ((impl->m_client_sock = accept(impl->m_server_sock, (SOCKADDR *)&impl->m_client_in, &l_client_in_size)) != INVALID_SOCKET)
	{
		// max size for TCP
		const int TAILLE_BUF = 65536;
		char msg[TAILLE_BUF];
		ZeroMemory(msg, TAILLE_BUF);

		// receive req desc xml
        int res = recv(impl->m_client_sock, msg, sizeof(msg), 0);

		// XML request analysis		
		MvNetParameter* conf = new MvNetParameter();
		CMvNetDescQuery * desc = new CMvNetDescQuery(*conf);
		desc->setXMLBuffer(msg);
		int res_desc = desc->parseXMLQuery();
		delete desc;
		return res_desc;
	}
	return 0;
}


/**
 * send XML description file to client
 */
int CMvNetDesc::sendResponse(int desc_type)
{

	std::string l_currXMLFile;
	FILE *file;
	const int BUF_SIZE = 65536;	// max BUFFER to send data via TCP Socket
	char msg[BUF_SIZE];
	int read;

	// select XML file matching treatment
	if (desc_type == CMvNetDesc::DESC_QUERY_TS)
	{
        l_currXMLFile = impl->m_xmlfile_desc_eits;
	}
	else if (desc_type == CMvNetDesc::DESC_QUERY_LAYER)
	{
        l_currXMLFile = impl->m_xmlfile_desc_eilayer;
	}
	else if (desc_type == CMvNetDesc::DESC_QUERY_SHOAL)
	{
        l_currXMLFile = impl->m_xmlfile_desc_eishoal;
	}
	else
	{
		return -1;
	}


	ZeroMemory(msg, BUF_SIZE);
	fopen_s(&file, l_currXMLFile.c_str(), "rb");


	char tcl_error[256];
	memset(tcl_error, 0, 256);

	sprintf_s(tcl_error, "Error can't open XML description file %s", l_currXMLFile.c_str());

	if (file == NULL)
	{
		M3D_LOG_ERROR(LoggerName, tcl_error);
		return -1;
	}

	// send XML buffer response
	while ((read = fread(&msg, 1, sizeof(msg), file)))
	{
		sendStr(msg);
	}
	fclose(file);
	return 0;
}


/**
 * send string with UDP socket
 */
int CMvNetDesc::sendStr(std::string description)
{
	int sent = 0;
    if ((sent = send(impl->m_client_sock, description.c_str(), description.length(), 0)) == SOCKET_ERROR)
	{
		M3D_LOG_ERROR(LoggerName, "MvNetDesc  Error socket, can't send data.");
		return -2;
	}
	return 0;
}


/**
 * stop thread: close socket & kill thread
 */
int CMvNetDesc::stopThread()
{
	impl->m_active = 0;
	impl->m_continue = false;

	closesocket(impl->m_server_sock);
	closesocket(impl->m_client_sock);

	waitEndThread();
	return 0;
}

void CMvNetDesc::updateConfig(MvNetParameter& newConfig)
{
    impl->m_currConfig = newConfig;
}
#else

CMvNetDesc::CMvNetDesc()
{
}

CMvNetDesc::CMvNetDesc(MvNetParameter& config)
{
}

std::string CMvNetDesc::getDirPath()
{
}

void CMvNetDesc::startThread()
{
}

CMvNetDesc::~CMvNetDesc()
{
}

void CMvNetDesc::internalThreadFunc()
{
}

int CMvNetDesc::createSocket()
{
    return 0;
}

int CMvNetDesc::listenSocket()
{
    return 0;
}

int CMvNetDesc::receiveRequest()
{
    return 0;
}

int CMvNetDesc::sendResponse(int desc_type)
{
    return 0;
}

int CMvNetDesc::sendStr(std::string description)
{
    return 0;
}

int CMvNetDesc::stopThread()
{
    return 0;
}

void CMvNetDesc::updateConfig(MvNetParameter& newConfig)
{
}

#endif
