//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetDescQuery
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class of MOVIES to analyse query description

#include "MovNetwork/MvNetDescQuery.h"
#include "MovNetwork/MvNetDesc.h"

#include "M3DKernel/utils/log/ILogger.h"

#include <xercesc/parsers/SAXParser.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationRegistry.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>
#include <xercesc/dom/DOMLSParser.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMAttr.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>

XERCES_CPP_NAMESPACE_USE

namespace
{
	constexpr const char * LoggerName = "MovNetwork.MvNetDescQuery";
}

/**
 * class constructor
 */
CMvNetDescQuery::CMvNetDescQuery(MvNetParameter& config)
{
	// MOVIES network config
	m_currConfig = config;

	m_XLMQuery = "";
}

/**
 * class desructor
 */
CMvNetDescQuery::~CMvNetDescQuery()
{

}

/**
 *
 */
void CMvNetDescQuery::setXMLBuffer(char* buffer)
{
	m_XLMQuery = buffer;
}

/**
 * parse & analyze XML description query
 */
int CMvNetDescQuery::parseXMLQuery()
{
	DOMImplementation *impl = NULL;
	DOMLSParser *parser = NULL;
	DOMDocument* pDoc = NULL;
	MemBufInputSource* memBufIS = NULL;
	Wrapper4InputSource* dsrc = NULL;

	// Initialize the XML4C2 system
	try
	{
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException&)
	{
		M3D_LOG_ERROR(LoggerName, "MvNetDescQuerry  Error XML initialization");
		return 1;
	}

	// Instantiate the DOM parser.
	static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
	impl = DOMImplementationRegistry::getDOMImplementation(gLS);
	parser = ((DOMImplementationLS*)impl)->createLSParser(DOMImplementationLS::MODE_SYNCHRONOUS, 0);

	const char * gMemBufId = "descquery";


	// to parse string, need an inputSource object
	memBufIS = new MemBufInputSource((const XMLByte*)m_XLMQuery.c_str()
		, m_XLMQuery.length()
		, gMemBufId
		, false);
	dsrc = new Wrapper4InputSource(memBufIS, false);


	try
	{
		// parse XML buffer
		pDoc = parser->parse(dsrc);

		// XML nodes name
		const std::string node_descquery = "descriptionrequest";
		const std::string node_attr_xmlns = "xmlns";
		const std::string node_attr_name = "name";
		const std::string node_attr_major = "major";
		const std::string node_attr_minor = "minor";

		const std::string desc_ei_layer = "MOVIES_EILayer";
		const std::string desc_ei_shoal = "MOVIES_EIShoal";
		const std::string desc_ei_ts = "MOVIES_TSAnalysis";

		//xmlns='http://www.ifremer.fr/flotte/equipements_sc/logiciels_embarques/movies+
		DOMAttr *pAttribNode = NULL;

		// test if node MAIN_NODE exists
		DOMNodeList* pListNode = pDoc->getElementsByTagName(XMLString::transcode(node_descquery.c_str()));
		if (pListNode->getLength() == 0)
		{
			M3D_LOG_ERROR(LoggerName, "MvNetDescQuery  Incorrect XML description request.");
			return -2;
		}

		DOMNode * pNode = pListNode->item(0);
		DOMNamedNodeMap * pAttribs = pNode->getAttributes();
		if (pAttribs->getLength() == 0)
		{
			M3D_LOG_ERROR(LoggerName, "MvNetDescQuery  XML message doesn't contain any attribs.");
			return -2;
		}

		//  TEST xmlns
		pAttribNode = (DOMAttr*)pAttribs->getNamedItem(XMLString::transcode(node_attr_xmlns.c_str()));
		if (pAttribNode == NULL)
		{
			M3D_LOG_ERROR(LoggerName, "MvNetDescQuery  XML xmlns attrib doesn't exist.");
			return -3;
		}
		char * val_xmlns = XMLString::transcode(pAttribNode->getValue());

		//  TEST name
		pAttribNode = (DOMAttr*)pAttribs->getNamedItem(XMLString::transcode(node_attr_name.c_str()));
		if (pAttribNode == NULL)
		{
			M3D_LOG_ERROR(LoggerName, "MvNetDescQuery  XML name attrib doesn't exist.");
			return -4;
		}
		char * val_name = XMLString::transcode(pAttribNode->getValue());

		//  TEST major
		pAttribNode = (DOMAttr*)pAttribs->getNamedItem(XMLString::transcode(node_attr_major.c_str()));
		if (pAttribNode == NULL)
		{
			M3D_LOG_ERROR(LoggerName, "MvNetDescQuery  XML major attrib doesn't exist.");
			return -5;
		}
		int val_major = atoi(XMLString::transcode(pAttribNode->getValue()));

		//  TEST minor
		pAttribNode = (DOMAttr*)pAttribs->getNamedItem(XMLString::transcode(node_attr_minor.c_str()));
		if (pAttribNode == NULL)
		{
			M3D_LOG_ERROR(LoggerName, "MvNetDescQuery  XML minor attrib doesn't exist.");
			return -6;
		}
		int val_minor = atoi(XMLString::transcode(pAttribNode->getValue()));


		// compare echo-integration
		// LAYER
		if (!strcmp(val_name, desc_ei_layer.c_str()))
		{
			if (m_currConfig.m_eiLayerMajor == val_major &&
				m_currConfig.m_eiLayerMinor == val_minor)
			{
				return CMvNetDesc::DESC_QUERY_LAYER;
			}
			else
			{
				M3D_LOG_ERROR(LoggerName, "MvNetDescQuery  XML description request for EI Layer, bad major or minor version.");
				return -7;
			}
		}
		// SHOAL
		else if (!strcmp(val_name, desc_ei_shoal.c_str()))
		{
			if (m_currConfig.m_eiShoalMajor == val_major &&
				m_currConfig.m_eiShoalMinor == val_minor)
			{
				return CMvNetDesc::DESC_QUERY_SHOAL;
			}
			else
			{
				M3D_LOG_ERROR(LoggerName, "MvNetDescQuery  XML description request for EI Shoal, bad major or minor version.");
				return -8;
			}
		}
		// TS
		else if (!strcmp(val_name, desc_ei_ts.c_str()))
		{
			if (m_currConfig.m_TSMajor == val_major &&
				m_currConfig.m_TSMinor == val_minor)
			{
				return CMvNetDesc::DESC_QUERY_TS;
			}
			else
			{
				M3D_LOG_ERROR(LoggerName, "MvNetDescQuery  XML description request for EI TS, bad major or minor version.");
				return -9;
			}
		}
		// unknown EI
		else
		{
			return CMvNetDesc::DESC_QUERY_ERR;
		}

		delete pDoc;

	}
	catch (const OutOfMemoryException&)
	{
		M3D_LOG_ERROR(LoggerName, "MvNetDescQuery  Error XML, outofMemoryException");
		return 5;
	}
	catch (const XMLException& e)
	{
		std::string str = "MvNetDescQuery  Error XML, XMLException : ";
		str.append(XMLString::transcode(e.getMessage()));
		M3D_LOG_ERROR(LoggerName, str.c_str());
		return 4;
	}


	delete parser;
	//delete memBufIS;
	delete dsrc;
	delete pDoc;
	delete impl;

	return 0;
}
