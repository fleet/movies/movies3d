
#include "MovNetwork/MvNetParameter.h"

// D�pendances
#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************
// Constructeur par d�faut
MvNetParameter::MvNetParameter() : BaseKernel::ParameterModule("NetworkParameter")
{
	// valeurs par d�faut
	m_cfgDelay = 10;
	m_netHost = "127.0.0.1";
	m_netCfgPort = 4000;
	m_netDataPort = 4970;
	m_netDescPort = 4960;

	m_eiLayerMajor = 1;
	m_eiLayerMinor = 0;
	m_eiShoalMajor = 1;
	m_eiShoalMinor = 0;
	m_TSMajor = 1;
	m_TSMinor = 0;
}


// operateur =
const MvNetParameter & MvNetParameter::operator=(const MvNetParameter &right)
{
	m_cfgDelay = right.m_cfgDelay;
	m_netHost = right.m_netHost;
	m_netCfgPort = right.m_netCfgPort;
	m_netDataPort = right.m_netDataPort;
	m_netDescPort = right.m_netDescPort;

	m_eiLayerMajor = right.m_eiLayerMajor;
	m_eiLayerMinor = right.m_eiLayerMinor;

	return *this;
}

// Destructeur
MvNetParameter::~MvNetParameter()
{

}

// s�rialisation des param�tres dans le fichier de configuration
bool MvNetParameter::Serialize(BaseKernel::MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	// s�rialisation des donn�es membres
	movConfig->SerializeData<unsigned short>(m_cfgDelay, eUShort, "CfgDelay");
	movConfig->SerializeData<std::string>(m_netHost, eString, "NetHost");
	movConfig->SerializeData<unsigned short>(m_netCfgPort, eUShort, "CfgPort");
	movConfig->SerializeData<unsigned short>(m_netDataPort, eUShort, "DataPort");
	movConfig->SerializeData<unsigned short>(m_netDescPort, eUShort, "DescPort");
	movConfig->SerializeData<unsigned int>(m_eiLayerMajor, eUInt, "EILayerMajor");
	movConfig->SerializeData<unsigned int>(m_eiLayerMinor, eUInt, "EILayerMinor");

	movConfig->SerializePushBack();

	return true;
}

// d�s�rialisation des param�tres depuis le fichier de configuration
bool MvNetParameter::DeSerialize(BaseKernel::MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);

	// debut de la d�s�rialisation du module
	if (result)
	{
		// d�s�rialisation des donn�es membres
		result = result && movConfig->DeSerializeData<unsigned short>(&m_cfgDelay, eUShort, "CfgDelay");
		result = result && movConfig->DeSerializeData<std::string>(&m_netHost, eString, "NetHost");
		result = result && movConfig->DeSerializeData<unsigned short>(&m_netCfgPort, eUShort, "CfgPort");
		result = result && movConfig->DeSerializeData<unsigned short>(&m_netDataPort, eUShort, "DataPort");
		result = result && movConfig->DeSerializeData<unsigned short>(&m_netDescPort, eUShort, "DescPort");
		result = result && movConfig->DeSerializeData<unsigned int>(&m_eiLayerMajor, eUInt, "EILayerMajor");
		result = result && movConfig->DeSerializeData<unsigned int>(&m_eiLayerMinor, eUInt, "EILayerMinor");
	}

	movConfig->DeSerializePushBack();

	return result;
}
