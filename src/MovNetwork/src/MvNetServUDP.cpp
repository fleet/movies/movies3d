//===================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M%	CMvNetServUDP
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	: 
//
// Description:
// ===========
//	class for sending data on network by UDP protocol
//
//===================================================================

#include "MovNetwork/MvNetServUDP.h"

//d�pendances
#include <iostream>

#include "M3DKernel/utils/log/ILogger.h"

#ifdef WIN32
#include <winsock2.h>

namespace
{
	constexpr const char * LoggerName = "MovNetwork.NvNetServUDP";
}

class CMvNetServUDP::Impl
{
public:
    int			m_serverPort;
    std::string	m_serverAddr;
    SOCKET		m_serverSock;
    SOCKADDR_IN m_serverAddrIn;
};


/**
 * constructor
 */
CMvNetServUDP::CMvNetServUDP()
    : CMvNetServUDP(DEFAULT_PORT)
{
}


/**
 * constructor
 */
CMvNetServUDP::CMvNetServUDP(int a_port)
    : impl(new CMvNetServUDP::Impl)
{
	setServerPort(a_port);
}


/**
 * destructor
 */
CMvNetServUDP::~CMvNetServUDP()
{
    delete impl;
}


/**
 * define a port for socket
 */
void CMvNetServUDP::setServerPort(int a_port)
{
    impl->m_serverPort = a_port;
}


/**
 * get broadcast address from host address
 * ex: host= 192.168.1.2  broadcast= 192.168.1.255
 */
std::string getBroadcastAddr(std::string hostaddr)
{
	int pos = -1;
	std::string broadcastAddr = "";

	pos = hostaddr.rfind('.');
	broadcastAddr = hostaddr.substr(0, pos + 1);
	broadcastAddr += "255";
	return broadcastAddr;
}


/**
 * create socket, and bind it to system
 */
int CMvNetServUDP::createSocket(std::string hostAddr)
{
	// set broadcast ip from LAN settings
    impl->m_serverAddr = getBroadcastAddr(hostAddr);

	// config UDP
    impl->m_serverAddrIn.sin_addr.s_addr = inet_addr(impl->m_serverAddr.c_str());
    impl->m_serverAddrIn.sin_family = AF_INET;
    impl->m_serverAddrIn.sin_port = htons(impl->m_serverPort);

	// create UDP Socket
    impl->m_serverSock = socket(AF_INET, SOCK_DGRAM, 0);

	// activate Broadcast mode
	std::string broadcast = "yes";
	//int optval;
    if (setsockopt(impl->m_serverSock, SOL_SOCKET, SO_BROADCAST, broadcast.c_str(), broadcast.length()) == SOCKET_ERROR)
	{
		M3D_LOG_ERROR(LoggerName, "Error: can't set socket UDP option broadcast.");
		return -4;
	}

	return 0;
}


/**
 * sending data with socket
 */
int CMvNetServUDP::sendData(std::string data)
{
    int res = sendto(impl->m_serverSock, data.c_str(), data.length(), 0, (SOCKADDR*)&impl->m_serverAddrIn, sizeof(impl->m_serverAddrIn));
	if (res == SOCKET_ERROR)
	{
        std::cout << "Error on sending data (port:" << impl->m_serverPort << ")." << std::endl;
		return -3;
	}
	return res;
}

/**
 * close socket and clean winsock
 */
int CMvNetServUDP::closeSocket()
{
    if (closesocket(impl->m_serverSock) == SOCKET_ERROR)
	{
        std::cout << "Error on closing socket (port:" << impl->m_serverPort << ")." << std::endl;
		return -2;
	}
	return 0;
}


int CMvNetServUDP::getServerPort()
{
    return impl->m_serverPort;
}


std::string CMvNetServUDP::getServerAddr()
{
    return impl->m_serverAddr;
}

#else // TODO Implémentation pour Linux

CMvNetServUDP::CMvNetServUDP()
    : CMvNetServUDP(DEFAULT_PORT)
{
}

CMvNetServUDP::CMvNetServUDP(int a_port)
{
}

CMvNetServUDP::~CMvNetServUDP()
{
}

void CMvNetServUDP::setServerPort(int a_port)
{
}

std::string getBroadcastAddr(std::string hostaddr)
{
    return {};
}

int CMvNetServUDP::createSocket(std::string hostAddr)
{
    return 0;
}

int CMvNetServUDP::sendData(std::string data)
{
    return 0;
}

int CMvNetServUDP::closeSocket()
{
    return 0;
}

int CMvNetServUDP::getServerPort()
{
    return 0;
}

std::string CMvNetServUDP::getServerAddr()
{
    return {};
}

#endif
