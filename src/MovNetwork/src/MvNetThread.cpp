//=================================================================
// Project : MOVIES Module : - Copyright (C) 2006 IFREMER
//
// CLASS		: %M% CMvNetThread
// LANGUAGE		: ANSI C++
// OS			: W95
// Author		: ATL PTJ
// Version		: %I%
// Date 		: %G%
// History	
//
// Description:
// ===========
//	basic class for thread
//
//=================================================================

#include <thread>
#include "MovNetwork/MvNetThread.h"

class CMvNetThread::Impl
{
public:
    std::thread m_thread;
};

/**
 * constructor
 */
CMvNetThread::CMvNetThread()
    : impl(new CMvNetThread::Impl)
{
}

/**
 * destructor
 */
CMvNetThread::~CMvNetThread()
{
	waitEndThread();
    delete impl;
}

/**
 * method executed in new thread
 */
void CMvNetThread::ThreadFunc(CMvNetThread* pThis)
{
	pThis->internalThreadFunc();
}

/**
 * create a thread
 */
void CMvNetThread::launchThread()
{
    impl->m_thread = std::thread(&ThreadFunc, this);
}

bool CMvNetThread::joinable()
{
    return impl->m_thread.joinable();
}

/**
 * create a thread
 */
void CMvNetThread::waitEndThread()
{
    if(impl->m_thread.joinable()){
        impl->m_thread.join();
    }
}


