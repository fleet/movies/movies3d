#pragma once
#include "NoiseLevelExport.h"
#include "M3DKernel/module/ProcessModule.h"

#include "NoiseLevelParameters.h"
#include "NoiseLevelResult.h"

class NOISELEVEL_API NoiseLevelModule : public ProcessModule
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	MovCreateMacro(NoiseLevelModule);

	virtual ~NoiseLevelModule();
	
	virtual void PingFanAdded(PingFan *pFan);
	virtual void ESUStart(ESUParameter * pWorkingESU);
	virtual void ESUEnd(ESUParameter * pWorkingESU, bool abort);

	virtual void SounderChanged(std::uint32_t sounderId);
	virtual void StreamClosed(const char *streamName);
	virtual void StreamOpened(const char *streamName);

	// IPSIS - OTK - on doit pouvoir d�sactiver un module de traitement
	// � la fin de l'ESU suivant
	virtual void DisableForNextESU();

	// les modules doivent �galement disposer d'un accesseur vers
	// leur param�tres poru que le outputManager puisse
	// savoir comment �mettre les r�sultats
	virtual BaseKernel::ParameterModule& GetParameterModule() override;
	NoiseLevelParameters& GetNoiseLevelParameters();
	
	// Get noise level results for all sounder on a given esu
	std::map<int, NoiseLevelResult> getResults(ESUParameter * pEsu) const;

	// Get noise level result for a specific sounder on a given esu
	NoiseLevelResult getResultForSounder(ESUParameter * pEsu, int iSounder) const;

private:

	class Impl;
	Impl * impl;

	// Constructeur
	NoiseLevelModule();
};

