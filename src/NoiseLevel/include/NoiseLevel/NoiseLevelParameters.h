#pragma once

#include "NoiseLevelExport.h"
#include "M3DKernel/parameter/ParameterModule.h"


class NOISELEVEL_API NoiseLevelParameters : public BaseKernel::ParameterModule
{
public:
	typedef std::map<double, double> ReferenceValues;

	NoiseLevelParameters();
	~NoiseLevelParameters();

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	void setRange(int minRange, int maxRange) { m_minRange = minRange; m_maxRange = maxRange; }
	int getMinRange() const { return m_minRange; }
	int getMaxRange() const { return m_maxRange; }

	void setSvThreshold(double threshold) { m_nlThreshold = threshold; }
	double getSvThreshold() const { return m_nlThreshold; }

	void setEnabled(bool value) { m_enabled = value; }
	bool isEnabled() const { return m_enabled; }

	const std::string & getReferenceValuesFilePath() const { return m_referenceValuesFilePath; }
	void setReferenceValuesFilePath(const std::string & filePath);

	const std::string & getReferenceName() const { return m_referenceName; }
	const std::map<int, ReferenceValues> & getReferenceValues() const { return m_referenceValues; }

private:

	bool loadReferenceValues();

	int m_minRange = 10;
	int m_maxRange = 100;
	double m_nlThreshold = -50.0;
	bool m_enabled = false;

	std::string m_referenceValuesFilePath;
	
	std::string m_referenceName;
	std::map<int, ReferenceValues> m_referenceValues;
};

