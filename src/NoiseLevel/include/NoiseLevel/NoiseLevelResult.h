#pragma once

#include "NoiseLevelExport.h"

#include <map>

class NOISELEVEL_API NoiseLevelResult
{
public:
	// map of passive pings count for each transducer (indexed by accoustic frequency)
	std::map<int, int> nbPassivePings;	
	
	// map of noise level count indexed by frequency
	std::map<int, double> noiseLevels;

	// map of ref noise level count indexed by frequency
	std::map<int, double> refNoiseLevels;

	// map of range count for each transducer (indexed by accoustic frequency)
	std::map<int, double> ranges;

	// map of ref range for each transducer (indexed by accoustic frequenc)
	std::map<int, double> refRanges;
};