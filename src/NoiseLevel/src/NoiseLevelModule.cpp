
#include "NoiseLevel/NoiseLevelModule.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/PingFanIterator.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/TransmitSignalObject.h"
#include "M3DKernel/datascheme/SignalFilteringObject.h"
#include "M3DKernel/datascheme/TransformSet.h"

#include "M3DKernel/datascheme/Environnement.h"

#include <algorithm>
#include <iterator>

namespace
{
	constexpr const char * LoggerName = "NoiseLevelModule.NoiseLevelModule";
}

const std::vector<double> me70TransmissionPower = { 15.8437 ,17.3916, 17.1748 ,15.9917 ,15.1948, 14.9090, 15.3041 ,15.5477, 13.9020, 10.9221, 8.4910, 9.9890, 12.0286, 14.8537, 15.7367, 15.0501, 14.9077, 15.5394, 16.5049, 17.5460, 16.8998 };

struct BeamNoiseLevelData
{
	BeamDataObject * beam;
	int acousticFrequency;
	int minRangeIndex;
	int maxRangeIndex;

	struct FreqPoint
	{
		double fixedNoisePart;
		double averagePower;
		double svOffset;
		double absorptionCoefficient;
		double beamSampleSpacing;

		FreqPoint()
			: fixedNoisePart(0)
			, averagePower(0)
			, svOffset(0)
			, absorptionCoefficient(0)
			, beamSampleSpacing(0)
		{
		}
	};

	std::map<int, FreqPoint> freqPoints;
};


namespace dict
{
	template <class Key, class Value>
	Value get(const std::map<Key, Value> & dict, const Key & key, const Value & defaultValue = Value())
	{
		const auto it = dict.find(key);
		if (it != dict.cend())
			return it->second;
		return defaultValue;
	}
}

struct ChannelNoiseLevelData
{
	std::vector<BeamNoiseLevelData> datas;
	
	inline void addData(const BeamNoiseLevelData & data)
	{
		datas.push_back(data);
	}	
	
	void runComputation(const double & svRef) const
	{
		int nbPassive = 0;
		int idxMax = -1;
		//double meanPower = 0.0;

		std::map<int, double> meanPower;

		// calcul du power moyen sur tous les pings passifs
        for(const auto & data : datas)
		{
			if (data.beam->m_transMode == BeamDataObject::PassiveTransducerMode)
			{
				nbPassive += 1;

				for (auto it : data.freqPoints)
				{
					double freqMeanPower = dict::get(meanPower, it.first, 0.0);
					freqMeanPower += pow(10.0, it.second.averagePower * 0.1);
					meanPower[it.first] = freqMeanPower;
				}

				// TODO use freq for maxRangeIndex ?
				idxMax = std::max(idxMax, data.maxRangeIndex);
			}
		}
		
		M3D_LOG_INFO(LoggerName, Log::format("Nb passif pings on ESU : %d", nbPassive));

		if (nbPassive == 0)
			return;

		// TODO ?
		//if (idxMax < 0)
		//	return;

		for (auto & it : meanPower)
		{
			double freqMeanPower = it.second;
			freqMeanPower /= nbPassive;
			freqMeanPower = 10 * log10(freqMeanPower);
			it.second = freqMeanPower;
		}					

		for (const BeamNoiseLevelData & data : datas)
		{
			if (data.beam->m_transMode == BeamDataObject::PassiveTransducerMode)
			{
				std::map<int, double> averageNoiseLevel = meanPower;

				for (auto & it : averageNoiseLevel)
				{
					it.second += dict::get(data.freqPoints, it.first).fixedNoisePart;
				}

				data.beam->setAverageNoiseLevel(averageNoiseLevel);
			}
		}

		// portée min de toute les fréquences
		int idx = -1;

		for (auto it : meanPower)
		{
			std::vector<int> svCount(idxMax + 1, 0);
			std::vector<double> sv(idxMax + 1, 0.0);
			
			// calcul des svmoy sur la colonne d'eau sur tous les pings passifs
			for (const BeamNoiseLevelData & data : datas)
			{
				if (data.beam->m_transMode == BeamDataObject::PassiveTransducerMode)
				{
					for (int i = data.minRangeIndex; i <= data.maxRangeIndex; ++i)
					{
						auto freqPoint = dict::get(data.freqPoints, it.first);

						double svMoy = it.second;
						double r = (i + 1) * freqPoint.beamSampleSpacing;
						svMoy += 20 * log10(r) + 2 * freqPoint.absorptionCoefficient * r;
						svMoy += freqPoint.svOffset;

						sv[i] += svMoy;
						svCount[i] += 1;
					}
				}
			}

			// recherche de la portée pour la fréquence
			int freqIdx = -1;
			for (int i = 0; i < idxMax; ++i)
			{
				int count = svCount[i];
				if (count > 0)
				{
					double value = sv[i] / count;
					if (value >= svRef)
					{
						freqIdx = i;
						break;
					}
				}
			}

			if (freqIdx != -1)
			{
				if (idx != -1)
				{
					idx = std::min(idx, freqIdx);
				}
				else
				{
					idx = freqIdx;
				}
			}
		}
		
		// assignation de la portée au pings
		if (idx != -1)
		{
            for(const BeamNoiseLevelData & data : datas)
			{
				data.beam->setNoiseRangeEchoNum(idx);
			}
		}
	}
};

struct TransducerNoiseLevelData
{
	std::map<unsigned short, ChannelNoiseLevelData> datas;

	inline void addData(unsigned short channelId, const BeamNoiseLevelData & data)
	{
		datas[channelId].addData(data);
	}
	
	void runComputation(const double & svRef) const
	{
        for(const auto & itData : datas)
		{
			itData.second.runComputation(svRef);
		}
	}
};

struct SounderNoiseLevelData
{
	std::map<unsigned int, TransducerNoiseLevelData> datas;
	
	inline void addData(unsigned int transducerIdx, unsigned short channelId, const BeamNoiseLevelData & data)
	{
		datas[transducerIdx].addData(channelId, data);
	}

	void runComputation(const double & svRef) const
	{
        for(const auto & itData : datas)
		{
			itData.second.runComputation(svRef);
		}
	}
};

struct ESUNoiseLevelData
{
	std::map<std::uint32_t, SounderNoiseLevelData> datas;

	void clear() { datas.clear(); }
	
	inline void addData(std::uint32_t sounderId, unsigned int transducerIdx, unsigned short channelId, const BeamNoiseLevelData & data)
	{
		datas[sounderId].addData(transducerIdx, channelId, data);
	}

	void runComputation(const double & svRef) const
	{
        for(const auto & itData : datas)
		{
			itData.second.runComputation(svRef);
		}
	}
};


class NoiseLevelModule::Impl
{
public:
	// indique si le traitement doit s'arr�ter � la fin de l'ESU courant
	bool m_DisableNextESU;

	// indique si un ESU est encours
	bool m_ESURunning;

	bool m_initialized;

	NoiseLevelParameters m_parameters;
	ESUNoiseLevelData esuNoiseLevelDatas;

	void estimateRangeIndex(Sounder *sounder, int transducerIndex, int channelIndex, PingFan * pFan, int & minRangeIndex, int & maxRangeIndex);
	
	void estimateRangeIndex(Sounder *sounder, int transducerIndex, int channelId, PingFan * pFan, SpectralAnalysisDataObject * spectralAnalysis, int & minRangeIndex, int & maxRangeIndex);
};

NoiseLevelModule::NoiseLevelModule(void)
	: ProcessModule("NoiseLevelModule")
	, impl(new NoiseLevelModule::Impl)
{
	impl->m_ESURunning = false;
	impl->m_DisableNextESU = false;
	impl->m_initialized = false;

	setEnable(true);
}

NoiseLevelModule::~NoiseLevelModule()
{
	delete impl;
}

void NoiseLevelModule::Impl::estimateRangeIndex(Sounder *sounder, int transducerIndex, int channelIndex, PingFan * pFan
	, int & minRangeIndex, int & maxRangeIndex)
{
	int minRange = m_parameters.getMinRange();
	int maxRange = m_parameters.getMaxRange();

	Transducer * pTrans = sounder->GetTransducer(transducerIndex);
	MemoryStruct *pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(transducerIndex);
	auto svValueSize = pMemStruct->GetDataFmt()->getSize();
	
	double beamSampleSpacing = pTrans->getBeamsSamplesSpacing();
	for (auto i = 0; i < svValueSize.y; ++i)
	{
		auto vecRange = sounder->GetPolarToGeoCoord(pFan, transducerIndex, channelIndex, i - pTrans->GetSampleOffset());
		auto range = vecRange.z;

		if (minRangeIndex == -1 && range >= minRange)
			minRangeIndex = i;

		if (maxRangeIndex == -1 && range >= maxRange)
			maxRangeIndex = i;

		if (minRangeIndex != -1 && maxRangeIndex != -1)
			break;
	}

	if (minRangeIndex == -1) minRangeIndex = 0;
	if (maxRangeIndex == -1) maxRangeIndex = svValueSize.y - 1;
}


void NoiseLevelModule::Impl::estimateRangeIndex(Sounder *sounder, int transducerIndex, int channelId, PingFan * pFan, SpectralAnalysisDataObject * spectralAnalysis, int & minRangeIndex, int & maxRangeIndex)
{
	Transducer * pTrans = sounder->GetTransducer(transducerIndex);
	SoftChannel * pSoftChan = pTrans->getSoftChannel(channelId);
	double offset = pTrans->GetSampleOffset() * pTrans->getBeamsSamplesSpacing();
	double compensateHeave = 0.0;
	if (pSoftChan)
	{
		compensateHeave = pFan->getHeaveChan(pSoftChan->m_softChannelId);
	}
	double depthOfffset = compensateHeave + offset;

	int minRange = m_parameters.getMinRange();
	int maxRange = m_parameters.getMaxRange();

	const auto numberOfRanges = spectralAnalysis->numberOfRanges();
	for (auto i = 0; i < numberOfRanges; ++i)
	{
		auto range = spectralAnalysis->rangeForIndex(i) + depthOfffset;

		if (minRangeIndex == -1 && range >= minRange)
			minRangeIndex = i;

		if (maxRangeIndex == -1 && range >= maxRange)
			maxRangeIndex = i;

		if (minRangeIndex != -1 && maxRangeIndex != -1)
			break;
	}

	if (minRangeIndex == -1) minRangeIndex = 0;
	if (maxRangeIndex == -1) maxRangeIndex = numberOfRanges -1;
}

BaseKernel::ParameterModule & NoiseLevelModule::GetParameterModule() { return impl->m_parameters; }
NoiseLevelParameters & NoiseLevelModule::GetNoiseLevelParameters() { return impl->m_parameters; }

void NoiseLevelModule::PingFanAdded(PingFan * pFan)
{
	if (!impl->m_parameters.isEnabled())
		return;

	TimeCounter timeCounter;
	timeCounter.StartCount();

	M3DKernel *pKern = M3DKernel::GetInstance();
	HacObjectMgr *pHacObj = pKern->getObjectMgr();
	TimeObjectContainer *pContainer = pHacObj->GetEnvironnementContainer();
	MovObjectPtr<Environnement> pEnv = (Environnement*)pContainer->FindDatedObject(pFan->m_ObjectTime);
	if (!pEnv || pEnv->GetNumberOfMeasure() == 0)
	{
		M3D_LOG_WARN(LoggerName, "No environmental data for noise analysis, use default environment values");
		pEnv = MovObjectPtr<Environnement>::make();

		const auto & kernelParameters = M3DKernel::GetInstance()->GetRefKernelParameter();

		Measure meas;
		meas.Temperature = kernelParameters.getTemperature();
		meas.SpeedOfSound = kernelParameters.getSpeedOfSound();
		meas.Salinity = kernelParameters.getSalinity();
		pEnv->AddMeasure(meas);
	}

	Sounder *sounder = pFan->m_pSounder;
	for (auto transducerIndex = 0; transducerIndex < sounder->GetTransducerCount(); ++transducerIndex)
	{
		Transducer * pTrans = sounder->GetTransducer(transducerIndex);

		auto channelIndex = 0;
		for (const auto &channelId : pTrans->GetChannelId())
		{			
			SoftChannel * pSoftChan = pTrans->getSoftChannel(channelId);
			if (pSoftChan)
			{
				auto beam = pFan->getBeam(channelId)->second;
												
				//Finds the range where the noise level should be calculated					
				BeamNoiseLevelData noiseLevelData;
				noiseLevelData.acousticFrequency = pSoftChan->m_acousticFrequency;
				noiseLevelData.beam = beam;

				int minRangeIndex = -1;
				int maxRangeIndex = -1;
				auto spectralAnalysis = pFan->getSpectralAnalysisDataObject(channelId);
				if (spectralAnalysis != nullptr)
				{
					impl->estimateRangeIndex(sounder, transducerIndex, channelId, pFan, spectralAnalysis, minRangeIndex, maxRangeIndex);
				}
				else
				{
					impl->estimateRangeIndex(sounder, transducerIndex, channelIndex, pFan, minRangeIndex, maxRangeIndex);
				}

				noiseLevelData.minRangeIndex = minRangeIndex;
				noiseLevelData.maxRangeIndex = maxRangeIndex;

				if (beam->m_transMode == BeamDataObject::PassiveTransducerMode)
				{						
					if (minRangeIndex >= 0 && maxRangeIndex > minRangeIndex)
					{
						std::map<int, double> averagePower;
						if (spectralAnalysis != nullptr)
						{
							int freqRefIdx = -1;

							const double beamSampleSpacing = pTrans->getBeamsSamplesSpacing();

							const auto nbFreq = spectralAnalysis->numberOfFrequencies();
							const auto bandwidth = spectralAnalysis->frequencyForIndex(nbFreq - 1) - spectralAnalysis->frequencyForIndex(0);
							for (int iFreq = 0; iFreq < nbFreq; ++iFreq)
							{
								const double freq = spectralAnalysis->frequencyForIndex(iFreq);
								const auto lambda = sounder->m_soundVelocity / freq;
								const auto DI = -(pSoftChan->m_beamEquTwoWayAngle - 20 * log10(freq / pSoftChan->m_acousticFrequency)) + 10 * log10(41000 / 5800);
			
								TransmitSignalObject * pTransmitSignal = pSoftChan->m_softChannelComputeData.GetTransmitSignalObject(pTrans, pSoftChan);
								double effectivePulseLength = pTransmitSignal->GetEffectivePulseLength() * pow(10, (2.0 * pSoftChan->m_beamSACorrection) / 10.0);
								double T = ((double)pTrans->m_pulseDuration) / 1000000.0;
								
								const auto fixedNoisePart =
									-10.0 * log10((lambda * lambda) / (4.0 * PI))
									- 10.0 * log10(bandwidth * T) - (pSoftChan->m_beamGain + 20 * log10(freq / pSoftChan->m_acousticFrequency)) + DI + 181.8;

								double transmissionPower = pSoftChan->m_transmissionPower;
									
								double absorptionCoefficient = pEnv->GetMeasure(0)->ComputeAbsorptionCoefficient(freq);
								
								double svOffset = -10.0 * log10(transmissionPower*pow(lambda, 2.0) * sounder->m_soundVelocity / (32.0 * PI*PI))
									//- 2.0 * (pSoftChan->m_beamGain + 20 * log10(freq / pSoftChan->m_acousticFrequency))
									- 10.0 * log10(effectivePulseLength)
									- 2 * pSoftChan->m_beamSACorrection
									- (pSoftChan->m_beamEquTwoWayAngle - 20 * log10(freq / pSoftChan->m_acousticFrequency) );

								auto averageSv = 0.0;								
								for (auto i = minRangeIndex; i <= maxRangeIndex; ++i)
								{
									auto sv = spectralAnalysis->sv(i, iFreq);
									double r = spectralAnalysis->firstRange+i*spectralAnalysis->rangeInterval;
									sv -= 20 * log10(r) + 2 * absorptionCoefficient * r;
									sv = pow(10.0, sv * 0.1);

									averageSv += sv;
								}
								averageSv /= (maxRangeIndex - minRangeIndex);
								double beamAveragePower = 10 * log10(averageSv) - svOffset;

								BeamNoiseLevelData::FreqPoint freqPoint;
								freqPoint.absorptionCoefficient = absorptionCoefficient;
								freqPoint.beamSampleSpacing = beamSampleSpacing;
								freqPoint.svOffset = svOffset;
								freqPoint.fixedNoisePart = fixedNoisePart;
								freqPoint.averagePower = beamAveragePower;

								noiseLevelData.freqPoints[freq] = freqPoint;

								// estimation de la portée de référence
								if (minRangeIndex >= 0 && maxRangeIndex > minRangeIndex)
								{
									const double svRef = impl->m_parameters.getSvThreshold();
									const std::map<int, NoiseLevelParameters::ReferenceValues> & refValues = impl->m_parameters.getReferenceValues();
									NoiseLevelParameters::ReferenceValues sounderRefValues;
									auto itSounderRefValues = refValues.find(sounder->m_SounderId);
									if (itSounderRefValues != refValues.cend())
									{
										sounderRefValues = itSounderRefValues->second;
									}

									auto itFreq = sounderRefValues.find(pSoftChan->m_acousticFrequency);
									if (itFreq != sounderRefValues.cend())
									{
										for (int i = minRangeIndex; i <= maxRangeIndex; ++i)
										{
											double svMoy = itFreq->second;
											double r = (i + 1) * pTrans->getBeamsSamplesSpacing();
											svMoy += 20 * log10(r) + 2 * absorptionCoefficient * r;
											svMoy += svOffset;

											if (svMoy >= svRef)
											{
												if (freqRefIdx == -1)
												{
													freqRefIdx = i;
												}
												else
												{
													freqRefIdx = std::min(freqRefIdx, i);
												}
												break;
											}
										}
									}
								}
							}

							if (freqRefIdx != -1)
							{
								beam->setRefNoiseRangeEchoNum(freqRefIdx);
							}
						}
						else
						{
							auto pMemStruct = pFan->GetMemorySetRef()->GetMemoryStruct(transducerIndex);
							auto svValues = ((short*)pMemStruct->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(channelIndex, 0)));
							auto svValueSize = pMemStruct->GetDataFmt()->getSize();

							const double beamSampleSpacing = pTrans->getBeamsSamplesSpacing();
							const double absorptionCoefficient = pSoftChan->m_absorptionCoef * 0.0001 * 0.001;

							//Computes the noise level
							double frequencyCenter = (pSoftChan->m_startFrequency + pSoftChan->m_endFrequency) / 2.0;
							if (frequencyCenter == 0)
								frequencyCenter = pSoftChan->m_acousticFrequency;

							const auto lambda = sounder->m_soundVelocity / frequencyCenter;	//longueur d'onde du signal accounstique, use frequencyCenter or frequency ?
							const auto DI = -(pSoftChan->m_beamEquTwoWayAngle - 20 * log10(frequencyCenter / pSoftChan->m_acousticFrequency)) + 10 * log10(41000 / 5800);
							const auto T = ((double)pTrans->m_pulseDuration) / 1000000.0;			//longueur d�impulsion du signal acoustique

							const auto fixedNoisePart =
								-10.0 * log10((lambda * lambda) / (4.0 * PI))
								- 10.0 * log10(1 / T) - (pSoftChan->m_beamGain + 20 * log10(frequencyCenter / pSoftChan->m_acousticFrequency)) + DI + 181.8;

							//Computes the power from the Sv
							double effectivePulseLength = 1.0;
							if (pTrans->m_pulseShape == 2)
							{
								TransmitSignalObject * pTransmitSignal = pSoftChan->m_softChannelComputeData.GetTransmitSignalObject(pTrans, pSoftChan);
								effectivePulseLength = pTransmitSignal->GetEffectivePulseLength();
								effectivePulseLength = effectivePulseLength * pow(10, (2.0 * pSoftChan->m_beamSACorrection) / 10.0);
							}
							else
							{
								effectivePulseLength = T * pow(10, (2.0 * pSoftChan->m_beamSACorrection) / 10.0);
							}

							double transmissionPower = pSoftChan->m_transmissionPower;
							if (transmissionPower == 0)
							{
								if (channelIndex < me70TransmissionPower.size())
									transmissionPower = me70TransmissionPower[channelIndex];
							}

							//Warning: m_transmissionPower can be 0 on ER60 and EK80 
							double svOffset = -10.0 * log10(transmissionPower*pow(lambda, 2.0) * sounder->m_soundVelocity / (32.0 * PI*PI))
								- 2.0 * (pSoftChan->m_beamGain + 20 * log10(frequencyCenter / pSoftChan->m_acousticFrequency))
								- 10.0 * log10(effectivePulseLength)
								- 2 * pSoftChan->m_beamSACorrection
								- (pSoftChan->m_beamEquTwoWayAngle - 20 * log10(frequencyCenter / pSoftChan->m_acousticFrequency));
							
							bool hasPower = false;
							auto beamAveragePower = beam->computeAveragePower(hasPower, minRangeIndex, maxRangeIndex);

							if (!hasPower)
							{
								auto averageSv = 0.0;
								for (auto i = minRangeIndex; i <= maxRangeIndex; ++i)
								{
									auto sv = svValues[i] * 0.01;

									double r = i * beamSampleSpacing;
									sv -= 20 * log10(r) + 2 * absorptionCoefficient * r;

									sv = pow(10.0, sv * 0.1);

									averageSv += sv;
								}
								averageSv /= (maxRangeIndex - minRangeIndex);
								beamAveragePower = 10 * log10(averageSv) - svOffset;
								hasPower = true;
							}

							BeamNoiseLevelData::FreqPoint freqPoint;
							freqPoint.absorptionCoefficient = absorptionCoefficient;
							freqPoint.beamSampleSpacing = beamSampleSpacing;
							freqPoint.svOffset = svOffset;
							freqPoint.fixedNoisePart = fixedNoisePart;
							freqPoint.averagePower = beamAveragePower;

							noiseLevelData.freqPoints[pSoftChan->m_acousticFrequency] = freqPoint;

							beam->clearPowers(); //Clear the power values, they won't be used naymore

							// estimation de la portée de référence
							if (minRangeIndex >= 0 && maxRangeIndex > minRangeIndex)
							{
								const double svRef = impl->m_parameters.getSvThreshold();
								const std::map<int, NoiseLevelParameters::ReferenceValues> & refValues = impl->m_parameters.getReferenceValues();
								NoiseLevelParameters::ReferenceValues sounderRefValues;
								auto itSounderRefValues = refValues.find(sounder->m_SounderId);
								if (itSounderRefValues != refValues.cend())
								{
									sounderRefValues = itSounderRefValues->second;
								}

								auto itFreq = sounderRefValues.find(pSoftChan->m_acousticFrequency);
								if (itFreq != sounderRefValues.cend())
								{
									for (int i = minRangeIndex; i <= maxRangeIndex; ++i)
									{
										double svMoy = itFreq->second;
										double r = (i + 1) * pTrans->getBeamsSamplesSpacing();
										svMoy += 20 * log10(r) + 2 * absorptionCoefficient * r;
										svMoy += svOffset;

										if (svMoy >= svRef)
										{
											beam->setRefNoiseRangeEchoNum(i);
											break;
										}
									}
								}
							}
						}

						++channelIndex;
					}
				}				
				
				impl->esuNoiseLevelDatas.addData(sounder->m_SounderId, transducerIndex, channelId, noiseLevelData);
			}
		}
	}

	timeCounter.StopCount();
	M3DKernel::GetInstance()->m_timeCounterRecord.AddTimeCounter("NoiseLevel fanAdded", timeCounter);
}

void NoiseLevelModule::ESUStart(ESUParameter * pWorkingESU)
{
	// clear temps datas
	impl->esuNoiseLevelDatas.clear();
}

void NoiseLevelModule::ESUEnd(ESUParameter * pWorkingESU, bool abort)
{
	if (!impl->m_parameters.isEnabled())
		return;

	TimeCounter timeCounter;
	timeCounter.StartCount();

	auto startTime = pWorkingESU->GetESUTime();
	auto endTime = pWorkingESU->GetESUEndTime();

	// R�cuperation des 
	M3DKernel * pKernel = M3DKernel::GetInstance();
	pKernel->Lock();

	// Compute datas
	double svRef = impl->m_parameters.getSvThreshold();
	impl->esuNoiseLevelDatas.runComputation(svRef);
	impl->esuNoiseLevelDatas.clear();
	
	pKernel->Unlock();

	timeCounter.StopCount();
	M3DKernel::GetInstance()->m_timeCounterRecord.AddTimeCounter("NoiseLevel esuClosed", timeCounter);
}

void NoiseLevelModule::DisableForNextESU()
{
}

void NoiseLevelModule::SounderChanged(std::uint32_t sounderId)
{
}

void NoiseLevelModule::StreamClosed(const char * streamName)
{
}

void NoiseLevelModule::StreamOpened(const char * streamName)
{
}

std::map<int, NoiseLevelResult> NoiseLevelModule::getResults(ESUParameter * pESU) const
{
	std::map<int, NoiseLevelResult> res;


	auto pKernel = M3DKernel::GetInstance();
	const auto & soundersDef = pKernel->getObjectMgr()->GetSounderDefinition();
	for (unsigned int iSounder = 0; iSounder < soundersDef.GetNbSounder(); iSounder++)
	{
		res[iSounder] = getResultForSounder(pESU, iSounder);
	}

	return res;
}

NoiseLevelResult NoiseLevelModule::getResultForSounder(ESUParameter * pESU, int iSounder) const
{
	NoiseLevelResult res;

	auto pKernel = M3DKernel::GetInstance();
	const auto & soundersDef = pKernel->getObjectMgr()->GetSounderDefinition();
	auto sounder = soundersDef.GetSounder(iSounder);
	if (sounder == nullptr)
		return res;

	auto startTime = pESU->GetESUTime();
	auto endTime = pESU->GetESUEndTime();

	const auto & noiseLevelParameters = impl->m_parameters;
	const auto & refValues = noiseLevelParameters.getReferenceValues();

	auto pingFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(sounder->m_SounderId);

	std::map<int, int> nbPings;
	std::map<int, int> nbPingsFM;
	std::map<int, double> averageNoise;
	std::map<int, double> averageRange;
	std::map<int, double> averageRefRange;

	for (auto transducerIndex = 0; transducerIndex < sounder->GetTransducerCount(); ++transducerIndex)
	{
		auto transducer = sounder->GetTransducer(transducerIndex);
		for (const auto &channelId : transducer->GetChannelId())
		{
			auto channel = transducer->getSoftChannel(channelId);
			bool hasData = false;

			// find sounder ref values
			NoiseLevelParameters::ReferenceValues sounderRefValues;
			auto itSounderRefValues = refValues.find(sounder->m_SounderId);
			if (itSounderRefValues != refValues.cend())
			{
				sounderRefValues = itSounderRefValues->second;
			}

			// find ref value;
			auto itFreq = sounderRefValues.find(channel->m_acousticFrequency);
			if (itFreq != sounderRefValues.cend())
			{
				//
				auto lambda = sounder->m_soundVelocity / channel->m_acousticFrequency;
				auto eta = 0.55;
				auto T = ((double)transducer->m_pulseDuration) / 1000000.0;

				const double fixedNoisePart = -10.0 * log10((lambda * lambda) / (4.0 * PI)) - 10.0 * log10(1 / T) - 10.0 * log10(eta) + 181.8;
				res.refNoiseLevels[channel->m_acousticFrequency] = itFreq->second + fixedNoisePart;
			}

			for (int pingFanIndex = 0; pingFanIndex < pingFanContainer->GetObjectCount(); ++pingFanIndex)
			{
				auto pingFan = (PingFan*)pingFanContainer->GetDatedObject(pingFanIndex);
				auto beam = pingFan->getBeam(channelId)->second;
				auto softChan = sounder->getSoftChannel(channelId);

				if (pingFan->m_ObjectTime >= startTime
					&& pingFan->m_ObjectTime <= endTime
					&& beam->m_transMode == BeamDataObject::PassiveTransducerMode)
				{
					auto nl = beam->getAverageNoiseLevel();
					for (auto it = nl.cbegin(); it != nl.cend(); ++it)
					{
						averageNoise[it->first] += it->second;
						nbPingsFM[it->first] += 1;
					}

					nbPings[channel->m_acousticFrequency] += 1;

					int range = beam->getNoiseRangeEchoNum();
					if (range != -1)
					{
						averageRange[channel->m_acousticFrequency] += range * transducer->getBeamsSamplesSpacing();
					}

					int refRange = beam->getRefNoiseRangeEchoNum();
					if (refRange != -1)
					{
						averageRefRange[channel->m_acousticFrequency] += refRange * transducer->getBeamsSamplesSpacing();
					}
				}
			}
		}
	}

	res.nbPassivePings = nbPings;

	for (auto it = nbPings.cbegin(); it != nbPings.cend(); ++it)
	{
		averageRange[it->first] /= it->second;
		averageRefRange[it->first] /= it->second;
	}

	res.ranges = averageRange;
	res.refRanges = averageRefRange;

	for (auto it = averageNoise.cbegin(); it != averageNoise.cend(); ++it)
	{
		const auto nl = it->second;
		const bool isValid = !isinf(nl) && !isnan(nl);
		if (isValid)
		{
			res.noiseLevels[it->first] = (nl / nbPingsFM[it->first]);
		}
	}

	return res;
}