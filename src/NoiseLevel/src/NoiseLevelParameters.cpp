
#include "NoiseLevel/NoiseLevelParameters.h"
#include "M3DKernel/config/MovConfig.h"
#include "M3DKernel/parameter/ParameterSerializable.h"
#include "M3DKernel/parameter/XmlChUtils.h"

#include <memory>

#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/sax2/Attributes.hpp>
#include <xercesc/util/XMLString.hpp>

XERCES_CPP_NAMESPACE_USE

using namespace BaseKernel;

NoiseLevelParameters::NoiseLevelParameters() : ParameterModule("NoiseLevelParameter")
{
}


NoiseLevelParameters::~NoiseLevelParameters()
{
}

// sauvegarde des param�tres dans le fichier de configuration associ�
bool NoiseLevelParameters::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	movConfig->SerializeData(m_enabled, eBool, "Enabled");
	movConfig->SerializeData(m_minRange, eInt, "MinRange");
	movConfig->SerializeData(m_maxRange, eInt, "MaxRange");
	movConfig->SerializeData(m_nlThreshold, eDouble, "NoiseLevelThreshold");
	movConfig->SerializeData(m_referenceValuesFilePath, eString, "ReferenceFilePath");

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();

	return true;
}


// lecture des param�tres dans le fichier de configuration associ�
bool NoiseLevelParameters::DeSerialize(MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	// debut de la d�s�rialisation du module
	if (result)
	{
		result = result && movConfig->DeSerializeData(&m_enabled, eBool, "Enabled");
		result = result && movConfig->DeSerializeData(&m_minRange, eInt, "MinRange");
		result = result && movConfig->DeSerializeData(&m_maxRange, eInt, "MaxRange");
		result = result && movConfig->DeSerializeData(&m_nlThreshold, eDouble, "NoiseLevelThreshold");
		
		std::string filePath;
		result = result && movConfig->DeSerializeData(&filePath, eString, "ReferenceFilePath");
		if (result) {
			setReferenceValuesFilePath(filePath);
		}
	}

	// fin de la d�s�rialisation du module
	movConfig->DeSerializePushBack();

	return result;
}

void NoiseLevelParameters::setReferenceValuesFilePath(const std::string & filePath)
{
	m_referenceValuesFilePath = filePath;
	loadReferenceValues();
}

class ReferenceValuesFileSAX2Handler : public xercesc::DefaultHandler
{
public:
	void startElement(
		const   XMLCh* const    uri,
		const   XMLCh* const    localname,
		const   XMLCh* const    qname,
		const   xercesc::Attributes&     attrs
	)
	{
        if (std::u16string(qname) == u"Root")
        {
            auto nameAttributeIndex = attrs.getIndex(u"Name");
            if (nameAttributeIndex >= 0)
            {
                m_name = XMLString::transcode(attrs.getValue(nameAttributeIndex));
            }
        }
        else if (std::u16string(qname) == u"Sounder")
        {
            auto idAttributeIndex = attrs.getIndex(u"Id");
            if (idAttributeIndex >= 0)
            {
                m_currentSounderId = XmlChUtils::XMLChToT<int>(attrs.getValue(idAttributeIndex));
                m_currentReferenceValues.clear();
            }
            else
            {
                m_currentSounderId = -1;
            }
        }
        else if (std::u16string(qname) == u"Point")
        {
            auto freqAttributeIndex = attrs.getIndex(u"Frequency");
            auto valueAttributeIndex = attrs.getIndex(u"Power");
            if (m_currentSounderId != -1 && freqAttributeIndex >= 0 && valueAttributeIndex >= 0)
            {
                double freq = XmlChUtils::XMLChToT<double>(attrs.getValue(freqAttributeIndex));
                double value = XmlChUtils::XMLChToT<double>(attrs.getValue(valueAttributeIndex));
                m_currentReferenceValues.insert(std::make_pair(freq, value));
            }
        }
	}

	void endElement(const XMLCh* const uri,
		const XMLCh* const localname,
		const XMLCh* const qname)
	{
        if (std::u16string(qname) == u"Sounder")
        {
            if (m_currentSounderId != -1)
            {
                m_referenceValues.insert(std::make_pair(m_currentSounderId, m_currentReferenceValues));
            }

            m_currentSounderId = -1;
        }
	}

	void fatalError(const xercesc::SAXParseException&) {}

	const std::string & getName() const { return m_name;  }
	const std::map<int, NoiseLevelParameters::ReferenceValues> & getReferenceValues() const { return m_referenceValues; }

private:
	int m_currentSounderId = -1;
	NoiseLevelParameters::ReferenceValues m_currentReferenceValues;

	std::string m_name;
	std::map<int, NoiseLevelParameters::ReferenceValues> m_referenceValues;
};

bool NoiseLevelParameters::loadReferenceValues()
{
	XMLPlatformUtils::Initialize();

	auto parser = std::unique_ptr<SAX2XMLReader>(XMLReaderFactory::createXMLReader());
	//parser->setFeature(XMLUni::fgSAX2CoreValidation, true);
	//parser->setFeature(XMLUni::fgSAX2CoreNameSpaces, true);   // optional

	auto defaultHandler = std::unique_ptr<ReferenceValuesFileSAX2Handler>(new ReferenceValuesFileSAX2Handler());
	parser->setContentHandler(defaultHandler.get());
	parser->setErrorHandler(defaultHandler.get());

	bool ok = true;
	try {
		parser->parse(m_referenceValuesFilePath.c_str());
	} catch (...) {
		ok = false;
	}

	m_referenceName = defaultHandler->getName();
	m_referenceValues = defaultHandler->getReferenceValues();

	return ok;
}
