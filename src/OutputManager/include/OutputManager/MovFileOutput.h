// -*- C++ -*-
// ****************************************************************************
// Class: MovFileOutput
//
// Description: Classe d�finissant un flux de sortie fichier pour 
// le module d'archivage fichier et r�seau OutputManager.
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mai 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once
#pragma warning( disable : 4275 ) 

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "OutputManager/OutputManagerExport.h"
#include "M3DKernel/output/MovOutput.h"
#include "M3DKernel/parameter/ParameterBroadcastAndRecord.h"
#include <fstream>   // file I/O



// ***************************************************************************
// Declarations
// ***************************************************************************
class OUTPUTMANAGER_API MovFileOutput : public MovOutput
{
public:
	// *********************************************************************
  // Constructeurs / Destructeur
  // *********************************************************************
	// Constructeur par d�faut
	MovFileOutput();
	MovFileOutput(std::string filePrefix, std::string fileSuffix,
		BaseKernel::ParameterBroadcastAndRecord::TTramaType tramaType, bool prefixDate,
		int enableCount);

	// Destructeur
	virtual ~MovFileOutput();

	// *********************************************************************
  // Accesseurs
  // *********************************************************************
	virtual bool isNetworkOutput() override { return false; };

	// *********************************************************************
  // Traitements
  // *********************************************************************
	// �crit une chaine dans le fichier courant du flux de sortie
	virtual void Write(std::string str);

	// cr�e un document XML (� partir d'un string)
	virtual void CreateXML(std::string str);

	// ajout un certain nombre de noeuds XML (sous forme de strings) au document XML
	virtual void AppendXML(std::vector<std::string>);

	// renvoie le chemin complet du fichier
	const std::string & getFilePath() const;
	// renvoie la taille du fichier courant du flux de sortie
	virtual std::uint32_t getFileSize();
	// renvoie la dur�e du fichier courant du flux de sortie en secoudes
	virtual unsigned int getFileTime();
	// type de fichier
	virtual BaseKernel::ParameterBroadcastAndRecord::TTramaType getTramaType() { return m_TramaType; }
	// "pr�fixe" utilis� (chemin + prefix�)
	virtual std::string getFilePrefix() { return m_FilePrefix; }
	// indique si on doit pr�fixer avec la date
	virtual bool getFileDatePrefix() { return m_FileDatePrefix; }
	// "suffixe" utilis�
	virtual std::string getFileSuffix() { return m_FileSuffix; }
	// num�ro d'activation du module au moment de la cr�ation du fichier
	virtual int getEnableCount() { return m_EnableCount; }

	// construit un nouveau nom de fichier de sortie � l'aide 
	// du m_FilePrefix et de l'heure courante.
	virtual void ConstructNewFileName();

protected:
	// *********************************************************************
  // Donn�es
  // *********************************************************************
	// flux de sortie fichier
	std::ofstream m_OFStream;
	// "cl�" identifiant le flux (chemin + prefixe)
	std::string m_FilePrefix;
	// chemin complet du fichier (repertoire + prefixe + suffixe)
	std::string m_FilePath;
	// suffixe (lay par exemple)
	std::string m_FileSuffix;
	// type de trame (permet de choisir l'extension xml ou csv)
	BaseKernel::ParameterBroadcastAndRecord::TTramaType m_TramaType;
	// prefixage ou non de la date
	bool				m_FileDatePrefix;
	// instant de cr�ation du fichier courant
	time_t			m_FileTime;
	// num�ro d'activation du module au moment de la cr�ation du fichier
	int					m_EnableCount;
};
