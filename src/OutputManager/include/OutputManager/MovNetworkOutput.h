// -*- C++ -*-
// ****************************************************************************
// Class: MovNetworkOutput
//
// Description: Classe d�finissant un flux de sortie r�seau pour 
// le module d'archivage fichier et r�seau OutputManager.
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mai 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once
#pragma warning( disable : 4275 ) 


#define WIN32_LEAN_AND_MEAN
#define NOMINMAX

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "M3DKernel/output/MovOutput.h"

// flux de sortie UDP
#include "OutputManager/OutputSocketStream.h"

// XERCES 
namespace xercesc_3_2 {
	class DOMDocument;
	class XercesDOMParser;
}
namespace xercesc = xercesc_3_2;

// ***************************************************************************
// Declarations
// ***************************************************************************
class OUTPUTMANAGER_API MovNetworkOutput : public MovOutput
{
public:
	// *********************************************************************
  // Constructeurs / Destructeur
  // *********************************************************************
	// Constructeur par d�faut
	MovNetworkOutput(std::string hostIP, unsigned int port);

	// Destructeur
	virtual ~MovNetworkOutput();

	// *********************************************************************
  // Accesseurs
  // *********************************************************************
	virtual bool isNetworkOutput() override { return true; };

	// *********************************************************************
  // Traitements
  // *********************************************************************
	virtual void Write(std::string str) override;

	// cr�e un document XML (� partir d'un string)
	virtual void CreateXML(std::string str) override;

	// ajout un certain nombre de noeuds XML (sous forme de strings) au document XML
	virtual void AppendXML(std::vector<std::string>) override;

protected:
	// *********************************************************************
  // Donn�es
  // *********************************************************************
	// flux de sortie r�seau
	COutputSocketStream * m_pOSStream;

	// conteneurs XML
	xercesc::DOMDocument * m_pCurrentDOMDocument;

	// parser XML
	xercesc::XercesDOMParser* m_pParserStr;
};
