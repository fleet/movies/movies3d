// -*- C++ -*-
// ****************************************************************************
// Class: CSocket
//
// Description: Socket utilis�e pour l'�mission r�seau des r�sultats des
// modules
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mai 2008
// ****************************************************************************

#pragma once

#ifndef WIN32
#define SOCKADDR_IN int
#else
#include <WinSock2.h>
#endif

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "OutputManagerExport.h"
#include "M3DKernel/utils/socket/SocketCore.h"

// ***************************************************************************
// Declarations
// ***************************************************************************

// ***************************************************************************
// Interface de la classe CSocket
// ***************************************************************************
class OUTPUTMANAGER_API CSockAddrInHandle
{

public:
	CSockAddrInHandle() {
	}

	~CSockAddrInHandle() {
    }

	inline SOCKADDR_IN& Get() {
		return m_SockAddrIn;
	}

	inline const SOCKADDR_IN& Get() const {
		return m_SockAddrIn;
	}

	inline int SizeOf() const {
		return sizeof(SOCKADDR_IN);
    }

private:
    SOCKADDR_IN m_SockAddrIn;
};


class OUTPUTMANAGER_API CSocket : CSocketCore
{
public:

	// *********************************************************************
	// D�claration de type
	// *********************************************************************
	typedef enum { RECV, SEND } TBufferType;

	// *********************************************************************
	// M�thodes statiques
	// *********************************************************************

	// Remplissage d'une structure SYS_SOCKADDR_IN
	static void Create(const char *ip, int port, SOCKADDR_IN& addr);

	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	CSocket();

	// Destructeur
	virtual ~CSocket();

	// *********************************************************************
	// Gestion des options
	// *********************************************************************
	// Gestion du mode broadcast
	virtual void SetBroadcast(bool broadcast = true);
	virtual bool IsBroadcast() const;

	// Gestion de la dimension des paquets
	virtual void SetBufferSize(int bufferSize, TBufferType type);
	virtual int GetBufferSize(TBufferType type) const;

	// Renvoie la taille max d'un message
	virtual int GetMaxMessageSize() const;

	// *********************************************************************
	// Traitements
	// *********************************************************************
	// Creation de la socket UDP
	virtual void CreateUDP();

	// Ecoute sur le port port
	virtual void Bind(int port);

	// Fermeture et destruction de la socket
	virtual void Close();

	// Renvoie vrai si la socket est ouverte
	virtual bool IsOpen() const;

	// Attendre des donn�es 
	virtual int Recv(char *buf, int len, SOCKADDR_IN* source);

	// Emettre des donn�es 
	virtual int SendTo(char *buf, int len, SOCKADDR_IN* target);

protected:

	// *********************************************************************
	// Traitements prot�g�s
	// *********************************************************************

private:

	// *********************************************************************
	// Donn�es
	// *********************************************************************
    class Impl;
    Impl * impl;
};
