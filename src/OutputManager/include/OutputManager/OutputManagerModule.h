// -*- C++ -*-
// ****************************************************************************
// Class: OutputManagerModule
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : Mai 2008
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "OutputManagerExport.h"
#include "M3DKernel/module/ProcessModule.h"
#include "M3DKernel/algorithm/base/AlgorithmOutput.h"
#include "MovNetwork/MvNetParameter.h"
#include "OutputManager/MovFileOutput.h"

#include "M3DKernel/parameter/ParameterModule.h"

#include <map>

typedef	 std::map<ProcessModule*, MovFileOutput*> MapMovFileOutput;

// ***************************************************************************
// Declarations
// ***************************************************************************
class OUTPUTMANAGER_API OutputManagerModule : public ProcessModule
{
public:
	// *********************************************************************
  // Constructeurs / Destructeur
  // *********************************************************************
	// Constructeur par d�faut
	OutputManagerModule();
	MovCreateMacro(OutputManagerModule);

	// Destructeur
	virtual ~OutputManagerModule();

	void PingFanAdded(PingFan *);
	void SounderChanged(std::uint32_t);
	void StreamClosed(const char *streamName);
	void StreamOpened(const char *streamName);
	void ESUStart(ESUParameter * pWorkingESU);

	// C'est cet �venement qui d�clenche la serialisation des sorties
	// enflux XML ou CSV vers les sorties (reseau ou fichier) pour tous les modules
	void ESUEnd(ESUParameter * pWorkingESU, bool abort);

	// m�thode appel�e par ESUEnd() qui realise le traitement pour un module.
    void SerializeAllOutputs(ProcessModule * tramaProducer);

	// m�thode appel�e par ESUEnd() lorsque la fin de l'ESU est forc�e. Les sorties ne sont pas serialis�es
    void FlushAllOutputs(ProcessModule * tramaProducer);

	// *********************************************************************
	// Accesseurs
	// *********************************************************************
	MvNetParameter&	GetMvNetParameter() { return m_parameter; };
	BaseKernel::ParameterModule& GetParameterModule() { return m_parameter; };
		
protected:

	void onEnableStateChange() override;
	void onInputModuleEnableStateChange(EchoAlgorithm* algo) override;

	// *********************************************************************
	// Donn�es
	// *********************************************************************

	MvNetParameter	m_parameter;

	// Map des flux de sortie fichier en fonction des chemins d�finis dans les modules
	MapMovFileOutput m_MovFileOutputContainer;

private:
	// Archivage dans un fichier XML ou CSV
	void RecordToFile(ProcessModule * tramaProducer, ModuleOutputConsumer * pEIConsumer);

	// Archivage dans un fichier NetCDF
	void RecordToNetCDF(ProcessModule * tramaProducer, ModuleOutputConsumer * pEIConsumer);

	// Récupération de l'algorithme d'écriture lié au module
	std::unique_ptr<WriteAlgorithm>& getWriteAlgorithm(ProcessModule * processModule);

	// Création d'un algorithme d'écriture
	void CreateWriteAlgorithm(ProcessModule * processModule);

	// Algorithme d'écriture du fichier NetCDF par module
	std::map<ProcessModule* ,std::unique_ptr<WriteAlgorithm>> m_writeAlgorithms;
};
