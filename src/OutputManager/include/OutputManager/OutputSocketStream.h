// -*- C++ -*-
// ****************************************************************************
// Class: COutputSocketStream
//
// Description: 
//
// Projet: Movies3D
// Auteur: Olivier Tonck
// Date  : Mai 2008
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
// --> Socket
#include "OutputManager/MovSocket.h"
#include "OutputManagerExport.h"

#include <cassert>

// ***************************************************************************
// Declarations
// ***************************************************************************

// ***************************************************************************
// Interface de la classe COutputSocketStream
// ***************************************************************************

class OUTPUTMANAGER_API COutputSocketStream
{
public:

	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par défaut
	COutputSocketStream(int maxMsgSize = 0);

	// Destructeur
	virtual ~COutputSocketStream();

	// *********************************************************************
	// Accesseurs
	// *********************************************************************
	// Accesseurs sur le socket
	virtual void SetSocket(CSocket * socket) { m_Socket = socket; };
	virtual CSocket * GetSocket() { assert(m_Socket != 0); return m_Socket; };

	// Accesseurs sur l'adresse cible
	virtual void SetSockAddrIn(CSockAddrInHandle * socket) { m_SockAddrIn = socket; };
	virtual CSockAddrInHandle * GetSockAddrIn() { assert(m_SockAddrIn != 0); return m_SockAddrIn; };

	inline void SetMaxMsgSize(int maxMsgSize) {
		m_MaxMsgSize = maxMsgSize;
	}

	inline int GetMaxMsgSize() const {
		return m_MaxMsgSize;
	}

	// *********************************************************************
	// Traitements
	// *********************************************************************
	// Ouvre le flux
	virtual void Open();

	// Le flux est-il ouvert
	virtual bool IsOpen();

	// Ferme le flux
	virtual void Close();

	// Ecrit jusqu'a n données de dimension size et les ecrit dans buf,
	// retourne le nombre de données ecrites
	virtual int Write(char *buf, int size, int n);

protected:

	// *********************************************************************
	// Traitements protégés
	// *********************************************************************

	// Ecrit jusqu'a n données de dimension size et les ecrit dans buf,
	// retourne le nombre de données ecrites
	virtual int WriteInternal(char *buf, int size, int n, void* target = 0, int tgtSize = 0);

private:

	// *********************************************************************
	// Données
	// *********************************************************************
	// Socket
	CSocket * m_Socket;
	CSockAddrInHandle * m_SockAddrIn;

	int m_MaxMsgSize;
};
