#include "OutputManager/MovFileOutput.h"

#include <sstream>
#include <iomanip>
#include <time.h>

#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/utils/M3DStdUtils.h"
#include "M3DKernel/utils/fileutils.h"

// XERCES
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLDouble.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>

//Xerces
XERCES_CPP_NAMESPACE_USE

using namespace BaseKernel;

namespace
{
	constexpr const char * LoggerName = "OutputManager.MovFileOutput";
}

// constructeur
MovFileOutput::MovFileOutput()
{
}

MovFileOutput::MovFileOutput(std::string filePrefix, std::string fileSuffix,
	BaseKernel::ParameterBroadcastAndRecord::TTramaType tramaType,
	bool prefixDate, int enableCount)
{
	// chemin + rpefix des fichiers correspondant au flux
	m_FilePrefix = filePrefix;

	// suffixe
	m_FileSuffix = fileSuffix;

	// type de fichier 
	m_TramaType = tramaType;

	// Prefixage de la date ?
	m_FileDatePrefix = prefixDate;

	// construction du nom complet du fichier.
	ConstructNewFileName();

	// num�ro d'activation du module correspondant au moment de la cr�ation du fichier
	m_EnableCount = enableCount;
}

// Destructeur
MovFileOutput::~MovFileOutput()
{
}

const std::string & MovFileOutput::getFilePath() const
{
	return m_FilePath;
}

// renvoie la taille du fichier courant utilis� par le flux
std::uint32_t MovFileOutput::getFileSize()
{
	return fileutils::getFileSize(m_FilePath);
}

// renvoie la dur�e du fichier courant utilis� par le flux en secondes
unsigned int MovFileOutput::getFileTime()
{
	// r�cup�ration de la date courante
	time_t rawtime;
	time(&rawtime);
	double nbDiffSeconds = difftime(m_FileTime, rawtime);

	return (unsigned int)nbDiffSeconds;
}

// construit un nouveau nom de fichier de sortie � l'aide 
// du m_FilePrefix et de l'heure courante.
void MovFileOutput::ConstructNewFileName()
{
	std::string result;

	// le nouveau fichier commence par la cl� du flux (chemin + prefixe)
	result = m_FilePrefix;

	if (m_FileDatePrefix)
	{
		// on ajoute la date s�par�e du prefixe par un '_'
		result.append("_");

		// r�cup�ration de la date
		time(&m_FileTime);
		tm timeinfo;
#ifdef WIN32
		localtime_s(&timeinfo, &m_FileTime);
#else
		localtime_r(&m_FileTime, &timeinfo);
#endif

		// formattage de la chaine de caract�res associ�e � la date
		std::ostringstream ss;
		ss << timeinfo.tm_year + 1900 << std::setw(2) << std::setfill('0') << (timeinfo.tm_mon + 1)
			<< std::setw(2) << std::setfill('0') << timeinfo.tm_mday << "_"
			<< std::setw(2) << std::setfill('0') << timeinfo.tm_hour
			<< std::setw(2) << std::setfill('0') << timeinfo.tm_min
			<< std::setw(2) << std::setfill('0') << timeinfo.tm_sec;

		result.append(ss.str());
	}

	// ajout du suffixe
	result.append("_");
	result.append(m_FileSuffix);

	// ajout de l'extention
	result.append(".");
	switch (m_TramaType)
	{
	case ParameterBroadcastAndRecord::XML_TRAMA:
		result.append("xml");
		break;
	case ParameterBroadcastAndRecord::CSV_TRAMA:
		result.append("csv");
		break;
	default:
		assert(false); // type de trame inconnu
		result.append("txt");
		break;
	}

	m_FilePath = result;
}

// traitements
void MovFileOutput::Write(std::string str)
{
	// ouverture du fichier en mode a+ (ajout � la fin du fichier)
	m_OFStream.open(m_FilePath.c_str(), std::ios::out | std::ios::app);

	// �criture dans le flux
	m_OFStream << str;

	// fermeture du flux
	m_OFStream.close();
}

// cr�ation du squelette du fichier XML (header) � partir de la string pass�e
void MovFileOutput::CreateXML(std::string str)
{
	// initialisation de Xerces
	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException&) {
		M3D_LOG_ERROR(LoggerName, "MovConfig  Error XML Initialisation");
	}

	// Creation du XercesDOMParser qui va parser notre string � ajouter au document
	XercesDOMParser* parserStr = new XercesDOMParser();

	try {
		DOMDocument * pCurrentDOMDocument;
		DOMImplementation *impl = NULL;
		static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
		impl = DOMImplementationRegistry::getDOMImplementation(gLS);

		// on parse la chaine pour en faire un DOMDocument
		MemBufInputSource is((const XMLByte*)str.c_str(), (unsigned int)str.length(), "fakeId");
		parserStr->parse(is);
		pCurrentDOMDocument = parserStr->getDocument();

		// on �crit le fichier correspondant
		if (!pCurrentDOMDocument)
		{
			// le fichier n'existe pas encore ou est vide...
			M3D_LOG_ERROR(LoggerName, "MovFileOutput  Mauvais format de trame XML...");
			return;
		}

		// on �crit le fichier
		DOMLSSerializer* theSerializer = ((DOMImplementationLS*)impl)->createLSSerializer();

		//d�finition de la sortie
		XMLCh tempStr[1024];
		XMLString::transcode(m_FilePath.c_str(), tempStr, 1023);

		// �criture du fichier de configuration
		theSerializer->writeToURI(pCurrentDOMDocument, tempStr);
		theSerializer->release();
	}
	catch (const XMLException& ex) {
		std::string str = "MovFileOutput  XMLException : ";
		char * xmlch = XMLString::transcode(ex.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_ERROR(LoggerName, str.c_str());
		XMLPlatformUtils::Terminate();
	}
	catch (const DOMException& ex) {
		std::string str = "MovFileOutput  DOMException : ";
		char * xmlch = XMLString::transcode(ex.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_ERROR(LoggerName, str.c_str());
		XMLPlatformUtils::Terminate();
	}
	catch (...) {
		M3D_LOG_ERROR(LoggerName, "MovFileOutput  UnkownException");
		XMLPlatformUtils::Terminate();
	}

	delete parserStr;
	XMLPlatformUtils::Terminate();
}

// ajout un noeud XML (sous forme de string) � un document XML au niveau level.
void MovFileOutput::AppendXML(std::vector<std::string> strvect)
{
	// initialisation de Xerces
	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException&) {
		M3D_LOG_ERROR(LoggerName, "MovConfig  Error XML Initialisation");
	}

	// Lecture du document courant
	// Creation du XercesDOMParser qui va parser notre fichier de r�sultats
	XercesDOMParser* parser = new XercesDOMParser();
	// Creation du XercesDOMParser qui va parser notre string � ajouter au document
	XercesDOMParser* parserStr = new XercesDOMParser();

	try {
		parser->parse(m_FilePath.c_str());
		XERCES_CPP_NAMESPACE_USE::DOMDocument * pCurrentDOMDocument = parser->getDocument();
		DOMNode * pCurrentDomNode;
		DOMImplementation *impl = NULL;
		static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
		impl = DOMImplementationRegistry::getDOMImplementation(gLS);

		// si pas de document (absence de fichier, on doit le cr�er)
		if (!pCurrentDOMDocument)
		{
			// le fichier n'existe pas encore ou est vide...
			XMLPlatformUtils::Terminate();
			M3D_LOG_ERROR(LoggerName, "MovFileOutput  Acces au fichier de r�sultats refus�.");
			return;
		}

		// on parse les chaines de caract�res � ajouter
		pCurrentDomNode = pCurrentDOMDocument->getDocumentElement();

		for (unsigned int i = 0; i < strvect.size(); i++)
		{
			std::string str = strvect[i];
			MemBufInputSource is((const XMLByte*)str.c_str(), (unsigned int)str.length(), "fakeId");
			parserStr->parse(is);
			XERCES_CPP_NAMESPACE_USE::DOMDocument * pStrDOMDocument = parserStr->getDocument();
			DOMNode * pStrDOMNode = pStrDOMDocument->getDocumentElement();
			if (pStrDOMNode)
			{
				pCurrentDomNode->appendChild(pCurrentDOMDocument->importNode(pStrDOMNode, true));
			}
		}

		// on �crit le fichier
		DOMLSSerializer* theSerializer = ((DOMImplementationLS*)impl)->createLSSerializer();

		//d�finition de la sortie
		XMLCh tempStr[1024];
		XMLString::transcode(m_FilePath.c_str(), tempStr, 1023);

		// �criture du fichier
		theSerializer->writeToURI(pCurrentDOMDocument, tempStr);
		theSerializer->release();
	}
	catch (const XMLException& ex) {
		std::string str = "MovFileOutput  XMLException : ";
		char * xmlch = XMLString::transcode(ex.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_ERROR(LoggerName, str.c_str());
		XMLPlatformUtils::Terminate();
	}
	catch (const DOMException& ex) {
		std::string str = "MovFileOutput  DOMException : ";
		char * xmlch = XMLString::transcode(ex.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_ERROR(LoggerName, str.c_str());
		XMLPlatformUtils::Terminate();
	}
	catch (...) {
		M3D_LOG_ERROR(LoggerName, "MovFileOutput  UnkownException");
		XMLPlatformUtils::Terminate();
	}

	delete parser;
	delete parserStr;
	XMLPlatformUtils::Terminate();
}

