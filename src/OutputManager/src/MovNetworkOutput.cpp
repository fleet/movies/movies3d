
#include "OutputManager/MovNetworkOutput.h"

#include "M3DKernel/utils/log/ILogger.h"

// XERCES
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLDouble.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/MemBufFormatTarget.hpp>

#include <thread>

//Xerces
XERCES_CPP_NAMESPACE_USE

namespace
{
	constexpr const char * LoggerName = "OutputManager.MovNetworkOutput";
}

// constructeur
MovNetworkOutput::MovNetworkOutput(std::string hostIP, unsigned int port)
{
	COutputSocketStream  *pOS = new COutputSocketStream();
	CSockAddrInHandle    *pAddrH = new CSockAddrInHandle();

	// d�termination de l'adresse de broadcast
	int pos = -1;
	std::string broadcastAddr = "";

	pos = (int)hostIP.rfind('.');
	broadcastAddr = hostIP.substr(0, pos + 1);
	broadcastAddr += "255";

	CSocket::Create(broadcastAddr.c_str(), port, pAddrH->Get());

	pOS->SetSockAddrIn(pAddrH);
	pOS->Open();
	pOS->GetSocket()->SetBroadcast();

	m_pOSStream = pOS;

	m_pCurrentDOMDocument = 0;

	// initialisation de Xerces
	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException&) {
		M3D_LOG_ERROR(LoggerName, "MovConfig  Error XML Initialisation");
	}

	m_pParserStr = new XercesDOMParser();
}

// Destructeur
MovNetworkOutput::~MovNetworkOutput()
{
	if (m_pParserStr)
	{
		delete m_pParserStr;
	}
	if (m_pOSStream)
	{
		delete m_pOSStream;
	}
	XMLPlatformUtils::Terminate();
}


// traitements
void MovNetworkOutput::Write(std::string str)
{
	m_pOSStream->Write((char*)str.c_str(), (int)sizeof(char), (int)str.length());
	// D�lai pour SDIV+ (visiblement il a du mal a tout r�cup�rer sinon).
	// ce sleep est le m�me que dans Movies+
    std::this_thread::sleep_for(std::chrono::seconds(50));
}

// cr�ation du squelette du fichier XML (header) � partir de la string pass�e
void MovNetworkOutput::CreateXML(std::string str)
{
	try {
		DOMImplementation *impl = NULL;
		static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
		impl = DOMImplementationRegistry::getDOMImplementation(gLS);

		// on parse la chaine pour en faire un DOMDocument
		MemBufInputSource is((const XMLByte*)str.c_str(), (unsigned int)str.length(), "fakeId");
		m_pParserStr->parse(is);
		m_pCurrentDOMDocument = m_pParserStr->getDocument();

		if (!m_pCurrentDOMDocument)
		{
			// le fichier n'existe pas encore ou est vide...
			M3D_LOG_ERROR(LoggerName, "MovNetworkOutput  Mauvais format de trame XML...");
			return;
		}
	}
	catch (const XMLException& ex) {
		std::string str = "MovNetworkOutput  XMLException : ";
		char * xmlch = XMLString::transcode(ex.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_ERROR(LoggerName, str.c_str());
	}
	catch (const DOMException& ex) {
		std::string str = "MovNetworkOutput  DOMException : ";
		char * xmlch = XMLString::transcode(ex.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_ERROR(LoggerName, str.c_str());
	}
	catch (...) {
		M3D_LOG_ERROR(LoggerName, "MovNetworkOutput  UnkownException");
	}
}

// ajoute un ou plusieurs noeuds XML (sous forme de string) � un document XML
void MovNetworkOutput::AppendXML(std::vector<std::string> strvect)
{
	try {

		DOMImplementation *impl = NULL;
		static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
		impl = DOMImplementationRegistry::getDOMImplementation(gLS);
		DOMNode * pCurrentDOMNode;

		// si pas de document (absence de fichier, on doit le cr�er)
		if (!m_pCurrentDOMDocument)
		{
			// le fichier n'existe pas encore ou est vide...
			M3D_LOG_ERROR(LoggerName, "MovFileOutput  Ecriture de donn�es XML dans un DOMDocument vide...");
			return;
		}

		// on parse les chaines de caract�res � ajouter
		pCurrentDOMNode = m_pCurrentDOMDocument->getDocumentElement();

		std::vector<DOMNode*> nodesToRemove;

		for (unsigned int i = 0; i < strvect.size(); i++)
		{
			std::string str = strvect[i];
			MemBufInputSource is((const XMLByte*)str.c_str(), (unsigned int)str.length(), "fakeId");
			m_pParserStr->parse(is);
			DOMDocument * pStrDOMDocument = m_pParserStr->getDocument();
			DOMNode * pStrDOMNode = pStrDOMDocument->getDocumentElement();
			if (pStrDOMNode)
			{
				nodesToRemove.push_back(pCurrentDOMNode->appendChild(m_pCurrentDOMDocument->importNode(pStrDOMNode, true)));
			}
		}

		// on �crit le fichier
		DOMLSSerializer* theSerializer = ((DOMImplementationLS*)impl)->createLSSerializer();

		//d�finition de la sortie
		MemBufFormatTarget* myFormatTarget = new MemBufFormatTarget();
		DOMLSOutput* theOutput = ((DOMImplementationLS*)impl)->createLSOutput();
		XMLCh * xmlch1 = XMLString::transcode("UTF-8");
		theOutput->setEncoding(xmlch1);
		XMLString::release(&xmlch1);

		theOutput->setByteStream(myFormatTarget);

		// �criture du fichier de configuration
		if (!theSerializer->write(m_pCurrentDOMDocument, theOutput))
		{
			M3D_LOG_ERROR(LoggerName, "MovFileOutput theSerializer->write() error");
		}
		else
		{
			std::string result = (const char*)((MemBufFormatTarget*)myFormatTarget)->getRawBuffer();

			// diffusion sur le r�seau.
			Write(result);
		}


		// suppression des noeuds ajout�s pour revenir dans l'�tat initial
		// (pas de copie du DOMDocument :/  )
		for (unsigned int i = 0; i < nodesToRemove.size(); i++)
		{
			pCurrentDOMNode->removeChild(nodesToRemove[i]);
		}

		theOutput->release();
		theSerializer->release();

		delete myFormatTarget;
	}
	catch (const XMLException& ex) {
		std::string str = "MovFileOutput  XMLException : ";
		char * xmlch = XMLString::transcode(ex.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_ERROR(LoggerName, str.c_str());
	}
	catch (const DOMException& ex) {
		std::string str = "MovFileOutput  DOMException : ";
		char * xmlch = XMLString::transcode(ex.getMessage());
		str.append(xmlch);
		XMLString::release(&xmlch);
		M3D_LOG_ERROR(LoggerName, str.c_str());
	}
	catch (...) {
		M3D_LOG_ERROR(LoggerName, "MovFileOutput  UnkownException");
	}
}


