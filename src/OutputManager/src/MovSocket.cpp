// -*- C++ -*-
// ***************************************************************************
// Class: CSocket
//
// Description: Implementation
//
// Project: Movies3D
// Author: Olivier TONCK, IPSIS
// Date: Mai 2008
// ***************************************************************************

// ***************************************************************************
// Dependencies
// ***************************************************************************
// Interface
#include "OutputManager/MovSocket.h"

#ifdef WIN32
#include "M3DKernel/utils/log/ILogger.h"

#include <cassert>

#pragma comment(lib,"ws2_32.lib")

#define USE_WSA

namespace
{
	constexpr const char * LoggerName = "OutputManager.MovSocket";
}

// ***************************************************************************
// Namespaces
// ***************************************************************************

// ***************************************************************************
// D�finitions
// ***************************************************************************
#define SO_DEFAULT_BUF 65535 // 16384

class CSocket::Impl
{
public:
    SOCKET                 m_SocketID = 0;
    SOCKADDR_IN            m_SockAddrIn;
};


// ***************************************************************************
/**
 * Remplissage d'une structure SYS_SOCKADDR_IN
 *
 * @date   25/05/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CSocket::Create(const char *ip, int port, SOCKADDR_IN& addr)
{
	// Initialisation de la structure � zero
	memset(&addr, 0, sizeof(SOCKADDR_IN));

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(ip);
	addr.sin_port = htons(port);
}

// ***************************************************************************
// Constructeurs / Destructeur
// ***************************************************************************

// ***************************************************************************
/**
 * Constructeur par d�faut
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
CSocket::CSocket()
	: CSocketCore()
    , impl(new CSocket::Impl)
{
}

// ***************************************************************************
/**
 * Detruit les donnees de l'objets courants
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
CSocket::~CSocket()
{
    delete impl;
}

// ***************************************************************************
// Gestion des options
// ***************************************************************************

// ***************************************************************************
/**
 * Initialisation du mode broadcast
 *
 * @date   14/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CSocket::SetBroadcast(bool broadcast)
{
	BOOL bBroadcast = broadcast;
	int  optLen = sizeof(BOOL);
	if (setsockopt(impl->m_SocketID, SOL_SOCKET, SO_BROADCAST, (char*)&bBroadcast, optLen) == SOCKET_ERROR) {
		M3D_LOG_ERROR(LoggerName, "Socket : Error setting broadcast mode");
	}
}

// ***************************************************************************
/**
 * La socket est elle en mode broadcast
 *
 * @date   14/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
bool CSocket::IsBroadcast() const
{
	bool result = false;
	BOOL bResult;
	int  optLen = sizeof(BOOL);

	getsockopt(impl->m_SocketID, SOL_SOCKET, SO_BROADCAST, (char*)&bResult, &optLen);

	result = bResult != 0;

	return result;
}

// ***************************************************************************
/**
 * Gestion de la dimension des paquets
 *
 * @date   15/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CSocket::SetBufferSize(int bufferSize, TBufferType type)
{
	// --------------------------------------------------------------------------
	 // Dimensionnement des buffers (fait ici avant de les passer en options
	 // de cr�ation des sockets)
	 // --------------------------------------------------------------------------
	int so_rcvbuf = bufferSize;
	int so_sndbuf = bufferSize;
	int optLen = sizeof(int);


	switch (type) {
	case RECV:
		if (setsockopt(impl->m_SocketID, SOL_SOCKET, SO_RCVBUF, (char*)&so_rcvbuf, optLen) == SOCKET_ERROR) {
			M3D_LOG_ERROR(LoggerName, "Socket : Error setting buffer size");
		}
		break;
	case SEND:
		if (setsockopt(impl->m_SocketID, SOL_SOCKET, SO_SNDBUF, (char*)&so_sndbuf, optLen) == SOCKET_ERROR) {
			M3D_LOG_ERROR(LoggerName, "Socket : Error setting buffer size");
		}
		break;
	default:
		assert(false);
	}

	// --------------------------------------------------------------------------
	// Fin dimensionnement des buffers
	// --------------------------------------------------------------------------
}

// ***************************************************************************
/**
 * Renvoie la dimension des buffers
 *
 * @date   15/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
int CSocket::GetBufferSize(TBufferType type) const
{
	int optVal = 0;
	int optLen = sizeof(int);

	switch (type) {
	case RECV:
		if (getsockopt(impl->m_SocketID, SOL_SOCKET, SO_RCVBUF, (char*)&optVal, &optLen) != SOCKET_ERROR) {
		}
		break;
	case SEND:
		if (getsockopt(impl->m_SocketID, SOL_SOCKET, SO_SNDBUF, (char*)&optVal, &optLen) != SOCKET_ERROR) {
		}
		break;
	default:
		assert(false);
	}

	return optVal;
}

// ***************************************************************************
/**
 * Renvoie la taille max d'un message
 *
 * @date   20/06/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
int CSocket::GetMaxMessageSize() const
{
	int optVal = 0;
	int optLen = sizeof(int);

	getsockopt(impl->m_SocketID, SOL_SOCKET, SO_MAX_MSG_SIZE, (char*)&optVal, &optLen);

	return optVal;
}

// ***************************************************************************
// Traitements
// ***************************************************************************
// ***************************************************************************
/**
 * Creation de la socket
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CSocket::CreateUDP()
{
	// *************************************************************************
	// Test si pas d�j� ouvert
	// *************************************************************************
	if (IsOpen()) {
		M3D_LOG_ERROR(LoggerName, "Socket : Socket already opened");
	}

	// *************************************************************************
	// Ouverture d'une Socket
	// *************************************************************************
#ifdef USE_WSA
	impl->m_SocketID = WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, NULL, 0, WSA_FLAG_OVERLAPPED);
#else
	impl->m_SocketID = socket(AF_INET, SOCK_DGRAM, 0);
#endif

	if (impl->m_SocketID == INVALID_SOCKET) {
		M3D_LOG_ERROR(LoggerName, "Socket : Invalid socket ID");
	}
}

// ***************************************************************************
/**
 * Ecoute sur le port port
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CSocket::Bind(int port)
{
	// *************************************************************************
	// Lie la socket � une ip et un port d'�coute
	// *************************************************************************
	// Initialisation de la structure a 0
	memset(&impl->m_SockAddrIn, 0, sizeof(impl->m_SockAddrIn));

	// Initialisation des champs de la structure
	impl->m_SockAddrIn.sin_family = AF_INET;
	impl->m_SockAddrIn.sin_addr.s_addr = INADDR_ANY; // Ecoute sur toutes les IP locales  
	impl->m_SockAddrIn.sin_port = htons(port); // Ecoute sur le port 33333

	// Bind 
	int res = bind(impl->m_SocketID, (struct sockaddr*)&impl->m_SockAddrIn, sizeof(impl->m_SockAddrIn));

	// Gestion de l'erreur
	if (res != 0) {
		M3D_LOG_ERROR(LoggerName, "Socket : Binding error");
	}
}

// ***************************************************************************
/**
 * Fermeture et destruction de la socket
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
void CSocket::Close()
{
	if (IsOpen()) {
		// ***********************************************************************
		// Fermeture de la socket correspondante � la commande socket()
		// ***********************************************************************
		int res = closesocket(impl->m_SocketID);

		if (res != 0) {
			M3D_LOG_ERROR(LoggerName, "Socket : Error closing socket");
		}
	}
}

// ***************************************************************************
/**
 * Renvoie vrai si la socket est ouverte
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
bool CSocket::IsOpen() const
{
	return impl->m_SocketID != 0;
}


// ***************************************************************************
/**
 * Attendre des donn�es
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
int CSocket::Recv(char *buf, int len, SOCKADDR_IN* source)
{
	int sourceLen = sizeof(*source);

#ifdef USE_WSA
	WSABUF DataBuf;
	DWORD  DataRead;
	DWORD  Flags = 0;

	DataBuf.buf = buf;
	DataBuf.len = len;

	int result = WSARecvFrom(impl->m_SocketID, &DataBuf, 1, &DataRead, &Flags, (struct sockaddr*)source, &sourceLen, NULL, NULL);

	if (result == 0) {
		result = DataRead;
	}

#else
	int result = recvfrom(impl->m_SocketID, buf, len, 0, (struct sockaddr*)source, &sourceLen);
#endif

	return result;
}

// ***************************************************************************
/**
 * Emettre des donn�es
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Cr�ation
 */
 // ***************************************************************************
int CSocket::SendTo(char *buf, int len, SOCKADDR_IN* target)
{
	assert(target != 0);

	int targetLen = sizeof(*target);
	int result = sendto(impl->m_SocketID, buf, len, 0, (struct sockaddr*)target, targetLen);

	return result;
}

#else

void CSocket::Create(const char *ip, int port, SOCKADDR_IN& addr)
{
}

CSocket::CSocket()
{
}

CSocket::~CSocket()
{
}

void CSocket::SetBroadcast(bool broadcast)
{
}

bool CSocket::IsBroadcast() const
{
    return 0;
}

void CSocket::SetBufferSize(int bufferSize, TBufferType type)
{
}

int CSocket::GetBufferSize(TBufferType type) const
{
    return 0;
}

int CSocket::GetMaxMessageSize() const
{
    return 0;
}

void CSocket::CreateUDP()
{
}

void CSocket::Bind(int port)
{
}

void CSocket::Close()
{
}

bool CSocket::IsOpen() const
{
    return false;
}

int CSocket::Recv(char *buf, int len, SOCKADDR_IN* source)
{
    return 0;
}

int CSocket::SendTo(char *buf, int len, SOCKADDR_IN* target)
{
    return 0;
}

#endif
