
#include "OutputManager/OutputManagerModule.h"

// d�pendances
#include "OutputManager/MovFileOutput.h"
#include "OutputManager/MovNetworkOutput.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/utils/M3DStdUtils.h"

#include "M3DKernel/module/ModuleOutputContainer.h"
#include "EchoIntegration/EchoIntegrationModule.h"
#include "EchoIntegration/EchoIntegrationOutput.h"

#include "Reader/ReaderCtrl.h"
#include "Reader/WriteAlgorithm/WriteAlgorithm.h"

using namespace BaseKernel;

// Constructeur
OutputManagerModule::OutputManagerModule() 
	: ProcessModule("OutputManager Module")
	, m_parameter()
{
}

// Destructeur
OutputManagerModule::~OutputManagerModule()
{
	// nettoyage des flux de sortie
	MapMovFileOutput::iterator res = m_MovFileOutputContainer.begin();
	while (res != m_MovFileOutputContainer.end())
	{
		delete res->second;
		res++;
	}
	m_MovFileOutputContainer.clear();
}

// Traitement associ� � l'ajout des pingFans :
// on diffuse les �ventuels r�sultats
void OutputManagerModule::PingFanAdded(PingFan *p)
{
	// Pour tous les modules parent...
	for (const auto& outputAlgo : getInputs())
	{
		ProcessModule * tramaProducer = dynamic_cast<ProcessModule*>(outputAlgo->getProducer());

		assert(tramaProducer != 0); // les modules n'h�ritant pas de tramaproducer ne 
		// sont pas dans la liste des entr�es du module OutputManagerModule.

		// Serialisation des sorties du module d'ID i
		SerializeAllOutputs(tramaProducer);
	}
}

// traitement associ� au d�but des ESU
void OutputManagerModule::ESUStart(ESUParameter * pWorkingESU)
{
}

void OutputManagerModule::onEnableStateChange()
{
	if (!getEnable()) {
		m_writeAlgorithms.clear();
	}
}

void OutputManagerModule::onInputModuleEnableStateChange(EchoAlgorithm* algo)
{
	if (!algo->getEnable()) {
		m_writeAlgorithms.clear();
	}
}

// traitement associ� � la fin des ESU:
void OutputManagerModule::ESUEnd(ESUParameter * pWorkingESU, bool abort)
{
}

void OutputManagerModule::SounderChanged(std::uint32_t sounderId)
{
}

void OutputManagerModule::StreamClosed(const char *streamName)
{
}

void OutputManagerModule::StreamOpened(const char *streamName)
{
}

// m�thode appel�e par ESUEnd() qui realise le traitement pour un module.
void OutputManagerModule::SerializeAllOutputs(ProcessModule * tramaProducer)
{
	// r�cup�ration du consumer correspondant au module d'indice i
	ModuleOutputConsumer *pEIConsumer = tramaProducer->GetOutputMgrConsumer();

	tramaProducer->GetOutputContainer().Lock();
	// r�cup�ration du nombre de sorties a serialiser pour le module d'indice i
	unsigned int count = tramaProducer->GetOutputContainer().GetOutPutCount(pEIConsumer->GetConsumerId());

	if (count > 0)
	{
		// si archivage fichier
		if (tramaProducer->GetParameterModule().GetParameterBroadcastAndRecord().m_enableFileOutput)
		{
			switch (tramaProducer->GetParameterModule().GetParameterBroadcastAndRecord().m_tramaType) {
			case ParameterBroadcastAndRecord::CSV_TRAMA:
			case ParameterBroadcastAndRecord::XML_TRAMA:
				RecordToFile(tramaProducer, pEIConsumer);
				break;

			case ParameterBroadcastAndRecord::NETCDF_TRAMA:
				RecordToNetCDF(tramaProducer, pEIConsumer);
				break;
			}
		}

		// si archivage r�seau
		if (tramaProducer->GetParameterModule().GetParameterBroadcastAndRecord().m_enableNetworkOutput)
		{
			// instanciation du NetworkOutput correspondant aux param�tres
			// Attention : on r�cupere les param�tres r�seau dans la config r�seau et pas 
			// la configuration du module. (memes param�tres pour tous les modules
			MovNetworkOutput * networkOutput = new MovNetworkOutput(m_parameter.m_netHost, m_parameter.m_netDataPort);

			// appel de la serialisation des r�sultats
			unsigned int k;
			for (k = 0; k < count; k++)
			{
				switch (tramaProducer->GetParameterModule().GetParameterBroadcastAndRecord().m_tramaType)
				{
				case ParameterBroadcastAndRecord::XML_TRAMA:
					tramaProducer->GetOutputContainer().GetOutPut(pEIConsumer->GetConsumerId(), k)->XMLHeaderSerialize(networkOutput, tramaProducer->GetParameterModule().GetTramaDescriptor());
					tramaProducer->GetOutputContainer().GetOutPut(pEIConsumer->GetConsumerId(), k)->XMLSerialize(networkOutput, tramaProducer->GetParameterModule().GetTramaDescriptor());
					break;
				case ParameterBroadcastAndRecord::CSV_TRAMA:
					tramaProducer->GetOutputContainer().GetOutPut(pEIConsumer->GetConsumerId(), k)->CSVHeaderSerialize(networkOutput, tramaProducer->GetParameterModule().GetTramaDescriptor());
					tramaProducer->GetOutputContainer().GetOutPut(pEIConsumer->GetConsumerId(), k)->CSVSerialize(networkOutput, tramaProducer->GetParameterModule().GetTramaDescriptor());
					break;
				}
			}

			delete networkOutput;
		}

		// flush du container de sorties du module en cours
		tramaProducer->GetOutputContainer().Flush(pEIConsumer->GetConsumerId());
	}
	
	// Suppression de l'algorithme d'écriture si la process est désactivé
	if (!tramaProducer->getEnable() && m_writeAlgorithms.count(tramaProducer) != 0) 
	{
		m_writeAlgorithms.erase(tramaProducer);
	}

	tramaProducer->GetOutputContainer().Unlock();
}

// m�thode appel�e par ESUEnd() qui vide les sorties (non s�rialis�es car la fin de l'ESU a �t� forc�e)
void OutputManagerModule::FlushAllOutputs(ProcessModule * tramaProducer)
{
	// r�cup�ration du consumer correspondant au module d'indice i
	ModuleOutputConsumer *pEIConsumer = tramaProducer->GetOutputMgrConsumer();

	tramaProducer->GetOutputContainer().Lock();
	// r�cup�ration du nombre de sorties a serialiser pour le module d'indice i
	unsigned int count = tramaProducer->GetOutputContainer().GetOutPutCount(pEIConsumer->GetConsumerId());

	if (count > 0)
	{
		// flush du container de sorties du module en cours
		tramaProducer->GetOutputContainer().Flush(pEIConsumer->GetConsumerId());
	}
	tramaProducer->GetOutputContainer().Unlock();
}

void OutputManagerModule::RecordToFile(ProcessModule * tramaProducer, ModuleOutputConsumer * pEIConsumer) 
{
	// Paramètres d'enregistrement
	const auto& broadcastAndRecordParameters = tramaProducer->GetParameterModule().GetParameterBroadcastAndRecord();

	// creation du chemin cible
	std::string filePrefix = broadcastAndRecordParameters.m_filePath;
	if (filePrefix.substr(filePrefix.length() - 1, 1).compare("\\")) {
		filePrefix.append("\\");
	}
	filePrefix.append(broadcastAndRecordParameters.m_filePrefix);

	// on r�cupere le flux dans notre map
	MovFileOutput * fileOutput;
	MapMovFileOutput::iterator res = m_MovFileOutputContainer.find(tramaProducer);
	// s'il existe, on le r�cupere
	bool newFile = false;
	if (res != m_MovFileOutputContainer.end()) 
	{
		fileOutput = res->second;
		// il faut v�rifier que le type de sortie est toujours le bon... sinon il faut changer.
		// OTK - FAE 185 - on regarde aussi si le chemin ou le prefixe � chang�.
		if (fileOutput->getTramaType() !=
			broadcastAndRecordParameters.m_tramaType
			|| (_stricmp(fileOutput->getFilePrefix().c_str(), filePrefix.c_str()) != 0))
		{
			delete fileOutput;
			m_MovFileOutputContainer.erase(res);
			newFile = true;
		}
		// OTK - FAE 193 - si les r�sultats correspondent � une autre activation du module,
		// et qu'on fait appara�tre la date dans le nom des fichiers, on cr�e un nouveau fichier.
		else if (broadcastAndRecordParameters.m_prefixDate
			&& tramaProducer->getEnableCount() != fileOutput->getEnableCount()) 
		{
			delete fileOutput;
			m_MovFileOutputContainer.erase(res);
			newFile = true;
		}
	}
	else // s'il n'existe pas on l'instancie et on l'ajoute dans la map
	{
		newFile = true;
	}

	if (newFile) 
	{
		fileOutput = new MovFileOutput(filePrefix,
			broadcastAndRecordParameters.m_fileSuffix,
			broadcastAndRecordParameters.m_tramaType,
			broadcastAndRecordParameters.m_prefixDate,
			tramaProducer->getEnableCount());
		m_MovFileOutputContainer.insert(MapMovFileOutput::value_type(tramaProducer, fileOutput));
	}

	// Si le fichier de sortie d�passe la taille limite (en octets ou en temps)
	// on passe au suivant
	if (broadcastAndRecordParameters.m_prefixDate)
	{
		switch (broadcastAndRecordParameters.m_cutType) 
		{
		case ParameterBroadcastAndRecord::SIZE_CUT:
			if (fileOutput->getFileSize() >= (broadcastAndRecordParameters.m_fileMaxSize*(1024 * 1024))) {
				newFile = true;
			}
			break;
		case ParameterBroadcastAndRecord::TIME_CUT:
			if (fileOutput->getFileTime() > broadcastAndRecordParameters.m_fileMaxTime) {
				newFile = true;
			}
			break;
		default:
			assert(false); // type de d�coupage inconnu
			break;
		}
	}

	// appel de la serialisation des r�sultats
	unsigned int k;
	const auto count = tramaProducer->GetOutputContainer().GetOutPutCount(pEIConsumer->GetConsumerId());
	for (k = 0; k < count; k++) 
	{
		const auto echoIntegrationOuput = tramaProducer->GetOutputContainer().GetOutPut(pEIConsumer->GetConsumerId(), k);

		if (newFile) 
		{
			// on cr�e un nouveau fichier pour le flux
			fileOutput->ConstructNewFileName();
			// on �crit le header
			switch (broadcastAndRecordParameters.m_tramaType) 
			{
			case ParameterBroadcastAndRecord::XML_TRAMA:
				echoIntegrationOuput->XMLHeaderSerialize(fileOutput, tramaProducer->GetParameterModule().GetTramaDescriptor());
				break;
			case ParameterBroadcastAndRecord::CSV_TRAMA:
				echoIntegrationOuput->CSVHeaderSerialize(fileOutput, tramaProducer->GetParameterModule().GetTramaDescriptor());
				break;
			}
			newFile = false;
		}

		switch (broadcastAndRecordParameters.m_tramaType)
		{
		case ParameterBroadcastAndRecord::XML_TRAMA:
			echoIntegrationOuput->XMLSerialize(fileOutput, tramaProducer->GetParameterModule().GetTramaDescriptor());
			break;
		case ParameterBroadcastAndRecord::CSV_TRAMA:
			echoIntegrationOuput->CSVSerialize(fileOutput, tramaProducer->GetParameterModule().GetTramaDescriptor());
			break;
		}
	}
}

void OutputManagerModule::RecordToNetCDF(ProcessModule * tramaProducer, ModuleOutputConsumer * pEIConsumer) 
{	
	// Récupération de l'algo d'écriture
	auto& writeAlgo = getWriteAlgorithm(tramaProducer);

	// On écrit rien si l'algorithme d'écriture n'est pas activé
	if (!writeAlgo->getEnable()) 
	{
		return;
	}

	// Récupération des données et insertion dans le fichier NetCDF
	const auto count = tramaProducer->GetOutputContainer().GetOutPutCount(pEIConsumer->GetConsumerId());
	for (auto i = 0; i < count; i++) 
	{
		// Récupération de la sortie de l'écho intégration
		const auto echoIntegrationOuput = (EchoIntegrationOutput*)tramaProducer->GetOutputContainer().GetOutPut(pEIConsumer->GetConsumerId(), i);

		// Récupération du sondeur associé
		const auto sounder = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().m_sounderDefinition.GetSounderWithId(echoIntegrationOuput->m_SounderID);

		// On délègue l'écriture au WriteAlgorithm
		writeAlgo->AddEchoIntegration(*sounder, *(EchoIntegrationModule*)tramaProducer, *echoIntegrationOuput);
	}
}

std::unique_ptr<WriteAlgorithm>& OutputManagerModule::getWriteAlgorithm(ProcessModule * processModule) 
{
	if (m_writeAlgorithms.count(processModule) == 0) 
	{
		CreateWriteAlgorithm(processModule);
	}
	return m_writeAlgorithms[processModule];
}

void OutputManagerModule::CreateWriteAlgorithm(ProcessModule * processModule) 
{
	// Paramètres d'enregistrement
	const auto& broadcastAndRecordParameters = processModule->GetParameterModule().GetParameterBroadcastAndRecord();
	
	// Création du pointeur vers l'algo
	auto& writeAlgo = m_writeAlgorithms[processModule];

	// Création de l'algo et configuration
	writeAlgo = std::unique_ptr<WriteAlgorithm>(WriteAlgorithm::Create());
	writeAlgo->AddInput(processModule, false /*addToExecutive*/);
	writeAlgo->SetArchivePreviousPings(false);

	// Prefix
	auto prefix = broadcastAndRecordParameters.m_filePrefix;
	if (prefix.back() != '_') 
	{
		prefix.append("_");
	}
	writeAlgo->SetFilePrefix(prefix.c_str());

	// Destination
	auto destination = broadcastAndRecordParameters.m_filePath;
	writeAlgo->SetDirectory(destination.c_str());

	// Détermination des limites
	if (broadcastAndRecordParameters.m_prefixDate)
	{
		switch (broadcastAndRecordParameters.m_cutType)
		{
		case ParameterBroadcastAndRecord::SIZE_CUT:
			writeAlgo->SetFileMaxSize(broadcastAndRecordParameters.m_fileMaxSize * 1024 * 1024);
			break;
		case ParameterBroadcastAndRecord::TIME_CUT:
			writeAlgo->SetFileMaxTime(broadcastAndRecordParameters.m_fileMaxTime);
			break;
		}
	}

	// Export en NetCDF + activation de l'écriture
	writeAlgo->SetReaderService(ReaderCtrl::getInstance()->GetActiveService());
	writeAlgo->SetOutputType(WriteAlgorithm::OutputType::NetCDF_EI);
}


