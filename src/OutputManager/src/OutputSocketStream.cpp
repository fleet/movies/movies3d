// -*- C++ -*-
// ***************************************************************************
// Class: COutputSocketStream
//
// Description: Implementation
//
// Project: Movies3D
// Author: Olivier TONCK, IPSIS
// Date: Mai 2008
// ***************************************************************************

// ***************************************************************************
// Dependencies
// ***************************************************************************
// Interface
#include "OutputManager/OutputSocketStream.h"

// ***************************************************************************
// Namespaces
// ***************************************************************************

// ***************************************************************************
// Implémentations
// ***************************************************************************

// ***************************************************************************
// Constructeurs / Destructeur
// ***************************************************************************

// ***************************************************************************
/**
 * Constructeur par defaut
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Création
 */
 // ***************************************************************************
COutputSocketStream::COutputSocketStream(int maxMsgSize)
	: m_MaxMsgSize(maxMsgSize)
{
}

// ***************************************************************************
/**
 * Destructeur
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Création
 */
 // ***************************************************************************
COutputSocketStream::~COutputSocketStream()
{
	Close();
}

// ***************************************************************************
/**
 * Ouvre le flux
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Création
 */
 // ***************************************************************************
void COutputSocketStream::Open()
{
	SetSocket(new CSocket());

	GetSocket()->CreateUDP();
}

// ***************************************************************************
/**
 * Le flux est-il ouvert
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Création
 */
 // ***************************************************************************
bool COutputSocketStream::IsOpen()
{
	bool result = (m_Socket != 0);

	if (result) {
		result = GetSocket()->IsOpen();
	}

	return result;
}

// ***************************************************************************
/**
 * Ferme le flux
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Création
 */
 // ***************************************************************************
void COutputSocketStream::Close()
{
	if (IsOpen()) {
		GetSocket()->Close();
	}
}

// ***************************************************************************
/**
 * Lit jusqu'a n données de dimension size et les ecrit dans buf,
 * retourne le nombre de données lues
 *
 * @date   24/05/2005 - Emmanuel Camus 3(IPSIS) - Création
 */
 // ***************************************************************************
int COutputSocketStream::WriteInternal(char *buf, int size, int n, void* target, int tgtSize)
{
	assert(target != 0);
	assert(tgtSize == sizeof(SOCKADDR_IN));

	int result = GetSocket()->SendTo(buf, size*n, (SOCKADDR_IN*)target);

	return result;
}

// ***************************************************************************
/**
 * Ecrit jusqu'a n données de dimension size et les ecrit dans buf,
 *  retourne le nombre de données ecrites
 *
 * @date   14/06/2005 - Emmanuel Camus 3(IPSIS) - Création
 */
 // ***************************************************************************
int COutputSocketStream::Write(char *buf, int size, int n)
{
	CSockAddrInHandle *addr = GetSockAddrIn();
	return WriteInternal(buf, size, n, &(addr->Get()), addr->SizeOf());
}