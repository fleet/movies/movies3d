#!/usr/bin/python
# -*- coding: utf-8 -*-

from pyMovies import *

from batch_EI_multiTHR import batch_EI_multiTHR
from EIlayRes_bind import EIlayRes_bind

import os
import numpy as np
import pickle
import datetime
import time

import matplotlib.pyplot as plt
import matplotlib.dates as md

path_hac_survey = 'C:\Users\lberger\Desktop\GAZCOGNE3'
path_config = path_hac_survey + '\Config_M3D\EI_Lay'
survey_name='GAZCOGNE3';
path_save = path_hac_survey + '\Result\\' + survey_name +'\\'

#If save path does not exist, create it
if not os.path.exists(path_save):
    os.makedirs(path_save)

## Echo-integration
#thresholds=np.arange(-80,-70,10)
thresholds=[-65]
print 'seuil'+str(thresholds)

tic=time.clock()
runs=[5]
#batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs)
#batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs,'40to43min',dateStart='31/08/2018',timeStart='06:30:00',dateEnd='31/08/2018',timeEnd='08:45:00')
toc=time.clock()

elapsedtime = toc - tic;

print elapsedtime


for ir in runs:
    for t in thresholds:
        str_run = "%03d" % (ir)
        filename_ME70 = '%s/%s-RUN%s-TH%d-ME70-EIlay.pickle' % (path_save,survey_name,str_run,t)
        filename_EK80 = '%s/%s-RUN%s-TH%d-EK80-EIlay.pickle' % (path_save,survey_name,str_run,t)
        filename_EK80h = '%s/%s-RUN%s-TH%d-EK80h-EIlay.pickle' % (path_save,survey_name,str_run,t)
        path_results= '%s/RUN%s/%d/' % (path_save,str_run,t)
        #EIlayRes_bind(path_results,filename_ME70,filename_EK80,filename_EK80h)
        
# Load bound EIlay results
threshold=-65
run=5
str_run = "%03d" % (run)
filename_ME70='%s/%s-RUN%s-TH%d-ME70-EIlay.pickle' % (path_save,survey_name,str_run,threshold)
filename_EK80='%s/%s-RUN%s-TH%d-EK80-EIlay.pickle' % (path_save,survey_name,str_run,threshold)
filename_EK80h='%s/%s-RUN%s-TH%d-EK80h-EIlay.pickle' % (path_save,survey_name,str_run,threshold)

with open(filename_EK80) as f:
    time_EK80_db,depth_surface_EK80_db,depth_bottom_EK80_db,Sv_surfEK80_db,Sv_botEK80_db,Sa_surfEK80_db,Sa_botEK80_db,lat_surfEK80_db,lon_surfEK80_db,lat_botEK80_db,lon_botEK80_db,vol_surfEK80_db,freqs_EK80_db = pickle.load(f)

#with open(filename_EK80h) as f:
#    time_EK80h_db,depth_surface_EK80h_db,Sv_surfEK80h_db,Sa_surfEK80h_db,lat_surfEK80h_db,lon_surfEK80h_db,vol_surfEK80h_db,freqs_EK80_db = pickle.load(f)

#with open(filename_ME70) as f:
#    time_ME70_db,depth_surface_ME70_db,depth_bottom_ME70_db,Sv_surfME70_db,Sv_botME70_db,Sa_surfME70_db,Sa_botME70_db,lat_surfME70_db,lon_surfME70_db,lat_botME70_db,lon_botME70_db,vol_surfME70_db,freqs_ME70_db = pickle.load(f)

# Exploratory plots
#choice for transducer
channelidx=1

# beam > layer > 
print 'type de depth_surface_EK80_db : ' + str(type(depth_surface_EK80_db))
print 'len  de depth_surface_EK80_db : ' + str(len(depth_surface_EK80_db))

print 'type  de depth_surface_EK80_db[0] : ' + str(type(depth_surface_EK80_db[0]))
print 'type  de depth_surface_EK80_db[0] : ' + str(len(depth_surface_EK80_db[0]))

print 'type  de depth_surface_EK80_db[0][0] : ' + str(type(depth_surface_EK80_db[0][0]))
print 'type  de depth_surface_EK80_db[0][0] : ' + str(len(depth_surface_EK80_db[0][0]))

print 'type  de depth_bottom_EK80_db[0][0] : ' + str(type(depth_bottom_EK80_db[0][0]))
print 'type  de depth_bottom_EK80_db[0][0] : ' + str(len(depth_bottom_EK80_db[0][0]))

print 'type de Sv_surfEK80_db : ' + str(type(Sv_surfEK80_db))
print 'len  de Sv_surfEK80_db : ' + str(len(Sv_surfEK80_db))

print 'type  de Sv_surfEK80_db[0] : ' + str(type(Sv_surfEK80_db[0]))
print 'type  de Sv_surfEK80_db[0] : ' + str(len(Sv_surfEK80_db[0]))

print 'type  de Sv_surfEK80_db[0][0] : ' + str(type(Sv_surfEK80_db[0][0]))
print 'type  de Sv_surfEK80_db[0][0] : ' + str(len(Sv_surfEK80_db[0][0]))

print 'type  de Sv_surfEK80_db[0][0][0] : ' + str(type(Sv_surfEK80_db[0][0][0]))
print 'type  de Sv_surfEK80_db[0][0][0] : ' + str(len(Sv_surfEK80_db[0][0][0]))

print 'type  de Sv_surfEK80_db[0][2][0] : ' + str(type(Sv_surfEK80_db[0][2][0]))
print 'type  de Sv_surfEK80_db[0][2][0] : ' + str(len(Sv_surfEK80_db[0][2][0]))


#Bottom detection (errors?)
fig, ax = plt.subplots()
ax.plot([datetime.datetime.utcfromtimestamp(ts) for ts in time_EK80_db],[depths[0][0] for depths in depth_bottom_EK80_db])
xfmt = md.DateFormatter('%H:%M')
ax.xaxis.set_major_formatter(xfmt)
ax.set_title('Bottom depth', fontsize=16)
plt.xlabel('Time', fontsize=15)
plt.ylabel('Bottom depth', fontsize=15)

# choice for surface layer
startlayer=1
endlayer=2


# plot Sv/ESU
fig, ax1 = plt.subplots()
plt.xlabel('Time', fontsize=15)
plt.ylabel('Sv', fontsize=15)
nfreq=len(freqs_EK80_db[0][channelidx])
for iFreq in range(nfreq):
    print "compute plot for freq " + str(iFreq)
    #nbBeams = len(Sv_surfEK80_db)
    #for iBeam in range(nbBeams):
    #    print str(len(Sv_surfEK80_db[iBeam][0][0]))
    for iLayer in range(startlayer,endlayer):
        ax1.plot([datetime.datetime.utcfromtimestamp(ts) for ts in time_EK80_db],[svs[channelidx][iLayer][iFreq] for svs in Sv_surfEK80_db], label=('Freq %d / Layer %d' % (iFreq+1, iLayer+1)))
ax1.xaxis.set_major_formatter(xfmt)

plt.grid()
ax1.legend()
ax1.set_title('Sv', fontsize=16)
plt.show()