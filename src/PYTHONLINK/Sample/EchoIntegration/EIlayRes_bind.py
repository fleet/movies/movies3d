﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

import pickle

def EIlayRes_bind(fpath,filename_ME70,filename_EK80,filename_EK80h):

    filelist = []
    for root, subFolders, files in os.walk(fpath):
        for file in files:
            if file.endswith('.pickle'):
                filelist.append(os.path.join(root,file))
            
    depth_bottom_ME70_db=[]
    depth_surface_ME70_db=[]
    lat_surfME70_db=[]
    lon_surfME70_db=[]
    lat_botME70_db=[]
    lon_botME70_db=[]
    Sa_surfME70_db = []
    Sa_botME70_db = []
    Sv_surfME70_db = []
    Sv_botME70_db = []
    time_ME70_db = []
    vol_surfME70_db = []

    depth_bottom_EK80_db=[]
    depth_surface_EK80_db=[]
    lat_surfEK80_db=[]
    lon_surfEK80_db=[]
    lat_botEK80_db=[]
    lon_botEK80_db=[]
    Sa_surfEK80_db = []
    Sa_botEK80_db = []
    Sv_surfEK80_db = []
    Sv_botEK80_db = []
    time_EK80_db = []
    vol_surfEK80_db = []

    depth_surface_EK80h_db=[]
    lat_surfEK80h_db=[]
    lon_surfEK80h_db=[]
    Sa_surfEK80h_db = []
    Sv_surfEK80h_db = []
    time_EK80h_db = []
    vol_surfEK80h_db = []

    freqs_EK80_db = []
    freqs_EK80h_db = []
    freqs_ME70_db = []

    for FileName in filelist:
        
        with open(FileName) as f:
            #time_EK80,time_EK80h,time_ME70,Sa_surfME70, Sa_botME70, Sa_surfEK80, Sa_botEK80, Sa_surfEK80h,Sv_surfME70, Sv_botME70, Sv_surfEK80, Sv_botEK80, Sv_surfEK80h,Lat_surfME70, Long_surfME70, Depth_surfME70,Lat_surfEK80,Long_surfEK80,Depth_surfEK80,Lat_surfEK80h,Long_surfEK80h,Depth_surfEK80h,Lat_botME70, Long_botME70, Depth_botME70,Lat_botEK80,Long_botEK80,Depth_botEK80,Volume_surfEK80,Volume_botEK80,Volume_surfEK80h,Volume_surfME70,Volume_botME70 = pickle.load(f)
            time_EK80, time_EK80h, time_ME70, Sv_surfME70, Sv_botME70, Sv_surfEK80, Sv_botEK80, Sv_surfEK80h, Lat_surfME70, Long_surfME70, Depth_surfME70, Lat_surfEK80, Long_surfEK80, Depth_surfEK80, Lat_surfEK80h, Long_surfEK80h, Depth_surfEK80h, Lat_botME70, Long_botME70, Depth_botME70, Lat_botEK80, Long_botEK80, Depth_botEK80, Volume_surfEK80, Volume_botEK80, Volume_surfEK80h, Volume_surfME70, Volume_botME70, Freqs_EK80, Freqs_EK80h, Freqs_ME70 = pickle.load(f)

            time_ME70_db = time_ME70_db + time_ME70
            depth_bottom_ME70_db = depth_bottom_ME70_db + Depth_botME70
            depth_surface_ME70_db = depth_surface_ME70_db + Depth_surfME70
            #Sa_surfME70_db = Sa_surfME70_db + Sa_surfME70
            #Sa_botME70_db = Sa_botME70_db + Sa_botME70
            Sv_surfME70_db = Sv_surfME70_db + Sv_surfME70
            Sv_botME70_db = Sv_botME70_db + Sv_botME70
            lat_surfME70_db = lat_surfME70_db + Lat_surfME70
            lat_botME70_db = lat_botME70_db + Lat_botME70
            lon_surfME70_db = lon_surfME70_db + Long_surfME70
            lon_botME70_db = lon_botME70_db + Long_botME70
            vol_surfME70_db = vol_surfME70_db + Volume_surfME70

            time_EK80_db = time_EK80_db + time_EK80
            depth_bottom_EK80_db = depth_bottom_EK80_db + Depth_botEK80
            depth_surface_EK80_db = depth_surface_EK80_db + Depth_surfEK80
            #Sa_surfEK80_db = Sa_surfEK80_db + Sa_surfEK80
            #Sa_botEK80_db = Sa_botEK80_db + Sa_botEK80
            Sv_surfEK80_db = Sv_surfEK80_db + Sv_surfEK80
            Sv_botEK80_db = Sv_botEK80_db + Sv_botEK80
            lat_surfEK80_db = lat_surfEK80_db + Lat_surfEK80
            lat_botEK80_db = lat_botEK80_db + Lat_botEK80
            lon_surfEK80_db = lon_surfEK80_db + Long_surfEK80
            lon_botEK80_db = lon_botEK80_db + Long_botEK80
            vol_surfEK80_db = vol_surfEK80_db + Volume_surfEK80
               
            time_EK80h_db = time_EK80h_db + time_EK80h
            depth_surface_EK80h_db = depth_surface_EK80h_db + Depth_surfEK80h
            #Sa_surfEK80h_db = Sa_surfEK80h_db + Sa_surfEK80h
            Sv_surfEK80h_db = Sv_surfEK80h_db + Sv_surfEK80h
            lat_surfEK80h_db = lat_surfEK80h_db + Lat_surfEK80h
            lon_surfEK80h_db = lon_surfEK80h_db + Long_surfEK80h
            vol_surfEK80h_db = vol_surfEK80h_db + Volume_surfEK80h

            freqs_EK80_db = freqs_EK80_db + Freqs_EK80
            freqs_EK80h_db = freqs_EK80h_db + Freqs_EK80h
            freqs_ME70_db = freqs_ME70_db + Freqs_ME70


    with open(filename_ME70, 'w') as f:
        pickle.dump([time_ME70_db,depth_surface_ME70_db,depth_bottom_ME70_db,Sv_surfME70_db,Sv_botME70_db,Sa_surfME70_db,Sa_botME70_db,lat_surfME70_db,lon_surfME70_db,lat_botME70_db,lon_botME70_db,vol_surfME70_db,freqs_ME70_db], f)
    with open(filename_EK80, 'w') as f:
        pickle.dump([time_EK80_db,depth_surface_EK80_db,depth_bottom_EK80_db,Sv_surfEK80_db,Sv_botEK80_db,Sa_surfEK80_db,Sa_botEK80_db,lat_surfEK80_db,lon_surfEK80_db,lat_botEK80_db,lon_botEK80_db,vol_surfEK80_db,freqs_EK80_db], f)
    with open(filename_EK80h, 'w') as f:
        pickle.dump([time_EK80h_db,depth_surface_EK80h_db,Sv_surfEK80h_db,Sa_surfEK80h_db,lat_surfEK80h_db,lon_surfEK80h_db,vol_surfEK80h_db,freqs_EK80h_db], f)
