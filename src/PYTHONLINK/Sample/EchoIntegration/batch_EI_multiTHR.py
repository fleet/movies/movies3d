﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

from pyMovies import *

from SampleEchoIntegration import SampleEchoIntegration

import os

def batch_EI_multiTHR(path_hac_survey,path_config,path_save,thresholds,runs,nameTransect=None,dateStart=None,timeStart=None,dateEnd=None,timeEnd=None):
    for ir in runs:
        for t in thresholds:
            #build dedicated string
            str_run = "%03d" % (ir)
            chemin_config = path_config + '\\' + str(t)
            if not os.path.exists(chemin_config):
               raise RuntimeError(chemin_config + ' : Invalid path for MOVIES3D configuration')
            
            chemin_hac = path_hac_survey + r'\RUN' + str_run
            if not os.path.exists(chemin_hac):
                raise RuntimeError(chemin_hac + ' : Invalid path for HAC files')
            else:
                filelist = [f for f in os.listdir(chemin_hac) if f.endswith('.hac')]
                if len(filelist) == 0:
                    raise RuntimeError(chemin_hac + ' does not contain HAC files')
            
            chemin_save = path_save + r'\RUN' + str_run + '\\' + str(t)
            
            #load configuration
            moLoadConfig(chemin_config)
       
            #Activate 'EchoIntegrModule'
            EchoIntegrModule = moEchoIntegration()
            EchoIntegrModule.setEnable(True)
            
            #Run EI
            SampleEchoIntegration(chemin_hac,chemin_save,EchoIntegrModule,nameTransect,dateStart,timeStart,dateEnd,timeEnd);
       
            # Disable 'EchoIntegrModule'
            EchoIntegrModule.setEnable(False)
