﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from matplotlib import colors


def DrawEchoGram(echoData, title=None, bottoms=None ):

    # Définition d'une nouvelle figure
    fig, ax = plt.subplots()
    
    # Définition de la liste des couleurs pour la colormap
    cmap = colors.ListedColormap(list(reversed(['#000000', '#800000', '#ac0000', '#d00000', '#f00000', '#ff0000', '#ff7979', '#ff8040', '#ff8000', '#ffff00', '#ffff80', '#00ff00', '#80ff80', '#0080c0', '#0080ff', '#80ffff', '#C0C0C0', '#FFFFFF'])))
    #cmap=None
    
    # On affiche l'échogramme
    cax = plt.imshow(echoData, cmap=cmap, vmin=-80, vmax=-30)
    
    # Affichage de la colorbar
    cbar = fig.colorbar(cax)
    
    # Affichage d'un titre
    if title is not None:
        ax.set_title(title)
    
    # Affichage du fond
    if bottoms is not None:
        plt.autoscale(False) # Pour que les dimensions de la fenêtres soient calées sur l'image originale
        plt.plot(bottoms,'w',linewidth=2)
    
    # Affichage de la grille
    plt.grid()
    
    # Affichage de la/les figures définies ci-dessus
    plt.show()
