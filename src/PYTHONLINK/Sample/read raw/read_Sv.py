﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

from pyMovies import *

from drawing import DrawEchoGram

import time
import calendar
import math
import numpy as np
import matplotlib.pyplot as plt


chemin_ini=r"L:\PELGAS18" + "\\"

FileName = chemin_ini + "PELGAS18_018_20180516_070522.hac"

MaxRange=150

if False:
    ParameterDef = moLoadKernelParameter()

    # longueur auto pour agrandir la mémoire pour permettre de lire un fichier
    # entier 1=auto
    ParameterDef.m_bAutoLengthEnable=1
    ParameterDef.m_MaxRange = MaxRange

    # on utilise la détection ou non pour l'allocation des données en range
    # 0=non
    ParameterDef.m_bAutoDepthEnable=0
    # 0 on prend en compte les phases
    ParameterDef.m_bIgnorePhaseData=0
    # 0 on prend en compte les pings même sans nav
    ParameterDef.m_bIgnorePingsWithNoNavigation=0

    moSaveKernelParameter(ParameterDef)

    ParameterDef=moLoadReaderParameter()

    ParameterDef.m_ChunckDef.m_chunkNumberOfPingFan=200
    
    moSaveReaderParameter(ParameterDef)
else:
    moLoadConfig(chemin_ini)

moOpenHac(FileName)

FileStatus = moGetFileStatus()
while not FileStatus.m_StreamClosed:
    moReadChunk()

print 'End of File'


list_sounder=moGetSounderDefinition()
nb_snd=list_sounder.GetNbSounder()

print 'nb sondeurs = ' + str(nb_snd)
for isdr in range(nb_snd):
    sounder = list_sounder.GetSounder(isdr)
    nb_transduc=sounder.m_numberOfTransducer
    print 'sondeur ' + str(isdr) + ':    index: ' + str(sounder.m_SounderId) + '   nb trans:' + str(nb_transduc)
    for itr in range(nb_transduc):
        trans = sounder.GetTransducer(itr)      
        for ibeam in range(trans.m_numberOfSoftChannel):
            softChan = trans.getSoftChannelPolarX(ibeam)
            print '   trans ' + str(itr) + ':    nom: ' + trans.m_transName + '   freq: ' + str(softChan.m_acousticFrequency/1000) + ' kHz';
            
            
#à modifier
index_sondeur=0
index_trans=1
sounder = list_sounder.GetSounder(index_sondeur)
transducer = sounder.GetTransducer(index_trans)
nb_beams=transducer.m_numberOfSoftChannel
nb_pings=moGetNumberOfPingFan()
nb_ech=int(math.floor(MaxRange/transducer.m_beamsSamplesSpacing))

dpth_ech=np.full([nb_pings,nb_beams], np.nan)
time_ping=np.full(nb_pings,np.nan)
u=np.full([nb_pings,nb_ech,nb_beams], np.nan)
indexping=0

for index in range(nb_pings):
    MX = moGetPingFan(index)
    SounderDesc = MX.m_pSounder
    if SounderDesc.m_SounderId == sounder.m_SounderId :
        SounderDesc_ref=SounderDesc;
        polarMat = MX.GetPolarMatrix(index_trans)
        echoValues = np.array(polarMat.m_Amplitude)/100.0
        for iEcho in range(min(len(polarMat.m_Amplitude), nb_ech)):
            u[indexping,iEcho,:]=echoValues[iEcho,:]
        for b in range(nb_beams):
            if MX.beam_data[b].m_bottomWasFound:
                dpth_ech[indexping,b]=MX.beam_data[b].m_bottomRange/SounderDesc.GetTransducer(index_trans).m_beamsSamplesSpacing                
        time_ping[indexping]=MX.m_meanTime.m_TimeCpu+MX.m_meanTime.m_TimeFraction/10000
        indexping=indexping+1
                
u.resize((indexping,nb_ech,nb_beams))
time_ping.resize((indexping))
dpth_ech.resize((indexping,nb_beams))
        
steering=np.zeros([nb_beams])
for b in range(nb_beams):
    steering[b]=SounderDesc_ref.GetTransducer(index_trans).getSoftChannelPolarX(b).m_mainBeamAthwartSteeringAngleRad
    
datestart = calendar.timegm(time.strptime('2018/04/20 17:53:00', '%Y/%m/%d %H:%M:%S'))
dateend = calendar.timegm(time.strptime('2018/06/05 17:59:00', '%Y/%m/%d %H:%M:%S'))

dateindex=np.where(np.logical_and(time_ping > datestart, time_ping < dateend))

for b in range(nb_beams):

    if abs(steering[b]*180/math.pi)<1 :
    
        # affichage
        DrawEchoGram(np.transpose(u[dateindex,:,b][0]),
            title=str(SounderDesc_ref.GetTransducer(index_trans).getSoftChannelPolarX(b).m_acousticFrequency/1000) + ' kHz',
            bottoms=dpth_ech[dateindex,b][0]+1.5)
