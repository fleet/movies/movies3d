#ifndef MOV_PY_CALIBRATION
#define MOV_PY_CALIBRATION

class CalibrationModule;

namespace pyMovies {
	
	CalibrationModule * pyCalibration();
	
	void wrapCalibration();
}

#endif // MOV_PY_CALIBRATION