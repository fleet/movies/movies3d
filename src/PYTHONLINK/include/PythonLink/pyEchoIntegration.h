/******************************************************************************/
/*	Project:	MOVIES 3D																											  */
/*	Author(s):	O. TONCK (IPSIS)																							*/
/*	File:		pyEchoIntegration.h																											*/
/******************************************************************************/

#ifndef MOV_PY_ECHOINTEGRATION
#define MOV_PY_ECHOINTEGRATION

class EchoIntegrationModule;

namespace pyMovies {

  EchoIntegrationModule * pyEchoIntegration();

  void wrapEchoIntegration();

}

#endif // MOV_PY_ECHOINTEGRATION