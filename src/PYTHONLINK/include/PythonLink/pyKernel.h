/******************************************************************************/
/*	Project:	MOVIES 3D																											  */
/*	Author(s):	O. TONCK (IPSIS)																							*/
/*	File:		pyKernel.h  																											*/
/******************************************************************************/

#ifndef MOV_PY_KERNEL
#define MOV_PY_KERNEL

class CurrentSounderDefinition;

namespace pyMovies {

  const CurrentSounderDefinition & pyGetSounderDefinition();

  void wrapKernel();

}

#endif // MOV_PY_KERNEL