/******************************************************************************/
/*	Project:	MOVIES 3D																											  */
/*	Author(s):	O. TONCK (IPSIS)																							*/
/*	File:		pyKernelParameter.h																											*/
/******************************************************************************/

#ifndef MOV_PY_KERNEL_PAREMETER
#define MOV_PY_KERNEL_PAREMETER

#include "M3DKernel/datascheme/KernelParameter.h"

namespace pyMovies {

  KernelParameter pyLoadKernelParameter();
  void pySaveKernelParameter(KernelParameter & kernelParams);

  void wrapKernelParameter();

}

#endif // MOV_PY_KERNEL_PAREMETER