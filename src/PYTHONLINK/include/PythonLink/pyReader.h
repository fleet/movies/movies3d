/******************************************************************************/
/*	Project:	MOVIES 3D																											  */
/*	Author(s):	O. TONCK (IPSIS)																							*/
/*	File:		pyReader.h  																											*/
/******************************************************************************/

#ifndef MOV_PY_READER
#define MOV_PY_READER

#include <cstddef>

class MovReadService;
class PingFan;

namespace pyMovies {

  void pyLoadConfig(const char * hacFile);
  void pyOpenHac(const char * hacFile);
  void pyOpenSonarNetCDF(const char * hacFile);
  MovReadService * pyGetFileStatus();
  void pyReadChunk();
  void pyGoTo(double dbTargetTime);

  unsigned int pyGetNumberOfPingFan();
  PingFan * pyGetPingFan(size_t pingIdx);
  void pyRemovePing(unsigned int pingId);

  void wrapReader();

}

#endif // MOV_PY_READER
