/******************************************************************************/
/*	Project:	MOVIES 3D																											  */
/*	Author(s):	O. TONCK (IPSIS)																							*/
/*	File:		pyReaderParameter.h																											*/
/******************************************************************************/

#ifndef MOV_PY_READER_PARAMETER
#define MOV_PY_READER_PARAMETER

#include "Reader/ReaderParameter.h"

namespace pyMovies {

  ReaderParameter pyLoadReaderParameter();
  void pySaveReaderParameter(ReaderParameter & readerParams);

  void wrapReaderParameter();

}

#endif // MOV_PY_READER_PARAMETER
