﻿#pragma once

class ShoalExtractionModule;

namespace pyMovies {

    ShoalExtractionModule * pyShoalExtraction();

    void wrapShoalExtraction();

}
