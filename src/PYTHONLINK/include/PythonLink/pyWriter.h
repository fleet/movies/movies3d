/****************************************************************************/
/*	Project:	MOVIES 3D													*/
/*	Author(s):	N. MENARD (ITLINK)											*/
/*	File:		pyWriter.h  												*/
/****************************************************************************/

#ifndef MOV_PY_WRITER
#define MOV_PY_WRITER

#include "Reader/WriteAlgorithm/WriteAlgorithm.h"

class MovReadService;
class PingFan;

namespace pyMovies {

  void pyStartWrite(const char * destDirBuffer, const char * fileNamePrefix, const WriteAlgorithm::OutputType& outputType = WriteAlgorithm::OutputType::Hac);
  void pyStopWrite();

  void wrapWriter();

}

#endif // MOV_PY_WRITER