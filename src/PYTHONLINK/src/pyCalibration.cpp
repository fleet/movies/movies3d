#include "PythonLink/pyCalibration.h"

#define HAVE_ROUND
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "ModuleManager/ModuleManager.h"
#include "Calibration/CalibrationModule.h"

namespace boost
{
	/*
	* ACN : Fix boost::get_pointer dans Visual Studio 2015 Update 3
	* https://stackoverflow.com/questions/38261530/unresolved-external-symbols-since-visual-studio-2015-update-3-boost-python-link
	* https://connect.microsoft.com/VisualStudio/Feedback/Details/2852624
	*/
	template <>
	inline CalibrationModule const volatile * get_pointer(class CalibrationModule const volatile *em) {
		return em;
	}
}

using namespace pyMovies;

CalibrationModule * pyMovies::pyCalibration()
{
	return CModuleManager::getInstance()->GetCalibrationModule();
}

void pyMovies::wrapCalibration()
{
  using namespace boost::python;

  def("moCalibration", pyCalibration, return_value_policy<reference_existing_object>());

  class_<CalibrationModule, boost::noncopyable>("CalibrationModule", no_init)
	  .def("getCalibrationGain", &CalibrationModule::getCalibrationGain);
}
