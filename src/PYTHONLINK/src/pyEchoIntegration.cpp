
#include "PythonLink/pyEchoIntegration.h"

#define HAVE_ROUND
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "ModuleManager/ModuleManager.h"
#include "EchoIntegration/EchoIntegrationModule.h"
#include "EchoIntegration/LayerDef.h"
#include "EchoIntegration/EchoIntegrationLayerResult.h"

namespace boost
{
	/*
	* ACN : Fix boost::get_pointer dans Visual Studio 2015 Update 3
	* https://stackoverflow.com/questions/38261530/unresolved-external-symbols-since-visual-studio-2015-update-3-boost-python-link
	* https://connect.microsoft.com/VisualStudio/Feedback/Details/2852624
	*/
	template <>
	inline EchoIntegrationModule const volatile * get_pointer(class EchoIntegrationModule const volatile *em) {
		return em;

	}
	template <>
	inline HacTime const volatile * get_pointer(class HacTime const volatile *em) {
		return em;

	}
	template <>
	inline EchoIntegrationChannelResult const volatile * get_pointer(class EchoIntegrationChannelResult const volatile *em) {
		return em;

	}
	template <>
	inline EchoIntegrationLayerResult const volatile * get_pointer(class EchoIntegrationLayerResult const volatile *em) {
		return em;

	}
	template <>
	inline EchoIntegrationParameter const volatile * get_pointer(class EchoIntegrationParameter const volatile *em) {
		return em;

	}
	template <>
	inline LayerDef const volatile * get_pointer(class LayerDef const volatile *em) {
		return em;

	}
	template <>
	inline EchoIntegrationOutput const volatile * get_pointer(class EchoIntegrationOutput const volatile *em) {
		return em;

	}
}

using namespace pyMovies;

namespace
{
	constexpr const char * LoggerName = "PythonLink.pyEchoIntegration";
}

EchoIntegrationModule * pyMovies::pyEchoIntegration()
{
  EchoIntegrationModule * pResult = CModuleManager::getInstance()->GetEchoIntegrationModule();
  // Instanciation du consumer matlab (ou python en l'occurence...)
  pResult->GetMatlabModuleConsumer();
  return pResult;
}

boost::python::object wrap_EchoIntegrationOutputList(EchoIntegrationModule * echoIntegrationModule)
{
  std::vector<EchoIntegrationOutput*> results;
  const auto& moduleOutputConsumer = echoIntegrationModule->GetMatlabModuleConsumer();
  if(moduleOutputConsumer)
  {
  	results = echoIntegrationModule->GetOutputs(moduleOutputConsumer->GetConsumerId());
  }
	
  return boost::python::object(results);
}

void wrap_EchoIntegrationReleaseOutputList(EchoIntegrationModule * echoIntegrationModule)
{
  const auto& moduleOutputConsumer = echoIntegrationModule->GetMatlabModuleConsumer();
  if(moduleOutputConsumer)
  {
    echoIntegrationModule->GetOutputContainer().Flush(moduleOutputConsumer->GetConsumerId());
  }
}

boost::python::object wrap_LayerVolumes(EchoIntegrationModule * echoIntegrationModule)
{
	boost::python::list result;

	M3DKernel * pKern = M3DKernel::GetInstance();

	// couches
	const auto& layers = echoIntegrationModule->GetEchoIntegrationParameter().getLayersDefs();
	const auto nbLayers = layers.size();

	// nombre de sondeurs
	unsigned int nbSounders = pKern->getObjectMgr()->GetSounderDefinition().GetNbSounder();

	for (size_t layerIdx = 0; layerIdx < nbLayers; ++layerIdx)
	{
		// cr�ation du tableau des volumes du sondeur pour la couche layerIdx
		boost::python::list layerResult;
		
		const auto & layerDef = layers[layerIdx];

		for (unsigned int sounderIdx = 0; sounderIdx < nbSounders; sounderIdx++)
		{
			boost::python::list sounderResult;

			Sounder * pSound = pKern->getObjectMgr()->GetSounderDefinition().GetSounder(sounderIdx);
			unsigned int nbTrans = pSound->GetTransducerCount();

			for (unsigned int transIdx = 0; transIdx < nbTrans; transIdx++)
			{
				Transducer * pTrans = pSound->GetTransducer(transIdx);
				unsigned int nbChannel = pTrans->m_numberOfSoftChannel;

				for (unsigned int channelIdx = 0; channelIdx < nbChannel; channelIdx++)
				{
					double volume = 0.0;
					SoftChannel * pSoftChan = pTrans->getSoftChannelPolarX(channelIdx);
					// r�cup�ration de la sonde pour le dernier ping
					// OTK - 09/03/2010 - en cas d'absence de pings pour un sondeur, on ne d�clenche pas d'erreur bloquante, mais simplement
					// un warning (le cas o� un sondeur n'�met pas ne doit pas bloquer l'utilisation des autres sondeurs)
					// le volume sera nul pour les sondeurs sans ping.
					TimeObjectContainer *pCont = pKern->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(pSound->m_SounderId);
					if (!pCont)
					{
						M3D_LOG_WARN(LoggerName, "No PingFan Container...");
					}
					else
					{
						PingFan * lastPingFan = (PingFan*)pCont->GetLastDatedObject();
						if (!lastPingFan)
						{
							M3D_LOG_WARN(LoggerName, "No PingFan In PingFan Container...");
						}
						else
						{
							std::uint32_t echoFondEval = 0;
							std::int32_t bottomRange = 0;
							bool found = false;
							lastPingFan->getBottom(pTrans->GetChannelId()[channelIdx], echoFondEval, bottomRange, found);
							double bottom = pSound->GetSoftChannelCoordinateToWorldCoord(lastPingFan, transIdx, channelIdx, BaseMathLib::Vector3D(0, 0, bottomRange / 1000.0)).z;

							double minDepth, maxDepth;
							// cas des couches d�finies en distance au transducteur
							if (layerDef->GetLayerType() == Layer::Type::DistanceLayer)
							{
								minDepth = layerDef->GetMinDepth();
								maxDepth = layerDef->GetMaxDepth();
							}
							else
							{
								// calcul des profondeurs r�f�renc�es transducteur et d�point�es
								double offset = pTrans->m_transDepthMeter + lastPingFan->getHeaveChan(pSoftChan->m_softChannelId);
								minDepth = layerDef->GetMinDepth(bottom) - offset;
								maxDepth = layerDef->GetMaxDepth(bottom) - offset;
                                minDepth = std::max(.0, minDepth);
                                maxDepth = std::max(.0, maxDepth);
								// application du d�pointage transversal
								minDepth = minDepth / cos(pSoftChan->m_mainBeamAthwartSteeringAngleRad);
								maxDepth = maxDepth / cos(pSoftChan->m_mainBeamAthwartSteeringAngleRad);
							}
							// calcul du volume - la profondeur est deja d�finie par rapport au transducteur, et d�point�e
							volume = (PI / 3.)*tan(pSoftChan->m_beam3dBWidthAlongRad / 2.)*tan(pSoftChan->m_beam3dBWidthAthwartRad / 2.)
								*((maxDepth*maxDepth*maxDepth) - (minDepth*minDepth*minDepth));
						}
					}

					// remplissage du tableau r�sultat
					sounderResult.append(volume);
				}
			}

			layerResult.append(sounderResult);
		} // Fin de la boucle sur les sondeurs

		result.append(layerResult);
	} // Fin de la boucle sur les couches

	return result;
}

void wrap_SetBroadcastAndRecordFilenamePrefix(EchoIntegrationParameter * echoIntegrationParameter, const std::string& filenamePrefix) {
	echoIntegrationParameter->GetParameterBroadcastAndRecord().m_filePrefix = filenamePrefix;
}

void wrap_SetBroadcastAndRecordFilePath(EchoIntegrationParameter * echoIntegrationParameter, const std::string& filePath) {
	echoIntegrationParameter->GetParameterBroadcastAndRecord().m_filePath = filePath;
}

void wrap_BroadcastAndRecordDisableCut(EchoIntegrationParameter * echoIntegrationParameter) {
	echoIntegrationParameter->GetParameterBroadcastAndRecord().m_prefixDate = false;
}

void wrap_SetBroadcastAndRecordSizeCut(EchoIntegrationParameter * echoIntegrationParameter, const int& sizeInMegaOctets) {
	echoIntegrationParameter->GetParameterBroadcastAndRecord().m_prefixDate = true;
	echoIntegrationParameter->GetParameterBroadcastAndRecord().m_cutType = BaseKernel::ParameterBroadcastAndRecord::TCutType::SIZE_CUT;
	echoIntegrationParameter->GetParameterBroadcastAndRecord().m_fileMaxSize = sizeInMegaOctets;
}

void wrap_SetBroadcastAndRecordTimeCut(EchoIntegrationParameter * echoIntegrationParameter, const int& timeInSeconds) {
	echoIntegrationParameter->GetParameterBroadcastAndRecord().m_prefixDate = true;
	echoIntegrationParameter->GetParameterBroadcastAndRecord().m_cutType = BaseKernel::ParameterBroadcastAndRecord::TCutType::TIME_CUT;
	echoIntegrationParameter->GetParameterBroadcastAndRecord().m_fileMaxTime = timeInSeconds;
}

void wrap_SetEnable(EchoIntegrationModule * echoIntegrationModule, const bool enable) {
	echoIntegrationModule->setEnable(enable);
	const auto parameter = echoIntegrationModule->GetEchoIntegrationParameter();
	if (parameter.isEnableTransducerFilter() && parameter.getActiveTransducerNames().empty())
	{
		throw std::runtime_error("EchoIntegration - No Transducer selected for EchoIntegration");
	}
}

void pyMovies::wrapEchoIntegration()
{
  using namespace boost::python;

  def("moEchoIntegration", pyEchoIntegration, return_value_policy<reference_existing_object>());

  class_<EchoIntegrationModule, boost::noncopyable>("EchoIntegrationModule", no_init)
	.def("setEnable", wrap_SetEnable)
	.def("getEnable", &EchoIntegrationModule::getEnable)
	.def("GetEchoIntegrationParameter", static_cast<EchoIntegrationParameter &(EchoIntegrationModule::*)()>(&EchoIntegrationModule::GetEchoIntegrationParameter), return_value_policy<reference_existing_object>())
	.def("GetOutputList", wrap_EchoIntegrationOutputList)
    .def("ReleaseOutputList", wrap_EchoIntegrationReleaseOutputList)
	.def("GetLayersVolumes", wrap_LayerVolumes);

  class_<EchoIntegrationParameter, boost::noncopyable>("EchoIntegrationParameter", no_init)
	.def("m_tabLayerDef", &EchoIntegrationParameter::getLayersDefs, return_value_policy<reference_existing_object>())
	.def("setFilenamePrefix", wrap_SetBroadcastAndRecordFilenamePrefix)
	.def("setFilePath", wrap_SetBroadcastAndRecordFilePath)
	.def("disableCut", wrap_BroadcastAndRecordDisableCut)
	.def("setSizeCutInMo", wrap_SetBroadcastAndRecordSizeCut)
	.def("setTimeCutInSeconds", wrap_SetBroadcastAndRecordTimeCut);

  class_<std::vector<LayerDef*> >("std::vector<LayerDef*>")
	  .def(vector_indexing_suite<std::vector<LayerDef*>, true >());

  enum_<Layer::Type>("LayerType")
	  .value("SurfaceLayer", Layer::Type::SurfaceLayer)
	  .value("BottomLayer", Layer::Type::BottomLayer)
	  .value("DistanceLayer", Layer::Type::DistanceLayer);

  class_<LayerDef, boost::noncopyable>("LayerDef", no_init)
	  .def_readonly("m_minDepth", &LayerDef::GetStart)
	  .def_readonly("m_maxDepth", &LayerDef::GetEnd)
	  .def_readonly("m_layerType", &LayerDef::GetLayerType);
  
  class_<std::vector<EchoIntegrationOutput*> >("std::vector<EchoIntegrationOutput*>")
	  .def(vector_indexing_suite<std::vector<EchoIntegrationOutput*>, true >());

  class_<EchoIntegrationOutput, boost::noncopyable>("EchoIntegrationOutput", no_init)
	.def_readonly("m_SounderId", &EchoIntegrationOutput::m_SounderID)
    .def_readonly("m_timeEnd", &EchoIntegrationOutput::m_timeEnd)
    .def_readonly("m_tabChannelResult", &EchoIntegrationOutput::m_tabChannelResult);

  class_<std::vector<EchoIntegrationChannelResult*> >("std::vector<EchoIntegrationChannelResult*>")
	  .def(vector_indexing_suite<std::vector<EchoIntegrationChannelResult*>, true >());

  class_<EchoIntegrationChannelResult, boost::noncopyable>("EchoIntegrationChannelResult", no_init)
	  .def_readonly("m_tabLayerResult", &EchoIntegrationChannelResult::m_tabLayerResult)
	  .def_readonly("m_tabFrequencies", &EchoIntegrationChannelResult::m_tabFrequencies)
	  .def_readonly("m_bottomDepth", &EchoIntegrationChannelResult::m_BottomDepth)
	  .def_readonly("m_bottomLatitude", &EchoIntegrationChannelResult::m_latitude)
	  .def_readonly("m_bottomLongitude", &EchoIntegrationChannelResult::m_longitude);

  class_< std::vector<double> >("std::vector<double>")
	  .def(vector_indexing_suite<std::vector<double>, true >());

  class_<std::vector<std::vector<EchoIntegrationLayerResult*> > >("std::vector<std::vector<EchoIntegrationLayerResult*> >")
	  .def(vector_indexing_suite<std::vector<std::vector<EchoIntegrationLayerResult*> >, true >());

  class_<std::vector<EchoIntegrationLayerResult*> >("std::vector<EchoIntegrationLayerResult*>")
	  .def(vector_indexing_suite<std::vector<EchoIntegrationLayerResult*>, true >());

  class_<EchoIntegrationLayerResult, boost::noncopyable>("EchoIntegrationLayerResult")
    .def_readonly("m_Sv", &EchoIntegrationLayerResult::GetSv)
    .def_readonly("m_Sa", &EchoIntegrationLayerResult::GetSa)
    .def_readonly("m_Depth", &EchoIntegrationLayerResult::GetDepth)
    .def_readonly("m_latitude", &EchoIntegrationLayerResult::GetLatitude)
    .def_readonly("m_longitude", &EchoIntegrationLayerResult::GetLongitude)
	.def_readonly("m_Ni", &EchoIntegrationLayerResult::GetNi)
	.def_readonly("m_Nt", &EchoIntegrationLayerResult::GetNt)
	.def_readonly("m_energy", &EchoIntegrationLayerResult::GetEnergy)
	.def_readonly("m_pondEnergy", &EchoIntegrationLayerResult::GetPondEnergy)
	.def_readonly("m_volume", &EchoIntegrationLayerResult::GetVolume)
	.def_readonly("m_surface", &EchoIntegrationLayerResult::GetSurface);
}
