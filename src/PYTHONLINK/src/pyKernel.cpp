
#include "PythonLink/pyKernel.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/Environnement.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/TransmitSignalObject.h"
#include "M3DKernel/datascheme/MovESUMgr.h"

#define HAVE_ROUND
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

namespace boost
{
	/*
	* ACN : Fix boost::get_pointer dans Visual Studio 2015 Update 3
	* https://stackoverflow.com/questions/38261530/unresolved-external-symbols-since-visual-studio-2015-update-3-boost-python-link
	* https://connect.microsoft.com/VisualStudio/Feedback/Details/2852624
	*/
	template <>
	inline HacObjectMgr const volatile * get_pointer(class HacObjectMgr const volatile *em) {
		return em;
	}
	template <>
	inline TimeObjectContainer const volatile * get_pointer(class TimeObjectContainer const volatile *em) {
		return em;
	}	
	template <>
	inline DatedObject const volatile * get_pointer(class DatedObject const volatile *em) {
		return em;
	}
	template <>
	inline Environnement const volatile * get_pointer(class Environnement const volatile *em) {
		return em;
	}
	template <>
	inline Measure const volatile * get_pointer(class Measure const volatile *em) {
		return em;
	}
	template <>
	inline SounderComputeData const volatile * get_pointer(class SounderComputeData const volatile *em) {
		return em;
	}
	template <>
	inline SoftChannelComputeData const volatile * get_pointer(class SoftChannelComputeData const volatile *em) {
		return em;
	}
	template <>
	inline CurrentSounderDefinition const volatile * get_pointer(class CurrentSounderDefinition const volatile *em) {
		return em;
	}
	template <>
	inline Sounder const volatile * get_pointer(class Sounder const volatile *em) {
		return em;
	}
	template <>
	inline Transducer const volatile * get_pointer(class Transducer const volatile *em) {
		return em;
	}
	template <>
	inline Platform const volatile * get_pointer(class Platform const volatile *em) {
		return em;
	}
	template <>
	inline SoftChannel const volatile * get_pointer(class SoftChannel const volatile *em) {
		return em;
	}
	template <>
	inline SplitBeamParameter const volatile * get_pointer(class SplitBeamParameter const volatile *em) {
		return em;
	}
	template <>
	inline TransmitSignalObject const volatile * get_pointer(class TransmitSignalObject const volatile *em) {
		return em;
	}
}

const CurrentSounderDefinition & pyMovies::pyGetSounderDefinition()
{
	return M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().m_sounderDefinition;
}

HacObjectMgr * pyGetObjectManager()
{
	return M3DKernel::GetInstance()->getObjectMgr();
}

MovESUMgr * pyGetEsuManager()
{
	return M3DKernel::GetInstance()->getMovESUManager();
}

double wrap_soundVelocity(Sounder const * pSounder) {
	return pSounder->m_soundVelocity.get();
}

boost::python::object wrap_transName(Transducer const * pTrans) {
	std::string transName = pTrans->m_transName;
	return boost::python::object(transName);
}

boost::python::object wrap_channelName(SoftChannel const * pSoftChan) {
  std::string channelName = pSoftChan->m_channelName;
  return boost::python::object(channelName);
}

template<typename T>
boost::python::list wrap_vector(const std::vector<T> & v)
{
	boost::python::list l;
	for (const T & e : v)
	{
		l.append(e);
	}
	return l;
}

boost::python::list wrap_env_measures(const Environnement * e)
{
	return wrap_vector(e->GetMeasures());

}

void pyMovies::wrapKernel()
{
  using namespace boost::python;

  def("moGetObjectManager", pyGetObjectManager, return_value_policy<reference_existing_object>());

  def("moGetSounderDefinition", pyGetSounderDefinition, return_value_policy<reference_existing_object>());
  
  class_<HacObjectMgr, boost::noncopyable>("HacObjectMgr", no_init)
	  .def("GetNavAttitudeContainerCount", &HacObjectMgr::GetNumberOfNavAttitudeSensorId)
	  .def("GetNavAttitudeSensorId", &HacObjectMgr::GetNavAttitudeSensorId)
	  .def("GetNavAttitudeContainer", &HacObjectMgr::GetNavAttitudeContainer, return_value_policy<reference_existing_object>())
	  .def("GetNavAttributesContainer", &HacObjectMgr::GetNavAttributesContainer, return_value_policy<reference_existing_object>())
	  .def("GetNavPositionContainer", &HacObjectMgr::GetNavPositionContainer, return_value_policy<reference_existing_object>())
	  .def("GetEventMarkerContainer", &HacObjectMgr::GetEventMarkerContainer, return_value_policy<reference_existing_object>())
	  .def("GetEnvironnementContainer", &HacObjectMgr::GetEnvironnementContainer, return_value_policy<reference_existing_object>());

  class_<TimeObjectContainer, boost::noncopyable>("TimeObjectContainer", no_init)
	  .def("count", &TimeObjectContainer::GetObjectCount)
	  .def("firstDatedObject", &TimeObjectContainer::GetFirstDatedObject, return_value_policy<reference_existing_object>())
	  .def("lastDatedObject", &TimeObjectContainer::GetLastDatedObject, return_value_policy<reference_existing_object>())
	  .def("datedObject", &TimeObjectContainer::GetDatedObject, return_value_policy<reference_existing_object>())
	  .def("findDatedObject", &TimeObjectContainer::FindDatedObject, return_value_policy<reference_existing_object>())
	  .def("findDatedObjects", &TimeObjectContainer::FindDatedObjects)
	  .def("findPreviousDatedObject", &TimeObjectContainer::FindPreviousDatedObject, return_value_policy<reference_existing_object>())
	  .def("findPreviousDatedObjects", &TimeObjectContainer::FindPreviousDatedObjects)
	  .def("findNextDatedObject", &TimeObjectContainer::FindNextDatedObject, return_value_policy<reference_existing_object>())
	  .def("findNextDatedObjects", &TimeObjectContainer::FindNextDatedObjects)
	  .def("findClosestDatedObject", &TimeObjectContainer::FindClosestDatedObject, return_value_policy<reference_existing_object>())
	  .def("findClosestDatedObjects", &TimeObjectContainer::FindClosestDatedObjects)
	;
  
  class_<std::vector<DatedObject*> >("std::vector<DatedObject*>").def(vector_indexing_suite<std::vector<DatedObject*>, true >());

  class_ <DatedObject, boost::noncopyable> ("DatedObject", no_init)
	  .def_readonly("objectTime", &DatedObject::m_ObjectTime);

  class_ <Environnement, bases<DatedObject>, boost::noncopyable>("Environnement", no_init)
	  .def("count", &Environnement::GetNumberOfMeasure)
	  .def("measure", &Environnement::GetMeasure, return_value_policy<reference_existing_object>())
	  .add_property("measures", wrap_env_measures);

  class_ <Measure>("Measure")
	  .def_readonly("pressure", &Measure::Pressure)
	  .def_readonly("temperature", &Measure::Temperature)
	  .def_readonly("conductivity", &Measure::Conductivity)
	  .def_readonly("speedOfSound", &Measure::SpeedOfSound)
	  .def_readonly("depthFromSurface", &Measure::DepthFromSurface)
	  .def_readonly("salinity", &Measure::Salinity)
	  .def_readonly("soundAbsorption", &Measure::SoundAbsorption);
 
  class_<CurrentSounderDefinition, boost::noncopyable>("CurrentSounderDefinition")
    .def("GetNbSounder", &CurrentSounderDefinition::GetNbSounder)
    .def("GetSounder", &CurrentSounderDefinition::GetSounder, return_value_policy<reference_existing_object>());

  class_<Sounder, boost::noncopyable>("Sounder", no_init)
    .def_readonly("m_SounderId", &Sounder::m_SounderId)
    .add_property("m_soundVelocity", wrap_soundVelocity)
    .def_readonly("m_triggerMode", &Sounder::m_triggerMode)
    .def_readonly("m_numberOfTransducer", &Sounder::m_numberOfTransducer)
    .def_readonly("m_pingInterval", &Sounder::m_pingInterval)
    .def_readonly("m_isMultiBeam", &Sounder::m_isMultiBeam)
    .def_readonly("m_sounderComputeData", &Sounder::m_sounderComputeData)
    .def("GetTransducer", &Sounder::GetTransducer, return_value_policy<reference_existing_object>());

  class_<SounderComputeData>("SounderComputeData")
    .def_readonly("m_nbBeamPerFan", &SounderComputeData::m_nbBeamPerFan);

  class_<Transducer, boost::noncopyable>("Transducer", no_init)
    .add_property("m_transName", wrap_transName)
    .def_readonly("m_transSoftVersion", &Transducer::m_transSoftVersion)
    .def_readonly("m_pulseDuration", &Transducer::m_pulseDuration)
    .def_readonly("m_pulseShape", &Transducer::m_pulseShape)
    .def_readonly("m_pulseSlope", &Transducer::m_pulseSlope)
    .def_readonly("m_pulseForm", &Transducer::m_pulseForm)
    .def_readonly("m_timeSampleInterval", &Transducer::m_timeSampleInterval)
    .def_readonly("m_frequencyBeamSpacing", &Transducer::m_frequencyBeamSpacing)
    .def_readonly("m_frequencySpaceShape", &Transducer::m_frequencySpaceShape)
    .def_readonly("m_transPower", &Transducer::m_transPower)
    .def_readonly("m_transDepthMeter", &Transducer::m_transDepthMeter)
    .def_readonly("m_platformId", &Transducer::m_platformId)
    .def_readonly("m_transShape", &Transducer::m_transShape)
    .def_readonly("m_transFaceAlongAngleOffsetRad", &Transducer::m_transFaceAlongAngleOffsetRad)
    .def_readonly("m_transFaceAthwarAngleOffsetRad", &Transducer::m_transFaceAthwarAngleOffsetRad)
    .def_readonly("m_transRotationAngleRad", &Transducer::m_transRotationAngleRad)
    .def_readonly("m_numberOfSoftChannel", &Transducer::m_numberOfSoftChannel)
    .add_property("m_beamsSamplesSpacing", &Transducer::getBeamsSamplesSpacing)
    .def("GetPlatform", &Transducer::GetPlatform, return_value_policy<reference_existing_object>())
    .def("getSoftChannelPolarX", &Transducer::getSoftChannelPolarX, return_value_policy<reference_existing_object>());

  class_<Platform, boost::noncopyable>("Platform", no_init)
	.def_readonly("sensor_ident", &Platform::m_attitudeSensorId)
    .def_readonly("along_ship_offset", &Platform::GetAlongShipOffset)
    .def_readonly("athwart_ship_offset", &Platform::GetAthwartShipOffset)
    .def_readonly("depth_offset", &Platform::GetDephtOffset);

  class_<SoftChannel, boost::noncopyable>("SoftChannel", no_init)
    .def_readonly("m_softChannelId", &SoftChannel::m_softChannelId)
    .add_property("m_channelName", wrap_channelName)
    .def_readonly("m_dataType", &SoftChannel::m_dataType)
    .def_readonly("m_beamType", &SoftChannel::m_beamType)
    .def_readonly("m_acousticFrequency", &SoftChannel::m_acousticFrequency)
    .def_readonly("m_startFrequency", &SoftChannel::m_startFrequency)
    .def_readonly("m_endFrequency", &SoftChannel::m_endFrequency)
    .def_readonly("m_startSample", &SoftChannel::m_startSample)
    .def_readonly("m_mainBeamAlongSteeringAngleRad", &SoftChannel::m_mainBeamAlongSteeringAngleRad)
    .def_readonly("m_mainBeamAthwartSteeringAngleRad", &SoftChannel::m_mainBeamAthwartSteeringAngleRad)
    .def_readonly("m_absorptionCoef", &SoftChannel::m_absorptionCoef)
    .def_readonly("m_transmissionPower", &SoftChannel::m_transmissionPower)
    .def_readonly("m_beamAlongAngleSensitivity", &SoftChannel::m_beamAlongAngleSensitivity)
    .def_readonly("m_beamAthwartAngleSensitivity", &SoftChannel::m_beamAthwartAngleSensitivity)
    .def_readonly("m_beam3dBWidthAlongRad", &SoftChannel::m_beam3dBWidthAlongRad)
    .def_readonly("m_beam3dBWidthAthwartRad", &SoftChannel::m_beam3dBWidthAthwartRad)
    .def_readonly("m_beamEquTwoWayAngle", &SoftChannel::m_beamEquTwoWayAngle)
    .def_readonly("m_beamGain", &SoftChannel::m_beamGain)
    .def_readonly("m_beamSACorrection", &SoftChannel::m_beamSACorrection)
    .def_readonly("m_bottomDetectionMinDepth", &SoftChannel::m_bottomDetectionMinDepth)
    .def_readonly("m_bottomDetectionMaxDepth", &SoftChannel::m_bottomDetectionMaxDepth)
    .def_readonly("m_bottomDetectionMinLevel", &SoftChannel::m_bottomDetectionMinLevel)
    .def_readonly("m_AlongTXRXWeightId", &SoftChannel::m_AlongTXRXWeightId)
    .def_readonly("m_AthwartTXRXWeightId", &SoftChannel::m_AthwartTXRXWeightId)
    .def_readonly("m_SplitBeamAlongTXRXWeightId", &SoftChannel::m_SplitBeamAlongTXRXWeightId)
    .def_readonly("m_SplitBeamAthwartTXRXWeightId", &SoftChannel::m_SplitBeamAthwartTXRXWeightId)
    .def_readonly("m_bandWidth", &SoftChannel::m_bandWidth)
    .def("GetSplitBeamParameter", &SoftChannel::GetSplitBeamParameter, return_value_policy<reference_existing_object>())
    .def_readonly("m_softChannelComputeData", &SoftChannel::m_softChannelComputeData);

  class_<SplitBeamParameter, boost::noncopyable>("SplitBeamParameter", no_init)
    .def_readonly("m_startLogTime", &SplitBeamParameter::m_startLogTime)
    .def_readonly("m_splitBeamParameterChannelId", &SplitBeamParameter::m_splitBeamParameterChannelId)
    .def_readonly("m_minimumValue", &SplitBeamParameter::m_minimumValue)
    .def_readonly("m_minimumEchoLength", &SplitBeamParameter::m_minimumEchoLength)
    .def_readonly("m_maximumEchoLenght", &SplitBeamParameter::m_maximumEchoLenght)
    .def_readonly("m_maximumGainCompensation", &SplitBeamParameter::m_maximumGainCompensation)
    .def_readonly("m_maximumPhaseCompensation", &SplitBeamParameter::m_maximumPhaseCompensation);

  class_<HacTime>("HacTime")
    .def_readwrite("m_TimeFraction", &HacTime::m_TimeFraction)
    .def_readwrite("m_TimeCpu", &HacTime::m_TimeCpu);

  class_<SoftChannelComputeData>("SoftChannelComputeData")
	  .def_readonly("m_PolarId", &SoftChannelComputeData::m_PolarId)
	  .def_readonly("m_isReferenceBeam", &SoftChannelComputeData::m_isReferenceBeam)
	  .def_readonly("m_groupId", &SoftChannelComputeData::m_groupId)
	  .def_readonly("m_isMultiBeam", &SoftChannelComputeData::m_isMultiBeam)
	  .def("GetTransmitSignalObject", &SoftChannelComputeData::GetTransmitSignalObject, return_value_policy<reference_existing_object>());

  class_<TransmitSignalObject, boost::noncopyable>("TransmitSignalObject", no_init)
	  .def_readonly("m_effectivePulseLength", &TransmitSignalObject::GetEffectivePulseLength);

  def("moGetEsuManager", pyGetEsuManager, return_value_policy<reference_existing_object>());

  class_<MovESUMgr, boost::noncopyable>("MovESUMgr", no_init)
	  .def("GetEsuManagerParameter", &MovESUMgr::GetRefParameter, return_value_policy<reference_existing_object>())
	  .def("GetEsuContainer", &MovESUMgr::GetESUContainer, return_value_policy<reference_existing_object>());;

  class_<ESUContainer, boost::noncopyable>("ESUContainer", no_init)
	  .def("GetESUNb", &ESUContainer::GetESUNb)
	  .def("GetESUWithIdx", &ESUContainer::GetESUWithIdx, return_value_policy<reference_existing_object>())
	  .def("GetESUWithPing", &ESUContainer::GetESUWithPing, return_value_policy<reference_existing_object>())
	  .def("GetMostRecentESU", &ESUContainer::GetMostRecentESU, return_value_policy<reference_existing_object>())
	  .def("GetMostRecentClosedESU", &ESUContainer::GetMostRecentClosedESU, return_value_policy<reference_existing_object>());

  enum_ <ESUParameter::TESUCutType> ("ESUCutType")
	  .value("EsuCutByDistance", ESUParameter::ESU_CUT_BY_DISTANCE)
	  .value("EsuCutByTime", ESUParameter::ESU_CUT_BY_TIME)
	  .value("EsuCutByPingNb", ESUParameter::ESU_CUT_BY_PING_NB);

  class_<TimeElapse>("TimeElapse")
	  .def_readonly("m_timeElapse", &TimeElapse::m_timeElapse);

  class_<ESUParameter, boost::noncopyable>("ESUParameter", no_init)
	  .add_property("m_esuCutType", &ESUParameter::GetESUCutType)
	  .add_property("m_esuId", &ESUParameter::GetESUId)
	  .add_property("m_distance", &ESUParameter::GetESUDistance)
	  .add_property("m_pingNumber", make_function(&ESUParameter::GetESUPingNumber, return_value_policy<copy_const_reference>()))
	  .add_property("m_lastPingNumber", make_function(&ESUParameter::GetESULastPingNumber, return_value_policy<copy_const_reference>()))
	  .add_property("m_time", &ESUParameter::GetESUTime)
	  .add_property("m_endTime", &ESUParameter::GetESUEndTime)
	  .add_property("m_maxTemporalGap", &ESUParameter::getMaxTemporalGap);
}
