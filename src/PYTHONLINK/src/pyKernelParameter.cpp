
#include "PythonLink/pyKernelParameter.h"

#include "M3DKernel/M3DKernel.h"

#define HAVE_ROUND
#include <boost/python.hpp>

KernelParameter pyMovies::pyLoadKernelParameter()
{
	return M3DKernel::GetInstance()->GetRefKernelParameter();
}

void pyMovies::pySaveKernelParameter(KernelParameter & kernelParams)
{
	M3DKernel::GetInstance()->UpdateKernelParameter(kernelParams);
}

void pyMovies::wrapKernelParameter()
{
  using namespace boost::python;

  class_<KernelParameter>("KernelParameter")
    .add_property("m_nbPingFanMax", &KernelParameter::getNbPingFanMax, &KernelParameter::setNbPingFanMax)
    .add_property("m_bAutoLengthEnable", &KernelParameter::getAutoLengthEnable, &KernelParameter::setAutoLengthEnable)
    .add_property("m_bAutoDepthEnable", &KernelParameter::getAutoDepthEnable, &KernelParameter::setAutoDepthEnable)
    .add_property("m_bAutoDepthTolerance", &KernelParameter::getAutoDepthTolerance, &KernelParameter::setAutoDepthTolerance)
    .add_property("m_ScreenPixelSizeX", &KernelParameter::getScreenPixelSizeX, &KernelParameter::setScreenPixelSizeX)
    .add_property("m_ScreenPixelSizeY", &KernelParameter::getScreenPixelSizeY, &KernelParameter::setScreenPixelSizeY)
    .add_property("m_MaxRange", &KernelParameter::getMaxRange, &KernelParameter::setMaxRange)
    .add_property("m_bIgnorePhaseData", &KernelParameter::getIgnorePhase, &KernelParameter::setIgnorePhase)
    .add_property("m_bIgnorePingsWithNoNavigation", &KernelParameter::getIgnorePingsWithNoNavigation, &KernelParameter::setIgnorePingsWithNoNavigation)
    .add_property("m_bIgnorePingsWithNoPosition", &KernelParameter::getIgnorePingsWithNoPosition, &KernelParameter::setIgnorePingsWithNoPosition)
	.add_property("m_bIgnoreIncompletePings", &KernelParameter::getIgnoreIncompletePings, &KernelParameter::setIgnoreIncompletePings)
	.add_property("m_bKeepQuadrantsInMemory", &KernelParameter::getKeepQuadrantsInMemory, &KernelParameter::setKeepQuadrantsInMemory);
  
  def("moLoadKernelParameter", pyLoadKernelParameter);
  def("moSaveKernelParameter", pySaveKernelParameter);
}
