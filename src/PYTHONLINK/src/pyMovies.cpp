#include "PythonLink/pyKernel.h"
#include "PythonLink/pyReader.h"
#include "PythonLink/pyWriter.h"
#include "PythonLink/pyKernelParameter.h"
#include "PythonLink/pyReaderParameter.h"
#include "PythonLink/pyEchoIntegration.h"
#include "PythonLink/pyNoiseLevel.h"
#include "PythonLink/pyCalibration.h"
#include "PythonLink/pyShoalExtraction.h"

#define HAVE_ROUND
#include <boost/python.hpp>
#include <boost/python/exception_translator.hpp>

using namespace pyMovies;

void translateError(const std::exception& e)
{
    PyErr_SetString(PyExc_Exception, e.what());
}

BOOST_PYTHON_MODULE(pyMovies)
{
    boost::python::register_exception_translator<std::exception>(translateError);

    wrapKernelParameter();
    wrapReaderParameter();
    wrapKernel();
    wrapReader();
    wrapWriter();
    wrapEchoIntegration();
    wrapNoiseLevel();
    wrapCalibration();
    wrapShoalExtraction();
}
