
#include "PythonLink/pyNoiseLevel.h"

#define HAVE_ROUND
#include <boost/python.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/ESUParameter.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "ModuleManager/ModuleManager.h"
#include "NoiseLevel/NoiseLevelModule.h"

namespace boost
{
	/*
	* ACN : Fix boost::get_pointer dans Visual Studio 2015 Update 3
	* https://stackoverflow.com/questions/38261530/unresolved-external-symbols-since-visual-studio-2015-update-3-boost-python-link
	* https://connect.microsoft.com/VisualStudio/Feedback/Details/2852624
	*/
	template <>
	inline NoiseLevelModule const volatile * get_pointer(class NoiseLevelModule const volatile *em) {
		return em;
	}


	template <>
	inline NoiseLevelParameters const volatile * get_pointer(class NoiseLevelParameters const volatile *em) {
		return em;
	}
}

using namespace pyMovies;

NoiseLevelModule * pyNoiseLevel()
{
  NoiseLevelModule * pResult = CModuleManager::getInstance()->GetNoiseLevelModule();
  // Instanciation du consumer matlab (ou python en l'occurence...)
  //pResult->GetMatlabModuleConsumer();
  return pResult;
}
/*
class NoiseLevelEsuResult
{
public:
	std::map<int, int> nbPassivePings;
	std::map<int, double> noiseLevels;
	std::map<int, double> refNoiseLevels;
	std::map<int, double> ranges;
	std::map<int, double> refRanges;
};

std::map<int, NoiseLevelEsuResult> wrap_EsuResult(NoiseLevelModule * pNoiseLevelModule, ESUParameter* esu)
{
	auto startTime = esu->GetESUTime();
	auto endTime = esu->GetESUEndTime();
	
	const auto & noiseLevelParameters = pNoiseLevelModule->GetNoiseLevelParameters();
	const auto & refValues = noiseLevelParameters.getReferenceValues();

	std::map<int, NoiseLevelEsuResult> results;

	auto pKernel = M3DKernel::GetInstance();

	const CurrentSounderDefinition & soundersDef = pKernel->getObjectMgr()->GetSounderDefinition();
	for (unsigned int iSounder = 0; iSounder < soundersDef.GetNbSounder(); iSounder++)
	{
		auto sounder = soundersDef.GetSounder(iSounder);
		if (!sounder)
			break;


		NoiseLevelEsuResult sounderResult;

		TimeObjectContainer *pingFanContainer = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(sounder->m_SounderId);

		std::map<int, int> nbPings;
		std::map<int, int> nbPingsFM;
		std::map<int, double> averageNoise;
		std::map<int, double> averageRange;
		std::map<int, double> averageRefRange;

		for (auto transducerIndex = 0; transducerIndex < sounder->GetTransducerCount(); ++transducerIndex)
		{
			auto transducer = sounder->GetTransducer(transducerIndex);
			for (const auto &channelId : transducer->GetChannelId())
			{
				auto channel = transducer->getSoftChannel(channelId);
				bool hasData = false;

				// find sounder ref values
				NoiseLevelParameters::ReferenceValues sounderRefValues;
				auto itSounderRefValues = refValues.find(sounder->m_SounderId);
				if (itSounderRefValues != refValues.cend())
				{
					sounderRefValues = itSounderRefValues->second;
				}

				// find ref value;
				auto itFreq = sounderRefValues.find(channel->m_acousticFrequency);
				if (itFreq != sounderRefValues.cend())
				{
					//
					auto lambda = sounder->m_soundVelocity / channel->m_acousticFrequency;
					auto eta = 0.55;
					auto T = ((double)transducer->m_pulseDuration) / 1000000.0;

					const double fixedNoisePart = -10.0 * log10((lambda * lambda) / (4.0 * PI)) - 10.0 * log10(1 / T) - 10.0 * log10(eta) + 181.8;
					sounderResult.refNoiseLevels[channel->m_acousticFrequency] = itFreq->second + fixedNoisePart;
				}

				for (int pingFanIndex = 0; pingFanIndex < pingFanContainer->GetObjectCount(); ++pingFanIndex)
				{
					auto pingFan = (PingFan*)pingFanContainer->GetDatedObject(pingFanIndex);
					auto beam = pingFan->getBeam(channelId)->second;
					auto softChan = sounder->getSoftChannel(channelId);

					if (pingFan->m_ObjectTime >= startTime
						&& pingFan->m_ObjectTime <= endTime
						&& beam->m_transMode == BeamDataObject::PassiveTransducerMode)
					{
						auto nl = beam->getAverageNoiseLevel();
						for (auto it = nl.cbegin(); it != nl.cend(); ++it)
						{
							averageNoise[it->first] += it->second;
							nbPingsFM[it->first] += 1;
						}

						nbPings[softChan->m_acousticFrequency] += 1;
						averageRange[softChan->m_acousticFrequency] += beam->getNoiseRangeEchoNum();
						averageRefRange[softChan->m_acousticFrequency] += beam->getRefNoiseRangeEchoNum();
					}
				}
			}
		}

		sounderResult.nbPassivePings = nbPings;

		for (auto it = nbPings.cbegin(); it != nbPings.cend(); ++it)
		{
			averageRange[it->first] /= it->second;
			averageRefRange[it->first] /= it->second;
		}
		
		sounderResult.ranges = averageRange;
		sounderResult.refRanges = averageRefRange;
		
		for (auto it = averageNoise.cbegin(); it != averageNoise.cend(); ++it)
		{
			const auto nl = it->second;
			const bool isValid = !isinf(nl) && !isnan(nl);
			if (isValid)
			{
				sounderResult.noiseLevels[it->first] = (nl / nbPingsFM[it->first]);
			}
		}

		results[sounder->m_SounderId] = sounderResult;
	}

    return results;
}
*/

void pyMovies::wrapNoiseLevel()
{
  using namespace boost::python;

  def("moNoiseLevel", pyNoiseLevel, return_value_policy<reference_existing_object>());

  class_<NoiseLevelModule, boost::noncopyable>("NoiseLevelModule", no_init)
	  .def("setEnable", &NoiseLevelModule::setEnable)
	  .def("getEnable", &NoiseLevelModule::getEnable)
	  .def("GetNoiseLevelParameters", &NoiseLevelModule::GetNoiseLevelParameters, return_value_policy<reference_existing_object>())
	  .def("GetResults", &NoiseLevelModule::getResults)
	  .def("GetResultForSounder", &NoiseLevelModule::getResultForSounder);

  class_<NoiseLevelResult>("NoiseLevelResult", no_init)
	.add_property("nbPassivePings", &NoiseLevelResult::nbPassivePings)
	.add_property("noiseLevels", &NoiseLevelResult::noiseLevels)
	.add_property("refNoiseLevels", &NoiseLevelResult::refNoiseLevels)
	.add_property("ranges", &NoiseLevelResult::ranges)
	.add_property("refRanges", &NoiseLevelResult::refRanges);

  class_ < std::map<int, int> >("std::map<int, int>")
	  .def(map_indexing_suite< std::map<int, int>, true >());

  class_ < std::map<int, double> >("std::map<int, double>")
	  .def(map_indexing_suite< std::map<int, double>, true >());

  class_ < std::map<int, NoiseLevelResult > > ("std::map<int, NoiseLevelResult>")
	  .def(map_indexing_suite< std::map<int, NoiseLevelResult>, true >());

  class_<NoiseLevelParameters, boost::noncopyable>("NoiseLevelParameters", no_init)
    .add_property("minRange", &NoiseLevelParameters::getMinRange)
	.add_property("maxRange", &NoiseLevelParameters::getMaxRange)
	.add_property("svThreshold", &NoiseLevelParameters::getSvThreshold)
    .add_property("referenceValuesFilePath", make_function(&NoiseLevelParameters::getReferenceValuesFilePath, return_value_policy<reference_existing_object>()))
	.add_property("referenceValues", make_function(&NoiseLevelParameters::getReferenceValues, return_value_policy<reference_existing_object>()))
	  ;
}
