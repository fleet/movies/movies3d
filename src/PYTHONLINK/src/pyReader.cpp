
#include "PythonLink/pyReader.h"

#define HAVE_ROUND
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

// Rmq. bizarre, si on inclu ca avant les ent�tes python, ca ne compile plus ...
#include "Reader/ReaderCtrl.h"
#include "Reader/MovReadService.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/SingleTargetDataObject.h"

#include "ModuleManager/ModuleManager.h"

#include "Calibration/CalibrationModule.h"

namespace boost
{
	/*
	* ACN : Fix boost::get_pointer dans Visual Studio 2015 Update 3
	* https://stackoverflow.com/questions/38261530/unresolved-external-symbols-since-visual-studio-2015-update-3-boost-python-link
	* https://connect.microsoft.com/VisualStudio/Feedback/Details/2852624
	*/
	template <>
	inline BeamDataObject const volatile * get_pointer(class BeamDataObject const volatile *em) {
		return em;
	}

	template <>
	inline MovReadService const volatile * get_pointer(class MovReadService const volatile *em) {
		return em;
	}

	template <>
	inline PingFan const volatile * get_pointer(class PingFan const volatile *em) {
		return em;
	}

	template <>
	inline MovFileRun const volatile * get_pointer(class MovFileRun const volatile *em) {
		return em;
	}

	template <>
	inline SingleTargetDataObject const volatile * get_pointer(class SingleTargetDataObject const volatile *em) {
		return em;
	}

	template <>
	inline NavPosition const volatile * get_pointer(class NavPosition const volatile *em) {
		return em;
	}

	template <>
	inline NavAttitude const volatile * get_pointer(class NavAttitude const volatile *em) {
		return em;
	}

	template <>
	inline NavAttributes const volatile * get_pointer(class NavAttributes const volatile *em) {
		return em;
	}
}

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "PythonLink.pyReader";
}

void pyMovies::pyOpenHac(const char * hacFile)
{
  ReaderCtrl *pRead=ReaderCtrl::getInstance();

  MovFileRun mFileRun;
  mFileRun.SetSourceName(hacFile, ".hac");

  pRead->OpenFileStream(mFileRun);
  pRead->ReadSounderConfig();
}

void pyMovies::pyOpenSonarNetCDF(const char * hacFile)
{
	ReaderCtrl *pRead = ReaderCtrl::getInstance();

	MovFileRun mFileRun;
	mFileRun.SetSourceName(hacFile, ".nc");

	pRead->OpenFileStream(mFileRun);
	pRead->ReadSounderConfig();
}

void pyMovies::pyLoadConfig(const char * configPath)
{
  std::string path, prefix;
  std::string workingStr = configPath;
  // on doit d�couper le chemin complet du dossier de configuration
  // pour avoir le chemin d'une part et le nom de la configuration d'autre part.
  size_t pos = workingStr.find_last_of("/");
  if(pos == std::string::npos)
  {
    pos = workingStr.find_last_of("\\");
  }
  path = workingStr.substr(0, pos);
  prefix = workingStr.substr(pos+1);

  // changement du r�pertoire de configuration
  CModuleManager::getInstance()->ChangeConfigurationCurrentDirectory(path, prefix);

  // chargement de la configuration (NULL car pas de parametres d'affichage a loader)
  CModuleManager::getInstance()->LoadConfiguration({});
}

MovReadService * pyMovies::pyGetFileStatus()
{
  return ReaderCtrl::getInstance()->GetActiveService();
}

void pyMovies::pyReadChunk()
{
  ReaderCtrl *pRead=ReaderCtrl::getInstance();

  pRead->SyncReadChunk();
  ChunckEventList ref;
  pRead->WaitEndChunck(ref);
}

void pyMovies::pyGoTo(double targetTime)
{
  ReaderCtrl *pRead=ReaderCtrl::getInstance();
  GoToTarget target;
  // on ne propose pas le goto par ping (a cause des ambiguit�s si plusieurs sondeurs)
  target.byPing = false;
  target.targetTime.m_TimeCpu = (std::uint32_t)targetTime;
  target.targetTime.m_TimeFraction = (unsigned short)((targetTime - (double)(target.targetTime.m_TimeCpu))*10000.0);
  pRead->GoTo(target);
}

unsigned int pyMovies::pyGetNumberOfPingFan()
{
  return (unsigned int)M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetNbFan();
}

PingFan * pyMovies::pyGetPingFan(size_t FanNum)
{
  PingFan * pResult = NULL;
  PingFanContainer & container = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer();
  if(FanNum < container.GetNbFan())
  {
    pResult=(PingFan*)container.GetPingFanContainer()->GetObjectWithIndex(FanNum);
  }   
  else
  {
    M3D_LOG_ERROR(LoggerName, "PingFan index is greater than PingFan Num");
  }
  return pResult;
}

void pyMovies::pyRemovePing(unsigned int pingId)
{
  PingFanContainer & pingFanContainer = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer();
  if(!pingFanContainer.RemovePing(pingId)) {
    M3D_LOG_WARN(LoggerName, Log::format("Unable to delete ping from id [%d]", pingId));
  }
}

class BeamDataObjectEx
{
protected:
	BeamDataObject * m_obj;
    std::uint64_t m_pingId;
	unsigned short m_channelId;

public:
	BeamDataObjectEx()
		: m_obj(nullptr)
	{
	}

    BeamDataObjectEx(BeamDataObject * obj, std::uint64_t pingId, unsigned short channelId)
		: m_obj(obj)
		, m_pingId(pingId)
		, m_channelId(channelId)
	{
	}

	BeamDataObjectEx(const BeamDataObjectEx & other) = default;

	bool operator==(const BeamDataObjectEx & other) const { return m_obj == other.m_obj; }

	unsigned short hacChannelId() const { return m_obj->m_hacChannelId; }
	
	HacTime time() const { return HacTime(m_obj->m_timeCPU, m_obj->m_timeFraction); }

	double bottomRange() const { return m_obj->m_bottomRange*0.001;	}
	void setBottomRange(double val) { m_obj->m_bottomRange = val *1000.0; }

	bool bottomWasFound() const { return m_obj->m_bottomWasFound; }
	void setBottomWasFound(bool found) const { m_obj->m_bottomWasFound = found;  }

	double compensateHeave() const	{ return m_obj->m_compensateHeave; }
	void setCompensateHeave(double val) { m_obj->m_compensateHeave = val; }

	double compensateHeaveMovies() const { return m_obj->m_compensateHeaveMovies; }
	void setCompensateHeaveMovies(double val) { m_obj->m_compensateHeaveMovies = val; }
	
	NavAttributes * navigationAttribute() const
	{
		NavAttributes * attr = nullptr;

		M3DKernel *pKernel = M3DKernel::GetInstance();
		HacObjectMgr * objectMgr = pKernel->getObjectMgr();
		PingFan * pFan = (PingFan*)objectMgr->GetPingFanContainer().GetPingFanWithId(m_pingId);
		if (pFan != nullptr)
		{
			attr = pFan->GetNavAttributesRef(m_channelId);
		}

		return attr;
	}

	NavPosition * navigationPosition() const
	{
		NavPosition * pos = NULL;

		M3DKernel *pKernel = M3DKernel::GetInstance();
		HacObjectMgr * objectMgr = pKernel->getObjectMgr();
		PingFan * pFan = (PingFan*)objectMgr->GetPingFanContainer().GetPingFanWithId(m_pingId);
		if (pFan != nullptr)
		{
			pos = pFan->GetNavPositionRef(m_channelId);
		}

		return pos;
	}

	NavAttitude * navigationAttitude() const
	{
		NavAttitude * attitude = NULL;

		M3DKernel *pKernel = M3DKernel::GetInstance();
		HacObjectMgr * objectMgr = pKernel->getObjectMgr();
		PingFan * pFan = (PingFan*)objectMgr->GetPingFanContainer().GetPingFanWithId(m_pingId);
		if (pFan != nullptr)
		{
			attitude = pFan->GetNavAttitudeRef(m_channelId);
		}

		return attitude;
	}
};


std::vector<BeamDataObjectEx> wrap_beamData(PingFan & pingFan) 
{
	unsigned int sizeBeam = pingFan.getSounderRef()->m_sounderComputeData.m_nbBeamPerFan;
	std::vector<BeamDataObjectEx> result(sizeBeam);
	size_t iBeam = 0;
	for (int i = 0; i<pingFan.getSounderRef()->m_numberOfTransducer; i++)
	{
		Transducer *pTrans = pingFan.getSounderRef()->GetTransducer(i);
		for (unsigned int chan = 0; chan<pTrans->m_numberOfSoftChannel; chan++)
		{
			SoftChannel *pChan = pTrans->getSoftChannelPolarX(chan);
			BeamDataObject *beamData = pingFan.getRefBeamDataObject(pChan->getSoftwareChannelId(), pChan->getSoftwareVirtualChannelId());
			result[iBeam++] = BeamDataObjectEx(beamData, pingFan.m_computePingFan.m_pingId , pChan->getSoftwareChannelId());
		}
	}
	return result;
}

boost::python::list wrap_quadrantData(PingFan & pingFan)
{
	boost::python::list quadrantDataList;

	auto nbTrans = pingFan.getSounderRef()->GetTransducerCount();
	for (int transIdx = 0; transIdx < nbTrans; ++transIdx)
	{
		Transducer *pTrans = pingFan.getSounderRef()->GetTransducer(transIdx);
		unsigned short channelId = pTrans->GetChannelId().front();

		boost::python::list channelData;
		auto quadrantData = pingFan.getQuadrantDataObject(channelId);
		if (quadrantData)
		{
			for (const auto & quadrant : quadrantData->samples)
			{
				boost::python::list realSamples;
				boost::python::list imagSamples;				

				for (const auto & c : quadrant)
				{
					realSamples.append(c.real());
					imagSamples.append(c.imag());
				}

				boost::python::list samples;
				samples.append(realSamples);
				samples.append(imagSamples);
				channelData.append(samples);
			}
		}
		quadrantDataList.append(channelData);
	}
	return quadrantDataList;
}


class PolarMatrix {
public:
  // rmq. : ona  essayer d'utiliser des vector de vector ici, mapp� avec python grace � vector_indexing_suite.
  // Les performances se sont av�r�es particuli�rement mauvaises. On passe donc par des listes python
  // directement ce qui est beaucoup plus rapide. L'inconv�nient est de ne pas r�percuter les modifs faites sur
  // des donn�es dans python c�t� C++, qui n'est pas grave (m�me comportement que l'interface matlab).
  // Une piste �ventuelle pour am�liorer encore les performances et r�tablir le mapping bidirectionnel
  // serait de cr�er un tableau numpy directement (inconv�nient : plus compliqu�, et obligerait � d�pendre
  // de numpy...
  boost::python::list m_Amplitude;
  boost::python::list m_Overlap;
  boost::python::list m_AthwartAngle;
  boost::python::list m_AlongAngle;
};

boost::python::object wrap_polarMatrix(PingFan * pingFan, unsigned int transIdx)
{
	PolarMatrix mat;

	if (pingFan)
	{
		auto pMemStruct = pingFan->GetMemorySetRef()->GetMemoryStruct(transIdx);
		if (pMemStruct)
		{
			auto pData = pMemStruct->GetDataFmt();
			auto pFilter = pMemStruct->GetFilterFlag();
			auto pPhase = pMemStruct->GetPhase();
			auto pOverlap = pMemStruct->GetOverLap();

			const Vector2I memSize = pData->getSize();

			for (int j = 0; j < memSize.y; ++j)
			{
				boost::python::list amplitudeY;
				boost::python::list overlapY;
				boost::python::list athwartY;
				boost::python::list alongY;
				boost::python::list quadrantY;

				for (int i = 0; i < memSize.x; ++i)
				{
					short value = pData->GetValueToVoxel(i, j);
					char filter = pFilter->GetValueToVoxel(i, j);
					if (filter > 0)
						value = UNKNOWN_DB;
					amplitudeY.append(value);

					if (pPhase)
					{
						Phase valueAngle = pPhase->GetValueToVoxel(i, j);
						athwartY.append(valueAngle.GetRawAthwartValue());
						alongY.append(valueAngle.GetRawAlongValue());
					}

					if (pOverlap)
					{
						short valueO = pOverlap->GetValueToVoxel(i, j);
						overlapY.append(valueO);
					}
				}

				mat.m_Amplitude.append(amplitudeY);
				mat.m_Overlap.append(overlapY);
				mat.m_AthwartAngle.append(athwartY);
				mat.m_AlongAngle.append(alongY);
			}
		}
	}

	return boost::python::object(mat);
}

void wrap_setPolarMatrix(PingFan * pingFan, unsigned int transIdx, PolarMatrix mat)
{
	if (!pingFan)
		return;

	MemoryStruct *pMemStruct = (MemoryStruct *)pingFan->GetMemorySetRef()->GetMemoryStruct(transIdx);
	Vector2I memSize = pMemStruct->GetDataFmt()->getSize();
	const int Xdim = memSize.x;
	const int Ydim = memSize.y;
	
	MemoryObjectPhase * pPhase = pMemStruct->GetPhase();
	MemoryOverlap * pOverlap = pMemStruct->GetOverLap();

	for (int j = 0; j < Ydim; j++)
	{
		boost::python::list amplitudeY = (boost::python::list)mat.m_Amplitude[j];

		for (int i = 0; i < Xdim; i++)
		{
			Vector2I pos(i, j);

			MemoryObjectDataFmt *pAmp = pMemStruct->GetDataFmt();
			if (pAmp)
			{
				short *pValue = ((short*)pAmp->GetPointerToVoxel(pos));
				*pValue = boost::python::extract<short>(amplitudeY[i]);
			}
		}
	}

	if (mat.m_AthwartAngle.is_none() || mat.m_AlongAngle.is_none())
		return;

	for (int j = 0; j < Ydim; j++)
	{
		boost::python::list athwartY = (boost::python::list)mat.m_AthwartAngle[j];
		boost::python::list alongY = (boost::python::list)mat.m_AlongAngle[j];

		for (int i = 0; i < Xdim; i++)
		{
			Vector2I pos(i, j);

			MemoryObjectPhase *pMemPhase = pMemStruct->GetPhase();
			if (pMemPhase)
			{
				Phase *pValue = pMemPhase->GetPointerToVoxel(pos);
				pValue->SetAthwart(boost::python::extract<double>(athwartY[i]));
				pValue->SetAlong(boost::python::extract<double>(alongY[i]));
			}
		}
	}
}


class PolarMatrixFM {
public:
	// rmq. : ona  essayer d'utiliser des vector de vector ici, mapp� avec python grace � vector_indexing_suite.
	// Les performances se sont av�r�es particuli�rement mauvaises. On passe donc par des listes python
	// directement ce qui est beaucoup plus rapide. L'inconv�nient est de ne pas r�percuter les modifs faites sur
	// des donn�es dans python c�t� C++, qui n'est pas grave (m�me comportement que l'interface matlab).
	// Une piste �ventuelle pour am�liorer encore les performances et r�tablir le mapping bidirectionnel
	// serait de cr�er un tableau numpy directement (inconv�nient : plus compliqu�, et obligerait � d�pendre
	// de numpy...
	boost::python::list m_Amplitude;
	boost::python::list m_Ranges;
	boost::python::list m_Frequencies;
};

boost::python::object wrap_polarMatrixFM(PingFan * pingFan, unsigned int transIdx)
{
	PolarMatrixFM mat;
	if (pingFan)
	{
		Transducer *pTrans = pingFan->getSounderRef()->GetTransducer(transIdx);		
		unsigned short channelId = pTrans->GetChannelId().front();
		auto spectralAnalysisDataObject = pingFan->getSpectralAnalysisDataObject(channelId);
	
		if (spectralAnalysisDataObject)
		{
			CalibrationModule* calibrationModule = CModuleManager::getInstance()->GetCalibrationModule();

			const int Ydim = spectralAnalysisDataObject->numberOfRanges();
			const int Fdim = spectralAnalysisDataObject->numberOfFrequencies();

			for (int k = 0; k < Fdim; ++k)
			{
				mat.m_Frequencies.append(spectralAnalysisDataObject->frequencyForIndex(k));
			}

			const std::vector<std::vector<double>> svValues = calibrationModule->getSvValuesWithGain(spectralAnalysisDataObject, pTrans->getSoftChannel(channelId));

			for (int j = 0; j < Ydim; j++)
			{
				mat.m_Ranges.append(spectralAnalysisDataObject->rangeForIndex(j));
			}

			for (int k = 0; k < Fdim; ++k)
			{
				boost::python::list amplitudeY;
				for (int j = 0; j < Ydim; j++)
				{
					amplitudeY.append(svValues[j][k]);
				}
				mat.m_Amplitude.append(amplitudeY);
			}
		}
	}

	return boost::python::object(mat);
}
/// Classe permettant de 'd�corer' les cibles simples pour ajouter les informations (pingNumber, sounderId et channelId)
/// n�cessaire aux m�thodes GetPositionWorldCoord et GetPositionGeoCoord
template<typename T>
class ExSingleTargetData : public T 
{
    std::uint64_t m_pingId;
	std::uint32_t  m_sounderId;
	unsigned short m_channelId;

public:

	ExSingleTargetData()
	{
	}
	
    ExSingleTargetData(const T & base, std::uint64_t pingId, std::uint32_t sounderId, std::uint32_t channelId)
		: T(base)
		, m_pingId(pingId)
		, m_sounderId(sounderId)
		, m_channelId(channelId)
	{
	}
	
	ExSingleTargetData(const ExSingleTargetData<T> & other) = default;

	BaseMathLib::Vector3D GetPositionWorldCoord() const
	{
		M3DKernel *pKernel = M3DKernel::GetInstance();
		HacObjectMgr * objectMgr = pKernel->getObjectMgr();
		Sounder * pSounder = objectMgr->GetSounderDefinition().GetSounderWithId(m_sounderId);
		if (pSounder == nullptr)
			return BaseMathLib::Vector3D();

		Transducer *pTransducer = pSounder->getTransducerForChannel(m_channelId);
		if (pTransducer == nullptr)
			return BaseMathLib::Vector3D();

		SoftChannel *pChannel = pTransducer->getSoftChannel(m_channelId);
		if (pChannel == nullptr)
			return BaseMathLib::Vector3D();

		PingFan * pFan = (PingFan*)objectMgr->GetPingFanContainer().GetPingFanWithId(m_pingId);
		if (pFan == nullptr)
			return BaseMathLib::Vector3D();

        BaseMathLib::Vector3D mSoftCoord = T::GetTargetPositionSoftChannel();
		return pSounder->GetSoftChannelCoordinateToWorldCoord(pTransducer, pChannel, pFan, mSoftCoord);
	}

	BaseMathLib::Vector3D GetPositionGeoCoord() const
	{
		M3DKernel *pKernel = M3DKernel::GetInstance();
		HacObjectMgr * objectMgr = pKernel->getObjectMgr();
		Sounder * pSounder = objectMgr->GetSounderDefinition().GetSounderWithId(m_sounderId);
		if (pSounder == nullptr)
			return BaseMathLib::Vector3D();

		Transducer *pTransducer = pSounder->getTransducerForChannel(m_channelId);
		if (pTransducer == nullptr)
			return BaseMathLib::Vector3D();

		SoftChannel *pChannel = pTransducer->getSoftChannel(m_channelId);
		if (pChannel == nullptr)
			return BaseMathLib::Vector3D();

		//PingFan * pFan = (PingFan*) objectMgr->GetPingFanContainer().GetPingFanContainer()->GetObjectWithIndex(m_pingNumber);
		PingFan * pFan = (PingFan*)objectMgr->GetPingFanContainer().GetPingFanWithId(m_pingId);
		if (pFan == nullptr)
			return BaseMathLib::Vector3D();

        BaseMathLib::Vector3D mSoftCoord = T::GetTargetPositionSoftChannel();
		return pSounder->GetSoftChannelCoordinateToGeoCoord(pTransducer, pChannel, pFan, mSoftCoord);
	}
};


class SingleTargetDataObjectEx
{
	SingleTargetDataObject * m_obj;
    std::uint64_t m_pingId;

public:
	SingleTargetDataObjectEx()
		: m_obj(nullptr)
	{
	}

    SingleTargetDataObjectEx(SingleTargetDataObject * obj, std::uint64_t pingId)
		: m_obj(obj)
		, m_pingId(pingId)
	{
	}
	SingleTargetDataObjectEx(const SingleTargetDataObjectEx & other) = default;

	bool operator==(const SingleTargetDataObjectEx & other) const { return m_obj == other.m_obj; }

	unsigned short parentSTId() const { return m_obj->m_parentSTId; }

	double selectStartRange() const { return m_obj->m_selectStartRange; }

	double selectEndRange() const { return m_obj->m_selectEndRange; }

	double detectedBottom() const { return m_obj->m_detectedBottom; }

	boost::python::list targetListCW()
	{
		boost::python::list result;

		M3DKernel *pKernel = M3DKernel::GetInstance();
		HacObjectMgr * objectMgr = pKernel->getObjectMgr();
		SplitBeamPair ref;
		bool found = objectMgr->GetChannelParentSplitBeam(m_obj->m_parentSTId, ref);
		if (found)
		{
			const int numberOfTarget = m_obj->GetSingleTargetDataCWCount();
			for (unsigned int i = 0; i < numberOfTarget; ++i)
			{
				ExSingleTargetData<SingleTargetDataCW> singleTarget(
					m_obj->GetSingleTargetDataCW(i)
					, m_pingId
					, ref.m_sounderId
					, ref.m_channelId);
				result.append(singleTarget);
			}
		}

		return result;
	}

	boost::python::list targetListFM()
	{
		boost::python::list result;

		M3DKernel *pKernel = M3DKernel::GetInstance();
		HacObjectMgr * objectMgr = pKernel->getObjectMgr();
		SplitBeamPair ref;
		bool found = objectMgr->GetChannelParentSplitBeam(m_obj->m_parentSTId, ref);
		if (found)
		{
			const int numberOfTarget = m_obj->GetSingleTargetDataFMCount();
			for (unsigned int i = 0; i < numberOfTarget; ++i)
			{
				ExSingleTargetData<SingleTargetDataFM> singleTarget(
					m_obj->GetSingleTargetDataFM(i)
					, m_pingId
					, ref.m_sounderId
					, ref.m_channelId);
				result.append(singleTarget);
			}
		}

		return result;
	}
};


/// M�thodes de construction de la liste des cibles simples
std::vector<SingleTargetDataObjectEx> wrap_singleTargets(PingFan & pingFan)
{
	M3DKernel *pKernel = M3DKernel::GetInstance();
	HacObjectMgr * objectMgr = pKernel->getObjectMgr();

	PingFanSingleTarget *pSingle = objectMgr->GetPingFanContainer().GetPingFanSingleTarget(pingFan.m_ObjectTime, pingFan.getSounderRef()->m_SounderId);
	Sounder *pSounder = pingFan.getSounderRef();

	const int numberOfTarget = (pSingle) ? pSingle->GetNumberOfTarget() : 0;
	std::vector<SingleTargetDataObjectEx> result;
	result.reserve(numberOfTarget);
	for (unsigned int i = 0; i < numberOfTarget; ++i)
	{
		SingleTargetDataObject *pSingleTargetData = pSingle->GetTarget(i);
		SplitBeamPair ref;
		bool found = objectMgr->GetChannelParentSplitBeam(pSingleTargetData->m_parentSTId, ref);
		if (found && ref.m_sounderId == pSounder->m_SounderId)
		{
			result.push_back(SingleTargetDataObjectEx(pSingleTargetData, pingFan.m_computePingFan.m_pingId));
		}
	}
	result.shrink_to_fit();
	return result;
}

void pyMovies::wrapReader()
{
  using namespace boost::python;

  def("moLoadConfig", pyLoadConfig);
  def("moOpenHac", pyOpenHac);
  def("moOpenSonarNetCDF", pyOpenSonarNetCDF);
  def("moGetFileStatus", pyGetFileStatus, return_value_policy<reference_existing_object>());
  def("moReadChunk", pyReadChunk);
  def("moGoTo", pyGoTo);
  def("moGetNumberOfPingFan", pyGetNumberOfPingFan);
  def("moGetPingFan", pyGetPingFan, return_value_policy<reference_existing_object>());
  def("moRemovePing",pyRemovePing);

  class_<MovReadService, boost::noncopyable>("MovReadService", no_init)
    .def_readonly("m_StreamClosed", &MovReadService::IsClosed)
    .def_readonly("m_StreamDescName", &MovReadService::getStreamDesc)
	.def("GetFileRun", &MovReadService::GetFileRun);
  
  class_<MovFileRun>("MovFileRun")
    .def("GetFileCount", &MovFileRun::GetFileCount)
    .def("GetFileName", &MovFileRun::GetFileName)
    .def("GetCurrentFileIdx", &MovFileRun::GetCurrentFileIdx);

  class_<PingFan, bases<DatedObject>, boost::noncopyable>("PingFan", no_init)
	  .def_readonly("m_meanTime", &PingFan::m_ObjectTime)
	  .add_property("m_pSounder", make_getter(&PingFan::m_pSounder, return_value_policy<reference_existing_object>()))
	  .def_readonly("m_computePingFan", &PingFan::m_computePingFan)
	  .def_readonly("m_relativePingFan", &PingFan::m_relativePingFan)
	  .add_property("beam_data", wrap_beamData)
	  .add_property("m_splitBeamData", wrap_singleTargets)
	  .add_property("quadrant_data", wrap_quadrantData)
	  .def("GetPolarMatrix", wrap_polarMatrix)
	  .def("SetPolarMatrix", wrap_setPolarMatrix)
	  .def("GetPolarMatrixFM", wrap_polarMatrixFM);

  class_<ComputePingFan>("ComputePingFan")
    .def_readonly("m_pingId", &ComputePingFan::m_pingId)
    .def_readonly("m_pingHacId", &ComputePingFan::m_filePingId)
    .def_readonly("m_maxRange", &ComputePingFan::m_maxRange)
    .def_readonly("m_maxRangeWasFound", &ComputePingFan::m_maxRangeFound);

  class_<ComputeRelativePingFan>("ComputeRelativePingFan")
    .def_readonly("m_cumulatedNav", &ComputeRelativePingFan::m_cumulatedNav)
    .def_readonly("m_cumulatedDistance", &ComputeRelativePingFan::m_cumulatedDistance);

  class_<Vector3D>("Vector3D")
	.def_readwrite("x", &Vector3D::x)
	.def_readwrite("y", &Vector3D::y)
	.def_readwrite("z", &Vector3D::z)
	.def_readwrite("Latitude", &Vector3D::x)
	.def_readwrite("Longitude", &Vector3D::y)
	.def_readwrite("Depth", &Vector3D::z);

  class_<NavAttributes, bases<DatedObject>, boost::noncopyable>("NavAttributes", no_init)
    .def_readonly("m_navigationSystem", &NavAttributes::m_NavigationSystem)
    .def_readonly("m_headingRad", &NavAttributes::m_headingRad)
    .def_readonly("m_speedMeter", &NavAttributes::m_speedMeter);

  class_<NavPosition, bases<DatedObject>, boost::noncopyable>("NavPosition", no_init)
    .def_readonly("m_longitudeDeg", &NavPosition::m_longitudeDeg)
    .def_readonly("m_lattitudeDeg", &NavPosition::m_lattitudeDeg)
    .def_readonly("m_gpsTime", &NavPosition::m_gpsTime)
    .def_readonly("m_positionningSystem", &NavPosition::m_positionningSystem);

  class_<NavAttitude, bases<DatedObject>, boost::noncopyable>("NavAttitude", no_init)
    .def_readonly("pitchRad", &NavAttitude::m_pitchRad)
    .def_readonly("rollRad", &NavAttitude::m_rollRad)
    .def_readonly("yawRad", &NavAttitude::m_yawRad)
    .def_readonly("sensorHeaveMeter", &NavAttitude::m_heaveMeter)
	.def_readonly("sensorId", &NavAttitude::m_AttitudeSensorId);

  class_<BeamDataObjectEx, BeamDataObjectEx*>("BeamDataObjectEx")
	  .add_property("m_hacChannelId", &BeamDataObjectEx::hacChannelId)
	  .add_property("m_time", &BeamDataObjectEx::time)
	  .add_property("m_bottomRange", &BeamDataObjectEx::bottomRange, &BeamDataObjectEx::setBottomRange)
	  .add_property("m_bottomWasFound", &BeamDataObjectEx::bottomWasFound, &BeamDataObjectEx::setBottomWasFound)
	  .add_property("m_compensateHeave", &BeamDataObjectEx::compensateHeave, &BeamDataObjectEx::setCompensateHeave)
	  .add_property("m_compensateHeaveMovies", &BeamDataObjectEx::compensateHeaveMovies, &BeamDataObjectEx::setCompensateHeaveMovies)
	  .add_property("navigationAttribute", make_function(&BeamDataObjectEx::navigationAttribute, return_value_policy<reference_existing_object>()))
	  .add_property("navigationPosition", make_function(&BeamDataObjectEx::navigationPosition, return_value_policy<reference_existing_object>()))
	  .add_property("navigationAttitude", make_function(&BeamDataObjectEx::navigationAttitude, return_value_policy<reference_existing_object>()));

  class_<std::vector<BeamDataObjectEx> >("std::vector<BeamDataObjectEx>").def(vector_indexing_suite<std::vector<BeamDataObjectEx> >());

  class_<SingleTargetDataObjectEx, SingleTargetDataObjectEx*>("SingleTargetDataObjectEx", no_init)
	  .add_property("m_parentSTId", &SingleTargetDataObjectEx::parentSTId)
	  .add_property("m_selectStartRange", &SingleTargetDataObjectEx::selectStartRange)
	  .add_property("m_selectEndRange", &SingleTargetDataObjectEx::selectEndRange)
	  .add_property("m_detectedBottom", &SingleTargetDataObjectEx::detectedBottom)
	  .add_property("m_targetListCW", &SingleTargetDataObjectEx::targetListCW)
	  .add_property("m_targetListFM", &SingleTargetDataObjectEx::targetListFM);

  class_<std::vector<SingleTargetDataObjectEx> >("std::vector<SingleTargetDataObjectEx>").def(vector_indexing_suite<std::vector<SingleTargetDataObjectEx> >());

  class_< SingleTargetData >("SingleTargetData")
	  .def_readonly("m_targetRange", &SingleTargetData::m_targetRange)
	  .def_readonly("m_trackLabel", &SingleTargetData::m_trackLabel)
	  .def_readonly("m_AlongShipAngleRad", &SingleTargetData::m_AlongShipAngleRad)
	  .def_readonly("m_AthwartShipAngleRad", &SingleTargetData::m_AthwartShipAngleRad)
	  .add_property("m_positionBeamCoord", &SingleTargetData::GetTargetPositionSoftChannel);

  class_< SingleTargetDataCW, bases<SingleTargetData> >("SingleTargetDataCW")
	  .def_readonly("m_compensatedTS", &SingleTargetDataCW::m_compensatedTS)
	  .def_readonly("m_unCompensatedTS", &SingleTargetDataCW::m_unCompensatedTS);

  class_< ExSingleTargetData<SingleTargetDataCW>, bases<SingleTargetDataCW> >("ExSingleTargetData<SingleTargetDataCW>")
	  .add_property("m_positionWorldCoord", &ExSingleTargetData<SingleTargetDataCW>::GetPositionWorldCoord)
	  .add_property("m_positionGeoCoord", &ExSingleTargetData<SingleTargetDataCW>::GetPositionGeoCoord);
  
  class_< SingleTargetDataFM, bases<SingleTargetData> >("SingleTargetDataFM")
	  .def_readonly("m_freq", &SingleTargetDataFM::m_freq)
	  .def_readonly("m_compensatedTS", &SingleTargetDataFM::m_compensatedTS)
	  .def_readonly("m_unCompensatedTS", &SingleTargetDataFM::m_unCompensatedTS)
	  .def_readonly("m_medianTS", &SingleTargetDataFM::m_medianTS);

  class_< ExSingleTargetData<SingleTargetDataFM>, bases<SingleTargetDataFM> >("ExSingleTargetData<SingleTargetDataFM>")
	  .add_property("m_positionWorldCoord", &ExSingleTargetData<SingleTargetDataFM>::GetPositionWorldCoord)
	  .add_property("m_positionGeoCoord", &ExSingleTargetData<SingleTargetDataFM>::GetPositionGeoCoord);
    
  class_<PolarMatrix>("PolarMatrix")
    .def_readonly("m_Amplitude", &PolarMatrix::m_Amplitude)
    .def_readonly("m_AthwartAngle", &PolarMatrix::m_AthwartAngle)
    .def_readonly("m_AlongAngle", &PolarMatrix::m_AlongAngle)
    .def_readonly("m_Overlap", &PolarMatrix::m_Overlap);

  class_<PolarMatrixFM>("PolarMatrixFM")
	  .def_readonly("m_Amplitude", &PolarMatrixFM::m_Amplitude)
	  .def_readonly("m_Ranges", &PolarMatrixFM::m_Ranges)
	  .def_readonly("m_Frequencies", &PolarMatrixFM::m_Frequencies);
}
