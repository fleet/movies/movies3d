
#include "PythonLink/pyReaderParameter.h"

#include "Reader/ReaderCtrl.h"

#define HAVE_ROUND
#include <boost/python.hpp>

ReaderParameter pyMovies::pyLoadReaderParameter()
{
  return ReaderCtrl::getInstance()->GetReaderParameter();
}

void pyMovies::pySaveReaderParameter(ReaderParameter & readerParams)
{
  ReaderCtrl::getInstance()->UpdateReaderParameter(readerParams);
}

void pyMovies::wrapReaderParameter()
{
  using namespace boost::python;

  class_<ReaderParameter>("ReaderParameter")
    .def_readwrite("m_ChunckDef", &ReaderParameter::m_ChunckDef)
    .add_property("m_sortDataBeforeWrite", &ReaderParameter::getSortDataBeforeWrite, &ReaderParameter::getSortDataBeforeWrite);

  class_<ChunckDef>("ChunckDef")
    .def_readwrite("m_chunkUseTimeStamp", &ChunckDef::m_bIsUseTimeDef)
    .def_readwrite("m_chunkTimeStamp", &ChunckDef::m_TimeElapsed)
    .def_readwrite("m_chunkSounderId", &ChunckDef::m_SounderId)
    .def_readwrite("m_chunkNumberOfPingFan", &ChunckDef::m_NumberOfFanToRead);

  def("moLoadReaderParameter", pyMovies::pyLoadReaderParameter);
  def("moSaveReaderParameter", pyMovies::pySaveReaderParameter);
}
