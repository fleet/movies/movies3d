﻿#include "PythonLink/pyShoalExtraction.h"

#define HAVE_ROUND
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "M3DKernel/utils/mathutils.h"
#include "OutputManager/OutputManagerModule.h"
#include "ShoalExtraction/ShoalExtractionModule.h"
#include "ModuleManager/ModuleManager.h"
#include "ShoalExtraction/data/pingdata.h"
#include "ShoalExtraction/data/pingshoalstat.h"
#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/shoalstat.h"

using namespace pyMovies;

namespace boost {
	/*
	* ACN : Fix boost::get_pointer dans Visual Studio 2015 Update 3
	* https://stackoverflow.com/questions/38261530/unresolved-external-symbols-since-visual-studio-2015-update-3-boost-python-link
	* https://connect.microsoft.com/VisualStudio/Feedback/Details/2852624
	*/
	template <>
	inline ShoalExtractionModule const volatile * get_pointer(ShoalExtractionModule const volatile *em) {
		return em;
	}

	template <>
	inline ShoalExtractionOutput const volatile * get_pointer(ShoalExtractionOutput const volatile *em) {
		return em;
	}

	template <>
	inline shoalextraction::EchoInfo const volatile * get_pointer(shoalextraction::EchoInfo const volatile *em) {
		return em;
	}
}

namespace {
	constexpr const char * LOGGER_NAME = "PythonLink.pyShoalExtraction";
}

ShoalExtractionModule* pyMovies::pyShoalExtraction() {
	const auto shoalExtractionModule = CModuleManager::getInstance()->GetShoalExtractionModule();
	// Instanciation du consumer matlab (ou python en l'occurence...)
	shoalExtractionModule->GetMatlabModuleConsumer();
	return shoalExtractionModule;
}

using Contour = std::vector<Vector3D>; 

boost::python::object wrap_ShoalExtractionOutputs(ShoalExtractionModule * shoalExtractionModule) {
	// Attente de la fin de l'éxécution du module (asynchrone)
	shoalExtractionModule->WaitExtractionActions();

	// On provoque un flush du OutputManager pour créer les flux CSV/XML des derniers résultats asynchrones,
	// et pour libérer la mémoire associée
	CModuleManager::getInstance()->GetOutputManagerModule()->PingFanAdded(nullptr);

	// Récupération des résultats
	std::vector<ShoalExtractionOutput*> outputs;
	const auto& moduleOutputConsumer = shoalExtractionModule->GetMatlabModuleConsumer();
	if (moduleOutputConsumer != nullptr) {
		outputs = shoalExtractionModule->GetOutputs(moduleOutputConsumer->GetConsumerId());
		//shoalExtractionModule->GetOutputContainer().Flush(moduleOutputConsumer->GetConsumerId());
	}

	return boost::python::object(outputs);
}

void wrap_releaseOutputs(ShoalExtractionModule* shoalExtractionModule)
{
	const auto& moduleOutputConsumer = shoalExtractionModule->GetMatlabModuleConsumer();
	if (moduleOutputConsumer != nullptr)
	{
		shoalExtractionModule->GetOutputContainer().Flush(moduleOutputConsumer->GetConsumerId());
	}	
}

uint32_t wrap_getExternId(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetExternId();
}

int wrap_getSounderId(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetSounderId();
}

int wrap_getTransId(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetTransId();
}

double wrap_getMinDepth(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetMinDepth();
}

double wrap_getMaxDepth(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetMaxDepth();
}

double wrap_getMinDistanceBottom(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetMinBottomDistance();
}

double wrap_getMaxDistanceBottom(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetMaxBottomDistance();
}

double wrap_getLength(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetLength();
}

double wrap_getWidth(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetWidth();
}

double wrap_getHeight(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetHeigth();
}

double wrap_getMeanSv(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetMeanSv();
}

double wrap_getSigmaAg(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetSigmaAg();
}

double wrap_getMeanPondSv(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetMeanPondSv();
}

uint32_t wrap_getNbEchos(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetNbEchos();
}

std::vector<shoalextraction::EchoInfo> wrap_getEchos(const ShoalExtractionOutput * shoalExtractionOutput) {
	std::vector<shoalextraction::EchoInfo> echos;
	// Parcours des pings
	for (const auto& pingData : shoalExtractionOutput->m_pClosedShoal->GetPings()) {
		// Ajout des echos par ping
		const auto& pingEchos = pingData->GetShoalStat()->GetEchoList();
		echos.insert(echos.cend(), pingEchos.cbegin(), pingEchos.cend());
	}

	return echos;
}

boost::python::object wrap_getContours(const ShoalExtractionOutput * shoalExtractionOutput) {
	std::vector<Contour> contours;

	// Parcours des contours GPS
	for (const auto& gpsContour : shoalExtractionOutput->m_pClosedShoal->GetGPSContourStrips()) {
		Contour contour;
		// Les différentes composantes des coordonnées sont stockées les unes après les autres
		for (size_t i = 0; i < gpsContour.size() - 2; i += 3) {
			contour.emplace_back(
				gpsContour.at(i),
				gpsContour.at(i + 1),
				gpsContour.at(i + 2)
			);
		}
		contours.push_back(contour);
	}

	return boost::python::object(contours);
}

double wrap_getVolume(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetVolume();
}

double wrap_getStartTime(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetBeginTime().ToSeconds();
}

double wrap_getEndTime(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetEndTime().ToSeconds();
}

Vector3D wrap_getEnergyCoordinates(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetGravityCenter();
}

Vector3D wrap_getGeoCenterCoordinates(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetGeographicCenter();
}

Vector3D wrap_getFirstPointCoordinates(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetFirstPoint();
}

Vector3D wrap_getLastPointCoordinates(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetLastPoint();
}

double wrap_getAcrossMinAngle(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetAcrossAngleMin();
}

double wrap_getAcrossMaxAngle(const ShoalExtractionOutput * shoalExtractionOutput) {
	return shoalExtractionOutput->m_pClosedShoal->GetShoalStat()->GetAcrossAngleMax();
}

void pyMovies::wrapShoalExtraction() {
	using namespace boost::python;

	def("moShoalExtraction", pyShoalExtraction, return_value_policy<reference_existing_object>());

	class_<ShoalExtractionModule, boost::noncopyable>("ShoalExtractionModule", no_init)
		.def("setEnable", &ShoalExtractionModule::setEnable)
		.def("getEnable", &ShoalExtractionModule::getEnable)
		.def("GetOutputList", wrap_ShoalExtractionOutputs)
		.def("ReleaseOutputs", wrap_releaseOutputs);

	class_<std::vector<ShoalExtractionOutput*> >("std::vector<ShoalExtractionOutput*>")
		.def(vector_indexing_suite<std::vector<ShoalExtractionOutput*>, true >());

	class_<ShoalExtractionOutput, boost::noncopyable>("ShoalExtractionOutput", no_init)
		.add_property("ShoalId", wrap_getExternId)
		.add_property("SounderId", wrap_getSounderId)
		.add_property("TransId", wrap_getTransId)
		.add_property("MinDepth", wrap_getMinDepth)
		.add_property("MaxDepth", wrap_getMaxDepth)
		.add_property("MinDistanceBottom", wrap_getMinDistanceBottom)
		.add_property("MaxDistanceBottom", wrap_getMaxDistanceBottom)
		.add_property("Length", wrap_getLength)
		.add_property("Width", wrap_getWidth)
		.add_property("Height", wrap_getHeight)
		.add_property("MeanSv", wrap_getMeanSv)
		.add_property("SigmaAg", wrap_getSigmaAg)
		.add_property("MeanPondSv", wrap_getMeanPondSv)
		.add_property("EchoCount", wrap_getNbEchos)
		.add_property("Echos", wrap_getEchos)
		.add_property("Contours", wrap_getContours)
		.add_property("Volume", wrap_getVolume)
		.add_property("StartTime", wrap_getStartTime)
		.add_property("EndTime", wrap_getEndTime)
		.add_property("EnergyCoordinates", wrap_getEnergyCoordinates)
		.add_property("GeoCenterCoordinates", wrap_getGeoCenterCoordinates)
		.add_property("FirstPointCoordinates", wrap_getFirstPointCoordinates)
		.add_property("LastPointCoordinates", wrap_getLastPointCoordinates)
		.add_property("AcrossMinAngle", wrap_getAcrossMinAngle)
		.add_property("AcrossMaxAngle", wrap_getAcrossMaxAngle);
	
	class_<std::vector<shoalextraction::EchoInfo> >("std::vector<EchoInfo>")
		.def(vector_indexing_suite<std::vector<shoalextraction::EchoInfo>, true >());

	class_<shoalextraction::EchoInfo, boost::noncopyable>("EchoInfo", no_init)
		.add_property("Coordinates", &shoalextraction::EchoInfo::position)
		.add_property("Energy", &shoalextraction::EchoInfo::energy)
		.add_property("ChannelCount", &shoalextraction::EchoInfo::channelNb);
	
	class_<std::vector<Contour> >("std::vector<Contour>")
		.def(vector_indexing_suite<std::vector<Contour>, true >());
	
	class_<Contour>("Contour")
		.def(vector_indexing_suite<std::vector<Vector3D>, true >());
}
