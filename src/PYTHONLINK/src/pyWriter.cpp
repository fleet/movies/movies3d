
#include "PythonLink/pyWriter.h"

#define HAVE_ROUND
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

// Rmq. bizarre, si on inclu ca avant les ent�tes python, ca ne compile plus ...
#include "Reader/ReaderCtrl.h"
#include "ModuleManager/ModuleManager.h"

void pyMovies::pyStartWrite(const char * destDirBuffer, const char * fileNamePrefix, const WriteAlgorithm::OutputType& outputType)
{
	ReaderCtrl *pRead = ReaderCtrl::getInstance();
	WriteAlgorithm*	pW = pRead->GetWriteAlgorithm();
	pW->CloseFile();
	if (fileNamePrefix)
	{
		pW->SetFilePrefix(fileNamePrefix);
	}
	if (destDirBuffer)
	{
		std::string name(destDirBuffer);
		pW->SetDirectory(name.c_str());
	}
	pW->SetOutputType(outputType);
}

BOOST_PYTHON_FUNCTION_OVERLOADS(pyStartWrite_overloads, pyMovies::pyStartWrite, 2, 3)

void pyMovies::pyStopWrite()
{
	ReaderCtrl *pRead = ReaderCtrl::getInstance();
	WriteAlgorithm*	pW = pRead->GetWriteAlgorithm();
	pW->CloseFile();
}

void pyMovies::wrapWriter()
{
  using namespace boost::python;

  def("moStartWrite", pyStartWrite, pyStartWrite_overloads(args("destDirBuffer", "fileNamePrefix", "outputType")));
  def("moStopWrite", pyStopWrite);

  enum_<WriteAlgorithm::OutputType>("WriteOutputType")
	  .value("Hac", WriteAlgorithm::OutputType::Hac)
	  .value("NetCDF", WriteAlgorithm::OutputType::NetCDF)
	  .value("Both", WriteAlgorithm::OutputType::Both)
	  ;
}
