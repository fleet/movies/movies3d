#pragma once

#include "ReaderExport.h"
#include <cstdint>

/*
*
*	This class will define the stop condition used while reading Ping Fan
*   possible mode are :
*		-Read n fan for a given sounderId, if no sounderId is given or it is unknown, read at least n fan for each sounder and stop
*		-Read for a given time : ie we consider the newer pingFan date, and read new one until the date of PingFan for all sounders becomes strictly superior to the startdate + the given time
*/
class READER_API ChunckDef
{
public:
	ChunckDef()
	{
		m_bIsUseTimeDef = false;
		m_TimeElapsed = 60;
		m_SounderId = 1;
		m_NumberOfFanToRead = 2;
	}
	/// @m_bIsUseTimeDef tell if we use a timed mode or if we use a numbered mode
	bool m_bIsUseTimeDef;
	/// @the number of second to wait for if mode is set to m_bIsUseTimeDef=true
	unsigned int  m_TimeElapsed;

	/// @the sounderId we are looking for if mode is set to m_bIsUseTimeDef=false
	std::uint32_t m_SounderId;

	unsigned int m_NumberOfFanToRead;
};

