#pragma once
#include "ReaderExport.h"

class READER_API ChunckEventList
{
public:
	ChunckEventList(void);
	~ChunckEventList(void);
	void Reset();
	bool m_fanAdded;
	bool m_sounderChanged;
	bool m_streamClosed;
	bool m_streamStreamOpened;
	bool m_singleTargetAdded;
	bool m_esuEnded;
	bool m_tupleHeaderUpdate;
};
