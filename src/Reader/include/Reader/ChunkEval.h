/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		ChunkEval.h												  */
/******************************************************************************/
#pragma once
#include "ReaderExport.h"
#include "M3DKernel/datascheme/DateTime.h"
#include "M3DKernel/algorithm/base/EchoAlgorithm.h"
#include "ChunckEventList.h"

#include <map>
typedef	 std::map<std::uint32_t, unsigned int > MapFrameAdded;

class READER_API ChunkEval : public EchoAlgorithm
{
public:
	virtual void Start() = 0;
	virtual bool CheckStop() = 0;
	virtual void Stop() {}
	virtual void PingFanAdded(PingFan *pFan)
	{
		m_CurrentEventList.m_fanAdded = true;
	}
	virtual void SounderChanged(std::uint32_t sounderId)
	{
		m_CurrentEventList.m_sounderChanged = true;
	}
	virtual void StreamClosed(const char *streamName)
	{
		m_CurrentEventList.m_streamClosed = true;
	}
	virtual void StreamOpened(const char *streamName)
	{
		m_CurrentEventList.m_streamStreamOpened = true;
	}
	virtual void SingleTargetAdded(std::uint32_t sounderId)
	{
		m_CurrentEventList.m_singleTargetAdded = true;
	}

	virtual void ESUEnd(ESUParameter * pWorkingESU, bool abort)
	{
		m_CurrentEventList.m_esuEnded = true;
	}

	virtual void TupleHeaderUpdate()
	{
		m_CurrentEventList.m_tupleHeaderUpdate = true;
	}

	virtual ~ChunkEval() {}
	ChunckEventList m_CurrentEventList;
	
	void Abort() 
	{
		m_Abort = true;
	}

protected:
	ChunkEval(const std::string& name);
	bool m_Abort;
};

class READER_API ChunkEvalNumbered : public ChunkEval
{
public:
	MovCreateMacro(ChunkEvalNumbered);
	virtual void PingFanAdded(PingFan *pFan);
	virtual void SounderChanged(std::uint32_t sounderId);
	virtual void StreamClosed(const char *streamName);
	void	Init(std::uint32_t SounderId, unsigned int num);

	virtual void Start();
	virtual bool CheckStop();
	virtual void Stop();

	virtual ~ChunkEvalNumbered() { Stop(); }

protected:
	ChunkEvalNumbered();

private:
	bool m_bShouldStop;
	std::uint32_t m_SounderChecked;
	unsigned int m_NumberExpected;
	MapFrameAdded m_mapNbFrame;
};

class READER_API ChunkEvalTimed : public ChunkEval
{
public:
	MovCreateMacro(ChunkEvalTimed);
	virtual void PingFanAdded(PingFan *pFan);
	virtual void StreamClosed(const char *streamName);

	void	Init(unsigned int numSecond);

	virtual void Start();
	virtual bool CheckStop();
	virtual void Stop();

	virtual ~ChunkEvalTimed() { Stop(); }

protected:
	ChunkEvalTimed();

private:
	bool m_bShouldStop;
	unsigned int m_NumberOfSecond;
	HacTime	m_StartDate;
	HacTime	m_OlderDate;
};
