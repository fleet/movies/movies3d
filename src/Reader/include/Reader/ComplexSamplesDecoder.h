#pragma once

class Sounder;
class Transducer;
class SoftChannel;
class Environnement;
class SpectralAnalysisDataObject;
class ReaderTraverser;

#include <complex>
#include <vector>

#include "BaseMathLib/BaseMathLib.h"

#include "M3DKernel/datascheme/BeamDataObject.h"
#include "M3DKernel/datascheme/PhaseDataObject.h"

class ReaderUtils
{
public:
	static void processComplexSamples(ReaderTraverser &trav
		, std::vector<std::complex<float>> & samples
		, Sounder* sound, SimradBeamType beamType, int nbQuadrants
		, MovObjectPtr<BeamDataObject> beamData, MovObjectPtr<PhaseDataObject> phaseData);

	static void sumQuadrantValues(std::vector<std::complex<float>> & complexSamples, unsigned int nbQuandrants);

	/// Computes the Sv(f) spectrum and returns it in a map of <Range, <Frequency, Value>>
	static void spectrumComputation(Sounder* sounder, Transducer* transducer, SoftChannel* softChannel, Environnement* env
		, double df, const std::vector<std::complex<float>>& samples
		, const std::vector<std::complex<float>> & filt
		, double signalSquaredNorm
		, SpectralAnalysisDataObject& values
		, int nbQuadrants);

};

