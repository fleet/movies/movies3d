#pragma once

#include "M3DKernel/utils/log/ILogger.h"

#include "M3DKernel/DefConstants.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

class ConversionTools {
private:
	static constexpr const char* LoggerName = "Reader.ConversionTools";

public:

	static SimradBeamType fromSonarBeamType(const sonarNetCDF::sonar::BeamType::Type& sonarBeamType) {
		switch (sonarBeamType) {
		case sonarNetCDF::sonar::BeamType::Type::SINGLE:
			return BeamTypeSingle;

		case sonarNetCDF::sonar::BeamType::Type::SPLIT_APERTURE_3_SUBBEAMS:
			return BeamTypeSplit3;

		case sonarNetCDF::sonar::BeamType::Type::SPLIT_APERTURE_3_1_SUBBEAMS:
			return BeamTypeSplit3CN;

		case sonarNetCDF::sonar::BeamType::Type::SPLIT_APERTURE_4_SUBBEAMS:
			return BeamTypeSplit;

		case sonarNetCDF::sonar::BeamType::Type::SPLIT_APERTURE_ANGLES:
			return BeamTypeSplit;

		default:
			M3D_LOG_WARN(LoggerName, Log::format("No match for NetCDF beam_type (%s)", sonarNetCDF::sonar::BeamType::getName(sonarBeamType).c_str()));
			return BeamTypeSplit;
		}
	}


	static sonarNetCDF::sonar::BeamType::Type fromSimradBeamType(const SimradBeamType& simradBeamType) {
		switch (simradBeamType) {
		case BeamTypeSingle:
			return sonarNetCDF::sonar::BeamType::Type::SINGLE;

		case BeamTypeSplit:
			return sonarNetCDF::sonar::BeamType::Type::SPLIT_APERTURE_4_SUBBEAMS;

		case BeamTypeSplit3:
			return sonarNetCDF::sonar::BeamType::Type::SPLIT_APERTURE_3_SUBBEAMS;

		case BeamTypeSplit3CN:
			return sonarNetCDF::sonar::BeamType::Type::SPLIT_APERTURE_3_1_SUBBEAMS;

		case BeamTypeRef:
		case BeamTypeRefB:
		case BeamTypeSplit2:
		case BeamTypeSplit3C:
		case BeamTypeSplit3CW:
		case BeamTypeSplit4B:
		case BeamTypeSplitFuture:
		case BeamTypeADCPSingle:
		case BeamTypeADCPSplit2:
		case BeamTypeSinglePassive:
			return sonarNetCDF::sonar::BeamType::Type::SPLIT_APERTURE_ANGLES;

		default :
			M3D_LOG_WARN(LoggerName, Log::format("No match for SIMRAD beam_type (%d)", simradBeamType));
			return sonarNetCDF::sonar::BeamType::Type::SPLIT_APERTURE_ANGLES;
		}
	}
};