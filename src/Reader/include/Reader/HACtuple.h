/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		HACTuple.h      											  */
/******************************************************************************/
#ifndef HAC_TUPLE
#define HAC_TUPLE

#include "ReaderExport.h"



#include "Reader/TupStructDef.h"



#include "M3DKernel/M3DKernel.h"

#include "MovStream.h"
#include "ReaderTraverser.h"

class READER_API HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	HACTuple();



	virtual void DecodeOrIgnore(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
	{
		if (Ignore())
			return;
		else
			Decode(IS, trav, taille_tup);
	}


	virtual bool Ignore() { return false; }


protected:

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup) = 0;



};


#endif //HAC_TUPLE
