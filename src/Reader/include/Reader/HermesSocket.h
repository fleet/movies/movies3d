#ifdef WIN32

#include <WinSock2.h>

//===================================================================
// Project : MOVIES 3D
//
//===================================================================

#ifndef HermesSocket_HH
#define HermesSocket_HH
#include "ReaderExport.h"

#include "M3DKernel/utils/socket/SocketCore.h"


const int	SIZE_MAX_HMS = 10240;

//====================== Other declarations =========================
#include <string>




//-------------------------------------------------------------------
//----------------------- class MvHermesSocket
//-------------------------------------------------------------------

class READER_API HermesSocket : CSocketCore
{
public:

	HermesSocket();

	virtual ~HermesSocket();

	virtual	bool		Open(const std::int32_t port2,
		const std::string address
	);
	// returns FALSE if problems with the following value in m_error_code
	// 1	Pb Creating the socket (see the explicit message in m_error)
	// 2	Pb connecting the socket
	// 3	Pb witn the reuse socket option.

	virtual int			Read();
	// Read the frame.

	char *				GetBuffer();

	int					GetRecLength();

	unsigned int		GetPort();
	// returns the port number

	std::string				GetAddress();
	// returns the address

	short GetErrorCode() { return m_error_code; }

	const sockaddr& getSenderAdress() const { return m_senderAddress; }

protected:


	//------------------------  Attributes

	unsigned int		m_port;			// No of port
	std::string			m_address;		// IP Adress
	SOCKET              m_asyncsocket;	// socket
	char				m_buffer[SIZE_MAX_HMS];

	int					m_length;
	short m_error_code;
	// number of readen bytes

	SOCKADDR_IN    m_SockAddrIn;
	sockaddr	m_senderAddress;


	//------------------------  Protected Methods

};

//===================================================================
//			   Inline functions
//===================================================================

//-------------------------------------------------------------------
// METHOD		: HermesSocket::GetPort
//-------------------------------------------------------------------
inline unsigned int HermesSocket::GetPort()
{
	return m_port;

} // HermesSocket::GetPort

//-------------------------------------------------------------------
// METHOD		: HermesSocket::GetAddress
//-------------------------------------------------------------------
inline std::string HermesSocket::GetAddress()
{
	return m_address;

} // HermesSocket::GetAddress

#endif // HermesSocket_HH

#endif

