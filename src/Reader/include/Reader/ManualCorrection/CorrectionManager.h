#pragma once

#include "Reader/ReaderExport.h"

#include "Reader/ManualCorrection/OriginalPingFanData.h"
#include "M3DKernel/datascheme/MemoryObject.h"
#include "Reader/SonarNetCDF/Sonar/GroupBeam.h"

#include <map>
#include <set>

class PingFan;
class Transducer;
class MovReadService;

namespace BaseMathLib {
	class Poly2D;
}

// Classe charg�e de la gestion de la correction du fond et des �chos depuis la
// vue 2D longitudinale.
class READER_API CorrectionManager
{

public:
	// Constructeur
	CorrectionManager();

	// Destructeur
	virtual ~CorrectionManager();

	// Modifie la valeur de fond
	void CorrectBottom(PingFan * pFan, unsigned short softchannelId, std::int32_t bottomRange);

	// Suppression d'un polygone d'�chos (monofaisceau uniquement)
	void DeletePolygon(std::uint32_t sounderId, unsigned int transducerIndex, BaseMathLib::Poly2D * polygon);

	// annule l'ensemble des modifications effectu�es
	void Undo();

	// sauvegarde l'ensemble des modifications effectu�es dans le ficheir hac, en cr�ant un fichier .bak s'il n'existe pas d�j�
	std::string Save(MovReadService * pFileReadService);

	// Indique si des modifications ont �t� apport�es ou non
	bool HasModifications();

private:
	// cr�ation d'un fichier .bak avant la modification si besoin est
	void CreateBakupFileIfNeeded(const std::string & fileName);

	// Ecriture des nouvelles valeurs d'�chos dans le fichier HAC
	bool ReplaceEchoes(std::fstream & fs, const OriginalBeamData & originalBeamData, MemoryObjectDataFmt * pBeam);

	// Sauvegarde des modifications dans un fichier HAC
	std::string SaveHac(const std::string& filename);

	// Sauvegarde des modifications dans un fichier NetCDF
	std::string SaveNetCDF(const std::string& filename);

	// Mise à jour des echos des variable backscatter
	void UpdateBackscatterVariableAtEchoIndexes(
		sonarNetCDF::GroupBeam* beamGroup,
		const uint64_t& pingTimeIndex,
		const std::set<int>& echoIndexes,
		MemoryObjectDataFmt* alteredDataFmt
	);

	// Structure de sauvegarde des donn�es originales
	std::map<PingFan*, OriginalPingFanData> * m_pBackup;
};
