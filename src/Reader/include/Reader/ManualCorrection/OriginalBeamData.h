#pragma once
#include <cstdint>

#include "M3DKernel/DefConstants.h"
#include <map>
#include <set>

class PingFan;

// Classe de sauvegarde des donn�es modifi�es manuellement pour une channel
class OriginalBeamData
{
public:
	// Constructeur
	OriginalBeamData();

	// D�finition des donn�es originales concernant le fond
	void SetOriginalBottomData(bool bottomFound, std::int32_t bottomRange);

	// D�finition des donn�es originales concernant les �chos
	void SetOriginalEchoData(int echoIndex, DataFmt echoValue);

	// Annulation des modifications
	void Undo(unsigned short softChannelId, PingFan * pFan);

	// Indique si la valeur du fond a �t� modifi�e
	bool IsBottomEdited() const;

	// Indique si un �cho a �t� modifi�
	bool IsEchoEdited(int echoIndex) const;

	// Indique si au moins un �cho a �t� modifi�
	bool IsEchoEdited() const;

	// Retourne l'ensemble des index d'écho modifiés
	std::set<int> EditedEchoIndexes() const;

private:
	// Pour le fond
	bool m_bIsBottomEdited;
	std::int32_t m_bottomRange;
	bool m_bottomFound;

	// Pour les �chos
	std::map<int, DataFmt> m_echoes;

};
