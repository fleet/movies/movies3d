#pragma once

#include "OriginalBeamData.h"

#include <map>

class PingFan;

// Classe de sauvegarde des donn�es modifi�es manuellement pour un ping
class OriginalPingFanData
{
public:
	// Constructeur
	OriginalPingFanData();

	// D�finition des donn�es originales concernant le fond
	void SetOriginalBottomData(unsigned short softChannelId, bool bottomFound, std::int32_t bottomRange);

	// D�finition des donn�es originales concernant les valeurs d'�chos
	void SetOriginalEchoData(unsigned short softChannelId, int echoIndex, DataFmt echoValue);

	// Annulation des modifications
	void Undo(PingFan * pFan);

	// Acc�s aux informations sauvegard�es pour le pingfan
	const std::map<unsigned short, OriginalBeamData> & GetData() const;

private:

	// Donn�es modifi�es pour chaque channel
	std::map<unsigned short, OriginalBeamData> m_Backup;
};
