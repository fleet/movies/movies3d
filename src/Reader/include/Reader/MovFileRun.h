/******************************************************************************/
/*	Project:	MOVIE3D														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		MovFileRun.h      											  */
/*	@Description:
/*		Class managing survey and hac file reprository
/*
/*
/******************************************************************************/

#ifndef MOVFILERUN_HH
#define MOVFILERUN_HH

#include "ReaderExport.h"
#include <string>
#include <vector>
#include <fstream>

#include "M3DKernel/datascheme/DateTime.h"

// Structure contenant les parametres nécessaires à un Goto
struct GoToTarget
{
	bool byPing; // true si la cible est un numéro de ping, false si c'est une date
	HacTime targetTime; // cible temporelle
	std::uint32_t targetPingNumber; // cible en numéro de ping
};

class READER_API MovFileRun
{
public:
	MovFileRun();

	virtual ~MovFileRun();

	void ClearFileList();

	void Copy(const MovFileRun &fileRun);

	unsigned int GetFileCount() const;
	std::string GetFileName(unsigned int idx) const;	
    std::string GetCurrentFileName() const;

	/// change the current file to a next one
	void SetCurrentFile(unsigned int idx);
	unsigned int GetCurrentFileIdx() const { return m_currentFileIdx; }
	void RemoveFile(unsigned int idx);
	void RemoveFile(const char *Name);

	/// this function check wether the source is a file or directory and call the subsequent init
	void SetSourceName(const std::string & SourceName, const std::string & ExtFilter);

	void SetFileList(const std::vector<std::string> & files);

	void setEndOfStreamReached();
	bool getEndOfStreamReached() const { return m_isEndOfStreamReached; }

protected:

	/// this function create a survey Path and create a file List
	void SetSurveyPath(const std::string & aSurveyPath, const std::string & aSurveyExtFilter);

	/// this function is used to read a single hac file
	void SetFilePath(const std::string & aSurveyPath);

private:
	std::string m_SurveyPath;
	std::string m_SurveyExtFilter;

	// list of the CFiles of these files 
	std::vector<std::string> m_fileList;

	// index over the current file in the lists
	unsigned int m_currentFileIdx;

	void BuildFileList();
	bool m_isEndOfStreamReached;
	void ParseDirectory(std::string DirName);
	void AddFile(std::string fileName);
};

#endif
