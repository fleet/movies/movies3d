/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	C.PONCELET													  */
/*	File:		 MovReadService.h											  */
/******************************************************************************/
#ifndef MOV_HACFILE_READ_SERVICE
#define MOV_HACFILE_READ_SERVICE

#include "ReaderExport.h"
#include "Reader/MovHacReadService.h"

#include <string>

#include <fstream>

#include "Reader/MovFileRun.h"

class READER_API MovHacFileReadService : public MovHacReadService
{
public:
	//	Constructors / Destructor
	//	*************************
	MovHacFileReadService(MovFileRun &HacDirOrFileName);
	virtual  ~MovHacFileReadService();

	virtual void Init(ReaderTraverser &ref);
	
	virtual void Close(ReaderTraverser &ref);

	virtual bool PrepareGoTo(GoToTarget target, ReaderTraverser &ref);

	virtual bool ExecuteGoTo(GoToTarget target, ReaderTraverser &ref);

	bool IsFileService() const;

	MovFileRun GetFileRun() const;

	bool GetLimits(HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing) const;

	std::uint32_t GetCurrentFilePos() const override;
	void Close();
	virtual bool SeekFilePos(std::uint32_t pos) override;

	virtual bool WillBeLastClose();

protected:	
	virtual int Read(ReaderTraverser &ref);

	void OpenNewHac(ReaderTraverser &ref);

private:
    std::ifstream* m_is;

	MovFileRun m_FileRun;
};

#endif //MOV_FILE_READ_SERVICE
