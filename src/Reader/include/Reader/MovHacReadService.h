/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	C.PONCELET													  */
/*	File:		 MovReadService.h											  */
/******************************************************************************/
#ifndef MOV_HACREAD_SERVICE
#define MOV_HACREAD_SERVICE

#include "MovReadService.h"
#include "ServiceFormatDesc.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"

#include "MovStream.h"
#include "ReaderTraverser.h"
#include "TupleDecoder.h"

#include <string>
#include <queue>

typedef std::queue<MovStream*> streamQueue;

class READER_API MovHacReadService : public MovReadService
{
public:
	//	Constructors / Destructor
	//	*************************
	MovHacReadService();
	virtual ~MovHacReadService();
	
	virtual const ServiceFormatDesc * getServiceFormatDesc() const;

	virtual bool IsClosed() const { return m_bIsClosed; }

	// OTK - FAE214 - Permet, dans le cas des fichiers, de ne pas pusher le ping en attente sur un changement de fichier
	// en effet, en mode de d�coupage par taille d'HERMES, un ping en attente peut avoir sa fin sur le ficheir suivant.
	virtual bool WillBeLastClose() { return true; }
	
	void Execute(ReaderTraverser &ref);

	virtual void Close(ReaderTraverser &ref) {}

	HacServiceFormatDesc m_ServiceFormatDesc;
	std::string getName() const { return m_Name; }

	// return the first stream to decode
	MovStream *PopStream();

	// push a stream to decode fifo
	void PushStream(MovStream *);

	// push back a stream into waiting fifo
	void ReleaseStream(MovStream *);

protected:
	// get a stream for reading operation
	MovStream *ReserveStream();

private:
	// allocate reserved stream
	void AllocateStream();
	void FreeStream();
	streamQueue m_DecodeQueue;
	streamQueue m_ReaderQueue;
	CRecursiveMutex m_stackLock;

protected:
	virtual void Init(ReaderTraverser &ref);

	// la fonction read va lire le media consid�r� et ajouter les buffers obtenus dans la stream m_Stream
	virtual int Read(ReaderTraverser &ref) = 0;

	bool m_bIsClosed;
	bool m_bInitDone;
	std::string m_Name;
	TimeCounter m_count;
	TupleDecoder m_tupleDecoder;
};

#endif //MOV_READ_SERVICE