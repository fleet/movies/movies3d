/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	C.PONCELET													  */
/*	File:		 MovReadService.h											  */
/******************************************************************************/
#ifndef MOV_HACSOCK_READ_SERVICE
#define MOV_HACSOCK_READ_SERVICE

#ifdef WIN32

#include "ReaderExport.h"
#include "MovHacReadService.h"
#include "M3DKernel/utils/multithread/Monitor.h"

#include <atomic>
#include <map>
#include <string>

#include <mutex>
#include <thread>
#include <condition_variable>

class CSocketBuffer
{
public:
	CSocketBuffer();

	~CSocketBuffer();

	void SetBufSize(int bufSize);
	
	// Attend que l'état 'm_consumeState' soit à l'état donné
	void WaitConsumeState(bool state);

	/// Change l'état de 'm_consumeState' et notifie le changement
	void SetConsumeState(bool state);
	
	char             *m_pBuffer;
	int		          m_BufLen; // Longueur valable du buffer
	int	              m_BufSize; // longueur allouée pour le buffer

private:
	bool m_consumeState;
	std::mutex m_mutex;
	std::condition_variable_any m_cond;


	// disable copy
	CSocketBuffer(const CSocketBuffer &) = delete;
	CSocketBuffer& operator=(const CSocketBuffer&) = delete;
};

class HermesSocket;
class READER_API MovHacSockReadService : public MovHacReadService
{
public:
	//	Constructors / Destructor
	//	*************************
	MovHacSockReadService(unsigned short a1, unsigned short a2, unsigned short a3, unsigned short a4, unsigned int por, unsigned int callbackPort);
	virtual ~MovHacSockReadService();

	bool IsFileService() const;

	MovFileRun GetFileRun() const;

	bool GetLimits(HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing) const;

	bool PrepareGoTo(GoToTarget target, ReaderTraverser &ref);
	bool ExecuteGoTo(GoToTarget target, ReaderTraverser &ref);

	virtual void Init(ReaderTraverser &ref);
	virtual void Close();
	virtual void Receive();

protected:

	enum class HermesMessageType
	{
		RCD, RCH, RCS, RCT, RCP, RCE, RC_UNKNOWN
	};

	/**
	* \brief Reads the socket buffer and decode hermes messages.
	* \note Because the communication uses the UDP protocol, the packets are not guaranted to arrive in the order they are sent.
	* However, this behavior is managed for RCD messages via a buffer (see m_messageBuffer).
	*/
	virtual int Read(ReaderTraverser &ref);
	int ReadMessageRCD(const char* message, int length);
	int replayRCDPackets(int from, int to, int& lastPacketSent);
	HermesMessageType getHermesMessageType(const char* message, int length) const;

	char CHad[16];
	unsigned int m_portNum;
	unsigned int m_callbackPortNum;
	HermesSocket *m_pSock;

	// OTK - 11/05/2009 - ajout test sur les numero de packets poru d�tection des pertes
	bool m_FirstMessage;
	int m_MessageNo;
	std::map<int, std::string> m_messageBuffer; ///<Message buffer for RCD messages which arrive in wrong order

	// OTK - 27/05/2009 - ajout d'un thread d�di� � l'�coute des messages r�seau
	std::thread m_thread;
	std::atomic_bool m_bContinueOk;

	CSocketBuffer *ReadBuffer();
	CSocketBuffer *SelectBuffer();

	// tableau des buffers recus
	CSocketBuffer             *m_Buffers;

	// taille du buffer des buffers
	int                        m_NBuf;
	int                        m_BufferIdx;
	int                        m_BufferReadIdx;

	// NMD - Buffer pour concatenation des paquest RCD
	MovStream * m_concatStream;
	unsigned short m_concatTupleType;

	/// NMD - Buffer pour concatenation des paquets RCH
	int headerencoursdereception;
	char bufferHeader[25000];
	int headersize;

	void askHacHeaders(const struct sockaddr* address, int port);
};

#endif
#endif //MOV_SOCK_READ_SERVICE
