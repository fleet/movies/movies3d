/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	C.PONCELET													  */
/*	File:		 MovReadService.h											  */
/******************************************************************************/
#ifndef MOV_READ_SERVICE
#define MOV_READ_SERVICE

#include "ReaderExport.h"
#include "ServiceFormatDesc.h"
#include "ReaderTraverser.h"
#include "ServiceFormatDesc.h"
#include "MovFileRun.h"

#include <string>

class READER_API MovReadService
{
public:
	//	Constructors / Destructor
	//	*************************
	MovReadService();
	virtual ~MovReadService();

	virtual bool ReOpen(std::uint32_t pos);
	virtual void Execute(ReaderTraverser & readerTraverser) = 0;
	virtual void Close(ReaderTraverser &ref) = 0;
	virtual void Close() = 0;
	virtual bool IsClosed() const = 0;

	const char * getStreamDesc() const;
	virtual void setStreamDesc(const char * streamDesc);

	virtual const ServiceFormatDesc * getServiceFormatDesc() const = 0;

	virtual std::uint32_t GetCurrentFilePos() const;
	virtual bool SeekFilePos(std::uint32_t pos) { return false; }

	virtual MovFileRun GetFileRun() const = 0;

	virtual bool GetLimits(HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing) const = 0;

	/// Pr�paration du service au GoTo, retourne vrai si un ReadChunk doit �tre execut� ant execution du GoTo, faux sinon
	virtual bool PrepareGoTo(GoToTarget target, ReaderTraverser &ref) = 0;

	/// Execution du GoTo
	virtual bool ExecuteGoTo(GoToTarget target, ReaderTraverser &ref) = 0;

	virtual bool IsFileService() const = 0;

protected:
	std::string m_StreamDesc;
};

#endif //MOV_READ_SERVICE