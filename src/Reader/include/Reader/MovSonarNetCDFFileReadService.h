/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	C.PONCELET													  */
/*	File:		 MovReadService.h											  */
/******************************************************************************/
#ifndef MOV_SONARNETCDFFILE_READ_SERVICE
#define MOV_SONARNETCDFFILE_READ_SERVICE

#include "MovReadService.h"
#include "Reader/SonarNetCDF/NetCDFUtils.h"

#include <memory>

class READER_API MovSonarNetCDFFileReadService : public MovReadService
{
public:
	//	Constructors / Destructor
	//	*************************
	MovSonarNetCDFFileReadService(MovFileRun &HacDirOrFileName);
	virtual ~MovSonarNetCDFFileReadService();

	const ServiceFormatDesc * getServiceFormatDesc() const;

	/// Return the state of the stream description flag
	bool hasStreamDesc() const;

	/// Reset the stream description flag to false
	void clearStreamDesc();

	/// Set the stream description, and set the stream description flag to true
	void setStreamDesc(const char * streamDesc) override;

	bool IsFileService() const;

	MovFileRun GetFileRun() const;
	
	bool GetLimits(HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing) const;
	
	virtual void Execute(ReaderTraverser & readerTraverser);

	virtual void Close(ReaderTraverser &ref);

	virtual void Close();

	virtual bool IsClosed() const;
	
	virtual bool ReOpen(std::uint32_t pos) override;
	
	virtual bool PrepareGoTo(GoToTarget target, ReaderTraverser &ref);

	virtual bool ExecuteGoTo(GoToTarget target, ReaderTraverser &ref);

private:
	struct Impl;
	std::unique_ptr<Impl> impl;
};

namespace sonarNetCDF {

	class GroupTopLevel;
	class GroupBeam;

	class SounderFactory {

		struct ChannelDef {
			std::map<std::string, std::uint32_t> channels;
		};

	public:
		SounderFactory();

		std::uint32_t getSounderId(const std::string & sounderSerialNumber);

		unsigned short getChannelId(const GroupBeam* beamGroup, const size_t& beamIndex);

		unsigned short getChannelId(std::uint32_t sounderId, const std::string & beamName);

	private:
		std::uint32_t m_nextSounderId;

		unsigned short m_nextChannelId;

		std::map<std::string, std::uint32_t> m_sounder_ids;

		std::map<std::uint32_t, ChannelDef> m_channel_defs;
	};

	class SonarFile : public ncFile {
	public:
		SonarFile(const std::string& fileName, SounderFactory* sounderFactory);

		void open(bool resetPosition = true);
		void close();

		void read(MovSonarNetCDFFileReadService * service, ReaderTraverser &ref, const HacTime & time);

		GroupTopLevel* getTopLevelGroup() const;

		const HacTime& getFileStart() const;
		const HacTime& getFileEnd() const;

		void setNextPingTime(const HacTime& nextPingTime);
		const HacTime& getNextPingTime() const;

		void setAtEnd(const bool& isAtEnd);
		const bool& isAtEnd() const;

		void setBeamPingIndex(const size_t& beamIndex, const size_t& pingIndex);
		void setAllBeamPingIndexTo(const size_t& pingIndex);
		const std::vector<size_t>& getBeamPingIndex() const;

	private:
		HacTime m_fileStart;

		HacTime m_fileEnd;

		HacTime m_nextPingTime;

		std::vector<size_t> m_beamPingIndex;

		std::unique_ptr<GroupTopLevel> m_topLevelGroup;

		SounderFactory* m_sounderFactory;

		bool m_isInit = false;
		bool m_atEnd = false;

		void initializeSounder(ReaderTraverser & ref);
		void decodeSv(ReaderTraverser &ref, const HacTime & hacTime, GroupBeam* beamGroup, size_t pingIndex);
		void decodeComplexSamples(ReaderTraverser &ref, const HacTime & hacTime, GroupBeam* beamGroup, size_t pingIndex);
	};
}

#endif //MOV_READ_SERVICE