/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET
/* 												  */
/*	File:		MOV_STREAM.h												  */
/******************************************************************************/
#pragma once
#include "ReaderExport.h"
#include <cstdint>

// this class will be use internally for read/write operation
// contient une zone de donn�e de dimension variable dans laquelle 
//l'utilisateur va pouvoir effectuer des op�ration simple d'ecriture, lecture, deplacement dans la zone de donn�e
enum eSTREAM_SEEK
{
	eSEEK_SET = 0,
	eSEEK_CUR = 1
};

struct libMove
{
	std::uint32_t LowPart;
};

class READER_API MovStream
{
public:
	MovStream();
	virtual ~MovStream();

	static const std::uint32_t defaultSize;

	// allocate and resize the data area (nb previous are lost)
	void	Reserve(std::uint32_t newSize);

	void  Resize(std::uint32_t newSize);

	// allocate and resize the data area (nb previous are NOT lost)
	void	ConservativeReserve(std::uint32_t newSize);

	// get a pointer to the write buffer
	char *	GetWriteBuffer(std::uint32_t sizeWanted);

	// get a pointer to the write buffer without losing previous data
	char *	GetConservativeWriteBuffer(std::uint32_t sizeWanted);

	// get a pointer to the read buffer
	char *	GetReadBuffer();

	// move the buffer
	bool	Seek(libMove dlibMove, std::uint32_t dwOrigin);

	// move the buffer to the start
	bool  Rewind();

	// read the buffer
	bool	Read(char* pv, std::uint32_t cb, std::uint32_t* pcbRead);

	// write the buffer
	bool	Write(char const* pv, std::uint32_t cb, std::uint32_t* pcbWritten);

	// add data to the buffer, the buffer is expanded if needed
	bool  Concat(char const* pv, std::uint32_t cb, std::uint32_t* pcbWritten);

private:
	std::uint32_t m_reservedSize;
	char *m_pData;
	char *m_pCurrent;
};

