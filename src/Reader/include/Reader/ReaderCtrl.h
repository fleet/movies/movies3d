#pragma once
#include "ReaderExport.h"
#include "ChunkEval.h"
#include "ReaderParameter.h"
#include "Reader/ReaderParameter.h"
#include "Reader/MovFileRun.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"

class MovFileRun;

class MovReadService;
class HacObjectMgr;
class EchoAlgorithmRoot;
class WriteAlgorithm;
class EchoAlgorithm;
class ChunkEval;
class AlgorithmAutoLength;
class TimeShiftEval;

class MovESUMgr;


class READER_API ReaderCtrl
{
public:
	static ReaderCtrl* getInstance();

	static void FreeMemory();

	static ReaderCtrl* m_pReaderCtrl;

	virtual ~ReaderCtrl();

	void OpenFileStream(MovFileRun &filerun);
	void OpenNetworkStream(unsigned short a1, unsigned short a2, unsigned short a3, unsigned short a4);

	// start reading a chunck (thread)
	void	StartReadChunck();

	// block until chunk is read
	void	WaitEndChunck(ChunckEventList&);

	// abort current chunck reading
	void	AbortReadChunck(ChunckEventList&);

	// tell if the chunck was read, non blocking
	bool	CheckChunckWasRead(ChunckEventList &ref);

	// OTK - 02/03/2009 - implémentation du GoTo
	bool	GoTo(GoToTarget target);

	void UpdateReaderParameter(ReaderParameter &RefParameter, bool ignoreChunkChange = false);
    ReaderParameter GetReaderParameter();
    void SetIgnoreSingleTargets(bool b);

    WriteAlgorithm*		GetWriteAlgorithm();
    MovReadService * GetActiveService();
	void ThreadRead();
	void ReadSounderConfig();

	bool IsFileService() const;

	// Récupération du filerun pour infos nécessaires au goto
	MovFileRun GetFileRun() const;

	// Détermination des limites du filerun en temps et en numero de ping
	bool GetLimits(HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing);
	
	// OTK - 05/11/2009 - lecture synchrone (dans le thread principal), utile pour matlab
	void SyncReadChunk();

private:
	ReaderCtrl();

	// Disable copy
	ReaderCtrl(const ReaderCtrl & other) = delete;
	ReaderCtrl & operator=(const ReaderCtrl & other) = delete;

	void ReadChunk();

	void UpdateChunckDef();

	void RemoveService();

    class Impl;
    Impl * impl;
};
