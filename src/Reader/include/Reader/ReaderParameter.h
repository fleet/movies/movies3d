#pragma once
#include "ReaderExport.h"
#include "ChunckDef.h"
#include "M3DKernel/parameter/ParameterModule.h"

class READER_API ReaderParameter : public BaseKernel::ParameterModule
{
public:
	ReaderParameter();
	void			ResetData();

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	void			setSortDataBeforeWrite(bool a) { m_sortDataBeforeWrite = a; }
	bool			getSortDataBeforeWrite() const { return m_sortDataBeforeWrite; }

	void			setIgnoreSingleTargets(bool a) { m_ignoreSingleTargets = a; }
	bool			getIgnoreSingleTargets() const { return m_ignoreSingleTargets; }

	void			setAcquisitionPort(int value) { m_acquisitionport = value; }
	int				getAcquisitionPort() const { return m_acquisitionport; }

	void			setCallbackPort(int value) { m_callbackPort = value; }
	int				getCallbackPort() const { return m_callbackPort; }

	ChunckDef		m_ChunckDef;

private:
	bool			m_sortDataBeforeWrite;
	bool			m_ignoreSingleTargets;

	int				m_acquisitionport;
	int				m_callbackPort;
};