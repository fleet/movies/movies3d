#pragma once

#include "M3DKernel/datascheme/Traverser.h"
class MovReadService;



class ReaderTraverser :
	public Traverser
{
public:
	ReaderTraverser(MovReadService *);
	virtual ~ReaderTraverser(void);
	MovReadService *GetActiveService() { return m_pReadService; }
protected:
	MovReadService *m_pReadService;
};
