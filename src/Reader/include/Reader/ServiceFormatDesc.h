#pragma once

#include "ReaderExport.h"
#include <cstdint>

#include <map>
#include <string>

class READER_API ServiceFormatDesc
{
public:
	virtual unsigned short getTupleType() const = 0;

	virtual std::map<std::string, std::string> getDesc() const = 0;
};

class READER_API HacServiceFormatDesc : public ServiceFormatDesc
{
public:
	HacServiceFormatDesc();

	unsigned short getTupleType() const;

	std::map<std::string, std::string> getDesc() const;
	
	unsigned short  m_TupleType;
	unsigned short	m_HacId;
	unsigned short	m_HacVersion;
	unsigned short  m_AcqSoftwareVersion;
	std::uint32_t	m_AcqSoftwareID;
    std::int32_t	m_tupleAttributes;
};
