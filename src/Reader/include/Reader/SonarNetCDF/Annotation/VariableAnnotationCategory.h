#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableAnnotationTime;

	// Optional category for the annotation, for use in grouping annotation types
	class VariableAnnotationCategory : public VariableSingleVariable<std::string, uint64_t> {
	public:
		VariableAnnotationCategory(ncObject* parent, VariableAnnotationTime* timeVariable);
		~VariableAnnotationCategory();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();
	};
}

