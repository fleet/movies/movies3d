#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableAnnotationTime;

	class VariableAnnotationText : public VariableSingleVariable<std::string, uint64_t> {
	public:
		VariableAnnotationText(ncObject* parent, VariableAnnotationTime* timeVariable);
		~VariableAnnotationText();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();
	};
}

