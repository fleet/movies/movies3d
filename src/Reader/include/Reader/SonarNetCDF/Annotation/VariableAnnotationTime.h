#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	class VariableAnnotationTime : public VariableSingleDimension<uint64_t> {
	public:
		VariableAnnotationTime(ncObject* parent, ncDimension* timeDimension);
		~VariableAnnotationTime();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();
	};
}

