#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableAcousticFrequency;

	// Indicative absorption values used to calculate the time - varied - gain(TVG), in the absence of more detailed data.
	class VariableAbsorptionIndicative : public VariableSingleVariable<float, float> {
	public:
		VariableAbsorptionIndicative(ncObject* parent, VariableAcousticFrequency* acousticFrequencyVariable);
		~VariableAbsorptionIndicative();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();
	};
}

