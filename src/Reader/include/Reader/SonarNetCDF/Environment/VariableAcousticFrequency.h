#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	class VariableAcousticFrequency : public VariableSingleDimension<float> {
	public:
		VariableAcousticFrequency(ncObject* parent, ncDimension* acousticFrequencyDimension);
		~VariableAcousticFrequency();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();
	};
}

