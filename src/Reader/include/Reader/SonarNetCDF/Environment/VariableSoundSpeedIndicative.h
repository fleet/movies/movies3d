#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	// Mean sound speed in water used to calculate echo range, in the absence of more detailed sound-speed data.
	class VariableSoundSpeedIndicative : public VariableScalar<float> {
	public:
		VariableSoundSpeedIndicative(ncObject* parent);
		~VariableSoundSpeedIndicative();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();
	};
}

