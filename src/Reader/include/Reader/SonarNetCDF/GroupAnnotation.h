#pragma once

#include "Reader/SonarNetCDF/GroupTopLevel.h"
#include "Reader/SonarNetCDF/NetCDFUtils.h"

#include <chrono>
#include <memory>
#include <vector>
#include <map>

namespace sonarNetCDF {

	class VariableAnnotationTime;
	class VariableAnnotationCategory;
	class VariableAnnotationText;

	class GroupAnnotation : public UnderTopLevelGroup {

	public:
		GroupAnnotation(ncGroup* const parent);
		~GroupAnnotation();

		// Group name
		const static std::string NC_GROUP_NAME;

		// Accesseurs
		const std::map<std::chrono::nanoseconds, std::pair<std::string, std::string>> getAnnotationMap();

		// Acc�s variables
		VariableAnnotationTime* getAnnotationTimeVariable() const;
		VariableAnnotationCategory* getAnnotationCategoryVariable() const;
		VariableAnnotationText* getAnnotationTextVariable() const;
		
	private:
		// Dimensions
		std::unique_ptr<ncDimension> m_timeDimension;

		// Variables
		std::unique_ptr<VariableAnnotationTime> m_timeVariable;
		std::unique_ptr<VariableAnnotationCategory> m_annotationCategoryVariable;
		std::unique_ptr<VariableAnnotationText> m_annotationTextVariable;

		// SubGroup
	};
}