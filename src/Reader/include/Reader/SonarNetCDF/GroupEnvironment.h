#pragma once

#include "Reader/SonarNetCDF/GroupTopLevel.h"
#include "Reader/SonarNetCDF/NetCDFUtils.h"

#include <memory>
#include <vector>
#include <map>

namespace sonarNetCDF {

	class VariableAbsorptionIndicative;
	class VariableAcousticFrequency;
	class VariableSoundSpeedIndicative;

	class GroupEnvironment : public UnderTopLevelGroup {

	public:
		GroupEnvironment(
			ncGroup* const parent,
			const size_t& frequencyCount = ncUtils::INVALID_SIZE
		);
		~GroupEnvironment();

		// Group name
		const static std::string NC_GROUP_NAME;

		// Acc�s variables
		VariableAbsorptionIndicative* getAbsorptionIndicativeVariable() const;
		VariableAcousticFrequency* getAcousticFrequencyVariable() const;
		VariableSoundSpeedIndicative* getSoundSpeedIndicativeVariable() const;

	private:
		// Dimensions
		std::unique_ptr<ncDimension> m_acousticFrequencyDimension;

		// Variables
		std::unique_ptr<VariableAbsorptionIndicative> m_absorptionIndicativeVariable;
		std::unique_ptr<VariableAcousticFrequency> m_acousticFrequenciesVariable;
		std::unique_ptr<VariableSoundSpeedIndicative> m_soundSpeedIndicativeVariable;
	};
}

