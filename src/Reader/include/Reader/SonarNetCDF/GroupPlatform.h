#pragma once

#include "GroupTopLevel.h"
#include "NetCDFUtils.h"

#include <memory>

namespace sonarNetCDF {

	class VariableMruIds;
	class VariableMruOffsetX;
	class VariableMruOffsetY;
	class VariableMruOffsetZ;
	class VariableMruRotationX;
	class VariableMruRotationY;
	class VariableMruRotationZ;
	class VariablePositionIds;
	class VariablePositionOffsetX;
	class VariablePositionOffsetY;
	class VariablePositionOffsetZ;
	class VariableTransducerIds;
	class VariableTransducerFunction;
	class VariableTransducerOffsetX;
	class VariableTransducerOffsetY;
	class VariableTransducerOffsetZ;
	class VariableTransducerRotationX;
	class VariableTransducerRotationY;
	class VariableTransducerRotationZ;
	class VariableWaterLevel;

	class GroupAttitude;
	class GroupPosition;

	class GroupPlatform : public UnderTopLevelGroup {

	public:
		GroupPlatform(
			ncGroup* const parent, 
			const size_t& transducerCount = ncUtils::INVALID_SIZE, 
			const size_t& positionCount = ncUtils::INVALID_SIZE, 
			const size_t& mruCount = ncUtils::INVALID_SIZE
		);
		~GroupPlatform();

		// Group name
		const static std::string NC_GROUP_NAME;

		// Dimensions
		ncDimension* const getTransducerDimension() const;
		ncDimension* const getPositionDimension() const;
		ncDimension* const getMruDimension() const;
		
		// Attributes
		void setPlatformCodeIces(const std::string& platformCodeIces);
		void setPlatformName(const std::string& platformName);
		void setPlatformType(const std::string& platformType);

		// Acc�s variables
		VariableMruIds* getMruIdsVariable() const;
		VariableMruOffsetX* getMruOffsetXVariable() const;
		VariableMruOffsetY* getMruOffsetYVariable() const;
		VariableMruOffsetZ* getMruOffsetZVariable() const;
		VariableMruRotationX* getMruRotationXVariable() const;
		VariableMruRotationY* getMruRotationYVariable() const;
		VariableMruRotationZ* getMruRotationZVariable() const;

		VariablePositionIds* getPositionIdsVariable() const;
		VariablePositionOffsetX* getPositionOffsetXVariable() const;
		VariablePositionOffsetY* getPositionOffsetYVariable() const;
		VariablePositionOffsetZ* getPositionOffsetZVariable() const;

		VariableTransducerIds* getTransducerIdsVariable() const;
		VariableTransducerFunction* getTransducerFunctionVariable() const;
		VariableTransducerOffsetX* getTransducerOffsetXVariable() const;
		VariableTransducerOffsetY* getTransducerOffsetYVariable() const;
		VariableTransducerOffsetZ* getTransducerOffsetZVariable() const;
		VariableTransducerRotationX* getTransducerRotationXVariable() const;
		VariableTransducerRotationY* getTransducerRotationYVariable() const;
		VariableTransducerRotationZ* getTransducerRotationZVariable() const;

		VariableWaterLevel* getWaterLevelVariable() const;

		// Accesseurs SubGroup 
		GroupAttitude* const getAttitudeSubGroup(const size_t& mruIndex = 0);
		GroupPosition* const getPositionSubGroup(const size_t& positionIndex = 0);

		void forEachAttitudeGroup(const std::function<void(GroupAttitude* const)>& doToAttitudeGroup);
		void forEachPositionGroup(const std::function<void(GroupPosition* const)>& doToPositionGroup);

		// Initialisation des sous groupes
		void initAttitudeSubGroup(const size_t& mruCount = 1);
		void initPositionSubGroup(const size_t& positionCount = 1);

	private:
		// Initialisation des attributs du groupe
		virtual void initializeAttributes();

		// Initialisation de types personnalis�s
		virtual void initializeCustomType();

		// Dimensions
		std::unique_ptr<ncDimension> m_transducerDimension;
		std::unique_ptr<ncDimension> m_positionDimension;
		std::unique_ptr<ncDimension> m_mruDimension;
		
		// Variables
		std::unique_ptr<VariableMruIds> m_mruIdsVariable;
		std::unique_ptr<VariableMruOffsetX> m_mruOffsetXVariable;
		std::unique_ptr<VariableMruOffsetY> m_mruOffsetYVariable;
		std::unique_ptr<VariableMruOffsetZ> m_mruOffsetZVariable;
		std::unique_ptr<VariableMruRotationX> m_mruRotationXVariable;
		std::unique_ptr<VariableMruRotationY> m_mruRotationYVariable;
		std::unique_ptr<VariableMruRotationZ> m_mruRotationZVariable;
		std::unique_ptr<VariablePositionIds> m_positionIdsVariable;
		std::unique_ptr<VariablePositionOffsetX> m_positionOffsetXVariable;
		std::unique_ptr<VariablePositionOffsetY> m_positionOffsetYVariable;
		std::unique_ptr<VariablePositionOffsetZ> m_positionOffsetZVariable;
		std::unique_ptr<VariableTransducerIds> m_transducerIdsVariable;
		std::unique_ptr<VariableTransducerFunction> m_transducerFunctionVariable;
		std::unique_ptr<VariableTransducerOffsetX> m_transducerOffsetXVariable;
		std::unique_ptr<VariableTransducerOffsetY> m_transducerOffsetYVariable;
		std::unique_ptr<VariableTransducerOffsetZ> m_transducerOffsetZVariable;
		std::unique_ptr<VariableTransducerRotationX> m_transducerRotationXVariable;
		std::unique_ptr<VariableTransducerRotationY> m_transducerRotationYVariable;
		std::unique_ptr<VariableTransducerRotationZ> m_transducerRotationZVariable;
		std::unique_ptr<VariableWaterLevel> m_waterLevelVariable;
		
		// Subgroups
		std::unique_ptr<ncGroup> m_attitudeGroup;
		std::vector<std::unique_ptr<GroupAttitude>> m_attitudeSubGroups;
		std::unique_ptr<ncGroup> m_positionGroup;
		std::vector<std::unique_ptr<GroupPosition>> m_positionSubGroups;

		// Accesseurs SubGroup 
		ncGroup* const getAttitudeGroup();
		ncGroup* const getPositionGroup();

	};
}

