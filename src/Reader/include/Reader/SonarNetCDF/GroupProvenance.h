#pragma once

#include "Reader/SonarNetCDF/GroupTopLevel.h"
#include "Reader/SonarNetCDF/NetCDFUtils.h"

#include <memory>

namespace sonarNetCDF {

	class VariableFileNames;

	class GroupProvenance : public UnderTopLevelGroup {

	public:
		GroupProvenance(ncGroup* const parent);
		~GroupProvenance();

		// Group name
		const static std::string NC_GROUP_NAME;

		// Param�trage du groupe
		void setConversionSoftwareName(const std::string& conversionSoftwareName);
		void setConversionSoftwareVersion(const std::string& conversionSoftwareVersion);
		void setConversionTime(const std::string& conversionTime);

		// Acc�s variables
		VariableFileNames* getFileNamesVariable() const;
		
	private:
		// Initialisation des attributs du groupe
		virtual void initializeAttributes();

		// Dimensions
		std::unique_ptr<ncDimension> m_fileNamesDimension;

		// Variables
		std::unique_ptr<VariableFileNames> m_fileNamesVariable;

		// SubGroup
	};
}