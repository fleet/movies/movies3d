#pragma once

#include "Reader/SonarNetCDF/GroupTopLevel.h"
#include "Reader/SonarNetCDF/NetCDFUtils.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

#include <memory>

namespace sonarNetCDF {

	class GroupBeam;
	class GroupGrid;

	class GroupSonar : public UnderTopLevelGroup {
		
	public:
		GroupSonar(ncGroup* const parent);
		~GroupSonar();

		// Group name
		const static std::string NC_GROUP_NAME;

		// Accesseurs attributs
		std::string getManufacturer() const;
		std::string getModel() const;
		std::string getSerialNumber() const;
		std::string getSoftwareName() const;
		std::string getSoftwareVersion() const;
		sonar::SonarType::Type getSonarType() const;

		void setManufacturer(const std::string& manufacturer);
		void setModel(const std::string& model);
		void setSerialNumber(const std::string& serialNumber);
		void setSoftwareName(const std::string& softwareName);
		void setSoftwareVersion(const std::string& softwareVersion);
		void setSonarType(const sonar::SonarType::Type& sonarType);

		// Subgroups
		size_t beamGroupsSize() const;
		std::vector<GroupBeam*> getAllBeamGroups() const;
		GroupBeam* const getBeamGroup(const size_t& beamGroupIndex);
		GroupBeam* const getBeamGroupWithTransducerName(const std::string& transducerName) const;

		std::vector<GroupGrid*> getAllGridGroups() const;
		GroupGrid* const getGridGroup(const size_t& gridGroupIndex);
		GroupGrid* const getGridGroupWithLayerTypeName(const std::string& layerTypeName);

		GroupBeam* const initBeamGroup(
			const size_t& beamCount = ncUtils::INVALID_SIZE,
			const size_t& subbeamCount = ncUtils::INVALID_SIZE,
			const size_t& frequencyCount = ncUtils::INVALID_SIZE,
			const size_t& txBeamCount = ncUtils::INVALID_SIZE
		);

		GroupGrid* const initGridGroup(
			const std::string& layerTypeName = std::string(),
			const size_t& beamCount = ncUtils::INVALID_SIZE,
			const size_t& subbeamCount = ncUtils::INVALID_SIZE,
			const size_t& frequencyCount = ncUtils::INVALID_SIZE,
			const size_t& pingAxisCount = ncUtils::INVALID_SIZE,
			const size_t& rangeAxisCount = ncUtils::INVALID_SIZE,
			const size_t& txBeamCount = ncUtils::INVALID_SIZE
		);


	private:
		GroupBeam* const initBeamGroupWithGroupIndex(
			const size_t& beamGroupIndex = ncUtils::INVALID_INDEX,
			const size_t& beamCount = ncUtils::INVALID_SIZE,
			const size_t& subbeamCount = ncUtils::INVALID_SIZE,
			const size_t& frequencyCount = ncUtils::INVALID_SIZE,
			const size_t& txBeamCount = ncUtils::INVALID_SIZE
		);

		GroupGrid* const initGridGroupWithGroupIndex(
			const size_t& gridGroupIndex = ncUtils::INVALID_INDEX,
			const std::string& layerTypeName = std::string(),
			const size_t& beamCount = ncUtils::INVALID_SIZE,
			const size_t& subbeamCount = ncUtils::INVALID_SIZE,
			const size_t& frequencyCount = ncUtils::INVALID_SIZE,
			const size_t& pingAxisCount = ncUtils::INVALID_SIZE,
			const size_t& rangeAxisCount = ncUtils::INVALID_SIZE,
			const size_t& txBeamCount = ncUtils::INVALID_SIZE
		);

		// Initialisation des attributs du groupe
		virtual void initializeAttributes();

		// Initialisation de type personnalisť
		virtual void initializeCustomType();
		
		// SubGroup
		std::map<size_t, std::unique_ptr<GroupBeam>> m_beamGroups;
		std::map<size_t, std::unique_ptr<GroupGrid>> m_gridGroups;
	};
}

