#pragma once

#include "Reader/SonarNetCDF/NetCDFUtils.h"
#include "Reader/SonarNetCDF/VersionSonarNetCDF.h"

#include <chrono>
#include <memory>

namespace sonarNetCDF {

	class GroupEnvironment;
	class GroupAnnotation;
	class GroupPlatform;
	class GroupProvenance;
	class GroupSonar;
	
	class GroupTopLevel : public ncGroup {
	public:
		GroupTopLevel(sonarNetCDF::ncFile* file);
		~GroupTopLevel();
		
		// Accesseurs attributs
		std::string getConventions() const;
		std::chrono::time_point<std::chrono::system_clock> getCreatedDate() const;
		std::string getKeywords() const;
		std::string getLicense() const;
		std::string getRights() const;
		std::string getSonarConventionAuthority() const;
		std::string getSonarConventionName() const;
		VersionSonarNetCDF getSonarConventionVersion() const;
		std::string getSummary() const;
		std::string getTitle() const;

		void setConventions(const std::string& conventions);
		void setCreatedDate(const std::chrono::time_point<std::chrono::system_clock>& createdDate);
		void setKeywords(const std::string& keywords);
		void setLicense(const std::string& license);
		void setRights(const std::string& rights);
		void setSonarConventionAuthority(const std::string& sonarConventionAuthority);
		void setSonarConventionName(const std::string& sonarConventionName);
		void setSonarConventionVersion(const std::string& sonarConventionVersion);
		void setSummary(const std::string& summary);
		void setTitle(const std::string& title);

		// Accesseurs groupes
		GroupEnvironment* getEnvironmentGroup();
		GroupAnnotation* getAnnotationGroup();
		GroupPlatform* getPlatformGroup();
		GroupProvenance* getProvenanceGroup();
		GroupSonar* getSonarGroup();

		// Initialisation des groupes
		GroupEnvironment* initEnvironmentGroup(const size_t& frequencyCount = ncUtils::INVALID_SIZE);
		GroupAnnotation* initAnnotationGroup();
		GroupPlatform* initPlatformGroup(const size_t& transducerCount = ncUtils::INVALID_SIZE, const size_t& positionCount = ncUtils::INVALID_SIZE, const size_t& mruCount = ncUtils::INVALID_SIZE);
		GroupProvenance* initProvenanceGroup();
		GroupSonar* initSonarGroup();


	private:
		// Initialisation des attributs du groupe
		virtual void initializeAttributes();

		// Subgroups
		std::unique_ptr<GroupEnvironment> m_environmentGroup;
		std::unique_ptr<GroupAnnotation> m_annotationGroup;
		std::unique_ptr<GroupPlatform> m_platformGroup;
		std::unique_ptr<GroupProvenance> m_provenanceGroup;
		std::unique_ptr<GroupSonar> m_sonarGroup;
	};

	// Classe décrivant les groupes sous le TopLevelGroup
	class UnderTopLevelGroup : public ncGroup {
	public:
		UnderTopLevelGroup(ncGroup* const parent, const std::string& name, const int& id = ncUtils::INVALID_ID);
		UnderTopLevelGroup(ncGroup* const parent, const int& id = ncUtils::INVALID_ID);

		// Récupération de la version SonarNetCDF
		VersionSonarNetCDF getVersion();

	};
}

