#pragma once

#include "netcdf.h"

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <algorithm>
#include <memory>

namespace sonarNetCDF
{
	class ncUtils {
	public:
		static const int INVALID_ID;
		static const int INVALID_SIZE;
		static const int INVALID_INDEX;

		static void logInfo(const char * format, ...);
		static void logError(const char * format, ...);

		static void setLogInfoFunction(std::function<void(const char * message, va_list args)> logInfoFunction);
		static void setLogErrorFunction(std::function<void(const char * message, va_list args)> logErrorFunction);
		
		template <typename T>		
		class ncType {
		public:
			static nc_type getType();
		};

		static nc_type getTypeIdByName(const int& groupId, const std::string & typeName);

	private:
		static std::function<void(const char * message, va_list args)> logInfoFunction;
		static std::function<void(const char * message, va_list args)> logErrorFunction;

	};

	class ncFile {
	public:
		ncFile();
		ncFile(const std::string& fileName);
		~ncFile();

		bool isOpen() const;
		const int& getId() const;
		const bool& isEditable() const;

		void setFileName(const std::string& fileName);
		const std::string& getFileName();

		bool open(const int& mode = NC_NOWRITE);
		void close();

		void sync() const;

		static std::unique_ptr<ncFile> createFile(const std::string& path, const int& mode = NC_CLOBBER);
		static std::unique_ptr<ncFile> createFile(const char* path, const int& mode = NC_CLOBBER);

		static std::unique_ptr<ncFile> openFile(const std::string& path, const int& mode = NC_NOWRITE);
		static std::unique_ptr<ncFile> openFile(const char* path, const int& mode = NC_NOWRITE);

	private:
		int m_id;
		std::string m_fileName;
		bool m_editable;
	};

	class ncObject
	{
	public:
		ncObject(ncObject* parent, const std::string& name, const int& id = ncUtils::INVALID_ID);
		~ncObject();

		ncObject& operator=(const ncObject &other);

		const int& getId() const;
		int getParentId() const;
		const std::string& getName() const;

		int getFileId() const;

	protected:
		virtual bool isValid() const;
		virtual void initialize() = 0;
		
		void setFile(ncFile* file);
		bool isEditable() const;

		ncObject* getParent() const;

		int m_id;
		std::string m_name;

	private:
		ncObject* m_parent;
		ncFile* m_file;
	};

	class ncTypeOwner {
	public:
		ncTypeOwner();

		template<typename U>
		const nc_type& createEnumType(const std::string& typeName) {
			auto foundType = m_types.find(typeName);
			if (foundType != m_types.end()) {
				return foundType->second;
			}

			m_types[typeName] = NC_NAT;
			if (getTypeGroupId() != ncUtils::INVALID_ID) {
				int ret = nc_def_enum(getTypeGroupId(), sonarNetCDF::ncUtils::ncType<U>::getType(), typeName.c_str(), &m_types[typeName]);
				
				if (ret == NC_EPERM) {
					ncUtils::logInfo("Can't add type %s, in read only mode", typeName.c_str());
				}
				else if (ret != NC_NOERR)
					ncUtils::logError("%s: nc_def_enum failed for type %s, error : %s (%d)", __func__, typeName.c_str(), nc_strerror(ret), ret);
			}

			return m_types[typeName];
		}

		template<typename U>
		void addEnum(const std::string& typeName, const std::string& identifierName, const U& value) {
			if (m_types.find(typeName) != m_types.end()) return;
			addEnum(m_types[typeName], identifierName, value);
		}

		template<typename U>
		void addEnum(const nc_type& type, const std::string& identifierName, const U& value) {
			if (getTypeGroupId() == ncUtils::INVALID_ID
				|| type <= NC_MAX_ATOMIC_TYPE // Ce n'est pas un type personnalisé
				) return; 

			int ret = nc_insert_enum(getTypeGroupId(), type, identifierName.c_str(), &value);

			switch (ret) {
			case NC_NOERR:
				break;

			case NC_ENAMEINUSE:
			case NC_ETYPDEFINED:
			case NC_EPERM:
				ncUtils::logInfo("Can't add enum type %s : %s (%d)", identifierName.c_str(), nc_strerror(ret), ret);
				break;

			default:
				ncUtils::logError("%s: nc_insert_enum failed for enum type %s, error : %s (%d)", __func__, identifierName.c_str(), nc_strerror(ret), ret);
			}
		}

		template<typename U>
		const nc_type& createArrayType(const std::string& typeName) {
			auto foundType = m_types.find(typeName);
			if (foundType != m_types.end()) {
				return foundType->second;
			}

			m_types[typeName] = NC_NAT;			
			if (getTypeGroupId() != ncUtils::INVALID_ID) {
				int ret = nc_def_vlen(getTypeGroupId(), typeName.c_str(), sonarNetCDF::ncUtils::ncType<U>::getType(), &m_types[typeName]);
				
				if (ret == NC_EPERM) {
					ncUtils::logInfo("Can't create type %s, in read only mode", typeName.c_str());
				}
				else if (ret != NC_NOERR) {
					ncUtils::logError("%s: nc_def_enum failed for type %s, error : %s (%d)", __func__, typeName.c_str(), nc_strerror(ret), ret);
				}
			}

			return m_types[typeName];
		}

	protected:
		// Renvoie le nc_type
		nc_type& getNcType(const std::string& typeName);

		// Initialisation de type personnalisé
		virtual void initializeCustomType() {};

		// Récupération des identifiants permettant de trouver le type personnalisé
		virtual int getTypeGroupId() const = 0;

		// Récupération des types personnalisés
		void retrieveCustomTypes();

	private:
		// Types
		std::map<std::string, nc_type> m_types;
	};
	
	class ncAttributeOwner {
	
	public:

		// Récupération de la valeur de l'attribut portant le nom demandé
		template<typename U>
		U getAttributeValue(const std::string& attributeName) const;
		
		// Création d'un attribut avec le nom et la valeur donnée (aucune modification si l'attribut existe déjà)
		template<typename U>
		inline void createAttribute(const std::string& attributeName, U value);

		template<typename U>
		inline void createAttribute(const std::string& attributeName, U* values);

		template<typename U>
		inline void createAttribute(const std::string& attributeName, const std::vector<U>& values);

		// Création d'un attribut sans valeur
		void createEmptyAttribute(const std::string& attributeName);

		// Attribution d'une valeur à l'attribut portant le nom donné
		template<typename U>
		inline void setAttributeValue(const std::string& attributeName, U value);

		template<typename U>
		inline void setAttributeValues(const std::string& attributeName, U* values);

		template<typename U>
		inline void setAttributeValues(const std::string& attributeName, const std::vector<U>& values);

	protected:
		// Initialisation des attributs
		virtual void initializeAttributes() {};

		// Récupération des identifiants permettant de trouver l'attribut
		virtual int getAttributesGroupId() const = 0;
		virtual int getAttributesVarId() const;

		// Liste d'attente d'initialisation d'attributs
		std::vector<std::function<void()>> m_waitingInitialisations;

	private:
		// Détermination attribut global ou appartenant à une variable
		int getVarId() const;

		// Méthode d'écriture par défaut de l'attribut
		template<typename U>
		inline void _setAttributeValues(const std::string& attributeName, const std::vector<U>& values);
	};
	
	template<typename U>
	inline U ncAttributeOwner::getAttributeValue(const std::string& attributeName) const
	{
		U value = U();
		
		if (getAttributesGroupId() == ncUtils::INVALID_ID) return value;

		int ret = nc_get_att(getAttributesGroupId(), getVarId(), attributeName.c_str(), &value);
		if (ret == NC_ENOTATT) {
			ncUtils::logInfo("Attribute %s doesn't exists", attributeName.c_str());
			return value;
		}
		else if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_get_att failed for attribute %s, error : %s (%d)", __func__, attributeName.c_str(), nc_strerror(ret), ret);
		}

		return value;
	}
		
	template<> // Spécification STRING
	inline std::string ncAttributeOwner::getAttributeValue<std::string>(const std::string& attributeName) const
	{
		if (getAttributesGroupId() == ncUtils::INVALID_ID) return std::string();

		size_t strLen = 0;
		int ret = nc_inq_attlen(getAttributesGroupId(), getVarId(), attributeName.c_str(), &strLen);
		if (ret == NC_ENOTATT) {
			ncUtils::logInfo("Attribute %s doesn't exists", attributeName.c_str());
			return std::string();
		}
		if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_inq_attlen failed for attribute %s, error : %s (%d)", __func__, attributeName.c_str(), nc_strerror(ret), ret);
		}

		std::string value(strLen + 1, '\0');
		ret = nc_get_att_text(getAttributesGroupId(), getVarId(), attributeName.c_str(), (char*)value.data());
		if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_get_att_text failed for attribute %s, error : %s (%d)", __func__, attributeName.c_str(), nc_strerror(ret), ret);
		}

		return value;
	}
	
	template<> // Spécification Char *
	inline void ncAttributeOwner::createAttribute(const std::string& attributeName, const char* value) {
		const auto string = std::string(value);
		createAttribute(attributeName, std::vector<std::string>(1, string));
	}	
	
	// Création d'un attribut avec le nom et la valeur donnée (aucune modification si l'attribut existe déjà)
	template<typename U>
	inline void ncAttributeOwner::createAttribute(const std::string& attributeName, U value) {
		createAttribute(attributeName, std::vector< std::remove_const_t<U> >(1, value));
	}
	
	template<typename U>
	inline void ncAttributeOwner::createAttribute(const std::string& attributeName, U* values) {
		std::vector< std::remove_const_t<U> > vector(values, values + sizeof(values) / sizeof(U));
		createAttribute(attributeName, vector);
	}
	
	template<typename U>
	inline void ncAttributeOwner::createAttribute(const std::string& attributeName, const std::vector<U>& values) {
		// Test si l'attribut existe déjà, si c'est le cas il ne sera pas créé
		int ret = nc_inq_attlen(
			getAttributesGroupId(),
			getVarId(),
			attributeName.c_str(),
			nullptr
		);
	
		if (ret == NC_ENOTATT) {
			ncUtils::logInfo("Create attribute %s", attributeName.c_str());
			setAttributeValues(attributeName, values);
		}
		else if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_inq_attlen failed for attribute %s, error : %s (%d)", __func__, attributeName.c_str(), nc_strerror(ret), ret);
		}
		else {
			ncUtils::logInfo("Attribute %s already exists", attributeName.c_str());
		}
	}
	
	template<typename U>
	inline void ncAttributeOwner::setAttributeValue(const std::string& attributeName, U value) {
		setAttributeValues(attributeName, std::vector< std::remove_const_t<U> >(1, value));
	}
	
	template<typename U>
	inline void ncAttributeOwner::setAttributeValues(const std::string& attributeName, U* values) {
		std::vector< std::remove_const_t<U> > vector(values, values + sizeof(values) / sizeof(U));
		setAttributeValues(attributeName, vector);
	}
	
	template<> // Spécification Char *
	inline void ncAttributeOwner::setAttributeValue(const std::string& attributeName, const char* value) {
		const auto string = std::string(value);
		setAttributeValue(attributeName, std::vector<std::string>(1, string));
	}
	
	template<typename U>
	inline void ncAttributeOwner::setAttributeValues(const std::string& attributeName, const std::vector<U>& values) {
		auto set = [=]() {
			if (getAttributesGroupId() != ncUtils::INVALID_ID)
				_setAttributeValues(attributeName, values); 
		};
	
		if (getAttributesVarId() == ncUtils::INVALID_ID) {
			m_waitingInitialisations.push_back(set);
		}
		else {
			set();
		}
	}
	
	template<typename U>
	inline void ncAttributeOwner::_setAttributeValues(const std::string& attributeName, const std::vector<U>& values) {
		int ret = nc_put_att(
			getAttributesGroupId(), 
			getVarId(),
			attributeName.c_str(), 
			sonarNetCDF::ncUtils::ncType<U>::getType(), 
			values.size(),
			values.size() == 1 ? &(values.front()) : values.data()
		);

		if (ret == NC_EPERM) {
			ncUtils::logInfo("Can't write attribute %s, in read only mode", attributeName.c_str());
		}
		else if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_put_att failed for attribute %s, error : %s (%d)", __func__, attributeName.c_str(), nc_strerror(ret), ret);
		}
	}

	template<> // Spécification String
	inline void ncAttributeOwner::_setAttributeValues(const std::string& attributeName, const std::vector<std::string>& values) {
		if (values.size() == 1) {
			const auto& stringToPut = values.front();

			int ret = nc_put_att_text(
				getAttributesGroupId(),
				getVarId(),
				attributeName.c_str(),
				stringToPut.size(),
				stringToPut.c_str()
			);
			if (ret == NC_EPERM) {
				ncUtils::logInfo("Can't write attribute %s, in read only mode", attributeName.c_str());
			}
			else if (ret != NC_NOERR) {
				ncUtils::logError("%s: nc_put_att_text failed for attribute %s, error : %s (%d)", __func__, attributeName.c_str(), nc_strerror(ret), ret);
			}
		}
		else {
			std::vector<const char*> charValues(values.size());
			std::transform(values.begin(), values.end(), charValues.begin(), [&](const std::string& str) {
				return str.c_str();
			});
			int ret = nc_put_att_string(
				getAttributesGroupId(),
				getVarId(),
				attributeName.c_str(),
				charValues.size(),
				charValues.data()
			);
			if (ret == NC_EPERM) {
				ncUtils::logInfo("Can't write attribute %s, in read only mode", attributeName.c_str());
			}
			else if (ret != NC_NOERR) {
				ncUtils::logError("%s: nc_put_att_string failed for attribute %s, error : %s (%d)", __func__, attributeName.c_str(), nc_strerror(ret), ret);
			}
		}
	}
	
	class ncDimension : public ncObject {
	public:
		ncDimension(ncObject* parent, const std::string& name, const size_t& size = ncUtils::INVALID_SIZE);
		~ncDimension();

		ncDimension& operator=(const ncDimension &other);

		void setUnlimited();
		bool isUnlimited() const;

		size_t incrementDimensionSize(const int = 1);

		size_t getSize() const;
		void setSize(size_t size);

	protected:
		virtual bool isValid() const;
		virtual void initialize();

	private:
		// tentative de création de la dimension (si elle n'existe pas déjà)
		void createDimension();

		// La dimension est illimitée ?
		bool m_isUnlimited;

		// Taille de la dimension
		size_t m_size;
	};

	template<typename T>
	class ncVariable : public ncObject, public ncAttributeOwner {

	public:
		ncVariable(ncObject* parent, const std::string& name, const std::vector<ncDimension*>& dimensions);
		ncVariable(ncObject* parent, const std::string& name, ncDimension* const dimension = nullptr);
		~ncVariable();

		// Accesseurs
		virtual const std::vector<ncDimension*>& getDimensions() const;
		size_t getNbDimensions() const;

		// Tentative de création de la variable (si elle n'existe pas déjà)
		void createVariable();

		// Type personnalisé
		void setTypeName(const std::string& typeName = std::string());

		// Lecture
		// 0D
		T getScalarValue() const;

		// 1D
		T getValue(const size_t& index) const;

		std::vector<T> getAll1DValue() const;

		size_t getIndex(const T& value) const;

		// ND
		T getIndexedValue(const std::vector<size_t>& indexes) const;

		// Ecriture
		// 0D
		void setScalarValue(const T& value);

		// 1D
		void set1DValues(const std::vector<T>& values);
		void set1DValue(const size_t& index, const T& value);
		void fill1DValues(const T value);
		size_t append1DValue(const T& value);
		size_t findOrAppend1DValue(const T& value);

		// nD
		void setIndexedValues(const std::vector<size_t>& indexes, const std::vector<size_t>& counts, const std::vector<T>& values);
		void setIndexedValues(const std::map<std::vector<size_t>, T>& valuesByIndexes);
		void setIndexedValue(const std::vector<size_t>& indexes, const T& value);

		void setFillValue(const T& fillValue);
		T getFillValue() const;
		
		void forEachDataFromDimension(const std::string& dimensionName, std::function<void(size_t dimensionIndex, T value)> doForEach);
		void forEachDataFromDimension(ncDimension* const dimension, std::function<void(size_t dimensionIndex, T value)> doForEach);

	protected:
		virtual bool isValid() const override;

		// Initialisation de la valeur par défaut de remplissage
		virtual void initializeFillValue() {};

	private:
		// Initialisation de la variable
		void initialize();

		// Pré écriture procédure (définition dimension, création variable)
		void preWrite1DData(const size_t& dimensionSize = 1);

		// Transformation des données avant écriture dans le NetCDF
		void write1DData(const std::vector<T>& values, const size_t& startIndex = 0);

		template<typename U>
		void writeNDData(const std::vector<size_t>& startInDimensions, const std::vector<size_t>& countInDimensions, const std::vector<U>& values);

		template<typename U>
		void writeNDData(const std::vector<size_t>& startInDimensions, const std::vector<size_t>& countInDimensions, const std::vector<std::vector<U>>& values);

		// Transformation des données après lecture du NetCDF
		void read1DData(std::vector<T>& values, const size_t& size, const size_t& startIndex = 0) const;

		template<typename U>
		void readNDData(const std::vector<size_t>& startInDimensions, const std::vector<size_t>& countInDimensions, std::vector<U>& values) const;

		template<typename U>
		void readNDData(const std::vector<size_t>& startInDimensions, const std::vector<size_t>& countInDimensions, std::vector<std::vector<U>>& values) const;

		// Retourne l'index d'une dimension
		size_t getDimensionIndex(ncDimension* const dimension) const;

		// Agrégation des identifiants des dimensions
		std::vector<int> getDimensionsId() const;

		// Itération sur chaque donnée d'une dimension donnée
		void forEachDataFromDimension(size_t dimensionIndex, std::function<void(size_t dimensionIndex, T value)> doForEach);
		
		// Dimension du jeu de données
		std::vector<ncDimension*> m_dimensions;

		// Type
		nc_type m_type;

		// ------ Méthodes liées à la gestion des attributs
		// Identifiant du groupe contenant la variable
		int getAttributesGroupId() const override;

		// Identifiant de la variable
		int getAttributesVarId() const override;
	};

	class ncGroup : public ncObject, public ncAttributeOwner, public ncTypeOwner
	{
	public:
		ncGroup(ncGroup* const parent, const std::string& name, const int& id = ncUtils::INVALID_ID);
		ncGroup(ncGroup* const parent, const int& id = ncUtils::INVALID_ID);
		~ncGroup();

		static std::string getGroupName(const int& groupId);

	protected:
		// Lien vers le parent
		ncGroup* const getParentGroup() const;

		// Lien vers le groupe racine
		ncGroup* const getRootGroup();

		// Recherche des identifiants de tous les sous groupes
		std::vector<int> searchAllSubgroupsIds();


	private:
		// Initialisation du groupe
		void initialize() override;

		// tentative de création du groupe (si il n'existe pas déjà)
		void createGroup();

		// ------ Méthodes liées à la gestion des attributs
		// Identifiant du groupe contenant la variable
		int getAttributesGroupId() const override;

		// ------ Méthodes liées à la gestion d'un type personnalisé
		// Récupération des identifiants permettant de trouver le type personnalisé
		int getTypeGroupId() const override;
	};
}