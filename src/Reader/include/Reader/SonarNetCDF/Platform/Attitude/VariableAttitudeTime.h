#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	// Time from attitude sensor
	class VariableAttitudeTime : public VariableSingleDimension<uint64_t> {
	public:
		VariableAttitudeTime(ncObject* parent, ncDimension* timeDimension);
		~VariableAttitudeTime();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();
	};
}

