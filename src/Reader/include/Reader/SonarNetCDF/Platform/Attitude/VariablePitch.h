#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableAttitudeTime;

	// Platform pitch.Positive values indicate a bow-up pitch.
	class VariablePitch : public VariableSingleVariable<float, uint64_t> {
	public:
		VariablePitch(ncObject* parent, VariableAttitudeTime* timeVariable);
		~VariablePitch();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

