#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableAttitudeTime;

	// Platform pitch rate
	class VariablePitchRate : public VariableSingleVariable<float, uint64_t> {
	public:
		VariablePitchRate(ncObject* parent, VariableAttitudeTime* timeVariable);
		~VariablePitchRate();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

