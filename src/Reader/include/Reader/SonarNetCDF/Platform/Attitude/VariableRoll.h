#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableAttitudeTime;

	// Platform roll.Positive values indicate a roll to starboard.
	class VariableRoll : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableRoll(ncObject* parent, VariableAttitudeTime* timeVariable);
		~VariableRoll();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

