#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableAttitudeTime;

	// Platform roll rate
	class VariableRollRate : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableRollRate(ncObject* parent, VariableAttitudeTime* timeVariable);
		~VariableRollRate();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

