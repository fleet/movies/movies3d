#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableAttitudeTime;

	// Distance from the nominal water level to the actual water level measured along the _z_axis of the platform coordinate system 
	// (positive values are when the actual water level is below the nominal water level). 
	// For ships and similar, this is called heave, but the concept applies equally well to underwater vehicle depth.
	// This offset is applied at the position given by (MRU_offset_x, MRU_offset_y, MRU_offset_z).
	class VariableVerticalOffset : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableVerticalOffset(ncObject* parent, VariableAttitudeTime* timeVariable);
		~VariableVerticalOffset();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

