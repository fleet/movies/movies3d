#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableAttitudeTime;

	// Platform heading. Measured clockwise from north.
	class VariableYaw : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableYaw(ncObject* parent, VariableAttitudeTime* timeVariable);
		~VariableYaw();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

