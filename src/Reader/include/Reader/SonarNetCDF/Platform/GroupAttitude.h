#pragma once

#include "Reader/SonarNetCDF/GroupTopLevel.h"
#include "Reader/SonarNetCDF/NetCDFUtils.h"

#include <memory>

namespace sonarNetCDF {

	class VariableAttitudeTime;
	class VariableYaw;
	class VariableYawRate;
	class VariablePitch;
	class VariablePitchRate;
	class VariableRoll;
	class VariableRollRate;
	class VariableVerticalOffset;

	class GroupAttitude : public UnderTopLevelGroup {

	public:
		GroupAttitude(ncGroup* const parent, const std::string& name);
		~GroupAttitude();

		// Group name
		const static std::string NC_GROUP_NAME;

		// Acc�s variables
		VariableAttitudeTime* getTimeVariable() const;
		VariableYaw* getYawVariable() const;
		VariableYawRate* getYawRateVariable() const;
		VariablePitch* getPitchVariable() const;
		VariablePitchRate* getPitchRateVariable() const;
		VariableRoll* getRollVariable() const;
		VariableRollRate* getRollRateVariable() const;
		VariableVerticalOffset* getVerticalOffsetVariable() const;

	private:
		// Initialisation des attributs du groupe
		virtual void initializeAttributes();

		// Dimensions
		std::unique_ptr<ncDimension> m_timeDimension;

		// Variables
		std::unique_ptr<VariableAttitudeTime> m_timeVariable;
		std::unique_ptr<VariableYaw> m_yawVariable;
		std::unique_ptr<VariableYawRate> m_yawRateVariable;
		std::unique_ptr<VariablePitch> m_pitchVariable;
		std::unique_ptr<VariablePitchRate> m_pitchRateVariable;
		std::unique_ptr<VariableRoll> m_rollVariable;
		std::unique_ptr<VariableRollRate> m_rollRateVariable;
		std::unique_ptr<VariableVerticalOffset> m_verticalOffsetVariable;
	};
}

