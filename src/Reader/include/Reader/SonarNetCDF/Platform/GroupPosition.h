#pragma once

#include "Reader/SonarNetCDF/GroupTopLevel.h"
#include "Reader/SonarNetCDF/NetCDFUtils.h"

#include <memory>

namespace sonarNetCDF {

	class VariablePositionTime;
	class VariableLatitude;
	class VariableLongitude;
	class VariableHeading;
	class VariableCourseOverGround;
	class VariableSpeedOverGround;
	class VariableSpeedRelative;
	class VariableHeightAboveReferenceEllipsoid;
	class VariableAltitude;
	class VariableDistance;

	class GroupPosition : public UnderTopLevelGroup {

	public:
		GroupPosition(ncGroup* const parent, const std::string& name);
		~GroupPosition();

		// Group name
		const static std::string NC_GROUP_NAME;

		// Acc�s variables
		VariablePositionTime* getTimeVariable() const;
		VariableLatitude* getLatitudeVariable() const;
		VariableLongitude* getLongitudeVariable() const;
		VariableHeading* getHeadingVariable() const;
		VariableCourseOverGround* getCourseOverGroundVariable() const;
		VariableSpeedOverGround* getSpeedOverGroundVariable() const;
		VariableSpeedRelative* getSpeedRelativeVariable() const;
		VariableHeightAboveReferenceEllipsoid* getHeightAboveReferenceEllipsoidVariable() const;
		VariableAltitude* getAltitudeVariable() const;
		VariableDistance* getDistanceVariable() const;

	private:
		// Initialisation des attributs du groupe
		virtual void initializeAttributes();

		// Dimensions
		std::unique_ptr<ncDimension> m_timeDimension;

		// Variables
		std::unique_ptr<VariablePositionTime> m_timeVariable;
		std::unique_ptr<VariableLatitude> m_latitudeVariable;
		std::unique_ptr<VariableLongitude> m_longitudeVariable;
		std::unique_ptr<VariableHeading> m_headingVariable;
		std::unique_ptr<VariableCourseOverGround> m_courseOverGroundVariable;
		std::unique_ptr<VariableSpeedOverGround> m_speedOverGroundVariable;
		std::unique_ptr<VariableSpeedRelative> m_speedRelativeVariable;
		std::unique_ptr<VariableHeightAboveReferenceEllipsoid> m_heightAboveReferenceEllipsoidVariable;
		std::unique_ptr<VariableAltitude> m_altitudeVariable;
		std::unique_ptr<VariableDistance> m_distanceVariable;
	};
}

