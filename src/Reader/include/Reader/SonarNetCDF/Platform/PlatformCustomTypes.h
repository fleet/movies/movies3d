#pragma once

#include <vector>

namespace sonarNetCDF {
	namespace platform {

		// Transducer function - transmit only, receive only or both (monostatic)
		class TransducerFunctionType {
		public:

			static const std::string getTypeName() {
				return "transducer_type_t";
			}

			enum class Type : uint8_t {
				RECEIVE_ONLY = 0,
				TRANSMIT_ONLY = 1,
				MONOSTATIC = 3
			};

			static const std::vector<Type> all() {
				return { Type::RECEIVE_ONLY, Type::TRANSMIT_ONLY, Type::MONOSTATIC };
			}

			static std::string getName(Type type) {
				switch (type) {
				case Type::RECEIVE_ONLY:
					return "receive_only";
				case Type::TRANSMIT_ONLY:
					return "transmit_only";
				case Type::MONOSTATIC:
					return "monostatic";
				default:
					return std::string();
				}
			}

			static const uint8_t getValue(Type type) {
				return (uint8_t)type;
			}

			static const Type getType(uint8_t value) {
				for (Type type : all())
					if (getValue(type) == value)
						return type;

				return Type::MONOSTATIC; /* Valeur par d�faut ? */
			}
		};
	}
}

