#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionTime;

	// Altitude is the (geometric) height of the platform reference point above the reference WGS-84 geoid. The geoid is similar to mean sea level.
	class VariableAltitude : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableAltitude(ncObject* parent, VariablePositionTime* timeVariable);
		~VariableAltitude();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

