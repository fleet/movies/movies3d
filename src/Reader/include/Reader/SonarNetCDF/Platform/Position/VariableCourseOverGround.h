#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionTime;

	// A platform course is the cardinal direction along which the platform is to be steered
	class VariableCourseOverGround : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableCourseOverGround(ncObject* parent, VariablePositionTime* timeVariable);
		~VariableCourseOverGround();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

