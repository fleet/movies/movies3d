#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionTime;

	// Distance travelled by the platform from an arbitrary location.
	class VariableDistance : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableDistance(ncObject* parent, VariablePositionTime* timeVariable);
		~VariableDistance();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

