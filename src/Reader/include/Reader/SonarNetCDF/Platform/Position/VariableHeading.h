#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionTime;

	// Heading refers to the direction a platform is pointing. 
	// This may or may not be the direction that the platform actually travels, which is known as its course or track. 
	// Any difference between course and heading is due to the motion of the underlying medium, the air or water, or other effects like skidding or slipping. 
	// Heading is typically based on compass directions, so 0� (or 360�) indicates a direction toward true North, 90� indicates a direction toward true East, 180� is true South, and 270� is true West.
	class VariableHeading : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableHeading(ncObject* parent, VariablePositionTime* timeVariable);
		~VariableHeading();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

