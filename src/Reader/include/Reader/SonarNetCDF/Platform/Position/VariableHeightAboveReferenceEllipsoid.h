#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionTime;

	// Height of the platform reference point above the WGS - 84 ellipsoid
	class VariableHeightAboveReferenceEllipsoid : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableHeightAboveReferenceEllipsoid(ncObject* parent, VariablePositionTime* timeVariable);
		~VariableHeightAboveReferenceEllipsoid();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

