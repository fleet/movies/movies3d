#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionTime;

	// Latitude of the platform reference point in WGS-84 reference system
	class VariableLatitude : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableLatitude(ncObject* parent, VariablePositionTime* timeVariable);
		~VariableLatitude();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

