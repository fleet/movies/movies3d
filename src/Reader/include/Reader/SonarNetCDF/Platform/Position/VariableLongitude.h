#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionTime;

	// Longitude of the platform reference point in WGS-84 reference system
	class VariableLongitude : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableLongitude(ncObject* parent, VariablePositionTime* timeVariable);
		~VariableLongitude();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

