#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	// Time from position sensor
	class VariablePositionTime : public VariableSingleDimension<uint64_t> {
	public:
		VariablePositionTime(ncObject* parent, ncDimension* timeDimension);
		~VariablePositionTime();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();
	};
}

