#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionTime;

	// Speed is the magnitude of velocity. The platform speed with respect to ground is relative to the solid Earth beneath it, i.e. the sea floor for a ship.
	class VariableSpeedOverGround : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableSpeedOverGround(ncObject* parent, VariablePositionTime* timeVariable);
		~VariableSpeedOverGround();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

