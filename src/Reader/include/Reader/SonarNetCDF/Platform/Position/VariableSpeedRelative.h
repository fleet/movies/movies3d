#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionTime;

	// Platform speed relative to water.
	class VariableSpeedRelative : public VariableSingleVariable<float, uint64_t> {
	public:
		VariableSpeedRelative(ncObject* parent, VariablePositionTime* timeVariable);
		~VariableSpeedRelative();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

