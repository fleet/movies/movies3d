#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	// MRU serial number or identification name.Must be a valid netCDF4 group name.
	class VariableMruIds : public VariableSingleDimension<std::string> {
	public:
		VariableMruIds(ncObject* parent, ncDimension* mruDimension);
		~VariableMruIds();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:

		// Initialisation des attributs
		void initializeAttributes() {};
	};
}

