#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableMruIds;

	// x-axis component of the vector from the platform coordinate system origin to the motion reference unit origin.
	class VariableMruOffsetX: public VariableSingleVariable<float, std::string> {
	public:
		VariableMruOffsetX(ncObject* parent, VariableMruIds* mruIdVariable);
		~VariableMruOffsetX();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

