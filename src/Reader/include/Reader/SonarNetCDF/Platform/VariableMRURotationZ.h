#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableMruIds;

	// Extrinsic angular rotation about the z-axis from the platform zero angle to the MRU zero angle.
	class VariableMruRotationZ : public VariableSingleVariable<float, std::string> {
	public:
		VariableMruRotationZ(ncObject* parent, VariableMruIds* mruIdVariable);
		~VariableMruRotationZ();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

