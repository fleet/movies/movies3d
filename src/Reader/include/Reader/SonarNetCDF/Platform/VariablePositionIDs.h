#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	// Position sensor serial number or identification name.Must be a valid netCDF4 group name.
	class VariablePositionIds : public VariableSingleDimension<std::string> {
	public:
		VariablePositionIds(ncObject* parent, ncDimension* positionDimension);
		~VariablePositionIds();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:

		// Initialisation des attributs
		void initializeAttributes() {};
	};
}

