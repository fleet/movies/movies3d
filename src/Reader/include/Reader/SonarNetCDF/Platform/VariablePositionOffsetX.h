#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionIds;

	// Distance from the platform coordinate system origin to the latitude / longitude position origin along the x-axis.
	class VariablePositionOffsetX: public VariableSingleVariable<float, std::string> {
	public:
		VariablePositionOffsetX(ncObject* parent, VariablePositionIds* positionIdVariable);
		~VariablePositionOffsetX();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

