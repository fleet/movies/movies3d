#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariablePositionIds;

	// Distance from the platform coordinate system origin to the latitude / longitude position origin along the y-axis.
	class VariablePositionOffsetY : public VariableSingleVariable<float, std::string> {
	public:
		VariablePositionOffsetY(ncObject* parent, VariablePositionIds* positionIdVariable);
		~VariablePositionOffsetY();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

