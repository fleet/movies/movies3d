#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Platform/PlatformCustomTypes.h"

namespace sonarNetCDF {

	class VariableTransducerIds;

	// The transducer function (that is, transmit_only, receive_only, or monostatic)
	class VariableTransducerFunction : public VariableSingleVariable<uint8_t, std::string> {
	public:

		VariableTransducerFunction(ncObject* parent, VariableTransducerIds* transducerIdVariable);
		~VariableTransducerFunction();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

		// Accesseurs
		platform::TransducerFunctionType::Type getFunctionTypeAtIndex(const size_t& transducerIndex) const;
		platform::TransducerFunctionType::Type getFunctionTypeAt(const std::string& transducerId) const;

		void setFunctionTypeAtIndex(const size_t& transducerIndex, platform::TransducerFunctionType::Type transducerFunctionType);
		void setFunctionTypeAt(const std::string& transducerId, platform::TransducerFunctionType::Type transducerFunctionType);

	private:
		// Initialisation des attributs
		void initializeAttributes();
	};
}

