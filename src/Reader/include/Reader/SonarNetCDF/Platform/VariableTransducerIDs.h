#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	// Transducer serial number or identification name
	class VariableTransducerIds : public VariableSingleDimension<std::string> {
	public:
		VariableTransducerIds(ncObject* parent, ncDimension* transducerDimension);
		~VariableTransducerIds();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:

		// Initialisation des attributs
		void initializeAttributes() {};
	};
}

