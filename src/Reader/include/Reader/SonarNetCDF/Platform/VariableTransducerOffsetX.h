#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableTransducerIds;

	// Distance from the platform coordinate system origin to the transducer along the x-axis.
	class VariableTransducerOffsetX: public VariableSingleVariable<float, std::string> {
	public:
		VariableTransducerOffsetX(ncObject* parent, VariableTransducerIds* transducerIdVariable);
		~VariableTransducerOffsetX();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

