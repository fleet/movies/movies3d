#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableTransducerIds;

	// Distance from the platform coordinate system origin to the transducer along the y-axis.
	class VariableTransducerOffsetY : public VariableSingleVariable<float, std::string> {
	public:
		VariableTransducerOffsetY(ncObject* parent, VariableTransducerIds* transducerIdVariable);
		~VariableTransducerOffsetY();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

