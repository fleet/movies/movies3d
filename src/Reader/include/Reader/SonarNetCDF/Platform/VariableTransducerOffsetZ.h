#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableTransducerIds;

	// Distance from the platform coordinate system origin to the transducer along the z-axis.
	class VariableTransducerOffsetZ : public VariableSingleVariable<float, std::string> {
	public:
		VariableTransducerOffsetZ(ncObject* parent, VariableTransducerIds* transducerIdVariable);
		~VariableTransducerOffsetZ();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

