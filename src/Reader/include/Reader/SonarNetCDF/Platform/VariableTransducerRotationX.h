#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableTransducerIds;

	// Extrinsic angular rotation about the x-axis from the transducer zero angle to the coordinate system origin zero angle.
	class VariableTransducerRotationX : public VariableSingleVariable<float, std::string> {
	public:
		VariableTransducerRotationX(ncObject* parent, VariableTransducerIds* transducerIdVariable);
		~VariableTransducerRotationX();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

