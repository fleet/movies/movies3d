#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class VariableTransducerIds;

	// Extrinsic angular rotation about the z-axis from the transducer zero angle to the coordinate system origin zero angle.
	class VariableTransducerRotationZ : public VariableSingleVariable<float, std::string> {
	public:
		VariableTransducerRotationZ(ncObject* parent, VariableTransducerIds* transducerIdVariable);
		~VariableTransducerRotationZ();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

