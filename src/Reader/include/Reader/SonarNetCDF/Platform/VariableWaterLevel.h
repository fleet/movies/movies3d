#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	// Distance from the origin of the platform coordinate system to the nominal water level measured along the z-axis of the platform coordinate system (positive values are below the origin).
	// The distance between the nominal and actual water level is provided by vertical_offset.
	class VariableWaterLevel : public VariableScalar<float> {
	public:
		VariableWaterLevel(ncObject* parent);
		~VariableWaterLevel();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();

		// Initialisation de la valeur par d�faut de remplissage
		void initializeFillValue();
	};
}

