#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	// Vector of datafile names that were used to generate the data in this SONAR - netCDF4 file.
	class VariableFileNames : public VariableSingleDimension<std::string> {
	public:
		VariableFileNames(ncObject* parent, ncDimension* filenameDimension);
		~VariableFileNames();

		// Nom de la variable
		static const std::string VARIABLE_NAME;

	private:
		// Initialisation des attributs
		void initializeAttributes();
	};
}

