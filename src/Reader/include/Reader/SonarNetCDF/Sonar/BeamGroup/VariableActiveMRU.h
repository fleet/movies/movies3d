#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Indicate the index of the MRU sensor used at the time of the ping to compute the platform attitude.
		class VariableActiveMRU : public VariableSingleVariable<int, uint64_t> {
		public:
			VariableActiveMRU(ncObject* parent, VariablePingTime* timeVariable);
			~VariableActiveMRU();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

