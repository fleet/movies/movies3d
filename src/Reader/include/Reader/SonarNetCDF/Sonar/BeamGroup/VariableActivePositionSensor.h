#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Indicate the index of the position sensor used at the time of the ping to compute the platform position.
		class VariableActivePositionSensor : public VariableSingleVariable<int, uint64_t> {
		public:
			VariableActivePositionSensor(ncObject* parent, VariablePingTime* timeVariable);
			~VariableActivePositionSensor();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

