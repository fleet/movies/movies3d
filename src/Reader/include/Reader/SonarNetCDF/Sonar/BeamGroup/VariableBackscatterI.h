#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

#include <memory>

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// Imaginary part of backscatter measurements. 
		// Each element in the 3D matrix is a variable length vector (of type sample_t) that contains the samples for that beam, ping time, and optionally subbeam.
		class IVariableBackscatterI {
		public:
			virtual ~IVariableBackscatterI() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			virtual void initializeAttributes() = 0;
		};

		template<typename T>
		class VariableBackscatterI : public VariableTripleVariable<T, uint64_t, std::string, size_t>, public IVariableBackscatterI {
		public:
			VariableBackscatterI(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, PseudoVariableSingleDimension* subbeamVariable);

		private:
			// Initialisation des attributs
			void initializeAttributes() override;
		};

		class VariableBackscatterIFactory {
		public:
			static std::unique_ptr<IVariableBackscatterI> build(const size_t& numberOfSubbeam, ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, PseudoVariableSingleDimension* subbeamVariable);
		};
	}
}

