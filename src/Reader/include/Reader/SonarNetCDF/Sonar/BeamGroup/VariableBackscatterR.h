#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

#include <memory>

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// Real part or amplitude or power of backscatter measurements.
		// Each element in the 3D matrix is a variable length vector(of type sample_t) that contains the samples for that beam, ping time, and optionally subbeam.
		class IVariableBackscatterR {
		public:
			virtual ~IVariableBackscatterR() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Attribut d'�chelle
			virtual float getScaleFactor() const = 0;
			virtual void setScaleFactor(const float& scaleFactor) = 0;

		private:
			// Initialisation des attributs
			virtual void initializeAttributes() = 0;
		};

		template<typename T>
		class VariableBackscatterR : public VariableTripleVariable<T, uint64_t, std::string, size_t>, public IVariableBackscatterR {
		public:
			VariableBackscatterR(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, PseudoVariableSingleDimension* subbeamVariable);

			// Attribut d'�chelle
			float getScaleFactor() const override;
			void setScaleFactor(const float& scaleFactor) override;

		private:
			// Initialisation des attributs
			void initializeAttributes() override;
		};

		class VariableBackscatterRFactory {
		public:
			static std::unique_ptr<IVariableBackscatterR> build(const size_t& numberOfSubbeam, ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, PseudoVariableSingleDimension* subbeamVariable);
		};
	}
}

