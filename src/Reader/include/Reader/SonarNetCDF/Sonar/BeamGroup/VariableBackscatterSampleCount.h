#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// The number of samples in each subbeam/beam/ping in the backscatter_r and backscatter_i variables. 
		// This value is not essential, but software that reads the backscatter_r and backscatter_i variables can use it to significantly improve data loading times.
		class VariableBackscatterSampleCount : public VariableTripleVariable<int, uint64_t, std::string, size_t> {
		public:
			VariableBackscatterSampleCount(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, PseudoVariableSingleDimension* subbeamVariable);

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

