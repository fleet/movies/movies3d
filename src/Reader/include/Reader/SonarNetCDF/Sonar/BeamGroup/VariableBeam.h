#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	namespace beamGroup {
		// Beam name (or number or identification code).
		class VariableBeam : public VariableSingleDimension<std::string> {
		public:
			VariableBeam(ncObject* parent, ncDimension* beamDimension);
			~VariableBeam();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Nom du transducteur associ�
			void setTransducerName(const std::string& transducerName);
			std::string getTransducerName() const;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

