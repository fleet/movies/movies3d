#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Indicates whether or not sonar beams have been compensated for platform motion.
		class VariableBeamStabilisation : private VariableSingleVariable<uint8_t, uint64_t> {
		public:
			VariableBeamStabilisation(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariableBeamStabilisation();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Accesseurs
			sonar::BeamStabilisation::Type getModeAtIndex(const size_t& pingTimeIndex) const;
			sonar::BeamStabilisation::Type getModeAt(const uint64_t& pingTime) const;

			void setModeAtIndex(const size_t& pingTimeIndex, sonar::BeamStabilisation::Type stabilisationMode);
			void setModeAt(const uint64_t& pingTime, sonar::BeamStabilisation::Type stabilisationMode);

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

