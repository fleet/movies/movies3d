#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {

	namespace beamGroup {
		// Type of split - aperture beam(or not).
		class VariableBeamType : private VariableScalar<uint8_t> {
		public:
			VariableBeamType(ncObject* parent);
			~VariableBeamType();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Accesseurs
			sonar::BeamType::Type getType() const;
			void setType(sonar::BeamType::Type type);

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

