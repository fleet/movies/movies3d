#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// One-way beam width at half power down in the horizontal direction of the receive beam.
		class VariableBeamWidthReceiveMajor : public VariableDoubleVariable<float, uint64_t, std::string> {
		public:
			VariableBeamWidthReceiveMajor(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);
			~VariableBeamWidthReceiveMajor();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

