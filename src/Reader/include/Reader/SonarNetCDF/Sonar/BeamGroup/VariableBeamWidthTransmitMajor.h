#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// One-way beam width at half power down in the horizontal direction of the transmit beam.
		class VariableBeamWidthTransmitMajor : public VariableDoubleVariable<float, uint64_t, size_t> {
		public:
			VariableBeamWidthTransmitMajor(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableBeamWidthTransmitMajor();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

