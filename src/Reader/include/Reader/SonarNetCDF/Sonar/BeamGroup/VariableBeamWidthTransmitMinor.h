#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// One-way beam width at half power down in the vertical direction of the transmit beam.
		class VariableBeamWidthTransmitMinor : public VariableDoubleVariable<float, uint64_t, size_t> {
		public:
			VariableBeamWidthTransmitMinor(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableBeamWidthTransmitMinor();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

