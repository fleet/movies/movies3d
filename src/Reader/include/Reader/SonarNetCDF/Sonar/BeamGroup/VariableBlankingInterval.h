#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;	

		// Amount of time during reception where samples are discarded.The number of discarded sample is given by blanking_interval*sample_interval.
		class VariableBlankingInterval : public VariableDoubleVariable<float, uint64_t, std::string> {
		public:
			VariableBlankingInterval(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);
			~VariableBlankingInterval();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

