#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariableFrequency;

		// Beam name (or number or identification code).
		class VariableCalibratedFrequency : public VariableSingleVariable<float, float> {
		public:
			VariableCalibratedFrequency(ncObject* parent, VariableFrequency* frequencyVariable);

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

