#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// Electrical phase angle of the incoming echoes at time ping_time relative to the direction of each beam. 
		// Only required if the beam_type variable for this ping_time is set to split_aperture_angles.
		class VariableEchoAngleMajor : public VariableDoubleVariable<sonar::AngleType::type, uint64_t, std::string> {
		public:
			VariableEchoAngleMajor(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);
			~VariableEchoAngleMajor();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

