#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariableBeam;

		// Scaling factor to convert electrical arrival angles into physical angles. Only required if beam_type is not set to single.
		class VariableEchoAngleMinorSensitivity : public VariableSingleVariable<float, std::string> {
		public:
			VariableEchoAngleMinorSensitivity(ncObject* parent, VariableBeam* beamVariable);
			~VariableEchoAngleMinorSensitivity();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

