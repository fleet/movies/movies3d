#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// Equivalent beam angle.
		class VariableEquivalentBeamAngle : public VariableDoubleVariable<float, uint64_t, std::string> {
		public:
			VariableEquivalentBeamAngle(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);
			~VariableEquivalentBeamAngle();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

