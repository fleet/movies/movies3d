#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	namespace beamGroup {
		// Frequencies for which transducer gain values have been estimated during the calibration exercise.
		class VariableFrequency : public VariableSingleDimension<float> {
		public:
			VariableFrequency(ncObject* parent, ncDimension* frequencyDimension);

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

