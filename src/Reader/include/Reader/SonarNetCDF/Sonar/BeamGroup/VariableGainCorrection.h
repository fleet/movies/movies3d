#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// Gain correction. This parameter is set from a calibration exercise. Necessary for type 2 conversion equation.
		class VariableGainCorrection : public VariableDoubleVariable<float, uint64_t, std::string> {
		public:
			VariableGainCorrection(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);
			~VariableGainCorrection();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

