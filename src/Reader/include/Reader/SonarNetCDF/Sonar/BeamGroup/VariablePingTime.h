#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	namespace beamGroup {
		// Timestamp at which each ping occurred.
		class VariablePingTime : public VariableSingleDimension<uint64_t> {
		public:
			VariablePingTime(ncObject* parent, ncDimension* timeDimension);
			~VariablePingTime();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

