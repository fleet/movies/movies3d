#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Heading of the platform at time of the ping.
		class VariablePlatformHeading : public VariableSingleVariable<float, uint64_t> {
		public:
			VariablePlatformHeading(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariablePlatformHeading();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

