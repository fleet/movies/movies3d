#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Latitude of the platform reference point in WGS-84 reference system at the time of the ping.
		class VariablePlatformLatitude : public VariableSingleVariable<double, uint64_t> {
		public:
			VariablePlatformLatitude(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariablePlatformLatitude();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

