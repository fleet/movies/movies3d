#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Longitude of the platform reference point in WGS - 84 reference system at the time of the ping.
		class VariablePlatformLongitude : public VariableSingleVariable<double, uint64_t> {
		public:
			VariablePlatformLongitude(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariablePlatformLongitude();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

