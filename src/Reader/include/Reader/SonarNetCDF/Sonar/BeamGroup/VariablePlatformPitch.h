#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Platform pitch at the time of the ping.
		class VariablePlatformPitch : public VariableSingleVariable<float, uint64_t> {
		public:
			VariablePlatformPitch(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariablePlatformPitch();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

