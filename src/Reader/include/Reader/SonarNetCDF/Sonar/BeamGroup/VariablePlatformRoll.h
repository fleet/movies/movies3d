#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Platform roll at the time of the ping.
		class VariablePlatformRoll : public VariableSingleVariable<float, uint64_t> {
		public:
			VariablePlatformRoll(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariablePlatformRoll();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

