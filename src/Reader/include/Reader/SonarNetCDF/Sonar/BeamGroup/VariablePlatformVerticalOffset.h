#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Distance from the platform reference point to the water line (distance are positives downwards). 
		// For ships and similar, this is called heave and is added to the dynamic draught at the time of the ping but the concept applies equally well to underwater vehicle depth.
		class VariablePlatformVerticalOffset : public VariableSingleVariable<float, uint64_t> {
		public:
			VariablePlatformVerticalOffset(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariablePlatformVerticalOffset();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

