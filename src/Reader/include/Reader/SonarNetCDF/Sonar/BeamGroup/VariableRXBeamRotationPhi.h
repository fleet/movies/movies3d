#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// The intrinsic z�y��x� clockwise rotation about the x-axis of the platform coordinate system needed to give the receive beam coordinate system. 
		// For ships and similar, if installation angles are close to zero, this rotation usually matches the beam pointing angle in the across track direction.
		class VariableRxBeamRotationPhi : public VariableDoubleVariable<float, uint64_t, std::string> {
		public:
			VariableRxBeamRotationPhi(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);
			~VariableRxBeamRotationPhi();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

