#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// The intrinsic z�y��x� clockwise rotation about the y-axis of the platform coordinate system needed to give the receive beam coordinate system. 
		// For ships and similar, if installation angles are close to zero, this rotation usually matches the beam pointing angle in the along track direction.
		class VariableRxBeamRotationTheta : public VariableDoubleVariable<float, uint64_t, std::string> {
		public:
			VariableRxBeamRotationTheta(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);
			~VariableRxBeamRotationTheta();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

