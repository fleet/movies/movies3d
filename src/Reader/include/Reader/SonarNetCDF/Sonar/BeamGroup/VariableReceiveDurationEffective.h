#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Effective duration of the received pulse.This is the duration of the square pulse containing the same energy as the actual receive pulse.
		// This parameter is either theoretical or comes from a calibration exercise and adjusts the nominal duration of the transmitted pulse to the measured one.
		// During calibration it is obtained by integrating the energy of the received signal on the calibration target normalised by its maximum energy.
		// Necessary for type 1, 2, 3 and 4 conversion equations.
		class VariableReceiveDurationEffective : public VariableDoubleVariable<float, uint64_t, size_t> {
		public:
			VariableReceiveDurationEffective(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableReceiveDurationEffective();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

