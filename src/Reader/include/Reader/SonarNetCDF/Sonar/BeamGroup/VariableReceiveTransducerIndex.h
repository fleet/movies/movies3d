#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariableBeam;

		// Receiving or monostatic transducer index associated with the given beam
		class VariableReceiveTransducerIndex : public VariableSingleVariable<int, std::string> {
		public:
			VariableReceiveTransducerIndex(ncObject* parent, VariableBeam* beamVariable);
			~VariableReceiveTransducerIndex();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

