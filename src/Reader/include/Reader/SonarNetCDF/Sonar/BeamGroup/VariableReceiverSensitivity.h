#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// Sensitivity of the sonar receiver for the current ping. Necessary for type 2 conversion equation.
		class VariableReceiverSensitivity : public VariableDoubleVariable<float, uint64_t, std::string> {
		public:
			VariableReceiverSensitivity(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);
			~VariableReceiverSensitivity();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

