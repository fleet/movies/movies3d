#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Time between individual samples along a beam.Common for all beams in a ping.
		class VariableSampleInterval : public VariableSingleVariable<float, uint64_t> {
		public:
			VariableSampleInterval(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariableSampleInterval();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

