#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Sound speed at transducer depth at the time of the ping
		class VariableSoundSpeedAtTransducer : public VariableSingleVariable<float, uint64_t> {
		public:
			VariableSoundSpeedAtTransducer(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariableSoundSpeedAtTransducer();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

