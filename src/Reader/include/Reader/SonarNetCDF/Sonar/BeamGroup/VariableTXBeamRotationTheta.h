#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// The intrinsic z�y��x� clockwise about the y-axis of the platform coordinate system needed to give the transmit beam coordinate system. 
		// For ships and similar, if installation angles are close to zero, this rotation usually matches the beam pointing angle in the along track direction.
		class VariableTxBeamRotationTheta : public VariableDoubleVariable<float, uint64_t, size_t> {
		public:
			VariableTxBeamRotationTheta(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTxBeamRotationTheta();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

