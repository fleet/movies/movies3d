#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Tx transducer depth below waterline at time of the ping (distance are positives downwards). 
		// This variable can be recomputed in most cases by applying lever arm and rotation matrix taking into account for roll and pitch, platform_vertical_offset but can also take into account for drop keel position	
		class VariableTxTransducerDepth : public VariableSingleVariable<float, uint64_t> {
		public:
			VariableTxTransducerDepth(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariableTxTransducerDepth();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

