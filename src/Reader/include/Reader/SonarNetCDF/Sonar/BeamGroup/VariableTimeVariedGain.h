#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

#include <memory>

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// 	Time-varied gain(TVG) used for each ping.Should contain TVG coefficient vectors.Necessary for type 2 conversion equations.
		class IVariableTimeVariedGain {
		public:
			virtual ~IVariableTimeVariedGain() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			virtual void initializeAttributes() = 0;
		};

		template<typename T>
		class VariableTimeVariedGain : public VariableSingleVariable<T, uint64_t>, public IVariableTimeVariedGain {
		public:
			VariableTimeVariedGain(ncObject* parent, VariablePingTime* pingTimeVariable);

		private:
			// Initialisation des attributs
			void initializeAttributes() override;
		};

		class VariableTimeVariedGainFactory {
		public:
			static std::unique_ptr<IVariableTimeVariedGain> build(const size_t& numberOfSubbeam, ncObject* parent, VariablePingTime* pingTimeVariable);
		};
	}
}

