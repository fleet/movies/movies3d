#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	class GroupBeam;

	namespace beamGroup {
		class VariablePingTime;
		class VariableBeam;
		class VariableFrequency;

		// Gain of the transducer beam. This is the parameter that is set from a calibration exercise. Necessary for conversion equation type 1, 3 and 4.
		class IVariableTransducerGain {
		public:
			virtual ~IVariableTransducerGain() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Gestion des valeurs
			virtual float getValueAtIndexes(const std::vector<size_t>& indexes) const = 0;
			virtual void setValueAtIndexes(const std::vector<size_t>& indexes, const float& value) = 0;

		private:
			// Initialisation des attributs
			virtual void initializeAttributes() = 0;
		};

		class VariableTransducerGain_1 : public VariableDoubleVariable<float, uint64_t, std::string>, public IVariableTransducerGain {
		public:
			VariableTransducerGain_1(GroupBeam* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);

			// Gestion des valeurs
			virtual float getValueAtIndexes(const std::vector<size_t>& indexes) const override;
			virtual void setValueAtIndexes(const std::vector<size_t>& indexes, const float& value) override;

		private:
			// Initialisation des attributs
			virtual void initializeAttributes() override;
		};

		class VariableTransducerGain_2 : public VariableTripleVariable<float, uint64_t, std::string, float>, public IVariableTransducerGain {
		public:
			VariableTransducerGain_2(GroupBeam* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, VariableFrequency* frequencyVariable);

			// Gestion des valeurs
			virtual float getValueAtIndexes(const std::vector<size_t>& indexes) const override;
			virtual void setValueAtIndexes(const std::vector<size_t>& indexes, const float& value) override;

		private:
			// Initialisation des attributs
			virtual void initializeAttributes() override;
		};

		class VariableTransducerGainFactory {
		public:
			static std::unique_ptr<IVariableTransducerGain> build(
				GroupBeam* parent,
				VariablePingTime* pingTimeVariable,
				VariableBeam* beamVariable,
				VariableFrequency* frequencyVariable
			);
		};
	}
}

