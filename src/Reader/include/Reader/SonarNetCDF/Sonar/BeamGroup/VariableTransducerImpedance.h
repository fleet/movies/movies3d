#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Impedance of the transducer. This is the impedance of the load over which the transceiver measures voltage on the transducer subbeam. Necessary for conversion equation type 4.
		class VariableTransducerImpedance : public VariableDoubleVariable<float, uint64_t, size_t> {
		public:
			VariableTransducerImpedance(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* subbeamVariable);

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

