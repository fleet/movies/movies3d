#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Estimated bandwidth of the transmitted pulse. For CW pulses, this is a function of the pulse duration and frequency. 
		// For FM pulses, this will be close to the difference between transmit_frequency_start and transmit_frequency_stop.
		class VariableTransmitBandwith : public VariableDoubleVariable<float, uint64_t, size_t> {
		public:
			VariableTransmitBandwith(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTransmitBandwith();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

