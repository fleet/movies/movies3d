#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// Transmit beam index associated with the given beam
		class VariableTransmitBeamIndex : public VariableDoubleVariable<int, uint64_t, std::string> {
		public:
			VariableTransmitBeamIndex(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);
			~VariableTransmitBeamIndex();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

