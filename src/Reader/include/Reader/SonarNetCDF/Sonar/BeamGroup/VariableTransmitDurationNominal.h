#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Nominal duration of the transmit pulse. This is not the effective pulse duration.
		class VariableTransmitDurationNominal : public VariableDoubleVariable<float, uint64_t, size_t> {
		public:
			VariableTransmitDurationNominal(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTransmitDurationNominal();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

