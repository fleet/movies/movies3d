#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Imaginary part of the model of the transmit pulse. 
		// The exact shape of the theoretical transmit pulse is given at the same sampling rate as the backscatter measurements. 
		// The shape reflects both the weighting of the pulse and the filters that have been applied. 
		// The pulse shape is used for matched filtering of complex samples in type 4 conversion equations.
		class VariableTransmitPulseModelI : public VariableDoubleVariable<sonar::PulseType::type, uint64_t, size_t> {
		public:
			VariableTransmitPulseModelI(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTransmitPulseModelI();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

