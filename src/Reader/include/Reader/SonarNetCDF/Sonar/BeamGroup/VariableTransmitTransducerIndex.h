#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;
		class VariableBeam;

		// Transmitting or monostatic transducer index associated with the given receiving beam
		class VariableTransmitTransducerIndex : public VariableDoubleVariable<int, uint64_t, std::string> {
		public:
			VariableTransmitTransducerIndex(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable);
			~VariableTransmitTransducerIndex();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

