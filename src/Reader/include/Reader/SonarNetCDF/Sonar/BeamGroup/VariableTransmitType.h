#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Type of transmit pulse.
		class VariableTransmitType : private VariableDoubleVariable<uint8_t, uint64_t, size_t> {
		public:
			VariableTransmitType(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTransmitType();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Accesseurs
			sonar::TransmitType::Type getTypeAtIndex(const size_t& pingTimeIndex, const size_t& txBeamIndex) const;
			sonar::TransmitType::Type getTypeAt(const uint64_t& pingTime, const size_t& txBeamIndex) const;

			void setTypeAtIndex(const size_t& pingTimeIndex, const size_t& txBeamIndex, sonar::TransmitType::Type transmitType);
			void setTypeAt(const uint64_t& pingTime, const size_t& txBeamIndex, sonar::TransmitType::Type transmitType);

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

