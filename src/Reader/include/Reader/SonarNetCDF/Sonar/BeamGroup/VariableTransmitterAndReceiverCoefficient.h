#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Sum of transmit source level (dB re 1 �Pa at 1 m), voltage sensitivity (dB re 1 V/�Pa), and system gain (dB). Necessary for type 6 conversion equations.
		class VariableTransmitterAndReceiverCoefficient : public VariableSingleVariable<float, uint64_t> {
		public:
			VariableTransmitterAndReceiverCoefficient(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariableTransmitterAndReceiverCoefficient();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

