#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {
	namespace beamGroup {

		class VariablePingTime;

		// Vertical translation vector at the time of the ping matching the distance from the water line to the chart data reference (typically Lowest Astronomical Tide or Mean Sea Level). 
		// This variable is the vector that contains the tide and allows for the positioning of samples in an absolute reference system.
		class VariableWaterlineToChartDatum : public VariableSingleVariable<float, uint64_t> {
		public:
			VariableWaterlineToChartDatum(ncObject* parent, VariablePingTime* pingTimeVariable);
			~VariableWaterlineToChartDatum();

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Accesseurs attributs
			sonar::VerticalCoordinateReferenceSystem::System getVerticalCoordinateReferenceSystem() const;
			void setVerticalCoordinateReferenceSystem(const sonar::VerticalCoordinateReferenceSystem::System& system);

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

