#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {

	namespace gridGroup {

		class VariableFrequency;

		// Standard definition of backscatter unit.
		class VariableBackscatterType : private VariableSingleVariable<uint8_t, float> {
		public:
			VariableBackscatterType(ncObject* parent, VariableFrequency* frequencyVariable);
			~VariableBackscatterType() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Accesseurs
			sonar::BackscatterType::Type getType(const float& frequency) const;
			
			void setType(const float& frequency, sonar::BackscatterType::Type type);
			void setTypeForAllFrequencies(sonar::BackscatterType::Type type);

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

