#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	namespace gridGroup {

		class VariableFrequency;

		// Beam name used for a given frequency.
		class VariableBeamReference : public VariableSingleVariable<std::string, float> {
		public:
			VariableBeamReference(ncObject* parent, VariableFrequency* frequencyVariable);
			~VariableBeamReference() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

