#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		class VariableBeam;

		// Depth of the cell to the water line (distance are positives downwards).
		class VariableCellDepth : public VariableDoubleVariable<double, size_t, std::string> {
		public:
			VariableCellDepth(ncObject* parent, PseudoVariableSingleDimension* rangeAxisVariable, VariableBeam* beamVariable);
			~VariableCellDepth() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

