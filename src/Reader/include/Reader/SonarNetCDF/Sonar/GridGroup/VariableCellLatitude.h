#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		class VariableBeam;

		// Mean latitude of the echoes contributing to the cell in WGS-84 reference system.
		class VariableCellLatitude : public VariableTripleVariable<double, size_t, size_t, std::string> {
		public:
			VariableCellLatitude(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* rangeAxisVariable, VariableBeam* beamVariable);
			~VariableCellLatitude() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

