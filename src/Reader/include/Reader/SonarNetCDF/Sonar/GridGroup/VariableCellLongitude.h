#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		class VariableBeam;

		// Mean longitude of the echoes contributing to the cell in WGS-84 reference system.
		class VariableCellLongitude : public VariableTripleVariable<double, size_t, size_t, std::string> {
		public:
			VariableCellLongitude(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* rangeAxisVariable, VariableBeam* beamVariable);
			~VariableCellLongitude() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

