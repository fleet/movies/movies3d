#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	namespace gridGroup {
		// Mean timestamp of the pings contributing to the cell.
		class VariableCellPingTime : public VariableDoubleVariable<uint64_t, size_t, size_t> {
		public:
			VariableCellPingTime(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableCellPingTime() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

