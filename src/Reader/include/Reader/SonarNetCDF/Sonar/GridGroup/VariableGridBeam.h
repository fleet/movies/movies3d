#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	namespace gridGroup {
		// Beam name (or number or identification code).
		class VariableBeam : public VariableSingleDimension<std::string> {
		public:
			VariableBeam(ncObject* parent, ncDimension* beamDimension);
			~VariableBeam() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

