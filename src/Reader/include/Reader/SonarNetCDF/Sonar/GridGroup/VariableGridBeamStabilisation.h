#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// Indicates whether or not sonar beams have been compensated for platform motion.
		class VariableBeamStabilisation : private VariableSingleVariable<uint8_t, size_t> {
		public:
			VariableBeamStabilisation(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable);
			~VariableBeamStabilisation() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Accesseurs
			sonar::BeamStabilisation::Type getModeAt(const size_t& pingAxisIndex) const;
			void setModeAt(const size_t& pingAxisIndex, sonar::BeamStabilisation::Type stabilisationMode);

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

