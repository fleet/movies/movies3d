#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {

	namespace gridGroup {

		class VariableBeam;

		// Type of split - aperture beam(or not).
		class VariableBeamType : private VariableSingleVariable<uint8_t, std::string> {
		public:
			VariableBeamType(ncObject* parent, VariableBeam* beamVariable);
			~VariableBeamType() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Accesseurs
			sonar::BeamType::Type getTypeAt(const std::string& beamName) const;
			void setTypeAt(const std::string& beamName, sonar::BeamType::Type type);

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

