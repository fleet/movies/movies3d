#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		class VariableBeam;

		// One-way beam width at half power down in the vertical direction of the receive beam.
		class VariableBeamWidthReceiveMinor : public VariableDoubleVariable<float, size_t, std::string> {
		public:
			VariableBeamWidthReceiveMinor(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable);
			~VariableBeamWidthReceiveMinor() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

