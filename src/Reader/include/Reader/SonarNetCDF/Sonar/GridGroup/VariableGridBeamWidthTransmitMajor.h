#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// One-way beam width at half power down in the horizontal direction of the transmit beam.
		class VariableBeamWidthTransmitMajor : public VariableDoubleVariable<float, size_t, size_t> {
		public:
			VariableBeamWidthTransmitMajor(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableBeamWidthTransmitMajor() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

