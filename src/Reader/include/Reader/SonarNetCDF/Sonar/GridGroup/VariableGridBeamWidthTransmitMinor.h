#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// One-way beam width at half power down in the vertical direction of the transmit beam.
		class VariableBeamWidthTransmitMinor : public VariableDoubleVariable<float, size_t, size_t> {
		public:
			VariableBeamWidthTransmitMinor(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableBeamWidthTransmitMinor() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

