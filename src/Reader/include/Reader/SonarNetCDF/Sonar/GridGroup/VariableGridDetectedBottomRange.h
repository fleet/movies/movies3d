#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		class VariableBeam;

		// Range from the transducer face where the bottom detection criteria were encountered for the amplitude or the phase of the backscattered echoes. 
		// The range of the bottom at index bottom_index with a monostatic transducer and if a constant sound speed is applied is given by 
		// detected_bottom_range= sound_speed_at_transducer*(blanking_interval+bottom_index*sample_interval - sample_time_offset)/2.
		class VariableDetectedBottomRange : public VariableDoubleVariable<float, size_t, std::string> {
		public:
			VariableDetectedBottomRange(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable);
			~VariableDetectedBottomRange() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();

			// Initialisation de la valeur par d�faut de remplissage
			void initializeFillValue();
		};
	}
}

