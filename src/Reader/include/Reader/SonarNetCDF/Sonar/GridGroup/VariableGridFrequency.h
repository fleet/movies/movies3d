#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	namespace gridGroup {
		// Frequency.
		class VariableFrequency : public VariableSingleDimension<float> {
		public:
			VariableFrequency(ncObject* parent, ncDimension* frequencyDimension);

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

