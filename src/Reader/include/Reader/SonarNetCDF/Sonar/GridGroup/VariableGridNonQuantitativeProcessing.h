#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// Settings of any processing that is applied prior to recording backscatter data that may prevent the calculation of calibrated backscatter. A value of 0 always indicates no such processing.
		class VariableNonQuantitativeProcessing : public VariableSingleVariable<short, size_t> {
		public:
			VariableNonQuantitativeProcessing(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable);
			~VariableNonQuantitativeProcessing() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Accesseurs attributs
			const std::string getFlagMeanings();
			const std::vector<std::string> getFlagMeaningsList();

			void setFlagMeanings(const std::string& flagMeanings);
			void setFlagMeaningsList(const std::vector<std::string>& flagMeaningsList);

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

