#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		class VariableBeam;

		// The intrinsic z�y��x� clockwise about the z-axis of the platform coordinate system needed to give the receive beam coordinate system. For most cases this angle is set to zero.
		class VariableRxBeamRotationPsi : public VariableDoubleVariable<float, size_t, std::string> {
		public:
			VariableRxBeamRotationPsi(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable);
			~VariableRxBeamRotationPsi() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

