#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		class VariableBeam;

		// The intrinsic z�y��x� clockwise rotation about the y-axis of the platform coordinate system needed to give the receive beam coordinate system. 
		// For ships and similar, if installation angles are close to zero, this rotation usually matches the beam pointing angle in the along track direction (also called tilt angle).
		class VariableRxBeamRotationTheta : public VariableDoubleVariable<float, size_t, std::string> {
		public:
			VariableRxBeamRotationTheta(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable);
			~VariableRxBeamRotationTheta() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

