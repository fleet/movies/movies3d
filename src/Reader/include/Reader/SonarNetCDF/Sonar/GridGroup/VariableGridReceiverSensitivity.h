#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		class VariableBeam;

		// Sensitivity of the sonar receiver for the current ping. Necessary for type 2 conversion equation.
		class VariableReceiverSensitivity : public VariableDoubleVariable<float, size_t, std::string> {
		public:
			VariableReceiverSensitivity(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable);
			~VariableReceiverSensitivity() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

