#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// Time offset applied to sample time-stamps and intended for applying a range correction (e.g. as caused by signal processing delays). 
		// Positive values reduce the calculated range to a sample. 
		// The range of a given sample at index sample_index and if a constant sound speed is applied is given by 
		// range= sound_speed_at_transducer*(blanking_interval+sample_index*sample_interval - sample_time_offset)/2
		class VariableSampleTimeOffset : public VariableDoubleVariable<float, size_t, size_t> {
		public:
			VariableSampleTimeOffset(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableSampleTimeOffset() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

