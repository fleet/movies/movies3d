#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// Sound speed at transducer depth at the time of the ping
		class VariableSoundSpeedAtTransducer : public VariableSingleVariable<float, size_t> {
		public:
			VariableSoundSpeedAtTransducer(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable);
			~VariableSoundSpeedAtTransducer() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

