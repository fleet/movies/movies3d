#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// The intrinsic z�y��x� clockwise rotation about the x-axis of the platform coordinate system needed to give the transmit beam coordinate system. 
		// For ships and similar, if installation angles are close to zero, this rotation usually matches the beam pointing angle in the across track direction.
		class VariableTxBeamRotationPhi : public VariableDoubleVariable<float, size_t, size_t> {
		public:
			VariableTxBeamRotationPhi(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTxBeamRotationPhi() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

