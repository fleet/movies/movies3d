#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// The intrinsic z�y��x� clockwise about the z-axis of the platform coordinate system needed to give the transmit beam coordinate system. For most cases this angle is set to zero.
		class VariableTxBeamRotationPsi : public VariableDoubleVariable<float, size_t, size_t> {
		public:
			VariableTxBeamRotationPsi(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTxBeamRotationPsi() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

