#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// The intrinsic z�y��x� clockwise about the y-axis of the platform coordinate system needed to give the transmit beam coordinate system. 
		// For ships and similar, if installation angles are close to zero, this rotation usually matches the beam pointing angle in the along track direction (also called tilt angle).
		class VariableTxBeamRotationTheta : public VariableDoubleVariable<float, size_t, size_t> {
		public:
			VariableTxBeamRotationTheta(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTxBeamRotationTheta() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

