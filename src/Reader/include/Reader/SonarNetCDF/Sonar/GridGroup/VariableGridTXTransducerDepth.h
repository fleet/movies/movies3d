#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// Tx transducer depth below waterline at time of the cell (distance are positives downwards).
		class VariableTxTransducerDepth : public VariableSingleVariable<float, size_t> {
		public:
			VariableTxTransducerDepth(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable);
			~VariableTxTransducerDepth() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

