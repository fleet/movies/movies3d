#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {

	namespace gridGroup {

		class VariableFrequency;

		// Gain of the transducer beam. This is the parameter that is set from a calibration exercise. Necessary for conversion equation type 1.
		class VariableTransducerGain : public VariableDoubleVariable<float, size_t, float> {
		public:
			VariableTransducerGain(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableFrequency* frequencyVariable);
			~VariableTransducerGain() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Définition de toutes les gains pour un ping
			void setAllGainsForPing(const size_t& pingIndex, const std::vector<float>& gains);

		private:
			// Initialisation des attributs
			virtual void initializeAttributes() override;
		};
	}
}

