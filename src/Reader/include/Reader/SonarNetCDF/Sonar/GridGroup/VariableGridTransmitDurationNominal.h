#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// Nominal duration of the transmit pulse. This is not the effective pulse duration.
		class VariableTransmitDurationNominal : public VariableDoubleVariable<float, size_t, size_t> {
		public:
			VariableTransmitDurationNominal(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTransmitDurationNominal() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

