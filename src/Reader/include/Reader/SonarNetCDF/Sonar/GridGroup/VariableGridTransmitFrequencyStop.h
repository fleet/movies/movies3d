#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// Frequency at the end of the transmit pulse.The beam dimension can be omitted, in which case the value apples to all beams in the ping.
		class VariableTransmitFrequencyStop : public VariableDoubleVariable<float, size_t, size_t> {
		public:
			VariableTransmitFrequencyStop(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTransmitFrequencyStop() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

