#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

namespace sonarNetCDF {
	namespace gridGroup {

		// Electrical transmit power used for the ping. Necessary for type 1 conversion equations
		class VariableTransmitPower : public VariableDoubleVariable<float, size_t, size_t> {
		public:
			VariableTransmitPower(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable);
			~VariableTransmitPower() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

