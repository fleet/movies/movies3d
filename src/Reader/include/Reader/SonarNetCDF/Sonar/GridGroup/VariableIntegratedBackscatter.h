#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

#include <memory>

namespace sonarNetCDF {
	namespace gridGroup {

		class VariableFrequency;

		class VariableIntegratedBackscatter : public VariableTripleVariable<float, size_t, size_t, float> {
		public:
			VariableIntegratedBackscatter(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* rangeAxisVariable, VariableFrequency* frequencyVariable);
			
			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

