#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {

	namespace gridGroup {
		// Reference for regridding data in ping axis.
		class VariablePingAxisIntervalType : private VariableScalar<uint8_t> {
		public:
			VariablePingAxisIntervalType(ncObject* parent);
			~VariablePingAxisIntervalType() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Accesseurs
			sonar::PingAxisIntervalType::Type getType() const;
			void setType(sonar::PingAxisIntervalType::Type type);

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

