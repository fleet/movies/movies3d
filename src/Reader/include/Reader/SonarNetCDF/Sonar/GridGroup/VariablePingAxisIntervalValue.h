#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {

	namespace gridGroup {
		// Value for data ping axis interval according to its specified type.
		class VariablePingAxisIntervalValue : public VariableScalar<float> {
		public:
			VariablePingAxisIntervalValue(ncObject* parent);
			~VariablePingAxisIntervalValue() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

