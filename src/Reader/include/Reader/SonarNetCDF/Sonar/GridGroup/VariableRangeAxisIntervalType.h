#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {

	namespace gridGroup {
		// Reference for regridding the data in range or depth axis.
		class VariableRangeAxisIntervalType : private VariableScalar<uint8_t> {
		public:
			VariableRangeAxisIntervalType(ncObject* parent);
			~VariableRangeAxisIntervalType() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

			// Accesseurs
			sonar::RangeAxisIntervalType::Type getType() const;
			void setType(sonar::RangeAxisIntervalType::Type type);

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

