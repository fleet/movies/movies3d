#pragma once

#include "Reader/SonarNetCDF/VariableAccessWrapper.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

namespace sonarNetCDF {

	namespace gridGroup {
		// Value for data range axis interval according to its specified type.
		class VariableRangeAxisIntervalValue : public VariableScalar<float> {
		public:
			VariableRangeAxisIntervalValue(ncObject* parent);
			~VariableRangeAxisIntervalValue() = default;

			// Nom de la variable
			static const std::string VARIABLE_NAME;

		private:
			// Initialisation des attributs
			void initializeAttributes();
		};
	}
}

