#pragma once

#include "Reader/SonarNetCDF/GroupTopLevel.h"
#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

#include <memory>

namespace sonarNetCDF {

	class GroupSonar;

	namespace beamGroup {
		class VariableBeam;
		class VariablePingTime;
		class VariableFrequency;

		class IVariableBackscatterI;
		class IVariableBackscatterR;
		class VariableBackscatterSampleCount;

		class VariableEchoAngleMajor;
		class VariableEchoAngleMinor;
		class VariableEchoAngleMajorSensitivity;
		class VariableEchoAngleMinorSensitivity;

		class VariableBeamWidthReceiveMajor;
		class VariableBeamWidthReceiveMinor;
		class VariableBeamWidthTransmitMajor;
		class VariableBeamWidthTransmitMinor;

		class VariableRxBeamRotationPhi;
		class VariableRxBeamRotationTheta;
		class VariableRxBeamRotationPsi;

		class VariableTxBeamRotationPhi;
		class VariableTxBeamRotationTheta;
		class VariableTxBeamRotationPsi;

		class VariableBeamStabilisation;
		class VariableBeamType;
		class VariableEquivalentBeamAngle;
		class VariableGainCorrection;
		class VariableNonQuantitativeProcessing;
		class VariableReceiverSensitivity;

		class VariableSampleInterval;
		class VariableSampleTimeOffset;
		class VariableBlankingInterval;

		class VariableDetectedBottomRange;
		class IVariableTimeVariedGain;
		class IVariableTransducerGain;
		class VariableTransducerImpedance;
		class VariableTransceiverImpedance;
		class VariableCalibratedFrequency;
		
		class VariableTransmitBandwith;
		class VariableTransmitDurationNominal;
		class VariableReceiveDurationEffective;
		class VariableTransmitFrequencyStart;
		class VariableTransmitFrequencyStop;
		class VariableTransmitPower;
		class VariableTransmitSourceLevel;
		class VariableTransmitterAndReceiverCoefficient;
		class VariableTransmitType;
		class VariableTransmitPulseModelI;
		class VariableTransmitPulseModelR;

		class VariableReceiveTransducerIndex;
		class VariableTransmitTransducerIndex;
		class VariableTransmitBeamIndex;

		class VariableActiveMRU;
		class VariableActivePositionSensor;

		class VariableSoundSpeedAtTransducer;
		class VariablePlatformLatitude;
		class VariablePlatformLongitude;
		class VariablePlatformHeading;
		class VariablePlatformPitch;
		class VariablePlatformRoll;
		class VariablePlatformVerticalOffset;
		class VariableTxTransducerDepth;
		class VariableWaterlineToChartDatum;
	}

	class GroupBeam : public UnderTopLevelGroup {

	public:
		GroupBeam(
			GroupSonar* const parent, 
			const size_t& beamGroupIndex = 0, 
			const size_t& beamCount = ncUtils::INVALID_SIZE, 
			const size_t& subbeamCount = ncUtils::INVALID_SIZE,
			const size_t& frequencyCount = ncUtils::INVALID_SIZE,
			const size_t& txBeamCount = ncUtils::INVALID_SIZE
		);
		~GroupBeam();

		// Group name
		const static std::string NC_GROUP_NAME;

		// Sonar Group
		GroupSonar* const getSonarGroup() const;

		// Beam group index
		const size_t& getBeamGroupIndex() const;

		// Dimensions
		ncDimension* const getPingTimeDimension() const;
		ncDimension* const getBeamDimension() const;
		ncDimension* const getSubbeamDimension() const;
		ncDimension* const getTxBeamDimension() const;
		
		// Accesseurs attributs
		sonar::BeamMode::Mode getBeamMode() const;
		sonar::ConversionEquationType::Type getConversionEquationType() const;
		int getPreferredMru() const;
		int getPreferredPosition() const;

		void setBeamMode(const sonar::BeamMode::Mode& beamMode);
		void setConversionEquationType(const sonar::ConversionEquationType::Type& conversionEquationType);
		void setPreferedMru(const int& preferedMru);
		void setPreferedPosition(const int& preferedPosition);

		void setNonQuantitativeProcessingMeanings(const std::vector<std::string> meanings);
		void setNonQuantitativeProcessingMeanings(const std::string spaceSeparatedMeanings);

		void setTransducerName(const std::string& transducerName);
		std::string getTransducerName() const;

		// ---------------------------------------------------------
		// ------------------ Accesseurs des variables -------------
		// ---------------------------------------------------------
		beamGroup::VariableBeam* getBeamVariable() const;
		beamGroup::VariablePingTime* getPingTimeVariable() const;
		beamGroup::VariableFrequency* getFrequencyVariable() const;

		beamGroup::IVariableBackscatterI* getBackscatterIVariable() const;
		beamGroup::IVariableBackscatterR* getBackscatterRVariable() const;
		beamGroup::VariableBackscatterSampleCount* getBackscatterSampleCountVariable() const;

		beamGroup::VariableEchoAngleMajor* getEchoAngleMajorVariable() const;
		beamGroup::VariableEchoAngleMinor* getEchoAngleMinorVariable() const;
		beamGroup::VariableEchoAngleMajorSensitivity* getEchoAngleMajorSensitivityVariable() const;
		beamGroup::VariableEchoAngleMinorSensitivity* getEchoAngleMinorSensitivityVariable() const;

		beamGroup::VariableBeamWidthReceiveMajor* getBeamWidthReceiveMajorVariable() const;
		beamGroup::VariableBeamWidthReceiveMinor* getBeamWidthReceiveMinorVariable() const;
		beamGroup::VariableBeamWidthTransmitMajor* getBeamWidthTransmitMajorVariable() const;
		beamGroup::VariableBeamWidthTransmitMinor* getBeamWidthTransmitMinorVariable() const;

		beamGroup::VariableRxBeamRotationPhi* getRxBeamRotationPhiVariable() const;
		beamGroup::VariableRxBeamRotationTheta* getRxBeamRotationThetaVariable() const;
		beamGroup::VariableRxBeamRotationPsi* getRxBeamRotationPsiVariable() const;

		beamGroup::VariableTxBeamRotationPhi* getTxBeamRotationPhiVariable() const;
		beamGroup::VariableTxBeamRotationTheta* getTxBeamRotationThetaVariable() const;
		beamGroup::VariableTxBeamRotationPsi* getTxBeamRotationPsiVariable() const;

		beamGroup::VariableBeamStabilisation* getBeamStabilisationVariable() const;
		beamGroup::VariableBeamType* getBeamTypeVariable() const;
		beamGroup::VariableEquivalentBeamAngle* getEquivalentBeamAngleVariable() const;
		beamGroup::VariableGainCorrection* getGainCorrectionVariable() const;
		beamGroup::VariableNonQuantitativeProcessing* getNonQuantitativeProcessingVariable() const;
		beamGroup::VariableReceiverSensitivity* getReceiverSensitivityVariable() const;

		beamGroup::VariableSampleInterval* getSampleIntervalVariable() const;
		beamGroup::VariableSampleTimeOffset* getSampleTimeOffsetVariable() const;
		beamGroup::VariableBlankingInterval* getBlankingIntervalVariable() const;

		beamGroup::VariableDetectedBottomRange* getDetectedBottomRangeVariable() const;
		beamGroup::IVariableTimeVariedGain* getIVariableTimeVariedGainVariable() const;
		beamGroup::IVariableTransducerGain* getTransducerGainVariable() const;
		beamGroup::VariableTransceiverImpedance* getTransceiverImpedanceVariable() const;
		beamGroup::VariableTransducerImpedance* getTransducerImpedanceVariable() const;
		beamGroup::VariableCalibratedFrequency* getCalibratedFrequencyVariable() const;

		beamGroup::VariableTransmitBandwith* getTransmitBandwithVariable() const;
		beamGroup::VariableTransmitDurationNominal* getTransmitDurationNominalVariable() const;
		beamGroup::VariableReceiveDurationEffective* getReceiveDurationEffectiveVariable() const;
		beamGroup::VariableTransmitFrequencyStart* getTransmitFrequencyStartVariable() const;
		beamGroup::VariableTransmitFrequencyStop* getTransmitFrequencyStopVariable() const;
		beamGroup::VariableTransmitPower* getTransmitPowerVariable() const;
		beamGroup::VariableTransmitSourceLevel* getTransmitSourceLevelVariable() const;
		beamGroup::VariableTransmitterAndReceiverCoefficient* getTransmitterAndReceiverCoefficientVariable() const;
		beamGroup::VariableTransmitType* getTransmitTypeVariable() const;
		beamGroup::VariableTransmitPulseModelI* getTransmitPulseModelIVariable() const;
		beamGroup::VariableTransmitPulseModelR* getTransmitPulseModelRVariable() const;

		beamGroup::VariableReceiveTransducerIndex* getReceiveTransducerIndexVariable() const;
		beamGroup::VariableTransmitTransducerIndex* getTransmitTransducerIndexVariable() const;
		beamGroup::VariableTransmitBeamIndex* getTransmitBeamIndexVariable() const;

		beamGroup::VariableActiveMRU* getActiveMRUVariable() const;
		beamGroup::VariableActivePositionSensor* getActivePositionSensorVariable() const;

		beamGroup::VariableSoundSpeedAtTransducer* getSoundSpeedAtTransducerVariable() const;
		beamGroup::VariablePlatformLatitude* getPlatformLatitudeVariable() const;
		beamGroup::VariablePlatformLongitude* getPlatformLongitudeVariable() const;
		beamGroup::VariablePlatformHeading* getPlatformHeadingVariable() const;
		beamGroup::VariablePlatformPitch* getPlatformPitchVariable() const;
		beamGroup::VariablePlatformRoll* getPlatformRollVariable() const;
		beamGroup::VariablePlatformVerticalOffset* getPlatformVerticalOffsetVariable() const;
		beamGroup::VariableTxTransducerDepth* getTxTransducerDepthVariable() const;
		beamGroup::VariableWaterlineToChartDatum* getWaterlineToChartDatumVariable() const;

	private:
		// ----------------------------- Members
		// Index du beam group
		const size_t m_beamGroupIndex;

		// Evolution de la vitesse du son
		double m_originalSoundSpeed;
		double m_lastSoundSpeed;

		// ----------------------------- Methods

		// Initialisation des attributs du groupe
		virtual void initializeAttributes();

		// Initialisation de types personnalisÚs
		virtual void initializeCustomType(const size_t& nbSubbeam);

		// -----------------------------------------------------------------------------------------
		// Dimensions
		std::unique_ptr<ncDimension> m_beamDimension;
		std::unique_ptr<ncDimension> m_subbeamDimension;
		std::unique_ptr<ncDimension> m_pingTimeDimension;
		std::unique_ptr<ncDimension> m_txBeamDimension;
		std::unique_ptr<ncDimension> m_frequencyDimension;

		// Variables
		std::unique_ptr<beamGroup::VariableBeam> m_beamVariable;
		std::unique_ptr<beamGroup::VariablePingTime> m_pingTimeVariable;
		std::unique_ptr<PseudoVariableSingleDimension> m_subbeamVariable;
		std::unique_ptr<PseudoVariableSingleDimension> m_txBeamVariable;
		std::unique_ptr<beamGroup::VariableFrequency> m_frequencyVariable;

		std::unique_ptr<beamGroup::IVariableBackscatterI> m_backscatterIVariable;
		std::unique_ptr<beamGroup::IVariableBackscatterR> m_backscatterRVariable;
		std::unique_ptr<beamGroup::VariableBackscatterSampleCount> m_backscatterSampleCountVariable;

		std::unique_ptr<beamGroup::VariableEchoAngleMajor> m_echoAngleMajorVariable;
		std::unique_ptr<beamGroup::VariableEchoAngleMinor> m_echoAngleMinorVariable;
		std::unique_ptr<beamGroup::VariableEchoAngleMajorSensitivity> m_echoAngleMajorSensitivityVariable;
		std::unique_ptr<beamGroup::VariableEchoAngleMinorSensitivity> m_echoAngleMinorSensitivityVariable;

		std::unique_ptr<beamGroup::VariableBeamWidthReceiveMajor> m_beamWidthReceiveMajorVariable;
		std::unique_ptr<beamGroup::VariableBeamWidthReceiveMinor> m_beamWidthReceiveMinorVariable;
		std::unique_ptr<beamGroup::VariableBeamWidthTransmitMajor> m_beamWidthTransmitMajorVariable;
		std::unique_ptr<beamGroup::VariableBeamWidthTransmitMinor> m_beamWidthTransmitMinorVariable;

		std::unique_ptr<beamGroup::VariableRxBeamRotationPhi> m_rxBeamRotationPhiVariable;
		std::unique_ptr<beamGroup::VariableRxBeamRotationTheta> m_rxBeamRotationThetaVariable;
		std::unique_ptr<beamGroup::VariableRxBeamRotationPsi> m_rxBeamRotationPsiVariable;

		std::unique_ptr<beamGroup::VariableTxBeamRotationPhi> m_txBeamRotationPhiVariable;
		std::unique_ptr<beamGroup::VariableTxBeamRotationTheta> m_txBeamRotationThetaVariable;
		std::unique_ptr<beamGroup::VariableTxBeamRotationPsi> m_txBeamRotationPsiVariable;

		std::unique_ptr<beamGroup::VariableBeamStabilisation> m_beamStabilisationVariable;
		std::unique_ptr<beamGroup::VariableBeamType> m_beamTypeVariable;
		std::unique_ptr<beamGroup::VariableEquivalentBeamAngle> m_equivalentBeamAngleVariable;
		std::unique_ptr<beamGroup::VariableGainCorrection> m_gainCorrectionVariable;
		std::unique_ptr<beamGroup::VariableNonQuantitativeProcessing> m_nonQuantitativeProcessingVariable;
		std::unique_ptr<beamGroup::VariableReceiverSensitivity> m_receiverSensitivityVariable;

		std::unique_ptr<beamGroup::VariableSampleInterval> m_sampleIntervalVariable;
		std::unique_ptr<beamGroup::VariableSampleTimeOffset> m_sampleTimeOffsetVariable;
		std::unique_ptr<beamGroup::VariableBlankingInterval> m_blankingIntervalVariable;

		std::unique_ptr<beamGroup::VariableDetectedBottomRange> m_detectedBottomRangeVariable;
		std::unique_ptr<beamGroup::IVariableTimeVariedGain> m_timeVariedGainVariable;
		std::unique_ptr<beamGroup::IVariableTransducerGain> m_transducerGainVariable;
		std::unique_ptr<beamGroup::VariableTransceiverImpedance> m_transceiverImpedanceVariable;
		std::unique_ptr<beamGroup::VariableTransducerImpedance> m_transducerImpedanceVariable;
		std::unique_ptr<beamGroup::VariableCalibratedFrequency> m_calibratedFrequencyVariable;

		std::unique_ptr<beamGroup::VariableTransmitBandwith> m_transmitBandwithVariable;
		std::unique_ptr<beamGroup::VariableTransmitDurationNominal> m_transmitDurationNominalVariable;
		std::unique_ptr<beamGroup::VariableReceiveDurationEffective> m_receivedDurationEffectiveVariable;
		std::unique_ptr<beamGroup::VariableTransmitFrequencyStart> m_transmitFrequencyStartVariable;
		std::unique_ptr<beamGroup::VariableTransmitFrequencyStop> m_transmitFrequencyStopVariable;
		std::unique_ptr<beamGroup::VariableTransmitPower> m_transmitPowerVariable;
		std::unique_ptr<beamGroup::VariableTransmitSourceLevel> m_transmitSourceLevelVariable;
		std::unique_ptr<beamGroup::VariableTransmitterAndReceiverCoefficient> m_transmitterAndReceiverCoefficientVariable;
		std::unique_ptr<beamGroup::VariableTransmitType> m_transmitTypeVariable;
		std::unique_ptr<beamGroup::VariableTransmitPulseModelI> m_transmitPulseModelIVariable;
		std::unique_ptr<beamGroup::VariableTransmitPulseModelR> m_transmitPulseModelRVariable;

		std::unique_ptr<beamGroup::VariableReceiveTransducerIndex> m_receiveTransducerIndexVariable;
		std::unique_ptr<beamGroup::VariableTransmitTransducerIndex> m_transmitTransducerIndexVariable;
		std::unique_ptr<beamGroup::VariableTransmitBeamIndex> m_transmitBeamIndexVariable;

		std::unique_ptr<beamGroup::VariableActiveMRU> m_activeMRUVariable;
		std::unique_ptr<beamGroup::VariableActivePositionSensor> m_activePositionSensorVariable;

		std::unique_ptr<beamGroup::VariableSoundSpeedAtTransducer> m_soundSpeedAtTransducerVariable;
		std::unique_ptr<beamGroup::VariablePlatformLatitude> m_platformLatitudeVariable;
		std::unique_ptr<beamGroup::VariablePlatformLongitude> m_platformLongitudeVariable;
		std::unique_ptr<beamGroup::VariablePlatformHeading> m_platformHeadingVariable;
		std::unique_ptr<beamGroup::VariablePlatformPitch> m_platformPitchVariable;
		std::unique_ptr<beamGroup::VariablePlatformRoll> m_platformRollVariable;
		std::unique_ptr<beamGroup::VariablePlatformVerticalOffset> m_platformVerticalOffsetVariable;
		std::unique_ptr<beamGroup::VariableTxTransducerDepth> m_txTransducerDepthVariable;
		std::unique_ptr<beamGroup::VariableWaterlineToChartDatum> m_waterlineToChartDatumVariable;
		
		// SubGroup
	};
}

