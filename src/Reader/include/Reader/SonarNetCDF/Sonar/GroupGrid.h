#pragma once

#include "Reader/SonarNetCDF/GroupTopLevel.h"
#include "Reader/SonarNetCDF/VariableAccessWrapper.h"

#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

#include <memory>

namespace sonarNetCDF {

	class GroupSonar;
	
	namespace gridGroup {
		class VariableBeam;
		class VariableFrequency;

		class VariableBeamReference;
		class VariableBackscatterType;
		class VariablePingAxisIntervalType;
		class VariablePingAxisIntervalValue;
		class VariableRangeAxisIntervalType;
		class VariableRangeAxisIntervalValue;

		class VariableCellPingTime;
		class VariableIntegratedBackscatter;

		class VariableBeamWidthReceiveMajor;
		class VariableBeamWidthReceiveMinor;
		class VariableBeamWidthTransmitMajor;
		class VariableBeamWidthTransmitMinor;

		class VariableRxBeamRotationPhi;
		class VariableRxBeamRotationTheta;
		class VariableRxBeamRotationPsi;
		class VariableTxBeamRotationPhi;
		class VariableTxBeamRotationTheta;
		class VariableTxBeamRotationPsi;

		class VariableBeamStabilisation;
		class VariableBeamType;
		class VariableEquivalentBeamAngle;
		class VariableGainCorrection;
		class VariableNonQuantitativeProcessing;
		class VariableReceiverSensitivity;
		class VariableSampleInterval;
		class VariableSampleTimeOffset;
		class VariableBlankingInterval;
		class VariableDetectedBottomRange;
		class IVariableTimeVariedGain;
		class VariableTransducerGain;
		class VariableTransmitBandwith;
		class VariableTransmitDurationNominal;
		class VariableTransmitType;
		class VariableReceiveDurationEffective;

		class VariableTransmitFrequencyStart;
		class VariableTransmitFrequencyStop;

		class VariableTransmitPower;
		class VariableTransmitSourceLevel;
		class VariableReceiveTransducerIndex;
		class VariableSoundSpeedAtTransducer;

		class VariableCellLatitude;
		class VariableCellLongitude;
		class VariableCellDepth;

		class VariableTxTransducerDepth;
		class VariableWaterlineToChartDatum;
	}

	class GroupGrid : public UnderTopLevelGroup {

	public:
		GroupGrid(
			GroupSonar* const parent,
			const size_t& gridGroupIndex = 0,
			const std::string& layerTypeName = std::string(),
			const size_t& beamCount = ncUtils::INVALID_SIZE, 
			const size_t& subbeamCount = ncUtils::INVALID_SIZE,
			const size_t& frequencyCount = ncUtils::INVALID_SIZE,
			const size_t& rangeAxisCount = ncUtils::INVALID_SIZE,
			const size_t& pingAxisCount = ncUtils::INVALID_SIZE,
			const size_t& txBeamCount = ncUtils::INVALID_SIZE
		);
		~GroupGrid();

		// Group name
		const static std::string NC_GROUP_NAME;

		// Sonar Group
		GroupSonar* const getSonarGroup() const;

		// Grid group index
		const size_t& getGridGroupIndex() const;

		// Layer type name
		const std::string& getLayerTypeName() const;

		// Dimensions
		ncDimension* const getBeamDimension() const;
		ncDimension* const getFrequencyDimension() const;
		ncDimension* const getTxBeamDimension() const;
		ncDimension* const getPingAxisDimension() const;
		ncDimension* const getRangeAxisDimension() const;
		
		// Accesseurs attributs
		sonar::BeamMode::Mode getBeamMode() const;
		sonar::ConversionEquationType::Type getConversionEquationType() const;

		void setBeamMode(const sonar::BeamMode::Mode& beamMode);
		void setConversionEquationType(const sonar::ConversionEquationType::Type& conversionEquationType);

		// ---------------------------------------------------------
		// ------------------ Accesseurs des variables -------------
		// ---------------------------------------------------------
		gridGroup::VariableBeam* getBeamVariable() const;
		gridGroup::VariableFrequency* getFrequencyVariable() const;

		gridGroup::VariableBeamReference* getBeamReferenceVariable() const;
		gridGroup::VariableBackscatterType* getBackscatterTypeVariable() const;
		gridGroup::VariablePingAxisIntervalType* getPingAxisIntervalType() const;
		gridGroup::VariablePingAxisIntervalValue* getPingAxisIntervalValue() const;
		gridGroup::VariableRangeAxisIntervalType* getRangeAxisIntervalType() const;
		gridGroup::VariableRangeAxisIntervalValue* getRangeAxisIntervalValue() const;

		gridGroup::VariableCellPingTime* getCellPingTimeVariable() const;
		gridGroup::VariableIntegratedBackscatter* getIntegratedBackscatterVariable() const;
		
		gridGroup::VariableBeamWidthReceiveMajor* getBeamWidthReceiveMajorVariable() const;
		gridGroup::VariableBeamWidthReceiveMinor* getBeamWidthReceiveMinorVariable() const;
		gridGroup::VariableBeamWidthTransmitMajor* getBeamWidthTransmitMajorVariable() const;
		gridGroup::VariableBeamWidthTransmitMinor* getBeamWidthTransmitMinorVariable() const;
		
		gridGroup::VariableRxBeamRotationPhi* getRxBeamRotationPhiVariable() const;
		gridGroup::VariableRxBeamRotationPsi* getRxBeamRotationPsiVariable() const;
		gridGroup::VariableRxBeamRotationTheta* getRxBeamRotationThetaVariable() const;
		gridGroup::VariableTxBeamRotationPhi* getTxBeamRotationPhiVariable() const;
		gridGroup::VariableTxBeamRotationPsi* getTxBeamRotationPsiVariable() const;
		gridGroup::VariableTxBeamRotationTheta* getTxBeamRotationThetaVariable() const;
		
		gridGroup::VariableBeamStabilisation* getBeamStabilisationVariable() const;
		gridGroup::VariableBeamType* getBeamTypeVariable() const;
		gridGroup::VariableEquivalentBeamAngle* getEquivalentBeamAngleVariable() const;
		gridGroup::VariableGainCorrection* getGainCorrectionVariable() const;
		gridGroup::VariableNonQuantitativeProcessing* getNonQuantitativeProcessingVariable() const;
		gridGroup::VariableReceiverSensitivity* getReceiverSensitivityVariable() const;
		gridGroup::VariableSampleInterval* getSampleIntervalVariable() const;
		gridGroup::VariableSampleTimeOffset* getSampleTimeOffsetVariable() const;
		gridGroup::VariableBlankingInterval* getBlankingIntervalVariable() const;
		gridGroup::VariableDetectedBottomRange* getDetectedBottomRangeVariable() const;
		gridGroup::IVariableTimeVariedGain* getTimeVariedGainVariable() const;
		gridGroup::VariableTransducerGain* getTransducerGainVariable() const;
		gridGroup::VariableTransmitBandwith* getTransmitBandwidthVariable() const;
		gridGroup::VariableTransmitDurationNominal* getTransmitDurationNominalVariable() const;
		gridGroup::VariableTransmitType* getTransmitTypeVariable() const;
		gridGroup::VariableReceiveDurationEffective* getReceiveDurationEffectiveVariable() const;
		
		gridGroup::VariableTransmitFrequencyStart* getTransmitFrequencyStartVariable() const;
		gridGroup::VariableTransmitFrequencyStop* getTransmitFrequencyStopVariable() const;
		
		gridGroup::VariableTransmitPower* getTransmitPowerVariable() const;
		gridGroup::VariableTransmitSourceLevel* getTransmitSourceLevelVariable() const;
		gridGroup::VariableReceiveTransducerIndex* getReceiveTransducerIndexVariable() const;
		gridGroup::VariableSoundSpeedAtTransducer* getSoundSpeedAtTransducerVariable() const;
		
		gridGroup::VariableCellLatitude* getCellLatitudeVariable() const;
		gridGroup::VariableCellLongitude* getCellLongitudeVariable() const;
		gridGroup::VariableCellDepth* getCellDepthVariable() const;
		
		gridGroup::VariableTxTransducerDepth* getTxTransducerDepthVariable() const;
		gridGroup::VariableWaterlineToChartDatum* getWaterlineToChartDatumVariable() const;


	private:
		// ----------------------------- Members
		// Index du grid group
		const size_t m_gridGroupIndex;

		// Type de couche associ�e au gridGroup -- Non pr�sent dans le fichier sonarNetCDF
		const std::string m_layerTypeName;

		// ----------------------------- Methods

		// Initialisation des attributs du groupe
		virtual void initializeAttributes();

		// Initialisation de types personnalis�s
		virtual void initializeCustomType(const size_t& nbSubbeam);

		// -----------------------------------------------------------------------------------------
		// Dimensions
		std::unique_ptr<ncDimension> m_beamDimension;
		std::unique_ptr<ncDimension> m_frequencyDimension;
		std::unique_ptr<ncDimension> m_txBeamDimension;
		std::unique_ptr<ncDimension> m_pingAxisDimension;
		std::unique_ptr<ncDimension> m_rangeAxisDimension;

		// Variables
		std::unique_ptr<gridGroup::VariableBeam> m_beamVariable;
		std::unique_ptr<gridGroup::VariableFrequency> m_frequencyVariable;
		std::unique_ptr<PseudoVariableSingleDimension> m_txBeamVariable;
		std::unique_ptr<PseudoVariableSingleDimension> m_pingAxisVariable;
		std::unique_ptr<PseudoVariableSingleDimension> m_rangeAxisVariable;

		std::unique_ptr<gridGroup::VariableBeamReference> m_beamReferenceVariable;
		std::unique_ptr<gridGroup::VariableBackscatterType> m_backscatterTypeVariable;
		std::unique_ptr<gridGroup::VariablePingAxisIntervalType> m_pingAxisIntervalTypeVariable;
		std::unique_ptr<gridGroup::VariablePingAxisIntervalValue> m_pingAxisIntervalValueVariable;
		std::unique_ptr<gridGroup::VariableRangeAxisIntervalType> m_rangeAxisIntervalTypeVariable;
		std::unique_ptr<gridGroup::VariableRangeAxisIntervalValue> m_rangeAxisIntervalValueVariable;

		std::unique_ptr<gridGroup::VariableCellPingTime> m_cellPingTimeVariable;
		std::unique_ptr<gridGroup::VariableIntegratedBackscatter> m_integratedBackscatterVariable;

		std::unique_ptr<gridGroup::VariableBeamWidthReceiveMajor> m_beamWidthReceiveMajorVariable;
		std::unique_ptr<gridGroup::VariableBeamWidthReceiveMinor> m_beamWidthReceiveMinorVariable;
		std::unique_ptr<gridGroup::VariableBeamWidthTransmitMajor> m_beamWidthTransmitMajorVariable;
		std::unique_ptr<gridGroup::VariableBeamWidthTransmitMinor> m_beamWidthTransmitMinorVariable;

		std::unique_ptr<gridGroup::VariableRxBeamRotationPhi> m_rxBeamRotationPhiVariable;
		std::unique_ptr<gridGroup::VariableRxBeamRotationTheta> m_rxBeamRotationThetaVariable;
		std::unique_ptr<gridGroup::VariableRxBeamRotationPsi> m_rxBeamRotationPsiVariable;
		std::unique_ptr<gridGroup::VariableTxBeamRotationPhi> m_txBeamRotationPhiVariable;
		std::unique_ptr<gridGroup::VariableTxBeamRotationTheta> m_txBeamRotationThetaVariable;
		std::unique_ptr<gridGroup::VariableTxBeamRotationPsi> m_txBeamRotationPsiVariable;

		std::unique_ptr<gridGroup::VariableBeamStabilisation> m_beamStabilisationVariable;
		std::unique_ptr<gridGroup::VariableBeamType> m_beamTypeVariable;
		std::unique_ptr<gridGroup::VariableEquivalentBeamAngle> m_equivalentBeamAngleVariable;
		std::unique_ptr<gridGroup::VariableGainCorrection> m_gainCorrectionVariable;
		std::unique_ptr<gridGroup::VariableNonQuantitativeProcessing> m_nonQuantitativeProcessingVariable;
		std::unique_ptr<gridGroup::VariableReceiverSensitivity> m_receiverSensitivityVariable;
		std::unique_ptr<gridGroup::VariableSampleInterval> m_sampleIntervalVariable;
		std::unique_ptr<gridGroup::VariableSampleTimeOffset> m_sampleTimeOffsetVariable;
		std::unique_ptr<gridGroup::VariableBlankingInterval> m_blankingIntervalVariable;
		std::unique_ptr<gridGroup::VariableDetectedBottomRange> m_detectedBottomRangeVariable;
		std::unique_ptr<gridGroup::IVariableTimeVariedGain> m_timeVariedGainVariable;
		std::unique_ptr<gridGroup::VariableTransducerGain> m_transducerGainVariable;
		std::unique_ptr<gridGroup::VariableTransmitBandwith> m_transmitBandwidthVariable;
		std::unique_ptr<gridGroup::VariableTransmitDurationNominal> m_transmitDurationNominalVariable;
		std::unique_ptr<gridGroup::VariableTransmitType> m_transmitTypeVariable;
		std::unique_ptr<gridGroup::VariableReceiveDurationEffective> m_receiveDurationEffectiveVariable;

		std::unique_ptr<gridGroup::VariableTransmitFrequencyStart> m_transmitFrequencyStartVariable;
		std::unique_ptr<gridGroup::VariableTransmitFrequencyStop> m_transmitFrequencyStopVariable;

		std::unique_ptr<gridGroup::VariableTransmitPower> m_transmitPowerVariable;
		std::unique_ptr<gridGroup::VariableTransmitSourceLevel> m_transmitSourceLevelVariable;
		std::unique_ptr<gridGroup::VariableReceiveTransducerIndex> m_receiveTransducerIndexVariable;
		std::unique_ptr<gridGroup::VariableSoundSpeedAtTransducer> m_soundSpeedAtTransducerVariable;

		std::unique_ptr<gridGroup::VariableCellLatitude> m_cellLatitudeVariable;
		std::unique_ptr<gridGroup::VariableCellLongitude> m_cellLongitudeVariable;
		std::unique_ptr<gridGroup::VariableCellDepth> m_cellDepthVariable;
		
		std::unique_ptr<gridGroup::VariableTxTransducerDepth> m_txTransducerDepthVariable;
		std::unique_ptr<gridGroup::VariableWaterlineToChartDatum> m_waterlineToChartDatumVariable;	
		
		// SubGroup
	};
}

