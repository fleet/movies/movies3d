#pragma once

#include <vector>

namespace sonarNetCDF {
	namespace sonar {
		
		// Type of sonar, chosen from a defined vocabulary (currently only one value): �omnisonar� (a sonar with a nominally omnidirectional mode).
		class SonarType {
		public:
			enum class Type {
				OMNI_SONAR, /* Pour l'instant ne contient qu'une valeur */
			};

			static const std::vector<Type> all() {
				return { Type::OMNI_SONAR };
			}

			static std::string getName(Type type) {
				switch (type) {
				case Type::OMNI_SONAR:
					return "omni-sonar";
				default:
					return std::string();
				}
			}

			static const std::string getValue(Type type) {
				return getName(type);
			}

			static const Type getType(std::string name) {
				for (Type type : all())
					if (getValue(type) == name)
						return type;

				return Type::OMNI_SONAR; /* Valeur par d�faut ? */
			}
		};

		// Mode of the beam in this subgroup, taken from the defined vocabulary of: 
		// �vertical� (a set of beams that form a vertical slice through the water), 
		// �horizontal� (a set of beams that form a nominally horizontal plane through the water), 
		// and �inspection� (a set of beams with arbitrary pointing directions).
		class BeamMode {
		public:
			enum class Mode {
				HORIZONTAL,
				VERTICAL,
				INSPECTION
			};

			static const std::vector<Mode> all() {
				return { Mode::HORIZONTAL, Mode::VERTICAL, Mode::INSPECTION };
			}

			static std::string getName(Mode mode) {
				switch (mode) {
				case Mode::HORIZONTAL:
					return "horizontal";
				case Mode::VERTICAL:
					return "vertical";
				case Mode::INSPECTION:
					return "inspection";
				default:
					return std::string();
				}
			}

			static const std::string getValue(Mode mode) {
				return getName(mode);
			}

			static const Mode getMode(std::string name) {
				for (Mode mode : all())
					if (getValue(mode) == name)
						return mode;

				return Mode::HORIZONTAL; /* Valeur par d�faut ? */
			}
		};

		// Whether or not the beam direction is compensated for platform motion.
		class BeamStabilisation {
		public:

			static const std::string getTypeName() {
				return "beam_stabilisation_t";
			}

			enum class Type : uint8_t {
				NOT_STABILISED = 0,
				STABILISED = 1
			};

			static const std::vector<Type> all() {
				return { Type::NOT_STABILISED, Type::STABILISED };
			}

			static std::string getName(Type type) {
				switch (type) {
				case Type::NOT_STABILISED:
					return "not_stabilised";
				case Type::STABILISED:
					return "stabilised";
				default:
					return std::string();
				}
			}

			static const uint8_t getValue(Type type) {
				return (uint8_t)type;
			}

			static const Type getMode(uint8_t value) {
				for (Type type : all())
					if (getValue(type) == value)
						return type;

				return Type::NOT_STABILISED; /* Valeur par d�faut ? */
			}
		};

		// Beam type. Split aperture indicates a beam that can detect the arrival angle of echoes, while single beam cannot.
		class BeamType {
		public:

			static const std::string getTypeName() {
				return "beam_t";
			}

			enum class Type : uint8_t {
				SINGLE = 0,
				SPLIT_APERTURE_ANGLES = 1,
				SPLIT_APERTURE_4_SUBBEAMS = 2,
				SPLIT_APERTURE_3_SUBBEAMS = 3,
				SPLIT_APERTURE_3_1_SUBBEAMS = 4
			};

			static const std::vector<Type> all() {
				return { Type::SINGLE, Type::SPLIT_APERTURE_ANGLES, Type::SPLIT_APERTURE_4_SUBBEAMS, Type::SPLIT_APERTURE_3_SUBBEAMS, Type::SPLIT_APERTURE_3_1_SUBBEAMS };
			}

			static std::string getName(Type mode) {
				switch (mode) {
				case Type::SINGLE:
					return "single";
				case Type::SPLIT_APERTURE_ANGLES:
					return "split aperture angles";
				case Type::SPLIT_APERTURE_4_SUBBEAMS:
					return "split aperture 4 subbeams";
				case Type::SPLIT_APERTURE_3_SUBBEAMS:
					return "split aperture 3 subbeams";
				case Type::SPLIT_APERTURE_3_1_SUBBEAMS:
					return "split aperture 3+1 subbeams";
				default:
					return std::string();
				}
			}

			static const uint8_t getValue(Type type) {
				return (uint8_t)type;
			}

			static const Type getType(uint8_t value) {
				for (Type type : all())
					if (getValue(type) == value)
						return type;

				return Type::SINGLE; /* Valeur par d�faut ? */
			}

			static const size_t getSubbeamNumber(const Type type) {
				switch (type) {
				case Type::SPLIT_APERTURE_4_SUBBEAMS:
				case Type::SPLIT_APERTURE_3_1_SUBBEAMS:
					return 4;
				case Type::SPLIT_APERTURE_3_SUBBEAMS:
					return 3;
				default:
					return 1;
				}
			}
		};

		// The type of equation used to convert backscatter measurements into volume backscattering and target strength.
		class ConversionEquationType {
		public:

			static const std::string getTypeName() {
				return "conversion_equation_t";
			}

			enum class Type : uint8_t {
				TYPE_1 = 1,
				TYPE_2 = 2,
				TYPE_3 = 3,
				TYPE_4 = 4,
				TYPE_5 = 5,
			};

			static const std::vector<Type> all() {
				return { Type::TYPE_1, Type::TYPE_2, Type::TYPE_3, Type::TYPE_4, Type::TYPE_5};
			}

			static std::string getName(Type mode) {
				switch (mode) {
				case Type::TYPE_1:
					return "type_1";
				case Type::TYPE_2:
					return "type_2";
				case Type::TYPE_3:
					return "type_3";
				case Type::TYPE_4:
					return "type_4";
				case Type::TYPE_5:
					return "type_5";
				default:
					return std::string();
				}
			}

			static const uint8_t getValue(Type type) {
				return (uint8_t)type;
			}

			static const Type getType(uint8_t value) {
				for (Type type : all())
					if (getValue(type) == value)
						return type;

				return Type::TYPE_1; /* Valeur par d�faut ? */
			}
		};

		// Type of transmit pulse. 
		// CW = continuous wave � a pulse nominally of one frequency, 
		// LFM = linear frequency modulation � a pulse which varies from transmit_frequency_start to transmit_frequency_stop in a linear manner over the nominal pulse duration (transmit_duration_nominal), 
		// HFM = hyperbolic frequency modulation � a pulse which varies from transmit_frequency_start to transmit_frequency_stop in a hyperbolic manner over the nominal pulse duration (transmit_duration_nominal).
		class TransmitType {
		public:

			static const std::string getTypeName() {
				return "transmit_t";
			}

			enum class Type : uint8_t {
				CW = 0,
				LFM = 1,
				HFM = 2,
			};

			static const std::vector<Type> all() {
				return { Type::CW, Type::LFM, Type::HFM };
			}

			static std::string getName(Type mode) {
				switch (mode) {
				case Type::CW:
					return "CW";
				case Type::LFM:
					return "LFM";
				case Type::HFM:
					return "HFM";
				default:
					return std::string();
				}
			}

			static const uint8_t getValue(Type type) {
				return (uint8_t)type;
			}

			static const Type getType(uint8_t value) {
				for (Type type : all())
					if (getValue(type) == value)
						return type;

				return Type::CW; /* Valeur par d�faut ? */
			}
		};

		// Beam groups custom array types
		// -- Sample
		class SampleType {
		public:
			typedef std::vector<short> typeShort;
			typedef std::vector<float> typeFloat;

			static const std::string getTypeName() {
				return "sample_t";
			}
		};

		// -- Angle
		class AngleType {
		public:
			typedef short subType;
			typedef std::vector<subType> type;

			static const std::string getTypeName() {
				return "angle_t";
			}
		};

		// -- Pulse
		class PulseType {
		public:
			typedef float subType;
			typedef std::vector<subType> type;

			static const std::string getTypeName() {
				return "pulse_t";
			}
		};

		// The vertical datum to which distance are referred to.Possible values are 'MSL Depth' or 'LAT Depth'
		class VerticalCoordinateReferenceSystem {
		public:
			enum class System {
				MSL,
				LAT
			};

			static const std::vector<System> all() {
				return{ System::MSL, System::LAT };
			}

			static std::string getName(System system) {
				switch (system) {
				case System::MSL:
					return "MSL Depth";
				case System::LAT:
					return "LAT Depth";
				default:
					return std::string();
				}
			}

			static const std::string getValue(System system) {
				return getName(system);
			}

			static const System getSystem(std::string name) {
				for (System type : all())
					if (getValue(type) == name)
						return type;

				return System::MSL; /* Valeur par d�faut ? */
			}
		};

		// In what form is the acoustic data stored? Controlled vocabulary options include : 
		// Sv, Volume backscattering strength (dB re 1 m-1) and Sa, Nautical area scattering coefficient (m2 nmi-2).
		class BackscatterType {
		public:

			static const std::string getTypeName() {
				return "backscatter_type_t";
			}

			enum class Type {
				SV = 0,
				SA
			};

			static const std::vector<Type> all() {
				return { Type::SV, Type::SA };
			}

			static std::string getName(Type type) {
				switch (type) {
				case Type::SV:
					return "SV";

				case Type::SA:
					return "SA";

				default:
					return std::string();
				}
			}

			static const uint8_t getValue(Type type) {
				return (uint8_t)type;
			}

			static const Type getType(uint8_t value) {
				for (Type type : all()) {
					if (getValue(type) == value) {
						return type;
					}
				}

				return Type::SV; /* Valeur par d�faut ? */
			}
		};

		// Range axis interval by which data have been regridded. Controlled vocabulary includes: Range (metres), Depth relative to waterline (metres).
		class RangeAxisIntervalType {
		public:

			static const std::string getTypeName() {
				return "range_axis_interval_type_t";
			}

			enum class Type {
				RANGE = 0,
				DEPTH
			};

			static const std::vector<Type> all() {
				return { Type::RANGE, Type::DEPTH };
			}

			static std::string getName(Type type) {
				switch (type) {
				case Type::RANGE:
					return "Range";

				case Type::DEPTH:
					return "Depth";

				default:
					return std::string();
				}
			}

			static const uint8_t getValue(Type type) {
				return (uint8_t)type;
			}

			static const Type getType(uint8_t value) {
				for (Type type : all()) {
					if (getValue(type) == value) {
						return type;
					}
				}

				return Type::RANGE; /* Valeur par d�faut ? */
			}
		};

		// Ping axis interval by which data have been regridded.
		// Controlled vocabulary includes: Time based intervals ( Time (seconds) ), Distance based intervals ( Distance (nautical miles); Distance (metres) ), Ping based intervals (Number of pings).
		class PingAxisIntervalType {
		public:

			static const std::string getTypeName() {
				return "ping_axis_interval_type_t";
			}

			enum class Type {
				TIME_SECONDS = 0,
				DISTANCE_NAUTICAL_MILES,
				DISTANCE_METERS,
				NUMBER_OF_PINGS
			};

			static const std::vector<Type> all() {
				return { Type::TIME_SECONDS, Type::DISTANCE_NAUTICAL_MILES, Type::DISTANCE_METERS, Type::NUMBER_OF_PINGS };
			}

			static std::string getName(Type type) {
				switch (type) {
				case Type::TIME_SECONDS:
					return "Time (seconds)";

				case Type::DISTANCE_NAUTICAL_MILES:
					return "Distance (nautical miles)";

				case Type::DISTANCE_METERS:
					return "Distance (meters)";

				case Type::NUMBER_OF_PINGS:
					return "Number of pings";

				default:
					return std::string();
				}
			}

			static const uint8_t getValue(Type type) {
				return (uint8_t)type;
			}

			static const Type getType(uint8_t value) {
				for (Type type : all()) {
					if (getValue(type) == value) {
						return type;
					}
				}

				return Type::TIME_SECONDS; /* Valeur par d�faut ? */
			}
		};
	}
}

