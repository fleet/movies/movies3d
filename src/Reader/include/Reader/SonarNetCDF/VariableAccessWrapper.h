#pragma once

#include "Reader/SonarNetCDF/NetCDFUtils.h"

#include <set>
#include <type_traits>

namespace sonarNetCDF {

	template <typename T>
	class VariableScalar : protected ncVariable<T> {
	public:
		VariableScalar(ncObject* parent, const std::string& variableName)
			: ncVariable<T>(parent, variableName) {
		}

		T getValue() const {
			return ncVariable<T>::getScalarValue();
		}

		void setValue(const T& value) {
			ncVariable<T>::setScalarValue(value);
		}
	};
	

	template <typename T>
	class VariableSingleDimension : protected ncVariable<T> {

		/// Indicates that cache is loaded
		mutable bool _isLoaded = false;

		/// Cache of values from netcdf
		mutable std::vector<T> _values;

		/// Cache of sorted values from netcdf
		mutable std::set<T> _orderedValues;

	public:
		VariableSingleDimension(ncObject* parent, const std::string& variableName, ncDimension* dimension) 
			: ncVariable<T>(parent, variableName, dimension)
		{
		}
		
		/// Load values in a cache, and 
		void loadValues() const {
			_values = ncVariable<T>::getAll1DValue();
			_orderedValues = std::set<T>{ _values.cbegin(), _values.cend() };
			_isLoaded = true;
		}

		virtual T getValueAtIndex(const size_t& index) const {			
			if (index == -1) {
				return ncVariable<T>::getFillValue();
			}

			return ncVariable<T>::getValue(index);
		}

		virtual std::vector<T> getAllValues() const {
			return _isLoaded ? _values : ncVariable<T>::getAll1DValue();
		}

		virtual void setValueAtIndex(const size_t& index, const T& value) {
			if (index != -1) {
				ncVariable<T>::set1DValue(index, value);
			}
		}

		virtual void setAllValues(const std::vector<T>& values) {
			ncVariable<T>::set1DValues(values);
		}

		virtual void setAllValuesTo(const T& value) {
			setAllValues(std::vector<T>(getDimension()->getSize(), value));
		}

		virtual size_t appendValue(const T& value) {
			return ncVariable<T>::append1DValue(value);
		}

		virtual size_t getIndex(const T& value) const {
			return ncVariable<T>::getIndex(value);
		}
		
		template<typename U = T, std::enable_if_t<std::is_arithmetic<U>::value>* = nullptr >
		size_t getNearestIndex(const U & value) const {
			// Récupération des données de la variable
			if(!_isLoaded)	{
				loadValues();
			}

			// En l'absence de données, retourner INVALID_INDEX
			if (_orderedValues.empty()) {
				return ncUtils::INVALID_INDEX;
			}

			// Recherche de l'index
			auto found = std::lower_bound(_orderedValues.cbegin(), _orderedValues.cend(), value);
			if (found == _orderedValues.cend()) {
				--found; // Si la valeur d'entrée est plus grande que l'ensemble des données, on retourne l'index de la plus grande valeur
			}
			else if (*found != value && found != _orderedValues.cbegin()) {
				const auto& foundValue = *found;
				const auto& previousValue = *(std::prev(found));

				if (std::abs((double)(previousValue - value)) < std::abs((double)(foundValue - value))) {
					--found;
				}
			}
			
			// Recherche de l'index
			auto valueFound = std::find(_values.cbegin(), _values.cend(), *found);
			if (valueFound != _values.cend()) {
				return std::distance(_values.cbegin(), valueFound);
			}

			// Retour ncUtils::INVALID_INDEX en cas d'absence
			return ncUtils::INVALID_INDEX;
		}

		virtual size_t findOrAppendValue(const T& value) {
			return ncVariable<T>::findOrAppend1DValue(value);
		}

		ncDimension * getDimension() const {
			return ncVariable<T>::getDimensions().front();
		}

		size_t getSize() const {
			return getDimension()->getSize();
		}
	};

	template <typename T>
	class VariableMultiDimension : public ncVariable<T> {
	public:
		VariableMultiDimension(ncObject* parent, const std::string& variableName, const std::vector<ncDimension*>& dimensions)
			: ncVariable<T>(parent, variableName, dimensions) {
		}

		T getValueAtIndexes(const std::vector<size_t>& indexes) const {
			for (const auto index : indexes) {
				if (index == -1) {
					return ncVariable<T>::getFillValue();
				}
			}

			return ncVariable<T>::getIndexedValue(indexes);
		}

		void setValueAtIndexes(const std::vector<size_t>& indexes, const T& value) {
			for (const auto index : indexes) {
				if (index == -1) {
					return;
				}
			}

			ncVariable<T>::setIndexedValue(indexes, value);
		}

		const std::vector<ncDimension*>& getDimensions() const override
		{
			return ncVariable<T>::getDimensions();
		}

		ncDimension * getDimension(const size_t& index) const {
			return getDimensions().at(index);
		}

		std::vector<size_t> getSizes() const {
			std::vector<size_t> sizes;

			for (const auto dimension : getDimensions()) {
				sizes.push_back(dimension->getSize());
			}

			return sizes;
		}
	};

	template <typename T>
	class VariableDoubleDimension : public VariableMultiDimension<T> {
	public:
		VariableDoubleDimension(ncObject* parent, const std::string& variableName, ncDimension* dimension1, ncDimension* dimension2)
			: VariableMultiDimension<T>(parent, variableName, { dimension1, dimension2 }) {
		}

		T getValueAtIndexes(const size_t& index1, const size_t& index2) const {
			return VariableMultiDimension<T>::getValueAtIndexes({ index1, index2 });
		}

		void setValueAtIndexes(const size_t& index1, const size_t& index2, const T& value) {
			VariableMultiDimension<T>::setValueAtIndexes({ index1, index2 }, value);
		}
	};

	template <typename T>
	class VariableTripleDimension : public VariableMultiDimension<T> {
	public:
		VariableTripleDimension(ncObject* parent, const std::string& variableName, ncDimension* dimension1, ncDimension* dimension2, ncDimension* dimension3)
			: VariableMultiDimension<T>(parent, variableName, { dimension1, dimension2, dimension3 }) {
		}

		T getValueAtIndexes(const size_t& index1, const size_t& index2, const size_t& index3) const {
			return VariableMultiDimension<T>::getValueAtIndexes({ index1, index2, index3 });
		}

		void setValueAtIndexes(const size_t& index1, const size_t& index2, const size_t& index3, const T& value) {
			VariableMultiDimension<T>::setValueAtIndexes({ index1, index2, index3 }, value);
		}
	};

	template <typename T, typename U>
	class VariableSingleVariable : public VariableSingleDimension<T> {
	public:
		VariableSingleVariable(ncObject* parent, const std::string& variableName, VariableSingleDimension<U>* dimensionVariable)
			: VariableSingleDimension<T>(parent, variableName, dimensionVariable->getDimension()) 
		{
			m_dimensionVariable = dimensionVariable;
		}

		T getValueAt(const U& dimensionValue) const {
			return VariableSingleDimension<T>::getValueAtIndex(getDimensionVariableIndexFor(dimensionValue));
		}

		void setValueAt(const U& dimensionValue, const T& value) {
			VariableSingleDimension<T>::set1DValue(getDimensionVariableIndexFor(dimensionValue), value);
		}

	private:
		VariableSingleDimension<U>* m_dimensionVariable;

		size_t getDimensionVariableIndexFor(const U& dimensionValue) const {
			return m_dimensionVariable->getIndex(dimensionValue);
		}

		// On masque les méthodes d'ajout de valeur car ce type de variable ne controle pas la taille de la dimension (seulement VariableSingleDimension)
		using VariableSingleDimension<T>::findOrAppendValue;
		using VariableSingleDimension<T>::appendValue;
	};

	template <typename T, typename U, typename V>
	class VariableDoubleVariable : public VariableDoubleDimension<T> {
	public:
		VariableDoubleVariable(ncObject* parent, const std::string& variableName, VariableSingleDimension<U>* const dimensionVariable1, VariableSingleDimension<V>* dimensionVariable2)
			: VariableDoubleDimension<T>(parent, variableName, dimensionVariable1->getDimension(), dimensionVariable2->getDimension()) {
			m_dimensionVariable1 = dimensionVariable1;
			m_dimensionVariable2 = dimensionVariable2;
		}

		T getValueAt(const U& dimensionValue1, const V& dimensionValue2) const {
			return VariableMultiDimension<T>::getValueAtIndexes(getDimensionVariableIndexesFor(dimensionValue1, dimensionValue2));
		}

		void setValueAt(const U& dimensionValue1, const V& dimensionValue2, const T& value) {
			VariableMultiDimension<T>::setValueAtIndexes(getDimensionVariableIndexesFor(dimensionValue1, dimensionValue2), value);
		}

	private:
		VariableSingleDimension<U>* m_dimensionVariable1;
		VariableSingleDimension<V>* m_dimensionVariable2;

		std::vector<size_t> getDimensionVariableIndexesFor(const U& dimensionValue1, const V& dimensionValue2) const {
			std::vector<size_t> indexes;

			indexes.push_back(m_dimensionVariable1->getIndex(dimensionValue1));
			indexes.push_back(m_dimensionVariable2->getIndex(dimensionValue2));

			return indexes;
		}
	};
	
	template <typename T, typename U, typename V, typename W>
	class VariableTripleVariable : public VariableTripleDimension<T> {
	public:
		VariableTripleVariable(ncObject* parent, const std::string& variableName, VariableSingleDimension<U>* const dimensionVariable1, VariableSingleDimension<V>* dimensionVariable2, VariableSingleDimension<W>* dimensionVariable3)
			: VariableTripleDimension<T>(parent, variableName, dimensionVariable1->getDimension(), dimensionVariable2->getDimension(), dimensionVariable3->getDimension()) {
			m_dimensionVariable1 = dimensionVariable1;
			m_dimensionVariable2 = dimensionVariable2;
			m_dimensionVariable3 = dimensionVariable3;
		}

		T getValueAt(const U& dimensionValue1, const V& dimensionValue2, const W& dimensionValue3) const {
			return VariableMultiDimension<T>::getValueAtIndexes(getDimensionVariableIndexesFor(dimensionValue1, dimensionValue2, dimensionValue3));
		}

		void setValueAt(const U& dimensionValue1, const V& dimensionValue2, const W& dimensionValue3, const T& value) {
			VariableMultiDimension<T>::setValueAtIndexes(getDimensionVariableIndexesFor(dimensionValue1, dimensionValue2, dimensionValue3), value);
		}

	private:
		VariableSingleDimension<U>* m_dimensionVariable1;
		VariableSingleDimension<V>* m_dimensionVariable2;
		VariableSingleDimension<W>* m_dimensionVariable3;

		std::vector<size_t> getDimensionVariableIndexesFor(const U& dimensionValue1, const V& dimensionValue2, const W& dimensionValue3) const {
			std::vector<size_t> indexes;

			indexes.push_back(m_dimensionVariable1->getIndex(dimensionValue1));
			indexes.push_back(m_dimensionVariable2->getIndex(dimensionValue2));
			indexes.push_back(m_dimensionVariable3->getIndex(dimensionValue3));

			return indexes;
		}
	};
	
	// Variable sans donneés, juste les indexs de la dimension associée
	class PseudoVariableSingleDimension : public VariableSingleDimension<size_t> {
	public:
		PseudoVariableSingleDimension(ncDimension* dimension)
			: VariableSingleDimension<size_t>(nullptr, std::string(), dimension) {
		}

		size_t getValueAtIndex(const size_t& index) const override {
			return index;
		}

		std::vector<size_t> getAllValues() const override {
			std::vector<size_t> values;

			for (int i = 0; i < getSize(); ++i) {
				values.push_back(i);
			}

			return values;
		}

		void setValueAtIndex(const size_t& index, const size_t& value) override {
			// Inutile
		}

		size_t appendValue(const size_t& value) override {
			if (value >= getDimension()->getSize()) {
				getDimension()->setSize(value + 1);
			}
			return value;
		}

		size_t getIndex(const size_t& value) const override {
			return value;
		}
		
		size_t findOrAppendValue(const size_t& value) override {
			if (value < getDimension()->getSize()) {
				return value;
			}
			else {
				return appendValue(value);
			}
		}
	};
}
