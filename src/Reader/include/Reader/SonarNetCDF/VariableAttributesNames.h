#pragma once

#include <string>

namespace sonarNetCDF {

	class VariableAttributesNames {
	public:
		const static std::string LONG_NAME;
		const static std::string STANDARD_NAME;
		const static std::string UNITS;
		const static std::string VALID_MIN;
		const static std::string VALID_MAX;
		const static std::string VALID_RANGE;
		const static std::string SCALE_FACTOR;
		const static std::string AXIS;
		const static std::string CALENDAR; 
		const static std::string COORDINATES;
		const static std::string CF_ROLE;
		const static std::string MISSING_VALUE;
	};
}
