#pragma once

#include <string>

namespace sonarNetCDF {

	// Class outil permettant de g�rer la version du protocole SonarNetCDF
	class VersionSonarNetCDF {

	public:
		// Constructeur
		VersionSonarNetCDF(const unsigned short& major = 0, const unsigned short& minor = 0);
		VersionSonarNetCDF(const std::string& version);

		// Accesseurs
		const unsigned short& getMajor() const;
		const unsigned short& getMinor() const;

		std::string toString() const;

		// Operators
		bool operator==(const VersionSonarNetCDF& other) const;
		bool operator>(const VersionSonarNetCDF& other) const;
		bool operator<(const VersionSonarNetCDF& other)const;
		bool operator>=(const VersionSonarNetCDF& other)const;
		bool operator<=(const VersionSonarNetCDF& other)const;

	private:
		// Numero de version majeur
		unsigned short m_major;

		// Numero de version mineur
		unsigned short m_minor;
	};
}

