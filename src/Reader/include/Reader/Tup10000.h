/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup10000.h
/* appele aussi ping tuple u32       											  */
/******************************************************************************/
#ifndef TUP10000
#define TUP10000

#include "Reader/HACtuple.h"

class PingFan;
class Transducer;

class Tup10000 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup10000();

	virtual ~Tup10000();


	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

private:
	int m_logwarnCount;
};


#endif //Tup10000
