#pragma once


class Transducer;

#include "Reader/HACtuple.h"

class Tup10011 : public HACTuple
{
public:
	Tup10011(void);
	~Tup10011(void);
	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, PingFan *aPingFan, unsigned short aChanId, unsigned short aVirtualChanId,
		Transducer *pTransducer, unsigned int transducerIndex,
		unsigned int indexXInPolarMem);

private:
	int m_logwarnCount;
};
