/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup10030.h
/* appele aussi ping tuple u16       											  */
/******************************************************************************/
#ifndef TUP10030
#define TUP10030

#include "Reader/HACtuple.h"

class PingFan;
class Transducer;

class Tup10030 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup10030();

	virtual ~Tup10030();


	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

private:
	int m_logwarnCount;
};


#endif //TUP10030
