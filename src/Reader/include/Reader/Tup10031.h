#ifndef TUP10031
#define TUP10031

class Transducer;

#include "Reader/HACtuple.h"

class Tup10031 : public HACTuple
{
public:
	Tup10031();

	~Tup10031();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, PingFan *aPingFan, unsigned short aChanId, unsigned short aVirtualChanId,
		Transducer *pTransducer, unsigned int transducerIndex,
		unsigned int indexXInPolarMem);

private:
	int m_logwarnCount;
};

#endif
