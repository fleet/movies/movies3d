/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup10040.h
/* appele aussi ping tuple c16       											  */
/******************************************************************************/
#ifndef TUP10040
#define TUP10040

#include "Reader/HACtuple.h"

class PingFan;
class Transducer;

class Tup10040 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup10040();

	virtual ~Tup10040();


	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, PingFan *aPingFan, unsigned short aChanId, unsigned short aVirtualChanId,
		Transducer *pTransducer, unsigned int transducerIndex,
		unsigned int indexXInPolarMem);
private:
	float m_EvaluateCompressionRatio;
	int m_logwarnCount;
};


#endif //TUP10040
