/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup10060.h
/* appele aussi ping tuple cs16       											  */
/******************************************************************************/
#ifndef TUP10060
#define TUP10060

#include "Reader/HACtuple.h"

class PingFan;
class Transducer;

class Tup10060 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup10060();

	virtual ~Tup10060();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, PingFan *aPingFan, unsigned short aChanId, unsigned short aVirtualChanId,
		Transducer *pTransducer, unsigned int transducerIndex, unsigned int indexXInPolarMem);

private:
	int m_logwarnCount;
};

#endif //TUP10060
