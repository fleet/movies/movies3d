#pragma once
#include "Reader/HACtuple.h"
class PingFan;
class Transducer;

class Tup10090 : public HACTuple
{
public:
	Tup10090(void);
	virtual ~Tup10090(void);

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, PingFan *aPingFan, unsigned short aChanId);
private:
	int m_logwarnCount;
};
