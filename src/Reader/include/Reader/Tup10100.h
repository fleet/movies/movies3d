/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup10100.h
/* appele aussi attitude sensor tuple        											  */
/******************************************************************************/
#ifndef TUP10100
#define TUP10100

#include "Reader/HACtuple.h"
class Threshold;


class Tup10100 : public HACTuple
{
public:

	Tup10100();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, Threshold *pEvent);
};


#endif //TUP10100
