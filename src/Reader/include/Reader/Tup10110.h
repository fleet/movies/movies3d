/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup10110.h
/* appele aussi attitude sensor tuple        											  */
/******************************************************************************/
#ifndef TUP10110
#define TUP10110

#include "Reader/HACtuple.h"
class EventMarker;


class Tup10110 : public HACTuple
{
public:

	Tup10110();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, EventMarker *pEvent);
};


#endif //TUP10110
