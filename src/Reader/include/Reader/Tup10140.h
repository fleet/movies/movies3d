/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup10140.h
/* appele aussi attitude sensor tuple        											  */
/******************************************************************************/
#ifndef TUP10140
#define TUP10140

#include "Reader/HACtuple.h"


class NavAttitude;

class Tup10140 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup10140();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, NavAttitude*pNavAttitude);

};


#endif //TUP10140
