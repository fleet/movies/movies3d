/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup10142.h      											  */
/******************************************************************************/
#ifndef TUP10142
#define TUP10142

#include "Reader/HACtuple.h"

class TrawlPlatformPosition;

class Tup10142 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup10142();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);


	static std::uint32_t Encode(MovStream & IS, TrawlPlatformPosition *p);


};


#endif //TUP10142
