/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup11000.h
/* appele aussi attitude sensor tuple        											  */
/******************************************************************************/
#ifndef TUP11000
#define TUP11000

#include "Reader/HACtuple.h"


class Environnement;

class Tup11000 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup11000();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, Environnement*pEnvironnement);

};


#endif //TUP11000
