/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup20.h      											  */
/******************************************************************************/
#ifndef TUP20
#define TUP20

#include "Reader/HACtuple.h"

class NavPosition;


class Tup20 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup20();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, NavPosition *p);

};


#endif //TUP20
