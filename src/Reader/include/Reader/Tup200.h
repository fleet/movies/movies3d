/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup200.h      											  */
/******************************************************************************/
#ifndef TUP200
#define TUP200

#include "Reader/HACtuple.h"

class SounderEk500;
class Tup200 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup200();


	static std::uint32_t Encode(MovStream & IS, SounderEk500 *);

protected:
	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);


};


#endif //TUP200
