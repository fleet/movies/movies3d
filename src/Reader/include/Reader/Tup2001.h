/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup2001.h      											  */
/******************************************************************************/
#ifndef TUP2001
#define TUP2001

#include "Reader/HACtuple.h"

class Sounder;
class Transducer;

class Tup2001 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup2001();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer);

};


#endif //TUP2001
