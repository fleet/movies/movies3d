/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup210.h      											  */
/******************************************************************************/
#ifndef TUP210
#define TUP210

#include "Reader/HACtuple.h"

class SounderER60;
class Tup210 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup210();




	static std::uint32_t Encode(MovStream & IS, SounderER60 *);

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);
};


#endif //TUP210
