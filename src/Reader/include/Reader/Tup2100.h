/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup2100.h      											  */
/******************************************************************************/
#ifndef TUP2100
#define TUP2100

#include "Reader/HACtuple.h"

class Sounder;
class Transducer;

class Tup2100 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup2100();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer);

};


#endif //TUP2100
