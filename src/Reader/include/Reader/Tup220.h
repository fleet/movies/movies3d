/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup220.h      											  */
/******************************************************************************/
#ifndef TUP220
#define TUP220

#include "Reader/HACtuple.h"
class Sounder;
class Transducer;

class Tup220 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup220();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer);

};


#endif //TUP220
