/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup2200.h      											  */
/******************************************************************************/
#ifndef TUP2200
#define TUP2200

#include "Reader/HACtuple.h"

class Sounder;
class Transducer;

class Tup2200 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup2200();


	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer, unsigned int PolarChannelId);


};


#endif //TUP2200
