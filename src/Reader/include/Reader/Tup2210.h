/******************************************************************************/
/*	Project:	SMFH 3D																													*/
/*	Author(s):	O. TONCK (IPSIS)																							*/
/*	File:		Tup2210.h      																										*/
/******************************************************************************/
#ifndef TUP2210
#define TUP2210

#include "Reader/HACtuple.h"

class Sounder;
class Transducer;

class Tup2210 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup2210();


	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer, unsigned int PolarChannelId);


};


#endif //TUP2210
