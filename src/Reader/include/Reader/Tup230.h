/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup230.h      											  */
/******************************************************************************/
#ifndef TUP230
#define TUP230

#include "Reader/HACtuple.h"

class SounderEK80;
class Tup230 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup230();




	static std::uint32_t Encode(MovStream & IS, SounderEK80 *);

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);
};


#endif //TUP230
