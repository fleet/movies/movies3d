/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup2300.h      											  */
/******************************************************************************/
#ifndef TUP2300
#define TUP2300

#include "Reader/HACtuple.h"

class Sounder;
class Transducer;

class Tup2300 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup2300();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer);

};


#endif //TUP2300
