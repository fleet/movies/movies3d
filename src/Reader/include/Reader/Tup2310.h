/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup2310.h      											  */
/******************************************************************************/
#ifndef TUP2310
#define TUP2310

#include "Reader/HACtuple.h"

class SignalFilteringObject;

class Tup2310 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup2310();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, SignalFilteringObject *pFilter);

};


#endif //TUP2310
