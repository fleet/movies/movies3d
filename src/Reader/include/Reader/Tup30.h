/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup30.h      											  */
/******************************************************************************/
#ifndef TUP30
#define TUP30

#include "Reader/HACtuple.h"
class NavAttributes;

class Tup30 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup30();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);
	static std::uint32_t Encode(MovStream & IS, NavAttributes*pNavAttr);

};


#endif //TUP30
