#pragma once

class Transducer;

#include "Reader/HACtuple.h"

class Tup4000 :
	public HACTuple
{
public:
	Tup4000(void);
	virtual ~Tup4000(void);

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, unsigned short aChanId,
		Transducer *pTransducer);

};
