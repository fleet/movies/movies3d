/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup41.h      											  */
/******************************************************************************/
#ifndef TUP41
#define TUP41

#include "Reader/HACtuple.h"

class Platform;
class Tup41 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup41();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);
	static std::uint32_t Encode(MovStream & IS, Platform *pPlatform, unsigned short channelId);

};


#endif //TUP41
