/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup42.h      											  */
/******************************************************************************/
#ifndef TUP42
#define TUP42

#include "Reader/HACtuple.h"

class DynamicPlatformPosition;

class Tup42 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup42();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);
	static std::uint32_t Encode(MovStream & IS, DynamicPlatformPosition *pDynPlat);
};


#endif //TUP42
