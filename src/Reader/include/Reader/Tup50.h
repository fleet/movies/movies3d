/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup50.h      											  */
/******************************************************************************/
#ifndef TUP50
#define TUP50

#include "Reader/HACtuple.h"

class TrawlPositionAttitude;

class Tup50 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup50();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, TrawlPositionAttitude *pPosAtt);

};


#endif //TUP50
