/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup65534.h
/* end tuple       											  */
/******************************************************************************/
#ifndef TUP65534
#define TUP65534

#include "Reader/HACtuple.h"

class HacTime;

class Tup65534 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup65534();


	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);


	static std::uint32_t Encode(MovStream & IS, HacTime &refTime);

};


#endif //TUP65534
