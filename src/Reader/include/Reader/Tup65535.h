/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup65535.h
/* end tuple       											  */
/******************************************************************************/
#ifndef TUP65535
#define TUP65535

#include "Reader/HACtuple.h"



class Tup65535 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup65535();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS);
};


#endif //TUP65535
