/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup9001.h      											  */
/******************************************************************************/
#ifndef TUP9001
#define TUP9001

#include "Reader/HACtuple.h"

class Sounder;
class Transducer;

class Tup9001 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup9001();

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);

	static std::uint32_t Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer);

};


#endif //TUP9001
