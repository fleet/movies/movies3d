/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup901.h      											  */
/******************************************************************************/
#ifndef TUP901
#define TUP901

#include "Reader/HACtuple.h"

class SounderGeneric;
class Tup901 : public HACTuple
{
public:
	//	Constructors / Destructor
	//	*************************
	Tup901();




	static std::uint32_t Encode(MovStream & IS, SounderGeneric *);

	virtual void Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup);
};


#endif //TUP901
