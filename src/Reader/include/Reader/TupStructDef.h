/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R. LHULLIER													  */
/*	File:		Tup30.h      											  */
/******************************************************************************/
#ifndef TUP_STRUCT_DEF
#define TUP_STRUCT_DEF

#include "HACtuple.h"
#include "M3DKernel/datascheme/HacTupleDef.h"
#include <cstdint>


struct Hac210
{
	unsigned short	numOfChannel;
	std::uint32_t	docId;
	unsigned short	soundSpeed;
	unsigned short	pingMode;
	unsigned short	pingInterval;
	unsigned short	space;
	char			remarks[40];
    std::int32_t			tupleAttributes;

};

struct Hac230
{
	unsigned short	numOfChannel;
	std::uint32_t	docId;
	unsigned short	soundSpeed;
	unsigned short	pingMode;
	unsigned short	pingInterval;
	unsigned short	space;
	char			remarks[40];
    std::int32_t			tupleAttributes;

};

struct Hac901
{
	unsigned short	numOfChannel;
	std::uint32_t	docId;
	unsigned short	soundSpeed;
	unsigned short	pingInterval;
	unsigned short  triggerMode;
	unsigned short	space;
	char			remarks[100];
    std::int32_t			tupleAttributes;

};
class Hac9001 : public HacTupleDef
{
public:
	unsigned short ChannelId;
	std::uint32_t docId;
	std::uint32_t SamplingRate;
	std::uint32_t SamplingInterval;
	std::uint32_t	AcousticFrequency;
	unsigned short	TransChannelNumber;
	unsigned short	dataType;
	unsigned short	TimeVariedGain;
	unsigned short	TVGBlankMode;
	unsigned short	TVGMinRange;
	unsigned short	TVGMaxRange;
	std::uint32_t	BlankUpToRange;
	std::uint32_t	SampleRange;
	std::uint32_t	TransDepht;
	unsigned short PlatFormid;
	unsigned short space;

    std::int32_t TransAlongOffset;
    std::int32_t TransAthwartOffset;
    std::int32_t TransVerticalOffset;

	short TransFaceAlongAngleOffset;
	short TransFaceAthwartAngleOffset;
	short RotationAngle;

	short TransMainBeamAlongBeamAngleOffset;
	short TransMainBeamAthwartAngleOffset;

	unsigned short SoundAbsorption;
	std::uint32_t PulseDuration;
	unsigned short TransShape;
	unsigned short BandWidth;
	unsigned short TransShapeMode;
	unsigned short Along3DbWidth;
	unsigned short Athwart3DbWidth;
	short TwoWayBeamAngle;

	unsigned short CalibSourceLevel;
	short CalibReceivLevel;
	short SVVR;

	short BottomDetectionMinLevel;
	std::uint32_t BottomDetectionMinDepth;
	std::uint32_t BottomDetectionMaxDepth;
	char remarks[40];
    std::int32_t tupleAttributes;
};

struct Hac2100
{
	unsigned short ChannelId;
	std::uint32_t docId;

	char  m_FrequencyName[48];
	char TransVersion[30];
	char TransName[30];
	std::uint32_t TimeSampleInterval;
	unsigned short dataType;
	unsigned short beamType;
	std::uint32_t AcousticFrequency;
	std::uint32_t TransDepht;
	std::uint32_t StartSample;
	unsigned short PlatFormid;
	unsigned short TransShape;
    std::int32_t TransFaceAlongAngleOffset;
    std::int32_t TransFaceAthwartAngleOffset;
    std::int32_t RotationAngle;
    std::int32_t TransMainBeamAlongBeamAngleOffset;
    std::int32_t TransMainBeamAthwartAngleOffset;
	std::uint32_t AbsorptionCoeff;
	std::uint32_t PulseDuration;
	std::uint32_t Bandwidth;
	std::uint32_t TransmissionPower;
	std::uint32_t AngleSensitivity;
	std::uint32_t TranAthwartAngleSensitivity;
	std::uint32_t Along3DbWidth;
	std::uint32_t Athwart3DbWidth;
    std::int32_t TwoWayBeamAngle;
	std::uint32_t TransGain;
    std::int32_t TranssACorrection;
	std::uint32_t BottomDetectionMinDepth;
	std::uint32_t BottomDetectionMaxDepth;
    std::int32_t BottomDetectionMinLevel;
	char remarks[40];
    std::int32_t tupleAttributes;
};

struct Hac2300
{
	unsigned short ChannelId;
	std::uint32_t docId;

	char  m_FrequencyName[48];
	char TransVersion[30];
	char TransName[30];
	std::uint32_t TimeSampleInterval;
	unsigned short dataType;
	unsigned short beamType;
	std::uint32_t NominalFrequency;
	std::uint32_t StartFrequency;
	std::uint32_t EndFrequency;
	unsigned short PulseShape;
	std::uint32_t PulseSlope;
	std::uint32_t PulseDuration;
	std::uint32_t Bandwidth;
	std::uint32_t TransDepht;
	std::uint32_t StartSample;
	unsigned short PlatFormid;
	unsigned short TransShape;
    std::int32_t TransFaceAlongAngleOffset;
    std::int32_t TransFaceAthwartAngleOffset;
    std::int32_t RotationAngle;
    std::int32_t TransMainBeamAlongBeamAngleOffset;
    std::int32_t TransMainBeamAthwartAngleOffset;
	std::uint32_t AbsorptionCoeff;
	std::uint32_t TransmissionPower;
	std::uint32_t AngleSensitivity;
	std::uint32_t TranAthwartAngleSensitivity;
	std::uint32_t Along3DbWidth;
	std::uint32_t Athwart3DbWidth;
    std::int32_t TwoWayBeamAngle;
	std::uint32_t TransGain;
    std::int32_t TranssACorrection;
	std::uint32_t BottomDetectionMinDepth;
	std::uint32_t BottomDetectionMaxDepth;
    std::int32_t BottomDetectionMinLevel;
	std::uint32_t InitialWBTSamplingFrequency;
	std::uint32_t TransducerImpedance;
	std::uint32_t WBTImpedance;
	unsigned short FPGAFilterIdentifier;
	unsigned short FPGADecimationFactor;
	unsigned short EK80ApplicationFilterIdentifier;
	unsigned short EK80ApplicationDecimationFactor;
	unsigned short FrequencyDependantCalibrationIdentifier;
	char remarks[40];
    std::int32_t tupleAttributes;
};

struct Hac220
{
	/// Nombre de canaux logiciels associ�s au sondeur  (fichier HAC-sbi :nbbeam)
	unsigned short  NumberSoftChannel;
	/// Vitesse du son (pr�cision 0.1)
	std::uint32_t	EchoSounderId;
	char			TransName[50];
	char			TransSoftVersion[30];
	unsigned short  SoundSpeed;
	unsigned short  TriggerMode;
	unsigned short  PingInterval;
	unsigned short  PulseForm;
	std::uint32_t   PulseDuration;
	std::uint32_t   TimeSampleInterv;
	unsigned short  FrequencyBeamSpacing;
	unsigned short FrequencySpaceShape;
	std::uint32_t  TransceiverPower;
	std::uint32_t   TransDepth;

	/// Identifiant unique du sondeur acoustique
	unsigned short   PlatFormId;
	unsigned short   TransducerShape;
    std::int32_t			 TransFaceAlongAngleOffset;
    std::int32_t			TransFaceAthwartAngleOffset;
    std::int32_t			RotationAngle;
	char			remarks[40];
    std::int32_t			tupleAttributes;
};


struct Hac2200
{
	// Identifiant unique du canal de donn�es (logiciel) 
	unsigned short  SoftwareChannelId;
	std::uint32_t	SounderId;
	char			FrequencyName[48];
	unsigned short	DataType;
	unsigned short	BeamType;
	std::uint32_t	AcousticFrequency;
	std::uint32_t	StartSample;
    std::int32_t			AlongShipSteeringAngle;
	// D�pointage dans la dimension perpendiculaire � l'avanc�e du navire (pr�cision 0.0001)
    std::int32_t			AthwartShipSteeringAngle;
	// largeur angulaire d'un faisceau perpendiculairement � l'avanc�e du navire (pr�cision 0.0001)
	std::uint32_t	AbsorptionCoefficient;
	std::uint32_t	BandWitdh;
	std::uint32_t	TransmissionPower;
	std::uint32_t	BeamAlongShiphAngleSensitivity;
	std::uint32_t	BeamArthwartShipAngleSensitivity;
	std::uint32_t	BeamAlongShip3dbWidth;
	std::uint32_t	BeamAthwart3dbWidth;
    std::int32_t			BeamEquivalentTwoWayBeamAngle;
	std::uint32_t	BeamGain;

    std::int32_t			BeamSACorrection;
	std::uint32_t	BottomDetectionMinDepth;
	std::uint32_t	BottomDetectionMaxDepth;
    std::int32_t			BottomDetectionMinLevel;
	unsigned short	AlongTXRXWeightId;
	unsigned short	ArthwartTXRXWeightId;
	unsigned short	AlongSplitBeamRXWId;
	unsigned short  ArthwartSplitBeamRXWId;
	char			remarks[40];
    std::int32_t			tupleAttributes;
};

struct Hac2210
{
	unsigned short	ID;
	unsigned int		N; // nombre de points
    std::int32_t			tupleAttributes;
};

struct Hac30
{
	/// Vitesse de navigation (precision 0.001 m s-1)
	unsigned short	TimeFraction;
	std::uint32_t	TimeCPU;
	unsigned short	NavigationSystem;
	short			Heading;
	unsigned short  NavigationSpeed;
	unsigned short  space;
    std::int32_t			tupleAttributes;
};

struct Hac41
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short	DependentAttitudeSensorId;
	unsigned short  TransChannelId;
	unsigned short  PlatformType;
	short			AlongshipOffset;
	short			AthwartshipOffset;
	short			VerticalOffset;
	char			remarks[30];
	unsigned short  space;
    std::int32_t			tupleAttributes;
};

struct Hac10110
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;

    std::int32_t			tupleAttributes;
};



struct Hac10140
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short  AttitudeSensorId;
	short			Pitch;
	short			Roll;
	short			Heave;
	short			Yaw;
	unsigned short  space;
    std::int32_t			tupleAttributes;
};
struct Hac200
{
	unsigned short	numOfChannel;
	std::uint32_t	docId;
	unsigned short	soundSpeed;
	unsigned short	pingMode;
	unsigned short	pingInterval;
	unsigned short  transmitPower;
	unsigned short	noiseMargin;
	unsigned short  sampleRange;
	unsigned short	superLayerType;
	unsigned short	superLayerNumber;
	unsigned short	superLayerRange;
    std::int32_t			superLayerStart;
	unsigned short	superLayerMargin;
	unsigned short  superLayerThreshold;
	std::uint32_t   ek500version;
	char			remarks[30];
    std::int32_t			tupleAttributes;

};


struct Hac2001
{
	unsigned short	ChannelId;
	std::uint32_t	docId;
	std::uint32_t	SamplingInterval;
	unsigned short	dataType;
	unsigned short	transceiverChannelNumber;
	std::uint32_t	AcousticFrequency;
	std::uint32_t	TransDepht;
	std::uint32_t	blankingRange;
	unsigned short	PlatFormid;
	unsigned short	TransShape;
	short			TransFaceAlongAngleOffset;
	short			TransFaceAthwartAngleOffset;
	short			RotationAngle;
	short			TransMainBeamAlongBeamAngleOffset;
	short			TransMainBeamAthwartAngleOffset;
	unsigned short	AbsorptionSound;
	unsigned short	PulseLengthMode;
	unsigned short	BandWidthMode;
	unsigned short	MaximumPower;
	unsigned short	AngleSensitivity;
	unsigned short	TranAthwartAngleSensitivity;
	unsigned short	Along3DbWidth;
	unsigned short	Athwart3DbWidth;
	short			TwoWayBeamAngle;
	unsigned short	CalibrationTransGain;
	short			BottomDetectionMinLevel;
	std::uint32_t	BottomDetectionMinDepth;
	std::uint32_t	BottomDetectionMaxDepth;

	char remarks[30];
	unsigned short space;
    std::int32_t tupleAttributes;
};

struct Hac20
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	std::uint32_t   GpsTime;
	unsigned short  PositionningSystem;
	unsigned short	space;
    std::int32_t			Latitude;
    std::int32_t			Longitude;
    std::int32_t			tupleAttributes;
};

struct Hac10142
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short	DistanceSensorID;
	unsigned short	DepthSensorId;
    std::int32_t			AlongshipDistance;
    std::int32_t			AthwartShipDistance;
    std::int32_t			Depth;
    std::int32_t			tupleAttributes;
};
struct Hac50
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short	HeadRopeDepth;
	unsigned short	VerticalOpening;
	unsigned short	HeadRopeAltitude;
	unsigned short	FootRopeAltitude;
	unsigned short	FootRopeOnBottom;
	unsigned short	HorizontalOpening1;
	unsigned short	HorizontalOpening2;
	short			PortDoorAlongShipAngle;
	short			PortDoorAthwartShipAngle;
	unsigned short	AlongShipSpeedOnFootRope;
	unsigned short	AthwartShipSpeedOnFootRope;
	unsigned short	Temperature;
	unsigned short	TrawlFill1;
	unsigned short	TrawlFill2;
	unsigned short	TrawlFill3;
	unsigned short	TrawlFill4;
    std::int32_t			tupleAttributes;
};


struct Hac42
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short	DistanceSensorID;
	unsigned short	DepthSensorId;
	unsigned short	TransChannelId;
	unsigned short	PlatformType;
	unsigned short	DistanceSensorType;
	unsigned short	DepthSensorType;
	short			AlongshipOffset;
	short			AthwartshipOffset;
	short			VerticalOffset;
	unsigned short  space;
	char			remarks[30];
	unsigned short  space2;
    std::int32_t			tupleAttributes;

};

struct Hac65534
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short  ClosingMode;
	unsigned short  space;
    std::int32_t			tupleAttributes;
};

struct Hac65535
{
	unsigned short	HacId;
	unsigned short	HacVersion;
	unsigned short  AcqSoftwareVersion;
	std::uint32_t	AcqSoftwareID;
    std::int32_t			tupleAttributes;
};

struct Hac10100
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short  SoftwareChannelID;
	unsigned short  TVGMaxRange;
	unsigned short  TVGMinRange;
	unsigned short  TVTEvaluationMode;
	unsigned short  TVTEvaluationInterval;
	unsigned short  TVTEvaluationNumberOfPing;
	std::uint32_t	TVTStartPingNumber;
    std::int32_t			TVTParameter;
	std::uint32_t   TVTAmpParameter;
    std::int32_t			tupleAttributes;
};
struct Hac11000
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short  SensorType;
	unsigned short  NumberOfMeasure;

    std::int32_t			tupleAttributes;
};
struct Hac11000Record
{
	std::uint32_t	Pressure;
    std::int32_t			Temperature;
	unsigned short	Conductivity;
	unsigned short	SpeedOfSound;
	std::uint32_t   DepthFromSurface;
	std::uint32_t   Salinity;
	std::uint32_t	SoundAbsorption;
};

struct Hac10011
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short	SoftwareChannelId;
	unsigned short	TransceiverMode;
	std::uint32_t	PingNumber;
    std::int32_t			DetectedBottomRange;
	std::uint32_t	NumberOfSample;

    std::int32_t			tupleAttributes;
};

struct Hac10090
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short	ParentSubChannelId;
	unsigned short	space;
	std::uint32_t	PingNumber;
	std::uint32_t	SelectStartRange;
	std::uint32_t	SelectEndRange;
    std::int32_t			DetectedBottomRange;
	std::uint32_t	NumberOfTarget;


    std::int32_t tupleAttributes;
};

struct Hac10090Target
{
    std::int32_t Range;
	short CompensatedTS;
	short UnCompensatedTS;
	short AlongShipAngle;
	short AthwartShipAngle;

};

struct Hac4000
{
	unsigned short	TimeFraction;
	std::uint32_t	TimeCpu;
	unsigned short	ParentSoftwareId;
	unsigned short	DetectedSTParameterSubChannelId;
	short			MinimumValue;
	unsigned short	MinimumEchoLength;
	unsigned short	MaximumEchoLength;
	unsigned short	MaximumGainCompensation;
	unsigned short	MaximumPhaseCompensation;
	char			Remark[30];
    std::int32_t			tupleAttributes;

};

#endif //TUP_STRUCT_DEF
