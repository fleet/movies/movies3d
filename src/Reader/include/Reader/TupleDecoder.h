#pragma once
#include "Reader/HACtuple.h"
#include "ReaderExport.h"
#include "Reader/MovStream.h"

#include <map>
typedef	 std::map<int, HACTuple*> MapTuple;

class READER_API TupleDecoder
{
public:
	TupleDecoder(void);
	~TupleDecoder(void);
	/**
	la fonction Decode va decoder les donn�es contenues dans un Stream
	*/
	void Decode(ReaderTraverser &ref, MovStream *pStream);
private:
	MapTuple m_tuple;
	// OTK - 21/12/2009 - detection de la lecture des premiers pings pour clore la definition
	// des sondeurs m�me si le nombre de voies est incoh�rent.
	MapTuple m_pingTuples;

};
