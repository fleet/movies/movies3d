/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		WriteAlgorithm.h												  */
/******************************************************************************/
#ifndef WRITE_ALGORITHM
#define WRITE_ALGORITHM

#include "Reader/ReaderExport.h"
#include "Reader/MovStream.h"

#include "M3DKernel/algorithm/base/EchoAlgorithm.h"
#include "M3DKernel/datascheme/DateTime.h"
#include "M3DKernel/datascheme/PingFan.h"

#include <stdio.h>
#include <fstream>

class Sounder;
class MovReadService;
class WriteAlgorithmToFileHac;
class WriteAlgorithmToFileNetCDF;
class EchoIntegrationModule;
class EchoIntegrationOutput;

// OTK - 08/06/2009 - classe utilis�e pour faire le tri des ping par date
class READER_API LinkedHacTime : public HacTime
{
public:
	LinkedHacTime() :HacTime() {};
	virtual ~LinkedHacTime() {};

	PingFan * m_pLinkedPingFan;
};

class READER_API WriteAlgorithm : public EchoAlgorithm
{
public:
	enum class OutputType : uint8_t {
		None = 0x0,
		Hac = 0x1,
		NetCDF = 0x2,
		Both = Hac | NetCDF,
		EI = 0x4,
		NetCDF_EI = NetCDF | EI
	};

	MovCreateMacro(WriteAlgorithm);
	virtual ~WriteAlgorithm();

	void SetFilePrefix(const std::string & filePrefix);
	void SetDirectory(const std::string & dirName);

	void SetReaderService(MovReadService *pReadService);

	void CloseFile() { CloseStream(); setEnable(false); }
	void SetUseSort(const bool& sort);

	void SetOutputType(const OutputType& outputType);

	void SetFileMaxSize(const uint32_t& sizeInOctets);
	void SetFileMaxTime(const uint32_t& timeInSeconds);

	void SetArchivePreviousPings(const bool& archivePreviousPings);

	void AddEchoIntegration(const Sounder& sounder, const EchoIntegrationModule& eiModule, const EchoIntegrationOutput& eiOutput);

protected:
	WriteAlgorithm();

	inline virtual void PingFanAdded(PingFan *pFan);
	inline virtual void SounderChanged(std::uint32_t sounderId);

	virtual void StreamClosed(const char *streamName);
	virtual void StreamOpened(const char *streamName);
	void WritePingFan(PingFan* pingFan, Sounder* souder);

	virtual void onEnableStateChange() override;	
	virtual void onInputModuleEnableStateChange(EchoAlgorithm* algo) override;

private:
	OutputType m_outputType;
	std::unique_ptr<WriteAlgorithmToFileHac> m_writeToHac;
	std::unique_ptr<WriteAlgorithmToFileNetCDF> m_writeToNetCDF;

	MovReadService		*m_pReadService;

	/// Ce flag permet de trier les données avant insertion lors de l'initialisation de de l'écriture
	bool	m_bUseSort;

	/// Flag pour activer l'écriture des précédents pings lors de l'activation de l'écriture
	bool m_archivePreviousPings;

	std::string m_filePrefix;
	std::string m_directoryRecord;

	bool hacOutputEnabled() const;
	bool netCDFOutputEnabled() const;
	bool echoIntegrationOutputEnabled() const;

	void CloseStream();
	void OpenStream(const char* fileName);

	void CheckSort();
};

#endif //WRITE_ALGORITHM
