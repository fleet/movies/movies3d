#ifndef WRITE_ALGORITHM_TO_FILE
#define WRITE_ALGORITHM_TO_FILE

#include "M3DKernel/datascheme/DateTime.h"

#include <string>
#include <set>

#include "M3DKernel/datascheme/DatedObject.h"

class Sounder;
class PingFan;
class SoftChannel;
class Transducer;
class Environnement;
class NavAttributes;
class NavPosition;
class NavAttitude;
class TrawlPositionAttitude;
class DynamicPlatformPosition;
class TrawlSensorPositionId;
class TrawlPlatformPosition;
class EventMarker;
class EchoIntegrationOutput;
class EchoIntegrationModule;

class WriteAlgorithmToFile
{
public:
	WriteAlgorithmToFile();
	virtual ~WriteAlgorithmToFile() = default;

	virtual void SounderChanged(const uint32_t& sounderId) = 0;
	virtual void WritePingFan(PingFan* pingFan, Sounder* sounder);

	virtual void Close() = 0;
	virtual void Open(const std::string& sourcePath);

	void SetUseSort(const bool& sort);
	
	const std::string& GetSourcePath() const;

	void SetFileMaxSize(const uint32_t& sizeInOctets);
	void SetFileMaxTime(const uint32_t& timeInSeconds);

	virtual void AddEchoIntegration(const Sounder& sounder, const EchoIntegrationModule& eiModule, const EchoIntegrationOutput& eiOutput) {};

	void UpdateWrittenDatedObjects();

protected:
	virtual std::string GetFileExtension() const = 0;
	const std::string AdaptExtension(const std::string & fileName) const;

	bool FindSmallestDate(HacTime start, HacTime stop, HacTime &dateFound);
	unsigned int WriteDatedObject(HacTime date);
	
	virtual void ChangeFile();

	virtual bool WriteBeamPingFan(PingFan* pingFan, SoftChannel* softChannel, Transducer* transducer, const size_t& transducerNum, const size_t& beamNum) = 0;
	virtual bool WriteEnvironment(Environnement* environment) = 0;
	virtual bool WriteNavAttributes(NavAttributes* navAttributes) = 0;
	virtual bool WriteNavPosition(NavPosition* navPosition) = 0;
	virtual bool WriteNavAttitude(NavAttitude* navAttitude) = 0;
	virtual bool WriteTrawlPositionAttitude(TrawlPositionAttitude* trawlPositionAttitude) = 0;
	virtual bool WriteDynamicPlatformPosition(DynamicPlatformPosition* dynamicPlatformPosition, const TrawlSensorPositionId& sensorId) = 0;
	virtual bool WriteTrawlPlatformPosition(TrawlPlatformPosition* trawlPlatformPosition) = 0;
	virtual bool WriteEventMarker(EventMarker* eventMarker) = 0;

	HacTime m_LastPingDateWritten;
	bool	m_bUseSort;


	// Taille max par fichier en octets
	uint32_t m_fileMaxSize;

	// Durée max par fichier en secondes
	uint32_t m_fileMaxTime;

private:
	std::string m_sourcePath;

	// Liste des objets datés écrits
	std::set<DatedObject*> m_writtenDatedObjects;

	// Vérification si l'objet a déjà été écrit
	bool CheckIfAlreadyWritten(const DatedObject* const dated_object);
};

#endif //WRITE_ALGORITHM_TO_FILE
