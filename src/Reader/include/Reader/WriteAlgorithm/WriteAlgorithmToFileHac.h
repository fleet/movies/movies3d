#ifndef WRITE_ALGORITHM_TO_FILE_HAC
#define WRITE_ALGORITHM_TO_FILE_HAC

#include "Reader/WriteAlgorithm/WriteAlgorithmToFile.h"
#include "Reader/MovStream.h"

#include <fstream>
#include <memory>

class WriteAlgorithmToFileHac : public WriteAlgorithmToFile
{
public:
	WriteAlgorithmToFileHac();
	~WriteAlgorithmToFileHac();

	void SounderChanged(const uint32_t& sounderId) override;

	void Close() override;
	void Open(const std::string& fileName) override;

private:
	std::unique_ptr<std::ofstream> m_outputStream;
	MovStream m_StreamBuffer;

	bool m_bEnvInHeader; // pour compatibilit� avec des fichiers HAC o� le tuple env n'est pas dans l'ent�te
	bool m_bPlatFormAttitudeInHeader; // on suppose que le tup42 est dans l'ent�te pour tous les sondeurs (cas hermes) ou pour aucun
	
	virtual bool WriteBeamPingFan(PingFan* pingFan, SoftChannel* softChannel, Transducer* transducer, const size_t& transducerNum, const size_t& beamNum) override;
	virtual bool WriteEnvironment(Environnement* environment) override;
	virtual bool WriteNavAttributes(NavAttributes* navAttributes) override;
	virtual bool WriteNavPosition(NavPosition* navPosition) override;
	virtual bool WriteNavAttitude(NavAttitude* navAttitude) override;
	virtual bool WriteTrawlPositionAttitude(TrawlPositionAttitude* trawlPositionAttitude) override;
	virtual bool WriteDynamicPlatformPosition(DynamicPlatformPosition* dynamicPlatformPosition, const TrawlSensorPositionId& sensorId) override;
	virtual bool WriteTrawlPlatformPosition(TrawlPlatformPosition* trawlPlatformPosition) override;
	virtual bool WriteEventMarker(EventMarker* eventMarker) override;

	void FlushStream(std::uint32_t size);

	std::string GetFileExtension() const override;
};

#endif //WRITE_ALGORITHM_TO_FILE_HAC
