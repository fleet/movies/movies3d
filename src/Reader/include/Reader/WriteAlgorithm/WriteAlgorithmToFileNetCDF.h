#ifndef WRITE_ALGORITHM_TO_FILE_NETCDF
#define WRITE_ALGORITHM_TO_FILE_NETCDF

#include "Reader/WriteAlgorithm/WriteAlgorithmToFile.h"
#include "EchoIntegration/EchoIntegrationParameter.h"

#include <memory>

namespace sonarNetCDF {
	class ncFile;
	class GroupTopLevel;
	class GroupSonar;
}

class WriteAlgorithmToFileNetCDF : public WriteAlgorithmToFile 
{
public:
	WriteAlgorithmToFileNetCDF();
	~WriteAlgorithmToFileNetCDF();

	void SounderChanged(const uint32_t& sounderId) override;
	void WritePingFan(PingFan* pingFan, Sounder* sounder) override;

	void AddEchoIntegration(const Sounder& sounder, const EchoIntegrationModule& eiModule, const EchoIntegrationOutput& eiOutput) override;

	void Close() override;
private:
	std::string GetFileExtension() const override;

	void createSounderFile(const Sounder& sounder);
	void createSounderFile(const std::string& filePath, const int& sounderId);

	void initSounderFile(const Sounder& sounder);

	sonarNetCDF::GroupSonar* initSonarGroup(sonarNetCDF::GroupTopLevel* topLevelGroup, const Sounder& sounder);
	void initGridGroups(sonarNetCDF::GroupSonar* sonarGroup, const Sounder& sounder, const EchoIntegrationModule& eiModule, const EchoIntegrationOutput& eiOutput);

	virtual void ChangeFile() override;

	virtual bool WriteBeamPingFan(PingFan* pingFan, SoftChannel* softChannel, Transducer* transducer, const size_t& transducerNum, const size_t& beamNum) override;
	virtual bool WriteEnvironment(Environnement* environment) override;
	virtual bool WriteNavAttributes(NavAttributes* navAttributes) override;
	virtual bool WriteNavPosition(NavPosition* navPosition) override;
	virtual bool WriteNavAttitude(NavAttitude* navAttitude) override;
	virtual bool WriteTrawlPositionAttitude(TrawlPositionAttitude* trawlPositionAttitude) override;
	virtual bool WriteDynamicPlatformPosition(DynamicPlatformPosition* dynamicPlatformPosition, const TrawlSensorPositionId& sensorId) override;
	virtual bool WriteTrawlPlatformPosition(TrawlPlatformPosition* trawlPlatformPosition) override;
	virtual bool WriteEventMarker(EventMarker* eventMarker) override;

	std::map<uint32_t, std::unique_ptr<sonarNetCDF::ncFile>> m_sounderFiles;
	std::map<uint32_t, std::unique_ptr<sonarNetCDF::GroupTopLevel>> m_sounderTopLevelGroup;
};

#endif //WRITE_ALGORITHM_TO_FILE_NETCDF
