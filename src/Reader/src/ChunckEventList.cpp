
#include "Reader/ChunckEventList.h"

ChunckEventList::ChunckEventList(void)
{
	Reset();
}

ChunckEventList::~ChunckEventList(void)
{
}
void ChunckEventList::Reset()
{
	m_fanAdded = false;
	m_sounderChanged = false;
	m_streamClosed = false;
	m_streamStreamOpened = false;
	m_singleTargetAdded = false;
	m_esuEnded = false;
	m_tupleHeaderUpdate = false;
}
