
#include "Reader/ChunkEval.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

namespace
{
	constexpr const char * LoggerName = "Reader.Chunk.ChunkEval";
}


ChunkEval::ChunkEval(const std::string & name)
	: EchoAlgorithm(name)
	, m_Abort(false)
{
}

ChunkEvalNumbered::ChunkEvalNumbered() 
	: ChunkEval("ChunkEvalNumbered")
	, m_bShouldStop(false)
	// OTK - 08/03/2010 - par d�faut, on se base sur le rythme de lecture du sondeur 1
	, m_SounderChecked(1)
	, m_NumberExpected(1)
{
}

void ChunkEvalNumbered::Start()
{
	m_mapNbFrame.clear();
	m_bShouldStop = false;
	m_CurrentEventList.Reset();
	m_Abort = false;
}

bool ChunkEvalNumbered::CheckStop()
{
	if (m_bShouldStop || m_Abort)
		return true;

	M3DKernel *pKernel = M3DKernel::GetInstance();
	Sounder *pSounderChecked = pKernel->getObjectMgr()->GetSounderDefinition().GetSounderWithId(m_SounderChecked);
	if (pSounderChecked)
	{
		// sounder known, check if we got enough data
		MapFrameAdded::iterator res = m_mapNbFrame.find(m_SounderChecked);
		if (res != m_mapNbFrame.end())
		{
			unsigned int num = res->second;
			if (num >= m_NumberExpected)
			{
				m_bShouldStop = true;
			}
		}
	}
	else
	{
		// OTK - 08/03/2010 - en cas de sondeur de r�f�rence non d�fini,
		// on arr�te la lecture du chunk d�s qu'un sondeur respecte le nombre
		// de pings lus (afin d'�viter les chunks trop longs si un sondeur 
		// n'�met pas).
		bool oneOK = false;
		// sounder unknown, check for all
		MapFrameAdded::iterator res = m_mapNbFrame.begin();
		while (res != m_mapNbFrame.end())
		{
			unsigned int num = res->second;
			if (num >= m_NumberExpected)
			{
				oneOK = true;
			}
			res++;
		}
		if (m_mapNbFrame.size() > 0)
		{
			m_bShouldStop = oneOK;
		}
	}
	return m_bShouldStop;
}

void ChunkEvalNumbered::Stop()
{
}

void ChunkEvalNumbered::Init(std::uint32_t SounderId, unsigned int num)
{
	m_SounderChecked = SounderId;
	m_NumberExpected = num;
}

void ChunkEvalNumbered::PingFanAdded(PingFan *pFan)
{
	ChunkEval::PingFanAdded(pFan);
	MapFrameAdded::iterator res = m_mapNbFrame.find(pFan->m_pSounder->m_SounderId);
	if (res != m_mapNbFrame.end())
	{
		unsigned int num = res->second;
		res->second = num + 1;
	}
	else
	{
		m_mapNbFrame.insert(MapFrameAdded::value_type(pFan->m_pSounder->m_SounderId, 1));
	}
}

void ChunkEvalNumbered::SounderChanged(std::uint32_t sounderId)
{
	ChunkEval::SounderChanged(sounderId);
	// OTK - 18/01/2010 - on n'arr�te pas la lecture du chunk sur l'�v�nement SounderChanged
	/*
	  if(sounderId==m_SounderChecked)
	  {
		  m_bShouldStop=true;
		  M3D_LOG_INFO(LoggerName, "Sounder reconfiguration End of ReadChunk");
	  }*/
	MapFrameAdded::iterator res = m_mapNbFrame.find(sounderId);
	if (res == m_mapNbFrame.end())
	{
		m_mapNbFrame.insert(MapFrameAdded::value_type(sounderId, 0));
	}
}

void ChunkEvalNumbered::StreamClosed(const char *streamName)
{
	ChunkEval::StreamClosed(streamName);
	m_bShouldStop = true;
	M3D_LOG_INFO(LoggerName, "Stream Changed : End of ReadChunk");
}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

ChunkEvalTimed::ChunkEvalTimed() 
	: ChunkEval("ChunkEvalTimed")
	, m_bShouldStop(false)
	, m_NumberOfSecond(0)
{
	m_StartDate.SetToNull();
	m_OlderDate.SetToNull();
}

void ChunkEvalTimed::Start()
{
	m_Abort = false;
	m_bShouldStop = false;
	m_StartDate.SetToNull();
	m_OlderDate.SetToNull();
}

bool ChunkEvalTimed::CheckStop()
{
	if (!m_StartDate.IsNull())
	{
		HacTime stop = m_StartDate;
		stop.m_TimeCpu += m_NumberOfSecond;
		if (stop < m_OlderDate)
		{
			m_bShouldStop = true;
		}
	}
	return m_bShouldStop;
}

void ChunkEvalTimed::Stop()
{
	m_StartDate.SetToNull();
	m_OlderDate.SetToNull();
}

void ChunkEvalTimed::Init(unsigned int numSecond)
{
	m_NumberOfSecond = numSecond;
	m_StartDate.SetToNull();
	m_OlderDate.SetToNull();
}

void ChunkEvalTimed::PingFanAdded(PingFan *pFan)
{
	ChunkEval::PingFanAdded(pFan);
	M3DKernel *pKernel = M3DKernel::GetInstance();
	Sounder *pSounderChecked = pKernel->getObjectMgr()->GetSounderDefinition().GetSounderWithId(pFan->m_pSounder->m_SounderId);
	if (pSounderChecked)
	{
		TimeObjectContainer *pTime = pKernel->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(pSounderChecked->m_SounderId);
		unsigned int numFan = pTime->GetObjectCount();
		if (numFan > 0)
		{
			if (pFan->m_ObjectTime > m_OlderDate)
			{
				m_OlderDate = pFan->m_ObjectTime;
				if (m_StartDate.IsNull())
				{
					m_StartDate = m_OlderDate;
				}
			}
		}
	}
}

void ChunkEvalTimed::StreamClosed(const char *streamName)
{
	ChunkEval::StreamClosed(streamName);
	m_bShouldStop = true;
	M3D_LOG_INFO(LoggerName, "Stream Changed : End of ReadChunk");
}

