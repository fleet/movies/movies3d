﻿#include "Reader/ComplexSamplesDecoder.h"
#include "Reader/ReaderCtrl.h"
#include "Reader/ReaderTraverser.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Environnement.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/TransmitSignalObject.h"
#include "M3DKernel/datascheme/SpectralAnalysisDataObject.h"

#include "BaseMathLib/SignalUtils.h"
#include "BaseMathLib/FFT.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include <numeric>

namespace
{
	constexpr const char * LoggerName = "Reader.Utils.ComplexSamplesDecoder";
}

void ReaderUtils::processComplexSamples(ReaderTraverser & trav, std::vector<std::complex<float>> & samples, Sounder * sound, SimradBeamType beamType, int nbQuadrants, MovObjectPtr<BeamDataObject> beamData, MovObjectPtr<PhaseDataObject> phaseData)
{
	TimeObjectContainer *pContainer = trav.m_pHacObjectMgr->GetEnvironnementContainer();	
	MovObjectPtr<Environnement> pEnv = (Environnement*)pContainer->GetLastDatedObject();
	if (!pEnv || pEnv->GetNumberOfMeasure() == 0)
	{
		M3D_LOG_WARN(LoggerName, "No environmental data required to decode complexe samples, use default environment values");
		pEnv = MovObjectPtr<Environnement>::make();

		const auto & kernelParameters = M3DKernel::GetInstance()->GetRefKernelParameter();

		Measure meas;
		meas.Temperature = kernelParameters.getTemperature();
		meas.SpeedOfSound = kernelParameters.getSpeedOfSound();
		meas.Salinity = kernelParameters.getSalinity();
		pEnv->AddMeasure(meas);
	}

	int nbData = samples.size() / nbQuadrants;

	SpectralAnalysisDataObject * spectralAnalysisDataObject = nullptr;
	Transducer * pTrans = sound->getTransducerForChannel(beamData->m_hacChannelId);
	SoftChannel * pSoftChan = pTrans->getSoftChannel(beamData->m_hacChannelId);
	TransmitSignalObject * pTransmitSignal = pSoftChan->m_softChannelComputeData.GetTransmitSignalObject(pTrans, pSoftChan);

	QuadrantDataObject * quadrantDataObject = nullptr;
	M3DKernel *pKern = M3DKernel::GetInstance();
	const auto & kernelParameter = pKern->GetRefKernelParameter();
	if (kernelParameter.getKeepQuadrantsInMemory())
	{
		quadrantDataObject = new QuadrantDataObject();
		quadrantDataObject->samples.resize(nbQuadrants);
		for (int q = 0; q < nbQuadrants; ++q)
		{
			auto & qSamples = quadrantDataObject->samples[q];
			qSamples.resize(nbData);
		}

		// copy data
		auto pComplexSample = (std::complex<float>*)(samples.data());
		for (int i = 0; i < nbData; ++i)
		{
			for (int q = 0; q < nbQuadrants; ++q)
			{
				quadrantDataObject->samples[q][i] = *pComplexSample++;
			}
		}
	}

	if (pTrans->m_pulseShape == 2) // FM mode
	{
		ReaderCtrl *pRead = ReaderCtrl::getInstance();

		//sv(f) computation
		const auto & params = M3DKernel::GetInstance()->GetSpectralAnalysisParameters();

		//sv computation		
		if (params.isPulseCompressionEnabled())
		{
			pTransmitSignal->PerformPulseCompression(samples, params.getFrequencyResolution(), nbQuadrants);

			if (phaseData)
			{
				const double frequencyCenter = (pSoftChan->m_startFrequency + pSoftChan->m_endFrequency) / 2.0;
				const double frequency = pSoftChan->m_acousticFrequency;
				auto pSamples = samples.data();
				const size_t nbSamples = samples.size() / (2 * nbQuadrants);
				Phase phase;
				const double alongSensitivity = pSoftChan->m_beamAlongAngleSensitivity * frequencyCenter / frequency;
				const double athwartSensitivity = pSoftChan->m_beamAthwartAngleSensitivity * frequencyCenter / frequency;

				if (beamType == BeamTypeSplit3)
				{
					for (size_t i = 0; i < nbSamples; i++)
					{
						const std::complex<double> q1 = *pSamples++;
						const std::complex<double> q2 = *pSamples++;
						const std::complex<double> q3 = *pSamples++;

						const double phi31 = atan2(q3.imag() * q1.real() - q3.real() * q1.imag(), q3.real() * q1.real() + q3.imag() * q1.imag());
						const double phi32 = atan2(q3.imag() * q2.real() - q3.real() * q2.imag(), q3.real() * q2.real() + q3.imag() * q2.imag());

						short along = (short)round(100.0*RAD_TO_DEG(phi31 + phi32) / (sqrt(3)*alongSensitivity));
						short athwart = (short)round(100.0*RAD_TO_DEG(phi32 - phi31) / athwartSensitivity);

						phase.SetRawValues(along, athwart);
						phaseData->push_back(phase);
					}
				}
				else if (beamType == BeamTypeSplit3CN)
				{
					for (size_t i = 0; i < nbSamples; i++)
					{
						std::complex<double> q1 = *pSamples++;
						std::complex<double> q2 = *pSamples++;
						std::complex<double> q3 = *pSamples++;
						std::complex<double> q4 = *pSamples++;

						q1 += q4;
						q2 += q4;
						q3 += q4;

						const double phi31 = atan2(q3.imag() * q1.real() - q3.real() * q1.imag(), q3.real() * q1.real() + q3.imag() * q1.imag());
						const double phi32 = atan2(q3.imag() * q2.real() - q3.real() * q2.imag(), q3.real() * q2.real() + q3.imag() * q2.imag());

						short along = (short)round(100.0*RAD_TO_DEG(phi31 + phi32) / (sqrt(3)*alongSensitivity));
						short athwart = (short)round(100.0*RAD_TO_DEG(phi32 - phi31) / athwartSensitivity);

						phase.SetRawValues(along, athwart);
						phaseData->push_back(phase);
					}
				}
				else if (beamType == BeamTypeSplit)
				{
					for (size_t i = 0; i < nbSamples; i++)
					{
						const std::complex<double> q1 = *pSamples++;
						const std::complex<double> q2 = *pSamples++;
						const std::complex<double> q3 = *pSamples++;
						const std::complex<double> q4 = *pSamples++;

						const double alongRad = std::arg((q3 + q4) * std::conj(q1 + q2));
						const double athwartRad = std::arg((q1 + q4) * std::conj(q2 + q3));

						short along = (short)round(100.0*RAD_TO_DEG(alongRad) / alongSensitivity);
						short athwart = (short)round(100.0*RAD_TO_DEG(athwartRad) / athwartSensitivity);

						phase.SetRawValues(along, athwart);
						phaseData->push_back(phase);
					}
				}
			}

			sumQuadrantValues(samples, nbQuadrants);

			spectralAnalysisDataObject = new SpectralAnalysisDataObject();
			spectrumComputation(sound, pTrans, pSoftChan, pEnv.get(), params.getFrequencyResolution()
				, samples
				, pTransmitSignal->GetFilt()
				, pTransmitSignal->GetSignalSquaredNorm()
				, *spectralAnalysisDataObject
				, nbQuadrants);
		}
		else
		{
			sumQuadrantValues(samples, nbQuadrants);
		}

		const size_t nbSamples = samples.size();

		const double powerFactor = 0.5 * pow((double)(pTrans->m_WBTImpedance + pTrans->m_transducerImpedance) / pTrans->m_WBTImpedance, 2.0) / pTrans->m_transducerImpedance;
		const double pulseLength = ((double)pTrans->m_pulseDuration) / 1000000;
		const double tvgDelay = pulseLength * sound->m_soundVelocity / 2;
		const double startTvg = std::max(tvgDelay, (double)1);
		const double rangeCWOffset = (pTrans->m_pulseShape == 2) ? 0 : sound->m_soundVelocity * pulseLength / 4;
		const double beamSampleSpacing = pTrans->getBeamsSamplesSpacing();
		const double frequencyCenter = (pSoftChan->m_startFrequency + pSoftChan->m_endFrequency) / 2.0;
		const double frequency = pSoftChan->m_acousticFrequency;
		const double absorptionCoefficients = pEnv->GetMeasure(0)->ComputeAbsorptionCoefficient(frequencyCenter);
		const double effectivePulseLength = pTransmitSignal->GetEffectivePulseLength();
		const double svOffset = -10 * log10(pSoftChan->m_transmissionPower*pow(sound->m_soundVelocity / frequencyCenter, 2)*sound->m_soundVelocity / (32 * PI*PI)) 
			- 2 * (pSoftChan->m_beamGain + 20 * log10(frequencyCenter / frequency)) 
			- 10 * log10(effectivePulseLength) 
			- (pSoftChan->m_beamEquTwoWayAngle + 20 * log10(frequency / frequencyCenter));

		for (size_t i = 0; i < nbSamples; i++)
		{
			const double power = std::norm(samples[i]) * (1 / (nbQuadrants*4.0)) * powerFactor;
			beamData->push_back_power(power);

			const double phase = RAD_TO_DEG(std::arg(samples[i]));
			short phase_deg = (short)round(round(100.0 *phase));
			beamData->push_back_angle(phase_deg);

			const double tvgrange = i*beamSampleSpacing;

			// sv
			double sv = 10 * log10(power) + svOffset;

			// tvg20
			if (tvgrange > startTvg)
			{
				sv += 20 * log10(tvgrange - rangeCWOffset) + 2 * absorptionCoefficients*(tvgrange - rangeCWOffset);
			}

			if (std::numeric_limits<short>::min() < sv && sv <= std::numeric_limits<short>::max())
			{
				beamData->push_back_amplitude((short)round(sv * 100));
			}
			else
			{
				beamData->push_back_amplitude(UNKNOWN_DB);
			}
		}
	}
	else //if (pTrans->m_pulseShape != 2)
	{
		//CW Mode
		if (phaseData)
		{
			phaseData->reserve(nbData);
		}

		const size_t nbSamples = samples.size() / nbQuadrants;

		//double power, sv, realpart, imagpart, tvgrange;
		const double powerFactor = 0.5 * pow((double)(pTrans->m_WBTImpedance + pTrans->m_transducerImpedance) / pTrans->m_WBTImpedance, 2.0) / pTrans->m_transducerImpedance;
		const double pulseLength = ((double)pTrans->m_pulseDuration) / 1000000;
		const double tvgDelay = pulseLength * sound->m_soundVelocity / 2;
		const double startTvg = std::max(tvgDelay, (double)1);
		const double rangeCWOffset = (pTrans->m_pulseShape == 2) ? 0 : sound->m_soundVelocity * pulseLength / 4;
		const double beamSampleSpacing = pTrans->getBeamsSamplesSpacing();
		const double frequencyCenter = (pSoftChan->m_startFrequency + pSoftChan->m_endFrequency) / 2.0;
		const double frequency = pSoftChan->m_acousticFrequency;
		const double absorptionCoefficients = pEnv->GetMeasure(0)->ComputeAbsorptionCoefficient(frequencyCenter);
		const double effectivePulseLength = pTransmitSignal->GetEffectivePulseLength();
		const double svOffset = -10 * log10(pSoftChan->m_transmissionPower*pow(sound->m_soundVelocity / frequencyCenter, 2)*sound->m_soundVelocity / (32 * PI*PI)) - 2 * (pSoftChan->m_beamGain + 20 * log10(frequencyCenter / frequency)) - 10 * log10(effectivePulseLength) - (pSoftChan->m_beamEquTwoWayAngle + 20 * log10(frequency / frequencyCenter));

		auto pSamples = samples.data();
		Phase phase;
		const double alongSensitivity = pSoftChan->m_beamAlongAngleSensitivity * frequencyCenter / frequency;
		const double athwartSensitivity = pSoftChan->m_beamAthwartAngleSensitivity * frequencyCenter / frequency;

		if (beamType == BeamTypeSplit3)
		{
			for (size_t i = 0; i < nbSamples; i++)
			{
				const std::complex<double> q1 = *pSamples++;
				const std::complex<double> q2 = *pSamples++;
				const std::complex<double> q3 = *pSamples++;
				
				// power
				const std::complex<double> qsum = q1 + q2 + q3;
				const double power = std::norm(qsum) * (1 / (nbQuadrants * 4.0)) * powerFactor;
				beamData->push_back_power(power);

				const double tvgrange = i*beamSampleSpacing;
				// sv
				double sv = 10 * log10(power) + svOffset;

				// tvg20
				if (tvgrange > startTvg)
				{
					sv += 20 * log10(tvgrange - rangeCWOffset) + 2 * absorptionCoefficients*(tvgrange - rangeCWOffset);
				}

				if (sv <= std::numeric_limits<short>::min() < sv && sv <= std::numeric_limits<short>::max())
				{
					beamData->push_back_amplitude((short)round(sv * 100));
				}
				else
				{
					beamData->push_back_amplitude(UNKNOWN_DB);
				}

				if (phaseData)
				{
					short along = (short)round(100.0*RAD_TO_DEG(  atan2(q3.imag() * q1.real() - q3.real() * q1.imag(), q3.real() * q1.real() + q3.imag() * q1.imag())) / alongSensitivity);
					short athwart = (short)round(100.0*RAD_TO_DEG(atan2(q3.imag() * q2.real() - q3.real() * q2.imag(), q3.real() * q2.real() + q3.imag() * q2.imag())) / athwartSensitivity);
					 
					phase.SetRawValues(along, athwart);
					phaseData->push_back(phase);
				}
			}
		}
		else if (beamType == BeamTypeSplit3CN)
		{
			for (size_t i = 0; i < nbSamples; i++)
			{
				std::complex<double> q1 = *pSamples++;
				std::complex<double> q2 = *pSamples++;
				std::complex<double> q3 = *pSamples++;
				std::complex<double> q4 = *pSamples++;

				// power
				const std::complex<double> secsum = q1 + q2 + q3 + q4;
				const double power = std::norm(secsum) * (1 / (nbQuadrants * 4.0)) * powerFactor;
				beamData->push_back_power(power);

				const double tvgrange = i*beamSampleSpacing;
				
				// sv
				double sv = 10 * log10(power) + svOffset;

				// tvg20
				if (tvgrange > startTvg)
				{
					sv += 20 * log10(tvgrange - rangeCWOffset) + 2 * absorptionCoefficients*(tvgrange - rangeCWOffset);
				}

				if (std::numeric_limits<short>::min() < sv && sv <= std::numeric_limits<short>::max())
				{
					beamData->push_back_amplitude((short)round(sv * 100));
				}
				else
				{
					beamData->push_back_amplitude(UNKNOWN_DB);
				}

				q1 += q4;
				q2 += q4;
				q3 += q4;

				if (phaseData)
				{
					const double phi31 = atan2(q3.imag() * q1.real() - q3.real() * q1.imag(), q3.real() * q1.real() + q3.imag() * q1.imag());
					const double phi32 = atan2(q3.imag() * q2.real() - q3.real() * q2.imag(), q3.real() * q2.real() + q3.imag() * q2.imag());

					short along = (short)round(100.0*RAD_TO_DEG(phi31 + phi32) / (sqrt(3)*alongSensitivity));
					short athwart = (short)round(100.0*RAD_TO_DEG(phi32 - phi31) / athwartSensitivity);

					phase.SetRawValues(along, athwart);
					phaseData->push_back(phase);
				}
			}
		}
		else if (beamType == BeamTypeSplit)
		{
			for (size_t i = 0; i < nbSamples; i++)
			{
				const std::complex<double> q1 = *pSamples++;
				const std::complex<double> q2 = *pSamples++;
				const std::complex<double> q3 = *pSamples++;
				const std::complex<double> q4 = *pSamples++;

				// power
				const std::complex<double> qsum = q1 + q2 + q3 + q4;
				const double power = std::norm(qsum) * (1 / (nbQuadrants * 4.0)) * powerFactor;
				beamData->push_back_power(power);

				const double tvgrange = i*beamSampleSpacing;
				// sv
				double sv = 10 * log10(power) + svOffset;

				// tvg20
				if (tvgrange > startTvg)
				{
					sv += 20 * log10(tvgrange - rangeCWOffset) + 2 * absorptionCoefficients*(tvgrange - rangeCWOffset);
				}

				if (std::numeric_limits<short>::min() < sv && sv <= std::numeric_limits<short>::max())
				{
					beamData->push_back_amplitude((short)round(sv * 100));
				}
				else
				{
					beamData->push_back_amplitude(UNKNOWN_DB);
				}

				if (phaseData)
				{
					// d�codage donn�es de phase en 100 �mes de degr�s
					const double alongRad = std::arg((q3 + q4) * std::conj(q1 + q2));
					const double athwartRad = std::arg((q1 + q4) * std::conj(q2 + q3));

					short along = (short)round(100.0*RAD_TO_DEG(alongRad) / alongSensitivity);
					short athwart = (short)round(100.0*RAD_TO_DEG(athwartRad) / athwartSensitivity);

					phase.SetRawValues(along, athwart);
					phaseData->push_back(phase);
				}
			}
		}
	}

	sound->AddBeamDataObject(beamData.get(), phaseData.get(), spectralAnalysisDataObject, quadrantDataObject);
}


void ReaderUtils::sumQuadrantValues(std::vector<std::complex<float>> & complexSamples, unsigned int nbQuandrants)
{
	assert(nbQuandrants > 0);

	const int nbSamples = complexSamples.size() / nbQuandrants;

	auto * pSamples = complexSamples.data();
	for (size_t i = 0; i < nbSamples; i++)
	{
		std::complex<float> avg(0.f, 0.f);
		for (auto j = 0; j < nbQuandrants; ++j)
		{
			avg += *pSamples++;
		}
		complexSamples[i] = avg;
	}

	complexSamples.resize(nbSamples);
}

void ReaderUtils::spectrumComputation(Sounder* sounder, Transducer* transducer, SoftChannel* softChannel, Environnement* env
	, double df
	, const std::vector<std::complex<float>>& samples
	, const std::vector<std::complex<float>> & filt
	, double signalSquaredNorm
	, SpectralAnalysisDataObject& dataObject
	, int nbQuadrants)
{
	TimeCounter spectrumComputationTimeCounter;
	spectrumComputationTimeCounter.StartCount();

	const auto nominalTransducerImpedance = 75;
	const auto wbtImpedanceRx = 5e3;
	const auto fe = 1 / (transducer->m_timeSampleInterval / 1000000.0);
	const auto soundSpeed = sounder->m_soundVelocity;

	const auto f1 = softChannel->m_startFrequency;
	const auto f2 = softChannel->m_endFrequency;
	const auto T = ((double)transducer->m_pulseDuration) / 1000000.0;
	const auto B = f2 - f1;
	
	const auto maxDf = B * 0.5;
	const auto minDf = 1.0 / T;

	assert(minDf <= maxDf);

	if (df > maxDf)
		df = round(maxDf); //Too large frequency resolution requested
	if (df < minDf)
		df = round(minDf); //Too small frequency resolution requested

	const std::int32_t Ndf = round(fe / df * 1.25);
	const std::int32_t Nfft = pow(2, ceil(log2(Ndf))); //size of the fft
	const auto Tcft = Nfft / fe;
	const auto resol_f = fe / Ndf * 1.25;

	const std::int32_t shift = std::max((double)1, floor(Ndf / 4.0));

	//Hanning Window on sur 2*0.2 (a bit different than the classic hann window)
	const float nShapingSamples = floor(0.4 / 2.0 * Ndf);
	const std::vector<float> windowFunction = hanning2(nShapingSamples);
	std::vector<float> shapingWindow(Ndf, 1);
	memcpy(&shapingWindow[0], &windowFunction[0], nShapingSamples * sizeof(float));
	memcpy(&shapingWindow[Ndf - nShapingSamples], &windowFunction[nShapingSamples], nShapingSamples * sizeof(float));

	//frequencies indexes
	const auto f2per = f2 % (std::int32_t(fe));
	const auto f1per = f1 % (std::int32_t(fe));
	const std::int32_t nper = floor(f1 / fe);
	std::vector<std::pair<double, int>> freq(Nfft);

	const auto freqInterval = fe / Nfft;

	if (f1per < f2per)
	{
		for (auto i = 0; i < Nfft; ++i)
		{
			freq[i] = std::make_pair((i * freqInterval) + (nper * fe), i);
		}
	}
	else
	{
		auto midValue = (f1per + f2per) / 2.0 - (fe / Nfft);
		for (auto i = 0; i < Nfft; ++i)
		{
			double f = i * freqInterval;
			if (f > midValue)
			{
				f += nper * fe;
			}
			else
			{
				f += (nper + 1) * fe;
			}

			freq[i] = std::make_pair(f, i);
		}
	}

	sort(freq.begin(), freq.end(), [](const std::pair<double, int>& v1, const std::pair<double, int>& v2) {return v1.first < v2.first; });

	//Compute ffts
	const auto ns = samples.size();

	std::vector<std::complex<float>> MFfilt;
	xcorr(filt, MFfilt);

	const int imed = ceil(MFfilt.size() / 2);

	const int idx1 = imed - floor((Ndf - 1) / 2);
	const int idx2 = imed + floor(Ndf / 2);

	std::vector<std::complex<float>> fft_filt_complex(Nfft);
	for (int i = idx1; i <= idx2; ++i)
	{
		int j = i - idx1;
		fft_filt_complex[j] = MFfilt[i] * shapingWindow[j];
	}
	FFTComputer::forward(fft_filt_complex);

	const int fft_filt_size = fft_filt_complex.size();
	std::vector<float> fft_filt(fft_filt_size);
	for (int i = 0; i < fft_filt_size; ++i)
	{
		fft_filt[i] = sqrt(std::abs(fft_filt_complex[i]));
	}

	//number of shifted ffts to run
	const std::int32_t nb_fft = floor((ns - Ndf) / shift);

	assert(Nfft >= Ndf);

	const float multiplier = (wbtImpedanceRx + nominalTransducerImpedance) / wbtImpedanceRx / (2.0 * sqrt(2.0 * nominalTransducerImpedance * nbQuadrants));
	std::vector<std::vector<std::complex<float>>> spectrums(nb_fft, std::vector<std::complex<float>>(Nfft));
	for (std::int32_t ic = 0; ic < nb_fft; ++ic)
	{		
		std::vector<std::complex<float>>& spectrum = spectrums[ic];
		for (int i = 0; i<Ndf; ++i)
		{
			int j = (i + ic*shift);
			spectrum[i] = samples[j] * multiplier * shapingWindow[i];
		}
		FFTComputer::forward(spectrum);

		for (int i = 0; i < Nfft; ++i)
		{
			spectrum[i] /= fft_filt[i];
		}
	}

	//Computes ranges
	double lastFreq = 0;
	const auto firstFreqIt = std::find_if(freq.cbegin(), freq.cend(), [&f1, &f2](const std::pair<double, int>& value) { return value.first >= f1 && value.first <= f2; });
	auto lastFreqIt = std::find_if(freq.crbegin(), freq.crend(), [&f1, &f2](const std::pair<double, int>& value) { return value.first >= f1 && value.first <= f2; }); //reverse iterators to take the last value
	assert(lastFreqIt != freq.rend());
	lastFreq = lastFreqIt->first;

	const auto firstFreqIndex = firstFreqIt - freq.cbegin();

	const auto freqNfft = T / B * fe / shift;
	const auto nr = nb_fft - (std::int32_t)floor((lastFreq - f1) * freqNfft); //number of ranges
	const auto dec_filtSim = 0;

	const auto transmitPower = softChannel->m_transmissionPower;

	const auto shappingWindowSquareSum = std::inner_product(shapingWindow.begin(), shapingWindow.end(), shapingWindow.begin(), 0.0);
	const auto effectivePulselLength = (shappingWindowSquareSum / shapingWindow.size()) * Ndf / fe;
	const auto effectivePulselLengthLog = 10.0 * log10(effectivePulselLength);

	//Computes the final Sv spectrum
	dataObject.svValues.clear();
	dataObject.svValues.resize(nr);

	const int nbFinalFreq = std::count_if(freq.cbegin(), freq.cend(), [&f1, &f2](auto value) -> bool { return value.first >= f1 && value.first <= f2; });
	const auto lastFreqIndex = firstFreqIndex + nbFinalFreq;

	//Compute all values for Sv which only depends on f

	std::vector<double> fixedFrequencyValue(nbFinalFreq);
	std::vector<double> absorbtionCoefficientValues(nbFinalFreq);
	int freqIndex = 0;

	for (auto itFreq = firstFreqIt; itFreq != lastFreqIt.base(); ++itFreq)
	{
		const auto f = itFreq->first;
		const auto dsp_Pt = (double)transmitPower*((fft_filt[itFreq->second] * signalSquaredNorm)*(fft_filt[itFreq->second] * signalSquaredNorm)) / ((fe*fe) * T);
		const auto dspPi = dsp_Pt *  soundSpeed / (32.0 * M_PI * M_PI);

		const auto lambda = soundSpeed / f;
		const auto psi = softChannel->m_beamEquTwoWayAngle - 20 * log10(f / softChannel->m_acousticFrequency);
		const auto value = -10.0 * log10(dspPi * lambda * lambda) - effectivePulselLengthLog - psi;
		//Note: In the algorithm, the gain should be added here, but it depends on a calibration file defned in the settings.
		//		So it is better to add it later when we use the actual svValues. See SpectralAnalysisDataObject::getSvValuesWithGain.
		//		value -= 2.0 * gain;
		fixedFrequencyValue[freqIndex] = value;

		absorbtionCoefficientValues[freqIndex] = 2.0 * env->GetMeasure(0)->ComputeAbsorptionCoefficient(f);;

		++freqIndex;
	}

	std::vector<double> ranges(nr);
	for (int rangeIndex = 0; rangeIndex < nr; ++rangeIndex)
	{
		dataObject.svValues[rangeIndex].resize(nbFinalFreq);
		double r = (rangeIndex * shift + Ndf / 2.0) / fe * soundSpeed / 2.0; //compute r_spectrum value
		r = std::max(r - dec_filtSim / fe * soundSpeed / 2.0, 0.1); // shift of pulse beginning due to WBT filtering

		ranges[rangeIndex] = r;
	}

	const auto firstRangeIt = std::find_if(ranges.cbegin(), ranges.cend(), [](auto v) { return v > 0.1; });
	const int firstRangeIndex = firstRangeIt - ranges.cbegin();
	dataObject.svValues.resize(nr - firstRangeIndex);

	const auto valueFixedPart = -10.0 * log10(fe * Ndf) + 10.0 * log10(Ndf / fe / T);
	const auto rangeInterval = shift / fe * soundSpeed / 2.0;

#pragma loop(hint_parallel(0))  // Forces the parallelisation of the loop with the /QPar compiler option
#pragma loop(ivdep)				// The automatic parallelisation detect a data dependecy in the loop but it is not really here, this pragma allows bypassing this check.
	for (auto rangeIndex = firstRangeIndex; rangeIndex < nr; ++rangeIndex)
	{
		const auto actualRangeIndex = rangeIndex - firstRangeIndex;

		const auto r = ranges[rangeIndex];
		const auto fixedValueForRange = valueFixedPart + 20 * log10(r);

		for (int frequencyIndex = firstFreqIndex; frequencyIndex < lastFreqIndex; ++frequencyIndex)
		{
			const auto actualFrequencyIndex = frequencyIndex - firstFreqIndex;

			const auto& freqData = freq[frequencyIndex];
			const double f = freqData.first;
			assert(f >= f1 && f <= f2);

			const double freqIndex = freqData.second;
			const auto c = spectrums[rangeIndex][freqIndex];

			double value = fixedValueForRange;
			value += 20.0 * log10(std::abs(c));
			value += r * absorbtionCoefficientValues[actualFrequencyIndex];;
			value += fixedFrequencyValue[actualFrequencyIndex];

			dataObject.svValues[actualRangeIndex][actualFrequencyIndex] = value;
		}
	}

	if (!ranges.empty())
	{
		dataObject.firstRange = ranges[firstRangeIndex] + transducer->m_transDepthMeter;
		dataObject.lastRange = ranges.back();
	}

	dataObject.rangeInterval = rangeInterval;

	dataObject.firstFrequency = firstFreqIt->first;
	dataObject.lastFrequency = lastFreqIt->first;
	dataObject.frequencyInterval = freqInterval;

	spectrumComputationTimeCounter.StopCount();
	M3DKernel::GetInstance()->m_timeCounterRecord.AddTimeCounter("Spectral Analysis Computation", spectrumComputationTimeCounter);
}