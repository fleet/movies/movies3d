#ifdef WIN32

#include "Reader/HermesSocket.h"
#include <fstream>
#include <assert.h>
#include "M3DKernel/utils/log/ILogger.h"

namespace
{
	constexpr const char * LoggerName = "Reader.HermesSocket";
}

//=================================================================
// METHODE		: HermesSocket::HermesSocket
//=================================================================
HermesSocket::HermesSocket()
{
	m_asyncsocket = -1;
	m_error_code = 0;
	m_senderAddress.sa_family = AF_UNSPEC;

} // Fin HermesSocket::HermesSocket

//=================================================================
// METHODE		: HermesSocket::~HermesSocket
//=================================================================

HermesSocket::~HermesSocket()
{
	if (m_asyncsocket != -1)
	{
		closesocket(m_asyncsocket);
	}

} // Fin HermesSocket::~HermesSocket


//=================================================================
// METHODE		: HermesSocket::Open
//=================================================================

bool HermesSocket::Open(const std::int32_t port, const std::string address)
{
	m_error_code = 0;

	// variables locales
	int			erreur;

	m_port = (unsigned int)port;
	m_address = address;
	m_asyncsocket = socket(AF_INET, SOCK_DGRAM, 0);   //  ?  modifier   ?
	
	int on = 1;

	// Le buffer socket etait trop petit en multifrequence
	// int size_buffer = 100000;
	//int size_buffer = 200000;   
	// OTK - 27/05/2009 - mettons plutot 5Mo comme dans Hermes ARC
	int size_buffer = 5 * 1024 * 1024;
	int optLen = sizeof(int);
	
	if (setsockopt(m_asyncsocket, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on))
		== SOCKET_ERROR)
	{
		M3D_LOG_INFO(LoggerName, "setsockopt error ");
		m_error_code = 3;
		return FALSE;
	}

	if (setsockopt(m_asyncsocket, SOL_SOCKET, SO_RCVBUF, (char*)&size_buffer, optLen)
		== SOCKET_ERROR)
	{
		M3D_LOG_INFO(LoggerName, "setsockopt error ");
		m_error_code = 4;
		return FALSE;
	}

	// OTK - 28/04/2009 - timeout socket de reception : 100ms
	int recvto = 100;
	if (setsockopt(m_asyncsocket, SOL_SOCKET, SO_RCVTIMEO, (char*)&recvto, optLen)
		== SOCKET_ERROR)
	{
		M3D_LOG_INFO(LoggerName, "setsockopt error ");
		m_error_code = 5;
		return FALSE;
	}

	// OTK - 28/04/2009 - on passe en socket bloquante
	/*u_long argp=1;

	ioctlsocket(  m_asyncsocket,FIONBIO,&argp);*/


	// *************************************************************************
	// Lie la socket � une ad ip et un port d'�coute
	// *************************************************************************
	// Initialisation de la structure a 0
	memset(&m_SockAddrIn, 0, sizeof(m_SockAddrIn));

	// Initialisation des champs de la structure
	m_SockAddrIn.sin_family = AF_INET;
	m_SockAddrIn.sin_addr.s_addr = INADDR_ANY; // Ecoute sur toutes les IP locales  
	m_SockAddrIn.sin_port = htons((unsigned short)port); // Ecoute sur le port 

	// Bind
	int res = bind(m_asyncsocket, (struct sockaddr*)&m_SockAddrIn, sizeof(m_SockAddrIn));

	if (res < 0)
	{
		erreur = WSAGetLastError();

		switch (erreur)
		{
		case WSANOTINITIALISED:
			M3D_LOG_WARN(LoggerName, "WSANOTINITIALISED error ");
			break;
		case WSAEAFNOSUPPORT:
			M3D_LOG_WARN(LoggerName, "WSAEAFNOSUPPORT error ");
			break;
		case WSAEMFILE:
			M3D_LOG_WARN(LoggerName, "WSAEMFILE error ");
			break;
		case WSAENOBUFS:
			M3D_LOG_WARN(LoggerName, "WSAENOBUFS error ");
			break;
		case WSAEPROTOTYPE:
			M3D_LOG_WARN(LoggerName, "WSAEPROTOTYPE error ");
			break;
		case WSAESOCKTNOSUPPORT:
			M3D_LOG_WARN(LoggerName, "WSAESOCKTNOSUPPORT error ");
			break;
		case WSAENETDOWN:
			M3D_LOG_WARN(LoggerName, "WSAENETDOWN error ");
			break;
		case WSAEINPROGRESS:
			M3D_LOG_WARN(LoggerName, "WSAEINPROGRESS error ");
			break;
		case WSAEPROTONOSUPPORT:
			M3D_LOG_WARN(LoggerName, "WSAEPROTONOSUPPORT error ");
			break;
		case WSAEACCES:
			M3D_LOG_WARN(LoggerName, "WSAEACCES error ");
			break;
		default:
			//			m_error = "error ";
			M3D_LOG_INFO(LoggerName, Log::format("Sock Bind error : %d", erreur));
			m_error_code = 1;
		} // fin switch

		m_error_code = 1;
		return FALSE;
	}

	return TRUE;
} // Fin HermesSocket::Open

//=================================================================
// METHODE		: HermesSocket::Read
//=================================================================

int HermesSocket::Read()
{
	// variables locales
	int			erreur;

	int senderAddressSize = sizeof(m_senderAddress);

	m_length = recvfrom(m_asyncsocket,
		m_buffer, SIZE_MAX_HMS, 0, &m_senderAddress, &senderAddressSize);

	if (m_length <= 0)
	{
		m_senderAddress.sa_family = AF_UNSPEC;
		erreur = GetLastError();
		switch (erreur)
		{
		case WSANOTINITIALISED:
			M3D_LOG_INFO(LoggerName, "WSANOTINITIALISED error with CSocket::Read()");
			break;
		case WSAENETDOWN:
			M3D_LOG_INFO(LoggerName, "WSAENETDOWN error with CSocket::Read()");
			break;
		case WSAENOTCONN:
			M3D_LOG_INFO(LoggerName, "WSAENOTCONN error with CSocket::Read()");
			break;
		case WSAEINPROGRESS:
			M3D_LOG_INFO(LoggerName, "WSAEINPROGRESS error with CSocket::Read()");
			break;
		case WSAENOTSOCK:
			M3D_LOG_INFO(LoggerName, "WSAENOTSOCK error with CSocket::Read()");
			break;
		case WSAEOPNOTSUPP:
			M3D_LOG_INFO(LoggerName, "WSAEOPNOTSUPP error with CSocket::Read()");
			break;
		case WSAESHUTDOWN:
			M3D_LOG_INFO(LoggerName, "WSAESHUTDOWN error with CSocket::Read()");
			break;
		case WSAEWOULDBLOCK:
			//		M3D_LOG_INFO(LoggerName, "WSAEWOULDBLOCK error with CSocket::Read()");
			break;
		case WSAEMSGSIZE:
			M3D_LOG_INFO(LoggerName, "WSAEMSGSIZE error with CSocket::Read()");
			break;
		case WSAEINVAL:
			M3D_LOG_INFO(LoggerName, "WSAEINVAL error with CSocket::Read()");
			break;
		case WSAECONNABORTED:
			M3D_LOG_INFO(LoggerName, "WSAECONNABORTED error with CSocket::Read()");
			break;
		case WSAECONNRESET:
			M3D_LOG_INFO(LoggerName, "WSAECONNRESET error with CSocket::Read()");
			break;
		case WSAETIMEDOUT:
			//D�clenchement du Timeout de r�ception : pas d'erreur.
			//M3D_LOG_INFO(LoggerName, "WSAETIMEDOUT error with CSocket::Read()");
			break;
		default:
			M3D_LOG_INFO(LoggerName, "error with CSocket::Read()");

		} // fin switch

		m_error_code = 1;
		m_length = 0;
		return 0;
	} // fin if ...

	return m_length;

} // Fin HermesSocket::Read


//=================================================================
// METHODE		: HermesSocket::GetBuffer()
//=================================================================

char * HermesSocket::GetBuffer()
{

	return m_buffer;

} // Fin HermesSocket::GetBuffer


//=================================================================
// METHODE		: HermesSocket::GetRecLength()
//=================================================================

int HermesSocket::GetRecLength()
{

	return m_length;

} // Fin HermesSocket::GetRecLength

#endif
