#include "Reader/ManualCorrection/CorrectionManager.h"

#include "M3DKernel/datascheme/PingFan.h"
#include "Reader/MovReadService.h"
#include "Reader/MovHacFileReadService.h"
#include "Reader/MovSonarNetCDFFileReadService.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "BaseMathLib/geometry/GeometryTools2D.h"
#include "BaseMathLib/geometry/Poly2D.h"

#include "Reader/SonarNetCDF/GroupTopLevel.h"
#include "Reader/SonarNetCDF/GroupSonar.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterI.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterR.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterSampleCount.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableDetectedBottomRange.h"

#include <sys/stat.h>
#include <fstream>
#include <fstream>
#include <numeric>
#include <vector>


namespace {
	constexpr const char * LoggerName = "Reader.CorrectionManager";
	constexpr const char * BakExtension = "BAK";
}

CorrectionManager::CorrectionManager() {
	m_pBackup = new std::map<PingFan*, OriginalPingFanData>();
}

CorrectionManager::~CorrectionManager() {
	delete m_pBackup;
	m_pBackup = NULL;
}

void CorrectionManager::CorrectBottom(PingFan * pFan, unsigned short softchannelId, std::int32_t bottomRange) {
	// Sauvegarde pr�alable de la donn�e originale
	std::uint32_t fondEval;
	std::int32_t range;
	bool found;
	pFan->getBottom(softchannelId, fondEval, range, found);
	(*m_pBackup)[pFan].SetOriginalBottomData(softchannelId, found, range);

	// Modification de la valeur du fond
	pFan->setBottom(softchannelId, bottomRange, true);
}

void CorrectionManager::DeletePolygon(std::uint32_t sounderId, unsigned int transducerIndex, BaseMathLib::Poly2D * polygon) {
	// recherche du min et du max (en pings) pour ce polygon polygon
	std::int32_t minPing = (std::int32_t)(*polygon->GetPointX(0));
	std::int32_t maxPing = (std::int32_t)(*polygon->GetPointX(0));
	for (int i = 1; i < polygon->GetNbPoints(); i++) {
		double ping = *polygon->GetPointX(i);
		if (ping < minPing) minPing = ping;
		if (ping > maxPing) maxPing = ping;
	}

	// recherche des echos appartenant au polygone
	// pour chaque ping
	for (std::uint64_t pingId = minPing; pingId <= maxPing; pingId++) {
		PingFan* pFan = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanWithId(pingId);
		if (pFan && pFan->m_pSounder->m_SounderId == sounderId) {
			Sounder * pSound = pFan->getSounderRef();
			MemoryStruct *pMem = pFan->GetMemorySetRef()->GetMemoryStruct(transducerIndex);
			BaseMathLib::Vector2I size = pMem->GetDataFmt()->getSize();

			// rmq. : 0 car uniquement dans le cas Monofaisceau pour l'instant
			unsigned short softChannelId = pSound->GetTransducer(transducerIndex)->GetChannelId()[0];

			// parcours de chaque echo pouvant appartenir au polygon
			for (int iEcho = 0; iEcho < size.y; iEcho++) {
				// verification qu'il appartient au polygone
				double depth = pSound->GetEchoDistance(pFan, transducerIndex, 0, iEcho);
				double tabPoint[2] = { pingId, depth };
				if (BaseMathLib::GeometryTools2D::PointInPolygon(polygon->GetPoints(), polygon->GetNbPoints(), tabPoint)) {
					// sauvegarde de la valeur de l'�cho originale rmq. : beam 0 car uniquement dans le cas monofaisceaux pour l'instant
					(*m_pBackup)[pFan].SetOriginalEchoData(softChannelId, iEcho, pMem->GetDataFmt()->GetValueToVoxel(0, iEcho));

					// mise � UNKNOWN_DB de l'�cho. rmq. : beam 0 car uniquement dans le cas monofaisceaux pour l'instant
					pMem->GetDataFmt()->SetValueToVoxel(0, iEcho, UNKNOWN_DB);
				}
			}
		}
	}
}

void CorrectionManager::Undo() {
	std::map<PingFan*, OriginalPingFanData>::iterator iter;
	for (iter = m_pBackup->begin(); iter != m_pBackup->end(); ++iter) {
		iter->second.Undo(iter->first);
	}
}

std::string CorrectionManager::Save(MovReadService * pFileReadService) {
	// La fonctionnalit� devrait �tre innaccessible en mode acquisition.
	assert(pFileReadService != NULL);

	std::vector<std::string> errorMessages;

	// On commence par lister les fichiers concern�s par les modifications � sauvegarder
	std::set<std::string> listModifiedFiles;
	std::map<PingFan*, OriginalPingFanData>::iterator iter;
	for (iter = m_pBackup->begin(); iter != m_pBackup->end(); ++iter) {
		std::map<unsigned short, OriginalBeamData>::const_iterator iterChannel;
		for (iterChannel = iter->second.GetData().begin(); iterChannel != iter->second.GetData().end(); ++iterChannel) {
			std::string fileName;
			std::uint32_t streamPos;
			iter->first->getBeam(iterChannel->first)->second->GetPositionInFileRun(fileName, streamPos);
			listModifiedFiles.insert(fileName);
		}
	}

	// On parcourt les fichiers du run. Si le fichier est modifi�, on le remplace.
	std::set<std::string> unsavedModifiedFiles;
	for (unsigned int iFile = 0; iFile < pFileReadService->GetFileRun().GetFileCount(); iFile++) {
		std::string runfileName = pFileReadService->GetFileRun().GetFileName(iFile);

		if (listModifiedFiles.find(runfileName) != listModifiedFiles.end()) {
			// Le fichier contient des tuples modifi�s.

			// Si un fichier .bak n'existe pas, on commence par faire une copie du fichier
			CreateBakupFileIfNeeded(runfileName);

			// si le fichier est le fichier courant du run, on le ferme en nottant la position dans le fichier
			bool bIsCurrentFile = iFile == pFileReadService->GetFileRun().GetCurrentFileIdx();
			std::uint32_t currentPos = pFileReadService->GetCurrentFilePos();
			// rmq. : le fichier courant peut ne pas �tre ouvert (si on est arriv� � la fin et que la lecture est termin�e ?)
			bIsCurrentFile = bIsCurrentFile && (currentPos != -1);
			if (bIsCurrentFile) {
				pFileReadService->Close();
			}

			// Sauvegarde sp�cifique au format de fichier
			if (dynamic_cast<MovHacFileReadService*>(pFileReadService)) {
				const auto errorMessage = SaveHac(runfileName);
				errorMessages.push_back(errorMessage);
			}
			else if (dynamic_cast<MovSonarNetCDFFileReadService*>(pFileReadService)) {
				const auto errorMessage = SaveNetCDF(runfileName);
				errorMessages.push_back(errorMessage);
			}
			else {
				unsavedModifiedFiles.insert(runfileName);
			}

			// R�ouverture du fichier 
			if (bIsCurrentFile) {
				pFileReadService->ReOpen(currentPos);
			}
		}
	}

	// Message indiquant la liste des fichiers � modifier non sauvegard�s
	if (!unsavedModifiedFiles.empty()) {
		std::string join;
		for (const auto &fileName : unsavedModifiedFiles) {
			join += (join.empty() ? "" : "\n") + fileName;
		}

		errorMessages.push_back("Changed echoes could not be saved for files :\n" + join);
	}

	// Construction du message de retour concat�n�
	errorMessages.erase(std::remove(errorMessages.begin(), errorMessages.end(), ""), errorMessages.end());
	std::string returnMessage;
	for (const auto& errorMessage : errorMessages) {
		returnMessage += (returnMessage.empty() ? "" : "\n") + errorMessage;
	}
	return returnMessage;
}

std::string CorrectionManager::SaveHac(const std::string & filename) {

	bool bComplexDataCoundNotBeWritten = false;

	// On ouvre le fichier .hac pour �dition
	std::fstream fs(filename.c_str(), std::ios::in | std::ios::out | std::ios::binary);

	// Ensuite, on applique les modifications dans le fichier .hac
	for (auto iter = m_pBackup->begin(); iter != m_pBackup->end(); ++iter) {
		std::map<unsigned short, OriginalBeamData>::const_iterator iterChannel;
		for (iterChannel = iter->second.GetData().begin(); iterChannel != iter->second.GetData().end(); ++iterChannel) {
			std::string fileName;
			std::uint32_t streamPos;
			BeamDataObject * pBeam = iter->first->getBeam(iterChannel->first)->second;
			pBeam->GetPositionInFileRun(fileName, streamPos);
			if (!fileName.compare(filename)) {
				// on se positionne sur le backlink du tuple associ�
				fs.seekg(streamPos - 4);
				// lecture du backlink
				std::uint32_t backlink;
				fs.read(reinterpret_cast<char*> (&backlink), sizeof(backlink));

				bool bChange = false;

				// Modification du fond
				if (iterChannel->second.IsBottomEdited()) {
					// Positionnement sur la position du bottomRange
					// taille et type de tuple : (4 + 2) + champs avant le bottomRange : (2 + 4 + 2 + 2 + 4)

					// NMD : l'utilisation de seekg avec std::ios_base::cur semble �tre la cause de la g�n�ration de fichier de taille ab�rante...
					//fs.seekg(20 - backlink, std::ios_base::cur);
					fs.seekg(streamPos - backlink + 20);

					fs.write(reinterpret_cast<char*> (&pBeam->m_bottomRange), sizeof(pBeam->m_bottomRange));
					bChange = true;
				}

				// Modification des valeurs d'�chos
				if (iterChannel->second.IsEchoEdited()) {
					// On se positionne sur la taille du tuple
					fs.seekg(streamPos - backlink);

					// R�cup�ration de la structure des donn�es d'�chos associ�e
					// On r�cup�re l'index du sondeur
					Sounder * pSound = iter->first->getSounderRef();
					Transducer * pTrans = pSound->getTransducerForChannel(iterChannel->first);
					unsigned int iTrans;
					for (iTrans = 0; iTrans < pSound->GetTransducerCount(); iTrans++) {
						if (pTrans == pSound->GetTransducer(iTrans)) {
							break;
						}
					}
					MemoryObjectDataFmt * pData = iter->first->GetMemorySetRef()->GetMemoryStruct(iTrans)->GetDataFmt();

					// Modification des valeurs d'�chos
					if (!ReplaceEchoes(fs, iterChannel->second, pData)) {
						bComplexDataCoundNotBeWritten = true;
					}
					else {
						bChange = true;
					}
				}

				if (bChange) {
					// positionnement du tuple attribute � 1
					// backlink + attribute = (4 + 4)
					fs.seekg(streamPos - 8);
					std::int32_t attribute = 1;
					fs.write(reinterpret_cast<char*> (&attribute), sizeof(attribute));
				}
			}
		}
	}

	// Fermeture du fichier une fois toutes les modifications faites
	fs.close();

	// Message d'erreur
	if (bComplexDataCoundNotBeWritten) {
		return "Changed echoes on tuples 10060 could not be saved (operation not handled by Movies3D)";
	}

	return std::string();
}

std::string CorrectionManager::SaveNetCDF(const std::string & filename) {

	// Si aucune modification n'a �t� enregistr�, on ne fait rien
	if (!m_pBackup || m_pBackup->empty()) {
		M3D_LOG_INFO(LoggerName, "No changes needed to be saved");
		return std::string();
	}

	// On ouvre le fichier netCDF en �criture
	auto sonarFile = sonarNetCDF::ncFile::openFile(filename, NC_WRITE);
	auto topLevelGroup = std::make_unique<sonarNetCDF::GroupTopLevel>(sonarFile.get());
	auto sonarGroup = topLevelGroup->getSonarGroup();

	// Parcours de toutes les modifications � effectuer
	for (const auto backupItem : *m_pBackup) {
		const auto& pingFan = backupItem.first; // Le ping fan modifi� en m�moire
		const auto& pingFanBackup = backupItem.second; // Un ping fan contenant exclusivement les anciennes donn�es aux ping modifi�s

													   // Donc parcourir l'ensemble des donn�es du pingFanBackup donnera tous les canaux � modifier
		for (const auto pingFanBackupDataItem : pingFanBackup.GetData()) {
			const auto& channelId = pingFanBackupDataItem.first; // L'identifiant du canal mis � jour
			const auto& beamBackupData = pingFanBackupDataItem.second; // Un beam contenant exclusivement les anciens echos aux ping modifi�s

			const auto sounder = pingFan->getSounderRef(); // Le sondeur modifi�
			const auto transducerIndex = sounder->getTransducerIndexForChannel(channelId); // L'index du transducteur modifi�
			const auto beamDataObject = pingFan->getBeam(channelId)->second; // Le beam modifi�			
			const auto alteredData = pingFan->GetMemorySetRef()->GetMemoryStruct(transducerIndex); // Les donn�es modifi�es

			const uint64_t& pingTimeIndex = beamDataObject->m_filePingNumber; // L'index du ping time � modifier

																			  // On ne traitera que les beam positionn�s dans le fichier sp�cifi� en entr�e
			if (filename != beamDataObject->GetRunFileName()) {
				continue;
			}

			// On r�cup�re le beam group correspondant au transducteur modifi�
			sonarNetCDF::GroupBeam* beamGroup;
			for (const auto& beamGroupTmp : sonarGroup->getAllBeamGroups()) {
				if (strcmp(beamGroupTmp->getBeamVariable()->getTransducerName().c_str(), sounder->GetTransducer(transducerIndex)->m_transName) == 0) {
					beamGroup = beamGroupTmp;
					break;
				}
			}

			if (!beamGroup) {
				continue;
			}

			if (beamBackupData.IsBottomEdited()) {
				auto detectedBottomRangeVariable = beamGroup->getDetectedBottomRangeVariable();
				detectedBottomRangeVariable->setValueAtIndexes(pingTimeIndex, 0, beamDataObject->m_bottomRange * 1e-3);
			}

			if (beamBackupData.IsEchoEdited()) {
				// Mise � jour des donn�es backscatter
				UpdateBackscatterVariableAtEchoIndexes(
					beamGroup,
					pingTimeIndex,
					beamBackupData.EditedEchoIndexes(),
					alteredData->GetDataFmt()
				);
			}
		}
	}


	// On referme le fichier
	sonarFile->close();

	// Message d'erreur
	return std::string();
}

void CorrectionManager::UpdateBackscatterVariableAtEchoIndexes(
	sonarNetCDF::GroupBeam* beamGroup,
	const uint64_t& pingTimeIndex,
	const std::set<int>& echoIndexes,
	MemoryObjectDataFmt* alteredDataFmt
) {
	// On r�cup�re l'index dans NetCDF du canal modifi�			
	const uint64_t beamIndex = 0; // Pour l'instant, seul le cas mono faisceau est pris en compte.

								  // Nombre de subbeam du groupBeam
	const auto subbeamCount = beamGroup->getSubbeamDimension()->getSize();

	// R�cup�ration des variables backscatter
	auto backscatterRVariable = beamGroup->getBackscatterRVariable();
	auto backscatterIVariable = beamGroup->getBackscatterIVariable();

	// It�ration sur l'ensemble des subbeams
	for (uint64_t subbeamIndex = 0; subbeamIndex < subbeamCount; ++subbeamIndex) {
		// Indexes dans la variable
		const std::vector<uint64_t> variableIndexes = { pingTimeIndex, beamIndex, subbeamIndex };

		// Cas complexe
		if (beamGroup->getConversionEquationType() == sonarNetCDF::sonar::ConversionEquationType::Type::TYPE_4) {

			// Cast des variables dans les "bons" types
			auto castedBackscatteRVariable = dynamic_cast<sonarNetCDF::beamGroup::VariableBackscatterR<sonarNetCDF::sonar::SampleType::typeFloat>*>(backscatterRVariable);
			auto castedBackscatteIVariable = dynamic_cast<sonarNetCDF::beamGroup::VariableBackscatterI<sonarNetCDF::sonar::SampleType::typeFloat>*>(backscatterIVariable);
			if (castedBackscatteRVariable && castedBackscatteIVariable) {
				// R�cup�ration des valeurs
				auto backscatterRValues = castedBackscatteRVariable->getValueAtIndexes(pingTimeIndex, beamIndex, subbeamIndex);
				auto backscatterIValues = castedBackscatteIVariable->getValueAtIndexes(pingTimeIndex, beamIndex, subbeamIndex);

				// Modification de chaque echo
				const auto nanValue = std::nanf("");
				for (const auto& echoIndex : echoIndexes) {
					if (echoIndex < backscatterRValues.size()) {
						backscatterRValues[echoIndex] = nanValue;
					}
					if (echoIndex < backscatterIValues.size()) {
						backscatterIValues[echoIndex] = nanValue;
					}
				}

				// R��criture des donn�es
				beamGroup->getBackscatterSampleCountVariable()->setValueAtIndexes(pingTimeIndex, beamIndex, subbeamIndex, backscatterRValues.size());
				castedBackscatteRVariable->setValueAtIndexes(pingTimeIndex, beamIndex, subbeamIndex, backscatterRValues);
				castedBackscatteIVariable->setValueAtIndexes(pingTimeIndex, beamIndex, subbeamIndex, backscatterIValues);
			}
		}
		// Cas r�el
		else {

			// Cast de la variable dans le "bon" type
			auto castedBackscatteRVariable = dynamic_cast<sonarNetCDF::beamGroup::VariableBackscatterR<sonarNetCDF::sonar::SampleType::typeShort>*>(backscatterRVariable);
			if (castedBackscatteRVariable) {
				// R�cup�ration des valeurs
				auto backscatterRValues = castedBackscatteRVariable->getValueAtIndexes(pingTimeIndex, beamIndex, subbeamIndex);

				// Modification de chaque echo
				for (const auto& echoIndex : echoIndexes) {
					if (echoIndex < backscatterRValues.size()) {
						backscatterRValues[echoIndex] = alteredDataFmt->GetValueToVoxel(beamIndex, echoIndex);
					}
				}

				// R��criture des donn�es
				castedBackscatteRVariable->setValueAtIndexes(pingTimeIndex, beamIndex, subbeamIndex, backscatterRValues);
			}
		}
	}
}

bool CorrectionManager::HasModifications() {
	return m_pBackup->size() != 0;
}

void CorrectionManager::CreateBakupFileIfNeeded(const std::string & fileName) {

	// Remplacement de l'extension par .bak
	auto backupFileName = fileName.substr(0, fileName.find_last_of('.') + 1) + BakExtension;

	// On cr�e le fichier BAK s'il n'existe pas.
	struct stat buf;
	const bool backupFileExists = stat(backupFileName.c_str(), &buf) != -1;
		
	if (!backupFileExists) {
		std::ofstream backupStream(backupFileName, std::ofstream::out | std::ofstream::binary);
		std::ifstream originalStream(fileName, std::ifstream::in| std::ifstream::binary);
		backupStream << originalStream.rdbuf();
	}
}

// Structure des �chantillons pour les trois types de tuples ping (non phase) g�r�s par M3D
struct donneecompr {
	short valeur : 15;
	unsigned short ubit : 1;
};

struct sample32 {
	std::uint32_t sample_no;
	std::int32_t sample_value;
};

struct sample16 {
	unsigned short sample_no;
	short sample_value;
};

struct sample10060 {
	float r, i;
};

bool CorrectionManager::ReplaceEchoes(std::fstream & fs, const OriginalBeamData & originalBeamData, MemoryObjectDataFmt * pBeam) {
	// Lecture de la taille du tuple
	std::uint32_t tupleSize;
	fs.read(reinterpret_cast<char*> (&tupleSize), sizeof(tupleSize));

	// lecture du type de tuple
	unsigned short tupleType;
	fs.read(reinterpret_cast<char*> (&tupleType), sizeof(tupleType));

	// r�cup�ration du nombre d'�chantillons du tuple et positionement sur le premier �chantillon
	std::uint32_t nbSamples = 0;
	std::uint32_t sizeOfSample = 0;
	std::uint32_t nbQuadrants = 0;
	switch (tupleType) {
	case 10000:
	{
		// On se postionne devant les �chantillons
		fs.seekg(18, std::ios_base::cur);

		nbSamples = (tupleSize - 22) / 8;

		sizeOfSample = sizeof(sample32);
	}
	break;

	case 10030:
	{
		// On se postionne devant les �chantillons
		fs.seekg(18, std::ios_base::cur);

		nbSamples = (tupleSize - 22) / 4;

		sizeOfSample = sizeof(sample16);
	}
	break;

	case 10040:
	{
		// On se postionne devant le nombre d'�chantillons
		fs.seekg(18, std::ios_base::cur);

		// lecture du nombre d'�chantillons
		fs.read(reinterpret_cast<char*> (&nbSamples), sizeof(nbSamples));

		sizeOfSample = sizeof(donneecompr);
	}
	break;

	case 10060:
	{
		// On se postionne devant le nombre d'�chantillons
		fs.seekg(18, std::ios_base::cur);

		// lecture du nombre d'�chantillons
		fs.read(reinterpret_cast<char*> (&nbSamples), sizeof(nbSamples));

		// calcul du nombre de quadrants en fonction de la taille du fichier et du nombre d'�chantillons
		nbQuadrants = (tupleSize - 26) / (nbSamples * 8);

		// calcul de la taille d'un �chantillon complets (complex * nb quadrants)
		sizeOfSample = sizeof(sample10060) * nbQuadrants;
	}
	break;

	default:
	{
		assert(0);
	}
	break;
	}

	// lecture dans le fichier du buffer correspondant aux donn�es
	char * samplesBuffer = new char[sizeOfSample * nbSamples];
	fs.read(samplesBuffer, sizeOfSample * nbSamples);

	// Pour chaque �chantillon du fichier, r�cup�ration du num�ro d'�cho. S'il s'agit d'un �cho modifi�,
	// on modifie le buffer en cette position

	switch (tupleType) {
	case 10000:
	{
		for (unsigned int iSample = 0; iSample < nbSamples; iSample++) {
			sample32 * sample = (sample32*)(samplesBuffer + iSample*sizeOfSample);
			if (originalBeamData.IsEchoEdited(sample->sample_no)) {
				//rmq. : 0 pour le x car cas monofaisceaux uniquement pour l'instant
				sample->sample_value = pBeam->GetValueToVoxel(0, sample->sample_no) * 10000;
			}
		}
	}
	break;

	case 10030:
	{
		for (unsigned int iSample = 0; iSample < nbSamples; iSample++) {
			sample16 * sample = (sample16*)(samplesBuffer + iSample*sizeOfSample);
			if (originalBeamData.IsEchoEdited(sample->sample_no)) {
				//rmq. : 0 pour le x car cas monofaisceaux uniquement pour l'instant
				sample->sample_value = pBeam->GetValueToVoxel(0, sample->sample_no);
			}
		}
	}
	break;

	case 10040:
	{
		unsigned int iEcho = 0;
		for (unsigned int iSample = 0; iSample < nbSamples; iSample++) {
			donneecompr * comprData = (donneecompr *)(samplesBuffer + iSample * sizeof(donneecompr));
			short valeur = (short)(comprData->valeur);
			short ubit = (short)(comprData->ubit);

			// il s"agit d'un �cho non compress�
			if (ubit == 0) {
				if (originalBeamData.IsEchoEdited(iEcho)) {
					comprData->ubit = 1;
					comprData->valeur = 0;
				}
			}

			if (ubit == 0) {
				iEcho++;
			}
			else {
				iEcho += valeur + 1;
			}
		}
	}
	break;

	case 10060:
	{
		for (unsigned int iSample = 0; iSample < nbSamples; iSample++) {
			sample10060 * sample = (sample10060*)(samplesBuffer + iSample*sizeOfSample);
			if (originalBeamData.IsEchoEdited(iSample)) {
				for (size_t q = 0; q < nbQuadrants; ++q) {
					sample[q].r = 0.0f;
					sample[q].i = 0.0f;
				}
			}
		}
	}
	break;

	}


	// �criture du buffer modifi� dans le fichier : on revient sur la position de d�but du buffer
	fs.seekg(-(std::int32_t)(sizeOfSample * nbSamples), std::ios_base::cur);

	// Puis on ecrit le buffer
	fs.write(samplesBuffer, sizeOfSample * nbSamples);

	return true;
}
