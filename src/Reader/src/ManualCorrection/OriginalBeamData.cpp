#include "Reader/ManualCorrection/OriginalBeamData.h"

#include "M3DKernel/datascheme/PingFan.h"

OriginalBeamData::OriginalBeamData()
{
	m_bIsBottomEdited = false;
}

void OriginalBeamData::SetOriginalBottomData(bool bottomFound, std::int32_t bottomRange)
{
	if (!IsBottomEdited())
	{
		m_bottomRange = bottomRange;
		m_bottomFound = bottomFound;
		m_bIsBottomEdited = true;
	}
}

void OriginalBeamData::SetOriginalEchoData(int echoIndex, DataFmt echoValue)
{
	if (!IsEchoEdited(echoIndex))
	{
		m_echoes[echoIndex] = echoValue;
	}
}

void OriginalBeamData::Undo(unsigned short softChannelId, PingFan * pFan)
{
	if (m_bIsBottomEdited)
	{
		pFan->setBottom(softChannelId, m_bottomRange, m_bottomFound);
	}

	// On r�cup�re l'index du sondeur
	Sounder * pSound = pFan->getSounderRef();
	Transducer * pTrans = pSound->getTransducerForChannel(softChannelId);
	unsigned int iTrans;
	for (iTrans = 0; iTrans < pSound->GetTransducerCount(); iTrans++)
	{
		if (pTrans == pSound->GetTransducer(iTrans))
		{
			break;
		}
	}

	MemoryObjectDataFmt * pData = pFan->GetMemorySetRef()->GetMemoryStruct(iTrans)->GetDataFmt();

	std::map<int, DataFmt>::const_iterator iter;
	for (iter = m_echoes.begin(); iter != m_echoes.end(); ++iter)
	{
		// rmq. 0 car uniquement dans le cas monofaisceaux pour l'instant
		pData->SetValueToVoxel(0, iter->first, iter->second);
	}
}

bool OriginalBeamData::IsBottomEdited() const
{
	return m_bIsBottomEdited;
}

bool OriginalBeamData::IsEchoEdited(int echoIndex) const
{
	return m_echoes.find(echoIndex) != m_echoes.end();
}

// Indique si au moins un �cho a �t� modifi�
bool OriginalBeamData::IsEchoEdited() const
{
	return m_echoes.size() != 0;
}

std::set<int> OriginalBeamData::EditedEchoIndexes() const 
{
	std::set<int> keys;
	for (const auto& item : m_echoes) {
		keys.insert(item.first);
	}
	return keys;
}

