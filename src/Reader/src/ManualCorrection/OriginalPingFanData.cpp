#include "Reader/ManualCorrection/OriginalPingFanData.h"

OriginalPingFanData::OriginalPingFanData()
{
}

void OriginalPingFanData::SetOriginalBottomData(unsigned short softChannelId, bool bottomFound, std::int32_t bottomRange)
{
	m_Backup[softChannelId].SetOriginalBottomData(bottomFound, bottomRange);
}

void OriginalPingFanData::SetOriginalEchoData(unsigned short softChannelId, int echoIndex, DataFmt echoValue)
{
	m_Backup[softChannelId].SetOriginalEchoData(echoIndex, echoValue);
}

void OriginalPingFanData::Undo(PingFan * pFan)
{
	std::map<unsigned short, OriginalBeamData>::iterator iter;
	for (iter = m_Backup.begin(); iter != m_Backup.end(); ++iter)
	{
		iter->second.Undo(iter->first, pFan);
	}
}

const std::map<unsigned short, OriginalBeamData> & OriginalPingFanData::GetData() const
{
	return m_Backup;
}