#include "Reader/MovFileRun.h"
#include "M3DKernel/DefConstants.h"
#include <algorithm>
#include <set>

#include <stdio.h>
#include <cstring>
#include <cctype>

#ifndef WIN32
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#else
// Exclure les en-têtes Windows rarement utilisés
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

MovFileRun::MovFileRun()
{
	m_currentFileIdx = 0;
	setEndOfStreamReached();
}

MovFileRun::~MovFileRun()
{
}

void MovFileRun::Copy(const MovFileRun &fileRun)
{
	ClearFileList();
	for (unsigned int i = 0; i < fileRun.GetFileCount(); i++)
	{
		this->AddFile(fileRun.GetFileName(i));
	}
	m_isEndOfStreamReached = !(m_fileList.size() != 0);
}

void MovFileRun::ClearFileList()
{
	setEndOfStreamReached();
	m_fileList.clear();
	m_currentFileIdx = 0;
}

unsigned int MovFileRun::GetFileCount() const
{
	return m_fileList.size();
}

std::string	MovFileRun::GetFileName(unsigned int idx) const
{
	if (idx >= 0 && idx < m_fileList.size())
		return m_fileList[idx];
	else
		return std::string();
}

std::string MovFileRun::GetCurrentFileName() const
{
	return GetFileName(m_currentFileIdx);
}

void MovFileRun::setEndOfStreamReached()
{
	m_isEndOfStreamReached = true;
}

void MovFileRun::SetSurveyPath(const std::string & aSurveyPath, const std::string & aSurveyExtFilter)
{
	m_SurveyPath = aSurveyPath;
	m_SurveyExtFilter = aSurveyExtFilter;
	BuildFileList();
}

void MovFileRun::SetFilePath(const std::string & aFileName)
{
    ClearFileList();
	m_fileList.push_back(aFileName);
}

void MovFileRun::SetSourceName(const std::string & SourceName, const std::string & ExtFilter)
{
    bool isDirectory;
#ifdef WIN32 //TODO Améliorer avec std::filesystem::is_directory C++17
	std::string search = SourceName;
	DWORD fileAtt = GetFileAttributes(search.c_str());
    isDirectory = fileAtt & FILE_ATTRIBUTE_DIRECTORY;
#else
    struct stat path_stat;
    stat(SourceName.c_str(), &path_stat);
    isDirectory = S_ISDIR(path_stat.st_mode) != 0;
#endif
    if (isDirectory)
	{
		SetSurveyPath(SourceName, ExtFilter);
	}
	else 
	{
		SetFilePath(SourceName);
	}
	m_isEndOfStreamReached = !(m_fileList.size() != 0);
}

void MovFileRun::SetFileList(const std::vector<std::string> & files)
{
	ClearFileList();
	m_fileList = files;
	m_isEndOfStreamReached = !(files.size() != 0);
}

void MovFileRun::BuildFileList() 
{
	ClearFileList();

	// Scan directory
	ParseDirectory(m_SurveyPath);

	m_isEndOfStreamReached = !(m_fileList.size() != 0);
}

void MovFileRun::ParseDirectory(std::string DirName)
{
#ifdef WIN32
	HANDLE hFind;
	WIN32_FIND_DATA FindData;
	std::string search = DirName + "\\*.*";
	hFind = FindFirstFile(search.c_str(), &FindData);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		// Si le fichier trouv� n'est pas un dossier mais bien un fichier, on affiche son nom
		if ((FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && strcmp(FindData.cFileName, ".") != 0 && strcmp(FindData.cFileName, "..") != 0)
		{
			std::string localdirName = DirName + "\\" + FindData.cFileName;
			ParseDirectory(localdirName);
		}
		// Fichiers suivants
		while (FindNextFile(hFind, &FindData))
		{
			if ((FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && strcmp(FindData.cFileName, ".") != 0 && strcmp(FindData.cFileName, "..") != 0)
			{
				std::string localdirName = DirName + "\\" + FindData.cFileName;
				ParseDirectory(localdirName);
			}
		}
	}
	FindClose(hFind);

	/// check for file
	std::string fileName = DirName + "\\*";
	hFind = FindFirstFile(fileName.c_str(), &FindData);

	if (hFind != INVALID_HANDLE_VALUE)
	{
		// Si le fichier trouvé n'est pas un dossier mais bien un fichier, on affiche son nom
		if (!(FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			if (FindData.nFileSizeHigh != 0 || FindData.nFileSizeLow != 0)
			{
				std::string fileName = DirName + "\\" + FindData.cFileName;
				if (std::equal(m_SurveyExtFilter.rbegin(), m_SurveyExtFilter.rend(), fileName.rbegin(), [](auto&& l, auto&&r) { return std::tolower(l) == std::tolower(r); }))
				{
					AddFile(fileName);
				}
			}
		}
		// Fichiers suivants
		while (FindNextFile(hFind, &FindData))
		{
			if (!(FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				if (FindData.nFileSizeHigh != 0 || FindData.nFileSizeLow != 0)
				{
					std::string fileName = DirName + "\\" + FindData.cFileName;
					if (std::equal(m_SurveyExtFilter.rbegin(), m_SurveyExtFilter.rend(), fileName.rbegin(), [](auto&& l, auto&&r) { return std::tolower(l) == std::tolower(r); }))
					{
						AddFile(fileName);
					}
				}
			}
		}
	}
	FindClose(hFind);

#else
    DIR *dir;
    struct dirent *fileOrDir;

    if (!(dir = opendir(DirName.c_str())))
        return;

    while ((fileOrDir = readdir(dir)) != NULL) 
	{
		const std::string completePath = DirName + "/" + fileOrDir->d_name;
        if (fileOrDir->d_type == DT_DIR) 
		{
            if (strcmp(fileOrDir->d_name, ".") == 0 || strcmp(fileOrDir->d_name, "..") == 0)
                continue;

            ParseDirectory(completePath);
        } 
		else 
		{
            std::string fileName = fileOrDir->d_name;
			if (std::equal(m_SurveyExtFilter.rbegin(), m_SurveyExtFilter.rend(), fileName.rbegin(), [](auto&& l, auto&&r) { return std::tolower(l) == std::tolower(r); }))
			{
                //TODO tester la taille du fichier
                AddFile(completePath);
            }
        }
    }
    closedir(dir);
#endif
}

void MovFileRun::AddFile(std::string fileName)
{
	m_fileList.push_back(fileName);
	std::sort(m_fileList.begin(), m_fileList.end());
}

void MovFileRun::RemoveFile(unsigned int idx)
{
	if (idx < m_fileList.size())
		m_fileList.erase(m_fileList.begin() + idx);
}

void MovFileRun::RemoveFile(const char *Name)
{
	for (unsigned int idx = 0; idx < m_fileList.size(); idx++)
	{
		if (strcmp(Name, m_fileList[idx].c_str()) == 0)
		{
			m_fileList.erase(m_fileList.begin() + idx);
			break;
		}
	}
}

/** change the current file to a next one*/
void MovFileRun::SetCurrentFile(unsigned int idx)
{
	if (idx <= m_fileList.size() - 1)
	{
		m_currentFileIdx = idx;
		m_isEndOfStreamReached = false;
	}
	else
	{
		setEndOfStreamReached();
	}
}