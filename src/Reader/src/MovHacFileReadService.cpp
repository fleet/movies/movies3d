/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	C.PONCELET													  */
/*	File:		 MovFileReadService.cpp										  */
/******************************************************************************/

#include "Reader/MovHacFileReadService.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/MovESUMgr.h"

#include <fstream>	
#include <set>

#include "assert.h"
#include "Reader/Tup65534.h"

using namespace std;

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

namespace
{
	constexpr const char * LoggerName = "Reader.Services.MovHacFileReadService";

	bool TupleTypeIsPingType(unsigned short tupleType)
	{
		return tupleType == 10000
			|| tupleType == 10001
			|| tupleType == 10010
			|| tupleType == 10011
			|| tupleType == 10030
			|| tupleType == 10031
			|| tupleType == 10040
			|| tupleType == 10060;
	}

	void ReadTimeAndPingNumber(std::ifstream &ifs, HacTime &pingTime, std::uint32_t &pingNumber)
	{
		// lecture de la date
		ifs.read((char*)& pingTime.m_TimeFraction, sizeof(pingTime.m_TimeFraction));
		ifs.read((char*)& pingTime.m_TimeCpu, sizeof(pingTime.m_TimeCpu));

		// on passe les identifiants transducteur et channel
		std::int32_t space;
		ifs.read((char*)& space, sizeof(space));

		// lecture du ping number
		ifs.read((char*)& pingNumber, sizeof(pingNumber));
	}

	// ajout de la détermination des limites du filerun en temps et en numero de ping
	bool GetLimitForFile(std::ifstream &ifs, HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing)
	{
		bool status = true;

		//***************************
		// Recherche du min
		//***************************
		// saut de la premiere donn�e de v�rif
		std::uint32_t codeverif;
		ifs.read((char*)& codeverif, sizeof(codeverif));

		// recherche du premier pingC16
		bool firstC16Found = false;
		while (!firstC16Found && !ifs.eof() && ifs.good() && status)
		{
			std::streampos debut = ifs.tellg();
			std::uint32_t tupleSize;
			ifs.read((char*)& tupleSize, sizeof(tupleSize));
			unsigned short tupleType = 0;
			ifs.read((char*)& tupleType, sizeof(tupleType));

			if (ifs.good())
			{
				// si le tuple est du type recherch� = C16 = 10040
				if (TupleTypeIsPingType(tupleType))
				{
					// r�cup�ration de la date + numero de ping
					ReadTimeAndPingNumber(ifs, minTime, minPing);

					if (ifs.good())
					{
						firstC16Found = true;
					}
				}
				else
				{
					// on saute au prochain tuple
					std::streampos fin = debut + ((std::streampos)(6 + tupleSize + 4));
					ifs.seekg(fin);
				}
			}
			else
			{
				status = false;
			}
		}

		//***************************
		// Recherche du max
		//***************************
		bool lastC16Found = false;

		// seulement si on a trouv� le min
		if (firstC16Found)
		{

			// on se positionne de facon a lire le backlink du dernier tuple du fichier
			ifs.seekg(0, std::ios_base::end);
			// on note nexttuplestart le d�but du prochain tuple (au d�but c'est la fin du fichier)
			std::streamoff nexttuplestart = ifs.tellg();

			//  tant qu'on a pas trouv� le dernier ping, on continue

			// NMD ajour de nexttuplestart!= 0 pour verifier qu'on est pas au d�but deu fichier
			// cas de fichier coup� ou corrompu, le backlink est mauvais
			while (!lastC16Found && ifs.good() && nexttuplestart != 0)
			{
				// on se positionne sur le backlink
				ifs.seekg(nexttuplestart - 4);

				// on lit le backlink
				std::uint32_t backlink;
				ifs.read((char*)& backlink, sizeof(backlink));


				// OTK - FAE 2152 - cas du goto sur un fichier mal form� (PELGAS13_030_20130525_044155.hac)
				// Dans ce cas, le backlink lu est clairement incoh�rent. On stop la recherche par la fin du fichier
				// en on recherche depuis le d�but
				if (nexttuplestart - (std::int64_t)backlink < 0)
				{
					break;
				}

				// on se positionne au d�but du tuple
				ifs.seekg(nexttuplestart - backlink);
				nexttuplestart = ifs.tellg();

				// lecture des donn�es du tuple
				std::uint32_t tupleSize;
				ifs.read((char*)& tupleSize, sizeof(tupleSize));
				unsigned short tupleType = 0;
				ifs.read((char*)& tupleType, sizeof(tupleType));

				// si le tuple est du type recherch� = C16 = 10040
				if (TupleTypeIsPingType(tupleType))
				{
					// r�cup�ration de la date + numero de ping
					ReadTimeAndPingNumber(ifs, maxTime, maxPing);

					if (ifs.good())
					{
						lastC16Found = true;
					}
				}
				else
				{
					// on saute au prochain tuple
					ifs.seekg(nexttuplestart);
				}
			}
		}

		// OTK - 09/06/2009 - certains fichiers sont incorrects (coup�s au milieu d'un tuple)
		// par exemple un fichier OASIS dont on a arret� brutalement l'�criture. Dans ce cas
		// on n'arrive pas a d�terminer la date du dernier ping en partant de la fin : on part du d�but
		if (!lastC16Found)
		{
			M3D_LOG_WARN(LoggerName, "Unexpected EOF : The Goto may take some time...");
			ifs.clear();
			ifs.seekg(sizeof(codeverif), std::ios_base::beg);
			while (!ifs.eof() && ifs.good() && status)
			{
				std::streampos debut = ifs.tellg();
				std::uint32_t tupleSize;
				ifs.read((char*)& tupleSize, sizeof(tupleSize));
				unsigned short tupleType = 0;
				ifs.read((char*)& tupleType, sizeof(tupleType));

				if (ifs.good())
				{
					// si le tuple est du type recherch� = C16 = 10040
					if (TupleTypeIsPingType(tupleType))
					{
						// r�cup�ration de la date + numero de ping
						HacTime time;
						std::uint32_t pingNo;
						ReadTimeAndPingNumber(ifs, time, pingNo);

						if (!ifs.good())
						{
							status = false;
						}
						else
						{
							maxTime = time;
							maxPing = pingNo;
							lastC16Found = true;
						}
					}

					// on saute au prochain tuple
					std::streampos fin = debut + ((std::streampos)(6 + tupleSize + 4));
					ifs.seekg(fin);
				}
				else
				{
					status = false;
				}
			}
			if (lastC16Found)
			{
				status = true;
			}
		}

		return status;
	}

	// détermination de la position dans le fichier donné de la cible du GoTo
	bool GetStreamPosOfTarget(std::streampos &sp, std::ifstream & ifs, GoToTarget target)
	{
		bool status = true;

		// saut de la premiere donn�e de v�rif
		std::uint32_t codeverif;
		ifs.read((char*)& codeverif, sizeof(codeverif));

		// recherche du premier pingC16
		bool targetFound = false;
		while (!targetFound && !ifs.eof() && ifs.good() && status)
		{
			std::streampos debut = ifs.tellg();
			std::uint32_t tupleSize;
			ifs.read((char*)& tupleSize, sizeof(tupleSize));
			unsigned short tupleType = 0;
			ifs.read((char*)& tupleType, sizeof(tupleType));

			if (ifs.good())
			{
				// si le tuple est du type recherch� = C16 = 10040
				if (TupleTypeIsPingType(tupleType))
				{
					// r�cup�ration de la date + numero de ping
					HacTime pingTime;
					std::uint32_t pingNumber;
					ReadTimeAndPingNumber(ifs, pingTime, pingNumber);

					if (ifs.good())
					{
						// comparaison du ping et de la target
						if ((target.byPing && (pingNumber >= target.targetPingNumber))
							|| (!target.byPing && (!(pingTime < target.targetTime))))
						{
							// on a trouv�
							targetFound = true;
							sp = debut;
						}
					}
				}

				// dans tous les cas, on saute au prochain tuple
				std::streampos fin = debut + ((std::streampos)(6 + tupleSize + 4));
				ifs.seekg(fin);

			}
			else
			{
				status = false;
			}
		}

		status = status && targetFound;

		return status;
	}

	// détermination de la position dans le fichier et de l'identifiant du fichier correspondant à la cible du GoTo
	bool GetStreamPosOfTarget(const MovFileRun & fileRun, std::streampos &sp, unsigned int &fileIdx, GoToTarget target, bool bFirstTry = true)
	{
		bool status = false;

		// OTK - FAE 216 - On note les bornes des fichiers du run dans le cas o� la date cible n'est pas trouv�e dans
		// l'un des fichiers du run => on fais dans ce cas un goto vers le fihier le plus proche avec un warning
		std::set<HacTime> vectFileTimeBoundaries;

		bool fileFound = false;
		for (unsigned int fileId = 0; fileId < fileRun.GetFileCount() && !fileFound; fileId++)
		{
			std::ifstream ifs;
			ifs.open(fileRun.GetFileName(fileId).c_str(), std::ios::in | std::ios::binary);
			if (ifs.is_open())
			{
				HacTime fileMinTime, fileMaxTime;
				std::uint32_t fileMinPing, fileMaxPing;
				if (GetLimitForFile(ifs, fileMinTime, fileMinPing, fileMaxTime, fileMaxPing))
				{
					// cas d'une recherche par num�ro de ping
					if (target.byPing)
					{
						if ((target.targetPingNumber <= fileMaxPing) &&
							(target.targetPingNumber >= fileMinPing))
						{
							fileIdx = fileId;
							fileFound = true;
						}
					}
					// cas d'une recherche par date
					else
					{
						if (!(target.targetTime > fileMaxTime) &&
							!(target.targetTime < fileMinTime))
						{
							fileIdx = fileId;
							fileFound = true;
						}
						else
						{
							vectFileTimeBoundaries.insert(fileMinTime);
							vectFileTimeBoundaries.insert(fileMaxTime);
						}
					}
					// recherche de la position demand�e dans le fichier
					if (fileFound)
					{
						ifs.clear(); // la recherche a potentiellement mis le flux dans un �tat "bad", on reinit le flux
						ifs.seekg(0, std::ios_base::beg);
						status = GetStreamPosOfTarget(sp, ifs, target);
					}
				}
				ifs.close();
			}
		}

		// OTK - FAE 216 - Si date pas trouv�e, on fais un GoTo vers le fichier le plus proche
		// avec warning si diff�rence de date de plus d'une seconde.
		if (bFirstTry && !target.byPing && !fileFound && !vectFileTimeBoundaries.empty())
		{
			GoToTarget fallbackTarget;
			fallbackTarget.byPing = false;
			fallbackTarget.targetTime = *vectFileTimeBoundaries.begin();
			TimeElapse timeDiff = target.targetTime - fallbackTarget.targetTime;
			for (std::set<HacTime>::const_iterator iter = vectFileTimeBoundaries.begin(); iter != vectFileTimeBoundaries.end(); ++iter)
			{
				TimeElapse timeDiffMin = target.targetTime - *iter;
				if (std::llabs(timeDiffMin.m_timeElapse) < std::llabs(timeDiff.m_timeElapse))
				{
					fallbackTarget.targetTime = *iter;
					timeDiff = timeDiffMin;
				}
			}

			// Avertissement de l'utilisateur si la destination est diff�rente de plus d'une seconde de la date cibl�e
			if (std::llabs(timeDiff.m_timeElapse) > 10000)
			{
				M3D_LOG_WARN(LoggerName, Log::format("The reached DateTime is different from the target DateTime by more than the one second tolerance : %gs", abs((double)timeDiff.m_timeElapse / 10000)));
			}

			return GetStreamPosOfTarget(fileRun, sp, fileIdx, fallbackTarget, false);
		}

		return status;
	}
}

MovHacFileReadService::MovHacFileReadService(MovFileRun &fileRun) : MovHacReadService()
{
	m_Name = "HAC File Read Service";
	m_FileRun.Copy(fileRun);
	m_is = nullptr;
}

MovHacFileReadService::~MovHacFileReadService()
{
}

bool MovHacFileReadService::IsFileService() const
{
	return true;
}

MovFileRun MovHacFileReadService::GetFileRun() const
{
	return m_FileRun;
}

bool MovHacFileReadService::GetLimits(HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing) const
{
	bool status = false;

	for (unsigned int fileIdx = 0; fileIdx < m_FileRun.GetFileCount(); fileIdx++)
	{
		std::ifstream ifs;
		ifs.open(m_FileRun.GetFileName(fileIdx).c_str(), std::ios::in | std::ios::binary);
		if (ifs.is_open())
		{
			HacTime fileMinTime, fileMaxTime;
			std::uint32_t fileMinPing, fileMaxPing;
			bool fileStatus = GetLimitForFile(ifs, fileMinTime, fileMinPing, fileMaxTime, fileMaxPing);
			if (fileStatus)
			{
				if (!status)
				{
					// c'est le premier fichier
					minTime = fileMinTime;
					maxTime = fileMaxTime;
					minPing = fileMinPing;
					maxPing = fileMaxPing;
				}
				else
				{
					// mise � jour des min-max
					minTime = std::min<HacTime>(minTime, fileMinTime);
					maxTime = std::max<HacTime>(maxTime, fileMaxTime);
					minPing = std::min<std::uint32_t>(minPing, fileMinPing);
					maxPing = std::max<std::uint32_t>(maxPing, fileMaxPing);
				}
				// on est en succes si on a pu le faire pour au moins 1 fichier
				status = true;
			}
			ifs.close();
		}
	}

	return status;
}

void MovHacFileReadService::Init(ReaderTraverser &ref)
{
	// OTK - FAE070 - reinitialisation du conteneurs des limites d'ESU si
	// on lance une nouvelle lecture HAC
	M3DKernel::GetInstance()->getMovESUManager()->GetESUContainer().ClearAll();

	MovHacReadService::Init(ref);
	bool error = !m_bInitDone;
	/// place my init here
	this->m_bInitDone = !error;
	OpenNewHac(ref);
}

void MovHacFileReadService::Close(ReaderTraverser &ref)
{
	unsigned int i = m_FileRun.GetCurrentFileIdx();

    std::string currentFileName = m_FileRun.GetCurrentFileName();

    ref.StreamClosed(currentFileName.c_str());

	m_FileRun.SetCurrentFile(++i);

	if (m_FileRun.getEndOfStreamReached())
	{
		m_bIsClosed = true;
	}
	else
	{
		OpenNewHac(ref);
	}
}

bool MovHacFileReadService::WillBeLastClose()
{
	return m_FileRun.GetCurrentFileIdx() + 1 == m_FileRun.GetFileCount();
}

void MovHacFileReadService::OpenNewHac(ReaderTraverser &ref)
{
	std::string filename = m_FileRun.GetCurrentFileName();
	M3D_LOG_INFO(LoggerName, (std::string("Load ") + filename + std::string(" ...")).data());
	setStreamDesc(filename.c_str());
	ref.StreamOpened(filename.c_str());

	// Fermeture de l'objet précédent de lecture
	Close();

	// creation objet de lecture de fichier
	m_is = new ifstream(filename.c_str(), ios::in | ios::binary);

	if (!m_is->good())
	{
		Close();
		M3D_LOG_ERROR(LoggerName, Log::format("Error opening the file %s", filename.c_str()));
		Close(ref);
		return;
	}

	m_bIsClosed = false;

	assert(m_is->good());

	// saut des premiers bits du fichier HAC
	std::uint32_t codeverif;
    m_is->read((char*)& codeverif, sizeof(codeverif));
}

int MovHacFileReadService::Read(ReaderTraverser &ref)
{
	int nbTupReceived = 0;

	libMove posSt;
	posSt.LowPart = 0;
	if (m_is)
	{
		MovStream *pReadStream = ReserveStream();
		// in case of file if no stream avalaible we do not read anything, just wait for the process to continue decode
		if (pReadStream)
		{
			pReadStream->Seek(posSt, eSEEK_SET);
			std::uint32_t taille_tup;
			std::streamoff debut = m_is->tellg();        //lecture taille et type
            m_is->read((char*)& taille_tup, sizeof(taille_tup));

			if (m_is->good())
			{
				std::streampos fin = debut + (int)(6 /**taille tup*/ + 4 /**back link ?*/ + taille_tup);

				// OTK - FAE051 - si le tuple n'est pas valide (test sur la valeur du backlink != taille_tup + 6 + 4),
				// on saute la fin du fichier HAC (permet d'�tre robuste par rapport aux fichiers invalides)
				m_is->seekg((int)fin - 4);
				std::uint32_t backlink;
                m_is->read((char*)& backlink, sizeof(backlink));

				// si le tuple est incomplet ou incoh�rent...
				if (!m_is->good() || backlink != 10 + taille_tup)
				{
					// arr�t de la lecture
					nbTupReceived = 0;
					M3D_LOG_WARN(LoggerName, "Wrong HAC Tuple (backlink != tuple_size+10), skipping the end of the file...");
					Close(ref);
				}
				else
				{
					m_is->seekg(debut);

					std::uint32_t iData = 0;

					m_is->read(pReadStream->GetWriteBuffer((taille_tup + 2 + 4 + 4) * sizeof(char)), (taille_tup + 2 + 4 + 4) * sizeof(char));

					m_is->seekg(fin);


					/// Attention dans le decode, la stream peut se retrouver detruite
					nbTupReceived = 1; // fichiers toujours lus un par un
				}
			}
			else
			{
				HacTime a;
				Tup65534::Encode(*pReadStream, a);

				if (m_is->eof())
				{
					//				assert(!"EOF");
					M3D_LOG_INFO(LoggerName, "Unexpected EOF");
				}
				else {
					assert(!"BAD BIT ?");
					M3D_LOG_INFO(LoggerName, "BAD BIT");
				}
			}
			this->PushStream(pReadStream);
		}
	}
	return nbTupReceived;
}

std::uint32_t MovHacFileReadService::GetCurrentFilePos() const
{
	std::uint32_t currentFilePos = -1;
	if (m_is != nullptr)
	{
		currentFilePos = m_is->tellg();
	}
	return currentFilePos;
}

void MovHacFileReadService::Close()
{
	m_bIsClosed = true;
	if (m_is)
	{
		delete m_is;
		m_is = nullptr;
	}
}

bool MovHacFileReadService::SeekFilePos(std::uint32_t pos)
{
	std::string filename = m_FileRun.GetCurrentFileName();
	m_is = new ifstream(filename.c_str(), ios::in | ios::binary);
	m_is->seekg(pos);
	m_bIsClosed = false;
	return true;
}

// OTK - 02/03/2009 - Impl�mentation du GoTo
bool MovHacFileReadService::PrepareGoTo(GoToTarget target, ReaderTraverser &ref)
{
	bool newFileOpened = false;
	std::streampos targetPos;
	unsigned int targetFileIdx;

	if (GetStreamPosOfTarget(m_FileRun, targetPos, targetFileIdx, target))
	{
		// si le fichier est le fichier courant, on se d�place au bon offset
		if (targetFileIdx == m_FileRun.GetCurrentFileIdx())
		{
			// dans le cas ou on est arriv� a la fin du fichier courant,
			// on doit l'ouvrir
			if (IsClosed())
			{
				m_FileRun.SetCurrentFile(targetFileIdx);
				OpenNewHac(ref);
				m_bIsClosed = false;
				newFileOpened = true;
			}
		}
		// si c'est un autre fichier, on ouvre le nouveau
		else
		{
			if (!IsClosed())
			{
                std::string currentFileName = m_FileRun.GetCurrentFileName();
                ref.StreamClosed(currentFileName.c_str());
			}
			m_FileRun.SetCurrentFile(targetFileIdx);
			OpenNewHac(ref);
			m_bIsClosed = false;
			newFileOpened = true;
		}
	}

	return newFileOpened;
}

// OTK - 02/03/2009 - Impl�mentation du GoTo
bool MovHacFileReadService::ExecuteGoTo(GoToTarget target, ReaderTraverser &ref)
{
	bool status = false;
	bool newFileOpened = false;
	std::streampos targetPos;
	unsigned int targetFileIdx;

	if (GetStreamPosOfTarget(m_FileRun, targetPos, targetFileIdx, target))
	{
		// si le fichier est le fichier courant, on se d�place au bon offset
		if (targetFileIdx == m_FileRun.GetCurrentFileIdx())
		{
			if (!IsClosed())
			{
				if (m_is)
				{
					m_is->seekg(targetPos);
					status = true;
				}
			}
		}
	}

	return status;
}
