/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	C.PONCELET													  */
/*	File:		 MovReadService.cpp										  */
/******************************************************************************/

#include "Reader/MovHacReadService.h"
#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

const int PreAllocatedTupleStream = 1000;

namespace
{
	constexpr const char * LoggerName = "Reader.Services.MovHacReadService";
}

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/
MovHacReadService::MovHacReadService()
	: MovReadService()
{
	m_Name = "Read Service";
	m_bIsClosed = false;
	m_bInitDone = false;
	AllocateStream();
}

MovHacReadService::~MovHacReadService()
{
	FreeStream();
}

const ServiceFormatDesc * MovHacReadService::getServiceFormatDesc() const
{
	return &m_ServiceFormatDesc;
}

void MovHacReadService::Init(ReaderTraverser &ref) 
{
	// initialisation de la memoire utilisée pour la lecture
	bool error = false;
	this->m_bInitDone = !error;
}

void MovHacReadService::Execute(ReaderTraverser & traverser)
{
	if (!m_bInitDone)
	{
		Init(traverser);
	}
	m_count.StartCount();
	Read(traverser);
	m_count.StopCount();

	M3DKernel::GetInstance()->Lock();
	MovStream *pStreamToDecode = this->PopStream();
	bool bEndSignature, bLastClose;
	while (pStreamToDecode)
	{
		m_tupleDecoder.Decode(traverser, pStreamToDecode);
		bEndSignature = traverser.getUpdateEvent().m_EndSignatureReached;
		bLastClose = bEndSignature && this->WillBeLastClose();
		traverser.m_pHacObjectMgr->ComputePostRead(traverser, bLastClose);

		// OTK - FAE214 - fermeture du service apr�s le ComputePostRead plut�t que dans le Decode du tuple 65534
		// sous peine de plantage dans le WriteAlgorithm (le m_pPendingPingFan doit �tre trait� avant la cloture du flux)
		if (bEndSignature)
		{
			this->Close(traverser);
		}

		this->ReleaseStream(pStreamToDecode);
		pStreamToDecode = this->PopStream();
	}

	traverser.m_pHacObjectMgr->CheckTimeObject();
	M3DKernel::GetInstance()->Unlock();
}

MovStream *MovHacReadService::PopStream()
{
	MovStream *pRet = NULL;
	m_stackLock.Lock();
	if (!m_DecodeQueue.empty())
	{
		pRet = m_DecodeQueue.front();
		m_DecodeQueue.pop();
	}
	m_stackLock.Unlock();
	return pRet;
}

void MovHacReadService::PushStream(MovStream *a)
{
	m_stackLock.Lock();
	m_DecodeQueue.push(a);
	m_stackLock.Unlock();
}

void MovHacReadService::ReleaseStream(MovStream *a)
{
	m_stackLock.Lock();
	m_ReaderQueue.push(a);
	m_stackLock.Unlock();
}

MovStream *MovHacReadService::ReserveStream()
{
	MovStream *pRet = NULL;
	m_stackLock.Lock();
	if (!m_ReaderQueue.empty())
	{
		pRet = m_ReaderQueue.front();
		m_ReaderQueue.pop();
	}
	else
	{
		M3D_LOG_WARN(LoggerName, "No more space reserved for reading, Freeze ?");
	}
	m_stackLock.Unlock();
	return pRet;
}

void MovHacReadService::AllocateStream()
{
	m_stackLock.Lock();
	for (unsigned int i = 0; i < PreAllocatedTupleStream; i++)
		m_ReaderQueue.push(new MovStream());
	m_stackLock.Unlock();
}

void MovHacReadService::FreeStream()
{
	m_stackLock.Lock();
	while (!m_DecodeQueue.empty())
	{

		delete (m_DecodeQueue.front());
		m_DecodeQueue.pop();
	}
	while (!m_ReaderQueue.empty())
	{
		delete (m_ReaderQueue.front());
		m_ReaderQueue.pop();
	}
	m_stackLock.Unlock();
}

