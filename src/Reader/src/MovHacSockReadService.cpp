#ifdef WIN32
/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	C.PONCELET													  */
/*	File:		 MovSockReadService.cpp										  */
/******************************************************************************/


#include "Reader/MovHacSockReadService.h"
#include "Reader/HermesSocket.h"
#include "M3DKernel/utils/log/ILogger.h"

#include "M3DKernel/base/MovObject.h"
#include "M3DKernel/utils/multithread/Synchronized.h"

#include <WinSock2.h>

#include <iostream>
#include <sstream>

const int NB_MAX_STORED_PACKAGES = 20;					///<Maximum number of packets stored in m_messageBuffer
const int NB_MESSAGES_FOR_PACKAGE_RECOVERY = 500;		///<Maximum number of packets to lost before recover

namespace
{
	constexpr const char * LoggerName = "Reader.Services.MovHacSockReadService";
}

/******************************************************************************/
/*	CSocketBuffer function		                                              */
/******************************************************************************/
CSocketBuffer::CSocketBuffer()
	: m_pBuffer(nullptr)
	, m_BufLen(0)
	, m_consumeState(true) {
}

CSocketBuffer::~CSocketBuffer() {
	delete[] m_pBuffer;
}

void CSocketBuffer::SetBufSize(int bufSize) {
	delete[] m_pBuffer;
	m_pBuffer = new char[bufSize];
	m_BufSize = bufSize;
}

void CSocketBuffer::WaitConsumeState(bool state)
{
	std::unique_lock<std::mutex> lk(m_mutex);
	if (m_consumeState != state)
	{
		m_cond.wait(lk, [this, state] {return m_consumeState == state; });
	}
}

void CSocketBuffer::SetConsumeState(bool state)
{
	assert(m_consumeState != state);
	{
		std::lock_guard<std::mutex> lk(m_mutex);
		m_consumeState = state;
	}
	m_cond.notify_all();
}


/******************************************************************************/
/*	Thread function																														*/
/******************************************************************************/
int MySockServiceThreadFunction(MovHacSockReadService * service)
{
	service->Receive();
	return 0;
}

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/
MovHacSockReadService::MovHacSockReadService(unsigned short a1, unsigned short a2, unsigned short a3, unsigned short a4, unsigned int por, unsigned int callbackPort)
	: MovHacReadService()
{
	m_Name = "File Read Service";
	m_pSock = NULL;
	sprintf_s(CHad, 16, "%03d.%03d.%03d.%03d", a1, a2, a3, a4);
	m_portNum = por;
	m_callbackPortNum = callbackPort;
	std::string a = "Reading socket :";
	a += CHad;
	setStreamDesc(a.c_str());
	m_Buffers = NULL;
	m_concatStream = NULL;

	headerencoursdereception = -1;
	memset(bufferHeader, 0, 25000);
	headersize = 0;
}

MovHacSockReadService::~MovHacSockReadService()
{
	if (m_concatStream) delete m_concatStream;
}


bool MovHacSockReadService::IsFileService() const
{
	return false;
}

MovFileRun MovHacSockReadService::GetFileRun() const
{
	return MovFileRun();
}

bool MovHacSockReadService::GetLimits(HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing) const
{
	return false;
}

bool MovHacSockReadService::ExecuteGoTo(GoToTarget target, ReaderTraverser &ref)
{
	return false;
}

bool MovHacSockReadService::PrepareGoTo(GoToTarget target, ReaderTraverser &ref)
{
	return false;
}

void MovHacSockReadService::Init(ReaderTraverser &ref)
{
	MovHacReadService::Init(ref);
	bool error = !m_bInitDone;
	/// place my init here

	// OTK - 11/05/2009 - ajout test sur les numero de packets pour d�tection des pertes
	m_FirstMessage = true;

	if (m_pSock != NULL) delete m_pSock;
	m_pSock = new HermesSocket;

	if (m_pSock->Open(this->m_portNum, CHad) == FALSE)    //EK500="134.246.11.230"
	{
		error = true;

		std::stringstream ss;
		ss << CHad << ':' << m_portNum << " connection failed!";
		M3D_LOG_INFO(LoggerName, ss.str().c_str());
	}
	else
	{
		std::stringstream ss;
		ss << CHad << ':' << m_portNum << " connection succeeded!";
		M3D_LOG_INFO(LoggerName, ss.str().c_str());

		m_NBuf = 500; // on a un tampon de 100 packets (comme dans Hermes...)
		if (m_Buffers)
		{
			delete[] m_Buffers;
		}
		m_Buffers = new CSocketBuffer[m_NBuf];
		for (int i = 0; i < m_NBuf; i++) {
			m_Buffers[i].SetBufSize(SIZE_MAX_HMS);
		}
		
		m_BufferIdx = -1;
		m_BufferReadIdx = -1;
		m_bContinueOk = true;

		headerencoursdereception = 0;

		m_thread = std::thread(MySockServiceThreadFunction, this);
	}

	this->m_bInitDone = !error;
};

void MovHacSockReadService::askHacHeaders(const struct sockaddr* address, int port)
{
	auto writeSocket = socket(AF_INET, SOCK_DGRAM, 0);

	struct sockaddr_in *addr_in = (struct sockaddr_in *)address;
	addr_in->sin_port = htons(port);

	auto addressString = inet_ntoa(addr_in->sin_addr);

	int res = connect(writeSocket, (struct sockaddr *)addr_in, sizeof(*addr_in));
	if (res < 0)
	{
		int error = WSAGetLastError();

		std::stringstream ss;
		ss << "Writing connection to " << addressString << ":" << port << " failed with error " << error << ".";

		M3D_LOG_ERROR(LoggerName, ss.str().c_str());
		return;
	}

	int bytesSent = send(writeSocket, "ASK_HAC_HEADERS", 15, 0);
	if (bytesSent < 0)
	{
		int error = WSAGetLastError();

		std::stringstream ss;
		ss << "Couldn't send ASK_HAC_HEADERS message to " << addressString << ":" << port << ". Error " << error << ".";

		M3D_LOG_ERROR(LoggerName, ss.str().c_str());
	}

	std::stringstream ss;
	ss << "ASK_HAC_HEADERS message sent to " << addressString << ":" << port << ".";
	M3D_LOG_INFO(LoggerName, ss.str().c_str());

	closesocket(writeSocket);
}

void MovHacSockReadService::Close()
{
	// arrêt du thread d'écoute et attente de la terminaison
	m_bContinueOk = false;
	m_thread.join();
	m_bIsClosed = true;

	if (m_Buffers)
	{
		delete[] m_Buffers;
		m_Buffers = NULL;
	}
}

// OTK - 27/05/2009 - reception des messages hermes en continu
void MovHacSockReadService::Receive()
{
	CSocketBuffer	*pSocketBuffer = 0;

	bool askHeaderSent = false;

	while (m_bContinueOk)
	{
		if (pSocketBuffer == 0 || pSocketBuffer->m_BufLen > 0) {
			pSocketBuffer = SelectBuffer();
		}

		pSocketBuffer->m_BufLen = m_pSock->Read();
		memcpy(pSocketBuffer->m_pBuffer, m_pSock->GetBuffer(), pSocketBuffer->m_BufLen);

		if (pSocketBuffer->m_BufLen != 0)
		{
			// Positionnement de l'indicateur de consommation du buffer
			pSocketBuffer->SetConsumeState(false);
		}		

		if (!askHeaderSent && m_pSock->getSenderAdress().sa_family != AF_UNSPEC)
		{
			//Ask Hermes to send the .hac headers if it is not already done and if the received message is really comming from hermes
			//The message is sent to the same IP adress from the last received frame
			if (getHermesMessageType(pSocketBuffer->m_pBuffer, pSocketBuffer->m_BufLen) != HermesMessageType::RC_UNKNOWN)
			{
				askHeaderSent = true;
				askHacHeaders(&m_pSock->getSenderAdress(), m_callbackPortNum);
			}
		}
	}
}

// Sélection du buffer qu'on va remplir
CSocketBuffer *MovHacSockReadService::SelectBuffer()
{
	m_BufferIdx = (m_BufferIdx + 1) % m_NBuf;
	m_Buffers[m_BufferIdx].WaitConsumeState(true);
	return &(m_Buffers[m_BufferIdx]);
}

// Récupération d'un buffer lu
CSocketBuffer *MovHacSockReadService::ReadBuffer()
{
	m_BufferReadIdx = (m_BufferReadIdx + 1) % m_NBuf;
	m_Buffers[m_BufferIdx].WaitConsumeState(false);
	return &(m_Buffers[m_BufferReadIdx]);
}

MovHacSockReadService::HermesMessageType MovHacSockReadService::getHermesMessageType(const char* message, int length) const
{
	if ((strncmp("RCD", message, 3) == 0) && (length >= 36))
		return HermesMessageType::RCD;
	else if ((strncmp("RCH", message, 3) == 0) && (length >= 6))
		return HermesMessageType::RCH;
	else if ((strncmp("RCS", message, 3) == 0) && (length >= 4))
		return HermesMessageType::RCS;
	else if ((strncmp("RCT", message, 3) == 0) && (length >= 4))
		return HermesMessageType::RCT;
	else if ((strncmp("RCP", message, 3) == 0) && (length >= 4))
		return HermesMessageType::RCP;
	else if ((strncmp("RCE", message, 3) == 0) && (length >= 4))
		return HermesMessageType::RCE;

	return HermesMessageType::RC_UNKNOWN;
}

int MovHacSockReadService::ReadMessageRCD(const char* message, int length)
{
	int nbTupReceived = 0;	

	int message_idx;
	int message_count;
	memcpy(&message_idx, message + 8, 4);
	memcpy(&message_count, message + 12, 4);

	std::uint32_t retour;

	if (message_count == 1)
	{
		// if packet lost... current concat must be invalidated
		if (m_concatStream != NULL)
		{
			M3D_LOG_WARN(LoggerName, "Hermes Packet Suite current reconstruction cancelled...");
			this->ReleaseStream(m_concatStream);
			m_concatStream = NULL;
		}

		MovStream *pReadStream = ReserveStream();

		// in case of network if no stream avalaible we discard tuple
		if (pReadStream)
		{
			pReadStream->Rewind();
			pReadStream->Write(message + 36, length - 36, &retour);
			this->PushStream(pReadStream);
		}
		nbTupReceived++;
	}
	else
	{
		//M3D_LOG_INFO(LoggerName, "Hermes RCD Packet Received : %d/%d)", message_idx, message_count);
		if (message_idx == 1)
		{
			if (m_concatStream == NULL)
			{
				m_concatStream = ReserveStream();
			}
			else
			{
				M3D_LOG_WARN(LoggerName, "New Hermes Packet Suite, current reconstruction will be lost...");
			}
			m_concatStream->Rewind();
		}

		if (m_concatStream != NULL)
		{
			m_concatStream->Concat(message + 36, length - 36, &retour);
			if (message_idx == message_count)
			{
				M3D_LOG_INFO(LoggerName, Log::format("Hermes Packet Suite reconstruction finished : %d packets received", message_count));
				this->PushStream(m_concatStream);
				m_concatStream = NULL;
				nbTupReceived++;
			}
		}
	}

	return nbTupReceived;
}

int MovHacSockReadService::replayRCDPackets(int from, int to, int& lastPacketSent)
{
	int nbTupReceived = 0;

	for (int i = from; i <= to; ++i)
	{
		if (m_messageBuffer.find(i) != m_messageBuffer.cend())
		{
			M3D_LOG_WARN(LoggerName, Log::format("Hermes Packet replayed (%d)", i));
			const auto& message = m_messageBuffer[i];
			nbTupReceived += ReadMessageRCD(message.c_str(), message.size());
			m_messageBuffer.erase(i);
			lastPacketSent = i;
		}
		else
		{
			break;
		}
	}

	return nbTupReceived;
}

int MovHacSockReadService::Read(ReaderTraverser &ref)
{
	if (m_Buffers == nullptr)
		return 0;

	// variables locales
	int			i = 0;
	//int Alire = 1000;
	int nbTupReceived = 0;
	unsigned short retour = 0;

	bool ping_not_found = TRUE;

	int rl_nbRead = -1;
	char  tcl_readBuffer[SIZE_MAX_HMS];


	// placement au d�but de IStream
	//libMove essai;
	//essai.LowPart = 0;

	// debut lecture tuples envoyés par HERMES

	// récupération du plus ancien message en stock
	CSocketBuffer * pSocketBuffer = ReadBuffer();
	rl_nbRead = pSocketBuffer->m_BufLen;

	if (rl_nbRead == 0)
	{
		pSocketBuffer->SetConsumeState(true);
		return 0;
	}
	// si aucune donnee : retour

	// copy input buffer
	memcpy(tcl_readBuffer, pSocketBuffer->m_pBuffer, rl_nbRead);

	auto hermesType = getHermesMessageType(tcl_readBuffer, rl_nbRead);

	// RCD messages : data tuples
	if (hermesType == HermesMessageType::RCD && headerencoursdereception == -1)
	{
		// OTK - 11/05/2009 - ajout test sur les numero de packets pour d�tection des pertes
		int messageNo;
		memcpy(&messageNo, tcl_readBuffer + 4, 4);

		bool doReadMessage = true;
		if (messageNo != -2)	// on ignore les messages RCD venant de l'IHM (le numero vaut -2 en dur)
		{
			if (m_FirstMessage)
			{
				m_FirstMessage = false;
			}
			else if (messageNo > m_MessageNo + 1)
			{	
				//The packets might arrive in a incorrect order
				//If one of them is late, stores the packets in a buffer to process them in the correct order later
				if (messageNo - m_MessageNo < NB_MAX_STORED_PACKAGES) //Limit the number of packet to store in case a package is really lost
				{
					int lastPacketSent = m_MessageNo;
					nbTupReceived += replayRCDPackets(m_MessageNo + 1, messageNo - 1, lastPacketSent);
						
					if (lastPacketSent < messageNo - 1)
					{
						//Could not replay the packets until the curent num, store the current packet in the buffer
						doReadMessage = false;
						m_messageBuffer[messageNo] = std::string(tcl_readBuffer, rl_nbRead);
						M3D_LOG_WARN(LoggerName, Log::format("Hermes Packet Stored (%d)", messageNo));

						m_MessageNo = lastPacketSent;
					}
				}
				else
				{
					//Chances are that a packet has been completly lost (or it is really late but this is unlikeky)
					//Try to recover from the oldest packet in the buffer
					M3D_LOG_ERROR(LoggerName, Log::format("Hermes Packets Lost (last : %d, current : %d)", m_MessageNo, messageNo));

					// if packet lost... current concat must be invalidated
					if (m_concatStream != NULL)
					{
						M3D_LOG_WARN(LoggerName, "Hermes Packet Suite current reconstruction cancelled...");
						this->ReleaseStream(m_concatStream);
						m_concatStream = NULL;
					}

					if (!m_messageBuffer.empty())
					{
						int minPacketStored = m_messageBuffer.cbegin()->first;
						if (minPacketStored > m_MessageNo + 1) //Just to ensure the cache is not corrupted with old packets
						{
							int lastPacketSent = 0;
							nbTupReceived += replayRCDPackets(minPacketStored, messageNo - 1, lastPacketSent);

							if (lastPacketSent < messageNo - 1)
							{
								if (messageNo - lastPacketSent < NB_MAX_STORED_PACKAGES)
								{
									//Could not replay the packets until the curent num, store the current packet in the buffer
									doReadMessage = false;
									m_messageBuffer[messageNo] = std::string(tcl_readBuffer, rl_nbRead);
									M3D_LOG_WARN(LoggerName, Log::format("Hermes Packet Stored (%d)", messageNo));
								}
								else
								{
									//To be safe and avoid potential lost packets.
									//Too much gap between the stored packets and the current packet
									//Clean everything
									if (m_concatStream != NULL)
									{
										M3D_LOG_WARN(LoggerName, Log::format("Hermes Packet Suite current reconstruction cancelled..."));
										this->ReleaseStream(m_concatStream);
										m_concatStream = NULL;
									}

									M3D_LOG_WARN(LoggerName, Log::format("Corrupted Cache Packets Cleaned", m_MessageNo, messageNo));
									m_messageBuffer.clear();
								}
							}
						}
						else
						{
							M3D_LOG_WARN(LoggerName, Log::format("Corrupted Cache Packets Cleaned", m_MessageNo, messageNo));
							m_messageBuffer.clear();
						}
					}					
				}				
			}
			else if (messageNo < m_MessageNo + 1)
			{
				m_messageBuffer.clear();
				M3D_LOG_ERROR(LoggerName, Log::format("Hermes Packet Inversion (last : %d, current : %d)", m_MessageNo, messageNo));

				// if packet lost... current concat must be invalidated
				if (m_concatStream != NULL)
				{
					M3D_LOG_WARN(LoggerName, "Hermes Packet Suite current reconstruction cancelled...");
					this->ReleaseStream(m_concatStream);
					m_concatStream = NULL;
				}

				if (m_MessageNo - messageNo < NB_MESSAGES_FOR_PACKAGE_RECOVERY)
				{
					//The message comes to late, it is ignored
					doReadMessage = false;
				}
				else
				{
					//Big inversion. This can happen if Hermes is restarted whithout Movies. Recover from the current message.
					M3D_LOG_WARN(LoggerName, Log::format("Recover from message %d", messageNo));
				}
			}   			
		}
		

		if (doReadMessage)
		{
			if (messageNo != -2)
				m_MessageNo = messageNo;

			nbTupReceived += ReadMessageRCD(tcl_readBuffer, rl_nbRead);
		}
	}
	else if (hermesType == HermesMessageType::RCH)
	{
		if (headerencoursdereception == -1)
		{
			M3D_LOG_INFO(LoggerName, "Start RCH packet decoding");
		}
		headerencoursdereception = 1;
		
		memcpy(bufferHeader + headersize, tcl_readBuffer + 32, rl_nbRead - 32);
		headersize += rl_nbRead - 32;
		unsigned int msgNumber = 0;
		unsigned int numberOfMsg = 0;
		memcpy(&msgNumber, tcl_readBuffer + 4, 4);
		memcpy(&numberOfMsg, tcl_readBuffer + 8, 4);

		// in this cases all the fragmented messages have been merged
		if (msgNumber == numberOfMsg)
			headerencoursdereception = 2;

		M3D_LOG_INFO(LoggerName, Log::format("Message %d of %d [%d]", msgNumber, numberOfMsg, headersize));

		if (headerencoursdereception == 2)
		{
			//  entetes re�us
			headerencoursdereception = -1;

			char bufferTuple[5000];
			memset(bufferTuple, 0, 5000);

			std::uint32_t sizeTuple = 0;
			unsigned int typeTuple = 0;

			int offsetheader = 0;
			while (offsetheader < headersize)
			{
				memcpy(&sizeTuple, bufferHeader + offsetheader, 4);
				memcpy(&typeTuple, bufferHeader + 4 + offsetheader, 2);
				
				if (sizeTuple == 0)
					break;
				if (sizeTuple < 5000)
				{
					memcpy(bufferTuple, bufferHeader + offsetheader, sizeTuple + 10);
					//   copie de caracteres vers Istream

					std::uint32_t retour;
					libMove essai;
					essai.LowPart = 0;

					if (typeTuple == 210 || typeTuple == 11000 || typeTuple == 220 || typeTuple == 230 || typeTuple == 41 || typeTuple == 2200
						|| typeTuple == 2300 || typeTuple == 2100 || typeTuple == 10100 || typeTuple == 2210 || typeTuple == 2310 || typeTuple == 4000
						|| typeTuple == 65535)
					{

						M3D_LOG_INFO(LoggerName, Log::format("Try to Push tuple %d", typeTuple));
						MovStream *pReadStream = ReserveStream();
						if (pReadStream)
						{
							pReadStream->Seek(essai, eSEEK_SET);
							pReadStream->Write(bufferHeader + offsetheader, sizeTuple + 10, &retour);
							this->PushStream(pReadStream);
						}
						offsetheader += sizeTuple + 10;
						nbTupReceived++;
					}
					else
					{
						M3D_LOG_WARN(LoggerName, Log::format("Tuple ignored %d", typeTuple));
						offsetheader = 0;
						headersize = 0;
					}
				}
				else
				{
					M3D_LOG_ERROR(LoggerName, Log::format("Unknown Error on Network : sizeTuple = %d", sizeTuple));
					offsetheader = 0;
					headersize = 0;
				};
			}// fin while offsetheader...

			headersize = 0;

			M3D_LOG_INFO(LoggerName, "RCH packet decoding done");
		}
	}
	else if (hermesType == HermesMessageType::RCS)
	{
		// r��mission des ent�tes pour l'archivage hermes (deja recu ici dans un RCH)
		M3D_LOG_INFO(LoggerName, "RCS message received");
	}
	else if (hermesType == HermesMessageType::RCT)
	{
		// Nouveau tuple environnement pour l'archivage Hermes (deja recu ici dans un RCH)
		M3D_LOG_INFO(LoggerName, "RCT message received");
	}
	else if (hermesType == HermesMessageType::RCP)
	{
		// Parametrage sondeur (normalement pas sur la m�me ip que movies, donc impossible)
		M3D_LOG_WARN(LoggerName, "Sounder configuration message received !");
	}
	else if (hermesType == HermesMessageType::RCE)
	{
		// Fin de l'enregistrement Hermes
		M3D_LOG_INFO(LoggerName, "Hermes archiving stopped");
	}
	else
	{
		M3D_LOG_INFO(LoggerName, "Unknown Header Received ");
	}
	
	pSocketBuffer->SetConsumeState(true);

	return nbTupReceived;
}

#endif
