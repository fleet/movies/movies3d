/******************************************************************************/
/*	Project:	AVITIS														  */
/*	Author(s):	C.PONCELET													  */
/*	File:		 MovReadService.cpp										  */
/******************************************************************************/

#include "Reader/MovReadService.h"

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/
MovReadService::MovReadService()
	: m_StreamDesc{ "Stream Not Initialized" }
{
}

MovReadService::~MovReadService()
{
	// TODO call close...
}

bool MovReadService::ReOpen(std::uint32_t pos)
{
	Close();
	return SeekFilePos(pos);
}

const char * MovReadService::getStreamDesc() const
{
	return m_StreamDesc.c_str();
}

void MovReadService::setStreamDesc(const char * streamDesc)
{
	m_StreamDesc = streamDesc;
}

std::uint32_t MovReadService::GetCurrentFilePos() const
{
	return 0;
}