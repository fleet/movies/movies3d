
#include "Reader/MovStream.h"

#include <stdio.h>
#include <string.h>

const std::uint32_t MovStream::defaultSize = 100;

MovStream::MovStream()
    : m_pData(nullptr)
{
	Resize(defaultSize);
}

MovStream::~MovStream()
{
	if (m_pData)
		delete[] m_pData;
}

void MovStream::Reserve(std::uint32_t newSize)
{
	if (m_reservedSize < newSize)
	{
		m_reservedSize = newSize + 100;
		if (m_pData)
			delete[] m_pData;
		m_pData = new char[m_reservedSize];
		m_pCurrent = m_pData;
	}
}

void MovStream::Resize(std::uint32_t newSize)
{
	m_reservedSize = newSize + 100;
	if (m_pData) { delete[] m_pData; }
	m_pData = new char[m_reservedSize];
	m_pCurrent = m_pData;
}

// OTK - 04/03/2009 - on conserve les donn�es lors du ConservativeReserve
// (utile lorsqu'on ne sait pas � l'avance la quantit� de donn�es � �crire
// dans le MovStream : Tup10090::Encode, mais pas a faire syst�matiquement
// car plus couteux : on garde le Reserve)
void	MovStream::ConservativeReserve(std::uint32_t newSize)
{
	if (m_reservedSize < newSize)
	{
		std::uint32_t oldSize = m_reservedSize;
		m_reservedSize = newSize + 100;
        char * bufTemp = nullptr;
		if (m_pData)
		{
			bufTemp = new char[oldSize];
			memcpy(bufTemp, m_pData, oldSize);
			delete[] m_pData;
		}
		m_pData = new char[m_reservedSize];

		if (bufTemp)
		{
			memcpy(m_pData, bufTemp, oldSize);
			delete[] bufTemp;
		}
		m_pCurrent = m_pData;
	}
}

char *	MovStream::GetWriteBuffer(std::uint32_t sizeWanted)
{
	Reserve(sizeWanted);
	return m_pData;
}

// OTK - 04/03/2009 - on conserve les donn�es lors du ConservativeReserve
// (utile lorsqu'on ne sait pas � l'avance la quantit� de donn�es � �crire
// dans le MovStream : Tup10090::Encode, mais pas a faire syst�matiquement
// car plus couteux : on garde le Reserve)
char *	MovStream::GetConservativeWriteBuffer(std::uint32_t sizeWanted)
{
	ConservativeReserve(sizeWanted);
	return m_pData;
}

char *	MovStream::GetReadBuffer()
{
	return m_pData;
}

bool MovStream::Seek(libMove dlibMove, std::uint32_t dwOrigin)
{
	if (dwOrigin == eSEEK_SET)
		m_pCurrent = m_pData;
	m_pCurrent += dlibMove.LowPart;
	return true;
}

bool MovStream::Rewind()
{
	m_pCurrent = m_pData;
	return true;
}

bool	MovStream::Read(char* pv, std::uint32_t cb, std::uint32_t* pcbRead)
{
	memcpy(pv, m_pCurrent, cb);
	m_pCurrent += cb;
	if (pcbRead)
		*pcbRead = cb;
	return true;
}

bool	MovStream::Write(char const* pv, std::uint32_t cb, std::uint32_t* pcbWritten)
{
	const int currentOffset = m_pCurrent - m_pData;
	Reserve(currentOffset + cb);
	memcpy(m_pCurrent, pv, cb);
	
	m_pCurrent += cb;
	return true;
}

bool	MovStream::Concat(char const* pv, std::uint32_t cb, std::uint32_t* pcbWritten)
{
	const int currentOffset = m_pCurrent - m_pData;
	const int concatSize = currentOffset + cb;
	if (concatSize > m_reservedSize)
	{
		ConservativeReserve(concatSize);
		m_pCurrent = m_pData + currentOffset;
	}
	memcpy(m_pCurrent, pv, cb);
	m_pCurrent += cb;
	return true;
}
