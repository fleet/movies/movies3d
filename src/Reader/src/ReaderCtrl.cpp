
#include "Reader/ReaderCtrl.h"
#include "Reader/ReaderTraverser.h"

#include "Reader/MovSonarNetCDFFileReadService.h"
#include "Reader/MovHacSockReadService.h"
#include "Reader/MovHacFileReadService.h"

#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/algorithm/base/EchoAlgorithmRoot.h"
#include "Reader/WriteAlgorithm/WriteAlgorithm.h"
#include "M3DKernel/M3DKernel.h"

#include <thread>
#include <atomic>
#include <cctype>

#ifdef _MANAGED
#pragma managed(push, off)
#endif

#ifdef WIN32
// Exclure les en-t�tes Windows rarement utilis�s
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

#else
__attribute__((destructor)) void dllUnload()
{
    ReaderCtrl::FreeMemory();
}
#endif

#ifdef _MANAGED
#pragma managed(pop)
#endif

namespace
{
	constexpr const char * LoggerName = "Reader.ReaderCtrl";
}

class ReaderCtrl::Impl
{
public:
	MovReadService *m_pService;
    ChunkEval *m_pChunkEval;

    WriteAlgorithm *m_pWriteAlgorithm;

    ReaderParameter m_readerParam;

    CRecursiveMutex m_ThreadLock;

    std::uint32_t   m_dwThreadIdArray;
    std::thread m_hThread;
	std::atomic_bool m_ThreadRunning;

	static std::string getExtension(const std::string & str)
	{
		std::string ext;
		const auto pos = str.find_last_of('.');
		if (pos != std::string::npos)
		{
			ext = str.substr(pos + 1);
		}
		return ext;
	}

};


ReaderCtrl* ReaderCtrl::m_pReaderCtrl = nullptr;

ReaderCtrl* ReaderCtrl::getInstance()
{
	if (!m_pReaderCtrl)
	{
		m_pReaderCtrl = new ReaderCtrl();
	}
	return m_pReaderCtrl;
}

void ReaderCtrl::FreeMemory()
{
	if (m_pReaderCtrl)
	{
		delete m_pReaderCtrl;
		m_pReaderCtrl = nullptr;
	}
}

ReaderCtrl::ReaderCtrl()
    : impl(new ReaderCtrl::Impl)
{
	impl->m_ThreadRunning = false;

    impl->m_pService = nullptr;
    impl->m_pChunkEval = nullptr;
	UpdateChunckDef();

    impl->m_pWriteAlgorithm = WriteAlgorithm::Create();
    impl->m_pWriteAlgorithm->AddInput(M3DKernel::GetInstance()->getWriteAlgorithmRoot());
    MovRef(impl->m_pWriteAlgorithm);
}

ReaderCtrl::~ReaderCtrl()
{
    MovUnRefDelete(impl->m_pChunkEval);
    MovUnRefDelete(impl->m_pWriteAlgorithm);

    if (impl->m_pService)
	{
		RemoveService();
	}

	if (impl->m_hThread.joinable())
	{
		impl->m_hThread.join();
	}

    delete impl;
}


void ReaderCtrl::ThreadRead()
{
	impl->m_ThreadRunning = true;
	TimeCounter readerTime;
	readerTime.StartCount();
	ReadChunk();
	readerTime.StopCount();
	M3DKernel::GetInstance()->m_timeCounterRecord.AddTimeCounter("Reader Time", readerTime);
	impl->m_ThreadRunning = false;
}

void MyThreadFunction(ReaderCtrl* lpParam)
{
    lpParam->ThreadRead();
}

// start reading a chunck (thread)
void ReaderCtrl::StartReadChunck()
{
    if (impl->m_hThread.joinable())
	{
        ChunckEventList ref;
        WaitEndChunck(ref);
	}
    impl->m_hThread = std::thread(&MyThreadFunction, this);
}

// block until chunk is read
void ReaderCtrl::WaitEndChunck(ChunckEventList &ref)
{
    if (impl->m_hThread.joinable())
	{
        impl->m_hThread.join();
	}
    ref = impl->m_pChunkEval->m_CurrentEventList;
}

// abort current chunck reading
void ReaderCtrl::AbortReadChunck(ChunckEventList &ref)
{
	impl->m_ThreadLock.Lock();
    impl->m_pChunkEval->Abort();
	impl->m_ThreadLock.Unlock();
	WaitEndChunck(ref);
}

// tell if the chunck was read, non blocking
bool ReaderCtrl::CheckChunckWasRead(ChunckEventList &ref)
{
	bool stillRunning = true;
    if (impl->m_ThreadRunning)
	{
		stillRunning = true;
	}
	else
	{
        ref = impl->m_pChunkEval->m_CurrentEventList;
        stillRunning = false;
	}
	return !stillRunning;
}

// OTK - 02/03/2009 - impl�mentation du GoTo
bool ReaderCtrl::GoTo(GoToTarget target)
{
	if (impl->m_pService == nullptr)
		return false;

	if (!impl->m_pService->IsFileService()) {
		M3D_LOG_ERROR(LoggerName, "Execution of GoTo failed : current service is not a file service !");
		return false;
	}

	M3DKernel::GetInstance()->Lock();

	ReaderTraverser traverser(GetActiveService());
	traverser.m_pHacObjectMgr->Update(traverser);

	// NMD - FAE 115
	// ouverture du fichier pour le goto
	if (impl->m_pService->PrepareGoTo(target, traverser))
	{
		ReadChunk();
	}

	bool status = impl->m_pService->ExecuteGoTo(target, traverser);

	if (target.byPing)
	{
		M3D_LOG_INFO(LoggerName, Log::format("GoTo ping : %d", target.targetPingNumber));
	}
	else
	{
		char desc[255];
		target.targetTime.GetTimeDesc(desc, 254);
		M3D_LOG_INFO(LoggerName, Log::format("GoTo date : %s", desc));
	}

	M3DKernel::GetInstance()->Unlock();

	return status;
}

// OTK - 05/11/2009 - lecture synchrone (dans le thread principal), utile pour matlab
void ReaderCtrl::SyncReadChunk()
{
	ReadChunk();
}

void ReaderCtrl::ReadChunk()
{
	///ReadSounderConfig : we stop once we got at least one Fan
	bool shouldRead = true;
    if (impl->m_pChunkEval == NULL)
	{
		M3D_LOG_ERROR(LoggerName, "No avalaible chunk Def");
		return;
	}

	impl->m_ThreadLock.Lock();
    impl->m_pChunkEval->Start();
	impl->m_ThreadLock.Unlock();

	while (shouldRead)
	{
		M3DKernel::GetInstance()->Lock();
		ReaderTraverser traverser(GetActiveService());
		traverser.m_pHacObjectMgr->Update(traverser);
		M3DKernel::GetInstance()->Unlock();

		if (impl->m_pService)
		{
            impl->m_pService->Execute(traverser);
		}
		else
		{			
			shouldRead = false;
			M3D_LOG_ERROR(LoggerName, "No Read Service Set");
			break;
		}

		/// now evaluate if we stop reading
		impl->m_ThreadLock.Lock();
        shouldRead = !impl->m_pChunkEval->CheckStop();
		impl->m_ThreadLock.Unlock();
	}

	impl->m_ThreadLock.Lock();
    impl->m_pChunkEval->Stop();
	impl->m_ThreadLock.Unlock();
}

void ReaderCtrl::ReadSounderConfig()
{
    MovUnRefDelete(impl->m_pChunkEval);

	ChunkEvalNumbered *pChunkEval = ChunkEvalNumbered::Create();
	pChunkEval->AddInput(M3DKernel::GetInstance()->getEchoAlgorithmRoot());
    impl->m_pChunkEval = pChunkEval;
    MovRef(impl->m_pChunkEval);

	// OTK - 14/04/2010 - sous matlab, (on appelle ReadSounderConfig que sous matlab)
	// tout se fait dans le thread Matlab pour pouvoir g�rer les sorties console correctement
	//StartReadChunck();
	SyncReadChunk();
	ChunckEventList ref;
	WaitEndChunck(ref);

	UpdateChunckDef();
}

bool ReaderCtrl::IsFileService() const
{
	bool ret = false;
	if (impl->m_pService)
	{
		ret = impl->m_pService->IsFileService();
	}
	return ret;
}

MovFileRun ReaderCtrl::GetFileRun() const
{
	MovFileRun fRun;
	if (impl->m_pService)
	{
		fRun = impl->m_pService->GetFileRun();
	}
	return fRun;
}

bool ReaderCtrl::GetLimits(HacTime &minTime, std::uint32_t &minPing, HacTime &maxTime, std::uint32_t &maxPing)
{
	bool status = false;
	if (impl->m_pService)
	{
		status = impl->m_pService->GetLimits(minTime, minPing, maxTime, maxPing);
	}
	return status;
}

void ReaderCtrl::UpdateChunckDef()
{
    MovUnRefDelete(impl->m_pChunkEval);

    if (!impl->m_readerParam.m_ChunckDef.m_bIsUseTimeDef)
	{
		ChunkEvalNumbered *pchunkEval = ChunkEvalNumbered::Create();
        pchunkEval->Init(impl->m_readerParam.m_ChunckDef.m_SounderId, impl->m_readerParam.m_ChunckDef.m_NumberOfFanToRead);
		pchunkEval->AddInput(M3DKernel::GetInstance()->getEchoAlgorithmRoot());
        impl->m_pChunkEval = pchunkEval;
	}
	else
	{
		ChunkEvalTimed *pchunkEval = ChunkEvalTimed::Create();
        pchunkEval->Init(impl->m_readerParam.m_ChunckDef.m_TimeElapsed);
		pchunkEval->AddInput(M3DKernel::GetInstance()->getEchoAlgorithmRoot());
        impl->m_pChunkEval = pchunkEval;
	}
    MovRef(impl->m_pChunkEval);
}

void ReaderCtrl::UpdateReaderParameter(ReaderParameter &RefParameter, bool ignoreChunkChange)
{
	impl->m_ThreadLock.Lock();

    impl->m_readerParam.setSortDataBeforeWrite(RefParameter.getSortDataBeforeWrite());
    impl->m_pWriteAlgorithm->SetUseSort(impl->m_readerParam.getSortDataBeforeWrite());

    impl->m_readerParam.setAcquisitionPort(RefParameter.getAcquisitionPort());
    impl->m_readerParam.setCallbackPort(RefParameter.getCallbackPort());

	if (!ignoreChunkChange)
	{
        impl->m_readerParam.m_ChunckDef = RefParameter.m_ChunckDef;
		UpdateChunckDef();
	}
	impl->m_ThreadLock.Unlock();

	// OTK - FAE064 - mise � jour du sondeur de r�f�rence dans l'ESU Manager
	M3DKernel::GetInstance()->getMovESUManager()->SetSounderRef(RefParameter.m_ChunckDef.m_SounderId);
}

void ReaderCtrl::RemoveService()
{
	if (impl->m_pService == nullptr)
		return;

	impl->m_pService->Close();
	delete impl->m_pService;
    impl->m_pService = nullptr;
}

void ReaderCtrl::OpenNetworkStream(unsigned short a1, unsigned short a2, unsigned short a3, unsigned short a4)
{
	impl->m_ThreadLock.Lock();
	M3DKernel::GetInstance()->ClearObjectManager();
    if (impl->m_pService)
	{
		RemoveService();
	}
	
#ifdef WIN32
    impl->m_pService = new MovHacSockReadService(a1, a2, a3, a4, impl->m_readerParam.getAcquisitionPort(), impl->m_readerParam.getCallbackPort());
#else
    M3D_LOG_WARN(LoggerName, "MovSockReadService not implemented on linux");
#endif

	//LB 06/10/2008 on passe le service � l'�criture
    impl->m_pWriteAlgorithm->SetReaderService(impl->m_pService);
	impl->m_ThreadLock.Unlock();
}

void ReaderCtrl::OpenFileStream(MovFileRun &filerun)
{
	impl->m_ThreadLock.Lock();
	M3DKernel::GetInstance()->ClearObjectManager();
    if (impl->m_pService)
	{
		RemoveService();
	}
	auto ext = ReaderCtrl::Impl::getExtension(filerun.GetFileName(0));
	std::transform(ext.begin(), ext.end(), ext.begin(), [](unsigned char c) { return std::tolower(c); }); // To lowercase

	// TODO Check extension consitency
	if (ext == "hac")
	{
		impl->m_pService = new MovHacFileReadService(filerun);
	}
	else if (ext == "nc")
	{
		impl->m_pService = new MovSonarNetCDFFileReadService(filerun);
	}
	else
	{
		M3D_LOG_WARN(LoggerName, Log::format("Unsupported file format : %s", ext));
	}

	if (impl->m_pService)
	{	
		// LB 06/10/2008 on passe le service à l'écriture
		impl->m_pWriteAlgorithm->SetReaderService(impl->m_pService);
	}

	impl->m_ThreadLock.Unlock();
}

ReaderParameter ReaderCtrl::GetReaderParameter()
{
    return impl->m_readerParam;
}

void ReaderCtrl::SetIgnoreSingleTargets(bool b)
{
    impl->m_readerParam.setIgnoreSingleTargets(b);
}

WriteAlgorithm* ReaderCtrl::GetWriteAlgorithm()
{
    return impl->m_pWriteAlgorithm;
}

MovReadService * ReaderCtrl::GetActiveService()
{
    return impl->m_pService;
}
