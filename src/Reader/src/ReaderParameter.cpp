
#include "Reader/ReaderParameter.h"
#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

ReaderParameter::ReaderParameter(void) : ParameterModule("ReaderParameter")
{
	ResetData();
}


void ReaderParameter::ResetData()
{
	m_sortDataBeforeWrite = false;
	m_ignoreSingleTargets = false;
	m_acquisitionport = 37778;
	m_callbackPort = 37781;
}

// sauvegarde des param�tres dans le fichier de configuration associ�
bool ReaderParameter::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	// tri des donn�es pour l'�criture
	movConfig->SerializeData<bool>(m_sortDataBeforeWrite, eBool, "SortDataBeforeWrite");
	// parametres de dimensionnement du chunk pour la lecture
	movConfig->SerializeData<bool>(m_ChunckDef.m_bIsUseTimeDef, eBool, "IsUseTimeDef");
	movConfig->SerializeData<unsigned int>(m_ChunckDef.m_TimeElapsed, eUInt, "TimeElapsed");
	movConfig->SerializeData<std::uint32_t>(m_ChunckDef.m_SounderId, eUInt, "SounderId");
	movConfig->SerializeData<unsigned int>(m_ChunckDef.m_NumberOfFanToRead, eUInt, "NumberOfFanToRead");

	movConfig->SerializeData<int>(m_acquisitionport, eUInt, "AcquisitionPort");
	movConfig->SerializeData<int>(m_callbackPort, eUInt, "CallbackPort");

	// fin de la s�rialisation du module
	movConfig->SerializePushBack();

	return true;
}

// lecture des param�tres dans le fichier de configuration associ�
bool ReaderParameter::DeSerialize(MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	// debut de la d�s�rialisation du module
	if (result)
	{
		// tri des donn�es pour l'�criture
		result = result && movConfig->DeSerializeData<bool>(&m_sortDataBeforeWrite, eBool, "SortDataBeforeWrite");
		// parametres de dimensionnement du chunk pour la lecture
		result = result && movConfig->DeSerializeData<bool>(&m_ChunckDef.m_bIsUseTimeDef, eBool, "IsUseTimeDef");
		result = result && movConfig->DeSerializeData<unsigned int>(&m_ChunckDef.m_TimeElapsed, eUInt, "TimeElapsed");
		result = result && movConfig->DeSerializeData<std::uint32_t>(&m_ChunckDef.m_SounderId, eUInt, "SounderId");
		result = result && movConfig->DeSerializeData<unsigned int>(&m_ChunckDef.m_NumberOfFanToRead, eUInt, "NumberOfFanToRead");

		result = result && movConfig->DeSerializeData<int>(&m_acquisitionport, eUInt, "AcquisitionPort");
		result = result && movConfig->DeSerializeData<int>(&m_callbackPort, eUInt, "CallbackPort");
	}

	// fin de la d�s�rialisation du module
	movConfig->DeSerializePushBack();

	return result;
}
