#include "Reader/ServiceFormatDesc.h"


HacServiceFormatDesc::HacServiceFormatDesc()
	: m_TupleType(0)
	, m_HacId(0)
	, m_HacVersion(0)
	, m_AcqSoftwareVersion(0)
	, m_AcqSoftwareID(0)
	, m_tupleAttributes(0)
{
}

unsigned short HacServiceFormatDesc::getTupleType() const
{
	return m_TupleType;
}

std::map<std::string, std::string> HacServiceFormatDesc::getDesc() const
{
	std::map<std::string, std::string> desc;

	desc["tuple_type"]  = m_TupleType;
	desc["hac_ident"]   = m_HacId;
	desc["hac_version"] = m_HacVersion;
	desc["acq_version"] = m_AcqSoftwareVersion;
	desc["acq_ident"]   = m_AcqSoftwareID;

	return desc;
}