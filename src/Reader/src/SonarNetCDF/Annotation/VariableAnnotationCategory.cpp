#include "Reader/SonarNetCDF/Annotation/VariableAnnotationCategory.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Annotation/VariableAnnotationTime.h"

using namespace sonarNetCDF;

// Variable name
const std::string VariableAnnotationCategory::VARIABLE_NAME = "annotation_category";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Annotation category";
}

VariableAnnotationCategory::VariableAnnotationCategory(ncObject* parent, VariableAnnotationTime* timeVariable)
	: VariableSingleVariable<std::string, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableAnnotationCategory::~VariableAnnotationCategory() {
}

void VariableAnnotationCategory::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
