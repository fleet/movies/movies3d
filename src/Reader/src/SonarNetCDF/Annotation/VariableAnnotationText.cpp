#include "Reader/SonarNetCDF/Annotation/VariableAnnotationText.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Annotation/VariableAnnotationTime.h"

using namespace sonarNetCDF;

// Variable name
const std::string VariableAnnotationText::VARIABLE_NAME = "annotation_text";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Annotation text";
}

VariableAnnotationText::VariableAnnotationText(ncObject* parent, VariableAnnotationTime* timeVariable)
	: VariableSingleVariable<std::string, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableAnnotationText::~VariableAnnotationText() {
}

void VariableAnnotationText::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
