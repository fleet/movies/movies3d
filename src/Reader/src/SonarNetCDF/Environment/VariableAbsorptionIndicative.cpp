#include "Reader/SonarNetCDF/Environment/VariableAbsorptionIndicative.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Environment/VariableAcousticFrequency.h"

using namespace sonarNetCDF;

// Variable name
const std::string VariableAbsorptionIndicative::VARIABLE_NAME = "absorption_indicative";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Indicative acoustic absorption";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB/m";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableAbsorptionIndicative::VariableAbsorptionIndicative(ncObject* parent, VariableAcousticFrequency* acousticFrequencyVariable)
	: VariableSingleVariable<float, float>(parent, VARIABLE_NAME, acousticFrequencyVariable)
{
	createVariable();
}

VariableAbsorptionIndicative::~VariableAbsorptionIndicative() {
}

void VariableAbsorptionIndicative::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
