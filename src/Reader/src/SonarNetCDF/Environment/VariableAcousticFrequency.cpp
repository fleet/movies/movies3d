#include "Reader/SonarNetCDF/Environment/VariableAcousticFrequency.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;

// Variable name
const std::string VariableAcousticFrequency::VARIABLE_NAME = "frequency";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Acoustic Frequency";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "sound_frequency";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "Hz";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableAcousticFrequency::VariableAcousticFrequency(ncObject* parent, ncDimension * acousticFrequencyDimension)
	: VariableSingleDimension<float>(parent, VARIABLE_NAME, acousticFrequencyDimension)
{
	createVariable();
}

VariableAcousticFrequency::~VariableAcousticFrequency() {
}

void VariableAcousticFrequency::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
