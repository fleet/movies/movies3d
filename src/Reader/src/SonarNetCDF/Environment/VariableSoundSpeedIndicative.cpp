#include "Reader/SonarNetCDF/Environment/VariableSoundSpeedIndicative.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;

// Variable name
const std::string VariableSoundSpeedIndicative::VARIABLE_NAME = "sound_speed_indicative";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Indicative sound speed";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "speed_of_sound_in_sea_water";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m/s";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableSoundSpeedIndicative::VariableSoundSpeedIndicative(ncObject* parent)
	: VariableScalar<float>(parent, VARIABLE_NAME)
{
	createVariable();
}

VariableSoundSpeedIndicative::~VariableSoundSpeedIndicative() {
}

void VariableSoundSpeedIndicative::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
