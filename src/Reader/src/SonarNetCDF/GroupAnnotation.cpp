#include "Reader/SonarNetCDF/GroupAnnotation.h"

#include "Reader/SonarNetCDF/Annotation/VariableAnnotationTime.h"
#include "Reader/SonarNetCDF/Annotation/VariableAnnotationCategory.h"
#include "Reader/SonarNetCDF/Annotation/VariableAnnotationText.h"

#include <iomanip>

using namespace sonarNetCDF;

// Group name
const std::string GroupAnnotation::NC_GROUP_NAME = "Annotation";

GroupAnnotation::GroupAnnotation(ncGroup* const parent) : UnderTopLevelGroup(parent, NC_GROUP_NAME)
{
	// Cr�ation des dimensions
	m_timeDimension = std::make_unique<ncDimension>(this, VariableAnnotationTime::VARIABLE_NAME, NC_UNLIMITED);

	m_timeVariable = std::make_unique<VariableAnnotationTime>(this, m_timeDimension.get());

	// Cr�ation des variables
	m_annotationCategoryVariable = std::make_unique<VariableAnnotationCategory>(this, m_timeVariable.get());
	m_annotationTextVariable = std::make_unique<VariableAnnotationText>(this, m_timeVariable.get());
}

GroupAnnotation::~GroupAnnotation() {
}

const std::map<std::chrono::nanoseconds, std::pair<std::string, std::string>> GroupAnnotation::getAnnotationMap() {
	std::map<std::chrono::nanoseconds, std::pair<std::string, std::string>> annotationMap;

	for (auto time : m_timeVariable->getAllValues()) {
		annotationMap[std::chrono::nanoseconds(time)] = 
			std::make_pair<std::string, std::string>(
				m_annotationCategoryVariable->getValueAt(time), 
				m_annotationTextVariable->getValueAt(time)
				);
	}

	return annotationMap;
}

VariableAnnotationTime * GroupAnnotation::getAnnotationTimeVariable() const {
	return m_timeVariable.get();
}

VariableAnnotationCategory * GroupAnnotation::getAnnotationCategoryVariable() const {
	return m_annotationCategoryVariable.get();
}

VariableAnnotationText * GroupAnnotation::getAnnotationTextVariable() const {
	return m_annotationTextVariable.get();
}
