#include "Reader/SonarNetCDF/GroupEnvironment.h"

#include "Reader/SonarNetCDF/Environment/VariableAcousticFrequency.h"
#include "Reader/SonarNetCDF/Environment/VariableAbsorptionIndicative.h"
#include "Reader/SonarNetCDF/Environment/VariableSoundSpeedIndicative.h"

using namespace sonarNetCDF;

// Group name
const std::string GroupEnvironment::NC_GROUP_NAME = "Environment";

GroupEnvironment::GroupEnvironment(
	ncGroup* const parent,
	const size_t& frequencyCount
) : UnderTopLevelGroup(parent, NC_GROUP_NAME)
{
	// Cr�ation des dimensions
	m_acousticFrequencyDimension = std::make_unique<ncDimension>(this, VariableAcousticFrequency::VARIABLE_NAME, frequencyCount);

	m_acousticFrequenciesVariable = std::make_unique<VariableAcousticFrequency>(this, m_acousticFrequencyDimension.get());

	// Cr�ation des variables
	m_absorptionIndicativeVariable = std::make_unique<VariableAbsorptionIndicative>(this, m_acousticFrequenciesVariable.get());
	m_soundSpeedIndicativeVariable = std::make_unique<VariableSoundSpeedIndicative>(this);
}

GroupEnvironment::~GroupEnvironment() {
}

VariableAbsorptionIndicative * GroupEnvironment::getAbsorptionIndicativeVariable() const {
	return m_absorptionIndicativeVariable.get();
}

VariableAcousticFrequency * GroupEnvironment::getAcousticFrequencyVariable() const {
	return m_acousticFrequenciesVariable.get();
}

VariableSoundSpeedIndicative * GroupEnvironment::getSoundSpeedIndicativeVariable() const {
	return m_soundSpeedIndicativeVariable.get();
}
