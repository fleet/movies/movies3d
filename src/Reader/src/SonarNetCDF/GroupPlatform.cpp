#include "Reader/SonarNetCDF/GroupPlatform.h"

#include "Reader/SonarNetCDF/Platform/PlatformCustomTypes.h"

#include "Reader/SonarNetCDF/Platform/VariableMRUIDs.h"
#include "Reader/SonarNetCDF/Platform/VariableMRUOffsetX.h"
#include "Reader/SonarNetCDF/Platform/VariableMRUOffsetY.h"
#include "Reader/SonarNetCDF/Platform/VariableMRUOffsetZ.h"
#include "Reader/SonarNetCDF/Platform/VariableMRURotationX.h"
#include "Reader/SonarNetCDF/Platform/VariableMRURotationY.h"
#include "Reader/SonarNetCDF/Platform/VariableMRURotationZ.h"
#include "Reader/SonarNetCDF/Platform/VariablePositionIDs.h"
#include "Reader/SonarNetCDF/Platform/VariablePositionOffsetX.h"
#include "Reader/SonarNetCDF/Platform/VariablePositionOffsetY.h"
#include "Reader/SonarNetCDF/Platform/VariablePositionOffsetZ.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerIDs.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerFunction.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerOffsetX.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerOffsetY.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerOffsetZ.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerRotationX.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerRotationY.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerRotationZ.h"
#include "Reader/SonarNetCDF/Platform/VariableWaterLevel.h"

#include "Reader/SonarNetCDF/Platform/GroupAttitude.h"
#include "Reader/SonarNetCDF/Platform/GroupPosition.h"

using namespace sonarNetCDF;
using namespace platform;

// Group name
const std::string GroupPlatform::NC_GROUP_NAME = "Platform";

namespace {
	// Dimension names
	constexpr const char* TRANSDUCER_DIMENSION_NAME = "transducer";
	constexpr const char* POSITION_DIMENSION_NAME = "position";
	constexpr const char* MRU_DIMENSION_NAME = "MRU";

	// Attribute names
	constexpr const char* PLATFORM_CODE_ICES_ATTRIBUTE_NAME = "platform_code_ICES";
	constexpr const char* PLATFORM_NAME_ATTRIBUTE_NAME = "platform_name";
	constexpr const char* PLATFORM_TYPE_ATTRIBUTE_NAME = "platform_type";
}

GroupPlatform::GroupPlatform(
	ncGroup* const parent,
	const size_t& transducerCount,
	const size_t& positionCount,
	const size_t& mruCount)
	: UnderTopLevelGroup(parent, NC_GROUP_NAME)
{
	if (isEditable()) {
		// Cr�ation des attributs
		initializeAttributes();

		// Cr�ation des types personnalis�s
		initializeCustomType();
	}

	// Cr�ation des dimensions
	m_transducerDimension = std::make_unique<ncDimension>(this, TRANSDUCER_DIMENSION_NAME, transducerCount);
	m_positionDimension = std::make_unique<ncDimension>(this, POSITION_DIMENSION_NAME, positionCount);
	m_mruDimension = std::make_unique<ncDimension>(this, MRU_DIMENSION_NAME, mruCount);

	m_transducerIdsVariable = std::make_unique<VariableTransducerIds>(this, m_transducerDimension.get());
	m_positionIdsVariable = std::make_unique<VariablePositionIds>(this, m_positionDimension.get());
	m_mruIdsVariable = std::make_unique<VariableMruIds>(this, m_mruDimension.get());

	// Cr�ation des variables
	m_mruOffsetXVariable = std::make_unique<VariableMruOffsetX>(this, m_mruIdsVariable.get());
	m_mruOffsetYVariable = std::make_unique<VariableMruOffsetY>(this, m_mruIdsVariable.get());
	m_mruOffsetZVariable = std::make_unique<VariableMruOffsetZ>(this, m_mruIdsVariable.get());
	m_mruRotationXVariable = std::make_unique<VariableMruRotationX>(this, m_mruIdsVariable.get());
	m_mruRotationYVariable = std::make_unique<VariableMruRotationY>(this, m_mruIdsVariable.get());
	m_mruRotationZVariable = std::make_unique<VariableMruRotationZ>(this, m_mruIdsVariable.get());

	m_positionOffsetXVariable = std::make_unique<VariablePositionOffsetX>(this, m_positionIdsVariable.get());
	m_positionOffsetYVariable = std::make_unique<VariablePositionOffsetY>(this, m_positionIdsVariable.get());
	m_positionOffsetZVariable = std::make_unique<VariablePositionOffsetZ>(this, m_positionIdsVariable.get());

	m_transducerFunctionVariable = std::make_unique<VariableTransducerFunction>(this, m_transducerIdsVariable.get());
	m_transducerOffsetXVariable = std::make_unique<VariableTransducerOffsetX>(this, m_transducerIdsVariable.get());
	m_transducerOffsetYVariable = std::make_unique<VariableTransducerOffsetY>(this, m_transducerIdsVariable.get());
	m_transducerOffsetZVariable = std::make_unique<VariableTransducerOffsetZ>(this, m_transducerIdsVariable.get());
	m_transducerRotationXVariable = std::make_unique<VariableTransducerRotationX>(this, m_transducerIdsVariable.get());
	m_transducerRotationYVariable = std::make_unique<VariableTransducerRotationY>(this, m_transducerIdsVariable.get());
	m_transducerRotationZVariable = std::make_unique<VariableTransducerRotationZ>(this, m_transducerIdsVariable.get());

	m_waterLevelVariable = std::make_unique<VariableWaterLevel>(this);
}

GroupPlatform::~GroupPlatform() {
}


ncDimension * const GroupPlatform::getTransducerDimension() const {
	return m_transducerDimension.get();
}

ncDimension * const GroupPlatform::getPositionDimension() const {
	return m_positionDimension.get();
}

ncDimension * const GroupPlatform::getMruDimension() const {
	return m_mruDimension.get();
}

void GroupPlatform::setPlatformCodeIces(const std::string & platformCodeIces) {
	setAttributeValue(PLATFORM_CODE_ICES_ATTRIBUTE_NAME, platformCodeIces);
}

void GroupPlatform::setPlatformName(const std::string & platformName) {
	setAttributeValue(PLATFORM_NAME_ATTRIBUTE_NAME, platformName);
}

void GroupPlatform::setPlatformType(const std::string & platformType) {
	setAttributeValue(PLATFORM_TYPE_ATTRIBUTE_NAME, platformType);
}

VariableMruIds * GroupPlatform::getMruIdsVariable() const {
	return m_mruIdsVariable.get();
}

VariableMruOffsetX * GroupPlatform::getMruOffsetXVariable() const {
	return m_mruOffsetXVariable.get();
}

VariableMruOffsetY * GroupPlatform::getMruOffsetYVariable() const {
	return m_mruOffsetYVariable.get();
}

VariableMruOffsetZ * GroupPlatform::getMruOffsetZVariable() const {
	return m_mruOffsetZVariable.get();
}

VariableMruRotationX * GroupPlatform::getMruRotationXVariable() const {
	return m_mruRotationXVariable.get();
}

VariableMruRotationY * GroupPlatform::getMruRotationYVariable() const {
	return m_mruRotationYVariable.get();
}

VariableMruRotationZ * GroupPlatform::getMruRotationZVariable() const {
	return m_mruRotationZVariable.get();
}

VariablePositionIds * GroupPlatform::getPositionIdsVariable() const {
	return m_positionIdsVariable.get();
}

VariablePositionOffsetX * GroupPlatform::getPositionOffsetXVariable() const {
	return m_positionOffsetXVariable.get();
}

VariablePositionOffsetY * GroupPlatform::getPositionOffsetYVariable() const {
	return m_positionOffsetYVariable.get();
}

VariablePositionOffsetZ * GroupPlatform::getPositionOffsetZVariable() const {
	return m_positionOffsetZVariable.get();
}

VariableTransducerIds * GroupPlatform::getTransducerIdsVariable() const {
	return m_transducerIdsVariable.get();
}

VariableTransducerFunction * GroupPlatform::getTransducerFunctionVariable() const {
	return m_transducerFunctionVariable.get();
}

VariableTransducerOffsetX * GroupPlatform::getTransducerOffsetXVariable() const {
	return m_transducerOffsetXVariable.get();
}

VariableTransducerOffsetY * GroupPlatform::getTransducerOffsetYVariable() const {
	return m_transducerOffsetYVariable.get();
}

VariableTransducerOffsetZ * GroupPlatform::getTransducerOffsetZVariable() const {
	return m_transducerOffsetZVariable.get();
}

VariableTransducerRotationX * GroupPlatform::getTransducerRotationXVariable() const {
	return m_transducerRotationXVariable.get();
}

VariableTransducerRotationY * GroupPlatform::getTransducerRotationYVariable() const {
	return m_transducerRotationYVariable.get();
}

VariableTransducerRotationZ * GroupPlatform::getTransducerRotationZVariable() const {
	return m_transducerRotationZVariable.get();
}

VariableWaterLevel * GroupPlatform::getWaterLevelVariable() const {
	return m_waterLevelVariable.get();
}

ncGroup * const GroupPlatform::getAttitudeGroup() {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (!m_attitudeGroup) {
		m_attitudeGroup = std::make_unique<ncGroup>(this, GroupAttitude::NC_GROUP_NAME);
	}

	return m_attitudeGroup.get();
}

GroupAttitude * const GroupPlatform::getAttitudeSubGroup(const size_t& mruIndex) {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (m_attitudeSubGroups.size() <= mruIndex) {
		initAttitudeSubGroup(mruIndex + 1);
	}

	return m_attitudeSubGroups.at(mruIndex).get();
}

ncGroup * const GroupPlatform::getPositionGroup() {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (!m_positionGroup) {
		m_positionGroup = std::make_unique<ncGroup>(this, GroupPosition::NC_GROUP_NAME);
	}

	return m_positionGroup.get();
}

GroupPosition * const GroupPlatform::getPositionSubGroup(const size_t& positionIndex) {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (m_positionSubGroups.size() <= positionIndex) {
		initPositionSubGroup(positionIndex + 1);
	}

	return m_positionSubGroups.at(positionIndex).get();
}

void GroupPlatform::forEachAttitudeGroup(const std::function<void(GroupAttitude*const)>& doToAttitudeGroup) {
	for (const auto& subAttitudeGroup : m_attitudeSubGroups) {
		doToAttitudeGroup(subAttitudeGroup.get());
	}
}

void GroupPlatform::forEachPositionGroup(const std::function<void(GroupPosition*const)>& doToPositionGroup) {
	for (const auto& subPositionGroup : m_positionSubGroups) {
		doToPositionGroup(subPositionGroup.get());
	}
}

void GroupPlatform::initAttitudeSubGroup(const size_t & mruCount) {
	while (m_attitudeSubGroups.size() <= mruCount) {
		const auto newGroupIndex = std::to_string(m_attitudeSubGroups.size());
		m_attitudeSubGroups.push_back(std::make_unique<GroupAttitude>(getAttitudeGroup(), newGroupIndex));
	}
}

void GroupPlatform::initPositionSubGroup(const size_t & positionCount) {
	while (m_positionSubGroups.size() < positionCount) {
		const auto newGroupIndex = std::to_string(m_positionSubGroups.size());
		m_positionSubGroups.push_back(std::make_unique<GroupPosition>(getPositionGroup(), newGroupIndex));
	}
}

void GroupPlatform::initializeAttributes() {
	// attributs � remplir
	createEmptyAttribute(PLATFORM_CODE_ICES_ATTRIBUTE_NAME);
	createEmptyAttribute(PLATFORM_NAME_ATTRIBUTE_NAME);
	createEmptyAttribute(PLATFORM_TYPE_ATTRIBUTE_NAME);
}

void GroupPlatform::initializeCustomType() {
	// Transducer function
	const nc_type transducerFunctionType = createEnumType<uint8_t>(TransducerFunctionType::getTypeName());
	for (TransducerFunctionType::Type type : TransducerFunctionType::all()) {
		addEnum(transducerFunctionType, TransducerFunctionType::getName(type), TransducerFunctionType::getValue(type));
	}
}
