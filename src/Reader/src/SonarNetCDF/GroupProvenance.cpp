#include "Reader/SonarNetCDF/GroupProvenance.h"

#include "Reader/SonarNetCDF/Provenance/VariableFileNames.h"

using namespace sonarNetCDF;

// Group name
const std::string GroupProvenance::NC_GROUP_NAME = "Provenance";

namespace {
	// Attribute names
	constexpr const char* CONVERSION_SOFTWARE_NAME_ATTRIBUTE_NAME = "conversion_software_name";
	constexpr const char* CONVERSION_SOFTWARE_VERSION_ATTRIBUTE_NAME = "conversion_software_version";
	constexpr const char* CONVERSION_TIME_ATTRIBUTE_NAME = "conversion_time";

	// Attributes values
	constexpr const char* APP_NAME = "MOVIES3D";
}

GroupProvenance::GroupProvenance(ncGroup* const parent) : UnderTopLevelGroup(parent, NC_GROUP_NAME)
{
	if (isEditable()) {
		// Cr�ation des attributs
		initializeAttributes();
	}

	// Cr�ation des dimensions
	m_fileNamesDimension = std::make_unique<ncDimension>(this, VariableFileNames::VARIABLE_NAME, NC_UNLIMITED);

	// Cr�ation des variables
	m_fileNamesVariable = std::make_unique<VariableFileNames>(this, m_fileNamesDimension.get());
}

GroupProvenance::~GroupProvenance() {
}


void GroupProvenance::setConversionSoftwareName(const std::string & conversionSoftwareName) {
	setAttributeValue(CONVERSION_SOFTWARE_NAME_ATTRIBUTE_NAME, conversionSoftwareName);
}

void GroupProvenance::setConversionSoftwareVersion(const std::string & conversionSoftwareVersion) {
	setAttributeValue(CONVERSION_SOFTWARE_VERSION_ATTRIBUTE_NAME, conversionSoftwareVersion);
}

void GroupProvenance::setConversionTime(const std::string & conversionTime) {
	setAttributeValue(CONVERSION_TIME_ATTRIBUTE_NAME, conversionTime);
}

VariableFileNames * GroupProvenance::getFileNamesVariable() const {
	return m_fileNamesVariable.get();
}

void GroupProvenance::initializeAttributes() {
	createAttribute(CONVERSION_SOFTWARE_NAME_ATTRIBUTE_NAME, APP_NAME);

	// attributs � remplir
	createEmptyAttribute(CONVERSION_SOFTWARE_VERSION_ATTRIBUTE_NAME);
	createEmptyAttribute(CONVERSION_TIME_ATTRIBUTE_NAME);
}
