#include "Reader/SonarNetCDF/GroupSonar.h"

#include "Reader/SonarNetCDF/Sonar/GroupBeam.h"
#include "Reader/SonarNetCDF/Sonar/GroupGrid.h"

using namespace sonarNetCDF;
using namespace sonar;

// Group name
const std::string GroupSonar::NC_GROUP_NAME = "Sonar";

namespace {
	// Attribute names
	constexpr const char* SONAR_MANUFACTURER_ATTRIBUTE_NAME = "sonar_manufacturer";
	constexpr const char* SONAR_MODEL_ATTRIBUTE_NAME = "sonar_model";
	constexpr const char* SONAR_SERIAL_NUMBER_ATTRIBUTE_NAME = "sonar_serial_number";
	constexpr const char* SONAR_SOFTWARE_NAME_ATTRIBUTE_NAME = "sonar_software_name";
	constexpr const char* SONAR_SOFTWARE_VERSION_ATTRIBUTE_NAME = "sonar_software_version";
	constexpr const char* SONAR_TYPE_ATTRIBUTE_NAME = "sonar_type";
}

GroupSonar::GroupSonar(sonarNetCDF::ncGroup* const parent) : UnderTopLevelGroup(parent, NC_GROUP_NAME)
{
	if (isEditable()) {
		// Cr�ation des attributs
		initializeAttributes();

		// Cr�ation des types personnalis�s
		initializeCustomType();
	}

	// Recherche des beam groupes
	const auto subGroupIds = searchAllSubgroupsIds();
	for (const auto& subGroupId : subGroupIds) {
		const auto groupName = ncGroup::getGroupName(subGroupId);
		if (groupName.rfind(GroupBeam::NC_GROUP_NAME, 0) == 0) {
			const auto lastIndex = groupName.find_last_not_of("0123456789");
			const auto beamGroupNumber = std::stoi(groupName.substr(lastIndex + 1)) - 1;
			m_beamGroups[beamGroupNumber] = std::make_unique<GroupBeam>(this, beamGroupNumber);
		}
		else if (groupName.rfind(GroupGrid::NC_GROUP_NAME, 0) == 0) {
			const auto lastIndex = groupName.find_last_not_of("0123456789");
			const auto gridGroupNumber = std::stoi(groupName.substr(lastIndex + 1)) - 1;
			m_gridGroups[gridGroupNumber] = std::make_unique<GroupGrid>(this, gridGroupNumber);
		}
	}
}

GroupSonar::~GroupSonar() {
}

std::string GroupSonar::getManufacturer() const {
	return getAttributeValue<std::string>(SONAR_MANUFACTURER_ATTRIBUTE_NAME);
}

std::string GroupSonar::getModel() const {
	return getAttributeValue<std::string>(SONAR_MODEL_ATTRIBUTE_NAME);
}

std::string GroupSonar::getSerialNumber() const {
	return getAttributeValue<std::string>(SONAR_SERIAL_NUMBER_ATTRIBUTE_NAME);
}

std::string GroupSonar::getSoftwareName() const {
	return getAttributeValue<std::string>(SONAR_SOFTWARE_NAME_ATTRIBUTE_NAME);
}

std::string GroupSonar::getSoftwareVersion() const {
	return getAttributeValue<std::string>(SONAR_SOFTWARE_VERSION_ATTRIBUTE_NAME);
}

SonarType::Type GroupSonar::getSonarType() const {
	return SonarType::getType(
		getAttributeValue<std::string>(SONAR_TYPE_ATTRIBUTE_NAME)
	);
}

void GroupSonar::setManufacturer(const std::string & manufacturer) {
	setAttributeValue(SONAR_MANUFACTURER_ATTRIBUTE_NAME, manufacturer);
}

void GroupSonar::setModel(const std::string & model) {
	setAttributeValue(SONAR_MODEL_ATTRIBUTE_NAME, model);
}

void GroupSonar::setSerialNumber(const std::string & serialNumber) {
	setAttributeValue(SONAR_SERIAL_NUMBER_ATTRIBUTE_NAME, serialNumber);
}

void GroupSonar::setSoftwareName(const std::string & softwareName) {
	setAttributeValue(SONAR_SOFTWARE_NAME_ATTRIBUTE_NAME, softwareName);
}

void GroupSonar::setSoftwareVersion(const std::string & softwareVersion) {
	setAttributeValue(SONAR_SOFTWARE_VERSION_ATTRIBUTE_NAME, softwareVersion);
}

void GroupSonar::setSonarType(const SonarType::Type & sonarType) {
	setAttributeValue(SONAR_TYPE_ATTRIBUTE_NAME, SonarType::getValue(sonarType));
}

size_t GroupSonar::beamGroupsSize() const {
	return m_beamGroups.size();
}

std::vector<GroupBeam*> GroupSonar::getAllBeamGroups() const {
	std::vector<GroupBeam*> beamGroups;

	for (auto& item : m_beamGroups) {
		beamGroups.push_back(item.second.get());
	}

	return beamGroups;
}

GroupBeam* const GroupSonar::getBeamGroup(const size_t & beamGroupIndex) {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (m_beamGroups.find(beamGroupIndex) == m_beamGroups.end()) {
		return initBeamGroupWithGroupIndex(beamGroupIndex);
	}

	return m_beamGroups[beamGroupIndex].get();
}

GroupBeam * const GroupSonar::getBeamGroupWithTransducerName(const std::string & transducerName) const {

	for (const auto& item : m_beamGroups) {
		if (item.second->getTransducerName() == transducerName) {
			return item.second.get();
		}
	}

	return nullptr;
}

std::vector<GroupGrid*> GroupSonar::getAllGridGroups() const {
	std::vector<GroupGrid*> gridGroups;

	for (auto& item : m_gridGroups) {
		gridGroups.push_back(item.second.get());
	}

	return gridGroups;
}

GroupGrid * const sonarNetCDF::GroupSonar::getGridGroup(const size_t & gridGroupIndex) {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (m_gridGroups.find(gridGroupIndex) == m_gridGroups.end()) {
		return initGridGroupWithGroupIndex(gridGroupIndex);
	}

	return m_gridGroups[gridGroupIndex].get();
}

GroupGrid * const GroupSonar::getGridGroupWithLayerTypeName(const std::string & layerTypeName) {
	for (const auto& item : m_gridGroups) {
		if (item.second->getLayerTypeName() == layerTypeName) {
			return item.second.get();
		}
	}

	return nullptr;
}

GroupBeam * const GroupSonar::initBeamGroup(
	const size_t & beamCount, 
	const size_t & subbeamCount,
	const size_t& frequencyCount,
	const size_t & txBeamCount
) {
	const auto beamGroupIndex = m_beamGroups.size();
	return initBeamGroupWithGroupIndex(beamGroupIndex, beamCount, subbeamCount, frequencyCount, txBeamCount);
}

GroupBeam * const GroupSonar::initBeamGroupWithGroupIndex(
	const size_t & beamGroupIndex, 
	const size_t & beamCount, 
	const size_t & subbeamCount,
	const size_t& frequencyCount,
	const size_t & txBeamCount
) {
	m_beamGroups[beamGroupIndex] = std::make_unique<GroupBeam>(this, beamGroupIndex, beamCount, subbeamCount, frequencyCount, txBeamCount);
	return m_beamGroups[beamGroupIndex].get();
}

GroupGrid * const GroupSonar::initGridGroup(
	const std::string& layerTypeName,
	const size_t & beamCount,
	const size_t & subbeamCount,
	const size_t & frequencyCount,
	const size_t & rangeAxisCount,
	const size_t & pingAxisCount,
	const size_t & txBeamCount
) {
	const auto gridGroupIndex = m_gridGroups.size();
	return initGridGroupWithGroupIndex(gridGroupIndex, layerTypeName, beamCount, subbeamCount, frequencyCount, rangeAxisCount, pingAxisCount, txBeamCount);
}

GroupGrid * const GroupSonar::initGridGroupWithGroupIndex(
	const size_t & gridGroupIndex,
	const std::string& layerTypeName,
	const size_t & beamCount, 
	const size_t & subbeamCount, 
	const size_t & frequencyCount,
	const size_t & rangeAxisCount,
	const size_t & pingAxisCount, 
	const size_t & txBeamCount
) {
	m_gridGroups[gridGroupIndex] = std::make_unique<GroupGrid>(this, gridGroupIndex, layerTypeName, beamCount, subbeamCount, frequencyCount, rangeAxisCount, pingAxisCount, txBeamCount);
	return m_gridGroups[gridGroupIndex].get();
}

void GroupSonar::initializeAttributes() {
	// attributs � remplir
	createEmptyAttribute(SONAR_MANUFACTURER_ATTRIBUTE_NAME);
	createEmptyAttribute(SONAR_MODEL_ATTRIBUTE_NAME);
	createEmptyAttribute(SONAR_SERIAL_NUMBER_ATTRIBUTE_NAME);
	createEmptyAttribute(SONAR_SOFTWARE_NAME_ATTRIBUTE_NAME);
	createEmptyAttribute(SONAR_SOFTWARE_VERSION_ATTRIBUTE_NAME);
	createEmptyAttribute(SONAR_TYPE_ATTRIBUTE_NAME);
}

void GroupSonar::initializeCustomType() {
	// Beam Stabilisation
	const nc_type beamStabilisationType = createEnumType<uint8_t>(BeamStabilisation::getTypeName());
	for (BeamStabilisation::Type type : BeamStabilisation::all()) {
		addEnum(beamStabilisationType, BeamStabilisation::getName(type), BeamStabilisation::getValue(type));
	}

	// Beam Type
	const nc_type beamTypeType = createEnumType<uint8_t>(BeamType::getTypeName());
	for (BeamType::Type type : BeamType::all()) {
		addEnum(beamTypeType, BeamType::getName(type), BeamType::getValue(type));
	}

	// Conversion Equation
	const nc_type conversionEquationType = createEnumType<uint8_t>(ConversionEquationType::getTypeName());
	for (ConversionEquationType::Type type : ConversionEquationType::all()) {
		addEnum(conversionEquationType, ConversionEquationType::getName(type), ConversionEquationType::getValue(type));
	}

	// Transmit type
	const nc_type transmitType = createEnumType<uint8_t>(TransmitType::getTypeName());
	for (TransmitType::Type type : TransmitType::all()) {
		addEnum(transmitType, TransmitType::getName(type), TransmitType::getValue(type));
	}
}
