#include "Reader/SonarNetCDF/GroupTopLevel.h"

#include "M3DKernel/utils/timeutils.h"

#include "Reader/SonarNetCDF/GroupEnvironment.h"
#include "Reader/SonarNetCDF/GroupAnnotation.h"
#include "Reader/SonarNetCDF/GroupPlatform.h"
#include "Reader/SonarNetCDF/GroupProvenance.h"
#include "Reader/SonarNetCDF/GroupEnvironment.h"
#include "Reader/SonarNetCDF/GroupSonar.h"

#include <iomanip>
#include <sstream>

using namespace sonarNetCDF;

namespace {
	// Attributes names
	constexpr const char* CONVENTIONS_ATTRIBUTE_NAME = "Conventions";
	constexpr const char* DATE_CREATED_ATTRIBUTE_NAME = "date_created";
	constexpr const char* KEYWORDS_ATTRIBUTE_NAME = "keywords";
	constexpr const char* LICENSE_ATTRIBUTE_NAME = "license";
	constexpr const char* RIGHTS_ATTRIBUTE_NAME = "rights";
	constexpr const char* SONAR_CONVENTION_AUTHORITY_ATTRIBUTE_NAME = "sonar_convention_authority";
	constexpr const char* SONAR_CONVENTION_NAME_ATTRIBUTE_NAME = "sonar_convention_name";
	constexpr const char* SONAR_CONVENTION_VERSION_ATTRIBUTE_NAME = "sonar_convention_version";
	constexpr const char* SUMMARY_ATTRIBUTE_NAME = "summary";
	constexpr const char* TITLE_ATTRIBUTE_NAME = "title";

	// Attributes default values
	constexpr const char* CONVENTIONS_ATTRIBUTE_DEFAULT_VALUE = "CF-1.7,SONAR-netCDF4-2.0,ACDD-1.3";
	constexpr const char* SONAR_CONVENTION_AUTHORITY_ATTRIBUTE_DEFAULT_VALUE = "ICES";
	constexpr const char* SONAR_CONVENTION_NAME_ATTRIBUTE_DEFAULT_VALUE = "SONAR-netCDF4";
	constexpr const char* SONAR_CONVENTION_VERSION_ATTRIBUTE_DEFAULT_VALUE = "2.0";
	constexpr const char* SUMMARY_ATTRIBUTE_DEFAULT_VALUE = "Created by Movies3D";
}

GroupTopLevel::GroupTopLevel(sonarNetCDF::ncFile* file)
	: ncGroup(nullptr, file->getId()) 
{
	setFile(file);

	if (isEditable()) {
		initializeAttributes();
	}
}

GroupTopLevel::~GroupTopLevel() {
}

std::string GroupTopLevel::getConventions() const {
	return getAttributeValue<std::string>(CONVENTIONS_ATTRIBUTE_NAME);
}

std::chrono::time_point<std::chrono::system_clock> GroupTopLevel::getCreatedDate() const {
	return timeutils::timePointFromStringUTC(getAttributeValue<std::string>(DATE_CREATED_ATTRIBUTE_NAME));
}

std::string GroupTopLevel::getKeywords() const {
	return getAttributeValue<std::string>(KEYWORDS_ATTRIBUTE_NAME);
}

std::string GroupTopLevel::getLicense() const {
	return getAttributeValue<std::string>(LICENSE_ATTRIBUTE_NAME);
}

std::string GroupTopLevel::getRights() const {
	return getAttributeValue<std::string>(RIGHTS_ATTRIBUTE_NAME);
}

std::string GroupTopLevel::getSonarConventionAuthority() const {
	return getAttributeValue<std::string>(SONAR_CONVENTION_AUTHORITY_ATTRIBUTE_NAME);
}

std::string GroupTopLevel::getSonarConventionName() const {
	return getAttributeValue<std::string>(SONAR_CONVENTION_NAME_ATTRIBUTE_NAME);
}

VersionSonarNetCDF GroupTopLevel::getSonarConventionVersion() const {
	return VersionSonarNetCDF(getAttributeValue<std::string>(SONAR_CONVENTION_VERSION_ATTRIBUTE_NAME));
}

std::string GroupTopLevel::getSummary() const {
	return getAttributeValue<std::string>(SUMMARY_ATTRIBUTE_NAME);
}

std::string GroupTopLevel::getTitle() const {
	return getAttributeValue<std::string>(TITLE_ATTRIBUTE_NAME);
}

void GroupTopLevel::setConventions(const std::string& conventions) {
	setAttributeValue(CONVENTIONS_ATTRIBUTE_NAME, conventions);
}

void GroupTopLevel::setKeywords(const std::string& keywords) {
	setAttributeValue(KEYWORDS_ATTRIBUTE_NAME, keywords);
}

void GroupTopLevel::setCreatedDate(const std::chrono::time_point<std::chrono::system_clock>& createdDate) {
	setAttributeValue(DATE_CREATED_ATTRIBUTE_NAME, timeutils::timePointToStringUTC(createdDate));
}

void GroupTopLevel::setLicense(const std::string& license) {
	setAttributeValue(LICENSE_ATTRIBUTE_NAME, license);
}

void GroupTopLevel::setRights(const std::string& rights) {
	setAttributeValue(RIGHTS_ATTRIBUTE_NAME, rights);
}

void GroupTopLevel::setSonarConventionAuthority(const std::string& sonarConventionAuthority) {
	setAttributeValue(SONAR_CONVENTION_AUTHORITY_ATTRIBUTE_NAME, sonarConventionAuthority);
}

void GroupTopLevel::setSonarConventionName(const std::string& sonarConventionName) {
	setAttributeValue(SONAR_CONVENTION_NAME_ATTRIBUTE_NAME, sonarConventionName);
}

void GroupTopLevel::setSonarConventionVersion(const std::string& sonarConventionVersion) {
	setAttributeValue(SONAR_CONVENTION_VERSION_ATTRIBUTE_NAME, sonarConventionVersion);
}

void GroupTopLevel::setSummary(const std::string& summary) {
	setAttributeValue(SUMMARY_ATTRIBUTE_NAME, summary);
}

void GroupTopLevel::setTitle(const std::string& title) {
	setAttributeValue(TITLE_ATTRIBUTE_NAME, title);
}

GroupEnvironment * GroupTopLevel::getEnvironmentGroup() {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (!m_environmentGroup) {
		return initEnvironmentGroup();
	}
	
	return m_environmentGroup.get();
}

GroupAnnotation * GroupTopLevel::getAnnotationGroup() {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (!m_annotationGroup) {
		return initAnnotationGroup();
	}

	return m_annotationGroup.get();
}

GroupPlatform * GroupTopLevel::getPlatformGroup() {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (!m_platformGroup) {
		return initPlatformGroup();
	}

	return m_platformGroup.get();
}

GroupProvenance * GroupTopLevel::getProvenanceGroup() {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (!m_provenanceGroup) {
		return initProvenanceGroup();
	}

	return m_provenanceGroup.get();
}

GroupSonar * GroupTopLevel::getSonarGroup() {
	// Cr�ation de l'objet groupe si il n'existe pas encore
	if (!m_sonarGroup) {
		return initSonarGroup();
	}

	return m_sonarGroup.get();
}

GroupEnvironment * sonarNetCDF::GroupTopLevel::initEnvironmentGroup(const size_t & frequencyCount) {
	m_environmentGroup = std::make_unique<GroupEnvironment>(this, frequencyCount);
	return m_environmentGroup.get();
}

GroupAnnotation * GroupTopLevel::initAnnotationGroup() {
	m_annotationGroup = std::make_unique<GroupAnnotation>(this);
	return m_annotationGroup.get();
}

GroupPlatform * GroupTopLevel::initPlatformGroup(const size_t & transducerCount, const size_t & positionCount, const size_t & mruCount) {
	m_platformGroup = std::make_unique<GroupPlatform>(this, transducerCount, positionCount, mruCount);
	return m_platformGroup.get();
}

GroupProvenance * GroupTopLevel::initProvenanceGroup() {
	m_provenanceGroup = std::make_unique<GroupProvenance>(this);
	return m_provenanceGroup.get();
}

GroupSonar * GroupTopLevel::initSonarGroup() {
	m_sonarGroup = std::make_unique<GroupSonar>(this);
	return m_sonarGroup.get();
}

void GroupTopLevel::initializeAttributes() {
	createAttribute(DATE_CREATED_ATTRIBUTE_NAME, timeutils::timePointToStringUTC(std::chrono::system_clock::now()));
	createAttribute(CONVENTIONS_ATTRIBUTE_NAME, CONVENTIONS_ATTRIBUTE_DEFAULT_VALUE);
	createAttribute(SONAR_CONVENTION_AUTHORITY_ATTRIBUTE_NAME, SONAR_CONVENTION_AUTHORITY_ATTRIBUTE_DEFAULT_VALUE);
	createAttribute(SONAR_CONVENTION_NAME_ATTRIBUTE_NAME, SONAR_CONVENTION_NAME_ATTRIBUTE_DEFAULT_VALUE);
	createAttribute(SONAR_CONVENTION_VERSION_ATTRIBUTE_NAME, SONAR_CONVENTION_VERSION_ATTRIBUTE_DEFAULT_VALUE);
	createAttribute(SUMMARY_ATTRIBUTE_NAME, SUMMARY_ATTRIBUTE_DEFAULT_VALUE);

	// attributs � remplir
	createEmptyAttribute(KEYWORDS_ATTRIBUTE_NAME);
	createEmptyAttribute(LICENSE_ATTRIBUTE_NAME);
	createEmptyAttribute(RIGHTS_ATTRIBUTE_NAME);
	createEmptyAttribute(TITLE_ATTRIBUTE_NAME);
}

UnderTopLevelGroup::UnderTopLevelGroup(ncGroup * const parent, const std::string & name, const int & id) : ncGroup(parent, name, id) {
}

UnderTopLevelGroup::UnderTopLevelGroup(ncGroup * const parent, const int & id) : ncGroup(parent, id) {
}

VersionSonarNetCDF UnderTopLevelGroup::getVersion() {
	const auto topLevelRootGroup = dynamic_cast<GroupTopLevel*>(getRootGroup());
	if (topLevelRootGroup) {
		return topLevelRootGroup->getSonarConventionVersion();
	}
	return VersionSonarNetCDF();
}
