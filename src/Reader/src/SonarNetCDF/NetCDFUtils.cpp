#include "Reader/SonarNetCDF/NetCDFUtils.h"

#include <algorithm>
#include <numeric>
#include <set>
#include <list>

#include <string.h>
#include <stdarg.h>
#include <assert.h>

namespace {
	const constexpr unsigned short MAX_CUSTOM_TYPES = 100;
	const constexpr unsigned short CUSTOM_TYPE_NAME_MAX_SIZE = 100;
}

using namespace sonarNetCDF;

// ------------ NcUtils
const int ncUtils::INVALID_ID = -1;
const int ncUtils::INVALID_SIZE = -1;
const int ncUtils::INVALID_INDEX = -1;

std::function<void(const char * message, va_list args)> ncUtils::logInfoFunction = std::function<void(const char * message, va_list args)>();
std::function<void(const char * message, va_list args)> ncUtils::logErrorFunction = std::function<void(const char * message, va_list args)>();

void ncUtils::logInfo(const char * format, ...) {
	if (logInfoFunction) {
		va_list arg_ptr;
		va_start(arg_ptr, format);
		logInfoFunction(format, arg_ptr);
		va_end(arg_ptr);
	}
}

void ncUtils::logError(const char * format, ...) {
	if (logErrorFunction) {
		va_list arg_ptr;
		va_start(arg_ptr, format);
		logErrorFunction(format, arg_ptr);
		va_end(arg_ptr);
	}
}

void ncUtils::setLogInfoFunction(std::function<void(const char*message, va_list args)> logInfoFunction) {
	ncUtils::logInfoFunction = logInfoFunction;
}

void ncUtils::setLogErrorFunction(std::function<void(const char*message, va_list args)> logErrorFunction) {
	ncUtils::logErrorFunction = logErrorFunction;
}

nc_type ncUtils::getTypeIdByName(const int & groupId, const std::string & typeName) {

	// -----------------
	// NC_INQ_TYPEID est buggué et ne retourne pas toujours le type personnalisé du groupe demandé
	// Cas problèmatique :
	// 2 Groupes voisins avec des types perso différents mais portant le même nom
	// Lors d'une recherche via son nom du type du groupe 2, cette méthode renvoyait le type du groupe 1
	// Pas de problème avec le type du groupe 1.
	//
	// Solution retenue :
	// On recherche alors tous les types du groupe et on filtre par nom
	// On poursuit la recherche sur le groupe parent si la recherche est infructueuse
	// -------------------

	// Recherche de tous les types personnalisés du groupe
	int nbCustomTypes;
	int typeIdList[MAX_CUSTOM_TYPES];
	int ret = nc_inq_typeids(groupId, &nbCustomTypes, typeIdList);
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_inq_typeids failed : %s (%d)", __func__, nc_strerror(ret), ret);
		return NC_NAT;
	}

	// Recherche du type personnalisé ayant le nom donné en entrée
	for (int i = 0; i < nbCustomTypes; ++i) {
		int typeId = typeIdList[i];
		char name[CUSTOM_TYPE_NAME_MAX_SIZE];
		ret = nc_inq_user_type(groupId, typeId, name, nullptr, nullptr, nullptr, nullptr);
		if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_inq_user_type failed : %s (%d)", __func__, nc_strerror(ret), ret);
		}

		if (strcmp(name, typeName.c_str()) == 0) {
			return typeId;
		}
	}

	// Si le type personnalisé n'a pas été trouvé, on interroge les parents
	int parentGroupId;
	ret = nc_inq_grp_parent(groupId, &parentGroupId);
	if (ret == NC_ENOGRP) {
		return NC_NAT; // On a atteint le groupe racine
	}
	else if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_inq_grp_parent failed : %s (%d)", __func__, nc_strerror(ret), ret);
	}

	return getTypeIdByName(parentGroupId, typeName);
}

template<typename T>
nc_type ncUtils::ncType<T>::getType() {
	return NC_NAT;
}

template<> // Spécification BYTE
nc_type ncUtils::ncType<uint8_t>::getType() {
	return NC_BYTE;
}

template<> // Spécification CHAR
nc_type ncUtils::ncType<char>::getType() {
	return NC_CHAR;
}

template<> // Spécification SHORT
nc_type ncUtils::ncType<short>::getType() {
	return NC_SHORT;
}

template<> // Spécification INT
nc_type ncUtils::ncType<int>::getType() {
	return NC_INT;
}

template<> // Spécification DOUBLE
nc_type ncUtils::ncType<double>::getType() {
	return NC_DOUBLE;
}

template<> // Spécification FLOAT
nc_type ncUtils::ncType<float>::getType() {
	return NC_FLOAT;
}

template<> // Spécification STRING
nc_type ncUtils::ncType<std::string>::getType() {
	return NC_STRING;
}

template<> // Spécification CHAR*
nc_type ncUtils::ncType<const char*>::getType() {
	return NC_STRING;
}

template<> // Spécification UINT64_T
nc_type ncUtils::ncType<uint64_t>::getType() {
	return NC_UINT64;
}

// ------------ NcFile

ncFile::ncFile() : ncFile(std::string())
{
}

ncFile::ncFile(const std::string & fileName) : m_fileName(fileName), m_id(ncUtils::INVALID_ID), m_editable(false) {
}

ncFile::~ncFile() {
	close();
}

bool ncFile::isOpen() const {
	return getId() != ncUtils::INVALID_ID;
}

const int& ncFile::getId() const{
	return m_id;
}

const bool& ncFile::isEditable() const {
	return m_editable;
}

void ncFile::setFileName(const std::string & fileName) {
	m_fileName = fileName;
}

const std::string & ncFile::getFileName() {
	return m_fileName;
}

bool ncFile::open(const int & mode) {
	int status = nc_open(m_fileName.c_str(), mode, &m_id);
	if (status != NC_NOERR) {
		ncUtils::logError("%s: nc_open failed for path %s, error : %s (%d)", __func__, m_fileName.c_str(), nc_strerror(status), status);
		return false;
	}
	ncUtils::logInfo("NetCDF file opened : %s", m_fileName.c_str());
	return true;
}

void ncFile::close() {
	if (!isOpen()) return; // Already closed

	int status = nc_close(m_id);
	if (status != NC_NOERR) {
		ncUtils::logError("%s: nc_close failed for file %s (id %s), error : %s (%d)", __func__, m_fileName.c_str(), m_id, nc_strerror(status), status);
	}
	else {
		ncUtils::logInfo("NetCDF file closed : %s", m_fileName.c_str());
		m_id = ncUtils::INVALID_ID;
	}
}

void ncFile::sync() const {
	if (!isOpen()) return; // File closed

	int status = nc_sync(m_id);
	if (status != NC_NOERR) {
		ncUtils::logError("%s: nc_sync failed for file %s (id %s), error : %s (%d)", __func__, m_fileName.c_str(), m_id, nc_strerror(status), status);
	}
}

std::unique_ptr<ncFile> ncFile::createFile(const std::string& path, const int& mode) {
	return createFile(path.c_str(), mode);
}

std::unique_ptr<ncFile> ncFile::createFile(const char * path, const int& mode) {
	ncFile* ncFile = new sonarNetCDF::ncFile();

	// utilisation d'un cache netcdf à 1Mo pour que le fichier se synchronise régulièrement sur disque 
	// et ce afin de pouvoir découper celui-ci en fonction de sa taille
	// rmq : nc_set_chunk_cache est gloabl à la lib netcdf, aussi bien en écriture qu'en lecture, a voir si cela impacte négativement les perfs de lecture...
	const size_t expected_cache_size = 1 * 1000 * 1000;
	size_t cache_size, cache_nelems;
	float cache_preemption;
	nc_get_chunk_cache(&cache_size, &cache_nelems, &cache_preemption);
	if (expected_cache_size != cache_size)
	{
		nc_set_chunk_cache(expected_cache_size, cache_nelems, cache_preemption);
	}

	int status = nc_create(path, mode | NC_NETCDF4, &ncFile->m_id);
	if (status != NC_NOERR) {
		ncUtils::logError("%s: nc_create failed for path %s, error : %s (%d)", __func__, path, nc_strerror(status), status);

		delete ncFile;
		return nullptr;
	}

	ncUtils::logInfo("NetCDF file created : %s", path);
	ncFile->m_fileName = path;
	ncFile->m_editable = true;
	return std::unique_ptr<sonarNetCDF::ncFile>(ncFile);
}

std::unique_ptr<ncFile> ncFile::openFile(const std::string & path, const int & mode) {
	return openFile(path.c_str(), mode);
}

std::unique_ptr<ncFile> ncFile::openFile(const char * path, const int & mode) {
	ncFile* ncFile = new sonarNetCDF::ncFile(std::string(path));

	// Tentative d'ouverture
	if (!ncFile->open(mode)) {
		delete ncFile;
		return nullptr;
	}

	return std::unique_ptr<sonarNetCDF::ncFile>(ncFile);
}

// ------------ NcObject
ncObject::ncObject(ncObject* parent, const std::string& name, const int& id) :
	m_file(parent == nullptr ? nullptr : parent->m_file),
	m_parent(parent),
	m_id(id),
	m_name(name)
{
}

ncObject::~ncObject() {
}

ncObject & ncObject::operator=(const ncObject & other) {
	this->m_file = other.m_file;
	this->m_parent = other.m_parent;
	this->m_id = other.m_id;
	this->m_name = other.m_name;
	return *this;
}

const int& ncObject::getId() const {
	return m_id;
}

int ncObject::getParentId() const {
	return m_parent == nullptr ? ncUtils::INVALID_ID : m_parent->getId();
}

const std::string& ncObject::getName() const {
	return m_name;
}

int ncObject::getFileId() const {
	return m_file == nullptr ? ncUtils::INVALID_ID : m_file->getId();
}

bool ncObject::isValid() const {
	return m_id != ncUtils::INVALID_ID;
}

void ncObject::setFile(ncFile * file) {
	m_file = file;
}

bool ncObject::isEditable() const {
	return m_file != nullptr && m_file->isEditable();
}

ncObject * ncObject::getParent() const {
	return m_parent;
}

// ------------ ncAttributeOwner

void ncAttributeOwner::createEmptyAttribute(const std::string& attributeName) {
	createAttribute(attributeName, "null");
}

// ------------ NcDimension

ncDimension::ncDimension(ncObject* parent, const std::string& name, const size_t& size) : ncObject(parent, name), m_size(size) {
	if (size != ncUtils::INVALID_SIZE) {
		setSize(size);
	}

	initialize();
}

ncDimension::~ncDimension() {
}

ncDimension & ncDimension::operator=(const ncDimension & other) {
	ncObject::operator=(other);
	this->m_size = other.m_size;
	return *this;
}

void ncDimension::setUnlimited() {
	setSize(NC_UNLIMITED);
}

bool ncDimension::isUnlimited() const {
	return m_isUnlimited;
}

size_t ncDimension::incrementDimensionSize(const int) {
	return ++m_size;
}

size_t ncDimension::getSize() const {
	return m_size;
}

void ncDimension::setSize(size_t size) {
	assert(size >= 0 || size == NC_UNLIMITED);
	m_size = size;
	m_isUnlimited = size == NC_UNLIMITED;

	// Si l'identifiant n'est pas conforme, la dimension n'existe pas, on tente alors de la créer
	if (getId() == ncUtils::INVALID_ID) {
		createDimension();
	}
}

void ncDimension::createDimension() {
	// Pas de création en cas
	if (!isEditable() // La création n'est pas permise
		|| getId() != ncUtils::INVALID_ID // d'identifiant existant (dimension déjà créée)
		|| getParentId() == ncUtils::INVALID_ID // de parent absent (pas de destination)
		|| getName().empty() // de nom vide
		|| (m_size <= 0 && !m_isUnlimited) // Taille non définie
		) return;

	// Création de dimension
	int status = nc_def_dim(getParentId(), getName().c_str(), m_size, &m_id);
	if (status != NC_NOERR) {
		ncUtils::logError("%s: nc_def_dim failed parent %s and name %s, error : %s (%d)", __func__, getParent()->getName().c_str(), getName().c_str(), nc_strerror(status), status);
	}
}

bool ncDimension::isValid() const {
	return getId() != ncUtils::INVALID_ID && getParentId() != ncUtils::INVALID_ID && !getName().empty();
}

void ncDimension::initialize() {
	if (getParentId() == ncUtils::INVALID_ID) return;

	// Identifiant
	if (!getName().empty() && getId() == ncUtils::INVALID_ID) {
		int ret = nc_inq_dimid(getParentId(), getName().c_str(), &m_id);
		if (ret == NC_EBADDIM) {
			if (!isEditable()) { // Message ignoré en mode création
				ncUtils::logInfo("Dimension %s doesn't exist.", getName().c_str());
			}
		}
		else if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_inq_dimid failed for dimension %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
		}
	}

	// Taille
	if (getId() != ncUtils::INVALID_ID && (getSize() == ncUtils::INVALID_SIZE || m_isUnlimited)) {
		int ret = nc_inq_dimlen(getParentId(), getId(), &m_size);
		if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_inq_dimlen failed for dimension %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
		}
	}
}

// ------------ NcVariable

template<typename T>
ncVariable<T>::ncVariable(ncObject* parent, const std::string & name, const std::vector<ncDimension*>& dimensions)
	: ncObject(parent, name), 
	m_dimensions(dimensions),
	m_type(NC_NAT)
{
	initialize();
}

template<typename T>
ncVariable<T>::ncVariable(ncObject* parent, const std::string & name, ncDimension * const dimension)
	: ncVariable(parent, 
		name, 
		dimension == nullptr ? std::vector<ncDimension*>() : std::vector<ncDimension*>(1, dimension)
	) 
{
}

template<typename T>
ncVariable<T>::~ncVariable() {
}

template<typename T>
const std::vector<ncDimension*>& ncVariable<T>::getDimensions() const {
	return m_dimensions;
}

template<typename T>
size_t ncVariable<T>::getNbDimensions() const {
	return m_dimensions.size();
}

template<typename T>
void ncVariable<T>::setTypeName(const std::string & typeName) {
	if (typeName.empty()) {
		m_type = ncUtils::ncType<T>::getType();
	}
	else {
		m_type = ncUtils::getTypeIdByName(getParentId(), typeName);
	}
}

template<typename T>
void ncVariable<T>::setFillValue(const T & fillValue) {
	int ret = nc_def_var_fill(getParentId(), getId(), NC_FILL, &fillValue);
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_def_var_fill failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}
}

template<typename T>
T ncVariable<T>::getFillValue() const {
	int noFillMode;
	T fillValue;

	int ret = nc_inq_var_fill(getParentId(), getId(), &noFillMode, &fillValue);
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_inq_var_fill failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}

	return fillValue;
}

template<typename T>
T ncVariable<T>::getScalarValue() const {
	assert(m_dimensions.empty());

	T value = T();

	// Test validité
	if (!isValid()) {
		return value;
	}

	// Lecture de la donnée
	int ret = nc_get_var1(getParentId(), getId(), {}, &value);
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_put_var1 failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}

	return value;
}

template<> // Spécification STRING
std::string ncVariable<std::string>::getScalarValue() const {
	assert(m_dimensions.empty());

	std::string value;

	// Test validité
	if (!isValid()) {
		return value;
	}

	// Lecture de la donnée (char *)
	int ret = nc_get_var1(getParentId(), getId(), {}, (char *)value.data());
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_put_var1 failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}

	return value;
}

template<typename T>
T ncVariable<T>::getValue(const size_t& index) const {
	const std::vector<size_t> indexes = { index };
	return getIndexedValue(indexes);
}

template<typename T>
std::vector<T> ncVariable<T>::getAll1DValue() const {
	assert(getNbDimensions() == 1);

	// Taille de la dimension
	ncDimension* const dimension = *getDimensions().begin();
	const size_t size = dimension->getSize();
	if (size == ncUtils::INVALID_SIZE || size == 0) {
		return std::vector<T>();
	}

	// Récupération des valeurs
	std::vector<T> values;
	read1DData(values, size);
	return values;
}

template<typename T>
size_t ncVariable<T>::getIndex(const T & value) const {
	assert(getNbDimensions() == 1);
	
	// Récupération des données de la variable
	std::vector<T> values = getAll1DValue();

	// Recherche de l'index
	auto found = std::find(values.begin(), values.end(), value);
	if (found != values.end()) {
		return std::distance(values.begin(), found);
	}

	// Retour ncUtils::INVALID_INDEX en cas d'absence
	return ncUtils::INVALID_INDEX;
}

template<typename T>
T ncVariable<T>::getIndexedValue(const std::vector<size_t>& indexes) const {
	if (!isValid()) return T();

	// Construction des tableaux d'index	
	std::vector<size_t> startInDimensions(indexes);
	startInDimensions.resize(getNbDimensions());
	std::vector<size_t> countInDimensions(getNbDimensions(), 1);

	// Lecture des donnés
	std::vector<T> values;
	readNDData(startInDimensions, countInDimensions, values);

	if (values.empty()) {
		return T();
	}

	return values.front();
}

template<typename T>
void ncVariable<T>::setScalarValue(const T & value) {
	assert(m_dimensions.empty());

	// Créer la variable si elle n'existe pas dans l'environnement NetCDF
	if (getId() == ncUtils::INVALID_ID) {
		createVariable();
	}

	// Test validité
	if (!isValid()) return;
	
	// Ecriture de la donnée
	int ret = nc_put_var1(getParentId(), getId(), {}, &value);
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_put_var1 failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}
}

template<typename T>
void ncVariable<T>::set1DValues(const std::vector<T>& values) {
	assert(getNbDimensions() == 1);

	// Pré écriture script
	preWrite1DData(values.size());

	// Test validité
	if (!isValid()) return;
	
	// Ecriture
	write1DData(values);
}

template<typename T>
void ncVariable<T>::set1DValue(const size_t & index, const T & value) {
	assert(getNbDimensions() == 1);

	// Pré écriture script
	preWrite1DData(index + 1);

	// Ecriture
	write1DData({ value }, index);
}

template<typename T>
void ncVariable<T>::fill1DValues(const T value) {
	// Taille de la dimension
	ncDimension* const dimension = *getDimensions().begin();
	size_t size = dimension->getSize();
	size = size == ncUtils::INVALID_SIZE ? 0 : size;

	// Construction des valeurs
	const std::vector<T> filledValues(size, value);
	set1DValues(filledValues);
}

template<typename T>
size_t ncVariable<T>::append1DValue(const T & value) {
	assert(getNbDimensions() == 1);

	// Taille de la dimension
	ncDimension* const dimension = *getDimensions().begin();
	size_t size = dimension->getSize();
	size = size == ncUtils::INVALID_SIZE ? 0 : size;

	// Ecriture
	set1DValue(size, value);

	// Incrémentation de la taille de la dimension illimité
	if (dimension->isUnlimited()) {
		dimension->incrementDimensionSize();
	}

	return size;
}

template<typename T>
size_t ncVariable<T>::findOrAppend1DValue(const T & value) {
	size_t index = getIndex(value);
	if (index == ncUtils::INVALID_INDEX) {
		return append1DValue(value);
	}
	else {
		return index;
	}
}

template<typename T>
void ncVariable<T>::setIndexedValues(const std::vector<size_t>& indexes, const std::vector<size_t>& counts, const std::vector<T> & values) {
	// Arrêter la fonction si les valeurs sont vides
	if (values.empty()) {
		return;
	}

	// Créer la variable si elle n'existe pas dans l'environnement NetCDF
	if (getId() == ncUtils::INVALID_ID) {
		createVariable();
	}

	// Test de validité
	if (!isValid()) return;

	// Ecriture des données
	writeNDData(indexes, counts, values);
}

template<typename T>
void ncVariable<T>::setIndexedValues(const std::map<std::vector<size_t>, T>& valuesByIndexes) {
	// Arrêter la fonction si les valeurs sont vides
	if (valuesByIndexes.empty()) {
		return;
	}

	// Les valeurs d'index doivent se suivre !!!!
	// On récupére la liste des index pour chaque dimension et on vérifie qu'ils sont adjacents
	std::vector<std::set<size_t>> indexSeries(getNbDimensions());
	for (std::pair<std::vector<size_t>, T> item : valuesByIndexes) {
		for (auto i = 0; i < getNbDimensions(); ++i) {
			indexSeries[i].insert(item.first[i]);
		}
	}

	for (auto i = 0; i < getNbDimensions(); ++i) {
		const auto& indexSerie = indexSeries[i];
		for (auto j = 1; j < indexSerie.size(); ++j) {
			const auto& previous = *std::next(indexSerie.cbegin(), j - 1);
			const auto& nextIndex = *std::next(indexSerie.cbegin(), j);
			if (nextIndex - previous != 1) {
				// On est dans le cas d'une série d'index non adjacents -- on annule l'écriture
				ncUtils::logError("%s: non consecutives indexes to set values for variable %s", __func__, getName().c_str());
				return;
			}
		}
	}

	// Construction des tableaux d'index	
	std::vector<size_t> startInDimensions(getNbDimensions(), 0);
	std::vector<size_t> countInDimensions(getNbDimensions(), 0);	

	for (int i = 0; i < getNbDimensions(); ++i) {
		if (indexSeries[i].empty()) {
			continue;
		}

		// Récupération des plus petits index pour le point de départ
		startInDimensions[i] = *indexSeries[i].begin();

		// Récupération de la différence entre le plus grand index et le plus petit pour connaitre la taille d'écriture dans la dimension
		countInDimensions[i] = (*std::prev(indexSeries[i].end()) - *indexSeries[i].begin()) + 1;
	}

	// Définir les dimensions si elles n'ont pas encore été définies
	for (auto i = 0; i < getNbDimensions(); ++i) {
		if (getDimensions()[i]->getSize() == ncUtils::INVALID_SIZE && !indexSeries[i].empty()) {
			const auto& lastIndex = *(std::prev(indexSeries[i].cend())) + 1;
			getDimensions()[i]->setSize(std::max(1, static_cast<int>(lastIndex))); // Taille minimum de la dimension est 1 car sinon elle est considérée comme UNLIMITED
		}
	}

	// Créer la variable si elle n'existe pas dans l'environnement NetCDF
	if (getId() == ncUtils::INVALID_ID) {
		createVariable();
	}

	// Test de validité
	if (!isValid()) return;
	
	// Construction des données
	size_t dataSize = 0; // Taille des données
	for (size_t size : countInDimensions) {
		if (size > 0) {
			dataSize = dataSize == 0 ? size : dataSize * size;
		}
	}

	std::vector<T> values(dataSize, getFillValue()); // Constructions des données avec la valeur par défaut

	// Pour chaque valeur
	for (int i = 0; i < values.size(); ++i) {

		// Construire le jeu d'index des dimensions
		std::vector<size_t> indexes(getNbDimensions(), 0);
		size_t divider = 1;
		for (size_t j = getNbDimensions(); j > 0; --j) {
			indexes[j - 1] = startInDimensions[j - 1] + ((i / divider) % countInDimensions[j - 1]);
			divider *= countInDimensions[j - 1];
		}

		// Récupérer la valeur associée à ce jeu d'index
		for (auto item : valuesByIndexes) {
			if (item.first == indexes) {
				values[i] = item.second;
				break;
			}
		}
	}

	// Ecriture des données
	writeNDData(startInDimensions, countInDimensions, values);
}

template<typename T>
void ncVariable<T>::setIndexedValue(const std::vector<size_t>& indexes, const T & value) {
	std::map<std::vector<size_t>, T> indexedMap;
	indexedMap[indexes] = value;
	setIndexedValues(indexedMap);
}

template<typename T>
void ncVariable<T>::forEachDataFromDimension(const std::string & dimensionName, std::function<void(size_t dimensionIndex, T value)> doForEach) {
	std::vector<ncDimension*>::iterator it = std::find_if(
		m_dimensions.begin(), 
		m_dimensions.end(), 
		[&](ncDimension* const dimension) { return dimension->getName() == dimensionName; }
	);

	forEachDataFromDimension(std::distance(m_dimensions.begin(), it), doForEach);
}

template<typename T>
void ncVariable<T>::forEachDataFromDimension(ncDimension * const dimension, std::function<void(size_t dimensionIndex, T value)> doForEach) {
	forEachDataFromDimension(getDimensionIndex(dimension), doForEach);
}

template<typename T>
void ncVariable<T>::forEachDataFromDimension(size_t dimensionIndex, std::function<void(size_t dimensionIndex, T value)> doForEach) {
	if (dimensionIndex == ncUtils::INVALID_INDEX) return;

	for (size_t i = 0; i < m_dimensions[dimensionIndex]->getSize(); ++i) {
		std::vector<size_t> indexes(m_dimensions.size());
		indexes.at(dimensionIndex) = i;

		doForEach(i, getIndexedValue(indexes));
	}
}

template<typename T>
int ncVariable<T>::getAttributesGroupId() const {
	return getParentId();
}

template<typename T>
int ncVariable<T>::getAttributesVarId() const {
	return getId();
}

template<typename T>
bool ncVariable<T>::isValid() const {
	return ncObject::isValid()
		&& getParentId() != ncUtils::INVALID_ID
		&& getDimensionsId().size() == getNbDimensions();
}

template<typename T>
void ncVariable<T>::initialize() {
	if (getParentId() == ncUtils::INVALID_ID) return;
	
	// Identifiant
	if (!getName().empty() && getId() == ncUtils::INVALID_ID) {
		int ret = nc_inq_varid(getParentId(), getName().c_str(), &m_id);
		if (ret == NC_ENOTVAR) {
			if (!isEditable()) { // Message ignoré en mode création
				ncUtils::logInfo("Variable %s doesn't exist.", getName().c_str());
			}
		}
		else if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_inq_varid failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
		}
	}

	// Nom
	if (getId() != 1 && getName().empty()) {
		char name[NC_MAX_NAME + 1];
		int ret = nc_inq_varname(getParentId(), getId(), name);
		if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_inq_varname failed for variable with id=%s, error : %s (%d)", __func__, getId(), nc_strerror(ret), ret);
		}
		else {
			m_name = std::string(name);
		}
	}
}

template<typename T>
void ncVariable<T>::createVariable() {
	// Pas de création en cas
	if (!isEditable() // La création n'est pas permise
		|| getId() != ncUtils::INVALID_ID // d'identifiant existant (variable déjà créée)
		|| getParentId() == ncUtils::INVALID_ID // de parent absent (pas de destination)
		|| getName().empty() // de nom vide
		|| getDimensionsId().size() != getNbDimensions() // de dimension non crée (identifiant à ncUtils::INVALID_ID)
		) return;

	if (m_type == NC_NAT) { // Recherche du type par défaut si non défini
		setTypeName();
	}

	// Création variable
	int status = nc_def_var(getParentId(), getName().c_str(), m_type, (int)m_dimensions.size(), getDimensionsId().data(), &m_id);
	if (status != NC_NOERR) {
		ncUtils::logError("%s: nc_def_grp failed parent %d and name %s, error : %s (%d)", __func__, getParentId(), getName().c_str(), nc_strerror(status), status);
	}
	
	// Reste de l'initialisation en cas de création de variable réussie
	if (getId() != ncUtils::INVALID_ID) {
		// Initialisation des attributs de la variable
		initializeAttributes();

		// Post initialisation
		for (auto postInit : m_waitingInitialisations) {
			postInit();
		}

		m_waitingInitialisations.clear();

		// Initialisation de la valeur de remplissage
		initializeFillValue();
	}
}

template<typename T>
void ncVariable<T>::preWrite1DData(const size_t & dimensionSize) {
	// Définir la dimension si elle n'a pas encore été définie
	ncDimension* const dimension = *getDimensions().begin();
	if (dimension->getSize() == ncUtils::INVALID_SIZE) {
		dimension->setSize(std::max(1, (int)dimensionSize)); // Taille minimum de la dimension est 1 car sinon elle est considérée comme UNLIMITED
	}

	// Créer la variable si elle n'existe pas dans l'environnement NetCDF
	if (getId() == ncUtils::INVALID_ID) {
		createVariable();
	}
}

template<typename T>
void ncVariable<T>::write1DData(const std::vector<T>& values, const size_t& startIndex) {
	// Construction des tableaux d'index
	std::vector<size_t> startInDimensions(1, startIndex);
	std::vector<size_t> countInDimensions(1, values.size());

	// Ecriture
	writeNDData(startInDimensions, countInDimensions, values);
}

template<typename T>
template<typename U>
void ncVariable<T>::writeNDData(const std::vector<size_t>& startInDimensions, const std::vector<size_t>& countInDimensions, const std::vector<U>& values) {
	int ret = nc_put_vara(getParentId(), getId(), startInDimensions.data(), countInDimensions.data(), values.data());
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_put_vara failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}
}

template<> // Spécification STRING
template<>
void ncVariable<std::string>::writeNDData(const std::vector<size_t>& startInDimensions, const std::vector<size_t>& countInDimensions, const std::vector<std::string>& values) {
	// Cast des strings en char*
	std::vector<const char*> charValues(values.size(), nullptr);
	std::transform(values.begin(), values.end(), charValues.begin(), [&](const std::string& str) {
		return str.c_str();
	});

	// Ecriture
	int ret = nc_put_vara(getParentId(), getId(), startInDimensions.data(), countInDimensions.data(), charValues.data());
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_put_vara failed for variable %s; error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}
}

template<typename T>
template<typename U>
void ncVariable<T>::writeNDData(const std::vector<size_t>& startInDimensions, const std::vector<size_t>& countInDimensions, const std::vector<std::vector<U>>& values) {

	std::vector<nc_vlen_t> vlenValues;

	for (const auto& value : values) {
		nc_vlen_t vlen;
		vlen.p = (U*)value.data();
		vlen.len = value.size();
		vlenValues.push_back(vlen);
	}
	
	int ret = nc_put_vara(getParentId(), getId(), startInDimensions.data(), countInDimensions.data(), vlenValues.data());
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_put_vara failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}
	
	// Ne pas faire de nc_free_vlen, les données sont gérées ailleurs
}

template<typename T>
void ncVariable<T>::read1DData(std::vector<T>& values, const size_t& size, const size_t& startIndex) const {
	if (!isValid()) {
		values.clear();
		return;
	}

	// Construction des tableaux d'index	
	std::vector<size_t> startInDimensions(1, startIndex);
	std::vector<size_t> countInDimensions(1, size != ncUtils::INVALID_SIZE ? size : values.size());

	// Récupération de toutes les valeurs de la variable
	readNDData(startInDimensions, countInDimensions, values);
}

template<typename T>
template<typename U>
void ncVariable<T>::readNDData(const std::vector<size_t>& startInDimensions, const std::vector<size_t>& countInDimensions, std::vector<U>& values) const {
	if (!isValid()) {
		values.clear();
		return;
	}

	// Nombre de résultats attendus
	const auto resultSize = std::accumulate(countInDimensions.begin(), countInDimensions.end(), 1, std::multiplies<size_t>());
	values.resize(resultSize);

	// Récupération de toutes les valeurs de la variable
	int ret = nc_get_vara(getParentId(), getId(), startInDimensions.data(), countInDimensions.data(), values.data());
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_put_vara failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}
}

template<> // Spécification STRING
template<>
void ncVariable<std::string>::readNDData(const std::vector<size_t>& startInDimensions, const std::vector<size_t>& countInDimensions, std::vector<std::string>& values) const {
	if (!isValid()) {
		values.clear();
		return;
	}

	// Nombre de résultats attendus
	const auto resultSize = std::accumulate(countInDimensions.begin(), countInDimensions.end(), 1, std::multiplies<size_t>());
	values.resize(resultSize);

	// Vector d'accueil des valeurs
	std::vector<char*> charValues(resultSize);

	// Récupération de toutes les valeurs de la variable
	int ret = nc_get_vara(getParentId(), getId(), startInDimensions.data(), countInDimensions.data(), charValues.data());
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_put_vara failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}

	// Cast des char* en string
	std::transform(charValues.begin(), charValues.end(), values.begin(), [&](const char* value) {
		return std::string(value);
	});

	// Destruction des valeurs allouées dans la librarie netcdf
	nc_free_string(resultSize, charValues.data());
}

template<typename T>
template<typename U>
void ncVariable<T>::readNDData(const std::vector<size_t>& startInDimensions, const std::vector<size_t>& countInDimensions, std::vector<std::vector<U>>& values) const {
	if (!isValid()) {
		values.clear();
		return;
	}

	// Nombre de résultats attendus
	const auto resultSize = std::accumulate(countInDimensions.begin(), countInDimensions.end(), 1, std::multiplies<size_t>());

	// Vector d'accueil des valeurs
	std::vector<nc_vlen_t> vlenValues(resultSize);

	// Récupération de toutes les valeurs de la variable
	int ret = nc_get_vara(getParentId(), getId(), startInDimensions.data(), countInDimensions.data(), vlenValues.data());
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_get_vara failed for variable %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
	}

	// Conversion des Vlen
	size_t index = 0;
	for (auto vlen : vlenValues) {
		U* vlenArray = (U*)vlen.p;
		std::vector<U> tmpValues(vlenArray, vlenArray + vlen.len);
		values.push_back(tmpValues);

		nc_free_vlen(&vlen);
	}
}


template<typename T>
size_t ncVariable<T>::getDimensionIndex(ncDimension * const dimension) const {
	auto it = std::find(
		m_dimensions.begin(),
		m_dimensions.end(),
		dimension
	);

	if (it == m_dimensions.end()) return ncUtils::INVALID_INDEX;

	return std::distance(m_dimensions.begin(), it);
}

template<typename T>
std::vector<int> ncVariable<T>::getDimensionsId() const {
	std::vector<int> ids;

	for (ncDimension* const dimension : m_dimensions) {
		if (dimension->getId() != ncUtils::INVALID_ID) {
			ids.push_back(dimension->getId());
		}
	}

	return ids;
}

// Explicit template instantiation
template class ncVariable<uint8_t>;
template class ncVariable<uint64_t>;
template class ncVariable<short>;
template class ncVariable<int>;
template class ncVariable<double>;
template class ncVariable<float>;
template class ncVariable<std::string>;
template class ncVariable<std::vector<short>>;
template class ncVariable<std::vector<float>>;

// ------------ NcGroup

ncGroup::ncGroup(ncGroup* const parent, const std::string & name, const int& id) 
	: ncObject(parent, name, id)
{
	initialize();
}

ncGroup::ncGroup(ncGroup* const parent, const int & id) : ncGroup(parent, std::string(), id) {
}

ncGroup::~ncGroup() {
}

std::string ncGroup::getGroupName(const int & groupId) {
	char name[NC_MAX_NAME + 1];
	int ret = nc_inq_grpname(groupId, name);
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_inq_grpname failed for group with id=%d, error : %s (%d)", __func__, groupId, nc_strerror(ret), ret);
		return std::string();
	}
	
	return std::string(name);
}

ncGroup * const ncGroup::getParentGroup() const {
	return dynamic_cast<ncGroup*>(getParent());
}

ncGroup * const ncGroup::getRootGroup() {
	auto parentGroup = getParentGroup();
	if (parentGroup != nullptr) {
		return parentGroup->getRootGroup();
	}
	else {
		return this;
	}
}

std::vector<int> ncGroup::searchAllSubgroupsIds() {
	// Recherche de tous les identifiants de groupes
	int size = NC_MAX_VARS;
	std::vector<int> groupIds(size, ncUtils::INVALID_ID);

	int ret = nc_inq_grps(getId(), &size, groupIds.data());
	if (ret) {
		ncUtils::logError("%s: nc_inq_grps failed for group %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
		return groupIds;
	}
	groupIds.resize(size);

	return groupIds;
}

void ncGroup::createGroup() {
	// Pas de création en cas
	if (!isEditable() // la création n'est pas permise
		|| getId() != ncUtils::INVALID_ID // d'identifiant existant (groupe déjà créé)
		|| getParentId() == ncUtils::INVALID_ID // de parent absent (pas de destination)
		|| getName().empty() // de nom vide
		) return;

	// Création groupe
	int status = nc_def_grp(getParentId(), getName().c_str(), &m_id);
	if (status != NC_NOERR) {
		ncUtils::logError("%s: nc_def_grp failed parent %s and name %s, error : %s (%d)", __func__, getParent() ? getParent()->getName().c_str() : "Root", getName().c_str(), nc_strerror(status), status);
	}
}

void ncGroup::initialize() {
	if (getParent() != nullptr && getParentId() == ncUtils::INVALID_ID) return;

	// Identifiant
	if (!getName().empty() && getId() == ncUtils::INVALID_ID) {
		int ret = nc_inq_ncid(getParentId(), getName().c_str(), &m_id);
		if (ret == NC_ENOGRP) { // Si le groupe n'est pas trouvé, on le créé
			createGroup();
		}
		else if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_inq_ncid failed for group %s, error : %s (%d)", __func__, getName().c_str(), nc_strerror(ret), ret);
		}
	}

	// Nom
	if (getId() != ncUtils::INVALID_ID && getName().empty()) {
		getGroupName(getId());
	}

	// Récupération des types personnalisés
	if (getId() != ncUtils::INVALID_ID) {
		retrieveCustomTypes();
	}
}

int ncGroup::getAttributesGroupId() const {
	return getId();
}

int ncGroup::getTypeGroupId() const {
	return getId();
}

ncTypeOwner::ncTypeOwner() {
}

nc_type& ncTypeOwner::getNcType(const std::string& typeName) {
	return m_types[typeName];
}

void ncTypeOwner::retrieveCustomTypes() {

	// Recherche de tous les identifiants de types
	int nbCustomTypes;
	int typeIdList[MAX_CUSTOM_TYPES];
	int ret = nc_inq_typeids(getTypeGroupId(), &nbCustomTypes, typeIdList);
	if (ret != NC_NOERR) {
		ncUtils::logError("%s: nc_inq_typeids failed : %s (%d)", __func__, nc_strerror(ret), ret);
		return;
	}

	// Recherche de tous les noms de types
	for (int i = 0; i < nbCustomTypes; ++i) {
		int typeId = typeIdList[i];
		char name[CUSTOM_TYPE_NAME_MAX_SIZE];
		ret = nc_inq_user_type(getTypeGroupId(), typeId, name, nullptr, nullptr, nullptr, nullptr);
		if (ret != NC_NOERR) {
			ncUtils::logError("%s: nc_inq_user_type failed : %s (%d)", __func__, nc_strerror(ret), ret);
			continue;
		}

		m_types[name] = typeId;
	}
}

int ncAttributeOwner::getAttributesVarId() const {
	// On garde le même identifiant que le groupe par défaut
	// Lors de l'écriture de l'attribut, on testera l'égalité entre l'id groupe et l'id variable.
	// Si il sont identiques, on appliquera alors NC_GLOBAL
	return getAttributesGroupId();
}


int ncAttributeOwner::getVarId() const{
	// Si l'id groupe et l'id variable sont identiques, c'est un attribut global
	return getAttributesVarId() == getAttributesGroupId() ? NC_GLOBAL : getAttributesVarId();
}