#include "Reader/SonarNetCDF/Platform/Attitude/VariablePitch.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Attitude/VariableAttitudeTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariablePitch::VARIABLE_NAME = "pitch";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Pitch angle";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "platform_pitch_angle";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degrees";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -90.0f, 90.0f };
}

VariablePitch::VariablePitch(ncObject* parent, VariableAttitudeTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariablePitch::~VariablePitch() {
}

void VariablePitch::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}

void VariablePitch::initializeFillValue() {
	setFillValue(std::nanf(""));
}
