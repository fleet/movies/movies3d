#include "Reader/SonarNetCDF/Platform/Attitude/VariablePitchRate.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Attitude/VariableAttitudeTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariablePitchRate::VARIABLE_NAME = "pitch_rate";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Pitch rate";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "platform_pitch_rate";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "degree/s";
}

VariablePitchRate::VariablePitchRate(ncObject* parent, VariableAttitudeTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariablePitchRate::~VariablePitchRate() {
}

void VariablePitchRate::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariablePitchRate::initializeFillValue() {
	setFillValue(std::nanf(""));
}
