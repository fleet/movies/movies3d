#include "Reader/SonarNetCDF/Platform/Attitude/VariableRoll.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Attitude/VariableAttitudeTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableRoll::VARIABLE_NAME = "roll";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Roll angle";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "platform_roll_angle";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degrees";
}

VariableRoll::VariableRoll(ncObject* parent, VariableAttitudeTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableRoll::~VariableRoll() {
}

void VariableRoll::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableRoll::initializeFillValue() {
	setFillValue(std::nanf(""));
}
