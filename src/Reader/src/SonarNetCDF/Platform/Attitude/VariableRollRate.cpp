#include "Reader/SonarNetCDF/Platform/Attitude/VariableRollRate.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Attitude/VariableAttitudeTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableRollRate::VARIABLE_NAME = "roll_rate";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Roll rate";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "platform_roll_rate";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "degree/s";
}

VariableRollRate::VariableRollRate(ncObject* parent, VariableAttitudeTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableRollRate::~VariableRollRate() {
}

void VariableRollRate::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableRollRate::initializeFillValue() {
	setFillValue(std::nanf(""));
}
