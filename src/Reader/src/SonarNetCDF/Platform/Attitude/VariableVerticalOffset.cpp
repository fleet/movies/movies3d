#include "Reader/SonarNetCDF/Platform/Attitude/VariableVerticalOffset.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Attitude/VariableAttitudeTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableVerticalOffset::VARIABLE_NAME = "vertical_offset";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Platform vertical offset from nominal";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariableVerticalOffset::VariableVerticalOffset(ncObject* parent, VariableAttitudeTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableVerticalOffset::~VariableVerticalOffset() {
}

void VariableVerticalOffset::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableVerticalOffset::initializeFillValue() {
	setFillValue(std::nanf(""));
}
