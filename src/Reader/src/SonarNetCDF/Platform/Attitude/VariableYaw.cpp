#include "Reader/SonarNetCDF/Platform/Attitude/VariableYaw.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Attitude/VariableAttitudeTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableYaw::VARIABLE_NAME = "heading";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Platform heading(true)";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "platform_orientation";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "degrees_north";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { 0.0f, 360.0f };
}

VariableYaw::VariableYaw(ncObject* parent, VariableAttitudeTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableYaw::~VariableYaw() {
}

void VariableYaw::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}

void VariableYaw::initializeFillValue() {
	setFillValue(std::nanf(""));
}
