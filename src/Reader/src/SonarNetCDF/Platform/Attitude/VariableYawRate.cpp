#include "Reader/SonarNetCDF/Platform/Attitude/VariableYawRate.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Attitude/VariableAttitudeTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableYawRate::VARIABLE_NAME = "heading_rate";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Platform heading rate";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "degree/s";
}

VariableYawRate::VariableYawRate(ncObject* parent, VariableAttitudeTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableYawRate::~VariableYawRate() {
}

void VariableYawRate::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableYawRate::initializeFillValue() {
	setFillValue(std::nanf(""));
}
