#include "Reader/SonarNetCDF/Platform/GroupAttitude.h"

#include "Reader/SonarNetCDF/Platform/Attitude/VariableYaw.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariableYawRate.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariablePitch.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariablePitchRate.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariableRoll.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariableRollRate.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariableAttitudeTime.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariableVerticalOffset.h"

using namespace sonarNetCDF;

// Group name
const std::string GroupAttitude::NC_GROUP_NAME = "Attitude";

namespace {
	// Attribute names
	constexpr const char* DESCRIPTION_ATTRIBUTE_NAME = "description";
}

GroupAttitude::GroupAttitude(ncGroup* const parent, const std::string& name) : UnderTopLevelGroup(parent, name)
{
	if (isEditable()) {
		// Cr�ation des attributs
		initializeAttributes();
	}

	// Cr�ation des dimensions
	m_timeDimension = std::make_unique<ncDimension>(this, VariableAttitudeTime::VARIABLE_NAME, NC_UNLIMITED);

	m_timeVariable = std::make_unique<VariableAttitudeTime>(this, m_timeDimension.get());

	// Cr�ation des variables
	m_yawVariable = std::make_unique<VariableYaw>(this, m_timeVariable.get());
	m_yawRateVariable = std::make_unique<VariableYawRate>(this, m_timeVariable.get());
	m_pitchVariable = std::make_unique<VariablePitch>(this, m_timeVariable.get());
	m_pitchRateVariable = std::make_unique<VariablePitchRate>(this, m_timeVariable.get());
	m_rollVariable = std::make_unique<VariableRoll>(this, m_timeVariable.get());
	m_rollRateVariable = std::make_unique<VariableRollRate>(this, m_timeVariable.get());
	m_verticalOffsetVariable = std::make_unique<VariableVerticalOffset>(this, m_timeVariable.get());
}

GroupAttitude::~GroupAttitude() {
}

VariableAttitudeTime * GroupAttitude::getTimeVariable() const {
	return m_timeVariable.get();
}

VariableYaw * GroupAttitude::getYawVariable() const {
	return m_yawVariable.get();
}

VariableYawRate * GroupAttitude::getYawRateVariable() const {
	return m_yawRateVariable.get();
}

VariablePitch * GroupAttitude::getPitchVariable() const {
	return m_pitchVariable.get();
}

VariablePitchRate * GroupAttitude::getPitchRateVariable() const {
	return m_pitchRateVariable.get();
}

VariableRoll * GroupAttitude::getRollVariable() const {
	return m_rollVariable.get();
}

VariableRollRate * GroupAttitude::getRollRateVariable() const {
	return m_rollRateVariable.get();
}

VariableVerticalOffset * GroupAttitude::getVerticalOffsetVariable() const {
	return m_verticalOffsetVariable.get();
}

void GroupAttitude::initializeAttributes() {
	// attributs � remplir
	createEmptyAttribute(DESCRIPTION_ATTRIBUTE_NAME);
}
