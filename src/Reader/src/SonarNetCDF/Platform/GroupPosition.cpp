#include "Reader/SonarNetCDF/Platform/GroupPosition.h"

#include "Reader/SonarNetCDF/Platform/Position/VariableAltitude.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableCourseOverGround.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableDistance.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableHeading.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableHeightAboveReferenceEllipsoid.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableLatitude.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableLongitude.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableSpeedOverGround.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableSpeedRelative.h"
#include "Reader/SonarNetCDF/Platform/Position/VariablePositionTime.h"

using namespace sonarNetCDF;

// Group name
const std::string GroupPosition::NC_GROUP_NAME = "Position";

namespace {
	// Attribute names
	constexpr const char* DESCRIPTION_ATTRIBUTE_NAME = "description";
}

GroupPosition::GroupPosition(ncGroup* const parent, const std::string& name) : UnderTopLevelGroup(parent, name)
{
	if (isEditable()) {
		// Cr�ation des attributs
		initializeAttributes();
	}

	// Cr�ation des dimensions
	m_timeDimension = std::make_unique<ncDimension>(this, VariablePositionTime::VARIABLE_NAME, NC_UNLIMITED);

	m_timeVariable = std::make_unique<VariablePositionTime>(this, m_timeDimension.get());

	// Cr�ation des variables
	m_altitudeVariable = std::make_unique<VariableAltitude>(this, m_timeVariable.get());
	m_courseOverGroundVariable = std::make_unique<VariableCourseOverGround>(this, m_timeVariable.get());
	m_distanceVariable = std::make_unique<VariableDistance>(this, m_timeVariable.get());
	m_headingVariable = std::make_unique<VariableHeading>(this, m_timeVariable.get());
	m_heightAboveReferenceEllipsoidVariable = std::make_unique<VariableHeightAboveReferenceEllipsoid>(this, m_timeVariable.get());
	m_latitudeVariable = std::make_unique<VariableLatitude>(this, m_timeVariable.get());
	m_longitudeVariable = std::make_unique<VariableLongitude>(this, m_timeVariable.get());
	m_speedOverGroundVariable = std::make_unique<VariableSpeedOverGround>(this, m_timeVariable.get());
	m_speedRelativeVariable = std::make_unique<VariableSpeedRelative>(this, m_timeVariable.get());
}

GroupPosition::~GroupPosition() {
}

VariablePositionTime * GroupPosition::getTimeVariable() const {
	return m_timeVariable.get();
}

VariableLatitude * GroupPosition::getLatitudeVariable() const {
	return m_latitudeVariable.get();
}

VariableLongitude * GroupPosition::getLongitudeVariable() const {
	return m_longitudeVariable.get();
}

VariableHeading * GroupPosition::getHeadingVariable() const {
	return m_headingVariable.get();
}

VariableCourseOverGround * GroupPosition::getCourseOverGroundVariable() const {
	return m_courseOverGroundVariable.get();
}

VariableSpeedOverGround * GroupPosition::getSpeedOverGroundVariable() const {
	return m_speedOverGroundVariable.get();
}

VariableSpeedRelative * GroupPosition::getSpeedRelativeVariable() const {
	return m_speedRelativeVariable.get();
}

VariableHeightAboveReferenceEllipsoid * GroupPosition::getHeightAboveReferenceEllipsoidVariable() const {
	return m_heightAboveReferenceEllipsoidVariable.get();
}

VariableAltitude * GroupPosition::getAltitudeVariable() const {
	return m_altitudeVariable.get();
}

VariableDistance * GroupPosition::getDistanceVariable() const {
	return m_distanceVariable.get();
}

void GroupPosition::initializeAttributes() {
	// attributs � remplir
	createEmptyAttribute(DESCRIPTION_ATTRIBUTE_NAME);
}
