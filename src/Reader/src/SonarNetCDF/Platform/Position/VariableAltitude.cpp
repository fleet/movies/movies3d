#include "Reader/SonarNetCDF/Platform/Position/VariableAltitude.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Position/VariablePositionTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableAltitude::VARIABLE_NAME = "altitude";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "altitude";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "altitude";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "time latitude longitude";
}

VariableAltitude::VariableAltitude(ncObject* parent, VariablePositionTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableAltitude::~VariableAltitude() {
}

void VariableAltitude::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}

void VariableAltitude::initializeFillValue() {
	setFillValue(std::nanf(""));
}
