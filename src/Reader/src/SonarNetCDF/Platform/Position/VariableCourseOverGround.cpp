#include "Reader/SonarNetCDF/Platform/Position/VariableCourseOverGround.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Position/VariablePositionTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableCourseOverGround::VARIABLE_NAME = "course_over_ground";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "degree";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "platform_course";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m/s";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "time latitude longitude";
}

VariableCourseOverGround::VariableCourseOverGround(ncObject* parent, VariablePositionTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable) 
{
	createVariable();
}

VariableCourseOverGround::~VariableCourseOverGround() {
}

void VariableCourseOverGround::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}

void VariableCourseOverGround::initializeFillValue() {
	setFillValue(std::nanf(""));
}
