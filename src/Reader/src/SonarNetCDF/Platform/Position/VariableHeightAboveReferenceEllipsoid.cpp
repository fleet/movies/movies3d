#include "Reader/SonarNetCDF/Platform/Position/VariableHeightAboveReferenceEllipsoid.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Position/VariablePositionTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableHeightAboveReferenceEllipsoid::VARIABLE_NAME = "height_above_reference_ellipsoid";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "height_above_reference_ellipsoid";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "height_above_reference_ellipsoid";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "time latitude longitude";
}

VariableHeightAboveReferenceEllipsoid::VariableHeightAboveReferenceEllipsoid(ncObject* parent, VariablePositionTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableHeightAboveReferenceEllipsoid::~VariableHeightAboveReferenceEllipsoid() {
}

void VariableHeightAboveReferenceEllipsoid::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}

void VariableHeightAboveReferenceEllipsoid::initializeFillValue() {
	setFillValue(std::nanf(""));
}
