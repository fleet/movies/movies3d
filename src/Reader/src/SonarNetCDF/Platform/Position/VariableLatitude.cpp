#include "Reader/SonarNetCDF/Platform/Position/VariableLatitude.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Position/VariablePositionTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableLatitude::VARIABLE_NAME = "latitude";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "latitude";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "Platform latitude";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "degrees_north";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -90.0f, 90.0f };
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "time latitude longitude";
}

VariableLatitude::VariableLatitude(ncObject* parent, VariablePositionTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableLatitude::~VariableLatitude() {
}

void VariableLatitude::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}

void VariableLatitude::initializeFillValue() {
	setFillValue(std::nanf(""));
}
