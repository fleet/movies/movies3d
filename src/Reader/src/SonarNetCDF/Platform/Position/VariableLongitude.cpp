#include "Reader/SonarNetCDF/Platform/Position/VariableLongitude.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Position/VariablePositionTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableLongitude::VARIABLE_NAME = "longitude";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "longitude";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "Platform longitude";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "degrees_east";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -180.0f, 180.0f };
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "time latitude longitude";
}

VariableLongitude::VariableLongitude(ncObject* parent, VariablePositionTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableLongitude::~VariableLongitude() {
}

void VariableLongitude::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}

void VariableLongitude::initializeFillValue() {
	setFillValue(std::nanf(""));
}
