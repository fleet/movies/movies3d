#include "Reader/SonarNetCDF/Platform/Position/VariablePositionTime.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariablePositionTime::VARIABLE_NAME = "time";

namespace {
	// Attributes values
	constexpr const char* AXIS_ATTRIBUTE_VALUE = "T";
	constexpr const char* CALENDAR_ATTRIBUTE_VALUE = "gregorian";
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Timestamps for position data";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "time";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "nanoseconds since 1970-01-01 00:00:00Z";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "time latitude longitude";
}

VariablePositionTime::VariablePositionTime(ncObject* parent, ncDimension* timeDimension)
	: VariableSingleDimension<uint64_t>(parent, VARIABLE_NAME, timeDimension)
{
	createVariable();
}

VariablePositionTime::~VariablePositionTime() {
}

void VariablePositionTime::initializeAttributes() {
	createAttribute(VariableAttributesNames::AXIS, AXIS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::CALENDAR, CALENDAR_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}
