#include "Reader/SonarNetCDF/Platform/Position/VariableSpeedOverGround.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Position/VariablePositionTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableSpeedOverGround::VARIABLE_NAME = "speed_over_ground";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "speed_over_ground";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "platform_speed_wrt_ground";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m/s";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "time latitude longitude";
}

VariableSpeedOverGround::VariableSpeedOverGround(ncObject* parent, VariablePositionTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableSpeedOverGround::~VariableSpeedOverGround() {
}

void VariableSpeedOverGround::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}

void VariableSpeedOverGround::initializeFillValue() {
	setFillValue(std::nanf(""));
}
