#include "Reader/SonarNetCDF/Platform/Position/VariableSpeedRelative.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/Position/VariablePositionTime.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableSpeedRelative::VARIABLE_NAME = "speed_relative";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Platform speed relative to water";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "platform_speed_wrt_seawater";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m/s";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "time latitude longitude";
}

VariableSpeedRelative::VariableSpeedRelative(ncObject* parent, VariablePositionTime* timeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableSpeedRelative::~VariableSpeedRelative() {
}

void VariableSpeedRelative::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}

void VariableSpeedRelative::initializeFillValue() {
	setFillValue(std::nanf(""));
}
