#include "Reader/SonarNetCDF/Platform/VariableMRUIDs.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;

// Variable name
const std::string VariableMruIds::VARIABLE_NAME = "MRU_ids";

VariableMruIds::VariableMruIds(ncObject* parent, ncDimension * mruDimension) 
	: VariableSingleDimension<std::string>(parent, VARIABLE_NAME, mruDimension)
{
	createVariable();
}

VariableMruIds::~VariableMruIds() {
}
