#include "Reader/SonarNetCDF/Platform/VariableMRUOffsetX.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariableMRUIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableMruOffsetX::VARIABLE_NAME = "MRU_offset_x";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Distance along the x-axis from the platform coordinate system origin to the motion reference unit sensor origin";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariableMruOffsetX::VariableMruOffsetX(ncObject* parent, VariableMruIds* mruVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, mruVariable)
{
	createVariable();
}

VariableMruOffsetX::~VariableMruOffsetX() {
}

void VariableMruOffsetX::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableMruOffsetX::initializeFillValue() {
	setFillValue(std::nanf(""));
}
