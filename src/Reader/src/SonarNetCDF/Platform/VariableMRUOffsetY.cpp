#include "Reader/SonarNetCDF/Platform/VariableMRUOffsetY.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariableMRUIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableMruOffsetY::VARIABLE_NAME = "MRU_offset_y";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Distance along the y-axis from the platform coordinate system origin to the motion reference unit sensor origin";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariableMruOffsetY::VariableMruOffsetY(ncObject* parent, VariableMruIds* mruIdVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, mruIdVariable)
{
	createVariable();
}

VariableMruOffsetY::~VariableMruOffsetY() {
}

void VariableMruOffsetY::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableMruOffsetY::initializeFillValue() {
	setFillValue(std::nanf(""));
}

