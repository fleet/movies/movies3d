#include "Reader/SonarNetCDF/Platform/VariableMRUOffsetZ.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariableMRUIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableMruOffsetZ::VARIABLE_NAME = "MRU_offset_z";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Distance along the z-axis from the platform coordinate system origin to the motion reference unit sensor origin";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariableMruOffsetZ::VariableMruOffsetZ(ncObject* parent, VariableMruIds* mruIdVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, mruIdVariable)
{
	createVariable();
}

VariableMruOffsetZ::~VariableMruOffsetZ() {
}

void VariableMruOffsetZ::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableMruOffsetZ::initializeFillValue() {
	setFillValue(std::nanf(""));
}
