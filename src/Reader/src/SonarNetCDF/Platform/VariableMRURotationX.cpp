#include "Reader/SonarNetCDF/Platform/VariableMRURotationX.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariableMRUIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableMruRotationX::VARIABLE_NAME = "MRU_rotation_x";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Extrinsic rotation about the x-axis from the platform to MRU coordinate systems";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -180.0f, 180.0f };
}

VariableMruRotationX::VariableMruRotationX(ncObject* parent, VariableMruIds* mruIdVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, mruIdVariable)
{
	createVariable();
}

VariableMruRotationX::~VariableMruRotationX() {
}

void VariableMruRotationX::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}

void VariableMruRotationX::initializeFillValue() {
	setFillValue(std::nanf(""));
}


