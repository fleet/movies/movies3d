#include "Reader/SonarNetCDF/Platform/VariablePositionIDs.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;

// Variable name
const std::string VariablePositionIds::VARIABLE_NAME = "position_ids";

VariablePositionIds::VariablePositionIds(ncObject* parent, ncDimension * positionDimension)
	: VariableSingleDimension<std::string>(parent, VARIABLE_NAME, positionDimension)
{
	createVariable();
}

VariablePositionIds::~VariablePositionIds() {
}
