#include "Reader/SonarNetCDF/Platform/VariablePositionOffsetX.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariablePositionIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariablePositionOffsetX::VARIABLE_NAME = "position_offset_x";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Distance along the x-axis from the platform coordinate system origin to the latitude/longitude sensor origin";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariablePositionOffsetX::VariablePositionOffsetX(ncObject* parent, VariablePositionIds* positionIdVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, positionIdVariable)
{
	createVariable();
}

VariablePositionOffsetX::~VariablePositionOffsetX() {
}

void VariablePositionOffsetX::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariablePositionOffsetX::initializeFillValue() {
	setFillValue(std::nanf(""));
}

