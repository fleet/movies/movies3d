#include "Reader/SonarNetCDF/Platform/VariablePositionOffsetY.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariablePositionIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariablePositionOffsetY::VARIABLE_NAME = "position_offset_y";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Distance along the y-axis from the platform coordinate system origin to the motion reference unit sensor origin";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariablePositionOffsetY::VariablePositionOffsetY(ncObject* parent, VariablePositionIds* positionIdVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, positionIdVariable)
{
	createVariable();
}

VariablePositionOffsetY::~VariablePositionOffsetY() {
}

void VariablePositionOffsetY::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariablePositionOffsetY::initializeFillValue() {
	setFillValue(std::nanf(""));
}

