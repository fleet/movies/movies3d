#include "Reader/SonarNetCDF/Platform/VariablePositionOffsetZ.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariablePositionIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariablePositionOffsetZ::VARIABLE_NAME = "position_offset_z";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Distance along the z-axis from the platform coordinate system origin to the motion reference unit sensor origin";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariablePositionOffsetZ::VariablePositionOffsetZ(ncObject* parent, VariablePositionIds* positionIdVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, positionIdVariable)
{
	createVariable();
}

VariablePositionOffsetZ::~VariablePositionOffsetZ() {
}

void VariablePositionOffsetZ::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariablePositionOffsetZ::initializeFillValue() {
	setFillValue(std::nanf(""));
}
