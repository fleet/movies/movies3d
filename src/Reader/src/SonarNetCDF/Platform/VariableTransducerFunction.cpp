#include "Reader/SonarNetCDF/Platform/VariableTransducerFunction.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"
#include "Reader/SonarNetCDF/Platform/PlatformCustomTypes.h"

#include "Reader/SonarNetCDF/Platform/VariableTransducerIDs.h"

using namespace sonarNetCDF;

// Variable name
const std::string VariableTransducerFunction::VARIABLE_NAME = "transducer_function";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Transducer function (transmit_only, receive_only, monostatic)";
}

VariableTransducerFunction::VariableTransducerFunction(ncObject* parent, VariableTransducerIds* transducerIdVariable)
	: VariableSingleVariable<uint8_t, std::string>(parent, VARIABLE_NAME, transducerIdVariable)
{
	setTypeName(platform::TransducerFunctionType::getTypeName());
	createVariable();
}

VariableTransducerFunction::~VariableTransducerFunction() {
}

platform::TransducerFunctionType::Type VariableTransducerFunction::getFunctionTypeAtIndex(const size_t & transducerIndex) const {
	return platform::TransducerFunctionType::Type(getValueAtIndex(transducerIndex));
}

platform::TransducerFunctionType::Type VariableTransducerFunction::getFunctionTypeAt(const std::string & transducerId) const {
	return platform::TransducerFunctionType::Type(getValueAt(transducerId));
}

void VariableTransducerFunction::setFunctionTypeAtIndex(const size_t & transducerIndex, platform::TransducerFunctionType::Type transducerFunctionType) {
	setValueAtIndex(transducerIndex, platform::TransducerFunctionType::getValue(transducerFunctionType));
}

void VariableTransducerFunction::setFunctionTypeAt(const std::string & transducerId, platform::TransducerFunctionType::Type transducerFunctionType) {
	setValueAt(transducerId, platform::TransducerFunctionType::getValue(transducerFunctionType));
}

void VariableTransducerFunction::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
