#include "Reader/SonarNetCDF/Platform/VariableTransducerIDs.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;

// Variable name
const std::string VariableTransducerIds::VARIABLE_NAME = "transducer_ids";

VariableTransducerIds::VariableTransducerIds(ncObject* parent, ncDimension * transducerDimension)
	: VariableSingleDimension<std::string>(parent, VARIABLE_NAME, transducerDimension)
{
	createVariable();
}

VariableTransducerIds::~VariableTransducerIds() {
}
