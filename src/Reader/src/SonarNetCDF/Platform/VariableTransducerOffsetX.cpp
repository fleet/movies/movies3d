#include "Reader/SonarNetCDF/Platform/VariableTransducerOffsetX.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariableTransducerIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableTransducerOffsetX::VARIABLE_NAME = "transducer_offset_x";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "x-axis distance from the platform coordinate system origin to the sonar transducer";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariableTransducerOffsetX::VariableTransducerOffsetX(ncObject* parent, VariableTransducerIds* transducerIdVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, transducerIdVariable)
{
	createVariable();
}

VariableTransducerOffsetX::~VariableTransducerOffsetX() {
}

void VariableTransducerOffsetX::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableTransducerOffsetX::initializeFillValue() {
	setFillValue(std::nanf(""));
}

