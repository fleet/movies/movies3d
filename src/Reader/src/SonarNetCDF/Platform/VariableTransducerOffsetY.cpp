#include "Reader/SonarNetCDF/Platform/VariableTransducerOffsetY.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariableTransducerIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableTransducerOffsetY::VARIABLE_NAME = "transducer_offset_y";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "y-axis distance from the platform coordinate system origin to the sonar transducer";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariableTransducerOffsetY::VariableTransducerOffsetY(ncObject* parent, VariableTransducerIds* transducerIdVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, transducerIdVariable)
{
	createVariable();
}

VariableTransducerOffsetY::~VariableTransducerOffsetY() {
}

void VariableTransducerOffsetY::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableTransducerOffsetY::initializeFillValue() {
	setFillValue(std::nanf(""));
}
