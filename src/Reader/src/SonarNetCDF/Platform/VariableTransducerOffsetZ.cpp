#include "Reader/SonarNetCDF/Platform/VariableTransducerOffsetZ.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariableTransducerIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableTransducerOffsetZ::VARIABLE_NAME = "transducer_offset_z";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "z-axis distance from the platform coordinate system origin to the sonar transducer";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariableTransducerOffsetZ::VariableTransducerOffsetZ(ncObject* parent, VariableTransducerIds* transducerIdVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, transducerIdVariable)
{
	createVariable();
}

VariableTransducerOffsetZ::~VariableTransducerOffsetZ() {
}

void VariableTransducerOffsetZ::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableTransducerOffsetZ::initializeFillValue() {
	setFillValue(std::nanf(""));
}

