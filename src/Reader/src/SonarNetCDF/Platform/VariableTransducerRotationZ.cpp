#include "Reader/SonarNetCDF/Platform/VariableTransducerRotationZ.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Platform/VariableTransducerIDs.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableTransducerRotationZ::VARIABLE_NAME = "transducer_rotation_z";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Extrinsic rotation about the z-azis from the transducer to reference coordinate systems";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -180.0f, 180.0f };
}

VariableTransducerRotationZ::VariableTransducerRotationZ(ncObject* parent, VariableTransducerIds* transducerIdVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, transducerIdVariable)
{
	createVariable();
}

VariableTransducerRotationZ::~VariableTransducerRotationZ() {
}

void VariableTransducerRotationZ::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}

void VariableTransducerRotationZ::initializeFillValue() {
	setFillValue(std::nanf(""));
}
