#include "Reader/SonarNetCDF/Platform/VariableWaterLevel.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include <cmath>

using namespace sonarNetCDF;

// Variable name
const std::string VariableWaterLevel::VARIABLE_NAME = "water_level";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Distance from the platform coordinate system origin to the nominal water level along the z-axis";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariableWaterLevel::VariableWaterLevel(ncObject* parent) 
	: VariableScalar<float>(parent, VARIABLE_NAME)
{
	createVariable();
}

VariableWaterLevel::~VariableWaterLevel() {
}

void VariableWaterLevel::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

void VariableWaterLevel::initializeFillValue() {
	setFillValue(std::nanf(""));
}
