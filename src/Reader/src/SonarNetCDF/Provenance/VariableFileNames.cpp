#include "Reader/SonarNetCDF/Provenance/VariableFileNames.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;

// Variable name
const std::string VariableFileNames::VARIABLE_NAME = "source_filenames";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Source filenames";
}

VariableFileNames::VariableFileNames(ncObject* parent, ncDimension* filenameDimension)
	: VariableSingleDimension<std::string>(parent, VARIABLE_NAME, filenameDimension)
{
	createVariable();
}

VariableFileNames::~VariableFileNames() {
}

void VariableFileNames::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
