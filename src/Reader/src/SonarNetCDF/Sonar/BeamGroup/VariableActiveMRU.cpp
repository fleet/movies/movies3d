#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableActiveMRU.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableActiveMRU::VARIABLE_NAME = "active_MRU";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Active MRU sensor index";
	const constexpr int VALID_MIN_ATTRIBUTE_VALUE = 0;
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariableActiveMRU::VariableActiveMRU(ncObject* parent, VariablePingTime* VariablePingTime)
	: VariableSingleVariable<int, uint64_t>(parent, VARIABLE_NAME, VariablePingTime)
{
	createVariable();
}

VariableActiveMRU::~VariableActiveMRU() {
}

void VariableActiveMRU::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}