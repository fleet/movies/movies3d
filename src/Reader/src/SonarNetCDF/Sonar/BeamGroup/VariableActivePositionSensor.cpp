#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableActivePositionSensor.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableActivePositionSensor::VARIABLE_NAME = "active_position_sensor";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Active position sensor index";
	const constexpr int VALID_MIN_ATTRIBUTE_VALUE = 0;
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariableActivePositionSensor::VariableActivePositionSensor(ncObject* parent, VariablePingTime* timeVariable)
	: VariableSingleVariable<int, uint64_t>(parent, VARIABLE_NAME, timeVariable)
{
	createVariable();
}

VariableActivePositionSensor::~VariableActivePositionSensor() {
}

void VariableActivePositionSensor::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}