#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterI.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;
using namespace sonar;

// Variable name
const std::string IVariableBackscatterI::VARIABLE_NAME = "backscatter_i";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Raw backscatter measurements (imaginary part)";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "as appropriate";
}

template<typename T>
VariableBackscatterI<T>::VariableBackscatterI(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, PseudoVariableSingleDimension* subbeamVariable)
	: VariableTripleVariable<T, uint64_t, std::string, size_t>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable, subbeamVariable)
{
	this->setTypeName(SampleType::getTypeName());
	this->createVariable();
}

template<typename T>
void VariableBackscatterI<T>::initializeAttributes() {
	this->createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	this->createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

// Explicit template instantiation
template class VariableBackscatterI<sonar::SampleType::typeShort>;
template class VariableBackscatterI<sonar::SampleType::typeFloat>;

std::unique_ptr<IVariableBackscatterI> VariableBackscatterIFactory::build(const size_t & numberOfSubbeam, ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, PseudoVariableSingleDimension* subbeamVariable) {
	return numberOfSubbeam > 1
		? std::unique_ptr<IVariableBackscatterI>(new VariableBackscatterI<sonar::SampleType::typeFloat>(parent, pingTimeVariable, beamVariable, subbeamVariable))
		: std::unique_ptr<IVariableBackscatterI>(new VariableBackscatterI<sonar::SampleType::typeShort>(parent, pingTimeVariable, beamVariable, subbeamVariable));
}
