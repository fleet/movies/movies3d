#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterR.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

#include <limits>

using namespace sonarNetCDF;
using namespace beamGroup;
using namespace sonar;

// Variable name
const std::string IVariableBackscatterR::VARIABLE_NAME = "backscatter_r";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Raw backscatter measurements (real part)";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "as appropriate";
	const constexpr short MISSING_VALUE_ATTRIBUTE_VALUE = std::numeric_limits<short>::min();
}

template<typename T>
VariableBackscatterR<T>::VariableBackscatterR(ncObject * parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, PseudoVariableSingleDimension* subbeamVariable)
	: VariableTripleVariable<T, uint64_t, std::string, size_t>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable, subbeamVariable)
{
	this->setTypeName(SampleType::getTypeName());
	this->createVariable();
}

template<typename T>
float VariableBackscatterR<T>::getScaleFactor() const {
	return ncAttributeOwner::getAttributeValue<float>(VariableAttributesNames::SCALE_FACTOR);
}

template<typename T>
void VariableBackscatterR<T>::setScaleFactor(const float & scaleFactor) {
	this->setAttributeValue(VariableAttributesNames::SCALE_FACTOR, scaleFactor);
}

template<typename T>
void VariableBackscatterR<T>::initializeAttributes() {
	this->createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	this->createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

template<> // Spécification Short
void VariableBackscatterR<sonar::SampleType::typeShort>::initializeAttributes() {
	this->createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	this->createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	this->createAttribute(VariableAttributesNames::MISSING_VALUE, MISSING_VALUE_ATTRIBUTE_VALUE);
}

// Explicit template instantiation
template class VariableBackscatterR<sonar::SampleType::typeShort>;
template class VariableBackscatterR<sonar::SampleType::typeFloat>;

std::unique_ptr<IVariableBackscatterR> VariableBackscatterRFactory::build(const size_t & numberOfSubbeam, ncObject * parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, PseudoVariableSingleDimension* subbeamVariable) {
	return numberOfSubbeam > 1
		? std::unique_ptr<IVariableBackscatterR>(new VariableBackscatterR<sonar::SampleType::typeFloat>(parent, pingTimeVariable, beamVariable, subbeamVariable))
		: std::unique_ptr<IVariableBackscatterR>(new VariableBackscatterR<sonar::SampleType::typeShort>(parent, pingTimeVariable, beamVariable, subbeamVariable));
}
