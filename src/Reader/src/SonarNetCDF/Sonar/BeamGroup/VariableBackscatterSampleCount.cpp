#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterSampleCount.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableBackscatterSampleCount::VARIABLE_NAME = "sample_count";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Number of samples per ping in each beam, and optionally subbeam";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "1";
	const constexpr int VALID_MIN_ATTRIBUTE_VALUE = 0;
}

VariableBackscatterSampleCount::VariableBackscatterSampleCount(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable, PseudoVariableSingleDimension* subbeamVariable)
	: VariableTripleVariable<int, uint64_t, std::string, size_t>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable, subbeamVariable)
{
	createVariable();
}

void VariableBackscatterSampleCount::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}