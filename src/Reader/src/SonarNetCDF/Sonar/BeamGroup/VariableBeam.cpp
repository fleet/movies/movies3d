#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableBeam::VARIABLE_NAME = "beam";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Beam name";
}

VariableBeam::VariableBeam(ncObject* parent, ncDimension* beamDimension)
	: VariableSingleDimension<std::string>(parent, VARIABLE_NAME, beamDimension)
{
	createVariable();
}

VariableBeam::~VariableBeam() {
}

void VariableBeam::setTransducerName(const std::string & transducerName) {
	setAttributeValue(VariableAttributesNames::LONG_NAME, transducerName);
}

std::string VariableBeam::getTransducerName() const {
	return getAttributeValue<std::string>(VariableAttributesNames::LONG_NAME);
}

void VariableBeam::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}