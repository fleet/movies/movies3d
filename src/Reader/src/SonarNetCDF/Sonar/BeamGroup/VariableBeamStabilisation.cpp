#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamStabilisation.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace beamGroup;

// Variable name
const std::string VariableBeamStabilisation::VARIABLE_NAME = "beam_stabilisation";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Beam stabilisation applied (or not)";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariableBeamStabilisation::VariableBeamStabilisation(ncObject* parent, VariablePingTime* pingTimeVariable)
	: VariableSingleVariable<uint8_t, uint64_t>(parent, VARIABLE_NAME, pingTimeVariable)
{
	setTypeName(BeamStabilisation::getTypeName());
	createVariable();
}

VariableBeamStabilisation::~VariableBeamStabilisation() {
}

BeamStabilisation::Type VariableBeamStabilisation::getModeAtIndex(const size_t & pingTimeIndex) const {
	return BeamStabilisation::getMode(getValueAtIndex(pingTimeIndex));
}

BeamStabilisation::Type VariableBeamStabilisation::getModeAt(const uint64_t & pingTime) const {
	return sonar::BeamStabilisation::Type(getValueAt(pingTime));
}

void VariableBeamStabilisation::setModeAtIndex(const size_t & pingTimeIndex, sonar::BeamStabilisation::Type stabilisationMode) {
	setValueAtIndex(pingTimeIndex, sonar::BeamStabilisation::getValue(stabilisationMode));
}

void VariableBeamStabilisation::setModeAt(const uint64_t & pingTime, sonar::BeamStabilisation::Type stabilisationMode) {
	setValueAt(pingTime, sonar::BeamStabilisation::getValue(stabilisationMode));
}

void VariableBeamStabilisation::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}