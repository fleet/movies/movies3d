#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamType.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace beamGroup;

// Variable name
const std::string VariableBeamType::VARIABLE_NAME = "beam_type";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Type of beam";
}

VariableBeamType::VariableBeamType(ncObject* parent)
	: VariableScalar<uint8_t>(parent, VARIABLE_NAME)
{
	setTypeName(BeamType::getTypeName());
	createVariable();
}

VariableBeamType::~VariableBeamType() {
}

sonar::BeamType::Type VariableBeamType::getType() const {
	return BeamType::getType(getValue());
}

void VariableBeamType::setType(sonar::BeamType::Type type) {
	setValue(BeamType::getValue(type));
}

void VariableBeamType::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
