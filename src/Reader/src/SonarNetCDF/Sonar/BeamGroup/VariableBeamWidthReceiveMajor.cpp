#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthReceiveMajor.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableBeamWidthReceiveMajor::VARIABLE_NAME = "beamwidth_receive_major";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Half power one-way receive beam width along major (horizontal) axis of beam";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -0.0f, 360.0f };
}

VariableBeamWidthReceiveMajor::VariableBeamWidthReceiveMajor(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

VariableBeamWidthReceiveMajor::~VariableBeamWidthReceiveMajor() {
}

void VariableBeamWidthReceiveMajor::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
