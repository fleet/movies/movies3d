#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthReceiveMinor.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableBeamWidthReceiveMinor::VARIABLE_NAME = "beamwidth_receive_minor";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Half power one-way receive beam width along minor (vertical) axis of beam";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -0.0f, 360.0f };
}

VariableBeamWidthReceiveMinor::VariableBeamWidthReceiveMinor(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

VariableBeamWidthReceiveMinor::~VariableBeamWidthReceiveMinor() {
}

void VariableBeamWidthReceiveMinor::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
