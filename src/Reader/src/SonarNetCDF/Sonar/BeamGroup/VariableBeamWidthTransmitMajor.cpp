#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthTransmitMajor.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableBeamWidthTransmitMajor::VARIABLE_NAME = "beamwidth_transmit_major";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Half power one-way transmit beam width along major (horizontal) axis of beam";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -0.0f, 360.0f };
}

VariableBeamWidthTransmitMajor::VariableBeamWidthTransmitMajor(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, uint64_t, size_t>(parent, VARIABLE_NAME, pingTimeVariable, txBeamVariable)
{
	createVariable();
}

VariableBeamWidthTransmitMajor::~VariableBeamWidthTransmitMajor() {
}

void VariableBeamWidthTransmitMajor::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
