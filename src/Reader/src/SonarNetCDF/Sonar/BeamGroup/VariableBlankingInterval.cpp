#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBlankingInterval.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableBlankingInterval::VARIABLE_NAME = "blanking_interval";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Amount of time during reception where samples are discarded";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "s";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableBlankingInterval::VariableBlankingInterval(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

VariableBlankingInterval::~VariableBlankingInterval() {
}

void VariableBlankingInterval::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
