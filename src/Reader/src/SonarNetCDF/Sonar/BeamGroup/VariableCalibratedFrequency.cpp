#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableCalibratedFrequency.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableFrequency.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableCalibratedFrequency::VARIABLE_NAME = "calibrated_frequency";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Calibration gain frequencies";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "Hz";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableCalibratedFrequency::VariableCalibratedFrequency(ncObject* parent, VariableFrequency* frequencyVariable)
	: VariableSingleVariable<float, float>(parent, VARIABLE_NAME, frequencyVariable)
{
	createVariable();
}

void VariableCalibratedFrequency::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}