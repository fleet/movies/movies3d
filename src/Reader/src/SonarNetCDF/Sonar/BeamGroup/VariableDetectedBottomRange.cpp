#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableDetectedBottomRange.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

#include <cmath>

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableDetectedBottomRange::VARIABLE_NAME = "detected_bottom_range";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Detected range of the bottom";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableDetectedBottomRange::VariableDetectedBottomRange(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

VariableDetectedBottomRange::~VariableDetectedBottomRange() {
}

void VariableDetectedBottomRange::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}

void VariableDetectedBottomRange::initializeFillValue() {
	setFillValue(std::nanf(""));
}
