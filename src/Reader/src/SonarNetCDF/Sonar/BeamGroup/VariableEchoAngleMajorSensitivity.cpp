#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEchoAngleMajorSensitivity.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableEchoAngleMajorSensitivity::VARIABLE_NAME = "echoangle_major_sensitivity";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Major angle scaling factor";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "1";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableEchoAngleMajorSensitivity::VariableEchoAngleMajorSensitivity(ncObject* parent, VariableBeam* beamVariable)
	: VariableSingleVariable<float, std::string>(parent, VARIABLE_NAME, beamVariable)
{
	createVariable();
}

VariableEchoAngleMajorSensitivity::~VariableEchoAngleMajorSensitivity() {
}

void VariableEchoAngleMajorSensitivity::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
