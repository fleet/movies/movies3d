#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEchoAngleMinor.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

#include <limits>

using namespace sonarNetCDF;
using namespace beamGroup;
using namespace sonar;

// Variable name
const std::string VariableEchoAngleMinor::VARIABLE_NAME = "echoangle_minor";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Echo arrival angle in the minor beam coordinate";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr short MISSING_VALUE_ATTRIBUTE_VALUE = std::numeric_limits<short>::min();
	const constexpr float SCALE_FACTOR_VALUE = 0.01f;
	const constexpr short VALID_RANGE_ATTRIBUTE_VALUE[2] = { (short)(-180 / SCALE_FACTOR_VALUE), (short)(180 / SCALE_FACTOR_VALUE) };
}

VariableEchoAngleMinor::VariableEchoAngleMinor(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<AngleType::type, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	setTypeName(AngleType::getTypeName());
	createVariable();
}

VariableEchoAngleMinor::~VariableEchoAngleMinor() {
}

void VariableEchoAngleMinor::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::SCALE_FACTOR, SCALE_FACTOR_VALUE);
	createAttribute(VariableAttributesNames::MISSING_VALUE, MISSING_VALUE_ATTRIBUTE_VALUE);
}