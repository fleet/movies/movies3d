#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEquivalentBeamAngle.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableEquivalentBeamAngle::VARIABLE_NAME = "equivalent_beam_angle";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Equivalent beam angle";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "sr";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { 0.0f, (float)(4 * M_PI) };
}

VariableEquivalentBeamAngle::VariableEquivalentBeamAngle(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

VariableEquivalentBeamAngle::~VariableEquivalentBeamAngle() {
}

void VariableEquivalentBeamAngle::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
