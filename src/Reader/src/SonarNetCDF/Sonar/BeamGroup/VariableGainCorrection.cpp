#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableGainCorrection.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableGainCorrection::VARIABLE_NAME = "gain_correction";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Gain correction";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB";
}

VariableGainCorrection::VariableGainCorrection(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

VariableGainCorrection::~VariableGainCorrection() {
}

void VariableGainCorrection::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}
