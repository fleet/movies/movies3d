#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableNonQuantitativeProcessing.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableNonQuantitativeProcessing::VARIABLE_NAME = "non_quantitative_processing";

namespace {
	// Attribute names
	constexpr const char* FLAG_MEANINGS_ATTRIBUTE_NAME = "flag_meanings";

	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Presence or not of non-quantitative processing applied to the backscattering data (sonar specific)";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariableNonQuantitativeProcessing::VariableNonQuantitativeProcessing(ncObject* parent, VariablePingTime* pingTimeVariable)
	: VariableSingleVariable<short, uint64_t>(parent, VARIABLE_NAME, pingTimeVariable)
{
	createVariable();
}

VariableNonQuantitativeProcessing::~VariableNonQuantitativeProcessing() {
}

const std::string VariableNonQuantitativeProcessing::getFlagMeanings() {
	return getAttributeValue<std::string>(FLAG_MEANINGS_ATTRIBUTE_NAME);
}

const std::vector<std::string> VariableNonQuantitativeProcessing::getFlagMeaningsList() {
	std::vector<std::string> flagMeaningsList;
	std::string flagMeanings = getFlagMeanings();

	std::string delimiter = " ";
	size_t startIndex = 0;
	size_t foundIndex = flagMeanings.find(" ");
	while (foundIndex != std::string::npos) {
		flagMeaningsList.push_back(flagMeanings.substr(startIndex, foundIndex - startIndex));
		startIndex = foundIndex + delimiter.length();
		foundIndex = flagMeanings.find(delimiter, startIndex);
	}

	return flagMeaningsList;
}

void VariableNonQuantitativeProcessing::setFlagMeanings(const std::string & flagMeanings) {
	setAttributeValue(FLAG_MEANINGS_ATTRIBUTE_NAME, flagMeanings);
}

void VariableNonQuantitativeProcessing::setFlagMeaningsList(const std::vector<std::string>& flagMeaningsList) {
	std::string spaceSeparatedMeanings;
	for (std::string meaning : flagMeaningsList)
		spaceSeparatedMeanings += meaning + " ";
	spaceSeparatedMeanings.pop_back();

	setFlagMeanings(spaceSeparatedMeanings);
}

void VariableNonQuantitativeProcessing::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}