#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariablePingTime::VARIABLE_NAME = "ping_time";

namespace {
	// Attributes values
	constexpr const char* AXIS_ATTRIBUTE_VALUE = "T";
	constexpr const char* CALENDAR_ATTRIBUTE_VALUE = "gregorian";
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Time-stamp of each ping";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "time";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "nanoseconds since 1970-01-01 00:00:00Z";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariablePingTime::VariablePingTime(ncObject* parent, ncDimension* timeDimension)
	: VariableSingleDimension<uint64_t>(parent, VARIABLE_NAME, timeDimension)
{
	createVariable();
}

VariablePingTime::~VariablePingTime() {
}

void VariablePingTime::initializeAttributes() {
	createAttribute(VariableAttributesNames::AXIS, AXIS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::CALENDAR, CALENDAR_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}