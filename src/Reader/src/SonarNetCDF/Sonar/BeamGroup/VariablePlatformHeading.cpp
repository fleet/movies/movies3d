#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformHeading.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariablePlatformHeading::VARIABLE_NAME = "platform_heading";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Platform heading(true)";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "platform_orientation";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "degrees_north";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { 0.0f, 360.0f };
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariablePlatformHeading::VariablePlatformHeading(ncObject* parent, VariablePingTime* pingTimeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, pingTimeVariable)
{
	createVariable();
}

VariablePlatformHeading::~VariablePlatformHeading() {
}

void VariablePlatformHeading::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}