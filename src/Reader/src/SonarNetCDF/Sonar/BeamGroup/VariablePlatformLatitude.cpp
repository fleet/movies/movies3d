#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformLatitude.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariablePlatformLatitude::VARIABLE_NAME = "platform_latitude";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "latitude";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "Platform latitude";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "degrees_north";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -90.0, 90.0 };
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariablePlatformLatitude::VariablePlatformLatitude(ncObject* parent, VariablePingTime* pingTimeVariable)
	: VariableSingleVariable<double, uint64_t>(parent, VARIABLE_NAME, pingTimeVariable)
{
	createVariable();
}

VariablePlatformLatitude::~VariablePlatformLatitude() {
}

void VariablePlatformLatitude::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}