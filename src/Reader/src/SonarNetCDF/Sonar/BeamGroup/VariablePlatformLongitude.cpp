#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformLongitude.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariablePlatformLongitude::VARIABLE_NAME = "platform_longitude";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "longitude";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "Platform longitude";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "degrees_east";
	const constexpr double VALID_RANGE_ATTRIBUTE_VALUE[2] = { -180.0, 180.0 };
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariablePlatformLongitude::VariablePlatformLongitude(ncObject* parent, VariablePingTime* pingTimeVariable)
	: VariableSingleVariable<double, uint64_t>(parent, VARIABLE_NAME, pingTimeVariable)
{
	createVariable();
}

VariablePlatformLongitude::~VariablePlatformLongitude() {
}

void VariablePlatformLongitude::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}