#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformPitch.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariablePlatformPitch::VARIABLE_NAME = "platform_pitch";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "pitch angle";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "platform_pitch_angle";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -90.0f, 90.0f };
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariablePlatformPitch::VariablePlatformPitch(ncObject* parent, VariablePingTime* pingTimeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, pingTimeVariable)
{
	createVariable();
}

VariablePlatformPitch::~VariablePlatformPitch() {
}

void VariablePlatformPitch::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}