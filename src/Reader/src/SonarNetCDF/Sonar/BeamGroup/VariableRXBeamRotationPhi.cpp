#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableRXBeamRotationPhi.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableRxBeamRotationPhi::VARIABLE_NAME = "rx_beam_rotation_phi";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "receive beam angular rotation about the x axis";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -180.0f, 180.0f };
}

VariableRxBeamRotationPhi::VariableRxBeamRotationPhi(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

VariableRxBeamRotationPhi::~VariableRxBeamRotationPhi() {
}

void VariableRxBeamRotationPhi::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
