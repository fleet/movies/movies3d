#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableReceiveDurationEffective.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableReceiveDurationEffective::VARIABLE_NAME = "receive_duration_effective";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Effective duration of received pulse";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "s";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableReceiveDurationEffective::VariableReceiveDurationEffective(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, uint64_t, size_t>(parent, VARIABLE_NAME, pingTimeVariable, txBeamVariable)
{
	createVariable();
}

VariableReceiveDurationEffective::~VariableReceiveDurationEffective() {
}

void VariableReceiveDurationEffective::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
