#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableReceiveTransducerIndex.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableReceiveTransducerIndex::VARIABLE_NAME = "receive_transducer_index";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Receive transducer index";
	const constexpr int VALID_MIN_ATTRIBUTE_VALUE = 0;
}

VariableReceiveTransducerIndex::VariableReceiveTransducerIndex(ncObject* parent, VariableBeam* beamVariable)
	: VariableSingleVariable<int, std::string>(parent, VARIABLE_NAME, beamVariable)
{
	createVariable();
}

VariableReceiveTransducerIndex::~VariableReceiveTransducerIndex() {
}

void VariableReceiveTransducerIndex::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
