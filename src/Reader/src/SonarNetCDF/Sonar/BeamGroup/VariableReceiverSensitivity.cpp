#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableReceiverSensitivity.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableReceiverSensitivity::VARIABLE_NAME = "receiver_sensitivity";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Receiver sensitivity";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB re 1/�";
}

VariableReceiverSensitivity::VariableReceiverSensitivity(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

VariableReceiverSensitivity::~VariableReceiverSensitivity() {
}

void VariableReceiverSensitivity::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}
