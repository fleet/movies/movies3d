#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableSampleTimeOffset.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableSampleTimeOffset::VARIABLE_NAME = "sample_time_offset";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Time offset that is subtracted from the timestamp of each sample";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "s";
}

VariableSampleTimeOffset::VariableSampleTimeOffset(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, uint64_t, size_t>(parent, VARIABLE_NAME, pingTimeVariable, txBeamVariable)
{
	createVariable();
}

VariableSampleTimeOffset::~VariableSampleTimeOffset() {
}

void VariableSampleTimeOffset::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}
