#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableSoundSpeedAtTransducer.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableSoundSpeedAtTransducer::VARIABLE_NAME = "sound_speed_at_transducer";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Indicative sound speed at ping time and transducer depth";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "speed_of_sound_in_sea_water";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m/s";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariableSoundSpeedAtTransducer::VariableSoundSpeedAtTransducer(ncObject* parent, VariablePingTime* pingTimeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, pingTimeVariable)
{
	createVariable();
}

VariableSoundSpeedAtTransducer::~VariableSoundSpeedAtTransducer() {
}

void VariableSoundSpeedAtTransducer::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}