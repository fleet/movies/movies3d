#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTXBeamRotationPhi.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableTxBeamRotationPhi::VARIABLE_NAME = "tx_beam_rotation_phi";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "transmit beam angular rotation about the x axis";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -180.0f, 180.0f };
}

VariableTxBeamRotationPhi::VariableTxBeamRotationPhi(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, uint64_t, size_t>(parent, VARIABLE_NAME, pingTimeVariable, txBeamVariable)
{
	createVariable();
}

VariableTxBeamRotationPhi::~VariableTxBeamRotationPhi() {
}

void VariableTxBeamRotationPhi::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
