#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTimeVariedGain.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace beamGroup;

// Variable name
const std::string IVariableTimeVariedGain::VARIABLE_NAME = "time_varied_gain";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Time-varied-gain coefficients";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

template<typename T>
VariableTimeVariedGain<T>::VariableTimeVariedGain(ncObject* parent, VariablePingTime* pingTimeVariable)
	: VariableSingleVariable<T, uint64_t>(parent, VARIABLE_NAME, pingTimeVariable)
{
	this->setTypeName(SampleType::getTypeName());
	this->createVariable();
}

template<typename T>
void VariableTimeVariedGain<T>::initializeAttributes() {
	this->createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	this->createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	this->createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}

std::unique_ptr<IVariableTimeVariedGain> VariableTimeVariedGainFactory::build(const size_t & numberOfSubbeam, ncObject * parent, VariablePingTime* pingTimeVariable) {
	return numberOfSubbeam > 1
		? std::unique_ptr<IVariableTimeVariedGain>(new VariableTimeVariedGain<sonar::SampleType::typeFloat>(parent, pingTimeVariable))
		: std::unique_ptr<IVariableTimeVariedGain>(new VariableTimeVariedGain<sonar::SampleType::typeShort>(parent, pingTimeVariable));
}
