#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransceiverImpedance.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableTransceiverImpedance::VARIABLE_NAME = "transceiver_impedance";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Impedance of transceiver";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "ohm";
}

VariableTransceiverImpedance::VariableTransceiverImpedance(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* subbeamVariable)
	: VariableDoubleVariable<float, uint64_t, size_t>(parent, VARIABLE_NAME, pingTimeVariable, subbeamVariable)
{
	createVariable();
}

void VariableTransceiverImpedance::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}
