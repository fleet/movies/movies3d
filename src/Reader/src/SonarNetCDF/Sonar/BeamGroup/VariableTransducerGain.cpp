#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransducerGain.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GroupBeam.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableFrequency.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string IVariableTransducerGain::VARIABLE_NAME = "transducer_gain";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Gain of transducer";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB";
}

void IVariableTransducerGain::initializeAttributes() {
}

VariableTransducerGain_1::VariableTransducerGain_1(GroupBeam * parent, VariablePingTime * pingTimeVariable, VariableBeam * beamVariable) 
	: VariableDoubleVariable<float, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

float VariableTransducerGain_1::getValueAtIndexes(const std::vector<size_t>& indexes) const {
	return VariableMultiDimension::getValueAtIndexes(indexes);
}

void VariableTransducerGain_1::setValueAtIndexes(const std::vector<size_t>& indexes, const float & value) {
	VariableMultiDimension::setValueAtIndexes(indexes, value);
}

void VariableTransducerGain_1::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

VariableTransducerGain_2::VariableTransducerGain_2(GroupBeam * parent, VariablePingTime * pingTimeVariable, VariableBeam * beamVariable, VariableFrequency * frequencyVariable) 
	: VariableTripleVariable<float, uint64_t, std::string, float>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable, frequencyVariable)
{
	createVariable();
}

float VariableTransducerGain_2::getValueAtIndexes(const std::vector<size_t>& indexes) const {
	return VariableMultiDimension::getValueAtIndexes(indexes);
}

void VariableTransducerGain_2::setValueAtIndexes(const std::vector<size_t>& indexes, const float & value) {
	VariableMultiDimension::setValueAtIndexes(indexes, value);
}

void VariableTransducerGain_2::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}

std::unique_ptr<IVariableTransducerGain> VariableTransducerGainFactory::build(
	GroupBeam * parent, 
	VariablePingTime * pingTimeVariable, 
	VariableBeam * beamVariable, 
	VariableFrequency * frequencyVariable
) {
	if (parent->getVersion() >= VersionSonarNetCDF(2)) {
		return std::unique_ptr<IVariableTransducerGain>(new VariableTransducerGain_2(parent, pingTimeVariable, beamVariable, frequencyVariable));
	}

	return std::unique_ptr<IVariableTransducerGain>(new VariableTransducerGain_1(parent, pingTimeVariable, beamVariable));
}
