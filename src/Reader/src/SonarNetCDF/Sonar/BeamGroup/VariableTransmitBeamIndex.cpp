#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitBeamIndex.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableTransmitBeamIndex::VARIABLE_NAME = "transmit_beam_index";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Transmit beam index associated with the given beam";
	const constexpr int VALID_MIN_ATTRIBUTE_VALUE = 0;
}

VariableTransmitBeamIndex::VariableTransmitBeamIndex(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<int, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

VariableTransmitBeamIndex::~VariableTransmitBeamIndex() {
}

void VariableTransmitBeamIndex::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
