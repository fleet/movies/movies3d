#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitDurationNominal.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableTransmitDurationNominal::VARIABLE_NAME = "transmit_duration_nominal";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Nominal duration of transmitted pulse";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "s";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableTransmitDurationNominal::VariableTransmitDurationNominal(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, uint64_t, size_t>(parent, VARIABLE_NAME, pingTimeVariable, txBeamVariable)
{
	createVariable();
}

VariableTransmitDurationNominal::~VariableTransmitDurationNominal() {
}

void VariableTransmitDurationNominal::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
