#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitPower.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableTransmitPower::VARIABLE_NAME = "transmit_power";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Nominal transmit power";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "W";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableTransmitPower::VariableTransmitPower(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, uint64_t, size_t>(parent, VARIABLE_NAME, pingTimeVariable, txBeamVariable)
{
	createVariable();
}

VariableTransmitPower::~VariableTransmitPower() {
}

void VariableTransmitPower::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
