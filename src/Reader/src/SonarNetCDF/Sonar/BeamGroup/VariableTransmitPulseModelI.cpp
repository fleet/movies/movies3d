#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitPulseModelI.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;
using namespace sonar;

// Variable name
const std::string VariableTransmitPulseModelI::VARIABLE_NAME = "transmit_pulse_model_i";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Imaginary part of the model of the transmit pulse";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = -1.0f;
	const constexpr float VALID_MAX_ATTRIBUTE_VALUE = 1.0f;
}

VariableTransmitPulseModelI::VariableTransmitPulseModelI(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<PulseType::type, uint64_t, size_t>(parent, VARIABLE_NAME, pingTimeVariable, txBeamVariable) 
{
	setTypeName(PulseType::getTypeName());
	createVariable();
}

VariableTransmitPulseModelI::~VariableTransmitPulseModelI() {
}

void VariableTransmitPulseModelI::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MAX, VALID_MAX_ATTRIBUTE_VALUE);
}