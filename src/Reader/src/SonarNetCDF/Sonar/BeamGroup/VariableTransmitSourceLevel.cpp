#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitSourceLevel.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableTransmitSourceLevel::VARIABLE_NAME = "transmit_source_level";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Transmit source level";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB re 1�Pa at 1m";
}

VariableTransmitSourceLevel::VariableTransmitSourceLevel(ncObject* parent, VariablePingTime* pingTimeVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, uint64_t, size_t>(parent, VARIABLE_NAME, pingTimeVariable, txBeamVariable)
{
	createVariable();
}

VariableTransmitSourceLevel::~VariableTransmitSourceLevel() {
}

void VariableTransmitSourceLevel::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}
