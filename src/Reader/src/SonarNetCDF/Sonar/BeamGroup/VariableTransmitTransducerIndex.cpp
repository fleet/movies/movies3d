#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitTransducerIndex.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableTransmitTransducerIndex::VARIABLE_NAME = "transmit_transducer_index";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Transmit transducer index";
	const constexpr int VALID_MIN_ATTRIBUTE_VALUE = 0;
}

VariableTransmitTransducerIndex::VariableTransmitTransducerIndex(ncObject* parent, VariablePingTime* pingTimeVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<int, uint64_t, std::string>(parent, VARIABLE_NAME, pingTimeVariable, beamVariable)
{
	createVariable();
}

VariableTransmitTransducerIndex::~VariableTransmitTransducerIndex() {
}

void VariableTransmitTransducerIndex::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
