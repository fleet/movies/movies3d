#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitterAndReceiverCoefficient.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace beamGroup;

// Variable name
const std::string VariableTransmitterAndReceiverCoefficient::VARIABLE_NAME = "transmitter_and_receiver_coefficient";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Transmitter and receiver coefficient";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB";
}

VariableTransmitterAndReceiverCoefficient::VariableTransmitterAndReceiverCoefficient(ncObject* parent, VariablePingTime* pingTimeVariable)
	: VariableSingleVariable<float, uint64_t>(parent, VARIABLE_NAME, pingTimeVariable)
{
	createVariable();
}

VariableTransmitterAndReceiverCoefficient::~VariableTransmitterAndReceiverCoefficient() {
}

void VariableTransmitterAndReceiverCoefficient::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}