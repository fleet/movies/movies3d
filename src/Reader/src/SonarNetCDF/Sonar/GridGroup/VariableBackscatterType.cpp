#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableBackscatterType.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridFrequency.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Variable name
const std::string VariableBackscatterType::VARIABLE_NAME = "backscatter_type";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Backscatter type for gridded data";
}

VariableBackscatterType::VariableBackscatterType(ncObject* parent, VariableFrequency* frequencyVariable)
	: VariableSingleVariable<uint8_t, float>(parent, VARIABLE_NAME, frequencyVariable)
{
	setTypeName(BeamType::getTypeName());
	createVariable();
}

sonar::BackscatterType::Type VariableBackscatterType::getType(const float& frequency) const {
	return BackscatterType::getType(getValueAt(frequency));
}

void VariableBackscatterType::setType(const float& frequency, sonar::BackscatterType::Type type) {
	setValueAt(frequency, BackscatterType::getValue(type));
}

void VariableBackscatterType::setTypeForAllFrequencies(sonar::BackscatterType::Type type) {
	setAllValuesTo(BackscatterType::getValue(type));
}

void VariableBackscatterType::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
