#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableBeamReference.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridFrequency.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableBeamReference::VARIABLE_NAME = "beam_reference";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Reference to the beam for a given frequency";
}

VariableBeamReference::VariableBeamReference(ncObject* parent, VariableFrequency* frequencyVariable)
	: VariableSingleVariable<std::string, float>(parent, VARIABLE_NAME, frequencyVariable)
{
	createVariable();
}

void VariableBeamReference::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}