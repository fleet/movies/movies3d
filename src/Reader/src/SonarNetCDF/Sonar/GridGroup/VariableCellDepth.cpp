#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableCellDepth.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableCellDepth::VARIABLE_NAME = "cell_depth";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Cell depth below water line";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
}

VariableCellDepth::VariableCellDepth(ncObject* parent, PseudoVariableSingleDimension* rangeAxisVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<double, size_t, std::string>(parent, VARIABLE_NAME, rangeAxisVariable, beamVariable)
{
	createVariable();
}

void VariableCellDepth::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}