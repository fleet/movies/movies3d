#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableCellLongitude.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableCellLongitude::VARIABLE_NAME = "cell_longitude";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "longitude";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "Platform longitude";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "degrees_east";
	const constexpr double VALID_RANGE_ATTRIBUTE_VALUE[2] = { -180.0, 180.0 };
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_axis cell_latitude cell_longitude";
}

VariableCellLongitude::VariableCellLongitude(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* rangeAxisVariable, VariableBeam* beamVariable)
	: VariableTripleVariable<double, size_t, size_t, std::string>(parent, VARIABLE_NAME, pingAxisVariable, rangeAxisVariable, beamVariable)
{
	createVariable();
}

void VariableCellLongitude::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}