#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableBeam::VARIABLE_NAME = "beam";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Beam name";
}

VariableBeam::VariableBeam(ncObject* parent, ncDimension* beamDimension)
	: VariableSingleDimension<std::string>(parent, VARIABLE_NAME, beamDimension)
{
	createVariable();
}

void VariableBeam::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}