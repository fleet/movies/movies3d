#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamStabilisation.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Variable name
const std::string VariableBeamStabilisation::VARIABLE_NAME = "beam_stabilisation";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Beam stabilisation applied (or not)";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_axis platform_latitude platform_longitude";
}

VariableBeamStabilisation::VariableBeamStabilisation(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable)
	: VariableSingleVariable<uint8_t, size_t>(parent, VARIABLE_NAME, pingAxisVariable)
{
	setTypeName(BeamStabilisation::getTypeName());
	createVariable();
}

sonar::BeamStabilisation::Type gridGroup::VariableBeamStabilisation::getModeAt(const size_t & pingAxisIndex) const {
	return sonar::BeamStabilisation::Type(getValueAt(pingAxisIndex));
}

void gridGroup::VariableBeamStabilisation::setModeAt(const size_t & pingAxisIndex, sonar::BeamStabilisation::Type stabilisationMode) {
	setValueAt(pingAxisIndex, sonar::BeamStabilisation::getValue(stabilisationMode));
}

void VariableBeamStabilisation::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}