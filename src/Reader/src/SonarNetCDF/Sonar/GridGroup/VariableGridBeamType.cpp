#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamType.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Variable name
const std::string VariableBeamType::VARIABLE_NAME = "beam_type";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Type of beam";
}

VariableBeamType::VariableBeamType(ncObject* parent, VariableBeam* beamVariable)
	: VariableSingleVariable<uint8_t, std::string>(parent, VARIABLE_NAME, beamVariable)
{
	setTypeName(BeamType::getTypeName());
	createVariable();
}

sonar::BeamType::Type VariableBeamType::getTypeAt(const std::string& beamName) const {
	return BeamType::getType(getValueAt(beamName));
}

void VariableBeamType::setTypeAt(const std::string& beamName, sonar::BeamType::Type type) {
	setValueAt(beamName, BeamType::getValue(type));
}

void VariableBeamType::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
