#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamWidthReceiveMinor.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableBeamWidthReceiveMinor::VARIABLE_NAME = "beamwidth_receive_minor";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Half power one-way receive beam width along minor (vertical) axis of beam";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -0.0f, 360.0f };
}

VariableBeamWidthReceiveMinor::VariableBeamWidthReceiveMinor(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, size_t, std::string>(parent, VARIABLE_NAME, pingAxisVariable, beamVariable)
{
	createVariable();
}

void VariableBeamWidthReceiveMinor::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
