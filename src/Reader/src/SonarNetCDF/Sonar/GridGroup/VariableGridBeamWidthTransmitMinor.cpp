#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamWidthTransmitMinor.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableBeamWidthTransmitMinor::VARIABLE_NAME = "beamwidth_transmit_minor";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Half power one-way transmit beam width along minor (vertical) axis of beam";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -0.0f, 360.0f };
}

VariableBeamWidthTransmitMinor::VariableBeamWidthTransmitMinor(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, size_t, size_t>(parent, VARIABLE_NAME, pingAxisVariable, txBeamVariable)
{
	createVariable();
}

void VariableBeamWidthTransmitMinor::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
