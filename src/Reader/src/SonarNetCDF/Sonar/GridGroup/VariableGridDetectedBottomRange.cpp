#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridDetectedBottomRange.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"

#include <cmath>

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableDetectedBottomRange::VARIABLE_NAME = "detected_bottom_range";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Detected range of the bottom";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableDetectedBottomRange::VariableDetectedBottomRange(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, size_t, std::string>(parent, VARIABLE_NAME, pingAxisVariable, beamVariable)
{
	createVariable();
}

void VariableDetectedBottomRange::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}

void VariableDetectedBottomRange::initializeFillValue() {
	setFillValue(std::nanf(""));
}
