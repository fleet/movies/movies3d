#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridFrequency.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableFrequency::VARIABLE_NAME = "frequency";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Frequency of the receive echo from spectral analysis of the FM pulse or frequency of the CW pulse.";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "Hz";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableFrequency::VariableFrequency(ncObject* parent, ncDimension* frequencyDimension)
	: VariableSingleDimension<float>(parent, VARIABLE_NAME, frequencyDimension)
{
	createVariable();
}

void VariableFrequency::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}