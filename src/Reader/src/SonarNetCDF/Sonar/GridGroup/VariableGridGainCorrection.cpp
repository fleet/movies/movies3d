#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridGainCorrection.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableGainCorrection::VARIABLE_NAME = "gain_correction";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Gain correction";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB";
}

VariableGainCorrection::VariableGainCorrection(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, size_t, std::string>(parent, VARIABLE_NAME, pingAxisVariable, beamVariable)
{
	createVariable();
}

void VariableGainCorrection::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}
