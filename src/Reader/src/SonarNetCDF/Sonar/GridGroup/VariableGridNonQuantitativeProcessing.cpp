#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridNonQuantitativeProcessing.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableNonQuantitativeProcessing::VARIABLE_NAME = "non_quantitative_processing";

namespace {
	// Attribute names
	constexpr const char* FLAG_MEANINGS_ATTRIBUTE_NAME = "flag_meanings";

	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Presence or not of non-quantitative processing applied to the backscattering data (sonar specific)";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_axis platform_latitude platform_longitude";
}

VariableNonQuantitativeProcessing::VariableNonQuantitativeProcessing(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable)
	: VariableSingleVariable<short, size_t>(parent, VARIABLE_NAME, pingAxisVariable)
{
	createVariable();
}

const std::string VariableNonQuantitativeProcessing::getFlagMeanings() {
	return getAttributeValue<std::string>(FLAG_MEANINGS_ATTRIBUTE_NAME);
}

const std::vector<std::string> VariableNonQuantitativeProcessing::getFlagMeaningsList() {
	std::vector<std::string> flagMeaningsList;
	std::string flagMeanings = getFlagMeanings();

	std::string delimiter = " ";
	size_t startIndex = 0;
	size_t foundIndex = flagMeanings.find(" ");
	while (foundIndex != std::string::npos) {
		flagMeaningsList.push_back(flagMeanings.substr(startIndex, foundIndex - startIndex));
		startIndex = foundIndex + delimiter.length();
		foundIndex = flagMeanings.find(delimiter, startIndex);
	}

	return flagMeaningsList;
}

void VariableNonQuantitativeProcessing::setFlagMeanings(const std::string & flagMeanings) {
	setAttributeValue(FLAG_MEANINGS_ATTRIBUTE_NAME, flagMeanings);
}

void VariableNonQuantitativeProcessing::setFlagMeaningsList(const std::vector<std::string>& flagMeaningsList) {
	std::string spaceSeparatedMeanings;
	for (std::string meaning : flagMeaningsList)
		spaceSeparatedMeanings += meaning + " ";
	spaceSeparatedMeanings.pop_back();

	setFlagMeanings(spaceSeparatedMeanings);
}

void VariableNonQuantitativeProcessing::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}