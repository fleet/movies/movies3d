#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridRXBeamRotationPhi.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableRxBeamRotationPhi::VARIABLE_NAME = "rx_beam_rotation_phi";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "receive beam angular rotation about the x axis";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -180.0f, 180.0f };
}

VariableRxBeamRotationPhi::VariableRxBeamRotationPhi(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, size_t, std::string>(parent, VARIABLE_NAME, pingAxisVariable, beamVariable)
{
	createVariable();
}

void VariableRxBeamRotationPhi::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
