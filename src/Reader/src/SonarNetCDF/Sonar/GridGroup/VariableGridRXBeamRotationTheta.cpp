#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridRXBeamRotationTheta.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableRxBeamRotationTheta::VARIABLE_NAME = "rx_beam_rotation_theta";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "receive beam angular rotation about the y axis";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -90.0f, 90.0f };
}

VariableRxBeamRotationTheta::VariableRxBeamRotationTheta(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, size_t, std::string>(parent, VARIABLE_NAME, pingAxisVariable, beamVariable)
{
	createVariable();
}

void VariableRxBeamRotationTheta::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
