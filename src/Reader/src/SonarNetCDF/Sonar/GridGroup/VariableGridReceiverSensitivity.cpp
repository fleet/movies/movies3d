#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridReceiverSensitivity.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableReceiverSensitivity::VARIABLE_NAME = "receiver_sensitivity";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Receiver sensitivity (re 1/�Pa)";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB";
}

VariableReceiverSensitivity::VariableReceiverSensitivity(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, VariableBeam* beamVariable)
	: VariableDoubleVariable<float, size_t, std::string>(parent, VARIABLE_NAME, pingAxisVariable, beamVariable)
{
	createVariable();
}

void VariableReceiverSensitivity::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}
