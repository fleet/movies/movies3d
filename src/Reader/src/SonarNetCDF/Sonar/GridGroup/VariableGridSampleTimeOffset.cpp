#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridSampleTimeOffset.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableSampleTimeOffset::VARIABLE_NAME = "sample_time_offset";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Time offset that is subtracted from the timestamp of each sample";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "s";
}

VariableSampleTimeOffset::VariableSampleTimeOffset(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, size_t, size_t>(parent, VARIABLE_NAME, pingAxisVariable, txBeamVariable)
{
	createVariable();
}

void VariableSampleTimeOffset::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}
