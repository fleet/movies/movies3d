#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTXBeamRotationPsi.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableTxBeamRotationPsi::VARIABLE_NAME = "tx_beam_rotation_psi";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "transmit beam angular rotation about the z axis";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "arc_degree";
	const constexpr float VALID_RANGE_ATTRIBUTE_VALUE[2] = { -180.0f, 180.0f };
}

VariableTxBeamRotationPsi::VariableTxBeamRotationPsi(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, size_t, size_t>(parent, VARIABLE_NAME, pingAxisVariable, txBeamVariable)
{
	createVariable();
}

void VariableTxBeamRotationPsi::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_RANGE, VALID_RANGE_ATTRIBUTE_VALUE);
}
