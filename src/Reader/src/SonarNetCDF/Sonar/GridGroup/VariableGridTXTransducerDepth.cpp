#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTXTransducerDepth.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableTxTransducerDepth::VARIABLE_NAME = "tx_transducer_depth";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Tx transducer depth below waterline";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_axis platform_latitude platform_longitude";
}

VariableTxTransducerDepth::VariableTxTransducerDepth(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable)
	: VariableSingleVariable<float, size_t>(parent, VARIABLE_NAME, pingAxisVariable)
{
	createVariable();
}

void VariableTxTransducerDepth::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}