#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTimeVariedGain.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"
#include "Reader/SonarNetCDF/Sonar/SonarCustomTypes.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Variable name
const std::string IVariableTimeVariedGain::VARIABLE_NAME = "time_varied_gain";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Time-varied-gain coefficients";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_axis platform_latitude platform_longitude";
}

template<typename T>
VariableTimeVariedGain<T>::VariableTimeVariedGain(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable)
	: VariableSingleVariable<T, uint64_t>(parent, VARIABLE_NAME, pingAxisVariable)
{
	VariableSingleVariable<T, uint64_t>::setTypeName(SampleType::getTypeName());
	VariableSingleVariable<T, uint64_t>::createVariable();
}

template<typename T>
void VariableTimeVariedGain<T>::initializeAttributes() {
	VariableSingleVariable<T, uint64_t>::createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	VariableSingleVariable<T, uint64_t>::createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	VariableSingleVariable<T, uint64_t>::createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
}

std::unique_ptr<IVariableTimeVariedGain> VariableTimeVariedGainFactory::build(const size_t & numberOfSubbeam, ncObject * parent, PseudoVariableSingleDimension* pingAxisVariable) {
	return numberOfSubbeam > 1
		? std::unique_ptr<IVariableTimeVariedGain>(new VariableTimeVariedGain<sonar::SampleType::typeFloat>(parent, pingAxisVariable))
		: std::unique_ptr<IVariableTimeVariedGain>(new VariableTimeVariedGain<sonar::SampleType::typeShort>(parent, pingAxisVariable));
}
