#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransducerGain.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GroupBeam.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridFrequency.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableTransducerGain::VARIABLE_NAME = "transducer_gain";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Gain of transducer";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB";
}

VariableTransducerGain::VariableTransducerGain(ncObject * parent, PseudoVariableSingleDimension * pingAxisVariable, VariableFrequency * frequencyVariable)
	: VariableDoubleVariable<float, size_t, float>(parent, VARIABLE_NAME, pingAxisVariable, frequencyVariable)
{
	createVariable();
}

void gridGroup::VariableTransducerGain::setAllGainsForPing(const size_t & pingIndex, const std::vector<float>& gains) {
	for (size_t i = 0; i < gains.size(); ++i) {
		setValueAtIndexes(pingIndex, i, gains[i]);
	}
}

void VariableTransducerGain::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}