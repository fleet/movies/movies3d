#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitBandWidth.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableTransmitBandwith::VARIABLE_NAME = "transmit_bandwidth";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Nominal bandwidth of transmitted pulse";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "Hz";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableTransmitBandwith::VariableTransmitBandwith(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, size_t, size_t>(parent, VARIABLE_NAME, pingAxisVariable, txBeamVariable)
{
	createVariable();
}

void VariableTransmitBandwith::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
