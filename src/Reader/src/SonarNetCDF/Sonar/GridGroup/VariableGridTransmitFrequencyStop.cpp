#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitFrequencyStop.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableTransmitFrequencyStop::VARIABLE_NAME = "transmit_frequency_stop";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Stop frequency in transmitted pulse";
	constexpr const char* STANDARD_NAME_ATTRIBUTE_VALUE = "sound_frequency";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "Hz";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableTransmitFrequencyStop::VariableTransmitFrequencyStop(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, size_t, size_t>(parent, VARIABLE_NAME, pingAxisVariable, txBeamVariable)
{
	createVariable();
}

void VariableTransmitFrequencyStop::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::STANDARD_NAME, STANDARD_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
