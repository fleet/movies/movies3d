#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitPower.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableTransmitPower::VARIABLE_NAME = "transmit_power";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Nominal transmit power";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "W";
	const constexpr float VALID_MIN_ATTRIBUTE_VALUE = 0.0f;
}

VariableTransmitPower::VariableTransmitPower(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, size_t, size_t>(parent, VARIABLE_NAME, pingAxisVariable, txBeamVariable)
{
	createVariable();
}

void VariableTransmitPower::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::VALID_MIN, VALID_MIN_ATTRIBUTE_VALUE);
}
