#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitSourceLevel.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableTransmitSourceLevel::VARIABLE_NAME = "transmit_source_level";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Transmit source level (re 1�Pa at 1m)";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "dB";
}

VariableTransmitSourceLevel::VariableTransmitSourceLevel(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<float, size_t, size_t>(parent, VARIABLE_NAME, pingAxisVariable, txBeamVariable)
{
	createVariable();
}

void VariableTransmitSourceLevel::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}
