#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitType.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Variable name
const std::string VariableTransmitType::VARIABLE_NAME = "transmit_type";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Type of transmitted pulse";
}

VariableTransmitType::VariableTransmitType(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* txBeamVariable)
	: VariableDoubleVariable<uint8_t, size_t, size_t>(parent, VARIABLE_NAME, pingAxisVariable, txBeamVariable)
{
	setTypeName(TransmitType::getTypeName());
	createVariable();
}

VariableTransmitType::~VariableTransmitType() {
}

sonar::TransmitType::Type VariableTransmitType::getTypeAtIndex(const size_t & pingTimeIndex, const size_t & txBeamIndex) const {
	return sonar::TransmitType::getType(getValueAtIndexes(pingTimeIndex, txBeamIndex));
}

void VariableTransmitType::setTypeAtIndex(const size_t & pingTimeIndex, const size_t & txBeamIndex, sonar::TransmitType::Type transmitType) {
	setValueAtIndexes(pingTimeIndex, txBeamIndex, sonar::TransmitType::getValue(transmitType));
}

void VariableTransmitType::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
