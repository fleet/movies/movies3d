#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridWaterlineToChartDatum.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Variable name
const std::string VariableWaterlineToChartDatum::VARIABLE_NAME = "waterline_to_chart_datum";

namespace {
	// Attribute names
	constexpr const char* VERTICAL_COORDINATE_REFERENCE_SYSTEM_ATTRIBUTE_NAME = "vertical_coordinate_reference_system";

	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "vertical translation from waterline to chart datum reference";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "m";
	constexpr const char* COORDINATES_ATTRIBUTE_VALUE = "ping_time platform_latitude platform_longitude";
}

VariableWaterlineToChartDatum::VariableWaterlineToChartDatum(ncObject* parent, PseudoVariableSingleDimension* pingAxisVariable)
	: VariableSingleVariable<float, size_t>(parent, VARIABLE_NAME, pingAxisVariable)
{
	createVariable();
}

VerticalCoordinateReferenceSystem::System VariableWaterlineToChartDatum::getVerticalCoordinateReferenceSystem() const {
	return VerticalCoordinateReferenceSystem::getSystem(
		getAttributeValue<std::string>(VERTICAL_COORDINATE_REFERENCE_SYSTEM_ATTRIBUTE_NAME)
	);
}

void VariableWaterlineToChartDatum::setVerticalCoordinateReferenceSystem(const VerticalCoordinateReferenceSystem::System & system) {
	setAttributeValue(VERTICAL_COORDINATE_REFERENCE_SYSTEM_ATTRIBUTE_NAME, VerticalCoordinateReferenceSystem::getValue(system));
}

void VariableWaterlineToChartDatum::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::COORDINATES, COORDINATES_ATTRIBUTE_VALUE);
	createAttribute(VERTICAL_COORDINATE_REFERENCE_SYSTEM_ATTRIBUTE_NAME, VerticalCoordinateReferenceSystem::getValue(VerticalCoordinateReferenceSystem::System::MSL)); // Valeur par d�faut dans la norme SONAR-netCDF4 - version 2.0
}