#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableIntegratedBackscatter.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridFrequency.h"

using namespace sonarNetCDF;
using namespace gridGroup;

// Variable name
const std::string VariableIntegratedBackscatter::VARIABLE_NAME = "integrated_backscatter";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Integrated backscatter of the raw backscatter measurements sampled in this cell for each frequency.";
	constexpr const char* UNITS_ATTRIBUTE_VALUE = "as appropriate";
}

VariableIntegratedBackscatter::VariableIntegratedBackscatter(ncObject * parent, PseudoVariableSingleDimension* pingAxisVariable, PseudoVariableSingleDimension* rangeAxisVariable, VariableFrequency* frequencyVariable)
	: VariableTripleVariable<float, size_t, size_t, float>(parent, VARIABLE_NAME, pingAxisVariable, rangeAxisVariable, frequencyVariable)
{
	createVariable();
}

void VariableIntegratedBackscatter::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
	createAttribute(VariableAttributesNames::UNITS, UNITS_ATTRIBUTE_VALUE);
}