#include "Reader/SonarNetCDF/Sonar/GridGroup/VariablePingAxisIntervalType.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Variable name
const std::string VariablePingAxisIntervalType::VARIABLE_NAME = "ping_axis_interval_type";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Interval type for regridding the data in ping axis";
}

VariablePingAxisIntervalType::VariablePingAxisIntervalType(ncObject* parent)
	: VariableScalar<uint8_t>(parent, VARIABLE_NAME)
{
	setTypeName(BeamType::getTypeName());
	createVariable();
}

sonar::PingAxisIntervalType::Type VariablePingAxisIntervalType::getType() const {
	return PingAxisIntervalType::getType(getValue());
}

void VariablePingAxisIntervalType::setType(sonar::PingAxisIntervalType::Type type) {
	setValue(PingAxisIntervalType::getValue(type));
}

void VariablePingAxisIntervalType::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
