#include "Reader/SonarNetCDF/Sonar/GridGroup/VariablePingAxisIntervalValue.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Variable name
const std::string VariablePingAxisIntervalValue::VARIABLE_NAME = "ping_axis_interval_value";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Ping axis interval for regridding the data";
}

VariablePingAxisIntervalValue::VariablePingAxisIntervalValue(ncObject* parent)
	: VariableScalar<float>(parent, VARIABLE_NAME)
{
	setTypeName(BeamType::getTypeName());
	createVariable();
}

void VariablePingAxisIntervalValue::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
