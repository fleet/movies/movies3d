#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableRangeAxisIntervalType.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Variable name
const std::string VariableRangeAxisIntervalType::VARIABLE_NAME = "range_axis_interval_type";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Interval type for regridding the data in range axis";
}

VariableRangeAxisIntervalType::VariableRangeAxisIntervalType(ncObject* parent)
	: VariableScalar<uint8_t>(parent, VARIABLE_NAME)
{
	setTypeName(BeamType::getTypeName());
	createVariable();
}

sonar::RangeAxisIntervalType::Type VariableRangeAxisIntervalType::getType() const {
	return RangeAxisIntervalType::getType(getValue());
}

void VariableRangeAxisIntervalType::setType(sonar::RangeAxisIntervalType::Type type) {
	setValue(RangeAxisIntervalType::getValue(type));
}

void VariableRangeAxisIntervalType::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
