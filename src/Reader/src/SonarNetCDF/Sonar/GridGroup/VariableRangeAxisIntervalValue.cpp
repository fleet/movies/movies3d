#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableRangeAxisIntervalValue.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Variable name
const std::string VariableRangeAxisIntervalValue::VARIABLE_NAME = "range_axis_interval_value";

namespace {
	// Attributes values
	constexpr const char* LONG_NAME_ATTRIBUTE_VALUE = "Range axis interval for regridding the data";
}

VariableRangeAxisIntervalValue::VariableRangeAxisIntervalValue(ncObject* parent)
	: VariableScalar<float>(parent, VARIABLE_NAME)
{
	setTypeName(BeamType::getTypeName());
	createVariable();
}

void VariableRangeAxisIntervalValue::initializeAttributes() {
	createAttribute(VariableAttributesNames::LONG_NAME, LONG_NAME_ATTRIBUTE_VALUE);
}
