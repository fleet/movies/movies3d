#include "Reader/SonarNetCDF/Sonar/GroupBeam.h"
#include "Reader/SonarNetCDF/GroupSonar.h"

#include "Reader/SonarNetCDF/VariableAttributesNames.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableFrequency.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterI.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterR.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterSampleCount.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEchoAngleMajor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEchoAngleMajorSensitivity.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEchoAngleMinor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEchoAngleMinorSensitivity.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthReceiveMajor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthReceiveMinor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthTransmitMajor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthTransmitMinor.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableRXBeamRotationPhi.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableRXBeamRotationPsi.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableRXBeamRotationTheta.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTXBeamRotationPhi.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTXBeamRotationPsi.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTXBeamRotationTheta.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamStabilisation.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamType.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEquivalentBeamAngle.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableGainCorrection.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableNonQuantitativeProcessing.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableReceiverSensitivity.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableSampleInterval.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableSampleTimeOffset.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBlankingInterval.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableDetectedBottomRange.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTimeVariedGain.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransducerGain.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransceiverImpedance.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransducerImpedance.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableCalibratedFrequency.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitBandWidth.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitDurationNominal.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableReceiveDurationEffective.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitFrequencyStart.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitFrequencyStop.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitPower.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitSourceLevel.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitterAndReceiverCoefficient.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitType.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitPulseModelI.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitPulseModelR.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableReceiveTransducerIndex.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitTransducerIndex.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitBeamIndex.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableActiveMRU.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableActivePositionSensor.h"

#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableSoundSpeedAtTransducer.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformLatitude.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformLongitude.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformHeading.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformPitch.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformRoll.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformVerticalOffset.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTXTransducerDepth.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableWaterlineToChartDatum.h"

#include <cmath>

using namespace sonarNetCDF;
using namespace beamGroup;
using namespace sonar;

// Group name
const std::string GroupBeam::NC_GROUP_NAME = "Beam_group";

namespace {
	// Dimension names
	constexpr const char* SUBBEAM_DIMENSION_NAME = "subbeam";
	constexpr const char* TX_BEAM_DIMENSION_NAME = "tx_beam";

	// Attribute names
	constexpr const char* BEAM_MODE_ATTRIBUTE_NAME = "beam_mode";
	constexpr const char* CONVERSION_EQUATION_TYPE_ATTRIBUTE_NAME = "conversion_equation_type";
	constexpr const char* PREFERRED_MRU_ATTRIBUTE_NAME = "preferred_MRU";
	constexpr const char* PREFERRED_POSITION_ATTRIBUTE_NAME = "preferred_position";
}

GroupBeam::GroupBeam(
	GroupSonar* const parent, 
	const size_t& beamGroupIndex,
	const size_t& beamCount,
	const size_t& subbeamCount,
	const size_t& frequencyCount,
	const size_t& txBeamCount
)
	: UnderTopLevelGroup(parent, NC_GROUP_NAME + std::to_string(std::max(beamGroupIndex, (size_t)0) + 1)),
	m_beamGroupIndex(std::max(beamGroupIndex, (size_t)0)),
	m_originalSoundSpeed(std::nan("")),
	m_lastSoundSpeed(std::nan(""))
{
	// Cr�ation des dimensions
	m_pingTimeDimension = std::make_unique<ncDimension>(this, VariablePingTime::VARIABLE_NAME, NC_UNLIMITED);
	m_beamDimension = std::make_unique<ncDimension>(this, VariableBeam::VARIABLE_NAME, beamCount);
	m_txBeamDimension = std::make_unique<ncDimension>(this, TX_BEAM_DIMENSION_NAME, txBeamCount != ncUtils::INVALID_SIZE ? txBeamCount : beamCount);
	m_subbeamDimension = std::make_unique<ncDimension>(this, SUBBEAM_DIMENSION_NAME, subbeamCount);
	m_frequencyDimension = std::make_unique<ncDimension>(this, VariableFrequency::VARIABLE_NAME, frequencyCount);

	m_pingTimeVariable = std::make_unique<VariablePingTime>(this, m_pingTimeDimension.get());
	m_beamVariable = std::make_unique<VariableBeam>(this, m_beamDimension.get());
	m_txBeamVariable = std::make_unique<PseudoVariableSingleDimension>(m_txBeamDimension.get());
	m_subbeamVariable = std::make_unique<PseudoVariableSingleDimension>(m_subbeamDimension.get());
	m_frequencyVariable = std::make_unique<VariableFrequency>(this, m_frequencyDimension.get());

	if (isEditable()) {
		// Cr�ation des attributs
		initializeAttributes();

		// Cr�ation des types personnalis�s
		initializeCustomType(m_subbeamDimension->getSize());
	}

	//Cr�ation des variables
	m_activeMRUVariable = std::make_unique<VariableActiveMRU>(this, m_pingTimeVariable.get());
	m_activePositionSensorVariable = std::make_unique<VariableActivePositionSensor>(this, m_pingTimeVariable.get());

	m_backscatterIVariable = VariableBackscatterIFactory::build(m_subbeamDimension->getSize(), this, m_pingTimeVariable.get(), m_beamVariable.get(), m_subbeamVariable.get());
	m_backscatterRVariable = VariableBackscatterRFactory::build(m_subbeamDimension->getSize(), this, m_pingTimeVariable.get(), m_beamVariable.get(), m_subbeamVariable.get());
	m_backscatterSampleCountVariable = std::make_unique<VariableBackscatterSampleCount>(this, m_pingTimeVariable.get(), m_beamVariable.get(), m_subbeamVariable.get());

	m_beamStabilisationVariable = std::make_unique<VariableBeamStabilisation>(this, m_pingTimeVariable.get());
	m_beamTypeVariable = std::make_unique<VariableBeamType>(this);

	m_beamWidthReceiveMajorVariable = std::make_unique<VariableBeamWidthReceiveMajor>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_beamWidthReceiveMinorVariable = std::make_unique<VariableBeamWidthReceiveMinor>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_beamWidthTransmitMajorVariable = std::make_unique<VariableBeamWidthTransmitMajor>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_beamWidthTransmitMinorVariable = std::make_unique<VariableBeamWidthTransmitMinor>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());

	m_blankingIntervalVariable = std::make_unique<VariableBlankingInterval>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_calibratedFrequencyVariable = std::make_unique<VariableCalibratedFrequency>(this, m_frequencyVariable.get());
	m_detectedBottomRangeVariable = std::make_unique<VariableDetectedBottomRange>(this, m_pingTimeVariable.get(), m_beamVariable.get());

	m_echoAngleMajorVariable = std::make_unique<VariableEchoAngleMajor>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_echoAngleMajorSensitivityVariable = std::make_unique<VariableEchoAngleMajorSensitivity>(this, m_beamVariable.get());
	m_echoAngleMinorVariable = std::make_unique<VariableEchoAngleMinor>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_echoAngleMinorSensitivityVariable = std::make_unique<VariableEchoAngleMinorSensitivity>(this, m_beamVariable.get());

	m_equivalentBeamAngleVariable = std::make_unique<VariableEquivalentBeamAngle>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_gainCorrectionVariable = std::make_unique<VariableGainCorrection>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_nonQuantitativeProcessingVariable = std::make_unique<VariableNonQuantitativeProcessing>(this, m_pingTimeVariable.get());

	m_platformHeadingVariable = std::make_unique<VariablePlatformHeading>(this, m_pingTimeVariable.get());
	m_platformLatitudeVariable = std::make_unique<VariablePlatformLatitude>(this, m_pingTimeVariable.get());
	m_platformLongitudeVariable = std::make_unique<VariablePlatformLongitude>(this, m_pingTimeVariable.get());
	m_platformPitchVariable = std::make_unique<VariablePlatformPitch>(this, m_pingTimeVariable.get());
	m_platformRollVariable = std::make_unique<VariablePlatformRoll>(this, m_pingTimeVariable.get());
	m_platformVerticalOffsetVariable = std::make_unique<VariablePlatformVerticalOffset>(this, m_pingTimeVariable.get());

	m_receivedDurationEffectiveVariable = std::make_unique<VariableReceiveDurationEffective>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_receiveTransducerIndexVariable = std::make_unique<VariableReceiveTransducerIndex>(this, m_beamVariable.get());
	m_receiverSensitivityVariable = std::make_unique<VariableReceiverSensitivity>(this, m_pingTimeVariable.get(), m_beamVariable.get());

	m_rxBeamRotationPhiVariable = std::make_unique<VariableRxBeamRotationPhi>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_rxBeamRotationPsiVariable = std::make_unique<VariableRxBeamRotationPsi>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_rxBeamRotationThetaVariable = std::make_unique<VariableRxBeamRotationTheta>(this, m_pingTimeVariable.get(), m_beamVariable.get());

	m_sampleIntervalVariable = std::make_unique<VariableSampleInterval>(this, m_pingTimeVariable.get());
	m_sampleTimeOffsetVariable = std::make_unique<VariableSampleTimeOffset>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_soundSpeedAtTransducerVariable = std::make_unique<VariableSoundSpeedAtTransducer>(this, m_pingTimeVariable.get());

	m_timeVariedGainVariable = VariableTimeVariedGainFactory::build(m_subbeamDimension->getSize(), this, m_pingTimeVariable.get());
	m_transceiverImpedanceVariable = std::make_unique<VariableTransceiverImpedance>(this, m_pingTimeVariable.get(), m_subbeamVariable.get());
	m_transducerGainVariable = VariableTransducerGainFactory::build(this, m_pingTimeVariable.get(), m_beamVariable.get(), m_frequencyVariable.get());
	m_transducerImpedanceVariable = std::make_unique<VariableTransducerImpedance>(this, m_pingTimeVariable.get(), m_subbeamVariable.get());

	m_transmitBandwithVariable = std::make_unique<VariableTransmitBandwith>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_transmitBeamIndexVariable = std::make_unique<VariableTransmitBeamIndex>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_transmitDurationNominalVariable = std::make_unique<VariableTransmitDurationNominal>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_transmitFrequencyStartVariable = std::make_unique<VariableTransmitFrequencyStart>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_transmitFrequencyStopVariable = std::make_unique<VariableTransmitFrequencyStop>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_transmitPowerVariable = std::make_unique<VariableTransmitPower>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_transmitSourceLevelVariable = std::make_unique<VariableTransmitSourceLevel>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_transmitterAndReceiverCoefficientVariable = std::make_unique<VariableTransmitterAndReceiverCoefficient>(this, m_pingTimeVariable.get());
	m_transmitTransducerIndexVariable = std::make_unique<VariableTransmitTransducerIndex>(this, m_pingTimeVariable.get(), m_beamVariable.get());
	m_transmitTypeVariable = std::make_unique<VariableTransmitType>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_transmitPulseModelIVariable = std::make_unique<VariableTransmitPulseModelI>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_transmitPulseModelRVariable = std::make_unique<VariableTransmitPulseModelR>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());

	m_txBeamRotationPhiVariable = std::make_unique<VariableTxBeamRotationPhi>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_txBeamRotationPsiVariable = std::make_unique<VariableTxBeamRotationPsi>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());
	m_txBeamRotationThetaVariable = std::make_unique<VariableTxBeamRotationTheta>(this, m_pingTimeVariable.get(), m_txBeamVariable.get());

	m_txTransducerDepthVariable = std::make_unique<VariableTxTransducerDepth>(this, m_pingTimeVariable.get());
	m_waterlineToChartDatumVariable = std::make_unique<VariableWaterlineToChartDatum>(this, m_pingTimeVariable.get());
}

GroupBeam::~GroupBeam() {
}

GroupSonar * const GroupBeam::getSonarGroup() const {
	return dynamic_cast<GroupSonar*>(getParentGroup());
}

const size_t & GroupBeam::getBeamGroupIndex() const {
	return m_beamGroupIndex;
}

ncDimension * const GroupBeam::getPingTimeDimension() const {
	return m_pingTimeDimension.get();
}

ncDimension * const GroupBeam::getBeamDimension() const {
	return m_beamDimension.get();
}

ncDimension * const GroupBeam::getSubbeamDimension() const {
	return m_subbeamDimension.get();
}

ncDimension * const GroupBeam::getTxBeamDimension() const {
	return m_txBeamDimension.get();
}

BeamMode::Mode GroupBeam::getBeamMode() const {
	return BeamMode::getMode(
		getAttributeValue<std::string>(BEAM_MODE_ATTRIBUTE_NAME)
	);
}

ConversionEquationType::Type GroupBeam::getConversionEquationType() const {
	return ConversionEquationType::getType(
		getAttributeValue<uint8_t>(CONVERSION_EQUATION_TYPE_ATTRIBUTE_NAME)
	);
}

int GroupBeam::getPreferredMru() const {
	return getAttributeValue<int>(PREFERRED_MRU_ATTRIBUTE_NAME);
}

int GroupBeam::getPreferredPosition() const {
	return getAttributeValue<int>(PREFERRED_POSITION_ATTRIBUTE_NAME);
}

void GroupBeam::setBeamMode(const BeamMode::Mode& beamMode) {
	std::string beamModeValue = BeamMode::getValue(beamMode);
	setAttributeValue(BEAM_MODE_ATTRIBUTE_NAME, BeamMode::getValue(beamMode));
}

void GroupBeam::setConversionEquationType(const ConversionEquationType::Type& conversionEquationType) {
	uint8_t conversionEquationTypeValue = ConversionEquationType::getValue(conversionEquationType);
	setAttributeValue(CONVERSION_EQUATION_TYPE_ATTRIBUTE_NAME, conversionEquationTypeValue);

	// On est dans le cas SV - le backscatter R a donc un facteur d'�chelle
	if (conversionEquationType == ConversionEquationType::Type::TYPE_5) {
		m_backscatterRVariable->setScaleFactor(0.01f); // TODO -- utiliser constante !
	}
}

void GroupBeam::setPreferedMru(const int& preferedMru) {
	setAttributeValue(PREFERRED_MRU_ATTRIBUTE_NAME, preferedMru);
}

void GroupBeam::setPreferedPosition(const int& preferedPosition) {
	setAttributeValue(PREFERRED_POSITION_ATTRIBUTE_NAME, preferedPosition);
}

void GroupBeam::setNonQuantitativeProcessingMeanings(const std::vector<std::string> meanings) {
	m_nonQuantitativeProcessingVariable->setFlagMeaningsList(meanings);
}

void GroupBeam::setNonQuantitativeProcessingMeanings(const std::string spaceSeparatedMeanings) {
	m_nonQuantitativeProcessingVariable->setFlagMeanings(spaceSeparatedMeanings);
}

void GroupBeam::setTransducerName(const std::string & transducerName) {
	getBeamVariable()->setTransducerName(transducerName);
}

std::string GroupBeam::getTransducerName() const {
	return getBeamVariable()->getTransducerName();
}

VariableBeam * GroupBeam::getBeamVariable() const {
	return m_beamVariable.get();
}

VariablePingTime * GroupBeam::getPingTimeVariable() const {
	return m_pingTimeVariable.get();
}

VariableFrequency * GroupBeam::getFrequencyVariable() const {
	return m_frequencyVariable.get();
}

IVariableBackscatterI * GroupBeam::getBackscatterIVariable() const {
	return m_backscatterIVariable.get();
}

IVariableBackscatterR * GroupBeam::getBackscatterRVariable() const {
	return m_backscatterRVariable.get();
}

VariableBackscatterSampleCount * GroupBeam::getBackscatterSampleCountVariable() const {
	return m_backscatterSampleCountVariable.get();
}

VariableEchoAngleMajor * GroupBeam::getEchoAngleMajorVariable() const {
	return m_echoAngleMajorVariable.get();
}

VariableEchoAngleMinor * GroupBeam::getEchoAngleMinorVariable() const {
	return m_echoAngleMinorVariable.get();
}

VariableEchoAngleMajorSensitivity * GroupBeam::getEchoAngleMajorSensitivityVariable() const {
	return m_echoAngleMajorSensitivityVariable.get();
}

VariableEchoAngleMinorSensitivity * GroupBeam::getEchoAngleMinorSensitivityVariable() const {
	return m_echoAngleMinorSensitivityVariable.get();
}

VariableBeamWidthReceiveMajor * GroupBeam::getBeamWidthReceiveMajorVariable() const {
	return m_beamWidthReceiveMajorVariable.get();
}

VariableBeamWidthReceiveMinor * GroupBeam::getBeamWidthReceiveMinorVariable() const {
	return m_beamWidthReceiveMinorVariable.get();
}

VariableBeamWidthTransmitMajor * GroupBeam::getBeamWidthTransmitMajorVariable() const {
	return m_beamWidthTransmitMajorVariable.get();
}

VariableBeamWidthTransmitMinor * GroupBeam::getBeamWidthTransmitMinorVariable() const {
	return m_beamWidthTransmitMinorVariable.get();
}

VariableRxBeamRotationPhi * GroupBeam::getRxBeamRotationPhiVariable() const {
	return m_rxBeamRotationPhiVariable.get();
}

VariableRxBeamRotationTheta * GroupBeam::getRxBeamRotationThetaVariable() const {
	return m_rxBeamRotationThetaVariable.get();
}

VariableRxBeamRotationPsi * GroupBeam::getRxBeamRotationPsiVariable() const {
	return m_rxBeamRotationPsiVariable.get();
}

VariableTxBeamRotationPhi * GroupBeam::getTxBeamRotationPhiVariable() const {
	return m_txBeamRotationPhiVariable.get();
}

VariableTxBeamRotationTheta * GroupBeam::getTxBeamRotationThetaVariable() const {
	return m_txBeamRotationThetaVariable.get();
}

VariableTxBeamRotationPsi * GroupBeam::getTxBeamRotationPsiVariable() const {
	return m_txBeamRotationPsiVariable.get();
}

VariableBeamStabilisation * GroupBeam::getBeamStabilisationVariable() const {
	return m_beamStabilisationVariable.get();
}

VariableBeamType * GroupBeam::getBeamTypeVariable() const {
	return m_beamTypeVariable.get();
}

VariableEquivalentBeamAngle * GroupBeam::getEquivalentBeamAngleVariable() const {
	return m_equivalentBeamAngleVariable.get();
}

VariableGainCorrection * GroupBeam::getGainCorrectionVariable() const {
	return m_gainCorrectionVariable.get();
}

VariableNonQuantitativeProcessing * GroupBeam::getNonQuantitativeProcessingVariable() const {
	return m_nonQuantitativeProcessingVariable.get();
}

VariableReceiverSensitivity * GroupBeam::getReceiverSensitivityVariable() const {
	return m_receiverSensitivityVariable.get();
}

VariableSampleInterval * GroupBeam::getSampleIntervalVariable() const {
	return m_sampleIntervalVariable.get();
}

VariableSampleTimeOffset * GroupBeam::getSampleTimeOffsetVariable() const {
	return m_sampleTimeOffsetVariable.get();
}

VariableBlankingInterval * GroupBeam::getBlankingIntervalVariable() const {
	return m_blankingIntervalVariable.get();
}

VariableDetectedBottomRange * GroupBeam::getDetectedBottomRangeVariable() const {
	return m_detectedBottomRangeVariable.get();
}

IVariableTimeVariedGain * GroupBeam::getIVariableTimeVariedGainVariable() const {
	return m_timeVariedGainVariable.get();
}

IVariableTransducerGain * GroupBeam::getTransducerGainVariable() const {
	return m_transducerGainVariable.get();
}

VariableTransceiverImpedance * GroupBeam::getTransceiverImpedanceVariable() const {
	return m_transceiverImpedanceVariable.get();
}

VariableTransducerImpedance * GroupBeam::getTransducerImpedanceVariable() const {
	return m_transducerImpedanceVariable.get();
}

VariableCalibratedFrequency * GroupBeam::getCalibratedFrequencyVariable() const {
	return m_calibratedFrequencyVariable.get();
}

VariableTransmitBandwith * GroupBeam::getTransmitBandwithVariable() const {
	return m_transmitBandwithVariable.get();
}

VariableTransmitDurationNominal * GroupBeam::getTransmitDurationNominalVariable() const {
	return m_transmitDurationNominalVariable.get();
}

VariableReceiveDurationEffective * GroupBeam::getReceiveDurationEffectiveVariable() const {
	return m_receivedDurationEffectiveVariable.get();
}

VariableTransmitFrequencyStart * GroupBeam::getTransmitFrequencyStartVariable() const {
	return m_transmitFrequencyStartVariable.get();
}

VariableTransmitFrequencyStop * GroupBeam::getTransmitFrequencyStopVariable() const {
	return m_transmitFrequencyStopVariable.get();
}

VariableTransmitPower * GroupBeam::getTransmitPowerVariable() const {
	return m_transmitPowerVariable.get();
}

VariableTransmitSourceLevel * GroupBeam::getTransmitSourceLevelVariable() const {
	return m_transmitSourceLevelVariable.get();
}

VariableTransmitterAndReceiverCoefficient * GroupBeam::getTransmitterAndReceiverCoefficientVariable() const {
	return m_transmitterAndReceiverCoefficientVariable.get();
}

VariableTransmitType * GroupBeam::getTransmitTypeVariable() const {
	return m_transmitTypeVariable.get();
}

VariableTransmitPulseModelI * GroupBeam::getTransmitPulseModelIVariable() const {
	return m_transmitPulseModelIVariable.get();
}

VariableTransmitPulseModelR * GroupBeam::getTransmitPulseModelRVariable() const {
	return m_transmitPulseModelRVariable.get();
}

VariableReceiveTransducerIndex * GroupBeam::getReceiveTransducerIndexVariable() const {
	return m_receiveTransducerIndexVariable.get();
}

VariableTransmitTransducerIndex * GroupBeam::getTransmitTransducerIndexVariable() const {
	return m_transmitTransducerIndexVariable.get();
}

VariableTransmitBeamIndex * GroupBeam::getTransmitBeamIndexVariable() const {
	return m_transmitBeamIndexVariable.get();
}

VariableActiveMRU * GroupBeam::getActiveMRUVariable() const {
	return m_activeMRUVariable.get();
}

VariableActivePositionSensor * GroupBeam::getActivePositionSensorVariable() const {
	return m_activePositionSensorVariable.get();
}

VariableSoundSpeedAtTransducer * GroupBeam::getSoundSpeedAtTransducerVariable() const {
	return m_soundSpeedAtTransducerVariable.get();
}

VariablePlatformLatitude * GroupBeam::getPlatformLatitudeVariable() const {
	return m_platformLatitudeVariable.get();
}

VariablePlatformLongitude * GroupBeam::getPlatformLongitudeVariable() const {
	return m_platformLongitudeVariable.get();
}

VariablePlatformHeading * GroupBeam::getPlatformHeadingVariable() const {
	return m_platformHeadingVariable.get();
}

VariablePlatformPitch * GroupBeam::getPlatformPitchVariable() const {
	return m_platformPitchVariable.get();
}

VariablePlatformRoll * GroupBeam::getPlatformRollVariable() const {
	return m_platformRollVariable.get();
}

VariablePlatformVerticalOffset * GroupBeam::getPlatformVerticalOffsetVariable() const {
	return m_platformVerticalOffsetVariable.get();
}

VariableTxTransducerDepth * GroupBeam::getTxTransducerDepthVariable() const {
	return m_txTransducerDepthVariable.get();
}

VariableWaterlineToChartDatum * GroupBeam::getWaterlineToChartDatumVariable() const {
	return m_waterlineToChartDatumVariable.get();
}

void GroupBeam::initializeAttributes() {
	// D�finition de l'attribute Beam Mode
	// Dans tous les cas, on consid�re ici que le mode des faisceaux est forc�ment VERTICAL
	createAttribute(BEAM_MODE_ATTRIBUTE_NAME, BeamMode::getValue(BeamMode::Mode::VERTICAL));

	// D�finition de des attributs Preferred MRU / POSITION
	createAttribute(PREFERRED_MRU_ATTRIBUTE_NAME, (int)m_beamGroupIndex); // TODO - valeur variable ????
	createAttribute(PREFERRED_POSITION_ATTRIBUTE_NAME, 0); // TODO - valeur variable ????

	// attributs � remplir
	createEmptyAttribute(CONVERSION_EQUATION_TYPE_ATTRIBUTE_NAME);
}

void GroupBeam::initializeCustomType(const size_t& nbSubbeam) {
	// Cr�ation des types personnalis�s
	// --- Sample_t
	if (nbSubbeam == 1) {
		createArrayType<short>(SampleType::getTypeName());
	}
	else {
		createArrayType<float>(SampleType::getTypeName());
	}

	// --- Angle_t
	createArrayType<short>(AngleType::getTypeName());

	// --- Pulse_t
	createArrayType<float>(PulseType::getTypeName());
}
