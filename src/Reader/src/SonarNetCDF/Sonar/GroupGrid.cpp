#include "Reader/SonarNetCDF/Sonar/GroupGrid.h"
#include "Reader/SonarNetCDF/GroupSonar.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridFrequency.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableBeamReference.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableBackscatterType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariablePingAxisIntervalType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariablePingAxisIntervalValue.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableRangeAxisIntervalType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableRangeAxisIntervalValue.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableCellPingTime.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableIntegratedBackscatter.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamWidthReceiveMajor.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamWidthReceiveMinor.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamWidthTransmitMajor.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamWidthTransmitMinor.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridRXBeamRotationPsi.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridRXBeamRotationPhi.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridRXBeamRotationTheta.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTXBeamRotationPsi.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTXBeamRotationPhi.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTXBeamRotationTheta.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamStabilisation.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridEquivalentBeamAngle.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridGainCorrection.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridNonQuantitativeProcessing.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridReceiverSensitivity.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridSampleInterval.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridSampleTimeOffset.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBlankingInterval.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridDetectedBottomRange.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTimeVariedGain.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransducerGain.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitBandWidth.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitDurationNominal.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridReceiveDurationEffective.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitFrequencyStart.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitFrequencyStop.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitPower.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitSourceLevel.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridReceiveTransducerIndex.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridSoundSpeedAtTransducer.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableCellLatitude.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableCellLongitude.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableCellDepth.h"

#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTXTransducerDepth.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridWaterlineToChartDatum.h"


using namespace sonarNetCDF;
using namespace sonar;
using namespace gridGroup;

// Group name
const std::string GroupGrid::NC_GROUP_NAME = "Grid_group";

namespace {
	// Dimension names
	const constexpr char* TX_BEAM_DIMENSION_NAME = "tx_beam";
	const constexpr char* PING_AXIS_DIMENSION_NAME = "ping_axis";
	const constexpr char* RANGE_AXIS_DIMENSION_NAME = "range_axis";

	// Attribute names
	const constexpr char* BEAM_MODE_ATTRIBUTE_NAME = "beam_mode";
	const constexpr char* CONVERSION_EQUATION_TYPE_ATTRIBUTE_NAME = "conversion_equation_type";
}

GroupGrid::GroupGrid(
	GroupSonar* const parent,
	const size_t& gridGroupIndex,
	const std::string& layerTypeName,
	const size_t& beamCount,
	const size_t& subbeamCount,
	const size_t& frequencyCount,
	const size_t& rangeAxisCount,
	const size_t& pingAxisCount,
	const size_t& txBeamCount
)
	: UnderTopLevelGroup(parent, NC_GROUP_NAME + std::to_string(std::max(gridGroupIndex, (size_t)0) + 1)),
	m_layerTypeName(layerTypeName),
	m_gridGroupIndex(std::max(gridGroupIndex, (size_t)0))
{
	// Cr�ation des dimensions
	m_beamDimension = std::make_unique<ncDimension>(this, VariableBeam::VARIABLE_NAME, beamCount);
	m_frequencyDimension = std::make_unique<ncDimension>(this, VariableFrequency::VARIABLE_NAME, frequencyCount);
	m_txBeamDimension = std::make_unique<ncDimension>(this, TX_BEAM_DIMENSION_NAME, txBeamCount != ncUtils::INVALID_SIZE ? txBeamCount : beamCount);
	m_pingAxisDimension = std::make_unique<ncDimension>(this, PING_AXIS_DIMENSION_NAME, pingAxisCount);
	m_rangeAxisDimension = std::make_unique<ncDimension>(this, RANGE_AXIS_DIMENSION_NAME, rangeAxisCount);

	m_beamVariable = std::make_unique<VariableBeam>(this, m_beamDimension.get());
	m_frequencyVariable = std::make_unique<VariableFrequency>(this, m_frequencyDimension.get());
	m_txBeamVariable = std::make_unique<PseudoVariableSingleDimension>(m_txBeamDimension.get());
	m_pingAxisVariable = std::make_unique<PseudoVariableSingleDimension>(m_pingAxisDimension.get());
	m_rangeAxisVariable = std::make_unique<PseudoVariableSingleDimension>(m_rangeAxisDimension.get());

	if (isEditable()) {
		// Cr�ation des attributs
		initializeAttributes();

		// Cr�ation des types personnalis�s
		initializeCustomType(subbeamCount);
	}

	//Cr�ation des variables
	m_beamReferenceVariable = std::make_unique<VariableBeamReference>(this, m_frequencyVariable.get());
	m_backscatterTypeVariable = std::make_unique<VariableBackscatterType>(this, m_frequencyVariable.get());
	m_pingAxisIntervalTypeVariable = std::make_unique<VariablePingAxisIntervalType>(this);
	m_pingAxisIntervalValueVariable = std::make_unique<VariablePingAxisIntervalValue>(this);
	m_rangeAxisIntervalTypeVariable = std::make_unique<VariableRangeAxisIntervalType>(this);
	m_rangeAxisIntervalValueVariable = std::make_unique<VariableRangeAxisIntervalValue>(this);

	m_cellPingTimeVariable = std::make_unique<VariableCellPingTime>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_integratedBackscatterVariable = std::make_unique<VariableIntegratedBackscatter>(this, m_pingAxisVariable.get(), m_rangeAxisVariable.get(), m_frequencyVariable.get());

	m_beamWidthReceiveMajorVariable = std::make_unique<VariableBeamWidthReceiveMajor>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_beamWidthReceiveMinorVariable = std::make_unique<VariableBeamWidthReceiveMinor>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_beamWidthTransmitMajorVariable = std::make_unique<VariableBeamWidthTransmitMajor>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_beamWidthTransmitMinorVariable = std::make_unique<VariableBeamWidthTransmitMinor>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());

	m_rxBeamRotationPhiVariable = std::make_unique<VariableRxBeamRotationPhi>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_rxBeamRotationPsiVariable = std::make_unique<VariableRxBeamRotationPsi>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_rxBeamRotationThetaVariable = std::make_unique<VariableRxBeamRotationTheta>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_txBeamRotationPhiVariable = std::make_unique<VariableTxBeamRotationPhi>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_txBeamRotationPsiVariable = std::make_unique<VariableTxBeamRotationPsi>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_txBeamRotationThetaVariable = std::make_unique<VariableTxBeamRotationTheta>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());

	m_beamStabilisationVariable = std::make_unique<VariableBeamStabilisation>(this, m_pingAxisVariable.get());
	m_beamTypeVariable = std::make_unique<VariableBeamType>(this, m_beamVariable.get());
	m_equivalentBeamAngleVariable = std::make_unique<VariableEquivalentBeamAngle>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_gainCorrectionVariable = std::make_unique<VariableGainCorrection>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_nonQuantitativeProcessingVariable = std::make_unique<VariableNonQuantitativeProcessing>(this, m_pingAxisVariable.get());
	m_receiverSensitivityVariable = std::make_unique<VariableReceiverSensitivity>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_sampleIntervalVariable = std::make_unique<VariableSampleInterval>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_sampleTimeOffsetVariable = std::make_unique<VariableSampleTimeOffset>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_blankingIntervalVariable = std::make_unique<VariableBlankingInterval>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_detectedBottomRangeVariable = std::make_unique<VariableDetectedBottomRange>(this, m_pingAxisVariable.get(), m_beamVariable.get());
	m_timeVariedGainVariable = VariableTimeVariedGainFactory::build(subbeamCount, this, m_pingAxisVariable.get());
	m_transducerGainVariable = std::make_unique<VariableTransducerGain>(this, m_pingAxisVariable.get(), m_frequencyVariable.get());
	m_transmitBandwidthVariable = std::make_unique<VariableTransmitBandwith>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_transmitDurationNominalVariable = std::make_unique<VariableTransmitDurationNominal>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_transmitTypeVariable = std::make_unique<VariableTransmitType>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_receiveDurationEffectiveVariable = std::make_unique<VariableReceiveDurationEffective>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());

	m_transmitFrequencyStartVariable = std::make_unique<VariableTransmitFrequencyStart>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_transmitFrequencyStopVariable = std::make_unique<VariableTransmitFrequencyStop>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	
	m_transmitPowerVariable = std::make_unique<VariableTransmitPower>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_transmitSourceLevelVariable = std::make_unique<VariableTransmitSourceLevel>(this, m_pingAxisVariable.get(), m_txBeamVariable.get());
	m_receiveTransducerIndexVariable = std::make_unique<VariableReceiveTransducerIndex>(this, m_beamVariable.get());
	m_soundSpeedAtTransducerVariable = std::make_unique<VariableSoundSpeedAtTransducer>(this, m_pingAxisVariable.get());

	m_cellLatitudeVariable = std::make_unique<VariableCellLatitude>(this, m_pingAxisVariable.get(), m_rangeAxisVariable.get(), m_beamVariable.get());
	m_cellLongitudeVariable = std::make_unique<VariableCellLongitude>(this, m_pingAxisVariable.get(), m_rangeAxisVariable.get(), m_beamVariable.get());
	m_cellDepthVariable = std::make_unique<VariableCellDepth>(this, m_rangeAxisVariable.get(), m_beamVariable.get());

	m_txTransducerDepthVariable = std::make_unique<VariableTxTransducerDepth>(this, m_pingAxisVariable.get());
	m_waterlineToChartDatumVariable = std::make_unique<VariableWaterlineToChartDatum>(this, m_pingAxisVariable.get());
}

GroupGrid::~GroupGrid() {
}

GroupSonar * const GroupGrid::getSonarGroup() const {
	return dynamic_cast<GroupSonar*>(getParentGroup()); 
}

const size_t & GroupGrid::getGridGroupIndex() const {
	return m_gridGroupIndex;
}

const std::string & GroupGrid::getLayerTypeName() const {
	return m_layerTypeName;
}

ncDimension * const GroupGrid::getBeamDimension() const {
	return m_beamDimension.get();
}

ncDimension * const GroupGrid::getFrequencyDimension() const {
	return m_frequencyDimension.get();
}

ncDimension * const GroupGrid::getTxBeamDimension() const {
	return m_txBeamDimension.get();
}

ncDimension * const GroupGrid::getPingAxisDimension() const {
	return m_pingAxisDimension.get();
}

ncDimension * const GroupGrid::getRangeAxisDimension() const {
	return m_rangeAxisDimension.get();
}

sonar::BeamMode::Mode GroupGrid::getBeamMode() const {
	return BeamMode::getMode(
		getAttributeValue<std::string>(BEAM_MODE_ATTRIBUTE_NAME)
	);
}

sonar::ConversionEquationType::Type GroupGrid::getConversionEquationType() const {
	return ConversionEquationType::getType(
		getAttributeValue<uint8_t>(CONVERSION_EQUATION_TYPE_ATTRIBUTE_NAME)
	);
}

void GroupGrid::setBeamMode(const sonar::BeamMode::Mode & beamMode) {
	std::string beamModeValue = BeamMode::getValue(beamMode);
	setAttributeValue(BEAM_MODE_ATTRIBUTE_NAME, BeamMode::getValue(beamMode));
}

void GroupGrid::setConversionEquationType(const sonar::ConversionEquationType::Type & conversionEquationType) {
	uint8_t conversionEquationTypeValue = ConversionEquationType::getValue(conversionEquationType);
	setAttributeValue(CONVERSION_EQUATION_TYPE_ATTRIBUTE_NAME, conversionEquationTypeValue);
}

gridGroup::VariableBeam * GroupGrid::getBeamVariable() const {
	return m_beamVariable.get();
}

gridGroup::VariableFrequency * GroupGrid::getFrequencyVariable() const {
	return m_frequencyVariable.get();
}

gridGroup::VariableBeamReference * GroupGrid::getBeamReferenceVariable() const {
	return m_beamReferenceVariable.get();
}

gridGroup::VariableBackscatterType * GroupGrid::getBackscatterTypeVariable() const {
	return m_backscatterTypeVariable.get();
}

gridGroup::VariablePingAxisIntervalType * GroupGrid::getPingAxisIntervalType() const {
	return m_pingAxisIntervalTypeVariable.get();
}

gridGroup::VariablePingAxisIntervalValue * GroupGrid::getPingAxisIntervalValue() const {
	return m_pingAxisIntervalValueVariable.get();
}

gridGroup::VariableRangeAxisIntervalType * GroupGrid::getRangeAxisIntervalType() const {
	return m_rangeAxisIntervalTypeVariable.get();
}

gridGroup::VariableRangeAxisIntervalValue * GroupGrid::getRangeAxisIntervalValue() const {
	return m_rangeAxisIntervalValueVariable.get();
}

gridGroup::VariableCellPingTime * GroupGrid::getCellPingTimeVariable() const {
	return m_cellPingTimeVariable.get();
}

gridGroup::VariableIntegratedBackscatter * GroupGrid::getIntegratedBackscatterVariable() const {
	return m_integratedBackscatterVariable.get();
}

gridGroup::VariableBeamWidthReceiveMajor * GroupGrid::getBeamWidthReceiveMajorVariable() const {
	return m_beamWidthReceiveMajorVariable.get();
}

gridGroup::VariableBeamWidthReceiveMinor * GroupGrid::getBeamWidthReceiveMinorVariable() const {
	return m_beamWidthReceiveMinorVariable.get();
}

gridGroup::VariableBeamWidthTransmitMajor * GroupGrid::getBeamWidthTransmitMajorVariable() const {
	return m_beamWidthTransmitMajorVariable.get();
}

gridGroup::VariableBeamWidthTransmitMinor * GroupGrid::getBeamWidthTransmitMinorVariable() const {
	return m_beamWidthTransmitMinorVariable.get();
}

gridGroup::VariableRxBeamRotationPhi * GroupGrid::getRxBeamRotationPhiVariable() const {
	return m_rxBeamRotationPhiVariable.get();
}

gridGroup::VariableRxBeamRotationPsi * GroupGrid::getRxBeamRotationPsiVariable() const {
	return m_rxBeamRotationPsiVariable.get();
}

gridGroup::VariableRxBeamRotationTheta * GroupGrid::getRxBeamRotationThetaVariable() const {
	return m_rxBeamRotationThetaVariable.get();
}

gridGroup::VariableTxBeamRotationPhi * GroupGrid::getTxBeamRotationPhiVariable() const {
	return m_txBeamRotationPhiVariable.get();
}

gridGroup::VariableTxBeamRotationPsi * GroupGrid::getTxBeamRotationPsiVariable() const {
	return m_txBeamRotationPsiVariable.get();
}

gridGroup::VariableTxBeamRotationTheta * GroupGrid::getTxBeamRotationThetaVariable() const {
	return m_txBeamRotationThetaVariable.get();
}

gridGroup::VariableBeamStabilisation * GroupGrid::getBeamStabilisationVariable() const {
	return m_beamStabilisationVariable.get();
}

gridGroup::VariableBeamType * GroupGrid::getBeamTypeVariable() const {
	return m_beamTypeVariable.get();
}

gridGroup::VariableEquivalentBeamAngle * GroupGrid::getEquivalentBeamAngleVariable() const {
	return m_equivalentBeamAngleVariable.get();
}

gridGroup::VariableGainCorrection * GroupGrid::getGainCorrectionVariable() const {
	return m_gainCorrectionVariable.get();
}

gridGroup::VariableNonQuantitativeProcessing * GroupGrid::getNonQuantitativeProcessingVariable() const {
	return m_nonQuantitativeProcessingVariable.get();
}

gridGroup::VariableReceiverSensitivity * GroupGrid::getReceiverSensitivityVariable() const {
	return m_receiverSensitivityVariable.get();
}

gridGroup::VariableSampleInterval * GroupGrid::getSampleIntervalVariable() const {
	return m_sampleIntervalVariable.get();
}

gridGroup::VariableSampleTimeOffset * GroupGrid::getSampleTimeOffsetVariable() const {
	return m_sampleTimeOffsetVariable.get();
}

gridGroup::VariableBlankingInterval * GroupGrid::getBlankingIntervalVariable() const {
	return m_blankingIntervalVariable.get();
}

gridGroup::VariableDetectedBottomRange * GroupGrid::getDetectedBottomRangeVariable() const {
	return m_detectedBottomRangeVariable.get();
}

gridGroup::IVariableTimeVariedGain * GroupGrid::getTimeVariedGainVariable() const {
	return m_timeVariedGainVariable.get();
}

gridGroup::VariableTransducerGain * GroupGrid::getTransducerGainVariable() const {
	return m_transducerGainVariable.get();
}

gridGroup::VariableTransmitBandwith * GroupGrid::getTransmitBandwidthVariable() const {
	return m_transmitBandwidthVariable.get();
}

gridGroup::VariableTransmitDurationNominal * GroupGrid::getTransmitDurationNominalVariable() const {
	return m_transmitDurationNominalVariable.get();
}

gridGroup::VariableTransmitType * sonarNetCDF::GroupGrid::getTransmitTypeVariable() const {
	return m_transmitTypeVariable.get();
}

gridGroup::VariableReceiveDurationEffective * GroupGrid::getReceiveDurationEffectiveVariable() const {
	return m_receiveDurationEffectiveVariable.get();
}

gridGroup::VariableTransmitFrequencyStart * GroupGrid::getTransmitFrequencyStartVariable() const {
	return m_transmitFrequencyStartVariable.get();
}

gridGroup::VariableTransmitFrequencyStop * GroupGrid::getTransmitFrequencyStopVariable() const {
	return m_transmitFrequencyStopVariable.get();
}

gridGroup::VariableTransmitPower * GroupGrid::getTransmitPowerVariable() const {
	return m_transmitPowerVariable.get();
}

gridGroup::VariableTransmitSourceLevel * GroupGrid::getTransmitSourceLevelVariable() const {
	return m_transmitSourceLevelVariable.get();
}

gridGroup::VariableReceiveTransducerIndex * GroupGrid::getReceiveTransducerIndexVariable() const {
	return m_receiveTransducerIndexVariable.get();
}

gridGroup::VariableSoundSpeedAtTransducer * GroupGrid::getSoundSpeedAtTransducerVariable() const {
	return m_soundSpeedAtTransducerVariable.get();
}

gridGroup::VariableCellLatitude * GroupGrid::getCellLatitudeVariable() const {
	return m_cellLatitudeVariable.get();
}

gridGroup::VariableCellLongitude * GroupGrid::getCellLongitudeVariable() const {
	return m_cellLongitudeVariable.get();
}

gridGroup::VariableCellDepth * GroupGrid::getCellDepthVariable() const {
	return m_cellDepthVariable.get();
}

gridGroup::VariableTxTransducerDepth * GroupGrid::getTxTransducerDepthVariable() const {
	return m_txTransducerDepthVariable.get();
}

gridGroup::VariableWaterlineToChartDatum * GroupGrid::getWaterlineToChartDatumVariable() const {
	return m_waterlineToChartDatumVariable.get();
}

void GroupGrid::initializeAttributes() {
	// D�finition de l'attribute Beam Mode
	// Dans tous les cas, on consid�re ici que le mode des faisceaux est forc�ment VERTICAL
	createAttribute(BEAM_MODE_ATTRIBUTE_NAME, BeamMode::getValue(BeamMode::Mode::VERTICAL));

	// attributs � remplir
	createEmptyAttribute(CONVERSION_EQUATION_TYPE_ATTRIBUTE_NAME);
}

void GroupGrid::initializeCustomType(const size_t & nbSubbeam) {
	// Cr�ation des types personnalis�s
	// --- Sample_t
	if (nbSubbeam == 1) {
		createArrayType<short>(SampleType::getTypeName());
	}
	else {
		createArrayType<float>(SampleType::getTypeName());
	}

	// Backscatter Type
	const nc_type backscatterTypeType = createEnumType<uint8_t>(BackscatterType::getTypeName());
	for (BackscatterType::Type type : BackscatterType::all()) {
		addEnum(backscatterTypeType, BackscatterType::getName(type), BackscatterType::getValue(type));
	}

	// Range Axis Interval Type
	const nc_type rangeAxisIntervalTypeType = createEnumType<uint8_t>(RangeAxisIntervalType::getTypeName());
	for (RangeAxisIntervalType::Type type : RangeAxisIntervalType::all()) {
		addEnum(rangeAxisIntervalTypeType, RangeAxisIntervalType::getName(type), RangeAxisIntervalType::getValue(type));
	}

	// Ping Axis Interval Type
	const nc_type pingAxisIntervalTypeType = createEnumType<uint8_t>(PingAxisIntervalType::getTypeName());
	for (PingAxisIntervalType::Type type : PingAxisIntervalType::all()) {
		addEnum(pingAxisIntervalTypeType, PingAxisIntervalType::getName(type), PingAxisIntervalType::getValue(type));
	}
}
