#include "Reader/SonarNetCDF/VariableAttributesNames.h"

using namespace sonarNetCDF;

const std::string VariableAttributesNames::LONG_NAME = "long_name";
const std::string VariableAttributesNames::STANDARD_NAME = "standard_name";
const std::string VariableAttributesNames::UNITS = "units";
const std::string VariableAttributesNames::VALID_MIN = "valid_min";
const std::string VariableAttributesNames::VALID_MAX = "valid_max";
const std::string VariableAttributesNames::VALID_RANGE = "valid_range";
const std::string VariableAttributesNames::SCALE_FACTOR = "scale_factor";
const std::string VariableAttributesNames::AXIS = "axis";
const std::string VariableAttributesNames::CALENDAR = "calendar";
const std::string VariableAttributesNames::COORDINATES = "coordinates";
const std::string VariableAttributesNames::CF_ROLE = "cf_role";
const std::string VariableAttributesNames::MISSING_VALUE = "missing_value";