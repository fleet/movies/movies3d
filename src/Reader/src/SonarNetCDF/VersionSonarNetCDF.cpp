#include "Reader/SonarNetCDF/VersionSonarNetCDF.h"

#include <stdexcept>
#include <vector>

namespace {
	constexpr const char* VERSION_DELIMITER = ".";
}

using namespace sonarNetCDF;

VersionSonarNetCDF::VersionSonarNetCDF(const unsigned short & major, const unsigned short & minor) : m_major(major), m_minor(minor) {
}

VersionSonarNetCDF::VersionSonarNetCDF(const std::string & version) {
	auto strVersion = version;
	std::vector<unsigned short> versionNumbers;

	// Parse string
	try {
		size_t pos = 0;
		while ((pos = strVersion.find(VERSION_DELIMITER)) != std::string::npos) {
			versionNumbers.push_back(std::stoi(strVersion.substr(0, pos)));
			strVersion.erase(0, pos + 1);
		}
		versionNumbers.push_back(std::stoi(strVersion.substr(0, pos)));
	}
	catch (std::invalid_argument const& ex) {
		return;
	}

	if (versionNumbers.size() >= 1) {
		m_major = versionNumbers.at(0);
	}

	if (versionNumbers.size() >= 2) {
		m_minor = versionNumbers.at(1);
	}
}

const unsigned short & VersionSonarNetCDF::getMajor() const {
	return m_major;
}

const unsigned short & VersionSonarNetCDF::getMinor() const {
	return m_minor;
}

std::string VersionSonarNetCDF::toString() const {
	return std::to_string(m_major) + VERSION_DELIMITER + std::to_string(m_minor);
}

bool VersionSonarNetCDF::operator==(const VersionSonarNetCDF & other) const {
	return m_major == other.m_major && m_minor == other.m_minor;
}

bool VersionSonarNetCDF::operator>(const VersionSonarNetCDF & other) const {
	return m_major > other.m_major || (m_major == other.m_major && m_minor > other.m_minor);
}

bool VersionSonarNetCDF::operator<(const VersionSonarNetCDF & other) const {
	return m_major < other.m_major || (m_major == other.m_major && m_minor < other.m_minor);
}

bool VersionSonarNetCDF::operator>=(const VersionSonarNetCDF & other) const {
	return !operator<(other);
}

bool VersionSonarNetCDF::operator<=(const VersionSonarNetCDF & other) const {
	return !operator>(other);
}
