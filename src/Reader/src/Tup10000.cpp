/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup10000.cpp												  */
/******************************************************************************/

#include "Reader/Tup10000.h"

#include <fstream>
#include <assert.h>
#include <time.h>


#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/TransformMap.h"
#include "M3DKernel/datascheme/TimeObjectContainer.h"
#include "M3DKernel/datascheme/MemoryStruct.h"
#include "M3DKernel/BackWardConst.h"
#include "Reader/MovReadService.h"

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup10000";
}

/******************************************************************************/
/*	Statics																	  */
/******************************************************************************/
struct sample32 {
	std::uint32_t sample_no;
	std::int32_t sample_value;
};

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup10000::Tup10000()
{
	m_logwarnCount = 0;
}


Tup10000::~Tup10000()
{

}



void Tup10000::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	M3DKernel *pKern = M3DKernel::GetInstance();
	libMove essai;
	essai.LowPart = 0;
	int nbData = (taille_tup - 22) / 8;

	BeamDataObject *beamData = pKern->getObjectMgr()->GetBeamDataObject();


	if (beamData)
	{
		MovRef(beamData);
		pKern->getObjectMgr()->PopBeamDataObject();
		// FAE1975 - on note la position des tuples pings pour �dition du fichier si besoin est
		auto pReadService = trav.GetActiveService();
		if (pReadService)
		{
			beamData->SetPositionInFileRun(pReadService->getStreamDesc(), pReadService->GetCurrentFilePos());
		}
	}
	else
	{
		assert(0);
		return;
	}


	IS.Read((char*)& beamData->m_timeFraction, sizeof(beamData->m_timeFraction), NULL);
	IS.Read((char*)& beamData->m_timeCPU, sizeof(beamData->m_timeCPU), NULL);
	IS.Read((char*)& beamData->m_hacChannelId, sizeof(beamData->m_hacChannelId), NULL);
#ifdef MOVE_CHANNEL_SOUNDER_ID 
	beamData->m_hacChannelId += 30;
#endif

	IS.Read((char*)& beamData->m_transMode, sizeof(beamData->m_transMode), NULL);
	IS.Read((char*)& beamData->m_filePingNumber, sizeof(beamData->m_filePingNumber), NULL);
	IS.Read((char*)& beamData->m_bottomRange, sizeof(beamData->m_bottomRange), NULL);
	beamData->m_bottomWasFound = (beamData->m_bottomRange != FondNotFound);

	// parcours des donn�es
	size_t iData = 0;
	if (nbData > 0)
	{
		char * data = new char[nbData * sizeof(sample32)];
		IS.Read(data, nbData * sizeof(sample32), NULL);

		sample32 * sample = (sample32*)(data + (nbData - 1) * sizeof(sample32));
		unsigned int realNbData = sample->sample_no + 1;
		if (sample->sample_no > 0)
		{
			for (unsigned int i = 0; i < realNbData; i++)
			{
				beamData->push_back_amplitude(UNKNOWN_DB);
			}

			for (int i = 0; i < nbData; i++)
			{
				sample = (sample32*)(data + i * sizeof(sample32));
				if (sample->sample_no < realNbData)
				{
					beamData->setAt(sample->sample_no, (short)(sample->sample_value / 10000));
				}
			}
		}

		delete[] data;
	}

	IS.Read((char*)&beamData->m_tupleAttribute, sizeof(beamData->m_tupleAttribute), NULL);


	unsigned int taille = beamData->size();
	if (taille == 0)
	{
		M3D_LOG_WARN(LoggerName, "Empty Beam Received, dropping pingFan");
	}

	/// find sounder id 
	std::uint32_t id;
	bool found = trav.m_pHacObjectMgr->FindValidSounderContainingChannel(beamData->m_hacChannelId, id);
	if (taille != 0)
	{
		if (found)
		{
			Sounder *sound = trav.m_pHacObjectMgr->GetLastValidSounder(id);
			if (sound)
			{
				HacTime myDate(beamData->m_timeCPU, beamData->m_timeFraction);

				// compensate time shift
				myDate = myDate + sound->m_sounderComputeData.m_timeShift;
				beamData->m_timeCPU = myDate.m_TimeCpu;
				beamData->m_timeFraction = myDate.m_TimeFraction;

				sound->AddBeamDataObject(beamData);
			}
			else
			{
				assert(0);
			}
		}
		else
		{
			if (m_logwarnCount == 0)
				M3D_LOG_WARN(LoggerName, "Unknow sonder, dropping ping Fan");
			m_logwarnCount++;
		}
	}
	MovUnRefDelete(beamData);
}
