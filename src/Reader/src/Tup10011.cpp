
#include "Reader/Tup10011.h"
#include "M3DKernel/DefConstants.h"


#include "M3DKernel/datascheme/PhaseDataObject.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Transducer.h"

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup10011";
}

struct Tup10011PhaseData
{
	std::int32_t AthwartValue : 16;
	std::int32_t AlongValue : 15;
	std::uint32_t ubit : 1;
};

struct Tup10011ZeroPhaseData
{
	long nbSamples : 31;
	unsigned long ubit : 1;
};

/******************************************************************************/
/*	Constructors / Destructor									                        			  */
/******************************************************************************/

Tup10011::Tup10011(void)
{
	m_logwarnCount = 0;
}

Tup10011::~Tup10011(void)
{
}

/******************************************************************************/
/*	Treatments                                           										  */
/******************************************************************************/
void Tup10011::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	M3DKernel *pKern = M3DKernel::GetInstance();
	if (pKern->GetRefKernelParameter().getIgnorePhase())
		return;

	libMove essai;
	essai.LowPart = 0;

	Hac10011 myHac;
	IS.Seek(essai, eSEEK_CUR);

	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.SoftwareChannelId, sizeof(myHac.SoftwareChannelId), NULL);
	IS.Read((char*)& myHac.TransceiverMode, sizeof(myHac.TransceiverMode), NULL);
	IS.Read((char*)& myHac.PingNumber, sizeof(myHac.PingNumber), NULL);
	IS.Read((char*)& myHac.DetectedBottomRange, sizeof(myHac.DetectedBottomRange), NULL);
	IS.Read((char*)& myHac.NumberOfSample, sizeof(myHac.NumberOfSample), NULL);
	
	std::uint32_t iData = 0;
	PhaseDataObject *phaseData = pKern->getObjectMgr()->GetPhaseDataObject();
	if (phaseData == nullptr)
	{
		assert(0);
		return;
	}

	MovRef(phaseData);
	pKern->getObjectMgr()->PopPhaseDataObject();

	phaseData->m_timeFraction = myHac.TimeFraction;
	phaseData->m_timeCPU = myHac.TimeCpu;
	phaseData->m_hacChannelId = myHac.SoftwareChannelId;
	phaseData->m_transMode = myHac.TransceiverMode;
	phaseData->m_filePingNumber = myHac.PingNumber;
	phaseData->m_detectedBottomRange = myHac.DetectedBottomRange;
	phaseData->reserve(myHac.NumberOfSample);

	if (myHac.NumberOfSample > 0)
	{
		// OTK - FAE214 - compensation du d�calage d'une longueur d'impulsion entre amplitude et phase dans le cas du ME70
		std::uint32_t numberOfSamplesToIgnore = 0;

		// detection de la version d'hermes, si elle est inferieure � la version 1.22
		// on echange atwarth et along (cf FAE Hermes 117)
		bool correctAngles = false;
		int hermesVersion = -1;
		double steer_at, ouv_at, ouv_al;
		std::uint32_t id;
		bool found = trav.m_pHacObjectMgr->FindValidSounderContainingChannel(phaseData->m_hacChannelId, id);
		if (found)
		{
			Sounder * pSounder = trav.m_pHacObjectMgr->GetLastValidSounder(id);
			if (pSounder)
			{
				hermesVersion = pSounder->GetHermesVersionFromRemarks();
				correctAngles = (hermesVersion != -1 && hermesVersion <= 122);

				SoftChannel *  pSoftChannel = pSounder->getSoftChannel(phaseData->m_hacChannelId);
				steer_at = pSoftChannel->m_mainBeamAthwartSteeringAngleRad;
				ouv_at = pSoftChannel->m_beam3dBWidthAthwartRad;
				ouv_al = pSoftChannel->m_beam3dBWidthAlongRad;

				// FAE214 - Uniquement dans le cas du ME70
				if (dynamic_cast<SounderMulti*>(pSounder) != NULL && hermesVersion != -1)
				{
					// On ignore les premiers �chantillons sur une longueur d'impulsion
					Transducer * pTrans = pSounder->getTransducerForChannel(myHac.SoftwareChannelId);
					numberOfSamplesToIgnore = (std::uint32_t)round(pTrans->m_pulseDuration / pTrans->m_timeSampleInterval);
				}
			}
		}

		char * data = new char[myHac.NumberOfSample * sizeof(Phase)];
		IS.Read(data, myHac.NumberOfSample * sizeof(Phase), NULL);

		double Ath_l_cor, Al_l_cor;

		while (iData < myHac.NumberOfSample)
		{
			Tup10011PhaseData * comprData = (Tup10011PhaseData *)(data + iData * sizeof(Phase));
			if (comprData->ubit == 0)
			{
				Phase phase(comprData->AthwartValue * 10, comprData->AlongValue * 10);

				// correction enregistrement angles switch� dans les hac
				if (correctAngles)
				{
					Ath_l_cor = asin(sin(steer_at) + cos(steer_at) * ouv_at / ouv_al * sin(phase.GetAlong()));
					Ath_l_cor = Ath_l_cor - steer_at;
					if (hermesVersion < 118)
					{
						Al_l_cor = phase.GetAthwart()* ouv_al / ouv_at;
					}
					else
					{
						Al_l_cor = asin(1 / cos(steer_at) * ouv_al / ouv_at * (sin(phase.GetAthwart()) - sin(steer_at)));
					}

					phase.SetAthwart(Ath_l_cor);
					phase.SetAlong(Al_l_cor);
				}

				if (iData > numberOfSamplesToIgnore)
				{
					phaseData->push_back(phase);
				}
			}
			else
			{
				Tup10011ZeroPhaseData * zeroComprData = (Tup10011ZeroPhaseData*)comprData;
				const long nbSamples = zeroComprData->nbSamples + 1;

				Phase phase(UNKNWON_ANGLE, UNKNWON_ANGLE);
				for (int i = 0; i < nbSamples; ++i)
				{
					phaseData->push_back(phase);
				}
			}
			iData++;
		}

		delete[] data;
	}

	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);
	phaseData->tupleAttributes = myHac.tupleAttributes;

	/// find sounder id 
	std::uint32_t id;
	bool found = trav.m_pHacObjectMgr->FindValidSounderContainingChannel(phaseData->m_hacChannelId, id);
	if (phaseData->size() != 0)
	{
		if (found)
		{
			Sounder *sound = trav.m_pHacObjectMgr->GetLastValidSounder(id);
			if (sound)
			{
				sound->AddPhaseDataObject(phaseData);
			}
			else
			{
				assert(0);
			}
		}
		else
		{
			if (m_logwarnCount == 0)
				M3D_LOG_WARN(LoggerName, "Unknow sonder, dropping Phase Data");
			m_logwarnCount++;
		}
	}
	else
	{
		M3D_LOG_WARN(LoggerName, "Empty Phase Received");
	}

	MovUnRefDelete(phaseData);
}


std::uint32_t Tup10011::Encode(MovStream & IS, PingFan *aPingFan, unsigned short aChanId, unsigned short aVirtualChanId,
	Transducer *pTransducer, unsigned int transducerIndex, unsigned int indexXInPolarMem)
{
	// first getBeam Data 
	unsigned short tupleCode = 10011;

	PhaseDataObject *phaseData = aPingFan->getRefPhaseDataObject(aChanId, aVirtualChanId);
	// parcours des donn�es pour evaluer le tupleSize
	if (!phaseData)
		return 0;

	std::uint32_t backlink = 28;

	MemoryStruct *pMem = aPingFan->GetMemorySetRef()->GetMemoryStruct(transducerIndex);
	unsigned int nbSample = pMem->GetSizeDepthUsed();

	// OTK - FAE214 - compensation du d�calage d'une longueur d'impulsion entre amplitude et phase dans le cas du ME70
	std::uint32_t numberOfIgnoredSamples = 0;

	// detection de la version d'hermes, si elle est inferieure � la version 1.22
	// on echange atwarth et along (cf FAE Hermes 117)
	bool correctAngles = false;
	int hermesVersion = -1;
	double steer_at, ouv_at, ouv_al;
	Sounder * pSounder = aPingFan->getSounderRef();
	if (pSounder)
	{
		hermesVersion = pSounder->GetHermesVersionFromRemarks();
		correctAngles = (hermesVersion != -1 && hermesVersion <= 122);

		SoftChannel *  pSoftChannel = pSounder->getSoftChannel(aChanId);
		steer_at = pSoftChannel->m_mainBeamAthwartSteeringAngleRad;
		ouv_at = pSoftChannel->m_beam3dBWidthAthwartRad;
		ouv_al = pSoftChannel->m_beam3dBWidthAlongRad;

		if (dynamic_cast<SounderMulti*>(pSounder) != NULL)
		{
			// On doit remplir les donn�es de phase ignor�e � la lecture lorsqu'on r�-�crit le fichier...
			Transducer * pTrans = pSounder->getTransducerForChannel(pSoftChannel->getSoftwareChannelId());
			numberOfIgnoredSamples = (std::uint32_t)round(pTrans->m_pulseDuration / pTrans->m_timeSampleInterval);
		}
	}

	IS.GetWriteBuffer(28 + 4 + 4 + (nbSample + numberOfIgnoredSamples) * sizeof(std::int32_t));

	libMove posit;
	posit.LowPart = 28;
	IS.Seek(posit, eSEEK_SET);            //positionnement du "get pointer" pour lecture

	// OTK - 01/06/2009 - on n'�crit pas les donn�es en queue de ping qui correspondent � un remplissage
	// par des valeurs non d�finies de la zone m�moire (on se base sur les valeurs de l'amplitude,
	// car on ne peut pas faire la difference entre une pahse nulle et une pahse de remplissage)
	bool found = false;
	unsigned int noSample = nbSample - 1;
	unsigned int nbUnknownValues = 0;
	while (!found && noSample >= 0)
	{
		short donnee;

		Vector2I pos(indexXInPolarMem, noSample);

		donnee = (*(
			(short*)
			pMem->GetDataFmt()->GetPointerToVoxel(pos)
			));

		if (donnee == UNKNOWN_DB)
		{
			nbUnknownValues++;
		}
		else
		{
			found = true;
		}

		noSample--;
	}

	double Ath_l_cor, Al_l_cor;
	std::uint32_t nbSampleFinal = nbSample - nbUnknownValues + numberOfIgnoredSamples;
	for (noSample = 0; noSample < numberOfIgnoredSamples; noSample++)
	{
		Phase emptyPhase;
		IS.Write((char*)& emptyPhase, sizeof(std::int32_t), NULL);
	}
	for (; noSample < nbSampleFinal; noSample++)
	{
		Phase donnee = (*((Phase*)
			pMem->GetPhase()->GetPointerToVoxel(Vector2I(indexXInPolarMem, noSample))
			));

		// correction enregistrement angles switch� dans les hac (on reswitch pour l'�criture, sinon le switch se refera alors qu'il ne faut pas � la r�lecture...)
		if (correctAngles)
		{
			Al_l_cor = asin((sin(donnee.GetAthwart() + steer_at) - sin(steer_at)) / (cos(steer_at) * ouv_at / ouv_al));

			if (hermesVersion < 118)
			{
				Ath_l_cor = donnee.GetAlong() / (ouv_al / ouv_at);
			}
			else
			{
				Ath_l_cor = asin(sin(donnee.GetAlong()) / (1 / cos(steer_at) * ouv_al / ouv_at) + sin(steer_at));
			}

			donnee.SetAthwart(Ath_l_cor);
			donnee.SetAlong(Al_l_cor);
		}

		IS.Write((char*)& donnee, sizeof(std::int32_t), NULL);
	}


	backlink += nbSampleFinal * sizeof(std::int32_t);
	backlink += 4 + 4;

	std::int32_t tupleAttribute = 1;//phaseData->tupleAttributes;
	IS.Write((char*)&tupleAttribute, sizeof(tupleAttribute), NULL);
	IS.Write((char*)&backlink, sizeof(backlink), NULL);


	posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_SET);            //positionnement du "get pointer" pour lecture

	std::uint32_t tupleSize = backlink - 10;

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& phaseData->m_timeFraction, sizeof(phaseData->m_timeFraction), NULL);
	IS.Write((char*)& phaseData->m_timeCPU, sizeof(phaseData->m_timeCPU), NULL);


	// NMD - On �crit le numero du channel qui est �crit dans le fichier
	IS.Write((char*)& aChanId, sizeof(aChanId), NULL);
	IS.Write((char*)& phaseData->m_transMode, sizeof(phaseData->m_transMode), NULL);
	IS.Write((char*)& phaseData->m_filePingNumber, sizeof(phaseData->m_filePingNumber), NULL);
	IS.Write((char*)& phaseData->m_detectedBottomRange, sizeof(phaseData->m_detectedBottomRange), NULL);
	IS.Write((char*)& nbSampleFinal, sizeof(nbSampleFinal), NULL);

	return backlink;
}
