/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup10030.cpp												  */
/******************************************************************************/

#include "Reader/Tup10030.h"

#include <fstream>
#include <assert.h>
#include <time.h>


#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/TransformMap.h"
#include "M3DKernel/datascheme/TimeObjectContainer.h"
#include "M3DKernel/datascheme/MemoryStruct.h"
#include "M3DKernel/BackWardConst.h"
#include "Reader/MovReadService.h"

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup10030";
}

/******************************************************************************/
/*	Statics																	  */
/******************************************************************************/
struct sample16 {
	unsigned short sample_no;
	short sample_value;
};

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup10030::Tup10030()
{
	m_logwarnCount = 0;
}


Tup10030::~Tup10030()
{

}



void Tup10030::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	M3DKernel *pKern = M3DKernel::GetInstance();
	libMove essai;
	essai.LowPart = 0;
	int nbData = (taille_tup - 22) / 4;

	BeamDataObject *beamData = pKern->getObjectMgr()->GetBeamDataObject();


	if (beamData)
	{
		MovRef(beamData);
		pKern->getObjectMgr()->PopBeamDataObject();
		// FAE1975 - on note la position des tuples pings pour �dition du fichier si besoin est
		auto pReadService = trav.GetActiveService();
		if (pReadService)
		{
			beamData->SetPositionInFileRun(pReadService->getStreamDesc(), pReadService->GetCurrentFilePos());
		}
	}
	else
	{
		assert(0);
		return;
	}


	IS.Read((char*)& beamData->m_timeFraction, sizeof(beamData->m_timeFraction), NULL);
	IS.Read((char*)& beamData->m_timeCPU, sizeof(beamData->m_timeCPU), NULL);
	IS.Read((char*)& beamData->m_hacChannelId, sizeof(beamData->m_hacChannelId), NULL);
#ifdef MOVE_CHANNEL_SOUNDER_ID 
	beamData->m_hacChannelId += 30;
#endif

	IS.Read((char*)& beamData->m_transMode, sizeof(beamData->m_transMode), NULL);
	IS.Read((char*)& beamData->m_filePingNumber, sizeof(beamData->m_filePingNumber), NULL);
	IS.Read((char*)& beamData->m_bottomRange, sizeof(beamData->m_bottomRange), NULL);
	beamData->m_bottomWasFound = (beamData->m_bottomRange != FondNotFound);

	// parcours des donn�es
	size_t iData = 0;
	if (nbData > 0)
	{
		char * data = new char[nbData * sizeof(sample16)];
		IS.Read(data, nbData * sizeof(sample16), NULL);

		sample16 * sample = (sample16*)(data + (nbData - 1) * sizeof(sample16));
		unsigned int realNbData = sample->sample_no + 1;
		if (sample->sample_no > 0)
		{
			for (unsigned int i = 0; i < realNbData; i++)
			{
				beamData->push_back_amplitude(UNKNOWN_DB);
			}

			for (int i = 0; i < nbData; i++)
			{
				sample = (sample16*)(data + i * sizeof(sample16));
				if (sample->sample_no < realNbData)
				{
					beamData->setAt(sample->sample_no, sample->sample_value);
				}
			}
		}

		delete[] data;
	}

	IS.Read((char*)&beamData->m_tupleAttribute, sizeof(beamData->m_tupleAttribute), NULL);


	unsigned int taille = beamData->size();
	if (taille == 0)
	{
		M3D_LOG_WARN(LoggerName, "Empty Beam Received, dropping pingFan");
	}

	/// find sounder id 
	std::uint32_t id;
	bool found = trav.m_pHacObjectMgr->FindValidSounderContainingChannel(beamData->m_hacChannelId, id);
	if (taille != 0)
	{
		if (found)
		{

			Sounder *sound = trav.m_pHacObjectMgr->GetLastValidSounder(id);
			if (sound)
			{
				HacTime myDate(beamData->m_timeCPU, beamData->m_timeFraction);

				// compensate time shift
				myDate = myDate + sound->m_sounderComputeData.m_timeShift;
				beamData->m_timeCPU = myDate.m_TimeCpu;
				beamData->m_timeFraction = myDate.m_TimeFraction;

				sound->AddBeamDataObject(beamData);
			}
			else
			{
				assert(0);
			}
		}
		else
		{
			if (m_logwarnCount == 0)
				M3D_LOG_WARN(LoggerName, "Unknow sonder, dropping ping Fan");
			m_logwarnCount++;

		}
	}
	MovUnRefDelete(beamData);
}
