
#include "Reader/Tup10031.h"
#include "Reader/ComplexSamplesDecoder.h"

#include "M3DKernel/DefConstants.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Environnement.h"
#include "M3DKernel/datascheme/Transducer.h"

#include "BaseMathLib/Vector2.h"

#include <set>

/******************************************************************************/
/*	Statics																	  */
/******************************************************************************/
struct angle16 {
	unsigned short angle_no;
	short angle_along;
	short angle_athwart;
};

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup10031::Tup10031(void)
{
	m_logwarnCount = 0;
}

Tup10031::~Tup10031(void)
{
}


/******************************************************************************/
/*	Treatments                                           										  */
/******************************************************************************/

void Tup10031::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	M3DKernel *pKern = M3DKernel::GetInstance();
	if (pKern->GetRefKernelParameter().getIgnorePhase())
		return;

	PhaseDataObject *phaseData = pKern->getObjectMgr()->GetPhaseDataObject();
	if (phaseData)
	{
		MovRef(phaseData);
		pKern->getObjectMgr()->PopPhaseDataObject();
	}
	else
	{
		assert(0);
		return;
	}

	libMove essai;
	essai.LowPart = 0;
	int nbData = (taille_tup - 22) / 6;

	IS.Seek(essai, eSEEK_CUR);

	IS.Read((char*)& phaseData->m_timeFraction, sizeof(phaseData->m_timeFraction), NULL);
	IS.Read((char*)& phaseData->m_timeCPU, sizeof(phaseData->m_timeCPU), NULL);
	IS.Read((char*)& phaseData->m_hacChannelId, sizeof(phaseData->m_hacChannelId), NULL);
	IS.Read((char*)& phaseData->m_transMode, sizeof(phaseData->m_transMode), NULL);
	IS.Read((char*)& phaseData->m_filePingNumber, sizeof(phaseData->m_filePingNumber), NULL);
	IS.Read((char*)& phaseData->m_detectedBottomRange, sizeof(phaseData->m_detectedBottomRange), NULL);

	// parcours des donn�es
	size_t iData = 0;
	if (nbData > 0)
	{
		char * data = new char[nbData * sizeof(angle16)];
		IS.Read(data, nbData * sizeof(angle16), NULL);

		IS.Read((char*)&phaseData->tupleAttributes, sizeof(phaseData->tupleAttributes), NULL);

		std::uint32_t id;
		bool found = trav.m_pHacObjectMgr->FindValidSounderContainingChannel(phaseData->m_hacChannelId, id);
		if (found)
		{
			Sounder * pSounder = trav.m_pHacObjectMgr->GetLastValidSounder(id);

			if (pSounder)
			{
				HacTime myDate(phaseData->m_timeCPU, phaseData->m_timeFraction);

				// compensate time shift
				myDate = myDate + pSounder->m_sounderComputeData.m_timeShift;
				phaseData->m_timeCPU = myDate.m_TimeCpu;
				phaseData->m_timeFraction = myDate.m_TimeFraction;

				Transducer * pTrans = pSounder->getTransducerForChannel(phaseData->m_hacChannelId);

				// OTK - FAE214 - compensation du d�calage d'une longueur d'impulsion entre amplitude et phase dans le cas du ME70
				std::uint32_t numberOfSamplesToIgnore = 0;

				// variables pour la conversion de la phase
				SoftChannel *  pSoftChannel = pSounder->getSoftChannel(phaseData->m_hacChannelId);

				bool bIsMBES = dynamic_cast<SounderMulti*>(pSounder) != NULL;

				double factAlong;
				double factAthwart;
				double alongSin, athwartSin;
				if (bIsMBES)
				{
					factAlong = cos(pSoftChannel->m_mainBeamAlongSteeringAngleRad)*(PI / 128.0) / pSoftChannel->m_beamAlongAngleSensitivity / 10.0;
					factAthwart = cos(pSoftChannel->m_mainBeamAthwartSteeringAngleRad)*(PI / 128.0) / pSoftChannel->m_beamAthwartAngleSensitivity / 10.0;
					alongSin = sin(pSoftChannel->m_mainBeamAlongSteeringAngleRad);
					athwartSin = sin(pSoftChannel->m_mainBeamAthwartSteeringAngleRad);

					// On ignore les premiers �chantillons sur une longueur d'impulsion
					numberOfSamplesToIgnore = (std::uint32_t)round(pTrans->m_pulseDuration / pTrans->m_timeSampleInterval);
				}
				else
				{
					factAlong = 14.0625 / pSoftChannel->m_beamAlongAngleSensitivity;
					factAthwart = 14.0625 / pSoftChannel->m_beamAthwartAngleSensitivity;

					// Special scaling for split beam transducers with three sectors
					bool threeSectorScalingRequired = false;
					if (pSounder->GetHermesVersionFromRemarks() < HERMES_VERSION_INTRODUCING_BEAM_TYPES) {
						threeSectorScalingRequired = strncmp("ES38-7", pTrans->m_transName, 6) == 0 || strncmp("ES38-10", pTrans->m_transName, 7) == 0 || strncmp("ES38-18DK", pTrans->m_transName, 9) == 0;
					}
					else {
						const auto specialScalingBeamType = std::set<SimradBeamType>({ BeamTypeSplit3, BeamTypeSplit3C, BeamTypeSplit3CN, BeamTypeSplit3CW });
						threeSectorScalingRequired = specialScalingBeamType.count((SimradBeamType)pSoftChannel->m_beamType) > 0;
					}

					if (threeSectorScalingRequired)
					{
						factAlong = factAlong*2/sqrt(3);
						factAthwart = factAthwart*2;
					}
				}


				double dbAlong, dbAthwart;
				for (int i = 0; i < nbData; i++)
				{
					angle16 * angle = (angle16*)(data + i * sizeof(angle16));
					if (bIsMBES)
					{
						// 1 - Passage en pas de phase �lectrique en angle �lectrique (la conversion dixi�mes de phase steps du format HAC vers pahse steps
						// est incluse dans les facteurs factAlong et factAthwart)
						dbAlong = factAlong * angle->angle_along;
						dbAthwart = factAthwart * angle->angle_athwart;

						// 2 - Passage en angle m�canique et passage en centi�mes de degr�s
						dbAlong = 100.0*RAD_TO_DEG(asin(alongSin + dbAlong) - pSoftChannel->m_mainBeamAlongSteeringAngleRad);
						dbAthwart = 100.0*RAD_TO_DEG(asin(athwartSin + dbAthwart) - pSoftChannel->m_mainBeamAthwartSteeringAngleRad);
					}
					else
					{
						// rmq. dans le cas monofaisceau, le passage de dixi�mes en centi�mes de degr�s est inclu dans les facteurs factAlong et factAthwart
						dbAlong = factAlong * angle->angle_along;
						dbAthwart = factAthwart * angle->angle_athwart;
					}

					// On les positionne en m�moire en centi�mes de degr�s
					int compensatedIndex = i - (int)numberOfSamplesToIgnore;
					if (compensatedIndex >= 0)
					{
						Phase phase(floor(dbAthwart + 0.5), floor(dbAlong + 0.5));
						phaseData->push_back(phase);
					}
				}

				pSounder->AddPhaseDataObject(phaseData);
			}
		}

		delete[] data;
	}
	else
	{
		IS.Read((char*)&phaseData->tupleAttributes, sizeof(phaseData->tupleAttributes), NULL);
	}

	MovUnRefDelete(phaseData);
}

std::uint32_t Tup10031::Encode(MovStream & IS, PingFan *aPingFan, unsigned short aChanId, unsigned short aVirtualChanId,
	Transducer *pTransducer, unsigned int transducerIndex, unsigned int indexXInPolarMem)
{
	// first getBeam Data 
	unsigned short tupleCode = 10031;

	PhaseDataObject *phaseData = aPingFan->getRefPhaseDataObject(aChanId, aVirtualChanId);
	// parcours des donn�es pour evaluer le tupleSize
	if (!phaseData)
		return 0;

	// OTK - FAE214 - compensation du d�calage d'une longueur d'impulsion entre amplitude et phase dans le cas du ME70
	unsigned short numberOfIgnoredSamples = 0;

	Sounder * pSounder = aPingFan->getSounderRef();
	SoftChannel *  pSoftChannel = pSounder->getSoftChannel(aChanId);

	bool bIsMBES = dynamic_cast<SounderMulti*>(pSounder) != NULL;

	double factAlong;
	double factAthwart;
	double alongSin, athwartSin;
	if (bIsMBES)
	{
		factAlong = 10.0*pSoftChannel->m_beamAlongAngleSensitivity / cos(pSoftChannel->m_mainBeamAlongSteeringAngleRad) / (PI / 128.0);
		factAthwart = 10.0*pSoftChannel->m_beamAthwartAngleSensitivity / cos(pSoftChannel->m_mainBeamAthwartSteeringAngleRad) / (PI / 128.0);
		alongSin = sin(pSoftChannel->m_mainBeamAlongSteeringAngleRad);
		athwartSin = sin(pSoftChannel->m_mainBeamAthwartSteeringAngleRad);

		// On doit remplir les donn�es de phase ignor�e � la lecture lorsqu'on r�-�crit le fichier...
		Transducer * pTrans = pSounder->getTransducerForChannel(pSoftChannel->getSoftwareChannelId());
		numberOfIgnoredSamples = (unsigned short)round(pTrans->m_pulseDuration / pTrans->m_timeSampleInterval);
	}
	else
	{
		factAlong = (double)pSoftChannel->m_beamAlongAngleSensitivity / 14.0625;
		factAthwart = (double)pSoftChannel->m_beamAthwartAngleSensitivity / 14.0625;

		// Special scaling for split beam transducers with three sectors
		bool threeSectorScalingRequired = false;
		if (pSounder->GetHermesVersionFromRemarks() < HERMES_VERSION_INTRODUCING_BEAM_TYPES) {
			threeSectorScalingRequired = strncmp("ES38-7", pTransducer->m_transName, 6) == 0 || strncmp("ES38-10", pTransducer->m_transName, 7) == 0 || strncmp("ES38-18DK", pTransducer->m_transName, 9) == 0;
		}
		else {
			const auto specialScalingBeamType = std::set<SimradBeamType>({ BeamTypeSplit3, BeamTypeSplit3C, BeamTypeSplit3CN, BeamTypeSplit3CW });
			threeSectorScalingRequired = specialScalingBeamType.count((SimradBeamType)pSoftChannel->m_beamType) > 0;
		}

		if (threeSectorScalingRequired)
		{
			factAlong /=  2 / sqrt(3);
			factAthwart /= 2;
		}
	}

	// TODO
	std::uint32_t backlink = 24;

	MemoryStruct *pMem = aPingFan->GetMemorySetRef()->GetMemoryStruct(transducerIndex);
	const unsigned int nbSample = pMem->GetSizeDepthUsed();

	int bufferSize = backlink + (nbSample) * 6 + 4 + 4;
	if ((bufferSize % 4) != 0) {
		bufferSize += 2;
	}
	IS.GetWriteBuffer(bufferSize);

	libMove posit;
	posit.LowPart = 24;
	IS.Seek(posit, eSEEK_SET);            //positionnement du "get pointer" pour lecture 


	// WRITE DATA
	short along;
	short athwart;
	double dbAlong, dbAthwart;
	unsigned int nbSampleFinal = 0;
	for (unsigned int noSample = 0; noSample < nbSample; ++noSample)
	{
		BaseMathLib::Vector2I pos(indexXInPolarMem, noSample);

		Phase phaseData = (*((Phase*)
			pMem->GetPhase()->GetPointerToVoxel(BaseMathLib::Vector2I(indexXInPolarMem, noSample))
			));

		unsigned short compensatedIndex = noSample + numberOfIgnoredSamples;
		IS.Write((char*)& compensatedIndex, sizeof(unsigned short), NULL);

		if (bIsMBES)
		{
			dbAlong = factAlong*(sin(DEG_TO_RAD((double)phaseData.GetRawAlongValue() / 100.0)) - alongSin);
			dbAthwart = factAthwart*(sin(DEG_TO_RAD((double)phaseData.GetRawAthwartValue() / 100.0)) - athwartSin);
		}
		else
		{
			dbAlong = factAlong * phaseData.GetRawAlongValue();
			dbAthwart = factAthwart * phaseData.GetRawAthwartValue();
		}

			//Le probl�me de r�archivage des donn�es Simrad est que les donn�es du tuple 10031 sont sock�es en 1/10 de phase step dans le hac
			//mais la pr�cision du sondeur est le phase step (0.06� pour sensibilit� de 23).
			//Il faudrait donc � la r��criture arrondir les valeurs au phase step pr�s pour ne pas avoir de probl�mes:

		along = floor(floor(dbAlong + 0.5) / 10 + 0.5) * 10;
		athwart = floor(floor(dbAthwart + 0.5) / 10 + 0.5) * 10;

		IS.Write((char*)& along, sizeof(short), NULL);
		IS.Write((char*)& athwart, sizeof(short), NULL);

		++nbSampleFinal;
	}

	bool bAddSpace = false;
	backlink += nbSampleFinal * 6 + 4 + 4;
	if ((backlink % 4) != 0) {
		backlink += 2;
		bAddSpace = true;
		unsigned short space = 0;
		IS.Write((char*)&space, sizeof(space), NULL);
	}

	std::int32_t tupleAttribute = 1; //phaseData->tupleAttributes;
	IS.Write((char*)&tupleAttribute, sizeof(tupleAttribute), NULL);
	IS.Write((char*)&backlink, sizeof(backlink), NULL);

	posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_SET);            //positionnement du "get pointer" pour lecture

	std::uint32_t tupleSize = backlink - 10;

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& phaseData->m_timeFraction, sizeof(phaseData->m_timeFraction), NULL);
	IS.Write((char*)& phaseData->m_timeCPU, sizeof(phaseData->m_timeCPU), NULL);


	// NMD - On �crit le numero du channel qui est �crit dans le fichier
	IS.Write((char*)& aChanId, sizeof(aChanId), NULL);
	IS.Write((char*)& phaseData->m_transMode, sizeof(phaseData->m_transMode), NULL);
	IS.Write((char*)& phaseData->m_filePingNumber, sizeof(phaseData->m_filePingNumber), NULL);
	IS.Write((char*)& phaseData->m_detectedBottomRange, sizeof(phaseData->m_detectedBottomRange), NULL);

	return backlink;
}
