/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup10040.cpp												  */
/******************************************************************************/

#include "Reader/Tup10040.h"

#include <fstream>
#include <assert.h>
#include <time.h>


#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/TransformMap.h"
#include "M3DKernel/datascheme/TimeObjectContainer.h"
#include "M3DKernel/datascheme/MemoryStruct.h"
#include "M3DKernel/BackWardConst.h"
#include "Reader/MovReadService.h"

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup10040";
}

/******************************************************************************/
/*	Statics																	  */
/******************************************************************************/


struct donneecompr {
	short valeur : 15;
	unsigned short ubit : 1;
};

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup10040::Tup10040()
{
	m_EvaluateCompressionRatio = 1.3f;
	m_logwarnCount = 0;
}


Tup10040::~Tup10040()
{

}



void Tup10040::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	M3DKernel *pKern = M3DKernel::GetInstance();
	libMove essai;
	essai.LowPart = 0;
	std::uint32_t nbData;

	BeamDataObject *beamData = pKern->getObjectMgr()->GetBeamDataObject();


	if (beamData)
	{
		MovRef(beamData);
		pKern->getObjectMgr()->PopBeamDataObject();
		// FAE1975 - on note la position des tuples pings pour �dition du fichier si besoin est
		auto pReadService = trav.GetActiveService();
		if (pReadService)
		{
			beamData->SetPositionInFileRun(pReadService->getStreamDesc(), pReadService->GetCurrentFilePos());
		}
	}
	else
	{
		assert(0);
		return;
	}


	IS.Read((char*)& beamData->m_timeFraction, sizeof(beamData->m_timeFraction), NULL);
	IS.Read((char*)& beamData->m_timeCPU, sizeof(beamData->m_timeCPU), NULL);
	IS.Read((char*)& beamData->m_hacChannelId, sizeof(beamData->m_hacChannelId), NULL);
#ifdef MOVE_CHANNEL_SOUNDER_ID 
	beamData->m_hacChannelId += 30;
#endif

	IS.Read((char*)& beamData->m_transMode, sizeof(beamData->m_transMode), NULL);
	IS.Read((char*)& beamData->m_filePingNumber, sizeof(beamData->m_filePingNumber), NULL);
	IS.Read((char*)& beamData->m_bottomRange, sizeof(beamData->m_bottomRange), NULL);
	beamData->m_bottomWasFound = (beamData->m_bottomRange != FondNotFound);
	IS.Read((char*)& nbData, sizeof(nbData), NULL);


	// compression is around 30%
	beamData->reserve((unsigned int)(nbData*m_EvaluateCompressionRatio));

	std::uint32_t iData = 0;
	// parcour des donn�es

	// OTK -  OPTI - on ne fait qu'un read
	if (nbData > 0)
	{
		char * data = new char[nbData * sizeof(donneecompr)];
		IS.Read(data, nbData * sizeof(donneecompr), NULL);

		while (iData < nbData)
		{
			/// We decompress data
			donneecompr * comprData = (donneecompr *)(data + iData * sizeof(donneecompr));
			short valeur = (short)(comprData->valeur);

			short ubit = (short)(comprData->ubit);
			if (ubit == 0)
			{
				beamData->push_back_amplitude(valeur);
			}
			else
			{
				for (int i = 0; i < valeur + 1; i++)
				{
					beamData->push_back_amplitude(UNKNOWN_DB);
				}
			}
			iData++;
		}

		delete[] data;
	}

	if (iData % 2 != 0)
	{
		short space;
		//	pos 24
		IS.Read((char*)& space, sizeof(space), NULL);

	}

	IS.Read((char*)&beamData->m_tupleAttribute, sizeof(beamData->m_tupleAttribute), NULL);


	unsigned int taille = beamData->size();
	//assert(taille!=0);
	//	pKern->getWorkFrame()->AddBeamData(*beamData, pKern->getSequence().getSequenceData());
	if (taille == 0)
	{
		M3D_LOG_WARN(LoggerName, "Empty Beam Received, dropping pingFan");
	}

	/// find sounder id 
	std::uint32_t id;
	bool found = trav.m_pHacObjectMgr->FindValidSounderContainingChannel(beamData->m_hacChannelId, id);
	//m_EvaluateCompressionRatio=(m_EvaluateCompressionRatio+(taille*1.0/nbData))/2.0;
	if (taille != 0)
	{
		if (found)
		{

			Sounder *sound = trav.m_pHacObjectMgr->GetLastValidSounder(id);
			if (sound)
			{
				HacTime myDate(beamData->m_timeCPU, beamData->m_timeFraction);

				// compensate time shift
				myDate = myDate + sound->m_sounderComputeData.m_timeShift;
				beamData->m_timeCPU = myDate.m_TimeCpu;
				beamData->m_timeFraction = myDate.m_TimeFraction;

				sound->AddBeamDataObject(beamData);
			}
			else
			{
				assert(0);
			}
		}
		else
		{
			if (m_logwarnCount == 0)
				M3D_LOG_WARN(LoggerName, "Unknow sonder, dropping ping Fan");
			m_logwarnCount++;
		}
	}
	MovUnRefDelete(beamData);
}

std::uint32_t Tup10040::Encode(MovStream & IS, PingFan *aPingFan, unsigned short aChanId, unsigned short aVirtualChanId,
	Transducer *pTransducer, unsigned int transducerIndex, unsigned int indexXInPolarMem)
{

	/// first getBeam Data 
	unsigned short tupleCode = 10040;

	BeamDataObject *beamData = aPingFan->getRefBeamDataObject(aChanId, aVirtualChanId);
	// parcours des donn�es pour evaluer le tupleSize
	if (!beamData)
		return 0;
	std::uint32_t backlink = 28;

	MemoryStruct *pMem = aPingFan->GetMemorySetRef()->GetMemoryStruct(transducerIndex);
	unsigned int nbSample = pMem->GetSizeDepthUsed();


	IS.GetWriteBuffer(28 + 4 + 4 + nbSample * sizeof(short));


	libMove posit;
	posit.LowPart = 28;
	IS.Seek(posit, eSEEK_SET);            //positionnement du "get pointer" pour lecture

	// OTK - 01/06/2009 - on n'�crit pas les donn�es en queue de ping qui correspondent � un remplissage
	// par des valeurs non d�finies de la zone m�moire.
	bool found = false;
	unsigned int noSample = nbSample - 1;
	unsigned int nbUnknownValues = 0;
	while (!found && noSample >= 0)
	{
		short donnee;

		Vector2I pos(indexXInPolarMem, noSample);

		donnee = (*(
			(short*)
			pMem->GetDataFmt()->GetPointerToVoxel(pos)
			));

		if (donnee == UNKNOWN_DB)
		{
			nbUnknownValues++;
		}
		else
		{
			found = true;
		}

		noSample--;
	}


	unsigned int nbData = 0;
	unsigned int nbBlanc = 0;
	for (unsigned int noSample = 0; noSample < (nbSample - nbUnknownValues); noSample++)
	{
		short donnee;

		Vector2I pos(indexXInPolarMem, noSample);


		donnee = (*(
			(short*)
			pMem->GetDataFmt()->GetPointerToVoxel(pos)
			));

		if (donnee <= UNKNOWN_DB)
		{
			nbBlanc++;
		}
		else
		{
			if (nbBlanc > 0)
			{

				donneecompr donnee2;
				donnee2.valeur = nbBlanc - 1;
				donnee2.ubit = 1;

				IS.Write((char*)& donnee2, sizeof(short), NULL);

				nbBlanc = 0;
				nbData++;
			}
			donneecompr donnee2;
			donnee2.valeur = donnee;
			donnee2.ubit = 0;

			IS.Write((char*)& donnee2, sizeof(short), NULL);
			nbData++;
		}

	} // Fin for (i= 0; i < nb; i++)
	// il faut neanmoins enregistrer les dernieres valeurs
	// inferieures � la threshold.

	if (nbBlanc > 0)	// on enregistre k dans la table HAC
	{

		donneecompr donnee2;
		donnee2.valeur = nbBlanc - 1;
		donnee2.ubit = 1;

		IS.Write((char*)& donnee2, sizeof(short), NULL);
		nbData++;
	}



	backlink += nbData * sizeof(short);
	if (nbData % 2 != 0)
	{
		backlink += sizeof(short);
	}
	backlink += 4 + 4;
	//	IS.GetWriteBuffer(backlink);


	if (nbData % 2 != 0)
	{
		short space = 0;
		//	pos 24
		IS.Write((char*)& space, sizeof(space), NULL);
	}
	std::int32_t tupleAttribute = 1;
	IS.Write((char*)&tupleAttribute, sizeof(tupleAttribute), NULL);
	IS.Write((char*)&backlink, sizeof(backlink), NULL);


	posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_SET);            //positionnement du "get pointer" pour lecture

	std::uint32_t tupleSize = backlink - 10;

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& beamData->m_timeFraction, sizeof(beamData->m_timeFraction), NULL);
	IS.Write((char*)& beamData->m_timeCPU, sizeof(beamData->m_timeCPU), NULL);


	// NMD - On �crit le numero du channel qui est �crit dans le fichier
	IS.Write((char*)& aChanId, sizeof(aChanId), NULL);
	IS.Write((char*)& beamData->m_transMode, sizeof(beamData->m_transMode), NULL);
	IS.Write((char*)& beamData->m_filePingNumber, sizeof(beamData->m_filePingNumber), NULL);
	if (beamData->m_bottomWasFound)
		IS.Write((char*)& beamData->m_bottomRange, sizeof(beamData->m_bottomRange), NULL);
	else
	{
		IS.Write((char*)&FondNotFound, sizeof(FondNotFound), NULL);
	}
	std::uint32_t nbSampleFinal = nbData;
	IS.Write((char*)& nbSampleFinal, sizeof(nbSampleFinal), NULL);




	return backlink;
}



