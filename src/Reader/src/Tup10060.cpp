/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup10060.cpp												  */
/******************************************************************************/

#include "Reader/Tup10060.h"

#include "Reader/MovReadService.h"
#include "Reader/ComplexSamplesDecoder.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Environnement.h"
#include "M3DKernel/datascheme/Transducer.h"

using namespace BaseMathLib;

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup10060";
}

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup10060::Tup10060()
{
	m_logwarnCount = 0;
}

Tup10060::~Tup10060()
{
}

void Tup10060::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	M3DKernel *pKern = M3DKernel::GetInstance();
	libMove essai;
	essai.LowPart = 0;
	std::uint32_t nbData;


	MovObjectPtr<BeamDataObject> beamData = pKern->getObjectMgr()->GetBeamDataObject();
	if (beamData == nullptr)
	{
		assert(0);
		return;
	}

	pKern->getObjectMgr()->PopBeamDataObject();
	// FAE1975 - on note la position des tuples pings pour �dition du fichier si besoin est
	auto pReadService = trav.GetActiveService();
	if (pReadService)
	{
		beamData->SetPositionInFileRun(pReadService->getStreamDesc(), pReadService->GetCurrentFilePos());
	}

	MovObjectPtr<PhaseDataObject> phaseData;
	if (!pKern->GetRefKernelParameter().getIgnorePhase())
	{
		phaseData = pKern->getObjectMgr()->GetPhaseDataObject();
		if (phaseData)
		{
			pKern->getObjectMgr()->PopPhaseDataObject();
		}
		else
		{
			assert(0);
			return;
		}
	}


	IS.Read((char*)& beamData->m_timeFraction, sizeof(beamData->m_timeFraction), NULL);
	IS.Read((char*)& beamData->m_timeCPU, sizeof(beamData->m_timeCPU), NULL);
	IS.Read((char*)& beamData->m_hacChannelId, sizeof(beamData->m_hacChannelId), NULL);
#ifdef MOVE_CHANNEL_SOUNDER_ID 
	beamData->m_hacChannelId += 30;
#endif

	IS.Read((char*)& beamData->m_transMode, sizeof(beamData->m_transMode), NULL);
	IS.Read((char*)& beamData->m_filePingNumber, sizeof(beamData->m_filePingNumber), NULL);
	IS.Read((char*)& beamData->m_bottomRange, sizeof(beamData->m_bottomRange), NULL);
	beamData->m_bottomWasFound = (beamData->m_bottomRange != FondNotFound);

	IS.Read((char*)& nbData, sizeof(nbData), NULL);
	if (nbData == 0)
	{
		M3D_LOG_WARN(LoggerName, "Empty Beam Received, dropping pingFan");
		return;
	}

	/// find sounder id 
	std::uint32_t id;
	bool found = trav.m_pHacObjectMgr->FindValidSounderContainingChannel(beamData->m_hacChannelId, id);
	if (!found)
	{
		if (m_logwarnCount == 0)
			M3D_LOG_WARN(LoggerName, "Unknow sonder, dropping ping Fan");
		m_logwarnCount++;
		return;
	}

	Sounder *sound = trav.m_pHacObjectMgr->GetLastValidSounder(id);
	if (sound == nullptr)
	{
		assert(0);
		return;
	}
	HacTime myDate(beamData->m_timeCPU, beamData->m_timeFraction);

	// compensate time shift
	myDate = myDate + sound->m_sounderComputeData.m_timeShift;
	beamData->m_timeCPU = myDate.m_TimeCpu;
	beamData->m_timeFraction = myDate.m_TimeFraction;
	if (phaseData)
	{
		phaseData->m_timeFraction = beamData->m_timeFraction;
		phaseData->m_timeCPU = beamData->m_timeCPU;
	}

	Transducer * pTrans = sound->getTransducerForChannel(beamData->m_hacChannelId);
	SoftChannel * pSoftChan = pTrans->getSoftChannel(beamData->m_hacChannelId);

	// Try to compute slit beam type and sector count
	const int hermesVersion = sound->GetHermesVersionFromRemarks();
	bool valid = false;

	const int nbQuadrants = (taille_tup - 26) / (nbData * 8);
	SimradBeamType beamType = BeamTypeSplit;
	if (hermesVersion != -1 && hermesVersion < HERMES_VERSION_INTRODUCING_BEAM_TYPES)
	{
		switch (nbQuadrants)
		{
		case 3:
			M3D_LOG_INFO(LoggerName, Log::format("Nb quadrants detected : %d", nbQuadrants));
			beamType = BeamTypeSplit3;
			valid = true;
			break;

		case 4:
			if (strncmp("ES38-7", pTrans->m_transName, 6) == 0)
			{
				beamType = BeamTypeSplit3CN;
				valid = true;
			}
			else
			{
				beamType = BeamTypeSplit;
				valid = true;
			}
			break;

		default:
			M3D_LOG_WARN(LoggerName, Log::format("Unexpected quadrant count : %d (tuple size = %d, nb data = %d)", nbQuadrants, taille_tup, nbData));
			break;
		}
	}
	else
	{
		switch (pSoftChan->m_beamType)
		{
		case BeamTypeSplit:
			if (nbQuadrants == 4)
			{
				beamType = BeamTypeSplit;
				valid = true;
			}
			else
			{
				M3D_LOG_WARN(LoggerName, Log::format("Unexpected quadrant count for beamType 'BeamTypeSplit' : expected 4, got %d", nbQuadrants));
			}
			break;

		case BeamTypeSplit3:
			if (nbQuadrants == 3)
			{
				beamType = BeamTypeSplit3;
				valid = true;
			}
			else
			{
				M3D_LOG_WARN(LoggerName, Log::format("Unexpected quadrant count for beamType 'BeamTypeSplit3' : expected 3, got %d", nbQuadrants));
			}
			break;

		case BeamTypeSplit3CN:
			if (nbQuadrants == 4)
			{
				beamType = BeamTypeSplit3CN;
				valid = true;
			}
			else
			{
				M3D_LOG_WARN(LoggerName, Log::format("Unexpected quadrant count for beamType 'BeamTypeSplit3CN' : expected 4, got %d", nbQuadrants));
			}
			break;
		case BeamTypeSingle:
			if (nbQuadrants == 1)
			{
				beamType = BeamTypeSingle;
				valid = true;
			}
			else
			{
				M3D_LOG_WARN(LoggerName, Log::format("Unexpected quadrant count for beamType 'BeamTypeSingle' : expected 1, got %d", nbQuadrants));
			}
			break;
		default:
			M3D_LOG_WARN(LoggerName, Log::format("Unsupported beam type (%d)...", pSoftChan->m_beamType));
			break;
		}
	}

	if (valid == false)
	{
		return;
	}

	//The samples are a list of complex values separated on N quadrants (1 complex per quandrant)
	const int nbSamples = nbData * nbQuadrants;
	std::vector<std::complex<float>> samples(nbSamples);
	for (int i = 0; i < nbSamples; ++i)
	{
		float realPart, imagPart;
		IS.Read((char*)&realPart, sizeof(float), NULL);
		IS.Read((char*)&imagPart, sizeof(float), NULL);
		samples[i] = std::complex<float>(realPart, imagPart);
	}
	beamData->reserve(nbData);

	IS.Read((char*)&beamData->m_tupleAttribute, sizeof(beamData->m_tupleAttribute), NULL);
	if (phaseData)
	{
		phaseData->m_hacChannelId = beamData->m_hacChannelId;
		phaseData->m_transMode = beamData->m_transMode;
		phaseData->m_filePingNumber = beamData->m_filePingNumber;
		phaseData->m_detectedBottomRange = beamData->m_bottomRange;
		phaseData->tupleAttributes = beamData->m_tupleAttribute;
	}

	// Prepare QuadrantDataObject
	QuadrantDataObject * quadrantDataObject = nullptr;
	const auto & kernelParameter = pKern->GetRefKernelParameter();
	if (kernelParameter.getKeepQuadrantsInMemory())
	{
		quadrantDataObject = new QuadrantDataObject();
		quadrantDataObject->samples.resize(nbQuadrants);
		for (int q = 0; q < nbQuadrants; ++q)
		{
			auto & qSamples = quadrantDataObject->samples[q];
			qSamples.resize(nbData);
		}

		// copy data
		auto pComplexSample = (std::complex<float>*)(samples.data());
		for (int i = 0; i < nbData; ++i)
		{
			for (int q = 0; q < nbQuadrants; ++q)
			{
				quadrantDataObject->samples[q][i] = *pComplexSample++;
			}
		}
	}

	ReaderUtils::processComplexSamples(trav, samples, sound, beamType, nbQuadrants, beamData, phaseData);
}


std::uint32_t Tup10060::Encode(MovStream & IS, PingFan *aPingFan, unsigned short aChanId, unsigned short aVirtualChanId, Transducer *pTransducer, unsigned int transducerIndex, unsigned int indexXInPolarMem)
{
	BeamDataObject *beamData = aPingFan->getRefBeamDataObject(aChanId, aVirtualChanId);
	if (!beamData)
		return 0;

	MemoryObjectDataFmt * pBeam = aPingFan->GetMemorySetRef()->GetMemoryStruct(transducerIndex)->GetDataFmt();
	if (!pBeam)
		return 0;

	// Copy source data from file
	std::string fileName;
	std::uint32_t streamPos;
	beamData->GetPositionInFileRun(fileName, streamPos);

	std::fstream fs(fileName.c_str(), std::ios::in | std::ios::out | std::ios::binary);

	// on se positionne sur le backlink du tuple associ�
	fs.seekg(streamPos - 4);

	// lecture du backlink
	std::uint32_t backlink;
	fs.read(reinterpret_cast<char*> (&backlink), sizeof(backlink));

	// On se positionne sur la taille du tuple
	fs.seekg(streamPos - backlink);

	char * tupleData = new char[backlink];
	fs.read(tupleData, backlink);

	std::uint32_t tupleSize;
	unsigned short tupleType;

	memcpy(&tupleSize, tupleData, sizeof(std::uint32_t));
	memcpy(&tupleType, tupleData + 4, sizeof(unsigned short));

	std::uint32_t nbSamples = 0;
	memcpy(&nbSamples, tupleData + 24, sizeof(std::uint32_t));

	const int nbQuadrants = (tupleSize - 26) / (nbSamples * 8);
	const int beamSize = std::min<std::uint32_t>(nbSamples, pBeam->getSize().y);

	// R��criture du fond
	std::int32_t bottom = beamData->m_bottomWasFound ? beamData->m_bottomRange : FondNotFound;
	memcpy(tupleData + 20, &bottom, sizeof(std::int32_t));
	
	// R��criture des �chantillons modifi�
	float * samples = (float*)(tupleData + 28);	
	for (unsigned int iSample = 0; iSample < beamSize; iSample++)
	{
		DataFmt val = pBeam->GetValueToVoxel(0, iSample);
		if (val <= UNKNOWN_DB)
		{
			unsigned int pos = iSample * nbQuadrants * 2;
			for (int q = 0; q < nbQuadrants; ++q)
			{
				samples[pos + q*2] = 0.0f;
				samples[pos + q*2 + 1] = 0.0f;
			}
		}
	}

	// R��criture des �chantillons de fins
	for (unsigned int iSample = beamSize; iSample < nbSamples; ++iSample)
	{
		unsigned int pos = iSample * nbQuadrants * 2;
		for (int q = 0; q < nbQuadrants; ++q)
		{
			samples[pos + q * 2] = 0.0f;
			samples[pos + q * 2 + 1] = 0.0f;
		}
	}

	IS.Write(tupleData, backlink, NULL);

	delete[] tupleData;

	return backlink;
}
