
#include "Reader/Tup10090.h"
#include "M3DKernel/datascheme/SingleTargetDataObject.h"
#include "Reader/TupStructDef.h"

#include <fstream>

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SplitBeamParameter.h"
#include "Reader/ReaderCtrl.h"

namespace 
{
	constexpr const char * LoggerName = "Tup10090";
}

Tup10090::Tup10090(void)
{
	m_logwarnCount = 0;
}

Tup10090::~Tup10090(void)
{
}

void Tup10090::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	M3DKernel *pKern = M3DKernel::GetInstance();
	libMove essai;
	essai.LowPart = 0;

	Hac10090 myHac;

	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.ParentSubChannelId, sizeof(myHac.ParentSubChannelId), NULL);
	IS.Read((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Read((char*)& myHac.PingNumber, sizeof(myHac.PingNumber), NULL);
	IS.Read((char*)& myHac.SelectStartRange, sizeof(myHac.SelectStartRange), NULL);
	IS.Read((char*)& myHac.SelectEndRange, sizeof(myHac.SelectEndRange), NULL);
	IS.Read((char*)& myHac.DetectedBottomRange, sizeof(myHac.DetectedBottomRange), NULL);
	IS.Read((char*)& myHac.NumberOfTarget, sizeof(myHac.NumberOfTarget), NULL);


	SingleTargetDataObject *pMyObject = SingleTargetDataObject::Create();
	MovRef(pMyObject);

	pMyObject->m_parentSTId = myHac.ParentSubChannelId;
	pMyObject->m_PingTime.m_TimeCpu = myHac.TimeCpu;
	pMyObject->m_PingTime.m_TimeFraction = myHac.TimeFraction;
	pMyObject->m_pingNumber = myHac.PingNumber;
	pMyObject->m_selectStartRange = myHac.SelectStartRange*0.0001;
	pMyObject->m_selectEndRange = myHac.SelectEndRange*0.0001;
	pMyObject->m_detectedBottom = myHac.DetectedBottomRange*0.0001;

	for (unsigned int i = 0; i < myHac.NumberOfTarget; i++)
	{
		Hac10090Target Target;
		IS.Read((char*)& Target.Range, sizeof(Target.Range), NULL);
		IS.Read((char*)& Target.CompensatedTS, sizeof(Target.CompensatedTS), NULL);
		IS.Read((char*)& Target.UnCompensatedTS, sizeof(Target.UnCompensatedTS), NULL);
		IS.Read((char*)& Target.AlongShipAngle, sizeof(Target.AlongShipAngle), NULL);
		IS.Read((char*)& Target.AthwartShipAngle, sizeof(Target.AthwartShipAngle), NULL);

		SingleTargetDataCW myTarget;
		myTarget.m_targetRange = Target.Range*0.0001;
		/*	if(pMyObject->m_parentSTId<80)
		myTarget.m_targetRange+=6.15;
		*/
		myTarget.m_compensatedTS = Target.CompensatedTS*0.01;
		myTarget.m_unCompensatedTS = Target.UnCompensatedTS*0.01;
		myTarget.m_AlongShipAngleRad = DEG_TO_RAD(Target.AlongShipAngle*0.01);
		myTarget.m_AthwartShipAngleRad = DEG_TO_RAD(Target.AthwartShipAngle*0.01);
		pMyObject->PushSingleTargetDataCW(myTarget);
	}
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	pMyObject->m_tupleAttributes = myHac.tupleAttributes;


	// OTK - FAE214 - si l'algorithme de d�tection des cibles simples est activ�,
	// il faut en ignorer la lecture (la solution initiale de lecture puis suppression
	// par le module de d�tection lui-m�me n'est pas satisfaisante si un tuple 10090
	// arrive apr�s que le pingfan correspondant ait �t� complet�).
	if (!ReaderCtrl::getInstance()->GetReaderParameter().getIgnoreSingleTargets())
	{
		if (pMyObject->GetSingleTargetDataCWCount() != 0)
		{
			SplitBeamPair ref;
			bool found = trav.m_pHacObjectMgr->GetChannelParentSplitBeam(pMyObject->m_parentSTId, ref);

			if (!found)
			{
				std::uint32_t sounderId;
				trav.m_pHacObjectMgr->FindValidSounderContainingChannel(pMyObject->m_parentSTId, sounderId);
				Sounder *sound = trav.m_pHacObjectMgr->GetLastValidSounder(sounderId);

				M3D_LOG_WARN(LoggerName, Log::format("Creating new Default single Target Alg for channel %d", pMyObject->m_parentSTId));
				if (sound)
				{
					SplitBeamParameter *pAlg = SplitBeamParameter::Create();
					pAlg->m_splitBeamParameterChannelId = pMyObject->m_parentSTId;

					MovRef(pAlg);
					pKern->getObjectMgr()->AddSplitBeamParameter(pAlg, pMyObject->m_parentSTId);
					MovUnRefDelete(pAlg);
					trav.m_pHacObjectMgr->GetChannelParentSplitBeam(pMyObject->m_parentSTId, ref);
				}
				else
				{
					M3D_LOG_WARN(LoggerName, Log::format("cannot find channel %d", pMyObject->m_parentSTId));
				}
			}

			Sounder *sound = trav.m_pHacObjectMgr->GetLastValidSounder(ref.m_sounderId);
			if (sound)
			{
				HacTime myDate(pMyObject->m_PingTime.m_TimeCpu, pMyObject->m_PingTime.m_TimeFraction);
				myDate = myDate + sound->m_sounderComputeData.m_timeShift;
				pMyObject->m_PingTime = myDate;
				pMyObject->SetSounderRef(sound);
				trav.m_pHacObjectMgr->GetPingFanContainer().PushSingleTarget(pMyObject);
				trav.SingleTargetAdded(sound->m_SounderId);
			}
			else
			{
				//NMD - FAE 100 - passage de error � warning        
				if (m_logwarnCount == 0)
					M3D_LOG_WARN(LoggerName, "Unknow sonder, dropping Data");
				m_logwarnCount++;
			}
		}
		else 
		{
			M3D_LOG_WARN(LoggerName, "Empty Single Target Received");
		}
	}

	MovUnRefDelete(pMyObject);
}

std::uint32_t Tup10090::Encode(MovStream & IS, PingFan *aPingFan, unsigned short aChanId)
{
	unsigned short tupleCode = 10090;
	std::uint32_t backlink = 0;

	// r�cup�ration du PingFanSingleTarget du pingFan
	M3DKernel *pKern = M3DKernel::GetInstance();
	PingFanSingleTarget *pSingle = pKern->getObjectMgr()->GetPingFanContainer()
		.GetPingFanSingleTarget(aPingFan->m_ObjectTime, aPingFan->getSounderRef()->m_SounderId);

	// s'il n'y a pas de singletarget, on sort.
	if (!pSingle)
		return 0;

	// un tuple pour chaque PingFanSingleTarget : on a besoin d'un offset pour boucler
	// et ne pas overwriter les PingFanSingleTarget pr�c�dents.
	unsigned int offset = 0;
	unsigned int nbSingleTarget = pSingle->GetNumberOfTarget();
	for (unsigned int noSingleTarget = 0; noSingleTarget < nbSingleTarget; noSingleTarget++)
	{
		SingleTargetDataObject * stDataObj = pSingle->GetTarget(noSingleTarget);
		if (stDataObj->m_parentSTId == aChanId)
		{
			backlink = 36;

			std::uint32_t nbSamples = stDataObj->GetSingleTargetDataCWCount();

			// 12 = sizeof(Hac10090Target) dans le cas ou il y aurait un #pragma pack(1) dans la definition
			// de la structure !
			IS.GetConservativeWriteBuffer(offset + 36 + 4 + 4 + nbSamples * 12);

			libMove posit;
			posit.LowPart = offset + 36;
			IS.Seek(posit, eSEEK_SET);            // on se place de fa�on a �crire les targets

			for (unsigned int noSample = 0; noSample < nbSamples; noSample++)
			{
				// mise en forme des donn�es
				SingleTargetDataCW sampleData = stDataObj->GetSingleTargetDataCW(noSample);
				Hac10090Target target;
				target.AlongShipAngle = (short)round(100 * RAD_TO_DEG(sampleData.m_AlongShipAngleRad));
				target.AthwartShipAngle = (short)round(100 * RAD_TO_DEG(sampleData.m_AthwartShipAngleRad));
				target.UnCompensatedTS = (short)round(100 * sampleData.m_unCompensatedTS);
				target.CompensatedTS = (short)round(100 * sampleData.m_compensatedTS);
				target.Range = (std::int32_t)round(10000 * sampleData.m_targetRange);

				// �criture des donn�es dans le flux
				IS.Write((char*)& target.Range, sizeof(target.Range), NULL);
				IS.Write((char*)& target.CompensatedTS, sizeof(target.CompensatedTS), NULL);
				IS.Write((char*)& target.UnCompensatedTS, sizeof(target.UnCompensatedTS), NULL);
				IS.Write((char*)& target.AlongShipAngle, sizeof(target.AlongShipAngle), NULL);
				IS.Write((char*)& target.AthwartShipAngle, sizeof(target.AthwartShipAngle), NULL);
			}

			std::int32_t tupleAttribute = 1;
			backlink = 36 + 4 + 4 + nbSamples * 12;
			IS.Write((char*)&tupleAttribute, sizeof(tupleAttribute), NULL);
			IS.Write((char*)&backlink, sizeof(backlink), NULL);

			// on se replace afin d'�crire les donn�es statiques du tuple
			posit;
			posit.LowPart = offset + 0;
			IS.Seek(posit, eSEEK_SET);

			std::uint32_t tupleSize = backlink - 10;

			IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
			IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

			IS.Write((char*)& aPingFan->m_ObjectTime.m_TimeFraction, sizeof(aPingFan->m_ObjectTime.m_TimeFraction), NULL);
			IS.Write((char*)& aPingFan->m_ObjectTime.m_TimeCpu, sizeof(aPingFan->m_ObjectTime.m_TimeCpu), NULL);
			IS.Write((char*)& stDataObj->m_parentSTId, sizeof(stDataObj->m_parentSTId), NULL);
			short space = 0;
			IS.Write((char*)& space, sizeof(space), NULL);
			IS.Write((char*)& stDataObj->m_pingNumber, sizeof(stDataObj->m_pingNumber), NULL);
			std::uint32_t bott = (std::uint32_t)(stDataObj->m_selectStartRange * 10000);
			IS.Write((char*)& bott, sizeof(bott), NULL);
			bott = (std::uint32_t)(stDataObj->m_selectEndRange * 10000);
			IS.Write((char*)& bott, sizeof(bott), NULL);
			bott = (std::uint32_t)(stDataObj->m_detectedBottom * 10000);
			IS.Write((char*)& bott, sizeof(bott), NULL);
			IS.Write((char*)& nbSamples, sizeof(nbSamples), NULL);

			offset += backlink;
		}
	}

	return offset;
}
