/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup10100.cpp												  */
/******************************************************************************/

#include "Reader/Tup10100.h"

#include <fstream>
#include <assert.h>


#include "M3DKernel/datascheme/NavAttributes.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/datascheme/Threshold.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup10100::Tup10100()
{

}

void Tup10100::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	libMove essai;
	essai.LowPart = 0;

	Hac10100 myHac;
	IS.Seek(essai, eSEEK_CUR);



	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.SoftwareChannelID, sizeof(myHac.SoftwareChannelID), NULL);
	IS.Read((char*)& myHac.TVGMaxRange, sizeof(myHac.TVGMaxRange), NULL);
	IS.Read((char*)& myHac.TVGMinRange, sizeof(myHac.TVGMinRange), NULL);
	IS.Read((char*)& myHac.TVTEvaluationMode, sizeof(myHac.TVTEvaluationMode), NULL);
	IS.Read((char*)& myHac.TVTEvaluationInterval, sizeof(myHac.TVTEvaluationInterval), NULL);
	IS.Read((char*)& myHac.TVTEvaluationNumberOfPing, sizeof(myHac.TVTEvaluationNumberOfPing), NULL);
	IS.Read((char*)& myHac.TVTStartPingNumber, sizeof(myHac.TVTStartPingNumber), NULL);
	IS.Read((char*)& myHac.TVTParameter, sizeof(myHac.TVTParameter), NULL);
	IS.Read((char*)& myHac.TVTAmpParameter, sizeof(myHac.TVTAmpParameter), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);




	Threshold *p = Threshold::Create();

	MovRef(p);
	p->m_ObjectTime.m_TimeCpu = myHac.TimeCpu;
	p->m_ObjectTime.m_TimeFraction = myHac.TimeFraction;

	p->m_SoftwareChannelID = myHac.SoftwareChannelID;
	p->m_TVGMaxRange = myHac.TVGMaxRange*0.1;
	p->m_TVGMinRange = myHac.TVGMinRange*0.1;
	p->m_TVTEvaluationMode = myHac.TVTEvaluationMode;
	p->m_TVTEvaluationInterval = myHac.TVTEvaluationInterval;
	p->m_TVTEvaluationNumberOfPing = myHac.TVTEvaluationNumberOfPing;
	p->m_TVTStartPingNumber = myHac.TVTStartPingNumber;
	p->m_TVTParameter = myHac.TVTParameter*0.000001;
	p->m_TVTAmpParameter = myHac.TVTAmpParameter*0.000001;
	p->m_tupleAttributes = myHac.tupleAttributes;

	std::uint32_t sounderId;
	bool found = trav.m_pHacObjectMgr->FindWorkingSounderContainingChannel(myHac.SoftwareChannelID, sounderId);
	Sounder	*pSounder = NULL;
	if (found)
	{
		pSounder = trav.m_pHacObjectMgr->GetWorkingSounder(sounderId);
	}
	else {
		found = trav.m_pHacObjectMgr->FindValidSounderContainingChannel(myHac.SoftwareChannelID, sounderId);
		if (found)
		{
			pSounder = trav.m_pHacObjectMgr->GetWorkingSounder(sounderId);
		}
	}
	if (pSounder)
	{
		Transducer *pTrans = pSounder->getTransducerForChannel(myHac.SoftwareChannelID);
		if (pTrans)
		{
			SoftChannel *pChan = pTrans->getSoftChannel(myHac.SoftwareChannelID);
			if (pChan)
			{
				pChan->SetThreshold(p);
				trav.TupleHeaderUpdate();
			}
		}
		assert(pTrans);
	}

	//	assert(pSounder);


	MovUnRefDelete(p);
}


std::uint32_t Tup10100::Encode(MovStream & IS, Threshold *pThres)
{


	Hac10100 myHac;
	std::uint32_t tupleSize = 34;
	unsigned short tupleCode = 10100;
	std::uint32_t backlink = 44;

	myHac.TimeCpu = pThres->m_ObjectTime.m_TimeCpu;
	myHac.TimeFraction = pThres->m_ObjectTime.m_TimeFraction;

	myHac.SoftwareChannelID = pThres->m_SoftwareChannelID;
	myHac.TVGMaxRange = (unsigned short)round(pThres->m_TVGMaxRange / 0.1);
	myHac.TVGMinRange = (unsigned short)round(pThres->m_TVGMinRange / 0.1);
	myHac.TVTEvaluationMode = pThres->m_TVTEvaluationMode;
	myHac.TVTEvaluationInterval = pThres->m_TVTEvaluationInterval;
	myHac.TVTEvaluationNumberOfPing = pThres->m_TVTEvaluationNumberOfPing;
	myHac.TVTStartPingNumber = pThres->m_TVTStartPingNumber;
	myHac.TVTParameter = (std::int32_t)round(pThres->m_TVTParameter / 0.000001);
	myHac.TVTAmpParameter = (std::uint32_t)round(pThres->m_TVTAmpParameter / 0.000001);
	myHac.tupleAttributes = pThres->m_tupleAttributes;



	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Write((char*)& myHac.SoftwareChannelID, sizeof(myHac.SoftwareChannelID), NULL);
	IS.Write((char*)& myHac.TVGMaxRange, sizeof(myHac.TVGMaxRange), NULL);
	IS.Write((char*)& myHac.TVGMinRange, sizeof(myHac.TVGMinRange), NULL);
	IS.Write((char*)& myHac.TVTEvaluationMode, sizeof(myHac.TVTEvaluationMode), NULL);
	IS.Write((char*)& myHac.TVTEvaluationInterval, sizeof(myHac.TVTEvaluationInterval), NULL);
	IS.Write((char*)& myHac.TVTEvaluationNumberOfPing, sizeof(myHac.TVTEvaluationNumberOfPing), NULL);
	IS.Write((char*)& myHac.TVTStartPingNumber, sizeof(myHac.TVTStartPingNumber), NULL);
	IS.Write((char*)& myHac.TVTParameter, sizeof(myHac.TVTParameter), NULL);
	IS.Write((char*)& myHac.TVTAmpParameter, sizeof(myHac.TVTAmpParameter), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;

}
