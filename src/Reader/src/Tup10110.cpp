/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup10110.cpp												  */
/******************************************************************************/

#include "Reader/Tup10110.h"

#include <fstream>
#include <assert.h>


#include "M3DKernel/datascheme/NavAttributes.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/datascheme/EventMarker.h"

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup10110::Tup10110()
{

}

void Tup10110::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{

	libMove essai;
	essai.LowPart = 0;

	Hac10110 myHac;
	IS.Seek(essai, eSEEK_CUR);



	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	EventMarker *p = EventMarker::Create();

	p->m_ObjectTime.m_TimeCpu = myHac.TimeCpu;
	p->m_ObjectTime.m_TimeFraction = myHac.TimeFraction;



	///read Comments
	std::uint32_t sizeComment = taille_tup - 10;
	if (sizeComment > 0)
	{
		p->Allocate(sizeComment);
		IS.Read(p->m_Msg, sizeComment, NULL);
	}

	M3D_LOG_HAC("Reader.Tup10110", p->m_Msg);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	p->m_tupleAttributes = myHac.tupleAttributes;

	trav.m_pHacObjectMgr->GetEventMarkerContainer()->AddObject(p);
}


std::uint32_t Tup10110::Encode(MovStream & IS, EventMarker *pEvent)
{
	unsigned short tupleCode = 10110;


	Hac10110 myHac;
	myHac.TimeCpu = pEvent->m_ObjectTime.m_TimeCpu;
	myHac.TimeFraction = pEvent->m_ObjectTime.m_TimeFraction;
	myHac.tupleAttributes = 0;//pEvent->m_tupleAttributes;
	size_t taille = strlen(pEvent->m_Msg);

	// Dans HERMES : nbChar = (UInt32)(((Math.Floor((Comment.Length)/4.0)+1)*4.0));
	size_t nbChar = ((int)(taille / 4.0) + 1) * 4;


	char *Mesg = new char[nbChar];
	std::uint32_t backlink = 12 + 8 + nbChar;

	std::uint32_t tupleSize = backlink - 10;
	// OTK - FAE179 - utilisation de strncpy plut�t que strcpy_s pour remplir de zeros apr�s la fin de la cha�ne
	// (sinon on a des caract�res non initialis�s qui compliquent la comparaison bit � bit apr�s r��criture du fichier)
	strncpy(Mesg, pEvent->m_Msg, nbChar);

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);
	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);

	IS.Write(Mesg, nbChar * sizeof(char), NULL);

	IS.Write((char*)&myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);
	IS.Write((char*)&backlink, sizeof(backlink), NULL);

	delete Mesg;
	return backlink;
}
