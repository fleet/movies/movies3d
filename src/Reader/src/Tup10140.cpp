/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup10140.cpp												  */
/******************************************************************************/

#include "Reader/Tup10140.h"

#include <fstream>
#include <assert.h>



#include "M3DKernel/datascheme/NavAttributes.h"


#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/Platform.h"


/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup10140::Tup10140()
{

}

void Tup10140::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	libMove essai;
	essai.LowPart = 0;
	IS.Seek(essai, eSEEK_CUR);

	Hac10140 myHac;


	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.AttitudeSensorId, sizeof(myHac.AttitudeSensorId), NULL);
	IS.Read((char*)& myHac.Pitch, sizeof(myHac.Pitch), NULL);
	IS.Read((char*)& myHac.Roll, sizeof(myHac.Roll), NULL);
	IS.Read((char*)& myHac.Heave, sizeof(myHac.Heave), NULL);
	IS.Read((char*)& myHac.Yaw, sizeof(myHac.Yaw), NULL);
	IS.Read((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);


	NavAttitude *pNavAttitude = NavAttitude::Create();

	pNavAttitude->m_source = eTupleSource;
	//pNavAttitude->m_heaveMeter = myHac.Heave*0.01;
	pNavAttitude->m_pitchRad = DEG_TO_RAD(((double)myHac.Pitch)*0.1);
	pNavAttitude->m_rollRad = DEG_TO_RAD(((double)myHac.Roll)*0.1);
	if (myHac.Yaw != -32768)
	{
		pNavAttitude->m_yawRad = DEG_TO_RAD(((double)myHac.Yaw)*0.1);
	}
	else
	{
		pNavAttitude->m_yawRad = 0;
		pNavAttitude->m_bDefaultYaw = true;
	}

	pNavAttitude->m_ObjectTime.m_TimeFraction = myHac.TimeFraction;
	pNavAttitude->m_ObjectTime.m_TimeCpu = myHac.TimeCpu;


	// compensate time shift
	for (unsigned int index = 0; index < trav.m_pHacObjectMgr->GetSounderDefinition().GetNbSounder(); index++)
	{
		Sounder *pSound = trav.m_pHacObjectMgr->GetSounderDefinition().GetSounder(index);
		for (unsigned int transIndex = 0; transIndex < pSound->GetTransducerCount(); transIndex++)
		{
			Transducer *ptrans = pSound->GetTransducer(transIndex);
			if (ptrans->GetPlatform()->m_attitudeSensorId == myHac.AttitudeSensorId)
				pNavAttitude->m_ObjectTime = pNavAttitude->m_ObjectTime + pSound->m_sounderComputeData.m_timeShift;
			if (pSound->IsHeaveCM())
			{
				pNavAttitude->m_heaveMeter = myHac.Heave*0.1;
				pNavAttitude->m_heaveCM = true;
			}
			else
			{
				pNavAttitude->m_heaveMeter = myHac.Heave*0.01;
				pNavAttitude->m_heaveCM = false;
			}
				
		}
	}


	pNavAttitude->m_AttitudeSensorId = myHac.AttitudeSensorId;
	pNavAttitude->m_tupleAttributes = myHac.tupleAttributes;
	pNavAttitude->m_AttitudeSensorId = myHac.AttitudeSensorId;


	TimeObjectContainer *pContainer = trav.m_pHacObjectMgr->GetNavAttitudeContainer(myHac.AttitudeSensorId);
	pContainer->AddObject(pNavAttitude);
}



std::uint32_t Tup10140::Encode(MovStream & IS, NavAttitude*pNavAttitude)
{
	Hac10140		myHac;
	std::uint32_t tupleSize = 22;
	unsigned short tupleCode = 10140;
	std::uint32_t backlink = 32;

	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);
	
	if (pNavAttitude->m_heaveCM)
		myHac.Heave = (short)round(pNavAttitude->m_heaveMeter*10.0);
	else
		myHac.Heave = (short)round(pNavAttitude->m_heaveMeter*100.0);

	myHac.Pitch = (short)round(RAD_TO_DEG(pNavAttitude->m_pitchRad)*10.0);
	myHac.Roll = (short)round(10.0*RAD_TO_DEG(pNavAttitude->m_rollRad));
	if (pNavAttitude->m_bDefaultYaw)
	{
		myHac.Yaw = -32768;
	}
	else
	{
		myHac.Yaw = (short)round(10.0*RAD_TO_DEG(pNavAttitude->m_yawRad));
	}

	myHac.TimeFraction = pNavAttitude->m_ObjectTime.m_TimeFraction;

	myHac.TimeCpu = pNavAttitude->m_ObjectTime.m_TimeCpu;
	myHac.AttitudeSensorId = pNavAttitude->m_AttitudeSensorId;
	myHac.tupleAttributes = pNavAttitude->m_tupleAttributes;
	myHac.AttitudeSensorId = pNavAttitude->m_AttitudeSensorId;
	myHac.space = 0;

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Write((char*)& myHac.AttitudeSensorId, sizeof(myHac.AttitudeSensorId), NULL);
	IS.Write((char*)& myHac.Pitch, sizeof(myHac.Pitch), NULL);
	IS.Write((char*)& myHac.Roll, sizeof(myHac.Roll), NULL);
	IS.Write((char*)& myHac.Heave, sizeof(myHac.Heave), NULL);
	IS.Write((char*)& myHac.Yaw, sizeof(myHac.Yaw), NULL);
	IS.Write((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);


	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}
