/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup10142.cpp												  */
/******************************************************************************/

#include "Reader/Tup10142.h"

#include <fstream>
#include <assert.h>
#include "BaseMathLib/Vector3.h"
#include "M3DKernel/datascheme/Trawl.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
using namespace BaseMathLib;
/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/


Tup10142::Tup10142()
{

}





void Tup10142::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{


	libMove essai;
	essai.LowPart = 0;



	Hac10142 myHac;
	IS.Seek(essai, eSEEK_CUR);



	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.DistanceSensorID, sizeof(myHac.DistanceSensorID), NULL);
	IS.Read((char*)& myHac.DepthSensorId, sizeof(myHac.DepthSensorId), NULL);
	IS.Read((char*)& myHac.AlongshipDistance, sizeof(myHac.AlongshipDistance), NULL);
	IS.Read((char*)& myHac.AthwartShipDistance, sizeof(myHac.AthwartShipDistance), NULL);
	IS.Read((char*)& myHac.Depth, sizeof(myHac.Depth), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);


	Vector3D p;

	TrawlPlatformPosition  *mySensor = TrawlPlatformPosition::Create();
	mySensor->m_ObjectTime.m_TimeCpu = myHac.TimeCpu;
	mySensor->m_ObjectTime.m_TimeFraction = myHac.TimeFraction;

	mySensor->m_SensorId.m_depthId = myHac.DepthSensorId;
	mySensor->m_SensorId.m_distanceId = myHac.DistanceSensorID;

	p.z = myHac.AlongshipDistance*0.0001;
	p.x = myHac.AthwartShipDistance*0.0001;
	p.y = myHac.Depth*-0.0001;
	mySensor->m_Offset = p;
	mySensor->tupleAttributes = myHac.tupleAttributes;
	TimeObjectContainer *objCont = trav.m_pHacObjectMgr->GetTrawl()->GetPlatformPositionContainer(mySensor->m_SensorId);
	objCont->AddObject(mySensor);

}
std::uint32_t Tup10142::Encode(MovStream & IS, TrawlPlatformPosition *pPlatPos)
{
	if (!pPlatPos)
		return 0;

	Hac10142 myHac;
	std::uint32_t tupleSize = 26;
	unsigned short tupleCode = 10142;
	std::uint32_t backlink = 36;
	IS.GetWriteBuffer(backlink);

	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);


	myHac.TimeFraction = pPlatPos->m_ObjectTime.m_TimeFraction;
	myHac.TimeCpu = pPlatPos->m_ObjectTime.m_TimeCpu;
	myHac.DistanceSensorID = pPlatPos->m_SensorId.m_distanceId;
	myHac.DepthSensorId = pPlatPos->m_SensorId.m_depthId;
	myHac.AlongshipDistance = (std::int32_t)round(pPlatPos->m_Offset.z / 0.0001);
	myHac.AthwartShipDistance = (std::int32_t)round(pPlatPos->m_Offset.x / 0.0001);
	myHac.Depth = (std::int32_t)round(pPlatPos->m_Offset.y / (-0.0001));

	myHac.tupleAttributes = pPlatPos->tupleAttributes;

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Write((char*)& myHac.DistanceSensorID, sizeof(myHac.DistanceSensorID), NULL);
	IS.Write((char*)& myHac.DepthSensorId, sizeof(myHac.DepthSensorId), NULL);
	IS.Write((char*)& myHac.AlongshipDistance, sizeof(myHac.AlongshipDistance), NULL);
	IS.Write((char*)& myHac.AthwartShipDistance, sizeof(myHac.AthwartShipDistance), NULL);
	IS.Write((char*)& myHac.Depth, sizeof(myHac.Depth), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);


	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;

}
