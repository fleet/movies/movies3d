/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup11000.cpp												  */
/******************************************************************************/

#include "Reader/Tup11000.h"

#include <fstream>
#include <assert.h>



#include "M3DKernel/datascheme/Environnement.h"


#include "M3DKernel/datascheme/HacObjectMgr.h"

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup11000::Tup11000()
{

}









void Tup11000::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{

	libMove essai;
	essai.LowPart = 0;
	IS.Seek(essai, eSEEK_CUR);

	Hac11000 myHac;



	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.SensorType, sizeof(myHac.SensorType), NULL);
	IS.Read((char*)& myHac.NumberOfMeasure, sizeof(myHac.NumberOfMeasure), NULL);


	Environnement *pEnv = Environnement::Create();
	pEnv->m_ObjectTime.m_TimeFraction = myHac.TimeFraction;
	pEnv->m_ObjectTime.m_TimeCpu = myHac.TimeCpu;
	pEnv->m_SensorType = myHac.SensorType;
	//	pEnv->m_NumberOfMeasure=myHac.NumberOfMeasure;

	for (unsigned int i = 0; i < myHac.NumberOfMeasure; i++)
	{
		Measure mes;
		Hac11000Record record;
		IS.Read((char*)& record.Pressure, sizeof(record.Pressure), NULL);
		IS.Read((char*)& record.Temperature, sizeof(record.Temperature), NULL);
		IS.Read((char*)& record.Conductivity, sizeof(record.Conductivity), NULL);
		IS.Read((char*)& record.SpeedOfSound, sizeof(record.SpeedOfSound), NULL);

		IS.Read((char*)& record.DepthFromSurface, sizeof(record.DepthFromSurface), NULL);

		IS.Read((char*)& record.Salinity, sizeof(record.Salinity), NULL);
		IS.Read((char*)& record.SoundAbsorption, sizeof(record.SoundAbsorption), NULL);


		mes.Pressure = record.Pressure*0.001;
		mes.Temperature = record.Temperature*0.0001;
		mes.Conductivity = record.Conductivity*0.001;
		mes.SpeedOfSound = record.SpeedOfSound*0.1;
		mes.DepthFromSurface = record.DepthFromSurface*0.0001;
		mes.Salinity = record.Salinity*0.001;
		mes.SoundAbsorption = record.SoundAbsorption;


		pEnv->AddMeasure(mes);

	}


	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);
	pEnv->m_tupleAttributes = myHac.tupleAttributes;



	TimeObjectContainer *pContainer = trav.m_pHacObjectMgr->GetEnvironnementContainer();
	pContainer->AddObject(pEnv);
}



std::uint32_t Tup11000::Encode(MovStream & IS, Environnement*pEnv)
{
	if (!pEnv)
		return 0;

	Hac11000		myHac;
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture



	myHac.TimeFraction = pEnv->m_ObjectTime.m_TimeFraction;
	myHac.TimeCpu = pEnv->m_ObjectTime.m_TimeCpu;
	myHac.SensorType = pEnv->m_SensorType;
	myHac.NumberOfMeasure = pEnv->GetNumberOfMeasure();
	myHac.tupleAttributes = 0;//pEnv->m_tupleAttributes;


	std::uint32_t tupleSize = 10/*Start*/ + 4/*Tuple Attibute*/ + 24 * pEnv->GetNumberOfMeasure();
	unsigned short tupleCode = 11000;
	std::uint32_t backlink = tupleSize + 10;

	IS.GetWriteBuffer(backlink);


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Write((char*)& myHac.SensorType, sizeof(myHac.SensorType), NULL);
	IS.Write((char*)& myHac.NumberOfMeasure, sizeof(myHac.NumberOfMeasure), NULL);
	for (unsigned int i = 0; i < myHac.NumberOfMeasure; i++)
	{
		Hac11000Record record;
		Measure *mes = pEnv->GetMeasure(i);

		record.Pressure = (std::uint32_t)round(mes->Pressure / 0.001);
		record.Temperature = (std::int32_t)round(mes->Temperature / 0.0001);
		record.Conductivity = (unsigned short)round(mes->Conductivity / 0.001);
		record.SpeedOfSound = (unsigned short)round(mes->SpeedOfSound / 0.1);
		record.DepthFromSurface = (std::uint32_t)round(mes->DepthFromSurface / 0.0001);
		record.Salinity = (std::uint32_t)round(mes->Salinity / 0.001);
		record.SoundAbsorption = mes->SoundAbsorption;

		IS.Write((char*)& record.Pressure, sizeof(record.Pressure), NULL);
		IS.Write((char*)& record.Temperature, sizeof(record.Temperature), NULL);
		IS.Write((char*)& record.Conductivity, sizeof(record.Conductivity), NULL);
		IS.Write((char*)& record.SpeedOfSound, sizeof(record.SpeedOfSound), NULL);

		IS.Write((char*)& record.DepthFromSurface, sizeof(record.DepthFromSurface), NULL);

		IS.Write((char*)& record.Salinity, sizeof(record.Salinity), NULL);
		IS.Write((char*)& record.SoundAbsorption, sizeof(record.SoundAbsorption), NULL);
	}

	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);
	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}
