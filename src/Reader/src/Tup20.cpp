/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup20.cpp												  */
/******************************************************************************/

#include "Reader/Tup20.h"

#include <fstream>
#include <assert.h>

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/NavAttributes.h"


/******************************************************************************/
/*	Statics																	  */
/******************************************************************************/


/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup20::Tup20()
{

}




void Tup20::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{

	libMove essai;
	essai.LowPart = 0;

	Hac20 myHac;
	IS.Seek(essai, eSEEK_CUR);


	//	std::int32_t, position 20

	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.GpsTime, sizeof(myHac.GpsTime), NULL);
	IS.Read((char*)& myHac.PositionningSystem, sizeof(myHac.PositionningSystem), NULL);
	IS.Read((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Read((char*)& myHac.Latitude, sizeof(myHac.Latitude), NULL);
	IS.Read((char*)& myHac.Longitude, sizeof(myHac.Longitude), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	NavPosition *p = NavPosition::Create();
	MovRef(p);


	p->m_lattitudeDeg = myHac.Latitude*0.000001;
	p->m_longitudeDeg = myHac.Longitude*0.000001;

	p->m_gpsTime = myHac.GpsTime;
	p->m_positionningSystem = myHac.PositionningSystem;
	p->m_tupleAttributes = myHac.tupleAttributes;
	p->m_ObjectTime.m_TimeFraction = myHac.TimeFraction;
	p->m_ObjectTime.m_TimeCpu = myHac.TimeCpu;
	p->m_source = eTupleSource;


	trav.m_pHacObjectMgr->GetNavPositionContainer()->AddObject(p);


	// FAE 86 - calcul des informations de navigations (si necessaire)
	trav.m_pHacObjectMgr->ComputeNavAttributes(p);


	MovUnRefDelete(p);
}


std::uint32_t Tup20::Encode(MovStream & IS, NavPosition *p)
{
	if (!p)
		return 0;

	Hac20 myHac;
	std::uint32_t tupleSize = 26;
	unsigned short tupleCode = 20;
	std::uint32_t backlink = 36;
	IS.GetWriteBuffer(backlink);

	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);

	myHac.TimeFraction = p->m_ObjectTime.m_TimeFraction;
	myHac.TimeCpu = p->m_ObjectTime.m_TimeCpu;

	myHac.GpsTime = p->m_gpsTime;
	myHac.PositionningSystem = p->m_positionningSystem;
	myHac.space = 0;
	myHac.Latitude = (std::int32_t)round(p->m_lattitudeDeg / 0.000001);
	myHac.Longitude = (std::int32_t)round(p->m_longitudeDeg / 0.000001);
	myHac.tupleAttributes = p->m_tupleAttributes;


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Write((char*)& myHac.GpsTime, sizeof(myHac.GpsTime), NULL);
	IS.Write((char*)& myHac.PositionningSystem, sizeof(myHac.PositionningSystem), NULL);
	IS.Write((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Write((char*)& myHac.Latitude, sizeof(myHac.Latitude), NULL);
	IS.Write((char*)& myHac.Longitude, sizeof(myHac.Longitude), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);


	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;

}
