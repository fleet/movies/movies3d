/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup200.cpp												  */
/******************************************************************************/

#include "Reader/Tup200.h"

#include <fstream>
#include <assert.h>

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/Transducer.h"
/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup200::Tup200()
{

}


void Tup200::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{

	libMove posit;
	posit.LowPart = 0;


	Hac200 myHac;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture
	//unsigned short

	IS.Read((char*)& myHac.numOfChannel, sizeof(myHac.numOfChannel), NULL);
	IS.Read((char*)& myHac.docId, sizeof(myHac.docId), NULL);
	IS.Read((char*)& myHac.soundSpeed, sizeof(myHac.soundSpeed), NULL);
	IS.Read((char*)& myHac.pingMode, sizeof(myHac.pingMode), NULL);
	IS.Read((char*)& myHac.pingInterval, sizeof(myHac.pingInterval), NULL);
	IS.Read((char*)& myHac.transmitPower, sizeof(myHac.transmitPower), NULL);
	IS.Read((char*)& myHac.noiseMargin, sizeof(myHac.noiseMargin), NULL);
	IS.Read((char*)& myHac.sampleRange, sizeof(myHac.sampleRange), NULL);
	IS.Read((char*)& myHac.superLayerType, sizeof(myHac.superLayerType), NULL);
	IS.Read((char*)& myHac.superLayerNumber, sizeof(myHac.superLayerNumber), NULL);
	IS.Read((char*)& myHac.superLayerRange, sizeof(myHac.superLayerRange), NULL);
	IS.Read((char*)& myHac.superLayerStart, sizeof(myHac.superLayerStart), NULL);
	IS.Read((char*)& myHac.superLayerMargin, sizeof(myHac.superLayerMargin), NULL);
	IS.Read((char*)& myHac.superLayerThreshold, sizeof(myHac.superLayerThreshold), NULL);
	IS.Read((char*)& myHac.ek500version, sizeof(myHac.ek500version), NULL);
	IS.Read((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	SounderEk500 *p = SounderEk500::Create();

	p->m_SounderId = myHac.docId;
	p->m_soundVelocity = myHac.soundSpeed*0.1;
	p->m_triggerMode = 1;
	p->m_numberOfTransducer = myHac.numOfChannel;
	p->m_pingInterval = myHac.pingInterval*0.01;
	p->m_isMultiBeam = false;
	p->m_tupleType = 200;
	p->SetRemarks(myHac.remarks);

	p->pingMode = myHac.pingMode;
	p->transmitPower = myHac.transmitPower;
	p->noiseMargin = myHac.noiseMargin;
	p->sampleRange = myHac.sampleRange;
	p->superLayerType = myHac.superLayerType;
	p->superLayerNumber = myHac.superLayerNumber;
	p->superLayerRange = myHac.superLayerRange;
	p->superLayerStart = myHac.superLayerStart;
	p->superLayerMargin = myHac.superLayerMargin;
	p->superLayerThreshold = myHac.superLayerThreshold;
	p->ek500version = myHac.ek500version;
	p->DataChanged();

	MovRef(p);

	trav.m_pHacObjectMgr->AddSounder(p);
	trav.TupleHeaderUpdate();


	MovUnRefDelete(p);


}
std::uint32_t Tup200::Encode(MovStream & IS, SounderEk500 *p)
{
	Hac200 myHac;
	std::uint32_t tupleSize = 70;
	unsigned short tupleCode = 200;
	std::uint32_t backlink = 80;

	IS.GetWriteBuffer(backlink);

	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture

	myHac.docId = p->m_SounderId;
	myHac.soundSpeed = (unsigned short)round(p->m_soundVelocity / 0.1);
	myHac.numOfChannel = p->m_numberOfTransducer;
	myHac.pingInterval = (unsigned short)round(p->m_pingInterval / 0.01);

	strncpy(myHac.remarks, p->GetRemarks(), sizeof(p->GetRemarks()));
	myHac.tupleAttributes = 1;

	myHac.pingMode = p->pingMode;
	myHac.transmitPower = p->transmitPower;
	myHac.noiseMargin = p->noiseMargin;
	myHac.sampleRange = p->sampleRange;
	myHac.superLayerType = p->superLayerType;
	myHac.superLayerNumber = p->superLayerNumber;
	myHac.superLayerRange = p->superLayerRange;
	myHac.superLayerStart = p->superLayerStart;
	myHac.superLayerMargin = p->superLayerMargin;
	myHac.superLayerThreshold = p->superLayerThreshold;
	myHac.ek500version = p->ek500version;


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.numOfChannel, sizeof(myHac.numOfChannel), NULL);
	IS.Write((char*)& myHac.docId, sizeof(myHac.docId), NULL);
	IS.Write((char*)& myHac.soundSpeed, sizeof(myHac.soundSpeed), NULL);
	IS.Write((char*)& myHac.pingMode, sizeof(myHac.pingMode), NULL);
	IS.Write((char*)& myHac.pingInterval, sizeof(myHac.pingInterval), NULL);
	IS.Write((char*)& myHac.transmitPower, sizeof(myHac.transmitPower), NULL);
	IS.Write((char*)& myHac.noiseMargin, sizeof(myHac.noiseMargin), NULL);
	IS.Write((char*)& myHac.sampleRange, sizeof(myHac.sampleRange), NULL);
	IS.Write((char*)& myHac.superLayerType, sizeof(myHac.superLayerType), NULL);
	IS.Write((char*)& myHac.superLayerNumber, sizeof(myHac.superLayerNumber), NULL);
	IS.Write((char*)& myHac.superLayerRange, sizeof(myHac.superLayerRange), NULL);
	IS.Write((char*)& myHac.superLayerStart, sizeof(myHac.superLayerStart), NULL);
	IS.Write((char*)& myHac.superLayerMargin, sizeof(myHac.superLayerMargin), NULL);
	IS.Write((char*)& myHac.superLayerThreshold, sizeof(myHac.superLayerThreshold), NULL);
	IS.Write((char*)& myHac.ek500version, sizeof(myHac.ek500version), NULL);
	IS.Write((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}



