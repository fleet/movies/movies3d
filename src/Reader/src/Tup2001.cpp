/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup2001.cpp												  */
/******************************************************************************/

#include "Reader/Tup2001.h"

#include <fstream>
#include <assert.h>
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/utils/M3DStdUtils.h"

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup2001";
}

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup2001::Tup2001()
{
}

void Tup2001::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	libMove essai;
	essai.LowPart = 0;

	Hac2001 myHac;
	IS.Seek(essai, eSEEK_CUR);

	IS.Read((char*)& myHac.ChannelId, sizeof(myHac.ChannelId), NULL);
	IS.Read((char*)& myHac.docId, sizeof(myHac.docId), NULL);
	IS.Read((char*)& myHac.SamplingInterval, sizeof(myHac.SamplingInterval), NULL);
	IS.Read((char*)& myHac.dataType, sizeof(myHac.dataType), NULL);
	IS.Read((char*)& myHac.transceiverChannelNumber, sizeof(myHac.transceiverChannelNumber), NULL);
	IS.Read((char*)& myHac.AcousticFrequency, sizeof(myHac.AcousticFrequency), NULL);
	IS.Read((char*)& myHac.TransDepht, sizeof(myHac.TransDepht), NULL);
	IS.Read((char*)& myHac.blankingRange, sizeof(myHac.blankingRange), NULL);
	IS.Read((char*)& myHac.PlatFormid, sizeof(myHac.PlatFormid), NULL);
	IS.Read((char*)& myHac.TransShape, sizeof(myHac.TransShape), NULL);
	IS.Read((char*)& myHac.TransFaceAlongAngleOffset, sizeof(myHac.TransFaceAlongAngleOffset), NULL);
	IS.Read((char*)& myHac.TransFaceAthwartAngleOffset, sizeof(myHac.TransFaceAthwartAngleOffset), NULL);
	IS.Read((char*)& myHac.RotationAngle, sizeof(myHac.RotationAngle), NULL);
	IS.Read((char*)& myHac.TransMainBeamAlongBeamAngleOffset, sizeof(myHac.TransMainBeamAlongBeamAngleOffset), NULL);
	IS.Read((char*)& myHac.TransMainBeamAthwartAngleOffset, sizeof(myHac.TransMainBeamAthwartAngleOffset), NULL);
	IS.Read((char*)& myHac.AbsorptionSound, sizeof(myHac.AbsorptionSound), NULL);
	IS.Read((char*)& myHac.PulseLengthMode, sizeof(myHac.PulseLengthMode), NULL);
	IS.Read((char*)& myHac.BandWidthMode, sizeof(myHac.BandWidthMode), NULL);
	IS.Read((char*)& myHac.MaximumPower, sizeof(myHac.MaximumPower), NULL);
	IS.Read((char*)& myHac.AngleSensitivity, sizeof(myHac.AngleSensitivity), NULL);
	IS.Read((char*)& myHac.TranAthwartAngleSensitivity, sizeof(myHac.TranAthwartAngleSensitivity), NULL);
	IS.Read((char*)& myHac.Along3DbWidth, sizeof(myHac.Along3DbWidth), NULL);
	IS.Read((char*)& myHac.Athwart3DbWidth, sizeof(myHac.Athwart3DbWidth), NULL);
	IS.Read((char*)& myHac.TwoWayBeamAngle, sizeof(myHac.TwoWayBeamAngle), NULL);
	IS.Read((char*)& myHac.CalibrationTransGain, sizeof(myHac.CalibrationTransGain), NULL);
	IS.Read((char*)& myHac.BottomDetectionMinLevel, sizeof(myHac.BottomDetectionMinLevel), NULL);
	IS.Read((char*)& myHac.BottomDetectionMinDepth, sizeof(myHac.BottomDetectionMinDepth), NULL);
	IS.Read((char*)& myHac.BottomDetectionMaxDepth, sizeof(myHac.BottomDetectionMaxDepth), NULL);
	IS.Read((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Read((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	Sounder	*pSounder = trav.m_pHacObjectMgr->GetWorkingSounder(myHac.docId);

	// OTK - FAE067 - si pas de sondeur en cours de construction, on recopie le sondeur existant
	// et on repart de cet objet pour mettre � jour le channel
	if (pSounder == NULL)
	{
		SounderEk500* pExistingSounder = dynamic_cast<SounderEk500*>(trav.m_pHacObjectMgr->GetSounderDefinition().GetSounderWithId(myHac.docId));
		if (pExistingSounder == NULL)
		{
			// rien � faire : le tuple est perdu
			M3D_LOG_WARN(LoggerName, "Receiving tuple 2001 with no <building> 200");
		}
		else
		{
			// cr�ation et recopie du sondeur existant
			SounderEk500* pNewSounder = SounderEk500::Create();
			*pNewSounder = *pExistingSounder;
			pNewSounder->DataChanged();
			trav.m_pHacObjectMgr->AddSounder(pNewSounder);
			pSounder = pNewSounder;
		}
	}

	if (pSounder)
	{
		// now create the transducer Object 
		Transducer *pTrans = Transducer::Create();
		MovRef(pTrans);

		// OTK - FAE069 - ajout d'un nom pour les channels des tuples 2001
		char TransName[50];
        sprintf(TransName, "EK500%d", myHac.ChannelId);
		strcpy_s(pTrans->m_transName, 50, TransName);
		memset(pTrans->m_transSoftVersion, 0, sizeof(pTrans->m_transSoftVersion));

		pTrans->m_timeSampleInterval = (2.0*myHac.SamplingInterval / pSounder->m_soundVelocity); // time sample interval micro seconds

		pTrans->m_transDepthMeter = (myHac.TransDepht*0.01);
		pTrans->m_platformId = myHac.PlatFormid;
		pTrans->m_transShape = myHac.TransShape;
		pTrans->m_transFaceAlongAngleOffsetRad = DEG_TO_RAD(myHac.TransFaceAlongAngleOffset*0.1);
		pTrans->m_transFaceAthwarAngleOffsetRad = DEG_TO_RAD(myHac.TransFaceAthwartAngleOffset*0.1);
		pTrans->m_transRotationAngleRad = DEG_TO_RAD(myHac.RotationAngle*0.01);

		pTrans->m_pulseDuration = myHac.PulseLengthMode;
		pTrans->m_transPower = myHac.MaximumPower;

		pTrans->m_pulseForm = 65535;  // pulse form 
		pTrans->m_frequencyBeamSpacing = 65535;
		pTrans->m_frequencySpaceShape = 3;
		pTrans->m_numberOfSoftChannel = 1;

		pTrans->DataChanged();

		////
		pSounder->addTransducer(pTrans);

		SoftChannel *p = SoftChannel::Create();
		MovRef(p);

		/// inside data
		p->m_tupleType = 2001;
		p->m_softChannelComputeData.m_groupId = 0;
		p->m_softChannelComputeData.m_isReferenceBeam = false;
		p->m_softChannelComputeData.m_isMultiBeam = false;
		p->m_softChannelComputeData.m_PolarId = 0;

		/// start Copying
		p->m_softChannelId = myHac.ChannelId;
		memset(p->m_channelName, 0, sizeof(p->m_channelName));

		p->m_dataType = myHac.dataType;
		p->m_beamType = 0;
		p->m_acousticFrequency = myHac.AcousticFrequency;
		p->m_startSample = 0;
		if (myHac.TransMainBeamAlongBeamAngleOffset != -32768)
		{
			p->m_mainBeamAlongSteeringAngleRad = DEG_TO_RAD((myHac.TransMainBeamAlongBeamAngleOffset*0.01));
		}
		else
		{
			p->m_mainBeamAlongSteeringAngleRad = 0;
			p->m_bDefaultAlongAngleOffset = true;
		}

		if (myHac.TransMainBeamAthwartAngleOffset != -32768)
		{
			p->m_mainBeamAthwartSteeringAngleRad = DEG_TO_RAD((myHac.TransMainBeamAthwartAngleOffset*0.01));
		}
		else
		{
			p->m_mainBeamAthwartSteeringAngleRad = 0;
			p->m_bDefaultAthwartAngleOffset = true;
		}

		p->m_absorptionCoefHAC = myHac.AbsorptionSound * 100;
		p->m_absorptionCoef = p->m_absorptionCoefHAC;
		p->m_bandWidth = myHac.BandWidthMode;

		p->m_transmissionPower = myHac.MaximumPower;
		p->m_beamAlongAngleSensitivity = (myHac.AngleSensitivity*0.1);
		p->m_beamAthwartAngleSensitivity = (myHac.TranAthwartAngleSensitivity*0.1);
		p->m_beam3dBWidthAlongRad = DEG_TO_RAD(myHac.Along3DbWidth*0.01);
		p->m_beam3dBWidthAthwartRad = DEG_TO_RAD(myHac.Athwart3DbWidth*0.01);
		p->m_beamEquTwoWayAngle = myHac.TwoWayBeamAngle*0.01;
		p->m_beamGain = myHac.CalibrationTransGain*0.01;
		p->m_beamSACorrection = 0;
		p->m_bottomDetectionMinDepth = myHac.BottomDetectionMinDepth*0.01;
		p->m_bottomDetectionMaxDepth = myHac.BottomDetectionMaxDepth*0.01;
		p->m_bottomDetectionMinLevel = myHac.BottomDetectionMinLevel*0.01;
		p->m_AlongTXRXWeightId = 65535;
		p->m_AthwartTXRXWeightId = 65535;
		p->m_SplitBeamAlongTXRXWeightId = 65535;
		p->m_SplitBeamAthwartTXRXWeightId = 65535;

		p->m_calibMainBeamAlongSteeringAngleRad = p->m_beamAlongAngleSensitivity;
		p->m_calibMainBeamAthwartSteeringAngleRad = p->m_beamAthwartAngleSensitivity;
		p->m_calibBeam3dBWidthAlongRad = p->m_beam3dBWidthAlongRad;
		p->m_calibBeam3dBWidthAthwartRad = p->m_beam3dBWidthAthwartRad;
		p->m_calibBeamGain = p->m_beamGain;
		p->m_calibBeamSACorrection = p->m_beamSACorrection;

		pTrans->AddSoftChannel(p);
		MovUnRefDelete(p);
		MovUnRefDelete(pTrans);
	}
	trav.TupleHeaderUpdate();
}

std::uint32_t Tup2001::Encode(MovStream & IS, Sounder *pSounder, Transducer *pTrans)
{
	Hac2001 myHac;
	std::uint32_t tupleSize = 106;
	unsigned short tupleCode = 2001;
	std::uint32_t backlink = 116;

	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	SoftChannel *p = pTrans->getSoftChannelPolarX(0);

	myHac.ChannelId = p->m_softChannelId;
	myHac.docId = pSounder->m_SounderId;
	myHac.SamplingInterval = (std::uint32_t)round(pTrans->m_timeSampleInterval*pSounder->m_soundVelocity / 2.0); // time sample interval micro seconds
	myHac.dataType = p->m_dataType;
	myHac.transceiverChannelNumber = p->m_softChannelId;
	myHac.AcousticFrequency = p->m_acousticFrequency;
	myHac.TransDepht = (std::uint32_t)round(pTrans->m_transDepthMeter / 0.01);
	myHac.blankingRange = 0;
	myHac.PlatFormid = pTrans->m_platformId;
	myHac.TransShape = pTrans->m_transShape;
	myHac.TransFaceAlongAngleOffset = (short)round(RAD_TO_DEG(pTrans->m_transFaceAlongAngleOffsetRad) / 0.1);
	myHac.TransFaceAthwartAngleOffset = (short)round(RAD_TO_DEG(pTrans->m_transFaceAthwarAngleOffsetRad) / 0.1);
	myHac.RotationAngle = (short)round(RAD_TO_DEG(pTrans->m_transRotationAngleRad) / 0.01);
	myHac.TransMainBeamAlongBeamAngleOffset = (short)round(RAD_TO_DEG(p->m_calibMainBeamAlongSteeringAngleRad) / 0.01);
	myHac.TransMainBeamAthwartAngleOffset = (short)round(RAD_TO_DEG(p->m_calibMainBeamAthwartSteeringAngleRad) / 0.01);
	myHac.AbsorptionSound = (unsigned short)round(p->m_absorptionCoef / 100.0);
	myHac.PulseLengthMode = (unsigned short)pTrans->m_pulseDuration;
	myHac.BandWidthMode = (unsigned short)p->m_bandWidth;
	myHac.MaximumPower = (unsigned short)pTrans->m_transPower;
	myHac.AngleSensitivity = (unsigned short)round(p->m_beamAlongAngleSensitivity / 100.0);
	myHac.TranAthwartAngleSensitivity = (unsigned short)round(p->m_beamAthwartAngleSensitivity / 100.0);
	myHac.Along3DbWidth = (unsigned short)round(RAD_TO_DEG(p->m_calibBeam3dBWidthAlongRad) / 0.01);
	myHac.Athwart3DbWidth = (unsigned short)round(RAD_TO_DEG(p->m_calibBeam3dBWidthAthwartRad) / 0.01);
	myHac.TwoWayBeamAngle = (short)round(p->m_beamEquTwoWayAngle / 0.01);
	myHac.CalibrationTransGain = (unsigned short)round(p->m_calibBeamGain / 0.01);
	myHac.BottomDetectionMinLevel = (short)round(p->m_bottomDetectionMinLevel / 0.01);
	myHac.BottomDetectionMinDepth = (std::uint32_t)round(p->m_bottomDetectionMinDepth / 0.01);
	myHac.BottomDetectionMaxDepth = (std::uint32_t)round(p->m_bottomDetectionMaxDepth / 0.01);
	myHac.space = 0;

	memset(myHac.remarks, 0, sizeof(myHac.remarks));
	myHac.tupleAttributes = 1;

	IS.Write((char*)& myHac.ChannelId, sizeof(myHac.ChannelId), NULL);
	IS.Write((char*)& myHac.docId, sizeof(myHac.docId), NULL);
	IS.Write((char*)& myHac.SamplingInterval, sizeof(myHac.SamplingInterval), NULL);
	IS.Write((char*)& myHac.dataType, sizeof(myHac.dataType), NULL);
	IS.Write((char*)& myHac.transceiverChannelNumber, sizeof(myHac.transceiverChannelNumber), NULL);
	IS.Write((char*)& myHac.AcousticFrequency, sizeof(myHac.AcousticFrequency), NULL);
	IS.Write((char*)& myHac.TransDepht, sizeof(myHac.TransDepht), NULL);
	IS.Write((char*)& myHac.blankingRange, sizeof(myHac.blankingRange), NULL);
	IS.Write((char*)& myHac.PlatFormid, sizeof(myHac.PlatFormid), NULL);
	IS.Write((char*)& myHac.TransShape, sizeof(myHac.TransShape), NULL);
	IS.Write((char*)& myHac.TransFaceAlongAngleOffset, sizeof(myHac.TransFaceAlongAngleOffset), NULL);
	IS.Write((char*)& myHac.TransFaceAthwartAngleOffset, sizeof(myHac.TransFaceAthwartAngleOffset), NULL);
	IS.Write((char*)& myHac.RotationAngle, sizeof(myHac.RotationAngle), NULL);
	IS.Write((char*)& myHac.TransMainBeamAlongBeamAngleOffset, sizeof(myHac.TransMainBeamAlongBeamAngleOffset), NULL);
	IS.Write((char*)& myHac.TransMainBeamAthwartAngleOffset, sizeof(myHac.TransMainBeamAthwartAngleOffset), NULL);
	IS.Write((char*)& myHac.AbsorptionSound, sizeof(myHac.AbsorptionSound), NULL);
	IS.Write((char*)& myHac.PulseLengthMode, sizeof(myHac.PulseLengthMode), NULL);
	IS.Write((char*)& myHac.BandWidthMode, sizeof(myHac.BandWidthMode), NULL);
	IS.Write((char*)& myHac.MaximumPower, sizeof(myHac.MaximumPower), NULL);
	IS.Write((char*)& myHac.AngleSensitivity, sizeof(myHac.AngleSensitivity), NULL);
	IS.Write((char*)& myHac.TranAthwartAngleSensitivity, sizeof(myHac.TranAthwartAngleSensitivity), NULL);
	IS.Write((char*)& myHac.Along3DbWidth, sizeof(myHac.Along3DbWidth), NULL);
	IS.Write((char*)& myHac.Athwart3DbWidth, sizeof(myHac.Athwart3DbWidth), NULL);
	IS.Write((char*)& myHac.TwoWayBeamAngle, sizeof(myHac.TwoWayBeamAngle), NULL);
	IS.Write((char*)& myHac.CalibrationTransGain, sizeof(myHac.CalibrationTransGain), NULL);
	IS.Write((char*)& myHac.BottomDetectionMinLevel, sizeof(myHac.BottomDetectionMinLevel), NULL);
	IS.Write((char*)& myHac.BottomDetectionMinDepth, sizeof(myHac.BottomDetectionMinDepth), NULL);
	IS.Write((char*)& myHac.BottomDetectionMaxDepth, sizeof(myHac.BottomDetectionMaxDepth), NULL);
	IS.Write((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Write((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}
