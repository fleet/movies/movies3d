/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup210.cpp												  */
/******************************************************************************/

#include "Reader/Tup210.h"

#include <fstream>
#include <assert.h>

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/Transducer.h"
/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/




Tup210::Tup210()
{

}





void Tup210::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{



	libMove posit;
	posit.LowPart = 0;


	Hac210 myHac;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture
	//unsigned short

	IS.Read((char*)& myHac.numOfChannel, sizeof(myHac.numOfChannel), NULL);
	IS.Read((char*)& myHac.docId, sizeof(myHac.docId), NULL);
	IS.Read((char*)& myHac.soundSpeed, sizeof(myHac.soundSpeed), NULL);
	IS.Read((char*)& myHac.pingMode, sizeof(myHac.pingMode), NULL);
	IS.Read((char*)& myHac.pingInterval, sizeof(myHac.pingInterval), NULL);
	IS.Read((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Read((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);

	SounderER60 *p = SounderER60::Create();

	p->m_SounderId = myHac.docId;
	p->m_soundVelocity = myHac.soundSpeed*0.1;
	p->m_triggerMode = 1;
	p->m_numberOfTransducer = myHac.numOfChannel;
	p->m_pingInterval = myHac.pingInterval*0.01;
	p->m_pingMode = myHac.pingMode;
	p->m_isMultiBeam = false;
	p->SetRemarks(myHac.remarks);
	p->m_tupleType = 210;

	p->DataChanged();

	MovRef(p);

	trav.m_pHacObjectMgr->AddSounder(p);
	trav.TupleHeaderUpdate();


	MovUnRefDelete(p);


}
std::uint32_t Tup210::Encode(MovStream & IS, SounderER60 *p)
{
	Hac210 myHac;
	std::uint32_t tupleSize = 58;
	unsigned short tupleCode = 210;
	std::uint32_t backlink = 68;

	IS.GetWriteBuffer(backlink);

	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture

	myHac.docId = p->m_SounderId;
	myHac.soundSpeed = (unsigned short)round(p->m_soundVelocity / 0.1);
	myHac.numOfChannel = p->m_numberOfTransducer;
	myHac.pingInterval = (unsigned short)round(p->m_pingInterval / 0.01);
	myHac.pingMode = p->m_pingMode;
	myHac.space = 0;
	strncpy(myHac.remarks, p->GetRemarks(), sizeof(p->GetRemarks()));
	myHac.tupleAttributes = 1;


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.numOfChannel, sizeof(myHac.numOfChannel), NULL);
	IS.Write((char*)& myHac.docId, sizeof(myHac.docId), NULL);
	IS.Write((char*)& myHac.soundSpeed, sizeof(myHac.soundSpeed), NULL);
	IS.Write((char*)& myHac.pingMode, sizeof(myHac.pingMode), NULL);
	IS.Write((char*)& myHac.pingInterval, sizeof(myHac.pingInterval), NULL);
	IS.Write((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Write((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}



