/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup2100.cpp												  */
/******************************************************************************/

#include "Reader/Tup2100.h"

#include <fstream>
#include <sstream>
#include <assert.h>
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Transducer.h"

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup2100";
}

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup2100::Tup2100()
{
}

void Tup2100::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	libMove essai;
	essai.LowPart = 0;

	Hac2100 myHac;
	IS.Seek(essai, eSEEK_CUR);

	IS.Read((char*)& myHac.ChannelId, sizeof(myHac.ChannelId), NULL);
	IS.Read((char*)& myHac.docId, sizeof(myHac.docId), NULL);
	IS.Read((char*)& myHac.m_FrequencyName, sizeof(myHac.m_FrequencyName), NULL);
	IS.Read((char*)& myHac.TransVersion, sizeof(myHac.TransVersion), NULL);
	IS.Read((char*)& myHac.TransName, sizeof(myHac.TransName), NULL);
	IS.Read((char*)& myHac.TimeSampleInterval, sizeof(myHac.TimeSampleInterval), NULL);
	IS.Read((char*)& myHac.dataType, sizeof(myHac.dataType), NULL);
	IS.Read((char*)& myHac.beamType, sizeof(myHac.beamType), NULL);
	IS.Read((char*)& myHac.AcousticFrequency, sizeof(myHac.AcousticFrequency), NULL);
	IS.Read((char*)& myHac.TransDepht, sizeof(myHac.TransDepht), NULL);
	IS.Read((char*)& myHac.StartSample, sizeof(myHac.StartSample), NULL);
	IS.Read((char*)& myHac.PlatFormid, sizeof(myHac.PlatFormid), NULL);
	IS.Read((char*)& myHac.TransShape, sizeof(myHac.TransShape), NULL);
	IS.Read((char*)& myHac.TransFaceAlongAngleOffset, sizeof(myHac.TransFaceAlongAngleOffset), NULL);
	IS.Read((char*)& myHac.TransFaceAthwartAngleOffset, sizeof(myHac.TransFaceAthwartAngleOffset), NULL);
	IS.Read((char*)& myHac.RotationAngle, sizeof(myHac.RotationAngle), NULL);
	IS.Read((char*)& myHac.TransMainBeamAlongBeamAngleOffset, sizeof(myHac.TransMainBeamAlongBeamAngleOffset), NULL);
	IS.Read((char*)& myHac.TransMainBeamAthwartAngleOffset, sizeof(myHac.TransMainBeamAthwartAngleOffset), NULL);
	IS.Read((char*)& myHac.AbsorptionCoeff, sizeof(myHac.AbsorptionCoeff), NULL);
	IS.Read((char*)& myHac.PulseDuration, sizeof(myHac.PulseDuration), NULL);
	IS.Read((char*)& myHac.Bandwidth, sizeof(myHac.Bandwidth), NULL);
	IS.Read((char*)& myHac.TransmissionPower, sizeof(myHac.TransmissionPower), NULL);
	IS.Read((char*)& myHac.AngleSensitivity, sizeof(myHac.AngleSensitivity), NULL);
	IS.Read((char*)& myHac.TranAthwartAngleSensitivity, sizeof(myHac.TranAthwartAngleSensitivity), NULL);
	IS.Read((char*)& myHac.Along3DbWidth, sizeof(myHac.Along3DbWidth), NULL);
	IS.Read((char*)& myHac.Athwart3DbWidth, sizeof(myHac.Athwart3DbWidth), NULL);
	IS.Read((char*)& myHac.TwoWayBeamAngle, sizeof(myHac.TwoWayBeamAngle), NULL);
	IS.Read((char*)& myHac.TransGain, sizeof(myHac.TransGain), NULL);
	IS.Read((char*)& myHac.TranssACorrection, sizeof(myHac.TranssACorrection), NULL);
	IS.Read((char*)& myHac.BottomDetectionMinDepth, sizeof(myHac.BottomDetectionMinDepth), NULL);
	IS.Read((char*)& myHac.BottomDetectionMaxDepth, sizeof(myHac.BottomDetectionMaxDepth), NULL);
	IS.Read((char*)& myHac.BottomDetectionMinLevel, sizeof(myHac.BottomDetectionMinLevel), NULL);

	// NMD - FAE MOVIESDDD-182 - On ne lie que les donn�es de type Sv
	if (myHac.dataType != 2) return;

	Sounder	*pSounder = trav.m_pHacObjectMgr->GetWorkingSounder(myHac.docId);

	// OTK - FAE067 - si pas de sondeur en cours de construction, on recopie le sondeur existant
	// et on repart de cet objet pour mettre � jour le channel
	if (pSounder == NULL)
	{
		SounderER60* pExistingSounder = dynamic_cast<SounderER60*>(trav.m_pHacObjectMgr->GetSounderDefinition().GetSounderWithId(myHac.docId));
		if (pExistingSounder == NULL)
		{
			// rien � faire : le tuple est perdu
			M3D_LOG_WARN(LoggerName, "Receiving tuple 2100 with no <building> 210");
		}
		else
		{
			// cr�ation et recopie du sondeur existant
			SounderER60* pNewSounder = SounderER60::Create();
			*pNewSounder = *pExistingSounder;
			pNewSounder->DataChanged();
			trav.m_pHacObjectMgr->AddSounder(pNewSounder);
			pSounder = pNewSounder;
		}
	}

	if (pSounder)
	{
		// NMD - FAE 105 -recherche d'un transducer pouvant correspondre  
		Transducer * pTrans = pSounder->GetTransducerWithName(myHac.TransName);

		if (pTrans == NULL)
		{
			// now create the transducer Object 
			pTrans = Transducer::Create();
			MovRef(pTrans);
			memcpy(pTrans->m_transName, myHac.TransName, sizeof(myHac.TransName));
			memcpy(pTrans->m_transSoftVersion, myHac.TransVersion, sizeof(myHac.TransVersion));
			pTrans->m_timeSampleInterval = myHac.TimeSampleInterval; // time sample interval micro seconds
			pTrans->m_transDepthMeter = myHac.TransDepht*0.0001;
			pTrans->m_platformId = myHac.PlatFormid;
			pTrans->m_transShape = myHac.TransShape;
			pTrans->m_transFaceAlongAngleOffsetRad = DEG_TO_RAD(myHac.TransFaceAlongAngleOffset*0.0001);
			pTrans->m_transFaceAthwarAngleOffsetRad = DEG_TO_RAD(myHac.TransFaceAthwartAngleOffset*0.0001);
			pTrans->m_transRotationAngleRad = DEG_TO_RAD(myHac.RotationAngle*0.0001);

			pTrans->m_pulseDuration = myHac.PulseDuration;

			pTrans->m_pulseForm = 65535;  // pulse form 
			pTrans->m_frequencyBeamSpacing = 65535;
			pTrans->m_frequencySpaceShape = 3;
			pTrans->m_transPower = 1;
			pTrans->m_numberOfSoftChannel = 1;

			pTrans->DataChanged();
			////

			pSounder->addTransducer(pTrans);

			SoftChannel *p = SoftChannel::Create();
			MovRef(p);

			/// inside data
			p->m_tupleType = 2100;
			p->m_softChannelComputeData.m_groupId = 0;
			p->m_softChannelComputeData.m_isReferenceBeam = false;
			p->m_softChannelComputeData.m_isMultiBeam = false;
			p->m_softChannelComputeData.m_PolarId = 0;

			/// start Copying
			p->m_softChannelId = myHac.ChannelId;
			memcpy(p->m_channelName, myHac.m_FrequencyName, sizeof(myHac.m_FrequencyName));
			memcpy(p->m_transNameShort, myHac.TransName, sizeof(myHac.TransName));
			p->m_dataType = myHac.dataType;
			p->m_beamType = myHac.beamType;
			p->m_acousticFrequency = myHac.AcousticFrequency;
			p->m_startSample = myHac.StartSample;
			p->m_mainBeamAlongSteeringAngleRad = DEG_TO_RAD((myHac.TransMainBeamAlongBeamAngleOffset*0.0001));
			p->m_mainBeamAthwartSteeringAngleRad = DEG_TO_RAD((myHac.TransMainBeamAthwartAngleOffset*0.0001));
			p->m_absorptionCoefHAC = myHac.AbsorptionCoeff;
			p->m_absorptionCoef = p->m_absorptionCoefHAC;
			p->m_bandWidth = myHac.Bandwidth;
			p->m_transmissionPower = myHac.TransmissionPower;
			p->m_beamAlongAngleSensitivity = myHac.AngleSensitivity * 0.0001;
			p->m_beamAthwartAngleSensitivity = myHac.TranAthwartAngleSensitivity * 0.0001;
			p->m_beam3dBWidthAlongRad = DEG_TO_RAD(myHac.Along3DbWidth*0.0001);
			p->m_beam3dBWidthAthwartRad = DEG_TO_RAD(myHac.Athwart3DbWidth*0.0001);
			p->m_beamEquTwoWayAngle = myHac.TwoWayBeamAngle*0.0001;
			p->m_beamGain = myHac.TransGain*0.0001;
			p->m_beamSACorrection = myHac.TranssACorrection*0.0001;
			p->m_bottomDetectionMinDepth = myHac.BottomDetectionMinDepth*0.0001;
			p->m_bottomDetectionMaxDepth = myHac.BottomDetectionMaxDepth*0.0001;
			p->m_bottomDetectionMinLevel = myHac.BottomDetectionMinLevel*0.0001;
			p->m_AlongTXRXWeightId = 65535;
			p->m_AthwartTXRXWeightId = 65535;
			p->m_SplitBeamAlongTXRXWeightId = 65535;
			p->m_SplitBeamAthwartTXRXWeightId = 65535;

			p->m_calibMainBeamAlongSteeringAngleRad = p->m_mainBeamAlongSteeringAngleRad;
			p->m_calibMainBeamAthwartSteeringAngleRad = p->m_mainBeamAthwartSteeringAngleRad;
			p->m_calibBeam3dBWidthAlongRad = p->m_beam3dBWidthAlongRad;
			p->m_calibBeam3dBWidthAthwartRad = p->m_beam3dBWidthAthwartRad;
			p->m_calibBeamGain = p->m_beamGain;
			p->m_calibBeamSACorrection = p->m_beamSACorrection;

			pTrans->AddSoftChannel(p);

			MovUnRefDelete(p);
			MovUnRefDelete(pTrans);
		}
		else
		{
			// NMD - FAE 105 - creation d'un channel virtuel
			SoftChannel *pSoftChannel = pTrans->getSoftChannelByName(myHac.m_FrequencyName);
			if (pSoftChannel)
			{
				pTrans->AddVirtualSoftChannel(pSoftChannel, myHac.ChannelId);
			}
		}
	}
	trav.TupleHeaderUpdate();
}

std::uint32_t Tup2100::Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer)
{
	Hac2100 myHac;
	std::uint32_t tupleSize = 258;
	unsigned short tupleCode = 2100;
	std::uint32_t backlink = 268;

	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	SoftChannel *p = pTransducer->getSoftChannelPolarX(0);

	myHac.ChannelId = p->m_softChannelId;
	myHac.docId = pSounder->m_SounderId;
	memcpy(myHac.m_FrequencyName, p->m_channelName, sizeof(myHac.m_FrequencyName));
	memcpy(myHac.TransVersion, pTransducer->m_transSoftVersion, sizeof(myHac.TransVersion));
	memcpy(myHac.TransName, p->m_transNameShort, sizeof(myHac.TransName));
	myHac.TimeSampleInterval = pTransducer->m_timeSampleInterval; // time sample interval micro seconds
	myHac.dataType = p->m_dataType;
	myHac.beamType = p->m_beamType;
	myHac.AcousticFrequency = p->m_acousticFrequency;
	myHac.TransDepht = (std::uint32_t)round(pTransducer->m_transDepthMeter / 0.0001);
	myHac.StartSample = 0;//p->m_startSample;		

	myHac.PlatFormid = pTransducer->m_platformId;
	myHac.TransShape = pTransducer->m_transShape;
	myHac.TransFaceAlongAngleOffset = (std::int32_t)round(RAD_TO_DEG(pTransducer->m_transFaceAlongAngleOffsetRad) / 0.0001);
	myHac.TransFaceAthwartAngleOffset = (std::int32_t)round(RAD_TO_DEG(pTransducer->m_transFaceAthwarAngleOffsetRad) / 0.0001);
	myHac.RotationAngle = (std::int32_t)round(RAD_TO_DEG(pTransducer->m_transRotationAngleRad) / 0.0001);

	myHac.TransMainBeamAlongBeamAngleOffset = (std::int32_t)round(RAD_TO_DEG(p->m_calibMainBeamAlongSteeringAngleRad) / 0.0001);
	myHac.TransMainBeamAthwartAngleOffset = (std::int32_t)round(RAD_TO_DEG(p->m_calibMainBeamAthwartSteeringAngleRad) / 0.0001);
	
	myHac.AbsorptionCoeff = p->m_absorptionCoef;

	myHac.PulseDuration = pTransducer->m_pulseDuration;

	myHac.Bandwidth = p->m_bandWidth;
	myHac.TransmissionPower = p->m_transmissionPower;
	myHac.AngleSensitivity = p->m_beamAlongAngleSensitivity / 0.0001;
	myHac.TranAthwartAngleSensitivity = p->m_beamAthwartAngleSensitivity / 0.0001;

	myHac.Along3DbWidth = (std::uint32_t)round(RAD_TO_DEG(p->m_calibBeam3dBWidthAlongRad) / 0.0001);
	myHac.Athwart3DbWidth = (std::uint32_t)round(RAD_TO_DEG(p->m_calibBeam3dBWidthAthwartRad) / 0.0001);
	
	myHac.TwoWayBeamAngle = (std::int32_t)round(p->m_beamEquTwoWayAngle / 0.0001);
		
	myHac.TransGain = (std::uint32_t)round(p->m_calibBeamGain / 0.0001);
	myHac.TranssACorrection = (std::int32_t)round(p->m_calibBeamSACorrection / 0.0001);	

	myHac.BottomDetectionMinDepth = (std::uint32_t)round(p->m_bottomDetectionMinDepth / 0.0001);
	myHac.BottomDetectionMaxDepth = (std::uint32_t)round(p->m_bottomDetectionMaxDepth / 0.0001);
	myHac.BottomDetectionMinLevel = (std::int32_t)round(p->m_bottomDetectionMinLevel / 0.0001);

	memset(myHac.remarks, 0, sizeof(myHac.remarks));
	myHac.tupleAttributes = 0; // on ne modifie normalement pas ce tuple : attributes = 0

	IS.Write((char*)& myHac.ChannelId, sizeof(myHac.ChannelId), NULL);
	IS.Write((char*)& myHac.docId, sizeof(myHac.docId), NULL);
	IS.Write((char*)& myHac.m_FrequencyName, sizeof(myHac.m_FrequencyName), NULL);
	IS.Write((char*)& myHac.TransVersion, sizeof(myHac.TransVersion), NULL);
	IS.Write((char*)& myHac.TransName, sizeof(myHac.TransName), NULL);
	IS.Write((char*)& myHac.TimeSampleInterval, sizeof(myHac.TimeSampleInterval), NULL);
	IS.Write((char*)& myHac.dataType, sizeof(myHac.dataType), NULL);
	IS.Write((char*)& myHac.beamType, sizeof(myHac.beamType), NULL);
	IS.Write((char*)& myHac.AcousticFrequency, sizeof(myHac.AcousticFrequency), NULL);
	IS.Write((char*)& myHac.TransDepht, sizeof(myHac.TransDepht), NULL);
	IS.Write((char*)& myHac.StartSample, sizeof(myHac.StartSample), NULL);
	IS.Write((char*)& myHac.PlatFormid, sizeof(myHac.PlatFormid), NULL);
	IS.Write((char*)& myHac.TransShape, sizeof(myHac.TransShape), NULL);
	IS.Write((char*)& myHac.TransFaceAlongAngleOffset, sizeof(myHac.TransFaceAlongAngleOffset), NULL);
	IS.Write((char*)& myHac.TransFaceAthwartAngleOffset, sizeof(myHac.TransFaceAthwartAngleOffset), NULL);
	IS.Write((char*)& myHac.RotationAngle, sizeof(myHac.RotationAngle), NULL);
	IS.Write((char*)& myHac.TransMainBeamAlongBeamAngleOffset, sizeof(myHac.TransMainBeamAlongBeamAngleOffset), NULL);
	IS.Write((char*)& myHac.TransMainBeamAthwartAngleOffset, sizeof(myHac.TransMainBeamAthwartAngleOffset), NULL);
	IS.Write((char*)& myHac.AbsorptionCoeff, sizeof(myHac.AbsorptionCoeff), NULL);
	IS.Write((char*)& myHac.PulseDuration, sizeof(myHac.PulseDuration), NULL);
	IS.Write((char*)& myHac.Bandwidth, sizeof(myHac.Bandwidth), NULL);
	IS.Write((char*)& myHac.TransmissionPower, sizeof(myHac.TransmissionPower), NULL);
	IS.Write((char*)& myHac.AngleSensitivity, sizeof(myHac.AngleSensitivity), NULL);
	IS.Write((char*)& myHac.TranAthwartAngleSensitivity, sizeof(myHac.TranAthwartAngleSensitivity), NULL);
	IS.Write((char*)& myHac.Along3DbWidth, sizeof(myHac.Along3DbWidth), NULL);
	IS.Write((char*)& myHac.Athwart3DbWidth, sizeof(myHac.Athwart3DbWidth), NULL);
	IS.Write((char*)& myHac.TwoWayBeamAngle, sizeof(myHac.TwoWayBeamAngle), NULL);
	IS.Write((char*)& myHac.TransGain, sizeof(myHac.TransGain), NULL);
	IS.Write((char*)& myHac.TranssACorrection, sizeof(myHac.TranssACorrection), NULL);
	IS.Write((char*)& myHac.BottomDetectionMinDepth, sizeof(myHac.BottomDetectionMinDepth), NULL);
	IS.Write((char*)& myHac.BottomDetectionMaxDepth, sizeof(myHac.BottomDetectionMaxDepth), NULL);
	IS.Write((char*)& myHac.BottomDetectionMinLevel, sizeof(myHac.BottomDetectionMinLevel), NULL);
	IS.Write((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}
