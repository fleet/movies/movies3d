/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup220.cpp												  */
/******************************************************************************/

#include "Reader/Tup220.h"

#include <fstream>
#include <assert.h>

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/Transducer.h"
/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup220::Tup220()
{

}







void Tup220::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{


	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture




	Hac220 myHac;
	/// Nombre de canaux logiciels associ�s au sondeur  (fichier HAC-sbi :nbbeam)
	IS.Read((char*)& myHac.NumberSoftChannel, sizeof(myHac.NumberSoftChannel), NULL);
	/// Vitesse du son (pr�cision 0.1)
	IS.Read((char*)& myHac.EchoSounderId, sizeof(myHac.EchoSounderId), NULL);
	IS.Read((char*)& myHac.TransName, sizeof(myHac.TransName), NULL);
	IS.Read((char*)& myHac.TransSoftVersion, sizeof(myHac.TransSoftVersion), NULL);
	IS.Read((char*)& myHac.SoundSpeed, sizeof(myHac.SoundSpeed), NULL);
	IS.Read((char*)& myHac.TriggerMode, sizeof(myHac.TriggerMode), NULL);
	IS.Read((char*)& myHac.PingInterval, sizeof(myHac.PingInterval), NULL);
	IS.Read((char*)& myHac.PulseForm, sizeof(myHac.PulseForm), NULL);
	IS.Read((char*)& myHac.PulseDuration, sizeof(myHac.PulseDuration), NULL);
	IS.Read((char*)& myHac.TimeSampleInterv, sizeof(myHac.TimeSampleInterv), NULL);
	IS.Read((char*)& myHac.FrequencyBeamSpacing, sizeof(myHac.FrequencyBeamSpacing), NULL);
	IS.Read((char*)& myHac.FrequencySpaceShape, sizeof(myHac.FrequencySpaceShape), NULL);
	IS.Read((char*)& myHac.TransceiverPower, sizeof(myHac.TransceiverPower), NULL);
	IS.Read((char*)& myHac.TransDepth, sizeof(myHac.TransDepth), NULL);
	IS.Read((char*)& myHac.PlatFormId, sizeof(myHac.PlatFormId), NULL);
	IS.Read((char*)& myHac.TransducerShape, sizeof(myHac.TransducerShape), NULL);
	IS.Read((char*)& myHac.TransFaceAlongAngleOffset, sizeof(myHac.TransFaceAlongAngleOffset), NULL);
	IS.Read((char*)& myHac.TransFaceAthwartAngleOffset, sizeof(myHac.TransFaceAthwartAngleOffset), NULL);
	IS.Read((char*)& myHac.RotationAngle, sizeof(myHac.RotationAngle), NULL);
	IS.Read((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);


	SounderMulti *p = SounderMulti::Create();

	p->m_SounderId = myHac.EchoSounderId;
	p->m_soundVelocity = myHac.SoundSpeed*0.1;
	p->m_triggerMode = myHac.TriggerMode;
	p->m_numberOfTransducer = 1;
	p->m_pingInterval = myHac.PingInterval*0.01;
	p->m_isMultiBeam = true;
	p->DataChanged();
	p->SetRemarks(myHac.remarks);
	p->m_tupleType = 220;

	MovRef(p);

	trav.m_pHacObjectMgr->AddSounder(p);

	Transducer *pTrans = Transducer::Create();
	MovRef(pTrans);

	memcpy(pTrans->m_transName, myHac.TransName, sizeof(myHac.TransName));
	memcpy(pTrans->m_transSoftVersion, myHac.TransSoftVersion, sizeof(myHac.TransSoftVersion));
	pTrans->m_pulseDuration = myHac.PulseDuration;
	pTrans->m_pulseForm = myHac.PulseForm;  // pulse form 
	pTrans->m_timeSampleInterval = myHac.TimeSampleInterv; // time sample interval micro seconds
	pTrans->m_frequencyBeamSpacing = myHac.FrequencyBeamSpacing;
	pTrans->m_frequencySpaceShape = myHac.FrequencySpaceShape;
	pTrans->m_transPower = myHac.TransceiverPower;
	pTrans->m_transDepthMeter = myHac.TransDepth*0.0001;
	pTrans->m_platformId = myHac.PlatFormId;
	pTrans->m_transShape = myHac.TransducerShape;
	pTrans->m_transFaceAlongAngleOffsetRad = DEG_TO_RAD(myHac.TransFaceAlongAngleOffset*0.0001);
	pTrans->m_transFaceAthwarAngleOffsetRad = DEG_TO_RAD(myHac.TransFaceAthwartAngleOffset*0.0001);
	pTrans->m_transRotationAngleRad = DEG_TO_RAD(myHac.RotationAngle*0.0001);
	pTrans->m_numberOfSoftChannel = myHac.NumberSoftChannel;

	pTrans->DataChanged();

	p->addTransducer(pTrans);

	MovUnRefDelete(pTrans);
	MovUnRefDelete(p);
	trav.TupleHeaderUpdate();

}


std::uint32_t Tup220::Encode(MovStream & IS, Sounder *pSounder, Transducer *pTrans)
{


	Hac220 myHac;
	libMove essai;
	essai.LowPart = 0;
	std::uint32_t tupleSize = 174;
	unsigned short tupleCode = 220;
	std::uint32_t backlink = 184;

	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);




	myHac.EchoSounderId = pSounder->m_SounderId;
	myHac.SoundSpeed = (unsigned short)round(pSounder->m_soundVelocity / 0.1);
	myHac.TriggerMode = pSounder->m_triggerMode;
	myHac.PingInterval = (unsigned short)round(pSounder->m_pingInterval / 0.01);

	memcpy(myHac.TransName, pTrans->m_transName, sizeof(myHac.TransName));
	memcpy(myHac.TransSoftVersion, pTrans->m_transSoftVersion, sizeof(myHac.TransSoftVersion));
	myHac.PulseDuration = pTrans->m_pulseDuration;
	myHac.PulseForm = pTrans->m_pulseForm;  // pulse form 
	myHac.TimeSampleInterv = pTrans->m_timeSampleInterval; // time sample interval micro seconds
	myHac.FrequencyBeamSpacing = pTrans->m_frequencyBeamSpacing;
	myHac.FrequencySpaceShape = pTrans->m_frequencySpaceShape;
	myHac.TransceiverPower = pTrans->m_transPower;
	myHac.TransDepth = (std::uint32_t)round(pTrans->m_transDepthMeter / 0.0001);
	myHac.PlatFormId = pTrans->m_platformId;
	myHac.TransducerShape = pTrans->m_transShape;

	myHac.TransFaceAlongAngleOffset = (std::int32_t)round(RAD_TO_DEG(pTrans->m_transFaceAlongAngleOffsetRad) / 0.0001);
	myHac.TransFaceAthwartAngleOffset = (std::int32_t)round(RAD_TO_DEG(pTrans->m_transFaceAthwarAngleOffsetRad) / 0.0001);
	myHac.RotationAngle = (std::int32_t)round(RAD_TO_DEG(pTrans->m_transRotationAngleRad) / 0.0001);
	myHac.NumberSoftChannel = pTrans->m_numberOfSoftChannel;



	strncpy(myHac.remarks, pSounder->GetRemarks(), sizeof(pSounder->GetRemarks()));
	myHac.tupleAttributes = 1;

	IS.Write((char*)& myHac.NumberSoftChannel, sizeof(myHac.NumberSoftChannel), NULL);
	/// Vitesse du son (pr�cision 0.1)
	IS.Write((char*)& myHac.EchoSounderId, sizeof(myHac.EchoSounderId), NULL);
	IS.Write((char*)& myHac.TransName, sizeof(myHac.TransName), NULL);
	IS.Write((char*)& myHac.TransSoftVersion, sizeof(myHac.TransSoftVersion), NULL);
	IS.Write((char*)& myHac.SoundSpeed, sizeof(myHac.SoundSpeed), NULL);
	IS.Write((char*)& myHac.TriggerMode, sizeof(myHac.TriggerMode), NULL);
	IS.Write((char*)& myHac.PingInterval, sizeof(myHac.PingInterval), NULL);
	IS.Write((char*)& myHac.PulseForm, sizeof(myHac.PulseForm), NULL);
	IS.Write((char*)& myHac.PulseDuration, sizeof(myHac.PulseDuration), NULL);
	IS.Write((char*)& myHac.TimeSampleInterv, sizeof(myHac.TimeSampleInterv), NULL);
	IS.Write((char*)& myHac.FrequencyBeamSpacing, sizeof(myHac.FrequencyBeamSpacing), NULL);
	IS.Write((char*)& myHac.FrequencySpaceShape, sizeof(myHac.FrequencySpaceShape), NULL);
	IS.Write((char*)& myHac.TransceiverPower, sizeof(myHac.TransceiverPower), NULL);
	IS.Write((char*)& myHac.TransDepth, sizeof(myHac.TransDepth), NULL);
	IS.Write((char*)& myHac.PlatFormId, sizeof(myHac.PlatFormId), NULL);
	IS.Write((char*)& myHac.TransducerShape, sizeof(myHac.TransducerShape), NULL);
	IS.Write((char*)& myHac.TransFaceAlongAngleOffset, sizeof(myHac.TransFaceAlongAngleOffset), NULL);
	IS.Write((char*)& myHac.TransFaceAthwartAngleOffset, sizeof(myHac.TransFaceAthwartAngleOffset), NULL);
	IS.Write((char*)& myHac.RotationAngle, sizeof(myHac.RotationAngle), NULL);


	IS.Write((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);



	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}


