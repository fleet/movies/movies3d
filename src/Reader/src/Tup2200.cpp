/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup2200.cpp												  */
/******************************************************************************/

#include "Reader/Tup2200.h"

#include <fstream>
#include <assert.h>
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Transducer.h"

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup2200";
}

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup2200::Tup2200()
{
}

void Tup2200::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	Hac2200 myHac;

	libMove essai;
	essai.LowPart = 0;

	IS.Seek(essai, eSEEK_CUR);

	IS.Read((char*)& myHac.SoftwareChannelId, sizeof(myHac.SoftwareChannelId), NULL);
	IS.Read((char*)& myHac.SounderId, sizeof(myHac.SounderId), NULL);
	IS.Read((char*)& myHac.FrequencyName, sizeof(myHac.FrequencyName), NULL);
	IS.Read((char*)& myHac.DataType, sizeof(myHac.DataType), NULL);
	IS.Read((char*)& myHac.BeamType, sizeof(myHac.BeamType), NULL);
	IS.Read((char*)& myHac.AcousticFrequency, sizeof(myHac.AcousticFrequency), NULL);
	IS.Read((char*)& myHac.StartSample, sizeof(myHac.StartSample), NULL);
	IS.Read((char*)& myHac.AlongShipSteeringAngle, sizeof(myHac.AlongShipSteeringAngle), NULL);
	IS.Read((char*)& myHac.AthwartShipSteeringAngle, sizeof(myHac.AthwartShipSteeringAngle), NULL);
	IS.Read((char*)& myHac.AbsorptionCoefficient, sizeof(myHac.AbsorptionCoefficient), NULL);
	IS.Read((char*)& myHac.BandWitdh, sizeof(myHac.BandWitdh), NULL);
	IS.Read((char*)& myHac.TransmissionPower, sizeof(myHac.TransmissionPower), NULL);
	IS.Read((char*)& myHac.BeamAlongShiphAngleSensitivity, sizeof(myHac.BeamAlongShiphAngleSensitivity), NULL);
	IS.Read((char*)& myHac.BeamArthwartShipAngleSensitivity, sizeof(myHac.BeamArthwartShipAngleSensitivity), NULL);
	IS.Read((char*)& myHac.BeamAlongShip3dbWidth, sizeof(myHac.BeamAlongShip3dbWidth), NULL);
	IS.Read((char*)& myHac.BeamAthwart3dbWidth, sizeof(myHac.BeamAthwart3dbWidth), NULL);
	IS.Read((char*)& myHac.BeamEquivalentTwoWayBeamAngle, sizeof(myHac.BeamEquivalentTwoWayBeamAngle), NULL);
	IS.Read((char*)& myHac.BeamGain, sizeof(myHac.BeamGain), NULL);

	IS.Read((char*)& myHac.BeamSACorrection, sizeof(myHac.BeamSACorrection), NULL);
	IS.Read((char*)& myHac.BottomDetectionMinDepth, sizeof(myHac.BottomDetectionMinDepth), NULL);
	IS.Read((char*)& myHac.BottomDetectionMaxDepth, sizeof(myHac.BottomDetectionMaxDepth), NULL);
	IS.Read((char*)& myHac.BottomDetectionMinLevel, sizeof(myHac.BottomDetectionMinLevel), NULL);
	IS.Read((char*)& myHac.AlongTXRXWeightId, sizeof(myHac.AlongTXRXWeightId), NULL);
	IS.Read((char*)& myHac.ArthwartTXRXWeightId, sizeof(myHac.ArthwartTXRXWeightId), NULL);
	IS.Read((char*)& myHac.AlongSplitBeamRXWId, sizeof(myHac.AlongSplitBeamRXWId), NULL);
	IS.Read((char*)& myHac.ArthwartSplitBeamRXWId, sizeof(myHac.ArthwartSplitBeamRXWId), NULL);
	IS.Read((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);
	
	bool refBeamAdded = (myHac.BeamType > 2 && myHac.AthwartShipSteeringAngle != 0);

	SoftChannelMulti *p = SoftChannelMulti::Create();

	/// inside data
	p->m_tupleType = 2200;
	p->m_softChannelComputeData.m_groupId = 0;
	p->m_softChannelComputeData.m_isReferenceBeam = myHac.BeamType > 2 ? true : false;
	p->m_softChannelComputeData.m_isMultiBeam = true;
	p->m_softChannelComputeData.m_PolarId = 0;

	/// start Copying
	p->m_softChannelId = myHac.SoftwareChannelId;
	memcpy(p->m_channelName, myHac.FrequencyName, sizeof(myHac.FrequencyName));

	// OTK - FAE072 - comparaison des valeurs d'angles avec les noms du channel pour
	// correction �ventuelle si possible
	std::string chanName = p->m_channelName;
	size_t indexX = chanName.rfind("x=");
	size_t indexY = chanName.rfind(" y=");
	// avant cela, on v�rifie que le nom de la voie contient bien les infos n�cessaires
	if (indexX != std::string::npos && indexY != std::string::npos
        && chanName.length() >= std::max(indexX, indexY) + 3)
	{
		std::string xStr = chanName.substr(indexX + 2, indexY - (indexX + 2));
		double x = atof(xStr.c_str());
		std::string yStr = chanName.substr(indexY + 3);
		double y = atof(yStr.c_str());

		if (fabs(myHac.AlongShipSteeringAngle*0.0001 - x) > 1.0)
		{
			p->m_mainBeamAlongSteeringAngleRad = DEG_TO_RAD(x);
		}
		else
		{
			p->m_mainBeamAlongSteeringAngleRad = DEG_TO_RAD(myHac.AlongShipSteeringAngle*0.0001);
		}
		if (fabs(myHac.AthwartShipSteeringAngle*0.0001 - y) > 1.0)
		{
			p->m_mainBeamAthwartSteeringAngleRad = DEG_TO_RAD(y);
		}
		else
		{
			p->m_mainBeamAthwartSteeringAngleRad = DEG_TO_RAD(myHac.AthwartShipSteeringAngle*0.0001);
		}
	}
	else
	{
		p->m_mainBeamAlongSteeringAngleRad = DEG_TO_RAD(myHac.AlongShipSteeringAngle*0.0001);
		p->m_mainBeamAthwartSteeringAngleRad = DEG_TO_RAD(myHac.AthwartShipSteeringAngle*0.0001);
	}

	p->m_dataType = myHac.DataType;
	p->m_beamType = myHac.BeamType;
	p->m_acousticFrequency = myHac.AcousticFrequency;
	p->m_startSample = myHac.StartSample;
	p->m_absorptionCoefHAC = myHac.AbsorptionCoefficient;
	p->m_absorptionCoef = p->m_absorptionCoefHAC;
	p->m_bandWidth = myHac.BandWitdh;
	p->m_transmissionPower = myHac.TransmissionPower;
	p->m_beamAlongAngleSensitivity = myHac.BeamAlongShiphAngleSensitivity * 0.0001;
	p->m_beamAthwartAngleSensitivity = myHac.BeamArthwartShipAngleSensitivity * 0.0001;
	p->m_beam3dBWidthAlongRad = DEG_TO_RAD(myHac.BeamAlongShip3dbWidth*0.0001);
	p->m_beam3dBWidthAthwartRad = DEG_TO_RAD(myHac.BeamAthwart3dbWidth*0.0001);
	p->m_beamEquTwoWayAngle = myHac.BeamEquivalentTwoWayBeamAngle*0.0001;
	p->m_beamGain = myHac.BeamGain*0.0001;
	p->m_beamSACorrection = myHac.BeamSACorrection*0.0001;
	p->m_bottomDetectionMinDepth = myHac.BottomDetectionMinDepth*0.0001;
	p->m_bottomDetectionMaxDepth = myHac.BottomDetectionMaxDepth*0.0001;
	p->m_bottomDetectionMinLevel = myHac.BottomDetectionMinLevel*0.0001;
	p->m_AlongTXRXWeightId = myHac.AlongTXRXWeightId;
	p->m_AthwartTXRXWeightId = myHac.ArthwartTXRXWeightId;
	p->m_SplitBeamAlongTXRXWeightId = myHac.AlongSplitBeamRXWId;
	p->m_SplitBeamAthwartTXRXWeightId = myHac.ArthwartSplitBeamRXWId;

	p->m_calibMainBeamAlongSteeringAngleRad = p->m_mainBeamAlongSteeringAngleRad;
	p->m_calibMainBeamAthwartSteeringAngleRad = p->m_mainBeamAthwartSteeringAngleRad;
	p->m_calibBeam3dBWidthAlongRad = p->m_beam3dBWidthAlongRad;
	p->m_calibBeam3dBWidthAthwartRad = p->m_beam3dBWidthAthwartRad;
	p->m_calibBeamGain = p->m_beamGain;
	p->m_calibBeamSACorrection = p->m_beamSACorrection;

	MovRef(p);

	Sounder	*pSounder = trav.m_pHacObjectMgr->GetWorkingSounder(myHac.SounderId);

	// OTK - FAE067 - si pas de sondeur en cours de construction, on recopie le sondeur existant
	// et on repart de cet objet pour mettre � jour le channel
	if (pSounder == NULL)
	{
		SounderMulti* pExistingSounder = dynamic_cast<SounderMulti*>(trav.m_pHacObjectMgr->GetSounderDefinition().GetSounderWithId(myHac.SounderId));
		if (pExistingSounder == NULL)
		{
			// rien � faire : le tuple est perdu
			M3D_LOG_WARN(LoggerName, "Receiving tuple 2200 with no <building> 220");
		}
		else
		{
			// cr�ation et recopie du sondeur existant
			SounderMulti* pNewSounder = SounderMulti::Create();
			*pNewSounder = *pExistingSounder;
			pNewSounder->DataChanged();
			trav.m_pHacObjectMgr->AddSounder(pNewSounder);
			pSounder = pNewSounder;
		}
	}

	if (pSounder)
	{
		Transducer *pTrans = pSounder->GetTransducer(0);
		pTrans->AddSoftChannel(p);

		// OTK - 27/05/2009 - on doit conserver un pointeur vers le dernier channel ajout� pour
		// pouvoir associer les tuples 2210 qui vont eventuellement suivre � ce channel.
		trav.m_pHacObjectMgr->SetLastSoftChannel(p);
	}

	MovUnRefDelete(p);
	trav.TupleHeaderUpdate();
}

std::uint32_t Tup2200::Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer, unsigned int PolarChannelId)
{
	Hac2200 myHac;
	libMove essai;
	essai.LowPart = 0;
	std::uint32_t tupleSize = 178;
	unsigned short tupleCode = 2200;
	std::uint32_t backlink = 188;

	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	SoftChannel *pSoftChannel = pTransducer->getSoftChannelPolarX(PolarChannelId);

	myHac.SoftwareChannelId = pSoftChannel->m_softChannelId;
	myHac.SounderId = pSounder->m_SounderId;
	memcpy(myHac.FrequencyName, pSoftChannel->m_channelName, sizeof(myHac.FrequencyName));
	myHac.DataType = pSoftChannel->m_dataType;
	myHac.BeamType = pSoftChannel->m_beamType;
	myHac.AcousticFrequency = pSoftChannel->m_acousticFrequency;
	myHac.StartSample = 0;//pSoftChannel->m_startSample;		
	myHac.AlongShipSteeringAngle = (std::int32_t)round(RAD_TO_DEG(pSoftChannel->m_calibMainBeamAlongSteeringAngleRad) / 0.0001);
	myHac.AthwartShipSteeringAngle = (std::int32_t)round(RAD_TO_DEG(pSoftChannel->m_calibMainBeamAthwartSteeringAngleRad) / 0.0001);
	myHac.AbsorptionCoefficient = pSoftChannel->m_absorptionCoef;
	myHac.BandWitdh = pSoftChannel->m_bandWidth;
	myHac.TransmissionPower = pSoftChannel->m_transmissionPower;
	myHac.BeamAlongShiphAngleSensitivity = round(pSoftChannel->m_beamAlongAngleSensitivity / 0.0001);
	myHac.BeamArthwartShipAngleSensitivity = round(pSoftChannel->m_beamAthwartAngleSensitivity / 0.0001);
	myHac.BeamAlongShip3dbWidth = (std::uint32_t)round(RAD_TO_DEG(pSoftChannel->m_calibBeam3dBWidthAlongRad) / 0.0001);
	myHac.BeamAthwart3dbWidth = (std::uint32_t)round(RAD_TO_DEG(pSoftChannel->m_calibBeam3dBWidthAthwartRad) / 0.0001);
	myHac.BeamEquivalentTwoWayBeamAngle = (std::int32_t)round(pSoftChannel->m_beamEquTwoWayAngle / 0.0001);
	myHac.BeamGain = (std::uint32_t)round(pSoftChannel->m_calibBeamGain / 0.0001);

	myHac.BeamSACorrection = (std::int32_t)round(pSoftChannel->m_calibBeamSACorrection / 0.0001);
	myHac.BottomDetectionMinDepth = (std::uint32_t)round(pSoftChannel->m_bottomDetectionMinDepth / 0.0001);
	myHac.BottomDetectionMaxDepth = (std::uint32_t)round(pSoftChannel->m_bottomDetectionMaxDepth / 0.0001);
	myHac.BottomDetectionMinLevel = (std::int32_t)round(pSoftChannel->m_bottomDetectionMinLevel / 0.0001);

	myHac.AlongTXRXWeightId = pSoftChannel->m_AlongTXRXWeightId;
	myHac.ArthwartTXRXWeightId = pSoftChannel->m_AthwartTXRXWeightId;
	myHac.AlongSplitBeamRXWId = pSoftChannel->m_SplitBeamAlongTXRXWeightId;
	myHac.ArthwartSplitBeamRXWId = pSoftChannel->m_SplitBeamAthwartTXRXWeightId;

	memset(myHac.remarks, 0, sizeof(myHac.remarks));
	myHac.tupleAttributes = 1;

	IS.Write((char*)& myHac.SoftwareChannelId, sizeof(myHac.SoftwareChannelId), NULL);
	IS.Write((char*)& myHac.SounderId, sizeof(myHac.SounderId), NULL);
	IS.Write((char*)& myHac.FrequencyName, sizeof(myHac.FrequencyName), NULL);
	IS.Write((char*)& myHac.DataType, sizeof(myHac.DataType), NULL);
	IS.Write((char*)& myHac.BeamType, sizeof(myHac.BeamType), NULL);
	IS.Write((char*)& myHac.AcousticFrequency, sizeof(myHac.AcousticFrequency), NULL);
	IS.Write((char*)& myHac.StartSample, sizeof(myHac.StartSample), NULL);
	IS.Write((char*)& myHac.AlongShipSteeringAngle, sizeof(myHac.AlongShipSteeringAngle), NULL);
	IS.Write((char*)& myHac.AthwartShipSteeringAngle, sizeof(myHac.AthwartShipSteeringAngle), NULL);
	IS.Write((char*)& myHac.AbsorptionCoefficient, sizeof(myHac.AbsorptionCoefficient), NULL);
	IS.Write((char*)& myHac.BandWitdh, sizeof(myHac.BandWitdh), NULL);
	IS.Write((char*)& myHac.TransmissionPower, sizeof(myHac.TransmissionPower), NULL);
	IS.Write((char*)& myHac.BeamAlongShiphAngleSensitivity, sizeof(myHac.BeamAlongShiphAngleSensitivity), NULL);
	IS.Write((char*)& myHac.BeamArthwartShipAngleSensitivity, sizeof(myHac.BeamArthwartShipAngleSensitivity), NULL);
	IS.Write((char*)& myHac.BeamAlongShip3dbWidth, sizeof(myHac.BeamAlongShip3dbWidth), NULL);
	IS.Write((char*)& myHac.BeamAthwart3dbWidth, sizeof(myHac.BeamAthwart3dbWidth), NULL);
	IS.Write((char*)& myHac.BeamEquivalentTwoWayBeamAngle, sizeof(myHac.BeamEquivalentTwoWayBeamAngle), NULL);
	IS.Write((char*)& myHac.BeamGain, sizeof(myHac.BeamGain), NULL);

	IS.Write((char*)& myHac.BeamSACorrection, sizeof(myHac.BeamSACorrection), NULL);
	IS.Write((char*)& myHac.BottomDetectionMinDepth, sizeof(myHac.BottomDetectionMinDepth), NULL);
	IS.Write((char*)& myHac.BottomDetectionMaxDepth, sizeof(myHac.BottomDetectionMaxDepth), NULL);
	IS.Write((char*)& myHac.BottomDetectionMinLevel, sizeof(myHac.BottomDetectionMinLevel), NULL);
	IS.Write((char*)& myHac.AlongTXRXWeightId, sizeof(myHac.AlongTXRXWeightId), NULL);
	IS.Write((char*)& myHac.ArthwartTXRXWeightId, sizeof(myHac.ArthwartTXRXWeightId), NULL);
	IS.Write((char*)& myHac.AlongSplitBeamRXWId, sizeof(myHac.AlongSplitBeamRXWId), NULL);
	IS.Write((char*)& myHac.ArthwartSplitBeamRXWId, sizeof(myHac.ArthwartSplitBeamRXWId), NULL);

	IS.Write((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}

