/******************************************************************************/
/*	Project:	SMFH 3D																													*/
/*	Author(s):	O. TONCK (IPSIS)																							*/
/*	File:		Tup2200.cpp																												*/
/******************************************************************************/

#include "Reader/Tup2210.h"

#include <fstream>
#include <assert.h>
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Transducer.h"

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup2210";
}

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup2210::Tup2210()
{
}

void Tup2210::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	Hac2210 myHac;


	libMove essai;
	essai.LowPart = 0;


	IS.Seek(essai, eSEEK_CUR);

	IS.Read((char*)& myHac.ID, sizeof(myHac.ID), NULL);
	IS.Read((char*)& myHac.N, sizeof(myHac.N), NULL);

	BeamWeight *p = BeamWeight::Create();
	p->m_WeightID = myHac.ID;
	for (unsigned int i = 0; i < myHac.N; i++)
	{
		unsigned short weight;
		IS.Read((char*)&weight, sizeof(weight), NULL);
		p->m_W.push_back(weight / 10000.);
	}
	// lecture de l'�ventuel blanc
	if (myHac.N % 2 != 0)
	{
		short blank;
		IS.Read((char*)&blank, sizeof(blank), NULL);
	}
	// tuple attributes
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);
	p->m_tupleAttributes = myHac.tupleAttributes;

	MovRef(p);

	SoftChannel * pChan = trav.m_pHacObjectMgr->GetLastSoftChannel();
	if (pChan)
	{
		pChan->SetBeamWeight(p->m_WeightID, p);
	}
	else
	{
		M3D_LOG_WARN(LoggerName, "Receiving tuple 2210 with no <building> 2200");
	}

	MovUnRefDelete(p);
}
std::uint32_t Tup2210::Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer, unsigned int PolarChannelId)
{
	Hac2210 myHac;

	unsigned short tupleCode = 2210;
	unsigned int offset = 0;

	SoftChannel * pSoft = pTransducer->getSoftChannelPolarX(PolarChannelId);

	MapBeamWeight::const_iterator iter = pSoft->GetBeamWeights().begin();
	while (iter != pSoft->GetBeamWeights().end())
	{
		BeamWeight * pWeight = iter->second;
		std::uint32_t backlink = 20 + 2 * ((pWeight->m_W.size() % 2 == 1) ? pWeight->m_W.size() + 1 : pWeight->m_W.size());
		std::uint32_t tupleSize = backlink - 10;

		IS.GetConservativeWriteBuffer(offset + backlink);

		libMove posit;
		posit.LowPart = offset;
		IS.Seek(posit, eSEEK_SET);

		IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
		IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

		myHac.ID = pWeight->m_WeightID;
		myHac.N = pWeight->m_W.size();
		myHac.tupleAttributes = 0; // on ne modifie pas ce tuple.
		IS.Write((char*)&myHac.ID, sizeof(myHac.ID), NULL);
		IS.Write((char*)&myHac.N, sizeof(myHac.N), NULL);

		for (unsigned int i = 0; i < myHac.N; i++)
		{
			unsigned short weight = (unsigned short)(0.5 + 10000 * pWeight->m_W[i]);
			IS.Write((char*)&weight, sizeof(weight), NULL);
		}
		// �criture de l'�ventuel blanc
		if (myHac.N % 2 != 0)
		{
			unsigned short blank = 0;
			IS.Write((char*)&blank, sizeof(blank), NULL);
		}

		IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);
		IS.Write((char*)&backlink, sizeof(backlink), NULL);

		iter++;
		offset += backlink;
	}

	return offset;
}

