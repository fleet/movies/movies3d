/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup2310.cpp												  */
/******************************************************************************/

#include "Reader/Tup2310.h"

#include <fstream>
#include <assert.h>
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SignalFilteringObject.h"

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup2310";
}

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup2310::Tup2310()
{
}

void Tup2310::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	libMove essai;
	essai.LowPart = 0;

	IS.Seek(essai, eSEEK_CUR);

	unsigned short filterId;
	IS.Read((char*)& filterId, sizeof(filterId), NULL);

	std::uint32_t nbCoeffs;
	IS.Read((char*)& nbCoeffs, sizeof(nbCoeffs), NULL);

	std::vector<std::complex<float>> coeffs(nbCoeffs);
	if (nbCoeffs > 0)
	{
		for (int i = 0; i < nbCoeffs; ++i)
		{
			float realPart, imagPart;
			IS.Read((char*)&realPart, sizeof(float), NULL);
			IS.Read((char*)&imagPart, sizeof(float), NULL);
			coeffs[i] = std::complex<float>(realPart, imagPart);
		}
	}

	// Positionnement des coefficients du SignalFilteringObject correspondant
	bool bFound = false;
	const MapSounder & mapSounders = trav.m_pHacObjectMgr->GetWorkingSounderMap();
	MapSounder::const_iterator iter;
	for (iter = mapSounders.begin(); iter != mapSounders.end(); ++iter)
	{
		Sounder * pSounder = iter->second;
		for (unsigned int iTrans = 0; iTrans < pSounder->GetTransducerCount(); iTrans++)
		{
			Transducer * pTrans = pSounder->GetTransducer(iTrans);
			if (pTrans->GetFPGAFilter()->m_FilterID == filterId)
			{
				pTrans->GetFPGAFilter()->m_Coefficients = coeffs;
				bFound = true;
			}
			if (pTrans->GetApplicationFilter()->m_FilterID == filterId)
			{
				pTrans->GetApplicationFilter()->m_Coefficients = coeffs;
				bFound = true;
			}
		}
	}

	if (!bFound)
	{
		M3D_LOG_WARN(LoggerName, "Filter identifier of tuple 2310 not found in pr�vious tuples 2300");
	}

	trav.TupleHeaderUpdate();
}

std::uint32_t Tup2310::Encode(MovStream & IS, SignalFilteringObject *pFilter)
{
	std::uint32_t tupleSize = 10 + pFilter->m_Coefficients.size() * sizeof(float);
	unsigned short tupleCode = 2310;
	std::uint32_t backlink = tupleSize + 10;

	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)&pFilter->m_FilterID, sizeof(pFilter->m_FilterID), NULL);

	std::uint32_t nbCoeffs = pFilter->m_Coefficients.size();
	IS.Write((char*)&nbCoeffs, sizeof(nbCoeffs), NULL);

	for (size_t i = 0; i < pFilter->m_Coefficients.size(); i++)
	{
		float realPart = pFilter->m_Coefficients[i].real();
		IS.Write((char*)&realPart, sizeof(float), NULL);

		float imagPart = pFilter->m_Coefficients[i].imag();
		IS.Write((char*)&imagPart, sizeof(float), NULL);
	}

	std::int32_t tupleAttributes = 0;
	IS.Write((char*)& tupleAttributes, sizeof(tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}
