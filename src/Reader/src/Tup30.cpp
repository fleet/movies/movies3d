/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup30.cpp												  */
/******************************************************************************/

#include "Reader/Tup30.h"

#include <fstream>
#include <assert.h>
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/NavAttributes.h"


/******************************************************************************/
/*	Statics																	  */
/******************************************************************************/

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/
Tup30::Tup30()
{
}

void  Tup30::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	//streampos here = (IS.tellg())-6;
	libMove essai;
	essai.LowPart = 0;

	Hac30 myHac;
	IS.Seek(essai, eSEEK_CUR);

	essai.LowPart = 0;

	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCPU, sizeof(myHac.TimeCPU), NULL);
	IS.Read((char*)& myHac.NavigationSystem, sizeof(myHac.NavigationSystem), NULL);
	IS.Read((char*)& myHac.Heading, sizeof(myHac.Heading), NULL);
	IS.Read((char*)& myHac.NavigationSpeed, sizeof(myHac.NavigationSpeed), NULL);
	IS.Read((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);


	NavAttributes *p = NavAttributes::Create();

	double vitesse = (myHac.NavigationSpeed / 1000.0);


	p->m_bIsEstimated = false;
	p->m_speedMeter = vitesse;
	p->m_headingRad = DEG_TO_RAD(myHac.Heading*0.1);
	p->m_ObjectTime.m_TimeFraction = myHac.TimeFraction;
	p->m_ObjectTime.m_TimeCpu = myHac.TimeCPU;
	p->m_NavigationSystem = myHac.NavigationSystem;
	p->m_tupleAttributes = myHac.tupleAttributes;
	MovRef(p);
	// filtre rapide pour prevenir pb reseaux
	////if(myHac.space==0 && p->m_tupleAttributes==0)
	//{
	trav.m_pHacObjectMgr->GetNavAttributesContainer()->AddObject(p);

	// NME - FAE 86 flag de presence des tuples de navigation
	trav.m_pHacObjectMgr->SetHasNavAttributes(true);

	MovUnRefDelete(p);
}

std::uint32_t Tup30::Encode(MovStream & IS, NavAttributes*pNavAttr)
{
	if (!pNavAttr)
		return 0;

	// NMD - FAE 125 - On n'archive pas les informations de navigation si celle-ci sont estim�es
	if (pNavAttr->m_bIsEstimated)
		return 0;

	Hac30		myHac;
	std::uint32_t tupleSize = 18;
	unsigned short tupleCode = 30;
	std::uint32_t backlink = 28;

	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);


	myHac.TimeFraction = pNavAttr->m_ObjectTime.m_TimeFraction;
	myHac.TimeCPU = pNavAttr->m_ObjectTime.m_TimeCpu;
	myHac.NavigationSystem = pNavAttr->m_NavigationSystem;
	myHac.Heading = (short)round(10 * RAD_TO_DEG(pNavAttr->m_headingRad));
	myHac.NavigationSpeed = (unsigned short)round(1000 * pNavAttr->m_speedMeter);
	myHac.space = 0;
	myHac.tupleAttributes = pNavAttr->m_tupleAttributes;

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCPU, sizeof(myHac.TimeCPU), NULL);
	IS.Write((char*)& myHac.NavigationSystem, sizeof(myHac.NavigationSystem), NULL);
	IS.Write((char*)& myHac.Heading, sizeof(myHac.Heading), NULL);
	IS.Write((char*)& myHac.NavigationSpeed, sizeof(myHac.NavigationSpeed), NULL);
	IS.Write((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}
