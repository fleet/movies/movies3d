

#include "Reader/Tup4000.h"
#include "M3DKernel/datascheme/SplitBeamParameter.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/utils/M3DStdUtils.h"


Tup4000::Tup4000(void)
{
}

Tup4000::~Tup4000(void)
{
}

void Tup4000::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	libMove essai;
	essai.LowPart = 0;

	Hac4000 myHac;
	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);

	IS.Read((char*)& myHac.ParentSoftwareId, sizeof(myHac.ParentSoftwareId), NULL);
	IS.Read((char*)& myHac.DetectedSTParameterSubChannelId, sizeof(myHac.DetectedSTParameterSubChannelId), NULL);
	IS.Read((char*)& myHac.MinimumValue, sizeof(myHac.MinimumValue), NULL);
	IS.Read((char*)& myHac.MinimumEchoLength, sizeof(myHac.MinimumEchoLength), NULL);
	IS.Read((char*)& myHac.MaximumEchoLength, sizeof(myHac.MaximumEchoLength), NULL);
	IS.Read((char*)& myHac.MaximumGainCompensation, sizeof(myHac.MaximumGainCompensation), NULL);
	IS.Read((char*)& myHac.MaximumPhaseCompensation, sizeof(myHac.MaximumPhaseCompensation), NULL);
	IS.Read((char*)& myHac.Remark, sizeof(myHac.Remark), NULL);

	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);


	SplitBeamParameter *p = SplitBeamParameter::Create();
	MovRef(p);
	p->m_startLogTime.m_TimeCpu = myHac.TimeCpu;
	p->m_startLogTime.m_TimeFraction = myHac.TimeFraction;
	p->m_splitBeamParameterChannelId = myHac.DetectedSTParameterSubChannelId;

	p->m_minimumValue = myHac.MinimumValue*0.01;
	p->m_minimumEchoLength = myHac.MinimumEchoLength*0.01;
	p->m_maximumEchoLenght = myHac.MaximumEchoLength*0.01;
	p->m_maximumGainCompensation = myHac.MaximumGainCompensation*0.01;
	p->m_maximumPhaseCompensation = myHac.MaximumPhaseCompensation*0.01;
	//strcpy_s(p->m_Remark,sizeof(p->m_Remark),myHac.Remark);
	strcpy_s(p->m_Remark, 30, myHac.Remark);

	p->m_attribute = myHac.tupleAttributes;

	trav.m_pHacObjectMgr->AddSplitBeamParameter(p, myHac.ParentSoftwareId);
	trav.TupleHeaderUpdate();

	MovUnRefDelete(p);
}

std::uint32_t Tup4000::Encode(MovStream & IS, unsigned short aChanId,
	Transducer *pTransducer)
{
	// r�cup�ration des informations concernant le splibeam
	SoftChannel * pSoftChannel = pTransducer->getSoftChannel(aChanId);

	assert(pSoftChannel);

	SplitBeamParameter* splitBeamData = pSoftChannel->GetSplitBeamParameter();

	assert(splitBeamData);

	Hac4000 myHac;
	std::uint32_t tupleSize = 54;
	unsigned short tupleCode = 4000;
	std::uint32_t backlink = 64;
	IS.GetWriteBuffer(backlink);

	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);


	myHac.TimeFraction = splitBeamData->m_startLogTime.m_TimeFraction;
	myHac.TimeCpu = splitBeamData->m_startLogTime.m_TimeCpu;
	myHac.ParentSoftwareId = aChanId;
	myHac.DetectedSTParameterSubChannelId = splitBeamData->m_splitBeamParameterChannelId;
	myHac.MinimumValue = (short)round(splitBeamData->m_minimumValue*100.0);
	myHac.MinimumEchoLength = (unsigned short)round(splitBeamData->m_minimumEchoLength*100.0);
	myHac.MaximumEchoLength = (unsigned short)round(splitBeamData->m_maximumEchoLenght*100.0);
	myHac.MaximumGainCompensation = (unsigned short)round(splitBeamData->m_maximumGainCompensation*100.0);
	myHac.MaximumPhaseCompensation = (unsigned short)round(splitBeamData->m_maximumPhaseCompensation*100.0);
	memset(myHac.Remark, 0, sizeof(myHac.Remark));
	myHac.tupleAttributes = splitBeamData->m_attribute;

	// Ecriture des donn�es dans le flux
	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Write((char*)& myHac.ParentSoftwareId, sizeof(myHac.ParentSoftwareId), NULL);
	IS.Write((char*)& myHac.DetectedSTParameterSubChannelId, sizeof(myHac.DetectedSTParameterSubChannelId), NULL);
	IS.Write((char*)& myHac.MinimumValue, sizeof(myHac.MinimumValue), NULL);
	IS.Write((char*)& myHac.MinimumEchoLength, sizeof(myHac.MinimumEchoLength), NULL);
	IS.Write((char*)& myHac.MaximumEchoLength, sizeof(myHac.MaximumEchoLength), NULL);
	IS.Write((char*)& myHac.MaximumGainCompensation, sizeof(myHac.MaximumGainCompensation), NULL);
	IS.Write((char*)& myHac.MaximumPhaseCompensation, sizeof(myHac.MaximumPhaseCompensation), NULL);
	IS.Write((char*)& myHac.Remark, sizeof(myHac.Remark), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);

	return backlink;
}

