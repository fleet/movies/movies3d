/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup41.cpp												  */
/******************************************************************************/

#include "Reader/Tup41.h"
#include "M3DKernel/BackWardConst.h"


#include <fstream>
#include <assert.h>
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/Platform.h"

using namespace BaseMathLib;

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup41::Tup41()
{

}






/******************************************************************************/
/*	I/O Operations															  */
/******************************************************************************/


void Tup41::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{



	libMove essai;
	essai.LowPart = 0;
	IS.Seek(essai, eSEEK_CUR);

	Hac41 myHac;

	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.DependentAttitudeSensorId, sizeof(myHac.DependentAttitudeSensorId), NULL);
	IS.Read((char*)& myHac.TransChannelId, sizeof(myHac.TransChannelId), NULL);
	IS.Read((char*)& myHac.PlatformType, sizeof(myHac.PlatformType), NULL);
	IS.Read((char*)& myHac.AlongshipOffset, sizeof(myHac.AlongshipOffset), NULL);
	IS.Read((char*)& myHac.AthwartshipOffset, sizeof(myHac.AthwartshipOffset), NULL);
	IS.Read((char*)& myHac.VerticalOffset, sizeof(myHac.VerticalOffset), NULL);



	Platform *p = Platform::Create();

	MovRef(p);

	Vector3D offset;
	offset.x = myHac.AlongshipOffset / 100.0;
	offset.y = myHac.AthwartshipOffset / 100.0;
	offset.z = myHac.VerticalOffset / 100.0;

	p->SetAttitudeOffset(offset);

	p->m_attitudeSensorId = myHac.DependentAttitudeSensorId;
	p->m_timeCpu = myHac.TimeCpu;
	p->m_timeFrac = myHac.TimeFraction;
	p->m_platformType = myHac.PlatformType;

	std::uint32_t sounderId;
	bool found = false;
	if (myHac.TransChannelId != 65535)
	{
#ifdef MOVE_CHANNEL_SOUNDER_ID 
		myHac.TransChannelId += 30;
#endif

		found = trav.m_pHacObjectMgr->FindWorkingSounderContainingChannel(myHac.TransChannelId, sounderId);
	}
	else
	{
		MapSounder::iterator res = trav.m_pHacObjectMgr->GetWorkingSounderMap().begin();
		if (res != trav.m_pHacObjectMgr->GetWorkingSounderMap().end())
		{
			sounderId = res->second->m_SounderId;
			found = true;
		}
	}
	if (found)
	{
		Sounder	*pSounder = trav.m_pHacObjectMgr->GetWorkingSounder(sounderId);

		if (pSounder)
		{
			Transducer *pTrans = pSounder->getTransducerForChannel(myHac.TransChannelId);
			if (pTrans)
			{
				pTrans->SetPlatform(p);
				trav.TupleHeaderUpdate();
			}
			assert(pTrans);
		}

		assert(pSounder);
	}
	MovUnRefDelete(p);
}

std::uint32_t Tup41::Encode(MovStream & IS, Platform *pPlatform, unsigned short channelId)
{
	Hac41 myHac;
	std::uint32_t tupleSize = 54;
	unsigned short tupleCode = 41;
	std::uint32_t backlink = 64;

	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);


	Vector3D offset = pPlatform->GetOffset();



	myHac.TimeCpu = pPlatform->m_timeCpu;
	myHac.TimeFraction = pPlatform->m_timeFrac;
	myHac.DependentAttitudeSensorId = pPlatform->m_attitudeSensorId;
	myHac.TransChannelId = channelId;
	myHac.PlatformType = pPlatform->m_platformType;

	// OTK - 01/06/2009 - correction erreur d'arrondi
	myHac.AlongshipOffset = offset.x > 0 ? (short)(0.5 + offset.x*100.0) : (short)(-0.5 + offset.x*100.0);
	myHac.AthwartshipOffset = offset.y > 0 ? (short)(0.5 + offset.y*100.0) : (short)(-0.5 + offset.y*100.0);
	myHac.VerticalOffset = offset.z > 0 ? (short)(0.5 + offset.z*100.0) : (short)(-0.5 + offset.z*100.0);

	memset(myHac.remarks, 0, sizeof(myHac.remarks));
	myHac.tupleAttributes = 1;
	myHac.space = 0;

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Write((char*)& myHac.DependentAttitudeSensorId, sizeof(myHac.DependentAttitudeSensorId), NULL);
	IS.Write((char*)& myHac.TransChannelId, sizeof(myHac.TransChannelId), NULL);
	IS.Write((char*)& myHac.PlatformType, sizeof(myHac.PlatformType), NULL);
	IS.Write((char*)& myHac.AlongshipOffset, sizeof(myHac.AlongshipOffset), NULL);
	IS.Write((char*)& myHac.AthwartshipOffset, sizeof(myHac.AthwartshipOffset), NULL);
	IS.Write((char*)& myHac.VerticalOffset, sizeof(myHac.VerticalOffset), NULL);
	IS.Write((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Write((char*)& myHac.space, sizeof(myHac.space), NULL);

	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}
