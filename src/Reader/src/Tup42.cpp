/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup42.cpp												  */
/******************************************************************************/

#include "Reader/Tup42.h"

#include <fstream>
#include <assert.h>
#include "M3DKernel/datascheme/Trawl.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "BaseMathLib/Vector3.h"
using namespace BaseMathLib;
/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/


Tup42::Tup42()
{

}



void Tup42::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{


	libMove essai;
	essai.LowPart = 0;



	Hac42 myHac;
	IS.Seek(essai, eSEEK_CUR);

	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.DistanceSensorID, sizeof(myHac.DistanceSensorID), NULL);
	IS.Read((char*)& myHac.DepthSensorId, sizeof(myHac.DepthSensorId), NULL);
	IS.Read((char*)& myHac.TransChannelId, sizeof(myHac.TransChannelId), NULL);
	IS.Read((char*)& myHac.PlatformType, sizeof(myHac.PlatformType), NULL);
	IS.Read((char*)& myHac.DistanceSensorType, sizeof(myHac.DistanceSensorType), NULL);
	IS.Read((char*)& myHac.DepthSensorType, sizeof(myHac.DepthSensorType), NULL);
	IS.Read((char*)& myHac.AlongshipOffset, sizeof(myHac.AlongshipOffset), NULL);
	IS.Read((char*)& myHac.AthwartshipOffset, sizeof(myHac.AthwartshipOffset), NULL);
	IS.Read((char*)& myHac.VerticalOffset, sizeof(myHac.VerticalOffset), NULL);
	IS.Read((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Read((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Read((char*)& myHac.space2, sizeof(myHac.space2), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);


	DynamicPlatformPosition  *mySensor = DynamicPlatformPosition::Create();
	mySensor->m_ObjectTime.m_TimeCpu = myHac.TimeCpu;
	mySensor->m_ObjectTime.m_TimeFraction = myHac.TimeFraction;

	mySensor->m_SensorId.m_depthId = myHac.DepthSensorId;
	mySensor->m_SensorId.m_distanceId = myHac.DistanceSensorID;

	Vector3D offset;
	offset.x = myHac.AlongshipOffset / 100.0;
	offset.y = myHac.AthwartshipOffset / 100.0;
	offset.z = myHac.VerticalOffset;

	mySensor->m_Offset = offset;
	mySensor->m_PlatformType = myHac.PlatformType;
	mySensor->m_DistanceSensorType = myHac.DistanceSensorType;
	mySensor->m_DepthSensorType = myHac.DepthSensorType;
	mySensor->tupleAttributes = myHac.tupleAttributes;
	mySensor->m_TransChannelId = myHac.TransChannelId;

	TimeObjectContainer *objCont = trav.m_pHacObjectMgr->GetTrawl()->GetDynamicPlatformPositionContainer(mySensor->m_SensorId);
	objCont->AddObject(mySensor);
}
std::uint32_t Tup42::Encode(MovStream & IS, DynamicPlatformPosition *pDynPlat)
{
	if (!pDynPlat)
		return 0;
	Hac42 myHac;
	std::uint32_t tupleSize = 62;
	unsigned short tupleCode = 42;
	std::uint32_t backlink = 72;
	IS.GetWriteBuffer(backlink);

	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);

	myHac.TimeFraction = pDynPlat->m_ObjectTime.m_TimeFraction;
	myHac.TimeCpu = pDynPlat->m_ObjectTime.m_TimeCpu;
	myHac.DistanceSensorID = pDynPlat->m_SensorId.m_depthId;
	myHac.DepthSensorId = pDynPlat->m_SensorId.m_distanceId;
	myHac.TransChannelId = pDynPlat->m_TransChannelId;
	myHac.PlatformType = pDynPlat->m_PlatformType;
	myHac.DistanceSensorType = pDynPlat->m_DistanceSensorType;
	myHac.DepthSensorType = pDynPlat->m_DepthSensorType;
	// OTK - 01/06/2009 - correction erreur arrondi
	myHac.AlongshipOffset = pDynPlat->m_Offset.x > 0 ? (short)(0.5 + pDynPlat->m_Offset.x*100.0) : (short)(-0.5 + pDynPlat->m_Offset.x*100.0);
	myHac.AthwartshipOffset = pDynPlat->m_Offset.y > 0 ? (short)(0.5 + pDynPlat->m_Offset.y*100.0) : (short)(-0.5 + pDynPlat->m_Offset.y*100.0);
	myHac.VerticalOffset = pDynPlat->m_Offset.z > 0 ? (short)(0.5 + pDynPlat->m_Offset.z) : (short)(-0.5 + pDynPlat->m_Offset.z);
	myHac.space = 0;
	memset(myHac.remarks, 0, sizeof(myHac.remarks));
	myHac.space2 = 0;
	myHac.tupleAttributes = pDynPlat->tupleAttributes;

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Write((char*)& myHac.DistanceSensorID, sizeof(myHac.DistanceSensorID), NULL);
	IS.Write((char*)& myHac.DepthSensorId, sizeof(myHac.DepthSensorId), NULL);
	IS.Write((char*)& myHac.TransChannelId, sizeof(myHac.TransChannelId), NULL);
	IS.Write((char*)& myHac.PlatformType, sizeof(myHac.PlatformType), NULL);
	IS.Write((char*)& myHac.DistanceSensorType, sizeof(myHac.DistanceSensorType), NULL);
	IS.Write((char*)& myHac.DepthSensorType, sizeof(myHac.DepthSensorType), NULL);
	IS.Write((char*)& myHac.AlongshipOffset, sizeof(myHac.AlongshipOffset), NULL);
	IS.Write((char*)& myHac.AthwartshipOffset, sizeof(myHac.AthwartshipOffset), NULL);
	IS.Write((char*)& myHac.VerticalOffset, sizeof(myHac.VerticalOffset), NULL);
	IS.Write((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Write((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Write((char*)& myHac.space2, sizeof(myHac.space2), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;

}
