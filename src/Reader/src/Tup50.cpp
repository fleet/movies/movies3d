/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup50.cpp												  */
/******************************************************************************/

#include "Reader/Tup50.h"

#include <fstream>
#include <assert.h>
#include "M3DKernel/datascheme/Trawl.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/



Tup50::Tup50()
{

}
/******************************************************************************/
/*	I/O Operations															  */
/******************************************************************************/





void Tup50::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{



	libMove essai;
	essai.LowPart = 0;



	Hac50 myHac;
	IS.Seek(essai, eSEEK_CUR);

	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.HeadRopeDepth, sizeof(myHac.HeadRopeDepth), NULL);
	IS.Read((char*)& myHac.VerticalOpening, sizeof(myHac.VerticalOpening), NULL);
	IS.Read((char*)& myHac.HeadRopeAltitude, sizeof(myHac.HeadRopeAltitude), NULL);
	IS.Read((char*)& myHac.FootRopeAltitude, sizeof(myHac.FootRopeAltitude), NULL);
	IS.Read((char*)& myHac.FootRopeOnBottom, sizeof(myHac.FootRopeOnBottom), NULL);
	IS.Read((char*)& myHac.HorizontalOpening1, sizeof(myHac.HorizontalOpening1), NULL);
	IS.Read((char*)& myHac.HorizontalOpening2, sizeof(myHac.HorizontalOpening2), NULL);
	IS.Read((char*)& myHac.PortDoorAlongShipAngle, sizeof(myHac.PortDoorAlongShipAngle), NULL);
	IS.Read((char*)& myHac.PortDoorAthwartShipAngle, sizeof(myHac.PortDoorAthwartShipAngle), NULL);
	IS.Read((char*)& myHac.AlongShipSpeedOnFootRope, sizeof(myHac.AlongShipSpeedOnFootRope), NULL);
	IS.Read((char*)& myHac.AthwartShipSpeedOnFootRope, sizeof(myHac.AthwartShipSpeedOnFootRope), NULL);
	IS.Read((char*)& myHac.Temperature, sizeof(myHac.Temperature), NULL);
	IS.Read((char*)& myHac.TrawlFill1, sizeof(myHac.TrawlFill1), NULL);
	IS.Read((char*)& myHac.TrawlFill2, sizeof(myHac.TrawlFill2), NULL);
	IS.Read((char*)& myHac.TrawlFill3, sizeof(myHac.TrawlFill3), NULL);
	IS.Read((char*)& myHac.TrawlFill4, sizeof(myHac.TrawlFill4), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	TrawlPositionAttitude *pPosAtt = TrawlPositionAttitude::Create();

	pPosAtt->m_ObjectTime.m_TimeCpu = myHac.TimeCpu;
	pPosAtt->m_ObjectTime.m_TimeFraction = myHac.TimeFraction;

	pPosAtt->m_HeadRopeAlt = myHac.HeadRopeDepth*0.1;
	pPosAtt->m_VOpen = myHac.VerticalOpening / 10.0;

	pPosAtt->m_PortAlongShipAngle = DEG_TO_RAD(myHac.PortDoorAlongShipAngle*0.1);
	pPosAtt->m_PortArthwartAngle = DEG_TO_RAD(myHac.PortDoorAthwartShipAngle*0.1);
	pPosAtt->m_HOpen = myHac.HorizontalOpening1 / 10.0;
	pPosAtt->m_HOpenDoor = myHac.HorizontalOpening2 / 10.0;

	pPosAtt->m_HeadRopeAlt = myHac.HeadRopeAltitude*0.1;
	pPosAtt->m_FootRopeAltitude = myHac.FootRopeAltitude*0.1;
	pPosAtt->m_FootRopeOnBottom = myHac.FootRopeOnBottom;

	pPosAtt->m_AlongShipSpeedOnFootRope = myHac.AlongShipSpeedOnFootRope*0.1;
	pPosAtt->m_AthwartShipSpeedOnFootRope = myHac.AthwartShipSpeedOnFootRope*0.1;
	pPosAtt->m_Temperature = myHac.Temperature*0.1;
	pPosAtt->m_TrawlFill1 = myHac.TrawlFill1;
	pPosAtt->m_TrawlFill2 = myHac.TrawlFill2;
	pPosAtt->m_TrawlFill3 = myHac.TrawlFill3;
	pPosAtt->m_TrawlFill4 = myHac.TrawlFill4;
	pPosAtt->m_tupleAttributes = myHac.tupleAttributes;

	TimeObjectContainer *objCont = trav.m_pHacObjectMgr->GetTrawl()->GetTrawlPositionAttitudeContainer();

	objCont->AddObject(pPosAtt);



}
std::uint32_t Tup50::Encode(MovStream & IS, TrawlPositionAttitude *pPosAtt)
{
	if (!pPosAtt)
		return 0;
	Hac50 myHac;
	std::uint32_t tupleSize = 42;
	unsigned short tupleCode = 50;
	std::uint32_t backlink = 52;
	IS.GetWriteBuffer(backlink);

	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);

	myHac.TimeCpu = pPosAtt->m_ObjectTime.m_TimeCpu;
	myHac.TimeFraction = pPosAtt->m_ObjectTime.m_TimeFraction;
	myHac.HeadRopeDepth = (unsigned short)round(pPosAtt->m_HeadRopeAlt / 0.1);
	myHac.VerticalOpening = (unsigned short)round(pPosAtt->m_VOpen*10.0);
	myHac.HeadRopeAltitude = (unsigned short)round(pPosAtt->m_HeadRopeAlt / 0.1);
	myHac.FootRopeAltitude = (unsigned short)round(pPosAtt->m_FootRopeAltitude / 0.1);
	myHac.FootRopeOnBottom = pPosAtt->m_FootRopeOnBottom;
	myHac.HorizontalOpening1 = (unsigned short)round(pPosAtt->m_HOpen*10.0);
	myHac.HorizontalOpening2 = (unsigned short)round(pPosAtt->m_HOpenDoor*10.0);
	myHac.PortDoorAlongShipAngle = (short)round(RAD_TO_DEG(pPosAtt->m_PortAlongShipAngle) / 0.1);
	myHac.PortDoorAthwartShipAngle = (short)round(RAD_TO_DEG(pPosAtt->m_PortArthwartAngle) / 0.1);
	myHac.AlongShipSpeedOnFootRope = (unsigned short)round(pPosAtt->m_AlongShipSpeedOnFootRope / 0.1);
	myHac.AthwartShipSpeedOnFootRope = (unsigned short)round(pPosAtt->m_AthwartShipSpeedOnFootRope / 0.1);
	myHac.Temperature = (unsigned short)round(pPosAtt->m_Temperature / 0.1);
	myHac.TrawlFill1 = pPosAtt->m_TrawlFill1;
	myHac.TrawlFill2 = pPosAtt->m_TrawlFill2;
	myHac.TrawlFill3 = pPosAtt->m_TrawlFill3;
	myHac.TrawlFill4 = pPosAtt->m_TrawlFill4;
	myHac.tupleAttributes = pPosAtt->m_tupleAttributes;


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Write((char*)& myHac.HeadRopeDepth, sizeof(myHac.HeadRopeDepth), NULL);
	IS.Write((char*)& myHac.VerticalOpening, sizeof(myHac.VerticalOpening), NULL);
	IS.Write((char*)& myHac.HeadRopeAltitude, sizeof(myHac.HeadRopeAltitude), NULL);
	IS.Write((char*)& myHac.FootRopeAltitude, sizeof(myHac.FootRopeAltitude), NULL);
	IS.Write((char*)& myHac.FootRopeOnBottom, sizeof(myHac.FootRopeOnBottom), NULL);
	IS.Write((char*)& myHac.HorizontalOpening1, sizeof(myHac.HorizontalOpening1), NULL);
	IS.Write((char*)& myHac.HorizontalOpening2, sizeof(myHac.HorizontalOpening2), NULL);
	IS.Write((char*)& myHac.PortDoorAlongShipAngle, sizeof(myHac.PortDoorAlongShipAngle), NULL);
	IS.Write((char*)& myHac.PortDoorAthwartShipAngle, sizeof(myHac.PortDoorAthwartShipAngle), NULL);
	IS.Write((char*)& myHac.AlongShipSpeedOnFootRope, sizeof(myHac.AlongShipSpeedOnFootRope), NULL);
	IS.Write((char*)& myHac.AthwartShipSpeedOnFootRope, sizeof(myHac.AthwartShipSpeedOnFootRope), NULL);
	IS.Write((char*)& myHac.Temperature, sizeof(myHac.Temperature), NULL);
	IS.Write((char*)& myHac.TrawlFill1, sizeof(myHac.TrawlFill1), NULL);
	IS.Write((char*)& myHac.TrawlFill2, sizeof(myHac.TrawlFill2), NULL);
	IS.Write((char*)& myHac.TrawlFill3, sizeof(myHac.TrawlFill3), NULL);
	IS.Write((char*)& myHac.TrawlFill4, sizeof(myHac.TrawlFill4), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);



	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;

}
