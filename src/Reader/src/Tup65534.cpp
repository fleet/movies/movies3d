/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup65534.cpp												  */
/******************************************************************************/

#include "Reader/Tup65534.h"

#include <fstream>
#include <assert.h>
#include "Reader/MovReadService.h"
#include "M3DKernel/datascheme/DateTime.h"
/******************************************************************************/
/*	Statics																	  */
/******************************************************************************/



/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup65534::Tup65534()
{

}




void Tup65534::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{

	Hac65534 myHac;

	libMove essai;
	essai.LowPart = 0;
	IS.Seek(essai, eSEEK_CUR);

	IS.Read((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Read((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Read((char*)& myHac.ClosingMode, sizeof(myHac.ClosingMode), NULL);
	IS.Read((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	trav.getUpdateEvent().m_EndSignatureReached = true;
}

std::uint32_t Tup65534::Encode(MovStream & IS, HacTime &refTime)
{

	Hac65534 myHac;
	std::uint32_t tupleSize = 14;
	unsigned short tupleCode = 65534;
	std::uint32_t backlink = 24;

	myHac.TimeFraction = refTime.m_TimeFraction;
	myHac.TimeCpu = refTime.m_TimeCpu;
	myHac.ClosingMode = 1;
	myHac.space = 0;
	myHac.tupleAttributes = 0;

	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture
	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.TimeFraction, sizeof(myHac.TimeFraction), NULL);
	IS.Write((char*)& myHac.TimeCpu, sizeof(myHac.TimeCpu), NULL);
	IS.Write((char*)& myHac.ClosingMode, sizeof(myHac.ClosingMode), NULL);
	IS.Write((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;

}
