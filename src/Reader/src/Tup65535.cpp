/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup65535.cpp												  */
/******************************************************************************/

#include "Reader/Tup65535.h"

#include <fstream>
#include <assert.h>
#include "Reader/MovReadService.h"

/******************************************************************************/
/*	Statics																	  */
/******************************************************************************/



/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup65535::Tup65535()
{
}

void Tup65535::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	Hac65535 myHac;

	libMove essai;
	essai.LowPart = 0;
	IS.Seek(essai, eSEEK_CUR);
	IS.Read((char*)& myHac.HacId, sizeof(myHac.HacId), NULL);
	IS.Read((char*)& myHac.HacVersion, sizeof(myHac.HacVersion), NULL);
	IS.Read((char*)& myHac.AcqSoftwareVersion, sizeof(myHac.AcqSoftwareVersion), NULL);
	IS.Read((char*)& myHac.AcqSoftwareID, sizeof(myHac.AcqSoftwareID), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	HacServiceFormatDesc a;
	a.m_AcqSoftwareID = myHac.AcqSoftwareID;
	a.m_AcqSoftwareVersion = myHac.AcqSoftwareVersion;
	a.m_HacId = myHac.HacId;
	a.m_HacVersion = myHac.HacVersion;
	a.m_tupleAttributes = myHac.tupleAttributes;
	a.m_TupleType = 65535;

	// TODO
	//trav.GetActiveService()->m_ServiceFormatDesc = a;
	trav.TupleHeaderUpdate();
}

std::uint32_t Tup65535::Encode(MovStream & IS)
{
	//
	//if(!trav.GetActiveService())
	//	return 0;

	Hac65535 myHac;
	std::uint32_t tupleSize = 14;
	unsigned short tupleCode = 65535;
	std::uint32_t backlink = 24;
	IS.GetWriteBuffer(backlink);

	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture
//	ServiceFormatDesc a=trav.GetActiveService()->m_ServiceFormatDesc;

	myHac.AcqSoftwareID = 0;//a.m_AcqSoftwareID;
	myHac.AcqSoftwareVersion = 0;//a.m_AcqSoftwareVersion;
	myHac.HacId = 0;//a.m_HacId;
	myHac.HacVersion = 0;//a.m_HacVersion;
	myHac.tupleAttributes = 0;//a.m_tupleAttributes;


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.HacId, sizeof(myHac.HacId), NULL);
	IS.Write((char*)& myHac.HacVersion, sizeof(myHac.HacVersion), NULL);
	IS.Write((char*)& myHac.AcqSoftwareVersion, sizeof(myHac.AcqSoftwareVersion), NULL);
	IS.Write((char*)& myHac.AcqSoftwareID, sizeof(myHac.AcqSoftwareID), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}