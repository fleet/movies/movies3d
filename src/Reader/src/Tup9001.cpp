/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup9001.cpp												  */
/******************************************************************************/

#include "Reader/Tup9001.h"

#include <fstream>
#include <assert.h>
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/BackWardConst.h"
#include "M3DKernel/utils/M3DStdUtils.h"

namespace
{
	constexpr const char * LoggerName = "Reader.Tuple.Tup9001";
}

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/

Tup9001::Tup9001()
{
}

void Tup9001::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{
	libMove essai;
	essai.LowPart = 0;

	Hac9001 *myHac = new Hac9001();
	IS.Seek(essai, eSEEK_CUR);

	IS.Read((char*)& myHac->ChannelId, sizeof(myHac->ChannelId), NULL);
	IS.Read((char*)& myHac->docId, sizeof(myHac->docId), NULL);
#ifdef MOVE_CHANNEL_SOUNDER_ID 
	myHac->ChannelId += 30;
	myHac->docId = 3;
#endif	
	IS.Read((char*)& myHac->SamplingRate, sizeof(myHac->SamplingRate), NULL);
	IS.Read((char*)& myHac->SamplingInterval, sizeof(myHac->SamplingInterval), NULL);
	IS.Read((char*)& myHac->AcousticFrequency, sizeof(myHac->AcousticFrequency), NULL);
	IS.Read((char*)& myHac->TransChannelNumber, sizeof(myHac->TransChannelNumber), NULL);
	IS.Read((char*)& myHac->dataType, sizeof(myHac->dataType), NULL);
	IS.Read((char*)& myHac->TimeVariedGain, sizeof(myHac->TimeVariedGain), NULL);
	IS.Read((char*)& myHac->TVGBlankMode, sizeof(myHac->TVGBlankMode), NULL);
	IS.Read((char*)& myHac->TVGMinRange, sizeof(myHac->TVGMinRange), NULL);
	IS.Read((char*)& myHac->TVGMaxRange, sizeof(myHac->TVGMaxRange), NULL);
	IS.Read((char*)& myHac->BlankUpToRange, sizeof(myHac->BlankUpToRange), NULL);
	IS.Read((char*)& myHac->SampleRange, sizeof(myHac->SampleRange), NULL);
	IS.Read((char*)& myHac->TransDepht, sizeof(myHac->TransDepht), NULL);
	IS.Read((char*)& myHac->PlatFormid, sizeof(myHac->PlatFormid), NULL);
	IS.Read((char*)& myHac->space, sizeof(myHac->space), NULL);

	IS.Read((char*)& myHac->TransAlongOffset, sizeof(myHac->TransAlongOffset), NULL);
	IS.Read((char*)& myHac->TransAthwartOffset, sizeof(myHac->TransAthwartOffset), NULL);
	IS.Read((char*)& myHac->TransVerticalOffset, sizeof(myHac->TransVerticalOffset), NULL);

	IS.Read((char*)& myHac->TransFaceAlongAngleOffset, sizeof(myHac->TransFaceAlongAngleOffset), NULL);
	IS.Read((char*)& myHac->TransFaceAthwartAngleOffset, sizeof(myHac->TransFaceAthwartAngleOffset), NULL);
	IS.Read((char*)& myHac->RotationAngle, sizeof(myHac->RotationAngle), NULL);

	IS.Read((char*)& myHac->TransMainBeamAlongBeamAngleOffset, sizeof(myHac->TransMainBeamAlongBeamAngleOffset), NULL);
	IS.Read((char*)& myHac->TransMainBeamAthwartAngleOffset, sizeof(myHac->TransMainBeamAthwartAngleOffset), NULL);

	IS.Read((char*)& myHac->SoundAbsorption, sizeof(myHac->SoundAbsorption), NULL);
	IS.Read((char*)& myHac->PulseDuration, sizeof(myHac->PulseDuration), NULL);
	IS.Read((char*)& myHac->TransShape, sizeof(myHac->TransShape), NULL);
	IS.Read((char*)& myHac->BandWidth, sizeof(myHac->BandWidth), NULL);
	IS.Read((char*)& myHac->TransShapeMode, sizeof(myHac->TransShapeMode), NULL);
	IS.Read((char*)& myHac->Along3DbWidth, sizeof(myHac->Along3DbWidth), NULL);
	IS.Read((char*)& myHac->Athwart3DbWidth, sizeof(myHac->Athwart3DbWidth), NULL);
	IS.Read((char*)& myHac->TwoWayBeamAngle, sizeof(myHac->TwoWayBeamAngle), NULL);

	IS.Read((char*)& myHac->CalibSourceLevel, sizeof(myHac->CalibSourceLevel), NULL);
	IS.Read((char*)& myHac->CalibReceivLevel, sizeof(myHac->CalibReceivLevel), NULL);
	IS.Read((char*)& myHac->SVVR, sizeof(myHac->SVVR), NULL);

	IS.Read((char*)& myHac->BottomDetectionMinLevel, sizeof(myHac->BottomDetectionMinLevel), NULL);
	IS.Read((char*)& myHac->BottomDetectionMinDepth, sizeof(myHac->BottomDetectionMinDepth), NULL);
	IS.Read((char*)& myHac->BottomDetectionMaxDepth, sizeof(myHac->BottomDetectionMaxDepth), NULL);
	IS.Read((char*)& myHac->remarks, sizeof(myHac->remarks), NULL);
	IS.Read((char*)& myHac->tupleAttributes, sizeof(myHac->tupleAttributes), NULL);

	Sounder	*pSounder = trav.m_pHacObjectMgr->GetWorkingSounder(myHac->docId);

	// OTK - FAE067 - si pas de sondeur en cours de construction, on recopie le sondeur existant
	// et on repart de cet objet pour mettre � jour le channel
	if (pSounder == NULL)
	{
		Sounder* pExistingSounder = trav.m_pHacObjectMgr->GetSounderDefinition().GetSounderWithId(myHac->docId);
		if (pExistingSounder == NULL)
		{
			// rien � faire : le tuple est perdu
			delete myHac;
			M3D_LOG_WARN(LoggerName, "Receiving tuple 9001 with no <building> 901");
		}
		else
		{
			// NMD - FE 94 - Sondeur generique multi faisceaux
			if (pExistingSounder->m_isMultiBeam)
			{
				SounderMulti * pNewSounder = SounderMulti::Create();
				*pNewSounder = *((SounderMulti*)pExistingSounder);
				pNewSounder->DataChanged();
				trav.m_pHacObjectMgr->AddSounder(pNewSounder);
				pSounder = pNewSounder;
			}
			else
			{
				// cr�ation et recopie du sondeur existant
				SounderGeneric *pNewSounder = SounderGeneric::Create();
				*pNewSounder = *((SounderGeneric*)pExistingSounder);
				pNewSounder->DataChanged();
				trav.m_pHacObjectMgr->AddSounder(pNewSounder);
				pSounder = pNewSounder;
			}
		}
	}

	if (pSounder)
	{
		Transducer * pTrans = NULL;

		// NMD - FE 94 - Sondeur generique multi faisceaux
		if (pSounder->m_isMultiBeam)
		{
			pTrans = pSounder->GetTransducer(0);
			if (pTrans != NULL)
				MovRef(pTrans);

			// NMD - FE 094 : verification du platform id pour les sondeurs multi-faisceaux
			if (pTrans->getSoftChannel(0) != NULL && pTrans->m_platformId != myHac->PlatFormid)
			{
				M3D_LOG_WARN(LoggerName, "Receiving a different platform Id for a transducer of the multi-beam generic sounder");

				// retrogradage du sondeur
				Sounder * pGenericSounder = SounderGeneric::Create();
				pGenericSounder->m_SounderId = pSounder->m_SounderId;
				pGenericSounder->m_soundVelocity = pSounder->m_soundVelocity;
				pGenericSounder->m_triggerMode = pSounder->m_triggerMode;
				pGenericSounder->m_numberOfTransducer = pTrans->m_numberOfSoftChannel;
				pGenericSounder->m_pingInterval = pSounder->m_pingInterval;
				pGenericSounder->m_isMultiBeam = false;
				pGenericSounder->m_tupleType = pSounder->m_tupleType;

				// recopie des soft channel : 1 soft channel par transducer
				for (int i = 1; ; i++)
				{
					SoftChannel * pSoftChannel = pTrans->getSoftChannel(i);
					if (pSoftChannel == NULL)
						break;

					Transducer * pTransducer = Transducer::Create();

					//copie des attributs du transducer
					char TransName[50];
                    sprintf(TransName, "Generic Transducer %d", pSoftChannel->m_softChannelId);
					strcpy_s(pTransducer->m_transName, 50, TransName);
					strcpy_s(pTransducer->m_transSoftVersion, 30, "0.0");
					pTransducer->m_timeSampleInterval = pTrans->m_timeSampleInterval;
					pTransducer->m_transDepthMeter = pTrans->m_transDepthMeter;
					pTransducer->m_platformId = pTrans->m_platformId;
					pTransducer->m_transShape = pTrans->m_transShape;
					pTransducer->m_transFaceAlongAngleOffsetRad = pTrans->m_transFaceAlongAngleOffsetRad;
					pTransducer->m_bDefaultFaceAlongAngleOffset = pTrans->m_bDefaultFaceAlongAngleOffset;
					pTransducer->m_transFaceAthwarAngleOffsetRad = pTrans->m_transFaceAthwarAngleOffsetRad;
					pTransducer->m_bDefaultFaceAthwartAngleOffset = pTrans->m_bDefaultFaceAthwartAngleOffset;
					pTransducer->m_transRotationAngleRad = pTrans->m_bDefaultFaceAthwartAngleOffset;
					pTransducer->m_bDefaultRotation = pTrans->m_bDefaultRotation;
					pTransducer->m_pulseDuration = pTrans->m_pulseDuration;
					pTransducer->m_pulseForm = pTrans->m_pulseForm;
					pTransducer->m_frequencyBeamSpacing = pTrans->m_frequencyBeamSpacing;
					pTransducer->m_frequencySpaceShape = pTrans->m_frequencySpaceShape;
					pTransducer->m_transPower = pTrans->m_transPower;
					pTransducer->m_numberOfSoftChannel = 1;
					pTransducer->DataChanged();

					double beamSampleSpacing = pTransducer->computeSampleSpacing(pSounder->m_soundVelocity);
					pTransducer->setBeamSamplesSpacing(beamSampleSpacing);

					pTransducer->AddSoftChannel(pSoftChannel);

					pGenericSounder->addTransducer(pTransducer);
				}

				pGenericSounder->DataChanged();

				// on remplace le sondeur
				//trav.m_pHacObjectMgr->GetSounderDefinition().RemoveSounder(pSounder);

				//trav.m_pHacObjectMgr->GetPingFanContainer().UpdateRefSounder(pGenericSounder);
				trav.m_pHacObjectMgr->AddSounder(pGenericSounder);

				pSounder = pGenericSounder;

				MovUnRefDelete(pTrans);

				pTrans = pGenericSounder->getTransducerForChannel(myHac->ChannelId);
			}
		}

		bool bAddTransducer = false;
		if (pTrans == NULL)
		{
			// now create the transducer Object 
			pTrans = Transducer::Create();
			bAddTransducer = true;
		}
		MovRef(pTrans);

		//update transducer parameter
		char TransName[50];
		if (pSounder->m_isMultiBeam)
            sprintf(TransName, "Generic MultiBeam Transducer");
		else
            sprintf(TransName, "Generic Transducer %d", myHac->ChannelId);

		strcpy_s(pTrans->m_transName, 50, TransName);
		strcpy_s(pTrans->m_transSoftVersion, 30, "0.0");
		pTrans->m_timeSampleInterval = 1000000 / myHac->SamplingRate; // time sample interval micro seconds
		// OTK - FAE047 - comme dans Movies+, si la valeur est la valeur non d�finie, on prend 0.
		if (myHac->TransDepht != 4294967295)
		{
			pTrans->m_transDepthMeter = myHac->TransDepht*0.0001;
		}
		else
		{
			pTrans->m_transDepthMeter = 0;
		}
		pTrans->m_platformId = myHac->PlatFormid;

		pTrans->m_transShape = myHac->TransShape;
		if (myHac->TransFaceAlongAngleOffset != -32768 && myHac->TransFaceAlongAngleOffset != 32767)
		{
			pTrans->m_transFaceAlongAngleOffsetRad = DEG_TO_RAD((myHac->TransFaceAlongAngleOffset*0.01));
		}
		else
		{
			pTrans->m_transFaceAlongAngleOffsetRad = 0;
			pTrans->m_bDefaultFaceAlongAngleOffset = true;
		}
		if (myHac->TransFaceAthwartAngleOffset != -32768)
			pTrans->m_transFaceAthwarAngleOffsetRad = DEG_TO_RAD((myHac->TransFaceAthwartAngleOffset*0.01));
		else
		{
			pTrans->m_transFaceAthwarAngleOffsetRad = 0;
			pTrans->m_bDefaultFaceAthwartAngleOffset = true;
		}
		
		if (myHac->RotationAngle != -32768)
		{
			pTrans->m_transRotationAngleRad = DEG_TO_RAD(myHac->RotationAngle*0.01);
		}
		else
		{
			pTrans->m_transRotationAngleRad = 0;
			pTrans->m_bDefaultRotation = true;
		}

		pTrans->m_pulseDuration = myHac->PulseDuration*0.1;

		pTrans->m_pulseForm = 65535;  // pulse form 
		pTrans->m_frequencyBeamSpacing = 65535;
		pTrans->m_frequencySpaceShape = 3;
		pTrans->m_transPower = 1;

		if (!pSounder->m_isMultiBeam) {
			pTrans->m_numberOfSoftChannel = 1;
		}

		pTrans->DataChanged();

		////

		if (bAddTransducer)
			pSounder->addTransducer(pTrans);

		SoftChannel *p = SoftChannel::Create();
		MovRef(p);

		p->SetTupleUserData(myHac);

		/// inside data
		p->m_tupleType = 9001;
		p->m_softChannelComputeData.m_groupId = 0;
		p->m_softChannelComputeData.m_isReferenceBeam = false;
		p->m_softChannelComputeData.m_isMultiBeam = pSounder->m_isMultiBeam;
		p->m_softChannelComputeData.m_PolarId = 0;

		/// start Copying
		p->m_softChannelId = myHac->ChannelId;
		char ChanName[48];
        sprintf(ChanName, "Generic Channel %d", myHac->ChannelId);
		strcpy_s(p->m_channelName, 48, ChanName);
		p->m_dataType = myHac->dataType;
		p->m_beamType = 0;
		p->m_acousticFrequency = myHac->AcousticFrequency;
		// OTK - FAE047 - comme dans Movies+, si la valeur est la valeur non d�finie, on prend 0.
		if (myHac->BlankUpToRange != 4294967295)
		{
			p->m_startSample = myHac->BlankUpToRange*0.0001 / (myHac->SamplingInterval*0.000001);
		}
		else
		{
			p->m_startSample = 0;
		}

		if (myHac->TransMainBeamAlongBeamAngleOffset != -32768)
		{
			p->m_mainBeamAlongSteeringAngleRad = DEG_TO_RAD((myHac->TransMainBeamAlongBeamAngleOffset*0.01));
		}
		else
		{
			p->m_mainBeamAlongSteeringAngleRad = 0;
			p->m_bDefaultAlongAngleOffset = true;
		}

		if (myHac->TransMainBeamAthwartAngleOffset != -32768)
		{
			p->m_mainBeamAthwartSteeringAngleRad = DEG_TO_RAD((myHac->TransMainBeamAthwartAngleOffset*0.01));
		}
		else
		{
			p->m_mainBeamAthwartSteeringAngleRad = 0;
			p->m_bDefaultAthwartAngleOffset = true;
		}

		p->m_absorptionCoefHAC = myHac->SoundAbsorption * 100;
		p->m_absorptionCoef = p->m_absorptionCoefHAC;
		p->m_bandWidth = myHac->BandWidth * 10;
		p->m_transmissionPower = 0;
		p->m_beamAlongAngleSensitivity = 0;
		p->m_beamAthwartAngleSensitivity = 0;
		p->m_beam3dBWidthAlongRad = DEG_TO_RAD(myHac->Along3DbWidth*0.1);
		p->m_beam3dBWidthAthwartRad = DEG_TO_RAD(myHac->Athwart3DbWidth*0.1);
		p->m_beamEquTwoWayAngle = myHac->TwoWayBeamAngle*0.01;
		p->m_beamGain = 0;
		p->m_beamSACorrection = 0;
		p->m_bottomDetectionMinDepth = myHac->BottomDetectionMinDepth*0.01;
		p->m_bottomDetectionMaxDepth = myHac->BottomDetectionMaxDepth*0.01;
		p->m_bottomDetectionMinLevel = myHac->BottomDetectionMinLevel*0.01;
		p->m_AlongTXRXWeightId = 65535;
		p->m_AthwartTXRXWeightId = 65535;
		p->m_SplitBeamAlongTXRXWeightId = 65535;
		p->m_SplitBeamAthwartTXRXWeightId = 65535;

		p->m_calibMainBeamAlongSteeringAngleRad = p->m_mainBeamAlongSteeringAngleRad;
		p->m_calibMainBeamAthwartSteeringAngleRad = p->m_mainBeamAthwartSteeringAngleRad;
		p->m_calibBeam3dBWidthAlongRad = p->m_beam3dBWidthAlongRad;
		p->m_calibBeam3dBWidthAthwartRad = p->m_beam3dBWidthAthwartRad;
		p->m_calibBeamGain = p->m_beamGain;
		p->m_calibBeamSACorrection = p->m_beamSACorrection;

		pTrans->AddSoftChannel(p);

		MovUnRefDelete(p);
		MovUnRefDelete(pTrans);
	}
	trav.TupleHeaderUpdate();
}

std::uint32_t Tup9001::Encode(MovStream & IS, Sounder *pSounder, Transducer *pTransducer)
{
	Hac9001 *myHac = NULL;
	std::uint32_t tupleSize = 146;
	unsigned short tupleCode = 9001;
	std::uint32_t backlink = 156;

	IS.GetWriteBuffer(backlink);
	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture

	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	SoftChannel *p = pTransducer->getSoftChannelPolarX(0);
	myHac = (Hac9001 *)p->GetTupleUserData();
		
	IS.Write((char*)& myHac->ChannelId, sizeof(myHac->ChannelId), NULL);
	IS.Write((char*)& myHac->docId, sizeof(myHac->docId), NULL);
	IS.Write((char*)& myHac->SamplingRate, sizeof(myHac->SamplingRate), NULL);
	IS.Write((char*)& myHac->SamplingInterval, sizeof(myHac->SamplingInterval), NULL);
	IS.Write((char*)& myHac->AcousticFrequency, sizeof(myHac->AcousticFrequency), NULL);
	IS.Write((char*)& myHac->TransChannelNumber, sizeof(myHac->TransChannelNumber), NULL);
	IS.Write((char*)& myHac->dataType, sizeof(myHac->dataType), NULL);
	IS.Write((char*)& myHac->TimeVariedGain, sizeof(myHac->TimeVariedGain), NULL);
	IS.Write((char*)& myHac->TVGBlankMode, sizeof(myHac->TVGBlankMode), NULL);
	IS.Write((char*)& myHac->TVGMinRange, sizeof(myHac->TVGMinRange), NULL);
	IS.Write((char*)& myHac->TVGMaxRange, sizeof(myHac->TVGMaxRange), NULL);
	std::uint32_t zero = 0;
	IS.Write((char*)& zero/*myHac->BlankUpToRange*/, sizeof(myHac->BlankUpToRange), NULL);
	IS.Write((char*)& myHac->SampleRange, sizeof(myHac->SampleRange), NULL);
	IS.Write((char*)& myHac->TransDepht, sizeof(myHac->TransDepht), NULL);
	IS.Write((char*)& myHac->PlatFormid, sizeof(myHac->PlatFormid), NULL);
	IS.Write((char*)& myHac->space, sizeof(myHac->space), NULL);
	IS.Write((char*)& myHac->TransAlongOffset, sizeof(myHac->TransAlongOffset), NULL);
	IS.Write((char*)& myHac->TransAthwartOffset, sizeof(myHac->TransAthwartOffset), NULL);
	IS.Write((char*)& myHac->TransVerticalOffset, sizeof(myHac->TransVerticalOffset), NULL);
	short defaultValue = -32768;

	if (pTransducer->m_bDefaultFaceAlongAngleOffset)
	{
		IS.Write((char*)& defaultValue, sizeof(myHac->TransFaceAlongAngleOffset), NULL);
	}
	else
	{
		IS.Write((char*)& myHac->TransFaceAlongAngleOffset, sizeof(myHac->TransFaceAlongAngleOffset), NULL);
	}

	if (pTransducer->m_bDefaultFaceAthwartAngleOffset)
	{
		IS.Write((char*)& defaultValue, sizeof(myHac->TransFaceAthwartAngleOffset), NULL);
	}
	else
	{
		IS.Write((char*)& myHac->TransFaceAthwartAngleOffset, sizeof(myHac->TransFaceAthwartAngleOffset), NULL);
	}

	if (pTransducer->m_bDefaultRotation)
	{
		IS.Write((char*)& defaultValue, sizeof(myHac->RotationAngle), NULL);
	}
	else
	{
		IS.Write((char*)& myHac->RotationAngle, sizeof(myHac->RotationAngle), NULL);
	}

	if (p->m_bDefaultAlongAngleOffset)
	{
		IS.Write((char*)& defaultValue, sizeof(myHac->TransMainBeamAlongBeamAngleOffset), NULL);
	}
	else
	{
		short TransMainBeamAlongBeamAngleOffset = RAD_TO_DEG(p->m_calibMainBeamAlongSteeringAngleRad * 100);
		IS.Write((char*)&TransMainBeamAlongBeamAngleOffset, sizeof(TransMainBeamAlongBeamAngleOffset), NULL);
	}

	if (p->m_bDefaultAthwartAngleOffset)
	{
		IS.Write((char*)& defaultValue, sizeof(myHac->TransMainBeamAthwartAngleOffset), NULL);
	}
	else
	{
		short TransMainBeamAthwartAngleOffset = RAD_TO_DEG(p->m_calibMainBeamAthwartSteeringAngleRad * 100);
		IS.Write((char*)&TransMainBeamAthwartAngleOffset, sizeof(TransMainBeamAthwartAngleOffset), NULL);
	}

	IS.Write((char*)& myHac->SoundAbsorption, sizeof(myHac->SoundAbsorption), NULL);
	IS.Write((char*)& myHac->PulseDuration, sizeof(myHac->PulseDuration), NULL);
	IS.Write((char*)& myHac->TransShape, sizeof(myHac->TransShape), NULL);
	IS.Write((char*)& myHac->BandWidth, sizeof(myHac->BandWidth), NULL);
	IS.Write((char*)& myHac->TransShapeMode, sizeof(myHac->TransShapeMode), NULL);
	
	unsigned short Along3DbWidth = RAD_TO_DEG(p->m_calibBeam3dBWidthAlongRad * 10);
	unsigned short Athwart3DbWidth = RAD_TO_DEG(p->m_calibBeam3dBWidthAthwartRad * 10);
	IS.Write((char*)&Along3DbWidth, sizeof(Along3DbWidth), NULL);
	IS.Write((char*)&Athwart3DbWidth, sizeof(Athwart3DbWidth), NULL);
	
	IS.Write((char*)& myHac->TwoWayBeamAngle, sizeof(myHac->TwoWayBeamAngle), NULL);
	IS.Write((char*)& myHac->CalibSourceLevel, sizeof(myHac->CalibSourceLevel), NULL);
	IS.Write((char*)& myHac->CalibReceivLevel, sizeof(myHac->CalibReceivLevel), NULL);
	IS.Write((char*)& myHac->SVVR, sizeof(myHac->SVVR), NULL);
	IS.Write((char*)& myHac->BottomDetectionMinLevel, sizeof(myHac->BottomDetectionMinLevel), NULL);
	IS.Write((char*)& myHac->BottomDetectionMinDepth, sizeof(myHac->BottomDetectionMinDepth), NULL);
	IS.Write((char*)& myHac->BottomDetectionMaxDepth, sizeof(myHac->BottomDetectionMaxDepth), NULL);
	IS.Write((char*)& myHac->remarks, sizeof(myHac->remarks), NULL);
	IS.Write((char*)& myHac->tupleAttributes, sizeof(myHac->tupleAttributes), NULL);

	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}
