/******************************************************************************/
/*	Project:	SMFH 3D														  */
/*	Author(s):	R.  LHULLIER				     							  */
/*	File:		Tup901.cpp												  */
/******************************************************************************/

#include "Reader/Tup901.h"

#include <fstream>
#include <assert.h>

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SounderMulti.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/BackWardConst.h"

/******************************************************************************/
/*	Constructors / Destructor												  */
/******************************************************************************/




Tup901::Tup901()
{

}





void Tup901::Decode(MovStream& IS, ReaderTraverser &trav, std::uint32_t taille_tup)
{



	libMove posit;
	posit.LowPart = 0;


	Hac901 myHac;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture
	//unsigned short

	IS.Read((char*)& myHac.numOfChannel, sizeof(myHac.numOfChannel), NULL);
	IS.Read((char*)& myHac.docId, sizeof(myHac.docId), NULL);
	IS.Read((char*)& myHac.soundSpeed, sizeof(myHac.soundSpeed), NULL);
	IS.Read((char*)& myHac.pingInterval, sizeof(myHac.pingInterval), NULL);
	IS.Read((char*)& myHac.triggerMode, sizeof(myHac.triggerMode), NULL);
	IS.Read((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Read((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Read((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);

#ifdef MOVE_CHANNEL_SOUNDER_ID 
	myHac.docId = 3;
#endif


	// NMD - FE 94 - Sondeur generique multi faisceaux
	Sounder * p = NULL;
	if (myHac.numOfChannel > 1)
	{
		p = trav.m_pHacObjectMgr->GetWorkingSounder(myHac.docId);
		if (p != NULL)
			return;
		p = SounderMulti::Create();

		p->m_SounderId = myHac.docId;
		p->m_soundVelocity = myHac.soundSpeed*0.1;
		p->m_triggerMode = myHac.triggerMode;
		p->m_numberOfTransducer = 1;
		p->m_pingInterval = myHac.pingInterval*0.01;
		p->m_isMultiBeam = true;
		p->m_tupleType = 901;

		// Creation d'un transducer avec le bon nombre de channel
		Transducer *pTrans = Transducer::Create();
		MovRef(pTrans);
		pTrans->m_numberOfSoftChannel = myHac.numOfChannel;
		pTrans->DataChanged();
		p->addTransducer(pTrans);
		MovUnRefDelete(pTrans);
	}
	else
	{
		p = SounderGeneric::Create();

		p->m_SounderId = myHac.docId;
		p->m_soundVelocity = myHac.soundSpeed*0.1;
		p->m_triggerMode = myHac.triggerMode;
		p->m_numberOfTransducer = myHac.numOfChannel;
		p->m_pingInterval = myHac.pingInterval*0.01;
		p->m_isMultiBeam = false;
		p->m_tupleType = 901;
	}
	p->SetRemarks(myHac.remarks);

	p->DataChanged();

	MovRef(p);

	trav.m_pHacObjectMgr->AddSounder(p);
	trav.TupleHeaderUpdate();

	MovUnRefDelete(p);


}
std::uint32_t Tup901::Encode(MovStream & IS, SounderGeneric *p)
{
	Hac901 myHac;
	std::uint32_t tupleSize = 118;
	unsigned short tupleCode = 901;
	std::uint32_t backlink = 128;

	IS.GetWriteBuffer(backlink);

	libMove posit;
	posit.LowPart = 0;
	IS.Seek(posit, eSEEK_CUR);            //positionnement du "get pointer" pour lecture

	myHac.numOfChannel = p->m_numberOfTransducer;
	myHac.docId = p->m_SounderId;
	myHac.soundSpeed = (unsigned short)round(p->m_soundVelocity / 0.1);
	myHac.pingInterval = (unsigned short)round(p->m_pingInterval / 0.01);
	myHac.triggerMode = p->m_triggerMode;
	myHac.space = 0;
	strncpy(myHac.remarks, p->GetRemarks(), sizeof(p->GetRemarks()));
	myHac.tupleAttributes = 1;


	IS.Write((char*)&tupleSize, sizeof(tupleSize), NULL);
	IS.Write((char*)&tupleCode, sizeof(tupleCode), NULL);

	IS.Write((char*)& myHac.numOfChannel, sizeof(myHac.numOfChannel), NULL);
	IS.Write((char*)& myHac.docId, sizeof(myHac.docId), NULL);
	IS.Write((char*)& myHac.soundSpeed, sizeof(myHac.soundSpeed), NULL);
	IS.Write((char*)& myHac.pingInterval, sizeof(myHac.pingInterval), NULL);
	IS.Write((char*)& myHac.triggerMode, sizeof(myHac.triggerMode), NULL);
	IS.Write((char*)& myHac.space, sizeof(myHac.space), NULL);
	IS.Write((char*)& myHac.remarks, sizeof(myHac.remarks), NULL);
	IS.Write((char*)& myHac.tupleAttributes, sizeof(myHac.tupleAttributes), NULL);


	IS.Write((char*)&backlink, sizeof(backlink), NULL);
	return backlink;
}



