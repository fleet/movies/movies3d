
#include "Reader/TupleDecoder.h"

#include "Reader/Tup220.h"
#include "Reader/Tup10040.h"
#include "Reader/Tup10060.h"
#include "Reader/Tup10140.h"
#include "Reader/Tup20.h"
#include "Reader/Tup2200.h"
#include "Reader/Tup2210.h"
#include "Reader/Tup30.h"
#include "Reader/Tup210.h"
#include "Reader/Tup2100.h"
#include "Reader/Tup230.h"
#include "Reader/Tup2300.h"
#include "Reader/Tup2310.h"

#include "Reader/Tup41.h"

#include "Reader/Tup42.h"
#include "Reader/Tup10142.h"
#include "Reader/Tup50.h"
#include "Reader/Tup10110.h"
#include "Reader/Tup65534.h"
#include "Reader/Tup65535.h"

#include "Reader/Tup200.h"
#include "Reader/Tup2001.h"
#include "Reader/Tup11000.h"
#include "Reader/Tup10100.h"
#include "Reader/Tup10011.h"
#include "Reader/Tup10090.h"
#include "Reader/Tup4000.h"
#include "Reader/Tup901.h"
#include "Reader/Tup9001.h"

#include "Reader/Tup10000.h"
#include "Reader/Tup10030.h"
#include "Reader/Tup10031.h"

#include "Reader/Tup10060.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"

TupleDecoder::TupleDecoder(void)
{

	this->m_tuple.insert(MapTuple::value_type(2100, new Tup2100()));
	this->m_tuple.insert(MapTuple::value_type(2200, new Tup2200()));
	this->m_tuple.insert(MapTuple::value_type(2210, new Tup2210()));
	this->m_tuple.insert(MapTuple::value_type(2300, new Tup2300()));
	this->m_tuple.insert(MapTuple::value_type(2310, new Tup2310()));
	this->m_tuple.insert(MapTuple::value_type(210, new Tup210()));
	this->m_tuple.insert(MapTuple::value_type(220, new Tup220()));
	this->m_tuple.insert(MapTuple::value_type(230, new Tup230()));

	this->m_tuple.insert(MapTuple::value_type(10000, new Tup10000()));
	this->m_tuple.insert(MapTuple::value_type(10030, new Tup10030()));
	this->m_tuple.insert(MapTuple::value_type(10031, new Tup10031()));
	this->m_tuple.insert(MapTuple::value_type(10040, new Tup10040()));
	this->m_tuple.insert(MapTuple::value_type(10060, new Tup10060()));
	this->m_tuple.insert(MapTuple::value_type(10140, new Tup10140()));
	this->m_tuple.insert(MapTuple::value_type(20, new Tup20()));
	this->m_tuple.insert(MapTuple::value_type(30, new Tup30()));
	this->m_tuple.insert(MapTuple::value_type(41, new Tup41()));
	this->m_tuple.insert(MapTuple::value_type(65534, new Tup65534()));
	this->m_tuple.insert(MapTuple::value_type(10142, new Tup10142()));
	this->m_tuple.insert(MapTuple::value_type(42, new Tup42()));
	this->m_tuple.insert(MapTuple::value_type(50, new Tup50()));
	this->m_tuple.insert(MapTuple::value_type(10110, new Tup10110()));
	this->m_tuple.insert(MapTuple::value_type(200, new Tup200()));
	this->m_tuple.insert(MapTuple::value_type(2001, new Tup2001()));
	this->m_tuple.insert(MapTuple::value_type(65535, new Tup65535()));
	this->m_tuple.insert(MapTuple::value_type(10100, new Tup10100()));
	this->m_tuple.insert(MapTuple::value_type(11000, new Tup11000()));
	this->m_tuple.insert(MapTuple::value_type(10011, new Tup10011()));
	this->m_tuple.insert(MapTuple::value_type(10090, new Tup10090()));
	this->m_tuple.insert(MapTuple::value_type(4000, new Tup4000()));
	this->m_tuple.insert(MapTuple::value_type(901, new Tup901()));
	this->m_tuple.insert(MapTuple::value_type(9001, new Tup9001()));

	// OTK - 21/12/2009 - detection de la lecture des premiers pings pour clore la definition
	// des sondeurs m�me si le nombre de voies est incoh�rent.
	this->m_pingTuples.insert(MapTuple::value_type(10000, new Tup10000()));
	this->m_pingTuples.insert(MapTuple::value_type(10011, new Tup10011()));
	this->m_pingTuples.insert(MapTuple::value_type(10030, new Tup10030()));
	this->m_pingTuples.insert(MapTuple::value_type(10031, new Tup10031()));
	this->m_pingTuples.insert(MapTuple::value_type(10040, new Tup10040()));
	this->m_pingTuples.insert(MapTuple::value_type(10060, new Tup10060()));
	this->m_pingTuples.insert(MapTuple::value_type(10090, new Tup10090()));


}

TupleDecoder::~TupleDecoder(void)
{
	MapTuple::iterator res = m_tuple.begin();
	while (res != m_tuple.end())
	{
		if (res->second != NULL)
			delete(res->second);
		res++;
	}
	m_tuple.clear();

	res = m_pingTuples.begin();
	while (res != m_pingTuples.end())
	{
		if (res->second != NULL)
			delete(res->second);
		res++;
	}
	m_pingTuples.clear();
}

void TupleDecoder::Decode(ReaderTraverser &ref, MovStream *pStream)
{
	//	utilise des constructeurs de tuples avec en parametre IS 
	std::uint32_t taille_tup;
	unsigned short code_tup;


	libMove posSt;
	posSt.LowPart = 0;



	pStream->Seek(posSt, eSEEK_SET);


	pStream->Read((char*)& taille_tup, sizeof(taille_tup), NULL);

	pStream->Read((char*)& code_tup, sizeof(code_tup), NULL);

	// OTK - 21/12/2009 - detection de la lecture des premiers pings pour clore la definition
	// des sondeurs m�me si le nombre de voies est incoh�rent.
	MapTuple::iterator res = this->m_pingTuples.find(code_tup);
	if (res != m_pingTuples.end())
	{
		// Dans ce cas, on force la fermeture de la d�finition des sondeurs
		ref.m_pHacObjectMgr->CorrectChannelNumbers(ref);
	}

	res = this->m_tuple.find(code_tup);
	if (res != m_tuple.end())
	{
		res->second->DecodeOrIgnore(*pStream, ref, taille_tup);
	}
}
