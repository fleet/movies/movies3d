/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		WriteAlgorithm.cpp											  */
/******************************************************************************/
#include "Reader/WriteAlgorithm/WriteAlgorithm.h"
#include "M3DKernel/algorithm/base/EchoAlgorithm.h"

#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/SounderMulti.h"

#include "Reader/WriteAlgorithm/WriteAlgorithmToFileHac.h"
#include "Reader/WriteAlgorithm/WriteAlgorithmToFileNetCDF.h"

#include "EchoIntegration/EchoIntegrationModule.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "M3DKernel/M3DKernel.h"
#include "Reader/MovReadService.h"

#ifndef WIN32
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "M3DKernel/utils/M3DStdUtils.h"
#else
#include <Windows.h>
#endif

#include <algorithm>

namespace
{
	constexpr const char * LoggerName = "Reader.WriteAlgorithm";
}

WriteAlgorithm::WriteAlgorithm()
	: EchoAlgorithm("WriteAlgorithm")
	, m_pReadService(nullptr)
	, m_bUseSort(false)
	, m_archivePreviousPings(true)
	, m_outputType(OutputType::None)
	, m_writeToHac(std::make_unique<WriteAlgorithmToFileHac>())
	, m_writeToNetCDF(std::make_unique<WriteAlgorithmToFileNetCDF>())
{
	setEnable(false);
}

WriteAlgorithm::~WriteAlgorithm()
{
	CloseStream();
}

void WriteAlgorithm::SetUseSort(const bool& sort)
{
	m_writeToHac->SetUseSort(sort);
	m_writeToNetCDF->SetUseSort(sort);
	m_bUseSort = sort;
}

void WriteAlgorithm::SetOutputType(const OutputType & outputType) {
	m_outputType = outputType;
	setEnable(outputType != OutputType::None);
}

void WriteAlgorithm::SetFileMaxSize(const uint32_t & sizeInOctets) {
	m_writeToHac->SetFileMaxSize(sizeInOctets);
	m_writeToNetCDF->SetFileMaxSize(sizeInOctets);
}

void WriteAlgorithm::SetFileMaxTime(const uint32_t & timeInSeconds) {
	m_writeToHac->SetFileMaxTime(timeInSeconds);
	m_writeToNetCDF->SetFileMaxTime(timeInSeconds);
}

void WriteAlgorithm::SetArchivePreviousPings(const bool & archivePreviousPings) {
	m_archivePreviousPings = archivePreviousPings;
}

void WriteAlgorithm::AddEchoIntegration(const Sounder & sounder, const EchoIntegrationModule& eiModule, const EchoIntegrationOutput& eiOutput) {
	if (netCDFOutputEnabled() && echoIntegrationOutputEnabled()) {
		m_writeToNetCDF->AddEchoIntegration(sounder, eiModule, eiOutput);
	}
}

void WriteAlgorithm::SetFilePrefix(const std::string & filePrefix)
{
	if (filePrefix.empty())
	{
		M3D_LOG_WARN(LoggerName, "WriteAlgorithm NULL file prefix given");
	}
	m_filePrefix = filePrefix;
}

#if !defined (INVALID_FILE_ATTRIBUTES)
#define INVALID_FILE_ATTRIBUTES ((DWORD)-1)
#endif

void WriteAlgorithm::SetDirectory(const std::string & dirName)
{
	if (dirName.empty())
	{
		M3D_LOG_WARN(LoggerName, "WriteAlgorithm NULL dir name given");
	}

	
#ifdef WIN32 //TODO Améliorer avec std::filesystem::is_directory C++17
    DWORD fileAtt = GetFileAttributes(dirName.c_str());
    bool isDirectory = fileAtt != INVALID_FILE_ATTRIBUTES && fileAtt & FILE_ATTRIBUTE_DIRECTORY;
#else
    struct stat path_stat;
    stat(dirName.c_str(), &path_stat);
    bool isDirectory = S_ISDIR(path_stat.st_mode) != 0;
#endif

    if (isDirectory)
	{
		m_directoryRecord = dirName;
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, 
			Log::format("WriteAlgorithm given dir name '%s' is not a directory or does not exist, keeping old one", dirName.c_str())
		);
	}
}

void WriteAlgorithm::SetReaderService(MovReadService *pReadService)
{
	m_pReadService = pReadService;
}

void WriteAlgorithm::PingFanAdded(PingFan *pFan)
{
	if (!getEnable()) return;

	M3DKernel *pKern = M3DKernel::GetInstance();

	Sounder *p = pFan->m_pSounder;
	TimeObjectContainer *pTime = pKern->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(p->m_SounderId);
	unsigned int numFan = pTime->GetObjectCount();
	if (numFan > 0)
	{
		WritePingFan(pFan, p);
	}
	else
	{
		M3D_LOG_ERROR(LoggerName, "WriteAlgorithm:: PingFanAdded but number of Fan is NULL");
	}
}

void WriteAlgorithm::WritePingFan(PingFan* pingFan, Sounder *sounder)
{
	if (!getEnable() || echoIntegrationOutputEnabled())
	{
		return;
	}

	if (hacOutputEnabled()) {
		m_writeToHac->WritePingFan(pingFan, sounder);
	}
	if (netCDFOutputEnabled()) {
		m_writeToNetCDF->WritePingFan(pingFan, sounder);
	}

	// Le changement de statut des objets datés doit être effectué après toutes les écritures
	// Sinon, ils ne seront écrits que dans le premier format
	if (hacOutputEnabled()) {
		m_writeToHac->UpdateWrittenDatedObjects();
	}
	if (netCDFOutputEnabled()) {
		m_writeToNetCDF->UpdateWrittenDatedObjects();
	}
}

void WriteAlgorithm::onEnableStateChange()
{
	if (!m_pReadService)
	{
		M3D_LOG_WARN(LoggerName, "Trying to start write service with no file opened, ignoring");
		setEnable(false);
	}

	if (getEnable())
	{

		M3DKernel *pKern = M3DKernel::GetInstance();
		StreamOpened(m_pReadService->getStreamDesc());
		CurrentSounderDefinition &refMgr = pKern->getObjectMgr()->GetPingFanContainer().m_sounderDefinition;
#pragma message("---")
#pragma message("-------------voir si pr�sence de plusieurs sounder diff�rents")
#pragma message("-------------et voir pour couper les fichiers en morceaux")
#pragma message("---")

		CheckSort();

		for (size_t i = 0; i < refMgr.GetNbSounder(); i++)
		{
			Sounder *pSounder = refMgr.GetSounder(i);
			SounderChanged(pSounder->m_SounderId);
		}

		if (m_archivePreviousPings) {
			SounderIndexedTimeContainer *pTime = pKern->getObjectMgr()->GetPingFanContainer().GetPingFanContainer();

			// cas o� on ne trie pas les pings chronologiquement
			if (!m_bUseSort) {
				for (unsigned int numFan = 0; numFan < pTime->GetObjectCount(); numFan++) {
					PingFan* pingFan = (PingFan*)pTime->GetObjectWithIndex(numFan);
					WritePingFan(pingFan, pingFan->getSounderRef());
				}
			}
			// cas o� on trie les pings chronologiquement
			else {
				std::vector<LinkedHacTime> m_dateToWrite;
				for (unsigned int numFan = 0; numFan < pTime->GetObjectCount(); numFan++) {
					PingFan* pingFan = (PingFan*)pTime->GetObjectWithIndex(numFan);

					LinkedHacTime timeToWrite;
					timeToWrite.m_TimeFraction = pingFan->m_ObjectTime.m_TimeFraction;
					timeToWrite.m_TimeCpu = pingFan->m_ObjectTime.m_TimeCpu;
					timeToWrite.m_pLinkedPingFan = pingFan;

					m_dateToWrite.push_back(timeToWrite);
				}

				// tri des dates
				std::sort(m_dateToWrite.begin(), m_dateToWrite.end());

				// �criture des donn�es
				for (unsigned int indexTime = 0; indexTime < m_dateToWrite.size(); indexTime++) {
					LinkedHacTime timeToWrite = m_dateToWrite[indexTime];
					WritePingFan(timeToWrite.m_pLinkedPingFan, timeToWrite.m_pLinkedPingFan->getSounderRef());
				}
			}
		}
	}
	else
	{
		CloseStream();
	}
}

void WriteAlgorithm::onInputModuleEnableStateChange(EchoAlgorithm* algo)
{
	if (!algo->getEnable())
	{
		CloseStream();
	}
}

void WriteAlgorithm::CheckSort()
{
	if (m_bUseSort)
	{

		M3DKernel *pKern = M3DKernel::GetInstance();
		HacObjectMgr *pHacObj = pKern->getObjectMgr();
		pHacObj->GetNavAttributesContainer()->Sort();
		pHacObj->GetEnvironnementContainer()->Sort();
		pHacObj->GetNavPositionContainer()->Sort();
		pHacObj->GetEventMarkerContainer()->Sort();
		unsigned int i;
		for (i = 0; i < pHacObj->GetNumberOfNavAttitudeSensorId(); i++)
		{

			unsigned short SensorId = pHacObj->GetNavAttitudeSensorId(i);
			pHacObj->GetNavAttitudeContainer(SensorId)->Sort();
		}
		for (i = 0; i < pHacObj->GetTrawl()->GetNumberOfDynamicPlatformPositionSensorId(); i++)
		{
			TrawlSensorPositionId SensorId = pHacObj->GetTrawl()->GetDynamicPlatformPositionSensorId(i);
			pHacObj->GetTrawl()->GetDynamicPlatformPositionContainer(SensorId)->Sort();
		}
		for (i = 0; i < pHacObj->GetTrawl()->GetNumberOfPlatformPositionSensorId(); i++)
		{
			TrawlSensorPositionId SensorId = pHacObj->GetTrawl()->GetPlatformPositionSensorId(i);
			pHacObj->GetTrawl()->GetPlatformPositionContainer(SensorId)->Sort();
		}
		pHacObj->GetTrawl()->GetTrawlPositionAttitudeContainer()->Sort();
		pHacObj->GetPingFanContainer().GetPingFanContainer()->Sort();
	}
}

void WriteAlgorithm::SounderChanged(std::uint32_t sounderId)
{
	if (!getEnable()) return;

	if (hacOutputEnabled()) {
		m_writeToHac->SounderChanged(sounderId);
	}
	if (netCDFOutputEnabled()) {
		m_writeToNetCDF->SounderChanged(sounderId);
	}
}

void WriteAlgorithm::StreamClosed(const char *streamName)
{
	CloseStream();
}

void WriteAlgorithm::StreamOpened(const char *streamName)
{
	if (!getEnable() || !m_pReadService) 
		return;

	std::string file = m_pReadService->getStreamDesc();
	
	size_t found = file.find_last_of("/\\");
	std::string fileName = m_directoryRecord;
	fileName += "/";
	
	if (m_filePrefix.empty())
	{
		fileName += "MODIFIED_";
	}
	else
	{
		fileName += m_filePrefix;
	}
	
	fileName += file.substr(found + 1);
	M3D_LOG_INFO(LoggerName, Log::format("creating file : '%s'", fileName.c_str()));

	OpenStream(fileName.c_str());
}

bool WriteAlgorithm::hacOutputEnabled() const
{
	return static_cast<uint8_t>(m_outputType) & static_cast<uint8_t>(OutputType::Hac);
}

bool WriteAlgorithm::netCDFOutputEnabled() const 
{
	return static_cast<uint8_t>(m_outputType) & static_cast<uint8_t>(OutputType::NetCDF);
}

bool WriteAlgorithm::echoIntegrationOutputEnabled() const {
	return static_cast<uint8_t>(m_outputType) & static_cast<uint8_t>(OutputType::EI);
}

void WriteAlgorithm::CloseStream()
{
	m_writeToHac->Close();
	m_writeToNetCDF->Close();
}

void WriteAlgorithm::OpenStream(const char * fileName)
{
	if (hacOutputEnabled()) {
		m_writeToHac->Open(fileName);
	}
	
	if (netCDFOutputEnabled()) {
		m_writeToNetCDF->Open(fileName);
	}
}