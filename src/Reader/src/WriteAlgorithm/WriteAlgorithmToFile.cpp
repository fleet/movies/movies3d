#include "Reader/WriteAlgorithm/WriteAlgorithmToFile.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/utils/log/ILogger.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/EventMarker.h"

#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/Environnement.h"

#include "Reader/MovReadService.h"

namespace
{
	constexpr const char * LoggerName = "Reader.WriteAlgorithmToFile";
}

WriteAlgorithmToFile::WriteAlgorithmToFile() : 
	m_bUseSort(false),
	m_fileMaxSize(0),
	m_fileMaxTime(0)
{
}

void WriteAlgorithmToFile::WritePingFan(PingFan * pingFan, Sounder * sounder) {
	std::uint64_t pingId = pingFan->m_computePingFan.m_pingId;

	HacTime lastDate;
	HacTime newDate = pingFan->m_ObjectTime;

	HacObjectMgr *pHacObj = M3DKernel::GetInstance()->getObjectMgr();

	bool found = true;
	unsigned int totalWrite = 0;
	while (found) {
		// first check for the smallest corresponding date in our objects
		HacTime dateFound;
		found = FindSmallestDate(lastDate, newDate, dateFound);
		//found = false;
		if (found) {
			lastDate = dateFound;
			// now write object with date equal to the smallest
			totalWrite += WriteDatedObject(lastDate);
		}
	}
	totalWrite += WriteDatedObject(newDate);

	Sounder *pSounder = pingFan->getSounderRef();

	for (unsigned int numTrans = 0; numTrans < sounder->GetTransducerCount(); numTrans++) {
		Transducer *pTransducer = sounder->GetTransducer(numTrans);
		for (size_t numBeam = 0; numBeam < pTransducer->m_numberOfSoftChannel; numBeam++) {
			SoftChannel *chan = pTransducer->getSoftChannelPolarX(numBeam);

			WriteBeamPingFan(pingFan, chan, pTransducer, numTrans, numBeam);			
		}
	}

	m_LastPingDateWritten = newDate;
}

void WriteAlgorithmToFile::Open(const std::string& sourcePath) {
	Close();	
	m_LastPingDateWritten.SetToNull();
	m_sourcePath = AdaptExtension(sourcePath);
}

void WriteAlgorithmToFile::SetUseSort(const bool& sort)
{
	m_bUseSort = sort;
}

const std::string & WriteAlgorithmToFile::GetSourcePath() const {
	return m_sourcePath;
}

void WriteAlgorithmToFile::SetFileMaxSize(const uint32_t & sizeInOctets) {
	m_fileMaxSize = sizeInOctets;
}

void WriteAlgorithmToFile::SetFileMaxTime(const uint32_t & timeInSeconds) {
	m_fileMaxTime = timeInSeconds;
}

void WriteAlgorithmToFile::UpdateWrittenDatedObjects()
{
	for (auto& datedObject : m_writtenDatedObjects)
	{
		datedObject->m_bWritten = true;
	}

	m_writtenDatedObjects.clear();
}

const std::string WriteAlgorithmToFile::AdaptExtension(const std::string & sourcePath) const {

	// Est ce que l'extension est la bonne ?
	const auto extension = GetFileExtension();
	if (extension.size() < sourcePath.size()
		&& sourcePath.compare(sourcePath.size() - extension.size(), extension.size(), extension) == 0) {
		return sourcePath;
	}

	// Sinon on change l'extension
	const auto lastPointIndex = sourcePath.find_last_of('.');
	if (lastPointIndex == -1) { // Aucune extension trouv�e
		return sourcePath + extension;
	}

	return sourcePath.substr(0, lastPointIndex) + extension;
}

bool WriteAlgorithmToFile::FindSmallestDate(HacTime start, HacTime stop, HacTime &dateFound)
{
	bool found = false;
	assert(start < stop);

	HacTime t;
	bool localFound;

	M3DKernel *pKern = M3DKernel::GetInstance();
	HacObjectMgr *pHacObj = pKern->getObjectMgr();
	dateFound = stop;

	t = start;
	localFound = false;
	while (t < dateFound && !localFound)
	{
		std::vector<DatedObject*> datedObjects = pHacObj->GetNavAttributesContainer()->FindNextDatedObjects(t);
		if (datedObjects.empty())
			break;
		for (std::vector<DatedObject*>::iterator it = datedObjects.begin(); it != datedObjects.end(); ++it)
		{
			DatedObject * p = *it;
			t = p->m_ObjectTime;
			if (p->m_ObjectTime < dateFound && !CheckIfAlreadyWritten(p))
			{
				dateFound = p->m_ObjectTime;
				found = true;
				localFound = true;
				break;
			}
		}
	}

	t = start;
	localFound = false;
	while (t < dateFound && !localFound)
	{
		std::vector<DatedObject*> datedObjects = pHacObj->GetEnvironnementContainer()->FindNextDatedObjects(t);
		if (datedObjects.empty())
			break;
		for (std::vector<DatedObject*>::iterator it = datedObjects.begin(); it != datedObjects.end(); ++it)
		{
			DatedObject * p = *it;
			t = p->m_ObjectTime;
			if (p->m_ObjectTime < dateFound && !CheckIfAlreadyWritten(p))
			{
				dateFound = p->m_ObjectTime;
				found = true;
				localFound = true;
				break;
			}
		}
	}

	t = start;
	localFound = false;
	while (t < dateFound && !localFound)
	{
		std::vector<DatedObject*> datedObjects = pHacObj->GetNavPositionContainer()->FindNextDatedObjects(t);
		if (datedObjects.empty())
			break;
		for (std::vector<DatedObject*>::iterator it = datedObjects.begin(); it != datedObjects.end(); ++it)
		{
			NavPosition * p = (NavPosition*)*it;
			t = p->m_ObjectTime;
			if (p->m_ObjectTime < dateFound && p->m_source == eTupleSource && !CheckIfAlreadyWritten(p))
			{
				dateFound = p->m_ObjectTime;
				found = true;
				localFound = true;
				break;
			}
		}
	}
	t = start;
	localFound = false;
	while (t < dateFound && !localFound)
	{
		std::vector<DatedObject*> datedObjects = pHacObj->GetEventMarkerContainer()->FindNextDatedObjects(t);
		if (datedObjects.empty())
			break;
		for (std::vector<DatedObject*>::iterator it = datedObjects.begin(); it != datedObjects.end(); ++it)
		{
			DatedObject * p = *it;
			t = p->m_ObjectTime;
			if (p->m_ObjectTime < dateFound && !CheckIfAlreadyWritten(p))
			{
				dateFound = p->m_ObjectTime;
				found = true;
				localFound = true;
				break;
			}
		}
	}
	unsigned int i;
	for (i = 0; i < pHacObj->GetNumberOfNavAttitudeSensorId(); i++)
	{
		unsigned short SensorId = pHacObj->GetNavAttitudeSensorId(i);
		TimeObjectContainer * TOC = pHacObj->GetNavAttitudeContainer(SensorId);
		t = start;
		localFound = false;
		while (t < dateFound && !localFound)
		{
			std::vector<DatedObject*> datedObjects = TOC->FindNextDatedObjects(t);
			if (datedObjects.empty())
				break;
			for (std::vector<DatedObject*>::iterator it = datedObjects.begin(); it != datedObjects.end(); ++it)
			{
				NavAttitude * p = (NavAttitude*)*it;
				t = p->m_ObjectTime;
				if (p->m_ObjectTime < dateFound && p->m_source == eTupleSource && !CheckIfAlreadyWritten(p))
				{
					dateFound = p->m_ObjectTime;
					found = true;
					localFound = true;
					break;
				}
			}
		}
	}

	t = start;
	localFound = false;
	while (t < dateFound && !localFound)
	{
		std::vector<DatedObject*> datedObjects = pHacObj->GetTrawl()->GetTrawlPositionAttitudeContainer()->FindNextDatedObjects(t);
		if (datedObjects.empty())
			break;
		for (std::vector<DatedObject*>::iterator it = datedObjects.begin(); it != datedObjects.end(); ++it)
		{
			DatedObject * p = *it;
			t = p->m_ObjectTime;
			if (p->m_ObjectTime < dateFound && !CheckIfAlreadyWritten(p))
			{
				dateFound = p->m_ObjectTime;
				found = true;
				localFound = true;
				break;
			}
		}
	}
	for (i = 0; i < pHacObj->GetTrawl()->GetNumberOfDynamicPlatformPositionSensorId(); i++)
	{
		TrawlSensorPositionId SensorId = pHacObj->GetTrawl()->GetDynamicPlatformPositionSensorId(i);

		t = start;
		localFound = false;
		while (t < dateFound && !localFound)
		{
			std::vector<DatedObject*> datedObjects = pHacObj->GetTrawl()->GetDynamicPlatformPositionContainer(SensorId)->FindNextDatedObjects(t);
			if (datedObjects.empty())
				break;
			for (std::vector<DatedObject*>::iterator it = datedObjects.begin(); it != datedObjects.end(); ++it)
			{
				DatedObject * p = *it;
				t = p->m_ObjectTime;
				if (p->m_ObjectTime < dateFound && !CheckIfAlreadyWritten(p))
				{
					dateFound = p->m_ObjectTime;
					found = true;
					localFound = true;
					break;
				}
			}
		}
	}
	for (i = 0; i < pHacObj->GetTrawl()->GetNumberOfPlatformPositionSensorId(); i++)
	{
		TrawlSensorPositionId SensorId = pHacObj->GetTrawl()->GetPlatformPositionSensorId(i);

		t = start;
		localFound = false;
		while (t < dateFound && !localFound)
		{
			std::vector<DatedObject*> datedObjects = pHacObj->GetTrawl()->GetPlatformPositionContainer(SensorId)->FindNextDatedObjects(t);
			if (datedObjects.empty())
				break;
			for (std::vector<DatedObject*>::iterator it = datedObjects.begin(); it != datedObjects.end(); ++it)
			{
				DatedObject * p = *it;
				t = p->m_ObjectTime;
				if (p->m_ObjectTime < dateFound && !CheckIfAlreadyWritten(p))
				{
					dateFound = p->m_ObjectTime;
					found = true;
					localFound = true;
					break;
				}
			}
		}
	}
	return found;
}

unsigned int WriteAlgorithmToFile::WriteDatedObject(HacTime date) {
	unsigned int numObjectWritten = 0;
	M3DKernel *pKern = M3DKernel::GetInstance();
	HacObjectMgr *pHacObj = pKern->getObjectMgr();

	// Navigation attributes
	auto navAttributesAtDate = pHacObj->GetNavAttributesContainer()->FindDatedObjects(date);
	for (auto& datedObject : navAttributesAtDate) {
		if (datedObject->m_ObjectTime == date && !CheckIfAlreadyWritten(datedObject)) {
			auto navAttributes = dynamic_cast<NavAttributes*>(datedObject);
			if (navAttributes && WriteNavAttributes(navAttributes)) {
				m_writtenDatedObjects.insert(datedObject);
				numObjectWritten++;
			}
		}
	}

	// Environment
	auto environementsAtDate = pHacObj->GetEnvironnementContainer()->FindDatedObjects(date);
	for (auto& datedObject : environementsAtDate) {
		if (datedObject->m_ObjectTime == date && !CheckIfAlreadyWritten(datedObject)) {
			auto environment = dynamic_cast<Environnement*>(datedObject);
			if (environment && WriteEnvironment(environment)) {
				m_writtenDatedObjects.insert(datedObject);
				numObjectWritten++;
			}
		}
	}

	// Navigation position
	auto navigationPositionsAtDate = pHacObj->GetNavPositionContainer()->FindDatedObjects(date);
	for (auto& datedObject : navigationPositionsAtDate) {
		if (datedObject->m_ObjectTime == date && !CheckIfAlreadyWritten(datedObject)) {
			auto navigationPosition = dynamic_cast<NavPosition*>(datedObject);
			if (navigationPosition && WriteNavPosition(navigationPosition)) {
				m_writtenDatedObjects.insert(datedObject);
				numObjectWritten++;
			}
		}
	}

	// Event marker
	auto eventMarkers = pHacObj->GetEventMarkerContainer()->FindDatedObjects(date);
	for (auto& datedObject : eventMarkers) {
		if (datedObject->m_ObjectTime == date && !CheckIfAlreadyWritten(datedObject)) {
			auto eventMarker = dynamic_cast<EventMarker*>(datedObject);
			if (eventMarker && WriteEventMarker(eventMarker)) {
				m_writtenDatedObjects.insert(datedObject);
				numObjectWritten++;
			}
		}
	}

	// Navigation attitudes
	for (size_t i = 0; i < pHacObj->GetNumberOfNavAttitudeSensorId(); ++i) {
		const auto sensorId = pHacObj->GetNavAttitudeSensorId(i);
		auto navAttitudes = pHacObj->GetNavAttitudeContainer(sensorId)->FindDatedObjects(date);
		for (auto& datedObject : navAttitudes) {
			if (datedObject->m_ObjectTime == date && !CheckIfAlreadyWritten(datedObject)) {
				auto navAttitude = dynamic_cast<NavAttitude*>(datedObject);
				if (navAttitude && WriteNavAttitude(navAttitude)) {
					m_writtenDatedObjects.insert(datedObject);
					numObjectWritten++;
				}
			}
		}
	}

	// Trawl position attitudes
	auto trawlPositionAttitudes = pHacObj->GetTrawl()->GetTrawlPositionAttitudeContainer()->FindDatedObjects(date);
	for (auto& datedObject : trawlPositionAttitudes) {
		if (datedObject->m_ObjectTime == date && !CheckIfAlreadyWritten(datedObject)) {
			auto trawlPositionAttitude = dynamic_cast<TrawlPositionAttitude*>(datedObject);
			if (trawlPositionAttitude && WriteTrawlPositionAttitude(trawlPositionAttitude)) {
				m_writtenDatedObjects.insert(datedObject);
				numObjectWritten++;
			}
		}
	}

	// Dynamic platform position
	for (size_t i = 0; i < pHacObj->GetTrawl()->GetNumberOfDynamicPlatformPositionSensorId(); ++i) {
		const auto sensorId = pHacObj->GetTrawl()->GetDynamicPlatformPositionSensorId(i);
		auto dynamicPlatformPositions = pHacObj->GetTrawl()->GetDynamicPlatformPositionContainer(sensorId)->FindDatedObjects(date);
		for (auto& datedObject : dynamicPlatformPositions) {
			if (datedObject->m_ObjectTime == date && !CheckIfAlreadyWritten(datedObject)) {
				auto dynamicPlatformPosition = dynamic_cast<DynamicPlatformPosition*>(datedObject);
				if (dynamicPlatformPosition && WriteDynamicPlatformPosition(dynamicPlatformPosition, sensorId)) {
					m_writtenDatedObjects.insert(datedObject);
					numObjectWritten++;
				}
			}
		}
	}	

	// Trawl platform position
	for (size_t i = 0; i < pHacObj->GetTrawl()->GetNumberOfPlatformPositionSensorId(); ++i) {
		const auto sensorId = pHacObj->GetTrawl()->GetPlatformPositionSensorId(i);
		auto platformPositions = pHacObj->GetTrawl()->GetPlatformPositionContainer(sensorId)->FindDatedObjects(date);
		for (auto& datedObject : platformPositions) {
			if (datedObject->m_ObjectTime == date && !CheckIfAlreadyWritten(datedObject)) {
				auto trawlPlatformPosition = dynamic_cast<TrawlPlatformPosition*>(datedObject);
				if (trawlPlatformPosition && WriteTrawlPlatformPosition(trawlPlatformPosition)) {
					m_writtenDatedObjects.insert(datedObject);
					numObjectWritten++;
				}
			}
		}
	}

	return numObjectWritten;
}

void WriteAlgorithmToFile::ChangeFile() {
}

bool WriteAlgorithmToFile::CheckIfAlreadyWritten(const DatedObject* const dated_object)
{
	return dated_object->m_bWritten ||
		std::find(m_writtenDatedObjects.begin(), m_writtenDatedObjects.end(), dated_object) != m_writtenDatedObjects.end();
}
