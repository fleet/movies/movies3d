#include "Reader/WriteAlgorithm/WriteAlgorithmToFileHac.h"

#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/datascheme/SounderMulti.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "Reader/Tup210.h"
#include "Reader/Tup2100.h"
#include "Reader/Tup230.h"
#include "Reader/Tup2300.h"
#include "Reader/Tup2310.h"
#include "Reader/Tup41.h"
#include "Reader/Tup10040.h"
#include "Reader/Tup10060.h"
#include "Reader/Tup10011.h"
#include "Reader/Tup10031.h"
#include "Reader/Tup30.h"
#include "Reader/Tup10140.h"
#include "Reader/Tup4000.h"
#include "Reader/Tup10090.h"

#include "Reader/Tup220.h"
#include "Reader/Tup2200.h"
#include "Reader/Tup2210.h"
#include "Reader/Tup50.h"
#include "Reader/Tup42.h"
#include "Reader/Tup10142.h"

#include "Reader/Tup200.h"
#include "Reader/Tup2001.h"
#include "Reader/Tup65535.h"
#include "Reader/Tup65534.h"
#include "Reader/Tup20.h"
#include "Reader/Tup10110.h"
#include "Reader/Tup10100.h"
#include "Reader/Tup11000.h"

#include "Reader/Tup901.h"
#include "Reader/Tup9001.h"

#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/EventMarker.h"
#include "M3DKernel/datascheme/Environnement.h"

namespace
{
	constexpr const char * LoggerName = "Reader.WriteAlgorithmToFileHac";
	constexpr const char * FileExtension = ".hac";
}

WriteAlgorithmToFileHac::WriteAlgorithmToFileHac()
	: WriteAlgorithmToFile(),
	m_bEnvInHeader(false),
	m_bPlatFormAttitudeInHeader(false)
{
}

WriteAlgorithmToFileHac::~WriteAlgorithmToFileHac() {
	Close();
}

std::string WriteAlgorithmToFileHac::GetFileExtension() const {
	return std::string(FileExtension);
}

void WriteAlgorithmToFileHac::Close() {
	if (m_outputStream) {
		std::uint32_t size = Tup65534::Encode(m_StreamBuffer, m_LastPingDateWritten);
		FlushStream(size);

		m_outputStream->close();
	}
	m_outputStream.reset();
}

void WriteAlgorithmToFileHac::Open(const std::string & fileName) 
{
	WriteAlgorithmToFile::Open(fileName);

	m_outputStream = std::make_unique<std::ofstream>(fileName, std::ios::in | std::ios::binary | std::ios::trunc);

	std::uint32_t codeverif = 172;

	m_outputStream->write((char*)& codeverif, sizeof(codeverif));

	std::uint32_t size = Tup65535::Encode(m_StreamBuffer);
	FlushStream(size);

	// OTK - 01/06/2009 - on place le premier tuple environnement apr�s le tuple signature (comme dans Hermes)
	M3DKernel *pKern = M3DKernel::GetInstance();
	HacObjectMgr *pHacObj = pKern->getObjectMgr();
	Environnement  *p = (Environnement*)pHacObj->GetEnvironnementContainer()->GetDatedObject(0);
	if (p) {
		size = Tup11000::Encode(m_StreamBuffer, p);
		FlushStream(size);
		m_bEnvInHeader = true;
	}
	else {
		m_bEnvInHeader = false;
	}
}

void WriteAlgorithmToFileHac::SounderChanged(const uint32_t& sounderId)
{
	if (!m_outputStream) return;

	M3DKernel *pKern = M3DKernel::GetInstance();

	Sounder *p2 = pKern->getObjectMgr()->GetLastValidSounder(sounderId);
	SounderSBES* pSounderSBES = dynamic_cast<SounderSBES*> (p2);
	SounderEk500* pSounderEk500 = dynamic_cast<SounderEk500*> (p2);
	SounderGeneric* pSounderGeneric = dynamic_cast<SounderGeneric*> (p2);
	SounderMulti* pSounderMulti = dynamic_cast<SounderMulti*> (p2);
	m_bPlatFormAttitudeInHeader = false;

	HacObjectMgr * pHacObj = pKern->getObjectMgr();
	if (pSounderSBES)
	{
		SounderER60* pSounderER60 = dynamic_cast<SounderER60*> (p2);
		SounderEK80* pSounderEK80 = dynamic_cast<SounderEK80*> (p2);
		std::uint32_t size;
		if (pSounderER60)
		{
			size = Tup210::Encode(m_StreamBuffer, pSounderER60);
		}
		else
		{
			size = Tup230::Encode(m_StreamBuffer, pSounderEK80);
		}
		FlushStream(size);
		unsigned int numTrans;
		for (numTrans = 0; numTrans < pSounderSBES->GetTransducerCount(); numTrans++)
		{
			Transducer *pTransducer = pSounderSBES->GetTransducer(numTrans);
			if (pSounderER60)
			{
				size = Tup2100::Encode(m_StreamBuffer, pSounderER60, pTransducer);
			}
			else
			{
				size = Tup2300::Encode(m_StreamBuffer, pSounderEK80, pTransducer);
			}
			FlushStream(size);


			for (size_t numBeam = 0; numBeam < pTransducer->m_numberOfSoftChannel; numBeam++)
			{
				SoftChannel *chan = pTransducer->getSoftChannelPolarX(numBeam);

				// single target acquisition parameters
				if (chan->GetSplitBeamParameter())
				{
					size = Tup4000::Encode(m_StreamBuffer, chan->m_softChannelId, pTransducer);
					FlushStream(size);
				}

				if (chan->GetThreshold())
				{
					size = Tup10100::Encode(m_StreamBuffer, chan->GetThreshold());
					FlushStream(size);
				}
			}

			if (pSounderEK80)
			{
				size = Tup2310::Encode(m_StreamBuffer, pTransducer->GetFPGAFilter());
				FlushStream(size);

				size = Tup2310::Encode(m_StreamBuffer, pTransducer->GetApplicationFilter());
				FlushStream(size);
			}
		}

		for (numTrans = 0; numTrans < pSounderSBES->GetTransducerCount(); numTrans++)
		{
			Transducer *pTransducer = pSounderSBES->GetTransducer(numTrans);
			for (size_t numBeam = 0; numBeam < pTransducer->m_numberOfSoftChannel; numBeam++)
			{
				SoftChannel *chan = pTransducer->getSoftChannelPolarX(numBeam);
				size = Tup41::Encode(m_StreamBuffer, pTransducer->GetPlatform(), chan->m_softChannelId);
				FlushStream(size);
			}
		}
		for (size_t sensorIndex = 0; sensorIndex < pHacObj->GetTrawl()->GetNumberOfDynamicPlatformPositionSensorId(); ++sensorIndex)
		{
			TrawlSensorPositionId SensorId = pHacObj->GetTrawl()->GetDynamicPlatformPositionSensorId(sensorIndex);
			DynamicPlatformPosition *p = (DynamicPlatformPosition *)pHacObj->GetTrawl()->GetDynamicPlatformPositionContainer(SensorId)->GetDatedObject(0);
			if (p && SensorId.m_depthId == p2->m_SounderId)
			{
				std::uint32_t size = Tup42::Encode(m_StreamBuffer, p);
				FlushStream(size);
				m_bPlatFormAttitudeInHeader = true;
			}
		}
	}
	else if (pSounderEk500)
	{
		std::uint32_t size = Tup200::Encode(m_StreamBuffer, pSounderEk500);
		FlushStream(size);
		unsigned int numTrans;
		for (numTrans = 0; numTrans < pSounderEk500->GetTransducerCount(); numTrans++)
		{
			Transducer *pTransducer = pSounderEk500->GetTransducer(numTrans);
			size = Tup2001::Encode(m_StreamBuffer, pSounderEk500, pTransducer);
			FlushStream(size);

			for (size_t numBeam = 0; numBeam < pTransducer->m_numberOfSoftChannel; numBeam++)
			{
				SoftChannel *chan = pTransducer->getSoftChannelPolarX(numBeam);

				// single target acquisition parameters
				if (chan->GetSplitBeamParameter())
				{
					size = Tup4000::Encode(m_StreamBuffer, chan->m_softChannelId, pTransducer);
					FlushStream(size);
				}

				if (chan->GetThreshold())
				{
					size = Tup10100::Encode(m_StreamBuffer, chan->GetThreshold());
					FlushStream(size);

				}
			}


		}

		for (numTrans = 0; numTrans < pSounderEk500->GetTransducerCount(); numTrans++)
		{
			Transducer *pTransducer = pSounderEk500->GetTransducer(numTrans);
			for (size_t numBeam = 0; numBeam < pTransducer->m_numberOfSoftChannel; numBeam++)
			{
				SoftChannel *chan = pTransducer->getSoftChannelPolarX(numBeam);
				size = Tup41::Encode(m_StreamBuffer, pTransducer->GetPlatform(), chan->m_softChannelId);
				FlushStream(size);
			}
		}
		for (size_t sensorIndex = 0; sensorIndex < pHacObj->GetTrawl()->GetNumberOfDynamicPlatformPositionSensorId(); ++sensorIndex)
		{
			TrawlSensorPositionId SensorId = pHacObj->GetTrawl()->GetDynamicPlatformPositionSensorId(sensorIndex);
			DynamicPlatformPosition *p = (DynamicPlatformPosition *)pHacObj->GetTrawl()->GetDynamicPlatformPositionContainer(SensorId)->GetDatedObject(0);
			if (p && SensorId.m_depthId == p2->m_SounderId)
			{
				std::uint32_t size = Tup42::Encode(m_StreamBuffer, p);
				FlushStream(size);
				m_bPlatFormAttitudeInHeader = true;
			}
		}
	}
	else if (pSounderGeneric)
	{
		std::uint32_t size = Tup901::Encode(m_StreamBuffer, pSounderGeneric);
		FlushStream(size);
		unsigned int numTrans;
		for (numTrans = 0; numTrans < pSounderGeneric->GetTransducerCount(); numTrans++)
		{
			Transducer *pTransducer = pSounderGeneric->GetTransducer(numTrans);
			size = Tup9001::Encode(m_StreamBuffer, pSounderGeneric, pTransducer);
			FlushStream(size);

			for (size_t numBeam = 0; numBeam < pTransducer->m_numberOfSoftChannel; numBeam++)
			{
				SoftChannel *chan = pTransducer->getSoftChannelPolarX(numBeam);

				// single target acquisition parameters
				if (chan->GetSplitBeamParameter())
				{
					size = Tup4000::Encode(m_StreamBuffer, chan->m_softChannelId, pTransducer);
					FlushStream(size);
				}

				if (chan->GetThreshold())
				{
					size = Tup10100::Encode(m_StreamBuffer, chan->GetThreshold());
					FlushStream(size);

				}
			}

		}

		for (numTrans = 0; numTrans < pSounderGeneric->GetTransducerCount(); numTrans++)
		{
			Transducer *pTransducer = pSounderGeneric->GetTransducer(numTrans);
			for (size_t numBeam = 0; numBeam < pTransducer->m_numberOfSoftChannel; numBeam++)
			{
				SoftChannel *chan = pTransducer->getSoftChannelPolarX(numBeam);
				size = Tup41::Encode(m_StreamBuffer, pTransducer->GetPlatform(), chan->m_softChannelId);
				FlushStream(size);
			}
		}
		for (size_t sensorIndex = 0; sensorIndex < pHacObj->GetTrawl()->GetNumberOfDynamicPlatformPositionSensorId(); ++sensorIndex)
		{
			TrawlSensorPositionId SensorId = pHacObj->GetTrawl()->GetDynamicPlatformPositionSensorId(sensorIndex);
			DynamicPlatformPosition *p = (DynamicPlatformPosition *)pHacObj->GetTrawl()->GetDynamicPlatformPositionContainer(SensorId)->GetDatedObject(0);
			if (p && SensorId.m_depthId == p2->m_SounderId)
			{
				std::uint32_t size = Tup42::Encode(m_StreamBuffer, p);
				FlushStream(size);
				m_bPlatFormAttitudeInHeader = true;
			}
		}
	}
	else if (pSounderMulti)
	{
		unsigned int numTrans;
		for (numTrans = 0; numTrans < pSounderMulti->GetTransducerCount(); numTrans++)
		{
			Transducer *pTransducer = pSounderMulti->GetTransducer(numTrans);
			std::uint32_t size = Tup220::Encode(m_StreamBuffer, pSounderMulti, pTransducer);
			FlushStream(size);
			for (size_t numBeam = 0; numBeam < pTransducer->m_numberOfSoftChannel; numBeam++)
			{
				size = Tup2200::Encode(m_StreamBuffer, pSounderMulti, pTransducer, numBeam);
				FlushStream(size);

				// single target acquisition parameters
				SoftChannel *chan = pTransducer->getSoftChannelPolarX(numBeam);
				if (chan->GetSplitBeamParameter())
				{
					size = Tup4000::Encode(m_StreamBuffer, chan->m_softChannelId, pTransducer);
					FlushStream(size);
				}

				if (pTransducer->getSoftChannelPolarX(numBeam)->GetThreshold())
				{
					size = Tup10100::Encode(m_StreamBuffer, pTransducer->getSoftChannelPolarX(numBeam)->GetThreshold());
					FlushStream(size);
				}

				if (pTransducer->getSoftChannelPolarX(numBeam)->GetBeamWeights().size())
				{
					size = Tup2210::Encode(m_StreamBuffer, pSounderMulti, pTransducer, numBeam);
					FlushStream(size);
				}

			}
		}

		for (numTrans = 0; numTrans < pSounderMulti->GetTransducerCount(); numTrans++)
		{
			Transducer *pTransducer = pSounderMulti->GetTransducer(numTrans);
			for (size_t numBeam = 0; numBeam < pTransducer->m_numberOfSoftChannel; numBeam++)
			{
				SoftChannel *chan = pTransducer->getSoftChannelPolarX(numBeam);
				std::uint32_t size = Tup41::Encode(m_StreamBuffer, pTransducer->GetPlatform(), chan->m_softChannelId);
				FlushStream(size);
			}
		}
		for (size_t sensorIndex = 0; sensorIndex < pHacObj->GetTrawl()->GetNumberOfDynamicPlatformPositionSensorId(); ++sensorIndex)
		{
			TrawlSensorPositionId SensorId = pHacObj->GetTrawl()->GetDynamicPlatformPositionSensorId(sensorIndex);
			DynamicPlatformPosition *p = (DynamicPlatformPosition *)pHacObj->GetTrawl()->GetDynamicPlatformPositionContainer(SensorId)->GetDatedObject(0);
			if (p && SensorId.m_depthId == p2->m_SounderId)
			{
				std::uint32_t size = Tup42::Encode(m_StreamBuffer, p);
				FlushStream(size);
				m_bPlatFormAttitudeInHeader = true;
			}
		}
	}
}

bool WriteAlgorithmToFileHac::WriteBeamPingFan(PingFan * pingFan, SoftChannel * softChannel, Transducer * transducer, const size_t& transducerNum, const size_t& beamNum) {
	bool ignorePhaseData = M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePhase();

	// OTK - FAE179 - on �crit la phase avant l'amplitude pour �tre plus proche du format d'entr�e.
	// De plus, ceci permet de contourner un d�faut du test de completion des pingfans par rapport aux donn�es de phase
	// qui ne fonctionne pas s'il n'y a qu'une voie pour le pingfan (on ne sait alors pas si on doi attendre des donn�es
	// de phase ou non pour clore le pingfan).
	// phase data writing
	// don't write phase data if we ignore it
	if (!ignorePhaseData) {
		auto size = Tup10031::Encode(m_StreamBuffer
			, pingFan, softChannel->m_softChannelId, softChannel->getSoftwareVirtualChannelId(), transducer, transducerNum, beamNum);
		FlushStream(size);
	}

	// NMD - FAE 258, si on est en mode FM
	if (transducer->m_pulseShape == 2) // FM mode
	{
		// No write on tuple Tup10040
		auto size = Tup10060::Encode(m_StreamBuffer
			, pingFan, softChannel->m_softChannelId, softChannel->getSoftwareVirtualChannelId(), transducer, transducerNum, beamNum);
		FlushStream(size);
	}
	else {
		// No write on tuple Tup10040
		auto size = Tup10040::Encode(m_StreamBuffer
			, pingFan, softChannel->m_softChannelId, softChannel->getSoftwareVirtualChannelId(), transducer, transducerNum, beamNum);
		FlushStream(size);
	}

	// single target data
	auto size = Tup10090::Encode(m_StreamBuffer, pingFan, softChannel->getSoftwareChannelId());
	FlushStream(size);

	return true;
}

bool WriteAlgorithmToFileHac::WriteEnvironment(Environnement * environment) {
	if (!m_bEnvInHeader || environment != M3DKernel::GetInstance()->getObjectMgr()->GetEnvironnementContainer()->GetFirstDatedObject()) {
		auto size = Tup11000::Encode(m_StreamBuffer, environment);
		FlushStream(size);;
		return true;
	}
	return false;
}

bool WriteAlgorithmToFileHac::WriteNavAttributes(NavAttributes * navAttributes) {
	auto size = Tup30::Encode(m_StreamBuffer, navAttributes);
	FlushStream(size);
	return true;
}

bool WriteAlgorithmToFileHac::WriteNavPosition(NavPosition * navPosition) {
	if (navPosition->m_source == eTupleSource) {
		auto size = Tup20::Encode(m_StreamBuffer, navPosition);
		FlushStream(size);
		return true;
	}
	return false;
}

bool WriteAlgorithmToFileHac::WriteNavAttitude(NavAttitude * navAttitude) {
	if (navAttitude->m_source == eTupleSource) {
		auto size = Tup10140::Encode(m_StreamBuffer, navAttitude);
		FlushStream(size);
		return true;
	}

	return false;
}

bool WriteAlgorithmToFileHac::WriteTrawlPositionAttitude(TrawlPositionAttitude * trawlPositionAttitude) {
	auto size = Tup50::Encode(m_StreamBuffer, trawlPositionAttitude);
	FlushStream(size);
	return true;
}

bool WriteAlgorithmToFileHac::WriteDynamicPlatformPosition(DynamicPlatformPosition * dynamicPlatformPosition, const TrawlSensorPositionId& sensorId) {
	if (!m_bEnvInHeader || dynamicPlatformPosition != M3DKernel::GetInstance()->getObjectMgr()->GetTrawl()->GetDynamicPlatformPositionContainer(sensorId)->GetFirstDatedObject()) {
		std::uint32_t size = Tup42::Encode(m_StreamBuffer, dynamicPlatformPosition);
		FlushStream(size);
		return true;
	}
	return false;
}

bool WriteAlgorithmToFileHac::WriteTrawlPlatformPosition(TrawlPlatformPosition * trawlPlatformPosition) {
	auto size = Tup10142::Encode(m_StreamBuffer, trawlPlatformPosition);
	FlushStream(size);
	return true;
}

bool WriteAlgorithmToFileHac::WriteEventMarker(EventMarker * eventMarker) {
	auto size = Tup10110::Encode(m_StreamBuffer, eventMarker);
	FlushStream(size);
	return true;
}

void WriteAlgorithmToFileHac::FlushStream(std::uint32_t size) {
	libMove dlibMove;
	dlibMove.LowPart = 0;
	m_StreamBuffer.Seek(dlibMove, eSEEK_SET);

	if (m_outputStream) {
		m_outputStream->write(m_StreamBuffer.GetReadBuffer(), size);
		m_outputStream->flush();
	}
}