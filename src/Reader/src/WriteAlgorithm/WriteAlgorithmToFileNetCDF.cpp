/******************************************************************************/
/*	Project:	MOVIE														  */
/*	Author(s):	C. PONCELET													  */
/*	File:		WriteAlgorithm.cpp											  */
/******************************************************************************/
#include "Reader/WriteAlgorithm/WriteAlgorithmToFileNetCDF.h"

#include <ctime>
#include <chrono>

#include "M3DKernel/utils/log/ILogger.h"

#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/SounderMulti.h"

#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/EventMarker.h"
#include "M3DKernel/datascheme/Environnement.h"
#include "M3DKernel/datascheme/TransmitSignalObject.h"
#include "M3DKernel/datascheme/MovESUMgr.h"
#include "M3DKernel/utils/fileutils.h"
#include "M3DKernel/utils/timeutils.h"

#include "EchoIntegration/EchoIntegrationModule.h"
#include "Calibration/CalibrationModule.h"

#include "Reader/ConversionTools.h"

#include "Reader/SonarNetCDF/NetCDFUtils.h"
#include "Reader/SonarNetCDF/GroupTopLevel.h"

#include "Reader/SonarNetCDF/GroupEnvironment.h"
#include "Reader/SonarNetCDF/Environment/VariableAcousticFrequency.h"
#include "Reader/SonarNetCDF/Environment/VariableAbsorptionIndicative.h"
#include "Reader/SonarNetCDF/Environment/VariableSoundSpeedIndicative.h"

#include "Reader/SonarNetCDF/Platform/PlatformCustomTypes.h"
#include "Reader/SonarNetCDF/GroupPlatform.h"
#include "Reader/SonarNetCDF/Platform/VariableMRUIDs.h"
#include "Reader/SonarNetCDF/Platform/VariableMRUOffsetX.h"
#include "Reader/SonarNetCDF/Platform/VariableMRUOffsetY.h"
#include "Reader/SonarNetCDF/Platform/VariableMRUOffsetZ.h"
#include "Reader/SonarNetCDF/Platform/VariableMRURotationX.h"
#include "Reader/SonarNetCDF/Platform/VariableMRURotationY.h"
#include "Reader/SonarNetCDF/Platform/VariableMRURotationZ.h"
#include "Reader/SonarNetCDF/Platform/VariablePositionIDs.h"
#include "Reader/SonarNetCDF/Platform/VariablePositionOffsetX.h"
#include "Reader/SonarNetCDF/Platform/VariablePositionOffsetY.h"
#include "Reader/SonarNetCDF/Platform/VariablePositionOffsetZ.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerIDs.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerFunction.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerOffsetX.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerOffsetY.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerOffsetZ.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerRotationX.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerRotationY.h"
#include "Reader/SonarNetCDF/Platform/VariableTransducerRotationZ.h"

#include "Reader/SonarNetCDF/Platform/GroupAttitude.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariableAttitudeTime.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariableYaw.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariablePitch.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariableRoll.h"
#include "Reader/SonarNetCDF/Platform/Attitude/VariableVerticalOffset.h"

#include "Reader/SonarNetCDF/Platform/GroupPosition.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableHeading.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableLatitude.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableLongitude.h"
#include "Reader/SonarNetCDF/Platform/Position/VariablePositionTime.h"
#include "Reader/SonarNetCDF/Platform/Position/VariableSpeedRelative.h"

#include "Reader/SonarNetCDF/GroupAnnotation.h"

#include "Reader/SonarNetCDF/GroupProvenance.h"

#include "Reader/SonarNetCDF/GroupSonar.h"
#include "Reader/SonarNetCDF/Sonar/GroupBeam.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableActivePositionSensor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterR.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBackscatterSampleCount.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeam.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamStabilisation.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamType.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthReceiveMajor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthReceiveMinor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthTransmitMajor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBeamWidthTransmitMinor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableBlankingInterval.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableDetectedBottomRange.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEchoAngleMajor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEchoAngleMajorSensitivity.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEchoAngleMinor.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEchoAngleMinorSensitivity.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableEquivalentBeamAngle.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableFrequency.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableGainCorrection.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableNonQuantitativeProcessing.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePingTime.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformHeading.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformLatitude.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformLongitude.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformPitch.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformRoll.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariablePlatformVerticalOffset.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableReceiveDurationEffective.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableRXBeamRotationPhi.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableRXBeamRotationPsi.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableRXBeamRotationTheta.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableSampleInterval.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableSampleTimeOffset.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableSoundSpeedAtTransducer.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransducerGain.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitBandWidth.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitDurationNominal.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitFrequencyStart.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitFrequencyStop.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitPower.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTransmitType.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTXBeamRotationPhi.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTXBeamRotationPsi.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTXBeamRotationTheta.h"
#include "Reader/SonarNetCDF/Sonar/BeamGroup/VariableTXTransducerDepth.h"

#include "Reader/SonarNetCDF/Sonar/GroupGrid.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeam.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridFrequency.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariablePingAxisIntervalType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariablePingAxisIntervalValue.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableRangeAxisIntervalType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableBackscatterType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableBeamReference.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridSoundSpeedAtTransducer.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamStabilisation.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridNonQuantitativeProcessing.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableCellPingTime.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridDetectedBottomRange.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridSampleInterval.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBlankingInterval.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitPower.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamWidthTransmitMajor.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamWidthTransmitMinor.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamWidthReceiveMajor.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridBeamWidthReceiveMinor.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridGainCorrection.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridEquivalentBeamAngle.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitType.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridSampleTimeOffset.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitBandWidth.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTXBeamRotationPhi.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridRXBeamRotationPhi.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTXBeamRotationPsi.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridRXBeamRotationPsi.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTXBeamRotationTheta.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridRXBeamRotationTheta.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitDurationNominal.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitFrequencyStart.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransmitFrequencyStop.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridReceiveDurationEffective.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableCellLatitude.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableCellLongitude.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableCellDepth.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableIntegratedBackscatter.h"
#include "Reader/SonarNetCDF/Sonar/GridGroup/VariableGridTransducerGain.h"

namespace
{
	constexpr const char * LoggerName = "Reader.WriteAlgorithmToFileNetCDF";
	constexpr const char * FileDateFormat = "%Y%m%d_%H%M%S";
	constexpr const char * FileExtension = ".nc";
}

WriteAlgorithmToFileNetCDF::WriteAlgorithmToFileNetCDF()
	: WriteAlgorithmToFile()
{
}

WriteAlgorithmToFileNetCDF::~WriteAlgorithmToFileNetCDF() {
	Close();
}

void WriteAlgorithmToFileNetCDF::SounderChanged(const uint32_t & sounderId) {	
}

void WriteAlgorithmToFileNetCDF::WritePingFan(PingFan* pingFan, Sounder* sounder)
{
	// Cr�ation du fichier du sondeur si il n'existe pas
	if (!m_sounderFiles.count(sounder->m_SounderId)) {
		createSounderFile(*sounder);
		initSounderFile(*sounder);
	}

	WriteAlgorithmToFile::WritePingFan(pingFan, sounder);

	// Femeture de fichier si il y a une contrainte de taille ou temps
	ChangeFile();
}

void WriteAlgorithmToFileNetCDF::Close() {
	m_sounderTopLevelGroup.clear();
	m_sounderFiles.clear();
}

std::string WriteAlgorithmToFileNetCDF::GetFileExtension() const {
	return std::string(FileExtension);
}

void WriteAlgorithmToFileNetCDF::createSounderFile(const Sounder & sounder) {
	createSounderFile(GetSourcePath(), sounder.m_SounderId);
}

void WriteAlgorithmToFileNetCDF::createSounderFile(const std::string & filePath, const int & sounderId) {
	// D�termination nom du fichier
	auto newFilePath = filePath;

	// Recherche de l'index du point d'extension
	const auto extensionPointIndex = newFilePath.find_last_of('.');

	// D�termination de la date
	const auto nowStr = timeutils::timePointToStringLocal(std::chrono::system_clock::now(), FileDateFormat);
	
	// Insertion de la date et de l'identifiant du sondeur
	newFilePath.insert(extensionPointIndex, "_" + nowStr + "_" + std::to_string(sounderId));

	// Cr�ation et ouverture du fichier
	m_sounderFiles[sounderId] = sonarNetCDF::ncFile::createFile(newFilePath);

	// R�cup�ration du group racine
	m_sounderTopLevelGroup[sounderId] = std::make_unique<sonarNetCDF::GroupTopLevel>(m_sounderFiles[sounderId].get());
}

void WriteAlgorithmToFileNetCDF::initSounderFile(const Sounder& sounder) {
	// R�cup�ration du group racine
	auto topLevelGroup = m_sounderTopLevelGroup[sounder.m_SounderId].get();

	// Initialisation des donn�es
	// -- Plateforme
	const auto transducerCount = sounder.GetTransducerCount();
	auto platformGroup = topLevelGroup->initPlatformGroup(transducerCount, 1, transducerCount);

	const size_t positionIndex = 0;
	//platformGroup->getPositionIdsVariable() TODO ???
	platformGroup->getPositionOffsetXVariable()->setValueAtIndex(positionIndex, 0);
	platformGroup->getPositionOffsetYVariable()->setValueAtIndex(positionIndex, 0);
	platformGroup->getPositionOffsetZVariable()->setValueAtIndex(positionIndex, 0);

	for (auto transducerIndex = 0; transducerIndex < transducerCount; ++transducerIndex) {
		const auto& transducer = sounder.GetTransducer(transducerIndex);
		const auto& platform = transducer->GetPlatform();

		// platformGroup->getMruIdsVariable() TODO ???
		platformGroup->getMruOffsetXVariable()->setValueAtIndex(transducerIndex, 0);
		platformGroup->getMruOffsetYVariable()->setValueAtIndex(transducerIndex, 0);
		platformGroup->getMruOffsetZVariable()->setValueAtIndex(transducerIndex, platform->GetDephtOffset() - transducer->m_transDepthMeter);
		platformGroup->getMruRotationXVariable()->setValueAtIndex(transducerIndex, 0);
		platformGroup->getMruRotationXVariable()->setValueAtIndex(transducerIndex, 0);
		platformGroup->getMruRotationXVariable()->setValueAtIndex(transducerIndex, 0);

		platformGroup->getTransducerFunctionVariable()->setFunctionTypeAtIndex(transducerIndex, sonarNetCDF::platform::TransducerFunctionType::Type::MONOSTATIC); // On ne g�re que le cas MONOSTATIC
		platformGroup->getTransducerIdsVariable()->setValueAtIndex(transducerIndex, std::string(transducer->m_transName));
		platformGroup->getTransducerOffsetXVariable()->setValueAtIndex(transducerIndex, platform->GetAlongShipOffset());
		platformGroup->getTransducerOffsetYVariable()->setValueAtIndex(transducerIndex, platform->GetAthwartShipOffset());
		platformGroup->getTransducerOffsetZVariable()->setValueAtIndex(transducerIndex, platform->GetDephtOffset());
		platformGroup->getTransducerRotationXVariable()->setValueAtIndex(transducerIndex, RAD_TO_DEG(transducer->m_transFaceAlongAngleOffsetRad));
		platformGroup->getTransducerRotationYVariable()->setValueAtIndex(transducerIndex, RAD_TO_DEG(transducer->m_transFaceAthwarAngleOffsetRad));
		platformGroup->getTransducerRotationZVariable()->setValueAtIndex(transducerIndex, RAD_TO_DEG(transducer->m_transRotationAngleRad));
	}


	// ---- Position et Attitude
	platformGroup->initPositionSubGroup();
	platformGroup->initAttitudeSubGroup();

	// -- Annotation
	topLevelGroup->initAnnotationGroup();

	// -- Provenance
	topLevelGroup->initProvenanceGroup();

	// -- Environnement
	std::vector<float> acousticFrequencies;
	std::vector<float> absorptionCoef;
	for (const auto& transducer : sounder.GetAllTransducers()) {
		for (const auto& channelID : transducer->GetChannelId()) {
			const auto softChannel = transducer->getSoftChannel(channelID);

			acousticFrequencies.push_back((float)softChannel->m_acousticFrequency);
			absorptionCoef.push_back((float)softChannel->m_absorptionCoef * 1e-7);
		}
	}

	auto environmentGroup = topLevelGroup->initEnvironmentGroup(acousticFrequencies.size());

	environmentGroup->getSoundSpeedIndicativeVariable()->setValue(sounder.m_soundVelocity);
	environmentGroup->getAcousticFrequencyVariable()->setAllValues(acousticFrequencies);
	environmentGroup->getAbsorptionIndicativeVariable()->setAllValues(absorptionCoef);

	// -- Sonar
	auto sonarGroup = initSonarGroup(topLevelGroup, sounder);
	for (const auto& transducer : sounder.GetAllTransducers()) {

		// On ne reconstruit pas les donn�es complexes - On ne r��crit que les SV
		// Donc tous les faisceaux sont de type SPLIT_APERTURE_ANGLES
		// Et on ne g�re pas plusieurs subbeams
		const auto beamType = sonarNetCDF::sonar::BeamType::Type::SPLIT_APERTURE_ANGLES;
		const auto subbeamCount = 1;
		const auto conversionType = sonarNetCDF::sonar::ConversionEquationType::Type::TYPE_5;
		const auto beamMode = sonarNetCDF::sonar::BeamMode::Mode::VERTICAL;

		// Fr�quences -- Pour l'instant on ne r�cup�re que la fr�quence nominal peu importe le type de transmission
		std::vector<float> frequencies = { (float)transducer->getSoftChannelPolarX(0)->m_acousticFrequency };

		// Construction du beam group
		auto beamGroup = sonarGroup->initBeamGroup(transducer->m_numberOfSoftChannel, subbeamCount, frequencies.size());

		// Configuration
		beamGroup->setConversionEquationType(conversionType);
		beamGroup->setBeamMode(beamMode);

		beamGroup->getBeamVariable()->setTransducerName(transducer->m_transName);
		beamGroup->getBeamTypeVariable()->setType(beamType);

		beamGroup->getFrequencyVariable()->setAllValues(frequencies);

		size_t beamIndex = 0;
		for (const auto& channelID : transducer->GetChannelId()) {
			const auto softChannel = transducer->getSoftChannel(channelID);
			const auto channelName = std::string(softChannel->m_channelName);

			beamGroup->getBeamVariable()->setValueAtIndex(beamIndex, channelName);

			// En type SPLIT_APERTURE_ANGLES, les angles sont convertis en angles m�caniques par M3D on met donc les sensibilit�s � 1
			beamGroup->getEchoAngleMajorSensitivityVariable()->setValueAtIndex(beamIndex, 1.0f);
			beamGroup->getEchoAngleMinorSensitivityVariable()->setValueAtIndex(beamIndex, 1.0f);

			++beamIndex;
		}
	}
}

sonarNetCDF::GroupSonar* WriteAlgorithmToFileNetCDF::initSonarGroup(sonarNetCDF::GroupTopLevel * topLevelGroup, const Sounder & sounder) {
	auto sonarGroup = topLevelGroup->initSonarGroup();

	sonarGroup->setManufacturer("Simrad");
	sonarGroup->setSerialNumber(std::to_string(sounder.m_SounderId));
	sonarGroup->setModel(sounder.m_isMultiBeam ? "ME70" : "EK80");

	return sonarGroup;
}

void WriteAlgorithmToFileNetCDF::initGridGroups(sonarNetCDF::GroupSonar * sonarGroup, const Sounder & sounder, const EchoIntegrationModule& eiModule, const EchoIntegrationOutput& eiOutput) {

	// D�termination des types d'intervalle
	const auto& esuParameter = M3DKernel::GetInstance()->getMovESUManager()->GetRefParameter();
	auto pingAxisIntervalType = sonarNetCDF::sonar::PingAxisIntervalType::Type::TIME_SECONDS;
	auto pingAxisIntervalValue = 0;

	switch (esuParameter.GetESUCutType()) {
	case ESUParameter::TESUCutType::ESU_CUT_BY_TIME:
		pingAxisIntervalType = sonarNetCDF::sonar::PingAxisIntervalType::Type::TIME_SECONDS;
		pingAxisIntervalValue = esuParameter.GetESUTime().ToNanoseconds() * 1e-9;
		break;

	case ESUParameter::TESUCutType::ESU_CUT_BY_DISTANCE:
		pingAxisIntervalType = sonarNetCDF::sonar::PingAxisIntervalType::Type::DISTANCE_NAUTICAL_MILES;
		pingAxisIntervalValue = esuParameter.GetESUDistance();
		break;

	case ESUParameter::TESUCutType::ESU_CUT_BY_PING_NB:
		pingAxisIntervalType = sonarNetCDF::sonar::PingAxisIntervalType::Type::NUMBER_OF_PINGS;
		pingAxisIntervalValue = esuParameter.GetESUPingNumber();
		break;
	}

	// On ne r��crit que les SV
	// Et on ne g�re pas plusieurs subbeams
	const auto subbeamCount = 1;
	const auto conversionType = sonarNetCDF::sonar::ConversionEquationType::Type::TYPE_5;
	const auto beamMode = sonarNetCDF::sonar::BeamMode::Mode::VERTICAL;
	const auto backscatterType = sonarNetCDF::sonar::BackscatterType::Type::SV;

	std::set<float> frequencies;
	for (const auto& channelResult : eiOutput.m_tabChannelResult) {
		for (const auto& frequency : channelResult->m_tabFrequencies) {
			frequencies.insert(frequency);
		}
	}
	const auto& beamCount = eiOutput.m_tabChannelResult.size();
	const auto& frequencyCount = frequencies.size();

	// Pour chaque type de couche de l'�cho int�gration
	for (const auto& item : eiModule.GetEchoIntegrationParameter().getAllLayerTypeIndexes()) {
		const auto& layerType = item.first;
		const auto& layerTypeIndexes = item.second;

		const auto rangeAxisIntervalType = layerType == Layer::Type::SurfaceLayer
			? sonarNetCDF::sonar::RangeAxisIntervalType::Type::DEPTH
			: sonarNetCDF::sonar::RangeAxisIntervalType::Type::RANGE;

		const auto& rangeCount = layerTypeIndexes.size();

		// On cr�� un grid group
		auto gridGroup = sonarGroup->initGridGroup(
			Layer::typeToString(layerType),
			beamCount,
			subbeamCount,
			frequencyCount,
			rangeCount
		);

		// Configuration
		gridGroup->getPingAxisDimension()->setUnlimited();

		gridGroup->setConversionEquationType(conversionType);
		gridGroup->setBeamMode(beamMode);

		gridGroup->getFrequencyVariable()->setAllValues(std::vector<float>(frequencies.begin(), frequencies.end()));

		gridGroup->getPingAxisIntervalType()->setType(pingAxisIntervalType);
		gridGroup->getPingAxisIntervalValue()->setValue(pingAxisIntervalValue);

		gridGroup->getRangeAxisIntervalType()->setType(rangeAxisIntervalType);

		gridGroup->getBackscatterTypeVariable()->setTypeForAllFrequencies(backscatterType);

		// -- Pour chaque transducteur
		size_t beamIndex = 0;
		size_t transducerIndex = 0;
		for (const auto& transducer : sounder.GetAllTransducers()) {

			gridGroup->getBeamReferenceVariable()->setValueAtIndex(transducerIndex++, transducer->m_transName);

			// -- Pour chaque beam
			for (const auto& channelID : transducer->GetChannelId()) {
				const auto softChannel = transducer->getSoftChannel(channelID);
				const auto channelName = std::string(softChannel->m_channelName);

				gridGroup->getBeamVariable()->setValueAtIndex(beamIndex++, channelName);
				gridGroup->getBeamTypeVariable()->setTypeAt(channelName, ConversionTools::fromSimradBeamType((SimradBeamType)softChannel->m_beamType));
			}
		}
	}
}

void WriteAlgorithmToFileNetCDF::ChangeFile() {
	// Si la d�coupe est active
	if (m_fileMaxSize != 0 || m_fileMaxTime != 0) {

		std::set<uint32_t> sounderIdsToClose;

		// Pour chaque fichier de sondeur
		for(const auto & item : m_sounderFiles) {

			const auto& sounderId = item.first;
			const auto& sounderFile = item.second;

			// Surveillance de la taille d�pass�e
			if (m_fileMaxSize != 0) {

				// R�cup�ration de la taille du fichier
				const auto fileSize = fileutils::getFileSize(sounderFile->getFileName());

				// Changement n�cessaire
				if (fileSize > m_fileMaxSize) {
					sounderIdsToClose.insert(sounderId);
				}
			}

			// Surveillance de temps d�pass�
			if (m_fileMaxTime != 0) {
				const auto now = std::chrono::system_clock::now();

				// R�cup�ration de la date de cr�ation
				const auto fileStartTime = m_sounderTopLevelGroup[sounderId]->getCreatedDate();

				// Date limite avant d�coupe du fichier
				const auto limitTime = fileStartTime + std::chrono::seconds(m_fileMaxTime);

				if (now > limitTime) {
					sounderIdsToClose.insert(sounderId);
				}
			}
		}

		// On ferme les fichiers le n�cessitant
		for (const auto& sounderId : sounderIdsToClose) {
			m_sounderTopLevelGroup.erase(sounderId);
			m_sounderFiles.erase(sounderId);
		}
	}
}

bool WriteAlgorithmToFileNetCDF::WriteBeamPingFan(PingFan * pingFan, SoftChannel * softChannel, Transducer * transducer, const size_t & transducerNum, const size_t & beamNum) {
	assert(pingFan);
	assert(softChannel);
	assert(transducer);
	const auto subbeamNum = 0;

	// R�cup�ration du beam group associ� au channel
	auto sounder = pingFan->getSounderRef();
	auto topLevelGroup = m_sounderTopLevelGroup[sounder->m_SounderId].get();
	auto beamGroup = topLevelGroup->getSonarGroup()->getBeamGroup(transducerNum);

	// -- Traitement des donn�es
	auto pingTime = pingFan->m_ObjectTime.ToNanoseconds();
	auto beamData = pingFan->getRefBeamDataObject(softChannel->getSoftwareChannelId(), softChannel->getSoftwareVirtualChannelId());

	// Ajout du ping time
	const auto pingIndex = beamGroup->getPingTimeVariable()->findOrAppendValue(pingTime);
	
	// -- Navigation
	// ---- Attribute
	auto navigationAttribute = pingFan->GetNavAttributesRef(softChannel->getSoftwareChannelId());
	
	// Pas de cap enregistr� sur le premier ping
	if (navigationAttribute && pingIndex != 0 && navigationAttribute->m_headingRad != 0) {
		beamGroup->getPlatformHeadingVariable()->setValueAtIndex(pingIndex, RAD_TO_DEG(navigationAttribute->m_headingRad));
	}

	// --- Position
	auto navigationPosition = pingFan->GetNavPositionRef(softChannel->getSoftwareChannelId());
	if (navigationPosition) {
		beamGroup->getPlatformLatitudeVariable()->setValueAtIndex(pingIndex, navigationPosition->m_lattitudeDeg);
		beamGroup->getPlatformLongitudeVariable()->setValueAtIndex(pingIndex, navigationPosition->m_longitudeDeg);
	}

	// --- Attitude
	auto navigationAttitude = pingFan->GetNavAttitudeRef(softChannel->getSoftwareChannelId());
	if (navigationAttitude) {
		beamGroup->getPlatformPitchVariable()->setValueAtIndex(pingIndex, RAD_TO_DEG(navigationAttitude->m_pitchRad));
		beamGroup->getPlatformRollVariable()->setValueAtIndex(pingIndex, RAD_TO_DEG(navigationAttitude->m_rollRad));
		beamGroup->getPlatformVerticalOffsetVariable()->setValueAtIndex(pingIndex, navigationAttitude->m_heaveMeter);
	}

	// --
	beamGroup->getActivePositionSensorVariable()->setValueAtIndex(pingIndex, transducer->GetPlatform()->m_attitudeSensorId);
	beamGroup->getBeamStabilisationVariable()->setModeAtIndex( // stabilisation ? dans le contexte M3D => STABILISED si ME70, NOT_STABILISED sinon
		pingIndex, 
		sounder->m_isMultiBeam 
		? sonarNetCDF::sonar::BeamStabilisation::Type::STABILISED 
		: sonarNetCDF::sonar::BeamStabilisation::Type::NOT_STABILISED
	);
	beamGroup->getNonQuantitativeProcessingVariable()->setValueAtIndex(pingIndex, 0); // 0 dans le contexte M3D
	beamGroup->getSampleIntervalVariable()->setValueAtIndex(pingIndex, transducer->m_timeSampleInterval * 1e-6);
	beamGroup->getSoundSpeedAtTransducerVariable()->setValueAtIndex(pingIndex, sounder->m_soundVelocity);
	beamGroup->getTxTransducerDepthVariable()->setValueAtIndex(pingIndex, beamData->m_compensateHeave + transducer->m_transDepthMeter);

	// -- Amplitudes et angles
	const auto memoryStruct = pingFan->GetMemorySetRef()->GetMemoryStruct(transducerNum);
	const auto sampleSize = memoryStruct->GetSizeDepthUsed();

	// ---- Amplitudes
	const auto amplitudes = memoryStruct->GetDataFmt();
	const auto filter = memoryStruct->GetFilterFlag();

	// Comme on ne d�cide d'�crire que des donn�es non complexes, la variable backscatter contiendra exclusivement des donn�es de type SHORT
	auto backscatterVariable = dynamic_cast<sonarNetCDF::beamGroup::VariableBackscatterR<sonarNetCDF::sonar::SampleType::typeShort>*>(beamGroup->getBackscatterRVariable());
	if (backscatterVariable) {

		// On reconstruit la liste des donn�es de l'�chantillon
		sonarNetCDF::sonar::SampleType::typeShort sample;
		for (size_t sampleIndex = 0; sampleIndex < sampleSize; ++sampleIndex) {
			short amplitudeValue = amplitudes->GetValueToVoxel(beamNum, sampleIndex);
			if (filter->GetValueToVoxel(beamNum, sampleIndex) > 0) {
				amplitudeValue = UNKNOWN_DB;
			}
			sample.push_back(amplitudeValue);
		}
		
		// Les donn�es seront toujours des SV, on ne fait pas de conversion ici
		if (beamGroup->getConversionEquationType() == sonarNetCDF::sonar::ConversionEquationType::Type::TYPE_5) {
			beamGroup->getBackscatterSampleCountVariable()->setValueAtIndexes(pingIndex, beamNum, subbeamNum, sample.size());
			backscatterVariable->setValueAtIndexes(pingIndex, beamNum, subbeamNum, sample);
		}
		else {
			M3D_LOG_WARN(LoggerName, "Amplitudes not recorded, conversion type not handled");
		}
	}
	else {
		M3D_LOG_WARN(LoggerName, "Amplitudes not recorded, complexe data not handled");
	}

	// ---- Phase
	if (!M3DKernel::GetInstance()->GetRefKernelParameter().getIgnorePhase())
	{
		const auto phases = memoryStruct->GetPhase();	

		// On reconstruit les listes de donn�es de l'�chantillon
		sonarNetCDF::sonar::AngleType::type athwartAngles;
		sonarNetCDF::sonar::AngleType::type alongAngles;
		for (size_t sampleIndex = 0; sampleIndex < sampleSize; ++sampleIndex) {
			auto phaseValue = phases->GetValueToVoxel(beamNum, sampleIndex);
			athwartAngles.push_back(phaseValue.GetRawAthwartValue());
			alongAngles.push_back(phaseValue.GetRawAlongValue());
		}

		beamGroup->getEchoAngleMajorVariable()->setValueAtIndexes(pingIndex, beamNum, athwartAngles);
		beamGroup->getEchoAngleMinorVariable()->setValueAtIndexes(pingIndex, beamNum, alongAngles);
	}

	// --
	beamGroup->getBeamWidthTransmitMajorVariable()->setValueAtIndexes(pingIndex, beamNum, RAD_TO_DEG(softChannel->m_beam3dBWidthAthwartRad));
	beamGroup->getBeamWidthTransmitMinorVariable()->setValueAtIndexes(pingIndex, beamNum, RAD_TO_DEG(softChannel->m_beam3dBWidthAlongRad));
	beamGroup->getBeamWidthReceiveMajorVariable()->setValueAtIndexes(pingIndex, beamNum, RAD_TO_DEG(softChannel->m_beam3dBWidthAthwartRad));
	beamGroup->getBeamWidthReceiveMinorVariable()->setValueAtIndexes(pingIndex, beamNum, RAD_TO_DEG(softChannel->m_beam3dBWidthAlongRad));

	beamGroup->getGainCorrectionVariable()->setValueAtIndexes(pingIndex, beamNum, softChannel->m_beamSACorrection);
	beamGroup->getEquivalentBeamAngleVariable()->setValueAtIndexes(pingIndex, beamNum, powf(10, softChannel->m_beamEquTwoWayAngle * 0.1));
	if (beamData->m_bottomWasFound) {
		beamGroup->getDetectedBottomRangeVariable()->setValueAtIndexes(pingIndex, beamNum, beamData->m_bottomRange * 1e-3);
	}
	beamGroup->getBlankingIntervalVariable()->setValueAtIndexes(pingIndex, beamNum, 0);
	beamGroup->getTransmitTypeVariable()->setTypeAtIndex( // CW => CW(0), FM => LFM(1), pas HFM(2) dans M3D
		pingIndex, 
		beamNum, 
		transducer->m_pulseShape == 2 
		? sonarNetCDF::sonar::TransmitType::Type::LFM 
		: sonarNetCDF::sonar::TransmitType::Type::CW
	);
	beamGroup->getSampleTimeOffsetVariable()->setValueAtIndexes(pingIndex, beamNum, 0); // => internalDelayTranslation => modify transducteur, on laisse � 0
	beamGroup->getTransmitPowerVariable()->setValueAtIndexes(pingIndex, beamNum, softChannel->m_transmissionPower);
	beamGroup->getTransmitFrequencyStartVariable()->setValueAtIndexes(
		pingIndex,
		beamNum, 
		transducer->m_pulseShape == 2
		? softChannel->m_startFrequency
		: softChannel->m_acousticFrequency
	);
	beamGroup->getTransmitFrequencyStopVariable()->setValueAtIndexes(
		pingIndex,
		beamNum,
		transducer->m_pulseShape == 2
		? softChannel->m_endFrequency
		: softChannel->m_acousticFrequency
	);
	beamGroup->getTransducerGainVariable()->setValueAtIndexes({ pingIndex, beamNum, 0 }, softChannel->m_beamGain);
	beamGroup->getTransmitBandwithVariable()->setValueAtIndexes(pingIndex, beamNum, softChannel->m_bandWidth);

	beamGroup->getTxBeamRotationPhiVariable()->setValueAtIndexes(pingIndex, beamNum, RAD_TO_DEG(softChannel->m_mainBeamAthwartSteeringAngleRad));
	beamGroup->getRxBeamRotationPhiVariable()->setValueAtIndexes(pingIndex, beamNum, RAD_TO_DEG(softChannel->m_mainBeamAthwartSteeringAngleRad));
	beamGroup->getTxBeamRotationThetaVariable()->setValueAtIndexes(pingIndex, beamNum, RAD_TO_DEG(softChannel->m_mainBeamAlongSteeringAngleRad));
	beamGroup->getRxBeamRotationThetaVariable()->setValueAtIndexes(pingIndex, beamNum, RAD_TO_DEG(softChannel->m_mainBeamAlongSteeringAngleRad));
	beamGroup->getTxBeamRotationPsiVariable()->setValueAtIndexes(pingIndex, beamNum, 0);
	beamGroup->getRxBeamRotationPsiVariable()->setValueAtIndexes(pingIndex, beamNum, 0);

	// M - Nominal duration of the transmit pulse. This is not the effective pulse duration.
	beamGroup->getTransmitDurationNominalVariable()->setValueAtIndexes(pingIndex, beamNum, transducer->m_pulseDuration * 1e-6);

	// MA - Effective duration of the received pulse.This is the duration of the square pulse containing the same energy as the actual receive pulse.This parameter is either theoretical or comes from a calibration exercise and adjusts the nominal duration of the transmitted pulse to the measured one.During calibration it is obtained by integrating the energy of the received signal on the calibration target normalised by its maximum energy.Necessary for type 1, 2, 3 and 4 conversion equations.
	float effectivePulseLength = transducer->m_pulseDuration * 1e-6;
	if (transducer->m_pulseShape == 2) {
		effectivePulseLength = softChannel->m_softChannelComputeData.GetTransmitSignalObject(transducer, softChannel)->GetEffectivePulseLength();
	}
	effectivePulseLength *= powf(10, 2 * softChannel->m_beamSACorrection / 10);
	beamGroup->getReceiveDurationEffectiveVariable()->setValueAtIndexes(pingIndex, beamNum, effectivePulseLength);

	return true;
}

bool WriteAlgorithmToFileNetCDF::WriteEnvironment(Environnement * environment) {

	if (!environment) return false;

	const auto pingTime = environment->m_ObjectTime.ToNanoseconds();

	// Ecriture des donn�es pour tous les sondeurs
	for (const auto& item : m_sounderTopLevelGroup) {
		auto topLevelGroup = item.second.get();
		auto environmentGroup = topLevelGroup->getEnvironmentGroup();
	}

	return true;
}

bool WriteAlgorithmToFileNetCDF::WriteNavAttributes(NavAttributes * navAttributes) {
	if (!navAttributes) return false;

	const auto pingTime = navAttributes->m_ObjectTime.ToNanoseconds();

	// Ecriture des donn�es pour tous les sondeurs
	for (const auto& item : m_sounderTopLevelGroup) {
		auto topLevelGroup = item.second.get();

		// -- Groupe position
		auto platformGroup = topLevelGroup->getPlatformGroup();
		auto positionGroup = platformGroup->getPositionSubGroup();

		if (positionGroup) {
			const auto timeIndex = positionGroup->getTimeVariable()->findOrAppendValue(pingTime);
			positionGroup->getHeadingVariable()->setValueAtIndex(timeIndex, (float)RAD_TO_DEG(navAttributes->m_headingRad));
			positionGroup->getSpeedRelativeVariable()->setValueAtIndex(timeIndex, (float)navAttributes->m_speedMeter);
		}
	}

	return true;
}

bool WriteAlgorithmToFileNetCDF::WriteNavPosition(NavPosition * navPosition) {
	if (!navPosition) return false;

	const auto pingTime = navPosition->m_ObjectTime.ToNanoseconds();

	// Ecriture des donn�es pour tous les sondeurs
	for (const auto& item : m_sounderTopLevelGroup) {
		auto topLevelGroup = item.second.get();

		// -- Sound speed profile
		auto environmentGroup = topLevelGroup->getEnvironmentGroup();

		// -- Groupe position
		auto platformGroup = topLevelGroup->getPlatformGroup();
		platformGroup->forEachPositionGroup([&](sonarNetCDF::GroupPosition* const positionGroup) {
			const auto timeIndex = positionGroup->getTimeVariable()->findOrAppendValue(pingTime);
			positionGroup->getLatitudeVariable()->setValueAtIndex(timeIndex, (float)navPosition->m_lattitudeDeg);
			positionGroup->getLongitudeVariable()->setValueAtIndex(timeIndex, (float)navPosition->m_longitudeDeg);
		});
	}

	return true;
}

bool WriteAlgorithmToFileNetCDF::WriteNavAttitude(NavAttitude * navAttitude) {
	if (!navAttitude) return false;

	const auto pingTime = navAttitude->m_ObjectTime.ToNanoseconds();

	// Ecriture des donn�es pour tous les sondeurs
	for (const auto& item : m_sounderTopLevelGroup) {
		auto topLevelGroup = item.second.get();

		// -- Groupes attitude
		auto platformGroup = topLevelGroup->getPlatformGroup();
		platformGroup->forEachAttitudeGroup([&](sonarNetCDF::GroupAttitude* const attitudeGroup) {
			const auto timeIndex = attitudeGroup->getTimeVariable()->findOrAppendValue(pingTime);
			attitudeGroup->getVerticalOffsetVariable()->setValueAtIndex(timeIndex, (float)navAttitude->m_heaveMeter);
			attitudeGroup->getPitchVariable()->setValueAtIndex(timeIndex, (float)RAD_TO_DEG(navAttitude->m_pitchRad));
			attitudeGroup->getRollVariable()->setValueAtIndex(timeIndex, (float)RAD_TO_DEG(navAttitude->m_rollRad));
			attitudeGroup->getYawVariable()->setValueAtIndex(timeIndex, (float)RAD_TO_DEG(navAttitude->m_yawRad));
		});
	}

	return true;
}

bool WriteAlgorithmToFileNetCDF::WriteTrawlPositionAttitude(TrawlPositionAttitude * trawlPositionAttitude) {
	return false;
}

bool WriteAlgorithmToFileNetCDF::WriteDynamicPlatformPosition(DynamicPlatformPosition * dynamicPlatformPosition, const TrawlSensorPositionId & sensorId) {
	return false;
}

bool WriteAlgorithmToFileNetCDF::WriteTrawlPlatformPosition(TrawlPlatformPosition * trawlPlatformPosition) {
	return false;
}

bool WriteAlgorithmToFileNetCDF::WriteEventMarker(EventMarker * eventMarker) {
	return false;
}

void WriteAlgorithmToFileNetCDF::AddEchoIntegration(const Sounder & sounder, const EchoIntegrationModule& eiModule, const EchoIntegrationOutput& eiOutput) {

	// Cr�ation du fichier du sondeur si il n'existe pas
	if (!m_sounderFiles.count(sounder.m_SounderId)) {
		createSounderFile(sounder);

		// R�cup�ration du group racine
		auto topLevelGroup = m_sounderTopLevelGroup[sounder.m_SounderId].get();

		// Initialisation du groupe Sonar
		auto sonarGroup = initSonarGroup(topLevelGroup, sounder);

		// Initialisation des groupes Grid
		initGridGroups(sonarGroup, sounder, eiModule, eiOutput);

		// Synchronisation du fichier sur disque
		m_sounderFiles[sounder.m_SounderId]->sync();
	}
	
	// R�cup�ration du group racine
	auto topLevelGroup = m_sounderTopLevelGroup[sounder.m_SounderId].get();

	// R�cup�ration du groupe Sonar
	auto sonarGroup = topLevelGroup->getSonarGroup();

	// R�cup�ration de chaque grid groupe par type de couche
	std::map<Layer::Type, sonarNetCDF::GroupGrid*> gridGroups;
	for (const auto& layerType : Layer::allTypes()) {
		gridGroups[layerType] = sonarGroup->getGridGroupWithLayerTypeName(Layer::typeToString(layerType));
	}

	// La dimension ping �tant infinie, l'index de ping sera � la suite des donn�es d�j� �crites
	std::map<sonarNetCDF::GroupGrid*, size_t> pingIndexByGridGroup;
	for (const auto& gridGroup : sonarGroup->getAllGridGroups()) {
		const auto pingIndex = gridGroup->getPingAxisDimension()->incrementDimensionSize() - 1;

		// -- Ecriture dans le groupe des donn�es d�pendant uniquement du ping
		gridGroup->getSoundSpeedAtTransducerVariable()->setValueAtIndex(pingIndex, sounder.m_soundVelocity);
		gridGroup->getBeamStabilisationVariable()->setModeAt(pingIndex, sounder.m_isMultiBeam ? sonarNetCDF::sonar::BeamStabilisation::Type::STABILISED : sonarNetCDF::sonar::BeamStabilisation::Type::NOT_STABILISED);
		gridGroup->getNonQuantitativeProcessingVariable()->setValueAtIndex(pingIndex, 0); // TODO - Qu'est ce ?

		pingIndexByGridGroup[gridGroup] = pingIndex;
	}

	// Organisation d'un r�sultat d'�cho int�gration : [channel][frequency][layer]
	// Chaque channel
	for (size_t beamIndex = 0; beamIndex < eiOutput.m_tabChannelResult.size(); ++beamIndex) {
		
		const auto& eiChannelResult = eiOutput.m_tabChannelResult[beamIndex];
		const auto& channel = sounder.GetSoftChannelByIndex(beamIndex);
		const auto& transducer = sounder.getTransducerForChannel(channel->m_softChannelId);

		// Calcul des gains des transducteurs
		const auto gains = eiModule.getCalibrationModule()->getCalibrationGain(channel, eiChannelResult->m_tabFrequencies);
		
		// Les donn�es d�pendant uniquement du ping et du beam
		for (const auto& item : gridGroups) {

			const auto& layerType = item.first;
			auto gridGroup = item.second;

			if (gridGroup == nullptr) continue;

			const auto& pingIndex = pingIndexByGridGroup[gridGroup];

			gridGroup->getCellPingTimeVariable()->setValueAtIndexes(pingIndex, beamIndex, eiOutput.m_timeEnd.ToNanoseconds());
			gridGroup->getDetectedBottomRangeVariable()->setValueAtIndexes(pingIndex, beamIndex, eiChannelResult->m_BottomDepth);
			gridGroup->getSampleIntervalVariable()->setValueAtIndexes(pingIndex, beamIndex, transducer->m_timeSampleInterval * 1e-6);
			gridGroup->getBlankingIntervalVariable()->setValueAtIndexes(pingIndex, beamIndex, 0);

			gridGroup->getTransmitPowerVariable()->setValueAtIndexes(pingIndex, beamIndex, channel->m_transmissionPower);
			gridGroup->getBeamWidthTransmitMajorVariable()->setValueAtIndexes(pingIndex, beamIndex, RAD_TO_DEG(channel->m_beam3dBWidthAthwartRad));
			gridGroup->getBeamWidthTransmitMinorVariable()->setValueAtIndexes(pingIndex, beamIndex, RAD_TO_DEG(channel->m_beam3dBWidthAlongRad));
			gridGroup->getBeamWidthReceiveMajorVariable()->setValueAtIndexes(pingIndex, beamIndex, RAD_TO_DEG(channel->m_beam3dBWidthAthwartRad));
			gridGroup->getBeamWidthReceiveMinorVariable()->setValueAtIndexes(pingIndex, beamIndex, RAD_TO_DEG(channel->m_beam3dBWidthAlongRad));

			gridGroup->getGainCorrectionVariable()->setValueAtIndexes(pingIndex, beamIndex, channel->m_beamSACorrection);
			gridGroup->getEquivalentBeamAngleVariable()->setValueAtIndexes(pingIndex, beamIndex, std::pow(10, 0.1 * channel->m_beamEquTwoWayAngle));

			gridGroup->getTransmitTypeVariable()->setTypeAtIndex(pingIndex, beamIndex,
				transducer->m_pulseShape == 2
				? sonarNetCDF::sonar::TransmitType::Type::LFM
				: sonarNetCDF::sonar::TransmitType::Type::CW);

			gridGroup->getSampleTimeOffsetVariable()->setValueAtIndexes(pingIndex, beamIndex, 0);

			gridGroup->getTransmitBandwidthVariable()->setValueAtIndexes(pingIndex, beamIndex, channel->m_bandWidth);
			gridGroup->getTxBeamRotationPhiVariable()->setValueAtIndexes(pingIndex, beamIndex, RAD_TO_DEG(channel->m_mainBeamAthwartSteeringAngleRad));
			gridGroup->getRxBeamRotationPhiVariable()->setValueAtIndexes(pingIndex, beamIndex, RAD_TO_DEG(channel->m_mainBeamAthwartSteeringAngleRad));
			gridGroup->getTxBeamRotationThetaVariable()->setValueAtIndexes(pingIndex, beamIndex, RAD_TO_DEG(channel->m_mainBeamAlongSteeringAngleRad));
			gridGroup->getRxBeamRotationThetaVariable()->setValueAtIndexes(pingIndex, beamIndex, RAD_TO_DEG(channel->m_mainBeamAlongSteeringAngleRad));
			gridGroup->getTxBeamRotationPsiVariable()->setValueAtIndexes(pingIndex, beamIndex, 0);
			gridGroup->getRxBeamRotationPsiVariable()->setValueAtIndexes(pingIndex, beamIndex, 0);

			gridGroup->getTransmitDurationNominalVariable()->setValueAtIndexes(pingIndex, beamIndex, transducer->m_pulseDuration);

			gridGroup->getTransmitFrequencyStartVariable()->setValueAtIndexes(
				pingIndex,
				beamIndex,
				transducer->m_pulseShape == 2
				? channel->m_startFrequency
				: channel->m_acousticFrequency
			);
			gridGroup->getTransmitFrequencyStopVariable()->setValueAtIndexes(
				pingIndex,
				beamIndex,
				transducer->m_pulseShape == 2
				? channel->m_endFrequency
				: channel->m_acousticFrequency
			);

			// MA - Effective duration of the received pulse.This is the duration of the square pulse containing the same energy as the actual receive pulse.This parameter is either theoretical or comes from a calibration exercise and adjusts the nominal duration of the transmitted pulse to the measured one.During calibration it is obtained by integrating the energy of the received signal on the calibration target normalised by its maximum energy.Necessary for type 1, 2, 3 and 4 conversion equations.
			float effectivePulseLength = transducer->m_pulseDuration * 1e-6;
			if (transducer->m_pulseShape == 2) {
				effectivePulseLength = channel->m_softChannelComputeData.GetTransmitSignalObject(transducer, channel)->GetEffectivePulseLength();
			}
			effectivePulseLength *= powf(10, 2 * channel->m_beamSACorrection / 10);
			gridGroup->getReceiveDurationEffectiveVariable()->setValueAtIndexes(pingIndex, beamIndex, effectivePulseLength);

			// Parcours par couche
			const auto& layers = eiModule.GetEchoIntegrationParameter().getLayerTypeIndexes(layerType);

			// Archivage du transducer gain
			std::vector<size_t> frequencyIndexTab(eiChannelResult->m_tabFrequencies.size());
			for (size_t resulFrequencyIndex = 0; resulFrequencyIndex < eiChannelResult->m_tabFrequencies.size(); ++resulFrequencyIndex)
			{				
				const auto& frequency = eiChannelResult->m_tabFrequencies[resulFrequencyIndex];
				const auto frequencyIndex = gridGroup->getFrequencyVariable()->getIndex(frequency);
				frequencyIndexTab[resulFrequencyIndex] = frequencyIndex;

				gridGroup->getTransducerGainVariable()->setValueAtIndexes(pingIndex, frequencyIndex, gains[resulFrequencyIndex]);
			}

			// Pr�-construction des donn�es avant archivage netCDF
			std::vector<double> latitudes(layers.size());
			std::vector<double> longitudes(layers.size());
			std::vector<double> depths(layers.size());

			size_t rangeIndex = 0;
			for (const auto& layerIndex : layers)
			{
				const auto& eiLayerResult = eiChannelResult->m_tabLayerResult[0][layerIndex];

				// Donn�es d�pendantes du ping, du beam et de la couche
				latitudes[rangeIndex] = eiLayerResult->GetLatitude();
				longitudes[rangeIndex] = eiLayerResult->GetLongitude();
				depths[rangeIndex] = eiLayerResult->GetDepth();

				// Parcours des fr�quences
				for (size_t resulFrequencyIndex = 0; resulFrequencyIndex < eiChannelResult->m_tabFrequencies.size(); ++resulFrequencyIndex) 
				{
					const auto& eiFrequencyResult = eiChannelResult->m_tabLayerResult[resulFrequencyIndex];
					const auto sv = eiFrequencyResult[layerIndex]->GetSv();
					if (sv != XMLBadValue) 
					{
						gridGroup->getIntegratedBackscatterVariable()->setValueAtIndexes(pingIndex, rangeIndex, frequencyIndexTab[resulFrequencyIndex], sv);
					}
				}

				++rangeIndex;
			}

			// Archivage massif des donn�es netCDF sur la dimension "range"
			gridGroup->getCellLatitudeVariable()->setIndexedValues({pingIndex, 0, beamIndex}, {1, latitudes.size(), 1}, latitudes);
			gridGroup->getCellLongitudeVariable()->setIndexedValues({pingIndex, 0, beamIndex}, {1, longitudes.size(), 1}, longitudes);
			gridGroup->getCellDepthVariable()->setIndexedValues({0, beamIndex}, { depths.size(), 1}, depths);
		}
	}

	// Femeture de fichier si il y a une contrainte de taille ou temps
	ChangeFile();
}
