// -*- C++ -*-
// ****************************************************************************
// Class: ShoalExtractionGeometry
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "ShoalExtraction/data/pingdata.h"
#include "ShoalExtraction/geometry/Segment3D.h"
#include <memory>

namespace BaseMathLib
{
	class Poly2D;
}

namespace shoalextraction
{
	class ShoalId;
	class EchoGroup;
	class ShoalIdMgr;

    typedef std::map<std::uint64_t, shoalextraction::PingData*> PingDataMap;

	class ShoalExtractionGeometry
	{
	public:

		// Creation de la surface 2D d'un ensemble d'�chos
		static void CreateEchoSurf2D(double	dl_beamsSamplesSpacing,
			double		xIntegrationDistance,
			double		zIntegrationDistance,
			double		dl_angleSteering,
			double		dl_angle3dB,
			bool		computeMargins,
			EchoGroup* pEchoGroup);

		//Calcul du volume 3D d'un groupe d'echos
		static void ComputeVolumes(PingFan * pFan, Sounder *pSounder,
			int transId, int channelId,
			double cosAlong, double tanAlong,
			double cosAthwart, double tanAthwart,
			double beamsSamplesSpacing,
			double transTranslation,
			EchoGroup* pEchoGroup);

		// ComputeVolumes
		static void ComputeVolumes(PingData& pingData);

		// Calcul de la profondeur du fond pour un faisceau donn�
		static 	double ComputeGround(PingData* pPingData, const Transducer * pTrans, std::int32_t channelId, std::uint32_t& groundEcho);

		// Calcul de l'intersection d'un segment avec un faisceau d'angle donn�
		static 	int IntersectChannel(
			double cos_angleSteering, double tan_angleSteering,
			double beamsSamplesSpacing,
			const BaseMathLib::Vector2D& p1, const BaseMathLib::Vector2D& p2);

		// ajouter les echos inclus dans l'espace entre les 2 surfaces 2D
		//					� la liste des echos du banc
		static void AggregateEmptySpace2D(
			const BaseMathLib::Poly2D& surfA, const BaseMathLib::Poly2D& surfB,
			int transId, int channelIdA, int channelIdB,
			std::shared_ptr<ShoalId> pShoalId, PingData& pingData);

		// ajouter les echos inclus dans l'espace entre les 2 segments 2D [A1 B1] et [A2 B2]
		//					� la liste des echos du banc
		static void AggregateEmptySpace2D(
			const BaseMathLib::Vector2D& A1, const BaseMathLib::Vector2D& B1,
			const BaseMathLib::Vector2D& A2, const BaseMathLib::Vector2D& B2,
			int transId, int channelIdA, int channelIdB,
			std::shared_ptr<ShoalId> pShoalId, PingData& pingData);

		// renvoie le point d'intersection entre le segment et le plan dans le repere 2D du plan
		static void IntersectPlane(const Plane3D& plane,
			const Segment3D& segment,
			const BaseMathLib::Vector3D& planeOrigin,
			double cosPlanePitch,
			BaseMathLib::Vector2D& intersect2D);

		// r�cup�rer le segment intersectant le plan � partir des 4 points [A, B] et [C, D]
		//	on teste par odre de priorit� [B,C], [A,B], [C,D] et [A,D]
		static Segment3D* GetIntersectSegment(const Plane3D& plane,
			const BaseMathLib::Vector3D& pointA,
			const BaseMathLib::Vector3D& pointB,
			const BaseMathLib::Vector3D& pointC,
			const BaseMathLib::Vector3D& pointD);

		//agr�gation de l'espace 3D entre deux groupes d'�chos d'un mm banc
		static bool AggregateEmptySpace3D(int iChannel1, int iChannel2,
			EchoGroup* echoGroup1, EchoGroup* echoGroup2,
			const TransductData* pTransData,
			int sounderId,
            std::uint64_t pingA,
            std::uint64_t pingB,
			std::shared_ptr<ShoalId> pShoalId,
			ShoalIdMgr* pShoalIdMgr,
			PingDataMap& pings);

		//ajouter les echos inclus entre les vecteurs d'intersection et le faisceau 3D
		static EchoGroup* IntersectChannel3D(PingData* pPingData,
			Sounder	*pSounder,
			unsigned int iChannel,
			const TransductData* pTransData,
			std::shared_ptr<ShoalId> pShoalId,
			ShoalIdMgr* pShoalIdMgr,
			const BaseMathLib::Vector2D& intersect2D_HG,
			const BaseMathLib::Vector2D& intersect2D_HD,
			const BaseMathLib::Vector2D& intersect2D_BG,
			const BaseMathLib::Vector2D& intersect2D_BD);

	private:
	};
};
