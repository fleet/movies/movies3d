#pragma once

#include "ShoalExtraction/ShoalExtractionExport.h"
#include "M3DKernel/module/ProcessModule.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"

#include "M3DKernel/datascheme/MemorySet.h"
#include "ShoalExtraction/ShoalExtractionParameter.h"
#include <iostream>
#include <fstream>
#include <vector>

#include "ShoalExtraction/ShoalExtractionOutput.h"
#include "ShoalExtraction/multithread/ShoalExtractionActionStack.h"

namespace shoalextraction
{
	class ShoalData;
	class PingData;
	class ShoalIdMgr;
    typedef std::map<std::uint64_t, shoalextraction::PingData*> PingDataMap;
}

struct SShoalTrace
{
    std::uint64_t firstESU;
    std::uint64_t firstPing;
    std::uint64_t lastPing;

	SShoalTrace()
	{
		firstESU = std::numeric_limits<int>::max();
		firstPing = std::numeric_limits<int>::max();
		lastPing = -std::numeric_limits<int>::max();
	}
    SShoalTrace(std::uint64_t _firstESU, std::uint64_t _firstPing, std::uint64_t _lastPing)
	{
		firstESU = _firstESU;
		firstPing = _firstPing;
		lastPing = _lastPing;
	}
};

typedef std::map<std::uint64_t, SShoalTrace> ShoalTraceMap;

class SHOALEXTRACTION_API ShoalExtractionModule : public ProcessModule
{
public:
	MovCreateMacro(ShoalExtractionModule);
	virtual ~ShoalExtractionModule();

	void PingFanAdded(PingFan *);
	void SounderChanged(std::uint32_t);
	void StreamClosed(const char *streamName);
	void StreamOpened(const char *streamName);
	void onEnableStateChange() override;
	void Restart();

	void ESUStart(ESUParameter * pWorkingESU);
	void ESUEnd(ESUParameter * pWorkingESU, bool abort);

	void ForceCloseShoals();

	void WaitExtractionActions();

	BaseKernel::ParameterModule& GetParameterModule() { return m_parameter; }
	ShoalExtractionParameter&	GetShoalExtractionParameter() { return m_parameter; }

	//internal shoal storage
	const std::vector<shoalextraction::ShoalData*>& GetShoals() const { return m_shoals; }
	std::vector<shoalextraction::ShoalData*>& GetShoals() { return m_shoals; }

	// Outputs
	std::vector<ShoalExtractionOutput*> GetOutputs() const;
	std::vector<ShoalExtractionOutput*> GetOutputs(tConsumerId consumerId) const;	

	//*****************************************
	//functions accessible from worker thread

	//add ping fan (use from worker thread)
	void AddPingFan(PingFan *pFan);

	// Fermeture des donn�es qui n'ont pas de donn�es depuis pingIdOutOfRange
    void CloseData(std::uint64_t pingIdOutOfRange, std::uint64_t currentESUId);

	//close ESU
	void ESUEnd();

	//start ESU
	void ESUStart();

	//inc ESU Id
	void IncESUId();

	//ShoalIdMgr (use from worker thread)
	shoalextraction::ShoalIdMgr* GetShoalIdMgr() { return m_pShoalIdMgr; };

	// Purge forc�e de toutes les donn�es
	void ForcePurge();

	// OTK - 21/10/2009 - information du retard de l'extraction asynchrone
	void SetDelayInfoCallBack(DELAYINFO_CALLBACK cb)
	{
		m_ReaderPingFanCount = 0;
		m_DelayInfo = cb;
	};

protected:
	ShoalExtractionModule();

	//Initialisation de certaines donn�es du ping
	void InitPingData(shoalextraction::PingData& pingData);

	//agr�gation 1D des echos (intra-faisceau)
	void Extract1DEcho(shoalextraction::PingData& pingData);

	//agr�gation 2D des echos (inter-faisceaux)
	void Aggregate2DEcho(shoalextraction::PingData& pingData);

	// Agregation du ping avec les pings precedent
	void AggregatePing(shoalextraction::PingData* pingData);

	// agr�gation de deux pings
	void AggregatePings(shoalextraction::PingData* pingData1, shoalextraction::PingData* pingData2, bool previousPing);

	// Fermeture des pings en limite de distance
    void ClosePings(std::uint64_t pingIdOutOfRange);

	// Fermeture des bancs qui n'ont pas de donn�es depuis pingIdOutOfRange
    void CloseShoals(std::uint64_t pingIdOutOfRange, std::uint64_t currentESUId);

	// Purge des bancs ouverts et invalides
	void PurgeOpenShoals();

	// Purge de l'historique des pings
	void PurgePingHistory();

	//Fermeture du banc d'id donn� (le banc n'a plus de donn�es depuis le ping sp�cifi�)
    void CloseShoal(std::uint64_t shoalId, std::uint64_t firstPing, std::uint64_t lastPing);

	// Calcul de la boite englobante du banc
	void ComputeShoalBBox(shoalextraction::ShoalData* pShoalData);

	// Calcul des statistiques du banc
	bool ComputeShoalStats(shoalextraction::ShoalData* pShoalData);

	// ecriture des parametres du banc
	void Serialize(shoalextraction::ShoalData* pShoalData);

    inline void SetShoalTrace(std::uint64_t shoalId, std::uint64_t firstESU, std::uint64_t firstPing, std::uint64_t lastPing);
    inline void ResetShoalTrace(std::uint64_t shoalId);
    inline void UpdateShoalTrace(std::uint64_t shoalId, std::uint64_t firstESU, std::uint64_t firstPing, std::uint64_t lastPing);

	//gestion des flux de sortie csv
	std::ofstream* GetCsvStream(std::uint32_t sounderId, std::uint32_t transId);

	ShoalExtractionParameter	m_parameter;

private:

	//id de l'ESU courant
    std::uint64_t m_ESUId;

	//liste des donn�es par ping
	shoalextraction::PingDataMap m_Pings;

	//premier ESU et dernier ping inclus dans chaque banc ouvert
	ShoalTraceMap m_mapShoalTrace;

	//table des fichiers de sortie csv (un par transducteur)
	std::map<std::string, std::ofstream*> m_mapCsvStream;

	//internal shoal storage
	std::vector<shoalextraction::ShoalData*> m_shoals;

	//ShoalIdMgr
	shoalextraction::ShoalIdMgr* m_pShoalIdMgr;

	//extern shoal id
    std::uint64_t m_uiExternShoalId;

	// action stack to schedule extraction work
	shoalextraction::ShoalExtractionActionStack m_actionStack;

	// OTK - 21/12/2009 - gestion des configurations particulieres (extraction sur des pings
	// sortis de la zone m�moire qu'il faut garder en r�f�rence). Leur desctruction
	// doit se faire de fa�on synchrone avec le readerThread pour �viter les plantages.
	CRecursiveMutex m_lock;
	std::vector<PingFan*> m_PingFansToUnRef;

	// OTK - 20/10/2009 - pour informer (l'IHM) du retard �ventuel de l'extraction
	DELAYINFO_CALLBACK m_DelayInfo;
	std::uint32_t m_ReaderPingFanCount;

};
