#pragma once
#include "ShoalExtraction/ShoalExtractionExport.h"
#include "M3DKernel/module/ModuleOutput.h"
#include "M3DKernel/datascheme/MemorySet.h"
#include "MovNetwork/MvNetDataXMLShoalAnset.h"
#include "MovNetwork/MvNetDataXMLShipnav.h"
#include "MovNetwork/MvNetDataXMLSndset.h"
#include "MovNetwork/MvNetDataXMLShoal.h"
class vtkActor;

namespace shoalextraction {

	class ShoalData;
}

class ShoalExtractionParameter;
class SHOALEXTRACTION_API ShoalExtractionOutput : public ModuleOutput
{
public:
	MovCreateMacro(ShoalExtractionOutput);

	virtual ~ShoalExtractionOutput(void);
public:
	/**my data are recorded here*/
	shoalextraction::ShoalData* m_pClosedShoal;

	// red�finition des m�thodes de s�rialisation et de d�s�rialisation pour les sorties XML / CSV
	virtual void XMLSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);
	virtual void CSVSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);
	virtual void XMLHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);
	virtual void CSVHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor);

	// renseigne les objets n�cessaires � la s�rialisation (donn�es de navigation, etc...)
	virtual void PrepareForSerialization(ShoalExtractionParameter & params, PingFan * pFan);


protected:
	ShoalExtractionOutput(void);

private:
	// informations de navigation en d�but de banc
	CMvNetDataXMLShipnav							m_MvNetDataXMLShipnavStart;
	// informations de navigation en fin de banc
	CMvNetDataXMLShipnav							m_MvNetDataXMLShipnavStop;
	// param�tres d'extraction
	CMvNetDataXMLShoalAnset						m_MvNetDataXMLShoalAnset;
	// banc extrait
	CMvNetDataXMLShoal								m_MvNetDataXMLShoal;
	// stats channels
	std::vector<CMvNetDataXMLSndset>	m_MvNetDataXMLSndSets;
};

#include <vector>

class SHOALEXTRACTION_API ShoalExtractionOutputList : public std::vector<ShoalExtractionOutput * >
{
public:
	ShoalExtractionOutputList() {};
	virtual ~ShoalExtractionOutputList() {};

	//return the output found at the given point
	ShoalExtractionOutput * GetOutputAtPoint(
		const std::string& transducerName, int ping, int beam, int echoIdx) const;

private:

};
