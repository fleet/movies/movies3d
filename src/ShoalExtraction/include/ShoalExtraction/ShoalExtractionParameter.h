#pragma once
#include "ShoalExtraction/ShoalExtractionExport.h"
#include "M3DKernel/parameter/ParameterModuleTransducerFiltered.h"

#include <vector>

class SHOALEXTRACTION_API ShoalExtractionParameter :
	public BaseKernel::ParameterModuleTransducerFiltered
{
public:
	ShoalExtractionParameter();

	virtual bool Serialize(BaseKernel::MovConfig * movConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * movConfig);

	// *********************************************************************
	// Accesseurs
	// *********************************************************************

	//Vertical Integration Distance
	virtual float GetVerticalIntegrationDistance() const { return m_verticalIntegrationDistance; }
	virtual void SetVerticalIntegrationDistance(float d) { m_verticalIntegrationDistance = d; }

	//Along Integration Distance
	virtual float GetAlongIntegrationDistance() const { return m_alongIntegrationDistance; }
	virtual void SetAlongIntegrationDistance(float d) { m_alongIntegrationDistance = d; }

	//Across Integration Distance
	virtual float GetAcrossIntegrationDistance() const { return m_acrossIntegrationDistance; }
	virtual void SetAcrossIntegrationDistance(float d) { m_acrossIntegrationDistance = d; }

	//threshold
	virtual float GetThreshold() const { return m_threshold; }
	virtual void SetThreshold(float d) { m_threshold = d; }

	//Max threshold
	virtual float GetMaxThreshold() const { return m_maxThreshold; }
	virtual void SetMaxThreshold(float d) { m_maxThreshold = d; }

	//Minimum shoal Length
	virtual float GetMinLength() const { return m_minLength; }
	virtual void SetMinLength(float d) { m_minLength = d; }

	//Minimum shoal Width
	virtual float GetMinWidth() const { return m_minWidth; }
	virtual void SetMinWidth(float d) { m_minWidth = d; }

	//Minimum shoal Height
	virtual float GetMinHeight() const { return m_minHeight; }
	virtual void SetMinHeight(float d) { m_minHeight = d; }

	//Minimum shoal Depth
	virtual float GetMinDepth() const { return m_minDepth; }
	virtual void SetMinDepth(float d) { m_minDepth = d; }

	//Maximum shoal Depth
	virtual float GetMaxDepth() const { return m_maxDepth; }
	virtual void SetMaxDepth(float d) { m_maxDepth = d; }

	//SaveEchos option
	virtual bool GetSaveEchos() const { return m_saveEchos; }
	virtual void SetSaveEchos(bool save) { m_saveEchos = save; }

	//Save Contour Positions
	virtual bool GetSaveContour() const { return m_saveContourPositions; }
	virtual void SetSaveContour(bool save) { m_saveContourPositions = save; }

	//Record File Path
	virtual std::string GetRecordPath() const { return m_recordPath; }
	virtual void SetRecordPath(std::string path) { m_recordPath = path; }

	//Overlap managment
	virtual bool GetUseOverlapFilter() const { return m_useOverlapFilter; }
	virtual void SetUseOverlapFilter(bool use) { m_useOverlapFilter = use; }

	//use internal shoal storage
	virtual bool GetUseInternalShoalStorage() const { return m_useInternalShoalStorage; }
	virtual void SetUseInternalShoalStorage(bool use) { m_useInternalShoalStorage = use; }

	//use only central beam
	virtual bool GetUseOnlyCentralBeam() const { return m_useOnlyCentralBeam; }
	virtual void SetUseOnlyCentralBeam(bool val) { m_useOnlyCentralBeam = val; }

private:

	// *********************************************************************
	// Variables membres
	// *********************************************************************	

	//Vertical Integration Distance
	float m_verticalIntegrationDistance;
	//Along Integration Distance
	float m_alongIntegrationDistance;
	//Across Integration Distance
	float m_acrossIntegrationDistance;

	//threshold
	float  m_threshold;
	float  m_maxThreshold;

	//Minimum shoal Length
	float m_minLength;
	//Minimum shoal Width
	float m_minWidth;
	//Minimum shoal Height
	float m_minHeight;

	//Result file path
	std::string m_recordPath;
	//SaveEchos option
	bool m_saveEchos;
	bool m_saveContourPositions;

	// *********************************************************************	
	// extra parameters used to compute frequency response
	// *********************************************************************	

	//Overlap managment
	bool m_useOverlapFilter;

	//use internal shoal storage
	bool m_useInternalShoalStorage;

	//use only central beam
	bool m_useOnlyCentralBeam;

	//Minimum shoal Depth
	float m_minDepth;
	//Maximum shoal Depth
	float m_maxDepth;
};

