// -*- C++ -*-
// ****************************************************************************
// Class: ShoalExtractionStat
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "BaseMathLib/Vector3.h"

namespace shoalextraction
{
	class PingData;
	class ShoalData;
	class ShoalId;
	class Poly3D;
	struct HullItem;
	struct AggregatedHullItem;
	struct ExtrudedComponent;
	struct AngleSegment;

	enum PositionningType {
		GPS_POSITIONNING = 0,
		INCREMENT_POSITIONNING
	};

	class ShoalExtractionStat
	{
	public:

		// Initialisation des donn�es statistiques du ping
		static void CompletePingData(PingData* pPingData, double maxThreshold);

		// Calcul des statistiques du ping
		static void ComputePingStats(PingData* pPingData);

		// Calcul de la surface du banc pour une composante donn�e
		//	(sur un mm ping, un banc peut avoir plusieurs composantes issues de l'agr�gation 3D)
		static HullItem* ComputePingSurface(PingData* pPingData, const std::set<std::uint64_t>& pShoalIds,
			bool complete = true);

		// Calcul des surfaces du ping
		static void ComputePingSurfaces(PingData* pPingData);

		// Calcul de l'enveloppe 3D de la surface du ping
		static void ComputePingHull(PingData* pPingData);

		// Calcul des coordonn�es rep�re monde pour un HullItem
		static void ComputePingHullWorldCoords(double cosRoll, double cosPitch,
			double rollPitchFactor, double cosHeading, double sinHeading,
			const BaseMathLib::Vector3D& transOrigin,
			const BaseMathLib::Vector3D& transOriginGPS,
			HullItem* hullItem);

		// Calcul du volume d'un ping unique : 
		static double ComputePingVolume(PingData* pPingData);

		// Calcul des param�tres du ping pour la BBox
		static void ComputePingBBoxParameters(PingData* pPingData, double& centerAngle);

		// ComputeShoalVolume
		static void ComputeShoalVolume(ShoalData* pShoalData);

		// ComputeShoalEnergy
		static void ComputeShoalEnergy(ShoalData* pShoalData);

		// Calcul de la boite englobante 3D orient�e du ping
		static void ComputeShoalBBox(ShoalData* pShoalData,
			double steeringAngle,
			double pitchAngle,
			double rollAngle,
			BaseMathLib::Vector3D& centerPoint,
			BaseMathLib::Vector3D& centerPointGPS);

		// Filtre le banc par rapport a sa taille
		static bool FilterShoalSize(ShoalData* pShoalData);

		// Calcul des statistiques du banc
		static bool ComputeShoalStats(ShoalData* pShoalData, double maxThreshold);

		// OTK - calcul des facettes de contour du banc
		static void ComputeTriangleStrips(ShoalData* pShoalData);

		// OTK - calcul de la facette d'une composante 2D d'un banc
		static std::vector<double> ComputeHullStrip(AggregatedHullItem* aggregatedHull,
			PositionningType);

		// OTK - calcul de la tranche entre deux surfaces li�es une � une
		static std::vector<std::vector<double>>ComputeExclusiveLinkStrip(HullItem* hull,
			HullItem* previousHull, PositionningType);

		// OTK - calcul d'un trianglestrip entre deux lignes de points
		static void AddStripFromLines(Poly3D* line1, int startIdx1, int stopIdx1,
			Poly3D* line2, int startIdx2, int stopIdx2,
			std::vector<std::vector<double>>& stripResult);

		// OTK - tri des aggregats 2D en listes des diff�rents types
		static void ClassifyAggregateHullItems(std::vector<AggregatedHullItem*>& in,
			std::vector<AggregatedHullItem*>& simple, std::vector<ExtrudedComponent*>& merged);

		// OTK - triangulation des facettes dans le plan du ping
		// permettant de fermer le contour du banc
		static void TriangulateExtrudedSurface(PingData* pPingData,
			ExtrudedComponent* pExtrusionParameters,
			std::vector<std::vector<double>>& gpsStrips,
			std::vector<std::vector<double>>& incrementStrips);

	private:

		// OTK - calcul des segments d'une surface
		static void ComputeSegments(PingData* pPingData,
			std::set<std::uint64_t> pShoalIds,
			std::vector<AngleSegment>& resultingSegments);

		// OTK - ajout des segments "manquants" entre deux segments
		static void InterpolateMissingValues(std::vector<AngleSegment>& globalSegments,
			std::vector<AngleSegment>& segments, double sampleSpacing,
			double minDeltaAngle);

		// OTK - interpolation d'un segment de surface
		static void InterpolateSegment(double angle, double x1, double y1,
			double x2, double y2, double &x, double &y);

		// OTK - calcul des facettes
		static void ComputeExtrudedStrips(std::vector<AngleSegment>& globalSegments,
			ExtrudedComponent* pExtrusionParameters,
			std::vector<std::vector<double>>& gpsStrips,
			std::vector<std::vector<double>>& incrementStrips,
			PingData* pPingData,
			double minDeltaAngle);

		// OTK - triangulation d'une facette de 4 sommets
		static void TriangulateQuad(double x1, double y1, double x2, double y2,
			double x3, double y3, double x4, double y4,
			double rollPitchFactor, double cosHeading, double sinHeading,
			const BaseMathLib::Vector3D& transOrigin, const BaseMathLib::Vector3D& transOriginGPS,
			bool normalShipDirection,
			std::vector<std::vector<double>>& gpsStrips, std::vector<std::vector<double>>& incrementStrips);
	};
};
