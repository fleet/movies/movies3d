// -*- C++ -*-
// ****************************************************************************
// Class: ChannelData
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <vector>
#include "echogroup.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class ChannelData
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		ChannelData(void);

		// Destructeur
		virtual ~ChannelData(void);

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		// ajout d'un groupe d'�cho pour un banc donn�
		virtual EchoGroup* AddEchoGroup(std::int32_t echoMin, std::int32_t echoMax, std::shared_ptr<ShoalId> shoalId);

		// *********************************************************************
		// Accesseurs
		// *********************************************************************

		//num�ro de faisceau
		virtual inline int GetChannelId() const { return m_channelId; };
		virtual inline void SetChannelId(int id) { m_channelId = id; };

		//num�ro de faisceau kernel
		virtual inline int GetSoftwareChannelId() const { return m_softwareChannelId; };
		virtual inline void SetSoftwareChannelId(int id) { m_softwareChannelId = id; };

		//echo du fond
		virtual inline int GetGroundEcho() const { return m_groundEcho; };
		virtual inline void SetGroundEcho(int echo) { m_groundEcho = echo; };
		//profondeur du fond
		virtual inline double GetGroundDepth() const { return m_groundDepth; };
		virtual inline void SetGroundDepth(double depth) { m_groundDepth = depth; };

		// angle du faisceau
		virtual inline double GetSteeringAngle() const { return m_steeringAngle; };
		virtual inline void SetSteeringAngle(double d) { m_steeringAngle = d; };
		// angle d'ouverture longitudinal � 3dB du faisceau
		virtual inline double GetAthwartAngle() const { return m_athwartAngle; };
		virtual inline void SetAthwartAngle(double d) { m_athwartAngle = d; };
		// angle d'ouverture transversal � 3dB du faisceau
		virtual inline double GetAlongAngle() const { return m_alongAngle; };
		virtual inline void SetAlongAngle(double d) { m_alongAngle = d; };

		// tangage
		virtual inline double GetPitch() { return m_pitch; };
		virtual inline void SetPitch(double d) { m_pitch = d; };

		//liste des groupes d'�chos issus du seuillage initial
		virtual inline std::vector<EchoGroup*>& GetGroupsInitial() { return m_groupsInitial; };
		//liste des groupes d'�chos issus de l'aggr�gation des echos sous le seuil
		virtual inline std::vector<EchoGroup*>& GetGroupsFinal() { return m_groupsFinal; };

		//autorisation de detruire les donn�es
		virtual inline bool GetDeleteData() const { return m_deleteData; };
		virtual inline void SetDeleteData(bool b) { m_deleteData = b; };

		//frequence acoustique
		virtual inline double GetAcousticFrequency() const { return m_acousticFrequency; };
		virtual inline void SetAcousticFrequency(double val) { m_acousticFrequency = val; };

	protected:

		// *********************************************************************
		// M�thodes
		// *********************************************************************

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//num�ro de faisceau
		int m_channelId;
		//identifiant du faisceau
		int m_softwareChannelId;
		//echo du fond
		int m_groundEcho;
		//profondeur du fond
		double m_groundDepth;

		// angle du faisceau
		double m_steeringAngle;
		// angle d'ouverture longitudinal � 3dB du faisceau
		double m_athwartAngle;
		// angle d'ouverture transversal � 3dB du faisceau
		double m_alongAngle;
		// tangage
		double m_pitch;

		//liste des groupes d'�chos issus du seuillage initial
		std::vector<EchoGroup*> m_groupsInitial;

		//liste des groupes d'�chos issus de l'aggr�gation des echos sous le seuil
		std::vector<EchoGroup*> m_groupsFinal;

		//autorisation de detruire les donn�es
		bool m_deleteData;

		//frequence acoustique
		double m_acousticFrequency;
	};
};
