// -*- C++ -*-
// ****************************************************************************
// Class: EchoGroup
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <vector>
#include <memory>
#include "shoalid.h"
#include "BaseMathLib/geometry/Poly2D.h"
#include "ShoalExtraction/geometry/Poly3D.h"

// ***************************************************************************
// Declarations
// ***************************************************************************

using namespace BaseMathLib;

namespace shoalextraction
{
	class EchoGroup
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		EchoGroup(void);

		// Destructeur
		virtual ~EchoGroup(void);

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		// clone simple
		virtual EchoGroup* Clone() const;

		// *********************************************************************
		// Accesseurs
		// *********************************************************************

		//identificateur du banc
		virtual inline std::shared_ptr<ShoalId> GetShoalId() { return m_shoalId.lock(); };
		virtual inline void SetShoalId(std::shared_ptr<ShoalId> id) { m_shoalId = id; };

		// polygone repr�sentant les �chos
		virtual inline Poly2D& GetSurf2D() { return m_surf2D; };

		// polygone repr�sentant les �chos avec les marges de d�tection
		virtual inline Poly2D& GetMarginsSurf2d() { return m_marginsSurf2d; };

		//polygone r�pr�sentant les �chos en 3D
		virtual inline Poly3D& GetVol3D() { return m_vol3D; };

		//liste des id des echos d�tect�s dans le faisceau
		virtual inline void SetEchoMin(std::uint32_t i) { m_echoMin = i; };
        virtual inline void SetEchoMax(std::uint32_t i) { m_echoMax = i; };
		virtual inline std::uint32_t GetEchoMin() { return m_echoMin; };
        virtual inline std::uint32_t GetEchomax() { return m_echoMax; };

		//energie des echos
		virtual inline const std::vector<float>& GetEchosEnergy() const { return m_echosEnergy; };
		virtual inline std::vector<float>& GetEchosEnergy() { return m_echosEnergy; };

		//energie des echos
		std::vector<float> m_echosEnergy;
	protected:

		// *********************************************************************
		// M�thodes
		// *********************************************************************

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//identificateur du banc
		std::weak_ptr<ShoalId> m_shoalId;
		// polygone repr�sentant les �chos
		Poly2D m_surf2D;
		// polygone repr�sentant les �chos avec les marges de d�tection
		Poly2D m_marginsSurf2d;
		//polygone r�pr�sentant les �chos en 3D
		Poly3D m_vol3D;

		std::uint32_t m_echoMin;
		std::uint32_t m_echoMax;
	};
};
