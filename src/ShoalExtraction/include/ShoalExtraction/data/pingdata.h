// -*- C++ -*-
// ****************************************************************************
// Class: PingData
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Société : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <vector>
#include "M3DKernel/datascheme/DateTime.h"
#include "transductdata.h"
#include "ShoalExtraction/geometry/OBoundingBox3D.h"
#include "M3DKernel/datascheme/PingFan.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	typedef struct Navigation {
		double longitude{};
		double latitude{};
		double heading{};
		double pitch{};
		double roll{};
		HacTime time;
		double heave{};
		double surfSpeed{};
		double groundCourse{};
		double depth{};
	} Navigation;

	class PingShoalStat;

	class PingData
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par défaut
		PingData();

		// Destructeur
		virtual ~PingData();

		// *********************************************************************
		// Méthodes
		// *********************************************************************

		// MaJ des id de banc pour tout le ping
		virtual void UpdateShoalId();

		// *********************************************************************	
		// Accesseurs
		// *********************************************************************

		//id
		virtual std::int32_t GetPingId() const { return m_pingId; }
		virtual void SetPingId(const int id) { m_pingId = id; }

		//données du Fan
		virtual PingFan* GetPingFan() const { return m_pingFan; }
		virtual void SetPingFan(PingFan * pFan) { m_pingFan = pFan; }

		//identificateur du sondeur
		virtual std::int32_t GetSounderId() const { return m_sounderId; }
		virtual void SetSounderId(const std::int32_t id) { m_sounderId = id; }

		//BBox réprésentant le volume des donnees en 3D
		virtual OBoundingBox3D& GetBoundingBox() { return m_bbox; }

		//les données du ping sont-elles completes ?
		virtual bool GetComplete() const { return m_complete; }
		virtual void SetComplete(const bool b) { m_complete = b; }

		//données des transducteurs
		virtual void AddTransducer(TransductData* transductData) { m_transducers.push_back(transductData); }
		virtual const std::vector<TransductData*>& GetTransducers() const { return m_transducers; }
		virtual const TransductData* GetTransducer(int transId) const;

		//stat pour le banc
		virtual PingShoalStat* GetShoalStat() const { return m_pShoalStat; }
		virtual void SetShoalStat(PingShoalStat* pData) { m_pShoalStat = pData; }

		//données de navigation
		virtual Navigation& GetNavigation();
		virtual Navigation& GetNavigation(unsigned short channelId);
		virtual const std::map<unsigned short, Navigation>& GetNavigations() const;
		virtual void SetNavigations(const std::map<unsigned short, Navigation>& navigations);

	protected:

		// *********************************************************************
		// Méthodes
		// *********************************************************************

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//id
		std::int32_t m_pingId;

		//données du Fan
		PingFan *m_pingFan;

		//identificateur du sondeur
		std::int32_t m_sounderId;

		//les données du ping sont-elles completes ?
		bool m_complete;

		//données des transducteurs
		std::vector<TransductData*> m_transducers;

		//données de navigation
		std::map<unsigned short, Navigation> m_navigations;

		//stat pour le banc dans le ping
		PingShoalStat* m_pShoalStat;

		//BBox réprésentant le volume des données en 3D
		OBoundingBox3D m_bbox;
	};
};
