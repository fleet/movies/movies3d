// -*- C++ -*-
// ****************************************************************************
// Class: PingShoalStat
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <vector>
#include <algorithm>
#include "BaseMathLib/Vector3.h"
#include "BaseMathLib/geometry/Poly2D.h"
#include "M3DKernel/utils/mathutils.h"
#include "ShoalExtraction/geometry/Poly3D.h"
#include "ShoalExtraction/data/echogroup.h"
#include "ShoalExtraction/data/channeldata.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	// structure contenant les informations g�om�triques d'une composante
	// de banc dans le plan du ping. un banc est compos� d'un ou plusieurs
	// HullItem � chaque ping sur lequel il s'�tend.
	struct HullItem {
		Poly2D surfOrtho; // surface 2D dans le repere orthonorm� (pour repr�sentation 2D)
		Poly2D surfPolar; // surface 2D dans le repere polaire (pour calcul de l'aire)
		Poly2D surfOrtho2; // surface 2D simplifi�e dans le repere orthonorm� (pour repr�sentation 3D)
		double area;	  // aire de la surface 2D
		Poly3D surfHull;  // coordonn�es globales 3D de surfOrtho2 (position par incr�ment, en cartesien)
		Poly3D surfGeo; // OTK - FAE007 - coordonn�es globales 3D de surfOrtho2 (position GPS, en lat std::int32_t depth)
		BaseMathLib::Vector3D hullCenter; // centre de l'enveloppe 3D
		std::set<std::uint64_t> linkedShoal2DComponents; // IDs des surfaces li�es � celle-ci au ping pr�c�dent
		std::set<std::uint64_t> shoal2DComponentIDs;
		bool triangulated;

		HullItem()
		{
			triangulated = false;
		}
	};

	// OTK - 06/11/2009 - structure utilis�e pour le calcul des surfaces des pings du banc
	struct SurroundingEchoGroup
	{
		// groupe d'�chos sup�rieur
		EchoGroup * m_pFirstEchoGroup;
		// groupe d'�chos inf�rieur
		EchoGroup * m_pLastEchoGroup;
		// acc�s aux donn�es du channel (angles, ...)
		ChannelData * m_pChannelData;

		inline double GetMinAngle()
		{
			return m_pChannelData->GetSteeringAngle() - 0.5*m_pChannelData->GetAthwartAngle();
		}

		inline double GetMaxAngle()
		{
			return m_pChannelData->GetSteeringAngle() + 0.5*m_pChannelData->GetAthwartAngle();
		}

		inline double GetMinZ(double beamSampleSpacing) const
		{
			double result = (m_pFirstEchoGroup->GetEchoMin() - 0.5) * beamSampleSpacing;
			return std::max<double>(0.0, result);
		}

		inline double GetMaxZ(double beamSampleSpacing) const
		{
			return (m_pLastEchoGroup->GetEchomax() + 0.5) * beamSampleSpacing;
		}
	};

	// OTK - 06/11/2009 - structure utilis�e pour le calcul des surfaces des pings du banc
	struct SurfaceSegment
	{
		// groupe d'�chos englobant auquel appartient le semgent (couple de points)
		SurroundingEchoGroup * m_pEchoGroup;
		// pr�cise s'il s'agit du bord 1 ou 2 du faisceau (+ ou - angle-3dB)                                                               
		bool m_FirstEdge;

		// acces aux coordonn�es orthonorm�es des points du segment
		inline double GetPointSupX() const
		{
			return m_FirstEdge ? *m_pEchoGroup->m_pFirstEchoGroup->GetSurf2D().GetPointX(0) : *m_pEchoGroup->m_pFirstEchoGroup->GetSurf2D().GetPointX(1);
		}
		inline double GetPointSupY() const
		{
			return m_FirstEdge ? *m_pEchoGroup->m_pFirstEchoGroup->GetSurf2D().GetPointY(0) : *m_pEchoGroup->m_pFirstEchoGroup->GetSurf2D().GetPointY(1);
		}
		inline double GetPointInfX() const
		{
			return m_FirstEdge ? *m_pEchoGroup->m_pLastEchoGroup->GetSurf2D().GetPointX(3) : *m_pEchoGroup->m_pLastEchoGroup->GetSurf2D().GetPointX(2);
		}
		inline double GetPointInfY() const
		{
			return m_FirstEdge ? *m_pEchoGroup->m_pLastEchoGroup->GetSurf2D().GetPointY(3) : *m_pEchoGroup->m_pLastEchoGroup->GetSurf2D().GetPointY(2);
		}

		// acces aux coordonn�es polaires des points du segment
		inline double GetAngle() const
		{
			return m_pEchoGroup->m_pChannelData->GetSteeringAngle()
				+ (m_FirstEdge ? -0.5*m_pEchoGroup->m_pChannelData->GetAthwartAngle() :
					0.5*m_pEchoGroup->m_pChannelData->GetAthwartAngle());
		}
		inline double GetMinZ(double beamSampleSpacing) const
		{
			return m_pEchoGroup->GetMinZ(beamSampleSpacing);
		}
		inline double GetMaxZ(double beamSampleSpacing) const
		{
			return m_pEchoGroup->GetMaxZ(beamSampleSpacing);
		}

		// acces aux coordonn�es ortho d'un point sur le segment
		inline void GetPoint(double z, double sampleSpacing, double& x, double& y)
		{
			double minz = GetMinZ(sampleSpacing);
			double factor = (z - minz) / (GetMaxZ(sampleSpacing) - minz);
			x = GetPointSupX() + (GetPointInfX() - GetPointSupX())*factor;
			y = GetPointSupY() + (GetPointInfY() - GetPointSupY())*factor;
		}

		// pour le tri par la fonction STL sort()
		inline bool operator<(const SurfaceSegment &x) const
		{
			return this->GetAngle() < x.GetAngle();
		}
	};

	struct AngleSegment
	{
		// coordonn�es polaires (discr�tes pour le n� d'�cho)
		double angle;
		std::uint32_t echoMin;
		std::uint32_t echoMax;

		// coordonn�es r�elles
		double x1, y1;
		double x2, y2;

	public:
		void ComputeCoords(double sampleSpacing)
		{
			double echoFirstLimit = std::max<double>(0.0, (double)echoMin - 0.5);
			double echoLastLimit = (double)echoMax + 0.5;
			double sinus = sin(angle);
			double cosinus = cos(angle);
			x1 = echoFirstLimit*sinus*sampleSpacing;
			y1 = echoFirstLimit*cosinus*sampleSpacing;
			x2 = echoLastLimit*sinus*sampleSpacing;
			y2 = echoLastLimit*cosinus*sampleSpacing;
		}
	};

	// paire d'AngleSegment
	struct AngleSegmentPair
	{
		AngleSegment angleSegment1;
		AngleSegment angleSegment2;

		double echoMin;
		double echoMax;

		// pour le tri par la fonction STL sort()
		inline bool operator<(const AngleSegmentPair &x) const
		{
			return this->echoMin < x.echoMin;
		}

		inline void ComputeEchos()
		{
			echoMin = (double)((angleSegment1.echoMin + angleSegment2.echoMin)) / 2.0;
			echoMax = (double)((angleSegment1.echoMax + angleSegment2.echoMax)) / 2.0;
		}
	};

	// structure permettant de faire le lien entre un ensemble de hullItems
	// agr�g�s en un seul pour la repr�sentation 3D du banc
	struct AggregatedHullItem
	{
		// HullItem r�sultant de l'agr�gation des diff�rents hullItems
		HullItem* resultingHullItem;

		// HullItems "de base" agr�g�s
		std::set<HullItem*> hullItems;

		// composantes li�es au ping pr�c�dent
		AggregatedHullItem* prevAggregatedHullItem;

		// segments utilis�s lors de la triangulation
		// des surfaces extrud�es
		std::vector<AngleSegment> segments;

	public:

		AggregatedHullItem()
		{
			prevAggregatedHullItem = NULL;
		}
	};

	// structure contenant les informations n�cessaires
	// au calcul d'une facette par extrusion d'une surface par une autre.
	// cette structure est issue de la jointure de 2 ensembles d'AggregatedHullItem.
	struct ExtrudedComponent
	{
		// premier groupe de AggregatedHullItem
		std::set<AggregatedHullItem*> hullItems1;

		// second groupe de AggregatedHullItem
		std::set<AggregatedHullItem*> hullItems2;
	};

	struct EchoInfo
	{
		BaseMathLib::Vector3D position;
		double energy;
		int channelNb;

		friend bool operator==(const EchoInfo& lhs, const EchoInfo& rhs)
		{
			return lhs.position == rhs.position
				&& MathUtils::floatingPointEquals(lhs.energy, rhs.energy)
				&& lhs.channelNb == rhs.channelNb;
		}

		friend bool operator!=(const EchoInfo& lhs, const EchoInfo& rhs)
		{
			return !(lhs == rhs);
		}
	};

	class PingShoalStat
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		PingShoalStat(void);

		// Destructeur
		virtual ~PingShoalStat(void);

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		// *********************************************************************
		// Accesseurs
		// *********************************************************************

		//profondeur min
		virtual inline double GetMinDepth() const { return m_minDepth; };
		virtual inline void SetMinDepth(double d) { m_minDepth = d; };

		//profondeur max
		virtual inline double GetMaxDepth() const { return m_maxDepth; };
		virtual inline void SetMaxDepth(double d) { m_maxDepth = d; };

		//distance min au fond 
		virtual inline double GetMinBottomDistance() const { return m_minBottomDistance; };
		virtual inline void SetMinBottomDistance(double d) { m_minBottomDistance = d; };

		//distance max au fond 
		virtual inline double GetMaxBottomDistance() const { return m_maxBottomDistance; };
		virtual inline void SetMaxBottomDistance(double d) { m_maxBottomDistance = d; };

		//angle lat�ral min du banc
		virtual inline double GetAcrossAngleMin() const { return m_acrossAngleMin; };
		virtual inline void SetAcrossAngleMin(double d) { m_acrossAngleMin = d; };

		//angle lat�ral max du banc
        virtual inline double GetAcrossAngleMax() const { return m_acrossAngleMax; };
        virtual inline void SetAcrossAngleMax(double d) { m_acrossAngleMax = d; };

		//nombre d'echos
		virtual std::uint32_t GetNbEchos() const { return m_nbEchos; };
		virtual void SetNbEchos(std::uint32_t nbEchos) { m_nbEchos = nbEchos; };

		//liste des surfaces 2D convexes et de l'enveloppe 3D correspondante
		const std::vector<HullItem*>&	GetHullList() const { return m_hullList; };
		std::vector<HullItem*>&	GetHullList() { return m_hullList; };
		HullItem* GetHullItemByID(std::uint64_t id);
		std::set<HullItem*> GetHullItemsByIDs(const std::set<std::uint64_t>& ids);

		//aire totale
		virtual inline double GetArea() const { return m_area; };
		virtual inline void SetArea(double d) { m_area = d; };

		//angle moyen d'ouverture longitudinal
		virtual inline double GetMeanAlongAngle() const { return m_meanAlongAngle; };
		virtual inline void SetMeanAlongAngle(double d) { m_meanAlongAngle = d; };

		//point moyen du ping de l'enveloppe
		virtual inline const BaseMathLib::Vector2D& GetHull2DCenter() const { return m_hull2DCenter; };
		virtual inline BaseMathLib::Vector2D& GetHull2DCenter() { return m_hull2DCenter; };
		virtual inline void SetHull2DCenter(const BaseMathLib::Vector2D& point) { m_hull2DCenter = point; };

		//point moyen du ping de l'enveloppe
		virtual inline const BaseMathLib::Vector3D& GetHullCenter() const { return m_hullCenter; };
		virtual inline BaseMathLib::Vector3D& GetHullCenter() { return m_hullCenter; };
		virtual inline void SetHullCenter(const BaseMathLib::Vector3D& point) { m_hullCenter = point; };

		//point moyen du ping de l'enveloppe en coordonn�es lat std::int32_t GPS
		virtual inline const BaseMathLib::Vector3D& GetHullCenterGPS() const { return m_hullCenterGPS; };
		virtual inline BaseMathLib::Vector3D& GetHullCenterGPS() { return m_hullCenterGPS; };
		virtual inline void SetHullCenterGPS(const BaseMathLib::Vector3D& point) { m_hullCenterGPS = point; };

		//liste des echos
		virtual inline const std::vector<EchoInfo>& GetEchoList() const { return m_echoList; };
		virtual inline std::vector<EchoInfo>& GetEchoList() { return m_echoList; };

	protected:

		// *********************************************************************
		// M�thodes
		// *********************************************************************

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//profondeur min
		double m_minDepth;
		//profondeur max
		double m_maxDepth;

		//distance min au fond 
		double m_minBottomDistance;
		//distance max au fond 
		double m_maxBottomDistance;

		//angle lat�ral min du banc
		double m_acrossAngleMin;
		//angle lat�ral max du banc
		double m_acrossAngleMax;

		//nombre d'echos
		std::uint32_t m_nbEchos;

		//liste des surfaces 2D convexes et de l'enveloppe 3D correspondante
		std::vector<HullItem*>	m_hullList;

		//aire totale
		double m_area;
		//angle moyen d'ouverture longitudinal
		double m_meanAlongAngle;

		//point moyen du ping de l'enveloppe
		BaseMathLib::Vector2D m_hull2DCenter;
		BaseMathLib::Vector3D m_hullCenter;
		BaseMathLib::Vector3D m_hullCenterGPS;

		//liste des echos
		std::vector<EchoInfo> m_echoList;
	};
};
