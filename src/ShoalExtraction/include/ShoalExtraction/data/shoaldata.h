// -*- C++ -*-
// ****************************************************************************
// Class: ShoalData
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <vector>
//#include "pingdata.h"
//#include "shoalid.h"
#include "ShoalExtraction/geometry/BoundingBox3D.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class ShoalStat;
	class ShoalId;
	class PingData;

	class ShoalData
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		ShoalData(void);

		// Destructeur
		virtual ~ShoalData(void);

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//purge
		virtual void PurgePings();

		// ecriture des parametres du banc
		virtual void Serialize(const std::string& folder);
		virtual void SerializeCSV(std::ofstream& ofs);

		// check the shoal contains the given polar point
		virtual bool Contains(int ping, int beam, int echoIdx);

		// *********************************************************************
		// Accesseurs
		// *********************************************************************

		//identificateur du banc
		virtual inline ShoalId* GetShoalId() { return m_shoalId; };
		virtual inline void SetShoalId(ShoalId* id) { m_shoalId = id; };

		//identificateur du sondeur
		virtual inline int GetSounderId() { return m_sounderId; };
		virtual inline void SetSounderId(int id) { m_sounderId = id; };

		//identificateur du sondeur
		virtual inline std::uint32_t GetExternId() { return m_externId; };
		virtual inline void SetExternId(std::uint32_t id) { m_externId = id; };

		//identificateur du transducteur
		virtual inline int GetTransId() const { return m_transId; };
		virtual inline void SetTransId(int id) { m_transId = id; };
		virtual inline const std::string& GetTransName() const { return m_transName; };
		virtual inline void SetTransName(const std::string& name) { m_transName = name; };

		//volume englobant
		virtual inline BoundingBox3D& GetBbox() { return m_bbox; };
		virtual inline const BoundingBox3D& GetBbox() const { return m_bbox; };

		//liste des pings
		virtual inline std::vector<PingData*>& GetPings() { return m_pings; };

		// OTK - facettes du banc
		virtual inline std::vector<std::vector<double>>& GetGPSContourStrips() { return m_GPSContourStrips; };
		virtual inline std::vector<std::vector<double>>& GetContourStrips() { return m_ContourStrips; };
		virtual inline std::vector<std::vector<double>>& GetContourStrips(bool gpsPositionning) { return gpsPositionning ? m_GPSContourStrips : m_ContourStrips; };

		//stat pour le banc
		virtual ShoalStat* GetShoalStat() { return m_pShoalStat; };
		virtual void SetShoalStat(ShoalStat* pData) { m_pShoalStat = pData; };

		//lock de destruction (a usage des containers de ShoalData)
		virtual bool IsLockedDestruction() const { return m_iLockDestruction > 0; };
		virtual void LockDestruction() { m_iLockDestruction++; };
		virtual void UnlockDestruction() { m_iLockDestruction--; };

		//�tat de s�lection
		bool GetSelected() const { return m_bSelected; };
		void SetSelected(bool val) { m_bSelected = val; };

	protected:

		// *********************************************************************
		// M�thodes
		// *********************************************************************

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//id externe du banc
		std::uint32_t m_externId;

		//identificateur du banc
		ShoalId* m_shoalId;
		//identificateur du sondeur
		int m_sounderId;
		//identificateur du transducteur
		int m_transId;
		std::string m_transName;

		//volume englobant
		BoundingBox3D m_bbox;
		//liste des pings
		std::vector<PingData*> m_pings;

		//stat pour le banc
		ShoalStat* m_pShoalStat;

		//lock de destruction
		int m_iLockDestruction;

		//�tat de s�lection
		bool m_bSelected;

		// OTK - facettes du banc
		std::vector<std::vector<double>> m_GPSContourStrips; // coordonn�es GPS
		std::vector<std::vector<double>> m_ContourStrips; // coordonn�es par incr�ment
	};
};
