// -*- C++ -*-
// ****************************************************************************
// Class: ShoalId
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include <cstdint>
#include <vector>
#include <set>
#include <memory>

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class ShoalId : public std::enable_shared_from_this<ShoalId>
	{
	public:
		typedef std::set < std::weak_ptr<ShoalId>, std::owner_less< std::weak_ptr<ShoalId> > > ShoalIdSet;

		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		ShoalId(void);

		// Destructeur
		virtual ~ShoalId(void);

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		void Reset();

		// *********************************************************************
		// Accesseurs
		// *********************************************************************
		//identificateur du banc
        inline std::uint64_t GetShoalId() const { return m_shoalId; };
        inline void SetShoalId(std::uint64_t id) { m_shoalId = id; };

		//le num�ro de banc provient du ping pr�c�dent
		inline bool GetFromPreviousPing() const { return m_fromPreviousPing; }
		inline void SetFromPreviousPing(bool from) { m_fromPreviousPing = from; }

		//le banc est ferm�
		inline bool GetClosed() const { return m_closed; }
		inline void SetClosed(bool closed) { m_closed = closed; }
		inline bool GetClosing() const { return m_closing; }
		inline void SetClosing(bool closing) { m_closing = closing; }

		//lien r�cursif pour lier les echo a un mm banc
		std::shared_ptr<ShoalId> GetLinked2DShoalId();
		void SetLinked2DShoalId(std::shared_ptr<ShoalId> id);
		
		std::shared_ptr<ShoalId> GetLinked3DShoalId();
		void SetLinked3DShoalId(std::shared_ptr<ShoalId> id);

		// OTK - lien pour lier les surfaces 2D entre elles
		inline ShoalIdSet & GetPreviousPing2DShoalIds() { return m_previousPing2DShoalIds; };
		void AddPreviousPing2D(std::weak_ptr<ShoalId> shoalid) { m_previousPing2DShoalIds.insert(shoalid); };

        inline std::set<std::uint64_t>& GetMergedShoalIds() { return m_mergedShoalIds; };
        std::set<std::uint64_t> GetAllLinkedComponentIds();

		//num�ro du premier Ping concernant ce banc
        std::uint64_t GetFirstPing() const { return m_firstPing; };
        void SetFirstPing(std::uint64_t ping) { m_firstPing = ping; };

		//num�ro du premier ESU concernant ce banc
        std::uint64_t GetFirstESU() const { return m_firstESU; };
        void SetFirstESU(std::uint64_t esu) { m_firstESU = esu; };

	protected:

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//invalidation de l'id
		virtual void InvalidShoaldId();

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//identificateur du banc
        std::uint64_t m_shoalId;
		//le num�ro de banc provient du ping pr�c�dent
		bool m_fromPreviousPing;
		//le banc est ferm�
		bool m_closed;
		//le banc est en cours de fermeture
		bool m_closing;

		//lien r�cursif pour lier les echo a un mm banc 2D
		std::weak_ptr<ShoalId> m_linked2DShoalId;

		//lien r�cursif pour lier les echo a un mm banc 3D
		std::weak_ptr<ShoalId> m_linked3DShoalId;

		// OTK - ensemble des linked2DShoalId "en contact" avec ce shoalID
		ShoalIdSet m_previousPing2DShoalIds;
        //std::set<std::uint64_t> m_previousPing2DShoalIds;

		// OTK - ensemble des ID des groupes d'�chos qui ont �t�s concat�n�s � ce groupe
        std::set<std::uint64_t> m_mergedShoalIds;

		//num�ro du premier ESU concernant ce banc
        std::uint64_t m_firstESU;
		//num�ro du premier ping concernant ce banc
        std::uint64_t m_firstPing;
	};
};
