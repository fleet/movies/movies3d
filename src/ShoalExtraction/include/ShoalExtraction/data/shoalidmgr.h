// -*- C++ -*-
// ****************************************************************************
// Class: ShoalIdMgr
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <vector>
#include <map>
#include <memory>

#include "shoalid.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class ShoalIdMgr
	{
	public:

		// *********************************************************************
		// M�thodes
		// *********************************************************************
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		ShoalIdMgr(void);

		// Destructeur
		virtual ~ShoalIdMgr(void);

		//incrementation de l'id
		std::shared_ptr<ShoalId> IncrShoalId();

		//initialisation d'un nouvel ESU
		void InitESU(std::uint64_t esuId);

		//reset du composant
		virtual void Reset();

		//reset des id pour les esu jusqu'� l'esu indiqu�
		virtual void ResetESU(std::int64_t esuId);

		// *********************************************************************
		// Accesseurs
		// *********************************************************************

	protected:

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//liste des identificateurs de bancs
		inline std::map<std::uint64_t, std::vector< std::shared_ptr<ShoalId> > > & GetIdListByESU() { return m_idListByESU; };

		//reset des id du vector
		virtual void ResetIdList(std::vector< std::shared_ptr<ShoalId> > * idList);

		//Id de banc courant
		std::uint64_t GetCurrentId() { return m_currentId; };

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//liste des identificateurs de bancs
		std::map<std::uint64_t, std::vector< std::shared_ptr<ShoalId> > > m_idListByESU;

		//Id de banc courant
		std::uint64_t m_currentId;

		//donn�es de l'ESU courant
		std::uint64_t m_currentESUId;
	};
};
