// -*- C++ -*-
// ****************************************************************************
// Class: ShoalStat
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <vector>
#include "M3DKernel/datascheme/DateTime.h"
#include "BaseMathLib/Vector3.h"
#include "BaseMathLib/geometry/Poly2D.h"
#include "ShoalExtraction/geometry/Poly3D.h"
#include "ShoalExtraction/geometry/OBoundingBox3D.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
class ShoalExtractionParameter;

using namespace BaseMathLib;

namespace shoalextraction
{
	class ShoalStat
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		ShoalStat(void);

		// Destructeur
		virtual ~ShoalStat(void);

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		virtual void ComputeShoalDimensions(double headingShoalAngle,
			double pitchShoalAngle,
			double rollShoalAngle);

		// *********************************************************************
		// Accesseurs
		// *********************************************************************

		//profondeur min
		virtual inline double GetMinDepth() const { return m_minDepth; };
		virtual inline void SetMinDepth(double d) { m_minDepth = d; };

		//profondeur max
		virtual inline double GetMaxDepth() const { return m_maxDepth; };
		virtual inline void SetMaxDepth(double d) { m_maxDepth = d; };

		//distance min au fond 
		virtual inline double GetMinBottomDistance() const { return m_minBottomDistance; };
		virtual inline void SetMinBottomDistance(double d) { m_minBottomDistance = d; };

		//distance max au fond 
		virtual inline double GetMaxBottomDistance() const { return m_maxBottomDistance; };
		virtual inline void SetMaxBottomDistance(double d) { m_maxBottomDistance = d; };

		//angle lat�ral min du banc
		virtual inline double GetAcrossAngleMin() const { return m_acrossAngleMin; };
		virtual inline void SetAcrossAngleMin(double d) { m_acrossAngleMin = d; };

		//angle lat�ral max du banc
        virtual inline double GetAcrossAngleMax() const { return m_acrossAngleMax; };
        virtual inline void SetAcrossAngleMax(double d) { m_acrossAngleMax = d; };

		//nombre d'echos
		virtual std::uint32_t GetNbEchos() const { return m_nbEchos; };
		virtual void SetNbEchos(std::uint32_t nbEchos) { m_nbEchos = nbEchos; };

		//Sv moyen 
		virtual inline double GetMeanSv() const { return m_meanSv; };
		virtual inline void SetMeanSv(double d) { m_meanSv = d; };

		//Sv
		virtual inline double GetSigmaAg() const { return m_SigmaAg; };
		virtual inline void SetSigmaAg(double d) { m_SigmaAg = d; };

		//Sv moyen pond�r�
		virtual inline double GetMeanPondSv() const { return m_meanPondSv; };
		virtual inline void SetMeanPondSv(double d) { m_meanPondSv = d; };

		//volume du banc
		virtual inline double GetVolume() const { return m_volume; };
		virtual inline void SetVolume(double d) { m_volume = d; };

		//date de d�but du banc
		virtual inline const HacTime& GetBeginTime() const { return m_beginTime; };
		virtual inline HacTime& GetBeginTime() { return m_beginTime; };
		virtual inline void SetBeginTime(const HacTime& time) { m_beginTime = time; };

		//date de fin du banc
		virtual inline const HacTime& GetEndTime() const { return m_endTime; };
		virtual inline HacTime& GetEndTime() { return m_endTime; };
		virtual inline void SetEndTime(const HacTime& time) { m_endTime = time; };

		//centre de gravit�
		virtual inline const BaseMathLib::Vector3D& GetGravityCenter() const { return m_gravityCenter; };
		virtual inline void SetGravityCenter(const BaseMathLib::Vector3D& point) { m_gravityCenter = point; };

		//centre g�ographique
		virtual inline const BaseMathLib::Vector3D& GetGeographicCenter() const { return m_geographicCenter; };
		virtual inline void SetGeographicCenter(const BaseMathLib::Vector3D& point) { m_geographicCenter = point; };

		//premier point
		virtual inline const BaseMathLib::Vector3D& GetFirstPoint() const { return m_firstPoint; };
		virtual inline BaseMathLib::Vector3D& GetFirstPoint() { return m_firstPoint; };
		virtual inline void SetFirstPoint(const BaseMathLib::Vector3D& point) { m_firstPoint = point; };

		//dernier point
		virtual inline const BaseMathLib::Vector3D& GetLastPoint() const { return m_lastPoint; };
		virtual inline BaseMathLib::Vector3D& GetLastPoint() { return m_lastPoint; };
		virtual inline void SetLastPoint(const BaseMathLib::Vector3D& point) { m_lastPoint = point; };

		//distances d'agr�gation minimale
		virtual inline double GetVerticalIntegrationDistance() const { return m_verticalIntegrationDistance; };
		virtual inline void SetVerticalIntegrationDistance(double d) { m_verticalIntegrationDistance = d; };

		//angle minimal du transducteur
		virtual inline double GetMinAngle() const { return m_minAngle; };
		virtual inline void SetMinAngle(double d) { m_minAngle = d; };
		//angle maximal du transducteur
		virtual inline double GetMaxAngle() const { return m_maxAngle; };
		virtual inline void SetMaxAngle(double d) { m_maxAngle = d; };

		//boite englobante orient�e
		virtual inline const OBoundingBox3D& GetOBBox() const { return m_oBBox; };
		virtual inline OBoundingBox3D& GetOBBox() { return m_oBBox; };
		virtual inline void SetOBBox(const OBoundingBox3D& box) { m_oBBox = box; };

		//route du navire
		virtual inline double GetShipRoad() const { return m_shipRoad; };
		virtual inline void SetShipRoad(double d) { m_shipRoad = d; };
		//longueur du banc
		virtual inline double GetLength() const { return m_length; };
		virtual inline void SetLength(double d) { m_length = d; };
		//largeur du banc
		virtual inline double GetWidth() const { return m_width; };
		virtual inline void SetWidth(double d) { m_width = d; };
		//hauteur du banc
		virtual inline double GetHeigth() const { return m_heigth; };
		virtual inline void SetHeigth(double d) { m_heigth = d; };

		//param�trage
		virtual inline ShoalExtractionParameter* GetParameter() { return m_pParameter; };
		virtual inline void SetParameter(ShoalExtractionParameter* pParameter) { m_pParameter = pParameter; };

	protected:

		// *********************************************************************
		// M�thodes
		// *********************************************************************


	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//profondeur min
		double m_minDepth;
		//profondeur max
		double m_maxDepth;

		//distance min au fond 
		double m_minBottomDistance;
		//distance max au fond 
		double m_maxBottomDistance;

		//angle lat�ral min du banc
		double m_acrossAngleMin;
		//angle lat�ral max du banc
		double m_acrossAngleMax;

		//nombre d'echos
		std::uint32_t m_nbEchos;

		//Sv moyen 
		double m_meanSv;
		//Sigma_ag
		double m_SigmaAg;
		//Sv moyen pond�r�
		double m_meanPondSv;

		//volume du banc
		double m_volume;

		//date de d�but du banc
		HacTime m_beginTime;
		//date de fin du banc
		HacTime m_endTime;

		//centre de gravit�
		BaseMathLib::Vector3D m_gravityCenter;

		//centre g�ographique
		BaseMathLib::Vector3D m_geographicCenter;

		//premier point
		BaseMathLib::Vector3D m_firstPoint;

		//dernier point
		BaseMathLib::Vector3D m_lastPoint;

		//angle minimal du transducteur
		double m_minAngle;
		//angle maximal du transducteur
		double m_maxAngle;

		//distances d'agr�gation minimale
		double m_verticalIntegrationDistance;

		//boite englobante orient�e
		OBoundingBox3D m_oBBox;
		//route du navire
		double m_shipRoad;
		//longueur du banc
		double m_length;
		//largeur du banc
		double m_width;
		//hauteur du banc
		double m_heigth;

		//param�trage
		ShoalExtractionParameter* m_pParameter;
	};
};
