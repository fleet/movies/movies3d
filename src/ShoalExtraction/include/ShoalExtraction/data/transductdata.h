// -*- C++ -*-
// ****************************************************************************
// Class: TransductData
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <memory>
#include <vector>
#include <string>

#include "BaseMathLib/Vector3.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class BoundingBox3D;
	class ChannelData;
	class Plane3D;

	class TransductData
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par défaut
		TransductData();

		// Destructeur
		virtual ~TransductData();

		// *********************************************************************
		// Méthodes
		// *********************************************************************

		// recherche de la donnée de faisceau correspondant à l'id recherché
		virtual ChannelData* GetChannel(int channelId);

		// MaJ de l'ID de banc en essayant de trouver une agrégation 2D
		virtual void UpdateAllShoalId2D();

		// *********************************************************************
		// Accesseurs
		// *********************************************************************

		//numéro de transducteur
		virtual int GetTransId() const { return m_transId; }
		virtual void SetTransId(const int id) { m_transId = id; }
		virtual const std::string& GetTransName() const { return m_transName; }
		virtual void SetTransName(const std::string& name) { m_transName = name; }

		//BBox réprésentant le volume réel des données en 3D
		virtual const BoundingBox3D& GetVol3DData() const { return *m_vol3DData; }
		virtual void UpdateVol3DData(const BoundingBox3D& boundingBox3D);

		// espacement entre les échos
		virtual double GetBeamsSamplesSpacing() const { return m_beamsSamplesSpacing; }
		virtual void SetBeamsSamplesSpacing(const double d) { m_beamsSamplesSpacing = d; }

		//translation interne
		virtual double GetSampleOffset() const { return m_sampleOffset; }
		virtual void SetSampleOffset(const double d) { m_sampleOffset = d; }

		//liste des données de faisceau
		virtual const std::vector<ChannelData*>& GetChannels() const { return m_channels; }
		virtual void AddChannel(ChannelData* channelData);

		//représentation de l'origine du transducteur
		virtual const BaseMathLib::Vector3D& GetOrigin3D() const { return *m_origin3D; }
        virtual void SetOrigin3D(const BaseMathLib::Vector3D& origin);

		// OTK - FAE007 - représentation 3D des bancs en coordonnées GPS
		//représentation de l'origine du transducteur GPS
		virtual const BaseMathLib::Vector3D GetOrigin3DGPS() const { return *m_origin3DGPS; }
        virtual void SetOrigin3DGPS(const BaseMathLib::Vector3D& origin);

		//représentation du plan du ping
		virtual const Plane3D& GetPlane3D() const { return *m_plane3D; }
		virtual void SetPlane3D(const Plane3D& plane);

		//distance d'agrégation minimale
		virtual double GetVerticalIntegrationDistance() const { return m_verticalIntegrationDistance; }
		virtual void SetVerticalIntegrationDistance(const double d) { m_verticalIntegrationDistance = d; }

		//angle minimal du transducteur
		virtual double GetMinAngle() const { return m_minAngle; }
		virtual void SetMinAngle(const double d) { m_minAngle = d; }
		//angle maximal du transducteur
		virtual double GetMaxAngle() const { return m_maxAngle; }
		virtual void SetMaxAngle(const double d) { m_maxAngle = d; }


		// tangage
		virtual double GetPitch() const { return m_pitch; }
		virtual void SetPitch(const double d) { m_pitch = d; }
		
		//distance d'agrégation minimale dans l'axe X (prise en compte du cap)
		virtual double GetIntegrationDistanceX() const { return m_integrationDistanceX; }
		virtual void SetIntegrationDistanceX(const double integrationDistanceX) { m_integrationDistanceX = integrationDistanceX; }
		//distance d'agrégation minimale dans l'axe Y (prise en compte du cap)
		virtual double GetIntegrationDistanceY() const { return m_integrationDistanceY; }
		virtual void SetIntegrationDistanceY(const double integrationDistanceY) { m_integrationDistanceY = integrationDistanceY; }

		virtual void SetIntegrationDistances(const double integrationDistanceX, const double integrationDistanceY)
		{
			m_integrationDistanceX = integrationDistanceX;
			m_integrationDistanceY = integrationDistanceY;
		}


	protected:

		// *********************************************************************
		// Méthodes
		// *********************************************************************

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//id
		int m_transId;
		std::string m_transName;

		//BBox réprésentant le volume réel des données en 3D
		std::unique_ptr<BoundingBox3D> m_vol3DData;

		//représentation du plan du ping
		std::unique_ptr<Plane3D> m_plane3D;

		//origine du transducteur
		std::unique_ptr<BaseMathLib::Vector3D> m_origin3D;
		std::unique_ptr<BaseMathLib::Vector3D> m_origin3DGPS;

		// espacement entre les échos
		double m_beamsSamplesSpacing;
		//translation interne
		double m_sampleOffset;

		//liste des données de faisceau
		std::vector<ChannelData*> m_channels;

		//distance d'agrégation minimale
		double m_verticalIntegrationDistance;		

		//distance d'agrégation minimale dans l'axe X (prise en compte du cap)
		double m_integrationDistanceX;
		//distance d'agrégation minimale dans l'axe Y (prise en compte du cap)
		double m_integrationDistanceY;

		//angle minimal du transducteur
		double m_minAngle;
		//angle maximal du transducteur
		double m_maxAngle;

		// tangage
		double m_pitch;
	};
};
