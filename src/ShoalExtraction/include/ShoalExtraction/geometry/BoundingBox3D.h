// -*- C++ -*-
// ****************************************************************************
// Class: BoundingBox3D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

#include <vector>
#include <fstream>
#include "BaseMathLib/Vector3.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class BoundingBox3D
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		BoundingBox3D();

		// Destructeur
		virtual ~BoundingBox3D();

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//Reset
		virtual void Reset();

		//Initialise les donn�es membres
		virtual void Init(const BaseMathLib::Vector3D& point);

		//MaJ les donn�es membres
		virtual void Update(double x, double y, double z);
		virtual void Update(const BaseMathLib::Vector3D& point);
		virtual void Update(const std::vector<BaseMathLib::Vector3D>& points);

		//MaJ les donn�es membres
		void Update(const BoundingBox3D& other);

		//test d'intersection
		virtual bool Intersect(const BoundingBox3D& other) const;

		//test de distance
		bool CloseTo(const BoundingBox3D& other, double thresholdX, double thresholdY, double thresholdZ) const;

		//test de validit�
		virtual bool IsValid() const;

		//serialize
		virtual void Serialize(std::ofstream& ofs) const;
		virtual void SerializeSize(std::ofstream& ofs) const;

		// *********************************************************************
		// Variables membres
		// *********************************************************************
		inline double GetDeltaX() const { return m_dXMax - m_dXMin; }
		inline double GetDeltaY() const { return m_dYMax - m_dYMin; }
		inline double GetDeltaZ() const { return m_dZMax - m_dZMin; }

		inline double GetXMin() const { return m_dXMin; }
        inline double GetXMax() const { return m_dXMax; }
		inline double GetYMin() const { return m_dYMin; }
        inline double GetYMax() const { return m_dYMax; }
		inline double GetZMin() const { return m_dZMin; }
        inline double GetZMax() const { return m_dZMax; }

	protected:

		//Init
		void Init(double x, double y, double z);

		//MaJ les donn�es membres (optimis� pour r�duire les tests)
		//obligation d'appeler Init avant le premier appel � UpdateOpti
		virtual void UpdateOpti(double x, double y, double z);
		virtual void UpdateOpti(const BaseMathLib::Vector3D& point);

		//private :

			// *********************************************************************
			// Variables membres
			// *********************************************************************

		double m_dXMin;
		double m_dXMax;
		double m_dYMin;
		double m_dYMax;
		double m_dZMin;
		double m_dZMax;
	};
};
