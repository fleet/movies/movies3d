// -*- C++ -*-
// ****************************************************************************
// Class: IPoly3DIntersection
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <fstream>
#include <vector>
#include "BaseMathLib/Vector3.h"

namespace shoalextraction
{
	class Poly3D;

	class IPoly3DIntersection
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		IPoly3DIntersection() {}

		// Destructeur par d�faut
		virtual ~IPoly3DIntersection() {}

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//initialisation� partir des points 3D
		virtual void Init(const std::vector<BaseMathLib::Vector3D>& points,
			double thresholdX,
			double thresholdY,
			double thresholdZ) = 0;

		//test de distance
		virtual bool CloseTo(IPoly3DIntersection* other, double thresholdX, double thresholdY, double thresholdZ) = 0;

		// *********************************************************************
		// Methods
		// *********************************************************************

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************
	};
};
