#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "ShoalExtraction/geometry/BoundingBox3D.h"
#include "ShoalExtraction/geometry/IPoly3DIntersection.h"
#include "BaseMathLib/Vector3.h"
#include <vector>
#include <fstream>


// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class Line3D
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		Line3D();
		Line3D(const BaseMathLib::Vector3D& origin, const BaseMathLib::Vector3D& direction);

		// Destructeur
		virtual ~Line3D();

		BaseMathLib::Vector3D origin;
		BaseMathLib::Vector3D direction;
	};
};
