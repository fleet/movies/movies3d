// -*- C++ -*-
// ****************************************************************************
// Class: OBoundingBox3D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

#include "ShoalExtraction/geometry/BoundingBox3D.h"
#include "ShoalExtraction/geometry/Plane3D.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class OBoundingBox3D : public BoundingBox3D
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		OBoundingBox3D();

		// Destructeur
		virtual ~OBoundingBox3D();

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//Init
		virtual void Init(const BaseMathLib::Vector3D& point);
		virtual void InitAxis(const BaseMathLib::Vector3D& center,
			double xyAngle,
			double zxAngle,
			double yzAngle);

		// centre de la BBOx en coordonn�es GPS
		virtual void InitCenterGPS(double x, double y, double z);

		//MaJ les donn�es membres
		virtual void Update(const std::vector<BaseMathLib::Vector3D>& points);
		virtual void Update(const BaseMathLib::Vector3D& point);

		//test de distance
		bool CloseTo(const OBoundingBox3D& other, double thresholdX, double thresholdY, double thresholdZ) const;

		//serialize
		virtual void Serialize(std::ofstream& ofs) const;
		virtual void SerializeSize(std::ofstream& ofs) const;

		//Calcul des points
		virtual void ComputePoints();
		virtual void ComputePointsGPS(double points[24]) const;

		inline std::vector<BaseMathLib::Vector3D>& GetPoints() { return m_points; }
		inline const std::vector<BaseMathLib::Vector3D>& GetPoints() const { return m_points; }

	protected:

		// *********************************************************************
		// Accesseurs
		// *********************************************************************	
		inline const Plane3D& GetXY() const { return m_XY; }
		inline const Plane3D& GetZX() const { return m_ZX; }
		inline const Plane3D& GetYZ() const { return m_YZ; }
		inline Plane3D& GetXY() { return m_XY; }
		inline Plane3D& GetZX() { return m_ZX; }
		inline Plane3D& GetYZ() { return m_YZ; }

		inline void SetXY(const Plane3D & plane) { m_XY = plane; }
		inline void SetZX(const Plane3D & plane) { m_ZX = plane; }
		inline void SetYZ(const Plane3D & plane) { m_YZ = plane; }

		inline BaseMathLib::Vector3D& GetCenter() { return m_center; }
		inline const BaseMathLib::Vector3D& GetCenter() const { return m_center; }
		inline const BaseMathLib::Vector3D& GetCenterGPS() const { return m_centerGPS; }

		// *********************************************************************
		// Methodes
		// *********************************************************************	

		//changement de rep�re
		void ApplyBasis(const BaseMathLib::Vector3D& point, double& x, double& y, double& z);

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//Plans de coupe
		Plane3D m_XY;
		Plane3D m_ZX;
		Plane3D m_YZ;

		//Axes norm�s
		BaseMathLib::Vector3D m_X;
		BaseMathLib::Vector3D m_Y;
		BaseMathLib::Vector3D m_Z;

		//centre
		BaseMathLib::Vector3D m_center;
		BaseMathLib::Vector3D m_centerGPS;

		//vecteur de points repr�sentant la oobox
		std::vector<BaseMathLib::Vector3D> m_points;
	};
};
