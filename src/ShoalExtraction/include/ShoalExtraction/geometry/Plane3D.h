#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "ShoalExtraction/geometry/BoundingBox3D.h"
#include "ShoalExtraction/geometry/IPoly3DIntersection.h"
#include "BaseMathLib/Vector3.h"
#include <vector>
#include <fstream>


// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class Plane3D
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		Plane3D();
		Plane3D(const BaseMathLib::Vector3D& p0, const BaseMathLib::Vector3D& p1, const BaseMathLib::Vector3D& p2);

		// Destructeur
		virtual ~Plane3D();

		// *********************************************************************
		// Méthodes
		// *********************************************************************
		double distanceTo(const BaseMathLib::Vector3D& pt) const;

		int side(const BaseMathLib::Vector3D& pt) const;

		// *********************************************************************
		// Variables membres
		// *********************************************************************
		BaseMathLib::Vector3D normal;
		double constant;
	};
};
