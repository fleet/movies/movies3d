// -*- C++ -*-
// ****************************************************************************
// Class: Poly3D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "ShoalExtraction/geometry/BoundingBox3D.h"
#include "ShoalExtraction/geometry/IPoly3DIntersection.h"
#include "BaseMathLib/Vector3.h"
#include <vector>
#include <fstream>


// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class Poly3D
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		Poly3D();

		// Destructeur
		virtual ~Poly3D();

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//Reset
		virtual void Purge();
		virtual void PurgePoints();

		//Allocation de n points 3D
		void Alloc(int nbPoints);

		// Ajustement (r�duction) du nombre de points
		void Shrink(int newNbPoint);

		//ajout d'un point 3D
		inline void AddPoint(const BaseMathLib::Vector3D& p) { m_points.push_back(p); }

		//MaJ BBox
		void UpdateBBox() { m_bbox.Update(GetPoints()); }

		//initialisation de l'intersection
		void InitIntersection(double thresholdX, double thresholdY, double thresholdZ);

		//test de distance
		bool CloseTo(Poly3D& other, double thresholdX, double thresholdY, double thresholdZ);

		//serialize
		virtual void Serialize(std::ofstream& ofs) const;

		// *********************************************************************
		// Accesseurs
		// *********************************************************************
		inline BoundingBox3D& GetBoundingBox() { return m_bbox; }
		inline const BoundingBox3D& GetBoundingBox() const { return m_bbox; }

		inline std::vector<BaseMathLib::Vector3D>& GetPoints() { return m_points; }
		inline const std::vector<BaseMathLib::Vector3D>& GetPoints() const { return m_points; }

		inline IPoly3DIntersection* GetIntersection() { return m_pIntersection; };
		inline void SetIntersection(IPoly3DIntersection* pIPoly3DIntersection) { m_pIntersection = pIPoly3DIntersection; };

		// *********************************************************************
		// Methods
		// *********************************************************************

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************
		std::vector<BaseMathLib::Vector3D> m_points;
		BoundingBox3D m_bbox;
		IPoly3DIntersection* m_pIntersection;
	};
};
