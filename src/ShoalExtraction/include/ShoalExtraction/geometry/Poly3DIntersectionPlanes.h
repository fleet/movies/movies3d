// -*- C++ -*-
// ****************************************************************************
// Class: Poly3DIntersectionPlanes
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "IPoly3DIntersection.h"
#include "ShoalExtraction/geometry/Plane3D.h"

namespace shoalextraction
{
	class Triangle3D;

	class Poly3DIntersectionPlanes : public IPoly3DIntersection
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		Poly3DIntersectionPlanes();

		// Destructeur
		virtual ~Poly3DIntersectionPlanes();

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//ajout d'un point 3D
		virtual inline void Init(const std::vector<BaseMathLib::Vector3D>& points,
			double thresholdX,
			double thresholdY,
			double thresholdZ);

		//test de distance
		virtual bool CloseTo(IPoly3DIntersection* other, double thresholdX, double thresholdY, double thresholdZ);

		// *********************************************************************
		// Methods
		// *********************************************************************
	protected:

		virtual inline const std::vector<BaseMathLib::Vector3D> & GetPointsRef() { return m_pointsRef; }
		virtual inline std::vector<Plane3D>& GetPlanes() { return m_planes; }

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//conservation de la r�f�rence de la liste des points 
		std::vector<BaseMathLib::Vector3D> m_pointsRef;

		//liste des Planes pour intersection
		std::vector<Plane3D> m_planes;

	};
};
