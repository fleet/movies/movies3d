// -*- C++ -*-
// ****************************************************************************
// Class: Poly3DIntersectionTriangles
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "ShoalExtraction/geometry/IPoly3DIntersection.h"
#include "ShoalExtraction/geometry/Triangle3D.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class Triangle3D;

	class Poly3DIntersectionTriangles : public IPoly3DIntersection
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		Poly3DIntersectionTriangles();

		// Destructeur
		virtual ~Poly3DIntersectionTriangles();

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//ajout d'un point 3D
		virtual inline void Init(const std::vector<BaseMathLib::Vector3D>& points,
			double thresholdX,
			double thresholdY,
			double thresholdZ);

		//test de distance
		virtual bool CloseTo(IPoly3DIntersection* other, double thresholdX, double thresholdY, double thresholdZ);

		// *********************************************************************
		// Methods
		// *********************************************************************
	protected:

		virtual inline const std::vector<BaseMathLib::Vector3D>& GetPointsRef() { return m_pointsRef; }
		virtual inline std::vector<Triangle3D>& GetTriangles() { return m_triangles; }

	private:

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		//conservation de la r�f�rence de la liste des points 
		std::vector<BaseMathLib::Vector3D> m_pointsRef;

		//liste des triangles pour intersection
		std::vector<Triangle3D> m_triangles;

	};
};
