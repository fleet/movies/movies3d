#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "ShoalExtraction/geometry/BoundingBox3D.h"
#include "ShoalExtraction/geometry/IPoly3DIntersection.h"
#include "BaseMathLib/Vector3.h"
#include <vector>
#include <fstream>


// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class Segment3D
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		Segment3D();
		Segment3D(const BaseMathLib::Vector3D& p0, const BaseMathLib::Vector3D& p1);

		// Destructeur
		virtual ~Segment3D();

		// *********************************************************************
		// Variables membres
		// *********************************************************************
		BaseMathLib::Vector3D origin;
		BaseMathLib::Vector3D direction;
		double extent;
	};
};
