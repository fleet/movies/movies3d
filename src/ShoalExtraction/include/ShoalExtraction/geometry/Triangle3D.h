// -*- C++ -*-
// ****************************************************************************
// Class: Triangle3D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

#include <vector>
#include "ShoalExtraction/geometry/BoundingBox3D.h"

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	class Triangle3D
	{
	public:
		// *********************************************************************
		// Constructeurs / Destructeur
		// *********************************************************************
		// Constructeur par d�faut
		Triangle3D();
		Triangle3D(const BaseMathLib::Vector3D & point1, const BaseMathLib::Vector3D& point2, const BaseMathLib::Vector3D& point3);

		// Destructeur
		virtual ~Triangle3D();

		// *********************************************************************
		// M�thodes
		// *********************************************************************

		//test d'intersection
		virtual bool Intersect(const Triangle3D& other) const;

		//test de distance
		virtual bool CloseTo(const Triangle3D& other, double thresholdX, double thresholdY, double thresholdZ) const;

		// *********************************************************************
		// Variables membres
		// *********************************************************************

		BoundingBox3D bbox;
		BaseMathLib::Vector3D edges[3];
	};
};
