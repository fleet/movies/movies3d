// -*- C++ -*-
// ****************************************************************************
// Class: GeometryTools2D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************
#pragma once

#include "ShoalExtraction/geometry/Line3D.h"
#include "ShoalExtraction/geometry/Plane3D.h"
#include "ShoalExtraction/geometry/Poly3D.h"
#include "ShoalExtraction/geometry/Segment3D.h"
#include "ShoalExtraction/geometry/Triangle3D.h"

#include <vector>
#include "BaseMathLib/Vector3.h"

namespace shoalextraction {

	class GeometryTools3D {

	public:

		/**
		  * CloseTo
		  *
		  * Returns true if any of the points in points1 is closer than threshold from any points of points2:
		  *
		  * @param std::vector<Wm4::Vector3d*>* points1
		  * @param std::vector<Wm4::Vector3d*>* points2
		  * @param threshold_x double
		  * @param threshold_y double
		  * @param threshold_z double
		  * @return bool
		  */
		static bool CloseTo(const std::vector<BaseMathLib::Vector3D>& points1,
			const std::vector<BaseMathLib::Vector3D>& points2,
			double threshold_x,
			double threshold_y,
			double threshold_z);

		/**
		  * CloseTo
		  *
		  * Returns true if any of the triangles in triangles1 is closer than threshold from any triangles of triangles2:
		  *
		  * @param std::vector<Wm4::Triangle3d*>* triangles1
		  * @param std::vector<Wm4::Triangle3d*>* triangles2
		  * @param threshold_x double
		  * @param threshold_y double
		  * @param threshold_z double
		  * @return bool
		  */
		static bool CloseTo(const std::vector<Triangle3D>& triangles1,
			const std::vector<Triangle3D>& triangles2,
			double threshold_x,
			double threshold_y,
			double threshold_z);

		/**
		  * CloseTo
		  *
		  * Returns true if any of the points is in the poly enclosed by the planes:
		  *
		  * @param std::vector<Wm4::Vector3d*>* points
		  * @param std::vector<Wm4::Plane3d*>* planes
		  * @return bool
		  */
		static bool CloseTo(const std::vector<BaseMathLib::Vector3D>& points,
			const std::vector<Plane3D>& planes);

		/**
		  * Offset
		  *
		  * Offset the plane by the X, Y and Z values
		  *
		  * @param threshold_x double
		  * @param threshold_y double
		  * @param threshold_z double
		  * @param Wm4::Plane3d* plane
		  * @return bool
		  */
		static void Offset(double threshold_x,
			double threshold_y,
			double threshold_z,
			Plane3D& plane);

		/**
		  * CloseTo
		  *
		  * Returns true the point is on the negative side of the plane
		  *
		  * @param Wm4::Vector3d* point
		  * @param Wm4::Plane3d* plane
		  * @return bool
		  */
		static inline bool CloseTo(const BaseMathLib::Vector3D& point,
			const Plane3D& plane);


		static bool intersection(const Line3D& line, const Plane3D& plane, double& lineT);

		static bool intersection(const Segment3D& segment, const Plane3D& plane, double& lineT);

		static void GenerateComplementBasis(BaseMathLib::Vector3D& u, BaseMathLib::Vector3D& v, BaseMathLib::Vector3D& w);

		static double squareDistance(const BaseMathLib::Vector3D& pt, const Triangle3D& tri, BaseMathLib::Vector3D& p0, BaseMathLib::Vector3D& p1);

		static double squareDistance(const Line3D& line, const Segment3D& seg, BaseMathLib::Vector3D& p0, BaseMathLib::Vector3D& p1, double& lineParameter);

		static double squareDistance(const Line3D& line, const Triangle3D& tri, BaseMathLib::Vector3D& p0, BaseMathLib::Vector3D& p1, double& lineParameter);

		static double squareDistance(const Segment3D& seg, const Triangle3D& tri, BaseMathLib::Vector3D& p0, BaseMathLib::Vector3D& p1);

		static double squareDistance(const Triangle3D& tri0, const Triangle3D& tri1, BaseMathLib::Vector3D& p0, BaseMathLib::Vector3D& p1);

	};
};
