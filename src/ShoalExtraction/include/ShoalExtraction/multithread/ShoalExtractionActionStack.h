// -*- C++ -*-
// ****************************************************************************
// Class: ShoalExtractionActionStack
//
// Description: action stack for shoalextraction
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Sept 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <list>
#include "M3DKernel/utils/multithread/Event.h"
#include "M3DKernel/utils/multithread/RecursiveMutex.h"
#include "M3DKernel/utils/multithread/stack//ActionStack.h"
#include <cstdint>

class ShoalExtractionModule;
class PingFan;

// ***************************************************************************
// Declarations
// ***************************************************************************
typedef void(/*__stdcall*/ *DELAYINFO_CALLBACK)(std::uint32_t, std::uint32_t, std::uint32_t);

namespace shoalextraction
{
	class ShoalExtractionActionStack : public ActionStack
	{
	public:
		// *********************************************************************
		// Constructors / Destructor
		// *********************************************************************
		// d�fault constructor
		ShoalExtractionActionStack(ShoalExtractionModule* pModule);

		// Destructor
		virtual ~ShoalExtractionActionStack(void);

		// *********************************************************************
		// Accessors
		// *********************************************************************

		ShoalExtractionModule* GetModule() { return m_pModule; };

		// *********************************************************************
		// Methods
		// *********************************************************************

		//add action to process
		void ScheduleAddPingFan(PingFan* pFan, std::uint32_t pingFanCount,
			DELAYINFO_CALLBACK delayInfoCB);
		void ScheduleESUEnd();
		void ScheduleESUStart();
		void ScheduleIncESUId();

	protected:


	private:

		ShoalExtractionModule* m_pModule;
	};
};
