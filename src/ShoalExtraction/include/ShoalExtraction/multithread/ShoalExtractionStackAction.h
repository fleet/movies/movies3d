// -*- C++ -*-
// ****************************************************************************
// Class: ShoalExtractionStackAction
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Sept 2009
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "M3DKernel/utils/multithread/stack/StackAction.h"
#include "M3DKernel/utils/multithread/Event.h"
#include "ShoalExtraction/ShoalExtractionModule.h"

class PingFan;

// ***************************************************************************
// Declarations
// ***************************************************************************
namespace shoalextraction
{
	enum EExtractionAction
	{
		E_ADDPINGFAN_ACTION,
		E_ESUEND_ACTION,
		E_ESUSTART_ACTION,
		E_INCESUID_ACTION
	};

	class ShoalExtractionStackAction : public StackAction
	{
	public:
		// *********************************************************************
		// Constructors / Destructor
		// *********************************************************************
		// d�fault constructor
		ShoalExtractionStackAction();

		// Destructor
		virtual ~ShoalExtractionStackAction(void);

		// *********************************************************************
		// Accessors
		// *********************************************************************
		// *********************************************************************
		// Methods
		// *********************************************************************

		//process
		virtual void Process();

		// *********************************************************************
		// members
		// *********************************************************************

		EExtractionAction actionType;
		PingFan* pFan;
		ShoalExtractionModule* pModule;
		// pour information du retard � l'IHM
		std::uint32_t pingFanCount;
		DELAYINFO_CALLBACK delayInfoCB;
	};
};
