// -*- C++ -*-
// ****************************************************************************
// Class: ShoalExtractionGeometry
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include <assert.h>
#include <float.h>

#include "BaseMathLib/RotationMatrix.h"
#include "M3DKernel/datascheme/NavAttributes.h"
#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/M3DKernel.h"

#include "BaseMathLib/geometry/GeometryTools2D.h"
#include "ShoalExtraction/geometry/geometrytools3d.h"
#include "ShoalExtraction/ShoalExtractionGeometry.h"

#include "ShoalExtraction/data/channeldata.h"
#include "ShoalExtraction/data/echogroup.h"
#include "ShoalExtraction/data/shoalidmgr.h"

#ifdef _TRACE_PERF
//#define _GTOOLS_TRACE_PERF

#ifdef _GTOOLS_TRACE_PERF
extern std::ofstream trace_ofs;
#endif
#endif

using namespace shoalextraction;
using namespace BaseMathLib;
using namespace std;

//*****************************************************************************
// Name : CreateEchoSurf2D
// Description :
// Parameters : * (in)  PingFan *p
//				* (out)	EchoGroup* pEchoGroup = groupe d'�chos dont on calcule la surface 2D orthonorm�e
// Return : void
//*****************************************************************************
void ShoalExtractionGeometry::CreateEchoSurf2D(double		dl_beamsSamplesSpacing,
	double		xIntegrationDistance,
	double		verticalIntegrationDistance,
	double		dl_angleSteering,
	double		dl_angle3dB,
	bool		computeMargins,
	EchoGroup* pEchoGroup)
{
	if (pEchoGroup != NULL)
	{
		//*****************************************************************************
		//calcul des 4 points du quadrilatere englobant le groupe d'�chos
		//*****************************************************************************
		pEchoGroup->GetSurf2D().Alloc(4);

		double sin1 = sin(dl_angleSteering - dl_angle3dB);
		double sin2 = sin(dl_angleSteering + dl_angle3dB);
		double cos1 = cos(dl_angleSteering - dl_angle3dB);
		double cos2 = cos(dl_angleSteering + dl_angle3dB);

		//premier echo
        double dEchoFirstLimit = std::max(0.0, (double)pEchoGroup->GetEchoMin() - 0.5);
		//Haut-gauche
		pEchoGroup->GetSurf2D().GetPoints()[0] = dEchoFirstLimit*dl_beamsSamplesSpacing*sin1;
		pEchoGroup->GetSurf2D().GetPoints()[1] = dEchoFirstLimit*dl_beamsSamplesSpacing*cos1;
		//Haut-droit
		pEchoGroup->GetSurf2D().GetPoints()[2] = dEchoFirstLimit*dl_beamsSamplesSpacing*sin2;
		pEchoGroup->GetSurf2D().GetPoints()[3] = dEchoFirstLimit*dl_beamsSamplesSpacing*cos2;

		//dernier echo
        double dEchoLastLimit = (double)pEchoGroup->GetEchomax() + 0.5;
		//bas-droit
		pEchoGroup->GetSurf2D().GetPoints()[4] = dEchoLastLimit*dl_beamsSamplesSpacing*sin2;
		pEchoGroup->GetSurf2D().GetPoints()[5] = dEchoLastLimit*dl_beamsSamplesSpacing*cos2;
		//bas-gauche
		pEchoGroup->GetSurf2D().GetPoints()[6] = dEchoLastLimit*dl_beamsSamplesSpacing*sin1;
		pEchoGroup->GetSurf2D().GetPoints()[7] = dEchoLastLimit*dl_beamsSamplesSpacing*cos1;

		//MaJ BBox
		pEchoGroup->GetSurf2D().UpdateBBox();

		if (computeMargins)
		{
			//*****************************************************************************
			//calcul du polygone englobant le groupe d'�chos avec marge de d�tection
			//*****************************************************************************
			double * marginPoints = pEchoGroup->GetMarginsSurf2d().GetPoints();
			int size = pEchoGroup->GetMarginsSurf2d().GetNbPoints();

			GeometryTools2D::AddMargins(
				pEchoGroup->GetSurf2D().GetPoints(), pEchoGroup->GetSurf2D().GetNbPoints(),
				xIntegrationDistance, verticalIntegrationDistance,
				&marginPoints, size);

			//MaJ du nb de points calcul�s
			pEchoGroup->GetMarginsSurf2d().SetPoints(marginPoints, size);

			//MaJ BBox
			pEchoGroup->GetMarginsSurf2d().UpdateBBox();
		}
	}
}


//*****************************************************************************
// Name : ComputeVolumes
// Description : Calcul du volume 3D d'un groupe d'echos
// Parameters : * (in)  PingFan *pFan			: PingFan
//				* (in)  Sounder *pSounder		: Sondeur
//				* (in)  int iTrans				: indice du transducteur
//				* (in)  int iChannel			: indice du faisceau
//				* (in)  double cosAlong			: cosinus de l'angle longitudinal � 3dB
//				* (in)  double tanAlong			: tangente de l'angle transversal � 3dB
//				* (in)  double cosAthwart		: cosinus de l'angle longitudinal � 3dB
//				* (in)  double tanAthwart		: tangente de l'angle transversal � 3dB
//				* (in)  double beamsSamplesSpacing : espacement entre deux echos
//				* (in)  double transTranslation : translation du transducteur
//				* (int/out)	EchoGroup* pEchoGroup = groupe d'�chos dont on calcule le volume
// Return : void
//*****************************************************************************
void ShoalExtractionGeometry::ComputeVolumes(PingFan * pFan, Sounder *pSounder,
	int transId, int channelId,
	double cosAlong, double tanAlong,
	double cosAthwart, double tanAthwart,
	double beamsSamplesSpacing,
	double transTranslation,
	EchoGroup* pEchoGroup)
{
	//premier echo
	double dFirstEcho = (double)pEchoGroup->GetEchoMin() - 0.5;
    dFirstEcho = std::max(0.0, dFirstEcho + transTranslation);

	double dFirstEchoDepth = dFirstEcho * beamsSamplesSpacing;

	//compensation de la profondeur par les angles d'ouverture (cf.Sounder.cpp ::GetAxisAlignedBoundingBox)
	//pour tronquer la partie sup�rieure du volume r�el
	dFirstEchoDepth /= cosAlong * cosAthwart;

	//dernier echo
    double dLastEcho = (double)pEchoGroup->GetEchomax() + 0.5;
	double dLastEchoDepth = (dLastEcho + transTranslation) * beamsSamplesSpacing;

	//compensation de la profondeur par les angles d'ouverture (cf.Sounder.cpp ::GetAxisAlignedBoundingBox)
	//pour englober la partie inf�rieure du volume r�el
	dLastEchoDepth /= cosAlong * cosAthwart;

	//optimisation allocation m�moire
	pEchoGroup->GetVol3D().Alloc(8);

	Vector3D tmpV3D;
	auto iterPoints = pEchoGroup->GetVol3D().GetPoints().begin();

	//calcul des coordonn�es polaires dans le rep�re du faisceau et transformation vers le repere monde
	//d'abord la face 'back'

	//-X	+Y	FirstEcho
	tmpV3D.x = dFirstEchoDepth * (-tanAlong);
	tmpV3D.y = dFirstEchoDepth * tanAthwart;
	tmpV3D.z = dFirstEchoDepth;
	*iterPoints = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, channelId, tmpV3D),
	iterPoints++;

	//-X	-Y	FirstEcho
	tmpV3D.y = -tmpV3D.y;
	*iterPoints = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, channelId, tmpV3D);
	iterPoints++;

	//-X	-Y	LastEcho
	tmpV3D.x = dLastEchoDepth * (-tanAlong);
	tmpV3D.y = dLastEchoDepth * (-tanAthwart);
	tmpV3D.z = dLastEchoDepth;
	*iterPoints = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, channelId, tmpV3D);
	iterPoints++;

	//-X	+Y	LastEcho
	tmpV3D.y = -tmpV3D.y;
	*iterPoints = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, channelId, tmpV3D);
	iterPoints++;

	//ensuite la face 'front'

	//+X	+Y	FirstEcho
	tmpV3D.x = dFirstEchoDepth * tanAlong;
	tmpV3D.y = dFirstEchoDepth * tanAthwart;
	tmpV3D.z = dFirstEchoDepth;
	*iterPoints = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, channelId, tmpV3D);
	iterPoints++;

	//+X	-Y	FirstEcho
	tmpV3D.y = -tmpV3D.y;
	*iterPoints = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, channelId, tmpV3D);
	iterPoints++;

	//+X	-Y	LastEcho
	tmpV3D.x = dLastEchoDepth * tanAlong;
	tmpV3D.y = dLastEchoDepth * (-tanAthwart);
	tmpV3D.z = dLastEchoDepth;
	*iterPoints = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, channelId, tmpV3D);
	iterPoints++;

	//+X	+Y	LastEcho
	tmpV3D.y = -tmpV3D.y;
	*iterPoints = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, channelId, tmpV3D);

	//MaJ BBox
	pEchoGroup->GetVol3D().UpdateBBox();
}

//*****************************************************************************
// Name : ComputeVolumes
// Description :
// Parameters : * (in)  PingFan *p
//				* (out)	EchoGroup* pEchoGroup = groupe d'�chos dont on calcule le volume
// Return : void
//*****************************************************************************
void ShoalExtractionGeometry::ComputeVolumes(PingData& pingData)
{
	PingFan * pFan = pingData.GetPingFan();
	Sounder *pSounder = pFan->m_pSounder;
	MemorySet *pMemSet = pFan->GetMemorySetRef();

	// Pour chaque transducteur
	for (const auto& pTransData : pingData.GetTransducers())
	{
		const auto& pTrans = pSounder->GetTransducer(pTransData->GetTransId());

		int transId = pTransData->GetTransId();
		double  beamsSamplesSpacing = pTrans->getBeamsSamplesSpacing();
		double  transTranslation = pTrans->GetSampleOffset();

		//determiner le plan 3D du transducteur
		//prendre un point sur un faisceau
		Vector3D channelVect = pTrans->GetTransducerRotation() * Vector3D(0, 0, 1);

		// Les donn�es de navigation sont a priori les m�mes pour chaque canal du transducer : on prend le premier
		auto softChannelId = pTrans->getSoftChannelPolarX(0)->getSoftwareChannelId();
		//prendre un vecteur perpendiculaire � la direction du navire
		//(l'origine de l'angle de direction est l'axe X, donc l'angle est deja perpendiculaire au repere orthonorm� (axe Y))
		double perpHeadingRad = pingData.GetNavigation(softChannelId).heading;
		//on prend les points de facon a orienter la normale du plan vers Y > 0
		//Attention, Z est orient� vers le bas, donc le sens de Y est modifi�
		Vector3D perpHeadingVect(sin(perpHeadingRad), -cos(perpHeadingRad), 0);

		Vector3D origin = pTransData->GetOrigin3D();
		perpHeadingVect = perpHeadingVect + origin;
		channelVect = channelVect + origin;
        //auto tmp = Plane3D(pTransData->GetOrigin3D(), channelVect,perpHeadingVect);
//		pTransData->SetPlane3D(Wm4::Plane3d(pTransData->GetOrigin3D(),
//			Wm4::Vector3d(channelVect.x, channelVect.y, channelVect.z),
//			Wm4::Vector3d(perpHeadingVect.x, perpHeadingVect.y, perpHeadingVect.z)));

		double maxGroundDepth = 0;

		//pour chaque faisceau
		for (size_t iChannel = 0; iChannel < pTransData->GetChannels().size(); iChannel++)
		{
			ChannelData* pChannelData = pTransData->GetChannels()[iChannel];

			//MaJ des angles du transducteur
			pTransData->SetMinAngle(min(pTransData->GetMinAngle(), pChannelData->GetSteeringAngle() - pChannelData->GetAthwartAngle() * 0.5));
            pTransData->SetMaxAngle(std::max(pTransData->GetMaxAngle(), pChannelData->GetSteeringAngle() + pChannelData->GetAthwartAngle() * 0.5));

			// across angles
			double athwartShipAngleRad = pChannelData->GetAthwartAngle() * 0.5;
			double cosAthwart = cos(athwartShipAngleRad);
			double tanAthwart = tan(athwartShipAngleRad);

			// along angles
			double alongShipAngleRad = pChannelData->GetAlongAngle() * 0.5;
			double cosAlong = cos(alongShipAngleRad);
			double tanAlong = tan(alongShipAngleRad);

			//calcul du fond pour le faisceau
			std::uint32_t groundEcho = 0;

			pChannelData->SetGroundDepth(origin.z +
				ShoalExtractionGeometry::ComputeGround(&pingData, pTrans, pChannelData->GetChannelId(), groundEcho));
			pChannelData->SetGroundEcho(groundEcho);

            maxGroundDepth = std::max(maxGroundDepth, pChannelData->GetGroundDepth());

			//pour chaque groupe d'�chos
			size_t groupsSize = pChannelData->GetGroupsInitial().size();
			for (size_t iEchoGroup = 0; iEchoGroup < groupsSize; iEchoGroup++)
			{
				EchoGroup* pEchogroup = pChannelData->GetGroupsInitial()[iEchoGroup];
				//calcul du volume du groupe d'�chos
				ComputeVolumes(pFan, pSounder, pTransData->GetTransId(), pChannelData->GetChannelId(),
					cosAlong, tanAlong, cosAthwart, tanAthwart,
					beamsSamplesSpacing, transTranslation, pEchogroup);
			}
			if (groupsSize > 0)
			{
				//Maj de la BBox des donn�es du transducteur
				pTransData->UpdateVol3DData(pChannelData->GetGroupsInitial()[0]->GetVol3D().GetBoundingBox());
                //	assert(pChannelData->GetGroundDepth() >= pChannelData->GetGroupsInitial()[0]->GetVol3D().GetBoundingBox().GetZmax());
			}
			if (groupsSize > 1)
			{
				//Maj de la BBox des donn�es du transducteur
				pTransData->UpdateVol3DData(pChannelData->GetGroupsInitial()[groupsSize - 1]->GetVol3D().GetBoundingBox());
                //	assert(pChannelData->GetGroundDepth() >= pChannelData->GetGroupsInitial()[groupsSize-1]->GetVol3D().GetBoundingBox().GetZmax());
			}
		}

		//*****************************************************************************
		// Calcul du volume th�orique du transducteur pour obtenir le volume th�orique du ping
		//*****************************************************************************

		if (pTransData->GetChannels().size() > 0)
		{
			//premier echo
			ChannelData* firstChannel = pTransData->GetChannels()[0];
			double dFirstEchoDepth = transTranslation;
			int firstChannelId = firstChannel->GetChannelId();

			//dernier echo
			ChannelData* lastChannel = pTransData->GetChannels()[pTransData->GetChannels().size() - 1];
			double dLastEchoDepth = maxGroundDepth;
			int lastChannelId = lastChannel->GetChannelId();

			// across angles
			double athwartShipAngleRadStart = pTrans->getSoftChannelPolarX(firstChannelId)->m_beam3dBWidthAthwartRad * 0.5;
			double cosAthwartStart = cos(athwartShipAngleRadStart);
			double tanAthwartStart = tan(athwartShipAngleRadStart);
			double athwartShipAngleRadEnd = pTrans->getSoftChannelPolarX(lastChannelId)->m_beam3dBWidthAthwartRad * 0.5;
			double cosAthwartEnd = cos(athwartShipAngleRadEnd);
			double tanAthwartEnd = tan(athwartShipAngleRadEnd);

			// along angles
			double alongShipAngleRadStart = pTrans->getSoftChannelPolarX(firstChannelId)->m_beam3dBWidthAlongRad * 0.5;
			double cosAlongStart = cos(alongShipAngleRadStart);
			double tanAlongStart = tan(alongShipAngleRadStart);
			double alongShipAngleRadEnd = pTrans->getSoftChannelPolarX(lastChannelId)->m_beam3dBWidthAlongRad * 0.5;
			double cosAlongEnd = cos(alongShipAngleRadEnd);
			double tanAlongEnd = tan(alongShipAngleRadEnd);

			//compensation de la profondeur par les angles d'ouverture (cf.Sounder.cpp ::GetAxisAlignedBoundingBox)
			//pour tronquer la partie sup�rieure et englober la partie inf�rieure du volume r�el
			dFirstEchoDepth /= cosAlongStart * cosAthwartStart;
			dLastEchoDepth /= cosAlongEnd * cosAthwartEnd;

			//calcul de la oboundingbox3d du ping
			Vector3D tmpV3D;
			BaseMathLib::Vector3D tmpWm4V3D;
			//la obbox du ping est orient�e suivant le cap du navire uniquement
			pingData.GetBoundingBox().InitAxis(pTransData->GetOrigin3D(),
				pFan->GetNavAttributesRef(softChannelId)->m_headingRad, 0, 0);

			//calcul des coordonn�es polaires dans le rep�re du faisceau et transformation vers le repere monde
			//d'abord la face 'Back'

			// -X	+Y (last channel)	FirstEcho
			tmpV3D.x = dFirstEchoDepth * (-tanAlongEnd);
			tmpV3D.y = dFirstEchoDepth * tanAthwartEnd;
			tmpV3D.z = dFirstEchoDepth;
			tmpWm4V3D = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, lastChannelId, tmpV3D);
			pingData.GetBoundingBox().Update(tmpWm4V3D);

			// -X	-Y (first channel)	FirstEcho
			tmpV3D.x = dFirstEchoDepth * (-tanAlongStart);
			tmpV3D.y = dFirstEchoDepth * tanAthwartStart;
			tmpV3D.z = dFirstEchoDepth;
			tmpWm4V3D = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, firstChannelId, tmpV3D);
			pingData.GetBoundingBox().Update(tmpWm4V3D);

			// -X	-Y (first channel)	LastEcho
			tmpV3D.x = dLastEchoDepth * (-tanAlongStart);
			tmpV3D.y = dLastEchoDepth * tanAthwartStart;
			tmpV3D.z = dLastEchoDepth;
			tmpWm4V3D = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, firstChannelId, tmpV3D);
			pingData.GetBoundingBox().Update(tmpWm4V3D);

			// -X	+Y (last channel)	LastEcho
			tmpV3D.x = dLastEchoDepth * (-tanAlongEnd);
			tmpV3D.y = dLastEchoDepth * tanAthwartEnd;
			tmpV3D.z = dLastEchoDepth;
			tmpWm4V3D = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, lastChannelId, tmpV3D);
			pingData.GetBoundingBox().Update(tmpWm4V3D);

			//ensuite la face 'Front'
			// +X	+Y (last channel)	FirstEcho
			tmpV3D.x = dFirstEchoDepth * (tanAlongEnd);
			tmpV3D.y = dFirstEchoDepth * tanAthwartEnd;
			tmpV3D.z = dFirstEchoDepth;
			tmpWm4V3D = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, lastChannelId, tmpV3D);
			pingData.GetBoundingBox().Update(tmpWm4V3D);

			// +X	-Y (first channel)	FirstEcho
			tmpV3D.x = dFirstEchoDepth * (tanAlongStart);
			tmpV3D.y = dFirstEchoDepth * tanAthwartStart;
			tmpV3D.z = dFirstEchoDepth;
			tmpWm4V3D = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, firstChannelId, tmpV3D);
			pingData.GetBoundingBox().Update(tmpWm4V3D);

			// +X	-Y (first channel)	LastEcho
			tmpV3D.x = dLastEchoDepth * (tanAlongStart);
			tmpV3D.y = dLastEchoDepth * tanAthwartStart;
			tmpV3D.z = dLastEchoDepth;
			tmpWm4V3D = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, firstChannelId, tmpV3D);
			pingData.GetBoundingBox().Update(tmpWm4V3D);

			// +X	+Y (last channel)	LastEcho
			tmpV3D.x = dLastEchoDepth * (tanAlongEnd);
			tmpV3D.y = dLastEchoDepth * tanAthwartEnd;
			tmpV3D.z = dLastEchoDepth;
			tmpWm4V3D = pSounder->GetSoftChannelCoordinateToWorldCoord(pFan, transId, lastChannelId, tmpV3D);
			pingData.GetBoundingBox().Update(tmpWm4V3D);
		}
	}
}

//*****************************************************************************
// Name : ComputeGround
// Description : Calcul de la profondeur du fond (relative � l'origine du faisceau) pour un faisceau donn�
// Parameters : * (in) PingData* pPingData
//				* (in) Transducer *	pTrans
//				* (in) std::int32_t channelId
//				* (out) std::uint32_t& groundEcho
// Return : void
//*****************************************************************************
double ShoalExtractionGeometry::ComputeGround(PingData* pPingData, const Transducer * pTrans, std::int32_t channelId, std::uint32_t& groundEcho)
{
	double maxDepth = 0;

	//calcul du fond pour le faisceau
	std::int32_t a;
	bool found;
	SoftChannel* softChannel = pTrans->getSoftChannelPolarX(channelId);
	groundEcho = 0;

	pPingData->GetPingFan()->getBottom(softChannel->getSoftwareChannelId(), groundEcho, a, found);

	if (!found)
	{
		double MaxfondFrame;

		pPingData->GetPingFan()->m_computePingFan.getBottomMaxRangeMeter(MaxfondFrame, found);
		if (found)
		{
			groundEcho = (std::int32_t)(MaxfondFrame / pTrans->getBeamsSamplesSpacing());
		}
		else
		{
			static M3DKernel *pKern = M3DKernel::GetInstance();
			groundEcho = (std::uint32_t)(pKern->GetRefKernelParameter().getMaxRange() / pTrans->getBeamsSamplesSpacing());
		}
	}

	//evaluation de la profondeur max de l'echo
	double athwartAngle = softChannel->m_mainBeamAthwartSteeringAngleRad;
	double athwartAngle3dB = softChannel->m_beam3dBWidthAthwartRad * 0.5;
	double alongAngle = softChannel->m_mainBeamAlongSteeringAngleRad;
	double alongAngle3dB = softChannel->m_beam3dBWidthAlongRad * 0.5;

	//calcul de la longueur en m dans l'axe du faisceau
	maxDepth = ((double)groundEcho + pTrans->GetSampleOffset() + 0.5) * pTrans->getBeamsSamplesSpacing();
	//compensation de la longueur dans l'axe du faisceau par les angles d'ouverture (cf.Sounder.cpp ::GetAxisAlignedBoundingBox)
	//pour englober la partie inf�rieure du volume r�el
	maxDepth /= cos(alongAngle3dB) * cos(athwartAngle3dB);

	//calcul de la longueur 3D dans le repere r�el en utilisant les angles de d�pointage
	Vector3D tmpV3D;
	if (athwartAngle > 0)
	{
		tmpV3D.y = -athwartAngle3dB;
	}
	else
	{
		tmpV3D.y = +athwartAngle3dB;
	}
	if (alongAngle > 0)
	{
		tmpV3D.x = -alongAngle3dB;
	}
	else
	{
		tmpV3D.x = +alongAngle3dB;
	}

	tmpV3D.z = maxDepth;
	Vector3D res = pPingData->GetPingFan()->getSounderRef()->GetSoftChannelCoordinateToWorldCoord(
		pPingData->GetPingFan(), 0, channelId, tmpV3D);

	return res.z;
}

//*****************************************************************************
// Name : IntersectChannel
// Description : Calcul de l'intersection d'un segment [AB] avec un faisceau d'angle donn�
// Parameters : * (in) dl_angleSteering : angle du faisceau
//				* (in) double beamsSamplesSpacing : espacement entre les �chos
//				* (in) point A
//				* (in) point B
// Return : int : l'indice de l'echo dans le faisceau si intersection, sinon -1
//*****************************************************************************
int ShoalExtractionGeometry::IntersectChannel(double cos_angleSteering, double tan_angleSteering, double beamsSamplesSpacing, const Vector2D& A, const Vector2D& B)
{
	int result = -1;
	assert(beamsSamplesSpacing > 0);
	/*
		//segment AB
		double segmentAB[4] = {A.x, A.y, B.x, B.y};
		//calcul d'un segment pour le faisceau (origine 0, 0)
        double maxY = std::max(A.y, B.y);
		double segmentChannel[4] = {0, 0, maxY * tan(dl_angleSteering), maxY};
	*/
	//segment AB
	/*static */double segmentAB[4];
	segmentAB[0] = A.x;
	segmentAB[1] = A.y;
	segmentAB[2] = B.x;
	segmentAB[3] = B.y;

	//calcul d'un segment pour le faisceau (origine 0, 0)
	/*static */double segmentChannel[4];
    double maxY = std::max(A.y, B.y);
	segmentChannel[0] = 0;
	segmentChannel[1] = 0;
	segmentChannel[2] = 2.0 * maxY * tan_angleSteering;
	segmentChannel[3] = 2.0 * maxY;

	//test d'intersection
	/*static */double intersection[2];
	if (GeometryTools2D::intersectSegmentsSimple(segmentAB, segmentChannel, intersection) != TSEGMENTS_UNKNOWN)
	{
		//projection sur le faisceau
		double yChannel = intersection[1] / cos_angleSteering;
		//calcul de l'indice de l'�cho sur le faisceau
		result = (int)(yChannel / beamsSamplesSpacing + 0.5);
	}

	return result;
}

//*****************************************************************************
// Name : AggregateEmptySpace2D
// Description : ajouter les echos inclus dans l'espace entre les 2 surfaces 2D
//					� la liste des echos du banc
// Parameters : * (in) surfA : surface A (4 points)
//				* (in) surfB : surface B (4 points)
//				* (in) transId : transducteur
//				* (in) channelIdA : faisceau de la surface A
//				* (in) channelIdB : faisceau de la surface B
//				* (in) pShoalId : id du banc associ�
//				* (in/out) PingData& pingData
// Return : void
//*****************************************************************************
void ShoalExtractionGeometry::AggregateEmptySpace2D(const Poly2D& surfA, const Poly2D& surfB,
	int transId,
	int channelIdA,
	int channelIdB,
	std::shared_ptr<ShoalId> pShoalId,
	PingData& pingData)
{
	//l'op�ration ne sert que si il existe au moins un faisceau libre entre les 2 faisceaux supports
	if (channelIdB - channelIdA > 1)
	{
		assert(surfA.GetNbPoints() == 4);
		assert(surfB.GetNbPoints() == 4);

		//FRE - 14/10/2009 - pour chaque �cho, utiliser les points situ� sur le faisceau 
		// au lieu des points constituant le bord de la surface 2D de l'�cho
		// afin de s'abstraire du cas o� il y a recouvrement inter-faisceaux (angle3dB > espacement inter-faisceaux)

		//pour le point min, on utilise les points HG et HD (cf. CreateEchoSurf2D())
		BaseMathLib::Vector2D min_A((surfA.GetPoints()[0] + surfA.GetPoints()[2]) * 0.5,
			(surfA.GetPoints()[1] + surfA.GetPoints()[3]) * 0.5);
		//pour le point max, on utilise les points BG et BD (cf. CreateEchoSurf2D())
		BaseMathLib::Vector2D max_A((surfA.GetPoints()[4] + surfA.GetPoints()[6]) * 0.5,
			(surfA.GetPoints()[5] + surfA.GetPoints()[7]) * 0.5);

		//pour le point min, on utilise les points HG et HD (cf. CreateEchoSurf2D())
		BaseMathLib::Vector2D min_B((surfB.GetPoints()[0] + surfB.GetPoints()[2]) * 0.5,
			(surfB.GetPoints()[1] + surfB.GetPoints()[3]) * 0.5);
		//pour le point max, on utilise les points BG et BD (cf. CreateEchoSurf2D())
		BaseMathLib::Vector2D max_B((surfB.GetPoints()[4] + surfB.GetPoints()[6]) * 0.5,
			(surfB.GetPoints()[5] + surfB.GetPoints()[7]) * 0.5);


		AggregateEmptySpace2D(min_A, max_A, min_B, max_B,
			transId, channelIdA, channelIdB, pShoalId, pingData);
	}
}

//*****************************************************************************
// Name : AggregateEmptySpace2D
// Description : ajouter les echos inclus dans l'espace entre les 2 segments 2D [A1 B1] et [A2 B2]
//					� la liste des echos du banc
//				Les points A sont sur le faisceau A et les points B sur le faisceau B
// Parameters : * (in) A1 : point (xMin) du segment 1 (YMin) 
//				* (in) B1 : point (xMax) du segment 1 (YMin) 
//				* (in) A2 : point (xMin) du segment 2 (YMax) 
//				* (in) B2 : point (xMax) du segment 2 (YMax) 
//				* (in) transId : transducteur
//				* (in) channelIdA : faisceau des points A
//				* (in) channelIdB : faisceau des points B
//				* (in) pShoalId : id du banc associ�
//				* (in/out) PingData& pingData
// Return : void
//*****************************************************************************
void ShoalExtractionGeometry::AggregateEmptySpace2D(const BaseMathLib::Vector2D& A1, const BaseMathLib::Vector2D& B1,
	const BaseMathLib::Vector2D& A2, const BaseMathLib::Vector2D& B2,
	int transId,
	int channelIdA,
	int channelIdB,
	std::shared_ptr<ShoalId> pShoalId,
	PingData& pingData)
{
	//l'op�ration ne sert que si il existe au moins un faisceau libre entre les 2 faisceaux supports
	if (channelIdB - channelIdA > 1)
	{
		assert(channelIdA >= 0);

		Sounder			*pSounder = pingData.GetPingFan()->m_pSounder;
		Transducer		*pTrans = pSounder->GetTransducer(transId);
		TransductData	*pTransData = pingData.GetTransducers()[transId];

		double zIntegrationDistance = pTransData->GetVerticalIntegrationDistance();

		// On r�cup�re l'espacement entre les �chos
		double beamsSamplesSpacing = pTransData->GetBeamsSamplesSpacing();

		assert(channelIdB < (int)pTransData->GetChannels().size());

		//parcours de tous les id de faisceaux inclus entre le faisceau A et le faisceau B
		for (int iChannel = channelIdA + 1; iChannel < channelIdB; iChannel++)
		{
			//recherche de la donn�e de faisceau correspondant � l'id recherch�
			ChannelData* pChannelData = pTransData->GetChannels()[iChannel];
			assert(pChannelData != NULL);

			//recherche de l'angle d'orientation du faisceau
			double angleSteering = pChannelData->GetSteeringAngle();
			double cos_angleSteering = cos(angleSteering);
			double tan_angleSteering = tan(angleSteering);

			//calculer l'intersection entre les segments [A1 B1] et [A2 B2] et le faisceau
			int echoMin = IntersectChannel(cos_angleSteering, tan_angleSteering, beamsSamplesSpacing, A1, B1);
			int echoMax = IntersectChannel(cos_angleSteering, tan_angleSteering, beamsSamplesSpacing, A2, B2);

			if (echoMax >= 0 && echoMin >= 0)
			{
				int tmpEcho = echoMax;
                echoMax = std::max(echoMax, echoMin);
				echoMin = min(tmpEcho, echoMin);

				//Maj de l'�cho et ajout dans les donn�es finales du faisceau
				pChannelData->AddEchoGroup(echoMin, echoMax, pShoalId);

#ifdef _GTOOLS_TRACE_PERF
                //trace_ofs << " Channel/Echo " << iChannel <<"/" << intersectEchogroup->GetEchoMin() << "-" << intersectEchogroup->GetEchomax() ;
				//trace_ofs << std::endl;
#endif
			}
		}
	}
}

//*****************************************************************************
// Name : IntersectPlane
// Description : renvoie le point d'intersection entre le segment et le plan dans le repere 2D du plan
// Parameters : * (in) plane : plan de coupe (3D)
//				* (in) segment : segment 3D
//				* (in) transId : transducteur
//				* (in/out) Vector2D& intersect2D
// Return : void
//*****************************************************************************
void ShoalExtractionGeometry::IntersectPlane(const Plane3D& plane,
	const Segment3D& segment,
	const BaseMathLib::Vector3D& planeOrigin,
	double cosPlanePitch,
	BaseMathLib::Vector2D& intersect2D)
{
	//d�terminer l'intersection du segment dans le plan de coupe du ping
    //Wm4::IntrSegment3Plane3d intersect3D(segment, plane);
	//intersect3D.Find();

	double segmentT;
	GeometryTools3D::intersection(segment, plane, segmentT);

	BaseMathLib::Vector3D intersectPoint = segment.origin + segmentT * segment.direction;

	//recadrer les coordonn�es de l'intersection dans le plan
	intersectPoint -= planeOrigin;
	//le signe de la projection dans l'axe 2D X du plan est fonction du signe de Delta X et du sens de la normale au plan en Y
	//(attention, en 3D, Z en vers le bas, donc Y est invers�)
	double xSign = (intersectPoint.x*plane.normal.y > 0) ? -1.0 : 1.0;
	intersect2D.x = sqrt(intersectPoint.x * intersectPoint.x + intersectPoint.y * intersectPoint.y) * xSign;
	intersect2D.y = intersectPoint.z / cosPlanePitch;
}

//*****************************************************************************
// Name : GetIntersectSegment
// Description : r�cup�rer le segment intersectant le plan � partir des 4 points [A, B] et [C, D]
//				on teste par odre de priorit� [B,C], [B,D], [A,C] et [A,D]
//				les points de [AB] doivent etre dans le cot� positif du plan (avancement du navire)
// Parameters : * (in) volA : volume A (8 points)
//				* (in) volB : volume B (8 points)
//				* (in) transId : transducteur
//				* (in) channelIdA : faisceau de la surface A
//				* (in) channelIdB : faisceau de la surface B
//				* (in) pShoalId : id du banc associ�
//				* (in/out) PingData& pingData
// Return : Wm4::Segment3d*
//*****************************************************************************
Segment3D* ShoalExtractionGeometry::GetIntersectSegment(const Plane3D& plane,
	const BaseMathLib::Vector3D& pointA,
	const BaseMathLib::Vector3D& pointB,
	const BaseMathLib::Vector3D& pointC,
	const BaseMathLib::Vector3D& pointD)
{
	Segment3D* result = NULL;

	int pointBSide = plane.side(pointB);
	int pointCSide = plane.side(pointC);

	//les points de [AB] doivent etre dans le cot� positif du plan (avancement du navire)
	if (pointBSide > 0)
	{
		if (pointBSide != pointCSide)
		{
			result = new Segment3D(pointB, pointC);
		}
		else
		{
			int pointDSide = plane.side(pointD);
			if (pointBSide != pointDSide)
			{
				result = new Segment3D(pointB, pointD);
			}
		}
	}
	else
	{
		int pointASide = plane.side(pointA);
		if (pointASide > 0)
		{
			if (pointCSide != pointASide)
			{
				result = new Segment3D(pointA, pointC);
			}
			else
			{
				int pointDSide = plane.side(pointD);
				if (pointASide != pointDSide)
				{
					result = new Segment3D(pointA, pointD);
				}
			}
		}
	}

	return result;
}

//*****************************************************************************
// Name : AggregateEmptySpace3D
// Description : ajouter les echos inclus dans l'espace entre les 2 volumes 3D
//					� la liste des echos du banc
// Parameters : * (in) echoGroup1 : echogroup du ping 1
//				* (in) echoGroup2 : echogroup du ping 2
//				* (in) channelIdMean : faisceau median pour commencer la recherche
//				* (in) transId : transducteur
//				* (in) channelIdA : faisceau de la surface A
//				* (in) channelIdB : faisceau de la surface B
//				* (in) pShoalId : id du banc associ�
//				* (in/out) PingData& pingData
// Return : bool : true if one valid ping is found between pingIdA and pingIdB
//*****************************************************************************
bool ShoalExtractionGeometry::AggregateEmptySpace3D(int iChannel1, int iChannel2,
	EchoGroup* echoGroup1, EchoGroup* echoGroup2,
	const TransductData* pTransData,
	int sounderId,
    std::uint64_t pingIdA,
    std::uint64_t pingIdB,
	std::shared_ptr<ShoalId> pShoalId,
	ShoalIdMgr* pShoalIdMgr,
	PingDataMap& pings)
{
	bool result = false;

	Poly3D& volA = echoGroup1->GetVol3D();
	Poly3D& volB = echoGroup2->GetVol3D();

	//l'op�ration ne sert que si il existe au moins un ping libre entre les 2 pings
	const auto pingMinIterator = pings.find(std::min(pingIdA, pingIdA));
	const auto pingMaxIterator = pings.find(std::max(pingIdA, pingIdA));
	const auto nbPingsBetweenAb = std::distance(pingMinIterator, pingMaxIterator);
	if (nbPingsBetweenAb > 1)
	{
		assert(volA.GetPoints().size() == 8);
		assert(volB.GetPoints().size() == 8);

		int channelIdMean = (int)((iChannel1 + iChannel2)*0.5);

		// on conserve la liste des �chogroups ajout�s � chaque ping, afin de mettre � jour
		// les liaisons entre pings pour la repr�sentation du volume
		ShoalId::ShoalIdSet previousShoalIds;
		ShoalId::ShoalIdSet currentShoalIds;

		// pour le premier ping, le shoalid pr�c�dent est celui du pingB (echoGroup2)
		previousShoalIds.insert(echoGroup2->GetShoalId());

		//pour chaque ping entre pingIdA et pingIdB
		auto pingIterator = pingMinIterator;
		while (++pingIterator != pingMaxIterator && pingIterator != pings.end())
		{
			const auto pPingData = pingIterator->second;

			assert(pPingData != NULL);
			if (pPingData->GetSounderId() == sounderId)
			{
				result = true;

#ifdef _GTOOLS_TRACE_PERF
				trace_ofs << " Ping/Trans " << iPing << "/" << pTransData->getId();
				trace_ofs << std::endl;
#endif

				Sounder			*pSounder = pPingData->GetPingFan()->m_pSounder;

				//on utilise par d�faut les segments 3D
				//HG [minY_Back_HG maxY_Front_HG] 
				//HD [minY_Back_HD maxY_Front_HD] 
				//BD [minY_Back_BD maxY_Front_BD]
				//BG [minY_Back_BG maxY_Front_BG] 

				//r�cup�rer les segments d'intersection
				Segment3D* HD = GetIntersectSegment(pTransData->GetPlane3D(), volA.GetPoints()[0], volA.GetPoints()[4],
					volB.GetPoints()[0], volB.GetPoints()[4]);
				Segment3D* HG = GetIntersectSegment(pTransData->GetPlane3D(), volA.GetPoints()[1], volA.GetPoints()[5],
					volB.GetPoints()[1], volB.GetPoints()[5]);
				Segment3D* BG = GetIntersectSegment(pTransData->GetPlane3D(), volA.GetPoints()[2], volA.GetPoints()[6],
					volB.GetPoints()[2], volB.GetPoints()[6]);
				Segment3D* BD = GetIntersectSegment(pTransData->GetPlane3D(), volA.GetPoints()[3], volA.GetPoints()[7],
					volB.GetPoints()[3], volB.GetPoints()[7]);

				if (HG != NULL && HD != NULL && BD != NULL && BG != NULL)
				{
					double cosPlanePitch = cos(pTransData->GetPitch());

					//d�terminer l'intersection de chaque segment dans le plan de coupe du ping
					BaseMathLib::Vector2D intersect2D_HG;
					BaseMathLib::Vector2D intersect2D_HD;
					BaseMathLib::Vector2D intersect2D_BG;
					BaseMathLib::Vector2D intersect2D_BD;

					IntersectPlane(pTransData->GetPlane3D(), *HG, pTransData->GetOrigin3D(), cosPlanePitch, intersect2D_HG);
					IntersectPlane(pTransData->GetPlane3D(), *HD, pTransData->GetOrigin3D(), cosPlanePitch, intersect2D_HD);
					IntersectPlane(pTransData->GetPlane3D(), *BG, pTransData->GetOrigin3D(), cosPlanePitch, intersect2D_BG);
					IntersectPlane(pTransData->GetPlane3D(), *BD, pTransData->GetOrigin3D(), cosPlanePitch, intersect2D_BD);

					//parcours de tous les faisceaux vers le premier faisceau tant qu'on a intersection
					EchoGroup* echoGroupDownChannel;
					bool hasDownChannel = true;
					for (int iChannelDown = (int)channelIdMean; iChannelDown >= 0 && hasDownChannel; iChannelDown--)
					{
						echoGroupDownChannel = IntersectChannel3D(pPingData, pSounder, iChannelDown, pTransData,
							pShoalId, pShoalIdMgr, intersect2D_HG, intersect2D_HD, intersect2D_BG, intersect2D_BD);

						// ajout de l'�ventuel echogroup aggr�g�s � la liste des shoalIds aggr�g�s
						if (echoGroupDownChannel != NULL)
						{
							currentShoalIds.insert(echoGroupDownChannel->GetShoalId());
						}
						else
						{
							hasDownChannel = false;
						}
					}

					//parcours de tous les faisceaux vers le dernier faisceau tant qu'on a intersection
					size_t nbChannels = pTransData->GetChannels().size();
					EchoGroup* echoGroupUpChannel;
					bool hasUpChannel = true;
					for (int iChannelUp = (int)channelIdMean + 1; iChannelUp < (int)nbChannels && hasUpChannel; iChannelUp++)
					{
						echoGroupUpChannel = IntersectChannel3D(pPingData, pSounder, iChannelUp, pTransData,
							pShoalId, pShoalIdMgr, intersect2D_HG, intersect2D_HD, intersect2D_BG, intersect2D_BD);

						// ajout de l'�ventuel echogroup aggr�g�s � la liste des shoalIds aggr�g�s
						if (echoGroupUpChannel != NULL)
						{
							currentShoalIds.insert(echoGroupUpChannel->GetShoalId());
						}
						else
						{
							hasUpChannel = false;
						}
					}
				}
				if (HG != NULL)
				{
					delete HG;
				}
				if (HD != NULL)
				{
					delete HD;
				}
				if (BG != NULL)
				{
					delete BG;
				}
				if (BD != NULL)
				{
					delete BD;
				}

				// Dans le cas o� la m�thode g�om�trique ci-dessus n'a pas abouti (configurations
				// o� les ouvertures � 3dB ne sont pas contigues, pings espac�s, etc), on utilise
				// la m�thode ci-dessous (interpolation lineaire),
				// qui permet d'avoir au moins un groupe d'�cho intercal� entre les pings A et B,
				// qui nous permettra d'avoir une tranche de banc pour chaque ping quoi qu'il arrive,
				// et donc d'avoir une repr�sentation continue du banc en 3D)
				if (currentShoalIds.size() == 0)
				{
					int echoMin, echoMax, channel;
					const auto factor = static_cast<double>(std::distance(pingMinIterator, pingIterator)) / static_cast<double>(nbPingsBetweenAb);
					echoMin = (int)((double)(int)echoGroup2->GetEchoMin() + factor*(double)((int)echoGroup1->GetEchoMin() - (int)echoGroup2->GetEchoMin()) + 0.5);
                    echoMax = (int)((double)(int)echoGroup2->GetEchomax() + factor*(double)((int)echoGroup1->GetEchomax() - (int)echoGroup2->GetEchomax()) + 0.5);
					channel = (int)((double)iChannel2 + factor*(double)(iChannel1 - iChannel2) + 0.5);

					//ajouter une groupe d'�chos correspondant � l'espace d'intersection
					EchoGroup* intersectEchogroup = pTransData->GetChannels()[channel]->AddEchoGroup(echoMin, echoMax, NULL);

					if (!intersectEchogroup->GetShoalId())
					{
						//si pas de groupe d'�cho connexe, on attribue l'Id de banc du ping pr�c�dent					
						intersectEchogroup->SetShoalId(pShoalIdMgr->IncrShoalId());
					}

					if (intersectEchogroup->GetShoalId()->GetLinked3DShoalId() != pShoalId->GetLinked3DShoalId())
					{
						intersectEchogroup->GetShoalId()->SetLinked3DShoalId(pShoalId->GetLinked3DShoalId());
					}
					currentShoalIds.insert(intersectEchogroup->GetShoalId());
				}

				// pour chaque echogroupe int�gr� au ping courant, ajout 
				// des shoalid du ping pr�c�dent
				ShoalId::ShoalIdSet::const_iterator currentIter = currentShoalIds.begin();
				ShoalId::ShoalIdSet::const_iterator currentIterEnd = currentShoalIds.end();
				while (currentIter != currentIterEnd)
				{
					std::shared_ptr<ShoalId> currentShoalId = (*currentIter).lock();
					if (currentShoalId)
					{
						// parcours des echogroups du ping pr�c�dent
						ShoalId::ShoalIdSet::const_iterator previousIter = previousShoalIds.begin();
						ShoalId::ShoalIdSet::const_iterator previousIterEnd = previousShoalIds.end();
						while (previousIter != previousIterEnd)
						{
							currentShoalId->AddPreviousPing2D(*previousIter);
							previousIter++;
						}
					}
					currentIter++;
				}

				// l'ensemble des shoaid du ping courant devient l'ensemble des shoalid du ping pr�d�cent,
				// et on clean l'ensemble du ping courant
				previousShoalIds = currentShoalIds;
				currentShoalIds.clear();

			}//if(pPingData->GetSounderId() == sounderId)
		}//for(int iPing = pingIdB+1; iPing < pingIdA; iPing++)

	// ajout du lien entre les groupes d'�chos du ping A et des derniers �chos int�gr�s
		ShoalId::ShoalIdSet::const_iterator previousIter = previousShoalIds.begin();
		ShoalId::ShoalIdSet::const_iterator previousIterEnd = previousShoalIds.end();
		while (previousIter != previousIterEnd)
		{
			echoGroup1->GetShoalId()->AddPreviousPing2D(*previousIter);
			previousIter++;
		}

	}//if(abs(pingIdA - pingIdB) > 1)
	return result;
}


//*****************************************************************************
// Name : IntersectChannel3D
// Description : ajouter les echos inclus entre les vecteurs d'intersection et le faisceau 3D
//					� la liste des echos du banc
// Parameters : * (in) volA : volume A (8 points)
//				* (in) PingData* pPingData
//				* (in) Sounder	*pSounder
//				* (in) unsigned int iChannel
//				* (in) TransductData* pTransData
//				* (in) double verticalIntegrationDistance
//				* (in) double acrossIntegrationDistance
//				* (in) double alongIntegrationDistance
//				* (in) ShoalId* pShoalId
//				* (in) const BaseMathLib::Vector2D& intersect2D_HG
//				* (in) const BaseMathLib::Vector2D& intersect2D_HD
//				* (in) const BaseMathLib::Vector2D& intersect2D_BG
//				* (in) const BaseMathLib::Vector2D& intersect2D_BD
// Return : EchoGroup* : not null if channel is intersected
//*****************************************************************************
EchoGroup* ShoalExtractionGeometry::IntersectChannel3D(PingData* pPingData,
	Sounder	*pSounder,
	unsigned int iChannel,
	const TransductData* pTransData,
	std::shared_ptr<ShoalId> pShoalId,
	ShoalIdMgr* pShoalIdMgr,
	const Vector2D& intersect2D_HG,
	const Vector2D& intersect2D_HD,
	const Vector2D& intersect2D_BG,
	const Vector2D& intersect2D_BD)
{
	EchoGroup* intersectEchogroup = NULL;

	ChannelData* pChannelData = pTransData->GetChannels()[iChannel];
	double beamsSamplesSpacing = pTransData->GetBeamsSamplesSpacing();
	double transTranslation = pTransData->GetSampleOffset();

	double zIntegrationDistance = pTransData->GetVerticalIntegrationDistance();

	//recherche de l'angle d'orientation du faisceau
	double angleSteering = pChannelData->GetSteeringAngle();
	double cos_angleSteering = cos(angleSteering);
	double tan_angleSteering = tan(angleSteering);

	//effectuer un test d'intersection 2D avec les 4 segments 
	//[HG HD], [BG BD], [HG BG] et [HD BD]
	int echoMax = IntersectChannel(cos_angleSteering, tan_angleSteering,
		beamsSamplesSpacing, intersect2D_HG, intersect2D_HD);
	int echoMin = IntersectChannel(cos_angleSteering, tan_angleSteering,
		beamsSamplesSpacing, intersect2D_BG, intersect2D_BD);

	int tmpEcho = echoMax;
    echoMax = std::max(echoMax, echoMin);
	echoMin = min(tmpEcho, echoMin);

	if (echoMax < 0 || echoMin < 0)
	{
		int echo1 = IntersectChannel(cos_angleSteering, tan_angleSteering,
			beamsSamplesSpacing, intersect2D_HG, intersect2D_BG);
		int echo2 = IntersectChannel(cos_angleSteering, tan_angleSteering,
			beamsSamplesSpacing, intersect2D_HD, intersect2D_BD);

        echoMax = std::max(echo1, echoMax);
        echoMax = std::max(echo2, echoMax);

		if (echo1 > 0)
		{
			echoMin = (echoMin > 0) ? min(echo1, echoMin) : echo1;
		}
		if (echo2 > 0)
		{
			echoMin = (echoMin > 0) ? min(echo2, echoMin) : echo2;
		}
	}

	// OTK - FAE077 - attention, on n'applique pas l'offset de translation si les �chos trouv�s sont incorrects (= -1)
	// (sinon ils peuvent redevenir corrects !)
	if (echoMin != -1 && echoMax != -1)
	{
		//retrait de l'offset du transducteur
		echoMin -= (int)transTranslation;
		echoMax -= (int)transTranslation;
	}

	//if(echoMax < 0 || echoMin < 0)
	if (echoMax >= 0 && echoMin >= 0)
	{
		//ajouter une groupe d'�chos correspondant � l'espace d'intersection
		intersectEchogroup = pChannelData->AddEchoGroup(echoMin, echoMax, NULL);

#ifdef _GTOOLS_TRACE_PERF
        trace_ofs << " Channel/Echo " << iChannel << "/" << echoMin << "-" << echoMax << "\tUpdated group :" << intersectEchogroup->GetEchoMin() << "-" << intersectEchogroup->GetEchomax();
		trace_ofs << std::endl;
#endif

		if (intersectEchogroup->GetShoalId() == NULL)
		{
			//si pas de groupe d'�cho connexe, on attribue l'Id de banc du ping pr�c�dent					
			intersectEchogroup->SetShoalId(pShoalIdMgr->IncrShoalId());
		}

		if (pShoalId)
		{
			if (intersectEchogroup->GetShoalId()->GetLinked3DShoalId() != pShoalId->GetLinked3DShoalId())
			{
				intersectEchogroup->GetShoalId()->SetLinked3DShoalId(pShoalId->GetLinked3DShoalId());
			}
		}

	}//if(echoMax >= 0 && echoMin >= 0)
	return intersectEchogroup;
}

