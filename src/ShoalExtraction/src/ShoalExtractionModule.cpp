
#include <sstream>
#include <strstream>
#include <time.h>
#include <sys/timeb.h>
#include <algorithm>
#include <functional>
#include <assert.h>
#include <float.h>
//#include <process.h>    /* _beginthread, _endthread */

#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "BaseMathLib/Vector3.h"


#include "ShoalExtraction/ShoalExtractionOutput.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "Filtering/DataFilter.h"

#include "M3DKernel/utils/log/ILogger.h"
#include "M3DKernel/M3DKernel.h"

#include "BaseMathLib/geometry/GeometryTools2D.h"
#include "ShoalExtraction/ShoalExtractionModule.h"

#include <iterator>

#include "ShoalExtraction/ShoalExtractionGeometry.h"
#include "ShoalExtraction/ShoalExtractionStat.h"
#include "ShoalExtraction/data/shoalidmgr.h"
#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/shoalstat.h"
#include "ShoalExtraction/data/pingshoalstat.h"

using namespace shoalextraction;
using namespace BaseMathLib;
using namespace std;

#ifdef _TRACE_PERF
std::ofstream trace_ofs("d:/tmp/shoalextraction/state.txt");
#endif

//#define _USE_WORKER_THREAD

#define MAX_ESU_HISTORY 2

//constants
#define MEMORY_ALLOC_ERROR "Impossible d'allouer la m�moire n�cessaire"

namespace
{
	constexpr const char * LoggerName = "ShoalExtraction.ShoalExtractionModule";
}

ShoalExtractionModule::ShoalExtractionModule() 
	: ProcessModule("ShoalExtraction Module")
	, m_actionStack(this)
{
	m_ESUId = 0;
	m_uiExternShoalId = 0;

	//init shal id manager
	m_pShoalIdMgr = new ShoalIdMgr();
	setEnable(false);

	m_DelayInfo = NULL;
}

ShoalExtractionModule::~ShoalExtractionModule()
{
	m_actionStack.Reset();

	//fermeture et destruction des flux de sortie
	std::map<std::string, std::ofstream*>::iterator iterStream = m_mapCsvStream.begin();
	while (iterStream != m_mapCsvStream.end())
	{
		iterStream->second->close();
		delete iterStream->second;
		iterStream++;
	}
	m_mapCsvStream.clear();

	//destruction des bancs stock�s
	std::vector<ShoalData*>::iterator iterShoal = GetShoals().begin();
	while (iterShoal != GetShoals().end())
	{
		delete *iterShoal;
		iterShoal++;
	}
	GetShoals().clear();

	//destruction des donn�es de ping
	PingDataMap::iterator iterPing = m_Pings.begin();
	while (iterPing != m_Pings.end())
	{
		PingFan* pFan = iterPing->second->GetPingFan();
		MovUnRefDelete(pFan);
		delete iterPing->second;
		iterPing++;
	}
	m_Pings.clear();

	//purge des id internes de bancs
	m_pShoalIdMgr->Reset();
	delete m_pShoalIdMgr;

	m_lock.Lock();
	size_t pingFanNb = m_PingFansToUnRef.size();
	for (int i = 0; i < pingFanNb; i++)
	{
		MovUnRefDelete(m_PingFansToUnRef[i]);
	}
	m_PingFansToUnRef.clear();
	m_lock.Unlock();
}

void ShoalExtractionModule::PingFanAdded(PingFan *p)
{
	if (getEnable())
	{
		// Si au moins un transducteur du sondeur n'est pas éligible à l'extraction de banc, on ignore le sondeur
		const auto allTransducers = p->getSounderRef()->GetAllTransducers();
		if (std::any_of(
			allTransducers.cbegin(),
			allTransducers.cend(),
			[&](const Transducer * transducer){ return !m_parameter.isTransducerNameActive(transducer->m_transName); }
			))
		{
			return;
		}
		
		// on va ajouter le pingFan � notre historique, on le reference dans le
		// thread principal pour �viter les plantages
		MovRef(p);

#ifndef _USE_WORKER_THREAD
		//monothread
		AddPingFan(p);
#else
		//multithread
        int nbPingMax = M3DKernel::GetInstance()->GetRefKernelParameter().getNbPingFanMax() / 4;

		// on informe l'IHM de l'�tat d'avancement du  thread de lecture
		if (m_DelayInfo != NULL)
		{
			m_ReaderPingFanCount++;
			m_DelayInfo(m_ReaderPingFanCount, -1, (std::uint32_t)nbPingMax);
		}

		m_actionStack.ScheduleAddPingFan(p, m_ReaderPingFanCount, m_DelayInfo);

		//wait the stack size is under a portion of the buffer size
		m_actionStack.WaitStackLimit(nbPingMax);
#endif
	}

	// d�r�f�rencemenet des pingsFans plus utilis�s dans le thread principal � chaque ping.
	m_lock.Lock();
	size_t pingFanNb = m_PingFansToUnRef.size();
	for (int i = 0; i < pingFanNb; i++)
	{
		MovUnRefDelete(m_PingFansToUnRef[i]);
	}
	m_PingFansToUnRef.clear();
	m_lock.Unlock();
}

std::vector<ShoalExtractionOutput*> ShoalExtractionModule::GetOutputs() const
{
	return GetOutputContainer().getOutputs<ShoalExtractionOutput>();
}

std::vector<ShoalExtractionOutput*> ShoalExtractionModule::GetOutputs(const tConsumerId consumerId) const
{
	return GetOutputContainer().getOutputs<ShoalExtractionOutput>(consumerId);
}

//*****************************************************************************
// Name : AddPingFan
// Description : add ping fan (use from worker thread)
// Parameters : * (in)  PingFan *pFan
// Return : void
//*****************************************************************************
void ShoalExtractionModule::AddPingFan(PingFan *p)
{
	MemorySet *pMemSet = p->GetMemorySetRef();

	//ajout des donn�es du ping
	PingData* pingData = new PingData();

	pingData->SetPingFan(p);
	pingData->SetPingId((int)p->GetPingId());

	//Initialisation de certaines donn�es du ping
	InitPingData(*pingData);

	//extraction des donn�es 1D pour l'ensemble des transducteurs
	Extract1DEcho(*pingData);

	//agr�gation inter-faisceaux
	Aggregate2DEcho(*pingData);

	//construction des donn�es 3D pour l'ensemble des transducteurs
	ShoalExtractionGeometry::ComputeVolumes(*pingData);

	//agr�gation inter-ping
	AggregatePing(pingData);

	//ajout a l'historique des pings
	m_Pings[p->GetPingId()] = pingData;
}

//*****************************************************************************
// Name : InitPingData
// Description : Initialisation de certaines donn�es du ping
// Parameters : * (in)  (shoalextraction::PingData& pingData
// Return : void
//*****************************************************************************
void ShoalExtractionModule::InitPingData(shoalextraction::PingData& pingData)
{
	PingFan * pFan = pingData.GetPingFan();

	if (pFan != NULL)
	{
		//init de la navigation
		for (const auto id : pFan->GetChannelIds()) {

			// Récupération du transducteur pour déterminer si l'extraction de banc doit être faite
			const auto transducer = pFan->getSounderRef()->getTransducerForChannel(id);
			if (transducer != nullptr && !m_parameter.isTransducerNameActive(transducer->m_transName))
			{
				continue;
			}

			// Initialisation des données			
			if (pFan->GetNavAttributesRef(id) != NULL)
			{
				pingData.GetNavigation(id).heading = pFan->GetNavAttributesRef(id)->m_headingRad;
			}
			if (pFan->GetNavAttitudeRef(id) != NULL)
			{
				pingData.GetNavigation(id).roll = pFan->GetNavAttitudeRef(id)->m_rollRad;
				pingData.GetNavigation(id).pitch = pFan->GetNavAttitudeRef(id)->m_pitchRad;
			}
			if (pFan->GetNavPositionRef(id) != NULL)
			{
				pingData.GetNavigation(id).latitude = pFan->GetNavPositionRef(id)->m_lattitudeDeg;
				pingData.GetNavigation(id).longitude = pFan->GetNavPositionRef(id)->m_longitudeDeg;
				pingData.GetNavigation(id).time = pFan->GetNavPositionRef(id)->m_ObjectTime;
			}
			if (pFan->GetNavAttitudeRef(id) != NULL)
			{
				pingData.GetNavigation(id).heave = pFan->GetNavAttitudeRef(id)->m_heaveMeter;
			}
			if (pFan->GetNavAttributesRef(id) != NULL)
			{
				pingData.GetNavigation(id).surfSpeed = pFan->GetNavAttributesRef(id)->m_speedMeter*3.6 / 1.852;
				pingData.GetNavigation(id).groundCourse = pFan->GetNavAttributesRef(id)->m_headingRad;
			}
			pingData.GetNavigation(id).depth = pFan->m_computePingFan.m_minRange;
		}
	}
}

//*****************************************************************************
// Name : Extract1DEcho
// Description :
// Parameters : * (in)  PingFan *p
//				* (out)	std::vector<TransductData*>& pTransDataColl = liste des echos 1D pour chaque transducteur
// Return : void
//*****************************************************************************
void ShoalExtractionModule::Extract1DEcho(PingData& pingData)
{
	// indices de boucle
	unsigned int iTrans = 0;// transducteur
	unsigned int iChannel = 0;// channel

	//gestion du recouvrement
	bool useOverlap = m_parameter.GetUseOverlapFilter();
	//utilisation du faisceau central uniquement
	bool useCentralBeam = m_parameter.GetUseOnlyCentralBeam();

	//seuils de d�tection param�trables
	double detectThreshold = m_parameter.GetThreshold()*100.0;
	double detectMaxThreshold = m_parameter.GetMaxThreshold()*100.0;

	//Minimum shoal Depth
	float minDepth = m_parameter.GetMinDepth();
	//Maximum shoal Depth
	float maxDepth = m_parameter.GetMaxDepth();

	Sounder *pSounder = pingData.GetPingFan()->m_pSounder;
	MemorySet *pMemSet = pingData.GetPingFan()->GetMemorySetRef();

	std::int32_t pingId = pingData.GetPingId();

	pingData.SetSounderId(pSounder->m_SounderId);

	//**************************
	// Boucle Transducteurs
	//**************************
	for (iTrans = 0; iTrans < pMemSet->GetMemoryStructCount(); iTrans++)
	{
		const auto *pTrans = pSounder->GetTransducer(iTrans);		

		// L'extraction de banc doit-elle être faite pour ce transducteur ?
		if (pTrans != nullptr && !m_parameter.isTransducerNameActive(pTrans->m_transName))
		{
			continue;
		}

		// On r�cup�re l'espacement entre les �chos
		double beamsSamplesSpacing = pTrans->getBeamsSamplesSpacing();

		// calcul de la distance d'int�gration minimale en vertical (en m)
		double verticalIntegrationDistance = m_parameter.GetVerticalIntegrationDistance() *
			0.5*((double)pTrans->m_pulseDuration) / 1000000 * pSounder->m_soundVelocity;

		// On r�cup�re la structure de donn�es associ�e au pingFan
		MemoryStruct *pMem = pMemSet->GetMemoryStruct(iTrans);
		BaseMathLib::Vector2I size;

		//si on utilise les donn�es de recouvrement, on r�cup�re cette donn�e
		if (useOverlap)
		{
			pMem->LockOverLap();
			if (pMem->GetOverLap() != NULL)
			{
				size = pMem->GetOverLap()->getSize();
			}
			else
			{
				size.x = 0;
			}
			pMem->UnLockOverLap();
		}
		//sinon, on r�cup�re la matrice des niveaux d'�cho
		else
		{
			size = pMem->GetDataFmt()->getSize();
		}

		TransductData* pTransData = new TransductData();
		pTransData->SetTransId(iTrans);
		pTransData->SetTransName(pTrans->m_transName);

		//distance d'agr�gation verticale
		pTransData->SetVerticalIntegrationDistance(verticalIntegrationDistance);
		// espacement entre les �chos
		pTransData->SetBeamsSamplesSpacing(pTrans->getBeamsSamplesSpacing());
		// translation interne
		pTransData->SetSampleOffset(pTrans->GetSampleOffset());

		//determiner l'origine 3D du transducteur
		//Vector3D origin = pSounder->GetBaseTransducerToWorldCoord(pFan, iTrans);
		Vector3D origin = pSounder->GetSoftChannelCoordinateToWorldCoord(
            pingData.GetPingFan(), iTrans, 0, Vector3D(0, 0, 0));
        pTransData->SetOrigin3D(origin);
        Vector3D originGPS = pSounder->GetSoftChannelCoordinateToGeoCoord(
			pingData.GetPingFan(), iTrans, 0, Vector3D(0, 0, 0));
		pTransData->SetOrigin3DGPS(originGPS);

		pingData.AddTransducer(pTransData);

		//**************************
		// Boucle GetChannels()
		//**************************

		//si on n'utilise que le faisceau central en multi-faisceau
		unsigned int iBeamBegin = 0;
		unsigned int iBeamEnd = (unsigned int)size.x;
		if (useCentralBeam)
		{
			iBeamBegin = (unsigned int)(size.x / 2);
			iBeamEnd = iBeamBegin + 1;
		}

		double shipHeading = 0;
		for (iChannel = iBeamBegin; iChannel < iBeamEnd; iChannel++)
		{
			SoftChannel *pChannel = pTrans->getSoftChannelPolarX(iChannel);

			// calcul de la distance d'int�gration minimale en X (en m)
			auto softId = pChannel->getSoftwareChannelId();
			shipHeading += pingData.GetNavigation(softId).heading;
		}
		if (iBeamEnd > iBeamBegin) {
			shipHeading /= (iBeamEnd - iBeamBegin);
		}

		//init des distance d'agr�gation du ping
		double acrossIntegrationDistance = m_parameter.GetAcrossIntegrationDistance();
		double alongIntegrationDistance = m_parameter.GetAlongIntegrationDistance();

		// calcul de la distance d'intégration minimale en X (en m)
		const auto integrationDistanceX = std::max(fabs(alongIntegrationDistance  * cos(shipHeading)),
			fabs(acrossIntegrationDistance * sin(shipHeading)));

		// calcul de la distance d'intégration minimale en Y (en m)
		const auto integrationDistanceY = std::max(fabs(alongIntegrationDistance  * sin(shipHeading)),
			fabs(acrossIntegrationDistance * cos(shipHeading)));
		
		pTransData->SetIntegrationDistances(integrationDistanceX, integrationDistanceY);

		for (iChannel = iBeamBegin; iChannel < iBeamEnd; iChannel++)
		{
			SoftChannel *pChannel = pTrans->getSoftChannelPolarX(iChannel);
			auto softId = pChannel->getSoftwareChannelId();

			bool skipBeam = false;
			unsigned short chanId = pTrans->GetChannelId()[iChannel];
			if (pChannel)
			{
				PingFan * pFan = pingData.GetPingFan();
				auto beam = pFan->getBeam(chanId)->second;
				// Si le channel est passif, il n'est pas pris en compte dans le calcul
				if (beam->m_transMode == BeamDataObject::PassiveTransducerMode)
				{
					M3D_LOG_INFO(LoggerName, "ShoalExtraction - Passive Ping found, will not be used.");
					skipBeam = true;
				}
			}
			
			// angles du faisceau
			double angle3dB = pChannel->m_beam3dBWidthAthwartRad * 0.5;
			double angleSteering = pChannel->m_mainBeamAthwartSteeringAngleRad;
			double roll, pitch;
			pSounder->GetBeamOrientation(pingData.GetPingFan(), roll, pitch, iChannel);

			//distance maximale en nb d'indices d'�cho 
			std::int32_t distMaxNbEcho = (verticalIntegrationDistance / beamsSamplesSpacing);

			//liste des groupes d'�chos pour ce channel
			ChannelData* pChannelData = new ChannelData();
			pTransData->AddChannel(pChannelData);
			pChannelData->SetChannelId(iChannel);
			pChannelData->SetSoftwareChannelId(softId);
			//initialisation des angles du faisceau
			pChannelData->SetSteeringAngle(pChannel->m_mainBeamAthwartSteeringAngleRad);
			pChannelData->SetAthwartAngle(pChannel->m_beam3dBWidthAthwartRad);
			pChannelData->SetAlongAngle(pChannel->m_beam3dBWidthAlongRad);
			pChannelData->SetPitch(pitch);
			pChannelData->SetAcousticFrequency(pChannel->m_acousticFrequency);

			pTransData->SetPitch(pitch);

			//calcul du fond pour le faisceau
			std::uint32_t groundEcho = 0;
			ShoalExtractionGeometry::ComputeGround(&pingData, pTrans, pChannelData->GetChannelId(), groundEcho);
			pChannelData->SetGroundEcho(groundEcho);
			bool groundReached = false;

			//liste des �chos pour un banc pour ce channel
			EchoGroup* pEchoGroup = NULL;

			//indice de l'echo du dernier point du banc dans le channel
			std::int32_t lastEchoShoal = -1;

			// R�cup�ration donn�es �cho de chaque faisceau
			DataFmt * pStart = NULL;

			//Lock MemoryStruct
			pMem->LockOverLap();
			if (pMem->GetOverLap())
			{
				pMem->GetOverLap()->Lock();
			}
			pMem->GetDataFmt()->Lock();
			pMem->GetFilterFlag()->Lock();

			//si on utilise les donn�es de recouvrement, on ne filtre que sur cette donn�e
			if (useOverlap)
			{
				if (pMem->GetOverLap() == NULL) // cas multifaisceau : pas d'overlap calcul�, pas besoin de faire l'extraction
				{
					pStart = NULL;
				}
				else
				{
					pStart = pMem->GetOverLap()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, 0));
				}
			}
			//sinon, on filtre sur le niveau de l'�cho
			else
			{
				pStart = pMem->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, 0));
			}

			//ajout des donn�es pour le channel
			//**************************
			// Boucle Echos
			//**************************
			if (pStart != NULL)
			{
				//prise en compte des parametres de hauteur min et max des donn�es
				std::int32_t minEcho = 0;
				std::int32_t maxEcho = size.y;

				if (minDepth >= 0 && maxDepth > 0)
				{
					minEcho = (minDepth - pTransData->GetOrigin3D().z) /
						cos(pChannelData->GetSteeringAngle()) / beamsSamplesSpacing - 1;
					maxEcho = (maxDepth - pTransData->GetOrigin3D().z) /
						cos(pChannelData->GetSteeringAngle()) / beamsSamplesSpacing + 1;

                    minEcho = std::max((std::int32_t)0, minEcho);
                    maxEcho = min((std::int32_t)size.y, maxEcho);
				}

				DataFmt * pEchoVectStart = pStart + minEcho;
				DataFmt * pEchoVectEnd = pStart + maxEcho;
				DataFmt * pEchoVect = pEchoVectStart;
				char* pFilterStart = pMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, 0));

				while (pEchoVect != pEchoVectEnd && !groundReached)
				{
					// On calcule la valeur de l echo en dB
					DataFmt value = *pEchoVect;

					// D�termination du respect des seuils
					if (!skipBeam && value >= detectThreshold && value <= detectMaxThreshold)
					{
						// OTK - FAE006 - on n'exclu plus les echos positifs
						//if(value <= 0)
						//{
						std::int32_t iEcho = pEchoVect - pEchoVectStart + minEcho;

						if (iEcho >= 0 && iEcho < (std::int32_t)(size.y))
						{
							char filterValue = *(pFilterStart + iEcho);

							//filtrage param�tr� par l'utilisateur et filtre auto par le fond d�tect�
							groundReached = (groundEcho <= iEcho);
							if (!filterValue && !groundReached)
							{
								//si la distance au dernier echo du banc est sup�rieure � la distance d'agr�gation 1D,
								//fermeture du banc 1D
								if (lastEchoShoal >= 0 && (distMaxNbEcho < (iEcho - lastEchoShoal - 1)))
								{
                                    pEchoGroup->SetEchoMax(lastEchoShoal);

									//ajout d'une copie groupe dans la liste de groupes finale du channel
									pChannelData->GetGroupsFinal().push_back(pEchoGroup->Clone());
									pEchoGroup = NULL;
								}

								//cr�ation �ventuelle d'un groupe d'�cho
								if (pEchoGroup == NULL)
								{
									pEchoGroup = new EchoGroup();
									if (pEchoGroup == NULL)
									{
										throw MEMORY_ALLOC_ERROR;
									}
									pEchoGroup->SetEchoMin(iEcho);
									//incrementer le compteur de bancs
									pEchoGroup->SetShoalId(m_pShoalIdMgr->IncrShoalId());
									pEchoGroup->GetShoalId()->SetFirstESU(m_ESUId);
									pEchoGroup->GetShoalId()->SetFirstPing(pingId);

									//ajout du groupe dans la liste de groupes initiale du channel
									pChannelData->GetGroupsInitial().push_back(pEchoGroup);
								}
								//ajout de l'�cho dans le groupe d'�cho courant
								lastEchoShoal = iEcho;
							}//if if(!*pFilter)
						}
						//}//if if(value <= 0)
					}//if if(value >= detectThreshold)
					pEchoVect++;
				} // fin boucle sur les echos

				//finalisation du faisceau
				if (pEchoGroup != NULL)
				{
                    pEchoGroup->SetEchoMax(lastEchoShoal);

					//ajout d'une copie groupe dans la liste de groupes finale du channel
					pChannelData->GetGroupsFinal().push_back(pEchoGroup->Clone());
				}
			}//fin if pStart

			//Unlock MemoryStruct

			if (pMem->GetOverLap())
			{
				pMem->GetOverLap()->Unlock();
			}
			pMem->UnLockOverLap();
			pMem->GetDataFmt()->Unlock();
			pMem->GetFilterFlag()->Unlock();

			//parcours de tous les groupes d'�chos
			for (size_t iEchoGroup = 0; iEchoGroup < pChannelData->GetGroupsInitial().size(); iEchoGroup++)
			{
				EchoGroup* pEchoGroup = pChannelData->GetGroupsInitial()[iEchoGroup];

				//cr�ation de la surface orthonorm�e 2D du groupe d'�chos
				ShoalExtractionGeometry::CreateEchoSurf2D(
					beamsSamplesSpacing, integrationDistanceX, verticalIntegrationDistance,
					angleSteering, angle3dB, true, pEchoGroup);
			}
		} // fin boucle sur les GetChannels()

	} // fin boucle transducteur
}


//*****************************************************************************
// Name : Aggregate2DEcho
// Description :
// Parameters : * (in)  PingFan *p
//				* (out)	std::vector<TransductData*>& pTransDataColl = liste des echos 1D pour chaque transducteur
// Return : void
//*****************************************************************************
void ShoalExtractionModule::Aggregate2DEcho(PingData& pingData)
{
	std::uint64_t pingId = pingData.GetPingId();

	Sounder *pSounder = pingData.GetPingFan()->m_pSounder;

	// Indépendamment pour chaque transducteur
	for (const auto& pTransData : pingData.GetTransducers())
	{
		// calcul de la distance d'int�gration minimale en vertical (en m)
		double verticalIntegrationDistance = pTransData->GetVerticalIntegrationDistance();

		//parcours de tous les faisceaux
		for (size_t iChannel = 0; iChannel < pTransData->GetChannels().size(); iChannel++)
		{
			ChannelData* pChannelData = pTransData->GetChannels()[iChannel];
			std::int32_t channel1Id = pChannelData->GetChannelId();

			//parcours de tous les groupes d'�chos
			for (size_t iEchoGroup = 0; iEchoGroup < pChannelData->GetGroupsInitial().size(); iEchoGroup++)
			{
				EchoGroup* pEchoGroup = pChannelData->GetGroupsInitial()[iEchoGroup];
				std::shared_ptr<ShoalId> pShoalId1 = pEchoGroup->GetShoalId()->GetLinked2DShoalId();
				std::shared_ptr<ShoalId> pShoalId3D = pEchoGroup->GetShoalId()->GetLinked3DShoalId();

				//Maj de la table d'historique des donn�es pour le banc
				if (pShoalId3D)
				{
					SetShoalTrace(pShoalId3D->GetShoalId(), m_ESUId, pingId, pingId);
				}

				//parcours de tous les faisceaux suivants
				for (size_t iChannel2 = iChannel + 1; iChannel2 < pTransData->GetChannels().size(); iChannel2++)
				{
					ChannelData* pChannelData2 = pTransData->GetChannels()[iChannel2];
					std::int32_t channel2Id = pChannelData2->GetChannelId();

					//parcours de tous les groupes d'�chos
					for (size_t iEchoGroup2 = 0; iEchoGroup2 < pChannelData2->GetGroupsInitial().size(); iEchoGroup2++)
					{
						EchoGroup* pEchoGroup2 = pChannelData2->GetGroupsInitial()[iEchoGroup2];

						//pour les faisceaux contigus, on v�rifie que les num�ro de bancs sont diff�rents
						//(optimisation non valide pour les faisceaux non contigus car on essaie de prendre en compte
						//l'espace inclus dans le banc sous le seuil)
						shared_ptr<ShoalId> pShoalId2 = pEchoGroup2->GetShoalId()->GetLinked2DShoalId();

						if (channel2Id - channel1Id > 1 || pShoalId1 != pShoalId2)
						{
							//si les enveloppes englobantes s'intersectent, propagation du num�ro de banc
							if (pEchoGroup->GetMarginsSurf2d().Intersect(pEchoGroup2->GetSurf2D()))
							{
								if (pShoalId2 && pShoalId2 != pShoalId1)
								{
									pShoalId2->SetLinked2DShoalId(pShoalId1);
								}

								shared_ptr<ShoalId> pShoalId3D2 = pEchoGroup2->GetShoalId()->GetLinked3DShoalId();
								if (pShoalId3D2 && pShoalId3D2 != pShoalId3D)
								{
									pShoalId3D2->SetLinked3DShoalId(pShoalId3D);
								}

								if (channel2Id - channel1Id > 1)
								{
									//ajouter les echos inclus dans l'espace entre les 2 surfaces (mm en dessous du seuil)
									//� la liste des echos du banc
									//(on ne tient pas compte des marges d'int�gration donc on ne prend pas GetMarginsSurf2d())
									ShoalExtractionGeometry::AggregateEmptySpace2D(
										pEchoGroup->GetSurf2D(), pEchoGroup2->GetSurf2D(),
										pTransData->GetTransId(), pChannelData->GetChannelId(),
										pChannelData2->GetChannelId(),
										pEchoGroup->GetShoalId(), pingData);
								}
							}
						}
					}
				}

				//purge de certaines donn�es 2D de l'�cho
				pEchoGroup->GetMarginsSurf2d().Alloc(0);
			}
		}
	}
}


//*****************************************************************************
// Name : AggregatePing
// Description : agr�gation du ping avec l'ensemble des Pings pr�c�dents
// Parameters : * (in)  PingData& pingData
// Return : void
//*****************************************************************************
void ShoalExtractionModule::AggregatePing(PingData* pingData)
{
	//d�terminer les distances d'int�gration
	double alongIntegrationDistance = m_parameter.GetAlongIntegrationDistance();

	//agr�gation sur tous les Pings pr�c�dent qui sont dans la marge d'int�gration
	PingDataMap::reverse_iterator iterPing = m_Pings.rbegin();
	int iPing = 0;
	bool pingTooFar = false;
	bool previousPing = true; // dans le cas du ping directement avant, on associe les surfaces 2D entre elles dans AggregatePings()
	std::uint64_t pingIdOutOfRange = 0;
	while (!pingTooFar && iPing < m_Pings.size())
	{
		PingData* pingData2 = (*iterPing).second;
		std::int32_t sounderId1 = pingData->GetSounderId();
		std::int32_t sounderId2 = pingData2->GetSounderId();

		//comparer uniquement des sondeurs identiques
		if (sounderId1 == sounderId2)
		{
			//si le ping n'est pas deja ferm�
			pingTooFar = pingData2->GetComplete();
			if (!pingTooFar)
			{
				//les 2 pings sont ils plus proches que le seuil fix� dans l'axe d'avancement du bateau
				pingTooFar = !pingData->GetBoundingBox().CloseTo(pingData2->GetBoundingBox(), alongIntegrationDistance, std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
			}

			//tant que les volumes englobant des Pings sont plus proches que les seuils fix�s
			if (!pingTooFar)
			{
				//agr�gation
				AggregatePings(pingData, pingData2, previousPing);
				previousPing = false;
			}
			//sinon fermeture des bancs qui sont sans donn�es depuis le ping en limite de distance
			else
			{
				pingIdOutOfRange = pingData2->GetPingId();
			}
		}

		iterPing++;
		iPing++;
	}

	//in use internal storage mode, a forceCloseshoals should be called to close shoals
	if (!m_parameter.GetUseInternalShoalStorage())
	{
		CloseData(pingIdOutOfRange, m_ESUId);
	}
}

//*****************************************************************************
// Name : AggregatePings()
// Description : agr�gation des deux Pings
// Parameters : * (in)  PingData* pingData1
//				* (in)	PingData* pingData2
//				* (in)	bool      previousPing
// Return : void
//*****************************************************************************
void ShoalExtractionModule::AggregatePings(PingData* pingData1, PingData* pingData2, bool previousPing)
{
	Sounder *pSounder1 = pingData1->GetPingFan()->m_pSounder;
	Sounder *pSounder2 = pingData2->GetPingFan()->m_pSounder;

	//existe-t-il un ping intercal� valide pour l'agr�gation 3D ?
	bool validInterPing = true;

	//pour chaque transducteur du ping1
	for (const auto pTransData1 : pingData1->GetTransducers())
	{
		const auto pTrans1 = pSounder1->GetTransducer(pTransData1->GetTransId());

		//traiter les transducteurs 2 a 2 sans mélanger les tranducteurs d'un mm ping
		const auto& pTransData2 = pingData2->GetTransducer(pTransData1->GetTransId());
		
		// déterminer les distances d'intégration
		const auto xIntegrationDistance = 0.5 * (pTransData1->GetIntegrationDistanceX() + pTransData2->GetIntegrationDistanceX());
		const double yIntegrationDistance = 0.5 * (pTransData1->GetIntegrationDistanceY() + pTransData2->GetIntegrationDistanceY());

		// calcul de la distance d'int�gration minimale en vertical (en m)
		double verticalIntegrationDistance = pTransData1->GetVerticalIntegrationDistance();

		//si les volumes englobant des donn�es des transducteurs sont plus proches que les seuils fix�s
		if (pTransData1->GetVol3DData().CloseTo(pTransData2->GetVol3DData(),
			xIntegrationDistance, yIntegrationDistance, verticalIntegrationDistance))
		{
			//pour chaque faisceau du ping1
			size_t iChannel1 = 0;
			auto iterChannel1 = pTransData1->GetChannels().begin();
			auto iterChannelEnd1 = pTransData1->GetChannels().end();
			while (iterChannel1 != iterChannelEnd1)
			{
				ChannelData* pChannelData1 = *iterChannel1;

				double  beamsSamplesSpacing = pTrans1->getBeamsSamplesSpacing();

				//pour chaque groupe d'�chos du ping1
				auto iterEcho1 = pChannelData1->GetGroupsInitial().begin();
				auto iterEchoEnd1 = pChannelData1->GetGroupsInitial().end();
				while (iterEcho1 != iterEchoEnd1)
				{
					EchoGroup* pEchoGroup1 = *iterEcho1;
					if (pEchoGroup1 != NULL && pEchoGroup1->GetShoalId() != NULL && pEchoGroup1->GetShoalId()->GetLinked3DShoalId())
					{
						std::shared_ptr<ShoalId> pLinkedShoalId1 = pEchoGroup1->GetShoalId()->GetLinked3DShoalId();

						//d�terminer l'ESU de d�but du banc correspondant au groupe d'echos
						std::int32_t minESU = pLinkedShoalId1->GetFirstESU();

						//d�terminer le ping de d�but du banc correspondant au groupe d'echos
						std::int32_t firstPing = pLinkedShoalId1->GetFirstPing();

						//pour chaque faisceau du ping2
						size_t iChannel2 = 0;
						auto iterChannel2 = pTransData2->GetChannels().begin();
						auto iterChannelEnd2 = pTransData2->GetChannels().end();
						while (iterChannel2 != iterChannelEnd2)
						{
							ChannelData* pChannelData2 = *iterChannel2;

							//pour chaque groupe d'�chos du ping2
							auto iterEcho2 = pChannelData2->GetGroupsInitial().begin();
							auto iterEchoEnd2 = pChannelData2->GetGroupsInitial().end();
							while (iterEcho2 != iterEchoEnd2)
							{
								EchoGroup* pEchoGroup2 = *iterEcho2;
								if (pEchoGroup2 != NULL && pEchoGroup2->GetShoalId() != NULL && pEchoGroup2->GetShoalId()->GetLinked3DShoalId())
								{
									std::shared_ptr<ShoalId> pLinkedShoalId2 = pEchoGroup2->GetShoalId()->GetLinked3DShoalId();

									if (!pLinkedShoalId2->GetClosed() && !pLinkedShoalId2->GetClosing() &&
										m_ESUId - pLinkedShoalId2->GetFirstESU() < MAX_ESU_HISTORY)
									{
										//on v�rifie que les num�ro de bancs sont diff�rents ou qu'il existe au moins 
										//un ping intercal� pour l'agr�gation 3D
										//(on essaie de prendre en compte l'espace inclus dans le banc sous le seuil)
										if (validInterPing || pLinkedShoalId1 != pLinkedShoalId2)
										{
											//si les polygones 3D sont suffisamment proches pour l'aggregation
											if (pEchoGroup1->GetVol3D().CloseTo(pEchoGroup2->GetVol3D(),
												xIntegrationDistance, yIntegrationDistance, verticalIntegrationDistance))
											{
												//MaJ de l'ESU de debut de banc
                                                minESU = min(minESU, (std::int32_t)pLinkedShoalId2->GetFirstESU());
												assert((std::int64_t)m_ESUId - minESU < MAX_ESU_HISTORY);

												if (pLinkedShoalId1 != pLinkedShoalId2)
												{
													//d�terminer le ping de d�but du banc correspondant au groupe d'echos
                                                    firstPing = min(firstPing, (std::int32_t)pLinkedShoalId2->GetFirstPing());

													//si une agr�gation avec le ping pr�c�dent a deja �t� faite, on propage du ping n vers le ping n-1
													if (pEchoGroup1->GetShoalId()->GetFromPreviousPing())
													{
														//suppression du banc de la table "lastping"
														ResetShoalTrace(pLinkedShoalId2->GetShoalId());
														//r�f�rence vers le banc li� du ping suivant
														pLinkedShoalId2->SetLinked3DShoalId(pLinkedShoalId1);
														pLinkedShoalId1->SetFirstPing(firstPing);
														pLinkedShoalId1->SetFirstESU(minESU);
														//MaJ r�f�rence de banc point� par les echos du groupe 2
														pEchoGroup2->GetShoalId()->SetLinked3DShoalId(pLinkedShoalId1);
													}
													//sinon, on propage du ping n-1 vers le ping n
													else
													{
														//suppression du banc de la table "lastping"
														ResetShoalTrace(pLinkedShoalId1->GetShoalId());
														//r�f�rence vers le banc du ping suivant
														pLinkedShoalId1->SetLinked3DShoalId(pLinkedShoalId2);
														pLinkedShoalId2->SetFirstPing(firstPing);
														pLinkedShoalId2->SetFirstESU(minESU);
														pLinkedShoalId1 = pLinkedShoalId2;
														pEchoGroup1->GetShoalId()->SetFromPreviousPing(true);
													}
													// OTK - association des surfaces 2D entre pings voisins
													if (previousPing)
													{
														pEchoGroup1->GetShoalId()->AddPreviousPing2D(pEchoGroup2->GetShoalId());
													}
												}

												//si il existe au moins un ping intercal� valide pour l'agr�gation
												if (validInterPing)
												{
#ifdef _TRACE_PERF

													trace_ofs << "AggregateEmptySpace3D Ping/Trans/Channel/Echo " << pingData1->GetPingId() << "/" << iTrans1 << "/" << iChannel1 << "/" << pEchoGroup1->GetEchoMin() << "-" << pEchoGroup1->GetEchomax();
													trace_ofs << std::endl;
													trace_ofs << "                      Ping/Trans/Channel/Echo " << pingData2->GetPingId() << "/" << iTrans1 << "/" << iChannel2 << "/" << pEchoGroup2->GetEchoMin() << "-" << pEchoGroup2->GetEchomax();
													trace_ofs << std::endl;

#endif

													//agr�gation de l'espace 3D d'un ping compris entre 2 volumes d'un mm banc
													validInterPing = ShoalExtractionGeometry::AggregateEmptySpace3D(
														iChannel1,
														iChannel2,
														pEchoGroup1,
														pEchoGroup2,
														pTransData1,
														pingData1->GetSounderId(),
														pingData1->GetPingId(),
														pingData2->GetPingId(),
														pLinkedShoalId2,
														m_pShoalIdMgr,
														m_Pings);
												}//if(validInterPing)
											}//si les polygones 3D sont suffisamment proches pour l'aggregation
										}//si les groupes d'�chos n'appartiennent pas d�j� au mm bancs
									}
								}//if(pEchoGroup2 != NULL)
								iterEcho2++;
							}//while(iterEcho2 != iterEchoEnd2)

							iChannel2++;
							iterChannel2++;
						}//pour chaque faisceau du ping2

						//MaJ du dernier ping pour le banc du groupe d'�chos
						UpdateShoalTrace(pLinkedShoalId1->GetShoalId(), minESU, firstPing, pingData1->GetPingId());
					}
					iterEcho1++;
				}//pour chaque groupe d'�chos du ping1

				iChannel1++;
				iterChannel1++;
			}//pour chaque faisceau du ping1
		}//si les volumes englobant des donn�es des transducteurs sont plus proches que les seuils fix�s
	}//pour chaque transducteur du ping1
}

//*****************************************************************************
// Name : CloseData
// Description : Fermeture des donn�es en limite de distance
// Parameters : * (in) int pingIdOutOfRange
//				* (in) int currentESUId
// Return : void
//*****************************************************************************
void ShoalExtractionModule::CloseData(std::uint64_t pingIdOutOfRange, std::uint64_t currentESUId)
{
	// Purge des bancs ouverts et invalides
	PurgeOpenShoals();

	// Purge de l'historique des pings
	PurgePingHistory();

	// Fermeture des pings en limite de distance
	ClosePings(pingIdOutOfRange);

	// Fermeture des bancs qui n'ont pas de donn�es depuis pingIdOutOfRange
	CloseShoals(pingIdOutOfRange, currentESUId);
}

//*****************************************************************************
// Name : ClosePings
// Description : Fermeture des pings en limite de distance
// Parameters : * (in) int pingIdOutOfRange
//				* (in) int currentESUId
// Return : void
//*****************************************************************************
void ShoalExtractionModule::ClosePings(std::uint64_t pingIdOutOfRange)
{
	//fermeture de tous les Pings pr�c�dents
	PingDataMap::iterator iterPing = m_Pings.begin();

	std::uint64_t pingId = 0;
	PingData* pPingData = NULL;

	while (pingId <= pingIdOutOfRange && iterPing != m_Pings.end())
	{
		pingId = iterPing->first;
		pPingData = iterPing->second;

		if (pingId <= pingIdOutOfRange && pPingData != NULL)
		{
			//initialisation des donn�es statistiques pour le ping en limite de distance
			ShoalExtractionStat::CompletePingData(pPingData, m_parameter.GetMaxThreshold()*100.0);
		}

		iterPing++;
	}
}

//*****************************************************************************
// Name : CloseShoals
// Description : Fermeture des bancs qui n'ont pas de donn�es agr�g�es
//				 depuis le ping sp�cifi� ou si l'historique max d'ESU est atteinte pour ce banc
// Parameters : * (in) int pingIdOutOfRange
//				* (in) int currentESUId
// Return : void
//*****************************************************************************
void ShoalExtractionModule::CloseShoals(std::uint64_t pingIdOutOfRange, std::uint64_t currentESUId)
{
	//parcours de tous les bancs ouverts pour savoir ceux qui n'ont pas de donn�es depuis pingIdOutOfRange
	ShoalTraceMap tmpMap;

	tmpMap.insert(m_mapShoalTrace.begin(), m_mapShoalTrace.end());

	ShoalTraceMap::reverse_iterator iterShoals = tmpMap.rbegin();
	std::vector<std::uint32_t> toDelete;

	while (iterShoals != tmpMap.rend())
	{
		//si la derni�re MaJ du banc est ant�rieure a la limite pingIdOutOfRange
		// ou si l'historique max d'ESU est atteinte pour ce banc
		std::uint64_t shoalId = (*iterShoals).first;
		std::uint64_t firstESU = (*iterShoals).second.firstESU;
		std::uint64_t firstPing = (*iterShoals).second.firstPing;
		std::uint64_t lastPing = (*iterShoals).second.lastPing;

		if ((lastPing <= pingIdOutOfRange && lastPing >= 0) ||
			(currentESUId - firstESU >= MAX_ESU_HISTORY))
		{
			//fermeture du banc
			CloseShoal(shoalId, firstPing, lastPing);
			toDelete.push_back(shoalId);
		}//if((*iterShoals).second < pingIdOutOfRange)

		iterShoals++;
	}//while(iterShoals != m_mapShoalLastPing.rend())

	//suppression des bancs ferm�s
	std::vector<std::uint32_t>::iterator toDeleteIter = toDelete.begin();
	while (toDeleteIter != toDelete.end())
	{
		ShoalTraceMap::iterator iterMap = m_mapShoalTrace.find(*toDeleteIter);
		if (iterMap != m_mapShoalTrace.end())
		{
			m_mapShoalTrace.erase(iterMap);
		}
		toDeleteIter++;
	}
}


//*****************************************************************************
// Name : CloseShoal
// Description : Fermeture du banc d'id donné (le banc n'a plus de données depuis le ping spécifié)
// Parameters : * (in) int shoalId
//				* (in) int lastPing
// Return : void
//*****************************************************************************
void ShoalExtractionModule::CloseShoal(std::uint64_t shoalId, std::uint64_t firstPing, std::uint64_t lastPing)
{
	int sounderId = -1;

	//parcours de tous les pings recherchés
	auto iterPing = m_Pings.find(firstPing);
	auto iterPingEnd = m_Pings.find(lastPing);

	if (iterPing != m_Pings.end() && iterPingEnd != m_Pings.end())
	{		
		//incrémenter le dernier itérateur pour obtenir la condition de sortie de la boucle
		++iterPingEnd;

		ShoalData* pShoalData = nullptr;
		const PingData* pPingData = nullptr;
		while (iterPing != iterPingEnd)
		{
			pPingData = (*iterPing).second;

			//gestion des Pings antérieurs au lastPing
			assert(pPingData->GetPingId() <= static_cast<int32_t>(lastPing));

			//comparer uniquement des sondeurs identiques
			if (sounderId == pPingData->GetSounderId() || sounderId < 0)
			{
				//données d'un ping
				PingData* pShoalPing = nullptr;

				//un banc ne peut contenir les données que d'un transducteur,
				//une fois qu'il est trouvé, plus la peine de boucler sur les autres
				auto transducerFound = false;
				
				for (const auto pTrans : pPingData->GetTransducers())
				{
					TransductData* pShoalTrans = nullptr;

					//parcours de tous les faisceaux
					auto iterChannel = pTrans->GetChannels().begin();
					auto iterChannelEnd = pTrans->GetChannels().end();
					while (iterChannel != iterChannelEnd)
					{
						ChannelData* pChannelData = *iterChannel;
						ChannelData* pShoalChannelData = nullptr;

						//parcours de tous les groupes d'échos
						auto iterGroup = pChannelData->GetGroupsFinal().begin();
						auto iterGroupEnd = pChannelData->GetGroupsFinal().end();
						while (iterGroup != iterGroupEnd)
						{
							EchoGroup* pEchoGroup = *iterGroup;

							if (pEchoGroup != nullptr && pEchoGroup->GetShoalId())
							{
								std::shared_ptr<ShoalId> pEchoGroupShoalId = pEchoGroup->GetShoalId()->GetLinked3DShoalId();
								//si le banc du groupe d'écho n'est pas fermé
								if (pEchoGroupShoalId && (!pEchoGroupShoalId->GetClosed() || !pEchoGroupShoalId->GetClosing()))
								{
									//si le groupe d'écho appartient au banc
									if (pEchoGroupShoalId->GetShoalId() == shoalId)
									{
										if (pShoalData == nullptr)
										{
											m_uiExternShoalId++;
											pShoalData = new ShoalData();
											pShoalData->SetExternId(static_cast<uint32_t>(m_uiExternShoalId));
											pShoalData->SetTransId(pTrans->GetTransId());
											pShoalData->SetTransName(pTrans->GetTransName());

											//initialisation d'une donnée stat
											pShoalData->SetShoalStat(new ShoalStat());
											pShoalData->GetShoalStat()->SetVerticalIntegrationDistance(pTrans->GetVerticalIntegrationDistance());
											pShoalData->GetShoalStat()->SetMinAngle(pTrans->GetMinAngle());
											pShoalData->GetShoalStat()->SetMaxAngle(pTrans->GetMaxAngle());
											pShoalData->GetShoalStat()->SetParameter(&m_parameter);
										}
										if (pShoalPing == nullptr)
										{
											pShoalPing = new PingData();
											pShoalData->GetPings().push_back(pShoalPing);

											pShoalPing->SetComplete(pPingData->GetComplete());
											pShoalPing->SetPingFan(pPingData->GetPingFan());
											pShoalPing->SetPingId(pPingData->GetPingId());
											pShoalPing->SetSounderId(pPingData->GetSounderId());
											pShoalPing->SetNavigations({*pPingData->GetNavigations().find(static_cast<unsigned short>(pChannelData->GetSoftwareChannelId()))});
										}
										if (pShoalTrans == nullptr)
										{
											pShoalTrans = new TransductData();
											pShoalPing->AddTransducer(pShoalTrans);

											pShoalTrans->SetTransId(pTrans->GetTransId());
											pShoalTrans->SetBeamsSamplesSpacing(pTrans->GetBeamsSamplesSpacing());
											pShoalTrans->SetOrigin3D(pTrans->GetOrigin3D());
											pShoalTrans->SetOrigin3DGPS(pTrans->GetOrigin3DGPS());
											pShoalTrans->SetPitch(pTrans->GetPitch());
										}
										if (pShoalChannelData == nullptr)
										{
											pShoalChannelData = new ChannelData();
											pShoalTrans->AddChannel(pShoalChannelData);

											pShoalChannelData->SetChannelId(pChannelData->GetChannelId());
											pShoalChannelData->SetGroundDepth(pChannelData->GetGroundDepth());
											pShoalChannelData->SetSteeringAngle(pChannelData->GetSteeringAngle());
											pShoalChannelData->SetAlongAngle(pChannelData->GetAlongAngle());
											pShoalChannelData->SetAthwartAngle(pChannelData->GetAthwartAngle());
											pShoalChannelData->SetPitch(pChannelData->GetPitch());
											pShoalChannelData->SetAcousticFrequency(pChannelData->GetAcousticFrequency());

											//FRE - 24/07/2009 - conservation integrale des données dans le banc extrait
											pShoalChannelData->SetDeleteData(true);
										}

										//FRE - 24/07/2009 - conservation integrale des données dans le banc extrait
										// et déréférencement dans le container initial
										pShoalChannelData->GetGroupsFinal().push_back(pEchoGroup);
										*iterGroup = nullptr;

										if (pShoalData->GetShoalId() == nullptr)
										{
											pShoalData->SetShoalId(pEchoGroupShoalId.get());
											pShoalData->SetTransId(pTrans->GetTransId());
											pShoalData->SetSounderId(pPingData->GetSounderId());
											sounderId = pShoalData->GetSounderId();
											pEchoGroupShoalId->SetClosing(true);
										}

										transducerFound = true;
									}// if(pEchoGroup->GetShoalId()->GetLinkedShoalId()->GetShoalId() == shoalId)
								}//if(!pEchoGroup->GetShoalId()->GetLinkedShoalId()->closed)
							}

							++iterGroup;
						}//	while(iterGroup != iterGroupEnd)

						++iterChannel;
					}//while(iterChannel != iterChannelEnd)
					
					//un banc ne peut contenir les données que d'un transducteur,
					//une fois qu'il est trouvé, plus la peine de boucler sur les autres
					if (transducerFound)
					{
						break;
					}
				} // Parcours des transducteurs	
			}//comparer uniquement des sondeurs identiques
			++iterPing;
		}//while(iterPing != iterPingEnd)

		//si des données on été trouvées pour ce banc
		if (pShoalData != nullptr && pPingData != nullptr)
		{
			//Calcul des statistiques du banc
			if (ComputeShoalStats(pShoalData))
			{
#ifdef _TRACE_PERF
				trace_ofs << "\tSerialize Shoal ID : " << pShoalData->GetExternId() << " intern ID : " << pShoalData->GetShoalId()->GetLinked3DShoalId()->GetShoalId() << std::endl;
				pShoalData->Serialize(m_parameter.GetName());
#endif

				//si on utilise le stockage interne
				if (m_parameter.GetUseInternalShoalStorage())
				{
					GetShoals().push_back(pShoalData);
				}
				//sinon on utilise le processus de consommation de donn�es du module
				else
				{
					//serialisation des stats
					pShoalData->SerializeCSV(*GetCsvStream(pShoalData->GetSounderId(), pShoalData->GetTransId()));

					// on ajoute l'objet r�sultat au moduleOutputContainer
					ShoalExtractionOutput * pShoalExtractionOutput = ShoalExtractionOutput::Create();
					MovRef(pShoalExtractionOutput);

					pShoalExtractionOutput->m_pClosedShoal = pShoalData;
					pShoalExtractionOutput->PrepareForSerialization(m_parameter, pPingData->GetPingFan());
					// OTK - on synchronize l'acc�s au container car en mode multithread, il est acc�d� 
					// par le thread de lecture (outputmanager) et l'IHM (pour affichage des bancs)
					m_outputContainer.Lock();
					m_outputContainer.AddOutput(pShoalExtractionOutput);
					// on n'en a plus besoin ici, on le unref
					MovUnRefDelete(pShoalExtractionOutput);
					m_outputContainer.Unlock();
				}
			}
			else
			{
				delete pShoalData;
			}
		}
	}
}

//*****************************************************************************
// Name : PurgeOpenShoals
// Description : Purge des bancs ouverts et invalides
// Parameters : void
// Return : void
//*****************************************************************************
void ShoalExtractionModule::PurgeOpenShoals()
{
	//parcours de tous les bancs ouverts invalides (firstESU == std::numeric_limits<int>::max())
	ShoalTraceMap::iterator iterShoals = m_mapShoalTrace.begin();
	while (iterShoals != m_mapShoalTrace.end())
	{
		std::uint64_t firstESU = (*iterShoals).second.firstESU;

		if (firstESU == std::numeric_limits<int>::max())
		{
			iterShoals = m_mapShoalTrace.erase(iterShoals);
		}
		else
		{
			iterShoals++;
		}

	}
}

//*****************************************************************************
// Name : PurgePingHistory
// Description : Purge de l'historique des Pings
// Parameters : void
// Return : void
//*****************************************************************************
void ShoalExtractionModule::PurgePingHistory()
{
	//parcours de tous les ping de l'historique tant qu'ils sont clos
	bool closedPing = true;

	PingDataMap::iterator iterPing = m_Pings.begin();

	bool stop = m_Pings.size() == 0;
	while (!stop)
	{
		std::uint64_t pingId = iterPing->first;

		//parcours de tous les transducteurs
		for (size_t iTrans = 0; iTrans < (*iterPing).second->GetTransducers().size() && closedPing; iTrans++)
		{
			TransductData* pTrans = (*iterPing).second->GetTransducers()[iTrans];

			//parcours de tous les faisceaux
			for (size_t iChannel = 0; iChannel < pTrans->GetChannels().size() && closedPing; iChannel++)
			{
				ChannelData* pChannelData = pTrans->GetChannels()[iChannel];

				//parcours de tous les groupes d'�chos
				for (size_t iEchoGroup = 0; iEchoGroup < pChannelData->GetGroupsFinal().size() && closedPing; iEchoGroup++)
				{
					EchoGroup* pEchogroup = pChannelData->GetGroupsFinal()[iEchoGroup];

					if (pEchogroup != NULL)
					{
						//tout groupe d'�chos appartenant a un banc ferm� ou en fermeture est mis � NULL
						//dans le ping
						//--> si il existe un groupe d'�cho echo non null, le ping n'est pas vide
						closedPing = false;
					}
				}//for(int iEchoGroup = 0; iEchoGroup < pChannelData->GetGroups().size(); iEchoGroup++)
			}//for(int iChannel = 0; iChannel < pTrans->GetChannels().size(); iChannel++)
		}//for(int iTrans = 0; iTrans < (*iterPing)->GetTransducers().size() && !openShoal; iTrans++)		

		if (closedPing)
		{
			m_lock.Lock();
			PingData* pPingData = iterPing->second;
			m_PingFansToUnRef.push_back(pPingData->GetPingFan());
			delete pPingData;
			iterPing = m_Pings.erase(iterPing);
			m_lock.Unlock();
		}
		else
		{
			iterPing++;
		}
		stop = (iterPing == m_Pings.end()) || m_Pings.size() == 0 || !closedPing;

	}//while(iterPing != m_Pings.end())
}

//*****************************************************************************
// Name : ComputeShoalBBox
// Description : Calcul de la boite englobante du banc
// Parameters : * (in) ShoalData* pShoalData
// Return : void
//*****************************************************************************
void ShoalExtractionModule::ComputeShoalBBox(ShoalData* pShoalData)
{
	pShoalData->GetBbox().Reset();

	//parcours de tous les ping
	for (size_t iPing = 0; iPing < pShoalData->GetPings().size(); iPing++)
	{
		PingData* pPingData = pShoalData->GetPings()[iPing];

		//parcours de tous les transducers (normalement 1 seul)
		assert(pPingData->GetTransducers().size() == 1);
		for (size_t iTrans = 0; iTrans < pPingData->GetTransducers().size(); iTrans++)
		{
			TransductData* pTransData = pPingData->GetTransducers()[iTrans];

			//parcours de tous les faisceaux
			for (size_t iChannel = 0; iChannel < pTransData->GetChannels().size(); iChannel++)
			{
				ChannelData* pChannelData = pTransData->GetChannels()[iChannel];
				//parcours de tous les groupes d'�chos
				for (size_t iEchoGroup = 0; iEchoGroup < pChannelData->GetGroupsFinal().size(); iEchoGroup++)
				{
					EchoGroup* pEchoGroup = pChannelData->GetGroupsFinal()[iEchoGroup];
					pShoalData->GetBbox().Update(pEchoGroup->GetVol3D().GetBoundingBox());
				}
			}
		}
	}

#ifdef _TRACE_PERF
	trace_ofs << "\nComputeShoalBBox Shoal " << pShoalData->GetShoalId()->GetLinked3DShoalId()->GetShoalId();
	trace_ofs << "\tBBox " << pShoalData->GetBbox().GetDeltaX() << " " << pShoalData->GetBbox().GetDeltaY() << " " << pShoalData->GetBbox().GetDeltaZ();
	trace_ofs << std::endl;
#endif
}


//*****************************************************************************
// Name : ComputeShoalStats
// Description : Calcul des statistiques du banc
// Parameters : * (in) ShoalData* pShoalData
// Return : void
//*****************************************************************************
bool ShoalExtractionModule::ComputeShoalStats(ShoalData* pShoalData)
{
	bool result = false;

	ComputeShoalBBox(pShoalData);

	//la boite englobante align�e sur les axes sert de premier filtre sur la taille des bancs
	// mais il faut calculer les longueurs maximales possibles dans cette boite align�e
	double xyMax = sqrt(
		pow(pShoalData->GetBbox().GetDeltaX(), 2) + pow(pShoalData->GetBbox().GetDeltaY(), 2));
	double zMax = pShoalData->GetBbox().GetDeltaZ() * sqrt((float)2);

	double filterXYMin = sqrt(m_parameter.GetMinWidth() * m_parameter.GetMinWidth() +
		m_parameter.GetMinLength() * m_parameter.GetMinLength());

	if (xyMax > filterXYMin && zMax > m_parameter.GetMinHeight())
	{
#ifdef _TRACE_PERF
		trace_ofs << "ComputeShoalStats Shoal " << std::endl;
#endif
		//calcul des stat du banc
		result = ShoalExtractionStat::ComputeShoalStats(pShoalData, m_parameter.GetMaxThreshold()*100.0);
	}

	return result;
}

//*****************************************************************************
// Name : SetShoalTrace
// Description : Initialisation des param�tres du banc concernant les derni�res donn�es associ�es
// Parameters : * (in) std::uint32_t shoalId
//				* (in) std::uint32_t firstESU
//				* (in) std::uint64_t firstPing
//				* (in) std::uint64_t lastPing
// Return : void
//*****************************************************************************
void ShoalExtractionModule::SetShoalTrace(std::uint64_t shoalId, std::uint64_t firstESU, std::uint64_t firstPing, std::uint64_t lastPing)
{
	assert(shoalId > 0);

	m_mapShoalTrace[shoalId] = SShoalTrace(firstESU, firstPing, lastPing);
}

//*****************************************************************************
// Name : ResetShoalTrace
// Description : RaZ des param�tres du banc concernant les derni�res donn�es associ�es
// Parameters : * (in) std::uint32_t shoalId
//				* (in) std::int32_t firstESU
//				* (in) std::uint32_t firstPing
//				* (in) std::uint32_t lastPing
// Return : void
//*****************************************************************************
void ShoalExtractionModule::ResetShoalTrace(std::uint64_t shoalId)
{
	assert(shoalId > 0);
	m_mapShoalTrace[shoalId] = SShoalTrace(std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max());
}

//*****************************************************************************
// Name : UpdateShoalTrace
// Description : MaJ des param�tres du banc concernant les derni�res donn�es associ�es
// Parameters : * (in) std::uint32_t shoalId
//				* (in) std::int32_t firstESU
//				* (in) std::uint64_t firstPing
//				* (in) std::uint64_t lastPing
// Return : void
//*****************************************************************************
void ShoalExtractionModule::UpdateShoalTrace(std::uint64_t shoalId, std::uint64_t firstESU, std::uint64_t firstPing, std::uint64_t lastPing)
{
	assert(shoalId > 0);

	ShoalTraceMap::iterator iter = m_mapShoalTrace.find(shoalId);
	if (iter != m_mapShoalTrace.end())
	{
		(*iter).second.firstESU = min(firstESU, (*iter).second.firstESU);
		(*iter).second.firstPing = min(firstPing, (*iter).second.firstPing);
		(*iter).second.lastPing = std::max(lastPing, (*iter).second.lastPing);
	}
}

//*****************************************************************************
// Name : GetCsvStream
// Description : gestion des flux de sortie csv
// Parameters : * (in) std::uint32_t sounderId
//				* (in) std::uint32_t transId
// Return : void
//*****************************************************************************
std::ofstream* ShoalExtractionModule::GetCsvStream(std::uint32_t sounderId, std::uint32_t transId)
{
	std::ofstream* result = NULL;
	std::ostringstream sstream;
	sstream << "shoals_S" << sounderId << "_T" << transId;
	std::string streamName = sstream.str();

	std::map<std::string, std::ofstream*>::iterator iterStream = m_mapCsvStream.find(streamName);
	if (iterStream != m_mapCsvStream.end())
	{
		result = iterStream->second;
	}
	else
	{
		std::string fileName = m_parameter.GetRecordPath() + '/' +
			streamName + ".csv";
		result = new ofstream(fileName.c_str());

		//s�parateur local pour les d�cimales
		/*
		std::locale loc("");
		result->imbue(loc);
		*/
		m_mapCsvStream[streamName] = result;
	}

	return result;
}

//*****************************************************************************
// Name : ForcePurge
// Description : Purge forc�e de toutes les donn�es
// Parameters : void
// Return : void
//*****************************************************************************
void ShoalExtractionModule::ForcePurge()
{
	//d�sactivation pour ne pas obtenir de nouvelles actions
	m_actionStack.Disable();
	//reset des actions de calcul restantes
	m_actionStack.Reset();
	//r�activation
	m_actionStack.Enable();

	//destruction des pings restants
	m_mapShoalTrace.clear();

	PingDataMap::iterator iterPing = m_Pings.begin();
	while (iterPing != m_Pings.end())
	{
		PingFan * pFan = iterPing->second->GetPingFan();
		MovUnRefDelete(pFan);
		delete iterPing->second;
		iterPing++;
	}
	m_Pings.clear();
	m_pShoalIdMgr->Reset();

	m_ReaderPingFanCount = 0;
}

//*****************************************************************************
// Name : ForceCloseShoals
// Description : Fermeture forc�e de tous les bancs
// Parameters : void
// Return : void
//*****************************************************************************
void ShoalExtractionModule::ForceCloseShoals()
{
#ifdef _USE_WORKER_THREAD
	//attente de traitement de tous les pings de la liste de traitement
	m_actionStack.WaitFinished();
#endif

	//fermeture forc�e de tous les pings et de tous les bancs
	ClosePings(std::numeric_limits<int>::max());
	CloseShoals(0, std::numeric_limits<int>::max());
}

void ShoalExtractionModule::WaitExtractionActions()
{
#ifdef _USE_WORKER_THREAD
	//attente de traitement de tous les pings de la liste de traitement
	m_actionStack.WaitFinished();
#endif
}

void ShoalExtractionModule::ESUStart(ESUParameter * pWorkingESU)
{
#ifndef _USE_WORKER_THREAD
	IncESUId();
#else
	m_actionStack.ScheduleIncESUId();
#endif

	if (getEnable())
	{
		// l'ESU a �t� utilis�e : on doit le savoir pour l'afficher, par exemple.
		pWorkingESU->SetUsedByShoal();

		//init d'un nouvel ESU
#ifndef _USE_WORKER_THREAD
		ESUStart();
#else
		m_actionStack.ScheduleESUStart();
#endif

#ifdef _TRACE_PERF
		trace_ofs << "\nESUStart" << std::endl;
#endif
	}
}

void ShoalExtractionModule::IncESUId()
{
	m_ESUId++;
}

void ShoalExtractionModule::ESUStart()
{
	m_pShoalIdMgr->InitESU(m_ESUId);
}

void ShoalExtractionModule::ESUEnd(ESUParameter * pWorkingESU, bool abort)
{
	if (getEnable())
	{
#ifdef _TRACE_PERF
		trace_ofs << "\nESUEnd" << std::endl;
#endif

		//fermeture de tous les bancs d'historique trop important
#ifndef _USE_WORKER_THREAD
	//monothread
		ESUEnd();
#else
	//schedule ESU end on main stack in order to perform it once main stack action list is consumed
		m_actionStack.ScheduleESUEnd();
#endif
	}
}

void ShoalExtractionModule::ESUEnd()
{
	// Fermeture des bancs qui depassent l'historique d'ESU
	CloseShoals(0, m_ESUId + 1);

	//fermeture des anciens ESU
	if (m_ESUId >= MAX_ESU_HISTORY + 1)
	{
		m_pShoalIdMgr->ResetESU(m_ESUId - MAX_ESU_HISTORY - 1);
	}
}

void ShoalExtractionModule::onEnableStateChange()
{
	if (!getEnable())
	{
		// pas de distinction monothread multithread car le forcepurge annule
		// et attend la fin de la pile d'action

		// Purge forc�e de toutes les donn�es
		ForcePurge();

		//purge des id internes de bancs
		m_pShoalIdMgr->Reset();
		//init d'un nouvel ESU
		m_pShoalIdMgr->InitESU(m_ESUId);

		m_ReaderPingFanCount = 0;
	}
}

void ShoalExtractionModule::Restart()
{
	if (getEnable())
	{
		setEnable(false);
		setEnable(true);
	}
}

void ShoalExtractionModule::SounderChanged(std::uint32_t sounderId)
{
	// Purge forc�e de toutes les donn�es
	ForcePurge();

	m_ESUId = 0;
	m_uiExternShoalId = 0;

	//purge des id internes de bancs
	m_pShoalIdMgr->Reset();

	m_ReaderPingFanCount = 0;
}

void ShoalExtractionModule::StreamClosed(const char *streamName)
{
	ForceCloseShoals();
}
void ShoalExtractionModule::StreamOpened(const char *streamName)
{

}


