#include "ShoalExtraction/ShoalExtractionOutput.h"

#include "M3DKernel/datascheme/HacObjectMgr.h"
#include "M3DKernel/datascheme/Sounder.h"
#include "BaseMathLib/Vector3.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"

#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/shoalstat.h"
#include "ShoalExtraction/data/pingdata.h"
#include "ShoalExtraction/data/pingshoalstat.h"

#include "M3DKernel/output/MovOutput.h"

#include "ShoalExtraction/ShoalExtractionParameter.h"

#include <ctime>

using namespace shoalextraction;

ShoalExtractionOutput::ShoalExtractionOutput(void) :
	m_pClosedShoal(NULL)
{
}

ShoalExtractionOutput::~ShoalExtractionOutput(void)
{
	if (m_pClosedShoal != NULL)
	{
		delete m_pClosedShoal;
		m_pClosedShoal = NULL;
	}
}

void ShoalExtractionOutput::XMLSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor)
{
	std::string str = "";

	// tableau des chaines de caract�res
	std::vector<std::string> vectStr;

	// OTK - FAE006 - on calcule une fois pour toute l'ESU les descripteurs de trames
	sndsetDescriptor snddesc;
	shipnavDescriptor shipnavdesc;
	CMvNetDataXMLSndset::ComputeDescriptor(&tramaDescriptor, snddesc);
	CMvNetDataXMLShipnav::ComputeDescriptor(&tramaDescriptor, shipnavdesc);

	// TRAME SHIPNAV de d�but de banc
	str = m_MvNetDataXMLShipnavStart.createXMLSelectedOnly(shipnavdesc);
	vectStr.push_back(str);

	// TRAMES SNDSET (1 par channel)
	size_t nbSndSet = m_MvNetDataXMLSndSets.size();
	for (size_t i = 0; i < nbSndSet; i++)
	{
		str = m_MvNetDataXMLSndSets[i].createXMLSelectedOnly(snddesc);
		vectStr.push_back(str);
	}
	// On nettoie le vecteur une fois qu'on n'en a plus besoin (une seule serialisation)
	m_MvNetDataXMLSndSets.clear();

	// TRAME Extraction Parameters
	str = m_MvNetDataXMLShoalAnset.createXMLSelectedOnly(&tramaDescriptor);
	vectStr.push_back(str);

	// TRAME Extraction
	str = m_MvNetDataXMLShoal.createXMLSelectedOnly(&tramaDescriptor);
	vectStr.push_back(str);

	// TRAME SHIPNAV de fin de banc
	str = m_MvNetDataXMLShipnavStop.createXMLSelectedOnly(shipnavdesc);
	vectStr.push_back(str);

	movOutput->AppendXML(vectStr);
}


void ShoalExtractionOutput::CSVSerialize(MovOutput* movOutput, BaseKernel::TramaDescriptor& tramaDescriptor)
{
	// ------ Extraction des données de la trame
	const auto sounderSetDescriptor = CMvNetDataXMLSndset::BuildDescriptor(&tramaDescriptor);
	const auto shipNavDescriptor = CMvNetDataXMLShipnav::BuildDescriptor(&tramaDescriptor);

	// ------ Serialisation des infos de navigation
	auto navDataStr = std::string();

	// Ajout de la date système en premier
	time_t rawtime;
	time(&rawtime);
	navDataStr.append(CMvNetDataXMLShipnav::ToStr(HacTime(rawtime, 0)));
	navDataStr.append(";");

	// Ajout des données de navigation
	navDataStr.append(m_MvNetDataXMLShipnavStart.createCSV(shipNavDescriptor));

	// ------ Serialisation des paramètre d'extraction
	auto extractStr = std::string();
	// Trame des parametres d'extraction
	extractStr.append(m_MvNetDataXMLShoalAnset.createCSV(&tramaDescriptor));
	// Trame d'extraction (sans echos : trop de lignes en CSV !)
	extractStr.append(m_MvNetDataXMLShoal.createCSV(&tramaDescriptor));


	// ------ Sérialisation des données SndSet
	auto serializedStr = std::string();
	for (const auto& sndSet : m_MvNetDataXMLSndSets)
	{
		serializedStr.append(navDataStr);
		serializedStr.append(sndSet.createCSV(sounderSetDescriptor));
		serializedStr.append(extractStr);
		serializedStr.append("\n");
	}

	// On nettoie le vecteur une fois qu'on n'en a plus besoin (une seule serialisation)
	m_MvNetDataXMLSndSets.clear();

	if (!serializedStr.empty())
	{
		movOutput->Write(serializedStr);
	}
}

void ShoalExtractionOutput::XMLHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor)
{
	std::string header = CMvNetDataXML::createXMLHeader("MOVIES_EIShoal", 1, 0);
	header.append(CMvNetDataXML::createXMLFooter());
	movOutput->CreateXML(header);
}

void ShoalExtractionOutput::CSVHeaderSerialize(MovOutput * movOutput, BaseKernel::TramaDescriptor & tramaDescriptor)
{
	if (movOutput != NULL)
	{
		std::string header = "";

		// message description node cfg
		header += "MOVIES_EIShoal";

		//titre des colonnes

		// Titres shipnav debut de banc
        header += tramaDescriptor.GetTitlesByCat("MOVIES_EIShoal\\shipnav");

		// Titres sndset
		header += tramaDescriptor.GetTitlesByCat("MOVIES_EIShoal\\sndset");
		
		// Titres des parametres d'extraction
        header += tramaDescriptor.GetTitlesByCat("MOVIES_EIShoal\\Extraction Parameter");

		// Titres de l'extraction
        header += tramaDescriptor.GetTitlesByCat("MOVIES_EIShoal\\Extraction", true);

		header += "\n";

		movOutput->Write(header);
	}
}

// renseigne les objets n�cessaires � la s�rialisation (donn�es de navigation, etc...)
void ShoalExtractionOutput::PrepareForSerialization(ShoalExtractionParameter & params, PingFan * pFan)
{
	// Trames Shipnav (donn�es de navigation) : 1 en d�but de banc, une en fin de banc
	size_t pingNb = m_pClosedShoal->GetPings().size();

	assert(pingNb != 0); // on a forc�ment un ping ....

	// nav en d�but de banc
	shoalextraction::Navigation nav = m_pClosedShoal->GetPings()[0]->GetNavigation();
	m_MvNetDataXMLShipnavStart.m_acquisitionTime = nav.time.m_TimeCpu + nav.time.m_TimeFraction / 10000.0;
	m_MvNetDataXMLShipnavStart.m_roll = RAD_TO_DEG(nav.roll);
	m_MvNetDataXMLShipnavStart.m_pitch = RAD_TO_DEG(nav.pitch);
	m_MvNetDataXMLShipnavStart.m_heave = nav.heave;
	m_MvNetDataXMLShipnavStart.m_latitude = nav.latitude; // deja en �
	m_MvNetDataXMLShipnavStart.m_longitude = nav.longitude; // deja en �
	m_MvNetDataXMLShipnavStart.m_groundspeed = -1; // not available
	m_MvNetDataXMLShipnavStart.m_groundcourse = RAD_TO_DEG(nav.groundCourse);
	m_MvNetDataXMLShipnavStart.m_heading = RAD_TO_DEG(nav.heading);
	m_MvNetDataXMLShipnavStart.m_surfspeed = nav.surfSpeed;
	m_MvNetDataXMLShipnavStart.m_driftcourse = -1; // not available
	m_MvNetDataXMLShipnavStart.m_driftspeed = -1; // not available
	m_MvNetDataXMLShipnavStart.m_altitude = -10000000; // not available
	m_MvNetDataXMLShipnavStart.m_depth = nav.depth*0.001;

	// nav en fin de banc
	nav = m_pClosedShoal->GetPings()[pingNb - 1]->GetNavigation();
	m_MvNetDataXMLShipnavStop.m_acquisitionTime = nav.time.m_TimeCpu + nav.time.m_TimeFraction / 10000.0;
	m_MvNetDataXMLShipnavStop.m_roll = RAD_TO_DEG(nav.roll);
	m_MvNetDataXMLShipnavStop.m_pitch = RAD_TO_DEG(nav.pitch);
	m_MvNetDataXMLShipnavStop.m_heave = nav.heave;
	m_MvNetDataXMLShipnavStop.m_latitude = nav.latitude; // deja en �
	m_MvNetDataXMLShipnavStop.m_longitude = nav.longitude; // deja en �
	m_MvNetDataXMLShipnavStop.m_groundspeed = -1; // not available
	m_MvNetDataXMLShipnavStop.m_groundcourse = RAD_TO_DEG(nav.groundCourse);
	m_MvNetDataXMLShipnavStop.m_heading = RAD_TO_DEG(nav.heading);
	m_MvNetDataXMLShipnavStop.m_surfspeed = nav.surfSpeed;
	m_MvNetDataXMLShipnavStop.m_driftcourse = -1; // not available
	m_MvNetDataXMLShipnavStop.m_driftspeed = -1; // not available
	m_MvNetDataXMLShipnavStop.m_altitude = -10000000; // not available
	m_MvNetDataXMLShipnavStop.m_depth = nav.depth*0.001;

	// trame ExtractionParameters
	m_MvNetDataXMLShoalAnset.m_acquisitionTime = m_MvNetDataXMLShipnavStart.m_acquisitionTime;
	m_MvNetDataXMLShoalAnset.m_verticalintegrationdistance = params.GetVerticalIntegrationDistance();
	m_MvNetDataXMLShoalAnset.m_alongintegrationdistance = params.GetAlongIntegrationDistance();
	m_MvNetDataXMLShoalAnset.m_acrossintegrationdistance = params.GetAcrossIntegrationDistance();
	m_MvNetDataXMLShoalAnset.m_threshold = params.GetThreshold();
	m_MvNetDataXMLShoalAnset.m_minlength = params.GetMinLength();
	m_MvNetDataXMLShoalAnset.m_minheight = params.GetMinHeight();
	m_MvNetDataXMLShoalAnset.m_minwidth = params.GetMinWidth();

	// trame Extraction
	m_MvNetDataXMLShoal.m_acquisitionTime = m_MvNetDataXMLShipnavStart.m_acquisitionTime;
	m_MvNetDataXMLShoal.m_shoalId = m_pClosedShoal->GetExternId();
	m_MvNetDataXMLShoal.m_minDepth = (float)m_pClosedShoal->GetShoalStat()->GetMinDepth();
	m_MvNetDataXMLShoal.m_maxDepth = (float)m_pClosedShoal->GetShoalStat()->GetMaxDepth();
	m_MvNetDataXMLShoal.m_meanDepth = (float)m_pClosedShoal->GetShoalStat()->GetGeographicCenter().z;
	m_MvNetDataXMLShoal.m_minDtBot = (float)m_pClosedShoal->GetShoalStat()->GetMinBottomDistance();
	m_MvNetDataXMLShoal.m_maxDtBot = (float)m_pClosedShoal->GetShoalStat()->GetMaxBottomDistance();
	m_MvNetDataXMLShoal.m_maxLength = (float)m_pClosedShoal->GetShoalStat()->GetLength();
	m_MvNetDataXMLShoal.m_maxWidth = (float)m_pClosedShoal->GetShoalStat()->GetWidth();
	m_MvNetDataXMLShoal.m_maxHeight = (float)m_pClosedShoal->GetShoalStat()->GetHeigth();
	// sigma_ag
	m_MvNetDataXMLShoal.m_sigma_ag = (float)m_pClosedShoal->GetShoalStat()->GetSigmaAg();
	//Sv : sv moyen sur le banc (en dB)
	m_MvNetDataXMLShoal.m_MVBS = (float)m_pClosedShoal->GetShoalStat()->GetMeanSv();
	// Sv moyen pond�r�
	m_MvNetDataXMLShoal.m_MVBS_WEIGHTED = (float)m_pClosedShoal->GetShoalStat()->GetMeanPondSv();
	m_MvNetDataXMLShoal.m_volume = (float)m_pClosedShoal->GetShoalStat()->GetVolume();
	m_MvNetDataXMLShoal.m_nbOfEchoes = (float)m_pClosedShoal->GetShoalStat()->GetNbEchos();
	m_MvNetDataXMLShoal.m_startTS = m_pClosedShoal->GetShoalStat()->GetBeginTime().m_TimeCpu
		+ m_pClosedShoal->GetShoalStat()->GetBeginTime().m_TimeFraction / 10000.0;
	m_MvNetDataXMLShoal.m_endTS = m_pClosedShoal->GetShoalStat()->GetEndTime().m_TimeCpu
		+ m_pClosedShoal->GetShoalStat()->GetBeginTime().m_TimeFraction / 10000.0;
	m_MvNetDataXMLShoal.m_centerlat = m_pClosedShoal->GetShoalStat()->GetGeographicCenter().x;
	m_MvNetDataXMLShoal.m_centerlong = m_pClosedShoal->GetShoalStat()->GetGeographicCenter().y;
	m_MvNetDataXMLShoal.m_centerdepth = (float)m_pClosedShoal->GetShoalStat()->GetGeographicCenter().z;
    m_MvNetDataXMLShoal.m_maxAcrossAngle = (float)m_pClosedShoal->GetShoalStat()->GetAcrossAngleMax();
	m_MvNetDataXMLShoal.m_minAcrossAngle = (float)m_pClosedShoal->GetShoalStat()->GetAcrossAngleMin();

	// TRAMES SNDSETs
	int transId = m_pClosedShoal->GetTransId();
	Transducer * pTrans = pFan->getSounderRef()->GetTransducer(transId);
	MemorySet *pMemSet = pFan->GetMemorySetRef();
	MemoryStruct *pMem = pMemSet->GetMemoryStruct(transId);

	//Lock MemoryStruct
	pMem->GetDataFmt()->Lock();

	BaseMathLib::Vector2I size = pMem->GetDataFmt()->getSize();
	for (int iChannel = 0; iChannel < size.x; iChannel++)
	{
		SoftChannel * pSoftChan = pTrans->getSoftChannelPolarX(iChannel);

		CMvNetDataXMLSndset sndset(pTrans->m_transName, pSoftChan, pTrans->m_pulseDuration);
		sndset.m_soundcelerity = pFan->getSounderRef()->m_soundVelocity;
		sndset.m_acquisitionTime = m_MvNetDataXMLShipnavStart.m_acquisitionTime;
		sndset.m_sounderident = m_pClosedShoal->GetSounderId();
		m_MvNetDataXMLSndSets.push_back(sndset);
	}

	//Unlock MemoryStruct
	pMem->GetDataFmt()->Unlock();
}

//return the output found at the given point
ShoalExtractionOutput * ShoalExtractionOutputList::GetOutputAtPoint(const std::string& transducerName,
	int ping, int beam, int echoIdx) const
{
	ShoalExtractionOutput * result = NULL;

	//for each shoal, until one is matching
	bool matching = false;
	ShoalExtractionOutputList::const_iterator iterShoalOutput = begin();
	while (iterShoalOutput != end() && !matching)
	{
		ShoalData* pShoal = (*iterShoalOutput)->m_pClosedShoal;

		//verify the transducer is correct
		if (transducerName == pShoal->GetTransName())
		{
			matching = pShoal->Contains(ping, beam, echoIdx);
		}//end the transducer is correct

		if (matching)
		{
			result = *iterShoalOutput;
		}
		iterShoalOutput++;
	}

	return result;
}
