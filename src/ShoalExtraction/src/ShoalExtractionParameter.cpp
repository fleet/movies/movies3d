
#include "ShoalExtraction/ShoalExtractionParameter.h"
#include "M3DKernel/parameter/ParameterDataType.h"
#include "M3DKernel/config/MovConfig.h"

using namespace BaseKernel;

ShoalExtractionParameter::ShoalExtractionParameter() : BaseKernel::ParameterModuleTransducerFiltered("ShoalExtractionParameter", eShoal)
{
	//Vertical Integration Distance
	m_verticalIntegrationDistance = 1.0;
	//Along Integration Distance
	m_alongIntegrationDistance = 0.0;
	//Across Integration Distance
	m_acrossIntegrationDistance = 0.0;

	//threshold
	m_threshold = -60.0;
	m_maxThreshold = 0.0;

	//Minimum shoal Length
	m_minLength = 1.0;
	//Minimum shoal Width
	m_minWidth = 1.0;
	//Minimum shoal Height
	m_minHeight = 1.0;

	//Record path
	m_recordPath = "c:\\temp\\";
	//SaveEchos option
	m_saveEchos = false;
	m_saveContourPositions = false;

	GetParameterBroadcastAndRecord().m_fileSuffix = "shl";

	//Overlap managment
	m_useOverlapFilter = false;

	//use internal shoal storage
	m_useInternalShoalStorage = false;

	//use only central beam
	m_useOnlyCentralBeam = false;

	//Minimum shoal Depth
	m_minDepth = -1;

	//Maximum shoal Depth
	m_maxDepth = -1;
}

bool ShoalExtractionParameter::Serialize(BaseKernel::MovConfig * movConfig)
{	
	// Début de la sérialisation du modulee
	movConfig->SerializeData(this, eParameterModule);

	movConfig->SerializeData<float>(m_minLength, eFloat, "MinimumLength");
	movConfig->SerializeData<float>(m_minWidth, eFloat, "MinimumWidth");
	movConfig->SerializeData<float>(m_minHeight, eFloat, "MinimumHeight");
	movConfig->SerializeData<float>(m_threshold, eFloat, "Threshold");
	movConfig->SerializeData<float>(m_maxThreshold, eFloat, "MaxThreshold");
	movConfig->SerializeData<float>(m_verticalIntegrationDistance, eFloat, "VerticalIntegrationDistance");
	movConfig->SerializeData<float>(m_acrossIntegrationDistance, eFloat, "AcrossIntegrationDistance");
	movConfig->SerializeData<float>(m_alongIntegrationDistance, eFloat, "AlongIntegrationDistance");
	movConfig->SerializeData<std::string>(m_recordPath, eString, "RecordPath");
	movConfig->SerializeData<bool>(m_saveEchos, eBool, "SaveEchosInformation");
	movConfig->SerializeData<bool>(m_saveContourPositions, eBool, "SaveContourPointsPositions");


	GetParameterBroadcastAndRecord().Serialize(movConfig);
	
	// Sérialisation des transducteurs actifs
	SerializeActiveTransducers(movConfig);

	// Fin de la sérialisation du module
	movConfig->SerializePushBack();

	return true;
}
bool ShoalExtractionParameter::DeSerialize(MovConfig * movConfig)
{	
	// Début de la désérialisation du module
	auto result = movConfig->DeSerializeData(this, eParameterModule);
	if (result)
	{		
		result = result && movConfig->DeSerializeData<float>(&m_minLength, eFloat, "MinimumLength");
		result = result && movConfig->DeSerializeData<float>(&m_minWidth, eFloat, "MinimumWidth");
		result = result && movConfig->DeSerializeData<float>(&m_minHeight, eFloat, "MinimumHeight");
		result = result && movConfig->DeSerializeData<float>(&m_threshold, eFloat, "Threshold");
		result = result && movConfig->DeSerializeData<float>(&m_maxThreshold, eFloat, "MaxThreshold");
		result = result && movConfig->DeSerializeData<float>(&m_verticalIntegrationDistance, eFloat, "VerticalIntegrationDistance");
		result = result && movConfig->DeSerializeData<float>(&m_acrossIntegrationDistance, eFloat, "AcrossIntegrationDistance");
		result = result && movConfig->DeSerializeData<float>(&m_alongIntegrationDistance, eFloat, "AlongIntegrationDistance");
		result = result && movConfig->DeSerializeData<std::string>(&m_recordPath, eString, "RecordPath");
		result = result && movConfig->DeSerializeData<bool>(&m_saveEchos, eBool, "SaveEchosInformation");
		result = result && movConfig->DeSerializeData<bool>(&m_saveContourPositions, eBool, "SaveContourPointsPositions");

		result = result && GetParameterBroadcastAndRecord().DeSerialize(movConfig);
		
		// Désérialisation des transducteurs actifs
		result &= DeSerializeActiveTransducers(movConfig);
	}

	// Fin de la désérialisation du module
	movConfig->DeSerializePushBack();

	return result;
}



