// -*- C++ -*-
// ****************************************************************************
// Class: ShoalExtractionStat
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include <assert.h>
#include <float.h>
#include <iterator>

#include "M3DKernel/datascheme/PingFan.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/NavAttributes.h"
#include "M3DKernel/utils/carto/CartoTools.h"

#include "ShoalExtraction/data/pingdata.h"
#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/shoalstat.h"
#include "ShoalExtraction/data/pingshoalstat.h"

#include "ShoalExtraction/ShoalExtractionStat.h"
#include "ShoalExtraction/ShoalExtractionGeometry.h"
#include "ShoalExtraction/ShoalExtractionParameter.h"
#include "BaseMathLib/geometry/GeometryTools2D.h"
#include "ShoalExtraction/geometry/geometrytools3d.h"

using namespace shoalextraction;
using namespace BaseMathLib;
using namespace std;

#ifdef _TRACE_PERF
#define _SHOALSTAT_TRACE_PERF

#ifdef _SHOALSTAT_TRACE_PERF
extern std::ofstream trace_ofs;
#endif
#endif

//*****************************************************************************
// Name : CompletePingData
// Description : R�cup�ration de toutes les donn�es du ping
// Parameters : * (in) PingData* pPingData
// Return : void
//*****************************************************************************
void ShoalExtractionStat::CompletePingData(PingData* pPingData, double maxThreshold)
{
	//si les donn�es du ping ne sont pas completes
	if (!pPingData->GetComplete())
	{
		pPingData->SetComplete(true);


		//parcours de tous les transducers
		M3DKernel *pKern = M3DKernel::GetInstance();

		Sounder *pSounder = pPingData->GetPingFan()->m_pSounder;
		MemorySet *pMemSet = pPingData->GetPingFan()->GetMemorySetRef();

		for (const auto& pTransData : pPingData->GetTransducers())
		{
			if (pTransData != nullptr)
			{
				const auto integrationDistanceX = pTransData->GetIntegrationDistanceX();
				
				// On r�cup�re la structure de donn�es associ�e au pingFan
				MemoryStruct *pMem = pMemSet->GetMemoryStruct(pTransData->GetTransId());

				double beamsSamplesSpacing = pTransData->GetBeamsSamplesSpacing();
				double zIntegrationDistance = pTransData->GetVerticalIntegrationDistance();
				double transTranslation = pTransData->GetSampleOffset();

				//parcours de tous les faisceaux
				std::int32_t nbChannels = (std::int32_t)pTransData->GetChannels().size();
				for (int iChannel = 0; iChannel < nbChannels; iChannel++)
				{
					ChannelData* pChannelData = pTransData->GetChannels()[iChannel];

					// across angles
					double athwartShipAngleRad = pChannelData->GetAthwartAngle() * 0.5;
					double cosAthwart = cos(athwartShipAngleRad);
					double tanAthwart = tan(athwartShipAngleRad);

					// along angles
					double alongShipAngleRad = pChannelData->GetAlongAngle() * 0.5;
					double cosAlong = cos(alongShipAngleRad);
					double tanAlong = tan(alongShipAngleRad);

					//************************************************
					//finalisation des caract�ristiques 2D/3D echos 

					//parcours de tous les groupes d'�chos de la liste finale
					std::int32_t nbGroups = (std::int32_t)pChannelData->GetGroupsFinal().size();
					for (int iEchoGroup = 0; iEchoGroup < nbGroups; iEchoGroup++)
					{
						EchoGroup* pEchoGroup = pChannelData->GetGroupsFinal()[iEchoGroup];

						if (pEchoGroup != NULL)
						{
							//cr�ation de la surface 2D sans les marges
							ShoalExtractionGeometry::CreateEchoSurf2D(beamsSamplesSpacing, integrationDistanceX, zIntegrationDistance,
								pChannelData->GetSteeringAngle(), athwartShipAngleRad, true, pEchoGroup);

							//cr�ation du volume 3D
							ShoalExtractionGeometry::ComputeVolumes(pPingData->GetPingFan(), pSounder, pTransData->GetTransId(), pChannelData->GetChannelId(),
								cosAlong, tanAlong, cosAthwart, tanAthwart,
								beamsSamplesSpacing, transTranslation, pEchoGroup);

							pEchoGroup->GetSurf2D().UpdateBBox();
							pEchoGroup->GetVol3D().UpdateBBox();
							assert(pEchoGroup->GetSurf2D().GetNbPoints() > 0);
							assert(pEchoGroup->GetVol3D().GetPoints().size() > 0);
						}
					}//for groups

					//************************************************
					//finalisation des caract�ristiques d'energie des echos 

					// R�cup�ration donn�es �cho de chaque faisceau (lock de la memoire)
					pMem->GetDataFmt()->Lock();

					DataFmt * pStart = pMem->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(pChannelData->GetChannelId(), 0));
					BaseMathLib::Vector2I size = pMem->GetDataFmt()->getSize();

					//parcours de tous les groupes d'�chos de la liste finale
					for (int iEchoGroup = 0; iEchoGroup < nbGroups; iEchoGroup++)
					{
						EchoGroup* pEchoGroup = pChannelData->GetGroupsFinal()[iEchoGroup];
						if (pEchoGroup != NULL)
						{
							//on ne r�cup�re les donn�es qu'une seule fois !!
							if (pEchoGroup->GetEchosEnergy().size() == 0)
							{
                                std::uint32_t echoMin = std::max((std::uint32_t)0, pEchoGroup->GetEchoMin());
                                std::uint32_t echoMax = min((std::uint32_t)(size.y) - 1, pEchoGroup->GetEchomax());

								if (echoMin <= echoMax)
								{
									pEchoGroup->GetEchosEnergy().resize(echoMax - echoMin + 1);
									DataFmt * pStart2 = pStart + echoMin;
									echoMax -= echoMin;
									for (std::uint32_t i = 0; i <= echoMax; i++)
									{
										// OTK - FAE006 - on n'exclu plus les echos positifs	
										//sauvegarde de l'energie des echos
										//pEchoGroup->GetEchosEnergy()[i] = *pStart2++;
										// OTK - FAE213 - on remet en place un seuil max pour �viter les valeurs aberrantes
										if (*pStart2 <= maxThreshold)
										{
											pEchoGroup->GetEchosEnergy()[i] = *pStart2;
										}
										else
										{
											pEchoGroup->GetEchosEnergy()[i] = UNKNOWN_DB;
										}
										pStart2++;
									}
								}
							}//if
						}
					}//for groups
					pMem->GetDataFmt()->Unlock();

				}//fin for iChannel
			}
		}//fin for iTrans
	}
}

//*****************************************************************************
// Name : ComputePingStats
// Description : Calcul des statistiques du ping
// Parameters : * (in) PingData* pPingData
// Return : void
//*****************************************************************************
void ShoalExtractionStat::ComputePingStats(PingData* pPingData)
{
	//calcul des statistiques a partir des donn�es g�ometriques et energ�tiques
	PingShoalStat* pStat = pPingData->GetShoalStat();
	assert(pStat != NULL);

	//un banc ne peut etre calcul� que sur les donn�es d'un transducteur
	if (pPingData->GetTransducers().size() == 1)
	{
		TransductData*	pTransData = pPingData->GetTransducers()[0];
		std::uint32_t nbEchos = 0;

		//parcours de tous les faisceaux
		int nbChannels = (int)pTransData->GetChannels().size();
		for (int iChannel = 0; iChannel < nbChannels; iChannel++)
		{
			ChannelData* pChannelData = pTransData->GetChannels()[iChannel];

			//hauteur du fond pour le faisceau
			double groundDepth = pChannelData->GetGroundDepth();

			//MaJ de l'angle d'ouverture longitudinale moyen
			pStat->SetMeanAlongAngle(pStat->GetMeanAlongAngle() + pChannelData->GetAlongAngle());

			//parcours de tous les groupes d'�chos
			std::int32_t nbGroups = (std::int32_t)pChannelData->GetGroupsFinal().size();
			for (int iEchoGroup = 0; iEchoGroup < nbGroups; iEchoGroup++)
			{
				EchoGroup* pEchoGroup = pChannelData->GetGroupsFinal()[iEchoGroup];

				double echoGroupZMin = pEchoGroup->GetVol3D().GetBoundingBox().GetZMin();
                double echoGroupZMax = pEchoGroup->GetVol3D().GetBoundingBox().GetZMax();

				//le volume d'un echo peut etre l�g�rement en dessous du fond car le calcul du fond 
				//ne prend pas en compte la geom�trie (et notamment l'angle d'ouverture transversal) de l'echo
				//--> le volume de l'echo i peut avoir une partie plus profonde que le centre de l'�cho i+1

				//assert(groundDepth >= echoGroupZMax);
				//assert(groundDepth >= echoGroupZMin);

				//stat de distance par rapport au fond
				pStat->SetMinBottomDistance(min(pStat->GetMinBottomDistance(), groundDepth - echoGroupZMax));
				pStat->SetMaxBottomDistance(std::max(pStat->GetMaxBottomDistance(), groundDepth - echoGroupZMin));

				//stat de profondeur
				pStat->SetMinDepth(min(pStat->GetMinDepth(), echoGroupZMin));
				pStat->SetMaxDepth(std::max(pStat->GetMaxDepth(), echoGroupZMax));

				nbEchos += pEchoGroup->GetEchomax() - pEchoGroup->GetEchoMin() + 1;
			}

		}

		//MaJ de l'angle d'ouverture longitudinale moyen
		if (nbChannels > 0)
		{
			pStat->SetMeanAlongAngle(pStat->GetMeanAlongAngle() / nbChannels);
		}

		//calcul des angles lat�raux
		if (nbChannels >= 0)
		{
			ChannelData* pChannelMin = pTransData->GetChannels()[0];
			ChannelData* pChannelMax = pTransData->GetChannels()[nbChannels - 1];

			pStat->SetAcrossAngleMin(pChannelMin->GetSteeringAngle() - pChannelMin->GetAthwartAngle() * 0.5);
            pStat->SetAcrossAngleMax(pChannelMax->GetSteeringAngle() + pChannelMax->GetAthwartAngle() * 0.5);
		}
		pPingData->GetShoalStat()->SetNbEchos(nbEchos);
	}
}


//*****************************************************************************
// Name : ComputePingSurface
// Description : Calcul de la surface du banc pour une composante donn�e
//	(sur un mm ping, un banc peut avoir plusieurs composantes issues de l'agr�gation 3D)
// Parameters : * (in) PingData* pPingData
//              * (in) const std::set<ShoalId*>& pShoalIds
//              * (in) bool complete : permet de ne faire que certains calculs
// Return : HullItem* : donn�es de la surface de la composante
//*****************************************************************************
HullItem* ShoalExtractionStat::ComputePingSurface(PingData* pPingData,
	const std::set<std::uint64_t>& pShoalIds,
	bool complete)
{
	HullItem* result = new HullItem();

	TransductData*	pTransData = pPingData->GetTransducers()[0]; //un seul transducteur pour un banc donn�
	PingShoalStat* pStat = pPingData->GetShoalStat();
	double beamSampleSpacing = pTransData->GetBeamsSamplesSpacing();

	//*****************************************************************************
	// calcul du nb de faisceaux concern�s par cette composante
	//*****************************************************************************
	int nbChannels = 0;
	std::set<int> componentChannels;
	int iChannel = 0;

	auto iterChannel = pTransData->GetChannels().begin();
	while (iterChannel != pTransData->GetChannels().end())
	{
		ChannelData* pChannelData = *iterChannel;

		int nbGroups = (int)pChannelData->GetGroupsFinal().size();
		for (int iGroup = 0; iGroup < nbGroups; iGroup++)
		{
			EchoGroup* pEchogroup = pChannelData->GetGroupsFinal()[iGroup];
			if (pShoalIds.find(pEchogroup->GetShoalId()->GetLinked2DShoalId()->GetShoalId()) != pShoalIds.end())
			{
				componentChannels.insert(iChannel);

				// ajout de l'ensemble des ids 2d du ping pr�c�dent reli�s � la surface
				std::set< std::weak_ptr<ShoalId> >::const_iterator iterShoalIds = pEchogroup->GetShoalId()->GetPreviousPing2DShoalIds().begin();
				std::set< std::weak_ptr<ShoalId> >::const_iterator iterShoalIdsEnd = pEchogroup->GetShoalId()->GetPreviousPing2DShoalIds().end();
				while (iterShoalIds != iterShoalIdsEnd)
				{
					std::shared_ptr<ShoalId> shoalId = (*iterShoalIds).lock();
					if (shoalId)
					{
						set<std::uint64_t> ids = shoalId->GetAllLinkedComponentIds();
						result->linkedShoal2DComponents.insert(ids.begin(), ids.end());
					}

					iterShoalIds++;
				}
				set<std::uint64_t> ids = pEchogroup->GetShoalId()->GetAllLinkedComponentIds();
				result->shoal2DComponentIDs.insert(ids.begin(), ids.end());
			}
		}
		iterChannel++;
		iChannel++;
	}
	nbChannels = (int)componentChannels.size();

	//*****************************************************************************
	// Allocation des donn�es de la surface
	//*****************************************************************************
	// on a pour chaque channel, 2 segments. De plus, chaque segment produit 4 points sauf le premier qui en produit 2
	int nbPoints = std::max(0, (nbChannels * 2 - 1) * 4 + 2);
	int realNbPoints = 0;
	result->surfOrtho.Alloc(nbPoints);
	result->surfPolar.Alloc(nbPoints);


	// OTK - refonte de l'algo de calcul de la surface 2D du banc pour le ping
	// 1 - parcours de tous les channels (sur lesquels on a des groupes d'�chos)
	// 2 - construction du "groupe d'�cho englobant" les groupes d'�chos de chaque channel
	// 3 - pour chacun de ces groupes d'�chos englobant, on a un couple de points de part et d'autre
	//     de l'axe central du faisceau.
	// 4 - on trie ces couples par ordre de d�pointage croissant
	// 5 - pour chaque couple, on regarde quels groupes englobants 'recouvrent' le couple, et on construit
	//     en cons�quence la surface

	// liste des groupes d'�chos englobant pour chaque channel
	std::vector<SurroundingEchoGroup> surroundingEchoGroups;

	// 1 - parcours de tous les channels (sur lesquels on a des groupes d'�chos)
	std::set<int>::iterator iterComponentChannels = componentChannels.begin();
	for (int iChannel = 0; iterComponentChannels != componentChannels.end(); iChannel++, iterComponentChannels++)
	{
		ChannelData* pChannelData = pTransData->GetChannels()[*iterComponentChannels];

		// recherche des groupes concern�s
		EchoGroup* pFirstEchoGroup = NULL;
		EchoGroup* pLastEchoGroup = NULL;
		std::vector<EchoGroup*>::iterator iterGroup = pChannelData->GetGroupsFinal().begin();
		while (iterGroup != pChannelData->GetGroupsFinal().end())
		{
			if (pShoalIds.find((*iterGroup)->GetShoalId()->GetLinked2DShoalId()->GetShoalId()) != pShoalIds.end())
			{
				pFirstEchoGroup = (*iterGroup);
				break;
			}
			iterGroup++;
		}
		std::vector<EchoGroup*>::reverse_iterator iterGroupR = pChannelData->GetGroupsFinal().rbegin();
		while (iterGroupR != pChannelData->GetGroupsFinal().rend())
		{
			if (pShoalIds.find((*iterGroupR)->GetShoalId()->GetLinked2DShoalId()->GetShoalId()) != pShoalIds.end())
			{
				pLastEchoGroup = (*iterGroupR);
				break;
			}
			iterGroupR++;
		}

		// 2 - construction du "groupe d'�cho englobant" les groupes d'�chos de chaque channel
		if (pFirstEchoGroup != NULL && pLastEchoGroup != NULL)
		{
			SurroundingEchoGroup echoGroup;
			echoGroup.m_pFirstEchoGroup = pFirstEchoGroup;
			echoGroup.m_pLastEchoGroup = pLastEchoGroup;
			echoGroup.m_pChannelData = pChannelData;
			surroundingEchoGroups.push_back(echoGroup);
		}
	}

	// 3 - pour chacun de ces groupes d'�chos englobant, on a un couple de points de part et d'autre
	// de l'axe central du faisceau.
	size_t nbEchoGroups = surroundingEchoGroups.size();
	std::vector<SurfaceSegment> segments;
	segments.reserve(2 * nbEchoGroups);
	for (size_t i = 0; i < nbEchoGroups; i++)
	{
		SurfaceSegment segment;
		segment.m_pEchoGroup = &(surroundingEchoGroups[i]);
		segment.m_FirstEdge = true;
		segments.push_back(segment);
		segment.m_FirstEdge = false;
		segments.push_back(segment);
	}

	// 4 - on trie ces couples par ordre de d�pointage croissant
	std::sort(segments.begin(), segments.end());

	// 5 - pour chaque couple, on regarde quels groupes englobants 'recouvrent' le couple, et on construit
	// en cons�quence la surface
	int nbCouples = (int)segments.size();
	double lastzmin, lastzmax; // pour comparaison avec le segment pr�c�dent

	// pour la surface simplifi�e pour la repr�sentation 3D, 
	// on fusionne les segments dont les angles sont tr�s proches, pour ne pas avoir 
	// de courbes trop d�coup�es et "verticales", hachant les lignes du banc.
	double lastAngle = 0.0;
	// on autorise 1000 points sur la largeur du fan.
	PingFan *pFan = pPingData->GetPingFan();
	Sounder *pSounder = pFan->m_pSounder;
	Transducer *pTrans = pSounder->GetTransducer(pTransData->GetTransId());
	SoftChannel* firstSoftChannel = pTrans->getSoftChannelPolarX(0);
	SoftChannel* lastSoftChannel = pTrans->getSoftChannelPolarX((unsigned int)(pTrans->GetChannelId().size() - 1));
	double angleMin = firstSoftChannel->m_mainBeamAthwartSteeringAngleRad - 0.5*firstSoftChannel->m_beam3dBWidthAthwartRad;
	double angleMax = lastSoftChannel->m_mainBeamAthwartSteeringAngleRad + 0.5*lastSoftChannel->m_beam3dBWidthAthwartRad;
	static const double minDeltaAngle = (angleMax - angleMin) / 1000.0;
	// premiere boucle pour d�terminer les nombre de points de l'enveloppe 3D
	int nbSimplifiedPoints = 0;
	int nbSimplifiedCouples = 0;
	for (int i = 0; i < nbCouples; i++)
	{
		if (i == 0)
		{
			nbSimplifiedPoints += 2;
			nbSimplifiedCouples += 1;
		}
		else
		{
			if (segments[i].GetAngle() - segments[i - 1].GetAngle() > minDeltaAngle)
			{
				nbSimplifiedPoints += 2;
				nbSimplifiedCouples += 1;
			}
		}
	}
	result->surfOrtho2.Alloc(nbSimplifiedPoints);
	nbSimplifiedPoints = 0;

	for (int i = 0; i < nbCouples; i++)
	{
		// cas particulier pour le premier point, on ajoute le segment directement
		if (i == 0)
		{
			// surface ortho
			*result->surfOrtho.GetPointX(realNbPoints) = segments[0].GetPointSupX();
			*result->surfOrtho.GetPointY(realNbPoints) = segments[0].GetPointSupY();
			*result->surfOrtho.GetPointX(nbPoints - realNbPoints - 1) = segments[0].GetPointInfX();
			*result->surfOrtho.GetPointY(nbPoints - realNbPoints - 1) = segments[0].GetPointInfY();
			// surface ortho 2 (pour affichage 3D)
			*result->surfOrtho2.GetPointX(0) = segments[0].GetPointSupX();
			*result->surfOrtho2.GetPointY(0) = segments[0].GetPointSupY();
			*result->surfOrtho2.GetPointX(nbSimplifiedCouples) = segments[0].GetPointInfX();
			*result->surfOrtho2.GetPointY(nbSimplifiedCouples) = segments[0].GetPointInfY();
			nbSimplifiedPoints++;
			// surface polaire
			*result->surfPolar.GetPointX(realNbPoints) = segments[0].GetAngle();
			lastzmin = segments[0].GetMinZ(beamSampleSpacing);
			*result->surfPolar.GetPointY(realNbPoints) = lastzmin;
			*result->surfPolar.GetPointX(nbPoints - realNbPoints - 1) = segments[0].GetAngle();
			lastzmax = segments[0].GetMaxZ(beamSampleSpacing);
			*result->surfPolar.GetPointY(nbPoints - realNbPoints - 1) = lastzmax;

			realNbPoints++;
		}
		else
		{
			SurfaceSegment couple = segments[i];
			double angle = couple.GetAngle();
			double zmin = couple.GetMinZ(beamSampleSpacing);
			double zmax = couple.GetMaxZ(beamSampleSpacing);
			double minzmin = zmin;
			double maxzmax = zmax;
			// recherche des groupes d'�chos "englobants"
			for (size_t j = 0; j < nbEchoGroups; j++)
			{
				SurroundingEchoGroup group = surroundingEchoGroups[j];
				double minAngle = group.GetMinAngle();
				double maxAngle = group.GetMaxAngle();
				if (angle <= maxAngle && angle >= minAngle)
				{
					// dans ce cas, on a recouvrement
					double zmin2 = group.GetMinZ(beamSampleSpacing);
					double zmax2 = group.GetMaxZ(beamSampleSpacing);
					minzmin = min(minzmin, zmin2);
					maxzmax = std::max(maxzmax, zmax2);
				}
			}

			double x, y;
			// si le zmin diminue, on ajoute le point avec l'ancien zmin, puis le nouveau point
			if (minzmin < lastzmin)
			{
				// ajout du point avec l'ancien zmin
				// surface ortho
				couple.GetPoint(lastzmin, beamSampleSpacing, x, y);
				*result->surfOrtho.GetPointX(realNbPoints) = x;
				*result->surfOrtho.GetPointY(realNbPoints) = y;
				// surface polaire
				*result->surfPolar.GetPointX(realNbPoints) = angle;
				*result->surfPolar.GetPointY(realNbPoints) = lastzmin;

				// ajout du nouveau point
				// surface ortho
				couple.GetPoint(minzmin, beamSampleSpacing, x, y);
				*result->surfOrtho.GetPointX(realNbPoints + 1) = x;
				*result->surfOrtho.GetPointY(realNbPoints + 1) = y;
				// surface polaire
				*result->surfPolar.GetPointX(realNbPoints + 1) = angle;
				*result->surfPolar.GetPointY(realNbPoints + 1) = minzmin;
			}
			// si le zmin augmente, on ajoute le point � l'angle pr�cedent avec le nouveau zmin, puis le nouveau point
			else
			{
				// ajout du point � l'angle pr�c�dent avec le nouveau zmin
				// surface ortho
				segments[i - 1].GetPoint(minzmin, beamSampleSpacing, x, y);
				*result->surfOrtho.GetPointX(realNbPoints) = x;
				*result->surfOrtho.GetPointY(realNbPoints) = y;
				// surface polaire
				*result->surfPolar.GetPointX(realNbPoints) = segments[i - 1].GetAngle();
				*result->surfPolar.GetPointY(realNbPoints) = minzmin;

				// ajout du nouveau point
				// surface ortho
				couple.GetPoint(minzmin, beamSampleSpacing, x, y);
				*result->surfOrtho.GetPointX(realNbPoints + 1) = x;
				*result->surfOrtho.GetPointY(realNbPoints + 1) = y;
				// surface polaire
				*result->surfPolar.GetPointX(realNbPoints + 1) = angle;
				*result->surfPolar.GetPointY(realNbPoints + 1) = minzmin;
			}
			// surface ortho 2 (pour affichage 3D)
			// seulement si on n'a pas deja eu un point tr�s pr�s de celui-ci
			bool newSimplifiedPoint = angle - lastAngle > minDeltaAngle;
			if (newSimplifiedPoint)
			{
				// si les angles sont suffisamment diff�rents, on ajoute le nouveau point
				*result->surfOrtho2.GetPointX(nbSimplifiedPoints) = x;
				*result->surfOrtho2.GetPointY(nbSimplifiedPoints) = y;
			}
			// si on merge les points, on prend les points englobant les segments imbriqu�s
			else
			{
				if (minzmin < lastzmin)
				{
					*result->surfOrtho2.GetPointX(nbSimplifiedPoints - 1) = x;
					*result->surfOrtho2.GetPointY(nbSimplifiedPoints - 1) = y;
				}
			}

			// si le zmax augmente, on ajoute le point avec l'ancien zmax, puis le nouveau point
			if (maxzmax > lastzmax)
			{
				// ajout du point avec l'ancien zmax
				// surface ortho
				couple.GetPoint(lastzmax, beamSampleSpacing, x, y);
				*result->surfOrtho.GetPointX(nbPoints - realNbPoints - 1) = x;
				*result->surfOrtho.GetPointY(nbPoints - realNbPoints - 1) = y;
				// surface polaire
				*result->surfPolar.GetPointX(nbPoints - realNbPoints - 1) = angle;
				*result->surfPolar.GetPointY(nbPoints - realNbPoints - 1) = lastzmax;

				// ajout du nouveau point
				// surface ortho
				couple.GetPoint(maxzmax, beamSampleSpacing, x, y);
				*result->surfOrtho.GetPointX(nbPoints - realNbPoints - 2) = x;
				*result->surfOrtho.GetPointY(nbPoints - realNbPoints - 2) = y;
				// surface polaire
				*result->surfPolar.GetPointX(nbPoints - realNbPoints - 2) = angle;
				*result->surfPolar.GetPointY(nbPoints - realNbPoints - 2) = maxzmax;
			}
			// si le zmax diminue, on ajoute � l'angle pr�c�dent le point avec le nouveau zmax, puis le nouveau point
			else
			{
				// ajout du point � l'angle pr�c�dent avec le nouveau zmax
				// surface ortho
				segments[i - 1].GetPoint(maxzmax, beamSampleSpacing, x, y);
				*result->surfOrtho.GetPointX(nbPoints - realNbPoints - 1) = x;
				*result->surfOrtho.GetPointY(nbPoints - realNbPoints - 1) = y;
				// surface polaire
				*result->surfPolar.GetPointX(nbPoints - realNbPoints - 1) = segments[i - 1].GetAngle();
				*result->surfPolar.GetPointY(nbPoints - realNbPoints - 1) = maxzmax;

				// ajout du nouveau point
				// surface ortho
				couple.GetPoint(maxzmax, beamSampleSpacing, x, y);
				*result->surfOrtho.GetPointX(nbPoints - realNbPoints - 2) = x;
				*result->surfOrtho.GetPointY(nbPoints - realNbPoints - 2) = y;
				// surface polaire
				*result->surfPolar.GetPointX(nbPoints - realNbPoints - 2) = angle;
				*result->surfPolar.GetPointY(nbPoints - realNbPoints - 2) = maxzmax;
			}
			// surface ortho 2 (pour affichage 3D)
			// seulement si on n'a pas deja eu un point tr�s pr�s de celui-ci
			if (newSimplifiedPoint)
			{
				// si les angles sont suffisamment diff�rents, on ajoute le nouveau point
				*result->surfOrtho2.GetPointX(nbSimplifiedCouples + nbSimplifiedPoints) = x;
				*result->surfOrtho2.GetPointY(nbSimplifiedCouples + nbSimplifiedPoints) = y;
				nbSimplifiedPoints++;
			}
			// si on merge les points, on prend les points englobant les segments imbriqu�s
			else
			{
				if (maxzmax > lastzmax)
				{
					*result->surfOrtho2.GetPointX(nbSimplifiedCouples + nbSimplifiedPoints - 1) = x;
					*result->surfOrtho2.GetPointY(nbSimplifiedCouples + nbSimplifiedPoints - 1) = y;
				}
			}


			lastzmin = minzmin;
			lastzmax = maxzmax;
			realNbPoints += 2;
		}
		lastAngle = segments[i].GetAngle();
	}

	// on ne fait pas le calcul de l'aire si on n'en a pas besoin
	if (complete)
	{
		//*****************************************************************************
		//calcul de l'aire de la surface 2D
		//*****************************************************************************

		//calcul de l'aire de la surface 2D 
		//(polaire car elle est convexe et offre donc une meilleure pr�cision de calcul)
		result->area = result->surfPolar.ComputeConvexPolarArea();

		//MaJ BBox ortho
		result->surfOrtho.UpdateBBox();
	}

#ifdef _SHOALSTAT_TRACE_PERF
	for (int i = 0; i < result->surfOrtho.GetNbPoints(); i++)
	{
		trace_ofs << *result->surfOrtho.GetPointX(i) << "\t" << *result->surfOrtho.GetPointY(i) << std::endl;
	}
#endif

	return result;
}

//*****************************************************************************
// Name : ComputePingSurfaces
// Description : Calcul des surfaces du ping
// Parameters : * (in) PingData* pPingData
// Return : void
//*****************************************************************************
void ShoalExtractionStat::ComputePingSurfaces(PingData* pPingData)
{
#ifdef _SHOALSTAT_TRACE_PERF
	trace_ofs << "ComputePingSurfaces  Ping : " << pPingData->GetPingId() << std::endl;
#endif

	TransductData*	pTransData = pPingData->GetTransducers()[0]; //un seul transducteur pour un banc donn�
	PingShoalStat* pStat = pPingData->GetShoalStat();

	//MaJ des surfaces 2D
	pTransData->UpdateAllShoalId2D();

	//MaJ aire
	pStat->SetArea(0);

	//*****************************************************************************
	// Recherche de toutes les composants du banc sur ce ping
	//*****************************************************************************
	std::set< shared_ptr<ShoalId> > componentIds;
	shared_ptr<ShoalId> previousShoalId = NULL;

	//bbox de la surface
	BoundingBox2D bbox;

	auto iterChannel = pTransData->GetChannels().begin();
	while (iterChannel != pTransData->GetChannels().end())
	{
		ChannelData* pChannelData = *iterChannel;

		std::vector<EchoGroup*>::iterator iterGroup = pChannelData->GetGroupsFinal().begin();
		while (iterGroup != pChannelData->GetGroupsFinal().end())
		{
			shared_ptr<ShoalId> pShoalId = (*iterGroup)->GetShoalId()->GetLinked2DShoalId();

			if (previousShoalId != pShoalId)
			{
				std::set< shared_ptr<ShoalId> >::iterator iterShoal = componentIds.find(pShoalId);

				//pour chaque nouvelle composante
				if (iterShoal == componentIds.end())
				{
					componentIds.insert(pShoalId);

					//calcul de la surface associ�e
					set<std::uint64_t> component;
					component.insert(pShoalId->GetLinked2DShoalId()->GetShoalId());
					HullItem * pHullItem = ComputePingSurface(pPingData, component);
					if (pHullItem->surfPolar.GetNbPoints() > 0)
					{
						pStat->GetHullList().push_back(pHullItem);

						//MaJ aire
						pStat->SetArea(pStat->GetArea() + pHullItem->area);

						//MaJ bbox
						bbox.Update(pHullItem->surfOrtho.GetBoundingBox());
					}
					else
					{
						delete pHullItem;
					}
				}
			}
			previousShoalId = pShoalId;

			iterGroup++;
		}
		iterChannel++;
	}

	//MaJ point central
	double pointX_2D = (bbox.m_dXMax + bbox.m_dXMin) * 0.5;
	double pointY_2D = (bbox.m_dYMax + bbox.m_dYMin) * 0.5;
	pStat->SetHull2DCenter(Vector2D(pointX_2D, pointY_2D));

#ifdef _SHOALSTAT_TRACE_PERF
	trace_ofs << "Area : " << pStat->GetArea() << "\tNb Components : " << componentIds.size() << std::endl;
#endif

}

//*****************************************************************************
// Name : ComputePingHull
// Description : Calcul de l'enveloppe 3D de la surface du ping
// Parameters : * (in) PingData* pPingData
// Return : void
//*****************************************************************************
void ShoalExtractionStat::ComputePingHull(PingData* pPingData)
{
#ifdef _SHOALSTAT_TRACE_PERF
	//	trace_ofs << "ComputePingHull  Ping : " << pPingData->GetPingId() << std::endl;
#endif

  //un banc ne peut etre calcul� que sur les donn�es d'un transducteur
	if (pPingData->GetTransducers().size() == 1)
	{
		TransductData*	pTransData = pPingData->GetTransducers()[0];
		PingShoalStat* pStat = pPingData->GetShoalStat();

		double cosRoll = cos(0.0/*pPingData->GetNavigation().roll*/);
		double cosPitch = cos(pTransData->GetPitch());
		double rollPitchFactor = cosPitch * cosRoll;
		double cosHeading = cos(-pPingData->GetNavigation().heading);
		double sinHeading = sin(-pPingData->GetNavigation().heading);

		Vector3D transOrigin = pTransData->GetOrigin3D();
		Vector3D transOriginGPS = pTransData->GetOrigin3DGPS();

		//MaJ point central
		pStat->SetHullCenter(Vector3D(0, 0, 0));

		//liste des surfaces 2D orthonorm�es convexes
		int nbSurf = (int)pStat->GetHullList().size();
		std::vector<HullItem*>::iterator iterHull = pStat->GetHullList().begin();

		while (iterHull != pStat->GetHullList().end())
		{
			//calcul du point central de l'enveloppe � partir du point milieu de la BBox 2D orthonorm�e
			(*iterHull)->surfOrtho.UpdateBBox();
			double pointX_2D = ((*iterHull)->surfOrtho.GetBoundingBox().m_dXMax + (*iterHull)->surfOrtho.GetBoundingBox().m_dXMin) * 0.5;
			double pointY_2D = ((*iterHull)->surfOrtho.GetBoundingBox().m_dYMax + (*iterHull)->surfOrtho.GetBoundingBox().m_dYMin) * 0.5;

			(*iterHull)->hullCenter.x = pointX_2D * rollPitchFactor * sinHeading + transOrigin.x;
			(*iterHull)->hullCenter.y = pointX_2D * rollPitchFactor * cosHeading + transOrigin.y;
			(*iterHull)->hullCenter.z = pointY_2D * rollPitchFactor + transOrigin.z;

#ifdef _SHOALSTAT_TRACE_PERF
			//			trace_ofs << "Center : " << (*iterHull)->hullCenter.x << "\t" << (*iterHull)->hullCenter.y << "\t" << (*iterHull)->hullCenter.z << std::endl;
#endif

			// calcul des coordonn�es rep�re monde
			ComputePingHullWorldCoords(cosRoll, cosPitch, rollPitchFactor, cosHeading, sinHeading, transOrigin, transOriginGPS, *iterHull);

			iterHull++;
		}//while(iterHull != m_hullList.end())

		//MaJ point central
		double pointX_2D = pStat->GetHull2DCenter().x;
		double pointY_2D = pStat->GetHull2DCenter().y;

		pStat->GetHullCenter().x = pointX_2D * rollPitchFactor * sinHeading + transOrigin.x;
		pStat->GetHullCenter().y = pointX_2D * rollPitchFactor * cosHeading + transOrigin.y;
		pStat->GetHullCenter().z = pointY_2D * rollPitchFactor + transOrigin.z;

		// centre en coordonn�es Lat std::int32_t GPS
		CCartoTools::GetGeoCoords(transOriginGPS.x, transOriginGPS.y,
			pointX_2D * rollPitchFactor * sinHeading,
			pointX_2D * rollPitchFactor * cosHeading,
			pStat->GetHullCenterGPS().x,
			pStat->GetHullCenterGPS().y);
		pStat->GetHullCenterGPS().z = pointY_2D * rollPitchFactor + transOriginGPS.z;
	}
}

//*****************************************************************************
// Name : ComputePingHullWorldCoords
// Description : Calcul des coordonn�es rep�re monde des points de contour
// Parameters : * (in)
//				* (out) HullItem* hullItem
// Return : double
//*****************************************************************************
void ShoalExtractionStat::ComputePingHullWorldCoords(double cosRoll, double cosPitch,
	double rollPitchFactor, double cosHeading, double sinHeading,
	const BaseMathLib::Vector3D& transOrigin,
	const BaseMathLib::Vector3D& transOriginGPS,
	HullItem* hullItem)
{
	int nbPoints = hullItem->surfOrtho2.GetNbPoints();

	//allocation de la m�moire
	// OTK - FAE007 - ajout de la surface en coordonn�es GPS
	hullItem->surfHull.Alloc(nbPoints);
	hullItem->surfGeo.Alloc(nbPoints);
	auto iterPoints3D = hullItem->surfHull.GetPoints().begin();
	auto iterPoints3DGPS = hullItem->surfGeo.GetPoints().begin();

	Vector3D tmpV3D;
	double * iterPoints2D = hullItem->surfOrtho2.GetPoints();
	double lastX, lastY, lastZ;
	int realNbPoints = 0;

	double pointX_2D, pointY_2D;
	for (int i = 0; i < nbPoints; i++)
	{
		//coordonn�es 2D dans le plan
		pointX_2D = *(iterPoints2D);
		pointY_2D = *(iterPoints2D + 1);
		iterPoints2D += 2;


		//calcul des coordonn�es 3D en fonction des angles du navire et ajout de l'origine du transducteur
		double x = pointX_2D * rollPitchFactor * sinHeading;
		double y = pointX_2D * rollPitchFactor * cosHeading;
		double z = pointY_2D * rollPitchFactor;

		// suppression des doublons �ventuels (pas �limin�s avant car n�cessaires pour l'algo de calcul
		// de la surface 2D
		if (i == 0 || !GeometryTools2D::equals(x, lastX)
			|| !GeometryTools2D::equals(y, lastY)
			|| !GeometryTools2D::equals(z, lastZ))
		{
			iterPoints3D->x = x + transOrigin.x;
			iterPoints3D->y = y + transOrigin.y;
			iterPoints3D->z = z + transOrigin.z;

			// OTK - FAE007 - calcul des coordonn�es 3D GPS
			CCartoTools::GetGeoCoords(transOriginGPS.x, transOriginGPS.y,
				x, y, iterPoints3DGPS->x, iterPoints3DGPS->y);
			iterPoints3DGPS->z = z + transOriginGPS.z;

			iterPoints3D++;
			iterPoints3DGPS++;
			realNbPoints++;
		}

		lastX = x;
		lastY = y;
		lastZ = z;
	}

	// ajustement du nombre de points
	hullItem->surfHull.Shrink(realNbPoints);
	hullItem->surfGeo.Shrink(realNbPoints);
}

//*****************************************************************************
// Name : ComputePingBBoxParameters
// Description : Calcul des param�tres du ping pour la BBox
// Parameters : * (in) PingData* pPingData
//				* (out) double& centerAngle
// Return : double
//*****************************************************************************
void ShoalExtractionStat::ComputePingBBoxParameters(PingData* pPingData,
	double& centerAngle)
{
	//liste des surfaces 2D
	PingShoalStat* pStat = pPingData->GetShoalStat();
	int nbSurf = (int)pStat->GetHullList().size();
	std::vector<HullItem*>::iterator iterHull = pStat->GetHullList().begin();

	//centre des surfaces du ping
	Vector2D center2D = pStat->GetHull2DCenter();
	double pondDist = 0;

	while (iterHull != pStat->GetHullList().end())
	{
		int nbPoints = (*iterHull)->surfOrtho.GetNbPoints();

		double * iterPoints2D = (*iterHull)->surfOrtho.GetPoints();

		for (int i = 0; i < nbPoints; i++)
		{
			//coordonn�es 2D dans le plan
			Vector2D point2D(*(iterPoints2D), *(iterPoints2D + 1));
			double dist2D = (center2D - point2D).length();
			pondDist += dist2D;

			//calcul de l'angle au centre (-Pi/2 ; Pi/2) avec l'axe 2D X 
			double angle = atan2(point2D.x, point2D.y);
			angle = GeometryTools2D::SetInRange(-M_PI_2, M_PI_2, angle);

			centerAngle += angle * dist2D;

			iterPoints2D += 2;
		}

		iterHull++;
	}//while(iterHull != m_hullList.end())	

	if (pondDist != 0)
	{
		centerAngle /= pondDist;
	}

	//compensation avec l'angle de roulis du transducteur
	//centerAngle += pPingData->GetNavigation().roll;
}

//*****************************************************************************
// Name : ComputeShoalBBox
// Description : Calcul de la boite englobante 3D orient�e du ping
// Parameters : * (in) ShoalData* pShoalData
//              * (in) double headingShoalAngle,
//              * (in) double pitchShoalAngle,
//              * (in) double rollShoalAngle,
//              * (in) Vector3D& centerPoint
// Return : void
//*****************************************************************************
void ShoalExtractionStat::ComputeShoalBBox(ShoalData* pShoalData,
	double headingShoalAngle,
	double pitchShoalAngle,
	double rollShoalAngle,
	Vector3D& centerPoint,
	Vector3D& centerPointGPS)
{
	int nbPings = (int)pShoalData->GetPings().size();

	ShoalStat* pShoalStat = pShoalData->GetShoalStat();

	//Initialisation des axes de la obbox
	pShoalStat->GetOBBox().InitAxis(centerPoint, headingShoalAngle, pitchShoalAngle, rollShoalAngle);

	//Init box
	pShoalStat->GetOBBox().Init(centerPoint);
	pShoalStat->GetOBBox().InitCenterGPS(centerPointGPS.x, centerPointGPS.y, centerPointGPS.z);

	//pour tous les pings
	for (int iPing = 0; iPing < nbPings; iPing++)
	{
		PingData* pPingData = pShoalData->GetPings()[iPing];
		TransductData*	pTransData = pPingData->GetTransducers()[0]; //un seul transducteur pour un banc donn�

		//parcours de tous les faisceaux
		auto iterChannels = pTransData->GetChannels().begin();
		auto iterChannelsEnd = pTransData->GetChannels().end();
		while (iterChannels != iterChannelsEnd)
		{
			ChannelData* pChannelData = *iterChannels;
			size_t nbEchosGroup = pChannelData->GetGroupsFinal().size();

			if (nbEchosGroup > 0)
			{
				EchoGroup* pFirstEchoGroup = pChannelData->GetGroupsFinal()[0];

				//MaJ oBBox avec les points 3D du volume des groupes d'echos		
				pShoalStat->GetOBBox().Update(pFirstEchoGroup->GetVol3D().GetPoints());

				if (nbEchosGroup > 1)
				{
					EchoGroup* pLastEchoGroup = pChannelData->GetGroupsFinal()[nbEchosGroup - 1];
					pShoalStat->GetOBBox().Update(pLastEchoGroup->GetVol3D().GetPoints());
				}
			}
			iterChannels++;
		}//while(iterChannels != iterChannelsEnd)
	}

	//MaJ des dimensions du banc
	//on utilise le cap relatif du banc par rapport � la route moyenne du navire
	double relHeading = fabs(headingShoalAngle - pShoalStat->GetShipRoad());
	pShoalStat->ComputeShoalDimensions(relHeading, pitchShoalAngle, rollShoalAngle);

	//Calcul des points de la obbox
	pShoalStat->GetOBBox().ComputePoints();
}

//*****************************************************************************
// Name : ComputePingVolume
// Description : Calcul du volume d'un ping unique : 
// Parameters : * (in) PingData* pPingData
// Return : double
//*****************************************************************************
double ShoalExtractionStat::ComputePingVolume(PingData* pPingData)
{
	double result = 0;

	assert(pPingData->GetShoalStat() != NULL);
	//un banc ne peut etre calcul� que sur les donn�es d'un transducteur
	assert(pPingData->GetTransducers().size() == 1);

	TransductData* pTrans = pPingData->GetTransducers()[0];

	// le volume d'un ping unique est estim� par  :
	//	  aire * largeur moyenne 
	//--> aire * (Y2D_min + Y2D_max) * 0.5 * tan(angle longitudinal)
	//--> aire * (Z3D_min + Z3D_max)/(cosRoll * cosPitch) * 0.5 * tan(angle longitudinal)

	double cosRoll = cos(0.0/*pPingData->GetNavigation().roll*/);
	double cosPitch = cos(pTrans->GetPitch());
	double meanDepth3D = 0.5 * (pPingData->GetShoalStat()->GetMaxDepth() + pPingData->GetShoalStat()->GetMinDepth());
	double meanWidth = meanDepth3D / (cosRoll * cosPitch)  * tan(pPingData->GetShoalStat()->GetMeanAlongAngle());

	result = pPingData->GetShoalStat()->GetArea() * meanWidth;

	return result;
}

//*****************************************************************************
// Name : ComputeShoalVolume
// Description : Calcul du volume du banc : 
//		1/2 volume premier banc + 0.5 * (aire n + aire n+1) * distance [n n+1] + 1/2 volume dernier banc 
// Parameters : * (in) PingData* pPingData
// Return : void
//*****************************************************************************
void ShoalExtractionStat::ComputeShoalVolume(ShoalData* pShoalData)
{
	int nbPings = (int)pShoalData->GetPings().size();

	//ajout de la moiti� du volume du premier ping
	if (nbPings > 0)
	{
		pShoalData->GetShoalStat()->SetVolume(ComputePingVolume(pShoalData->GetPings()[0]) * 0.5);
	}

	//angle moyen au centre
	double centerAngle = 0.0;
	//point 3D central du banc
	Vector3D centerPoint;
	Vector3D centerPointGPS;
	//vecteur moyen 3D de ping � ping
	Vector3D longVect;
	//pond�ration des surfaces des pings
	double areaPond = 0.0;

	//cap moyen du navire
	double shipHeading = 0.0;

	//angles de la BBox
	double headingShoalAngle = 0.0;
	double pitchShoalAngle = 0.0;
	double rollShoalAngle = 0.0;

	//pour tous les intervalles de pings
	for (int iPing = 0; iPing < nbPings - 1; iPing++)
	{
		PingData* pPingN = pShoalData->GetPings()[iPing];
		PingData* pPingNP1 = pShoalData->GetPings()[iPing + 1];
		PingShoalStat* pStatN = pPingN->GetShoalStat();
		PingShoalStat* pStatNP1 = pPingNP1->GetShoalStat();

		//un banc ne peut etre calcul� que sur les donn�es d'un transducteur
		assert(pPingN->GetTransducers().size() == 1);
		assert(pPingNP1->GetTransducers().size() == 1);
		TransductData* pTransN = pPingN->GetTransducers()[0];
		TransductData* pTransNP1 = pPingNP1->GetTransducers()[0];

		//vecteur moyen 3D de ping � ping
		Vector3D longVectPing = pStatNP1->GetHullCenter() - pStatN->GetHullCenter();
		double dist = longVectPing.length();
		double vectHeading = atan2(longVectPing.y, longVectPing.x);
		// OTK - FAE016 - correction calcul volume du banc
		double horizontalDistance = sqrt(longVectPing.y*longVectPing.y + longVectPing.x*longVectPing.x);
		double vectPitch = 0;
		if (horizontalDistance != 0)
		{
			vectPitch = atan(longVectPing.z / horizontalDistance);
		}

		//projection de la distance en fonction des angles du navire
		double shipHeadingPing = 0.5 * (pPingN->GetNavigation().heading + pPingNP1->GetNavigation().heading);
		double shipPitch = 0.5 * (pTransN->GetPitch() + pTransNP1->GetPitch());
		dist = dist * fabs(cos(shipHeading - vectHeading)) * fabs(cos(shipPitch - vectPitch));

		//calcul de l'aire moyenne entre n et n+1
		double area = 0.5 * (pStatN->GetArea() + pStatNP1->GetArea());

		//MaJ volume
		pShoalData->GetShoalStat()->SetVolume(pShoalData->GetShoalStat()->GetVolume() + dist * area);

		//pond�ration des angles ping � ping par la surface moyenne des deux pings
		longVect = longVect + longVectPing * area;
		areaPond += area;
	}

	if (nbPings > 0)
	{
		//ajout de la moiti� du volume du dernier ping
		pShoalData->GetShoalStat()->SetVolume(
			pShoalData->GetShoalStat()->GetVolume() +
			ComputePingVolume(pShoalData->GetPings()[nbPings - 1]) * 0.5);

		//pond�ration des donn�es de la Bbox sur plusieurs pings
		if (nbPings > 1)
		{
			longVect /= areaPond;

			if (longVect.x != 0)
			{
				headingShoalAngle = atan2(longVect.y, longVect.x);
			}
			if (longVect.x != 0 && longVect.y != 0)
			{
				pitchShoalAngle = atan(longVect.z / sqrt(longVect.y * longVect.y + longVect.x * longVect.x));
			}

			//calcul de la route moyenne du navire
			BaseMathLib::Vector3D startPoint = pShoalData->GetPings()[0]->GetTransducers()[0]->GetOrigin3D();
			BaseMathLib::Vector3D endPoint = pShoalData->GetPings()[nbPings - 1]->GetTransducers()[0]->GetOrigin3D();
			BaseMathLib::Vector3D roadVect = endPoint - startPoint;

			pShoalData->GetShoalStat()->SetShipRoad(atan2(roadVect.y, roadVect.x));
		}
		//si il n'y a qu'un ping
		else
		{
			//on utilise l'orientation du navire
			PingData* pPingN = pShoalData->GetPings()[0];

			headingShoalAngle = pPingN->GetNavigation().heading;
			pitchShoalAngle = pPingN->GetNavigation().pitch;

			//sur un ping unique la route moyenne du navire est le cap du navire
			pShoalData->GetShoalStat()->SetShipRoad(pPingN->GetNavigation().heading);
		}
	}

	//pour tous les pings
	areaPond = 0.0;
	for (int iPing = 0; iPing < nbPings; iPing++)
	{
		PingData* pPingN = pShoalData->GetPings()[iPing];
		PingShoalStat* pStatN = pPingN->GetShoalStat();

		centerPoint = centerPoint + pStatN->GetHullCenter();
		centerPointGPS = centerPointGPS + pStatN->GetHullCenterGPS();

		//angle moyen au centre
		double centerAnglePing = 0.0;

		ComputePingBBoxParameters(pPingN, centerAnglePing);

		//projection de l'angle au centre sur le plan orthogonal au vecteur moyen du banc
		centerAnglePing *= cos(headingShoalAngle - pPingN->GetNavigation().heading);

		//pond�ration de l'angle moyen par la surface du ping
		rollShoalAngle += centerAnglePing * pStatN->GetArea();
		areaPond += pStatN->GetArea();
	}

	//pond�ration des donn�es de la Bbox
	if (nbPings > 0)
	{
		rollShoalAngle /= areaPond;
		centerPoint /= nbPings;
		centerPointGPS /= nbPings;
	}

	//calcul de la obbox
	ComputeShoalBBox(pShoalData, headingShoalAngle, pitchShoalAngle, rollShoalAngle,
		centerPoint, centerPointGPS);

#ifdef _SHOALSTAT_TRACE_PERF
	//trace_ofs << "Shoal Volume : " << pShoalData->GetShoalStat()->GetVolume() << "\tNb Pings : " << nbPings << std::endl;
#endif
}

//*****************************************************************************
// Name : ComputeShoalEnergy
// Description : Calcul des statistiques li�es aux energies du banc
// Parameters : * (in) ShoalData* pShoalData
//				* (in) bool extractEchos : extraction des echos
// Return : void
//*****************************************************************************
void ShoalExtractionStat::ComputeShoalEnergy(ShoalData* pShoalData)
{
	bool saveEchos = pShoalData->GetShoalStat()->GetParameter()->GetSaveEchos();

	PingFan * pFan = NULL;

	double energy = 0;
	double pondEnergy = 0;
	double volumeTotalEchos = 0;

	Vector3D gravityCenter;
	Vector3D geoCenter;
	Vector3D position;

	std::uint32_t nbEcho = 0;

	//parcours de tous les ping
	for (size_t iPing = 0; iPing < pShoalData->GetPings().size(); iPing++)
	{
		PingData* pPingData = pShoalData->GetPings()[iPing];

		double pingEnergy = 0;
		std::uint32_t nbEchoPing = 0;

		//un seul transducteur par banc
		TransductData* pTransData = pPingData->GetTransducers()[0];
		double beamsSamplesSpacing = pTransData->GetBeamsSamplesSpacing();

		pFan = pPingData->GetPingFan();
		int numTransducer = pTransData->GetTransId();
		MemoryStruct *pPolarMem = pFan->GetMemorySetRef()->GetMemoryStruct(numTransducer);

		//initialisation du stockage des echos
		if (saveEchos)
		{
			pPingData->GetShoalStat()->GetEchoList().resize(pPingData->GetShoalStat()->GetNbEchos());
		}

		//parcours de tous les faisceaux
		std::int32_t nbChannels = (std::int32_t)pTransData->GetChannels().size();

		for (int iChannel = 0; iChannel < nbChannels; iChannel++)
		{
			ChannelData* pChannelData = pTransData->GetChannels()[iChannel];
			int numBeam = pChannelData->GetChannelId();

			//calcul du volume unitaire pour le faisceau en fonction des angles d'ouverture
			// = volume du voxel d'indice 1 : volume de la pyramide de hauteur beamsSamplesSpacing
			// = 1/3 * b * h
			// = 1.0 / 3.0 * sin(angle longitudinal) * sin(angle transversal) * lg echant. faisceau ^ 3
			double unitVolume = 1.0 / 3.0 * sin(pChannelData->GetAlongAngle()) *
				sin(pChannelData->GetAthwartAngle()) *
				beamsSamplesSpacing*beamsSamplesSpacing*beamsSamplesSpacing;

			//parcours de tous les groupes d'�chos
			std::int32_t nbGroups = (std::int32_t)pChannelData->GetGroupsFinal().size();
			for (int iEchoGroup = 0; iEchoGroup < nbGroups; iEchoGroup++)
			{
				EchoGroup* pEchoGroup = pChannelData->GetGroupsFinal()[iEchoGroup];
				//parcours de tous les echos
				std::vector<float>::iterator iterEnergy = pEchoGroup->GetEchosEnergy().begin();
				std::int32_t iEcho = pEchoGroup->GetEchoMin();
				while (iterEnergy != pEchoGroup->GetEchosEnergy().end())
				{
					//calcul de l'�nergie de l'echo en lin�aire
					double echoEnergy = pow(10.0, *iterEnergy / 1000.0);

					//le volume du voxel d'indice X est :
					// volume de la pyramide de base (X+0.5) - volume de la pyramide de base (X-0.5)
					// = (X+0.5)^3 * volume unitaire -(X-0.5)^3 * volume unitaire
					double iEchoInf = iEcho - 0.5;
					double iEchoSup = iEcho + 0.5;
					double volumeEcho = unitVolume * (iEchoSup*iEchoSup*iEchoSup - iEchoInf*iEchoInf*iEchoInf);

					double along = 0;
					double athwart = 0;
					if (pPolarMem->GetPhase())
					{
						Phase valueAngle = *((Phase*)pPolarMem->GetPhase()->GetPointerToVoxel(Vector2I(numBeam, iEcho)));
						along = valueAngle.GetAlong();
						athwart = valueAngle.GetAthwart();
					}

					double temp = iEcho * beamsSamplesSpacing * cos(along) * cos(athwart);
					BaseMathLib::Vector3D coordSoftChannel(temp*tan(along), temp*tan(athwart), temp);



					
					position = pFan->getSounderRef()->GetSoftChannelCoordinateToGeoCoord(pFan, numTransducer, numBeam, coordSoftChannel);


					//     position = pFan->getSounderRef()->GetPolarToGeoCoord(pFan,
					  //     pTransData->GetTransId(), pChannelData->GetChannelId(), iEcho);

						 //MaJ des stat
					pingEnergy += echoEnergy;
					pondEnergy += volumeEcho * echoEnergy;
					volumeTotalEchos += volumeEcho;
					geoCenter = geoCenter + position;
					gravityCenter = gravityCenter + position * echoEnergy;

					if (nbEchoPing == 0 && nbEcho == 0)
					{
						pShoalData->GetShoalStat()->SetFirstPoint(position);
					}

					//extraction des echos � la demande
					if (saveEchos)
					{
						EchoInfo* pEchoInfo = &pPingData->GetShoalStat()->GetEchoList()[nbEchoPing];

						//position std::int32_t / lat / profondeur
						pEchoInfo->position = position;

						//energie  en DB
						pEchoInfo->energy = *iterEnergy * 0.01;

						pEchoInfo->channelNb = numBeam + 1;
					}
					nbEchoPing++;

					iterEnergy++;
					iEcho++;
				}
			}
		}

		energy += pingEnergy;
		nbEcho += nbEchoPing;
	}
	pShoalData->GetShoalStat()->SetLastPoint(position);
	pShoalData->GetShoalStat()->SetNbEchos(nbEcho);
	pShoalData->GetShoalStat()->SetSigmaAg(pondEnergy);

	if (nbEcho > 0)
	{
		pShoalData->GetShoalStat()->SetMeanSv(10.0 * log10(energy / (double)nbEcho));
		pShoalData->GetShoalStat()->SetGeographicCenter(geoCenter / (double)nbEcho);
	}
	if (volumeTotalEchos > 0)
	{
		pShoalData->GetShoalStat()->SetMeanPondSv(10.0 * log10(pondEnergy / volumeTotalEchos));
	}
	if (energy > 0)
	{
		pShoalData->GetShoalStat()->SetGravityCenter(gravityCenter / energy);
	}

#ifdef _SHOALSTAT_TRACE_PERF
	trace_ofs << "Shoal NbEchos : " << pShoalData->GetShoalStat()->GetNbEchos() << std::endl;
	trace_ofs << "Shoal SV mean pond : " << pShoalData->GetShoalStat()->GetMeanPondSv() << std::endl;
	trace_ofs << "Shoal SV mean : " << pShoalData->GetShoalStat()->GetMeanSv() << std::endl;
	trace_ofs << "Shoal SV total : " << pShoalData->GetShoalStat()->GetSv() << std::endl;
	trace_ofs << "Shoal GeographicCenter : " << pShoalData->GetShoalStat()->GetGeographicCenter() << std::endl;
	trace_ofs << "Shoal GravityCenter : " << pShoalData->GetShoalStat()->GetGravityCenter() << std::endl;

	trace_ofs << "Shoal Oriented Bounding Box " << std::endl;
	pShoalData->GetShoalStat()->GetOBBox().Serialize(trace_ofs);
#endif
}

//*****************************************************************************
// Name : FilterShoalSize
// Description : Filtre le banc par rapport a sa taille
// Parameters : * (in) ShoalData* pShoalData
// Return : true if the size filter is ok
//*****************************************************************************
bool ShoalExtractionStat::FilterShoalSize(ShoalData* pShoalData)
{
	bool result = false;

	ShoalExtractionParameter* param = pShoalData->GetShoalStat()->GetParameter();

	result = pShoalData->GetShoalStat()->GetLength() > param->GetMinLength() &&
		pShoalData->GetShoalStat()->GetWidth() > param->GetMinWidth() &&
		pShoalData->GetShoalStat()->GetHeigth() > param->GetMinHeight();

	return result;
}

//*****************************************************************************
// Name : ComputeShoalStats
// Description : Calcul des statistiques du banc
// Parameters : * (in) ShoalData* pShoalData
// Return : true if the size filter is ok
//*****************************************************************************
bool ShoalExtractionStat::ComputeShoalStats(ShoalData* pShoalData, double maxThreshold)
{
	bool result = false;

	//parcours de tous les ping
	for (size_t iPing = 0; iPing < pShoalData->GetPings().size(); iPing++)
	{
		PingData* pPingData = pShoalData->GetPings()[iPing];

		//initialisation d'une donn�e stat
		pPingData->SetShoalStat(new PingShoalStat());

		//si le ping n'as pas �t� completement ferm� 
		//(ESUEnd atteint pour le banc avant que le ping ne soit en limite de distance d'agr�gation), 
		//on force la r�cup�ration des donn�es du ping
		if (!pPingData->GetComplete())
		{
			CompletePingData(pPingData, maxThreshold);
		}

		//calcul des stat du ping
		ComputePingStats(pPingData);
		//calcul de la surface 2D de chaque composante du banc
		ComputePingSurfaces(pPingData);
		//calcul de l'enveloppe 3D de chaque composante du banc
		ComputePingHull(pPingData);

		//MaJ des stat du banc
		pShoalData->GetShoalStat()->SetAcrossAngleMin(min(pShoalData->GetShoalStat()->GetAcrossAngleMin(), pPingData->GetShoalStat()->GetAcrossAngleMin()));
        pShoalData->GetShoalStat()->SetAcrossAngleMax(std::max(pShoalData->GetShoalStat()->GetAcrossAngleMax(), pPingData->GetShoalStat()->GetAcrossAngleMax()));
		pShoalData->GetShoalStat()->SetMaxDepth(std::max(pShoalData->GetShoalStat()->GetMaxDepth(), pPingData->GetShoalStat()->GetMaxDepth()));
		pShoalData->GetShoalStat()->SetMinDepth(min(pShoalData->GetShoalStat()->GetMinDepth(), pPingData->GetShoalStat()->GetMinDepth()));
		pShoalData->GetShoalStat()->SetMaxDepth(std::max(pShoalData->GetShoalStat()->GetMaxDepth(), pPingData->GetShoalStat()->GetMaxDepth()));
		pShoalData->GetShoalStat()->SetMinBottomDistance(min(pShoalData->GetShoalStat()->GetMinBottomDistance(), pPingData->GetShoalStat()->GetMinBottomDistance()));
		pShoalData->GetShoalStat()->SetMaxBottomDistance(std::max(pShoalData->GetShoalStat()->GetMaxBottomDistance(), pPingData->GetShoalStat()->GetMaxBottomDistance()));

		// OTK - FAE019 - on doit se baser sur l'heure des pings et pas l'heure des tuple navigation !!
		if (pPingData->GetPingFan()->m_ObjectTime < pShoalData->GetShoalStat()->GetBeginTime())
		{
			pShoalData->GetShoalStat()->SetBeginTime(pPingData->GetPingFan()->m_ObjectTime);
		}
		if (pPingData->GetPingFan()->m_ObjectTime > pShoalData->GetShoalStat()->GetEndTime())
		{
			pShoalData->GetShoalStat()->SetEndTime(pPingData->GetPingFan()->m_ObjectTime);
		}
	}

	//calcul du volume du banc
	ComputeShoalVolume(pShoalData);

	//Filtrage de la taille du banc
	result = FilterShoalSize(pShoalData);

	if (result)
	{
		//Calcul des statistiques li�es aux energies du banc
		ComputeShoalEnergy(pShoalData);

#ifdef _SHOALSTAT_TRACE_PERF
		trace_ofs << "Shoal MinDepth : " << pShoalData->GetShoalStat()->GetMinDepth() << std::endl;
		trace_ofs << "Shoal MaxDepth : " << pShoalData->GetShoalStat()->GetMaxDepth() << std::endl;
		trace_ofs << "Shoal MinDistToBottom : " << pShoalData->GetShoalStat()->GetMinBottomDistance() << std::endl;
		trace_ofs << "Shoal MaxDistToBottom : " << pShoalData->GetShoalStat()->GetMaxBottomDistance() << std::endl;
#endif
	}

	//fermeture du banc
	pShoalData->GetShoalId()->SetClosed(true);

	// OTK - calcul des facettes de l'enveloppe du banc (trianglestrips)
	ComputeTriangleStrips(pShoalData);

	return result;
}


//*****************************************************************************
// Name : ComputeTriangleStrips
// Description : Calcul des facettes du banc
// Parameters : * (in) ShoalData* pShoalData
// Return : nothing
//*****************************************************************************
void ShoalExtractionStat::ComputeTriangleStrips(ShoalData* pShoalData)
{
	const vector<PingData*>& pings = pShoalData->GetPings();
	size_t nbPings = pings.size();

	// vecteur de travail pour les composantes du banc.
	// ce vecteur est utilis� pour les facettes dans le plan des pings.
	// ce vecteur est construit lors de la premi�re boucle sur les pings,
	// pour �tre utilis� lors de la second boucle.
	vector<vector<AggregatedHullItem*>> aggregatedComponents;
	aggregatedComponents.resize(nbPings);

	// Premi�re boucle sur l'ensemble des pings du banc pour le trac�
	// des facettes normales aux plan des pings,
	// et pr�paration de la g�n�ration des facettes dans le plan des pings
	// **************************************************

	// on note les surfaces merg�es cr�es pour pouvoir les d�truire
	vector<HullItem*> toDelete;

	for (size_t pingIdx = 1; pingIdx < nbPings; pingIdx++)
	{
		PingShoalStat* ping = pings[pingIdx]->GetShoalStat();
		// pour chaque surface du ping courant
		const vector<shoalextraction::HullItem*> &hulls = ping->GetHullList();

		// pour chaque surface du ping n
		size_t surfNb = hulls.size();
		for (size_t i = 0; i < surfNb; i++)
		{
			HullItem* hull = hulls[i];

			// si la composante a �t� trait�e pr�c�demment (dans le cas d'une liaison en Y ou W par exemple
			// on n'a pas a re-trianguler...
			if (!hull->triangulated)
			{
				// construction "r�cursive" de l'ensemble surfaces du ping pr�c�dent
				// li�es au ping courant, et de l'ensemble des surfaces du ping courant li�es � une de ces surfaces.

				// initialisation : on commence avec la surface du ping courant
				set<HullItem*> currentPingHulls;
				set<std::uint64_t> previousPingHullIDs;
				currentPingHulls.insert(hull);
				previousPingHullIDs = hull->linkedShoal2DComponents;

				// r�cup�ration des surfaces du ping pr�c�dent et construction de l'ensemble des surfaces du ping courant
				bool finished = false;
				while (!finished)
				{
					bool skip = false;
					// on parcourt les surfaces du ping courant � la recherche d'�ventuelle surfaces li�es aux surfaces du pign pr�c�dent
					for (size_t currentHullIdx = 0; currentHullIdx < surfNb && !skip; currentHullIdx++)
					{
						// si la surface est li�e avec une surface du ping pr�c�dent elle m�me li�e � la surface courante
						set<std::uint64_t>::iterator idsIter = previousPingHullIDs.begin();
						set<std::uint64_t>::iterator idsIterEnd = previousPingHullIDs.end();
						while (idsIter != idsIterEnd && !skip)
						{
							if (hulls[currentHullIdx]->linkedShoal2DComponents.find(*idsIter) != hulls[currentHullIdx]->linkedShoal2DComponents.end())
							{
								shoalextraction::HullItem* currentHull = hulls.at(currentHullIdx);
								// si la surface est d�j� l�, plus la peine de s'en occuper
								if (currentPingHulls.find(currentHull) == currentPingHulls.end())
								{
									// mise � jour de l'ensemble des composantes du ping courant
									currentPingHulls.insert(currentHull);
									// mise � jour des IDs des composantes du ping pr�c�dent
									previousPingHullIDs.insert(currentHull->linkedShoal2DComponents.begin(), currentHull->linkedShoal2DComponents.end());

									// on a trouv� une nouvelle surface pour le ping courant, on passe � l'it�ration suivante
									skip = true;
								}
							}
							idsIter++;
						}
					}
					if (!skip)
					{
						// on n'a rien trouv� de plus, c'est donc termin�
						finished = true;
					}
				}

				// cas d'une relation exclusive entre une surface du ping n-1 et une surface du ping n
				size_t nbCurrentHulls = currentPingHulls.size();
				// le nombre effectif de surfaces n'est pas le nombre de groupes d'�chos li�s � la surface courante
				// on doit rechercher l'ensemble de coques dont les IDs d'echogroupes intersecte
				// les IDs du std::set previousPingHullIDs
				set<HullItem*> previousConnectedHulls = pings[pingIdx - 1]->GetShoalStat()->GetHullItemsByIDs(previousPingHullIDs);
				size_t nbPreviousHulls = previousConnectedHulls.size();

				if (nbCurrentHulls == 1 && nbPreviousHulls == 1)
				{
					vector<vector<double>> gpsstrips = ComputeExclusiveLinkStrip(hull, *previousConnectedHulls.begin(), GPS_POSITIONNING);
					vector<vector<double>> incrementstrips = ComputeExclusiveLinkStrip(hull, *previousConnectedHulls.begin(), INCREMENT_POSITIONNING);

					for (size_t k = 0; k < gpsstrips.size(); k++)
					{
						pShoalData->GetGPSContourStrips().push_back(gpsstrips[k]);
					}

					for (size_t k = 0; k < incrementstrips.size(); k++)
					{
						pShoalData->GetContourStrips().push_back(incrementstrips[k]);
					}


					// remplissage du vecteur aggregatedComponents pour pr�paration de la seconde boucle
					AggregatedHullItem* prevAggregatedHull = new AggregatedHullItem();
					prevAggregatedHull->resultingHullItem = *previousConnectedHulls.begin();
					prevAggregatedHull->hullItems.insert(prevAggregatedHull->resultingHullItem);
					aggregatedComponents[pingIdx - 1].push_back(prevAggregatedHull);


					AggregatedHullItem* aggregatedHull = new AggregatedHullItem();
					aggregatedHull->resultingHullItem = hull;
					aggregatedHull->hullItems.insert(hull);
					aggregatedHull->prevAggregatedHullItem = prevAggregatedHull;
					aggregatedComponents[pingIdx].push_back(aggregatedHull);
				}
				// cas ou la surface courante est li�e � un ensemble de surfaces au ping pr�c�dent.
				else if (nbCurrentHulls >= 1 && nbPreviousHulls >= 1 && (nbCurrentHulls > 1 || nbPreviousHulls > 1))
				{
					// remplissage du vecteur aggregatedComponents pour pr�paration de la seconde boucle
					AggregatedHullItem* prevAggregatedHull = new AggregatedHullItem();
					aggregatedComponents[pingIdx - 1].push_back(prevAggregatedHull);


					AggregatedHullItem* aggregatedHull = new AggregatedHullItem();
					aggregatedHull->prevAggregatedHullItem = prevAggregatedHull;
					aggregatedComponents[pingIdx].push_back(aggregatedHull);

					// construction des surfaces englobantes

					// 1 - surface englobante au ping courant
					HullItem* currentBoundingHull = NULL;
					bool newCurrentHull = false; // indique si on doit d�truire la coque apr�s usage
					// si une seule surface, pas la peine de recalculer la surface englobante
					if (nbCurrentHulls == 1)
					{
						currentBoundingHull = *currentPingHulls.begin();
						aggregatedHull->hullItems.insert(currentBoundingHull);
					}
					else
					{
						set<std::uint64_t> currentComponents;
						set<HullItem*>::const_iterator hullIter = currentPingHulls.begin();
						set<HullItem*>::const_iterator hullIterEnd = currentPingHulls.end();
						while (hullIter != hullIterEnd)
						{
							currentComponents.insert((*hullIter)->shoal2DComponentIDs.begin(), (*hullIter)->shoal2DComponentIDs.end());
							aggregatedHull->hullItems.insert(*hullIter);
							// pour ne pas avoir du doublons dans les surfaces.
							(*hullIter)->triangulated = true;
							hullIter++;
						}

						currentBoundingHull = ComputePingSurface(pings[pingIdx], currentComponents, false);
						toDelete.push_back(currentBoundingHull);

						PingData* pPingData = pings[pingIdx];
						TransductData* pTransData = pPingData->GetTransducers()[0];
						double cosRoll = cos(0.0/*pPingData->GetNavigation().roll*/);
						double cosPitch = cos(pTransData->GetPitch());
						double rollPitchFactor = cosPitch * cosRoll;
						double cosHeading = cos(-pPingData->GetNavigation().heading);
						double sinHeading = sin(-pPingData->GetNavigation().heading);

						ComputePingHullWorldCoords(cosRoll, cosPitch, rollPitchFactor, cosHeading, sinHeading
							, pTransData->GetOrigin3D(), pTransData->GetOrigin3DGPS(), currentBoundingHull);
						newCurrentHull = true;
					}
					aggregatedHull->resultingHullItem = currentBoundingHull;

					// 2 - surface englobante au ping pr�c�dent
					HullItem* previousBoundingHull = NULL;
					bool newPreviousHull = false; // indique si on doit d�truire la coque apr�s usage
					if (nbPreviousHulls == 1)
					{
						previousBoundingHull = *previousConnectedHulls.begin();
						prevAggregatedHull->hullItems.insert(previousBoundingHull);
					}
					else
					{
						set<std::uint64_t> prevIds;
						set<HullItem*>::const_iterator prevHullsIter = previousConnectedHulls.begin();
						set<HullItem*>::const_iterator prevHullsIterEnd = previousConnectedHulls.end();
						while (prevHullsIter != prevHullsIterEnd)
						{
							prevIds.insert((*prevHullsIter)->shoal2DComponentIDs.begin(), (*prevHullsIter)->shoal2DComponentIDs.end());
							prevAggregatedHull->hullItems.insert(*prevHullsIter);
							prevHullsIter++;
						}

						previousBoundingHull = ComputePingSurface(pings[pingIdx - 1], prevIds, false);
						toDelete.push_back(previousBoundingHull);
						PingData* pPingData = pings[pingIdx - 1];
						TransductData* pTransData = pPingData->GetTransducers()[0];
						double cosRoll = cos(0.0/*pPingData->GetNavigation().roll*/);
						double cosPitch = cos(pTransData->GetPitch());
						double rollPitchFactor = cosPitch * cosRoll;
						double cosHeading = cos(-pPingData->GetNavigation().heading);
						double sinHeading = sin(-pPingData->GetNavigation().heading);

						ComputePingHullWorldCoords(cosRoll, cosPitch, rollPitchFactor, cosHeading, sinHeading
							, pTransData->GetOrigin3D(), pTransData->GetOrigin3DGPS(), previousBoundingHull);
						newPreviousHull = true;
					}
					prevAggregatedHull->resultingHullItem = previousBoundingHull;

					vector<vector<double>> gpsstrips = ComputeExclusiveLinkStrip(currentBoundingHull, previousBoundingHull, GPS_POSITIONNING);
					vector<vector<double>> incrementstrips = ComputeExclusiveLinkStrip(currentBoundingHull, previousBoundingHull, INCREMENT_POSITIONNING);

					for (size_t k = 0; k < gpsstrips.size(); k++)
					{
						pShoalData->GetGPSContourStrips().push_back(gpsstrips[k]);
					}

					for (size_t k = 0; k < incrementstrips.size(); k++)
					{
						pShoalData->GetContourStrips().push_back(incrementstrips[k]);
					}


				}
			} // endif triangulated
		}
	}

	// second boucle sur le pings pour les surfaces dans le plan des pings
	for (size_t pingIdx = 0; pingIdx < nbPings; pingIdx++)
	{
		// agr�gats mis en �vidence lors de la premi�re boucle
		vector<AggregatedHullItem*>& aggregates = aggregatedComponents[pingIdx];

		// on cr�e deux nouvelles listes d'aggr�gats � partir des aggr�gats de aggregatedComponents :
		// -> ceux ayant au moins 1 HullItem en commun, mais aussi au moins 1 diff�rent,
		//    son merg�s, et n�cessiteront une triangulation de surface par extrusion.
		// -> ceux qui n'ont rien en commun, auquel cas on trace la surface correspondante sans extrusion
		// -> les autres, cad ceux qui sont en double (m�mes composantes), pour lesquels rien
		//    ne doit �tre fait.

		// d�finition des facette resultant de la jointure de deux agr�gats (n�cessiteront une extrusion)
		vector<ExtrudedComponent*> mergedAggregates;
		// agregats n�cessitant le calcul d'une surface simple 
		vector<AggregatedHullItem*> simpleAggregates;

		// tri
		ClassifyAggregateHullItems(aggregates, simpleAggregates, mergedAggregates);

		// traitement des surfaces compl�tes
		size_t nbSurfaces = simpleAggregates.size();
		for (size_t i = 0; i < nbSurfaces; i++)
		{
			pShoalData->GetGPSContourStrips().push_back(ComputeHullStrip(simpleAggregates[i], GPS_POSITIONNING));
			pShoalData->GetContourStrips().push_back(ComputeHullStrip(simpleAggregates[i], INCREMENT_POSITIONNING));
			delete simpleAggregates[i];
		}

		// traitement des surfaces extrud�es
		nbSurfaces = mergedAggregates.size();
		for (size_t i = 0; i < nbSurfaces; i++)
		{
			vector<vector<double>> gpsStrips, incrementStrip;
			TriangulateExtrudedSurface(pings[pingIdx], mergedAggregates[i], gpsStrips, incrementStrip);
			for (size_t k = 0; k < gpsStrips.size(); k++)
			{
				pShoalData->GetGPSContourStrips().push_back(gpsStrips[k]);
			}

			for (size_t k = 0; k < incrementStrip.size(); k++)
			{
				pShoalData->GetContourStrips().push_back(incrementStrip[k]);
			}
			set<AggregatedHullItem*>::iterator mergeIter = mergedAggregates[i]->hullItems1.begin();
			set<AggregatedHullItem*>::iterator mergeIterEnd = mergedAggregates[i]->hullItems1.end();
			while (mergeIter != mergeIterEnd)
			{
				delete *mergeIter;
				mergeIter++;
			}
			mergeIter = mergedAggregates[i]->hullItems2.begin();
			mergeIterEnd = mergedAggregates[i]->hullItems2.end();
			while (mergeIter != mergeIterEnd)
			{
				delete *mergeIter;
				mergeIter++;
			}
			delete mergedAggregates[i];
		}
	}

	// Nettoyage des surfaces
	for (size_t pingIdx = 0; pingIdx < nbPings; pingIdx++)
	{
		size_t nbItems = aggregatedComponents[pingIdx].size();
		for (size_t itemIdx = 0; itemIdx < nbItems; itemIdx++)
		{
			delete aggregatedComponents[pingIdx][itemIdx];
		}

		const vector<HullItem*>& hulls = pings[pingIdx]->GetShoalStat()->GetHullList();
		size_t hullsNb = hulls.size();
		for (size_t hullIdx = 0; hullIdx < hullsNb; hullIdx++)
		{
			hulls[hullIdx]->surfGeo.PurgePoints();
			hulls[hullIdx]->surfHull.PurgePoints();
		}
	}

	size_t nbItemsToDelete = toDelete.size();
	for (size_t i = 0; i < nbItemsToDelete; i++)
	{
		delete toDelete[i];
	}
}


//*****************************************************************************
// Name : ComputeHullStrip
// Description : Calcul des facettes d'une composante d'un banc dans le plan
//               du ping
// Parameters : * (in) HullItem* hull
//                (in) PositionningType type
// Return : nothing
//*****************************************************************************
std::vector<double> ShoalExtractionStat::ComputeHullStrip(AggregatedHullItem* aggregatedHull,
	PositionningType positionType)
{
	HullItem* hull = aggregatedHull->resultingHullItem;

	std::vector<double> strip;

	Poly3D * stripSource;
	switch (positionType)
	{
	case GPS_POSITIONNING:
		stripSource = &hull->surfGeo;
		break;
	case INCREMENT_POSITIONNING:
		stripSource = &hull->surfHull;
		break;
	default:
		// type de positionnement inconnu
		assert(false);
		break;
	}

	// cr�ation du strip
	size_t nbPoints = stripSource->GetPoints().size();

	// les points vont en principe ici deux par deux.
	if (nbPoints % 2 == 0 && nbPoints > 3)
	{
		// normalDirection est vrai si la normale � la surface est dans la direction
		// (ou plutot le sens) du navire
		bool normalDirection = aggregatedHull->prevAggregatedHullItem == NULL ? false : true;

		size_t nbSegments = nbPoints / 2;

		// un strip est constitu� des 3 points du premier triangle, puis d'un point
		// pour chaque triangle suivant. Aucun point n'est donc r�p�t� inutilement.
		strip.reserve(nbPoints * 3);

		if (normalDirection)
		{
			// ajout des points au trianglestrip
			for (size_t i = 0; i < nbSegments; i++)
			{
				// point de la ligne inf�rieure de la surface 2D 
				strip.push_back(stripSource->GetPoints()[nbSegments + i].x);
				strip.push_back(stripSource->GetPoints()[nbSegments + i].y);
				strip.push_back(stripSource->GetPoints()[nbSegments + i].z);

				// point de la ligne sup�rieure de la surface 2D
				strip.push_back(stripSource->GetPoints()[i].x);
				strip.push_back(stripSource->GetPoints()[i].y);
				strip.push_back(stripSource->GetPoints()[i].z);
			}
		}
		else
		{
			// ajout des points au trianglestrip
			for (size_t i = 0; i < nbSegments; i++)
			{
				// point de la ligne sup�rieure de la surface 2D
				strip.push_back(stripSource->GetPoints()[i].x);
				strip.push_back(stripSource->GetPoints()[i].y);
				strip.push_back(stripSource->GetPoints()[i].z);

				// point de la ligne inf�rieure de la surface 2D 
				strip.push_back(stripSource->GetPoints()[nbSegments + i].x);
				strip.push_back(stripSource->GetPoints()[nbSegments + i].y);
				strip.push_back(stripSource->GetPoints()[nbSegments + i].z);
			}
		}
	}

	return strip;
}

//*****************************************************************************
// Name : ComputeExclusiveLinkStrip
// Description : Calcul des facettes d'une tranche de banc entre deux surfaces
//               associ�es une � une.
//               du ping
// Parameters : * (in) HullItem* hull
//                (in) HullItem* previousHull
//                (in) PositionningType type
// Return : nothing
//*****************************************************************************
vector<vector<double>> ShoalExtractionStat::ComputeExclusiveLinkStrip(HullItem* hull,
	HullItem* previousHull,
	PositionningType positionType)
{
	vector<vector<double>> strips;

	Poly3D * stripSource;
	Poly3D * previousStripSource;
	switch (positionType)
	{
	case GPS_POSITIONNING:
		stripSource = &hull->surfGeo;
		previousStripSource = &previousHull->surfGeo;
		break;
	case INCREMENT_POSITIONNING:
		stripSource = &hull->surfHull;
		previousStripSource = &previousHull->surfHull;
		break;
	default:
		// type de positionnement inconnu
		assert(false);
		break;
	}

	int hullPtsNb = (int)stripSource->GetPoints().size();
	int previousHullPtsNb = (int)previousStripSource->GetPoints().size();
	int nbPointsPerLine, previousNbPointsPerLine;
	nbPointsPerLine = hullPtsNb / 2;
	previousNbPointsPerLine = previousHullPtsNb / 2;

	// on commence par le plus simple : les deux tetraedres sur les c�t�s de la tranche

	// premier t�tra�dre
	vector<double> firstSide;
	firstSide.reserve(12);// 4 points * 3 dimensions

	// premier point de la ligne inf�rieure de la surface courante
	firstSide.push_back(stripSource->GetPoints()[nbPointsPerLine].x);
	firstSide.push_back(stripSource->GetPoints()[nbPointsPerLine].y);
	firstSide.push_back(stripSource->GetPoints()[nbPointsPerLine].z);

	// premier point de la ligne inf�rieure de la surface pr�c�dente
	firstSide.push_back(previousStripSource->GetPoints()[previousNbPointsPerLine].x);
	firstSide.push_back(previousStripSource->GetPoints()[previousNbPointsPerLine].y);
	firstSide.push_back(previousStripSource->GetPoints()[previousNbPointsPerLine].z);

	// premier point de la surface courante
	firstSide.push_back(stripSource->GetPoints()[0].x);
	firstSide.push_back(stripSource->GetPoints()[0].y);
	firstSide.push_back(stripSource->GetPoints()[0].z);

	// premier point de la surface pr�c�dente
	firstSide.push_back(previousStripSource->GetPoints()[0].x);
	firstSide.push_back(previousStripSource->GetPoints()[0].y);
	firstSide.push_back(previousStripSource->GetPoints()[0].z);

	strips.push_back(firstSide);


	// second t�tra�dre
	vector<double> lastSide;
	lastSide.reserve(12);// 4 points * 3 dimensions

	// dernier point de la ligne sup�rieure de la surface courante
	lastSide.push_back(stripSource->GetPoints()[nbPointsPerLine - 1].x);
	lastSide.push_back(stripSource->GetPoints()[nbPointsPerLine - 1].y);
	lastSide.push_back(stripSource->GetPoints()[nbPointsPerLine - 1].z);

	// dernier point de la ligne sup�rieure de la surface pr�c�dente
	lastSide.push_back(previousStripSource->GetPoints()[previousNbPointsPerLine - 1].x);
	lastSide.push_back(previousStripSource->GetPoints()[previousNbPointsPerLine - 1].y);
	lastSide.push_back(previousStripSource->GetPoints()[previousNbPointsPerLine - 1].z);

	// dernier point de la ligne inf�rieure de la surface courante
	lastSide.push_back(stripSource->GetPoints()[hullPtsNb - 1].x);
	lastSide.push_back(stripSource->GetPoints()[hullPtsNb - 1].y);
	lastSide.push_back(stripSource->GetPoints()[hullPtsNb - 1].z);

	// dernier point de la ligne inf�rieure de la surface pr�c�dente
	lastSide.push_back(previousStripSource->GetPoints()[previousHullPtsNb - 1].x);
	lastSide.push_back(previousStripSource->GetPoints()[previousHullPtsNb - 1].y);
	lastSide.push_back(previousStripSource->GetPoints()[previousHullPtsNb - 1].z);

	strips.push_back(lastSide);


	// ensuite: le dessus et le dessous de la tranche
	AddStripFromLines(stripSource, 0, nbPointsPerLine - 1,
		previousStripSource, 0, previousNbPointsPerLine - 1,
		strips);

	// dessous
	AddStripFromLines(previousStripSource, previousNbPointsPerLine, previousHullPtsNb - 1,
		stripSource, nbPointsPerLine, hullPtsNb - 1,
		strips);

	return strips;
}

// OTK - calcul d'un trianglestrip entre deux lignes de points
//*****************************************************************************
// Name : ComputeExclusiveLinkStrip
// Description : Calcul d'un trianglestrip entre deux lignes de points
//               du ping
// Parameters : * (in)  Poly3D* line1
//                (in)  size_t startIdx1
//                (in)  size_t stopIdx1
//                (in)  Poly3D* line2
//                (in)  size_t startIdx2
//                (in)  size_t stopIdx2
//                (out) vector<double>& stripResult
// Return : nothing
//*****************************************************************************
void ShoalExtractionStat::AddStripFromLines(Poly3D* line1, int startIdx1, int stopIdx1,
	Poly3D* line2, int startIdx2, int stopIdx2,
	vector<vector<double>>& stripResult)
{
	// on part du milieu de la ligne et on fait deux strips qui s'�cartent vers les extr�mit�s
	vector<double> rightStrip, leftStrip;

	int middleIndex1 = startIdx1 + (stopIdx1 - startIdx1) / 2;
	int middleIndex2 = startIdx2 + (stopIdx2 - startIdx2) / 2;
	int nbPts1 = stopIdx1 - middleIndex1 + 1;
	int nbPts2 = stopIdx2 - middleIndex2 + 1;


	// tant qu'il y a des points des deux c�t�s, on tisse le strip �quitablement entre les deux lignes
	int nbPts = min(nbPts1, nbPts2);
	// dans ce cas, pas assez de point pour faire un strip, on passe directement aux triangles � l'unit�
	if (nbPts > 1)
	{
		for (int i = 0; i < nbPts; i++)
		{
			rightStrip.push_back(line1->GetPoints()[i + middleIndex1].x);
			rightStrip.push_back(line1->GetPoints()[i + middleIndex1].y);
			rightStrip.push_back(line1->GetPoints()[i + middleIndex1].z);

			rightStrip.push_back(line2->GetPoints()[i + middleIndex2].x);
			rightStrip.push_back(line2->GetPoints()[i + middleIndex2].y);
			rightStrip.push_back(line2->GetPoints()[i + middleIndex2].z);
		}
		stripResult.push_back(rightStrip);
	}

	// ensuite, on ajoute un triangle pour chaque point suppl�mentaire de la ligne
	// la plus longue
	if (nbPts1 != nbPts2)
	{
		int nbTriangles, startOffset;
		bool mod;
		Poly3D * src;
		BaseMathLib::Vector3D p1;
		BaseMathLib::Vector3D p2;
		if (nbPts1 > nbPts2)
		{
			nbTriangles = nbPts1 - nbPts2;
			src = line1;
			startOffset = nbPts2 + middleIndex1;
			p2 = line2->GetPoints()[nbPts + middleIndex2 - 1];
			p1 = line1->GetPoints()[nbPts + middleIndex1 - 1];
			mod = false;
		}
		else
		{
			nbTriangles = nbPts2 - nbPts1;
			src = line2;
			startOffset = nbPts1 + middleIndex2;
			p2 = line1->GetPoints()[nbPts + middleIndex1 - 1];
			p1 = line2->GetPoints()[nbPts + middleIndex2 - 1];
			mod = true;
		}

		for (int i = 0; i < nbTriangles; i++)
		{
			vector<double> triangle;
			if (!mod)
			{
				triangle.push_back(p1.x);
				triangle.push_back(p1.y);
				triangle.push_back(p1.z);
				triangle.push_back(p2.x);
				triangle.push_back(p2.y);
				triangle.push_back(p2.z);
			}
			else
			{
				triangle.push_back(p2.x);
				triangle.push_back(p2.y);
				triangle.push_back(p2.z);
				triangle.push_back(p1.x);
				triangle.push_back(p1.y);
				triangle.push_back(p1.z);
			}

			p1 = src->GetPoints()[i + startOffset];
			triangle.push_back(p1.x);
			triangle.push_back(p1.y);
			triangle.push_back(p1.z);
			stripResult.push_back(triangle);
		}
	}

	// ligne de gauche
	nbPts1 = middleIndex1 - startIdx1 + 1;
	nbPts2 = middleIndex2 - startIdx2 + 1;
	nbPts = min(nbPts1, nbPts2);
	if (nbPts > 1)
	{
		for (int i = 0; i < nbPts; i++)
		{
			leftStrip.push_back(line2->GetPoints()[middleIndex2 - i].x);
			leftStrip.push_back(line2->GetPoints()[middleIndex2 - i].y);
			leftStrip.push_back(line2->GetPoints()[middleIndex2 - i].z);

			leftStrip.push_back(line1->GetPoints()[middleIndex1 - i].x);
			leftStrip.push_back(line1->GetPoints()[middleIndex1 - i].y);
			leftStrip.push_back(line1->GetPoints()[middleIndex1 - i].z);
		}
		stripResult.push_back(leftStrip);
	}

	// ensuite, on ajoute un triangle pour chaque point suppl�mentaire de la ligne
	// la plus longue
	if (nbPts1 != nbPts2)
	{
		int nbTriangles, startOffset;
		bool mod;
		Poly3D * src;
		BaseMathLib::Vector3D p1;
		BaseMathLib::Vector3D p2;
		if (nbPts1 > nbPts2)
		{
			nbTriangles = nbPts1 - nbPts2;
			src = line1;
			startOffset = middleIndex1 - nbPts2;
			p2 = line2->GetPoints()[middleIndex2 - nbPts + 1];
			p1 = line1->GetPoints()[middleIndex1 - nbPts + 1];
			mod = false;
		}
		else
		{
			nbTriangles = nbPts2 - nbPts1;
			src = line2;
			startOffset = middleIndex2 - nbPts1;
			p2 = line1->GetPoints()[middleIndex1 - nbPts + 1];
			p1 = line2->GetPoints()[middleIndex2 - nbPts + 1];
			mod = true;
		}

		for (int i = 0; i < nbTriangles; i++)
		{
			vector<double> triangle;
			if (!mod)
			{
				triangle.push_back(p2.x);
				triangle.push_back(p2.y);
				triangle.push_back(p2.z);
				triangle.push_back(p1.x);
				triangle.push_back(p1.y);
				triangle.push_back(p1.z);
			}
			else
			{
				triangle.push_back(p1.x);
				triangle.push_back(p1.y);
				triangle.push_back(p1.z);
				triangle.push_back(p2.x);
				triangle.push_back(p2.y);
				triangle.push_back(p2.z);
			}

			p1 = src->GetPoints()[startOffset - i];
			triangle.push_back(p1.x);
			triangle.push_back(p1.y);
			triangle.push_back(p1.z);
			stripResult.push_back(triangle);
		}
	}
}


// OTK - tri des aggregats 2D en listes des diff�rents types
//*****************************************************************************
// Name : ClassifyAggregateHullItems
// Description : tri des aggregats 2D en listes des diff�rents types.
// on cr�e deux nouvelles listes d'aggr�gats � partir des aggr�gats de aggregatedComponents :
// -> ceux ayant au moins 1 HullItem en commun, mais aussi au moins 1 diff�rent,
//    son merg�s, et n�cessiteront une triangulation de surface par extrusion.
// -> ceux qui n'ont rien en commun, auquel cas on trace la surface correspondante sans extrusion
// -> les autres, cad ceux qui sont en double (m�mes composantes), pour lesquels rien
//    ne doit �tre fait.
// Parameters : * (in)  const vector<AggregatedHullItem*>& in
//                (out)  vector<AggregatedHullItem*>& simple
//                (out)  vector<AggregatedHullItem*>& merged
// Return : nothing
//*****************************************************************************
void ShoalExtractionStat::ClassifyAggregateHullItems(vector<AggregatedHullItem*>& in,
	vector<AggregatedHullItem*>& simple, vector<ExtrudedComponent*>& merged)
{
	// on commence par mettre de c�t� l'ensemble des surfaces isol�es pour affichage complet
	// de la facette.
	bool deletedItem = false;
	for (size_t iter = 0; iter < in.size(); deletedItem ? iter : iter++)
	{
		deletedItem = false;
		bool isolated = true;
		AggregatedHullItem* couple = NULL;
		size_t coupleIndex;
		size_t nbAggregates = in.size();
		for (size_t i = 0; i < nbAggregates && isolated && !couple; i++)
		{
			AggregatedHullItem* aggregate2 = in[i];

			if (aggregate2 != in[iter])
			{
				// calcul de l'intersection entre les deux aggregats
				set<HullItem*> intersect;
				set_intersection(in[iter]->hullItems.begin(),
					in[iter]->hullItems.end(),
					aggregate2->hullItems.begin(),
					aggregate2->hullItems.end(),
					inserter(intersect, intersect.end()));

				if (intersect.size() != 0)
				{
					// il ne s'agit pas d'une surface isol�e
					isolated = false;
				}
				if (intersect.size() == aggregate2->hullItems.size()
					&& intersect.size() == in[iter]->hullItems.size())
				{
					// retrait et nettoyage des �l�ments en double (cas ou on n'a pas de facette � rajouter
					couple = aggregate2;
					coupleIndex = i;
				}
			}
		}

		if (isolated)
		{
			// il s'agit d'une surface isol�e, on supprime de l'entr�e et rajoute
			// dans la sortie
			simple.push_back(in[iter]);
			// on enleve l'�l�ment
			in.erase(in.begin() + iter);
			deletedItem = true;
		}
		if (couple)
		{
			// rien � faire, retrait des deux �l�ments du couple
			delete in[iter];
			delete in[coupleIndex];
			if (coupleIndex > iter)
			{
				in.erase(in.begin() + coupleIndex);
				in.erase(in.begin() + iter);
			}
			else
			{
				in.erase(in.begin() + iter);
				in.erase(in.begin() + coupleIndex);
			}

			deletedItem = true;
		}
	}

	// il ne reste dans "in" que les �l�ments induisant une facette extrud�e.
	// par construction, on a d'un cot� un agr�gat, de l'autre plusieurs aggr�gats ou surfaces englob�s
	// par le premier.

	// on vide le vecteur d'entr�e en construisant les merges les uns apr�s les autres
	while (in.size() != 0)
	{
		// construction de la d�finition du composant extrud� comprenant le premier �l�ment de in
		AggregatedHullItem* item = in[0];
		ExtrudedComponent* comp = new ExtrudedComponent();
		if (item->prevAggregatedHullItem == NULL)
		{
			comp->hullItems2.insert(item);
		}
		else
		{
			comp->hullItems1.insert(item);
		}
		// une fois un composant ajout�, on le supprime de "in"
		in.erase(in.begin());

		// on boucle sur les �l�ments restant de "in" pour trouver tous les �l�ments
		// en intersection avec comp.
		// si plus d'ajout en une passe, c'est fini
		bool change = true;
		while (change)
		{
			change = false;
			for (size_t i = 0; i < in.size(); i++)
			{
				item = in[i];
				if (item->prevAggregatedHullItem == NULL)
				{
					// intersection possible avec comp->hullItems1... on la teste
					bool found = false;
					set<AggregatedHullItem*>::const_iterator iter = comp->hullItems1.begin();
					set<AggregatedHullItem*>::const_iterator iterEnd = comp->hullItems1.end();
					while (iter != iterEnd && !found)
					{
						set<HullItem*> intersect;
						set_intersection(item->hullItems.begin(),
							item->hullItems.end(),
							(*iter)->hullItems.begin(),
							(*iter)->hullItems.end(),
							inserter(intersect, intersect.end()));

						if (intersect.size() != 0)
						{
							found = true;
						}

						iter++;
					}

					if (found)
					{
						comp->hullItems2.insert(item);
						change = true;
						// retrait de in
						in.erase(in.begin() + i);
						i--;
					}
				}
				else
				{
					// intersection possible avec comp->hullItems2... on la teste
					bool found = false;
					set<AggregatedHullItem*>::const_iterator iter = comp->hullItems2.begin();
					set<AggregatedHullItem*>::const_iterator iterEnd = comp->hullItems2.end();
					while (iter != iterEnd && !found)
					{
						set<HullItem*> intersect;
						set_intersection(item->hullItems.begin(),
							item->hullItems.end(),
							(*iter)->hullItems.begin(),
							(*iter)->hullItems.end(),
							inserter(intersect, intersect.end()));

						if (intersect.size() != 0)
						{
							found = true;
						}

						iter++;
					}

					if (found)
					{
						comp->hullItems1.insert(item);
						change = true;
						// retrait de in
						in.erase(in.begin() + i);
						i--;
					}
				}
			}
		}

		merged.push_back(comp);
		// on passe au suivant
	}
}

//*****************************************************************************
// Name : TriangulateExtrudedSurface
// Description : triangulation des facettes dans le plan du ping
//               permettant de fermer le contour du banc
// Parameters : * (in) PingData* pPingData
//              * (in) ExtrudedComponent* pExtrusionParameters
//              * (out) vector<vector<double>>& strips
// Return : void
//*****************************************************************************
void ShoalExtractionStat::TriangulateExtrudedSurface(PingData* pPingData,
	ExtrudedComponent* pExtrusionParameters,
	vector<vector<double>>& gpsStrips,
	vector<vector<double>>& incrementStrips)
{
	// *****************************************************************
	// on commence par r�cup�rer l'ensemble des segments de la surface globale
	// de fa�on � conna�tre la liste des angles � consid�rer, et �ventuellement � compl�ter
	// pour les agr�gats "sautant" des angles.
	// *****************************************************************

	// on fait la somme des IDs de toutes les surfaces pour avoir la surface globale
	set<std::uint64_t> pShoalIds;
	set<AggregatedHullItem*>::const_iterator iter = pExtrusionParameters->hullItems1.begin();
	set<AggregatedHullItem*>::const_iterator iterEnd = pExtrusionParameters->hullItems1.end();
	while (iter != iterEnd)
	{
		pShoalIds.insert((*iter)->resultingHullItem->shoal2DComponentIDs.begin(),
			(*iter)->resultingHullItem->shoal2DComponentIDs.end());
		iter++;
	}
	iter = pExtrusionParameters->hullItems2.begin();
	iterEnd = pExtrusionParameters->hullItems2.end();
	while (iter != iterEnd)
	{
		pShoalIds.insert((*iter)->resultingHullItem->shoal2DComponentIDs.begin(),
			(*iter)->resultingHullItem->shoal2DComponentIDs.end());
		iter++;
	}

	// on appelle la m�thode de calcul des segments sur cette surface globale
	vector<AngleSegment> globalSegments;
	ComputeSegments(pPingData, pShoalIds, globalSegments);

	TransductData* pTransData = pPingData->GetTransducers()[0];
	double sampleSpacing = pTransData->GetBeamsSamplesSpacing(); PingFan *pFan = pPingData->GetPingFan();

	Transducer *pTrans = pPingData->GetPingFan()->getSounderRef()->GetTransducer(pTransData->GetTransId());
	SoftChannel* firstSoftChannel = pTrans->getSoftChannelPolarX(0);
	SoftChannel* lastSoftChannel = pTrans->getSoftChannelPolarX((unsigned int)(pTrans->GetChannelId().size() - 1));
	double angleMin = firstSoftChannel->m_mainBeamAthwartSteeringAngleRad - 0.5*firstSoftChannel->m_beam3dBWidthAthwartRad;
	double angleMax = lastSoftChannel->m_mainBeamAthwartSteeringAngleRad + 0.5*lastSoftChannel->m_beam3dBWidthAthwartRad;
	static const double minDeltaAngle = (angleMax - angleMin) / 1000.0;


	// *****************************************************************
	// maintenant, pour chacune des surfaces intervenant dans l'extrusion,
	// on compl�te en calculant les intersections si il y a des
	// "trous d'angle" dedans.
	// *****************************************************************
	iter = pExtrusionParameters->hullItems1.begin();
	iterEnd = pExtrusionParameters->hullItems1.end();
	while (iter != iterEnd)
	{
		// calcul des segments de la composante
		ComputeSegments(pPingData, (*iter)->resultingHullItem->shoal2DComponentIDs,
			(*iter)->segments);

		// interpolation des segments manquants dans la composante
		// on recr�e un ensemble de segments en calculant les composantes en pr�cision double.
		// on fait ainsi d'une pierre deux coups : calcul des coordonn�es des surfaces,
		// et interpolation pr�cise (on ne doit pas se limiter aux numeros d'�chos)

		// calcul des coordonn�es des points de surface
		size_t nbSegments = (*iter)->segments.size();
		for (size_t i = 0; i < nbSegments; i++)
		{
			(*iter)->segments[i].ComputeCoords(sampleSpacing);
		}

		// interpolation des composantes manquantes, et calcul des n��chos �quivalents en arrondissant.
		InterpolateMissingValues(globalSegments, (*iter)->segments, sampleSpacing, minDeltaAngle);

		iter++;
	}
	iter = pExtrusionParameters->hullItems2.begin();
	iterEnd = pExtrusionParameters->hullItems2.end();
	while (iter != iterEnd)
	{
		// calcul des segments de la composante
		ComputeSegments(pPingData, (*iter)->resultingHullItem->shoal2DComponentIDs,
			(*iter)->segments);

		// interpolation des segments manquants dans la composante
		// on recr�e un ensemble de segments en calculant les composantes en pr�cision double.
		// on fait ainsi d'une pierre deux coups : calcul des coordonn�es des surfaces,
		// et interpolation pr�cise (on ne doit pas se limiter aux numeros d'�chos)

		// calcul des coordonn�es des points de surface
		size_t nbSegments = (*iter)->segments.size();
		for (size_t i = 0; i < nbSegments; i++)
		{
			(*iter)->segments[i].ComputeCoords(sampleSpacing);
		}

		// interpolation des composantes manquantes, et calcul des n��chos �quivalents en arrondissant.
		InterpolateMissingValues(globalSegments, (*iter)->segments, sampleSpacing, minDeltaAngle);

		iter++;
	}

	// triangulation des zones qui sont des facettes
	ComputeExtrudedStrips(globalSegments, pExtrusionParameters, gpsStrips, incrementStrips,
		pPingData, minDeltaAngle);
}


//*****************************************************************************
// Name : ComputeSegments
// Description : d�termination ent tri des segments constituant une surface de banc
// Parameters : * (in) TransductData* pTransData
//              * (in) set<std::uint64_t> pShoalIds
//              * (out) vector<AngleSegment>& resultingSegments
// Return : void
//*****************************************************************************
void ShoalExtractionStat::ComputeSegments(PingData* pPingData,
	set<std::uint64_t> pShoalIds,
	vector<AngleSegment>& resultingSegments)
{
	TransductData*	pTransData = pPingData->GetTransducers()[0]; //un seul transducteur pour un banc donn�
	std::set<int> componentChannels;
	int iChannel = 0;

	auto iterChannel = pTransData->GetChannels().begin();
	while (iterChannel != pTransData->GetChannels().end())
	{
		ChannelData* pChannelData = *iterChannel;

		int nbGroups = (int)pChannelData->GetGroupsFinal().size();
		for (int iGroup = 0; iGroup < nbGroups; iGroup++)
		{
			EchoGroup* pEchogroup = pChannelData->GetGroupsFinal()[iGroup];
			if (pShoalIds.find(pEchogroup->GetShoalId()->GetLinked2DShoalId()->GetShoalId()) != pShoalIds.end())
			{
				componentChannels.insert(iChannel);
				break;
			}
		}
		iterChannel++;
		iChannel++;
	}

	// liste des groupes d'�chos englobant pour chaque channel
	std::vector<SurroundingEchoGroup> surroundingEchoGroups;

	// 1 - parcours de tous les channels (sur lesquels on a des groupes d'�chos)
	std::set<int>::iterator iterComponentChannels = componentChannels.begin();
	for (int iChannel = 0; iterComponentChannels != componentChannels.end(); iChannel++, iterComponentChannels++)
	{
		ChannelData* pChannelData = pTransData->GetChannels()[*iterComponentChannels];

		// recherche des groupes concern�s
		EchoGroup* pFirstEchoGroup = NULL;
		EchoGroup* pLastEchoGroup = NULL;
		std::vector<EchoGroup*>::iterator iterGroup = pChannelData->GetGroupsFinal().begin();
		while (iterGroup != pChannelData->GetGroupsFinal().end())
		{
			if (pShoalIds.find((*iterGroup)->GetShoalId()->GetLinked2DShoalId()->GetShoalId()) != pShoalIds.end())
			{
				pFirstEchoGroup = (*iterGroup);
				break;
			}
			iterGroup++;
		}
		std::vector<EchoGroup*>::reverse_iterator iterGroupR = pChannelData->GetGroupsFinal().rbegin();
		while (iterGroupR != pChannelData->GetGroupsFinal().rend())
		{
			if (pShoalIds.find((*iterGroupR)->GetShoalId()->GetLinked2DShoalId()->GetShoalId()) != pShoalIds.end())
			{
				pLastEchoGroup = (*iterGroupR);
				break;
			}
			iterGroupR++;
		}

		// 2 - construction du "groupe d'�cho englobant" les groupes d'�chos de chaque channel
		if (pFirstEchoGroup != NULL && pLastEchoGroup != NULL)
		{
			SurroundingEchoGroup echoGroup;
			echoGroup.m_pFirstEchoGroup = pFirstEchoGroup;
			echoGroup.m_pLastEchoGroup = pLastEchoGroup;
			echoGroup.m_pChannelData = pChannelData;
			surroundingEchoGroups.push_back(echoGroup);
		}
	}

	// 3 - pour chacun de ces groupes d'�chos englobant, on a un couple de points de part et d'autre
	// de l'axe central du faisceau.
	size_t nbEchoGroups = surroundingEchoGroups.size();
	vector<SurfaceSegment> segments;
	segments.reserve(2 * nbEchoGroups);
	for (size_t i = 0; i < nbEchoGroups; i++)
	{
		SurfaceSegment segment;
		segment.m_pEchoGroup = &(surroundingEchoGroups[i]);
		segment.m_FirstEdge = true;
		segments.push_back(segment);
		segment.m_FirstEdge = false;
		segments.push_back(segment);
	}

	// 4 - on trie ces couples par ordre de d�pointage croissant
	std::sort(segments.begin(), segments.end());

	// 5 - pour chaque couple, on regarde quels groupes englobants 'recouvrent' le couple, et on construit
	// en cons�quence la surface
	int nbCouples = (int)segments.size();
	std::uint32_t lastechomin, lastechomax; // pour comparaison avec le segment pr�c�dent

	// pour la surface simplifi�e pour la repr�sentation 3D, 
	// on fusionne les segments dont les angles sont tr�s proches, pour ne pas avoir 
	// de courbes trop d�coup�es et "verticales", hachant les lignes du banc.
	double lastAngle = 0.0;
	// on autorise 1000 points sur la largeur du fan.
	PingFan *pFan = pPingData->GetPingFan();
	Sounder *pSounder = pFan->m_pSounder;
	Transducer *pTrans = pSounder->GetTransducer(pTransData->GetTransId());
	SoftChannel* firstSoftChannel = pTrans->getSoftChannelPolarX(0);
	SoftChannel* lastSoftChannel = pTrans->getSoftChannelPolarX((unsigned int)(pTrans->GetChannelId().size() - 1));
	double angleMin = firstSoftChannel->m_mainBeamAthwartSteeringAngleRad - 0.5*firstSoftChannel->m_beam3dBWidthAthwartRad;
	double angleMax = lastSoftChannel->m_mainBeamAthwartSteeringAngleRad + 0.5*lastSoftChannel->m_beam3dBWidthAthwartRad;
	static const double minDeltaAngle = (angleMax - angleMin) / 1000.0;

	for (int i = 0; i < nbCouples; i++)
	{
		// cas particulier pour le premier point, on ajoute le segment directement
		if (i == 0)
		{
			AngleSegment angleSegment;
			angleSegment.angle = segments[0].GetAngle();
			angleSegment.echoMin = segments[0].m_pEchoGroup->m_pFirstEchoGroup->GetEchoMin();
			angleSegment.echoMax = segments[0].m_pEchoGroup->m_pLastEchoGroup->GetEchomax();
			resultingSegments.push_back(angleSegment);
			lastAngle = segments[i].GetAngle();
		}
		else
		{
			SurfaceSegment couple = segments[i];
			double angle = couple.GetAngle();
			std::uint32_t echoMin = couple.m_pEchoGroup->m_pFirstEchoGroup->GetEchoMin();
			std::uint32_t echoMax = couple.m_pEchoGroup->m_pLastEchoGroup->GetEchomax();
			std::uint32_t minEchoMin = echoMin;
			std::uint32_t maxEchoMax = echoMax;
			// recherche des groupes d'�chos "englobants"
			for (size_t j = 0; j < nbEchoGroups; j++)
			{
				SurroundingEchoGroup group = surroundingEchoGroups[j];
				double minAngle = group.GetMinAngle();
				double maxAngle = group.GetMaxAngle();
				if (angle <= maxAngle && angle >= minAngle)
				{
					// dans ce cas, on a recouvrement
					std::uint32_t echoMin2 = group.m_pFirstEchoGroup->GetEchoMin();
					std::uint32_t echoMax2 = group.m_pLastEchoGroup->GetEchomax();
					minEchoMin = min(minEchoMin, echoMin2);
					maxEchoMax = std::max(maxEchoMax, echoMax2);
				}
			}

			// cr�ation du SurfaceSegment r�sultant si l'angle a assez vari�.
			bool newSimplifiedPoint = angle - lastAngle > minDeltaAngle;
			if (newSimplifiedPoint)
			{
				AngleSegment angleSegment;
				angleSegment.angle = angle;
				angleSegment.echoMin = minEchoMin;
				angleSegment.echoMax = maxEchoMax;
				resultingSegments.push_back(angleSegment);
				lastAngle = segments[i].GetAngle();
			}
			// sinon, modification du dernier AngleSegment en cons�quence
			else
			{
				if (minEchoMin < lastechomin)
				{
					resultingSegments[resultingSegments.size() - 1].echoMin = minEchoMin;
				}
				if (maxEchoMax > lastechomax)
				{
					resultingSegments[resultingSegments.size() - 1].echoMax = maxEchoMax;
				}
			}

			lastechomin = minEchoMin;
			lastechomax = maxEchoMax;
		}
	}
}

//*****************************************************************************
// Name : InterpolateMissingValues
// Description : ajout des segments "manquants" entre deux segments
// Parameters : * (in) vector<AngleSegment>& globalSegments
//              * (out) std::vector<AngleSegment>& segments
//              * (in) double sampleSpacing
// Return : void
//*****************************************************************************
void ShoalExtractionStat::InterpolateMissingValues(vector<AngleSegment>& globalSegments,
	std::vector<AngleSegment>& segments,
	double sampleSpacing,
	double minDeltaAngle)
{
	// on recherche le premier segment de "segments" dans le vecteur globalSegments.
	size_t firstGlobalIndex = 0;
	while (globalSegments[firstGlobalIndex].angle + minDeltaAngle < segments[0].angle)
	{
		firstGlobalIndex++;
	}
	// on recherche le dernier segment de "segments" dans le vecteur globalSegments
	size_t lastGlobalIndex = firstGlobalIndex;
	size_t realSegmentlastIndex = segments.size() - 1;
	while (globalSegments[lastGlobalIndex].angle < segments[realSegmentlastIndex].angle - minDeltaAngle)
	{
		lastGlobalIndex++;
	}

	// on parcours les segments globaux
	size_t realIndex = 0;
	for (size_t i = firstGlobalIndex; i <= lastGlobalIndex; i++)
	{
		// si l'angle du segment global n'est pas repr�sent� dans segments, on doit le rajouter
		// calculant les coordonn�es de l'intersection.
		if ((globalSegments[i].angle > (segments[realIndex].angle + minDeltaAngle)) || (globalSegments[i].angle < (segments[realIndex].angle - minDeltaAngle)))
		{
			AngleSegment newSegment;
			newSegment.angle = globalSegments[i].angle;
			// interpolation des coordonn�es :
			// point superieur du segment
			InterpolateSegment(globalSegments[i].angle, segments[realIndex - 1].x1, segments[realIndex - 1].y1,
				segments[realIndex].x1, segments[realIndex].y1, newSegment.x1, newSegment.y1);

			// point inf�rieur du segment
			InterpolateSegment(globalSegments[i].angle, segments[realIndex - 1].x2, segments[realIndex - 1].y2,
				segments[realIndex].x2, segments[realIndex].y2, newSegment.x2, newSegment.y2);

			// arrondi de l'�cho min et max (fonction inverse de celle qui fait de echo -> coordonn�e � coder)
			// calcul de l'hypoth�nuse
			double distance = sqrt(newSegment.x1*newSegment.x1 + newSegment.y1*newSegment.y1);
			newSegment.echoMin = (std::uint32_t)(distance / sampleSpacing + 0.5);
			distance = sqrt(newSegment.x2*newSegment.x2 + newSegment.y2*newSegment.y2);
			newSegment.echoMax = (std::uint32_t)(distance / sampleSpacing + 0.5);

			// insertion du nouveau AngleSegment au bon endroit
			segments.insert(segments.begin() + realIndex, newSegment);
		}
		realIndex++;
	}
}

//*****************************************************************************
// Name : InterpolateSegment
// Description : interpole un poitn du segment manquant
// Return : void
//*****************************************************************************
void ShoalExtractionStat::InterpolateSegment(double angle, double x1, double y1,
	double x2, double y2, double &x, double &y)
{
	// cas particulier de divide par 0 ...
	if (GeometryTools2D::equals(y2, y1))
	{
		// dans ce cas, on ne peut pas calculer (x2-x1/y1-y1) par la formule g�n�rale.
		// on proc�de donc autrement :
		y = y1;
		x = y*tan(angle);
	}
	else
	{
		// cas g�n�ral :
		double factor = (x2 - x1) / (y2 - y1);
		double tangente = tan(angle);

		// cas particulier angle nul
		if (GeometryTools2D::equals(tangente, 0.0))
		{
			x = 0.0;
			// factor nul impossible ... tous les points seraient sur le m�me faisceaux
			assert(!GeometryTools2D::equals(factor, 0.0));
			y = y1 - x1 / factor;
		}
		else
		{
			x = (x1 - factor*y1) / (1.0 - factor / tangente);
			y = x / tangente;
		}
	}
}

//*****************************************************************************
// Name : ComputeExtrudedStrips
// Description : calcul des facettes
// Return : void
//*****************************************************************************
void ShoalExtractionStat::ComputeExtrudedStrips(vector<AngleSegment>& globalSegments,
	ExtrudedComponent* pExtrusionParameters, vector<vector<double>>& gpsStrips,
	vector<vector<double>>& incrementStrips, PingData* pPingData,
	double minDeltaAngle)
{
	TransductData* pTransData = pPingData->GetTransducers()[0];
	double cosRoll = cos(0.0/*pPingData->GetNavigation().roll*/);
	double cosPitch = cos(pTransData->GetPitch());
	double rollPitchFactor = cosPitch * cosRoll;
	double cosHeading = cos(-pPingData->GetNavigation().heading);
	double sinHeading = sin(-pPingData->GetNavigation().heading);
	Vector3D transOrigin = pTransData->GetOrigin3D();
	Vector3D transOriginGPS = pTransData->GetOrigin3DGPS();

	// on travaille par pseudo-faisceau : un pseudo-faisceau est d�fini
	// par deux segments connexes de "globalSegments"
	size_t nbChannel = globalSegments.size() - 1;
	for (size_t i = 0; i < nbChannel; i++)
	{
		// on constitue un tableau1 et un tableau2 de l'ensemble des couples de segments
		// pour la liaison n-1 et n+1
		vector<AngleSegmentPair> array1;
		vector<AngleSegmentPair> array2;

		set<AggregatedHullItem*>::const_iterator iter = pExtrusionParameters->hullItems1.begin();
		set<AggregatedHullItem*>::const_iterator iterEnd = pExtrusionParameters->hullItems1.end();
		while (iter != iterEnd)
		{
			// recherche des segments correpondant et ajout au tableau
			size_t segmentsSize = (*iter)->segments.size();
			bool found = false;
			for (size_t j = 0; j < segmentsSize - 1 && !found; j++)
			{
				AngleSegment* pAngleSegment = &((*iter)->segments[j]);
				if (fabs(pAngleSegment->angle - globalSegments[i].angle) <= minDeltaAngle)
				{
					AngleSegmentPair pair;
					pair.angleSegment1 = *pAngleSegment;
					pair.angleSegment2 = ((*iter)->segments[j + 1]);
					pair.ComputeEchos();
					array1.push_back(pair);
					found = true;
				}
			}

			iter++;
		}
		iter = pExtrusionParameters->hullItems2.begin();
		iterEnd = pExtrusionParameters->hullItems2.end();
		while (iter != iterEnd)
		{
			// recherche des segments correpondants et ajout au tableau
			size_t segmentsSize = (*iter)->segments.size();
			bool found = false;
			for (size_t j = 0; j < segmentsSize - 1 && !found; j++)
			{
				AngleSegment* pAngleSegment = &((*iter)->segments[j]);
				if (fabs(pAngleSegment->angle - globalSegments[i].angle) <= minDeltaAngle)
				{
					AngleSegmentPair pair;
					pair.angleSegment1 = *pAngleSegment;
					pair.angleSegment2 = ((*iter)->segments[j + 1]);
					pair.ComputeEchos();
					array2.push_back(pair);
					found = true;
				}
			}

			iter++;
		}

		// trie les deux vecteurs
		sort(array1.begin(), array1.end());
		sort(array2.begin(), array2.end());

		// parcours du tableau 1 en strippant les zones non recouvertes par le tableau 2 et inversement
		bool finished = false;
		vector<AngleSegmentPair>* upperSource;
		vector<AngleSegmentPair>* lowerSource;
		while (array1.size() != 0 || array2.size() != 0)
		{
			// cas o� un tableau est vide � g�rer
			if (array1.size() == 0)
			{
				size_t size2 = array2.size();
				for (size_t j = 0; j < size2; j++)
				{
					TriangulateQuad(array2[0].angleSegment1.x1, array2[0].angleSegment1.y1,
						array2[0].angleSegment1.x2, array2[0].angleSegment1.y2,
						array2[0].angleSegment2.x1, array2[0].angleSegment2.y1,
						array2[0].angleSegment2.x2, array2[0].angleSegment2.y2,
						rollPitchFactor, cosHeading, sinHeading, transOrigin, transOriginGPS,
						false,
						gpsStrips, incrementStrips);

					array2.erase(array2.begin());
				}
			}
			else if (array2.size() == 0)
			{
				TriangulateQuad(array1[0].angleSegment1.x1, array1[0].angleSegment1.y1,
					array1[0].angleSegment1.x2, array1[0].angleSegment1.y2,
					array1[0].angleSegment2.x1, array1[0].angleSegment2.y1,
					array1[0].angleSegment2.x2, array1[0].angleSegment2.y2,
					rollPitchFactor, cosHeading, sinHeading, transOrigin, transOriginGPS,
					true,
					gpsStrips, incrementStrips);

				array1.erase(array1.begin());
			}
			else
			{
				bool upperIsArray1;
				if (array1[0].echoMin > array2[0].echoMin)
				{
					upperIsArray1 = false;
					lowerSource = &array1;
					upperSource = &array2;
				}
				else
				{
					upperIsArray1 = true;
					lowerSource = &array2;
					upperSource = &array1;
				}

				// pour toutes les composantes de upperSource sup�rieures ou �gales � la premi�re
				// composante de lowerSource, on triangule les facettes jusqu'� l'�chomax du lowerPair
				AngleSegmentPair lowerPair = (*lowerSource)[0];
				AngleSegmentPair upperPair = (*upperSource)[0];
				if (upperPair.echoMax <= lowerPair.echoMin)
				{
					// pas de recouvrement : on triangule toute la fa�ette
					TriangulateQuad(upperPair.angleSegment1.x1, upperPair.angleSegment1.y1,
						upperPair.angleSegment1.x2, upperPair.angleSegment1.y2,
						upperPair.angleSegment2.x1, upperPair.angleSegment2.y1,
						upperPair.angleSegment2.x2, upperPair.angleSegment2.y2,
						rollPitchFactor, cosHeading, sinHeading, transOrigin, transOriginGPS,
						upperIsArray1,
						gpsStrips, incrementStrips);

					// on supprime cette fa�cette qui ne sert plus � rien
					upperSource->erase(upperSource->begin());
				}
				else if (upperPair.echoMax > lowerPair.echoMin && upperPair.echoMax <= lowerPair.echoMax)
				{
					// triangulation de la partie entre les �chomin des deux composantes
					TriangulateQuad(upperPair.angleSegment1.x1, upperPair.angleSegment1.y1,
						lowerPair.angleSegment1.x1, lowerPair.angleSegment1.y1,
						upperPair.angleSegment2.x1, upperPair.angleSegment2.y1,
						lowerPair.angleSegment2.x1, lowerPair.angleSegment2.y1,
						rollPitchFactor, cosHeading, sinHeading, transOrigin, transOriginGPS,
						upperIsArray1,
						gpsStrips, incrementStrips);

					// le haut de la lowercomposante devient le bas de la upper
					(*lowerSource)[0].echoMin = upperSource->at(0).echoMax;
					(*lowerSource)[0].angleSegment1.x1 = upperSource->at(0).angleSegment1.x2;
					(*lowerSource)[0].angleSegment1.y1 = upperSource->at(0).angleSegment1.y2;
					(*lowerSource)[0].angleSegment2.x1 = upperSource->at(0).angleSegment2.x2;
					(*lowerSource)[0].angleSegment2.y1 = upperSource->at(0).angleSegment2.y2;

					// on supprima la upper
					upperSource->erase(upperSource->begin());
				}
				else
				{
					// dans ce cas, la upper recouvre enti�rement la lower :

					// triangulation de la partie entre les �chomin des deux composantes
					TriangulateQuad(upperPair.angleSegment1.x1, upperPair.angleSegment1.y1,
						lowerPair.angleSegment1.x1, lowerPair.angleSegment1.y1,
						upperPair.angleSegment2.x1, upperPair.angleSegment2.y1,
						lowerPair.angleSegment2.x1, lowerPair.angleSegment2.y1,
						rollPitchFactor, cosHeading, sinHeading, transOrigin, transOriginGPS,
						upperIsArray1,
						gpsStrips, incrementStrips);

					// le haut de la upper devient le bas de la lower
					(*upperSource)[0].echoMin = lowerSource->at(0).echoMax;
					(*upperSource)[0].angleSegment1.x1 = lowerSource->at(0).angleSegment1.x2;
					(*upperSource)[0].angleSegment1.y1 = lowerSource->at(0).angleSegment1.y2;
					(*upperSource)[0].angleSegment2.x1 = lowerSource->at(0).angleSegment2.x2;
					(*upperSource)[0].angleSegment2.y1 = lowerSource->at(0).angleSegment2.y2;

					// on supprime la lower
					lowerSource->erase(lowerSource->begin());
				}
			}
		}
	}
}

//*****************************************************************************
// Name : TriangulateQuad
// Description : triangulation d'une fecette � 4 sommets
// Return : void
//*****************************************************************************
void ShoalExtractionStat::TriangulateQuad(double x1, double y1, double x2, double y2,
	double x3, double y3, double x4, double y4,
	double rollPitchFactor, double cosHeading, double sinHeading,
	const Vector3D& transOrigin, const Vector3D& transOriginGPS,
	bool normalShipDirection,
	vector<vector<double>>& gpsStrips, vector<vector<double>>& incrementStrips)
{
	// premier triangle
	if (!(GeometryTools2D::equals(x1, x2) && GeometryTools2D::equals(y1, y2))
		&& !(GeometryTools2D::equals(x1, x3) && GeometryTools2D::equals(y1, y3))
		&& !(GeometryTools2D::equals(x3, x2) && GeometryTools2D::equals(y3, y2)))
	{
		vector<double> gpsStrip;
		gpsStrip.resize(9);
		vector<double> incrementStrip;
		incrementStrip.resize(9);

		double x = x1 * rollPitchFactor * sinHeading;
		double y = x1 * rollPitchFactor * cosHeading;
		double z = y1 * rollPitchFactor;
		CCartoTools::GetGeoCoords(transOriginGPS.x, transOriginGPS.y,
			x, y, gpsStrip[0], gpsStrip[1]);
		gpsStrip[2] = z + transOriginGPS.z;
		incrementStrip[0] = x + transOrigin.x;
		incrementStrip[1] = y + transOrigin.y;
		incrementStrip[2] = z + transOrigin.z;

		if (!normalShipDirection)
		{
			x = x2 * rollPitchFactor * sinHeading;
			y = x2 * rollPitchFactor * cosHeading;
			z = y2 * rollPitchFactor;
		}
		else
		{
			x = x3 * rollPitchFactor * sinHeading;
			y = x3 * rollPitchFactor * cosHeading;
			z = y3 * rollPitchFactor;
		}
		CCartoTools::GetGeoCoords(transOriginGPS.x, transOriginGPS.y,
			x, y, gpsStrip[3], gpsStrip[4]);
		gpsStrip[5] = z + transOriginGPS.z;
		incrementStrip[3] = x + transOrigin.x;
		incrementStrip[4] = y + transOrigin.y;
		incrementStrip[5] = z + transOrigin.z;

		if (!normalShipDirection)
		{
			x = x3 * rollPitchFactor * sinHeading;
			y = x3 * rollPitchFactor * cosHeading;
			z = y3 * rollPitchFactor;
		}
		else
		{
			x = x2 * rollPitchFactor * sinHeading;
			y = x2 * rollPitchFactor * cosHeading;
			z = y2 * rollPitchFactor;
		}
		CCartoTools::GetGeoCoords(transOriginGPS.x, transOriginGPS.y,
			x, y, gpsStrip[6], gpsStrip[7]);
		gpsStrip[8] = z + transOriginGPS.z;
		incrementStrip[6] = x + transOrigin.x;
		incrementStrip[7] = y + transOrigin.y;
		incrementStrip[8] = z + transOrigin.z;

		gpsStrips.push_back(gpsStrip);
		incrementStrips.push_back(incrementStrip);
	}

	if (!(GeometryTools2D::equals(x4, x2) && GeometryTools2D::equals(y4, y2))
		&& !(GeometryTools2D::equals(x4, x3) && GeometryTools2D::equals(y4, y3))
		&& !(GeometryTools2D::equals(x3, x2) && GeometryTools2D::equals(y3, y2)))
	{
		vector<double> gpsStrip;
		gpsStrip.resize(9);
		vector<double> incrementStrip;
		incrementStrip.resize(9);

		double x = x2 * rollPitchFactor * sinHeading;
		double y = x2 * rollPitchFactor * cosHeading;
		double z = y2 * rollPitchFactor;
		CCartoTools::GetGeoCoords(transOriginGPS.x, transOriginGPS.y,
			x, y, gpsStrip[0], gpsStrip[1]);
		gpsStrip[2] = z + transOriginGPS.z;
		incrementStrip[0] = x + transOrigin.x;
		incrementStrip[1] = y + transOrigin.y;
		incrementStrip[2] = z + transOrigin.z;

		if (normalShipDirection)
		{
			x = x3 * rollPitchFactor * sinHeading;
			y = x3 * rollPitchFactor * cosHeading;
			z = y3 * rollPitchFactor;
		}
		else
		{
			x = x4 * rollPitchFactor * sinHeading;
			y = x4 * rollPitchFactor * cosHeading;
			z = y4 * rollPitchFactor;
		}
		CCartoTools::GetGeoCoords(transOriginGPS.x, transOriginGPS.y,
			x, y, gpsStrip[3], gpsStrip[4]);
		gpsStrip[5] = z + transOriginGPS.z;
		incrementStrip[3] = x + transOrigin.x;
		incrementStrip[4] = y + transOrigin.y;
		incrementStrip[5] = z + transOrigin.z;

		if (normalShipDirection)
		{
			x = x4 * rollPitchFactor * sinHeading;
			y = x4 * rollPitchFactor * cosHeading;
			z = y4 * rollPitchFactor;
		}
		else
		{
			x = x3 * rollPitchFactor * sinHeading;
			y = x3 * rollPitchFactor * cosHeading;
			z = y3 * rollPitchFactor;
		}
		CCartoTools::GetGeoCoords(transOriginGPS.x, transOriginGPS.y,
			x, y, gpsStrip[6], gpsStrip[7]);
		gpsStrip[8] = z + transOriginGPS.z;
		incrementStrip[6] = x + transOrigin.x;
		incrementStrip[7] = y + transOrigin.y;
		incrementStrip[8] = z + transOrigin.z;


		gpsStrips.push_back(gpsStrip);
		incrementStrips.push_back(incrementStrip);
	}
}
