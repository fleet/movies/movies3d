// -*- C++ -*-
// ****************************************************************************
// Class: ChannelData
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "ShoalExtraction/data/channeldata.h"
#include <algorithm>

using namespace shoalextraction;
using namespace std;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
ChannelData::ChannelData()
{
	m_channelId = -1;
	m_groundEcho = 0;
	m_groundDepth = 0;

	// angle du faisceau
	m_steeringAngle = 0;
	// angle d'ouverture longitudinal � 3dB du faisceau
	m_athwartAngle = 0;
	// angle d'ouverture transversal � 3dB du faisceau
	m_alongAngle = 0;

	// tangage
	m_pitch = 0;

	//par d�faut le faisceau contient les donn�es d'�chos et les detruit en fin de vie
	m_deleteData = true;

	//frequence acoustique
	m_acousticFrequency = 0;
}

ChannelData::~ChannelData()
{
	//si les donn�es sont contenues et non pas seulement r�f�renc�es
	if (GetDeleteData())
	{
		std::vector<EchoGroup*>::iterator iter = m_groupsInitial.begin();
		while (iter != m_groupsInitial.end())
		{
			if (*iter != NULL)
			{
				delete *iter;
			}
			iter++;
		}
		iter = m_groupsFinal.begin();
		while (iter != m_groupsFinal.end())
		{
			if (*iter != NULL)
			{
				delete *iter;
			}
			iter++;
		}
	}
}


// *********************************************************************
// M�thodes
// *********************************************************************

//*****************************************************************************
// Name : AddEchoGroup
// Description : ajout d'un groupe d'�cho pour un banc donn�
//				 Les groupes d'�chos superpos�s sont fusionn�s
// Parameters : * (in) EchoGroup* : groupe d'�cho
// Return : EchoGroup* : pointeur vers le groupe d'�chos trouv�/cr��
//	Rq : les valeurs d'�chos sont en std::int32_t au lieu de std::uint32_t pour eviter des cast (perf!)
//*****************************************************************************
EchoGroup* ChannelData::AddEchoGroup(std::int32_t echoMin, std::int32_t echoMax, std::shared_ptr<ShoalId> shoalId)
{
	//recherche des groupe d'�cho superpos�s
	ChannelData* result = NULL;
	//premier groupe superpos�
	EchoGroup* pEchoGroup = NULL;

	std::vector<EchoGroup*>::iterator iter = GetGroupsFinal().begin();
	std::vector<EchoGroup*>::iterator previous = iter;
	std::vector<EchoGroup*> toDelete;

	std::int32_t currentEchoMax = 0;
	std::int32_t currentEchoMin = 0;

	//troncature par le fond
    echoMax = std::min(echoMax, (std::int32_t)GetGroundEcho());
    echoMin = std::min(echoMin, (std::int32_t)GetGroundEcho());

	while (iter != GetGroupsFinal().end() && (currentEchoMin - 2) < echoMax)
	{
		EchoGroup* currentGroup = *iter;

		if (currentGroup != NULL)
		{
			previous = iter;

			currentEchoMax = currentGroup->GetEchomax();
			currentEchoMin = currentGroup->GetEchoMin();

			//les groupes d'�chos contigus sont regroup�s
			if ((currentEchoMin - 2) < echoMax && (currentEchoMax + 2) > echoMin)
			{
				//r�cup�ration du premier groupe superpos� pour �viter d'avoir a r�allouer
				if (pEchoGroup == NULL)
				{
					pEchoGroup = currentGroup;
				}
				//sinon, suppression du groupe superpos� redondant
				else
				{
					//retrait du groupe d'�chos courant car il est superpos� au nouveau groupe, MaJ
					toDelete.push_back(currentGroup);
					// OTK - fusion des liens entre les groupes d'�chos
					pEchoGroup->GetShoalId()->GetPreviousPing2DShoalIds().insert(
						currentGroup->GetShoalId()->GetPreviousPing2DShoalIds().begin(),
						currentGroup->GetShoalId()->GetPreviousPing2DShoalIds().end());
					pEchoGroup->GetShoalId()->GetMergedShoalIds().insert(currentGroup->GetShoalId()->GetShoalId());
					pEchoGroup->GetShoalId()->GetMergedShoalIds().insert(currentGroup->GetShoalId()->GetMergedShoalIds().begin(),
						currentGroup->GetShoalId()->GetMergedShoalIds().end());
				}

				//MaJ des min et max du nouvel echo
                pEchoGroup->SetEchoMin(std::min((std::int32_t)pEchoGroup->GetEchoMin(), currentEchoMin));
                pEchoGroup->SetEchoMax(std::max((std::int32_t)pEchoGroup->GetEchomax(), currentEchoMax));
			}
		}
		iter++;
	}

	//cr�ation et insertion du groupe d'�cho si aucun groupe superpos�
	if (pEchoGroup == NULL)
	{
		pEchoGroup = new EchoGroup();
		pEchoGroup->SetEchoMin(echoMin);
        pEchoGroup->SetEchoMax(echoMax);
		pEchoGroup->SetShoalId(shoalId);

		//insertion ou ajout selon la position des echos dans le tableau
		if (currentEchoMin - 1 > echoMax)
		{
			GetGroupsFinal().insert(previous, pEchoGroup);
		}
		else
		{
			GetGroupsFinal().push_back(pEchoGroup);
		}
	}

	if (toDelete.size() > 0)
	{
		iter = toDelete.begin();
		while (iter != toDelete.end())
		{
			if (*iter != NULL)
			{
				std::vector<EchoGroup*>::iterator iterFind =
					std::find(GetGroupsFinal().begin(), GetGroupsFinal().end(), *iter);

				if (iterFind != GetGroupsFinal().end())
				{
					EchoGroup* pEchoGroupTmp = *iterFind;

					GetGroupsFinal().erase(iterFind);
					delete pEchoGroupTmp;
				}
			}
			iter++;
		}
		toDelete.clear();
	}

	return pEchoGroup;
}
