// -*- C++ -*-
// ****************************************************************************
// Class: EchoGroup
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "ShoalExtraction/data/echogroup.h"

using namespace shoalextraction;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
EchoGroup::EchoGroup()
{
	m_echoMin = 0;
	m_echoMax = 0;
}

EchoGroup::~EchoGroup()
{
}


// *********************************************************************
// M�thodes
// *********************************************************************

// clone simple
EchoGroup* EchoGroup::Clone() const
{
	EchoGroup* result = new EchoGroup();

	result->m_echoMin = m_echoMin;
	result->m_echoMax = m_echoMax;
	result->m_shoalId = m_shoalId;

	return result;
}
