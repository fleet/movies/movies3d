// -*- C++ -*-
// ****************************************************************************
// Class: PingData
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Société : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "ShoalExtraction/data/pingdata.h"
#include "ShoalExtraction/data/pingshoalstat.h"

using namespace shoalextraction;
using namespace std;

namespace
{
	constexpr const char * LOGGER_NAME = "ShoalExtraction.PingData";
}

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par défaut
PingData::PingData(): m_pingId(-1), m_pingFan(nullptr), m_sounderId(-1), m_complete(false), m_pShoalStat(nullptr)
{
}

PingData::~PingData()
{
	//destruction des données des transducteurs
	for (const auto transducer : m_transducers)
	{
		delete transducer;
	}
	m_transducers.clear();

	//destruction des données stat
	if (m_pShoalStat != nullptr)
	{
		delete m_pShoalStat;
		m_pShoalStat = nullptr;
	}
}

//*****************************************************************************
// Name : GetTransducer
// Description : Récupération des données transducteurs
// Parameters : * (in) int transId
// Return : const TransductData*
//*****************************************************************************
const TransductData* PingData::GetTransducer(const int transId) const
{
	const auto transducer = std::find_if(
		m_transducers.cbegin(),
		m_transducers.cend(),
		[&](const TransductData * transductData){ return transductData->GetTransId() == transId; }
		);

	return transducer != m_transducers.cend() ? *transducer : nullptr;
}

// Données de navigation

Navigation & PingData::GetNavigation() { 
	assert(m_navigations.size() == 1);
	return m_navigations.begin()->second; 
}

Navigation & PingData::GetNavigation(unsigned short channelId)
{
	return m_navigations[channelId];
}

const std::map<unsigned short, Navigation>& PingData::GetNavigations() const { return m_navigations; }
void PingData::SetNavigations(const std::map<unsigned short, Navigation>& navigations) { m_navigations = navigations; }


// *********************************************************************
// Méthodes
// *********************************************************************


//*****************************************************************************
// Name : UpdateShoalId
// Description : MaJ des id de banc pour tout le ping
// Parameters : * void
// Return :void
//*****************************************************************************
void PingData::UpdateShoalId()
{
}

