// -*- C++ -*-
// ****************************************************************************
// Class: PingShoalStat
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <float.h>
#include "ShoalExtraction/data/pingshoalstat.h"

using namespace shoalextraction;
using namespace std;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
PingShoalStat::PingShoalStat()
{
	//profondeur min
	m_minDepth = std::numeric_limits<float>::max();
	//profondeur max
	m_maxDepth = 0;

	//distance min au fond 
	m_minBottomDistance = std::numeric_limits<float>::max();
	//distance max au fond 
	m_maxBottomDistance = 0;

	//angle lat�ral min du banc
	m_acrossAngleMin = std::numeric_limits<double>::max();
	//angle lat�ral max du banc
	m_acrossAngleMax = -std::numeric_limits<double>::max();

	//nombre d'echos
	m_nbEchos = 0;

	//aire
	m_area = 0;
	//angle moyen d'ouverture longitudinal
	m_meanAlongAngle = 0;
}

PingShoalStat::~PingShoalStat()
{
	//liste des surfaces 2D convexes et de l'enveloppe 3D correspondante
	std::vector<HullItem*>::iterator iterHull = m_hullList.begin();

	while (iterHull != m_hullList.end())
	{
		delete *iterHull;
		iterHull++;
	}
	m_hullList.clear();
}


// *********************************************************************
// M�thodes
// *********************************************************************

// r�cup�ration d'une composante 2D par ID
HullItem* PingShoalStat::GetHullItemByID(std::uint64_t id)
{
	HullItem* result = NULL;
	vector<HullItem*>::const_iterator iter = m_hullList.begin();
	vector<HullItem*>::const_iterator iterEnd = m_hullList.end();
	while (iter != iterEnd && result == NULL)
	{
		if ((*iter)->shoal2DComponentIDs.find(id) != (*iter)->shoal2DComponentIDs.end())
		{
			result = *iter;
		}
		iter++;
	}

	return result;
}

// r�cup�ration des composantes d'IDs donn�es
set<HullItem*> PingShoalStat::GetHullItemsByIDs(const set<std::uint64_t>& ids)
{
	set<HullItem*> result;
	set<std::uint64_t>::const_iterator iter = ids.begin();
	set<std::uint64_t>::const_iterator iterEnd = ids.end();
	while (iter != iterEnd)
	{
		HullItem* hull = GetHullItemByID(*iter);
		if (hull)
		{
			result.insert(hull);
		}
		iter++;
	}

	return result;
}
