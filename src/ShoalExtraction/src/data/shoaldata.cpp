// -*- C++ -*-
// ****************************************************************************
// Class: ShoalData
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include <sstream>
#include "ShoalExtraction/data/shoaldata.h"
#include "ShoalExtraction/data/pingdata.h"
#include "ShoalExtraction/data/shoalstat.h"
#include "ShoalExtraction/data/pingshoalstat.h"
#include "ShoalExtraction/ShoalExtractionParameter.h"

using namespace shoalextraction;
using namespace std;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
ShoalData::ShoalData()
{
	m_externId = 0;
	m_shoalId = NULL;
	m_sounderId = -1;
	m_transId = -1;
	m_pShoalStat = NULL;
	m_iLockDestruction = 0;
	m_bSelected = false;
}

ShoalData::~ShoalData()
{
	PurgePings();

	//destruction des donn�es stat
	if (m_pShoalStat != NULL)
	{
		delete m_pShoalStat;
		m_pShoalStat = NULL;
	}
}

/**
* PurgePings
* Methode : purge
* @param void
* @return void
*/
void ShoalData::PurgePings()
{
	std::vector<PingData*>::iterator iter = m_pings.begin();
	while (iter != m_pings.end())
	{
		delete *iter;
		iter++;
	}
	m_pings.clear();
}


// *********************************************************************
// M�thodes
// *********************************************************************

//*****************************************************************************
// Name : Serialize
// Description : Ecriture des param�tres du banc
// Parameters :
// Return : void
//*****************************************************************************
void ShoalData::Serialize(const std::string& folder)
{
	std::ofstream ofs;
	std::ostringstream sstream;

	sstream << folder.c_str() << '/' << "shoal_" << GetExternId() << ".txt";
	ofs.open(sstream.str().c_str());
	if (!ofs.fail())
	{
		ofs << "Shoal ID : " << GetExternId() << " intern ID : " << GetShoalId()->GetLinked3DShoalId()->GetShoalId() << std::endl;

		//parcours de tous les ping
		for (size_t iPing = 0; iPing < GetPings().size(); iPing++)
		{
			PingData* pPingData = GetPings()[iPing];

			//parcours de tous les GetTransducers() (normalement 1 seul)
			assert(pPingData->GetTransducers().size() == 1);

			for (size_t iTrans = 0; iTrans < pPingData->GetTransducers().size(); iTrans++)
			{
				TransductData* pTransData = pPingData->GetTransducers()[iTrans];

				//parcours de tous les faisceaux
				for (size_t iChannel = 0; iChannel < pTransData->GetChannels().size(); iChannel++)
				{
					ChannelData* pChannelData = pTransData->GetChannels()[iChannel];

					//parcours de tous les groupes d'�chos
					for (size_t iEchoGroup = 0; iEchoGroup < pChannelData->GetGroupsFinal().size(); iEchoGroup++)
					{
						EchoGroup* pEchoGroup = pChannelData->GetGroupsFinal()[iEchoGroup];
						//ofs << "ping " << pPingData->GetPingId() << "\tsondeur " << pPingData->GetSounderId() << "\ttransducteur " << iTrans << "\tchannel " << pChannelData->GetChannelId() << "\techo " << pEchoGroup->GetEchoMin() << " <--> " << pEchoGroup->GetEchomax() << std::endl;

						if (iEchoGroup > 1)
						{
							ofs << "\tMULTIECHOS";
						}
						pEchoGroup->GetVol3D().Serialize(ofs);
					}
				}
			}
		}
		ofs.close();
	}
}

//*****************************************************************************
// Name : SerializeCSV
// Description : Ecriture des param�tres du banc
// Parameters :
// Return : void
//*****************************************************************************
void ShoalData::SerializeCSV(std::ofstream& ofs)
{
	ofs << std::endl;

	//version de l'algo
	ofs << "$SHOAL;ALGO;VERSION;0001;0000;;;;" << std::endl;

	//id des pings de debut et de fin
	ofs << "$SHOAL;PINGN;STARTEND;" << GetExternId() << ";"
		<< GetPings()[0]->GetPingId() << ";"
		<< GetPings()[GetPings().size() - 1]->GetPingId() << ";;;" << std::endl;

	//nb echos
	ofs << "$SHOAL;ECHOES;NUMBER;" << GetExternId()
		<< ";" << GetShoalStat()->GetNbEchos() << ";;;;" << std::endl;

	//Crit�res d�extraction du banc
	ofs.precision(4);
	ofs << "$SHOAL;CRITERIA;" << GetExternId() << ";"
		<< GetShoalStat()->GetVerticalIntegrationDistance() << ";"
		<< GetShoalStat()->GetParameter()->GetAlongIntegrationDistance() << ";"
		<< GetShoalStat()->GetParameter()->GetAcrossIntegrationDistance() << ";"
		<< GetShoalStat()->GetParameter()->GetThreshold() << ";"
		<< GetShoalStat()->GetParameter()->GetMinLength() << ":"
		<< GetShoalStat()->GetParameter()->GetMinWidth() << ":"
		<< GetShoalStat()->GetParameter()->GetMinHeight() << ";" << std::endl;

	//profondeur minimales et maximales
	ofs << "$SHOAL;DEPTH;MINMAX;" << GetExternId() << ";"
		<< GetShoalStat()->GetMinDepth() << ";" << GetShoalStat()->GetMaxDepth()
		<< ";;" << std::endl;

	//distance au fond minimales et maximales
	ofs << "$SHOAL;DEPTH;DTBOT;" << GetExternId() << ";"
		<< GetShoalStat()->GetMinBottomDistance() << ";" << GetShoalStat()->GetMaxBottomDistance()
		<< ";;" << std::endl;

	//temps de d�but et de fin
	char dateBegin[512];
	char dateEnd[512];
	GetShoalStat()->GetBeginTime().GetTimeDesc(dateBegin, sizeof(dateBegin));
	GetShoalStat()->GetEndTime().GetTimeDesc(dateEnd, sizeof(dateEnd));
	std::string beginTime = dateBegin;
	std::string endTime = dateEnd;
	beginTime.replace(beginTime.find(" "), 1, ";");
	endTime.replace(endTime.find(" "), 1, ";");
	ofs << "$SHOAL;TIME;STARTEND;" << GetExternId() << ";"
		<< beginTime.c_str() << ";"
		<< endTime.c_str() << ";" << std::endl;

	//longueur
	ofs << "$SHOAL;LENGTH;ALONG;" << GetExternId() << ";"
		<< GetShoalStat()->GetLength()
		<< ";;;" << std::endl;

	//largeur
	ofs << "$SHOAL;LENGTH;ACROSS;" << GetExternId() << ";"
		<< GetShoalStat()->GetWidth()
		<< ";;;" << std::endl;

	//hauteur
	ofs << "$SHOAL;HEIGHT;MAX;" << GetExternId() << ";"
		<< GetShoalStat()->GetHeigth()
		<< ";;;" << std::endl;

	//premier point du banc
	ofs.precision(10);
	ofs << "$SHOAL;LATLONG;FIRST;" << GetExternId() << ";"
		<< GetShoalStat()->GetFirstPoint().x << ";"
		<< GetShoalStat()->GetFirstPoint().y << ";"
		<< GetShoalStat()->GetFirstPoint().z << ";;" << std::endl;

	//dernier point du banc
	ofs << "$SHOAL;LATLONG;LAST;" << GetExternId() << ";"
		<< GetShoalStat()->GetLastPoint().x << ";"
		<< GetShoalStat()->GetLastPoint().y << ";"
		<< GetShoalStat()->GetLastPoint().z << ";;" << std::endl;

	//centre g�ographique 
	ofs << "$SHOAL;LATLONG;GEOCENTER;" << GetExternId() << ";"
		<< GetShoalStat()->GetGeographicCenter().x << ";"
		<< GetShoalStat()->GetGeographicCenter().y << ";"
		<< GetShoalStat()->GetGeographicCenter().z << ";;" << std::endl;

	//centre de gravit�
	ofs << "$SHOAL;LATLONG;ENERGYCENTER;" << GetExternId() << ";"
		<< GetShoalStat()->GetGravityCenter().x << ";"
		<< GetShoalStat()->GetGravityCenter().y << ";"
		<< GetShoalStat()->GetGravityCenter().z << ";;" << std::endl;

	//energie pond�r�e moyenne
	ofs.precision(8);
	ofs << "$SHOAL;TOTAL;MVBS_WEIGHTED;" << GetExternId() << ";"
		<< GetShoalStat()->GetMeanPondSv() << ";;;;" << std::endl;

	//energie non pond�r�e moyenne et totale
	ofs << "$SHOAL;TOTAL;MVBS;SIGMA_AG;" << GetExternId() << ";"
		<< GetShoalStat()->GetMeanSv() << ";"
		<< GetShoalStat()->GetSigmaAg() << ";;;" << std::endl;

	//volume
	ofs.precision(4);
	ofs << "$SHOAL;VOLUME;MEANVARIANCE;" << GetExternId() << ";"
		<< GetShoalStat()->GetVolume() << ";0.0000" << ";;;" << std::endl;

	//angles lat�raux
	ofs.precision(4);
	ofs << "$SHOAL;ANGLE;MINMAX;" << GetExternId() << ";"
		<< GetShoalStat()->GetAcrossAngleMin() * 180.0 / M_PI << ";"
        << GetShoalStat()->GetAcrossAngleMax() * 180.0 / M_PI << ";"
		<< ";;;" << std::endl;

	//angles du sondeur
	ofs << "$SHOAL;ANGLECONF;MINMAX;" << GetExternId() << ";"
		<< GetShoalStat()->GetMinAngle() * 180.0 / M_PI << ";"
		<< GetShoalStat()->GetMaxAngle() * 180.0 / M_PI << ";"
		<< ";;;" << std::endl;

	//Position g�ographique des �chos
	if (GetShoalStat()->GetParameter()->GetSaveEchos())
	{
		static ostringstream oss;
		oss << "$SHOAL;LATLONG;ECHO;" << GetExternId() << ";";
		static string commonStr = oss.str();

		std::vector<PingData*>::iterator iterPing = GetPings().begin();
		while (iterPing != GetPings().end())
		{
			PingData* pPingData = *iterPing;

			vector<EchoInfo>::const_iterator iterEcho = pPingData->GetShoalStat()->GetEchoList().begin();
			vector<EchoInfo>::const_iterator iterEchoEnd = pPingData->GetShoalStat()->GetEchoList().end();
			while (iterEcho != iterEchoEnd)
			{

				ofs.precision(10);
				ofs << commonStr
					<< iterEcho->position.x << ";"
					<< iterEcho->position.y << ";"
					<< iterEcho->position.z << ";";
				ofs.precision(4);
				ofs << iterEcho->energy << ";";
				ofs << iterEcho->channelNb << ";"
					<< std::endl;

				iterEcho++;
			}

			iterPing++;
		}
	}

	//Position g�ographique des points de contour
	if (GetShoalStat()->GetParameter()->GetSaveContour())
	{
		static ostringstream oss;
		oss << "$SHOAL;CONTOUR;LATLONGDEPTH;" << GetExternId() << ";";
		static string commonStr = oss.str();

		// pour chaque facette du banc, r�cup�ration et affichage du trianglestrip correspondant
		size_t nbStrips = m_GPSContourStrips.size();
		for (size_t i = 0; i < nbStrips; i++)
		{
			const vector<double>& strip = m_GPSContourStrips[i];
			size_t nbPoints = strip.size() / 3; // 3 doubles par point (X,Y,Z)

			for (size_t j = 0; j < nbPoints; j++)
			{
				ofs.precision(10);

				ofs << commonStr
					<< strip[j * 3] << ";"
					<< strip[j * 3 + 1] << ";"
					<< strip[j * 3 + 2] << ";"
					<< i << ";"
					<< std::endl;
			}
		}
	}

	ofs.flush();
}

//*****************************************************************************
// Name : Contains
// Description : check the shoal contains the given polar point
// Parameters :
// Return : void
//*****************************************************************************
bool ShoalData::Contains(int ping, int beam, int echoIdx)
{
	bool result = false;

	//for each shoal, until one is matching
	int nbPings = GetPings().size();
	int firstPingId = GetPings()[0]->GetPingId();
	int lastPingId = GetPings()[nbPings - 1]->GetPingId();

	//verify the ping range is correct
	if (firstPingId <= ping && lastPingId >= ping)
	{
		//get the ping data
		PingData* pRefPingData = NULL;
		for (int iPing = 0; iPing < nbPings && pRefPingData == NULL; iPing++)
		{
			PingData* pPingData = GetPings()[iPing];
			if (pPingData->GetPingId() == ping)
			{
				pRefPingData = pPingData;
			}
		}
		if (pRefPingData != NULL)
		{
			if (pRefPingData->GetTransducers().size() > 0)
			{
				//only one transducer for one shoal
				TransductData* pTransData = pRefPingData->GetTransducers()[0];

				int nbChannels = pTransData->GetChannels().size();
				int firstChannelId = pTransData->GetChannels()[0]->GetChannelId();
				int lastChannelId = pTransData->GetChannels()[nbChannels - 1]->GetChannelId();

				//verify the beam range is correct
				if (firstChannelId <= beam && lastChannelId >= beam)
				{
					//get the beam data
					ChannelData* pRefChannelData = NULL;
					for (int iChannel = 0; iChannel < nbChannels && pRefChannelData == NULL; iChannel++)
					{
						ChannelData* pChannelData = pTransData->GetChannels()[iChannel];
						if (pChannelData->GetChannelId() == beam)
						{
							pRefChannelData = pChannelData;
						}
					}
					if (pRefChannelData != NULL)
					{
						//verify the echo range is correct	
						int nbEchoGroup = pRefChannelData->GetGroupsFinal().size();
						for (int iEchoGroup = 0; iEchoGroup < nbEchoGroup && !result; iEchoGroup++)
						{
							EchoGroup* pEchoGroup = pRefChannelData->GetGroupsFinal()[iEchoGroup];
							if (pEchoGroup->GetEchoMin() <= echoIdx &&
								pEchoGroup->GetEchomax() >= echoIdx)
							{
								result = true;
							}
						}//end for
					}// end pRefChannelData != NULL
				}//end if GetChannels().size() >= beam
			}//end GetTransducers().size() > 0
		}//end if pRefPingData != NULL
	}//end ping range is correct

	return result;
}
