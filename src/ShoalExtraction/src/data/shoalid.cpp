// -*- C++ -*-
// ****************************************************************************
// Class: ShoalId
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "ShoalExtraction/data/shoalid.h"
#include <assert.h>

using namespace shoalextraction;
using namespace std;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
ShoalId::ShoalId()
{
	m_shoalId = 0;
	m_firstESU = 0;
	m_firstPing = 0;
	m_fromPreviousPing = false;
	m_closed = false;
	m_closing = false;
}
//destructeur
ShoalId::~ShoalId()
{
}

// *********************************************************************
// M�thodes
// *********************************************************************

 /**
 * Alloc
 * Methode : allocation des points
 * @param void
 * @return void
 */
void ShoalId::InvalidShoaldId()
{
	m_shoalId = 0;
}

/**
* GetLinkedShoalId
* Methode : d�r�f�rencement de l'Id
* @param void
* @return void
*/
void ShoalId::SetLinked2DShoalId(std::shared_ptr<ShoalId> id)
{
	//InvalidShoaldId();
	m_linked2DShoalId = id;
}

std::shared_ptr<ShoalId> ShoalId::GetLinked2DShoalId()
{	
	std::shared_ptr<ShoalId> linked2DShoalId = m_linked2DShoalId.lock();
	std::shared_ptr<ShoalId> this_shared_ptr = shared_from_this();
	return (linked2DShoalId && linked2DShoalId != this_shared_ptr) ? linked2DShoalId->GetLinked2DShoalId() : this_shared_ptr;
}


/**
* Reset
* Methode : init
* @param void
* @return void
*/
void ShoalId::Reset()
{
	m_shoalId = 0;
	m_firstESU = 0;
	m_firstPing = 0;
	m_fromPreviousPing = false;
	m_closed = false;
	m_closing = false;
}


/**
* GetLinkedShoalId
* Methode : d�r�f�rencement de l'Id
* @param void
* @return void
*/
void ShoalId::SetLinked3DShoalId(std::shared_ptr<ShoalId> id)
{
	//InvalidShoaldId();
	m_linked3DShoalId = id;
}

std::shared_ptr<ShoalId> ShoalId::GetLinked3DShoalId()
{
	std::shared_ptr<ShoalId> linked3DShoalId = m_linked3DShoalId.lock();
	std::shared_ptr<ShoalId> this_shared_ptr = shared_from_this();
	return (linked3DShoalId && linked3DShoalId != this_shared_ptr) ? linked3DShoalId->GetLinked3DShoalId() : this_shared_ptr;
}

/**
* GetAllLinkedComponentIds
* Methode : renvoi de l'ensemble des ID des echogroups li�s
* @param void
* @return set<std::uint64_t>
*/
set<std::uint64_t> ShoalId::GetAllLinkedComponentIds()
{
	set<std::uint64_t> result;
	
	std::shared_ptr<ShoalId> pShoalId = shared_from_this();
	do
	{
		result.insert(pShoalId->GetShoalId());
		result.insert(pShoalId->GetMergedShoalIds().begin(), pShoalId->GetMergedShoalIds().end());
		pShoalId = pShoalId->m_linked2DShoalId.lock();
	} while (pShoalId);

	return result;
}
