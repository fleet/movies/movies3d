// -*- C++ -*-
// ****************************************************************************
// Class: ShoalIdMgr
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#include "ShoalExtraction/data/shoalidmgr.h"
#include <assert.h>

#include <memory>

using namespace shoalextraction;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************
// Constructeur par d�faut
ShoalIdMgr::ShoalIdMgr()
{
	m_currentId = 0;
	m_currentESUId = 0;
}

ShoalIdMgr::~ShoalIdMgr()
{
	Reset();
}


// *********************************************************************
// M�thodes
// *********************************************************************

/**
* IncrShoalId
* Methode : incrementation de l'id
* @param void
* @return void
*/
std::shared_ptr<ShoalId> ShoalIdMgr::IncrShoalId()
{
	m_currentId++;

	std::shared_ptr<ShoalId> result = std::make_shared<ShoalId>();
	result->SetShoalId(m_currentId);

	GetIdListByESU()[m_currentESUId].push_back(result);

	return result;
}

/**
* InitESU
* Methode : initialisation d'un nouvel ESU
* @param void
* @return void
*/
void ShoalIdMgr::InitESU(std::uint64_t esuId)
{
	m_currentESUId = esuId;
}


/**
* Reset
* Methode : reset du composant
* @param void
* @return void
*/
void ShoalIdMgr::Reset()
{
	std::map<std::uint64_t, std::vector<std::shared_ptr<ShoalId> > >::iterator iterESU = GetIdListByESU().begin();

	while (iterESU != GetIdListByESU().end())
	{
		ResetIdList(&iterESU->second);
		iterESU++;
	}
	GetIdListByESU().clear();
	m_currentId = 0;
	m_currentESUId = 0;
}

/**
* ResetESU
* Methode : reset des id pour les esu jusqu'� l'esu indiqu�
* @param std::uint64_t esuId
* @return void
*/
void ShoalIdMgr::ResetESU(std::int64_t esuId)
{
	std::map<std::uint64_t, std::vector< std::shared_ptr<ShoalId> >>::iterator iterESU = GetIdListByESU().begin();
	std::map<std::uint64_t, std::vector< std::shared_ptr<ShoalId> >>::iterator iterESUPrevious = iterESU;

	bool esuIdOk = true;

	while (iterESU != m_idListByESU.end() && esuIdOk)
	{
		//test esu validity for cleaning
		esuIdOk = iterESU->first <= esuId;

		iterESUPrevious = iterESU;
		iterESU++;

		if (esuIdOk)
		{
			//reset interne des Id de l'ESU
			ResetIdList(&iterESUPrevious->second);
			
			//suppression de la liste globale des ESU
			GetIdListByESU().erase(iterESUPrevious, iterESU);
		}
	}
}

/**
* ResetIdList
* Methode : reset des id du vector
* @param std::vector<ShoalId*>& idList
* @return void
*/
void ShoalIdMgr::ResetIdList(std::vector< std::shared_ptr<ShoalId> >* idList)
{
	idList->clear();
}
