// -*- C++ -*-
// ****************************************************************************
// Class: ShoalStat
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include <float.h>
#include "ShoalExtraction/data/shoalstat.h"

#include "BaseMathLib/geometry/GeometryTools2D.h"

using namespace shoalextraction;
using namespace std;
using namespace BaseMathLib;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
ShoalStat::ShoalStat()
{
	//profondeur min
    m_minDepth = std::numeric_limits<double>::lowest();
	//profondeur max
	m_maxDepth = 0;

	//distance min au fond 
    m_minBottomDistance = std::numeric_limits<double>::max();
	//distance max au fond 
	m_maxBottomDistance = 0;

	//date de d�but du banc
    m_beginTime.m_TimeCpu = std::numeric_limits<std::uint32_t>::max();
	//date de fin du banc
	m_endTime.SetToNull();

	//angle lat�ral min du banc
    m_acrossAngleMin = std::numeric_limits<double>::max();
	//angle lat�ral max du banc
    m_acrossAngleMax = -std::numeric_limits<double>::max();

	//nombre d'echos
	m_nbEchos = 0;

	//Sv moyen 
	m_meanSv = 0;
	//Sigma_ag
	m_SigmaAg = 0;
	//Sv moyen pond�r�
	m_meanPondSv = 0;

	//distances d'agr�gation minimale
	m_verticalIntegrationDistance = 0;

	//angle minimal du transducteur
	m_minAngle = std::numeric_limits<double>::max();
	//angle maximal du transducteur
	m_maxAngle = -std::numeric_limits<double>::max();

	//param�trage
	m_pParameter = NULL;

	//route du navire
	m_shipRoad = 0;
	//longueur du banc
	m_length = 0;
	//largeur du banc
	m_width = 0;
	//hauteur du banc
	m_heigth = 0;
}

ShoalStat::~ShoalStat()
{
}


// *********************************************************************
// M�thodes
// *********************************************************************


//*****************************************************************************
// Name : ComputeShoalDimensions
// Description : Calcul des dimensions du banc
// Parameters : * (in) double headingShoalAngle
//              * (in) double pitchShoalAngle
//              * (in) double rollShoalAngle
// Return : void
//*****************************************************************************
void ShoalStat::ComputeShoalDimensions(double headingShoalAngle,
	double pitchShoalAngle,
	double rollShoalAngle)
{
	//calculer les valeurs des angles dans l'intervalle -PI/2;PI/2
	headingShoalAngle = GeometryTools2D::SetInRange(-M_PI_2, M_PI_2, headingShoalAngle);
	pitchShoalAngle = GeometryTools2D::SetInRange(-M_PI_2, M_PI_2, pitchShoalAngle);
	rollShoalAngle = GeometryTools2D::SetInRange(-M_PI_2, M_PI_2, rollShoalAngle);

	//on considere que les axes sont align�s correctement si l'angle qu'ils forment avec le repere
	//bateau est entre -PI/4 et PI/4
	bool headingSwitched = -M_PI_4 > headingShoalAngle || headingShoalAngle > M_PI_4;
	bool pitchSwitched = -M_PI_4 > pitchShoalAngle || pitchShoalAngle > M_PI_4;
	bool rollSwitched = -M_PI_4 > rollShoalAngle || rollShoalAngle > M_PI_4;

	double deltaX = GetOBBox().GetDeltaX();
	double deltaY = GetOBBox().GetDeltaY();
	double deltaZ = GetOBBox().GetDeltaZ();

	//Si  Abs(Cap Banc � Cap navire) < PI/4
	if (!headingSwitched)
	{
		//Si  Tangage Banc < PI/4
		if (!pitchSwitched)
		{
			SetLength(deltaX);

			//Si  Roulis Banc < PI/4
			if (!rollSwitched)
			{
				SetWidth(deltaY);
				SetHeigth(deltaZ);
			}
			else
			{
				SetWidth(deltaZ);
				SetHeigth(deltaY);
			}
		}
		else
		{
			SetLength(deltaZ);

			//Si  Roulis Banc < PI/4
			if (!rollSwitched)
			{
				SetWidth(deltaY);
				SetHeigth(deltaX);
			}
			else
			{
				SetWidth(deltaX);
				SetHeigth(deltaY);
			}
		}
	}
	else
	{
		//Si  Tangage Banc < PI/4
		if (!pitchSwitched)
		{
			SetLength(deltaY);

			//Si  Roulis Banc < PI/4
			if (!rollSwitched)
			{
				SetWidth(deltaX);
				SetHeigth(deltaZ);
			}
			else
			{
				SetWidth(deltaZ);
				SetHeigth(deltaX);
			}
		}
		else
		{
			SetLength(deltaZ);

			//Si  Roulis Banc < PI/4
			if (!rollSwitched)
			{
				SetWidth(deltaX);
				SetHeigth(deltaY);
			}
			else
			{
				SetWidth(deltaY);
				SetHeigth(deltaX);
			}
		}
	}
}
