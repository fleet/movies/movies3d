// -*- C++ -*-
// ****************************************************************************
// Class: TransductData
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Société : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "ShoalExtraction/data/transductdata.h"
#include "ShoalExtraction/data/channeldata.h"
#include "ShoalExtraction/geometry/Plane3D.h"

using namespace shoalextraction;
using namespace std;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par défaut
TransductData::TransductData():
	m_transId(-1),
	m_vol3DData(std::make_unique<BoundingBox3D>()),
	m_plane3D(std::make_unique<Plane3D>()),
	m_origin3D(std::make_unique<BaseMathLib::Vector3D>()),
	m_origin3DGPS(std::make_unique<BaseMathLib::Vector3D>()),
	m_beamsSamplesSpacing(0),
	m_sampleOffset(0),
	m_verticalIntegrationDistance(0),
	m_integrationDistanceX(0),
	m_integrationDistanceY(0),
	m_minAngle(std::numeric_limits<double>::max()),
	m_maxAngle(-std::numeric_limits<double>::max()),
	m_pitch(0)
{
}

TransductData::~TransductData()
{
	for (const auto channel : m_channels)
	{
		delete channel;
	}
	m_channels.clear();
}


// *********************************************************************
// Méthodes
// *********************************************************************

//*****************************************************************************
// Name : GetChannel
// Description : recherche de la donnée de faisceau correspondant à l'id recherché
//					création éventuelle si inexistant
// Parameters : * (in) channelId : id du faisceau recherché
// Return : ChannelData* : pointeur vers la donnée de faisceau trouvée
//*****************************************************************************
ChannelData* TransductData::GetChannel(const int channelId)
{
	ChannelData* result;
	auto currentChannelId = -1;

	auto iter = m_channels.cbegin();
	while (iter != m_channels.cend() && currentChannelId < channelId)
	{
		currentChannelId = (*iter)->GetChannelId();
		if (currentChannelId < channelId)
		{
			++iter;
		}
	}

	if (currentChannelId == channelId)
	{
		result = *iter;
	}
	else
	{
		result = new ChannelData();
		result->SetChannelId(channelId);
		m_channels.insert(iter, result);
	}

	return result;
}


//*****************************************************************************
// Name : UpdateAllShoalId2D
// Description : MaJ de l'ID de banc en essayant de trouver une agrégation 2D
// Parameters : void
// Return : void
//*****************************************************************************
void TransductData::UpdateAllShoalId2D()
{
	//parcours de tous les faisceaux
	for (size_t iChannel = 0; iChannel < GetChannels().size(); iChannel++)
	{
		ChannelData* pChannelData = GetChannels()[iChannel];

		//parcours de tous les groupes d'échos
		for (const auto pEchoGroup : pChannelData->GetGroupsFinal())
		{
			if (pEchoGroup != nullptr)
			{
				std::shared_ptr<ShoalId> pShoalId1 = pEchoGroup->GetShoalId()->GetLinked2DShoalId();

				//parcours de tous les faisceaux suivants
				for (size_t iChannel2 = iChannel; iChannel2 < GetChannels().size(); iChannel2++)
				{
					ChannelData* pChannelData2 = GetChannels()[iChannel2];

					//parcours de tous les groupes d'échos
					for (const auto pEchoGroup2 : pChannelData2->GetGroupsFinal())
					{
						if (pEchoGroup2 != nullptr)
						{
							std::shared_ptr<ShoalId> pShoalId2 = pEchoGroup2->GetShoalId()->GetLinked2DShoalId();

							//on vérifie que les numéro de bancs sont différents
							if (pShoalId2 && pShoalId1 != pShoalId2)
							{
								//si les enveloppes englobantes s'intersectent, propagation du numéro de banc
								if (pEchoGroup->GetMarginsSurf2d().Intersect(pEchoGroup2->GetSurf2D()))
								{
									//MaJ du banc pointé en 2D
									pShoalId2->SetLinked2DShoalId(pShoalId1);
								}
							}
						}//	if(pEchoGroup2 != NULL)
					}
				}
			}//if(pEchoGroup != NULL)
		}
	}
}

void TransductData::UpdateVol3DData(const BoundingBox3D& boundingBox3D)
{
	m_vol3DData->Update(boundingBox3D);
}

void TransductData::AddChannel(ChannelData* channelData)
{
	m_channels.push_back(channelData);
}

void TransductData::SetOrigin3D(const BaseMathLib::Vector3D& origin)
{
	m_origin3D = std::make_unique<BaseMathLib::Vector3D>(origin);
}

void TransductData::SetOrigin3DGPS(const BaseMathLib::Vector3D& origin)
{
	m_origin3DGPS = std::make_unique<BaseMathLib::Vector3D>(origin);
}

void TransductData::SetPlane3D(const Plane3D& plane)
{
	m_plane3D = std::make_unique<Plane3D>(plane);
}
