#include "ShoalExtraction/geometry/BoundingBox3D.h"
// ***************************************************************************
// Dependences
// ***************************************************************************

#include <math.h>
#include <float.h>

using namespace shoalextraction;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
BoundingBox3D::BoundingBox3D(void)
{
	m_dXMin = m_dYMin = m_dZMin = std::numeric_limits<double>::max();
	m_dXMax = m_dYMax = m_dZMax = -std::numeric_limits<double>::max();
}

// Destructeur
BoundingBox3D::~BoundingBox3D(void)
{}

// *********************************************************************
// M�thodes
// *********************************************************************

  /**
 * Reset
 * Methode : Initialise les donn�es membres
 * @param void
 * @return void
 */
void BoundingBox3D::Reset()
{
	m_dXMin = m_dYMin = m_dZMin = std::numeric_limits<double>::max();
	m_dXMax = m_dYMax = m_dZMax = -std::numeric_limits<double>::max();
}

/**
* Update
* Methode : Initialise les donn�es membres
* @param void
* @return void
*/
void BoundingBox3D::Update(const std::vector<BaseMathLib::Vector3D>& points)
{
	auto iter = points.begin();
	const auto iter_end = points.end();

	if (iter != iter_end)
	{
		//init avec le premier point pour optimiser les tests
		Init(iter->x, iter->y, iter->z);
		++iter;
	}

	while (iter != iter_end)
	{
		//MaJ optimis�
		UpdateOpti(*iter);
		++iter;
	}
}

/**
* Update
* Methode : MaJ les donn�es membres
* @param const Wm4::Vector3d* point
* @return void
*/
void BoundingBox3D::Update(const BaseMathLib::Vector3D& point)
{
	Update(point.x, point.y, point.z);
}

/**
* UpdateOpti
* Methode : MaJ donn�es membres (optimis� pour r�duire les tests)
*           obligation d'appeler Init avant le premier appel � UpdateOpti
* @param const Wm4::Vector3d* point
* @return void
*/
void BoundingBox3D::UpdateOpti(const BaseMathLib::Vector3D& point)
{
	UpdateOpti(point.x, point.y, point.z);
}

/**
* Init
* Methode : Initialise les donn�es membres
* @param const Wm4::Vector3d* point
* @return void
*/
void BoundingBox3D::Init(const BaseMathLib::Vector3D& point)
{
	Init(point.x, point.y, point.z);
}

/**
* Init
* Methode : Initialise les donn�es membres
* @param double x
* @param double y
* @param double z
* @return void
*/
void BoundingBox3D::Init(double x, double y, double z)
{
	m_dXMin = m_dXMax = x;
	m_dYMin = m_dYMax = y;
	m_dZMin = m_dZMax = z;
}

/**
* IsValid
* Methode :
* @return bool
*/
bool BoundingBox3D::IsValid() const
{
	return m_dXMax >= m_dXMin && m_dYMax >= m_dYMin && m_dZMax >= m_dZMin;
}

/**
* Update
* Methode : MaJ les donn�es membres
* @param void
* @return void
*/
void BoundingBox3D::Update(double x, double y, double z)
{
	if (m_dXMax < x)
	{
		m_dXMax = x;
	}
	if (m_dXMin > x)
	{
		m_dXMin = x;
	}
	if (m_dYMax < y)
	{
		m_dYMax = y;
	}
	if (m_dYMin > y)
	{
		m_dYMin = y;
	}
	if (m_dZMax < z)
	{
		m_dZMax = z;
	}
	if (m_dZMin > z)
	{
		m_dZMin = z;
	}
}


/**
 * UpdateOpti
 * Methode : MaJ donn�es membres (optimis� pour r�duire les tests)
 *           obligation d'appeler Init avant le premier appel � UpdateOpti
 * @param double x
 * @param double y
 * @param double z
 * @return void
 */
void BoundingBox3D::UpdateOpti(double x, double y, double z)
{
	if (m_dXMax < x)
	{
		m_dXMax = x;
	}
	else if (m_dXMin > x)
	{
		m_dXMin = x;
	}
	if (m_dYMax < y)
	{
		m_dYMax = y;
	}
	else if (m_dYMin > y)
	{
		m_dYMin = y;
	}
	if (m_dZMax < z)
	{
		m_dZMax = z;
	}
	else if (m_dZMin > z)
	{
		m_dZMin = z;
	}
}

/**
* Update
* Methode : MaJ les donn�es membres
* @param void
* @return void
*/
void BoundingBox3D::Update(const BoundingBox3D& other)
{
	if (m_dXMin > other.m_dXMin)
	{
		m_dXMin = other.m_dXMin;
	}
	if (m_dXMax < other.m_dXMax)
	{
		m_dXMax = other.m_dXMax;
	}
	if (m_dYMin > other.m_dYMin)
	{
		m_dYMin = other.m_dYMin;
	}
	if (m_dYMax < other.m_dYMax)
	{
		m_dYMax = other.m_dYMax;
	}
	if (m_dZMin > other.m_dZMin)
	{
		m_dZMin = other.m_dZMin;
	}
	if (m_dZMax < other.m_dZMax)
	{
		m_dZMax = other.m_dZMax;
	}
}

/**
* Intersect
* Methode : test d'intersection
* @param const BoundingBox3D& other
* @return bool
*/
bool BoundingBox3D::Intersect(const BoundingBox3D& other) const
{
	return CloseTo(other, 0, 0, 0);
}

//test de distance
  /**
 * CloseTo
 * Methode : test de distance
 * @param const BoundingBox3D& other
 * @param double thresholdX
 * @param double thresholdY
 * @param double thresholdZ
 * @return bool
 */
bool BoundingBox3D::CloseTo(const BoundingBox3D& other, double thresholdX, double thresholdY, double thresholdZ) const
{
	return	other.m_dXMin - m_dXMax <= thresholdX && m_dXMin - other.m_dXMax <= thresholdX &&
		other.m_dYMin - m_dYMax <= thresholdY && m_dYMin - other.m_dYMax <= thresholdY &&
		other.m_dZMin - m_dZMax <= thresholdZ && m_dZMin - other.m_dZMax <= thresholdZ;
}


/**
* Serialize
* Methode : serialisation
* @param std::ofstream& ofs
* @return void
*/
void BoundingBox3D::Serialize(std::ofstream& ofs) const
{
	ofs << "X\t";
	ofs << m_dXMin << "\t<-->\t"
		<< m_dXMax << "\t";

	ofs << "Y\t";
	ofs << m_dYMin << "\t<-->\t"
		<< m_dYMax << "\t";

	ofs << "Z\t";
	ofs << m_dZMin << "\t<-->\t"
		<< m_dZMax;

	ofs << std::endl;
}

/**
* SerializeSize
* Methode : serialisation de la taille
* @param std::ofstream& ofs
* @return void
*/
void BoundingBox3D::SerializeSize(std::ofstream& ofs) const
{
	ofs << "DeltaX\t";
	ofs << m_dXMax - m_dXMin << "\t";

	ofs << "DeltaY\t";
	ofs << m_dYMax - m_dYMin << "\t";

	ofs << "DeltaZ\t";
	ofs << m_dZMax - m_dZMin;

	ofs << std::endl;
}
