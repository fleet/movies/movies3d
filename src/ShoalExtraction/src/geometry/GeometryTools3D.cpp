// -*- C++ -*-
// ****************************************************************************
// Class: GeometryTools3D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************


#include "math.h"
#include "float.h"

#include "ShoalExtraction/geometry/Triangle3D.h"
#include "ShoalExtraction/geometry/geometrytools3d.h"

using namespace shoalextraction;

namespace {
	constexpr double zeroTolerance = 1e-8;
}

#ifdef _TRACE_PERF
//#define _G3DTRACE_PERF

#ifdef _G3DTRACE_PERF
extern std::ofstream trace_ofs;
#endif
#endif

/**
  * CloseTo
  *
  * Returns true if any of the points in points1 is closer than threshold from any points of points2:
  *
  * @param std::vector<Wm4::Vector3d*>& points1
  * @param std::vector<Wm4::Vector3d*>& points2
  * @param threshold_x double
  * @param threshold_y double
  * @param threshold_z double
  * @return bool
  */
bool GeometryTools3D::CloseTo(const std::vector<BaseMathLib::Vector3D> & points1,
	const std::vector<BaseMathLib::Vector3D> & points2,
	double threshold_x,
	double threshold_y,
	double threshold_z)
{
	bool result = false;

	if (threshold_x > 0 && threshold_y > 0 && threshold_z > 0)
	{
#ifdef _G3DTRACE_PERF
		int cpt = 0;
#endif
		auto iter1 = points1.begin();
        const auto end1 = points1.end();
        const auto end2 = points2.rend();

		double x, y, z;

		while (iter1 != end1 && !result)
		{
			x = iter1->x;
			y = iter1->y;
			z = iter1->z;

			auto iter2 = points2.rbegin();
			while (iter2 != end2 && !result)
			{
				result = fabs(x - iter2->x) <= threshold_x &&
					fabs(y - iter2->y) <= threshold_y &&
					fabs(z - iter2->z) <= threshold_z;
				++iter2;

#ifdef _G3DTRACE_PERF
				cpt++;
#endif
			}
			++iter1;
		}
#ifdef _G3DTRACE_PERF
		//trace_ofs << "GeometryTools3D::CloseTo_ Vector3d _: " << cpt << " tests" << std::endl;
#endif

	}

	return result;
}

/**
 * CloseTo
 *
 * Returns true if any of the triangles in triangles1 is closer than threshold from any triangles of triangles2:
 *
 * @param std::vector<Triangle3D*>& triangles1
 * @param std::vector<Triangle3D*>& triangles2
 * @param threshold_x double
 * @param threshold_y double
 * @param threshold_z double
 * @return bool
 */
bool GeometryTools3D::CloseTo(const std::vector<Triangle3D>& triangles1,
	const std::vector<Triangle3D>& triangles2,
	double threshold_x,
	double threshold_y,
	double threshold_z)
{
	bool result = false;

#ifdef _G3DTRACE_PERF
	int cpt = 0;
	int cpt1 = 0;
	int cpt2 = 0;
	double minDistX = std::numeric_limits<double>::max();
	double minDistY = std::numeric_limits<double>::max();
	double minDistZ = std::numeric_limits<double>::max();
#endif

	auto iter1 = triangles1.begin();
    const auto end1 = triangles1.end();
    const auto end2 = triangles2.rend();

	while (iter1 != end1 && !result)
	{
#ifdef _G3DTRACE_PERF
		cpt2 = 0;
#endif
		auto iter2 = triangles2.rbegin();
		while (iter2 != end2 && !result)
		{
			result = iter1->CloseTo(*iter2, threshold_x, threshold_y, threshold_z);
			++iter2;

#ifdef _G3DTRACE_PERF
			cpt++;
			cpt2++;
#endif

		}
		++iter1++;

#ifdef _G3DTRACE_PERF
		cpt1++;
#endif
	}

#ifdef _G3DTRACE_PERF
	trace_ofs << "GeometryTools3D::CloseTo_ Triangle3d _\t" << result << "\t" << cpt << "\t" << cpt1 << "\t" << cpt2 << "\t" << "\t" << threshold_x << "\t" << threshold_y << "\t" << threshold_z << std::endl;
#endif

	return result;
}

/**
  * CloseTo
  *
  * Returns true the point is closer than threshold from the plane:
  *
  * @param Wm4::Vector3d* point
  * @param Wm4::Plane3d* plane
  * @param threshold_x double
  * @param threshold_y double
  * @param threshold_z double
  * @return bool
  */
bool GeometryTools3D::CloseTo(const BaseMathLib::Vector3D& point,
	const Plane3D& plane)
{
	//la distance r�cup�r�e est sign�e (>0 pour le cot� positif du plan : dans la direction de la normale) 
	return (plane.distanceTo(point) <= 0);
}

/**
  * CloseTo
  *
  * Returns true if any of the points is closer than threshold from the poly enclosed by the planes:
  *
  * @param std::vector<Wm4::Vector3d*>* points
  * @param std::vector<Wm4::Plane3d*>* planes
  * @param threshold_x double
  * @param threshold_y double
  * @param threshold_z double
  * @return bool
  */
bool GeometryTools3D::CloseTo(const std::vector<BaseMathLib::Vector3D>& points,
	const std::vector<Plane3D>& planes)
{
	bool result = false;

	auto iter1 = points.begin();
    const auto end1 = points.end();
    const auto end2 = planes.rend();

	while (iter1 != end1 && !result)
	{
		auto iter2 = planes.rbegin();
		bool result_point = true;
		while (iter2 != end2 && result_point)
		{
			result_point &= CloseTo(*iter1, *iter2);
			++iter2;
		}
		//si un point est valide, le test global s'arrete
		result = result_point;
		++iter1;
	}
	return result;
}

/**
  * Offset
  *
  * Offset the plane by the X, Y and Z values
  *
  * @param threshold_x double
  * @param threshold_y double
  * @param threshold_z double
  * @param Wm4::Plane3d* plane
  * @return bool
  */
void GeometryTools3D::Offset(double threshold_x,
	double threshold_y,
	double threshold_z,
	Plane3D& plane)
{
	//pour effectuer un offset sur le plan, on modifie la constante du plan
	//la constante est la distance du plan au point (0, 0, 0)

	double constantSign = (plane.constant > 0) ? 1.0 : -1.0;

	//on calcule les composantes de la distance sur X, Y et Z
	double distX = plane.constant * plane.normal.x;
	double distY = plane.constant * plane.normal.y;
	double distZ = plane.constant * plane.normal.z;

	//on ajoute les seuils en fonction de l'orientation du plan
	distX += threshold_x * plane.normal.x;
	distY += threshold_y * plane.normal.y;
	distZ += threshold_z * plane.normal.z;

	//on calcule la nouvelle distance
	double distance = sqrt(distX * distX + distY * distY + distZ * distZ);

	// affecte la constante
	plane.constant = constantSign * distance;
}

bool GeometryTools3D::intersection(const Line3D& line, const Plane3D& plane, double& lineT)
{
	const double fDdN = line.direction.dotProduct(plane.normal);
	const double fSDistance = plane.distanceTo(line.origin);
	if (fabs(fDdN) > zeroTolerance)
	{
		// The line is not parallel to the plane, so they must intersect.
		lineT = -fSDistance / fDdN;
		return true;
	}

	// The Line and plane are parallel.  Determine if they are numerically
	// close enough to be coincident.
	if (fabs(fSDistance) <= zeroTolerance)
	{
		// The line is coincident with the plane, so choose t = 0 for the
		// parameter.
		lineT = 0.0;
		return true;
	}

	return false;
}

bool GeometryTools3D::intersection(const Segment3D& segment, const Plane3D& plane, double& segmentT)
{
	Line3D line(segment.origin, segment.direction);
	double lineT;
	auto found = intersection(line, plane, lineT);
	if (found)
	{
		// The line intersects the plane, but possibly at a point that is
		// not on the segment.
		segmentT = lineT;
		return fabs(segmentT) <= segment.extent;
	}

	return false;
}


void GeometryTools3D::GenerateComplementBasis(BaseMathLib::Vector3D& u, BaseMathLib::Vector3D& v, BaseMathLib::Vector3D& w)
{
    if (fabs(w.x) >= fabs(w.y))
    {
        // W.x or W.z is the largest magnitude component, swap them
        const double invLength = 1.0 / sqrt(w.x * w.x + w.z * w.z);

        u.x = -w.z * invLength;
        u.y = 0.0;
        u.z = +w.x * invLength;

        v.x = w.y * u.z;
        v.y = w.z * u.x - w.x * u.z;
        v.z = -w.y * u.x;
    }
    else
    {
        // W.y or W.z is the largest magnitude component, swap them
        const double invLength = 1.0 / sqrt(w.y * w.y + w.z * w.z);

        u.x = 0.0;
        u.y = +w.z * invLength;
        u.z = -w.y * invLength;

        v.x = w.y * u.z - w.z * u.y;
        v.y = -w.x * u.z;
        v.z = w.x * u.y;
    }
}

double GeometryTools3D::squareDistance(const BaseMathLib::Vector3D& pt, const Triangle3D& tri, BaseMathLib::Vector3D& p0, BaseMathLib::Vector3D& p1)
{
    const BaseMathLib::Vector3D kDiff = tri.edges[0] - pt;
    const BaseMathLib::Vector3D kEdge0 = tri.edges[1] - tri.edges[0];
    const BaseMathLib::Vector3D kEdge1 = tri.edges[2] - tri.edges[0];
    const double fA00 = kEdge0.squaredLength();
    const double fA01 = kEdge0.dotProduct(kEdge1);
    const double fA11 = kEdge1.squaredLength();
    const double fB0 = kDiff.dotProduct(kEdge0);
    const double fB1 = kDiff.dotProduct(kEdge1);
    const double fC = kDiff.squaredLength();
    const double fDet = fabs(fA00 * fA11 - fA01 * fA01);
    double fS = fA01 * fB1 - fA11 * fB0;
    double fT = fA01 * fB0 - fA00 * fB1;
    double fSqrDistance;

    if (fS + fT <= fDet)
    {
        if (fS < 0.0)
        {
            if (fT < 0.0)  // region 4
            {
                if (fB0 < 0.0)
                {
                    fT = 0.0;
                    if (-fB0 >= fA00)
                    {
                        fS = 1.0;
                        fSqrDistance = fA00 + 2.0 * fB0 + fC;
                    }
                    else
                    {
                        fS = -fB0 / fA00;
                        fSqrDistance = fB0 * fS + fC;
                    }
                }
                else
                {
                    fS = 0.0;
                    if (fB1 >= 0.0)
                    {
                        fT = 0.0;
                        fSqrDistance = fC;
                    }
                    else if (-fB1 >= fA11)
                    {
                        fT = 1.0;
                        fSqrDistance = fA11 + 2.0 * fB1 + fC;
                    }
                    else
                    {
                        fT = -fB1 / fA11;
                        fSqrDistance = fB1 * fT + fC;
                    }
                }
            }
            else  // region 3
            {
                fS = 0.0;
                if (fB1 >= 0.0)
                {
                    fT = 0.0;
                    fSqrDistance = fC;
                }
                else if (-fB1 >= fA11)
                {
                    fT = 1.0;
                    fSqrDistance = fA11 + 2.0 * fB1 + fC;
                }
                else
                {
                    fT = -fB1 / fA11;
                    fSqrDistance = fB1 * fT + fC;
                }
            }
        }
        else if (fT < 0.0)  // region 5
        {
            fT = 0.0;
            if (fB0 >= 0.0)
            {
                fS = 0.0;
                fSqrDistance = fC;
            }
            else if (-fB0 >= fA00)
            {
                fS = 1.0;
                fSqrDistance = fA00 + 2.0 * fB0 + fC;
            }
            else
            {
                fS = -fB0 / fA00;
                fSqrDistance = fB0 * fS + fC;
            }
        }
        else  // region 0
        {
            // minimum at interior point
            double fInvDet = 1.0 / fDet;
            fS *= fInvDet;
            fT *= fInvDet;
            fSqrDistance = fS * (fA00 * fS + fA01 * fT + 2.0 * fB0) +
                fT * (fA01 * fS + fA11 * fT + 2.0 * fB1) + fC;
        }
    }
    else
    {
        double fTmp0, fTmp1, fNumer, fDenom;

        if (fS < 0.0)  // region 2
        {
            fTmp0 = fA01 + fB0;
            fTmp1 = fA11 + fB1;
            if (fTmp1 > fTmp0)
            {
                fNumer = fTmp1 - fTmp0;
                fDenom = fA00 - 2.0f * fA01 + fA11;
                if (fNumer >= fDenom)
                {
                    fS = 1.0;
                    fT = 0.0;
                    fSqrDistance = fA00 + 2.0 * fB0 + fC;
                }
                else
                {
                    fS = fNumer / fDenom;
                    fT = 1.0 - fS;
                    fSqrDistance = fS * (fA00 * fS + fA01 * fT + 2.0f * fB0) +
                        fT * (fA01 * fS + fA11 * fT + 2.0 * fB1) + fC;
                }
            }
            else
            {
                fS = 0.0;
                if (fTmp1 <= 0.0)
                {
                    fT = 1.0;
                    fSqrDistance = fA11 + 2.0 * fB1 + fC;
                }
                else if (fB1 >= 0.0)
                {
                    fT = 0.0;
                    fSqrDistance = fC;
                }
                else
                {
                    fT = -fB1 / fA11;
                    fSqrDistance = fB1 * fT + fC;
                }
            }
        }
        else if (fT < 0.0)  // region 6
        {
            fTmp0 = fA01 + fB1;
            fTmp1 = fA00 + fB0;
            if (fTmp1 > fTmp0)
            {
                fNumer = fTmp1 - fTmp0;
                fDenom = fA00 - 2.0 * fA01 + fA11;
                if (fNumer >= fDenom)
                {
                    fT = 1.0;
                    fS = 0.0;
                    fSqrDistance = fA11 + 2.0 * fB1 + fC;
                }
                else
                {
                    fT = fNumer / fDenom;
                    fS = 1.0 - fT;
                    fSqrDistance = fS * (fA00 * fS + fA01 * fT + 2.0 * fB0) +
                        fT * (fA01 * fS + fA11 * fT + 2.0 * fB1) + fC;
                }
            }
            else
            {
                fT = 0.0;
                if (fTmp1 <= 0.0)
                {
                    fS = 1.0;
                    fSqrDistance = fA00 + 2.0 * fB0 + fC;
                }
                else if (fB0 >= 0.0)
                {
                    fS = 0.0;
                    fSqrDistance = fC;
                }
                else
                {
                    fS = -fB0 / fA00;
                    fSqrDistance = fB0 * fS + fC;
                }
            }
        }
        else  // region 1
        {
            fNumer = fA11 + fB1 - fA01 - fB0;
            if (fNumer <= 0.0)
            {
                fS = 0.0;
                fT = 1.0;
                fSqrDistance = fA11 + 2.0 * fB1 + fC;
            }
            else
            {
                fDenom = fA00 - 2.0f * fA01 + fA11;
                if (fNumer >= fDenom)
                {
                    fS = 1.0;
                    fT = 0.0;
                    fSqrDistance = fA00 + 2.0 * fB0 + fC;
                }
                else
                {
                    fS = fNumer / fDenom;
                    fT = 1.0 - fS;
                    fSqrDistance = fS * (fA00 * fS + fA01 * fT + 2.0 * fB0) +
                        fT * (fA01 * fS + fA11 * fT + 2.0 * fB1) + fC;
                }
            }
        }
    }

    // account for numerical round-off error
    if (fSqrDistance < 0.0)
    {
        fSqrDistance = 0.0;
    }

    p0 = pt;
    p1 = tri.edges[0] + fS * kEdge0 + fT * kEdge1;
    return fSqrDistance;
}


double GeometryTools3D::squareDistance(const Line3D& line, const Segment3D& seg, BaseMathLib::Vector3D& p0, BaseMathLib::Vector3D& p1, double& lineParameter)
{
    const BaseMathLib::Vector3D kDiff = line.origin - seg.origin;
    const double fA01 = -line.direction.dotProduct(seg.direction);
    const double fB0 = kDiff.dotProduct(line.direction);
    const double fC = kDiff.squaredLength();
    const double fDet = fabs(1.0 - fA01 * fA01);
    double fB1, fS0, fS1, fSqrDist, fExtDet;

    if (fDet >= zeroTolerance)
    {
        // The line and segment are not parallel.
        fB1 = -kDiff.dotProduct(seg.direction);
        fS1 = fA01 * fB0 - fB1;
        fExtDet = seg.extent * fDet;

        if (fS1 >= -fExtDet)
        {
            if (fS1 <= fExtDet)
            {
                // Two interior points are closest, one on the line and one
                // on the segment.
                const double fInvDet = 1.0 / fDet;
                fS0 = (fA01 * fB1 - fB0) * fInvDet;
                fS1 *= fInvDet;
                fSqrDist = fS0 * (fS0 + fA01 * fS1 + 2.0 * fB0) +
                    fS1 * (fA01 * fS0 + fS1 + 2.0 * fB1) + fC;
            }
            else
            {
                // The end point e1 of the segment and an interior point of
                // the line are closest.
                fS1 = seg.extent;
                fS0 = -(fA01 * fS1 + fB0);
                fSqrDist = -fS0 * fS0 + fS1 * (fS1 + 2.0 * fB1) + fC;
            }
        }
        else
        {
            // The end point e0 of the segment and an interior point of the
            // line are closest.
            fS1 = -seg.extent;
            fS0 = -(fA01 * fS1 + fB0);
            fSqrDist = -fS0 * fS0 + fS1 * (fS1 + 2.0 * fB1) + fC;
        }
    }
    else
    {
        // The line and segment are parallel.  Choose the closest pair so that
        // one point is at segment origin.
        fS1 = 0.0;
        fS0 = -fB0;
        fSqrDist = fB0 * fS0 + fC;
    }

    p0 = line.origin + fS0 * line.direction;
    p1 = seg.origin + fS1 * seg.direction;
    lineParameter = fS0;
    return fabs(fSqrDist);
}


double GeometryTools3D::squareDistance(const Line3D& line, const Triangle3D& tri, BaseMathLib::Vector3D& p0, BaseMathLib::Vector3D& p1, double& lineParameter)
{
    // Test if line intersects triangle.  If so, the squared distance is zero.
    const BaseMathLib::Vector3D kEdge0 = tri.edges[1] - tri.edges[0];
    const BaseMathLib::Vector3D kEdge1 = tri.edges[2] - tri.edges[0];
    const BaseMathLib::Vector3D kNormal = vectorialProduct(kEdge0, kEdge1).normalise();
    const double fNdD = kNormal.dotProduct(line.direction);
    if (std::fabs(fNdD) > zeroTolerance)
    {
        // The line and triangle are not parallel, so the line intersects
        // the plane of the triangle.
        const BaseMathLib::Vector3D kDiff = line.origin - tri.edges[0];
        BaseMathLib::Vector3D rkD = line.direction;
        BaseMathLib::Vector3D kU, kV;
        GenerateComplementBasis(kU, kV, rkD);
        const double fUdE0 = kU.dotProduct(kEdge0);
        const double fUdE1 = kU.dotProduct(kEdge1);
        const double fUdDiff = kU.dotProduct(kDiff);
        const double fVdE0 = kV.dotProduct(kEdge0);
        const double fVdE1 = kV.dotProduct(kEdge1);
        const double fVdDiff = kV.dotProduct(kDiff);
        const double fInvDet = 1.0 / (fUdE0 * fVdE1 - fUdE1 * fVdE0);

        // Barycentric coordinates for the point of intersection.
        const double fB1 = (fVdE1 * fUdDiff - fUdE1 * fVdDiff) * fInvDet;
        const double fB2 = (fUdE0 * fVdDiff - fVdE0 * fUdDiff) * fInvDet;
        const double fB0 = 1.0 - fB1 - fB2;

        if (fB0 >= 0.0 && fB1 >= 0.0 && fB2 >= 0.0)
        {
            // Line parameter for the point of intersection.
            const double fDdE0 = rkD.dotProduct(kEdge0);
            const double fDdE1 = rkD.dotProduct(kEdge1);
            const double fDdDiff = line.direction.dotProduct(kDiff);
            lineParameter = fB1 * fDdE0 + fB2 * fDdE1 - fDdDiff;

            // The intersection point is inside or on the triangle.
            p0 = line.origin + lineParameter * line.direction;

            p1 = tri.edges[0] + fB1 * kEdge0 + fB2 * kEdge1;
            return 0.0;
        }
    }

    // Either (1) the line is not parallel to the triangle and the point of
    // intersection of the line and the plane of the triangle is outside the
    // triangle or (2) the line and triangle are parallel.  Regardless, the
    // closest point on the triangle is on an edge of the triangle.  Compare
    // the line to all three edges of the triangle.
    double fSqrDist = std::numeric_limits<double>::max();
    for (int i0 = 2, i1 = 0; i1 < 3; i0 = i1++)
    {
        Segment3D seg;
        seg.origin = 0.5 * (tri.edges[i0] + tri.edges[i1]);
        seg.direction = tri.edges[i1] - tri.edges[i0];
        seg.extent = 0.5 * seg.direction.normalise();

        BaseMathLib::Vector3D p0Tmp, p1Tmp;
        double lineParamterTmp;
        double fSqrDistTmp = squareDistance(line, seg, p0Tmp, p1Tmp, lineParamterTmp);

        if (fSqrDistTmp < fSqrDist)
        {
            p0 = p0Tmp;
            p1 = p1Tmp;
            fSqrDist = fSqrDistTmp;

            lineParameter = lineParamterTmp;
        }
    }
    return fSqrDist;
}


double GeometryTools3D::squareDistance(const Segment3D& seg, const Triangle3D& tri, BaseMathLib::Vector3D& p0, BaseMathLib::Vector3D& p1)
{
    BaseMathLib::Vector3D p0Tmp, p1Tmp;
    double lineParameter;
    double sqrDist = squareDistance(Line3D(seg.origin, seg.direction), tri, p0Tmp, p1Tmp, lineParameter);

    if (lineParameter >= -seg.extent)
    {
        if (lineParameter <= seg.extent)
        {
            p0 = p0Tmp;
            p1 = p1Tmp;
        }
        else
        {
            p0 = seg.origin + seg.extent * seg.direction;

            BaseMathLib::Vector3D p0Tmp, p1Tmp;
            sqrDist = squareDistance(p0, tri, p0Tmp, p1Tmp);

            p1 = p1Tmp;
        }
    }
    else
    {
        p0 = seg.origin - seg.extent * seg.direction;

        BaseMathLib::Vector3D p0Tmp, p1Tmp;
        sqrDist = squareDistance(p0, tri, p0Tmp, p1Tmp);

        p1 = p1Tmp;
    }

    return sqrDist;
}

double GeometryTools3D::squareDistance(const Triangle3D& tri0, const Triangle3D& tri1, BaseMathLib::Vector3D& p0, BaseMathLib::Vector3D& p1)
{
    double sqrDist = std::numeric_limits<double>::max();
    Segment3D seg;
    int i0, i1;

    //i0 i1
    //2 - 0
    //0 - 1
    //1 - 2

    // compare edges of triangle0 to the interior of triangle1
    for (i0 = 2, i1 = 0; i1 < 3; i0 = i1++)
    {
        seg.origin = 0.5 * (tri0.edges[i0] + tri0.edges[i1]);
        seg.direction = tri0.edges[i1] - tri0.edges[i0];
        seg.extent = 0.5 * seg.direction.normalise();

        BaseMathLib::Vector3D p0Tmp, p1Tmp;
        double sqrDistTmp = squareDistance(seg, tri1, p0Tmp, p1Tmp);

        if (sqrDistTmp < sqrDist)
        {
            p0 = p0Tmp;
            p1 = p1Tmp;
            sqrDist = sqrDistTmp;

            if (sqrDistTmp <= zeroTolerance)
            {
                return 0.0;
            }
        }
    }

    // compare edges of triangle1 to the interior of triangle0
    for (i0 = 2, i1 = 0; i1 < 3; i0 = i1++)
    {
        seg.origin = 0.5 * (tri1.edges[i0] + tri1.edges[i1]);
        seg.direction = tri1.edges[i1] - tri1.edges[i0];
        seg.extent = 0.5 * seg.direction.normalise();

        BaseMathLib::Vector3D p0Tmp, p1Tmp;
        double sqrDistTmp = squareDistance(seg, tri0, p0Tmp, p1Tmp);

        if (sqrDistTmp < sqrDist)
        {
            p0 = p0Tmp;
            p1 = p1Tmp;
            sqrDist = sqrDistTmp;

            if (sqrDist <= zeroTolerance)
            {
                return 0.0;
            }
        }
    }

    return sqrDist;
}