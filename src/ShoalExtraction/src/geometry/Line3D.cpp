
#include "ShoalExtraction/geometry/Line3D.h"

using namespace shoalextraction;

Line3D::Line3D()
{
}

Line3D::Line3D(const BaseMathLib::Vector3D& _origin, const BaseMathLib::Vector3D& _direction)
    : origin(_origin)
    , direction(_direction)
{
}

Line3D::~Line3D()
{
}