// -*- C++ -*-
// ****************************************************************************
// Class: OBoundingBox3D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif
#include <math.h>

#include <assert.h>
#include <float.h>
#include "ShoalExtraction/geometry/OBoundingBox3D.h"

#include "M3DKernel/utils/carto/CartoTools.h"

using namespace shoalextraction;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
OBoundingBox3D::OBoundingBox3D()
{
	m_points.resize(8);
}

// Destructeur
OBoundingBox3D::~OBoundingBox3D()
{
}

// *********************************************************************
// M�thodes
// *********************************************************************

/**
* Update
* Methode : Initialise les donn�es membres
* @param void
* @return void
*/
void OBoundingBox3D::Update(const std::vector<BaseMathLib::Vector3D>& points)
{
	auto iter = points.cbegin();
	const auto iter_end = points.cend();

	while (iter != iter_end)
	{
		Update(*iter);
		++iter;
	}
}

/**
* Update
* Methode : MaJ les donn�es membres
* @param void
* @return void
*/
void OBoundingBox3D::Update(const BaseMathLib::Vector3D& point)
{
	//changement de rep�re
	double x, y, z;
	ApplyBasis(point, x, y, z);
	//MaJ
	BoundingBox3D::Update(x, y, z);
}

/**
* Init
* Methode : Initialise les donn�es membres
* @param const Wm4::Vector3d* point
* @return void
*/
void OBoundingBox3D::Init(const BaseMathLib::Vector3D& point)
{
	//changement de rep�re
	double x, y, z;
	ApplyBasis(point, x, y, z);
	//Initialisation
	BoundingBox3D::Init(x, y, z);
}

/**
* InitAxis
* Methode : Initialise les axes
* @param const Wm4::Vector3d& center : centre du repere
* @param double xyAngle : angle par rapport a l'axe X  dans le plan XY
* @param double zxAngle : angle par rapport a l'axe Z  dans le plan ZX
* @param double yzAngle : angle par rapport a l'axe Y  dans le plan YZ
* @return void
*/
void OBoundingBox3D::InitAxis(const BaseMathLib::Vector3D& center,
	double xyAngle,
	double zxAngle,
	double yzAngle)
{
	Reset();

	//cr�ation des plans de coupe pour chaque paire d'axe
	double cosXYAngle = cos(xyAngle);
	double sinXYAngle = sin(xyAngle);
	double cosZXAngle = cos(zxAngle);
	double sinZXAngle = sin(zxAngle);
	double cosYZAngle = cos(yzAngle);
	double sinYZAngle = sin(yzAngle);

	//(1, 0, 0) (0, -1, 0) (0, 0, 1)
	m_X = BaseMathLib::Vector3D(cosXYAngle * cosZXAngle, sinXYAngle * cosZXAngle, sinZXAngle);
	m_Y = BaseMathLib::Vector3D(sinXYAngle * cosYZAngle, -cosXYAngle * cosYZAngle, sinYZAngle);
	m_Z =vectorialProduct(m_X, m_Y);
	m_Z.normalise();

	m_center = center;

	//centrage des axes
	BaseMathLib::Vector3D xAxis = m_X + center;
	BaseMathLib::Vector3D yAxis = m_Y + center;
	BaseMathLib::Vector3D zAxis = m_Z + center;

	//plan XY
	m_XY = Plane3D(center, xAxis, yAxis);
	//plan ZX
	m_ZX = Plane3D(center, zAxis, xAxis);
	//plan YZ
	m_YZ = Plane3D(center, yAxis, zAxis);
}

/**
* InitCenterGPS
* Methode : initialisationdu centre en coordonn�es g�ographiques
* @param double x : coordonn�e en lat
* @param double y : coordonn�e en std::int32_t
* @param double z : coordonn�e en profondeur
* @return void
*/
void OBoundingBox3D::InitCenterGPS(double x, double y, double z)
{
	m_centerGPS.x = x;
	m_centerGPS.y = y;
	m_centerGPS.z = z;
}

/**
* ApplyBasis
* Methode : changement de rep�re
* @param const Wm4::Vector3d* point : point dans le rep�re monde
* @param double x : coordonn�e en X dans le rep�re de la OBBox
* @param double y : coordonn�e en X dans le rep�re de la OBBox
* @param double z : coordonn�e en X dans le rep�re de la OBBox
* @return void
*/
void OBoundingBox3D::ApplyBasis(const BaseMathLib::Vector3D& point, double& x, double& y, double& z)
{
	//la valeur en X dans le rep�re de la OBbox est donn� par la distance au plan YZ
	//la valeur en Y dans le rep�re de la OBbox est donn� par la distance au plan ZX
	//la valeur en Z dans le rep�re de la OBbox est donn� par la distance au plan XY
	x = m_YZ.distanceTo(point);
	y = m_ZX.distanceTo(point);
	z = m_XY.distanceTo(point);
}


/**
* Serialize
* Methode : serialisation
* @param std::ofstream& ofs
* @return void
*/
void OBoundingBox3D::Serialize(std::ofstream& ofs) const
{
	ofs << "Center\t";
	ofs << m_center.x << "\t" << m_center.y << "\t" << m_center.z << std::endl;

	ofs << "Size\t";
	ofs << GetDeltaX() << "\t" << GetDeltaY() << "\t" << GetDeltaZ() << std::endl;

	ofs << "Heading" << "\t" << 180.0 / M_PI * atan2(m_X.y, m_X.x) << std::endl;
	ofs << "Pitch" << "\t" << 180.0 / M_PI * asin(m_X.z) << std::endl;
	ofs << "Roll" << "\t" << 180.0 / M_PI * asin(m_Y.z) << std::endl;

	ofs << std::endl;
}

/**
* SerializeSize
* Methode : serialisation de la taille
* @param std::ofstream& ofs
* @return void
*/
void OBoundingBox3D::SerializeSize(std::ofstream& ofs) const
{
	ofs << "DeltaX\t";
	ofs << m_dXMax - m_dXMin << "\t";

	ofs << "DeltaY\t";
	ofs << m_dYMax - m_dYMin << "\t";

	ofs << "DeltaZ\t";
	ofs << m_dZMax - m_dZMin;

	ofs << std::endl;
}

/**
* ComputePoints
* Methode : Calcul des points
* @param void
* @return void
*/
void OBoundingBox3D::ComputePoints()
{
	//calcul des 8 coins (agenc�s pour etre utilis�s directement dans un objet vtkOutlineSource)
	m_points[0] = BaseMathLib::Vector3D(m_X * m_dXMin + m_Y * m_dYMin + m_Z * m_dZMin);
	m_points[1] = BaseMathLib::Vector3D(m_X * m_dXMax + m_Y * m_dYMin + m_Z * m_dZMin);
	m_points[2] = BaseMathLib::Vector3D(m_X * m_dXMin + m_Y * m_dYMax + m_Z * m_dZMin);
	m_points[3] = BaseMathLib::Vector3D(m_X * m_dXMax + m_Y * m_dYMax + m_Z * m_dZMin);
	m_points[4] = BaseMathLib::Vector3D(m_X * m_dXMin + m_Y * m_dYMin + m_Z * m_dZMax);
	m_points[5] = BaseMathLib::Vector3D(m_X * m_dXMax + m_Y * m_dYMin + m_Z * m_dZMax);
	m_points[6] = BaseMathLib::Vector3D(m_X * m_dXMin + m_Y * m_dYMax + m_Z * m_dZMax);
	m_points[7] = BaseMathLib::Vector3D(m_X * m_dXMax + m_Y * m_dYMax + m_Z * m_dZMax);

	for (size_t i = 0; i < 8; ++i)
	{
		m_points[i] += m_center;
	}
}


/**
* ComputePointsGPS
* Methode : Calcul des points en coordonn�es GPS
* @param void
* @return void
*/
void OBoundingBox3D::ComputePointsGPS(double points[24]) const
{
	BaseMathLib::Vector3D gpsPoints[8];

	//calcul des 8 coins (agenc�s pour etre utilis�s directement dans un objet vtkOutlineSource)
	gpsPoints[0] = BaseMathLib::Vector3D(m_X * m_dXMin + m_Y * m_dYMin + m_Z * m_dZMin);
	gpsPoints[1] = BaseMathLib::Vector3D(m_X * m_dXMax + m_Y * m_dYMin + m_Z * m_dZMin);
	gpsPoints[2] = BaseMathLib::Vector3D(m_X * m_dXMin + m_Y * m_dYMax + m_Z * m_dZMin);
	gpsPoints[3] = BaseMathLib::Vector3D(m_X * m_dXMax + m_Y * m_dYMax + m_Z * m_dZMin);
	gpsPoints[4] = BaseMathLib::Vector3D(m_X * m_dXMin + m_Y * m_dYMin + m_Z * m_dZMax);
	gpsPoints[5] = BaseMathLib::Vector3D(m_X * m_dXMax + m_Y * m_dYMin + m_Z * m_dZMax);
	gpsPoints[6] = BaseMathLib::Vector3D(m_X * m_dXMin + m_Y * m_dYMax + m_Z * m_dZMax);
	gpsPoints[7] = BaseMathLib::Vector3D(m_X * m_dXMax + m_Y * m_dYMax + m_Z * m_dZMax);

	for (size_t i = 0; i < 8; ++i)
	{
		auto& gpsPoint = gpsPoints[i];

		// calcul des coordonn�es du point en lat/std::int32_t
		CCartoTools::GetGeoCoords(m_centerGPS.x, m_centerGPS.y, gpsPoint.x, gpsPoint.y, gpsPoint.x, gpsPoint.y);

		// calcul des coordonn�es du point en cartesien
		CCartoTools::GetCartesianCoords(gpsPoint.x, gpsPoint.y, gpsPoint.x, gpsPoint.y);

		points[i * 3] = gpsPoint.y;
		points[i * 3 + 1] = -(gpsPoint.z + m_centerGPS.z);
		points[i * 3 + 2] = -gpsPoint.x;
	}
}


//test de distance
  /**
 * CloseTo
 * Methode : test de distance
 * @param const OBoundingBox3D& other
 * @param double thresholdX
 * @param double thresholdY
 * @param double thresholdZ
 * @return bool
 */
bool OBoundingBox3D::CloseTo(const OBoundingBox3D& other, double thresholdX, double thresholdY, double thresholdZ) const
{
	//comparaison en 3 �tapes dans chaque plan :
	//1 : calcul de distance avec le centre de (other)
	//2 : soustraction de la taille de other et (this) 
	//3 : comparaison au seuil
	double distX = m_YZ.distanceTo(other.GetCenter());
	double distY = m_ZX.distanceTo(other.GetCenter());
	double distZ = m_XY.distanceTo(other.GetCenter());

	//en fonction du signe de la distance, on soustrait la taille positive ou n�gative de la bbox
	if (distX > 0)
	{
		distX = distX - m_dXMax + other.m_dXMin;
	}
	else
	{
		distX = -distX + m_dXMin - other.m_dXMax;
	}
	if (distY > 0)
	{
		distY = distY - m_dYMax + other.m_dYMin;
	}
	else
	{
		distY = -distY + m_dYMin - other.m_dYMax;
	}
	if (distZ > 0)
	{
		distZ = distZ - m_dZMax + other.m_dZMin;
	}
	else
	{
		distZ = -distZ + m_dZMin - other.m_dZMax;
	}

	//comparaison aux seuils
	return distX < thresholdX && distY < thresholdY && distZ < thresholdZ;
}
