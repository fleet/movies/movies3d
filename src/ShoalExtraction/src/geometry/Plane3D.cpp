
#include "ShoalExtraction/geometry/Plane3D.h"

using namespace shoalextraction;

Plane3D::Plane3D()
{
}

Plane3D::Plane3D(const BaseMathLib::Vector3D& p0, const BaseMathLib::Vector3D& p1, const BaseMathLib::Vector3D& p2)
{
    BaseMathLib::Vector3D e1 = p1 - p0;
    BaseMathLib::Vector3D e2 = p2 - p0;
    normal = vectorialProduct(e1, e2);
    normal.normalise();
    constant = normal.dotProduct(p0);
}

Plane3D::~Plane3D()
{
}

double Plane3D::distanceTo(const BaseMathLib::Vector3D& pt) const
{
    return normal.dotProduct(pt) - constant;
}

int Plane3D::side(const BaseMathLib::Vector3D& pt) const
{
    const double d = distanceTo(pt);

    if (d < 0.0)
    {
        return -1;
    }

    if (d > 0.0)
    {
        return +1;
    }

    return 0;
}