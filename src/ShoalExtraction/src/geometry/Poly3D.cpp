// -*- C++ -*-
// ****************************************************************************
// Class: Poly3D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <float.h>

#include "ShoalExtraction/geometry/Poly3DIntersectionTriangles.h"
#include "ShoalExtraction/geometry/Poly3DIntersectionPlanes.h"

#include "BaseMathLib/geometry/GeometryTools2D.h"
#include "BaseMathLib/Vector3.h"

#include "ShoalExtraction/geometry/Poly3D.h"

using namespace shoalextraction;
using namespace BaseMathLib;

//#define USE_INTERSECTION_TRIANGLES 

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
Poly3D::Poly3D()
{
	m_pIntersection = NULL;
}

// Destructeur	
Poly3D::~Poly3D()
{
	Purge();
}

/**
* PurgePoints
* Methode : purge des points
* @param void
* @return void
*/
void Poly3D::PurgePoints()
{
	m_points.clear();
}

/**
* Purge
* Methode : purge des donn�es
* @param void
* @return void
*/
void Poly3D::Purge()
{
	//purge des points
	PurgePoints();

	//suppression de l'intersection
	if (m_pIntersection != NULL)
	{
		delete m_pIntersection;
		m_pIntersection = NULL;
	}
}

/**
* Alloc
* Methode : Allocation de n points 3D
* @param void
* @return void
*/
void Poly3D::Alloc(int nbPoints)
{

	//optimisation allocation m�moire
	if (m_points.size() != nbPoints)
	{
		PurgePoints();

		if (nbPoints > 0)
		{
			m_points.resize(nbPoints);
		}
	}

	//suppression de l'intersection
	if (m_pIntersection != NULL)
	{
		delete m_pIntersection;
		m_pIntersection = NULL;
	}
}

/**
* Alloc
* Methode : Ajustement (r�duction) du nombre de points n 3D
* @param void
* @return void
*/
void Poly3D::Shrink(int newNbPoint)
{
	m_points.resize(newNbPoint);
}

/**
* CloseTo
* Methode : test de distance
* @param const Poly3D& other
* @param double thresholdX
* @param double thresholdY
* @param double thresholdZ
* @return bool
*/
bool Poly3D::CloseTo(Poly3D& other, double thresholdX, double thresholdY, double thresholdZ)
{
	bool result = false;

	//test pr�liminaire : bounding box
	if (GetBoundingBox().CloseTo(other.GetBoundingBox(), thresholdX, thresholdY, thresholdZ))
	{
		//initialisation intersection
		InitIntersection(thresholdX, thresholdY, thresholdZ);
		other.InitIntersection(thresholdX, thresholdY, thresholdZ);

		//test intersection
		result = GetIntersection()->CloseTo(other.GetIntersection(), thresholdX, thresholdY, thresholdZ);
	}

	return result;
}


/**
* Serialize
* Methode : serialisation
* @param std::ofstream& ofs
* @return void
*/
void Poly3D::Serialize(std::ofstream& ofs) const
{/*
	std::vector<Wm4::Vector3d*>::iterator iterP = GetPoints().begin();
	while(iterP != GetPoints().end())
	{
		ofs << (*iterP)->X() << ";" << (*iterP)->Y() << ";" << (*iterP)->Z() << std::endl;
		iterP++;
	}*/

	//point 1
	const auto& p0 = m_points[0];
	ofs << "P1\t";
	ofs << p0.x << "\t"
		<< p0.y << "\t"
		<< p0.z << "\t";

	//point 7
	const auto& p6 = m_points[6];
	ofs << "P2\t";
	ofs << p6.x << "\t"
		<< p6.y << "\t"
		<< p6.z << std::endl;
}

//initialisation de l'intersection
/**
* InitIntersection
* Methode : initialisation de l'intersection
* @param double thresholdX
* @param double thresholdY
* @param double thresholdZ
* @return void
*/
void Poly3D::InitIntersection(double thresholdX, double thresholdY, double thresholdZ)
{
	if (GetIntersection() == NULL)
	{
#ifdef USE_INTERSECTION_TRIANGLES
		SetIntersection(new Poly3DIntersectionTriangles());
#else
		SetIntersection(new Poly3DIntersectionPlanes());
#endif
		GetIntersection()->Init(GetPoints(), thresholdX, thresholdY, thresholdZ);
	}
}
