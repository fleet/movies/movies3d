// -*- C++ -*-
// ****************************************************************************
// Class: Poly3DIntersectionPlanes
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <float.h>
#include <assert.h>

#include "ShoalExtraction/geometry/geometrytools3d.h"
#include "BaseMathLib/Vector3.h"

#include "ShoalExtraction/geometry/Poly3DIntersectionPlanes.h"

using namespace shoalextraction;
using namespace BaseMathLib;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
Poly3DIntersectionPlanes::Poly3DIntersectionPlanes()
{
}

// Destructeur	
Poly3DIntersectionPlanes::~Poly3DIntersectionPlanes()
{
}

// *********************************************************************
// M�thodes
// *********************************************************************

/**
  * Init
  *
  * Initialize the list of Planes3D
  * Polygons are parralelepipeds which points are clockwise sorted by bottom (A) and top (B) faces :
  *
  * @param const std::vector<Wm4::Vector3d*>& points
  * @return void
  */
void Poly3DIntersectionPlanes::Init(const std::vector<BaseMathLib::Vector3D>& points,
	double thresholdX,
	double thresholdY,
	double thresholdZ)
{
	assert(points.size() == 8);

	if (m_planes.size() == 0)
	{
		m_pointsRef = points;

		const BaseMathLib::Vector3D & p1 = points[0];
		const BaseMathLib::Vector3D & p2 = points[1];
		const BaseMathLib::Vector3D & p3 = points[2];
		const BaseMathLib::Vector3D & p4 = points[3];
		const BaseMathLib::Vector3D & p5 = points[4];
		const BaseMathLib::Vector3D & p6 = points[5];
		const BaseMathLib::Vector3D & p7 = points[6];
		const BaseMathLib::Vector3D & p8 = points[7];

		//attention a bien orienter les plans pour avoir le cot� positif � l'exterieur du poly
		//regle main gauche
		m_planes.resize(6);

		m_planes[0] = Plane3D(p1, p2, p3);//back
		m_planes[1] = Plane3D(p1, p6, p2);//top
		m_planes[2] = Plane3D(p2, p6, p7);//right
		m_planes[3] = Plane3D(p8, p5, p1);//left
		m_planes[4] = Plane3D(p4, p3, p8);//bottom
		m_planes[5] = Plane3D(p7, p6, p5);//front

		auto iterPlanes = m_planes.begin();
		const auto iterPlanesEnd = m_planes.end();

		while (iterPlanes != iterPlanesEnd)
		{
			//effectuer un offset pour tenir compte des seuils
			GeometryTools3D::Offset(thresholdX, thresholdY, thresholdZ, *iterPlanes);
			++iterPlanes;
		};
	}
}

/**
* CloseTo
* Methode : test de distance
* @param const Poly3DIntersectionPlanes& other
* @param double thresholdX
* @param double thresholdY
* @param double thresholdZ
* @return bool
*/
bool Poly3DIntersectionPlanes::CloseTo(IPoly3DIntersection* other,
	double thresholdX,
	double thresholdY,
	double thresholdZ)
{
	bool result = false;

	assert(other != NULL);

	//les valeurs de seuils ne sont pas utilis�es ici puisqu'elels sont deja int�gr�es 
	//dans le positionnement des plans

	//les intersection doivent etre de mm type pour comparaison
	Poly3DIntersectionPlanes* pOtherIP = dynamic_cast<Poly3DIntersectionPlanes*>(other);
	assert(pOtherIP != NULL);

	//test d'inclusion des points dans le polygone other
	result = GeometryTools3D::CloseTo(GetPointsRef(), pOtherIP->GetPlanes());

	if (!result)
	{
		//test d'inclusion des points de other dans le polygone 
		result = GeometryTools3D::CloseTo(pOtherIP->GetPointsRef(), GetPlanes());
	}
	return result;
}
