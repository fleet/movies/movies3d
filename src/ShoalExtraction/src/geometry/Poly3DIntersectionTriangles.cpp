// -*- C++ -*-
// ****************************************************************************
// Class: Poly3DIntersectionTriangles
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <float.h>
#include <assert.h>

#include "ShoalExtraction/geometry/geometrytools3d.h"
#include "ShoalExtraction/geometry/Triangle3D.h"
#include "BaseMathLib/Vector3.h"

#include "ShoalExtraction/geometry/Poly3DIntersectionTriangles.h"

using namespace shoalextraction;
using namespace BaseMathLib;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

// Constructeur par d�faut
Poly3DIntersectionTriangles::Poly3DIntersectionTriangles()
{
	m_triangles.resize(12);
}

// Destructeur	
Poly3DIntersectionTriangles::~Poly3DIntersectionTriangles()
{
}


/**
  * Init
  *
  * Initialize the list of Triangles3D
  * Polygons are parralelepipeds which points are clockwise sorted by bottom (A) and top (B) faces :
  *
  * @param const std::vector<Wm4::Vector3d*>& points
  * @return void
  */
void Poly3DIntersectionTriangles::Init(const std::vector<BaseMathLib::Vector3D>& points,
	double thresholdX,
	double thresholdY,
	double thresholdZ)
{
	assert(points.size() == 8);

	if (m_triangles.size() == 0)
	{
		m_pointsRef = points;

		const BaseMathLib::Vector3D & p1 = points[0];
		const BaseMathLib::Vector3D & p2 = points[1];
		const BaseMathLib::Vector3D & p3 = points[2];
		const BaseMathLib::Vector3D & p4 = points[3];
		const BaseMathLib::Vector3D & p5 = points[4];
		const BaseMathLib::Vector3D & p6 = points[5];
		const BaseMathLib::Vector3D & p7 = points[6];
		const BaseMathLib::Vector3D & p8 = points[7];

		m_triangles[0] = Triangle3D(p1, p2, p3);//back
		m_triangles[1] = Triangle3D(p3, p4, p1);//back

		m_triangles[2] = Triangle3D(p1, p2, p6);//top
		m_triangles[3] = Triangle3D(p6, p5, p1);//top

		m_triangles[4] = Triangle3D(p2, p6, p7);//right
		m_triangles[5] = Triangle3D(p7, p3, p2);//right
		
		m_triangles[6] = Triangle3D(p1, p5, p8);//left
		m_triangles[7] = Triangle3D(p8, p4, p1);//left

		m_triangles[8] = Triangle3D(p3, p7, p8);//bottom
		m_triangles[9] = Triangle3D(p8, p4, p3);//bottom

		m_triangles[10] = Triangle3D(p5, p6, p7);//front
		m_triangles[11] = Triangle3D(p7, p8, p5);//front
	}
}

/**
* CloseTo
* Methode : test de distance
* @param const Poly3DIntersectionTriangles& other
* @param double thresholdX
* @param double thresholdY
* @param double thresholdZ
* @return bool
*/
bool Poly3DIntersectionTriangles::CloseTo(IPoly3DIntersection* other,
	double thresholdX,
	double thresholdY,
	double thresholdZ)
{
	bool result = false;

	assert(other != NULL);

	//les intersection doivent etre de mm type pour comparaison
	Poly3DIntersectionTriangles* pOtherIT = dynamic_cast<Poly3DIntersectionTriangles*>(other);
	assert(pOtherIT != NULL);

	//si les marges ne sont pas nulles --> test le moins couteux et le plus probable : test sur les sommets
	if (thresholdX > 0 && thresholdY > 0 && thresholdZ > 0)
	{
		result = GeometryTools3D::CloseTo(GetPointsRef(), pOtherIT->GetPointsRef(), thresholdX, thresholdY, thresholdZ);
	}

	//test le plus couteux et le moins probable : test sur les triangles des faces
	if (!result)
	{
		result = GeometryTools3D::CloseTo(GetTriangles(), pOtherIT->GetTriangles(), thresholdX, thresholdY, thresholdZ);
	}

	return result;
}
