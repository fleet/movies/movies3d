
#include "ShoalExtraction/geometry/Segment3D.h"

using namespace shoalextraction;

Segment3D::Segment3D()
{
}

Segment3D::Segment3D(const BaseMathLib::Vector3D& p0, const BaseMathLib::Vector3D& p1)
{
    origin = (p0 + p1) * 0.5;
    direction = p1 - p0;
    extent = direction.length() * 0.5;
    direction.normalise();
}

Segment3D::~Segment3D()
{
}