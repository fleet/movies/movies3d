// -*- C++ -*-
// ****************************************************************************
// Class: Triangle3D
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Mars 2009
// Soci�t� : IPSIS
// ****************************************************************************
#include "ShoalExtraction/geometry/Triangle3D.h"

// ***************************************************************************
// Dependences
// ***************************************************************************

#include <math.h>
#include <float.h>
#include "ShoalExtraction/geometry/geometrytools3d.h"
#include "BaseMathLib/Vector3.h"
#include "ShoalExtraction/geometry/Line3D.h"
#include "ShoalExtraction/geometry/Segment3D.h"
#include "ShoalExtraction/geometry/Triangle3D.h"


#ifdef _TRACE_PERF
#define _TRIANGLE3D_TRACE_PERF

#ifdef _TRIANGLE3D_TRACE_PERF
extern std::ofstream trace_ofs;
#endif
#endif

using namespace shoalextraction;

// *********************************************************************
// Constructeurs / Destructeur
// *********************************************************************

Triangle3D::Triangle3D()
{
}

// Constructeur par d�faut
Triangle3D::Triangle3D(const BaseMathLib::Vector3D& point1, const BaseMathLib::Vector3D& point2, const BaseMathLib::Vector3D& point3)
{
    edges[0] = point1;
    edges[1] = point2;
    edges[2] = point3;

	//MaJ bbox
	bbox.Reset();
	bbox.Update(point1);
	bbox.Update(point2);
	bbox.Update(point3);
}

// Destructeur
Triangle3D::~Triangle3D()
{
}

// *********************************************************************
// M�thodes
// *********************************************************************

  /**
 * Intersect
 * Methode : test d'intersection
 * @param const Triangle3D& other
 * @return bool
 */
bool Triangle3D::Intersect(const Triangle3D& other) const
{
	return CloseTo(other, 0, 0, 0);
}

//test de distance
  /**
 * CloseTo
 * Methode : test de distance
 * @param const Triangle3D& other
 * @param double thresholdX
 * @param double thresholdY
 * @param double thresholdZ
 * @return bool
 */
bool Triangle3D::CloseTo(const Triangle3D& other, double thresholdX, double thresholdY, double thresholdZ) const
{
	bool result = false;

	//si les bounding box s'intersectent
	if (bbox.CloseTo(other.bbox, thresholdX, thresholdY, thresholdZ))
	{
        BaseMathLib::Vector3D closestPoint0, closestPoint1;
        GeometryTools3D::squareDistance(*this, other, closestPoint0, closestPoint0);

		double deltaX = fabs(closestPoint0.x - closestPoint1.x);
		double deltaY = fabs(closestPoint0.y - closestPoint1.y);
		double deltaZ = fabs(closestPoint0.z - closestPoint1.z);

		result = deltaX < FLT_EPSILON + thresholdX &&
			deltaY < FLT_EPSILON + thresholdY &&
			deltaZ < FLT_EPSILON + thresholdZ;
	}
	return result;
}

