// -*- C++ -*-
// ****************************************************************************
// Class: ShoalExtractionActionStack
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Sept 2009
// Soci�t� : IPSIS
// ****************************************************************************

#include "ShoalExtraction/multithread/ShoalExtractionActionStack.h"
#include "ShoalExtraction/multithread/ShoalExtractionStackAction.h"

#include <assert.h>

using namespace shoalextraction;

// *********************************************************************
// Constructors / Destructor
// *********************************************************************
// d�fault constructor
ShoalExtractionActionStack::ShoalExtractionActionStack(ShoalExtractionModule* pModule) :
	m_pModule(pModule)
{
}

// Destructor
ShoalExtractionActionStack::~ShoalExtractionActionStack(void)
{
}

// *********************************************************************
// Methods
// *********************************************************************

//add new ping fan action to process
void ShoalExtractionActionStack::ScheduleAddPingFan(PingFan* pFan,
	std::uint32_t pingFanCount,
	DELAYINFO_CALLBACK delayInfoCB)
{
	ShoalExtractionStackAction* pAction = new ShoalExtractionStackAction();
	pAction->actionType = E_ADDPINGFAN_ACTION;
	pAction->pFan = pFan;
	pAction->pModule = GetModule();
	pAction->pingFanCount = pingFanCount;
	pAction->delayInfoCB = delayInfoCB;

	ScheduleAction(pAction);
}
//add esu end action to process
void ShoalExtractionActionStack::ScheduleESUEnd()
{
	ShoalExtractionStackAction* pAction = new ShoalExtractionStackAction();
	pAction->actionType = E_ESUEND_ACTION;
	pAction->pModule = GetModule();

	ScheduleAction(pAction);
}
//add esu start action to process
void ShoalExtractionActionStack::ScheduleESUStart()
{
	ShoalExtractionStackAction* pAction = new ShoalExtractionStackAction();
	pAction->actionType = E_ESUSTART_ACTION;
	pAction->pModule = GetModule();

	ScheduleAction(pAction);
}
//add inc ESU Id action to process
void ShoalExtractionActionStack::ScheduleIncESUId()
{
	ShoalExtractionStackAction* pAction = new ShoalExtractionStackAction();
	pAction->actionType = E_INCESUID_ACTION;
	pAction->pModule = GetModule();

	ScheduleAction(pAction);
}

