// -*- C++ -*-
// ****************************************************************************
// Class: ShoalExtractionStackAction
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: F.Racap�
// Date  : Sept 2009
// Soci�t� : IPSIS
// ****************************************************************************

#include "ShoalExtraction/multithread/ShoalExtractionStackAction.h"

#include "ShoalExtraction/ShoalExtractionModule.h"
#include "ShoalExtraction/data/shoalidmgr.h"

#include <assert.h>

using namespace shoalextraction;

// *********************************************************************
// Constructors / Destructor
// *********************************************************************
// d�fault constructor
ShoalExtractionStackAction::ShoalExtractionStackAction()
{
	actionType = E_ESUEND_ACTION;
	pFan = NULL;
	pModule = NULL;
}

// Destructor
ShoalExtractionStackAction::~ShoalExtractionStackAction(void)
{
}

// *********************************************************************
// Methods
// *********************************************************************

//process
void ShoalExtractionStackAction::Process()
{
	assert(pModule != NULL);

	switch (actionType)
	{
	case E_ADDPINGFAN_ACTION:
		//perform the close action on the shoal extraction module
		pModule->AddPingFan(pFan);
		if (delayInfoCB != NULL)
			delayInfoCB(-1, pingFanCount, -1);
		break;
	case E_ESUEND_ACTION:
		pModule->ESUEnd();
		break;
	case E_ESUSTART_ACTION:
		pModule->ESUStart();
		break;
	case E_INCESUID_ACTION:
		pModule->IncESUId();
	default:
		break;
	}

}
