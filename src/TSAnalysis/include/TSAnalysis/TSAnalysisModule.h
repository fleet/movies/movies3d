// -*- C++ -*-
// ****************************************************************************
// Class: TSAnalysisModule
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : F�vrier 2016
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "TSAnalysis/TSAnalysisExport.h"
#include "M3DKernel/module/ProcessModule.h"

#include "TSAnalysis/TSAnalysisParameter.h"

#include <map>

// ***************************************************************************
// Declarations
// ***************************************************************************
class TSTrack;
class CalibrationModule;

class TSANALYSIS_API TSAnalysisModule : public ProcessModule
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	MovCreateMacro(TSAnalysisModule);

	// Destructeur
	virtual ~TSAnalysisModule();

	void PingFanAdded(PingFan *);
	void SounderChanged(std::uint32_t);
	void StreamClosed(const char *streamName);
	void StreamOpened(const char *streamName);
	void ESUStart(ESUParameter * pWorkingESU);
	void ESUEnd(ESUParameter * pWorkingESU, bool abort);

	BaseKernel::ParameterModule& GetParameterModule() { return m_parameter; }

	TSAnalysisParameter&	GetTSAnalysisParameter() { return m_parameter; }

    inline const std::map<std::uint64_t, TSTrack*> & getTracks() const { return m_mapTracks; }

	// Fonction permettant de savoir si la track r�pond aux exigences de nombre d'�chos 
	// et de trous d�finies dans les param�tres
	bool IsValidTrack(TSTrack * pTrack) const;

	inline void setCalibrationModule(CalibrationModule * calibrationModule) { m_pCalibrationModule = calibrationModule; }
	inline CalibrationModule * getCalibrationModule() { return m_pCalibrationModule; }

private:

	// Calcul de l'�cart type des phases
	void ComputeStdDevs(Phase * pPhaseStart, int iInf, int iSup, double alFactor, double athFactor,
		double & alStdDev, double & athStdDev);

	// Nettoyage de toutes les tracks calcul�es et encore en m�moire
	void ClearAll();

	// Nettoyage de toutes les tracks sortie de la zone m�moire et mise � jour de la liste des tracks potentiellement non closes
	void Clear(PingFan* pFan);

protected:
	//Constructeur
	TSAnalysisModule();

	// param�tres du module d'analyse TS
	TSAnalysisParameter m_parameter;

	// Ensemble des tracks pour les pingfans en m�moire. Poss�de l'ownership sur les tracks.
    std::map<std::uint64_t, TSTrack*> m_mapTracks;

	// Ensemble des tracks de travail (potentiellement non closes)
    std::map<std::uint64_t, TSTrack*> m_mapWorkingTracks;

	// Calibration module for TS correction
	CalibrationModule * m_pCalibrationModule;
};
