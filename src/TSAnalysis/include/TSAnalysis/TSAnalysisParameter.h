// -*- C++ -*-
// ****************************************************************************
// Class: TSAnalysisParameter
//
// Description: 
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : F�vrier 2016
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "TSAnalysis/TSAnalysisExport.h"
#include "M3DKernel/parameter/ParameterModule.h"

// ***************************************************************************
// Declarations
// ***************************************************************************

class TSANALYSIS_API TSAnalysisParameter :
	public BaseKernel::ParameterModule
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	TSAnalysisParameter();

	// Destructeur
	virtual ~TSAnalysisParameter();

	virtual bool Serialize(BaseKernel::MovConfig * MovConfig);
	virtual bool DeSerialize(BaseKernel::MovConfig * MovConfig);

	// *********************************************************************
	// Accesseurs
	// *********************************************************************

	// Activation ou non de la detection
	virtual bool GetDetectionEnabled() const { return m_bDetectionEnabled; }
	virtual void SetDetectionEnabled(bool b);

	//SeuilTS
	virtual float GetTSThreshold() const { return m_TSThreshold; }
	virtual void SetTSThreshold(float d) { m_TSThreshold = d; }

	//MinEchoLength
	virtual float GetMinEchoLength() const { return m_minEchoLength; }
	virtual void SetMinEchoLength(float d) { m_minEchoLength = d; }

	//MaxEchoLength
	virtual float GetMaxEchoLength() const { return m_maxEchoLength; }
	virtual void SetMaxEchoLength(float d) { m_maxEchoLength = d; }

	//MaxGainComp
	virtual float GetMaxGainComp() const { return m_maxGainComp; }
	virtual void SetMaxGainComp(float d) { m_maxGainComp = d; }

	//PhaseDev
	virtual float GetPhaseDev() const { return m_phaseDev; }
	virtual void SetPhaseDev(float d) { m_phaseDev = d; }

	//MinEchospace
	virtual float GetMinEchospace() const { return m_minEchoSpace; }
	virtual void SetMinEchospace(float d) { m_minEchoSpace = d; }

	// Activation ou non du tracking
	virtual bool GetTrackingEnabled() { return m_bTrackingEnabled; }
	virtual void SetTrackingEnabled(bool b) { m_bTrackingEnabled = b; }

	//maxSpeed
	virtual float GetMaxSpeed() const { return m_MaxSpeed; }
	virtual void SetMaxSpeed(float d) { m_MaxSpeed = d; }

	//maxHoles
	virtual int GetMaxHoles() const { return m_MaxHoles; }
	virtual void SetMaxHoles(int d) { m_MaxHoles = d; }

	//minEchoDepth
	virtual float GetMinEchoDepth() const { return m_minEchoDepth; }
	virtual void SetMinEchoDepth(float d) { m_minEchoDepth = d; }

	//maxEchoDepth
	virtual float GetMaxEchoDepth() const { return m_maxEchoDepth; }
	virtual void SetMaxEchoDepth(float d) { m_maxEchoDepth = d; }

	//minEchoNumber
	virtual int GetMinEchoNumber() const { return m_minEchoNumber; }
	virtual void SetMinEchoNumber(int d) { m_minEchoNumber = d; }

	//maxHolesPercent
	virtual float GetMaxHolesPercent() const { return m_maxHolesPercent; }
	virtual void SetMaxHolesPercent(float d) { m_maxHolesPercent = d; }

	//histogramMinValue
	virtual float GetHistogramMinValue() const { return m_histogramMinValue; }
	virtual void SetHistogramMinValue(float d) { m_histogramMinValue = d; }

	//histogramMaxValue
	virtual float GetHistogramMaxValue() const { return m_histogramMaxValue; }
	virtual void SetHistogramMaxValue(float d) { m_histogramMaxValue = d; }

	//histogramResolution
	virtual float GetHistogramResolution() const { return m_histogramResolution; }
	virtual void SetHistogramResolution(float d) { m_histogramResolution = d; }

	//histogramSounders
	virtual std::vector<std::uint32_t> & GetHistogramSounders() { return m_histogramSounders; }
	virtual const std::vector<std::uint32_t> & GetHistogramSounders() const { return m_histogramSounders; }

	//distPrevPost
	virtual float GetSpectrumDistPrevPost() const { return m_distPrevPost; }
	virtual void SetSpectrumDistPrevPost(float d) { m_distPrevPost = d; }

private:

	// *********************************************************************
	// Variables membres
	// *********************************************************************	

	//Activation de la d�tection
	bool m_bDetectionEnabled;

	//SeuilTS
	float m_TSThreshold;

	//Min/Max Echo Length
	float m_minEchoLength;
	float m_maxEchoLength;

	//MaxGainComp
	float m_maxGainComp;

	//PhaseDev
	float m_phaseDev;

	//MinEchoSpace
	float m_minEchoSpace;

	//TrackingEnabled
	bool m_bTrackingEnabled;

	//MaxSpeed
	float m_MaxSpeed;

	//MaxHoles
	int m_MaxHoles;

	// Fourchette de profondeur en dehors de laquelle ignorer les �chos pour le tracking
	float m_minEchoDepth;
	float m_maxEchoDepth;

	//MinEchoNumber
	int m_minEchoNumber;

	//MaxHolesPercent
	float m_maxHolesPercent;

	//HistogramMinValue
	float m_histogramMinValue;

	//HistogramMaxValue
	float m_histogramMaxValue;

	//HistogramResolution
	float m_histogramResolution;

	float m_distPrevPost;

	//HistogramSounders
	std::vector<std::uint32_t> m_histogramSounders;
};
