// -*- C++ -*-
// ****************************************************************************
// Class: TSTrack
//
// Description: Track
//
// Projet: MOVIES3D
// Auteur: Olivier TONCK
// Date  : F�vrier 2016
// Soci�t� : IPSIS
// ****************************************************************************

#pragma once

// ***************************************************************************
// Dependences
// ***************************************************************************
#include "TSAnalysis/TSAnalysisExport.h"
#include "M3DKernel/base/MovObject.h"
#include "BaseMathLib/Vector3.h"

#include <vector>

// ***************************************************************************
// Declarations
// ***************************************************************************
class Transducer;
class SingleTargetData;
class SingleTargetDataFM;

class TSANALYSIS_API TSTrack : public MovObject
{
public:
	// *********************************************************************
	// Constructeurs / Destructeur
	// *********************************************************************
	// Constructeur par d�faut
	MovCreateMacro(TSTrack);

	// Destructeur
	virtual ~TSTrack(void);

	// Ajout d'une track � la target
	void Appendtrack(SingleTargetData * pTS, std::uint64_t pingID, unsigned short softChanId,
		const BaseMathLib::Vector3D & position, const BaseMathLib::Vector3D & gpsPosition,
		int nbPings);

	// Calcule le pourcentage de trous dans la trace
	double GetMissingEchoesPercent();

	// *********************************************************************
	// Accesseurs
	// *********************************************************************
	Transducer * GetTransducer();
	void SetTransducer(Transducer * pTrans);

	std::uint64_t GetLastPingID();
	const BaseMathLib::Vector3D & GetLastPosition();

	std::uint64_t GetLabel();

	std::uint32_t GetSounderID();
	void SetSounderID(std::uint32_t sID);

	const std::vector<BaseMathLib::Vector3D> & GetPositions();
	const std::vector<BaseMathLib::Vector3D> & GetGPSPositions();
	const std::vector<std::uint64_t> & GetPingPositions();
	const std::vector<SingleTargetData*> & GetSingleTargets();
	const std::vector<unsigned short> & GetChannelIDs();

protected:
	//Constructeur
	TSTrack(void);

private:
	unsigned int m_sounderId;
	Transducer * m_pTransducer;

	std::uint64_t m_label;
	static std::uint64_t m_labelCount;

	// Liste des positions rep�re monde
	std::vector<BaseMathLib::Vector3D> m_Positions;

	// Liste des positions GPS
	std::vector<BaseMathLib::Vector3D> m_GPSPositions;

	// Liste des id de pings correspondants
	std::vector<std::uint64_t> m_PingPositions;

	// Liste des cibles simples constituantes
	std::vector<SingleTargetData*> m_SingleTargets;

	// Liste des identifiants de channel des cibles simples constituantes
	std::vector<unsigned short> m_SoftChannels;

	// Nombre de pings sur lesquels s'�tend la track. Permet de calculer un pourcentage
	// de "trous" dans la trace en comparant ce nombre avec le nombre de TS dans la trace.
	int m_nbPings;
};
