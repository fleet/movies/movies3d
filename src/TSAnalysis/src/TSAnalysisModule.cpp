#include "TSAnalysis/TSAnalysisModule.h"

#include "M3DKernel/datascheme/Environnement.h"
#include "M3DKernel/datascheme/TransmitSignalObject.h"
#include "M3DKernel/datascheme/Transducer.h"
#include "M3DKernel/datascheme/SoftChannel.h"
#include "M3DKernel/M3DKernel.h"
#include "M3DKernel/datascheme/HacObjectMgr.h"

#include "BaseMathLib/SignalUtils.h"
#include "BaseMathLib/FFT.h"

#include "TSAnalysis/TSTrack.h"

#include <algorithm>
#include <float.h>

#include "Calibration/CalibrationModule.h"

namespace
{
	constexpr const char * LoggerName = "TSAnalysisModule.TSAnalysisModule";
}

TSAnalysisModule::TSAnalysisModule()
	: ProcessModule("TS Analysis Module")
	, m_pCalibrationModule(nullptr)
{
	setEnable(true);
}

TSAnalysisModule::~TSAnalysisModule()
{
	ClearAll();
}

struct SingleTargetTuple {
	SingleTargetData * pTarget;
	BaseMathLib::Vector3D position;
	BaseMathLib::Vector3D gpsPosition;
	SoftChannel * pSoftChannel;
	Transducer * pTransducer;
};

struct TSCandidateCW {
	double m_ifin;
	SingleTargetDataCW data;
};

struct TSCandidateFM {
	double m_ifin;
	SingleTargetDataFM data;
};

struct ts_candidate_cw_decreasing_sorter
{
	inline bool operator() (const TSCandidateCW& struct1, const TSCandidateCW& struct2)
	{
		return (struct1.data.m_compensatedTS > struct2.data.m_compensatedTS);
	}
};

// Traitement du pingFan ajout�
void TSAnalysisModule::PingFanAdded(PingFan *p)
{
	if (getEnable())
	{
		if (m_parameter.GetDetectionEnabled())
		{
			///////////////////////////////
			// DETECTION
			///////////////////////////////

			// indices de boucle
			unsigned int iChannel = 0;// channel
			unsigned int iTrans = 0;// transducteur

			Sounder *pSounder = p->m_pSounder;
			MemorySet *pMemSet = p->GetMemorySetRef();

			double TSThreshold = m_parameter.GetTSThreshold();
			double minEchoLength = m_parameter.GetMinEchoLength();
			double maxEchoLength = m_parameter.GetMaxEchoLength();
			double maxGainComp = m_parameter.GetMaxGainComp();
			double phaseDev = m_parameter.GetPhaseDev();
			double minEchospace = m_parameter.GetMinEchospace();
			double minEchoDepth = m_parameter.GetMinEchoDepth();
			double maxEchoDepth = m_parameter.GetMaxEchoDepth();
			double distPrevPost = m_parameter.GetSpectrumDistPrevPost();

			double soundSpeed = pSounder->m_soundVelocity;

			HacObjectMgr * pHacObjectMgr = M3DKernel::GetInstance()->getObjectMgr();
			BeamDataObject *beamData = pHacObjectMgr->GetBeamDataObject();
			PingFanContainer & pingFanContainer = pHacObjectMgr->GetPingFanContainer();
						
			TimeObjectContainer * pEnvContainer = pHacObjectMgr->GetEnvironnementContainer();
			MovObjectPtr<Environnement> pEnv = (Environnement*)pEnvContainer->GetLastDatedObject();
			if (!pEnv || pEnv->GetNumberOfMeasure() == 0)
			{
				M3D_LOG_WARN(LoggerName, "No environmental data for TS analysis, use default environment values");
				pEnv = MovObjectPtr<Environnement>::make();

				const auto & kernelParameters = M3DKernel::GetInstance()->GetRefKernelParameter();

				Measure meas;
				meas.Temperature = kernelParameters.getTemperature();
				meas.SpeedOfSound = kernelParameters.getSpeedOfSound();
				meas.Salinity = kernelParameters.getSalinity();
				pEnv->AddMeasure(meas);
			}
			
			const auto & calibrationParameter = m_pCalibrationModule->GetCalibrationParameter();

			// OTK - remarque : finalement on ne les supprime plus ici, mais on les ignore au moment de la lecture : en effet, 
			// si un ping 10090 arrive apr�s la fin des tuples pings d'un fan, on arrive ici pour traiter le fan alors
			// que la single target n'a pas encore �t� ajout�e au ping : elle est donc ignor�e par le module et non supprim�e.

			// Suppression des �ventuelles single targets provenant du flux HAC d'entr�e (qu'on �crase par les r�sultats du module
			// lorsque celui-ci est activ�)
			/*PingFanSingleTarget* pDeleteSingleTargets = pingFanContainer.GetPingFanSingleTarget(p->m_ObjectTime, pSounder->m_SounderId);
			pingFanContainer.GetSingleTargetContainer()->RemoveObject(pDeleteSingleTargets, pSounder->m_SounderId);*/

			//**************************
			// Boucle Transducteurs
			//**************************
			for (iTrans = 0; iTrans < pMemSet->GetMemoryStructCount(); iTrans++)
			{
				// On r�cup�re l'espacement entre les �chos
				Transducer *pTrans = pSounder->GetTransducer(iTrans);
				double beamsSamplesSpacing = pTrans->getBeamsSamplesSpacing();
										
				double fe = 1 / (pTrans->m_timeSampleInterval / 1000000.0);

				// On r�cup�re la structure de donn�es associ�e au pingFan
				MemoryStruct *pMem = pMemSet->GetMemoryStruct(iTrans);
				BaseMathLib::Vector2I size = pMem->GetDataFmt()->getSize();

				// Si on n'a pas les donn�es de phase, on ne peut rien faire.
				if (!pMem->GetPhase())
				{
					continue;
				}

				double NechP = pTrans->m_pulseDuration / pTrans->m_timeSampleInterval;


				//**************************
				// Boucle Channels
				//**************************
				for (iChannel = 0; iChannel < (unsigned int)size.x; iChannel++)
				{
					SoftChannel * pSoftChan = pTrans->getSoftChannelPolarX(iChannel);

					const auto calibrationData = calibrationParameter.getCalibrationDataFM(pSoftChan->m_transNameShort);

					unsigned short chanId = pSoftChan->getSoftwareChannelId();

					TransmitSignalObject * pTransmitSignal = pSoftChan->m_softChannelComputeData.GetTransmitSignalObject(pTrans, pSoftChan);
					const double pulseLength = ((double)pTrans->m_pulseDuration) / 1000000;
					double effectivePulseLength = pulseLength;
					if (pTransmitSignal != NULL && pTransmitSignal->IsValid())
					{
						effectivePulseLength = pTransmitSignal->GetEffectivePulseLength();
					}

					const double psi = pSoftChan->m_beamEquTwoWayAngle;
					const double sv2ts = 10 * log10(soundSpeed*effectivePulseLength / 2.0) + psi + 2 * pSoftChan->m_calibBeamSACorrection;
					const double alpha = pSoftChan->m_absorptionCoef*0.0000001;
					const auto f1 = pSoftChan->m_startFrequency;
					const auto f2 = pSoftChan->m_endFrequency;
					const auto frequencyCenter = (f1 + f2) / 2.0;
					const double lambdaCenter = soundSpeed / frequencyCenter;
					const double gainCenter = pSoftChan->m_beamGain + 20. * log10(frequencyCenter / pSoftChan->m_acousticFrequency);
					const double psiCenter = psi - 20 * log10(frequencyCenter / pSoftChan->m_acousticFrequency);
					const double transmitPower = pSoftChan->m_transmissionPower;
					
					// R�cup�ration donn�es �cho de chaque faisceau
					DataFmt * pStart = pMem->GetDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, 0));
					DataFmt * pStartAngle = pMem->GetAngleDataFmt()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, 0));
					Phase * pStartPhase = pMem->GetPhase()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, 0));

					// r�cup�ration de la profondeur du fond verticale pour chaque channel
					std::uint32_t echoFondEval = 0;
					std::int32_t bottomRange = 0;
					bool found = false;
					p->getBottom(chanId, echoFondEval, bottomRange, found);

					// Si pas de fond, on va voir jusqu'au dernier �cho
					if (!found)
					{
						echoFondEval = (unsigned int)size.y;
					}
					// Longueur du tir (repris de l'algo de d�tection du fond BottomDetectionContourLine)
					unsigned int decTir = (unsigned int)(4 * ceil(pTrans->m_pulseDuration / pTrans->m_timeSampleInterval));

					//*********************************************************************
					// Boucle sur les �chos et calculs des diff�rentes variables de calcul
					//*********************************************************************

					const double decAlongAngle = pSoftChan->m_calibMainBeamAlongSteeringAngleRad;
					const double decAthwartAngle = pSoftChan->m_calibMainBeamAthwartSteeringAngleRad;

					// Calcul du range min et max de d�tection : on interpole, comme pour l'algo FixedBottom
					double depth1 = pSounder->GetPolarToWorldCoord(p, iTrans, iChannel, 0).z;
					double depth2 = pSounder->GetPolarToWorldCoord(p, iTrans, iChannel, echoFondEval - 1).z;
					unsigned int minEchoIndex, maxEchoIndex;
					if (depth1 != depth2)
					{
						double depthRatio = (double)(echoFondEval - 1) / (depth2 - depth1);
						double echoIndex1 = depthRatio*(minEchoDepth - depth1);
						double echoIndex2 = depthRatio*(maxEchoDepth - depth1);
                        double minEchoIndexDb = std::min(echoIndex1, echoIndex2);
                        double maxEchoIndexDb = std::max(echoIndex1, echoIndex2);

						// On ram�ne les num�ros d'�cho dans une plage correcte
                        minEchoIndexDb = std::max(minEchoIndexDb, .0);
                        minEchoIndexDb = std::min(minEchoIndexDb, (double)echoFondEval - 1);

                        maxEchoIndexDb = std::max(maxEchoIndexDb, .0);
                        maxEchoIndexDb = std::min(maxEchoIndexDb, (double)echoFondEval - 1);

						minEchoIndex = (unsigned int)floor(minEchoIndexDb);
						maxEchoIndex = (unsigned int)ceil(maxEchoIndexDb);
					}
					else
					{
						// Faisceau horizontal : 
						if (depth1 >= minEchoDepth && depth1 <= maxEchoDepth)
						{
							// on prend tout
							minEchoIndex = 0;
							maxEchoIndex = echoFondEval - 1;
						}
						else
						{
							// on ne prend rien
							minEchoIndex = -1;
							maxEchoIndex = -1;
						}
					}

					if (pTrans->m_pulseShape == 2) // if FM
					{
						double signalSquaredNorm = pTransmitSignal->GetSignalSquaredNorm();

						int nPrevPost = ceil(distPrevPost / (soundSpeed / 2. / fe) * 1.25);
						int Ndf = 2 * nPrevPost + 1;
						int Nfft = pow(2, ceil(log2(Ndf)));
						
						//Hanning Window on sur 2*0.2 (a bit different than the classic hann window)
						float nShapingSamples = floor(0.4 / 2.0 * Ndf);
						const std::vector<float> windowFunction = hanning2(nShapingSamples);
						std::vector<float> shapingWindow(Ndf, 1);
						memcpy(&shapingWindow[0], &windowFunction[0], nShapingSamples * sizeof(float));
						memcpy(&shapingWindow[Ndf - nShapingSamples], &windowFunction[nShapingSamples], nShapingSamples * sizeof(float));

						//frequencies indexes
						auto f2per = f2 % (std::int32_t(fe));
						auto f1per = f1 % (std::int32_t(fe));
						std::int32_t nper = floor(f1 / fe);
						std::vector<std::pair<double, int>> freq(Nfft);

						auto freqInterval = fe / Nfft;

						if (f1per < f2per)
						{
							for (auto i = 0; i < Nfft; ++i)
							{
								freq[i] = std::make_pair((i * freqInterval) + (nper * fe), i);
							}
						}
						else
						{
							int mid = 0;
							std::vector<int> index;
							for (auto i = 0; i < Nfft; ++i)
							{
								const double f = i * fe / Nfft;
								freq[i] = std::make_pair(f, i);
								if (f < f1per && f > f2per) {
									index.push_back(i);
									mid += 1;
								}
							}
							mid = ceil(mid * 0.5);
							const int midIdx = index[mid] - 1;

							for (int i = 0; i < midIdx; ++i)
							{
								freq[i].first += (nper + 1) * fe;
							}

							for (int i = midIdx; i < Nfft; ++i)
							{
								freq[i].first += nper * fe;
							}
						}

						sort(freq.begin(), freq.end(), [](const std::pair<double, int>& v1, const std::pair<double, int>& v2) {return v1.first < v2.first; });

						int freqOffset = freq.begin()->second;

						std::vector<std::complex<float>> MFfilt;
						xcorr(pTransmitSignal->GetFilt(), MFfilt);

						int imed = ceil(MFfilt.size() / 2);

						int idx1 = imed - floor((Ndf - 1) / 2);
						int idx2 = imed + floor(Ndf / 2);

						std::vector<std::complex<float>> fft_filt_complex(Nfft);
						for (int i = idx1; i <= idx2; ++i)
						{
							int j = i - idx1;
							fft_filt_complex[j] = MFfilt[i] * shapingWindow[j];
						}
						FFTComputer::forward(fft_filt_complex);

						const int fft_filt_size = fft_filt_complex.size();
						std::vector<float> fft_filt(fft_filt_size);
						for (int i = 0; i < fft_filt_size; ++i)
						{
							fft_filt[i] = sqrt(std::abs(fft_filt_complex[i]));
						}

						std::vector<double> dsp_Pt(fft_filt_size);
						for (int i = 0; i < fft_filt_size; ++i)
						{
							auto iF = freq[i].second;
							dsp_Pt[iF] = transmitPower * pow(fft_filt[iF] * signalSquaredNorm, 2.0) / (fe * fe * pulseLength);
						}

						std::vector<TSCandidateFM> laureatesFM;

						std::map<int, double> ind_level;
						std::vector<int> ind_loc_max;


						//int irmin = max(nPrevPost, (int)ceil(T / fe));

						std::vector<double> Sp_MF(maxEchoIndex, 0.0);
						for (unsigned int iEcho = minEchoIndex + nPrevPost; iEcho <= maxEchoIndex-nPrevPost; iEcho++)
						{
							// On calcule la valeur de l echo en DB
							double echoDB = *(pStart + iEcho);

							// On prend en compte l'�ventuel filtre
							char *pFilter = pMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, iEcho));
							if (*pFilter)
								echoDB = UNKNOWN_DB;

							echoDB = echoDB / 100.0;

							// Calcul du range de l'�cho
							double ts_range = std::max<double>(beamsSamplesSpacing, (iEcho + pTrans->GetSampleOffset())*beamsSamplesSpacing);

							const double r = iEcho * soundSpeed * 0.5 / fe;
                            const double LogR2 = std::max(.0, 20.*log10(r));
							//double LogR2 = max(0, 20.*log10(ts_range));
							const double Sp_MF_Echo = echoDB + LogR2 + 10 * log10(soundSpeed*effectivePulseLength / 2) + psiCenter;							
							Sp_MF[iEcho] = Sp_MF_Echo;
							if (Sp_MF_Echo >= (TSThreshold - 2 * maxGainComp))
							{
								ind_level[iEcho] = Sp_MF_Echo;
							}
						}

						while (!ind_level.empty())
						{
							// local maximum (find the strongest and cancel close ones, recursively)
							auto sp_max_pair = std::max_element
							(
								std::begin(ind_level), std::end(ind_level),
								[](const std::pair<int, double> & p1, const std::pair<int, double>& p2) {return p1.second < p2.second; }
							);
						
							const int iEcho = sp_max_pair->first;
							const int i1 = (iEcho - nPrevPost) < (minEchoIndex + nPrevPost) ? (minEchoIndex + nPrevPost) : (iEcho - nPrevPost);
							const int i2 = (iEcho + nPrevPost) > (maxEchoIndex - nPrevPost) ? (maxEchoIndex - nPrevPost) : (iEcho + nPrevPost);
														
							bool isLocalMaxima = true;

							for (int iMax = i1; iMax < i2; ++iMax)
							{
								if (Sp_MF[iMax] > sp_max_pair->second)
								{
									if (iMax != iEcho)
									{
										isLocalMaxima = false;
										break;
									}
								}
							}
							
							// remove from candidates
							ind_level.erase(iEcho);
							
							if (isLocalMaxima)
							{
								// add maxima
								ind_loc_max.push_back(iEcho);

								//reject detection
								for (int iMax = i1; iMax < iEcho; ++iMax)
								{
									auto it = ind_level.find(iMax);
									if (it != ind_level.end())
									{
										ind_level.erase(it);
									}
								}

								for (int iMax = iEcho + 1; iMax < i2; ++iMax)
								{
									auto it = ind_level.find(iMax);
									if (it != ind_level.end())
									{
										ind_level.erase(it);
									}
								}
							}
						}

						// Deuxieme boucle apres loc max
						for (int iEcho : ind_loc_max)
						{
							// On calcule la valeur de l echo en DB
							double echoDB = *(pStart + iEcho);
							const Phase echoPhase = *(pStartPhase + iEcho);

							// On prend en compte l'�ventuel filtre
							char *pFilter = pMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, iEcho));
							if (*pFilter)
								echoDB = UNKNOWN_DB;

							echoDB = echoDB / 100.0;

							// Calcul du range de l'�cho
							double ts_range = std::max<double>(beamsSamplesSpacing, (iEcho + pTrans->GetSampleOffset())*beamsSamplesSpacing);
							double r = iEcho * soundSpeed * 0.5 / fe;

							std::vector<std::complex<float>> spectrum(Nfft);
							std::vector<double> vectAlong(Ndf, 0.);
							std::vector<double> vectAthwart(Ndf, 0.);

							int zeros_pre = 0, zeros_post = 0;
							/*
							if (iEcho < nPrevPost)
							{
								zeros_pre = iEcho - nPrevPost;
							}
							if (echoFondEval - iEcho < nPrevPost)
							{
								zeros_post = ;
								//zeros_post = nPrevPost - (echoFondEval - iEcho);
							}
							*/
							//int jEcho = iEcho - (nPrevPost - zeros_pre);
							int iSpectrum = 0;
							int jEchoStart = iEcho - nPrevPost;
							if (jEchoStart < 0)
								jEchoStart = 0;

							int jEchoEnd = iEcho + nPrevPost;
							if (jEchoEnd > echoFondEval)
								jEchoEnd = echoFondEval;

							for (int jEcho = jEchoStart; jEcho <= jEchoEnd; ++jEcho)
							{
								const double r = jEcho * soundSpeed * 0.5 / fe;
								//double r = std::max<double>(beamsSamplesSpacing, (iEcho + pTrans->GetSampleOffset())*beamsSamplesSpacing);
                                const double tvg20 = std::max(.0, 20. * log10(r) + 2. * pEnv->GetMeasure(0)->ComputeAbsorptionCoefficient(frequencyCenter)*r);
								const double svMF = *(pStart + jEcho) / 100.;
								const double phaseMF = DEG_TO_RAD( *(pStartAngle + jEcho) ) / 100.;

								double powerMF = svMF - tvg20
									+ 10.* log10(transmitPower * pow(lambdaCenter, 2) * soundSpeed / (32.*pow(PI, 2)))
									+ 2. * gainCenter
									+ 10.* log10(effectivePulseLength)
									+ psiCenter;
																								
								powerMF = pow(10., powerMF / 20.);
								const double powerMF_realPart = powerMF * cos(phaseMF);
								const double powerMF_imagPart = powerMF * sin(phaseMF);

								spectrum[iSpectrum] = std::complex<float>(powerMF_realPart, powerMF_imagPart) * shapingWindow[iSpectrum];
							
								// Calcul du TS
								const Phase & jEchoPhase = *(pStartPhase + jEcho);

								vectAlong[iSpectrum] = jEchoPhase.GetAlong();
								vectAthwart[iSpectrum] = jEchoPhase.GetAthwart();

								++iSpectrum;
							}
							
							const auto minmax_along= std::minmax_element(begin(vectAlong), end(vectAlong));
							const auto minmax_athwart = std::minmax_element(begin(vectAthwart), end(vectAthwart));

							const auto al_min = *minmax_along.first;
							const auto al_max = *minmax_along.second;
							const auto ath_min = *minmax_athwart.first;
							const auto ath_max = *minmax_athwart.second;
							
							if ((al_max - al_min) < phaseDev && (ath_max - ath_min) < phaseDev)
							{
								FFTComputer::forward(spectrum);

								for (int i = 0; i < Nfft; ++i)
								{
									spectrum[i] /= fft_filt[i];
								}

								auto firstFreqIt = std::find_if(freq.cbegin(), freq.cend(), [&f1, &f2](const std::pair<double, int>& value) { return value.first >= f1 && value.first <= f2; });
								auto lastFreqIt  = std::find_if(freq.crbegin(), freq.crend(), [&f1, &f2](const std::pair<double, int>& value) { return value.first >= f1 && value.first <= f2; }); //reverse iterators to take the last value
								assert(lastFreqIt != freq.rend());

								int nbFreq = lastFreqIt.base() - firstFreqIt;
								std::vector<double> vectTSu(nbFreq, std::nan(""));
								std::vector<double> vectTS(nbFreq, std::nan(""));
								std::vector<double> vectFreq(nbFreq, std::nan(""));

								bool overThreshold = false;
								int i = 0;
								for (auto itFreq = firstFreqIt; (itFreq != lastFreqIt.base()); ++itFreq)
								{
									double f = itFreq->first;
									int iF = itFreq->second;
								
									double gainCalib = m_pCalibrationModule->getInterpolatedCalibrationGain(calibrationData, pSoftChan, f);
									//double gainHac = pSoftChan->m_beamGain;

									//double gain = pSoftChan->m_beamGain + 20 * log10(f / pSoftChan->m_acousticFrequency);

									const double absorptionCoefficients = pEnv->GetMeasure(0)->ComputeAbsorptionCoefficient(f);
									const double lambda = pow(soundSpeed / f, 2.);

									double TSu = 20. * log10(std::abs(spectrum[iF]))
										- 10. * log10(fe * fe * pulseLength)
										+ 40. * log10(r)
										+ 2.  * absorptionCoefficients * r
										- 10. * log10(dsp_Pt[iF] * lambda / (16. * PI*PI))
										- 2. * gainCalib;

									if (TSu > TSThreshold)
										overThreshold = true;

									double fratio = pSoftChan->m_acousticFrequency / f;
									double x = 2.0*(echoPhase.GetAlong() - decAlongAngle) / (pSoftChan->m_calibBeam3dBWidthAlongRad * fratio);
									double y = 2.0*(echoPhase.GetAthwart() - decAthwartAngle) / (pSoftChan->m_calibBeam3dBWidthAthwartRad * fratio);
									double x2 = x * x;
									double y2 = y * y;

									vectTSu[i] = TSu;
									vectTS[i] = 6.0206*(x2 + y2 - 0.18*x2*y2) + TSu;
									vectFreq[i] = f;
									++i;
								}

								if (overThreshold)
								{
									// Ajout du TS � la liste des candidats
									TSCandidateFM laureate;
									laureate.m_ifin = iEcho;
									laureate.data.m_compensatedTS = vectTS;
									laureate.data.m_unCompensatedTS = vectTSu;
									laureate.data.m_targetRange = ts_range;
									laureate.data.m_AlongShipAngleRad = echoPhase.GetAlong();
									laureate.data.m_AthwartShipAngleRad = echoPhase.GetAthwart();
									laureate.data.m_freq = vectFreq;

									// Median TS value
									auto sortedTS = vectTS;
									int med_pos = sortedTS.size() / 2;
									std::nth_element(sortedTS.begin(), sortedTS.begin() + med_pos, sortedTS.end());
									laureate.data.m_medianTS = sortedTS[med_pos];

									laureatesFM.push_back(laureate);
								}
							}
						}

						// Ajout des single tragets d�tect�es au conteneur appropri�
						if (!laureatesFM.empty())
						{
							SingleTargetDataObject *pMyObject = SingleTargetDataObject::Create();
							MovRef(pMyObject);

							pMyObject->m_parentSTId = pSoftChan->getSoftwareChannelId();
							pMyObject->m_PingTime = p->m_ObjectTime;
							pMyObject->m_pingNumber = p->m_computePingFan.m_filePingId;
							pMyObject->m_selectStartRange = std::max<double>(beamsSamplesSpacing, (minEchoIndex + pTrans->GetSampleOffset())*beamsSamplesSpacing);
							pMyObject->m_selectEndRange = std::max<double>(beamsSamplesSpacing, (maxEchoIndex + pTrans->GetSampleOffset())*beamsSamplesSpacing);
							pMyObject->m_detectedBottom = bottomRange*0.001;

							for (size_t i = 0; i < laureatesFM.size(); i++)
							{
								pMyObject->PushSingleTargetDataFM(laureatesFM[i].data);
							}

							pMyObject->m_tupleAttributes = 1;
							SplitBeamPair ref;
							bool found = pHacObjectMgr->GetChannelParentSplitBeam(pMyObject->m_parentSTId, ref);
							if (!found)
							{
								M3D_LOG_WARN(LoggerName, Log::format("TSAnalysis Module : Creating new Default single Target Alg for channel %d", pMyObject->m_parentSTId));
								SplitBeamParameter *pAlg = SplitBeamParameter::Create();
								pAlg->m_splitBeamParameterChannelId = pMyObject->m_parentSTId;
								pAlg->m_minimumValue = TSThreshold;
								pAlg->m_minimumEchoLength = minEchoLength;
								pAlg->m_maximumEchoLenght = maxEchoLength;
								pAlg->m_maximumGainCompensation = maxGainComp;
								pAlg->m_maximumPhaseCompensation = phaseDev;

								MovRef(pAlg);
								pHacObjectMgr->AddSplitBeamParameter(pAlg, pMyObject->m_parentSTId);
								MovUnRefDelete(pAlg);
								pHacObjectMgr->GetChannelParentSplitBeam(pMyObject->m_parentSTId, ref);
							}

							pMyObject->SetSounderRef(pSounder);
							pingFanContainer.PushSingleTarget(pMyObject);

							MovUnRefDelete(pMyObject);
						}
					}
					else
					{
						std::vector<double> Plike_mat(echoFondEval);
						std::vector<double> vectTSu(echoFondEval);
						std::vector<double> vectTS(echoFondEval);
						std::vector<double> vectTS_range(echoFondEval);
						for (unsigned int iEcho = 0; iEcho < echoFondEval; iEcho++)
						{
							// On calcule la valeur de l echo en DB
							double echoDB = *(pStart + iEcho);

							// On prend en compte l'�ventuel filtre
							char *pFilter = pMem->GetFilterFlag()->GetPointerToVoxel(BaseMathLib::Vector2I(iChannel, iEcho));
							if (*pFilter)
								echoDB = UNKNOWN_DB;

							echoDB = echoDB / 100.0;
							// Calcul du range de l'�cho
							double ts_range = std::max<double>(beamsSamplesSpacing, (iEcho + pTrans->GetSampleOffset())*beamsSamplesSpacing);

							// Calcul du TSu
							double TSu = echoDB + 20.0*log10(ts_range) + sv2ts;

							// Calcul du TS
							const Phase & echoPhase = *(pStartPhase + iEcho);
							double x = 2.0*(echoPhase.GetAlong() - decAlongAngle) / pSoftChan->m_calibBeam3dBWidthAlongRad;
							double y = 2.0*(echoPhase.GetAthwart() - decAthwartAngle) / pSoftChan->m_calibBeam3dBWidthAthwartRad;
							double x2 = x * x;
							double y2 = y * y;

							// Valeurs gard�es en m�moire
							vectTS_range[iEcho] = ts_range;
							vectTSu[iEcho] = TSu;
							vectTS[iEcho] = TSu + 6.0206*(x2 + y2 - 0.18*x2*y2);
							Plike_mat[iEcho] = TSu - 40.0*log10(ts_range) - 2.0*alpha*ts_range;
						}

						// Conteneur des candidats pour le ping pour le channel courant
						std::vector<TSCandidateCW> candidates;

						//*********************************************************************
						// Test des diff�rentes conditions pour la d�tection
						//*********************************************************************

						// Condition sur la position de l'�cho apr�s le tir et avant la d�tection du fond
						// ----------------------------------------------------------------------------------
						for (unsigned int iEcho = decTir + 1; iEcho < echoFondEval; iEcho++)
						{
							double TS = vectTS[iEcho];

							// Condition sur la profondeur
							// ----------------------------------
							if (iEcho >= minEchoIndex && iEcho <= maxEchoIndex)
							{
								// Condition sur le seuil de TS
								// -----------------------------------
								if (TS > TSThreshold)
								{
									// Condition sur Plike_mat
									// -----------------------------------
									double Plike = Plike_mat[iEcho];
									// rmq. test pour g�rer le cas o� pas de fond d�tect� : on va jusqu'� la fin du ping, dans ce cas iEcho+1 d�passe
									// la taille de PLike_mat : dans ce cas on ne valide pas la condition (la toute fin de ping sans fond d�tect� n'est
									// a priori pas int�ressante de toute fa�on).
									if (Plike >= Plike_mat[iEcho - 1] && (iEcho + 1 < echoFondEval && Plike >= Plike_mat[iEcho + 1]))
									{
										// Condition sur la longueur � -6dB
										// -----------------------------------
										int iSup, iInf;
										double pLikeThreshold = Plike - 6;
										for (iSup = iEcho; iSup < (int)echoFondEval; iSup++)
										{
											if (Plike_mat[iSup] < pLikeThreshold)
											{
												// On revient sur l'�cho pr�c�dent car l'�cho � plus de - 6dB ne doit pas �tre consid�r� pour le calcul de la longueur
												iSup = iSup - 1;
												break;
											}
										}
										// Si on n'a pas trouv� on revient sur le dernier �cho
                                        iSup = std::min((int)echoFondEval - 1, iSup);
										for (iInf = iEcho; iInf >= 0; iInf--)
										{
											if (Plike_mat[iInf] < pLikeThreshold)
											{
												// On revient sur l'�cho pr�c�dent car l'�cho � plus de - 6dB ne doit pas �tre consid�r� pour le calcul de la longueur
												iInf = iInf + 1;
												break;
											}
										}
										// Si on n'a pas trouv� on revient sur le premier �cho
                                        iInf = std::max(0, iInf);
										double L6dB = iSup - iInf + 1;
										if (L6dB >= (int)(NechP*minEchoLength + 0.5) && L6dB <= (int)(NechP*maxEchoLength + 0.5))
										{
											// Condition sur la compensation de directivit�
											// ----------------------------------------------
											if (TS - vectTSu[iEcho] <= maxGainComp*2.0)
											{
												// Condition sur l'�cart type des phases
												// ----------------------------------------------
												int pulseSemiWidth = (int)(0.5 + 0.5*NechP);
												double alStdDev, athStdDev;
												ComputeStdDevs(pStartPhase,
													// On se prot�ge de d�passement �ventuels
													(pulseSemiWidth > (int)iEcho ? 0 : iEcho - pulseSemiWidth),
													(iEcho + pulseSemiWidth >= echoFondEval ? echoFondEval - 1 : iEcho + pulseSemiWidth),
													pSoftChan->m_beamAlongAngleSensitivity*128.0 / PI,
													pSoftChan->m_beamAthwartAngleSensitivity*128.0 / PI,
													alStdDev, athStdDev);
												if (alStdDev <= phaseDev && athStdDev <= phaseDev)
												{
													// Toutes les conditions sont remplies : derniers calculs !

													double i_fin_num = 0.0;
													double i_fin_denom = 0.0;
													for (int i = iInf; i <= iSup; ++i)
													{
														double tmp = pow(10, Plike_mat[i] / 10.0);
														i_fin_num += (double)i * tmp;
														i_fin_denom += tmp;
													}
													double i_fin = i_fin_num / i_fin_denom;
													double r_ini = vectTS_range[iEcho];
													double r_fin = (i_fin + pTrans->GetSampleOffset())*beamsSamplesSpacing;
													double tmp = 40.0*log10(r_fin / r_ini) + 2.0*alpha*(r_fin - r_ini);
													double tsu_fin = vectTSu[iEcho] + tmp;
													double ts_fin = TS + tmp;

													// Ajout du TS � la liste des candidats
													TSCandidateCW candidate;
													candidate.m_ifin = i_fin;
													candidate.data.m_freq = pSoftChan->m_acousticFrequency;
													candidate.data.m_compensatedTS = ts_fin;
													candidate.data.m_unCompensatedTS = tsu_fin;
													candidate.data.m_targetRange = r_fin;
													const Phase & echoPhase = *(pStartPhase + iEcho);
													candidate.data.m_AlongShipAngleRad = echoPhase.GetAlong();
													candidate.data.m_AthwartShipAngleRad = echoPhase.GetAthwart();
													candidates.push_back(candidate);
												}
											}
										}
									}
								} // fin si condition sur le seuil TS
							} // fin si condition sur la profondeur
						} // fin boucle sur les echos

						// Traitement des candidats d�tect�s

						// Tri des candidats dans l'ordre des TS d�croissants
						std::sort(candidates.begin(), candidates.end(), ts_candidate_cw_decreasing_sorter());

						std::vector<TSCandidateCW> laureates;
						for (size_t i = 0; i < candidates.size(); i++)
						{
							const TSCandidateCW & candidate = candidates[i];
							// On regarde si les candidats pr�c�dents (donc de TS plus fort) sont suffisamment �loign�s pour valider ou non le candidat
							bool bFarEnough = true;
							for (int j = (int)i - 1; j >= 0; j--)
							{
								if (abs(candidate.m_ifin - candidates[j].m_ifin) < minEchospace*NechP)
								{
									bFarEnough = false;
									break;
								}
							}
							if (bFarEnough)
							{
								laureates.push_back(candidate);
							}
						}

						// Ajout des single tragets d�tect�es au conteneur appropri�
						if (!laureates.empty())
						{
							SingleTargetDataObject *pMyObject = SingleTargetDataObject::Create();
							MovRef(pMyObject);

							pMyObject->m_parentSTId = pSoftChan->getSoftwareChannelId();
							pMyObject->m_PingTime = p->m_ObjectTime;
							pMyObject->m_pingNumber = p->m_computePingFan.m_filePingId;
							if (minEchoIndex != -1 && maxEchoIndex != -1)
							{
								pMyObject->m_selectStartRange = vectTS_range[minEchoIndex];
								pMyObject->m_selectEndRange = vectTS_range[maxEchoIndex];
							}
							else
							{
								pMyObject->m_selectStartRange = -1;
								pMyObject->m_selectEndRange = -1;
							}
							pMyObject->m_detectedBottom = bottomRange*0.001;

							for (size_t i = 0; i < laureates.size(); i++)
							{
								pMyObject->PushSingleTargetDataCW(laureates[i].data);
							}

							pMyObject->m_tupleAttributes = 1;
							SplitBeamPair ref;
							bool found = pHacObjectMgr->GetChannelParentSplitBeam(pMyObject->m_parentSTId, ref);
							if (!found)
							{
								M3D_LOG_WARN(LoggerName, Log::format("TSAnalysis Module : Creating new Default single Target Alg for channel %d", pMyObject->m_parentSTId));
								SplitBeamParameter *pAlg = SplitBeamParameter::Create();
								pAlg->m_splitBeamParameterChannelId = pMyObject->m_parentSTId;
								pAlg->m_minimumValue = TSThreshold;
								pAlg->m_minimumEchoLength = minEchoLength;
								pAlg->m_maximumEchoLenght = maxEchoLength;
								pAlg->m_maximumGainCompensation = maxGainComp;
								pAlg->m_maximumPhaseCompensation = phaseDev;

								MovRef(pAlg);
								pHacObjectMgr->AddSplitBeamParameter(pAlg, pMyObject->m_parentSTId);
								MovUnRefDelete(pAlg);
								pHacObjectMgr->GetChannelParentSplitBeam(pMyObject->m_parentSTId, ref);
							}

							pMyObject->SetSounderRef(pSounder);
							pingFanContainer.PushSingleTarget(pMyObject);

							MovUnRefDelete(pMyObject);
						}
					} // fin boucle sur les channels
				}
			} // fin boucle transducteur
		} // fin algorithme de d�tection activ�


		/////////////////////////////////////
		// TRACKING
		/////////////////////////////////////
		if (m_parameter.GetTrackingEnabled())
		{
			int nbHoles = m_parameter.GetMaxHoles();

			// R�cup�ration des �chos simples d�tect�s pour le ping
			Sounder *pSounder = p->m_pSounder;
			HacObjectMgr * pHacObjectMgr = M3DKernel::GetInstance()->getObjectMgr();
			PingFanContainer & pingFanContainer = pHacObjectMgr->GetPingFanContainer();
			PingFanSingleTarget* pSingleTargets = pingFanContainer.GetPingFanSingleTarget(p->m_ObjectTime, pSounder->m_SounderId);
			if (pSingleTargets)
			{
				// Positions des single targets
				std::vector<SingleTargetTuple> targetsTuples;

				for (unsigned int iTarget = 0; iTarget < pSingleTargets->GetNumberOfTarget(); iTarget++)
				{
					SingleTargetDataObject * pTarget = pSingleTargets->GetTarget(iTarget);
					// Si la target ne correspond pas � une channel du sondeur associ� au ping, on l'ignore
					// (peut arriver si plusieurs sondeurs pings en m�me temps)
					Transducer * pTrans = pSounder->getTransducerForChannel(pTarget->m_parentSTId);
					if (pTrans)
					{
						for (unsigned int iTargetData = 0; iTargetData < pTarget->GetSingleTargetDataCWCount(); iTargetData++)
						{
							SingleTargetDataCW & singleTarget = pTarget->GetSingleTargetDataCW(iTargetData);

							// calcul de la position
							SoftChannel * pSoftChan = pTrans->getSoftChannel(pTarget->m_parentSTId);
							if (pSoftChan)
							{
								SingleTargetTuple targetTuple;
								targetTuple.pTarget = &singleTarget;
								targetTuple.pSoftChannel = pSoftChan;
								targetTuple.pTransducer = pTrans;

								BaseMathLib::Vector3D softChanPos = singleTarget.GetTargetPositionSoftChannel();
								targetTuple.position = pSounder->GetSoftChannelCoordinateToWorldCoord(pTrans, pSoftChan, p, softChanPos);
								targetTuple.gpsPosition = pSounder->GetSoftChannelCoordinateToCartesianCoord(pTrans, pSoftChan, p, softChanPos);

								targetsTuples.push_back(targetTuple);
							}
						}
						for (unsigned int iTargetData = 0; iTargetData < pTarget->GetSingleTargetDataFMCount(); iTargetData++)
						{
							SingleTargetDataFM & singleTarget = pTarget->GetSingleTargetDataFM(iTargetData);

							// calcul de la position
							SoftChannel * pSoftChan = pTrans->getSoftChannel(pTarget->m_parentSTId);
							if (pSoftChan)
							{
								SingleTargetTuple targetTuple;
								targetTuple.pTarget = &singleTarget;
								targetTuple.pSoftChannel = pSoftChan;
								targetTuple.pTransducer = pTrans;

								BaseMathLib::Vector3D softChanPos = singleTarget.GetTargetPositionSoftChannel();
								targetTuple.position = pSounder->GetSoftChannelCoordinateToWorldCoord(pTrans, pSoftChan, p, softChanPos);
								targetTuple.gpsPosition = pSounder->GetSoftChannelCoordinateToCartesianCoord(pTrans, pSoftChan, p, softChanPos);

								targetsTuples.push_back(targetTuple);
							}
						}
					} // Fin si test transducteur de la single target correspond au pingfan
				} // Fin boucle sur les targets

				// On remonte les pings jusque n�cessaire pour apparairer les targets du ping courant aux tracks des pings pr�c�dents
				TimeObjectContainer * pPingsContainer = pingFanContainer.GetPingFanContainer()->GetSounderIndexedContainer(pSounder->m_SounderId);
				size_t currentPingIndex = pPingsContainer->GetObjectCount() - 1;
                int lastPingIndex = std::max(0, (int)currentPingIndex - 1 - nbHoles);
				int nbPingsSinceLastTarget = 1;
				for (int iPing = (int)currentPingIndex - 1; iPing >= lastPingIndex; iPing--)
				{
					PingFan * pPrevPing = (PingFan*)pPingsContainer->GetDatedObject(iPing);

					// Distance maximale entre deux �chos d'une track
					TimeElapse deltaT = p->m_ObjectTime - pPrevPing->m_ObjectTime;
					double dbMaxDistance = m_parameter.GetMaxSpeed() * (double)deltaT.m_timeElapse / 10000.0;

					bool bContinue = true;
					while (bContinue)
					{
						// Boucle sur les tracks
						double dbMinDistance = std::numeric_limits<double>::max();
						TSTrack * pBestTrack = NULL;
						SingleTargetTuple * pBestTarget = NULL;
						size_t iBestTarget = 0;
                        std::map<std::uint64_t, TSTrack*>::iterator iterTrack;
						for (iterTrack = m_mapWorkingTracks.begin(); iterTrack != m_mapWorkingTracks.end(); ++iterTrack)
						{
							TSTrack * pTrack = iterTrack->second;

							if (pTrack->GetLastPingID() == pPrevPing->GetPingId())
							{
								// Boucle sur les targets restant � affecter � une track
								for (size_t iTarget = 0; iTarget < targetsTuples.size(); iTarget++)
								{
									SingleTargetTuple & targetTuple = targetsTuples[iTarget];

									// On n'autorise pas les tracks � changer de transducteur
									if (targetTuple.pTransducer == pTrack->GetTransducer())
									{
										// Calcul de la distance entre la track et chaque cible restante
										double dbDst = targetTuple.position.distance(pTrack->GetLastPosition());

										// Test du crit�re de distance maximale en fonction de la vitesse maximal de la cible
										if (dbDst <= dbMaxDistance)
										{
											// Si c'est mieux que la pr�c�dente solution, on conserve celle-ci sous le coude
											if (dbDst < dbMinDistance)
											{
												pBestTrack = pTrack;
												pBestTarget = &targetTuple;
												dbMinDistance = dbDst;
												iBestTarget = iTarget;
											}
										}
									}
								}
							}
						}

						if (pBestTrack)
						{
							// appairement de la melleure solution trouv�e s'il y en a une.
							pBestTrack->Appendtrack(pBestTarget->pTarget, p->GetPingId(),
								pBestTarget->pSoftChannel->m_softChannelId, pBestTarget->position, pBestTarget->gpsPosition, nbPingsSinceLastTarget);
							pBestTarget->pTarget->m_trackLabel = pBestTrack->GetLabel();

							// suppression du target tuple du vector
							targetsTuples.erase(targetsTuples.begin() + iBestTarget);
						}
						else
						{
							bContinue = false;
						}
					} // Fin du while

					nbPingsSinceLastTarget++;

				} // Fin boucle sur les pings pr�c�dents

				// Initialisation des tracks correspondant aux targets restantes
				for (size_t iTarget = 0; iTarget < targetsTuples.size(); iTarget++)
				{
					SingleTargetTuple & targetTuple = targetsTuples[iTarget];

					// Cr�ation d'une nouvelle track
					TSTrack * pNewTrack = TSTrack::Create();
					MovRef(pNewTrack);
					pNewTrack->SetSounderID(pSounder->m_SounderId);
					pNewTrack->SetTransducer(targetTuple.pTransducer);
					m_mapTracks[pNewTrack->GetLabel()] = pNewTrack;
					m_mapWorkingTracks[pNewTrack->GetLabel()] = pNewTrack;

					// Appariement de la nouvelle track et de la target associ�e
					pNewTrack->Appendtrack(targetTuple.pTarget, p->GetPingId(),
						targetTuple.pSoftChannel->getSoftwareChannelId(), targetTuple.position, targetTuple.gpsPosition, 1);
					targetTuple.pTarget->m_trackLabel = pNewTrack->GetLabel();
				}

			} // Fin si on a des single targets pour le ping      

		} 

	} // fin si getEnabled()

	// Nettoyage de toutes les tracks sortie de la zone m�moire et mise � jour de la liste des tracks potentiellement non closes
	Clear(p);
}

bool TSAnalysisModule::IsValidTrack(TSTrack * pTrack) const
{
	return (int)pTrack->GetSingleTargets().size() >= m_parameter.GetMinEchoNumber()
		&& pTrack->GetMissingEchoesPercent() <= m_parameter.GetMaxHolesPercent();
}

void TSAnalysisModule::ComputeStdDevs(Phase * pPhaseStart, int iInf, int iSup, double alFactor, double athFactor,
	double & alStdDev, double & athStdDev)
{
	double alMean = 0.0, alSumDeviation = 0.0;
	double athMean = 0.0, athSumDeviation = 0.0;
	double dbN = 0;
	for (int i = iInf; i <= iSup; ++i)
	{
		const Phase & phase = *(pPhaseStart + i);
		alMean += phase.GetAlong()*alFactor;
		athMean += phase.GetAthwart()*athFactor;
		dbN += 1;
	}
	alMean = alMean / dbN;
	athMean = athMean / dbN;
	for (int i = iInf; i <= iSup; ++i)
	{
		const Phase & phase = *(pPhaseStart + i);
		alSumDeviation += (phase.GetAlong()*alFactor - alMean)*(phase.GetAlong()*alFactor - alMean);
		athSumDeviation += (phase.GetAthwart()*athFactor - athMean)*(phase.GetAthwart()*athFactor - athMean);
	}
	//rmq. on utilise la m�thode de "N-1" pour se conformer au code matlab (stdeva vs stdevpa dans excel par exemple)
	if (dbN > 1)
	{
		dbN = dbN - 1;
	}
	alStdDev = sqrt(alSumDeviation / dbN);
	athStdDev = sqrt(athSumDeviation / dbN);
}

// Nettoyage de toutes les tracks calcul�es et encore en m�moire
void TSAnalysisModule::ClearAll()
{
	m_mapWorkingTracks.clear();

    std::map<std::uint64_t, TSTrack*>::iterator iter;
	for (iter = m_mapTracks.begin(); iter != m_mapTracks.end(); ++iter)
	{
		MovUnRefDelete(iter->second);
	}
	m_mapTracks.clear();
}

// Nettoyage de toutes les tracks sortie de la zone m�moire et mise � jour de la liste des tracks potentiellement non closes
void TSAnalysisModule::Clear(PingFan * p)
{
	// On sort de m_mapWorkingTracks les tracks qui sont trop anciennes pour �tre complet�es
	TimeObjectContainer * pPingsContainer = M3DKernel::GetInstance()->getObjectMgr()->GetPingFanContainer().GetPingFanContainer()->GetSounderIndexedContainer(p->m_pSounder->m_SounderId);
	size_t currentPingIndex = pPingsContainer->GetObjectCount() - 1;
    int lastPingIndex = std::max(0, (int)currentPingIndex - 1 - m_parameter.GetMaxHoles());

    std::map<std::uint64_t, TSTrack*>::iterator iterTrack;
	for (iterTrack = m_mapWorkingTracks.begin(); iterTrack != m_mapWorkingTracks.end(); )
	{
		TSTrack * pTrack = iterTrack->second;

		// On ne nettoie les tracks que pour le sondeur associ� au pingfan courant
		if (p->m_pSounder->GetTransducerWithName(pTrack->GetTransducer()->m_transName))
		{
			bool bStillOpen = false;

			bool bLastPingFound = false;
			for (int iPing = (int)currentPingIndex; iPing >= lastPingIndex; iPing--)
			{
				PingFan * pOldFan = (PingFan*)pPingsContainer->GetDatedObject(iPing);
				if (pOldFan->GetPingId() == pTrack->GetLastPingID())
				{
					bLastPingFound = true;
					break;
				}
			}

			if (!bLastPingFound)
			{
				m_mapWorkingTracks.erase(iterTrack++);
			}
			else
			{
				++iterTrack;
			}
		}
		else
		{
			++iterTrack;
		}
	}

	// On sort et on d�truit les tracks dont le dernier pingfan est sorti de la m�moire
	PingFan * pOldestPing = (PingFan*)pPingsContainer->GetFirstDatedObject();
	for (iterTrack = m_mapTracks.begin(); iterTrack != m_mapTracks.end(); )
	{
		TSTrack * pTrack = iterTrack->second;

		bool bDelete = false;

		// On ne nettoie les tracks que pour le sondeur associ� au pingfan courant
		if (p->m_pSounder->GetTransducerWithName(pTrack->GetTransducer()->m_transName))
		{
			bDelete = !pOldestPing || pOldestPing->GetPingId() > pTrack->GetLastPingID();
		}

		if (bDelete)
		{
			m_mapTracks.erase(iterTrack++);
			// On supprime aussi de la map des workingTracks au cas o�
			m_mapWorkingTracks.erase(pTrack->GetLabel());
			MovUnRefDelete(pTrack);
		}
		else
		{
			++iterTrack;
		}
	}
}

// D�marrage d'un ESU
void TSAnalysisModule::ESUStart(ESUParameter * pWorkingESU)
{
}

// Fin de l'ESU en cours
void TSAnalysisModule::ESUEnd(ESUParameter * pWorkingESU, bool abort)
{
}

// changement des propri�t�s d'un sondeur
void TSAnalysisModule::SounderChanged(std::uint32_t sounderId)
{
	ClearAll();
}

void TSAnalysisModule::StreamClosed(const char *streamName)
{

}
void TSAnalysisModule::StreamOpened(const char *streamName)
{

}
