#include "TSAnalysis/TSAnalysisParameter.h"

// d�pendances
#include "M3DKernel/parameter/ParameterDataType.h"
#include "M3DKernel/config/MovConfig.h"
#include "Reader/ReaderCtrl.h"

using namespace BaseKernel;

// constructeur
TSAnalysisParameter::TSAnalysisParameter(void) : ParameterModule("TSAnalysisParameter", eUndefined)
{
	m_bDetectionEnabled = false;
	m_TSThreshold = -60.0;
	m_minEchoLength = 0.8f;
	m_maxEchoLength = 1.8f;
	m_maxGainComp = 6.0;
	m_phaseDev = 8.0;
	m_minEchoSpace = 1.0f;
	m_minEchoDepth = 0;
	m_maxEchoDepth = 300;
	m_distPrevPost = 0.15;

	m_bTrackingEnabled = false;
	m_MaxSpeed = 3;
	m_MaxHoles = 1;
	m_minEchoNumber = 2;
	m_maxHolesPercent = 0.5f;
	m_histogramMinValue = -60.f;
	m_histogramMaxValue = -40.f;
	m_histogramResolution = 5.f;
}

// Destructeur
TSAnalysisParameter::~TSAnalysisParameter(void)
{
}

void TSAnalysisParameter::SetDetectionEnabled(bool b)
{
	if (m_bDetectionEnabled != b)
	{
		m_bDetectionEnabled = b;
		ReaderCtrl::getInstance()->SetIgnoreSingleTargets(m_bDetectionEnabled);
	}
}

// sauvegarde des param�tres dans le fichier de configuration associ�
bool TSAnalysisParameter::Serialize(MovConfig * movConfig)
{
	// debut de la s�rialisation du module
	movConfig->SerializeData((ParameterModule*)this, eParameterModule);

	movConfig->SerializeData<bool>(m_bDetectionEnabled, eBool, "DetectionEnabled");
	movConfig->SerializeData<float>(m_TSThreshold, eFloat, "TSThreshold");
	movConfig->SerializeData<float>(m_minEchoLength, eFloat, "MinEchoLength");
	movConfig->SerializeData<float>(m_maxEchoLength, eFloat, "MaxEchoLength");
	movConfig->SerializeData<float>(m_maxGainComp, eFloat, "MaxGainComp");
	movConfig->SerializeData<float>(m_phaseDev, eFloat, "PhaseDev");
	movConfig->SerializeData<float>(m_minEchoSpace, eFloat, "MinEchoSpace");
	movConfig->SerializeData<float>(m_minEchoDepth, eFloat, "MinEchoDepth");
	movConfig->SerializeData<float>(m_maxEchoDepth, eFloat, "MaxEchoDepth");

	movConfig->SerializeData<bool>(m_bTrackingEnabled, eBool, "TrackingEnabled");
	movConfig->SerializeData<float>(m_MaxSpeed, eFloat, "MaxSpeed");
	movConfig->SerializeData<int>(m_MaxHoles, eInt, "MaxHoles");
	movConfig->SerializeData<int>(m_minEchoNumber, eInt, "MinEchoNumber");
	movConfig->SerializeData<float>(m_maxHolesPercent, eFloat, "MaxHolesPercent");

	movConfig->SerializeData<float>(m_histogramMinValue, eFloat, "HistogramMinValue");
	movConfig->SerializeData<float>(m_histogramMaxValue, eFloat, "HistogramMaxValue");
	movConfig->SerializeData<float>(m_histogramResolution, eFloat, "HistogramResolution");

	movConfig->SerializeData<float>(m_distPrevPost, eFloat, "DistPrevPost");

	// Sauvegarde des sondeurs
	movConfig->SerializeData((ParameterObject*)this, eParameterObject, "HistogramSounders");
    for(std::uint32_t sounderId : m_histogramSounders)
	{
		movConfig->SerializeData<std::uint32_t>(sounderId, eUInt, "SounderID");
	}
	movConfig->SerializePushBack(); // HistogramSounders

  // fin de la s�rialisation du module
	movConfig->SerializePushBack();

	return true;
}

// lecture des param�tres dans le fichier de configuration associ�
bool TSAnalysisParameter::DeSerialize(MovConfig * movConfig)
{
	bool result = movConfig->DeSerializeData((ParameterModule*)this, eParameterModule);
	
	// debut de la désérialisation du module
	if (result)
	{
		bool bDetectionEnabled;
		result = result && movConfig->DeSerializeData<bool>(&bDetectionEnabled, eBool, "DetectionEnabled");
		SetDetectionEnabled(bDetectionEnabled);
		result = result && movConfig->DeSerializeData<float>(&m_TSThreshold, eFloat, "TSThreshold");
		result = result && movConfig->DeSerializeData<float>(&m_minEchoLength, eFloat, "MinEchoLength");
		result = result && movConfig->DeSerializeData<float>(&m_maxEchoLength, eFloat, "MaxEchoLength");
		result = result && movConfig->DeSerializeData<float>(&m_maxGainComp, eFloat, "MaxGainComp");
		result = result && movConfig->DeSerializeData<float>(&m_phaseDev, eFloat, "PhaseDev");
		result = result && movConfig->DeSerializeData<float>(&m_minEchoSpace, eFloat, "MinEchoSpace");
		result = result && movConfig->DeSerializeData<float>(&m_minEchoDepth, eFloat, "MinEchoDepth");
		result = result && movConfig->DeSerializeData<float>(&m_maxEchoDepth, eFloat, "MaxEchoDepth");

		result = result && movConfig->DeSerializeData<bool>(&m_bTrackingEnabled, eBool, "TrackingEnabled");
		result = result && movConfig->DeSerializeData<float>(&m_MaxSpeed, eFloat, "MaxSpeed");
		result = result && movConfig->DeSerializeData<int>(&m_MaxHoles, eInt, "MaxHoles");
		result = result && movConfig->DeSerializeData<int>(&m_minEchoNumber, eInt, "MinEchoNumber");
		result = result && movConfig->DeSerializeData<float>(&m_maxHolesPercent, eFloat, "MaxHolesPercent");

		result = result && movConfig->DeSerializeData<float>(&m_histogramMinValue, eFloat, "HistogramMinValue");
		result = result && movConfig->DeSerializeData<float>(&m_histogramMaxValue, eFloat, "HistogramMaxValue");
		result = result && movConfig->DeSerializeData<float>(&m_histogramResolution, eFloat, "HistogramResolution");

		result = result && movConfig->DeSerializeData<float>(&m_distPrevPost, eFloat, "DistPrevPost");

		// Lecture des sondeurs    
		m_histogramSounders.clear();
		result = result && movConfig->DeSerializeData((ParameterObject*)this, eParameterObject, "HistogramSounders");
		if (result == true)
		{
			while (result == true)
			{
				std::uint32_t sounderID;
				result = movConfig->DeSerializeData<std::uint32_t>(&sounderID, eUInt, "SounderID") && result;
				if (result)
				{
					m_histogramSounders.push_back(sounderID);
				}
			}
			result = true;
			movConfig->DeSerializePushBack();
		}
	}

	// fin de la désérialisation du module
	movConfig->DeSerializePushBack();

	return result;
}
