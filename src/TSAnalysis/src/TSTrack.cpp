
#include "TSAnalysis/TSTrack.h"

// Initialis� � 1 car 0 = pas de track.
std::uint64_t TSTrack::m_labelCount = 1;

TSTrack::TSTrack()
{
	m_sounderId = 0;
	m_pTransducer = NULL;
	m_label = m_labelCount++;
	m_nbPings = 0;
}

TSTrack::~TSTrack()
{
}

Transducer * TSTrack::GetTransducer()
{
	return m_pTransducer;
}

void TSTrack::Appendtrack(SingleTargetData * pTS, std::uint64_t pingID, unsigned short softChanId,
	const BaseMathLib::Vector3D & position, const BaseMathLib::Vector3D & gpsPosition,
	int nbPings)
{
	m_Positions.push_back(position);
	m_GPSPositions.push_back(gpsPosition);
	m_PingPositions.push_back(pingID);
	m_SingleTargets.push_back(pTS);
	m_SoftChannels.push_back(softChanId);
	m_nbPings += nbPings;
}

double TSTrack::GetMissingEchoesPercent()
{
	return (double)(m_nbPings - (int)m_SingleTargets.size()) / m_nbPings;
}

std::uint32_t TSTrack::GetSounderID()
{
	return m_sounderId;
}

void TSTrack::SetSounderID(std::uint32_t sID)
{
	m_sounderId = sID;
}

void TSTrack::SetTransducer(Transducer * pTrans)
{
	m_pTransducer = pTrans;
}

std::uint64_t TSTrack::GetLastPingID()
{
	return m_PingPositions.back();
}

const BaseMathLib::Vector3D & TSTrack::GetLastPosition()
{
	return m_Positions.back();
}

std::uint64_t TSTrack::GetLabel()
{
	return m_label;
}

const std::vector<BaseMathLib::Vector3D> & TSTrack::GetPositions()
{
	return m_Positions;
}

const std::vector<BaseMathLib::Vector3D> & TSTrack::GetGPSPositions()
{
	return m_GPSPositions;
}

const std::vector<std::uint64_t> & TSTrack::GetPingPositions()
{
	return m_PingPositions;
}

const std::vector<SingleTargetData*> & TSTrack::GetSingleTargets()
{
	return m_SingleTargets;
}

const std::vector<unsigned short> & TSTrack::GetChannelIDs()
{
	return m_SoftChannels;
}

