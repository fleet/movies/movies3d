Unit test use boost framework.

To add a new test project :
* create a subdirectory for the test
* add a CMakeLists.txt (see templates below for example)

A reference dataset is available is _datas_ subdirectory. This dataset is composed of a sub part of PELGAS13 campaign.
The dataset is automaticaly downloaded during build process from https://www.seanoe.org/data/00475/58652/data/61154.zip

# Templates
replace _my_test_project_ with your test project name

## CMakeLists.txt skeleton
```
cmake_minimum_required(VERSION 3.12)

project(_my_test_project_ VERSION 0.0.1)

find_package(Boost COMPONENTS unit_test_framework REQUIRED)

add_executable(${PROJECT_NAME} main.cpp)

# TODO add Movies3D dependencies such as M3DKernel, ModuleManager, Reader, ....
target_link_libraries(${PROJECT_NAME} PUBLIC Boost::unit_test_framework)

add_test(NAME ${PROJECT_NAME} COMMAND $<TARGET_FILE:${PROJECT_NAME}>)
```


## main.cpp skeleton
```
#define BOOST_TEST_MODULE TestsEchoIntegration
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(_my_test_project_)
{
	// TODO add test code
}
```