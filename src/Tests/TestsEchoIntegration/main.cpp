#define BOOST_TEST_MODULE TestsEchoIntegration
#include <boost/test/unit_test.hpp>

#include "M3DKernel/M3DKernel.h"
#include "Reader/ReaderCtrl.h"
#include "Reader/MovReadService.h"
#include "ModuleManager/ModuleManager.h"

#include "EchoIntegration/EchoIntegrationModule.h"

#include <fstream>
#include <sstream>

namespace utils
{
	struct result
	{
		unsigned line;
		unsigned int index;
		unsigned int sounderId;
		unsigned int channelIdx;
		double freq;
		unsigned int layerIdx;
		double latitude;
		double longitude;
		double depth;
		unsigned int ni;	// sans unit�
		unsigned int nt;	// sans unit�
		unsigned int sa;	// en 100eme de db
		unsigned int sv;	// en 100eme de db

	};

	bool operator!=(const result & r1, const result & r2)
	{
		return r1.ni != r2.ni
			|| r1.nt != r2.nt
			|| r1.sa != r2.sa
			|| r1.sv != r2.sv
			/*
			|| r1.latitude != r2.latitude
			|| r1.longitude != r2.longitude
			|| r1.depth != r2.depth
			*/;
	}

	std::ostream& operator<<(std::ostream& s, const result& r)
	{
		s << "ni=" << r.ni << ", nt=" << r.nt << ", sa=" << r.sa << ", sv" << r.sv;
		return s;
	}

	template<typename T>
	std::istream& readField(std::istream& is, T& data, const char sep = ';')
	{
		std::string field; 
		std::getline(is, field, sep);

		std::istringstream iss(field);
		iss >> data;

		return is;
	}

	std::istream& readLine(std::istream& is, result & res)
	{
		readField(is, res.line);
		readField(is, res.index);
		readField(is, res.sounderId);
		readField(is, res.channelIdx);
		readField(is, res.freq);
		readField(is, res.layerIdx);
		readField(is, res.latitude);
		readField(is, res.longitude);
		readField(is, res.depth);
		readField(is, res.ni);
		readField(is, res.nt);
		readField(is, res.sa);
		readField(is, res.sv, '\n');
		return is;
	}
}

bool loadRefDatas(const std::string& filePath, std::vector<utils::result> & refDatas)
{
	const char* csv_sep = ";";

	std::ifstream ifs;
	ifs.open(filePath);

	// Read header
	if (ifs.good())
	{
		std::string headerLine;
		std::getline(ifs, headerLine);
	}
	else
	{
		return false;
	}

	while (ifs.good())
	{
		utils::result res;
		utils::readLine(ifs, res);
		refDatas.push_back(res);
	}

	return true;
}

void run_ei()
{
	auto moduleManager = CModuleManager::getInstance();

	/// START EI
	auto eiModule = moduleManager->GetEchoIntegrationModule();
	if (eiModule == nullptr)
		return;

	auto eiConsumer = eiModule->GetMatlabModuleConsumer();
	if (eiConsumer == nullptr)
		return;

	moduleManager->ChangeConfigurationCurrentDirectory(".", "config");
	moduleManager->LoadConfiguration({});
	eiModule->setEnable(true);

	/// LOAD SOURCE FILE
	auto readerCtrl = ReaderCtrl::getInstance();

	MovFileRun mFileRun;
	mFileRun.SetSourceName("../datas", ".hac");

	readerCtrl->OpenFileStream(mFileRun);
	readerCtrl->ReadSounderConfig();

	/// READ AND WRITE
	bool isClosed = readerCtrl->GetActiveService()->IsClosed();
	while (!isClosed)
	{
		readerCtrl->SyncReadChunk();
		ChunckEventList ref;
		readerCtrl->WaitEndChunck(ref);

		isClosed = readerCtrl->GetActiveService()->IsClosed();
	}

	eiModule->setEnable(false);
}

/// <summary>
/// Chargement des r�sultats de l'�cho-int�gration en fct des num�ros de lignes du fichiers de r�f�rence
/// </summary>
/// <param name="results"></param>
/// <param name="lineFilter"></param>
void collectResults(std::vector<utils::result>& results, const std::vector<utils::result>& lineFilter)
{
	auto moduleManager = CModuleManager::getInstance();
	auto eiModule = moduleManager->GetEchoIntegrationModule();
	if (eiModule == nullptr)
		return;

	auto eiConsumer = eiModule->GetMatlabModuleConsumer();
	if (eiConsumer == nullptr)
		return;

	auto currentLineFilter = lineFilter.cbegin();

	unsigned int line = 0;
	
	unsigned int count = eiModule->GetOutputContainer().GetOutPutCount(eiConsumer->GetConsumerId());
	for (unsigned int iCount = 0; iCount < count; iCount++)
	{
		EchoIntegrationOutput* pOut = (EchoIntegrationOutput*)eiModule->GetOutputContainer().GetOutPut(eiConsumer->GetConsumerId(), iCount);

		unsigned int sounderID = pOut->m_SounderID;
		unsigned int nbChannel = pOut->m_tabChannelResult.size();
		unsigned int nbLayer = eiModule->GetEchoIntegrationParameter().getLayersDefs().size();

		// Pour chaque faisceau
		for (unsigned int x = 0; x < nbChannel; x++)
		{
			// Pour chaque frequence
			for (unsigned int i = 0; i < pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies().size(); i++)
			{
				double freq = pOut->m_tabChannelResult[x]->m_tabFrequencies[i];
				// Pour chaque couche
				for (unsigned int j = 0; j < pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[i].size(); j++)
				{		
					if (line == currentLineFilter->line)
					{
						utils::result res;
						res.line = line;
						res.index = iCount;
						res.sounderId = sounderID;
						res.channelIdx = x;
						res.freq = freq;
						res.layerIdx = j;
						res.latitude = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[i][j]->GetLatitude();
						res.longitude = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[i][j]->GetLongitude();
						res.depth = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[i][j]->GetDepth();
						res.ni = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[i][j]->GetNi();
						res.nt = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[i][j]->GetNt();
						res.sa = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[i][j]->GetSa() * 100;
						res.sv = pOut->m_tabChannelResult[x]->getTabLayerResultFrequencies()[i][j]->GetSv() * 100;
						results.push_back(res);

						++currentLineFilter;
						if (currentLineFilter == lineFilter.cend())
						{
							return;
						}
					}
					++line;
				}
			}
		}
	}
}

BOOST_AUTO_TEST_CASE(EchoIntegration)
{
	// Chargement d'un sous-ensemble des donn�es de r�f�rence
	std::vector<utils::result> refDatas;
	loadRefDatas("./results/ref.csv", refDatas);
	BOOST_REQUIRE_GT(refDatas.size(), 0);

	run_ei();

	std::vector<utils::result> resDatas;
	collectResults(resDatas, refDatas);

	BOOST_REQUIRE_EQUAL_COLLECTIONS(refDatas.cbegin(), refDatas.cend(), resDatas.cbegin(), resDatas.cend());
}
