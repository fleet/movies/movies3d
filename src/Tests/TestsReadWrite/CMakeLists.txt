cmake_minimum_required(VERSION 3.12)

project(TestsReadWrite)

find_package(Boost COMPONENTS unit_test_framework REQUIRED)

add_executable(${PROJECT_NAME} main.cpp)
target_link_libraries(${PROJECT_NAME} PUBLIC M3DKernel ModuleManager Reader Boost::unit_test_framework)

add_test(NAME ${PROJECT_NAME} COMMAND $<TARGET_FILE:${PROJECT_NAME}>)