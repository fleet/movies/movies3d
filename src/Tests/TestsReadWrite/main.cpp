#define BOOST_TEST_MODULE TestsReadWrite
#include <boost/test/unit_test.hpp>

#include "M3DKernel/M3DKernel.h"
#include "Reader/MovReadService.h"
#include "Reader/ReaderCtrl.h"
#include "Reader/WriteAlgorithm/WriteAlgorithm.h"
#include "ModuleManager/ModuleManager.h"

void run_readwrite()
{
	/// LOAD SOURCE FILE
	auto readerCtrl = ReaderCtrl::getInstance();

	MovFileRun mFileRun;
	mFileRun.SetSourceName("../datas", ".hac");

	readerCtrl->OpenFileStream(mFileRun);
	readerCtrl->ReadSounderConfig();

	/// START WRITE
	auto writeAlgorithm = readerCtrl->GetWriteAlgorithm();
	writeAlgorithm->CloseFile();
	writeAlgorithm->SetDirectory(".");
	writeAlgorithm->SetOutputType(WriteAlgorithm::OutputType::Hac);

	/// READ AND WRITE
	bool isClosed = readerCtrl->GetActiveService()->IsClosed();
	while (!isClosed)
	{
		readerCtrl->SyncReadChunk();
		ChunckEventList ref;
		readerCtrl->WaitEndChunck(ref);

		isClosed = readerCtrl->GetActiveService()->IsClosed();
	}

	/// STOP WRITE
	writeAlgorithm->CloseFile();
}


BOOST_AUTO_TEST_CASE(ReadWrite)
{
	run_readwrite();
}