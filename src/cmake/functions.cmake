# Macro for basic configuration of a project
macro(basic_configure)

    list(APPEND INCLUDES_DIRS ${PROJECT_SOURCE_DIR}/include)
   
    file(GLOB_RECURSE MODULE_HEADERS ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME}/*.h)
    file(GLOB_RECURSE MODULE_SOURCES ${PROJECT_SOURCE_DIR}/src/*.cpp ${PROJECT_SOURCE_DIR}/src/*.c)

    # Pour grouper les sources dans Visual Studio
    source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR}/include/${PROJECT_NAME} PREFIX headers FILES ${MODULE_HEADERS})
    source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR}/src PREFIX sources FILES ${MODULE_SOURCES})

    # Configuration du header et du rc (remplacement des numéros de version)
    configure_file(
        ${CMAKE_SOURCE_DIR}/cmake/template.version.h.in
        ${PROJECT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}.version.h )
    list(APPEND MODULE_HEADERS ${PROJECT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}.version.h)

    configure_file(
        ${CMAKE_SOURCE_DIR}/cmake/template.rc.in
        ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.rc )
    list(APPEND MODULE_SOURCES ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.rc)

    # Configuration du header et du rc (remplacement des numéros de version)
    #configure_file(
    #    ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME}/${PROJECT_NAME}.version.h.in
    #    ${PROJECT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}.version.h )
    #LIST(APPEND MODULE_HEADERS ${PROJECT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}.version.h)

    #if(WIN32)
    #configure_file(
    #    ${PROJECT_SOURCE_DIR}/src/${PROJECT_NAME}.rc.in
    #    ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.rc )
    #    LIST(APPEND MODULE_SOURCES ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.rc)
    #endif(WIN32)

    if(WIN32)

    if(MSVC)

    else(MSVC)

    set(CMAKE_CXX_FLAGS "-g -fno-omit-frame-pointer -gdwarf-3 -msse -msse2  ${CMAKE_CXX_FLAGS}")
    set(CMAKE_C_FLAGS  "-g -fno-omit-frame-pointer -gdwarf-3 ${CMAKE_C_FLAGS}")

    endif(MSVC)

    else(WIN32)

    set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)
    set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
    set(CMAKE_INSTALL_RPATH "$ORIGIN;$ORIGIN/../lib")

    endif(WIN32)

endmacro()

# Function to copy a file
function(copy_file targetFile dependency destination)
  
  add_custom_command(TARGET ${dependency} POST_BUILD
    COMMAND ${CMAKE_COMMAND} ARGS -E make_directory ${destination} )
  
  add_custom_command(TARGET ${dependency} POST_BUILD
    COMMENT "Copy file ${targetFile} to ${destination}"
    COMMAND ${CMAKE_COMMAND} ARGS -E copy_if_different ${targetFile} ${destination}/ )

endfunction()

# Function to deploy a file
function(deploy_files)

  set(options )
  set(oneValueArgs DEPENDENCY COMPONENT DESTINATION)
  set(multiValueArgs FILES)
  cmake_parse_arguments(DEPLOY_FILES "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  #add_custom_command(TARGET ${DEPLOY_FILE_DEPENDENCY} POST_BUILD
  #  COMMAND ${CMAKE_COMMAND} ARGS -E make_directory ${DEPLOY_FILE_DESTINATION} )
  
  foreach(tgtFile ${DEPLOY_FILES_FILES})
    add_custom_command(TARGET ${DEPLOY_FILES_DEPENDENCY} POST_BUILD
      COMMENT "Copy file ${tgtFile} to ${DEPLOY_FILES_DESTINATION}"
      COMMAND ${CMAKE_COMMAND} ARGS -E copy_if_different ${tgtFile} ${CMAKE_BINARY_DIR}/dist/$<CONFIG>/${DEPLOY_FILES_DESTINATION}/ )

    install(FILES ${tgtFile} DESTINATION ${DEPLOY_FILES_DESTINATION} COMPONENT ${DEPLOY_FILES_COMPONENT} )
  endforeach()
endfunction()

# Function to deploy an extern dependency (DLL)
function(deploy_dll)

  set(options )
  set(oneValueArgs DEPENDENCY COMPONENT)
  set(multiValueArgs TARGETS)
  cmake_parse_arguments(DEPLOY_DLL "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  foreach(tgt ${DEPLOY_DLL_TARGETS})

    add_custom_command(TARGET ${DEPLOY_DLL_DEPENDENCY} POST_BUILD
      COMMENT "Copy file $<TARGET_FILE:${tgt}> to ${destination}"
      COMMAND ${CMAKE_COMMAND} ARGS -E copy_if_different $<TARGET_FILE:${tgt}> ${CMAKE_LIBRARY_OUTPUT_DIRECTORY} )
  
    install(FILES $<TARGET_FILE:${tgt}>
      TYPE BIN 
      COMPONENT ${DEPLOY_DLL_COMPONENT})

  endforeach()
endfunction()